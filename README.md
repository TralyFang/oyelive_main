# oyelive_main

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


## 注意事项
### Assets - Images
```
    1.根据模块新建子文件夹
    2.子文件里面资源以文件名称做前缀
```
### Log
```
    尽量使用 Logger , 根据场景选择具体级别(v\i\e...)
    测试部分print代码尽量删除
```
### git
```
    提交信息的格式：feat等关键字 + 冒号 + 空格 + 描述
          eg：feat: 描述信息xxxx
          feat: 新特性
          fix: 修复问题
          docs: 文档修改
          perf: 提升性能的修改
          style: 代码格式修改
          refactor: 代码重构
          test: 测试用例修改
          chore: 其他修改, 比如构建流程, 依赖管理
          reset: 代码回滚
          upgrade: 依赖升级
```
### Version
```
当前版本2.7.4 buildcode 255
测试打包版本规则：2.73.1+255
测试包递增号：2.73.2+255

测试环境需要更改const.dart
Const.TEST_ENV = true;

日志级别log_util.dart
TPLogLevel.output = Output.Verbose;

```