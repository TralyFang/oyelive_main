import 'package:oyelive_main/ui/page/status/config/slog_ybd_config_from_server.dart';
import 'dart:async';

yBDSLogConfigFromServerFromJson(YBDSLogConfigFromServer data, Map<String, dynamic> json) {
	if (json['max_picture'] != null) {
		data.max_picture = json['max_picture'] is String
				? int.tryParse(json['max_picture'])
				: json['max_picture'].toInt();
	}
	if (json['max_record_duration'] != null) {
		data.max_record_duration = json['max_record_duration'] is String
				? int.tryParse(json['max_record_duration'])
				: json['max_record_duration'].toInt();
	}
	if (json['min_record_duration'] != null) {
		data.min_record_duration = json['min_record_duration'] is String
				? int.tryParse(json['min_record_duration'])
				: json['min_record_duration'].toInt();
	}
	if (json['bg_changing_interval'] != null) {
		data.bg_changing_interval = json['bg_changing_interval'] is String
				? int.tryParse(json['bg_changing_interval'])
				: json['bg_changing_interval'].toInt();
	}
	if (json['max_record_clip'] != null) {
		data.max_record_clip = json['max_record_clip'] is String
				? int.tryParse(json['max_record_clip'])
				: json['max_record_clip'].toInt();
	}
	if (json['max_text_length'] != null) {
		data.max_text_length = json['max_text_length'] is String
				? int.tryParse(json['max_text_length'])
				: json['max_text_length'].toInt();
	}
	if (json['max_text_line'] != null) {
		data.max_text_line = json['max_text_line'] is String
				? int.tryParse(json['max_text_line'])
				: json['max_text_line'].toInt();
	}
	if (json['min_play_visibility_percentage'] != null) {
		data.min_play_visibility_percentage = json['min_play_visibility_percentage'] is String
				? int.tryParse(json['min_play_visibility_percentage'])
				: json['min_play_visibility_percentage'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDSLogConfigFromServerToJson(YBDSLogConfigFromServer entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['max_picture'] = entity.max_picture;
	data['max_record_duration'] = entity.max_record_duration;
	data['min_record_duration'] = entity.min_record_duration;
	data['bg_changing_interval'] = entity.bg_changing_interval;
	data['max_record_clip'] = entity.max_record_clip;
	data['max_text_length'] = entity.max_text_length;
	data['max_text_line'] = entity.max_text_line;
	data['min_play_visibility_percentage'] = entity.min_play_visibility_percentage;
	return data;
}