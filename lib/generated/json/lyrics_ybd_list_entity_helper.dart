import 'package:oyelive_main/module/status/entity/lyrics_ybd_list_entity.dart';
import 'package:oyelive_main/module/status/entity/random_ybd_lyric_entity.dart';

yBDLyricsListEntityFromJson(YBDLyricsListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDLyricsListData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDLyricsListEntityToJson(YBDLyricsListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['message'] = entity.message;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDLyricsListDataFromJson(YBDLyricsListData data, Map<String, dynamic> json) {
	if (json['rows'] != null) {
		data.rows = (json['rows'] as List).map((v) => YBDRandomLyricDataLyric().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDLyricsListDataToJson(YBDLyricsListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rows'] =  entity.rows?.map((v) => v?.toJson())?.toList();
	return data;
}