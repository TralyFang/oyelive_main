import 'package:oyelive_main/ui/page/game_room/entity/chat_ybd_publish.dart';
import 'dart:async';
import 'package:oyelive_main/ui/widget/rich_ybd_text_decoder.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

yBDChatPublishFromJson(YBDChatPublish data, Map<String, dynamic> json) {
	if (json['body'] != null) {
		data.body = YBDChatPublishBody().fromJson(json['body']);
	}
	if (json['resources'] != null) {
		data.resources = YBDResources().fromJson(json['resources']);
	}
	return data;
}

Map<String, dynamic> yBDChatPublishToJson(YBDChatPublish entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['body'] = entity.body?.toJson();
	data['resources'] = entity.resources?.toJson();
	return data;
}

yBDChatPublishBodyFromJson(YBDChatPublishBody data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['sender'] != null) {
		data.sender = json['sender'].toString();
	}
	if (json['content'] != null) {
		data.content = YBDPlainText().fromJson(json['content']);
	}
	return data;
}

Map<String, dynamic> yBDChatPublishBodyToJson(YBDChatPublishBody entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['sender'] = entity.sender;
	data['content'] = entity.content?.toJson();
	return data;
}