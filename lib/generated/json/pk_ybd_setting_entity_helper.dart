import 'package:oyelive_main/module/entity/pk_ybd_setting_entity.dart';

yBDPkSettingEntityFromJson(YBDPkSettingEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDPkSettingRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDPkSettingEntityToJson(YBDPkSettingEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDPkSettingRecordFromJson(YBDPkSettingRecord data, Map<String, dynamic> json) {
	if (json['rejectFriends'] != null) {
		data.rejectFriends = json['rejectFriends'];
	}
	if (json['rejectSystem'] != null) {
		data.rejectSystem = json['rejectSystem'];
	}
	if (json['timeZone'] != null) {
		data.timeZone = json['timeZone'];
	}
	return data;
}

Map<String, dynamic> yBDPkSettingRecordToJson(YBDPkSettingRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rejectFriends'] = entity.rejectFriends;
	data['rejectSystem'] = entity.rejectSystem;
	data['timeZone'] = entity.timeZone;
	return data;
}