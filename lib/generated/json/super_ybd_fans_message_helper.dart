import 'package:oyelive_main/common/room_socket/message/common/super_ybd_fans_message.dart';
import 'package:oyelive_main/common/room_socket/entity/fans_ybd_info.dart';
import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';

yBDSuperFansMessageFromJson(YBDSuperFansMessage data, Map<String, dynamic> json) {
	if (json['content'] != null) {
		data.content = YBDFansContent().fromJson(json['content']);
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['receiver'] != null) {
		data.receiver = json['receiver'].toString();
	}
	if (json['receiverImg'] != null) {
		data.receiverImg = json['receiverImg'].toString();
	}
	if (json['receiverLevel'] != null) {
		data.receiverLevel = json['receiverLevel'] is String
				? int.tryParse(json['receiverLevel'])
				: json['receiverLevel'].toInt();
	}
	if (json['receiverSex'] != null) {
		data.receiverSex = json['receiverSex'] is String
				? int.tryParse(json['receiverSex'])
				: json['receiverSex'].toInt();
	}
	if (json['receiverVip'] != null) {
		data.receiverVip = json['receiverVip'] is String
				? int.tryParse(json['receiverVip'])
				: json['receiverVip'].toInt();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['sender'] != null) {
		data.sender = json['sender'].toString();
	}
	if (json['senderImg'] != null) {
		data.senderImg = json['senderImg'].toString();
	}
	if (json['senderLevel'] != null) {
		data.senderLevel = json['senderLevel'] is String
				? int.tryParse(json['senderLevel'])
				: json['senderLevel'].toInt();
	}
	if (json['senderSex'] != null) {
		data.senderSex = json['senderSex'] is String
				? int.tryParse(json['senderSex'])
				: json['senderSex'].toInt();
	}
	if (json['senderVip'] != null) {
		data.senderVip = json['senderVip'] is String
				? int.tryParse(json['senderVip'])
				: json['senderVip'].toInt();
	}
	if (json['time'] != null) {
		data.time = json['time'].toString();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDSuperFansMessageToJson(YBDSuperFansMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['content'] = entity.content?.toJson();
	data['destination'] = entity.destination;
	data['fromUser'] = entity.fromUser;
	data['mode'] = entity.mode;
	data['msgType'] = entity.msgType;
	data['receiver'] = entity.receiver;
	data['receiverImg'] = entity.receiverImg;
	data['receiverLevel'] = entity.receiverLevel;
	data['receiverSex'] = entity.receiverSex;
	data['receiverVip'] = entity.receiverVip;
	data['roomId'] = entity.roomId;
	data['sender'] = entity.sender;
	data['senderImg'] = entity.senderImg;
	data['senderLevel'] = entity.senderLevel;
	data['senderSex'] = entity.senderSex;
	data['senderVip'] = entity.senderVip;
	data['time'] = entity.time;
	data['toUser'] = entity.toUser;
	return data;
}

yBDFansContentFromJson(YBDFansContent data, Map<String, dynamic> json) {
	if (json['fans'] != null) {
		data.fans = (json['fans'] as List).map((v) => YBDFansInfo().fromJson(v)).toList();
	}
	if (json['superFans'] != null) {
		data.superFans = (json['superFans'] as List).map((v) => YBDFansInfo().fromJson(v)).toList();
	}
	if (json['dailyFans'] != null) {
		data.dailyFans = (json['dailyFans'] as List).map((v) => YBDFansInfo().fromJson(v)).toList();
	}
	if (json['weeklyFans'] != null) {
		data.weeklyFans = (json['weeklyFans'] as List).map((v) => YBDFansInfo().fromJson(v)).toList();
	}
	if (json['monthlyFans'] != null) {
		data.monthlyFans = (json['monthlyFans'] as List).map((v) => YBDFansInfo().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDFansContentToJson(YBDFansContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['fans'] =  entity.fans?.map((v) => v?.toJson())?.toList();
	data['superFans'] =  entity.superFans?.map((v) => v?.toJson())?.toList();
	data['dailyFans'] =  entity.dailyFans?.map((v) => v?.toJson())?.toList();
	data['weeklyFans'] =  entity.weeklyFans?.map((v) => v?.toJson())?.toList();
	data['monthlyFans'] =  entity.monthlyFans?.map((v) => v?.toJson())?.toList();
	return data;
}