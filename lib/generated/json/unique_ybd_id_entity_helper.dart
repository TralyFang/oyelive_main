import 'package:oyelive_main/module/entity/unique_ybd_id_entity.dart';

yBDUniqueIDEntityFromJson(YBDUniqueIDEntity data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['uniqueId'] != null) {
		data.uniqueId = json['uniqueId'] is String
				? int.tryParse(json['uniqueId'])
				: json['uniqueId'].toInt();
	}
	if (json['validity'] != null) {
		data.validity = json['validity'].toString();
	}
	return data;
}

Map<String, dynamic> yBDUniqueIDEntityToJson(YBDUniqueIDEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['uniqueId'] = entity.uniqueId;
	data['validity'] = entity.validity;
	return data;
}