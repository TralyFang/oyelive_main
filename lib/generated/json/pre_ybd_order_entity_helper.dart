import 'package:oyelive_main/common/service/iap_service/entity/pre_ybd_order_entity.dart';

yBDPreOrderEntityFromJson(YBDPreOrderEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDPreOrderData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDPreOrderEntityToJson(YBDPreOrderEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['message'] = entity.message;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDPreOrderDataFromJson(YBDPreOrderData data, Map<String, dynamic> json) {
	if (json['money'] != null) {
		data.money = json['money'] is String
				? int.tryParse(json['money'])
				: json['money'].toInt();
	}
	if (json['orderId'] != null) {
		data.orderId = json['orderId'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPreOrderDataToJson(YBDPreOrderData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['money'] = entity.money;
	data['orderId'] = entity.orderId;
	data['userId'] = entity.userId;
	return data;
}