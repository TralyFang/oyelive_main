import 'package:oyelive_main/module/status/entity/status_ybd_list_entity.dart';
import 'package:oyelive_main/module/status/entity/status_ybd_entity.dart';

yBDStatusListEntityFromJson(YBDStatusListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDStatusListData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDStatusListEntityToJson(YBDStatusListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['message'] = entity.message;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDStatusListDataFromJson(YBDStatusListData data, Map<String, dynamic> json) {
	if (json['list'] != null) {
		data.xList = YBDStatusListDataList().fromJson(json['list']);
	}
	if (json['tops'] != null) {
		data.tops = (json['tops'] as List).map((v) => YBDtopContent().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDStatusListDataToJson(YBDStatusListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['list'] = entity.xList?.toJson();
	data['tops'] =  entity.tops?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDStatusListDataListFromJson(YBDStatusListDataList data, Map<String, dynamic> json) {
	if (json['rows'] != null) {
		data.rows = (json['rows'] as List).map((v) => YBDStatusInfo().fromJson(v)).toList();
	}
	if (json['total'] != null) {
		data.total = json['total'] is String
				? int.tryParse(json['total'])
				: json['total'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDStatusListDataListToJson(YBDStatusListDataList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rows'] =  entity.rows?.map((v) => v?.toJson())?.toList();
	data['total'] = entity.total;
	return data;
}

yBDtopContentFromJson(YBDtopContent data, Map<String, dynamic> json) {
	if (json['no'] != null) {
		data.no = json['no'] is String
				? int.tryParse(json['no'])
				: json['no'].toInt();
	}
	if (json['status'] != null) {
		data.status = YBDStatusInfo().fromJson(json['status']);
	}
	return data;
}

Map<String, dynamic> yBDtopContentToJson(YBDtopContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['no'] = entity.no;
	data['status'] = entity.status?.toJson();
	return data;
}