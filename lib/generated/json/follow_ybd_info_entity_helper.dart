import 'package:oyelive_main/ui/page/home/entity/follow_ybd_info_entity.dart';
import 'dart:async';

yBDFollowInfoEntityFromJson(YBDFollowInfoEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDFollowInfoRecord().fromJson(json['record']);
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'];
	}
	return data;
}

Map<String, dynamic> yBDFollowInfoEntityToJson(YBDFollowInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDFollowInfoRecordFromJson(YBDFollowInfoRecord data, Map<String, dynamic> json) {
	if (json['users'] != null) {
		data.users = json['users'];
	}
	if (json['followed'] != null) {
		data.followed = (json['followed'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	if (json['fromid'] != null) {
		data.fromid = json['fromid'];
	}
	if (json['toid'] != null) {
		data.toid = json['toid'];
	}
	return data;
}

Map<String, dynamic> yBDFollowInfoRecordToJson(YBDFollowInfoRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['users'] = entity.users;
	data['followed'] = entity.followed;
	data['fromid'] = entity.fromid;
	data['toid'] = entity.toid;
	return data;
}