import 'package:oyelive_main/module/entity/new_ybd_result_entity.dart';

yBDNewResultEntityFromJson(YBDNewResultEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	return data;
}

Map<String, dynamic> yBDNewResultEntityToJson(YBDNewResultEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['message'] = entity.message;
	return data;
}