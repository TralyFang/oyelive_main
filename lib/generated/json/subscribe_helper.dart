import 'package:oyelive_main/common/room_socket/message/common/subscribe.dart';

yBDSubscribeFromJson(YBDSubscribe data, Map<String, dynamic> json) {
	if (json['protectData'] != null) {
		data.protectData = json['protectData'].toString();
	}
	return data;
}

Map<String, dynamic> yBDSubscribeToJson(YBDSubscribe entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['protectData'] = entity.protectData;
	return data;
}