import 'package:oyelive_main/module/status/entity/comment_ybd_list_entity.dart';

yBDCommentListEntityFromJson(YBDCommentListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDCommentListData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDCommentListEntityToJson(YBDCommentListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['message'] = entity.message;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDCommentListDataFromJson(YBDCommentListData data, Map<String, dynamic> json) {
	if (json['rows'] != null) {
		data.rows = (json['rows'] as List).map((v) => YBDCommantListDataRows().fromJson(v)).toList();
	}
	if (json['total'] != null) {
		data.total = json['total'] is String
				? int.tryParse(json['total'])
				: json['total'].toInt();
	}
	if (json['commentTotalCount'] != null) {
		data.commentTotalCount = json['commentTotalCount'] is String
				? int.tryParse(json['commentTotalCount'])
				: json['commentTotalCount'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDCommentListDataToJson(YBDCommentListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rows'] =  entity.rows?.map((v) => v?.toJson())?.toList();
	data['total'] = entity.total;
	data['commentTotalCount'] = entity.commentTotalCount;
	return data;
}

yBDCommantListDataRowsFromJson(YBDCommantListDataRows data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['tenantCode'] != null) {
		data.tenantCode = json['tenantCode'];
	}
	if (json['projectCode'] != null) {
		data.projectCode = json['projectCode'];
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'].toString();
	}
	if (json['creator'] != null) {
		data.creator = json['creator'];
	}
	if (json['creatorId'] != null) {
		data.creatorId = json['creatorId'].toString();
	}
	if (json['updateTime'] != null) {
		data.updateTime = json['updateTime'].toString();
	}
	if (json['updator'] != null) {
		data.updator = json['updator'];
	}
	if (json['updatorId'] != null) {
		data.updatorId = json['updatorId'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['deleted'] != null) {
		data.deleted = json['deleted'] is String
				? int.tryParse(json['deleted'])
				: json['deleted'].toInt();
	}
	if (json['remark'] != null) {
		data.remark = json['remark'];
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	if (json['statusId'] != null) {
		data.statusId = json['statusId'].toString();
	}
	if (json['statusUserId'] != null) {
		data.statusUserId = json['statusUserId'] is String
				? int.tryParse(json['statusUserId'])
				: json['statusUserId'].toInt();
	}
	if (json['content'] != null) {
		data.content = json['content'].toString();
	}
	if (json['likeCount'] != null) {
		data.likeCount = json['likeCount'] is String
				? int.tryParse(json['likeCount'])
				: json['likeCount'].toInt();
	}
	if (json['nickName'] != null) {
		data.nickName = json['nickName'].toString();
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	if (json['vip'] != null) {
		data.vip = json['vip'] is String
				? int.tryParse(json['vip'])
				: json['vip'].toInt();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'];
	}
	if (json['officialTalent'] != null) {
		data.officialTalent = json['officialTalent'];
	}
	if (json['like'] != null) {
		data.like = json['like'];
	}
	if (json['replyCount'] != null) {
		data.replyCount = json['replyCount'] is String
				? int.tryParse(json['replyCount'])
				: json['replyCount'].toInt();
	}
	if (json['replyList'] != null) {
		data.replyList = (json['replyList'] as List).map((v) => YBDReplyData().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDCommantListDataRowsToJson(YBDCommantListDataRows entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['tenantCode'] = entity.tenantCode;
	data['projectCode'] = entity.projectCode;
	data['version'] = entity.version;
	data['createTime'] = entity.createTime;
	data['creator'] = entity.creator;
	data['creatorId'] = entity.creatorId;
	data['updateTime'] = entity.updateTime;
	data['updator'] = entity.updator;
	data['updatorId'] = entity.updatorId;
	data['status'] = entity.status;
	data['deleted'] = entity.deleted;
	data['remark'] = entity.remark;
	data['userId'] = entity.userId;
	data['statusId'] = entity.statusId;
	data['statusUserId'] = entity.statusUserId;
	data['content'] = entity.content;
	data['likeCount'] = entity.likeCount;
	data['nickName'] = entity.nickName;
	data['sex'] = entity.sex;
	data['vip'] = entity.vip;
	data['avatar'] = entity.avatar;
	data['officialTalent'] = entity.officialTalent;
	data['like'] = entity.like;
	data['replyCount'] = entity.replyCount;
	data['replyList'] =  entity.replyList?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDReplyDataFromJson(YBDReplyData data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['tenantCode'] != null) {
		data.tenantCode = json['tenantCode'];
	}
	if (json['projectCode'] != null) {
		data.projectCode = json['projectCode'];
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'].toString();
	}
	if (json['creator'] != null) {
		data.creator = json['creator'];
	}
	if (json['creatorId'] != null) {
		data.creatorId = json['creatorId'].toString();
	}
	if (json['updateTime'] != null) {
		data.updateTime = json['updateTime'].toString();
	}
	if (json['updator'] != null) {
		data.updator = json['updator'];
	}
	if (json['updatorId'] != null) {
		data.updatorId = json['updatorId'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['deleted'] != null) {
		data.deleted = json['deleted'] is String
				? int.tryParse(json['deleted'])
				: json['deleted'].toInt();
	}
	if (json['remark'] != null) {
		data.remark = json['remark'];
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	if (json['statusId'] != null) {
		data.statusId = json['statusId'].toString();
	}
	if (json['statusUserId'] != null) {
		data.statusUserId = json['statusUserId'] is String
				? int.tryParse(json['statusUserId'])
				: json['statusUserId'].toInt();
	}
	if (json['content'] != null) {
		data.content = json['content'].toString();
	}
	if (json['likeCount'] != null) {
		data.likeCount = json['likeCount'] is String
				? int.tryParse(json['likeCount'])
				: json['likeCount'].toInt();
	}
	if (json['nickName'] != null) {
		data.nickName = json['nickName'].toString();
	}
	if (json['toUserId'] != null) {
		data.toUserId = json['toUserId'] is String
				? int.tryParse(json['toUserId'])
				: json['toUserId'].toInt();
	}
	if (json['toUserNickName'] != null) {
		data.toUserNickName = json['toUserNickName'].toString();
	}
	if (json['toCommentId'] != null) {
		data.toCommentId = json['toCommentId'] is String
				? int.tryParse(json['toCommentId'])
				: json['toCommentId'].toInt();
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	if (json['vip'] != null) {
		data.vip = json['vip'] is String
				? int.tryParse(json['vip'])
				: json['vip'].toInt();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'];
	}
	if (json['officialTalent'] != null) {
		data.officialTalent = json['officialTalent'];
	}
	if (json['like'] != null) {
		data.like = json['like'];
	}
	return data;
}

Map<String, dynamic> yBDReplyDataToJson(YBDReplyData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['tenantCode'] = entity.tenantCode;
	data['projectCode'] = entity.projectCode;
	data['version'] = entity.version;
	data['createTime'] = entity.createTime;
	data['creator'] = entity.creator;
	data['creatorId'] = entity.creatorId;
	data['updateTime'] = entity.updateTime;
	data['updator'] = entity.updator;
	data['updatorId'] = entity.updatorId;
	data['status'] = entity.status;
	data['deleted'] = entity.deleted;
	data['remark'] = entity.remark;
	data['userId'] = entity.userId;
	data['statusId'] = entity.statusId;
	data['statusUserId'] = entity.statusUserId;
	data['content'] = entity.content;
	data['likeCount'] = entity.likeCount;
	data['nickName'] = entity.nickName;
	data['toUserId'] = entity.toUserId;
	data['toUserNickName'] = entity.toUserNickName;
	data['toCommentId'] = entity.toCommentId;
	data['sex'] = entity.sex;
	data['vip'] = entity.vip;
	data['avatar'] = entity.avatar;
	data['officialTalent'] = entity.officialTalent;
	data['like'] = entity.like;
	return data;
}