import 'package:oyelive_main/ui/page/home/entity/daily_ybd_task_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/home/entity/daily_ybd_check_entity.dart';

yBDMyDailyTaskEntityFromJson(YBDMyDailyTaskEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDDailyCheckRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDMyDailyTaskEntityToJson(YBDMyDailyTaskEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}