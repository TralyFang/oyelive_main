import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_mic_info_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

yBDGameMicInfoEntityFromJson(YBDGameMicInfoEntity data, Map<String, dynamic> json) {
	if (json['body'] != null) {
		data.body = YBDGameMicInfoBody().fromJson(json['body']);
	}
	if (json['cmd'] != null) {
		data.cmd = json['cmd'].toString();
	}
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['codeMsg'] != null) {
		data.codeMsg = json['codeMsg'].toString();
	}
	if (json['resources'] != null) {
		data.resources = YBDGameMicInfoResources().fromJson(json['resources']);
	}
	if (json['route'] != null) {
		data.route = YBDGameMicInfoRoute().fromJson(json['route']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDGameMicInfoEntityToJson(YBDGameMicInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['body'] = entity.body?.toJson();
	data['cmd'] = entity.cmd;
	data['code'] = entity.code;
	data['codeMsg'] = entity.codeMsg;
	data['resources'] = entity.resources?.toJson();
	data['route'] = entity.route?.toJson();
	data['success'] = entity.success;
	return data;
}

yBDGameMicInfoRouteFromJson(YBDGameMicInfoRoute data, Map<String, dynamic> json) {
	if (json['from'] != null) {
		data.from = json['from'].toString();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['room'] != null) {
		data.room = json['room'].toString();
	}
	if (json['to'] != null) {
		data.to = (json['to'] as List).map((v) => v.toString()).toList().cast<String>();
	}
	return data;
}

Map<String, dynamic> yBDGameMicInfoRouteToJson(YBDGameMicInfoRoute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['from'] = entity.from;
	data['mode'] = entity.mode;
	data['room'] = entity.room;
	data['to'] = entity.to;
	return data;
}

yBDGameMicInfoBodyFromJson(YBDGameMicInfoBody data, Map<String, dynamic> json) {
	if (json['mic'] != null) {
		data.mic = (json['mic'] as List).map((v) => YBDGameMicInfoBodyMic().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDGameMicInfoBodyToJson(YBDGameMicInfoBody entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['mic'] =  entity.mic?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDGameMicInfoBodyMicFromJson(YBDGameMicInfoBodyMic data, Map<String, dynamic> json) {
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameMicInfoBodyMicToJson(YBDGameMicInfoBodyMic entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['index'] = entity.index;
	data['roomId'] = entity.roomId;
	data['status'] = entity.status;
	data['userId'] = entity.userId;
	data['version'] = entity.version;
	data['avatar'] = entity.avatar;
	return data;
}

yBDGameMicInfoResourcesFromJson(YBDGameMicInfoResources data, Map<String, dynamic> json) {
	if (json['user'] != null) {
		data.user = json['user'];
	}
	if (json['userAttribute'] != null) {
		data.userAttribute = YBDGameMicInfoResourcesUserAttribute().fromJson(json['userAttribute']);
	}
	return data;
}

Map<String, dynamic> yBDGameMicInfoResourcesToJson(YBDGameMicInfoResources entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['user'] = entity.user;
	data['userAttribute'] = entity.userAttribute?.toJson();
	return data;
}

yBDGameMicInfoResourcesUserAttributeFromJson(YBDGameMicInfoResourcesUserAttribute data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['identity'] != null) {
		data.identity = json['identity'].toString();
	}
	if (json['value'] != null) {
		data.value = YBDGameMicInfoResourcesUserAttributeValue().fromJson(json['value']);
	}
	if (json['expireAt'] != null) {
		data.expireAt = json['expireAt'] is String
				? int.tryParse(json['expireAt'])
				: json['expireAt'].toInt();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGameMicInfoResourcesUserAttributeToJson(YBDGameMicInfoResourcesUserAttribute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['name'] = entity.name;
	data['identity'] = entity.identity;
	data['value'] = entity.value?.toJson();
	data['expireAt'] = entity.expireAt;
	data['version'] = entity.version;
	return data;
}

yBDGameMicInfoResourcesUserAttributeValueFromJson(YBDGameMicInfoResourcesUserAttributeValue data, Map<String, dynamic> json) {
	return data;
}

Map<String, dynamic> yBDGameMicInfoResourcesUserAttributeValueToJson(YBDGameMicInfoResourcesUserAttributeValue entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	return data;
}