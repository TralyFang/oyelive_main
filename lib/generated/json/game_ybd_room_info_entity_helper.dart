import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_info_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

yBDGameRoomInfoEntityFromJson(YBDGameRoomInfoEntity data, Map<String, dynamic> json) {
	if (json['body'] != null) {
		data.body = YBDGameRoomInfoBody().fromJson(json['body']);
	}
	if (json['cmd'] != null) {
		data.cmd = json['cmd'].toString();
	}
	if (json['route'] != null) {
		data.route = YBDGameRoomInfoRoute().fromJson(json['route']);
	}
	if (json['ver'] != null) {
		data.ver = json['ver'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomInfoEntityToJson(YBDGameRoomInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['body'] = entity.body?.toJson();
	data['cmd'] = entity.cmd;
	data['route'] = entity.route?.toJson();
	data['ver'] = entity.ver;
	return data;
}

yBDGameRoomInfoBodyFromJson(YBDGameRoomInfoBody data, Map<String, dynamic> json) {
	if (json['userCount'] != null) {
		data.userCount = json['userCount'] is String
				? int.tryParse(json['userCount'])
				: json['userCount'].toInt();
	}
	if (json['property'] != null) {
		data.property = YBDGameRoomInfoBodyProperty().fromJson(json['property']);
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomInfoBodyToJson(YBDGameRoomInfoBody entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userCount'] = entity.userCount;
	data['property'] = entity.property?.toJson();
	data['roomId'] = entity.roomId;
	return data;
}

yBDGameRoomInfoRouteFromJson(YBDGameRoomInfoRoute data, Map<String, dynamic> json) {
	if (json['from'] != null) {
		data.from = json['from'].toString();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['room'] != null) {
		data.room = json['room'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomInfoRouteToJson(YBDGameRoomInfoRoute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['from'] = entity.from;
	data['mode'] = entity.mode;
	data['room'] = entity.room;
	return data;
}

yBDGameRoomInfoBodyPropertyFromJson(YBDGameRoomInfoBodyProperty data, Map<String, dynamic> json) {
	if (json['backgroundImg'] != null) {
		data.backgroundImg = json['backgroundImg'].toString();
	}
	if (json['roomLevel'] != null) {
		data.roomLevel = json['roomLevel'] is String
				? int.tryParse(json['roomLevel'])
				: json['roomLevel'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['poster'] != null) {
		data.poster = json['poster'].toString();
	}
	if (json['theme'] != null) {
		data.theme = YBDGameRoomInfoBodyPropertyTheme().fromJson(json['theme']);
	}
	return data;
}

Map<String, dynamic> yBDGameRoomInfoBodyPropertyToJson(YBDGameRoomInfoBodyProperty entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['backgroundImg'] = entity.backgroundImg;
	data['roomLevel'] = entity.roomLevel;
	data['name'] = entity.name;
	data['id'] = entity.id;
	data['title'] = entity.title;
	data['poster'] = entity.poster;
	data['theme'] = entity.theme?.toJson();
	return data;
}

yBDGameRoomInfoBodyPropertyThemeFromJson(YBDGameRoomInfoBodyPropertyTheme data, Map<String, dynamic> json) {
	if (json['resource'] != null) {
		data.resource = json['resource'].toString();
	}
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['attribute'] != null) {
		data.attribute = json['attribute'];
	}
	return data;
}

Map<String, dynamic> yBDGameRoomInfoBodyPropertyThemeToJson(YBDGameRoomInfoBodyPropertyTheme entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['resource'] = entity.resource;
	data['type'] = entity.type;
	data['name'] = entity.name;
	data['attribute'] = entity.attribute;
	return data;
}