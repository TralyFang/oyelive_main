import 'package:oyelive_main/common/service/iap_service/entity/iap_ybd_order_list_entity.dart';

yBDIapOrderListEntityFromJson(YBDIapOrderListEntity data, Map<String, dynamic> json) {
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDIapOrderListRecord().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDIapOrderListEntityToJson(YBDIapOrderListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDIapOrderListRecordFromJson(YBDIapOrderListRecord data, Map<String, dynamic> json) {
	if (json['productId'] != null) {
		data.productId = json['productId'].toString();
	}
	if (json['orderId'] != null) {
		data.orderId = json['orderId'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDIapOrderListRecordToJson(YBDIapOrderListRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['productId'] = entity.productId;
	data['orderId'] = entity.orderId;
	data['userId'] = entity.userId;
	return data;
}