import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_record_extend.dart';
import 'dart:async';

yBDCarListRecordExtendFromJson(YBDCarListRecordExtend data, Map<String, dynamic> json) {
	if (json['replacedSvgaImageKey'] != null) {
		data.replacedSvgaImageKey = json['replacedSvgaImageKey'].toString();
	}
	if (json['replacedSvgaImage'] != null) {
		data.replacedSvgaImage = json['replacedSvgaImage'].toString();
	}
	if (json['replacedStaticPictureKey'] != null) {
		data.replacedStaticPictureKey = json['replacedStaticPictureKey'].toString();
	}
	if (json['replacedStaticPicture'] != null) {
		data.replacedStaticPicture = json['replacedStaticPicture'].toString();
	}
	return data;
}

Map<String, dynamic> yBDCarListRecordExtendToJson(YBDCarListRecordExtend entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['replacedSvgaImageKey'] = entity.replacedSvgaImageKey;
	data['replacedSvgaImage'] = entity.replacedSvgaImage;
	data['replacedStaticPictureKey'] = entity.replacedStaticPictureKey;
	data['replacedStaticPicture'] = entity.replacedStaticPicture;
	return data;
}