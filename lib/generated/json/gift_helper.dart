import 'package:oyelive_main/common/room_socket/message/common/gift.dart';
import 'dart:convert';
import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

yBDGiftFromJson(YBDGift data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'] is String
				? int.tryParse(json['code'])
				: json['code'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['num'] != null) {
		data.num = json['num'] is String
				? int.tryParse(json['num'])
				: json['num'].toInt();
	}
	if (json['imgUrl'] != null) {
		data.imgUrl = json['imgUrl'].toString();
	}
	if (json['animationUrl'] != null) {
		data.animationUrl = json['animationUrl'].toString();
	}
	if (json['animation'] != null) {
		data.animation = json['animation'].toString();
	}
	if (json['senderVipIcon'] != null) {
		data.senderVipIcon = json['senderVipIcon'].toString();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['combo'] != null) {
		data.combo = json['combo'] is String
				? int.tryParse(json['combo'])
				: json['combo'].toInt();
	}
	if (json['comboId'] != null) {
		data.comboId = json['comboId'] is String
				? int.tryParse(json['comboId'])
				: json['comboId'].toInt();
	}
	if (json['customized'] != null) {
		data.customized = json['customized'].toString();
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['receiver'] != null) {
		data.receiver = json['receiver'].toString();
	}
	if (json['receiverImg'] != null) {
		data.receiverImg = json['receiverImg'].toString();
	}
	if (json['receiverLevel'] != null) {
		data.receiverLevel = json['receiverLevel'] is String
				? int.tryParse(json['receiverLevel'])
				: json['receiverLevel'].toInt();
	}
	if (json['receiverSex'] != null) {
		data.receiverSex = json['receiverSex'] is String
				? int.tryParse(json['receiverSex'])
				: json['receiverSex'].toInt();
	}
	if (json['receiverVip'] != null) {
		data.receiverVip = json['receiverVip'] is String
				? int.tryParse(json['receiverVip'])
				: json['receiverVip'].toInt();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['sender'] != null) {
		data.sender = json['sender'].toString();
	}
	if (json['senderImg'] != null) {
		data.senderImg = json['senderImg'].toString();
	}
	if (json['senderLevel'] != null) {
		data.senderLevel = json['senderLevel'] is String
				? int.tryParse(json['senderLevel'])
				: json['senderLevel'].toInt();
	}
	if (json['senderSex'] != null) {
		data.senderSex = json['senderSex'] is String
				? int.tryParse(json['senderSex'])
				: json['senderSex'].toInt();
	}
	if (json['senderVip'] != null) {
		data.senderVip = json['senderVip'] is String
				? int.tryParse(json['senderVip'])
				: json['senderVip'].toInt();
	}
	if (json['time'] != null) {
		data.time = json['time'].toString();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGiftToJson(YBDGift entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['name'] = entity.name;
	data['num'] = entity.num;
	data['imgUrl'] = entity.imgUrl;
	data['animationUrl'] = entity.animationUrl;
	data['animation'] = entity.animation;
	data['senderVipIcon'] = entity.senderVipIcon;
	data['price'] = entity.price;
	data['combo'] = entity.combo;
	data['comboId'] = entity.comboId;
	data['customized'] = entity.customized;
	data['destination'] = entity.destination;
	data['fromUser'] = entity.fromUser;
	data['mode'] = entity.mode;
	data['msgType'] = entity.msgType;
	data['receiver'] = entity.receiver;
	data['receiverImg'] = entity.receiverImg;
	data['receiverLevel'] = entity.receiverLevel;
	data['receiverSex'] = entity.receiverSex;
	data['receiverVip'] = entity.receiverVip;
	data['roomId'] = entity.roomId;
	data['sender'] = entity.sender;
	data['senderImg'] = entity.senderImg;
	data['senderLevel'] = entity.senderLevel;
	data['senderSex'] = entity.senderSex;
	data['senderVip'] = entity.senderVip;
	data['time'] = entity.time;
	data['toUser'] = entity.toUser;
	return data;
}

yBDGiftCustomizedFromJson(YBDGiftCustomized data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = (json['data'] as List).map((v) => YBDGiftCustomizedData().fromJson(v)).toList();
	}
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGiftCustomizedToJson(YBDGiftCustomized entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['data'] =  entity.data?.map((v) => v?.toJson())?.toList();
	data['type'] = entity.type;
	return data;
}

yBDGiftCustomizedDataFromJson(YBDGiftCustomizedData data, Map<String, dynamic> json) {
	if (json['imgKey'] != null) {
		data.imgKey = json['imgKey'].toString();
	}
	if (json['imgPath'] != null) {
		data.imgPath = json['imgPath'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGiftCustomizedDataToJson(YBDGiftCustomizedData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['imgKey'] = entity.imgKey;
	data['imgPath'] = entity.imgPath;
	return data;
}