import 'package:oyelive_main/ui/page/home/entity/combo_ybd_rank_entity.dart';
import 'dart:async';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

yBDComboRankEntityFromJson(YBDComboRankEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDComboRankRecord().fromJson(json['record']);
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'];
	}
	return data;
}

Map<String, dynamic> yBDComboRankEntityToJson(YBDComboRankEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDComboRankRecordFromJson(YBDComboRankRecord data, Map<String, dynamic> json) {
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['dateType'] != null) {
		data.dateType = json['dateType'] is String
				? int.tryParse(json['dateType'])
				: json['dateType'].toInt();
	}
	if (json['labelId'] != null) {
		data.labelId = json['labelId'];
	}
	if (json['rank'] != null) {
		data.rank = (json['rank'] as List).map((v) => YBDComboRankRecordRank().fromJson(v)).toList();
	}
	if (json['scores'] != null) {
		data.scores = json['scores'];
	}
	return data;
}

Map<String, dynamic> yBDComboRankRecordToJson(YBDComboRankRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['name'] = entity.name;
	data['dateType'] = entity.dateType;
	data['labelId'] = entity.labelId;
	data['rank'] =  entity.rank?.map((v) => v?.toJson())?.toList();
	data['scores'] = entity.scores;
	return data;
}

yBDComboRankRecordRankFromJson(YBDComboRankRecordRank data, Map<String, dynamic> json) {
	if (json['giftId'] != null) {
		data.giftId = json['giftId'] is String
				? int.tryParse(json['giftId'])
				: json['giftId'].toInt();
	}
	if (json['giftName'] != null) {
		data.giftName = json['giftName'].toString();
	}
	if (json['giftImg'] != null) {
		data.giftImg = json['giftImg'].toString();
	}
	if (json['combo'] != null) {
		data.combo = json['combo'] is String
				? int.tryParse(json['combo'])
				: json['combo'].toInt();
	}
	if (json['sendTime'] != null) {
		data.sendTime = json['sendTime'] is String
				? int.tryParse(json['sendTime'])
				: json['sendTime'].toInt();
	}
	if (json['sender'] != null) {
		data.sender = YBDRoomInfo().fromJson(json['sender']);
	}
	if (json['receiver'] != null) {
		data.receiver = YBDRoomInfo().fromJson(json['receiver']);
	}
	return data;
}

Map<String, dynamic> yBDComboRankRecordRankToJson(YBDComboRankRecordRank entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['giftId'] = entity.giftId;
	data['giftName'] = entity.giftName;
	data['giftImg'] = entity.giftImg;
	data['combo'] = entity.combo;
	data['sendTime'] = entity.sendTime;
	data['sender'] = entity.sender?.toJson();
	data['receiver'] = entity.receiver?.toJson();
	return data;
}