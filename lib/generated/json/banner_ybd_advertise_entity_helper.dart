import 'package:oyelive_main/ui/page/home/entity/banner_ybd_advertise_entity.dart';
import 'dart:async';

yBDBannerAdvertiseEntityFromJson(YBDBannerAdvertiseEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDBannerAdvertiseRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDBannerAdvertiseEntityToJson(YBDBannerAdvertiseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDBannerAdvertiseRecordFromJson(YBDBannerAdvertiseRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['adname'] != null) {
		data.adname = json['adname'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['detail'] != null) {
		data.detail = json['detail'].toString();
	}
	if (json['adurl'] != null) {
		data.adurl = json['adurl'].toString();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['adtype'] != null) {
		data.adtype = json['adtype'] is String
				? int.tryParse(json['adtype'])
				: json['adtype'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['channel'] != null) {
		data.channel = json['channel'].toString();
	}
	return data;
}

Map<String, dynamic> yBDBannerAdvertiseRecordToJson(YBDBannerAdvertiseRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['adname'] = entity.adname;
	data['title'] = entity.title;
	data['detail'] = entity.detail;
	data['adurl'] = entity.adurl;
	data['img'] = entity.img;
	data['adtype'] = entity.adtype;
	data['index'] = entity.index;
	data['channel'] = entity.channel;
	return data;
}