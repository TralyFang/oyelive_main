import 'package:oyelive_main/ui/page/game_lobby/entity/function_ybd_parase_entity.dart';
import 'dart:async';

yBDFunctionParaseEntityFromJson(YBDFunctionParaseEntity data, Map<String, dynamic> json) {
	if (json['functionName'] != null) {
		data.functionName = json['functionName'].toString();
	}
	if (json['functionParam'] != null) {
		data.functionParam = (json['functionParam'] as List).map((v) => v.toString()).toList().cast<String>();
	}
	return data;
}

Map<String, dynamic> yBDFunctionParaseEntityToJson(YBDFunctionParaseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['functionName'] = entity.functionName;
	data['functionParam'] = entity.functionParam;
	return data;
}