import 'package:oyelive_main/common/room_socket/message/common/mic_ybd_user_message.dart';

yBDMicUserMessageFromJson(YBDMicUserMessage data, Map<String, dynamic> json) {
	if (json['operType'] != null) {
		data.operType = json['operType'] is String
				? int.tryParse(json['operType'])
				: json['operType'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDMicUserMessageToJson(YBDMicUserMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['operType'] = entity.operType;
	data['index'] = entity.index;
	return data;
}