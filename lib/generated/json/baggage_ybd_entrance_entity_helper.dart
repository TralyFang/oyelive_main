import 'package:oyelive_main/ui/page/baggage/entity/baggage_ybd_entrance_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_record_extend.dart';

yBDBaggageEntranceEntityFromJson(YBDBaggageEntranceEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDBaggageEntranceRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDBaggageEntranceEntityToJson(YBDBaggageEntranceEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDBaggageEntranceRecordFromJson(YBDBaggageEntranceRecord data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['itemList'] != null) {
		data.itemList = (json['itemList'] as List).map((v) => YBDBaggageEntranceRecordItemList().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDBaggageEntranceRecordToJson(YBDBaggageEntranceRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['itemList'] =  entity.itemList?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDBaggageEntranceRecordItemListFromJson(YBDBaggageEntranceRecordItemList data, Map<String, dynamic> json) {
	if (json['number'] != null) {
		data.number = json['number'] is String
				? int.tryParse(json['number'])
				: json['number'].toInt();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['personalId'] != null) {
		data.personalId = json['personalId'] is String
				? int.tryParse(json['personalId'])
				: json['personalId'].toInt();
	}
	if (json['equipped'] != null) {
		data.equipped = json['equipped'];
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['expireAfter'] != null) {
		data.expireAfter = json['expireAfter'] is String
				? int.tryParse(json['expireAfter'])
				: json['expireAfter'].toInt();
	}
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['animationInfo'] != null) {
		data.animationInfo = YBDCarListRecordAnimationInfo().fromJson(json['animationInfo']);
	}
	if (json['replaceDto'] != null) {
		data.replaceDto = YBDCarListRecordExtend().fromJson(json['replaceDto']);
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['conditionType'] != null) {
		data.conditionType = json['conditionType'] is String
				? int.tryParse(json['conditionType'])
				: json['conditionType'].toInt();
	}
	if (json['conditionExtends'] != null) {
		data.conditionExtends = json['conditionExtends'].toString();
	}
	return data;
}

Map<String, dynamic> yBDBaggageEntranceRecordItemListToJson(YBDBaggageEntranceRecordItemList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['number'] = entity.number;
	data['img'] = entity.img;
	data['personalId'] = entity.personalId;
	data['equipped'] = entity.equipped;
	data['price'] = entity.price;
	data['name'] = entity.name;
	data['expireAfter'] = entity.expireAfter;
	data['id'] = entity.id;
	data['animationInfo'] = entity.animationInfo?.toJson();
	data['replaceDto'] = entity.replaceDto?.toJson();
	data['currency'] = entity.currency;
	data['conditionType'] = entity.conditionType;
	data['conditionExtends'] = entity.conditionExtends;
	return data;
}