import 'package:oyelive_main/ui/page/room/entity/mute_ybd_Info.dart';
import 'dart:async';

yBDMuteInfoFromJson(YBDMuteInfo data, Map<String, dynamic> json) {
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['muteUserId'] != null) {
		data.muteUserId = json['muteUserId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDMuteInfoToJson(YBDMuteInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['status'] = entity.status;
	data['muteUserId'] = entity.muteUserId;
	return data;
}