import 'package:oyelive_main/ui/page/baggage/entity/baggage_ybd_gift_entity.dart';
import 'dart:async';

yBDBaggageGiftEntityFromJson(YBDBaggageGiftEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDBaggageGiftRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDBaggageGiftEntityToJson(YBDBaggageGiftEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDBaggageGiftRecordFromJson(YBDBaggageGiftRecord data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['itemList'] != null) {
		data.itemList = (json['itemList'] as List).map((v) => YBDBaggageGiftRecordItemList().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDBaggageGiftRecordToJson(YBDBaggageGiftRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['itemList'] =  entity.itemList?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDBaggageGiftRecordItemListFromJson(YBDBaggageGiftRecordItemList data, Map<String, dynamic> json) {
	if (json['number'] != null) {
		data.number = json['number'] is String
				? int.tryParse(json['number'])
				: json['number'].toInt();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['personalId'] != null) {
		data.personalId = json['personalId'] is String
				? int.tryParse(json['personalId'])
				: json['personalId'].toInt();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['expireAfter'] != null) {
		data.expireAfter = json['expireAfter'] is String
				? int.tryParse(json['expireAfter'])
				: json['expireAfter'].toInt();
	}
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'];
	}
	if (json['animationInfo'] != null) {
		data.animationInfo = YBDBaggageGiftRecordItemListAnimationInfo().fromJson(json['animationInfo']);
	}
	return data;
}

Map<String, dynamic> yBDBaggageGiftRecordItemListToJson(YBDBaggageGiftRecordItemList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['number'] = entity.number;
	data['img'] = entity.img;
	data['personalId'] = entity.personalId;
	data['price'] = entity.price;
	data['name'] = entity.name;
	data['expireAfter'] = entity.expireAfter;
	data['id'] = entity.id;
	data['endTime'] = entity.endTime;
	data['animationInfo'] = entity.animationInfo?.toJson();
	return data;
}

yBDBaggageGiftRecordItemListAnimationInfoFromJson(YBDBaggageGiftRecordItemListAnimationInfo data, Map<String, dynamic> json) {
	if (json['animationId'] != null) {
		data.animationId = json['animationId'] is String
				? int.tryParse(json['animationId'])
				: json['animationId'].toInt();
	}
	if (json['animationType'] != null) {
		data.animationType = json['animationType'] is String
				? int.tryParse(json['animationType'])
				: json['animationType'].toInt();
	}
	if (json['foldername'] != null) {
		data.foldername = json['foldername'].toString();
	}
	if (json['framDuration'] != null) {
		data.framDuration = json['framDuration'];
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDBaggageGiftRecordItemListAnimationInfoToJson(YBDBaggageGiftRecordItemListAnimationInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['animationId'] = entity.animationId;
	data['animationType'] = entity.animationType;
	data['foldername'] = entity.foldername;
	data['framDuration'] = entity.framDuration;
	data['name'] = entity.name;
	data['version'] = entity.version;
	return data;
}