import 'package:oyelive_main/common/widget/local_ybd_animation_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_entity.dart';

yBDLocalAnimationEntityFromJson(YBDLocalAnimationEntity data, Map<String, dynamic> json) {
	if (json['items'] != null) {
		data.items = (json['items'] as List).map((v) => YBDLocalAnimationItems().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDLocalAnimationEntityToJson(YBDLocalAnimationEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['items'] =  entity.items?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDLocalAnimationItemsFromJson(YBDLocalAnimationItems data, Map<String, dynamic> json) {
	if (json['svga'] != null) {
		data.svga = json['svga'].toString();
	}
	if (json['mp3'] != null) {
		data.mp3 = json['mp3'].toString();
	}
	if (json['animationId'] != null) {
		data.animationId = json['animationId'] is String
				? int.tryParse(json['animationId'])
				: json['animationId'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDLocalAnimationItemsToJson(YBDLocalAnimationItems entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['svga'] = entity.svga;
	data['mp3'] = entity.mp3;
	data['animationId'] = entity.animationId;
	data['index'] = entity.index;
	return data;
}