import 'package:oyelive_main/module/thirdparty/entity/geo_ybd_location_entity.dart';

yBDGeoLocationEntityFromJson(YBDGeoLocationEntity data, Map<String, dynamic> json) {
	if (json['country_code'] != null) {
		data.countryCode = json['country_code'].toString();
	}
	if (json['country_name'] != null) {
		data.countryName = json['country_name'].toString();
	}
	if (json['city'] != null) {
		data.city = json['city'].toString();
	}
	if (json['postal'] != null) {
		data.postal = json['postal'];
	}
	if (json['latitude'] != null) {
		data.latitude = json['latitude'] is String
				? double.tryParse(json['latitude'])
				: json['latitude'].toDouble();
	}
	if (json['longitude'] != null) {
		data.longitude = json['longitude'] is String
				? double.tryParse(json['longitude'])
				: json['longitude'].toDouble();
	}
	if (json['IPv4'] != null) {
		data.iPv4 = json['IPv4'].toString();
	}
	if (json['state'] != null) {
		data.state = json['state'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGeoLocationEntityToJson(YBDGeoLocationEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['country_code'] = entity.countryCode;
	data['country_name'] = entity.countryName;
	data['city'] = entity.city;
	data['postal'] = entity.postal;
	data['latitude'] = entity.latitude;
	data['longitude'] = entity.longitude;
	data['IPv4'] = entity.iPv4;
	data['state'] = entity.state;
	return data;
}