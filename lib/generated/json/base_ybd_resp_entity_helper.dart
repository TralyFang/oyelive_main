import 'package:oyelive_main/module/entity/base_ybd_resp_entity.dart';

yBDBaseRespEntityFromJson(YBDBaseRespEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	return data;
}

Map<String, dynamic> yBDBaseRespEntityToJson(YBDBaseRespEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	return data;
}