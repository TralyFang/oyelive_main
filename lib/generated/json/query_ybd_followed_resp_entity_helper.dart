import 'package:oyelive_main/module/entity/query_ybd_followed_resp_entity.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

yBDQueryFollowedRespEntityFromJson(YBDQueryFollowedRespEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDQueryFollowedRespRecord().fromJson(json['record']);
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'];
	}
	return data;
}

Map<String, dynamic> yBDQueryFollowedRespEntityToJson(YBDQueryFollowedRespEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDQueryFollowedRespRecordFromJson(YBDQueryFollowedRespRecord data, Map<String, dynamic> json) {
	if (json['users'] != null) {
		data.users = (json['users'] as List).map((v) => YBDRoomInfo().fromJson(v)).toList();
	}
	if (json['followed'] != null) {
		data.followed = (json['followed'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	if (json['fromid'] != null) {
		data.fromid = json['fromid'];
	}
	if (json['toid'] != null) {
		data.toid = json['toid'];
	}
	return data;
}

Map<String, dynamic> yBDQueryFollowedRespRecordToJson(YBDQueryFollowedRespRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['users'] =  entity.users?.map((v) => v?.toJson())?.toList();
	data['followed'] = entity.followed;
	data['fromid'] = entity.fromid;
	data['toid'] = entity.toid;
	return data;
}