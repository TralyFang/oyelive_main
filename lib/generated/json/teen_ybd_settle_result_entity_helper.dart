import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_settle_result_entity.dart';
import 'dart:async';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';

yBDTeenSettleResultEntityFromJson(YBDTeenSettleResultEntity data, Map<String, dynamic> json) {
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['costTime'] != null) {
		data.costTime = (json['costTime'] as List).map((v) => v).toList().cast<dynamic>();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['content'] != null) {
		data.content = YBDTeenSettleResultContent().fromJson(json['content']);
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDTeenSettleResultEntityToJson(YBDTeenSettleResultEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['roomId'] = entity.roomId;
	data['costTime'] = entity.costTime;
	data['msgType'] = entity.msgType;
	data['fromUser'] = entity.fromUser;
	data['toUser'] = entity.toUser;
	data['mode'] = entity.mode;
	data['type'] = entity.type;
	data['content'] = entity.content?.toJson();
	data['version'] = entity.version;
	return data;
}

yBDTeenSettleResultContentFromJson(YBDTeenSettleResultContent data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['balance'] != null) {
		data.balance = json['balance'] is String
				? int.tryParse(json['balance'])
				: json['balance'].toInt();
	}
	if (json['winCoins'] != null) {
		data.winCoins = json['winCoins'] is String
				? int.tryParse(json['winCoins'])
				: json['winCoins'].toInt();
	}
	if (json['gameId'] != null) {
		data.gameId = json['gameId'] is String
				? int.tryParse(json['gameId'])
				: json['gameId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDTeenSettleResultContentToJson(YBDTeenSettleResultContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['balance'] = entity.balance;
	data['winCoins'] = entity.winCoins;
	data['gameId'] = entity.gameId;
	return data;
}