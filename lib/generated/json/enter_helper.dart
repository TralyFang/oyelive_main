import 'package:oyelive_main/common/room_socket/message/common/enter.dart';
import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';

yBDEnterFromJson(YBDEnter data, Map<String, dynamic> json) {
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['receiver'] != null) {
		data.receiver = json['receiver'].toString();
	}
	if (json['receiverImg'] != null) {
		data.receiverImg = json['receiverImg'].toString();
	}
	if (json['receiverLevel'] != null) {
		data.receiverLevel = json['receiverLevel'] is String
				? int.tryParse(json['receiverLevel'])
				: json['receiverLevel'].toInt();
	}
	if (json['receiverSex'] != null) {
		data.receiverSex = json['receiverSex'] is String
				? int.tryParse(json['receiverSex'])
				: json['receiverSex'].toInt();
	}
	if (json['receiverVip'] != null) {
		data.receiverVip = json['receiverVip'] is String
				? int.tryParse(json['receiverVip'])
				: json['receiverVip'].toInt();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['sender'] != null) {
		data.sender = json['sender'].toString();
	}
	if (json['senderImg'] != null) {
		data.senderImg = json['senderImg'].toString();
	}
	if (json['senderLevel'] != null) {
		data.senderLevel = json['senderLevel'] is String
				? int.tryParse(json['senderLevel'])
				: json['senderLevel'].toInt();
	}
	if (json['senderSex'] != null) {
		data.senderSex = json['senderSex'] is String
				? int.tryParse(json['senderSex'])
				: json['senderSex'].toInt();
	}
	if (json['senderVip'] != null) {
		data.senderVip = json['senderVip'] is String
				? int.tryParse(json['senderVip'])
				: json['senderVip'].toInt();
	}
	if (json['time'] != null) {
		data.time = json['time'].toString();
	}
	if (json['senderVipIcon'] != null) {
		data.senderVipIcon = json['senderVipIcon'].toString();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['carName'] != null) {
		data.carName = json['carName'].toString();
	}
	if (json['carImg'] != null) {
		data.carImg = json['carImg'].toString();
	}
	if (json['carAnimation'] != null) {
		data.carAnimation = json['carAnimation'].toString();
	}
	if (json['carExtend'] != null) {
		data.carExtend = json['carExtend'].toString();
	}
	if (json['carId'] != null) {
		data.carId = json['carId'] is String
				? int.tryParse(json['carId'])
				: json['carId'].toInt();
	}
	if (json['userLevelEnterTip'] != null) {
		data.userLevelEnterTip = json['userLevelEnterTip'].toString();
	}
	if (json['userEnterMedal'] != null) {
		data.userEnterMedal = json['userEnterMedal'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEnterToJson(YBDEnter entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['destination'] = entity.destination;
	data['fromUser'] = entity.fromUser;
	data['mode'] = entity.mode;
	data['msgType'] = entity.msgType;
	data['receiver'] = entity.receiver;
	data['receiverImg'] = entity.receiverImg;
	data['receiverLevel'] = entity.receiverLevel;
	data['receiverSex'] = entity.receiverSex;
	data['receiverVip'] = entity.receiverVip;
	data['roomId'] = entity.roomId;
	data['sender'] = entity.sender;
	data['senderImg'] = entity.senderImg;
	data['senderLevel'] = entity.senderLevel;
	data['senderSex'] = entity.senderSex;
	data['senderVip'] = entity.senderVip;
	data['time'] = entity.time;
	data['senderVipIcon'] = entity.senderVipIcon;
	data['toUser'] = entity.toUser;
	data['carName'] = entity.carName;
	data['carImg'] = entity.carImg;
	data['carAnimation'] = entity.carAnimation;
	data['carExtend'] = entity.carExtend;
	data['carId'] = entity.carId;
	data['userLevelEnterTip'] = entity.userLevelEnterTip;
	data['userEnterMedal'] = entity.userEnterMedal;
	return data;
}