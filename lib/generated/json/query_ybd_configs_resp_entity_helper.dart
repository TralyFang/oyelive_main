import 'package:oyelive_main/module/entity/query_ybd_configs_resp_entity.dart';
import 'dart:convert';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/entity/unique_ybd_id_entity.dart';

yBDQueryConfigsRespEntityFromJson(YBDQueryConfigsRespEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDConfigInfo().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDQueryConfigsRespEntityToJson(YBDQueryConfigsRespEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDAppVersionFromJson(YBDAppVersion data, Map<String, dynamic> json) {
	if (json['iosConfig'] != null) {
		data.iosConfig = YBDAppVersionConfig().fromJson(json['iosConfig']);
	}
	if (json['androidConfig'] != null) {
		data.androidConfig = YBDAppVersionConfig().fromJson(json['androidConfig']);
	}
	return data;
}

Map<String, dynamic> yBDAppVersionToJson(YBDAppVersion entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['iosConfig'] = entity.iosConfig?.toJson();
	data['androidConfig'] = entity.androidConfig?.toJson();
	return data;
}

yBDAppVersionConfigFromJson(YBDAppVersionConfig data, Map<String, dynamic> json) {
	if (json['forceUpdate'] != null) {
		data.forceUpdate = json['forceUpdate'];
	}
	if (json['showDialog'] != null) {
		data.showDialog = json['showDialog'];
	}
	if (json['versionCode'] != null) {
		data.versionCode = json['versionCode'].toString();
	}
	if (json['buildCode'] != null) {
		data.buildCode = json['buildCode'] is String
				? int.tryParse(json['buildCode'])
				: json['buildCode'].toInt();
	}
	if (json['description'] != null) {
		data.description = json['description'].toString();
	}
	if (json['minVersion'] != null) {
		data.minVersion = json['minVersion'] is String
				? int.tryParse(json['minVersion'])
				: json['minVersion'].toInt();
	}
	if (json['whiteList'] != null) {
		data.whiteList = (json['whiteList'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	return data;
}

Map<String, dynamic> yBDAppVersionConfigToJson(YBDAppVersionConfig entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['forceUpdate'] = entity.forceUpdate;
	data['showDialog'] = entity.showDialog;
	data['versionCode'] = entity.versionCode;
	data['buildCode'] = entity.buildCode;
	data['description'] = entity.description;
	data['minVersion'] = entity.minVersion;
	data['whiteList'] = entity.whiteList;
	return data;
}

yBDConfigInfoFromJson(YBDConfigInfo data, Map<String, dynamic> json) {
	if (json['super_room_manager'] != null) {
		data.superRoomManager = json['super_room_manager'].toString();
	}
	if (json['medal_price'] != null) {
		data.medalPrice = json['medal_price'].toString();
	}
	if (json['app_location_portaltype'] != null) {
		data.appLocationPortaltype = json['app_location_portaltype'].toString();
	}
	if (json['youtube_channels_home'] != null) {
		data.youtubeChannelsHome = json['youtube_channels_home'].toString();
	}
	if (json['ads_count_limit'] != null) {
		data.adsCountLimit = json['ads_count_limit'].toString();
	}
	if (json['ep_coin_price'] != null) {
		data.epCoinPrice = json['ep_coin_price'].toString();
	}
	if (json['wifi_support'] != null) {
		data.wifiSupport = json['wifi_support'].toString();
	}
	if (json['max_size_upload_file'] != null) {
		data.maxSizeUploadFile = json['max_size_upload_file'].toString();
	}
	if (json['report_content_user'] != null) {
		data.reportContentUser = json['report_content_user'].toString();
	}
	if (json['app_ramadan_event'] != null) {
		data.appRamadanEvent = json['app_ramadan_event'].toString();
	}
	if (json['label_show_in_home'] != null) {
		data.labelShowInHome = json['label_show_in_home'].toString();
	}
	if (json['app_invite_show'] != null) {
		data.appInviteShow = json['app_invite_show'].toString();
	}
	if (json['app_android'] != null) {
		data.appAndroid = json['app_android'].toString();
	}
	if (json['app_version'] != null) {
		data.appVersion = json['app_version'].toString();
	}
	if (json['payment_server_url'] != null) {
		data.paymentServerUrl = json['payment_server_url'].toString();
	}
	if (json['tlevel_privilege_mic_10'] != null) {
		data.tlevelPrivilegeMic10 = json['tlevel_privilege_mic_10'].toString();
	}
	if (json['youtube_key'] != null) {
		data.youtubeKey = json['youtube_key'].toString();
	}
	if (json['app_level_gift'] != null) {
		data.appLevelGift = json['app_level_gift'].toString();
	}
	if (json['path_share_image'] != null) {
		data.pathShareImage = json['path_share_image'].toString();
	}
	if (json['fb_base_url'] != null) {
		data.fbBaseUrl = json['fb_base_url'].toString();
	}
	if (json['amazon_s3_address2'] != null) {
		data.amazonS3Address2 = json['amazon_s3_address2'].toString();
	}
	if (json['app_broadcast_requirement'] != null) {
		data.appBroadcastRequirement = json['app_broadcast_requirement'].toString();
	}
	if (json['room_sticks'] != null) {
		data.roomSticks = json['room_sticks'].toString();
	}
	if (json['ulevel_privilege_mic_10'] != null) {
		data.ulevelPrivilegeMic10 = json['ulevel_privilege_mic_10'].toString();
	}
	if (json['ulevel_privilege_avatar'] != null) {
		data.ulevelPrivilegeAvatar = json['ulevel_privilege_avatar'].toString();
	}
	if (json['sign_by_account_pwd'] != null) {
		data.signByAccountPwd = json['sign_by_account_pwd'].toString();
	}
	if (json['tk_on_watch_ads'] != null) {
		data.tkOnWatchAds = json['tk_on_watch_ads'].toString();
	}
	if (json['app_topcoin_ranking'] != null) {
		data.appTopcoinRanking = json['app_topcoin_ranking'].toString();
	}
	if (json['support_paytm'] != null) {
		data.supportPaytm = json['support_paytm'].toString();
	}
	if (json['app_event_ranking'] != null) {
		data.appEventRanking = json['app_event_ranking'].toString();
	}
	if (json['tlevel_privilege_mic_9'] != null) {
		data.tlevelPrivilegeMic9 = json['tlevel_privilege_mic_9'].toString();
	}
	if (json['app_birthday_talents'] != null) {
		data.appBirthdayTalents = json['app_birthday_talents'].toString();
	}
	if (json['path_temp'] != null) {
		data.pathTemp = json['path_temp'].toString();
	}
	if (json['inbox_level'] != null) {
		data.inboxLevel = json['inbox_level'].toString();
	}
	if (json['topup_banner'] != null) {
		data.topupBanner = json['topup_banner'].toString();
	}
	if (json['new_label_id'] != null) {
		data.newLabelId = json['new_label_id'].toString();
	}
	if (json['whitelist_countries'] != null) {
		data.whitelistCountries = json['whitelist_countries'].toString();
	}
	if (json['wifi_credentials'] != null) {
		data.wifiCredentials = json['wifi_credentials'].toString();
	}
	if (json['need_picture_validation'] != null) {
		data.needPictureValidation = json['need_picture_validation'].toString();
	}
	if (json['amazon_s3_address'] != null) {
		data.amazonS3Address = json['amazon_s3_address'].toString();
	}
	if (json['android_log_expiry_period'] != null) {
		data.androidLogExpiryPeriod = json['android_log_expiry_period'].toString();
	}
	if (json['ulevel_privilege_mic_9'] != null) {
		data.ulevelPrivilegeMic9 = json['ulevel_privilege_mic_9'].toString();
	}
	if (json['youtube_channel_official'] != null) {
		data.youtubeChannelOfficial = json['youtube_channel_official'].toString();
	}
	if (json['amazon_s3_bucket_name2'] != null) {
		data.amazonS3BucketName2 = json['amazon_s3_bucket_name2'].toString();
	}
	if (json['currency_unit'] != null) {
		data.currencyUnit = json['currency_unit'].toString();
	}
	if (json['google_in_app_billing_item_ids'] != null) {
		data.googleInAppBillingItemIds = json['google_in_app_billing_item_ids'].toString();
	}
	if (json['push_channel_vip_talent'] != null) {
		data.pushChannelVipTalent = json['push_channel_vip_talent'].toString();
	}
	if (json['imageConfig'] != null) {
		data.imageConfig = json['imageConfig'];
	}
	if (json['sofa_price_add'] != null) {
		data.sofaPriceAdd = json['sofa_price_add'].toString();
	}
	if (json['cash_out_currency_rate'] != null) {
		data.cashOutCurrencyRate = json['cash_out_currency_rate'].toString();
	}
	if (json['super_vip'] != null) {
		data.superVip = json['super_vip'].toString();
	}
	if (json['report_content_video'] != null) {
		data.reportContentVideo = json['report_content_video'].toString();
	}
	if (json['currencyRate'] != null) {
		data.currencyRate = json['currencyRate'];
	}
	if (json['payment_server'] != null) {
		data.paymentServer = json['payment_server'].toString();
	}
	if (json['push_channel_all'] != null) {
		data.pushChannelAll = json['push_channel_all'].toString();
	}
	if (json['amazon_s3_bucket_name'] != null) {
		data.amazonS3BucketName = json['amazon_s3_bucket_name'].toString();
	}
	if (json['ulevel_post_status'] != null) {
		data.ulevel_post_status = json['ulevel_post_status'].toString();
	}
	if (json['app_login_page_show_sms'] != null) {
		data.loginShowSMS = json['app_login_page_show_sms'].toString();
	}
	if (json['level_page_url'] != null) {
		data.level_page_url = json['level_page_url'].toString();
	}
	if (json['ulevel_go_live'] != null) {
		data.ulevelGoLive = json['ulevel_go_live'].toString();
	}
	if (json['tlevel_go_live'] != null) {
		data.tlevelGoLive = json['tlevel_go_live'].toString();
	}
	if (json['go_live_cover'] != null) {
		data.goLiveCover = json['go_live_cover'].toString();
	}
	if (json['talent_apply_url'] != null) {
		data.talentApplyUrl = json['talent_apply_url'].toString();
	}
	if (json['razorpay_server_url'] != null) {
		data.razorpay_server_url = json['razorpay_server_url'].toString();
	}
	if (json['user_modify_country_day'] != null) {
		data.user_modify_country_day = json['user_modify_country_day'].toString();
	}
	if (json['default_avatar'] != null) {
		data.tSystemAvatar = json['default_avatar'].toString();
	}
	if (json['mic_request_user_level'] != null) {
		data.micRequestUserLevel = json['mic_request_user_level'].toString();
	}
	if (json['quick_entry_greedy_show'] != null) {
		data.showGreedy = json['quick_entry_greedy_show'].toString();
	}
	if (json['slog_config'] != null) {
		data.slogConfig = json['slog_config'].toString();
	}
	if (json['weeklyAr_url'] != null) {
		data.weeklyArUrl = json['weeklyAr_url'].toString();
	}
	if (json['max_room_manager'] != null) {
		data.maxRoomManager = json['max_room_manager'].toString();
	}
	if (json['uniqueIds'] != null) {
		data.uniqueIds = (json['uniqueIds'] as List).map((v) => YBDUniqueIDEntity().fromJson(v)).toList();
	}
	if (json['topup_offer'] != null) {
		data.topupOffer = json['topup_offer'].toString();
	}
	if (json['show_home_icon'] != null) {
		data.showHomeIcon = json['show_home_icon'].toString();
	}
	if (json['master_of_combos'] != null) {
		data.masterOfCombos = json['master_of_combos'].toString();
	}
	if (json['present_activity'] != null) {
		data.presentActivity = json['present_activity'].toString();
	}
	if (json['eid_special_activity'] != null) {
		data.eidSpecialActivity = json['eid_special_activity'].toString();
	}
	if (json['lottery_page_display'] != null) {
		data.lotteryPageDisplay = json['lottery_page_display'].toString();
	}
	if (json['lottery_game_url'] != null) {
		data.lotteryGameUrl = json['lottery_game_url'].toString();
	}
	if (json['topup_gift_activity'] != null) {
		data.topUpGiftActivity = json['topup_gift_activity'].toString();
	}
	if (json['reseller_url'] != null) {
		data.resellerUrl = json['reseller_url'].toString();
	}
	if (json['ludo_test_rooms'] != null) {
		data.ludoTestRooms = json['ludo_test_rooms'].toString();
	}
	if (json['top_talent_ranking_image'] != null) {
		data.toptalentRankingImages = json['top_talent_ranking_image'].toString();
	}
	if (json['pk_label_image'] != null) {
		data.pkLabelImage = json['pk_label_image'].toString();
	}
	if (json['pk_help'] != null) {
		data.pkHelp = json['pk_help'].toString();
	}
	if (json['transparent_method'] != null) {
		data.transparentMethod = json['transparent_method'].toString();
	}
	if (json['talent_payroll'] != null) {
		data.talentPayroll = json['talent_payroll'].toString();
	}
	if (json['talent_sign'] != null) {
		data.talentSign = json['talent_sign'].toString();
	}
	if (json['gems_beans'] != null) {
		data.gemsBeans = json['gems_beans'].toString();
	}
	if (json['error_modal'] != null) {
		data.errorModal = json['error_modal'].toString();
	}
	if (json['room_abuser_enable'] != null) {
		data.roomAbuserEnable = json['room_abuser_enable'].toString();
	}
	if (json['collect_life_cle'] != null) {
		data.collecLifeCleExt = json['collect_life_cle'].toString();
	}
	if (json['tp_card_transform'] != null) {
		data.tpCardTransform = json['tp_card_transform'].toString();
	}
	if (json['tp_seat_dance'] != null) {
		data.tpSeatDance = json['tp_seat_dance'].toString();
	}
	if (json['tp_check_balance'] != null) {
		data.tpCheckBalance = json['tp_check_balance'].toString();
	}
	if (json['tp_config'] != null) {
		data.tpConfig = json['tp_config'].toString();
	}
	if (json['room_operate'] != null) {
		data.roomOperate = json['room_operate'].toString();
	}
	if (json['tp_gems2beans'] != null) {
		data.tpGems2Beans = json['tp_gems2beans'].toString();
	}
	if (json['beans2golds_url'] != null) {
		data.beans2GoldsUrl = json['beans2golds_url'].toString();
	}
	if (json['skin_config'] != null) {
		data.skinConfig = json['skin_config'].toString();
	}
	if (json['share_switch'] != null) {
		data.shareSwitch = json['share_switch'].toString();
	}
	if (json['game_room_config'] != null) {
		data.gameRoomConfig = json['game_room_config'].toString();
	}
	if (json['analysis_api_duration'] != null) {
		data.analysisApiDuration = json['analysis_api_duration'].toString();
	}
	if (json['firebase_log'] != null) {
		data.firebaseLog = json['firebase_log'].toString();
	}
	if (json['track_switch'] != null) {
		data.trackSwitch = json['track_switch'].toString();
	}
	if (json['store_init_index'] != null) {
		data.storeInitIndex = json['store_init_index'].toString();
	}
	return data;
}

Map<String, dynamic> yBDConfigInfoToJson(YBDConfigInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['super_room_manager'] = entity.superRoomManager;
	data['medal_price'] = entity.medalPrice;
	data['app_location_portaltype'] = entity.appLocationPortaltype;
	data['youtube_channels_home'] = entity.youtubeChannelsHome;
	data['ads_count_limit'] = entity.adsCountLimit;
	data['ep_coin_price'] = entity.epCoinPrice;
	data['wifi_support'] = entity.wifiSupport;
	data['max_size_upload_file'] = entity.maxSizeUploadFile;
	data['report_content_user'] = entity.reportContentUser;
	data['app_ramadan_event'] = entity.appRamadanEvent;
	data['label_show_in_home'] = entity.labelShowInHome;
	data['app_invite_show'] = entity.appInviteShow;
	data['app_android'] = entity.appAndroid;
	data['app_version'] = entity.appVersion;
	data['payment_server_url'] = entity.paymentServerUrl;
	data['tlevel_privilege_mic_10'] = entity.tlevelPrivilegeMic10;
	data['youtube_key'] = entity.youtubeKey;
	data['app_level_gift'] = entity.appLevelGift;
	data['path_share_image'] = entity.pathShareImage;
	data['fb_base_url'] = entity.fbBaseUrl;
	data['amazon_s3_address2'] = entity.amazonS3Address2;
	data['app_broadcast_requirement'] = entity.appBroadcastRequirement;
	data['room_sticks'] = entity.roomSticks;
	data['ulevel_privilege_mic_10'] = entity.ulevelPrivilegeMic10;
	data['ulevel_privilege_avatar'] = entity.ulevelPrivilegeAvatar;
	data['sign_by_account_pwd'] = entity.signByAccountPwd;
	data['tk_on_watch_ads'] = entity.tkOnWatchAds;
	data['app_topcoin_ranking'] = entity.appTopcoinRanking;
	data['support_paytm'] = entity.supportPaytm;
	data['app_event_ranking'] = entity.appEventRanking;
	data['tlevel_privilege_mic_9'] = entity.tlevelPrivilegeMic9;
	data['app_birthday_talents'] = entity.appBirthdayTalents;
	data['path_temp'] = entity.pathTemp;
	data['inbox_level'] = entity.inboxLevel;
	data['topup_banner'] = entity.topupBanner;
	data['new_label_id'] = entity.newLabelId;
	data['whitelist_countries'] = entity.whitelistCountries;
	data['wifi_credentials'] = entity.wifiCredentials;
	data['need_picture_validation'] = entity.needPictureValidation;
	data['amazon_s3_address'] = entity.amazonS3Address;
	data['android_log_expiry_period'] = entity.androidLogExpiryPeriod;
	data['ulevel_privilege_mic_9'] = entity.ulevelPrivilegeMic9;
	data['youtube_channel_official'] = entity.youtubeChannelOfficial;
	data['amazon_s3_bucket_name2'] = entity.amazonS3BucketName2;
	data['currency_unit'] = entity.currencyUnit;
	data['google_in_app_billing_item_ids'] = entity.googleInAppBillingItemIds;
	data['push_channel_vip_talent'] = entity.pushChannelVipTalent;
	data['imageConfig'] = entity.imageConfig;
	data['sofa_price_add'] = entity.sofaPriceAdd;
	data['cash_out_currency_rate'] = entity.cashOutCurrencyRate;
	data['super_vip'] = entity.superVip;
	data['report_content_video'] = entity.reportContentVideo;
	data['currencyRate'] = entity.currencyRate;
	data['payment_server'] = entity.paymentServer;
	data['push_channel_all'] = entity.pushChannelAll;
	data['amazon_s3_bucket_name'] = entity.amazonS3BucketName;
	data['ulevel_post_status'] = entity.ulevel_post_status;
	data['app_login_page_show_sms'] = entity.loginShowSMS;
	data['level_page_url'] = entity.level_page_url;
	data['ulevel_go_live'] = entity.ulevelGoLive;
	data['tlevel_go_live'] = entity.tlevelGoLive;
	data['go_live_cover'] = entity.goLiveCover;
	data['talent_apply_url'] = entity.talentApplyUrl;
	data['razorpay_server_url'] = entity.razorpay_server_url;
	data['user_modify_country_day'] = entity.user_modify_country_day;
	data['default_avatar'] = entity.tSystemAvatar;
	data['mic_request_user_level'] = entity.micRequestUserLevel;
	data['quick_entry_greedy_show'] = entity.showGreedy;
	data['slog_config'] = entity.slogConfig;
	data['weeklyAr_url'] = entity.weeklyArUrl;
	data['max_room_manager'] = entity.maxRoomManager;
	data['uniqueIds'] =  entity.uniqueIds?.map((v) => v?.toJson())?.toList();
	data['topup_offer'] = entity.topupOffer;
	data['show_home_icon'] = entity.showHomeIcon;
	data['master_of_combos'] = entity.masterOfCombos;
	data['present_activity'] = entity.presentActivity;
	data['eid_special_activity'] = entity.eidSpecialActivity;
	data['lottery_page_display'] = entity.lotteryPageDisplay;
	data['lottery_game_url'] = entity.lotteryGameUrl;
	data['topup_gift_activity'] = entity.topUpGiftActivity;
	data['reseller_url'] = entity.resellerUrl;
	data['ludo_test_rooms'] = entity.ludoTestRooms;
	data['top_talent_ranking_image'] = entity.toptalentRankingImages;
	data['pk_label_image'] = entity.pkLabelImage;
	data['pk_help'] = entity.pkHelp;
	data['transparent_method'] = entity.transparentMethod;
	data['talent_payroll'] = entity.talentPayroll;
	data['talent_sign'] = entity.talentSign;
	data['gems_beans'] = entity.gemsBeans;
	data['error_modal'] = entity.errorModal;
	data['room_abuser_enable'] = entity.roomAbuserEnable;
	data['collect_life_cle'] = entity.collecLifeCleExt;
	data['tp_card_transform'] = entity.tpCardTransform;
	data['tp_seat_dance'] = entity.tpSeatDance;
	data['tp_check_balance'] = entity.tpCheckBalance;
	data['tp_config'] = entity.tpConfig;
	data['room_operate'] = entity.roomOperate;
	data['tp_gems2beans'] = entity.tpGems2Beans;
	data['beans2golds_url'] = entity.beans2GoldsUrl;
	data['skin_config'] = entity.skinConfig;
	data['share_switch'] = entity.shareSwitch;
	data['game_room_config'] = entity.gameRoomConfig;
	data['analysis_api_duration'] = entity.analysisApiDuration;
	data['firebase_log'] = entity.firebaseLog;
	data['track_switch'] = entity.trackSwitch;
	data['store_init_index'] = entity.storeInitIndex;
	return data;
}