import 'package:oyelive_main/module/inbox/entity/message_ybd_offset_entity.dart';

yBDMessageOffsetEntityFromJson(YBDMessageOffsetEntity data, Map<String, dynamic> json) {
	if (json['sequenceId'] != null) {
		data.sequenceId = json['sequenceId'] is String
				? int.tryParse(json['sequenceId'])
				: json['sequenceId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDMessageOffsetEntityToJson(YBDMessageOffsetEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['sequenceId'] = entity.sequenceId;
	return data;
}