import 'package:oyelive_main/module/status/entity/random_ybd_dialogue_entity.dart';

yBDRandomDialogueEntityFromJson(YBDRandomDialogueEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDRandomDialogueData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDRandomDialogueEntityToJson(YBDRandomDialogueEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDRandomDialogueDataFromJson(YBDRandomDialogueData data, Map<String, dynamic> json) {
	if (json['dialogue'] != null) {
		data.dialogue = YBDRandomDialogueDataDialogue().fromJson(json['dialogue']);
	}
	return data;
}

Map<String, dynamic> yBDRandomDialogueDataToJson(YBDRandomDialogueData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['dialogue'] = entity.dialogue?.toJson();
	return data;
}

yBDRandomDialogueDataDialogueFromJson(YBDRandomDialogueDataDialogue data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['author'] != null) {
		data.author = json['author'].toString();
	}
	if (json['text'] != null) {
		data.text = json['text'].toString();
	}
	if (json['top'] != null) {
		data.top = json['top'] is String
				? int.tryParse(json['top'])
				: json['top'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDRandomDialogueDataDialogueToJson(YBDRandomDialogueDataDialogue entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['author'] = entity.author;
	data['text'] = entity.text;
	data['top'] = entity.top;
	return data;
}