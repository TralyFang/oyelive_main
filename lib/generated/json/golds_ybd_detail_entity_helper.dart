import 'package:oyelive_main/module/entity/golds_ybd_detail_entity.dart';

yBDGoldsDetailEntityFromJson(YBDGoldsDetailEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDGoldsDetailRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGoldsDetailEntityToJson(YBDGoldsDetailEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDGoldsDetailRecordFromJson(YBDGoldsDetailRecord data, Map<String, dynamic> json) {
	if (json['createTime'] != null) {
		data.createTime = json['createTime'] is String
				? int.tryParse(json['createTime'])
				: json['createTime'].toInt();
	}
	if (json['golds'] != null) {
		data.golds = json['golds'] is String
				? int.tryParse(json['golds'])
				: json['golds'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGoldsDetailRecordToJson(YBDGoldsDetailRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['createTime'] = entity.createTime;
	data['golds'] = entity.golds;
	data['type'] = entity.type;
	data['userId'] = entity.userId;
	return data;
}