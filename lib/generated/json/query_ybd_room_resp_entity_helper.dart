import 'package:oyelive_main/module/user/entity/query_ybd_room_resp_entity.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

yBDQueryRoomRespEntityFromJson(YBDQueryRoomRespEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDRoomInfo().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDQueryRoomRespEntityToJson(YBDQueryRoomRespEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}