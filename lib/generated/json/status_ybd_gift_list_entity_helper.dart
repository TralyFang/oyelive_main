import 'package:oyelive_main/module/status/entity/status_ybd_gift_list_entity.dart';

yBDStatusGiftListEntityFromJson(YBDStatusGiftListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	if (json['data'] != null) {
		data.data = YBDStatusGiftListData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDStatusGiftListEntityToJson(YBDStatusGiftListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['success'] = entity.success;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDStatusGiftListDataFromJson(YBDStatusGiftListData data, Map<String, dynamic> json) {
	if (json['records'] != null) {
		data.records = (json['records'] as List).map((v) => YBDStatusGiftListDataRows().fromJson(v)).toList();
	}
	if (json['total'] != null) {
		data.total = json['total'] is String
				? int.tryParse(json['total'])
				: json['total'].toInt();
	}
	if (json['size'] != null) {
		data.size = json['size'] is String
				? int.tryParse(json['size'])
				: json['size'].toInt();
	}
	if (json['current'] != null) {
		data.current = json['current'] is String
				? int.tryParse(json['current'])
				: json['current'].toInt();
	}
	if (json['pages'] != null) {
		data.pages = json['pages'] is String
				? int.tryParse(json['pages'])
				: json['pages'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDStatusGiftListDataToJson(YBDStatusGiftListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['records'] =  entity.records?.map((v) => v?.toJson())?.toList();
	data['total'] = entity.total;
	data['size'] = entity.size;
	data['current'] = entity.current;
	data['pages'] = entity.pages;
	return data;
}

yBDStatusGiftListDataRowsFromJson(YBDStatusGiftListDataRows data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['slogId'] != null) {
		data.slogId = json['slogId'] is String
				? int.tryParse(json['slogId'])
				: json['slogId'].toInt();
	}
	if (json['slogUserId'] != null) {
		data.slogUserId = json['slogUserId'] is String
				? int.tryParse(json['slogUserId'])
				: json['slogUserId'].toInt();
	}
	if (json['presentUserId'] != null) {
		data.presentUserId = json['presentUserId'] is String
				? int.tryParse(json['presentUserId'])
				: json['presentUserId'].toInt();
	}
	if (json['giftId'] != null) {
		data.giftId = json['giftId'] is String
				? int.tryParse(json['giftId'])
				: json['giftId'].toInt();
	}
	if (json['giftNumber'] != null) {
		data.giftNumber = json['giftNumber'] is String
				? int.tryParse(json['giftNumber'])
				: json['giftNumber'].toInt();
	}
	if (json['giftPrice'] != null) {
		data.giftPrice = json['giftPrice'] is String
				? int.tryParse(json['giftPrice'])
				: json['giftPrice'].toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'].toString();
	}
	if (json['nickName'] != null) {
		data.nickName = json['nickName'].toString();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['gender'] != null) {
		data.gender = json['gender'] is String
				? int.tryParse(json['gender'])
				: json['gender'].toInt();
	}
	if (json['giftUrl'] != null) {
		data.giftUrl = json['giftUrl'].toString();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'].toString();
	}
	if (json['vipIcon'] != null) {
		data.vipIcon = json['vipIcon'].toString();
	}
	return data;
}

Map<String, dynamic> yBDStatusGiftListDataRowsToJson(YBDStatusGiftListDataRows entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['slogId'] = entity.slogId;
	data['slogUserId'] = entity.slogUserId;
	data['presentUserId'] = entity.presentUserId;
	data['giftId'] = entity.giftId;
	data['giftNumber'] = entity.giftNumber;
	data['giftPrice'] = entity.giftPrice;
	data['status'] = entity.status;
	data['createTime'] = entity.createTime;
	data['nickName'] = entity.nickName;
	data['level'] = entity.level;
	data['gender'] = entity.gender;
	data['giftUrl'] = entity.giftUrl;
	data['avatar'] = entity.avatar;
	data['vipIcon'] = entity.vipIcon;
	return data;
}