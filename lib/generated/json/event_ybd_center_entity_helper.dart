import 'package:oyelive_main/ui/page/home/entity/event_ybd_center_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/home/controller/event_ybd_dot_controller.dart' as eventCtr;

yBDEventCenterEntityFromJson(YBDEventCenterEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDEventCenterData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDEventCenterEntityToJson(YBDEventCenterEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	return data;
}

yBDEventCenterDataFromJson(YBDEventCenterData data, Map<String, dynamic> json) {
	if (json['eventInfos'] != null) {
		data.eventInfos = (json['eventInfos'] as List).map((v) => YBDEventCenterDataEventInfos().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'] is String
				? int.tryParse(json['recordSum'])
				: json['recordSum'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDEventCenterDataToJson(YBDEventCenterData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['eventInfos'] =  entity.eventInfos?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDEventCenterDataEventInfosFromJson(YBDEventCenterDataEventInfos data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['eventKey'] != null) {
		data.eventKey = json['eventKey'].toString();
	}
	if (json['eventName'] != null) {
		data.eventName = json['eventName'].toString();
	}
	if (json['eventDetail'] != null) {
		data.eventDetail = json['eventDetail'].toString();
	}
	if (json['eventUrl'] != null) {
		data.eventUrl = json['eventUrl'].toString();
	}
	if (json['eventImg'] != null) {
		data.eventImg = json['eventImg'].toString();
	}
	if (json['eventType'] != null) {
		data.eventType = json['eventType'].toString();
	}
	if (json['order'] != null) {
		data.order = json['order'] is String
				? int.tryParse(json['order'])
				: json['order'].toInt();
	}
	if (json['beginTime'] != null) {
		data.beginTime = json['beginTime'].toString();
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'].toString();
	}
	if (json['endDetail'] != null) {
		data.endDetail = json['endDetail'].toString();
	}
	if (json['country'] != null) {
		data.country = json['country'].toString();
	}
	if (json['tenantId'] != null) {
		data.tenantId = json['tenantId'];
	}
	if (json['projectId'] != null) {
		data.projectId = json['projectId'];
	}
	if (json['eventStatus'] != null) {
		data.eventStatus = json['eventStatus'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEventCenterDataEventInfosToJson(YBDEventCenterDataEventInfos entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['eventKey'] = entity.eventKey;
	data['eventName'] = entity.eventName;
	data['eventDetail'] = entity.eventDetail;
	data['eventUrl'] = entity.eventUrl;
	data['eventImg'] = entity.eventImg;
	data['eventType'] = entity.eventType;
	data['order'] = entity.order;
	data['beginTime'] = entity.beginTime;
	data['endTime'] = entity.endTime;
	data['endDetail'] = entity.endDetail;
	data['country'] = entity.country;
	data['tenantId'] = entity.tenantId;
	data['projectId'] = entity.projectId;
	data['eventStatus'] = entity.eventStatus;
	return data;
}