import 'package:oyelive_main/module/entity/device_ybd_unique_id.dart';

yBDDeviceUniqueIDFromJson(YBDDeviceUniqueID data, Map<String, dynamic> json) {
	if (json['blocked'] != null) {
		data.blocked = json['blocked'];
	}
	if (json['newDevice'] != null) {
		data.newDevice = json['newDevice'];
	}
	if (json['content'] != null) {
		data.content = json['content'].toString();
	}
	return data;
}

Map<String, dynamic> yBDDeviceUniqueIDToJson(YBDDeviceUniqueID entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['blocked'] = entity.blocked;
	data['newDevice'] = entity.newDevice;
	data['content'] = entity.content;
	return data;
}