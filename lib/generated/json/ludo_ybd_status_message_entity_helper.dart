import 'package:oyelive_main/common/room_socket/message/common/ludo_ybd_status_message_entity.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';

yBDLudoStatusMessageEntityFromJson(YBDLudoStatusMessageEntity data, Map<String, dynamic> json) {
	if (json['content'] != null) {
		data.content = YBDLudoStatusMessageContent().fromJson(json['content']);
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDLudoStatusMessageEntityToJson(YBDLudoStatusMessageEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['content'] = entity.content?.toJson();
	data['destination'] = entity.destination;
	data['fromUser'] = entity.fromUser;
	data['mode'] = entity.mode;
	data['msgType'] = entity.msgType;
	data['roomId'] = entity.roomId;
	data['toUser'] = entity.toUser;
	data['type'] = entity.type;
	return data;
}

yBDLudoStatusMessageContentFromJson(YBDLudoStatusMessageContent data, Map<String, dynamic> json) {
	if (json['pk'] != null) {
		data.pk = YBDLudoStatusMessagePk().fromJson(json['pk']);
	}
	if (json['ludo'] != null) {
		data.ludo = YBDLudoStatusMessageLudo().fromJson(json['ludo']);
	}
	if (json['gameRoom'] != null) {
		data.gameRoom = YBDLudoStatusMessageLudo().fromJson(json['gameRoom']);
	}
	return data;
}

Map<String, dynamic> yBDLudoStatusMessageContentToJson(YBDLudoStatusMessageContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['pk'] = entity.pk?.toJson();
	data['ludo'] = entity.ludo?.toJson();
	data['gameRoom'] = entity.gameRoom?.toJson();
	return data;
}

yBDLudoStatusMessagePkFromJson(YBDLudoStatusMessagePk data, Map<String, dynamic> json) {
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['gaming'] != null) {
		data.gaming = json['gaming'];
	}
	return data;
}

Map<String, dynamic> yBDLudoStatusMessagePkToJson(YBDLudoStatusMessagePk entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['roomId'] = entity.roomId;
	data['userId'] = entity.userId;
	data['gaming'] = entity.gaming;
	return data;
}

yBDLudoStatusMessageLudoFromJson(YBDLudoStatusMessageLudo data, Map<String, dynamic> json) {
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['gaming'] != null) {
		data.gaming = json['gaming'];
	}
	if (json['category'] != null) {
		data.category = json['category'].toString();
	}
	if (json['subCategory'] != null) {
		data.subCategory = json['subCategory'].toString();
	}
	return data;
}

Map<String, dynamic> yBDLudoStatusMessageLudoToJson(YBDLudoStatusMessageLudo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['roomId'] = entity.roomId;
	data['userId'] = entity.userId;
	data['gaming'] = entity.gaming;
	data['category'] = entity.category;
	data['subCategory'] = entity.subCategory;
	return data;
}