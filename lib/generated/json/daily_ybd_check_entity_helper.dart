import 'package:oyelive_main/ui/page/home/entity/daily_ybd_check_entity.dart';
import 'dart:async';

yBDDailyTaskEntityFromJson(YBDDailyTaskEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDDailyCheckRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDDailyTaskEntityToJson(YBDDailyTaskEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDDailyCheckRecordFromJson(YBDDailyCheckRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['reward'] != null) {
		data.reward = json['reward'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['receive'] != null) {
		data.receive = json['receive'];
	}
	if (json['extend'] != null) {
		data.extend = json['extend'].toString();
	}
	if (json['route'] != null) {
		data.route = json['route'].toString();
	}
	if (json['currencyImage'] != null) {
		data.currencyImage = json['currencyImage'].toString();
	}
	if (json['extendObj'] != null) {
		data.extendObj = YBDDailyCheckRecordExtend().fromJson(json['extendObj']);
	}
	return data;
}

Map<String, dynamic> yBDDailyCheckRecordToJson(YBDDailyCheckRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['reward'] = entity.reward;
	data['status'] = entity.status;
	data['receive'] = entity.receive;
	data['extend'] = entity.extend;
	data['route'] = entity.route;
	data['currencyImage'] = entity.currencyImage;
	data['extendObj'] = entity.extendObj?.toJson();
	return data;
}

yBDDailyCheckRecordExtendFromJson(YBDDailyCheckRecordExtend data, Map<String, dynamic> json) {
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['rewardList'] != null) {
		data.rewardList = (json['rewardList'] as List).map((v) => YBDDailyCheckRecordExtendRewardList().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDDailyCheckRecordExtendToJson(YBDDailyCheckRecordExtend entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['version'] = entity.version;
	data['rewardList'] =  entity.rewardList?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDDailyCheckRecordExtendRewardListFromJson(YBDDailyCheckRecordExtendRewardList data, Map<String, dynamic> json) {
	if (json['dailyReward'] != null) {
		data.dailyReward = (json['dailyReward'] as List).map((v) => YBDDailyCheckRecordExtendRewardListDailyReward().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDDailyCheckRecordExtendRewardListToJson(YBDDailyCheckRecordExtendRewardList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['dailyReward'] =  entity.dailyReward?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDDailyCheckRecordExtendRewardListDailyRewardFromJson(YBDDailyCheckRecordExtendRewardListDailyReward data, Map<String, dynamic> json) {
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['item'] != null) {
		data.item = json['item'].toString();
	}
	if (json['itemId'] != null) {
		data.itemId = json['itemId'] is String
				? int.tryParse(json['itemId'])
				: json['itemId'].toInt();
	}
	if (json['days'] != null) {
		data.days = json['days'] is String
				? int.tryParse(json['days'])
				: json['days'].toInt();
	}
	if (json['number'] != null) {
		data.number = json['number'] is String
				? int.tryParse(json['number'])
				: json['number'].toInt();
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['receive'] != null) {
		data.receive = json['receive'];
	}
	if (json['claimTime'] != null) {
		data.claimTime = json['claimTime'];
	}
	return data;
}

Map<String, dynamic> yBDDailyCheckRecordExtendRewardListDailyRewardToJson(YBDDailyCheckRecordExtendRewardListDailyReward entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['name'] = entity.name;
	data['item'] = entity.item;
	data['itemId'] = entity.itemId;
	data['days'] = entity.days;
	data['number'] = entity.number;
	data['icon'] = entity.icon;
	data['receive'] = entity.receive;
	data['claimTime'] = entity.claimTime;
	return data;
}