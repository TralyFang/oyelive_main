import 'package:oyelive_main/common/web_socket/tpg_match/entity/resp_ybd_match_entity.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/entity/match_ybd_cmd_base.dart';

yBDRespMatchEntityFromJson(YBDRespMatchEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['command'] != null) {
		data.command = json['command'].toString();
	}
	if (json['matchId'] != null) {
		data.matchId = json['matchId'].toString();
	}
	if (json['matchStatus'] != null) {
		data.matchStatus = json['matchStatus'].toString();
	}
	if (json['matchType'] != null) {
		data.matchType = json['matchType'].toString();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	if (json['operationTime'] != null) {
		data.operationTime = json['operationTime'] is String
				? num.tryParse(json['operationTime'])
				: json['operationTime'];
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? num.tryParse(json['version'])
				: json['version'];
	}
	return data;
}

Map<String, dynamic> yBDRespMatchEntityToJson(YBDRespMatchEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['command'] = entity.command;
	data['matchId'] = entity.matchId;
	data['matchStatus'] = entity.matchStatus;
	data['matchType'] = entity.matchType;
	data['message'] = entity.message;
	data['operationTime'] = entity.operationTime;
	data['version'] = entity.version;
	return data;
}