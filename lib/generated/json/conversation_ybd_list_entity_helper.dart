import 'package:oyelive_main/module/inbox/entity/conversation_ybd_list_entity.dart';

yBDConversationListEntityFromJson(YBDConversationListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = (json['data'] as List).map((v) => YBDConversationListData().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDConversationListEntityToJson(YBDConversationListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDConversationListDataFromJson(YBDConversationListData data, Map<String, dynamic> json) {
	if (json['queueId'] != null) {
		data.queueId = json['queueId'].toString();
	}
	if (json['principal'] != null) {
		data.principal = YBDConversationListDataPrincipal().fromJson(json['principal']);
	}
	if (json['category'] != null) {
		data.category = json['category'].toString();
	}
	if (json['disturb'] != null) {
		data.disturb = json['disturb'];
	}
	if (json['blockList'] != null) {
		data.blockList = json['blockList'];
	}
	return data;
}

Map<String, dynamic> yBDConversationListDataToJson(YBDConversationListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['queueId'] = entity.queueId;
	data['principal'] = entity.principal?.toJson();
	data['category'] = entity.category;
	data['disturb'] = entity.disturb;
	data['blockList'] = entity.blockList;
	return data;
}

yBDConversationListDataPrincipalFromJson(YBDConversationListDataPrincipal data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'].toString();
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDConversationListDataPrincipalToJson(YBDConversationListDataPrincipal entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['nickname'] = entity.nickname;
	data['avatar'] = entity.avatar;
	data['sex'] = entity.sex;
	return data;
}