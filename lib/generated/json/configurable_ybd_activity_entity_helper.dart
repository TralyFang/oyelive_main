import 'package:oyelive_main/ui/page/activty/entity/configurable_ybd_activity_entity.dart';
import 'dart:async';

yBDConfigurableActivityEntityFromJson(YBDConfigurableActivityEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDConfigurableActivityRecord().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDConfigurableActivityEntityToJson(YBDConfigurableActivityEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDConfigurableActivityRecordFromJson(YBDConfigurableActivityRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['triggerEvent'] != null) {
		data.triggerEvent = json['triggerEvent'] is String
				? int.tryParse(json['triggerEvent'])
				: json['triggerEvent'].toInt();
	}
	if (json['triggerUser'] != null) {
		data.triggerUser = json['triggerUser'] is String
				? int.tryParse(json['triggerUser'])
				: json['triggerUser'].toInt();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['link'] != null) {
		data.link = json['link'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['startTime'] != null) {
		data.startTime = json['startTime'] is String
				? int.tryParse(json['startTime'])
				: json['startTime'].toInt();
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'] is String
				? int.tryParse(json['endTime'])
				: json['endTime'].toInt();
	}
	if (json['rank'] != null) {
		data.rank = json['rank'] is String
				? int.tryParse(json['rank'])
				: json['rank'].toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'] is String
				? int.tryParse(json['createTime'])
				: json['createTime'].toInt();
	}
	if (json['deleted'] != null) {
		data.deleted = json['deleted'] is String
				? int.tryParse(json['deleted'])
				: json['deleted'].toInt();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['projectCode'] != null) {
		data.projectCode = json['projectCode'];
	}
	if (json['tenantCode'] != null) {
		data.tenantCode = json['tenantCode'];
	}
	return data;
}

Map<String, dynamic> yBDConfigurableActivityRecordToJson(YBDConfigurableActivityRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['type'] = entity.type;
	data['triggerEvent'] = entity.triggerEvent;
	data['triggerUser'] = entity.triggerUser;
	data['img'] = entity.img;
	data['link'] = entity.link;
	data['title'] = entity.title;
	data['startTime'] = entity.startTime;
	data['endTime'] = entity.endTime;
	data['rank'] = entity.rank;
	data['status'] = entity.status;
	data['createTime'] = entity.createTime;
	data['deleted'] = entity.deleted;
	data['version'] = entity.version;
	data['projectCode'] = entity.projectCode;
	data['tenantCode'] = entity.tenantCode;
	return data;
}