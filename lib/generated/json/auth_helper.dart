import 'package:oyelive_main/common/room_socket/message/common/auth.dart';

yBDAuthFromJson(YBDAuth data, Map<String, dynamic> json) {
	if (json['randomKey'] != null) {
		data.randomKey = json['randomKey'].toString();
	}
	if (json['client'] != null) {
		data.client = json['client'] is String
				? int.tryParse(json['client'])
				: json['client'].toInt();
	}
	if (json['version'] != null) {
		data.version = json['version'].toString();
	}
	if (json['protocol'] != null) {
		data.protocol = json['protocol'] is String
				? int.tryParse(json['protocol'])
				: json['protocol'].toInt();
	}
	if (json['unique'] != null) {
		data.unique = json['unique'].toString();
	}
	if (json['latitude'] != null) {
		data.latitude = json['latitude'] is String
				? double.tryParse(json['latitude'])
				: json['latitude'].toDouble();
	}
	if (json['longitude'] != null) {
		data.longitude = json['longitude'] is String
				? double.tryParse(json['longitude'])
				: json['longitude'].toDouble();
	}
	return data;
}

Map<String, dynamic> yBDAuthToJson(YBDAuth entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['randomKey'] = entity.randomKey;
	data['client'] = entity.client;
	data['version'] = entity.version;
	data['protocol'] = entity.protocol;
	data['unique'] = entity.unique;
	data['latitude'] = entity.latitude;
	data['longitude'] = entity.longitude;
	return data;
}