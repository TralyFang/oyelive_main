import 'package:oyelive_main/module/status/entity/status_ybd_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

yBDStatusInfoFromJson(YBDStatusInfo data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['country'] != null) {
		data.country = json['country'].toString();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'].toString();
	}
	if (json['creatorId'] != null) {
		data.creatorId = json['creatorId'].toString();
	}
	if (json['updateTime'] != null) {
		data.updateTime = json['updateTime'].toString();
	}
	if (json['updatorId'] != null) {
		data.updatorId = json['updatorId'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['deleted'] != null) {
		data.deleted = json['deleted'] is String
				? int.tryParse(json['deleted'])
				: json['deleted'].toInt();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	if (json['contentType'] != null) {
		data.contentType = json['contentType'] is String
				? int.tryParse(json['contentType'])
				: json['contentType'].toInt();
	}
	if (json['content'] != null) {
		data.content = YBDStatusContent().fromJson(json['content']);
	}
	if (json['label'] != null) {
		data.label = json['label'].toString();
	}
	if (json['location'] != null) {
		data.location = YBDLocationInfo().fromJson(json['location']);
	}
	if (json['shareCount'] != null) {
		data.shareCount = json['shareCount'] is String
				? int.tryParse(json['shareCount'])
				: json['shareCount'].toInt();
	}
	if (json['likeCount'] != null) {
		data.likeCount = json['likeCount'] is String
				? int.tryParse(json['likeCount'])
				: json['likeCount'].toInt();
	}
	if (json['commentCount'] != null) {
		data.commentCount = json['commentCount'] is String
				? int.tryParse(json['commentCount'])
				: json['commentCount'].toInt();
	}
	if (json['giftCount'] != null) {
		data.giftCount = json['giftCount'] is String
				? int.tryParse(json['giftCount'])
				: json['giftCount'].toInt();
	}
	if (json['likeCommentCount'] != null) {
		data.likeCommentCount = json['likeCommentCount'] is String
				? int.tryParse(json['likeCommentCount'])
				: json['likeCommentCount'].toInt();
	}
	if (json['cause'] != null) {
		data.cause = json['cause'].toString();
	}
	if (json['addTime'] != null) {
		data.addTime = json['addTime'].toString();
	}
	if (json['nickName'] != null) {
		data.nickName = json['nickName'].toString();
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	if (json['vip'] != null) {
		data.vip = json['vip'] is String
				? int.tryParse(json['vip'])
				: json['vip'].toInt();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'].toString();
	}
	if (json['vipIcon'] != null) {
		data.vipIcon = json['vipIcon'].toString();
	}
	if (json['officialTalent'] != null) {
		data.officialTalent = json['officialTalent'];
	}
	if (json['like'] != null) {
		data.like = json['like'];
	}
	if (json['follow'] != null) {
		data.follow = json['follow'];
	}
	if (json['live'] != null) {
		data.live = json['live'];
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['talentLevel'] != null) {
		data.talentLevel = json['talentLevel'] is String
				? int.tryParse(json['talentLevel'])
				: json['talentLevel'].toInt();
	}
	if (json['activityRanking'] != null) {
		data.activityRanking = json['activityRanking'] is String
				? int.tryParse(json['activityRanking'])
				: json['activityRanking'].toInt();
	}
	if (json['badges'] != null) {
		data.badges = (json['badges'] as List).map((v) => YBDBadgeInfo().fromJson(v)).toList();
	}
	if (json['headFrame'] != null) {
		data.headFrame = json['headFrame'].toString();
	}
	return data;
}

Map<String, dynamic> yBDStatusInfoToJson(YBDStatusInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['country'] = entity.country;
	data['version'] = entity.version;
	data['createTime'] = entity.createTime;
	data['creatorId'] = entity.creatorId;
	data['updateTime'] = entity.updateTime;
	data['updatorId'] = entity.updatorId;
	data['status'] = entity.status;
	data['deleted'] = entity.deleted;
	data['userId'] = entity.userId;
	data['contentType'] = entity.contentType;
	data['content'] = entity.content?.toJson();
	data['label'] = entity.label;
	data['location'] = entity.location?.toJson();
	data['shareCount'] = entity.shareCount;
	data['likeCount'] = entity.likeCount;
	data['commentCount'] = entity.commentCount;
	data['giftCount'] = entity.giftCount;
	data['likeCommentCount'] = entity.likeCommentCount;
	data['cause'] = entity.cause;
	data['addTime'] = entity.addTime;
	data['nickName'] = entity.nickName;
	data['sex'] = entity.sex;
	data['vip'] = entity.vip;
	data['avatar'] = entity.avatar;
	data['vipIcon'] = entity.vipIcon;
	data['officialTalent'] = entity.officialTalent;
	data['like'] = entity.like;
	data['follow'] = entity.follow;
	data['live'] = entity.live;
	data['level'] = entity.level;
	data['talentLevel'] = entity.talentLevel;
	data['activityRanking'] = entity.activityRanking;
	data['badges'] =  entity.badges?.map((v) => v?.toJson())?.toList();
	data['headFrame'] = entity.headFrame;
	return data;
}

yBDStatusContentFromJson(YBDStatusContent data, Map<String, dynamic> json) {
	if (json['text'] != null) {
		data.text = json['text'].toString();
	}
	if (json['images'] != null) {
		data.images = json['images'].toString();
	}
	if (json['audio'] != null) {
		data.audio = YBDAudioInfo().fromJson(json['audio']);
	}
	if (json['video'] != null) {
		data.video = json['video'].toString();
	}
	if (json['image'] != null) {
		data.image = (json['image'] as List).map((v) => YBDStatusImageInfo().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDStatusContentToJson(YBDStatusContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['text'] = entity.text;
	data['images'] = entity.images;
	data['audio'] = entity.audio?.toJson();
	data['video'] = entity.video;
	data['image'] =  entity.image?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDStatusImageInfoFromJson(YBDStatusImageInfo data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['url'] != null) {
		data.url = json['url'].toString();
	}
	if (json['ext'] != null) {
		data.ext = YBDExtInfo().fromJson(json['ext']);
	}
	return data;
}

Map<String, dynamic> yBDStatusImageInfoToJson(YBDStatusImageInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['url'] = entity.url;
	data['ext'] = entity.ext?.toJson();
	return data;
}

yBDExtInfoFromJson(YBDExtInfo data, Map<String, dynamic> json) {
	if (json['size'] != null) {
		data.size = json['size'].toString();
	}
	if (json['gauss'] != null) {
		data.gauss = json['gauss'];
	}
	return data;
}

Map<String, dynamic> yBDExtInfoToJson(YBDExtInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['size'] = entity.size;
	data['gauss'] = entity.gauss;
	return data;
}

yBDLocationInfoFromJson(YBDLocationInfo data, Map<String, dynamic> json) {
	if (json['ip'] != null) {
		data.ip = json['ip'].toString();
	}
	return data;
}

Map<String, dynamic> yBDLocationInfoToJson(YBDLocationInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['ip'] = entity.ip;
	return data;
}

yBDAudioInfoFromJson(YBDAudioInfo data, Map<String, dynamic> json) {
	if (json['resource'] != null) {
		data.resource = json['resource'].toString();
	}
	if (json['dialogue'] != null) {
		data.dialogue = json['dialogue'].toString();
	}
	if (json['lyric'] != null) {
		data.lyric = json['lyric'].toString();
	}
	if (json['dialogueName'] != null) {
		data.dialogueName = json['dialogueName'].toString();
	}
	if (json['lyricName'] != null) {
		data.lyricName = json['lyricName'].toString();
	}
	if (json['bgImage'] != null) {
		data.bgImage = json['bgImage'].toString();
	}
	if (json['text'] != null) {
		data.text = json['text'].toString();
	}
	if (json['mood'] != null) {
		data.mood = json['mood'].toString();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['duration'] != null) {
		data.duration = json['duration'] is String
				? int.tryParse(json['duration'])
				: json['duration'].toInt();
	}
	if (json['bgMusic'] != null) {
		data.bgMusic = json['bgMusic'].toString();
	}
	if (json['bgMusicId'] != null) {
		data.bgMusicId = json['bgMusicId'].toString();
	}
	if (json['bgMusicName'] != null) {
		data.bgMusicName = json['bgMusicName'].toString();
	}
	if (json['bgMusicSinger'] != null) {
		data.bgMusicSinger = json['bgMusicSinger'].toString();
	}
	if (json['volume'] != null) {
		data.volume = json['volume'].toString();
	}
	if (json['segment'] != null) {
		data.segment = json['segment'].toString();
	}
	return data;
}

Map<String, dynamic> yBDAudioInfoToJson(YBDAudioInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['resource'] = entity.resource;
	data['dialogue'] = entity.dialogue;
	data['lyric'] = entity.lyric;
	data['dialogueName'] = entity.dialogueName;
	data['lyricName'] = entity.lyricName;
	data['bgImage'] = entity.bgImage;
	data['text'] = entity.text;
	data['mood'] = entity.mood;
	data['type'] = entity.type;
	data['duration'] = entity.duration;
	data['bgMusic'] = entity.bgMusic;
	data['bgMusicId'] = entity.bgMusicId;
	data['bgMusicName'] = entity.bgMusicName;
	data['bgMusicSinger'] = entity.bgMusicSinger;
	data['volume'] = entity.volume;
	data['segment'] = entity.segment;
	return data;
}