import 'package:oyelive_main/ui/page/profile/aristocracy/entity/aristocracy_ybd_info_entity.dart';

yBDAristocracyInfoEntityFromJson(YBDAristocracyInfoEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDAristocracyInfoRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDAristocracyInfoEntityToJson(YBDAristocracyInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record.toJson();
	return data;
}

yBDAristocracyInfoRecordFromJson(YBDAristocracyInfoRecord data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['buyUrl'] != null) {
		data.buyUrl = json['buyUrl'].toString();
	}
	if (json['vipData'] != null) {
		data.vipData = (json['vipData'] as List).map((v) => YBDAristocracyInfoRecordVipData().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDAristocracyInfoRecordToJson(YBDAristocracyInfoRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['buyUrl'] = entity.buyUrl;
	data['vipData'] =  entity.vipData?.map((v) => v.toJson())?.toList();
	return data;
}

yBDAristocracyInfoRecordVipDataFromJson(YBDAristocracyInfoRecordVipData data, Map<String, dynamic> json) {
	if (json['vipName'] != null) {
		data.vipName = json['vipName'].toString();
	}
	if (json['vipCode'] != null) {
		data.vipCode = json['vipCode'] is String
				? int.tryParse(json['vipCode'])
				: json['vipCode'].toInt();
	}
	if (json['vipPrice'] != null) {
		data.vipPrice = json['vipPrice'].toString();
	}
	if (json['vipIcon'] != null) {
		data.vipIcon = json['vipIcon'].toString();
	}
	if (json['vipMiniIcon'] != null) {
		data.vipMiniIcon = json['vipMiniIcon'].toString();
	}
	if (json['ruleIcon'] != null) {
		data.ruleIcon = json['ruleIcon'].toString();
	}
	if (json['buySuccessNotice'] != null) {
		data.buySuccessNotice = json['buySuccessNotice'];
	}
	if (json['index'] != null) {
		data.index = json['index'].toString();
	}
	if (json['vipValidContent'] != null) {
		data.vipValidContent = json['vipValidContent'].toString();
	}
	if (json['checkInBoundsContent'] != null) {
		data.checkInBoundsContent = json['checkInBoundsContent'].toString();
	}
	if (json['introduction'] != null) {
		data.introduction = json['introduction'].toString();
	}
	if (json['privileges'] != null) {
		data.privileges = (json['privileges'] as List).map((v) => YBDAristocracyInfoRecordVipDataPrivileges().fromJson(v)).toList();
	}
	if (json['buyPrice'] != null) {
		data.buyPrice = (json['buyPrice'] as List).map((v) => YBDAristocracyInfoRecordVipDataBuyPrice().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDAristocracyInfoRecordVipDataToJson(YBDAristocracyInfoRecordVipData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['vipName'] = entity.vipName;
	data['vipCode'] = entity.vipCode;
	data['vipPrice'] = entity.vipPrice;
	data['vipIcon'] = entity.vipIcon;
	data['vipMiniIcon'] = entity.vipMiniIcon;
	data['ruleIcon'] = entity.ruleIcon;
	data['buySuccessNotice'] = entity.buySuccessNotice;
	data['index'] = entity.index;
	data['vipValidContent'] = entity.vipValidContent;
	data['checkInBoundsContent'] = entity.checkInBoundsContent;
	data['introduction'] = entity.introduction;
	data['privileges'] =  entity.privileges.map((v) => v.toJson()).toList();
	data['buyPrice'] =  entity.buyPrice?.map((v) => v.toJson())?.toList();
	return data;
}

yBDAristocracyInfoRecordVipDataPrivilegesFromJson(YBDAristocracyInfoRecordVipDataPrivileges data, Map<String, dynamic> json) {
	if (json['privilegeName'] != null) {
		data.privilegeName = json['privilegeName'].toString();
	}
	if (json['privilegeIcon'] != null) {
		data.privilegeIcon = json['privilegeIcon'].toString();
	}
	if (json['privilegeInfo'] != null) {
		data.privilegeInfo = json['privilegeInfo'].toString();
	}
	if (json['privilegeDetailIcon'] != null) {
		data.privilegeDetailIcon = json['privilegeDetailIcon'].toString();
	}
	if (json['index'] != null) {
		data.index = json['index'].toString();
	}
	if (json['havePrivilege'] != null) {
		data.havePrivilege = json['havePrivilege'];
	}
	return data;
}

Map<String, dynamic> yBDAristocracyInfoRecordVipDataPrivilegesToJson(YBDAristocracyInfoRecordVipDataPrivileges entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['privilegeName'] = entity.privilegeName;
	data['privilegeIcon'] = entity.privilegeIcon;
	data['privilegeInfo'] = entity.privilegeInfo;
	data['privilegeDetailIcon'] = entity.privilegeDetailIcon;
	data['index'] = entity.index;
	data['havePrivilege'] = entity.havePrivilege;
	return data;
}

yBDAristocracyInfoRecordVipDataBuyPriceFromJson(YBDAristocracyInfoRecordVipDataBuyPrice data, Map<String, dynamic> json) {
	if (json['productPriceId'] != null) {
		data.productPriceId = json['productPriceId'] is String
				? int.tryParse(json['productPriceId'])
				: json['productPriceId'].toInt();
	}
	if (json['periodType'] != null) {
		data.periodType = json['periodType'].toString();
	}
	if (json['period'] != null) {
		data.period = json['period'] is String
				? int.tryParse(json['period'])
				: json['period'].toInt();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['originalPrice'] != null) {
		data.originalPrice = json['originalPrice'] is String
				? int.tryParse(json['originalPrice'])
				: json['originalPrice'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'];
	}
	if (json['introduction'] != null) {
		data.introduction = json['introduction'].toString();
	}
	return data;
}

Map<String, dynamic> yBDAristocracyInfoRecordVipDataBuyPriceToJson(YBDAristocracyInfoRecordVipDataBuyPrice entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['productPriceId'] = entity.productPriceId;
	data['periodType'] = entity.periodType;
	data['period'] = entity.period;
	data['price'] = entity.price;
	data['originalPrice'] = entity.originalPrice;
	data['index'] = entity.index;
	data['introduction'] = entity.introduction;
	return data;
}