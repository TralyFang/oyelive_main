import 'package:oyelive_main/ui/page/share/entity/friend_ybd_share_resp_entity.dart';
import 'dart:async';

yBDFriendShareRespEntityFromJson(YBDFriendShareRespEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDFriendShareRespData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDFriendShareRespEntityToJson(YBDFriendShareRespEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDFriendShareRespDataFromJson(YBDFriendShareRespData data, Map<String, dynamic> json) {
	if (json['respList'] != null) {
		data.respList = (json['respList'] as List).map((v) => YBDFriendShareRespDataRespList().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDFriendShareRespDataToJson(YBDFriendShareRespData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['respList'] =  entity.respList?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDFriendShareRespDataRespListFromJson(YBDFriendShareRespDataRespList data, Map<String, dynamic> json) {
	if (json['messageId'] != null) {
		data.messageId = json['messageId'].toString();
	}
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['msg'] != null) {
		data.msg = json['msg'].toString();
	}
	if (json['requestId'] != null) {
		data.requestId = json['requestId'].toString();
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDFriendShareRespDataRespListToJson(YBDFriendShareRespDataRespList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['messageId'] = entity.messageId;
	data['code'] = entity.code;
	data['msg'] = entity.msg;
	data['requestId'] = entity.requestId;
	data['success'] = entity.success;
	return data;
}