import 'package:oyelive_main/ui/page/room/entity/mic_ybd_mute_response_message.dart';
import 'dart:async';
import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';

yBDMicMuteResponseMessageFromJson(YBDMicMuteResponseMessage data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['originSequence'] != null) {
		data.originSequence = json['originSequence'] is String
				? int.tryParse(json['originSequence'])
				: json['originSequence'].toInt();
	}
	if (json['direction'] != null) {
		data.direction = json['direction'] is String
				? int.tryParse(json['direction'])
				: json['direction'].toInt();
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['receiver'] != null) {
		data.receiver = json['receiver'].toString();
	}
	if (json['receiverImg'] != null) {
		data.receiverImg = json['receiverImg'].toString();
	}
	if (json['receiverLevel'] != null) {
		data.receiverLevel = json['receiverLevel'] is String
				? int.tryParse(json['receiverLevel'])
				: json['receiverLevel'].toInt();
	}
	if (json['receiverSex'] != null) {
		data.receiverSex = json['receiverSex'] is String
				? int.tryParse(json['receiverSex'])
				: json['receiverSex'].toInt();
	}
	if (json['receiverVip'] != null) {
		data.receiverVip = json['receiverVip'] is String
				? int.tryParse(json['receiverVip'])
				: json['receiverVip'].toInt();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['sender'] != null) {
		data.sender = json['sender'].toString();
	}
	if (json['senderImg'] != null) {
		data.senderImg = json['senderImg'].toString();
	}
	if (json['senderLevel'] != null) {
		data.senderLevel = json['senderLevel'] is String
				? int.tryParse(json['senderLevel'])
				: json['senderLevel'].toInt();
	}
	if (json['senderSex'] != null) {
		data.senderSex = json['senderSex'] is String
				? int.tryParse(json['senderSex'])
				: json['senderSex'].toInt();
	}
	if (json['senderVip'] != null) {
		data.senderVip = json['senderVip'] is String
				? int.tryParse(json['senderVip'])
				: json['senderVip'].toInt();
	}
	if (json['time'] != null) {
		data.time = json['time'].toString();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['sequence'] != null) {
		data.sequence = json['sequence'] is String
				? int.tryParse(json['sequence'])
				: json['sequence'].toInt();
	}
	if (json['commandId'] != null) {
		data.commandId = json['commandId'] is String
				? int.tryParse(json['commandId'])
				: json['commandId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDMicMuteResponseMessageToJson(YBDMicMuteResponseMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['index'] = entity.index;
	data['originSequence'] = entity.originSequence;
	data['direction'] = entity.direction;
	data['destination'] = entity.destination;
	data['fromUser'] = entity.fromUser;
	data['mode'] = entity.mode;
	data['msgType'] = entity.msgType;
	data['receiver'] = entity.receiver;
	data['receiverImg'] = entity.receiverImg;
	data['receiverLevel'] = entity.receiverLevel;
	data['receiverSex'] = entity.receiverSex;
	data['receiverVip'] = entity.receiverVip;
	data['roomId'] = entity.roomId;
	data['sender'] = entity.sender;
	data['senderImg'] = entity.senderImg;
	data['senderLevel'] = entity.senderLevel;
	data['senderSex'] = entity.senderSex;
	data['senderVip'] = entity.senderVip;
	data['time'] = entity.time;
	data['toUser'] = entity.toUser;
	data['sequence'] = entity.sequence;
	data['commandId'] = entity.commandId;
	return data;
}