import 'package:oyelive_main/module/inbox/entity/contact_ybd_info_entity.dart';
import 'package:oyelive_main/module/inbox/entity/conversation_ybd_list_entity.dart';

yBDContactInfoEntityFromJson(YBDContactInfoEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDConversationListData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDContactInfoEntityToJson(YBDContactInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}