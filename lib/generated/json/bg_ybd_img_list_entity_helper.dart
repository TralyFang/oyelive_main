import 'package:oyelive_main/module/status/entity/bg_ybd_img_list_entity.dart';
import 'package:oyelive_main/module/status/entity/random_ybd_background_entity.dart';

yBDBgImgListEntityFromJson(YBDBgImgListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDBgImgListData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDBgImgListEntityToJson(YBDBgImgListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDBgImgListDataFromJson(YBDBgImgListData data, Map<String, dynamic> json) {
	if (json['rows'] != null) {
		data.rows = (json['rows'] as List).map((v) => YBDRandomBackgroundDataBgimage().fromJson(v)).toList();
	}
	if (json['total'] != null) {
		data.total = json['total'] is String
				? int.tryParse(json['total'])
				: json['total'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDBgImgListDataToJson(YBDBgImgListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rows'] =  entity.rows?.map((v) => v?.toJson())?.toList();
	data['total'] = entity.total;
	return data;
}