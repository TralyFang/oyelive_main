import 'package:oyelive_main/module/entity/my_ybd_invites_entity.dart';

yBDMyInvitesEntityFromJson(YBDMyInvitesEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDMyInvitesRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDMyInvitesEntityToJson(YBDMyInvitesEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDMyInvitesRecordFromJson(YBDMyInvitesRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['parentId'] != null) {
		data.parentId = json['parentId'] is String
				? int.tryParse(json['parentId'])
				: json['parentId'].toInt();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['counts'] != null) {
		data.counts = json['counts'] is String
				? int.tryParse(json['counts'])
				: json['counts'].toInt();
	}
	if (json['beans'] != null) {
		data.beans = json['beans'] is String
				? int.tryParse(json['beans'])
				: json['beans'].toInt();
	}
	if (json['rewards'] != null) {
		data.rewards = json['rewards'] is String
				? int.tryParse(json['rewards'])
				: json['rewards'].toInt();
	}
	if (json['parentRewards'] != null) {
		data.parentRewards = json['parentRewards'] is String
				? int.tryParse(json['parentRewards'])
				: json['parentRewards'].toInt();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['extend'] != null) {
		data.extend = json['extend'];
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'] is String
				? int.tryParse(json['createTime'])
				: json['createTime'].toInt();
	}
	if (json['updateTime'] != null) {
		data.updateTime = json['updateTime'] is String
				? int.tryParse(json['updateTime'])
				: json['updateTime'].toInt();
	}
	if (json['maxId'] != null) {
		data.maxId = json['maxId'];
	}
	if (json['supCounts'] != null) {
		data.supCounts = json['supCounts'];
	}
	if (json['supRewards'] != null) {
		data.supRewards = json['supRewards'];
	}
	if (json['beginTime'] != null) {
		data.beginTime = json['beginTime'];
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'];
	}
	if (json['userHead'] != null) {
		data.userHead = json['userHead'].toString();
	}
	if (json['userName'] != null) {
		data.userName = json['userName'].toString();
	}
	if (json['concern'] != null) {
		data.concern = json['concern'];
	}
	if (json['page'] != null) {
		data.page = json['page'] is String
				? int.tryParse(json['page'])
				: json['page'].toInt();
	}
	if (json['pageSize'] != null) {
		data.pageSize = json['pageSize'] is String
				? int.tryParse(json['pageSize'])
				: json['pageSize'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDMyInvitesRecordToJson(YBDMyInvitesRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['type'] = entity.type;
	data['parentId'] = entity.parentId;
	data['userId'] = entity.userId;
	data['counts'] = entity.counts;
	data['beans'] = entity.beans;
	data['rewards'] = entity.rewards;
	data['parentRewards'] = entity.parentRewards;
	data['version'] = entity.version;
	data['extend'] = entity.extend;
	data['createTime'] = entity.createTime;
	data['updateTime'] = entity.updateTime;
	data['maxId'] = entity.maxId;
	data['supCounts'] = entity.supCounts;
	data['supRewards'] = entity.supRewards;
	data['beginTime'] = entity.beginTime;
	data['endTime'] = entity.endTime;
	data['userHead'] = entity.userHead;
	data['userName'] = entity.userName;
	data['concern'] = entity.concern;
	data['page'] = entity.page;
	data['pageSize'] = entity.pageSize;
	return data;
}