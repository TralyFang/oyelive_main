import 'package:oyelive_main/module/room/entity/balance_ybd_record_entity.dart';

yBDBalanceRecordEntityFromJson(YBDBalanceRecordEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDBalanceRecordRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDBalanceRecordEntityToJson(YBDBalanceRecordEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDBalanceRecordRecordFromJson(YBDBalanceRecordRecord data, Map<String, dynamic> json) {
	if (json['bean'] != null) {
		data.bean = json['bean'] is String
				? int.tryParse(json['bean'])
				: json['bean'].toInt();
	}
	if (json['gem'] != null) {
		data.gem = json['gem'] is String
				? int.tryParse(json['gem'])
				: json['gem'].toInt();
	}
	if (json['gold'] != null) {
		data.gold = json['gold'] is String
				? int.tryParse(json['gold'])
				: json['gold'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDBalanceRecordRecordToJson(YBDBalanceRecordRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['bean'] = entity.bean;
	data['gem'] = entity.gem;
	data['gold'] = entity.gold;
	return data;
}