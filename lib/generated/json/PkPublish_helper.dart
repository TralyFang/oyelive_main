import 'package:oyelive_main/common/room_socket/message/common/PkPublish.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';

yBDPkPublishFromJson(YBDPkPublish data, Map<String, dynamic> json) {
	if (json['costTime'] != null) {
		data.costTime = (json['costTime'] as List).map((v) => v).toList().cast<dynamic>();
	}
	if (json['content'] != null) {
		data.content = YBDPkInfoContent().fromJson(json['content']);
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPkPublishToJson(YBDPkPublish entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['costTime'] = entity.costTime;
	data['content'] = entity.content?.toJson();
	data['version'] = entity.version;
	data['destination'] = entity.destination;
	data['fromUser'] = entity.fromUser;
	data['mode'] = entity.mode;
	data['msgType'] = entity.msgType;
	data['roomId'] = entity.roomId;
	data['toUser'] = entity.toUser;
	data['type'] = entity.type;
	return data;
}

yBDPkInfoContentFromJson(YBDPkInfoContent data, Map<String, dynamic> json) {
	if (json['pkId'] != null) {
		data.pkId = json['pkId'] is String
				? int.tryParse(json['pkId'])
				: json['pkId'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['startTime'] != null) {
		data.startTime = json['startTime'] is String
				? int.tryParse(json['startTime'])
				: json['startTime'].toInt();
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'] is String
				? int.tryParse(json['endTime'])
				: json['endTime'].toInt();
	}
	if (json['remainingTime'] != null) {
		data.remainingTime = json['remainingTime'] is String
				? int.tryParse(json['remainingTime'])
				: json['remainingTime'].toInt();
	}
	if (json['sequelType'] != null) {
		data.sequelType = json['sequelType'] is String
				? int.tryParse(json['sequelType'])
				: json['sequelType'].toInt();
	}
	if (json['sourceUser'] != null) {
		data.sourceUser = json['sourceUser'] is String
				? int.tryParse(json['sourceUser'])
				: json['sourceUser'].toInt();
	}
	if (json['sourceRoom'] != null) {
		data.sourceRoom = json['sourceRoom'] is String
				? int.tryParse(json['sourceRoom'])
				: json['sourceRoom'].toInt();
	}
	if (json['sourceGifts'] != null) {
		data.sourceGifts = json['sourceGifts'] is String
				? int.tryParse(json['sourceGifts'])
				: json['sourceGifts'].toInt();
	}
	if (json['sourceOnline'] != null) {
		data.sourceOnline = json['sourceOnline'];
	}
	if (json['sourceTopGifts'] != null) {
		data.sourceTopGifts = (json['sourceTopGifts'] as List).map((v) => YBDPkInfoContentSourceTopGifts().fromJson(v)).toList();
	}
	if (json['targetUser'] != null) {
		data.targetUser = json['targetUser'] is String
				? int.tryParse(json['targetUser'])
				: json['targetUser'].toInt();
	}
	if (json['targetRoom'] != null) {
		data.targetRoom = json['targetRoom'] is String
				? int.tryParse(json['targetRoom'])
				: json['targetRoom'].toInt();
	}
	if (json['targetGifts'] != null) {
		data.targetGifts = json['targetGifts'] is String
				? int.tryParse(json['targetGifts'])
				: json['targetGifts'].toInt();
	}
	if (json['targetOnline'] != null) {
		data.targetOnline = json['targetOnline'];
	}
	if (json['targetTopGifts'] != null) {
		data.targetTopGifts = (json['targetTopGifts'] as List).map((v) => YBDPkInfoContentSourceTopGifts().fromJson(v)).toList();
	}
	if (json['sourceBanMics'] != null) {
		data.sourceBanMics = (json['sourceBanMics'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	if (json['targetBanMics'] != null) {
		data.targetBanMics = (json['targetBanMics'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	if (json['sourceHeadImg'] != null) {
		data.sourceHeadImg = json['sourceHeadImg'].toString();
	}
	if (json['sourceNickName'] != null) {
		data.sourceNickName = json['sourceNickName'].toString();
	}
	if (json['targetHeadImg'] != null) {
		data.targetHeadImg = json['targetHeadImg'].toString();
	}
	if (json['targetNickName'] != null) {
		data.targetNickName = json['targetNickName'].toString();
	}
	if (json['targetToken'] != null) {
		data.targetToken = json['targetToken'].toString();
	}
	if (json['sourceToken'] != null) {
		data.sourceToken = json['sourceToken'].toString();
	}
	if (json['winner'] != null) {
		data.winner = json['winner'] is String
				? int.tryParse(json['winner'])
				: json['winner'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPkInfoContentToJson(YBDPkInfoContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['pkId'] = entity.pkId;
	data['type'] = entity.type;
	data['status'] = entity.status;
	data['startTime'] = entity.startTime;
	data['endTime'] = entity.endTime;
	data['remainingTime'] = entity.remainingTime;
	data['sequelType'] = entity.sequelType;
	data['sourceUser'] = entity.sourceUser;
	data['sourceRoom'] = entity.sourceRoom;
	data['sourceGifts'] = entity.sourceGifts;
	data['sourceOnline'] = entity.sourceOnline;
	data['sourceTopGifts'] =  entity.sourceTopGifts?.map((v) => v?.toJson())?.toList();
	data['targetUser'] = entity.targetUser;
	data['targetRoom'] = entity.targetRoom;
	data['targetGifts'] = entity.targetGifts;
	data['targetOnline'] = entity.targetOnline;
	data['targetTopGifts'] =  entity.targetTopGifts?.map((v) => v?.toJson())?.toList();
	data['sourceBanMics'] = entity.sourceBanMics;
	data['targetBanMics'] = entity.targetBanMics;
	data['sourceHeadImg'] = entity.sourceHeadImg;
	data['sourceNickName'] = entity.sourceNickName;
	data['targetHeadImg'] = entity.targetHeadImg;
	data['targetNickName'] = entity.targetNickName;
	data['targetToken'] = entity.targetToken;
	data['sourceToken'] = entity.sourceToken;
	data['winner'] = entity.winner;
	return data;
}

yBDPkInfoContentSourceTopGiftsFromJson(YBDPkInfoContentSourceTopGifts data, Map<String, dynamic> json) {
	if (json['consume'] != null) {
		data.consume = json['consume'] is String
				? int.tryParse(json['consume'])
				: json['consume'].toInt();
	}
	if (json['user'] != null) {
		data.user = YBDPkInfoContentSourceTopGiftsUser().fromJson(json['user']);
	}
	return data;
}

Map<String, dynamic> yBDPkInfoContentSourceTopGiftsToJson(YBDPkInfoContentSourceTopGifts entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['consume'] = entity.consume;
	data['user'] = entity.user?.toJson();
	return data;
}

yBDPkInfoContentSourceTopGiftsUserFromJson(YBDPkInfoContentSourceTopGiftsUser data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['agency'] != null) {
		data.agency = json['agency'];
	}
	if (json['crest'] != null) {
		data.crest = json['crest'];
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['headimg'] != null) {
		data.headimg = json['headimg'].toString();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['roomlevel'] != null) {
		data.roomlevel = json['roomlevel'] is String
				? int.tryParse(json['roomlevel'])
				: json['roomlevel'].toInt();
	}
	if (json['vip'] != null) {
		data.vip = json['vip'] is String
				? int.tryParse(json['vip'])
				: json['vip'].toInt();
	}
	if (json['area'] != null) {
		data.area = json['area'];
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	if (json['tags'] != null) {
		data.tags = json['tags'];
	}
	return data;
}

Map<String, dynamic> yBDPkInfoContentSourceTopGiftsUserToJson(YBDPkInfoContentSourceTopGiftsUser entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['agency'] = entity.agency;
	data['crest'] = entity.crest;
	data['nickname'] = entity.nickname;
	data['headimg'] = entity.headimg;
	data['level'] = entity.level;
	data['roomlevel'] = entity.roomlevel;
	data['vip'] = entity.vip;
	data['area'] = entity.area;
	data['sex'] = entity.sex;
	data['tags'] = entity.tags;
	return data;
}