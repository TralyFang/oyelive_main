import 'package:oyelive_main/module/entity/invite_ybd_info_entity.dart';

yBDInviteInfoEntityFromJson(YBDInviteInfoEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = json['record'].toString();
	}
	return data;
}

Map<String, dynamic> yBDInviteInfoEntityToJson(YBDInviteInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record;
	return data;
}