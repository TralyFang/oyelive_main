import 'package:oyelive_main/ui/widget/rich_ybd_text_decoder.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/utils/quick_ybd_msg.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/go_ybd_tab_event.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';

yBDPlainTextFromJson(YBDPlainText data, Map<String, dynamic> json) {
	if (json['defaultSize'] != null) {
		data.defaultSize = json['defaultSize'] is String
				? num.tryParse(json['defaultSize'])
				: json['defaultSize'];
	}
	if (json['defaultColor'] != null) {
		data.defaultColor = json['defaultColor'].toString();
	}
	if (json['defaultFamily'] != null) {
		data.defaultFamily = json['defaultFamily'].toString();
	}
	if (json['defaultWeight'] != null) {
		data.defaultWeight = json['defaultWeight'] is String
				? int.tryParse(json['defaultWeight'])
				: json['defaultWeight'].toInt();
	}
	if (json['richLines'] != null) {
		data.richLines = (json['richLines'] as List).map((v) => YBDRichLines().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDPlainTextToJson(YBDPlainText entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['defaultSize'] = entity.defaultSize;
	data['defaultColor'] = entity.defaultColor;
	data['defaultFamily'] = entity.defaultFamily;
	data['defaultWeight'] = entity.defaultWeight;
	data['richLines'] =  entity.richLines?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDRichLinesFromJson(YBDRichLines data, Map<String, dynamic> json) {
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['fontSize'] != null) {
		data.fontSize = json['fontSize'] is String
				? num.tryParse(json['fontSize'])
				: json['fontSize'];
	}
	if (json['fontWeight'] != null) {
		data.fontWeight = json['fontWeight'] is String
				? num.tryParse(json['fontWeight'])
				: json['fontWeight'];
	}
	if (json['fontFamily'] != null) {
		data.fontFamily = json['fontFamily'].toString();
	}
	if (json['color'] != null) {
		data.color = json['color'].toString();
	}
	if (json['content'] != null) {
		data.content = json['content'].toString();
	}
	if (json['width'] != null) {
		data.width = json['width'] is String
				? num.tryParse(json['width'])
				: json['width'];
	}
	if (json['height'] != null) {
		data.height = json['height'] is String
				? num.tryParse(json['height'])
				: json['height'];
	}
	if (json['source'] != null) {
		data.source = json['source'].toString();
	}
	if (json['path'] != null) {
		data.path = json['path'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRichLinesToJson(YBDRichLines entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['destination'] = entity.destination;
	data['type'] = entity.type;
	data['fontSize'] = entity.fontSize;
	data['fontWeight'] = entity.fontWeight;
	data['fontFamily'] = entity.fontFamily;
	data['color'] = entity.color;
	data['content'] = entity.content;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['source'] = entity.source;
	data['path'] = entity.path;
	return data;
}

yBDResourcesFromJson(YBDResources data, Map<String, dynamic> json) {
	if (json['user'] != null) {
		data.user = json['user'];
	}
	return data;
}

Map<String, dynamic> yBDResourcesToJson(YBDResources entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['user'] = entity.user;
	return data;
}