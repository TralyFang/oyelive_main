import 'package:oyelive_main/common/web_socket/tpg_match/entity/match_ybd_confirm_entity.dart';

yBDMatchConfirmEntityFromJson(YBDMatchConfirmEntity data, Map<String, dynamic> json) {
	if (json['collectionId'] != null) {
		data.collectionId = json['collectionId'].toString();
	}
	if (json['connectionRoomUrl'] != null) {
		data.connectionRoomUrl = json['connectionRoomUrl'].toString();
	}
	if (json['command'] != null) {
		data.command = json['command'].toString();
	}
	if (json['confirmCommandTime'] != null) {
		data.confirmCommandTime = json['confirmCommandTime'] is String
				? int.tryParse(json['confirmCommandTime'])
				: json['confirmCommandTime'].toInt();
	}
	if (json['confirmTimeout'] != null) {
		data.confirmTimeout = json['confirmTimeout'] is String
				? int.tryParse(json['confirmTimeout'])
				: json['confirmTimeout'].toInt();
	}
	if (json['finalConfirmStatus'] != null) {
		data.finalConfirmStatus = json['finalConfirmStatus'] is String
				? int.tryParse(json['finalConfirmStatus'])
				: json['finalConfirmStatus'].toInt();
	}
	if (json['gameId'] != null) {
		data.gameId = json['gameId'].toString();
	}
	if (json['matchId'] != null) {
		data.matchId = json['matchId'].toString();
	}
	if (json['matchStatus'] != null) {
		data.matchStatus = json['matchStatus'].toString();
	}
	if (json['matchSuccessTime'] != null) {
		data.matchSuccessTime = json['matchSuccessTime'] is String
				? int.tryParse(json['matchSuccessTime'])
				: json['matchSuccessTime'].toInt();
	}
	if (json['matchType'] != null) {
		data.matchType = json['matchType'].toString();
	}
	if (json['needUserConfirm'] != null) {
		data.needUserConfirm = json['needUserConfirm'] is String
				? int.tryParse(json['needUserConfirm'])
				: json['needUserConfirm'].toInt();
	}
	if (json['operationTime'] != null) {
		data.operationTime = json['operationTime'] is String
				? int.tryParse(json['operationTime'])
				: json['operationTime'].toInt();
	}
	if (json['userConfirmStatus'] != null) {
		data.userConfirmStatus = json['userConfirmStatus'] is String
				? int.tryParse(json['userConfirmStatus'])
				: json['userConfirmStatus'].toInt();
	}
	if (json['userInfo'] != null) {
		data.userInfo = YBDMatchConfirmUserInfo().fromJson(json['userInfo']);
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDMatchConfirmEntityToJson(YBDMatchConfirmEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['collectionId'] = entity.collectionId;
	data['connectionRoomUrl'] = entity.connectionRoomUrl;
	data['command'] = entity.command;
	data['confirmCommandTime'] = entity.confirmCommandTime;
	data['confirmTimeout'] = entity.confirmTimeout;
	data['finalConfirmStatus'] = entity.finalConfirmStatus;
	data['gameId'] = entity.gameId;
	data['matchId'] = entity.matchId;
	data['matchStatus'] = entity.matchStatus;
	data['matchSuccessTime'] = entity.matchSuccessTime;
	data['matchType'] = entity.matchType;
	data['needUserConfirm'] = entity.needUserConfirm;
	data['operationTime'] = entity.operationTime;
	data['userConfirmStatus'] = entity.userConfirmStatus;
	data['userInfo'] = entity.userInfo?.toJson();
	data['version'] = entity.version;
	return data;
}

yBDMatchConfirmUserInfoFromJson(YBDMatchConfirmUserInfo data, Map<String, dynamic> json) {
	if (json['country'] != null) {
		data.country = json['country'].toString();
	}
	if (json['gems'] != null) {
		data.gems = json['gems'] is String
				? int.tryParse(json['gems'])
				: json['gems'].toInt();
	}
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['matchOperationTime'] != null) {
		data.matchOperationTime = json['matchOperationTime'] is String
				? int.tryParse(json['matchOperationTime'])
				: json['matchOperationTime'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDMatchConfirmUserInfoToJson(YBDMatchConfirmUserInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['country'] = entity.country;
	data['gems'] = entity.gems;
	data['id'] = entity.id;
	data['matchOperationTime'] = entity.matchOperationTime;
	data['nickname'] = entity.nickname;
	data['sex'] = entity.sex;
	return data;
}