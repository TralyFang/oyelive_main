import 'package:oyelive_main/ui/page/game_lobby/entity/enter_ybd_room_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

yBDEnterRoomEntityFromJson(YBDEnterRoomEntity data, Map<String, dynamic> json) {
	if (json['body'] != null) {
		data.body = YBDEnterRoomBody().fromJson(json['body']);
	}
	if (json['cmd'] != null) {
		data.cmd = json['cmd'].toString();
	}
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['codeMsg'] != null) {
		data.codeMsg = json['codeMsg'].toString();
	}
	if (json['header'] != null) {
		data.header = YBDEnterRoomHeader().fromJson(json['header']);
	}
	if (json['route'] != null) {
		data.route = YBDEnterRoomRoute().fromJson(json['route']);
	}
	if (json['ver'] != null) {
		data.ver = json['ver'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEnterRoomEntityToJson(YBDEnterRoomEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['body'] = entity.body?.toJson();
	data['cmd'] = entity.cmd;
	data['code'] = entity.code;
	data['codeMsg'] = entity.codeMsg;
	data['header'] = entity.header?.toJson();
	data['route'] = entity.route?.toJson();
	data['ver'] = entity.ver;
	return data;
}

yBDEnterRoomBodyFromJson(YBDEnterRoomBody data, Map<String, dynamic> json) {
	if (json['wsUrl'] != null) {
		data.wsUrl = json['wsUrl'].toString();
	}
	if (json['entryFrom'] != null) {
		data.entryFrom = json['entryFrom'].toString();
	}
	if (json['quickMsgConfig'] != null) {
		data.quickMsgConfig = json['quickMsgConfig'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEnterRoomBodyToJson(YBDEnterRoomBody entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['wsUrl'] = entity.wsUrl;
	data['entryFrom'] = entity.entryFrom;
	data['quickMsgConfig'] = entity.quickMsgConfig;
	return data;
}

yBDEnterRoomHeaderFromJson(YBDEnterRoomHeader data, Map<String, dynamic> json) {
	if (json['connection-id'] != null) {
		data.connectionId = json['connection-id'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEnterRoomHeaderToJson(YBDEnterRoomHeader entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['connection-id'] = entity.connectionId;
	return data;
}

yBDEnterRoomRouteFromJson(YBDEnterRoomRoute data, Map<String, dynamic> json) {
	if (json['from'] != null) {
		data.from = json['from'].toString();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['room'] != null) {
		data.room = json['room'].toString();
	}
	if (json['to'] != null) {
		data.to = (json['to'] as List).map((v) => v.toString()).toList().cast<String>();
	}
	return data;
}

Map<String, dynamic> yBDEnterRoomRouteToJson(YBDEnterRoomRoute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['from'] = entity.from;
	data['mode'] = entity.mode;
	data['room'] = entity.room;
	data['to'] = entity.to;
	return data;
}