import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'dart:async';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

yBDTpRoomEntityFromJson(YBDTpRoomEntity data, Map<String, dynamic> json) {
	if (json['command'] != null) {
		data.command = json['command'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDTpRoomData().fromJson(json['data']);
	}
	if (json['code'] != null) {
		data.code = json['code'] is String
				? int.tryParse(json['code'])
				: json['code'].toInt();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	return data;
}

Map<String, dynamic> yBDTpRoomEntityToJson(YBDTpRoomEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['command'] = entity.command;
	data['data'] = entity.data?.toJson();
	data['code'] = entity.code;
	data['message'] = entity.message;
	return data;
}

yBDTpRoomDataFromJson(YBDTpRoomData data, Map<String, dynamic> json) {
	if (json['battle'] != null) {
		data.battle = YBDTpRoomDataBattle().fromJson(json['battle']);
	}
	if (json['users'] != null) {
		data.users = (json['users'] as List).map((v) => YBDTpRoomDataUser().fromJson(v)).toList();
	}
	if (json['animation'] != null) {
		data.animation = (json['animation'] as List).map((v) => YBDTpRoomDataAnimation().fromJson(v)).toList();
	}
	if (json['code'] != null) {
		data.code = json['code'] is String
				? int.tryParse(json['code'])
				: json['code'].toInt();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'].toString();
	}
	if (json['standUpUser'] != null) {
		data.standUpUser = (json['standUpUser'] as List).map((v) => YBDTpRoomStandUpUser().fromJson(v)).toList();
	}
	if (json['leaderId'] != null) {
		data.leaderId = json['leaderId'] is String
				? int.tryParse(json['leaderId'])
				: json['leaderId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDTpRoomDataToJson(YBDTpRoomData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['battle'] = entity.battle?.toJson();
	data['users'] =  entity.users?.map((v) => v?.toJson())?.toList();
	data['animation'] =  entity.animation?.map((v) => v?.toJson())?.toList();
	data['code'] = entity.code;
	data['message'] = entity.message;
	data['roomId'] = entity.roomId;
	data['standUpUser'] =  entity.standUpUser?.map((v) => v?.toJson())?.toList();
	data['leaderId'] = entity.leaderId;
	return data;
}

yBDTpRoomStandUpUserFromJson(YBDTpRoomStandUpUser data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['beforePosition'] != null) {
		data.beforePosition = json['beforePosition'] is String
				? int.tryParse(json['beforePosition'])
				: json['beforePosition'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDTpRoomStandUpUserToJson(YBDTpRoomStandUpUser entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['beforePosition'] = entity.beforePosition;
	return data;
}

yBDTpRoomDataAnimationFromJson(YBDTpRoomDataAnimation data, Map<String, dynamic> json) {
	if (json['beans'] != null) {
		data.beans = json['beans'] is String
				? int.tryParse(json['beans'])
				: json['beans'].toInt();
	}
	if (json['operator'] != null) {
		data.operator = json['operator'] is String
				? int.tryParse(json['operator'])
				: json['operator'].toInt();
	}
	if (json['timeout'] != null) {
		data.timeout = json['timeout'] is String
				? int.tryParse(json['timeout'])
				: json['timeout'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['sync'] != null) {
		data.sync = json['sync'] is String
				? int.tryParse(json['sync'])
				: json['sync'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDTpRoomDataAnimationToJson(YBDTpRoomDataAnimation entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['beans'] = entity.beans;
	data['operator'] = entity.operator;
	data['timeout'] = entity.timeout;
	data['type'] = entity.type;
	data['sync'] = entity.sync;
	return data;
}

yBDTpRoomDataBattleFromJson(YBDTpRoomDataBattle data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['subStatus'] != null) {
		data.subStatus = json['subStatus'] is String
				? int.tryParse(json['subStatus'])
				: json['subStatus'].toInt();
	}
	if (json['startTime'] != null) {
		data.startTime = json['startTime'] is String
				? int.tryParse(json['startTime'])
				: json['startTime'].toInt();
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'] is String
				? int.tryParse(json['endTime'])
				: json['endTime'].toInt();
	}
	if (json['currentOperator'] != null) {
		data.currentOperator = json['currentOperator'] is String
				? int.tryParse(json['currentOperator'])
				: json['currentOperator'].toInt();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'].toString();
	}
	if (json['term'] != null) {
		data.term = json['term'] is String
				? int.tryParse(json['term'])
				: json['term'].toInt();
	}
	if (json['maxTerm'] != null) {
		data.maxTerm = json['maxTerm'] is String
				? int.tryParse(json['maxTerm'])
				: json['maxTerm'].toInt();
	}
	if (json['boot'] != null) {
		data.boot = json['boot'] is String
				? int.tryParse(json['boot'])
				: json['boot'].toInt();
	}
	if (json['chaalValue'] != null) {
		data.chaalValue = json['chaalValue'] is String
				? int.tryParse(json['chaalValue'])
				: json['chaalValue'].toInt();
	}
	if (json['minChaalValue'] != null) {
		data.minChaalValue = json['minChaalValue'] is String
				? int.tryParse(json['minChaalValue'])
				: json['minChaalValue'].toInt();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['timeout'] != null) {
		data.timeout = json['timeout'] is String
				? int.tryParse(json['timeout'])
				: json['timeout'].toInt();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['optTimesRemaining'] != null) {
		data.optTimesRemaining = json['optTimesRemaining'] is String
				? int.tryParse(json['optTimesRemaining'])
				: json['optTimesRemaining'].toInt();
	}
	if (json['optExpireAt'] != null) {
		data.optExpireAt = json['optExpireAt'] is String
				? int.tryParse(json['optExpireAt'])
				: json['optExpireAt'].toInt();
	}
	if (json['settle'] != null) {
		data.settle = (json['settle'] as List).map((v) => YBDTpRoomDataSettle().fromJson(v)).toList();
	}
	if (json['winners'] != null) {
		data.winners = (json['winners'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	if (json['finishBeforeAction'] != null) {
		data.finishBeforeAction = json['finishBeforeAction'] is String
				? int.tryParse(json['finishBeforeAction'])
				: json['finishBeforeAction'].toInt();
	}
	if (json['players'] != null) {
		data.players = (json['players'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	return data;
}

Map<String, dynamic> yBDTpRoomDataBattleToJson(YBDTpRoomDataBattle entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['status'] = entity.status;
	data['subStatus'] = entity.subStatus;
	data['startTime'] = entity.startTime;
	data['endTime'] = entity.endTime;
	data['currentOperator'] = entity.currentOperator;
	data['roomId'] = entity.roomId;
	data['term'] = entity.term;
	data['maxTerm'] = entity.maxTerm;
	data['boot'] = entity.boot;
	data['chaalValue'] = entity.chaalValue;
	data['minChaalValue'] = entity.minChaalValue;
	data['currency'] = entity.currency;
	data['timeout'] = entity.timeout;
	data['version'] = entity.version;
	data['optTimesRemaining'] = entity.optTimesRemaining;
	data['optExpireAt'] = entity.optExpireAt;
	data['settle'] =  entity.settle?.map((v) => v?.toJson())?.toList();
	data['winners'] = entity.winners;
	data['finishBeforeAction'] = entity.finishBeforeAction;
	data['players'] = entity.players;
	return data;
}

yBDTpRoomDataSettleFromJson(YBDTpRoomDataSettle data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['revenue'] != null) {
		data.revenue = json['revenue'] is String
				? int.tryParse(json['revenue'])
				: json['revenue'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDTpRoomDataSettleToJson(YBDTpRoomDataSettle entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['revenue'] = entity.revenue;
	data['index'] = entity.index;
	return data;
}

yBDTpRoomDataUserFromJson(YBDTpRoomDataUser data, Map<String, dynamic> json) {
	if (json['battleId'] != null) {
		data.battleId = json['battleId'] is String
				? int.tryParse(json['battleId'])
				: json['battleId'].toInt();
	}
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['term'] != null) {
		data.term = json['term'] is String
				? int.tryParse(json['term'])
				: json['term'].toInt();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['position'] != null) {
		data.position = json['position'] is String
				? int.tryParse(json['position'])
				: json['position'].toInt();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'].toString();
	}
	if (json['role'] != null) {
		data.role = json['role'] is String
				? int.tryParse(json['role'])
				: json['role'].toInt();
	}
	if (json['gender'] != null) {
		data.gender = json['gender'] is String
				? int.tryParse(json['gender'])
				: json['gender'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['chaalValue'] != null) {
		data.chaalValue = json['chaalValue'] is String
				? int.tryParse(json['chaalValue'])
				: json['chaalValue'].toInt();
	}
	if (json['cards'] != null) {
		data.cards = (json['cards'] as List).map((v) => YBDTpRoomDataCard().fromJson(v)).toList();
	}
	if (json['cardState'] != null) {
		data.cardState = json['cardState'] is String
				? int.tryParse(json['cardState'])
				: json['cardState'].toInt();
	}
	if (json['optPrivilege'] != null) {
		data.optPrivilege = YBDTpRoomDataUserOpt().fromJson(json['optPrivilege']);
	}
	if (json['balance'] != null) {
		data.balance = json['balance'] is String
				? int.tryParse(json['balance'])
				: json['balance'].toInt();
	}
	if (json['hasBet'] != null) {
		data.hasBet = json['hasBet'] is String
				? int.tryParse(json['hasBet'])
				: json['hasBet'].toInt();
	}
	if (json['isStandUp'] != null) {
		data.isStandUp = json['isStandUp'];
	}
	if (json['hasBetAfterSeen'] != null) {
		data.hasBetAfterSeen = json['hasBetAfterSeen'] is String
				? int.tryParse(json['hasBetAfterSeen'])
				: json['hasBetAfterSeen'].toInt();
	}
	if (json['userTag'] != null) {
		data.userTag = json['userTag'] is String
				? int.tryParse(json['userTag'])
				: json['userTag'].toInt();
	}
	if (json['iframe'] != null) {
		data.iframe = json['iframe'].toString();
	}
	return data;
}

Map<String, dynamic> yBDTpRoomDataUserToJson(YBDTpRoomDataUser entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['battleId'] = entity.battleId;
	data['id'] = entity.id;
	data['term'] = entity.term;
	data['roomId'] = entity.roomId;
	data['position'] = entity.position;
	data['avatar'] = entity.avatar;
	data['role'] = entity.role;
	data['gender'] = entity.gender;
	data['nickname'] = entity.nickname;
	data['chaalValue'] = entity.chaalValue;
	data['cards'] =  entity.cards?.map((v) => v?.toJson())?.toList();
	data['cardState'] = entity.cardState;
	data['optPrivilege'] = entity.optPrivilege?.toJson();
	data['balance'] = entity.balance;
	data['hasBet'] = entity.hasBet;
	data['isStandUp'] = entity.isStandUp;
	data['hasBetAfterSeen'] = entity.hasBetAfterSeen;
	data['userTag'] = entity.userTag;
	data['iframe'] = entity.iframe;
	return data;
}

yBDTpRoomDataCardFromJson(YBDTpRoomDataCard data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['number'] != null) {
		data.number = json['number'] is String
				? int.tryParse(json['number'])
				: json['number'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDTpRoomDataCardToJson(YBDTpRoomDataCard entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['number'] = entity.number;
	return data;
}

yBDTpRoomDataUserOptFromJson(YBDTpRoomDataUserOpt data, Map<String, dynamic> json) {
	if (json['canChaal'] != null) {
		data.canChaal = json['canChaal'];
	}
	if (json['canDoubleBet'] != null) {
		data.canDoubleBet = json['canDoubleBet'];
	}
	if (json['canComparedCard'] != null) {
		data.canComparedCard = json['canComparedCard'];
	}
	if (json['canShowCard'] != null) {
		data.canShowCard = json['canShowCard'];
	}
	if (json['canPackCard'] != null) {
		data.canPackCard = json['canPackCard'];
	}
	if (json['canSeeCard'] != null) {
		data.canSeeCard = json['canSeeCard'];
	}
	return data;
}

Map<String, dynamic> yBDTpRoomDataUserOptToJson(YBDTpRoomDataUserOpt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['canChaal'] = entity.canChaal;
	data['canDoubleBet'] = entity.canDoubleBet;
	data['canComparedCard'] = entity.canComparedCard;
	data['canShowCard'] = entity.canShowCard;
	data['canPackCard'] = entity.canPackCard;
	data['canSeeCard'] = entity.canSeeCard;
	return data;
}