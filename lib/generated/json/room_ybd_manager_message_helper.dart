import 'package:oyelive_main/common/room_socket/message/common/room_ybd_manager_message.dart';

yBDRoomManagerMessageFromJson(YBDRoomManagerMessage data, Map<String, dynamic> json) {
	if (json['operateType'] != null) {
		data.operateType = json['operateType'] is String
				? int.tryParse(json['operateType'])
				: json['operateType'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDRoomManagerMessageToJson(YBDRoomManagerMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['operateType'] = entity.operateType;
	return data;
}