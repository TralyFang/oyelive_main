import 'package:oyelive_main/module/status/entity/random_ybd_background_entity.dart';

yBDRandomBackgroundEntityFromJson(YBDRandomBackgroundEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDRandomBackgroundData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDRandomBackgroundEntityToJson(YBDRandomBackgroundEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDRandomBackgroundDataFromJson(YBDRandomBackgroundData data, Map<String, dynamic> json) {
	if (json['bgimage'] != null) {
		data.bgimage = YBDRandomBackgroundDataBgimage().fromJson(json['bgimage']);
	}
	return data;
}

Map<String, dynamic> yBDRandomBackgroundDataToJson(YBDRandomBackgroundData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['bgimage'] = entity.bgimage?.toJson();
	return data;
}

yBDRandomBackgroundDataBgimageFromJson(YBDRandomBackgroundDataBgimage data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['resource'] != null) {
		data.resource = json['resource'].toString();
	}
	if (json['top'] != null) {
		data.top = json['top'] is String
				? int.tryParse(json['top'])
				: json['top'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDRandomBackgroundDataBgimageToJson(YBDRandomBackgroundDataBgimage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['resource'] = entity.resource;
	data['top'] = entity.top;
	return data;
}