import 'package:oyelive_main/common/room_socket/entity/fans_ybd_info.dart';

yBDFansInfoFromJson(YBDFansInfo data, Map<String, dynamic> json) {
	if (json['senderId'] != null) {
		data.senderId = json['senderId'] is String
				? int.tryParse(json['senderId'])
				: json['senderId'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['score'] != null) {
		data.score = json['score'] is String
				? int.tryParse(json['score'])
				: json['score'].toInt();
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['agency'] != null) {
		data.agency = json['agency'] is String
				? int.tryParse(json['agency'])
				: json['agency'].toInt();
	}
	if (json['vip'] != null) {
		data.vip = json['vip'] is String
				? int.tryParse(json['vip'])
				: json['vip'].toInt();
	}
	if (json['vipIcon'] != null) {
		data.vipIcon = json['vipIcon'].toString();
	}
	return data;
}

Map<String, dynamic> yBDFansInfoToJson(YBDFansInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['senderId'] = entity.senderId;
	data['nickname'] = entity.nickname;
	data['level'] = entity.level;
	data['img'] = entity.img;
	data['score'] = entity.score;
	data['sex'] = entity.sex;
	data['status'] = entity.status;
	data['agency'] = entity.agency;
	data['vip'] = entity.vip;
	data['vipIcon'] = entity.vipIcon;
	return data;
}