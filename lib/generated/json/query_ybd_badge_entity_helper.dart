import 'package:oyelive_main/module/entity/query_ybd_badge_entity.dart';
import 'dart:convert';
import '../../ui/page/profile/my_profile/ctrl/badge_ybd_color.dart';

yBDQueryBadgeEntityFromJson(YBDQueryBadgeEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDQueryBadgeRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDQueryBadgeEntityToJson(YBDQueryBadgeEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDQueryBadgeRecordFromJson(YBDQueryBadgeRecord data, Map<String, dynamic> json) {
	if (json['badges'] != null) {
		data.badges = (json['badges'] as List).map((v) => YBDQueryBadgeRecordBadges().fromJson(v)).toList();
	}
	if (json['userBadge'] != null) {
		data.userBadge = (json['userBadge'] as List).map((v) => YBDQueryBadgeRecordUserBadge().fromJson(v)).toList();
	}
	if (json['wearBadge'] != null) {
		data.wearBadge = json['wearBadge'].toString();
	}
	return data;
}

Map<String, dynamic> yBDQueryBadgeRecordToJson(YBDQueryBadgeRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['badges'] =  entity.badges?.map((v) => v?.toJson())?.toList();
	data['userBadge'] =  entity.userBadge?.map((v) => v?.toJson())?.toList();
	data['wearBadge'] = entity.wearBadge;
	return data;
}

yBDQueryBadgeRecordBadgesFromJson(YBDQueryBadgeRecordBadges data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['badgeCode'] != null) {
		data.badgeCode = json['badgeCode'].toString();
	}
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['context'] != null) {
		data.context = json['context'].toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['beginTime'] != null) {
		data.beginTime = json['beginTime'];
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'];
	}
	if (json['business'] != null) {
		data.business = json['business'].toString();
	}
	if (json['extend'] != null) {
		data.extend = json['extend'];
	}
	if (json['remarks'] != null) {
		data.remarks = json['remarks'];
	}
	if (json['sort'] != null) {
		data.sort = json['sort'] is String
				? int.tryParse(json['sort'])
				: json['sort'].toInt();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'] is String
				? int.tryParse(json['createTime'])
				: json['createTime'].toInt();
	}
	if (json['updateTime'] != null) {
		data.updateTime = json['updateTime'] is String
				? int.tryParse(json['updateTime'])
				: json['updateTime'].toInt();
	}
	if (json['speed'] != null) {
		data.speed = YBDQueryBadgeRecordBadgesSpeed().fromJson(json['speed']);
	}
	if (json['get'] != null) {
		data.xGet = json['get'];
	}
	if (json['isRecive'] != null) {
		data.isRecive = json['isRecive'];
	}
	return data;
}

Map<String, dynamic> yBDQueryBadgeRecordBadgesToJson(YBDQueryBadgeRecordBadges entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['badgeCode'] = entity.badgeCode;
	data['type'] = entity.type;
	data['title'] = entity.title;
	data['context'] = entity.context;
	data['icon'] = entity.icon;
	data['status'] = entity.status;
	data['beginTime'] = entity.beginTime;
	data['endTime'] = entity.endTime;
	data['business'] = entity.business;
	data['extend'] = entity.extend;
	data['remarks'] = entity.remarks;
	data['sort'] = entity.sort;
	data['version'] = entity.version;
	data['createTime'] = entity.createTime;
	data['updateTime'] = entity.updateTime;
	data['speed'] = entity.speed?.toJson();
	data['get'] = entity.xGet;
	data['isRecive'] = entity.isRecive;
	return data;
}

yBDQueryBadgeRecordBadgesSpeedFromJson(YBDQueryBadgeRecordBadgesSpeed data, Map<String, dynamic> json) {
	if (json['max'] != null) {
		data.max = json['max'] is String
				? int.tryParse(json['max'])
				: json['max'].toInt();
	}
	if (json['beans'] != null) {
		data.beans = json['beans'] is String
				? int.tryParse(json['beans'])
				: json['beans'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDQueryBadgeRecordBadgesSpeedToJson(YBDQueryBadgeRecordBadgesSpeed entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['max'] = entity.max;
	data['beans'] = entity.beans;
	return data;
}

yBDQueryBadgeRecordUserBadgeFromJson(YBDQueryBadgeRecordUserBadge data, Map<String, dynamic> json) {
	if (json['orderId'] != null) {
		data.orderId = json['orderId'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['channel'] != null) {
		data.channel = json['channel'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	return data;
}

Map<String, dynamic> yBDQueryBadgeRecordUserBadgeToJson(YBDQueryBadgeRecordUserBadge entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['orderId'] = entity.orderId;
	data['name'] = entity.name;
	data['channel'] = entity.channel;
	data['status'] = entity.status;
	return data;
}