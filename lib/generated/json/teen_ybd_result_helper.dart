import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_result.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_info.dart';

yBDResultFromJson(YBDResult data, Map<String, dynamic> json) {
	if (json['gameId'] != null) {
		data.gameId = json['gameId'] is String
				? int.tryParse(json['gameId'])
				: json['gameId'].toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['remainSecToNextStatus'] != null) {
		data.remainSecToNextStatus = json['remainSecToNextStatus'] is String
				? int.tryParse(json['remainSecToNextStatus'])
				: json['remainSecToNextStatus'].toInt();
	}
	if (json['startTime'] != null) {
		data.startTime = json['startTime'] is String
				? int.tryParse(json['startTime'])
				: json['startTime'].toInt();
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'] is String
				? int.tryParse(json['endTime'])
				: json['endTime'].toInt();
	}
	if (json['lastStatusTime'] != null) {
		data.lastStatusTime = json['lastStatusTime'] is String
				? int.tryParse(json['lastStatusTime'])
				: json['lastStatusTime'].toInt();
	}
	if (json['bidLimit'] != null) {
		data.bidLimit = json['bidLimit'] is String
				? int.tryParse(json['bidLimit'])
				: json['bidLimit'].toInt();
	}
	if (json['prizeTimes'] != null) {
		data.prizeTimes = json['prizeTimes'] is String
				? double.tryParse(json['prizeTimes'])
				: json['prizeTimes'].toDouble();
	}
	if (json['bidValue1'] != null) {
		data.bidValue1 = json['bidValue1'] is String
				? int.tryParse(json['bidValue1'])
				: json['bidValue1'].toInt();
	}
	if (json['bidValue2'] != null) {
		data.bidValue2 = json['bidValue2'] is String
				? int.tryParse(json['bidValue2'])
				: json['bidValue2'].toInt();
	}
	if (json['bidValue3'] != null) {
		data.bidValue3 = json['bidValue3'] is String
				? int.tryParse(json['bidValue3'])
				: json['bidValue3'].toInt();
	}
	if (json['balance'] != null) {
		data.balance = json['balance'] is String
				? int.tryParse(json['balance'])
				: json['balance'].toInt();
	}
	if (json['totalBidValue1'] != null) {
		data.totalBidValue1 = json['totalBidValue1'] is String
				? int.tryParse(json['totalBidValue1'])
				: json['totalBidValue1'].toInt();
	}
	if (json['totalBidValue2'] != null) {
		data.totalBidValue2 = json['totalBidValue2'] is String
				? int.tryParse(json['totalBidValue2'])
				: json['totalBidValue2'].toInt();
	}
	if (json['totalBidValue3'] != null) {
		data.totalBidValue3 = json['totalBidValue3'] is String
				? int.tryParse(json['totalBidValue3'])
				: json['totalBidValue3'].toInt();
	}
	if (json['hands'] != null) {
		data.hands = (json['hands'] as List).map((v) => YBDHands().fromJson(v)).toList() as List<YBDHands>?;
	}
	return data;
}

Map<String, dynamic> yBDResultToJson(YBDResult entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['gameId'] = entity.gameId;
	data['status'] = entity.status;
	data['remainSecToNextStatus'] = entity.remainSecToNextStatus;
	data['startTime'] = entity.startTime;
	data['endTime'] = entity.endTime;
	data['lastStatusTime'] = entity.lastStatusTime;
	data['bidLimit'] = entity.bidLimit;
	data['prizeTimes'] = entity.prizeTimes;
	data['bidValue1'] = entity.bidValue1;
	data['bidValue2'] = entity.bidValue2;
	data['bidValue3'] = entity.bidValue3;
	data['balance'] = entity.balance;
	data['totalBidValue1'] = entity.totalBidValue1;
	data['totalBidValue2'] = entity.totalBidValue2;
	data['totalBidValue3'] = entity.totalBidValue3;
	data['hands'] =  entity.hands?.map((v) => v.toJson())?.toList();
	return data;
}