import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_entry_notify_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/game_room/entity/enter_ybd_room_body_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_user_attribute.dart';

yBDGameEntryNotifyEntityFromJson(YBDGameEntryNotifyEntity data, Map<String, dynamic> json) {
	if (json['body'] != null) {
		data.body = YBDGameEntryNotifyBody().fromJson(json['body']);
	}
	if (json['cmd'] != null) {
		data.cmd = json['cmd'].toString();
	}
	if (json['resources'] != null) {
		data.resources = YBDGameEntryNotifyResources().fromJson(json['resources']);
	}
	if (json['route'] != null) {
		data.route = YBDEnterRoomBodyEntityRoute().fromJson(json['route']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDGameEntryNotifyEntityToJson(YBDGameEntryNotifyEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['body'] = entity.body?.toJson();
	data['cmd'] = entity.cmd;
	data['resources'] = entity.resources?.toJson();
	data['route'] = entity.route?.toJson();
	data['success'] = entity.success;
	return data;
}

yBDGameEntryNotifyBodyFromJson(YBDGameEntryNotifyBody data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameEntryNotifyBodyToJson(YBDGameEntryNotifyBody entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	return data;
}

yBDGameEntryNotifyResourcesFromJson(YBDGameEntryNotifyResources data, Map<String, dynamic> json) {
	if (json['user'] != null) {
		data.user = YBDGameEntryNotifyResourcesUser().fromJson(json['user']);
	}
	return data;
}

Map<String, dynamic> yBDGameEntryNotifyResourcesToJson(YBDGameEntryNotifyResources entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['user'] = entity.user?.toJson();
	return data;
}

yBDGameEntryNotifyResourcesUserFromJson(YBDGameEntryNotifyResourcesUser data, Map<String, dynamic> json) {
	if (json['attribute'] != null) {
		data.attribute = YBDGameUsersAttribute().fromJson(json['attribute']);
	}
	if (json['clientType'] != null) {
		data.clientType = json['clientType'].toString();
	}
	if (json['connectionId'] != null) {
		data.connectionId = json['connectionId'] is String
				? int.tryParse(json['connectionId'])
				: json['connectionId'].toInt();
	}
	if (json['projectCode'] != null) {
		data.projectCode = json['projectCode'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'].toString();
	}
	if (json['sessionId'] != null) {
		data.sessionId = json['sessionId'].toString();
	}
	if (json['tenantCode'] != null) {
		data.tenantCode = json['tenantCode'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameEntryNotifyResourcesUserToJson(YBDGameEntryNotifyResourcesUser entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['attribute'] = entity.attribute?.toJson();
	data['clientType'] = entity.clientType;
	data['connectionId'] = entity.connectionId;
	data['projectCode'] = entity.projectCode;
	data['roomId'] = entity.roomId;
	data['sessionId'] = entity.sessionId;
	data['tenantCode'] = entity.tenantCode;
	data['userId'] = entity.userId;
	return data;
}