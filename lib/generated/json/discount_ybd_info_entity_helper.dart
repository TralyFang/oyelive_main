import 'package:oyelive_main/module/entity/discount_ybd_info_entity.dart';

yBDDiscountInfoEntityFromJson(YBDDiscountInfoEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = json['record'];
	}
	return data;
}

Map<String, dynamic> yBDDiscountInfoEntityToJson(YBDDiscountInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record;
	return data;
}