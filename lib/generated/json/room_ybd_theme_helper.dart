import 'package:oyelive_main/common/room_socket/message/common/room_ybd_theme.dart';

yBDRoomThemeFromJson(YBDRoomTheme data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['lable'] != null) {
		data.lable = json['lable'] is String
				? int.tryParse(json['lable'])
				: json['lable'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['image'] != null) {
		data.image = json['image'].toString();
	}
	if (json['display'] != null) {
		data.display = json['display'] is String
				? int.tryParse(json['display'])
				: json['display'].toInt();
	}
	if (json['attribute'] != null) {
		data.attribute = json['attribute'].toString();
	}
	if (json['personalId'] != null) {
		data.personalId = json['personalId'] is String
				? int.tryParse(json['personalId'])
				: json['personalId'].toInt();
	}
	if (json['expireAfter'] != null) {
		data.expireAfter = json['expireAfter'] is String
				? int.tryParse(json['expireAfter'])
				: json['expireAfter'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDRoomThemeToJson(YBDRoomTheme entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['status'] = entity.status;
	data['lable'] = entity.lable;
	data['index'] = entity.index;
	data['price'] = entity.price;
	data['image'] = entity.image;
	data['display'] = entity.display;
	data['attribute'] = entity.attribute;
	data['personalId'] = entity.personalId;
	data['expireAfter'] = entity.expireAfter;
	return data;
}

yBDRoomThemeContentFromJson(YBDRoomThemeContent data, Map<String, dynamic> json) {
	if (json['theme'] != null) {
		data.theme = YBDRoomTheme().fromJson(json['theme']);
	}
	return data;
}

Map<String, dynamic> yBDRoomThemeContentToJson(YBDRoomThemeContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['theme'] = entity.theme?.toJson();
	return data;
}