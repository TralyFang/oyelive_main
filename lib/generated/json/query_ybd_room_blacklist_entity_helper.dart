import 'package:oyelive_main/module/user/entity/query_ybd_room_blacklist_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

yBDQueryRoomBlacklistRespEntityFromJson(YBDQueryRoomBlacklistRespEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDRoomBlacklist().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDQueryRoomBlacklistRespEntityToJson(YBDQueryRoomBlacklistRespEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDRoomBlacklistFromJson(YBDRoomBlacklist data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['roomid'] != null) {
		data.roomid = json['roomid'] is String
				? int.tryParse(json['roomid'])
				: json['roomid'].toInt();
	}
	if (json['operator'] != null) {
		data.operator = YBDOperator().fromJson(json['operator']);
	}
	if (json['user'] != null) {
		data.user = YBDUserInfo().fromJson(json['user']);
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['createtime'] != null) {
		data.createtime = json['createtime'] is String
				? int.tryParse(json['createtime'])
				: json['createtime'].toInt();
	}
	if (json['validdate'] != null) {
		data.validdate = json['validdate'] is String
				? int.tryParse(json['validdate'])
				: json['validdate'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDRoomBlacklistToJson(YBDRoomBlacklist entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['roomid'] = entity.roomid;
	data['operator'] = entity.operator?.toJson();
	data['user'] = entity.user?.toJson();
	data['type'] = entity.type;
	data['createtime'] = entity.createtime;
	data['validdate'] = entity.validdate;
	return data;
}

yBDOperatorFromJson(YBDOperator data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['agency'] != null) {
		data.agency = json['agency'] is String
				? int.tryParse(json['agency'])
				: json['agency'].toInt();
	}
	if (json['crest'] != null) {
		data.crest = json['crest'] is String
				? int.tryParse(json['crest'])
				: json['crest'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['headimg'] != null) {
		data.headimg = json['headimg'].toString();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['roomlevel'] != null) {
		data.roomlevel = json['roomlevel'] is String
				? int.tryParse(json['roomlevel'])
				: json['roomlevel'].toInt();
	}
	if (json['vip'] != null) {
		data.vip = json['vip'] is String
				? int.tryParse(json['vip'])
				: json['vip'].toInt();
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDOperatorToJson(YBDOperator entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['agency'] = entity.agency;
	data['crest'] = entity.crest;
	data['nickname'] = entity.nickname;
	data['headimg'] = entity.headimg;
	data['level'] = entity.level;
	data['roomlevel'] = entity.roomlevel;
	data['vip'] = entity.vip;
	data['sex'] = entity.sex;
	return data;
}