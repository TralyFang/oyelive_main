import 'package:oyelive_main/module/entity/level_ybd_info_entity.dart';

yBDLevelInfoEntityFromJson(YBDLevelInfoEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDLevelInfoRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDLevelInfoEntityToJson(YBDLevelInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDLevelInfoRecordFromJson(YBDLevelInfoRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['rights'] != null) {
		data.rights = json['rights'] is String
				? int.tryParse(json['rights'])
				: json['rights'].toInt();
	}
	if (json['experience'] != null) {
		data.experience = json['experience'];
	}
	return data;
}

Map<String, dynamic> yBDLevelInfoRecordToJson(YBDLevelInfoRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['level'] = entity.level;
	data['type'] = entity.type;
	data['rights'] = entity.rights;
	data['experience'] = entity.experience;
	return data;
}