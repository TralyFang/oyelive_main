import 'package:oyelive_main/module/status/entity/block_ybd_user_entity.dart';

yBDBlockUserEntityFromJson(YBDBlockUserEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDBlockUserData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDBlockUserEntityToJson(YBDBlockUserEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	return data;
}

yBDBlockUserDataFromJson(YBDBlockUserData data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'];
	}
	if (json['fromId'] != null) {
		data.fromId = json['fromId'] is String
				? int.tryParse(json['fromId'])
				: json['fromId'].toInt();
	}
	if (json['toId'] != null) {
		data.toId = json['toId'] is String
				? int.tryParse(json['toId'])
				: json['toId'].toInt();
	}
	if (json['state'] != null) {
		data.state = json['state'] is String
				? int.tryParse(json['state'])
				: json['state'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'];
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'];
	}
	return data;
}

Map<String, dynamic> yBDBlockUserDataToJson(YBDBlockUserData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['fromId'] = entity.fromId;
	data['toId'] = entity.toId;
	data['state'] = entity.state;
	data['nickname'] = entity.nickname;
	data['avatar'] = entity.avatar;
	return data;
}