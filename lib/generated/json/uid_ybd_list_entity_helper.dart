import 'package:oyelive_main/module/entity/uid_ybd_list_entity.dart';

yBDUidListEntityFromJson(YBDUidListEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDUidListRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDUidListEntityToJson(YBDUidListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDUidListRecordFromJson(YBDUidListRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['number'] != null) {
		data.number = json['number'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['display'] != null) {
		data.display = json['display'] is String
				? int.tryParse(json['display'])
				: json['display'].toInt();
	}
	if (json['createtime'] != null) {
		data.createtime = json['createtime'];
	}
	if (json['extend'] != null) {
		data.extend = json['extend'];
	}
	if (json['stock'] != null) {
		data.stock = json['stock'] is String
				? int.tryParse(json['stock'])
				: json['stock'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['modify'] != null) {
		data.modify = json['modify'];
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['conditionType'] != null) {
		data.conditionType = json['conditionType'] is String
				? int.tryParse(json['conditionType'])
				: json['conditionType'].toInt();
	}
	if (json['conditionExtends'] != null) {
		data.conditionExtends = json['conditionExtends'].toString();
	}
	return data;
}

Map<String, dynamic> yBDUidListRecordToJson(YBDUidListRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['number'] = entity.number;
	data['status'] = entity.status;
	data['price'] = entity.price;
	data['display'] = entity.display;
	data['createtime'] = entity.createtime;
	data['extend'] = entity.extend;
	data['stock'] = entity.stock;
	data['name'] = entity.name;
	data['modify'] = entity.modify;
	data['currency'] = entity.currency;
	data['conditionType'] = entity.conditionType;
	data['conditionExtends'] = entity.conditionExtends;
	return data;
}