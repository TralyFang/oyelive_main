import 'package:oyelive_main/module/inbox/entity/block_ybd_list_entity.dart';

yBDBlockListEntityFromJson(YBDBlockListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDBlockListData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDBlockListEntityToJson(YBDBlockListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDBlockListDataFromJson(YBDBlockListData data, Map<String, dynamic> json) {
	if (json['blockList'] != null) {
		data.blockList = (json['blockList'] as List).map((v) => YBDBlockListDataBlockList().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDBlockListDataToJson(YBDBlockListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['blockList'] =  entity.blockList?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDBlockListDataBlockListFromJson(YBDBlockListDataBlockList data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'] is String
				? int.tryParse(json['createTime'])
				: json['createTime'].toInt();
	}
	if (json['nickName'] != null) {
		data.nickName = json['nickName'].toString();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'].toString();
	}
	return data;
}

Map<String, dynamic> yBDBlockListDataBlockListToJson(YBDBlockListDataBlockList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['createTime'] = entity.createTime;
	data['nickName'] = entity.nickName;
	data['avatar'] = entity.avatar;
	return data;
}