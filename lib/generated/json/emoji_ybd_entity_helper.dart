import 'package:oyelive_main/ui/page/room/entity/emoji_ybd_entity.dart';

yBDEmojiEntityFromJson(YBDEmojiEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['timestamp'] != null) {
		data.timestamp = json['timestamp'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDEmoji().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDEmojiEntityToJson(YBDEmojiEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['timestamp'] = entity.timestamp;
	data['record'] =  entity.record?.map((v) => v.toJson())?.toList();
	return data;
}

yBDEmojiFromJson(YBDEmoji data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['coverImg'] != null) {
		data.coverImg = json['coverImg'].toString();
	}
	if (json['price'] != null) {
		data.price = json['price'].toString();
	}
	if (json['emojiDatas'] != null) {
		data.emojiDatas = (json['emojiDatas'] as List).map((v) => YBDEmojiData().fromJson(v)).toList();
	}
	if (json['useCondition'] != null) {
		data.useCondition = (json['useCondition'] as List).map((v) => YBDUseCondition().fromJson(v)).toList();
	}
	if (json['updateTime'] != null) {
		data.updateTime = json['updateTime'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEmojiToJson(YBDEmoji entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['coverImg'] = entity.coverImg;
	data['price'] = entity.price;
	data['emojiDatas'] =  entity.emojiDatas.map((v) => v.toJson()).toList();
	data['useCondition'] =  entity.useCondition?.map((v) => v.toJson())?.toList();
	data['updateTime'] = entity.updateTime;
	data['status'] = entity.status;
	return data;
}

yBDEmojiDataFromJson(YBDEmojiData data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['pic'] != null) {
		data.pic = json['pic'].toString();
	}
	if (json['svga'] != null) {
		data.svga = json['svga'].toString();
	}
	if (json['index'] != null) {
		data.index = json['index'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEmojiDataToJson(YBDEmojiData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['pic'] = entity.pic;
	data['svga'] = entity.svga;
	data['index'] = entity.index;
	data['name'] = entity.name;
	return data;
}

yBDUseConditionFromJson(YBDUseCondition data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['condition'] != null) {
		data.condition = json['condition'].toString();
	}
	return data;
}

Map<String, dynamic> yBDUseConditionToJson(YBDUseCondition entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['condition'] = entity.condition;
	return data;
}

yBDSelfEmojiFromJson(YBDSelfEmoji data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDSelfEmojiRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDSelfEmojiToJson(YBDSelfEmoji entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDSelfEmojiRecordFromJson(YBDSelfEmojiRecord data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['emojiIds'] != null) {
		data.emojiIds = (json['emojiIds'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	return data;
}

Map<String, dynamic> yBDSelfEmojiRecordToJson(YBDSelfEmojiRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['emojiIds'] = entity.emojiIds;
	return data;
}