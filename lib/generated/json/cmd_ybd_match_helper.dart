import 'package:oyelive_main/common/web_socket/tpg_match/entity/cmd_ybd_match.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/entity/match_ybd_cmd_base.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

yBDMatchCmdFromJson(YBDMatchCmd data, Map<String, dynamic> json) {
	if (json['command'] != null) {
		data.command = json['command'].toString();
	}
	if (json['matchType'] != null) {
		data.matchType = json['matchType'].toString();
	}
	if (json['operationTime'] != null) {
		data.operationTime = json['operationTime'] is String
				? num.tryParse(json['operationTime'])
				: json['operationTime'];
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? num.tryParse(json['version'])
				: json['version'];
	}
	if (json['gameId'] != null) {
		data.gameId = json['gameId'] is String
				? num.tryParse(json['gameId'])
				: json['gameId'];
	}
	if (json['extent'] != null) {
		data.extent = json['extent'].toString();
	}
	if (json['userInfo'] != null) {
		data.userInfo = YBDUserInfo().fromJson(json['userInfo']);
	}
	return data;
}

Map<String, dynamic> yBDMatchCmdToJson(YBDMatchCmd entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['command'] = entity.command;
	data['matchType'] = entity.matchType;
	data['operationTime'] = entity.operationTime;
	data['version'] = entity.version;
	data['gameId'] = entity.gameId;
	data['extent'] = entity.extent;
	data['userInfo'] = entity.userInfo?.toJson();
	return data;
}