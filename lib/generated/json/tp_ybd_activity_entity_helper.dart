import 'package:oyelive_main/ui/page/home/entity/tp_ybd_activity_entity.dart';
import 'dart:async';

yBDTpActivityEntityFromJson(YBDTpActivityEntity data, Map<String, dynamic> json) {
	if (json['url'] != null) {
		data.url = json['url'].toString();
	}
	if (json['zip'] != null) {
		data.zip = json['zip'].toString();
	}
	if (json['showSkin'] != null) {
		data.showSkin = json['showSkin'];
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['primaryColor'] != null) {
		data.primaryColor = json['primaryColor'].toString();
	}
	if (json['dividerColor'] != null) {
		data.dividerColor = json['dividerColor'].toString();
	}
	if (json['backgroundColor'] != null) {
		data.backgroundColor = json['backgroundColor'].toString();
	}
	if (json['lightPrimaryColor'] != null) {
		data.lightPrimaryColor = json['lightPrimaryColor'].toString();
	}
	if (json['lightSecondaryColor'] != null) {
		data.lightSecondaryColor = json['lightSecondaryColor'].toString();
	}
	if (json['bodyText2Color'] != null) {
		data.bodyText2Color = json['bodyText2Color'].toString();
	}
	return data;
}

Map<String, dynamic> yBDTpActivityEntityToJson(YBDTpActivityEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['url'] = entity.url;
	data['zip'] = entity.zip;
	data['showSkin'] = entity.showSkin;
	data['name'] = entity.name;
	data['img'] = entity.img;
	data['primaryColor'] = entity.primaryColor;
	data['dividerColor'] = entity.dividerColor;
	data['backgroundColor'] = entity.backgroundColor;
	data['lightPrimaryColor'] = entity.lightPrimaryColor;
	data['lightSecondaryColor'] = entity.lightSecondaryColor;
	data['bodyText2Color'] = entity.bodyText2Color;
	return data;
}