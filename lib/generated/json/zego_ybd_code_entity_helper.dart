import 'package:oyelive_main/ui/page/game_room/entity/zego_ybd_code_entity.dart';
import 'dart:async';

yBDZegoCodeEntityFromJson(YBDZegoCodeEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDZegoCodeData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDZegoCodeEntityToJson(YBDZegoCodeEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	return data;
}

yBDZegoCodeDataFromJson(YBDZegoCodeData data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['expireDate'] != null) {
		data.expireDate = json['expireDate'] is String
				? int.tryParse(json['expireDate'])
				: json['expireDate'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDZegoCodeDataToJson(YBDZegoCodeData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['expireDate'] = entity.expireDate;
	return data;
}

yBDZegoMGAPPKeyEntityFromJson(YBDZegoMGAPPKeyEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDZegoMGAPPKeyData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDZegoMGAPPKeyEntityToJson(YBDZegoMGAPPKeyEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	return data;
}

yBDZegoMGAPPKeyDataFromJson(YBDZegoMGAPPKeyData data, Map<String, dynamic> json) {
	if (json['app_key'] != null) {
		data.appKey = json['app_key'].toString();
	}
	if (json['app_id'] != null) {
		data.appId = json['app_id'].toString();
	}
	return data;
}

Map<String, dynamic> yBDZegoMGAPPKeyDataToJson(YBDZegoMGAPPKeyData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['app_key'] = entity.appKey;
	data['app_id'] = entity.appId;
	return data;
}