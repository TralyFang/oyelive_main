import 'package:oyelive_main/module/room/entity/my_ybd_gift_entity.dart';
import 'package:oyelive_main/module/room/entity/gift_ybd_package_entity.dart';

yBDMyGiftEntityFromJson(YBDMyGiftEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDMyGiftRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDMyGiftEntityToJson(YBDMyGiftEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDMyGiftRecordFromJson(YBDMyGiftRecord data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['itemList'] != null) {
		data.itemList = (json['itemList'] as List).map((v) => YBDGiftPackageRecordGift().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDMyGiftRecordToJson(YBDMyGiftRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['itemList'] =  entity.itemList?.map((v) => v?.toJson())?.toList();
	return data;
}