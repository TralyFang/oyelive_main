import 'package:oyelive_main/module/status/entity/random_ybd_lyric_entity.dart';
import 'package:json_annotation/json_annotation.dart';

yBDRandomLyricEntityFromJson(YBDRandomLyricEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDRandomLyricData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDRandomLyricEntityToJson(YBDRandomLyricEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDRandomLyricDataFromJson(YBDRandomLyricData data, Map<String, dynamic> json) {
	if (json['lyric'] != null) {
		data.lyric = YBDRandomLyricDataLyric().fromJson(json['lyric']);
	}
	return data;
}

Map<String, dynamic> yBDRandomLyricDataToJson(YBDRandomLyricData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['lyric'] = entity.lyric?.toJson();
	return data;
}

yBDRandomLyricDataLyricFromJson(YBDRandomLyricDataLyric data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['author'] != null) {
		data.author = json['author'].toString();
	}
	if (json['no'] != null) {
		data.no = json['no'] is String
				? int.tryParse(json['no'])
				: json['no'].toInt();
	}
	if (json['top'] != null) {
		data.top = json['top'] is String
				? int.tryParse(json['top'])
				: json['top'].toInt();
	}
	if (json['text'] != null) {
		data.text = json['text'].toString();
	}
	if (json['mark'] != null) {
		data.mark = json['mark'];
	}
	if (json['activityH5'] != null) {
		data.activityH5 = json['activityH5'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRandomLyricDataLyricToJson(YBDRandomLyricDataLyric entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['author'] = entity.author;
	data['no'] = entity.no;
	data['top'] = entity.top;
	data['text'] = entity.text;
	data['mark'] = entity.mark;
	data['activityH5'] = entity.activityH5;
	return data;
}