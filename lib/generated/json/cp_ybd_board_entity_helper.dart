import 'package:oyelive_main/ui/page/home/entity/cp_ybd_board_entity.dart';
import 'dart:async';

yBDCpBoardEntityFromJson(YBDCpBoardEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDCpBoardRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDCpBoardEntityToJson(YBDCpBoardEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDCpBoardRecordFromJson(YBDCpBoardRecord data, Map<String, dynamic> json) {
	if (json['months'] != null) {
		data.months = (json['months'] as List).map((v) => YBDCpBoardRecordItem().fromJson(v)).toList();
	}
	if (json['weeks'] != null) {
		data.weeks = (json['weeks'] as List).map((v) => YBDCpBoardRecordItem().fromJson(v)).toList();
	}
	if (json['days'] != null) {
		data.days = (json['days'] as List).map((v) => YBDCpBoardRecordItem().fromJson(v)).toList();
	}
	if (json['threeYears'] != null) {
		data.threeYears = (json['threeYears'] as List).map((v) => YBDCpBoardRecordItem().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDCpBoardRecordToJson(YBDCpBoardRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['months'] =  entity.months?.map((v) => v?.toJson())?.toList();
	data['weeks'] =  entity.weeks?.map((v) => v?.toJson())?.toList();
	data['days'] =  entity.days?.map((v) => v?.toJson())?.toList();
	data['threeYears'] =  entity.threeYears?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDCpBoardRecordItemFromJson(YBDCpBoardRecordItem data, Map<String, dynamic> json) {
	if (json['talentId'] != null) {
		data.talentId = json['talentId'] is String
				? int.tryParse(json['talentId'])
				: json['talentId'].toInt();
	}
	if (json['talentName'] != null) {
		data.talentName = json['talentName'].toString();
	}
	if (json['talentHead'] != null) {
		data.talentHead = json['talentHead'].toString();
	}
	if (json['talentGender'] != null) {
		data.talentGender = json['talentGender'] is String
				? int.tryParse(json['talentGender'])
				: json['talentGender'].toInt();
	}
	if (json['beans'] != null) {
		data.beans = json['beans'] is String
				? int.tryParse(json['beans'])
				: json['beans'].toInt();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['userName'] != null) {
		data.userName = json['userName'].toString();
	}
	if (json['userHead'] != null) {
		data.userHead = json['userHead'].toString();
	}
	if (json['userGender'] != null) {
		data.userGender = json['userGender'] is String
				? int.tryParse(json['userGender'])
				: json['userGender'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDCpBoardRecordItemToJson(YBDCpBoardRecordItem entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['talentId'] = entity.talentId;
	data['talentName'] = entity.talentName;
	data['talentHead'] = entity.talentHead;
	data['talentGender'] = entity.talentGender;
	data['beans'] = entity.beans;
	data['userId'] = entity.userId;
	data['userName'] = entity.userName;
	data['userHead'] = entity.userHead;
	data['userGender'] = entity.userGender;
	return data;
}