import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_config_display_entity.dart';
import 'dart:async';

yBDGameConfigDisplayEntityFromJson(YBDGameConfigDisplayEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDGameConfigDisplayData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameConfigDisplayEntityToJson(YBDGameConfigDisplayEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	return data;
}

yBDGameConfigDisplayDataFromJson(YBDGameConfigDisplayData data, Map<String, dynamic> json) {
	if (json['logo'] != null) {
		data.logo = json['logo'].toString();
	}
	if (json['rule'] != null) {
		data.rule = YBDGameConfigDisplayDataRule().fromJson(json['rule']);
	}
	if (json['setUp'] != null) {
		data.setUp = YBDGameConfigDisplayDataSetUp().fromJson(json['setUp']);
	}
	return data;
}

Map<String, dynamic> yBDGameConfigDisplayDataToJson(YBDGameConfigDisplayData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['logo'] = entity.logo;
	data['rule'] = entity.rule?.toJson();
	data['setUp'] = entity.setUp?.toJson();
	return data;
}

yBDGameConfigDisplayDataRuleFromJson(YBDGameConfigDisplayDataRule data, Map<String, dynamic> json) {
	if (json['body'] != null) {
		data.body = json['body'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameConfigDisplayDataRuleToJson(YBDGameConfigDisplayDataRule entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['body'] = entity.body;
	data['title'] = entity.title;
	return data;
}

yBDGameConfigDisplayDataSetUpFromJson(YBDGameConfigDisplayDataSetUp data, Map<String, dynamic> json) {
	if (json['addTicketImg'] != null) {
		data.addTicketImg = json['addTicketImg'].toString();
	}
	if (json['bodyBgc'] != null) {
		data.bodyBgc = json['bodyBgc'].toString();
	}
	if (json['buttonBgC'] != null) {
		data.buttonBgC = json['buttonBgC'].toString();
	}
	if (json['buttonInnerC'] != null) {
		data.buttonInnerC = json['buttonInnerC'].toString();
	}
	if (json['selectedImg'] != null) {
		data.selectedImg = json['selectedImg'].toString();
	}
	if (json['titleBgC'] != null) {
		data.titleBgC = json['titleBgC'].toString();
	}
	if (json['selectedC'] != null) {
		data.selectedC = json['selectedC'].toString();
	}
	if (json['reduceTicketImg'] != null) {
		data.reduceTicketImg = json['reduceTicketImg'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameConfigDisplayDataSetUpToJson(YBDGameConfigDisplayDataSetUp entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['addTicketImg'] = entity.addTicketImg;
	data['bodyBgc'] = entity.bodyBgc;
	data['buttonBgC'] = entity.buttonBgC;
	data['buttonInnerC'] = entity.buttonInnerC;
	data['selectedImg'] = entity.selectedImg;
	data['titleBgC'] = entity.titleBgC;
	data['selectedC'] = entity.selectedC;
	data['reduceTicketImg'] = entity.reduceTicketImg;
	return data;
}