import 'package:oyelive_main/module/status/entity/status_ybd_detail_entity.dart';
import 'package:oyelive_main/module/status/entity/status_ybd_entity.dart';

yBDStatusDetailEntityFromJson(YBDStatusDetailEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDStatusInfo().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDStatusDetailEntityToJson(YBDStatusDetailEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}