import 'package:oyelive_main/module/entity/tp_ybd_save_entity.dart';

yBDTpSaveEntityFromJson(YBDTpSaveEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDTpSaveRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDTpSaveEntityToJson(YBDTpSaveEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDTpSaveRecordFromJson(YBDTpSaveRecord data, Map<String, dynamic> json) {
	if (json['users'] != null) {
		data.users = json['users'];
	}
	if (json['broadcastType'] != null) {
		data.broadcastType = json['broadcastType'];
	}
	if (json['title'] != null) {
		data.title = json['title'];
	}
	if (json['subTitle'] != null) {
		data.subTitle = json['subTitle'];
	}
	if (json['content'] != null) {
		data.content = json['content'].toString();
	}
	if (json['buttons'] != null) {
		data.buttons = (json['buttons'] as List).map((v) => YBDTpSaveRecordButtons().fromJson(v)).toList();
	}
	if (json['images'] != null) {
		data.images = json['images'];
	}
	if (json['layout'] != null) {
		data.layout = json['layout'].toString();
	}
	if (json['attrs'] != null) {
		data.attrs = YBDTpSaveRecordAttrs().fromJson(json['attrs']);
	}
	return data;
}

Map<String, dynamic> yBDTpSaveRecordToJson(YBDTpSaveRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['users'] = entity.users;
	data['broadcastType'] = entity.broadcastType;
	data['title'] = entity.title;
	data['subTitle'] = entity.subTitle;
	data['content'] = entity.content;
	data['buttons'] =  entity.buttons?.map((v) => v?.toJson())?.toList();
	data['images'] = entity.images;
	data['layout'] = entity.layout;
	data['attrs'] = entity.attrs?.toJson();
	return data;
}

yBDTpSaveRecordButtonsFromJson(YBDTpSaveRecordButtons data, Map<String, dynamic> json) {
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['type'] != null) {
		data.type = json['type'];
	}
	if (json['action'] != null) {
		data.action = json['action'];
	}
	if (json['url'] != null) {
		data.url = json['url'];
	}
	return data;
}

Map<String, dynamic> yBDTpSaveRecordButtonsToJson(YBDTpSaveRecordButtons entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['name'] = entity.name;
	data['type'] = entity.type;
	data['action'] = entity.action;
	data['url'] = entity.url;
	return data;
}

yBDTpSaveRecordAttrsFromJson(YBDTpSaveRecordAttrs data, Map<String, dynamic> json) {
	if (json['beans'] != null) {
		data.beans = YBDTpSaveRecordAttrsBeans().fromJson(json['beans']);
	}
	if (json['name'] != null) {
		data.name = YBDTpSaveRecordAttrsName().fromJson(json['name']);
	}
	return data;
}

Map<String, dynamic> yBDTpSaveRecordAttrsToJson(YBDTpSaveRecordAttrs entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['beans'] = entity.beans?.toJson();
	data['name'] = entity.name?.toJson();
	return data;
}

yBDTpSaveRecordAttrsBeansFromJson(YBDTpSaveRecordAttrsBeans data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value'].toString();
	}
	if (json['key'] != null) {
		data.key = json['key'].toString();
	}
	return data;
}

Map<String, dynamic> yBDTpSaveRecordAttrsBeansToJson(YBDTpSaveRecordAttrsBeans entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['key'] = entity.key;
	return data;
}

yBDTpSaveRecordAttrsNameFromJson(YBDTpSaveRecordAttrsName data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value'].toString();
	}
	if (json['key'] != null) {
		data.key = json['key'].toString();
	}
	return data;
}

Map<String, dynamic> yBDTpSaveRecordAttrsNameToJson(YBDTpSaveRecordAttrsName entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['key'] = entity.key;
	return data;
}