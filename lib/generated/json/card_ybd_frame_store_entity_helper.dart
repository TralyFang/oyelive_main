import 'package:oyelive_main/module/entity/card_ybd_frame_store_entity.dart';

yBDCardFrameStoreEntityFromJson(YBDCardFrameStoreEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDCardFrameStoreRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDCardFrameStoreEntityToJson(YBDCardFrameStoreEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDCardFrameStoreRecordFromJson(YBDCardFrameStoreRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['ids'] != null) {
		data.ids = json['ids'];
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['label'] != null) {
		data.label = json['label'] is String
				? int.tryParse(json['label'])
				: json['label'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['image'] != null) {
		data.image = json['image'].toString();
	}
	if (json['animation'] != null) {
		data.animation = json['animation'].toString();
	}
	if (json['display'] != null) {
		data.display = json['display'] is String
				? int.tryParse(json['display'])
				: json['display'].toInt();
	}
	if (json['attribute'] != null) {
		data.attribute = json['attribute'].toString();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'] is String
				? int.tryParse(json['createTime'])
				: json['createTime'].toInt();
	}
	if (json['updateTime'] != null) {
		data.updateTime = json['updateTime'] is String
				? int.tryParse(json['updateTime'])
				: json['updateTime'].toInt();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['conditionType'] != null) {
		data.conditionType = json['conditionType'];
	}
	if (json['conditionExtends'] != null) {
		data.conditionExtends = json['conditionExtends'];
	}
	if (json['modify'] != null) {
		data.modify = json['modify'];
	}
	if (json['thumbnail'] != null) {
		data.thumbnail = json['thumbnail'].toString();
	}
	if (json['equipped'] != null) {
		data.equipped = json['equipped'];
	}
	if (json['expireAfter'] != null) {
		data.expireAfter = json['expireAfter'] is String
				? int.tryParse(json['expireAfter'])
				: json['expireAfter'].toInt();
	}
	if (json['personalId'] != null) {
		data.personalId = json['personalId'] is String
				? int.tryParse(json['personalId'])
				: json['personalId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDCardFrameStoreRecordToJson(YBDCardFrameStoreRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['ids'] = entity.ids;
	data['name'] = entity.name;
	data['status'] = entity.status;
	data['label'] = entity.label;
	data['index'] = entity.index;
	data['price'] = entity.price;
	data['image'] = entity.image;
	data['animation'] = entity.animation;
	data['display'] = entity.display;
	data['attribute'] = entity.attribute;
	data['createTime'] = entity.createTime;
	data['updateTime'] = entity.updateTime;
	data['currency'] = entity.currency;
	data['conditionType'] = entity.conditionType;
	data['conditionExtends'] = entity.conditionExtends;
	data['modify'] = entity.modify;
	data['thumbnail'] = entity.thumbnail;
	data['equipped'] = entity.equipped;
	data['expireAfter'] = entity.expireAfter;
	data['personalId'] = entity.personalId;
	return data;
}