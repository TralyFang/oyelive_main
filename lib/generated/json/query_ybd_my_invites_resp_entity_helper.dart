import 'package:oyelive_main/module/user/entity/query_ybd_my_invites_resp_entity.dart';

yBDQueryMyInvitesRespEntityFromJson(YBDQueryMyInvitesRespEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDInviteeInfo().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDQueryMyInvitesRespEntityToJson(YBDQueryMyInvitesRespEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDInviteeInfoFromJson(YBDInviteeInfo data, Map<String, dynamic> json) {
	if (json['inviter'] != null) {
		data.inviter = json['inviter'] is String
				? int.tryParse(json['inviter'])
				: json['inviter'].toInt();
	}
	if (json['invitee'] != null) {
		data.invitee = json['invitee'] is String
				? int.tryParse(json['invitee'])
				: json['invitee'].toInt();
	}
	if (json['sharePlatform'] != null) {
		data.sharePlatform = json['sharePlatform'].toString();
	}
	if (json['inviteeAvatar'] != null) {
		data.inviteeAvatar = json['inviteeAvatar'].toString();
	}
	if (json['inviteeNickname'] != null) {
		data.inviteeNickname = json['inviteeNickname'].toString();
	}
	if (json['inviteeLevel'] != null) {
		data.inviteeLevel = json['inviteeLevel'] is String
				? int.tryParse(json['inviteeLevel'])
				: json['inviteeLevel'].toInt();
	}
	if (json['inviteeRegisterTime'] != null) {
		data.inviteeRegisterTime = json['inviteeRegisterTime'] is String
				? int.tryParse(json['inviteeRegisterTime'])
				: json['inviteeRegisterTime'].toInt();
	}
	if (json['inviteeSex'] != null) {
		data.inviteeSex = json['inviteeSex'] is String
				? int.tryParse(json['inviteeSex'])
				: json['inviteeSex'].toInt();
	}
	if (json['follow'] != null) {
		data.follow = json['follow'];
	}
	return data;
}

Map<String, dynamic> yBDInviteeInfoToJson(YBDInviteeInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['inviter'] = entity.inviter;
	data['invitee'] = entity.invitee;
	data['sharePlatform'] = entity.sharePlatform;
	data['inviteeAvatar'] = entity.inviteeAvatar;
	data['inviteeNickname'] = entity.inviteeNickname;
	data['inviteeLevel'] = entity.inviteeLevel;
	data['inviteeRegisterTime'] = entity.inviteeRegisterTime;
	data['inviteeSex'] = entity.inviteeSex;
	data['follow'] = entity.follow;
	return data;
}