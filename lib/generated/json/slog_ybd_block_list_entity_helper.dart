import 'package:oyelive_main/module/status/entity/slog_ybd_block_list_entity.dart';

yBDSlogBlockListEntityFromJson(YBDSlogBlockListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDSlogBlockListData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDSlogBlockListEntityToJson(YBDSlogBlockListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	return data;
}

yBDSlogBlockListDataFromJson(YBDSlogBlockListData data, Map<String, dynamic> json) {
	if (json['rows'] != null) {
		data.rows = (json['rows'] as List).map((v) => YBDSlogBlockListDataRows().fromJson(v)).toList();
	}
	if (json['total'] != null) {
		data.total = json['total'] is String
				? int.tryParse(json['total'])
				: json['total'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDSlogBlockListDataToJson(YBDSlogBlockListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rows'] =  entity.rows?.map((v) => v?.toJson())?.toList();
	data['total'] = entity.total;
	return data;
}

yBDSlogBlockListDataRowsFromJson(YBDSlogBlockListDataRows data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['fromId'] != null) {
		data.fromId = json['fromId'] is String
				? int.tryParse(json['fromId'])
				: json['fromId'].toInt();
	}
	if (json['toId'] != null) {
		data.toId = json['toId'] is String
				? int.tryParse(json['toId'])
				: json['toId'].toInt();
	}
	if (json['state'] != null) {
		data.state = json['state'] is String
				? int.tryParse(json['state'])
				: json['state'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'].toString();
	}
	return data;
}

Map<String, dynamic> yBDSlogBlockListDataRowsToJson(YBDSlogBlockListDataRows entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['fromId'] = entity.fromId;
	data['toId'] = entity.toId;
	data['state'] = entity.state;
	data['nickname'] = entity.nickname;
	data['avatar'] = entity.avatar;
	return data;
}