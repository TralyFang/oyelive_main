import 'package:oyelive_main/ui/page/store/entity/discount_ybd_entity.dart';
import 'dart:async';

yBDDiscountEntityFromJson(YBDDiscountEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDDiscountRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDDiscountEntityToJson(YBDDiscountEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDDiscountRecordFromJson(YBDDiscountRecord data, Map<String, dynamic> json) {
	if (json['unitType'] != null) {
		data.unitType = json['unitType'] is String
				? int.tryParse(json['unitType'])
				: json['unitType'].toInt();
	}
	if (json['priceInfo'] != null) {
		data.priceInfo = json['priceInfo'];
	}
	return data;
}

Map<String, dynamic> yBDDiscountRecordToJson(YBDDiscountRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['unitType'] = entity.unitType;
	data['priceInfo'] = entity.priceInfo;
	return data;
}

yBDDiscountRecordPriceInfo_1FromJson(YBDDiscountRecordPriceInfo_1 data, Map<String, dynamic> json) {
	if (json['unit'] != null) {
		data.unit = json['unit'].toString();
	}
	if (json['discount'] != null) {
		data.discount = json['discount'].toString();
	}
	return data;
}

Map<String, dynamic> yBDDiscountRecordPriceInfo_1ToJson(YBDDiscountRecordPriceInfo_1 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['unit'] = entity.unit;
	data['discount'] = entity.discount;
	return data;
}