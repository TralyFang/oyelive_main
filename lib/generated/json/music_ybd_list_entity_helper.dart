import 'package:oyelive_main/module/status/entity/music_ybd_list_entity.dart';

yBDMusicListEntityFromJson(YBDMusicListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDMusicListData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDMusicListEntityToJson(YBDMusicListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDMusicListDataFromJson(YBDMusicListData data, Map<String, dynamic> json) {
	if (json['rows'] != null) {
		data.rows = (json['rows'] as List).map((v) => YBDMusicListDataRow().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDMusicListDataToJson(YBDMusicListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rows'] =  entity.rows?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDMusicListDataRowFromJson(YBDMusicListDataRow data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'].toString();
	}
	if (json['creatorId'] != null) {
		data.creatorId = json['creatorId'].toString();
	}
	if (json['updateTime'] != null) {
		data.updateTime = json['updateTime'].toString();
	}
	if (json['updatorId'] != null) {
		data.updatorId = json['updatorId'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['deleted'] != null) {
		data.deleted = json['deleted'] is String
				? int.tryParse(json['deleted'])
				: json['deleted'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['author'] != null) {
		data.author = json['author'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['addTime'] != null) {
		data.addTime = json['addTime'].toString();
	}
	if (json['resource'] != null) {
		data.resource = json['resource'].toString();
	}
	if (json['count'] != null) {
		data.count = json['count'] is String
				? int.tryParse(json['count'])
				: json['count'].toInt();
	}
	if (json['duration'] != null) {
		data.duration = json['duration'] is String
				? int.tryParse(json['duration'])
				: json['duration'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDMusicListDataRowToJson(YBDMusicListDataRow entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['version'] = entity.version;
	data['createTime'] = entity.createTime;
	data['creatorId'] = entity.creatorId;
	data['updateTime'] = entity.updateTime;
	data['updatorId'] = entity.updatorId;
	data['status'] = entity.status;
	data['deleted'] = entity.deleted;
	data['name'] = entity.name;
	data['author'] = entity.author;
	data['userId'] = entity.userId;
	data['addTime'] = entity.addTime;
	data['resource'] = entity.resource;
	data['count'] = entity.count;
	data['duration'] = entity.duration;
	return data;
}