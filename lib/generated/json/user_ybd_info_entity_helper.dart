import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/login/handler/login_ybd_handler.dart';
import 'package:oyelive_main/ui/page/profile/setting/setting_ybd_item.dart';

yBDUserInfoFromJson(YBDUserInfo data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['account'] != null) {
		data.account = json['account'].toString();
	}
	if (json['agency'] != null) {
		data.agency = json['agency'] is String
				? int.tryParse(json['agency'])
				: json['agency'].toInt();
	}
	if (json['email'] != null) {
		data.email = json['email'].toString();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['password'] != null) {
		data.password = json['password'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['headimg'] != null) {
		data.headimg = json['headimg'].toString();
	}
	if (json['money'] != null) {
		data.money = json['money'] is String
				? int.tryParse(json['money'])
				: json['money'].toInt();
	}
	if (json['gems'] != null) {
		data.gems = json['gems'] is String
				? int.tryParse(json['gems'])
				: json['gems'].toInt();
	}
	if (json['golds'] != null) {
		data.golds = json['golds'] is String
				? int.tryParse(json['golds'])
				: json['golds'].toInt();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['experience'] != null) {
		data.experience = json['experience'] is String
				? int.tryParse(json['experience'])
				: json['experience'].toInt();
	}
	if (json['roomlevel'] != null) {
		data.roomlevel = json['roomlevel'] is String
				? int.tryParse(json['roomlevel'])
				: json['roomlevel'].toInt();
	}
	if (json['roomexper'] != null) {
		data.roomexper = json['roomexper'] is String
				? int.tryParse(json['roomexper'])
				: json['roomexper'].toInt();
	}
	if (json['vip'] != null) {
		data.vip = json['vip'] is String
				? int.tryParse(json['vip'])
				: json['vip'].toInt();
	}
	if (json['privateword'] != null) {
		data.privateword = json['privateword'].toString();
	}
	if (json['notice'] != null) {
		data.notice = json['notice'].toString();
	}
	if (json['roomimg'] != null) {
		data.roomimg = json['roomimg'].toString();
	}
	if (json['concern'] != null) {
		data.concern = json['concern'] is String
				? int.tryParse(json['concern'])
				: json['concern'].toInt();
	}
	if (json['fans'] != null) {
		data.fans = json['fans'] is String
				? int.tryParse(json['fans'])
				: json['fans'].toInt();
	}
	if (json['friends'] != null) {
		data.friends = json['friends'] is String
				? int.tryParse(json['friends'])
				: json['friends'].toInt();
	}
	if (json['friend'] != null) {
		data.friend = json['friend'];
	}
	if (json['birthday'] != null) {
		data.birthday = json['birthday'] is String
				? int.tryParse(json['birthday'])
				: json['birthday'].toInt();
	}
	if (json['vipDailyCheckRewardBeans'] != null) {
		data.vipDailyCheckRewardBeans = json['vipDailyCheckRewardBeans'] is String
				? int.tryParse(json['vipDailyCheckRewardBeans'])
				: json['vipDailyCheckRewardBeans'].toInt();
	}
	if (json['vipIcon'] != null) {
		data.vipIcon = json['vipIcon'].toString();
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	if (json['comment'] != null) {
		data.comment = json['comment'].toString();
	}
	if (json['classify'] != null) {
		data.classify = json['classify'] is String
				? int.tryParse(json['classify'])
				: json['classify'].toInt();
	}
	if (json['loginType'] != null) {
		data.loginType = json['loginType'] is String
				? int.tryParse(json['loginType'])
				: json['loginType'].toInt();
	}
	if (json['tenant'] != null) {
		data.tenant = json['tenant'].toString();
	}
	if (json['country'] != null) {
		data.country = json['country'].toString();
	}
	if (json['spLocal'] != null) {
		data.spLocal = json['spLocal'].toString();
	}
	if (json['portalType'] != null) {
		data.portalType = json['portalType'].toString();
	}
	if (json['tags'] != null) {
		data.tags = (json['tags'] as List).map((v) => YBDUserTag().fromJson(v)).toList();
	}
	if (json['snsInfos'] != null) {
		data.snsInfos = (json['snsInfos'] as List).map((v) => YBDSnsInfo().fromJson(v)).toList();
	}
	if (json['count'] != null) {
		data.count = json['count'] is String
				? int.tryParse(json['count'])
				: json['count'].toInt();
	}
	if (json['live'] != null) {
		data.live = json['live'];
	}
	if (json['cellphone'] != null) {
		data.cellphone = json['cellphone'].toString();
	}
	if (json['uniqueNum'] != null) {
		data.uniqueNum = json['uniqueNum'].toString();
	}
	if (json['extendVo'] != null) {
		data.extendVo = YBDExtendVo().fromJson(json['extendVo']);
	}
	if (json['badges'] != null) {
		data.badges = (json['badges'] as List).map((v) => YBDBadgeInfo().fromJson(v)).toList();
	}
	if (json['backgrounds'] != null) {
		data.backgrounds = YBDBackgroundInfo().fromJson(json['backgrounds']);
	}
	if (json['equipGoods'] != null) {
		data.equipGoods = YBDEquipGoods().fromJson(json['equipGoods']);
	}
	if (json['headFrame'] != null) {
		data.headFrame = json['headFrame'].toString();
	}
	return data;
}

Map<String, dynamic> yBDUserInfoToJson(YBDUserInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['account'] = entity.account;
	data['agency'] = entity.agency;
	data['email'] = entity.email;
	data['nickname'] = entity.nickname;
	data['password'] = entity.password;
	data['status'] = entity.status;
	data['headimg'] = entity.headimg;
	data['money'] = entity.money;
	data['gems'] = entity.gems;
	data['golds'] = entity.golds;
	data['level'] = entity.level;
	data['experience'] = entity.experience;
	data['roomlevel'] = entity.roomlevel;
	data['roomexper'] = entity.roomexper;
	data['vip'] = entity.vip;
	data['privateword'] = entity.privateword;
	data['notice'] = entity.notice;
	data['roomimg'] = entity.roomimg;
	data['concern'] = entity.concern;
	data['fans'] = entity.fans;
	data['friends'] = entity.friends;
	data['friend'] = entity.friend;
	data['birthday'] = entity.birthday;
	data['vipDailyCheckRewardBeans'] = entity.vipDailyCheckRewardBeans;
	data['vipIcon'] = entity.vipIcon;
	data['sex'] = entity.sex;
	data['comment'] = entity.comment;
	data['classify'] = entity.classify;
	data['loginType'] = entity.loginType;
	data['tenant'] = entity.tenant;
	data['country'] = entity.country;
	data['spLocal'] = entity.spLocal;
	data['portalType'] = entity.portalType;
	data['tags'] =  entity.tags?.map((v) => v?.toJson())?.toList();
	data['snsInfos'] =  entity.snsInfos?.map((v) => v?.toJson())?.toList();
	data['count'] = entity.count;
	data['live'] = entity.live;
	data['cellphone'] = entity.cellphone;
	data['uniqueNum'] = entity.uniqueNum;
	data['extendVo'] = entity.extendVo?.toJson();
	data['badges'] =  entity.badges?.map((v) => v?.toJson())?.toList();
	data['backgrounds'] = entity.backgrounds?.toJson();
	data['equipGoods'] = entity.equipGoods?.toJson();
	data['headFrame'] = entity.headFrame;
	return data;
}

yBDEquipGoodsFromJson(YBDEquipGoods data, Map<String, dynamic> json) {
	if (json['CARD_FRAME'] != null) {
		data.cardFrame = YBDCardFrame().fromJson(json['CARD_FRAME']);
	}
	return data;
}

Map<String, dynamic> yBDEquipGoodsToJson(YBDEquipGoods entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['CARD_FRAME'] = entity.cardFrame?.toJson();
	return data;
}

yBDCardFrameFromJson(YBDCardFrame data, Map<String, dynamic> json) {
	if (json['image'] != null) {
		data.image = json['image'].toString();
	}
	if (json['animation'] != null) {
		data.animation = json['animation'].toString();
	}
	return data;
}

Map<String, dynamic> yBDCardFrameToJson(YBDCardFrame entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['image'] = entity.image;
	data['animation'] = entity.animation;
	return data;
}

yBDUserTagFromJson(YBDUserTag data, Map<String, dynamic> json) {
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['value'] != null) {
		data.value = json['value'].toString();
	}
	if (json['expireAt'] != null) {
		data.expireAt = json['expireAt'].toString();
	}
	return data;
}

Map<String, dynamic> yBDUserTagToJson(YBDUserTag entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['name'] = entity.name;
	data['value'] = entity.value;
	data['expireAt'] = entity.expireAt;
	return data;
}

yBDSnsInfoFromJson(YBDSnsInfo data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['imei'] != null) {
		data.imei = json['imei'];
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'] is String
				? int.tryParse(json['createTime'])
				: json['createTime'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['snsId'] != null) {
		data.snsId = json['snsId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDSnsInfoToJson(YBDSnsInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['userId'] = entity.userId;
	data['type'] = entity.type;
	data['imei'] = entity.imei;
	data['createTime'] = entity.createTime;
	data['nickname'] = entity.nickname;
	data['snsId'] = entity.snsId;
	return data;
}

yBDBadgeInfoFromJson(YBDBadgeInfo data, Map<String, dynamic> json) {
	if (json['badgeCode'] != null) {
		data.badgeCode = json['badgeCode'].toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['iconPng'] != null) {
		data.iconPng = json['iconPng'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDBadgeInfoToJson(YBDBadgeInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['badgeCode'] = entity.badgeCode;
	data['icon'] = entity.icon;
	data['iconPng'] = entity.iconPng;
	data['userId'] = entity.userId;
	return data;
}

yBDBackgroundInfoFromJson(YBDBackgroundInfo data, Map<String, dynamic> json) {
	if (json['host'] != null) {
		data.host = json['host'].toString();
	}
	if (json['resources'] != null) {
		data.resources = (json['resources'] as List).map((v) => YBDBgInfoRes().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDBackgroundInfoToJson(YBDBackgroundInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['host'] = entity.host;
	data['resources'] =  entity.resources?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDBgInfoResFromJson(YBDBgInfoRes data, Map<String, dynamic> json) {
	if (json['resource'] != null) {
		data.resource = json['resource'].toString();
	}
	return data;
}

Map<String, dynamic> yBDBgInfoResToJson(YBDBgInfoRes entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['resource'] = entity.resource;
	return data;
}