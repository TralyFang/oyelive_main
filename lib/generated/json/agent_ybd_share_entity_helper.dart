import 'package:oyelive_main/module/status/entity/agent_ybd_share_entity.dart';

yBDAgentShareEntityFromJson(YBDAgentShareEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDAgentShareData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDAgentShareEntityToJson(YBDAgentShareEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	return data;
}

yBDAgentShareDataFromJson(YBDAgentShareData data, Map<String, dynamic> json) {
	if (json['layeredBeans'] != null) {
		data.layeredBeans = json['layeredBeans'] is String
				? int.tryParse(json['layeredBeans'])
				: json['layeredBeans'].toInt();
	}
	if (json['startTime'] != null) {
		data.startTime = json['startTime'] is String
				? int.tryParse(json['startTime'])
				: json['startTime'].toInt();
	}
	if (json['totalConsumption'] != null) {
		data.totalConsumption = json['totalConsumption'] is String
				? int.tryParse(json['totalConsumption'])
				: json['totalConsumption'].toInt();
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'] is String
				? int.tryParse(json['endTime'])
				: json['endTime'].toInt();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDAgentShareDataToJson(YBDAgentShareData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['layeredBeans'] = entity.layeredBeans;
	data['startTime'] = entity.startTime;
	data['totalConsumption'] = entity.totalConsumption;
	data['endTime'] = entity.endTime;
	data['userId'] = entity.userId;
	return data;
}