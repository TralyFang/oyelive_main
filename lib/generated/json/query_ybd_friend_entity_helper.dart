import 'package:oyelive_main/ui/page/follow/entity/query_ybd_friend_entity.dart';
import 'dart:async';

yBDQueryFriendEntityFromJson(YBDQueryFriendEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDQueryFriendRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDQueryFriendEntityToJson(YBDQueryFriendEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDQueryFriendRecordFromJson(YBDQueryFriendRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['fromid'] != null) {
		data.fromid = json['fromid'] is String
				? int.tryParse(json['fromid'])
				: json['fromid'].toInt();
	}
	if (json['toid'] != null) {
		data.toid = json['toid'] is String
				? int.tryParse(json['toid'])
				: json['toid'].toInt();
	}
	if (json['createtime'] != null) {
		data.createtime = json['createtime'] is String
				? int.tryParse(json['createtime'])
				: json['createtime'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'];
	}
	if (json['notice'] != null) {
		data.notice = json['notice'];
	}
	return data;
}

Map<String, dynamic> yBDQueryFriendRecordToJson(YBDQueryFriendRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['fromid'] = entity.fromid;
	data['toid'] = entity.toid;
	data['createtime'] = entity.createtime;
	data['type'] = entity.type;
	data['notice'] = entity.notice;
	return data;
}