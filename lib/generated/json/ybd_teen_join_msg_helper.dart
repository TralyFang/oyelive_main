import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_join_msg.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_result.dart';

yBDTeenJoinMsgFromJson(YBDTeenJoinMsg data, Map<String, dynamic> json) {
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['coins'] != null) {
		data.coins = json['coins'] is String
				? int.tryParse(json['coins'])
				: json['coins'].toInt();
	}
	if (json['gameId'] != null) {
		data.gameId = json['gameId'] is String
				? int.tryParse(json['gameId'])
				: json['gameId'].toInt();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['commandId'] != null) {
		data.commandId = json['commandId'] is String
				? int.tryParse(json['commandId'])
				: json['commandId'].toInt();
	}
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['desc'] != null) {
		data.desc = json['desc'].toString();
	}
	if (json['result'] != null) {
		data.result = YBDResult().fromJson(json['result']);
	}
	return data;
}

Map<String, dynamic> yBDTeenJoinMsgToJson(YBDTeenJoinMsg entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['index'] = entity.index;
	data['coins'] = entity.coins;
	data['gameId'] = entity.gameId;
	data['roomId'] = entity.roomId;
	data['commandId'] = entity.commandId;
	data['code'] = entity.code;
	data['desc'] = entity.desc;
	data['result'] = entity.result?.toJson();
	return data;
}