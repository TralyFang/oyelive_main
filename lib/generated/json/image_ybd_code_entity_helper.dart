import 'package:oyelive_main/module/entity/image_ybd_code_entity.dart';

yBDImageCodeEntityFromJson(YBDImageCodeEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDImageCodeRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDImageCodeEntityToJson(YBDImageCodeEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDImageCodeRecordFromJson(YBDImageCodeRecord data, Map<String, dynamic> json) {
	if (json['imageCode'] != null) {
		data.imageCode = json['imageCode'].toString();
	}
	return data;
}

Map<String, dynamic> yBDImageCodeRecordToJson(YBDImageCodeRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['imageCode'] = entity.imageCode;
	return data;
}