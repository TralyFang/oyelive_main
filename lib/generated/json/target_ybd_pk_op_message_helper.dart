import 'package:oyelive_main/common/room_socket/message/common/target_ybd_pk_op_message.dart';

yBDTargetPkOpMessageFromJson(YBDTargetPkOpMessage data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['source'] != null) {
		data.source = json['source'] is String
				? int.tryParse(json['source'])
				: json['source'].toInt();
	}
	if (json['target'] != null) {
		data.target = json['target'] is String
				? int.tryParse(json['target'])
				: json['target'].toInt();
	}
	if (json['timeZone'] != null) {
		data.timeZone = json['timeZone'].toString();
	}
	if (json['rejectFriends'] != null) {
		data.rejectFriends = json['rejectFriends'];
	}
	return data;
}

Map<String, dynamic> yBDTargetPkOpMessageToJson(YBDTargetPkOpMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['source'] = entity.source;
	data['target'] = entity.target;
	data['timeZone'] = entity.timeZone;
	data['rejectFriends'] = entity.rejectFriends;
	return data;
}