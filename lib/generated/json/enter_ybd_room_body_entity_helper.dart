import 'package:oyelive_main/ui/page/game_room/entity/enter_ybd_room_body_entity.dart';
import 'dart:async';

yBDEnterRoomBodyEntityEntityFromJson(YBDEnterRoomBodyEntityEntity data, Map<String, dynamic> json) {
	if (json['body'] != null) {
		data.body = YBDEnterRoomBodyEntityBody().fromJson(json['body']);
	}
	if (json['cmd'] != null) {
		data.cmd = json['cmd'].toString();
	}
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['codeMsg'] != null) {
		data.codeMsg = json['codeMsg'].toString();
	}
	if (json['header'] != null) {
		data.header = YBDEnterRoomBodyEntityHeader().fromJson(json['header']);
	}
	if (json['resources'] != null) {
		data.resources = YBDEnterRoomBodyEntityResources().fromJson(json['resources']);
	}
	if (json['route'] != null) {
		data.route = YBDEnterRoomBodyEntityRoute().fromJson(json['route']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	if (json['ver'] != null) {
		data.ver = json['ver'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEnterRoomBodyEntityEntityToJson(YBDEnterRoomBodyEntityEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['body'] = entity.body?.toJson();
	data['cmd'] = entity.cmd;
	data['code'] = entity.code;
	data['codeMsg'] = entity.codeMsg;
	data['header'] = entity.header?.toJson();
	data['resources'] = entity.resources?.toJson();
	data['route'] = entity.route?.toJson();
	data['success'] = entity.success;
	data['ver'] = entity.ver;
	return data;
}

yBDEnterRoomBodyEntityBodyFromJson(YBDEnterRoomBodyEntityBody data, Map<String, dynamic> json) {
	if (json['provider'] != null) {
		data.provider = YBDEnterRoomBodyEntityBodyProvider().fromJson(json['provider']);
	}
	return data;
}

Map<String, dynamic> yBDEnterRoomBodyEntityBodyToJson(YBDEnterRoomBodyEntityBody entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['provider'] = entity.provider?.toJson();
	return data;
}

yBDEnterRoomBodyEntityBodyProviderFromJson(YBDEnterRoomBodyEntityBodyProvider data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['token'] != null) {
		data.token = json['token'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEnterRoomBodyEntityBodyProviderToJson(YBDEnterRoomBodyEntityBodyProvider entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['token'] = entity.token;
	return data;
}

yBDEnterRoomBodyEntityHeaderFromJson(YBDEnterRoomBodyEntityHeader data, Map<String, dynamic> json) {
	if (json['connection-id'] != null) {
		data.connectionId = json['connection-id'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEnterRoomBodyEntityHeaderToJson(YBDEnterRoomBodyEntityHeader entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['connection-id'] = entity.connectionId;
	return data;
}

yBDEnterRoomBodyEntityResourcesFromJson(YBDEnterRoomBodyEntityResources data, Map<String, dynamic> json) {
	if (json['user'] != null) {
		data.user = json['user'];
	}
	if (json['userAttribute'] != null) {
		data.userAttribute = json['userAttribute'];
	}
	return data;
}

Map<String, dynamic> yBDEnterRoomBodyEntityResourcesToJson(YBDEnterRoomBodyEntityResources entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['user'] = entity.user;
	data['userAttribute'] = entity.userAttribute;
	return data;
}

yBDEnterRoomBodyEntityRouteFromJson(YBDEnterRoomBodyEntityRoute data, Map<String, dynamic> json) {
	if (json['from'] != null) {
		data.from = json['from'].toString();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['room'] != null) {
		data.room = json['room'].toString();
	}
	if (json['to'] != null) {
		data.to = (json['to'] as List).map((v) => v.toString()).toList().cast<String>();
	}
	return data;
}

Map<String, dynamic> yBDEnterRoomBodyEntityRouteToJson(YBDEnterRoomBodyEntityRoute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['from'] = entity.from;
	data['mode'] = entity.mode;
	data['room'] = entity.room;
	data['to'] = entity.to;
	return data;
}