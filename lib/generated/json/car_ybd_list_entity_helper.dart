import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_record_extend.dart';

yBDCarListEntityFromJson(YBDCarListEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDCarListRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDCarListEntityToJson(YBDCarListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDCarListRecordFromJson(YBDCarListRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['createtime'] != null) {
		data.createtime = json['createtime'];
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['animation'] != null) {
		data.animation = json['animation'].toString();
	}
	if (json['display'] != null) {
		data.display = json['display'] is String
				? int.tryParse(json['display'])
				: json['display'].toInt();
	}
	if (json['animationDisplay'] != null) {
		data.animationDisplay = json['animationDisplay'] is String
				? int.tryParse(json['animationDisplay'])
				: json['animationDisplay'].toInt();
	}
	if (json['animationFile'] != null) {
		data.animationFile = json['animationFile'].toString();
	}
	if (json['animationInfo'] != null) {
		data.animationInfo = YBDCarListRecordAnimationInfo().fromJson(json['animationInfo']);
	}
	if (json['modify'] != null) {
		data.modify = json['modify'];
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['replaceDto'] != null) {
		data.replaceDto = YBDCarListRecordExtend().fromJson(json['replaceDto']);
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['conditionType'] != null) {
		data.conditionType = json['conditionType'] is String
				? int.tryParse(json['conditionType'])
				: json['conditionType'].toInt();
	}
	if (json['conditionExtends'] != null) {
		data.conditionExtends = json['conditionExtends'].toString();
	}
	return data;
}

Map<String, dynamic> yBDCarListRecordToJson(YBDCarListRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['createtime'] = entity.createtime;
	data['status'] = entity.status;
	data['price'] = entity.price;
	data['img'] = entity.img;
	data['animation'] = entity.animation;
	data['display'] = entity.display;
	data['animationDisplay'] = entity.animationDisplay;
	data['animationFile'] = entity.animationFile;
	data['animationInfo'] = entity.animationInfo?.toJson();
	data['modify'] = entity.modify;
	data['fromUser'] = entity.fromUser;
	data['replaceDto'] = entity.replaceDto?.toJson();
	data['currency'] = entity.currency;
	data['conditionType'] = entity.conditionType;
	data['conditionExtends'] = entity.conditionExtends;
	return data;
}

yBDCarListRecordAnimationInfoFromJson(YBDCarListRecordAnimationInfo data, Map<String, dynamic> json) {
	if (json['animationId'] != null) {
		data.animationId = json['animationId'] is String
				? int.tryParse(json['animationId'])
				: json['animationId'].toInt();
	}
	if (json['animationType'] != null) {
		data.animationType = json['animationType'] is String
				? int.tryParse(json['animationType'])
				: json['animationType'].toInt();
	}
	if (json['foldername'] != null) {
		data.foldername = json['foldername'].toString();
	}
	if (json['framDuration'] != null) {
		data.framDuration = json['framDuration'] is String
				? int.tryParse(json['framDuration'])
				: json['framDuration'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDCarListRecordAnimationInfoToJson(YBDCarListRecordAnimationInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['animationId'] = entity.animationId;
	data['animationType'] = entity.animationType;
	data['foldername'] = entity.foldername;
	data['framDuration'] = entity.framDuration;
	data['name'] = entity.name;
	data['version'] = entity.version;
	return data;
}