import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

yBDRoomInfoFromJson(YBDRoomInfo data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['vip'] != null) {
		data.vip = json['vip'] is String
				? int.tryParse(json['vip'])
				: json['vip'].toInt();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['roomlevel'] != null) {
		data.roomlevel = json['roomlevel'] is String
				? int.tryParse(json['roomlevel'])
				: json['roomlevel'].toInt();
	}
	if (json['headimg'] != null) {
		data.headimg = json['headimg'].toString();
	}
	if (json['roomimg'] != null) {
		data.roomimg = json['roomimg'].toString();
	}
	if (json['count'] != null) {
		data.count = json['count'] is String
				? int.tryParse(json['count'])
				: json['count'].toInt();
	}
	if (json['time'] != null) {
		data.time = json['time'] is String
				? int.tryParse(json['time'])
				: json['time'].toInt();
	}
	if (json['plat'] != null) {
		data.plat = json['plat'];
	}
	if (json['comet'] != null) {
		data.comet = json['comet'];
	}
	if (json['category'] != null) {
		data.category = (json['category'] as List).map((v) => YBDRoomCategory().fromJson(v)).toList();
	}
	if (json['subcategory'] != null) {
		data.subcategory = (json['subcategory'] as List).map((v) => YBDRoomCategory().fromJson(v)).toList();
	}
	if (json['screen'] != null) {
		data.screen = json['screen'] is String
				? int.tryParse(json['screen'])
				: json['screen'].toInt();
	}
	if (json['fans'] != null) {
		data.fans = json['fans'] is String
				? int.tryParse(json['fans'])
				: json['fans'].toInt();
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	if (json['media'] != null) {
		data.media = json['media'] is String
				? int.tryParse(json['media'])
				: json['media'].toInt();
	}
	if (json['money'] != null) {
		data.money = json['money'] is String
				? int.tryParse(json['money'])
				: json['money'].toInt();
	}
	if (json['protectMode'] != null) {
		data.protectMode = json['protectMode'] is String
				? int.tryParse(json['protectMode'])
				: json['protectMode'].toInt();
	}
	if (json['roomexper'] != null) {
		data.roomexper = json['roomexper'] is String
				? int.tryParse(json['roomexper'])
				: json['roomexper'].toInt();
	}
	if (json['tags'] != null) {
		data.tags = (json['tags'] as List).map((v) => YBDRoomTag().fromJson(v)).toList();
	}
	if (json['live'] != null) {
		data.live = json['live'];
	}
	if (json['friend'] != null) {
		data.friend = json['friend'];
	}
	if (json['tag'] != null) {
		data.tag = json['tag'].toString();
	}
	if (json['micRequestEnable'] != null) {
		data.micRequestEnable = json['micRequestEnable'] is String
				? int.tryParse(json['micRequestEnable'])
				: json['micRequestEnable'].toInt();
	}
	if (json['extendVo'] != null) {
		data.extendVo = YBDExtendVo().fromJson(json['extendVo']);
	}
	if (json['activityRanking'] != null) {
		data.activityRanking = json['activityRanking'] is String
				? int.tryParse(json['activityRanking'])
				: json['activityRanking'].toInt();
	}
	if (json['playingLudo'] != null) {
		data.playingLudo = json['playingLudo'];
	}
	if (json['ludoIconUrl'] != null) {
		data.ludoIconUrl = json['ludoIconUrl'].toString();
	}
	if (json['roomMark'] != null) {
		data.roomMark = json['roomMark'].toString();
	}
	if (json['pk'] != null) {
		data.pk = json['pk'];
	}
	if (json['isOnline'] != null) {
		data.isOnline = json['isOnline'];
	}
	if (json['vipIcon'] != null) {
		data.vipIcon = json['vipIcon'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRoomInfoToJson(YBDRoomInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['nickname'] = entity.nickname;
	data['title'] = entity.title;
	data['vip'] = entity.vip;
	data['level'] = entity.level;
	data['roomlevel'] = entity.roomlevel;
	data['headimg'] = entity.headimg;
	data['roomimg'] = entity.roomimg;
	data['count'] = entity.count;
	data['time'] = entity.time;
	data['plat'] = entity.plat;
	data['comet'] = entity.comet;
	data['category'] =  entity.category?.map((v) => v?.toJson())?.toList();
	data['subcategory'] =  entity.subcategory?.map((v) => v?.toJson())?.toList();
	data['screen'] = entity.screen;
	data['fans'] = entity.fans;
	data['sex'] = entity.sex;
	data['media'] = entity.media;
	data['money'] = entity.money;
	data['protectMode'] = entity.protectMode;
	data['roomexper'] = entity.roomexper;
	data['tags'] =  entity.tags?.map((v) => v?.toJson())?.toList();
	data['live'] = entity.live;
	data['friend'] = entity.friend;
	data['tag'] = entity.tag;
	data['micRequestEnable'] = entity.micRequestEnable;
	data['extendVo'] = entity.extendVo?.toJson();
	data['activityRanking'] = entity.activityRanking;
	data['playingLudo'] = entity.playingLudo;
	data['ludoIconUrl'] = entity.ludoIconUrl;
	data['roomMark'] = entity.roomMark;
	data['pk'] = entity.pk;
	data['isOnline'] = entity.isOnline;
	data['vipIcon'] = entity.vipIcon;
	return data;
}

yBDRoomCategoryFromJson(YBDRoomCategory data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRoomCategoryToJson(YBDRoomCategory entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

yBDRoomTagFromJson(YBDRoomTag data, Map<String, dynamic> json) {
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['value'] != null) {
		data.value = json['value'].toString();
	}
	if (json['expireAt'] != null) {
		data.expireAt = json['expireAt'] is String
				? int.tryParse(json['expireAt'])
				: json['expireAt'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDRoomTagToJson(YBDRoomTag entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['name'] = entity.name;
	data['value'] = entity.value;
	data['expireAt'] = entity.expireAt;
	return data;
}

yBDExtendVoFromJson(YBDExtendVo data, Map<String, dynamic> json) {
	if (json['activityRanking'] != null) {
		data.activityRanking = json['activityRanking'] is String
				? int.tryParse(json['activityRanking'])
				: json['activityRanking'].toInt();
	}
	if (json['pk'] != null) {
		data.pk = json['pk'];
	}
	if (json['playingLudo'] != null) {
		data.playingLudo = json['playingLudo'];
	}
	if (json['ludoIconUrl'] != null) {
		data.ludoIconUrl = json['ludoIconUrl'].toString();
	}
	return data;
}

Map<String, dynamic> yBDExtendVoToJson(YBDExtendVo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['activityRanking'] = entity.activityRanking;
	data['pk'] = entity.pk;
	data['playingLudo'] = entity.playingLudo;
	data['ludoIconUrl'] = entity.ludoIconUrl;
	return data;
}