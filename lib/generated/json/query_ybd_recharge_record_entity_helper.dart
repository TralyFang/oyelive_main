import 'package:oyelive_main/ui/page/recharge/entity/query_ybd_recharge_record_entity.dart';
import 'dart:async';

yBDQueryRechargeRecordEntityFromJson(YBDQueryRechargeRecordEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['data'] != null) {
		data.data = (json['data'] as List).map((v) => YBDRechargeRecord().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDQueryRechargeRecordEntityToJson(YBDQueryRechargeRecordEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['returnMsg'] = entity.returnMsg;
	data['data'] =  entity.data?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDRechargeRecordFromJson(YBDRechargeRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'] is String
				? int.tryParse(json['createTime'])
				: json['createTime'].toInt();
	}
	if (json['money'] != null) {
		data.money = json['money'] is String
				? int.tryParse(json['money'])
				: json['money'].toInt();
	}
	if (json['virtualCurrencyAmount'] != null) {
		data.virtualCurrencyAmount = json['virtualCurrencyAmount'] is String
				? int.tryParse(json['virtualCurrencyAmount'])
				: json['virtualCurrencyAmount'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['transid'] != null) {
		data.transid = json['transid'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRechargeRecordToJson(YBDRechargeRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['createTime'] = entity.createTime;
	data['money'] = entity.money;
	data['virtualCurrencyAmount'] = entity.virtualCurrencyAmount;
	data['type'] = entity.type;
	data['transid'] = entity.transid;
	data['status'] = entity.status;
	return data;
}