import 'package:oyelive_main/common/room_socket/message/common/broadcast.dart';

yBDBroadcastFromJson(YBDBroadcast data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['screen'] != null) {
		data.screen = json['screen'] is String
				? int.tryParse(json['screen'])
				: json['screen'].toInt();
	}
	if (json['protectMode'] != null) {
		data.protectMode = json['protectMode'] is String
				? int.tryParse(json['protectMode'])
				: json['protectMode'].toInt();
	}
	if (json['protectData'] != null) {
		data.protectData = json['protectData'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['channel'] != null) {
		data.channel = json['channel'].toString();
	}
	if (json['tag'] != null) {
		data.tag = json['tag'].toString();
	}
	if (json['micRequestEnable'] != null) {
		data.micRequestEnable = json['micRequestEnable'] is String
				? int.tryParse(json['micRequestEnable'])
				: json['micRequestEnable'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDBroadcastToJson(YBDBroadcast entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['screen'] = entity.screen;
	data['protectMode'] = entity.protectMode;
	data['protectData'] = entity.protectData;
	data['title'] = entity.title;
	data['channel'] = entity.channel;
	data['tag'] = entity.tag;
	data['micRequestEnable'] = entity.micRequestEnable;
	return data;
}