import 'package:oyelive_main/ui/page/game_lobby/entity/game_ybd_lobby_entity.dart';
import 'dart:async';

yBDGameLobbyEntityFromJson(YBDGameLobbyEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = (json['data'] as List).map((v) => YBDGameLobbyData().fromJson(v)).toList();
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDGameLobbyEntityToJson(YBDGameLobbyEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v?.toJson())?.toList();
	data['success'] = entity.success;
	return data;
}

yBDGameLobbyDataFromJson(YBDGameLobbyData data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['tabIcon'] != null) {
		data.tabIcon = json['tabIcon'].toString();
	}
	if (json['tabIconSelected'] != null) {
		data.tabIconSelected = json['tabIconSelected'].toString();
	}
	if (json['titleIcon'] != null) {
		data.titleIcon = json['titleIcon'].toString();
	}
	if (json['item'] != null) {
		data.item = (json['item'] as List).map((v) => YBDGameLobbyDataItem().fromJson(v)).toList();
	}
	if (json['tabIndex'] != null) {
		data.tabIndex = json['tabIndex'] is String
				? int.tryParse(json['tabIndex'])
				: json['tabIndex'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGameLobbyDataToJson(YBDGameLobbyData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['tabIcon'] = entity.tabIcon;
	data['tabIconSelected'] = entity.tabIconSelected;
	data['titleIcon'] = entity.titleIcon;
	data['item'] =  entity.item?.map((v) => v?.toJson())?.toList();
	data['tabIndex'] = entity.tabIndex;
	return data;
}

yBDGameLobbyDataItemFromJson(YBDGameLobbyDataItem data, Map<String, dynamic> json) {
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['routeType'] != null) {
		data.routeType = json['routeType'] is String
				? int.tryParse(json['routeType'])
				: json['routeType'].toInt();
	}
	if (json['param'] != null) {
		data.param = json['param'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['gameRoomOpenConfig'] != null) {
		data.gameRoomOpenConfig = YBDGameRoomConfig().fromJson(json['gameRoomOpenConfig']);
	}
	if (json['modeName'] != null) {
		data.modeName = json['modeName'].toString();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['boot'] != null) {
		data.boot = json['boot'].toString();
	}
	if (json['minTicket'] != null) {
		data.minTicket = json['minTicket'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameLobbyDataItemToJson(YBDGameLobbyDataItem entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['img'] = entity.img;
	data['routeType'] = entity.routeType;
	data['param'] = entity.param;
	data['status'] = entity.status;
	data['index'] = entity.index;
	data['gameRoomOpenConfig'] = entity.gameRoomOpenConfig?.toJson();
	data['modeName'] = entity.modeName;
	data['currency'] = entity.currency;
	data['boot'] = entity.boot;
	data['minTicket'] = entity.minTicket;
	return data;
}

yBDGameRoomConfigFromJson(YBDGameRoomConfig data, Map<String, dynamic> json) {
	if (json['disableImgUrl'] != null) {
		data.disableImgUrl = json['disableImgUrl'].toString();
	}
	if (json['openStatus'] != null) {
		data.openStatus = json['openStatus'] is String
				? int.tryParse(json['openStatus'])
				: json['openStatus'].toInt();
	}
	if (json['alertMsg'] != null) {
		data.alertMsg = json['alertMsg'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomConfigToJson(YBDGameRoomConfig entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['disableImgUrl'] = entity.disableImgUrl;
	data['openStatus'] = entity.openStatus;
	data['alertMsg'] = entity.alertMsg;
	return data;
}