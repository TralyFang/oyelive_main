import 'package:oyelive_main/module/entity/agency_ybd_talent_entity.dart';

yBDAgencyTalentEntityFromJson(YBDAgencyTalentEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDAgencyTalentData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDAgencyTalentEntityToJson(YBDAgencyTalentEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	return data;
}

yBDAgencyTalentDataFromJson(YBDAgencyTalentData data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['agencyId'] != null) {
		data.agencyId = json['agencyId'].toString();
	}
	if (json['talentId'] != null) {
		data.talentId = json['talentId'] is String
				? int.tryParse(json['talentId'])
				: json['talentId'].toInt();
	}
	if (json['registerTime'] != null) {
		data.registerTime = json['registerTime'].toString();
	}
	if (json['joinTime'] != null) {
		data.joinTime = json['joinTime'].toString();
	}
	return data;
}

Map<String, dynamic> yBDAgencyTalentDataToJson(YBDAgencyTalentData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['agencyId'] = entity.agencyId;
	data['talentId'] = entity.talentId;
	data['registerTime'] = entity.registerTime;
	data['joinTime'] = entity.joinTime;
	return data;
}