import 'package:oyelive_main/module/status/entity/reply_ybd_list_entity.dart';
import 'package:oyelive_main/module/status/entity/comment_ybd_list_entity.dart';

yBDReplyListEntityFromJson(YBDReplyListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDReplyListData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	return data;
}

Map<String, dynamic> yBDReplyListEntityToJson(YBDReplyListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	data['message'] = entity.message;
	return data;
}

yBDReplyListDataFromJson(YBDReplyListData data, Map<String, dynamic> json) {
	if (json['rows'] != null) {
		data.rows = (json['rows'] as List).map((v) => YBDReplyData().fromJson(v)).toList();
	}
	if (json['total'] != null) {
		data.total = json['total'] is String
				? int.tryParse(json['total'])
				: json['total'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDReplyListDataToJson(YBDReplyListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rows'] =  entity.rows?.map((v) => v?.toJson())?.toList();
	data['total'] = entity.total;
	return data;
}