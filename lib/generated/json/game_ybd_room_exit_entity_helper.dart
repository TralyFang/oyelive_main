import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_exit_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

yBDGameRoomExitEntityFromJson(YBDGameRoomExitEntity data, Map<String, dynamic> json) {
	if (json['cmd'] != null) {
		data.cmd = json['cmd'].toString();
	}
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['codeMsg'] != null) {
		data.codeMsg = json['codeMsg'].toString();
	}
	if (json['route'] != null) {
		data.route = YBDGameRoomExitRoute().fromJson(json['route']);
	}
	return data;
}

Map<String, dynamic> yBDGameRoomExitEntityToJson(YBDGameRoomExitEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['cmd'] = entity.cmd;
	data['code'] = entity.code;
	data['codeMsg'] = entity.codeMsg;
	data['route'] = entity.route?.toJson();
	return data;
}

yBDGameRoomExitRouteFromJson(YBDGameRoomExitRoute data, Map<String, dynamic> json) {
	if (json['from'] != null) {
		data.from = json['from'].toString();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['room'] != null) {
		data.room = json['room'].toString();
	}
	if (json['to'] != null) {
		data.to = (json['to'] as List).map((v) => v.toString()).toList().cast<String>();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomExitRouteToJson(YBDGameRoomExitRoute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['from'] = entity.from;
	data['mode'] = entity.mode;
	data['room'] = entity.room;
	data['to'] = entity.to;
	return data;
}