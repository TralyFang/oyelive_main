import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_enter_entity.dart';
import 'package:intl/intl.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';

yBDGameRoomEnterEntityFromJson(YBDGameRoomEnterEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDGameRoomEnterData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomEnterEntityToJson(YBDGameRoomEnterEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	data['message'] = entity.message;
	return data;
}

yBDGameRoomEnterDataFromJson(YBDGameRoomEnterData data, Map<String, dynamic> json) {
	if (json['rows'] != null) {
		data.rows = (json['rows'] as List).map((v) => YBDGameRoomEnterDataRows().fromJson(v)).toList();
	}
	if (json['total'] != null) {
		data.total = json['total'] is String
				? int.tryParse(json['total'])
				: json['total'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomEnterDataToJson(YBDGameRoomEnterData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rows'] =  entity.rows?.map((v) => v.toJson())?.toList();
	data['total'] = entity.total;
	return data;
}

yBDGameRoomEnterDataRowsFromJson(YBDGameRoomEnterDataRows data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'];
	}
	if (json['projectCode'] != null) {
		data.projectCode = json['projectCode'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'].toString();
	}
	if (json['tenantCode'] != null) {
		data.tenantCode = json['tenantCode'].toString();
	}
	if (json['category'] != null) {
		data.category = json['category'].toString();
	}
	if (json['subCategory'] != null) {
		data.subCategory = json['subCategory'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['subStatus'] != null) {
		data.subStatus = json['subStatus'].toString();
	}
	if (json['property'] != null) {
		data.property = YBDGameRoomEnterDataRowsProperty().fromJson(json['property']);
	}
	if (json['users'] != null) {
		data.users = (json['users'] as List).map((v) => YBDGameRoomEnterDataRowsUsers().fromJson(v)).toList();
	}
	if (json['userCount'] != null) {
		data.userCount = json['userCount'] is String
				? int.tryParse(json['userCount'])
				: json['userCount'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomEnterDataRowsToJson(YBDGameRoomEnterDataRows entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['projectCode'] = entity.projectCode;
	data['roomId'] = entity.roomId;
	data['tenantCode'] = entity.tenantCode;
	data['category'] = entity.category;
	data['subCategory'] = entity.subCategory;
	data['status'] = entity.status;
	data['subStatus'] = entity.subStatus;
	data['property'] = entity.property?.toJson();
	data['users'] =  entity.users?.map((v) => v.toJson())?.toList();
	data['userCount'] = entity.userCount;
	return data;
}

yBDGameRoomEnterDataRowsPropertyFromJson(YBDGameRoomEnterDataRowsProperty data, Map<String, dynamic> json) {
	if (json['pubAttr'] != null) {
		data.pubAttr = YBDGameRoomEnterDataRowsPropertyPubAttr().fromJson(json['pubAttr']);
	}
	if (json['gameAttr'] != null) {
		data.gameAttr = YBDGameRoomEnterDataRowsPropertyGameAttr().fromJson(json['gameAttr']);
	}
	return data;
}

Map<String, dynamic> yBDGameRoomEnterDataRowsPropertyToJson(YBDGameRoomEnterDataRowsProperty entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['pubAttr'] = entity.pubAttr?.toJson();
	data['gameAttr'] = entity.gameAttr?.toJson();
	return data;
}

yBDGameRoomEnterDataRowsPropertyPubAttrFromJson(YBDGameRoomEnterDataRowsPropertyPubAttr data, Map<String, dynamic> json) {
	if (json['poster'] != null) {
		data.poster = json['poster'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomEnterDataRowsPropertyPubAttrToJson(YBDGameRoomEnterDataRowsPropertyPubAttr entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['poster'] = entity.poster;
	data['title'] = entity.title;
	return data;
}

yBDGameRoomEnterDataRowsPropertyGameAttrFromJson(YBDGameRoomEnterDataRowsPropertyGameAttr data, Map<String, dynamic> json) {
	if (json['model'] != null) {
		data.model = json['model'].toString();
	}
	if (json['amount'] != null) {
		data.amount = json['amount'] is String
				? int.tryParse(json['amount'])
				: json['amount'].toInt();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['modeName'] != null) {
		data.modeName = json['modeName'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomEnterDataRowsPropertyGameAttrToJson(YBDGameRoomEnterDataRowsPropertyGameAttr entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['model'] = entity.model;
	data['amount'] = entity.amount;
	data['currency'] = entity.currency;
	data['modeName'] = entity.modeName;
	return data;
}

yBDGameRoomEnterDataRowsUsersFromJson(YBDGameRoomEnterDataRowsUsers data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	if (json['attribute'] != null) {
		data.attribute = YBDGameRoomEnterDataRowsUsersAttribute().fromJson(json['attribute']);
	}
	return data;
}

Map<String, dynamic> yBDGameRoomEnterDataRowsUsersToJson(YBDGameRoomEnterDataRowsUsers entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['attribute'] = entity.attribute?.toJson();
	return data;
}

yBDGameRoomEnterDataRowsUsersAttributeFromJson(YBDGameRoomEnterDataRowsUsersAttribute data, Map<String, dynamic> json) {
	if (json['gender'] != null) {
		data.gender = json['gender'].toString();
	}
	if (json['nickName'] != null) {
		data.nickName = json['nickName'].toString();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'].toString();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['roomLevel'] != null) {
		data.roomLevel = json['roomLevel'] is String
				? int.tryParse(json['roomLevel'])
				: json['roomLevel'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGameRoomEnterDataRowsUsersAttributeToJson(YBDGameRoomEnterDataRowsUsersAttribute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['gender'] = entity.gender;
	data['nickName'] = entity.nickName;
	data['avatar'] = entity.avatar;
	data['level'] = entity.level;
	data['roomLevel'] = entity.roomLevel;
	return data;
}