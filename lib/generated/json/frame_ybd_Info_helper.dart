import 'package:oyelive_main/ui/page/room/entity/frame_ybd_Info.dart';
import 'dart:async';

yBDFrameInfoFromJson(YBDFrameInfo data, Map<String, dynamic> json) {
	if (json['image'] != null) {
		data.image = json['image'].toString();
	}
	if (json['personalId'] != null) {
		data.personalId = json['personalId'] is String
				? int.tryParse(json['personalId'])
				: json['personalId'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	return data;
}

Map<String, dynamic> yBDFrameInfoToJson(YBDFrameInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['image'] = entity.image;
	data['personalId'] = entity.personalId;
	data['name'] = entity.name;
	return data;
}