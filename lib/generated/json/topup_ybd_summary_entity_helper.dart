import 'package:oyelive_main/module/user/entity/topup_ybd_summary_entity.dart';

yBDTopUpSummaryEntityFromJson(YBDTopUpSummaryEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDTopUpSummaryRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDTopUpSummaryEntityToJson(YBDTopUpSummaryEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDTopUpSummaryRecordFromJson(YBDTopUpSummaryRecord data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['recharge'] != null) {
		data.recharge = json['recharge'];
	}
	return data;
}

Map<String, dynamic> yBDTopUpSummaryRecordToJson(YBDTopUpSummaryRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['recharge'] = entity.recharge;
	return data;
}