import 'package:oyelive_main/ui/page/room/entity/mic_ybd_item.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/room/entity/frame_ybd_Info.dart';
import 'package:oyelive_main/ui/page/room/entity/mute_ybd_Info.dart';

yBDMicItemFromJson(YBDMicItem data, Map<String, dynamic> json) {
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['time'] != null) {
		data.time = json['time'] is String
				? double.tryParse(json['time'])
				: json['time'].toDouble();
	}
	if (json['period'] != null) {
		data.period = json['period'] is String
				? int.tryParse(json['period'])
				: json['period'].toInt();
	}
	if (json['attributes'] != null) {
		data.attributes = YBDAttributes().fromJson(json['attributes']);
	}
	if (json['vipIcon'] != null) {
		data.vipIcon = json['vipIcon'].toString();
	}
	return data;
}

Map<String, dynamic> yBDMicItemToJson(YBDMicItem entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['status'] = entity.status;
	data['index'] = entity.index;
	data['userId'] = entity.userId;
	data['nickname'] = entity.nickname;
	data['img'] = entity.img;
	data['time'] = entity.time;
	data['period'] = entity.period;
	data['attributes'] = entity.attributes?.toJson();
	data['vipIcon'] = entity.vipIcon;
	return data;
}

yBDAttributesFromJson(YBDAttributes data, Map<String, dynamic> json) {
	if (json['frame'] != null) {
		data.frame = YBDFrameInfo().fromJson(json['frame']);
	}
	if (json['muteInfo'] != null) {
		data.muteInfo = YBDMuteInfo().fromJson(json['muteInfo']);
	}
	return data;
}

Map<String, dynamic> yBDAttributesToJson(YBDAttributes entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['frame'] = entity.frame?.toJson();
	data['muteInfo'] = entity.muteInfo?.toJson();
	return data;
}