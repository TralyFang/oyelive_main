import 'package:oyelive_main/module/user/entity/account_ybd_delete_resp_entity.dart';

yBDAccountDeleteRespEntityFromJson(YBDAccountDeleteRespEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDAccountDeleteRespRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDAccountDeleteRespEntityToJson(YBDAccountDeleteRespEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDAccountDeleteRespRecordFromJson(YBDAccountDeleteRespRecord data, Map<String, dynamic> json) {
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	return data;
}

Map<String, dynamic> yBDAccountDeleteRespRecordToJson(YBDAccountDeleteRespRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['message'] = entity.message;
	data['status'] = entity.status;
	return data;
}