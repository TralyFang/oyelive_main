import 'package:oyelive_main/ui/page/home/entity/rank_ybd_info_entity.dart';
import 'dart:async';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

yBDRankInfoEntityFromJson(YBDRankInfoEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDRankInfoRecord().fromJson(json['record']);
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'];
	}
	return data;
}

Map<String, dynamic> yBDRankInfoEntityToJson(YBDRankInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDRankInfoRecordFromJson(YBDRankInfoRecord data, Map<String, dynamic> json) {
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['dateType'] != null) {
		data.dateType = json['dateType'] is String
				? int.tryParse(json['dateType'])
				: json['dateType'].toInt();
	}
	if (json['labelId'] != null) {
		data.labelId = json['labelId'];
	}
	if (json['rank'] != null) {
		data.rank = (json['rank'] as List).map((v) => YBDRoomInfo().fromJson(v)).toList();
	}
	if (json['scores'] != null) {
		data.scores = (json['scores'] as List).map((v) => v is String
				? double.tryParse(v)
				: v.toDouble()).toList().cast<double>();
	}
	return data;
}

Map<String, dynamic> yBDRankInfoRecordToJson(YBDRankInfoRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['name'] = entity.name;
	data['dateType'] = entity.dateType;
	data['labelId'] = entity.labelId;
	data['rank'] =  entity.rank?.map((v) => v?.toJson())?.toList();
	data['scores'] = entity.scores;
	return data;
}