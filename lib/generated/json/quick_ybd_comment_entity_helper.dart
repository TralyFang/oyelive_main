import 'package:oyelive_main/ui/page/game_room/entity/quick_ybd_comment_entity.dart';

yBDQuickCommentEntityFromJson(YBDQuickCommentEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = (json['data'] as List).map((v) => YBDQuickCommentData().fromJson(v)).toList();
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDQuickCommentEntityToJson(YBDQuickCommentEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v?.toJson())?.toList();
	data['success'] = entity.success;
	return data;
}

yBDQuickCommentDataFromJson(YBDQuickCommentData data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['data'] != null) {
		data.data = json['data'].toString();
	}
	return data;
}

Map<String, dynamic> yBDQuickCommentDataToJson(YBDQuickCommentData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['data'] = entity.data;
	return data;
}