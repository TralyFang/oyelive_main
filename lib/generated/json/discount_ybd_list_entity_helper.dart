import 'package:oyelive_main/ui/page/home/entity/discount_ybd_list_entity.dart';
import 'dart:async';

yBDDiscountListEntityFromJson(YBDDiscountListEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDDiscountListRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDDiscountListEntityToJson(YBDDiscountListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDDiscountListRecordFromJson(YBDDiscountListRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['itemId'] != null) {
		data.itemId = json['itemId'] is String
				? int.tryParse(json['itemId'])
				: json['itemId'].toInt();
	}
	if (json['itemType'] != null) {
		data.itemType = json['itemType'] is String
				? int.tryParse(json['itemType'])
				: json['itemType'].toInt();
	}
	if (json['sold'] != null) {
		data.sold = json['sold'] is String
				? int.tryParse(json['sold'])
				: json['sold'].toInt();
	}
	if (json['stock'] != null) {
		data.stock = json['stock'] is String
				? int.tryParse(json['stock'])
				: json['stock'].toInt();
	}
	if (json['frozen'] != null) {
		data.frozen = json['frozen'] is String
				? int.tryParse(json['frozen'])
				: json['frozen'].toInt();
	}
	if (json['originalPrice'] != null) {
		data.originalPrice = json['originalPrice'] is String
				? int.tryParse(json['originalPrice'])
				: json['originalPrice'].toInt();
	}
	if (json['discountPrice'] != null) {
		data.discountPrice = json['discountPrice'] is String
				? int.tryParse(json['discountPrice'])
				: json['discountPrice'].toInt();
	}
	if (json['startTime'] != null) {
		data.startTime = json['startTime'];
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'];
	}
	if (json['startTimestamp'] != null) {
		data.startTimestamp = json['startTimestamp'] is String
				? int.tryParse(json['startTimestamp'])
				: json['startTimestamp'].toInt();
	}
	if (json['endTimestamp'] != null) {
		data.endTimestamp = json['endTimestamp'] is String
				? int.tryParse(json['endTimestamp'])
				: json['endTimestamp'].toInt();
	}
	if (json['rank'] != null) {
		data.rank = json['rank'] is String
				? int.tryParse(json['rank'])
				: json['rank'].toInt();
	}
	if (json['count'] != null) {
		data.count = json['count'];
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'] is String
				? int.tryParse(json['createTime'])
				: json['createTime'].toInt();
	}
	if (json['deleted'] != null) {
		data.deleted = json['deleted'] is String
				? int.tryParse(json['deleted'])
				: json['deleted'].toInt();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['projectCode'] != null) {
		data.projectCode = json['projectCode'];
	}
	if (json['tenantCode'] != null) {
		data.tenantCode = json['tenantCode'];
	}
	if (json['thumbnail'] != null) {
		data.thumbnail = json['thumbnail'].toString();
	}
	if (json['animation'] != null) {
		data.animation = json['animation'];
	}
	if (json['animationDisplay'] != null) {
		data.animationDisplay = json['animationDisplay'];
	}
	if (json['animationFile'] != null) {
		data.animationFile = json['animationFile'];
	}
	if (json['animationInfo'] != null) {
		data.animationInfo = json['animationInfo'];
	}
	if (json['name'] != null) {
		data.name = json['name'];
	}
	return data;
}

Map<String, dynamic> yBDDiscountListRecordToJson(YBDDiscountListRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['itemId'] = entity.itemId;
	data['itemType'] = entity.itemType;
	data['sold'] = entity.sold;
	data['stock'] = entity.stock;
	data['frozen'] = entity.frozen;
	data['originalPrice'] = entity.originalPrice;
	data['discountPrice'] = entity.discountPrice;
	data['startTime'] = entity.startTime;
	data['endTime'] = entity.endTime;
	data['startTimestamp'] = entity.startTimestamp;
	data['endTimestamp'] = entity.endTimestamp;
	data['rank'] = entity.rank;
	data['count'] = entity.count;
	data['status'] = entity.status;
	data['createTime'] = entity.createTime;
	data['deleted'] = entity.deleted;
	data['version'] = entity.version;
	data['projectCode'] = entity.projectCode;
	data['tenantCode'] = entity.tenantCode;
	data['thumbnail'] = entity.thumbnail;
	data['animation'] = entity.animation;
	data['animationDisplay'] = entity.animationDisplay;
	data['animationFile'] = entity.animationFile;
	data['animationInfo'] = entity.animationInfo;
	data['name'] = entity.name;
	return data;
}