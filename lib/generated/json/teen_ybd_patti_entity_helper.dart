import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_entity.dart';
import 'dart:async';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

yBDTeenPattiHistoryEntityFromJson(YBDTeenPattiHistoryEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDTeenPattiHistoryRecordEntity().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDTeenPattiHistoryEntityToJson(YBDTeenPattiHistoryEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDTeenPattiHistoryRecordEntityFromJson(YBDTeenPattiHistoryRecordEntity data, Map<String, dynamic> json) {
	if (json['gameId'] != null) {
		data.gameId = json['gameId'] is String
				? int.tryParse(json['gameId'])
				: json['gameId'].toInt();
	}
	if (json['date'] != null) {
		data.date = json['date'] is String
				? int.tryParse(json['date'])
				: json['date'].toInt();
	}
	if (json['winIndex'] != null) {
		data.winIndex = json['winIndex'] is String
				? int.tryParse(json['winIndex'])
				: json['winIndex'].toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	return data;
}

Map<String, dynamic> yBDTeenPattiHistoryRecordEntityToJson(YBDTeenPattiHistoryRecordEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['gameId'] = entity.gameId;
	data['date'] = entity.date;
	data['winIndex'] = entity.winIndex;
	data['status'] = entity.status;
	return data;
}

yBDTeenPattiREntityFromJson(YBDTeenPattiREntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDTeenPattiRecordEntity().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDTeenPattiREntityToJson(YBDTeenPattiREntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDTeenPattiRecordEntityFromJson(YBDTeenPattiRecordEntity data, Map<String, dynamic> json) {
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['updateTime'] != null) {
		data.updateTime = json['updateTime'] is String
				? int.tryParse(json['updateTime'])
				: json['updateTime'].toInt();
	}
	if (json['rank'] != null) {
		data.rank = (json['rank'] as List).map((v) => YBDTeenPattiRankEntity().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDTeenPattiRecordEntityToJson(YBDTeenPattiRecordEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['name'] = entity.name;
	data['updateTime'] = entity.updateTime;
	data['rank'] =  entity.rank?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDTeenPattiRankEntityFromJson(YBDTeenPattiRankEntity data, Map<String, dynamic> json) {
	if (json['count'] != null) {
		data.count = json['count'] is String
				? int.tryParse(json['count'])
				: json['count'].toInt();
	}
	if (json['user'] != null) {
		data.user = YBDUserInfo().fromJson(json['user']);
	}
	return data;
}

Map<String, dynamic> yBDTeenPattiRankEntityToJson(YBDTeenPattiRankEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['count'] = entity.count;
	data['user'] = entity.user?.toJson();
	return data;
}