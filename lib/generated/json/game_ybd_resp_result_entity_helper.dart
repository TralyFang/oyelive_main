import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_resp_result_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/game_room/entity/enter_ybd_room_body_entity.dart';

yBDGameRespResultEntityFromJson(YBDGameRespResultEntity data, Map<String, dynamic> json) {
	if (json['cmd'] != null) {
		data.cmd = json['cmd'].toString();
	}
	if (json['ver'] != null) {
		data.ver = json['ver'].toString();
	}
	if (json['route'] != null) {
		data.route = YBDEnterRoomBodyEntityRoute().fromJson(json['route']);
	}
	if (json['code'] != null) {
		data.code = json['code'] is String
				? int.tryParse(json['code'])
				: json['code'].toInt();
	}
	if (json['codeMsg'] != null) {
		data.codeMsg = json['codeMsg'].toString();
	}
	if (json['body'] != null) {
		data.body = json['body'];
	}
	return data;
}

Map<String, dynamic> yBDGameRespResultEntityToJson(YBDGameRespResultEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['cmd'] = entity.cmd;
	data['ver'] = entity.ver;
	data['route'] = entity.route?.toJson();
	data['code'] = entity.code;
	data['codeMsg'] = entity.codeMsg;
	data['body'] = entity.body;
	return data;
}