import 'package:oyelive_main/ui/page/store/entity/price_ybd_info_entity.dart';
import 'dart:async';

yBDPriceInfoEntityFromJson(YBDPriceInfoEntity data, Map<String, dynamic> json) {
	if (json['unit'] != null) {
		data.unit = json['unit'].toString();
	}
	if (json['discount'] != null) {
		data.discount = json['discount'].toString();
	}
	if (json['price'] != null) {
		data.price = json['price'].toString();
	}
	if (json['unitType'] != null) {
		data.unitType = json['unitType'] is String
				? int.tryParse(json['unitType'])
				: json['unitType'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPriceInfoEntityToJson(YBDPriceInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['unit'] = entity.unit;
	data['discount'] = entity.discount;
	data['price'] = entity.price;
	data['unitType'] = entity.unitType;
	return data;
}