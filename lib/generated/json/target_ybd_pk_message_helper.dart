import 'package:oyelive_main/common/room_socket/message/common/target_ybd_pk_message.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';

yBDTargetPkMessageFromJson(YBDTargetPkMessage data, Map<String, dynamic> json) {
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['content'] != null) {
		data.content = YBDTargetContent().fromJson(json['content']);
	}
	return data;
}

Map<String, dynamic> yBDTargetPkMessageToJson(YBDTargetPkMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['destination'] = entity.destination;
	data['fromUser'] = entity.fromUser;
	data['mode'] = entity.mode;
	data['msgType'] = entity.msgType;
	data['roomId'] = entity.roomId;
	data['toUser'] = entity.toUser;
	data['type'] = entity.type;
	data['content'] = entity.content?.toJson();
	return data;
}

yBDTargetContentFromJson(YBDTargetContent data, Map<String, dynamic> json) {
	if (json['sourceNickName'] != null) {
		data.sourceNickName = json['sourceNickName'].toString();
	}
	if (json['sourceHeadImg'] != null) {
		data.sourceHeadImg = json['sourceHeadImg'].toString();
	}
	if (json['targetNickName'] != null) {
		data.targetNickName = json['targetNickName'].toString();
	}
	if (json['targetHeadImg'] != null) {
		data.targetHeadImg = json['targetHeadImg'].toString();
	}
	if (json['target'] != null) {
		data.target = json['target'] is String
				? int.tryParse(json['target'])
				: json['target'].toInt();
	}
	if (json['source'] != null) {
		data.source = json['source'] is String
				? int.tryParse(json['source'])
				: json['source'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDTargetContentToJson(YBDTargetContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['sourceNickName'] = entity.sourceNickName;
	data['sourceHeadImg'] = entity.sourceHeadImg;
	data['targetNickName'] = entity.targetNickName;
	data['targetHeadImg'] = entity.targetHeadImg;
	data['target'] = entity.target;
	data['source'] = entity.source;
	return data;
}