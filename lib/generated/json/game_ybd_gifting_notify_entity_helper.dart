import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gifting_notify_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/game_room/entity/enter_ybd_room_body_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

yBDGameGiftingNotifyEntityFromJson(YBDGameGiftingNotifyEntity data, Map<String, dynamic> json) {
	if (json['cmd'] != null) {
		data.cmd = json['cmd'].toString();
	}
	if (json['ver'] != null) {
		data.ver = json['ver'].toString();
	}
	if (json['route'] != null) {
		data.route = YBDEnterRoomBodyEntityRoute().fromJson(json['route']);
	}
	if (json['code'] != null) {
		data.code = json['code'] is String
				? int.tryParse(json['code'])
				: json['code'].toInt();
	}
	if (json['codeMsg'] != null) {
		data.codeMsg = json['codeMsg'].toString();
	}
	if (json['body'] != null) {
		data.body = YBDGameGiftingNotifyBody().fromJson(json['body']);
	}
	if (json['resource'] != null) {
		data.resource = YBDEnterRoomBodyEntityResources().fromJson(json['resource']);
	}
	return data;
}

Map<String, dynamic> yBDGameGiftingNotifyEntityToJson(YBDGameGiftingNotifyEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['cmd'] = entity.cmd;
	data['ver'] = entity.ver;
	data['route'] = entity.route?.toJson();
	data['code'] = entity.code;
	data['codeMsg'] = entity.codeMsg;
	data['body'] = entity.body?.toJson();
	data['resource'] = entity.resource?.toJson();
	return data;
}

yBDGameGiftingNotifyBodyFromJson(YBDGameGiftingNotifyBody data, Map<String, dynamic> json) {
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['imageUrl'] != null) {
		data.imageUrl = json['imageUrl'].toString();
	}
	if (json['animationUrl'] != null) {
		data.animationUrl = json['animationUrl'].toString();
	}
	if (json['giftId'] != null) {
		data.giftId = json['giftId'] is String
				? int.tryParse(json['giftId'])
				: json['giftId'].toInt();
	}
	if (json['number'] != null) {
		data.number = json['number'] is String
				? int.tryParse(json['number'])
				: json['number'].toInt();
	}
	if (json['receiver'] != null) {
		data.receiver = json['receiver'] is String
				? int.tryParse(json['receiver'])
				: json['receiver'].toInt();
	}
	if (json['sender'] != null) {
		data.sender = json['sender'] is String
				? int.tryParse(json['sender'])
				: json['sender'].toInt();
	}
	if (json['animationLocalPath'] != null) {
		data.animationLocalPath = json['animationLocalPath'].toString();
	}
	if (json['animationMp3LocalPath'] != null) {
		data.animationMp3LocalPath = json['animationMp3LocalPath'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameGiftingNotifyBodyToJson(YBDGameGiftingNotifyBody entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['name'] = entity.name;
	data['imageUrl'] = entity.imageUrl;
	data['animationUrl'] = entity.animationUrl;
	data['giftId'] = entity.giftId;
	data['number'] = entity.number;
	data['receiver'] = entity.receiver;
	data['sender'] = entity.sender;
	data['animationLocalPath'] = entity.animationLocalPath;
	data['animationMp3LocalPath'] = entity.animationMp3LocalPath;
	return data;
}

yBDGamePlayerPositionFromJson(YBDGamePlayerPosition data, Map<String, dynamic> json) {
	if (json['centerX'] != null) {
		data.centerX = json['centerX'] is String
				? num.tryParse(json['centerX'])
				: json['centerX'];
	}
	if (json['centerY'] != null) {
		data.centerY = json['centerY'] is String
				? num.tryParse(json['centerY'])
				: json['centerY'];
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGamePlayerPositionToJson(YBDGamePlayerPosition entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['centerX'] = entity.centerX;
	data['centerY'] = entity.centerY;
	data['userId'] = entity.userId;
	return data;
}