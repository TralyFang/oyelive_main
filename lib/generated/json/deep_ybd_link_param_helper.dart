import 'package:oyelive_main/module/entity/deep_ybd_link_param.dart';

yBDDeepLinkParamFromJson(YBDDeepLinkParam data, Map<String, dynamic> json) {
	if (json['spf'] != null) {
		data.spf = json['spf'].toString();
	}
	if (json['inviter'] != null) {
		data.inviter = json['inviter'] is String
				? int.tryParse(json['inviter'])
				: json['inviter'].toInt();
	}
	if (json['statusId'] != null) {
		data.statusId = json['statusId'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDDeepLinkParamToJson(YBDDeepLinkParam entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['spf'] = entity.spf;
	data['inviter'] = entity.inviter;
	data['statusId'] = entity.statusId;
	data['roomId'] = entity.roomId;
	return data;
}