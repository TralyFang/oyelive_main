import 'package:oyelive_main/module/entity/vip_ybd_valid_info_entity.dart';

yBDVipValidInfoEntityFromJson(YBDVipValidInfoEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDVipValidInfoRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDVipValidInfoEntityToJson(YBDVipValidInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDVipValidInfoRecordFromJson(YBDVipValidInfoRecord data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['itemList'] != null) {
		data.itemList = (json['itemList'] as List).map((v) => YBDVipValidInfoRecordItemList().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDVipValidInfoRecordToJson(YBDVipValidInfoRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['itemList'] =  entity.itemList?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDVipValidInfoRecordItemListFromJson(YBDVipValidInfoRecordItemList data, Map<String, dynamic> json) {
	if (json['number'] != null) {
		data.number = json['number'] is String
				? int.tryParse(json['number'])
				: json['number'].toInt();
	}
	if (json['personalId'] != null) {
		data.personalId = json['personalId'] is String
				? int.tryParse(json['personalId'])
				: json['personalId'].toInt();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'] is String
				? int.tryParse(json['name'])
				: json['name'].toInt();
	}
	if (json['expireAfter'] != null) {
		data.expireAfter = json['expireAfter'] is String
				? int.tryParse(json['expireAfter'])
				: json['expireAfter'].toInt();
	}
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['vipName'] != null) {
		data.vipName = json['vipName'].toString();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['vipIcon'] != null) {
		data.vipIcon = json['vipIcon'].toString();
	}
	return data;
}

Map<String, dynamic> yBDVipValidInfoRecordItemListToJson(YBDVipValidInfoRecordItemList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['number'] = entity.number;
	data['personalId'] = entity.personalId;
	data['price'] = entity.price;
	data['name'] = entity.name;
	data['expireAfter'] = entity.expireAfter;
	data['id'] = entity.id;
	data['vipName'] = entity.vipName;
	data['index'] = entity.index;
	data['vipIcon'] = entity.vipIcon;
	return data;
}