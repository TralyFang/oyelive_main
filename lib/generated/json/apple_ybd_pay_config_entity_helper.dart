import 'package:oyelive_main/common/service/iap_service/entity/apple_ybd_pay_config_entity.dart';

yBDApplePayConfigEntityFromJson(YBDApplePayConfigEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = (json['data'] as List).map((v) => YBDApplePayConfigData().fromJson(v)).toList();
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDApplePayConfigEntityToJson(YBDApplePayConfigEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v?.toJson())?.toList();
	data['success'] = entity.success;
	return data;
}

yBDApplePayConfigDataFromJson(YBDApplePayConfigData data, Map<String, dynamic> json) {
	if (json['nickName'] != null) {
		data.nickName = json['nickName'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['items'] != null) {
		data.items = (json['items'] as List).map((v) => YBDApplePayConfigDataItem().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDApplePayConfigDataToJson(YBDApplePayConfigData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['nickName'] = entity.nickName;
	data['name'] = entity.name;
	data['icon'] = entity.icon;
	data['items'] =  entity.items?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDApplePayConfigDataItemFromJson(YBDApplePayConfigDataItem data, Map<String, dynamic> json) {
	if (json['money'] != null) {
		data.money = json['money'] is String
				? double.tryParse(json['money'])
				: json['money'].toDouble();
	}
	if (json['productId'] != null) {
		data.productId = json['productId'].toString();
	}
	if (json['value'] != null) {
		data.value = json['value'] is String
				? int.tryParse(json['value'])
				: json['value'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDApplePayConfigDataItemToJson(YBDApplePayConfigDataItem entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['money'] = entity.money;
	data['productId'] = entity.productId;
	data['value'] = entity.value;
	return data;
}