import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';

yBDUserCertificationEntityFromJson(YBDUserCertificationEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDUserCertificationRecord().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDUserCertificationEntityToJson(YBDUserCertificationEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDUserCertificationRecordFromJson(YBDUserCertificationRecord data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['certificationInfos'] != null) {
		data.certificationInfos = (json['certificationInfos'] as List).map((v) => YBDUserCertificationRecordCertificationInfos().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDUserCertificationRecordToJson(YBDUserCertificationRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['certificationInfos'] =  entity.certificationInfos?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDUserCertificationRecordCertificationInfosFromJson(YBDUserCertificationRecordCertificationInfos data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['userName'] != null) {
		data.userName = json['userName'];
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['hidUserLevel'] != null) {
		data.hidUserLevel = json['hidUserLevel'] is String
				? int.tryParse(json['hidUserLevel'])
				: json['hidUserLevel'].toInt();
	}
	if (json['hidTalentLevel'] != null) {
		data.hidTalentLevel = json['hidTalentLevel'] is String
				? int.tryParse(json['hidTalentLevel'])
				: json['hidTalentLevel'].toInt();
	}
	if (json['hidVip'] != null) {
		data.hidVip = json['hidVip'] is String
				? int.tryParse(json['hidVip'])
				: json['hidVip'].toInt();
	}
	if (json['hidBadge'] != null) {
		data.hidBadge = json['hidBadge'] is String
				? int.tryParse(json['hidBadge'])
				: json['hidBadge'].toInt();
	}
	if (json['hidGender'] != null) {
		data.hidGender = json['hidGender'] is String
				? int.tryParse(json['hidGender'])
				: json['hidGender'].toInt();
	}
	if (json['hidTalent'] != null) {
		data.hidTalent = json['hidTalent'] is String
				? int.tryParse(json['hidTalent'])
				: json['hidTalent'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'];
	}
	if (json['updateTime'] != null) {
		data.updateTime = json['updateTime'];
	}
	if (json['operator'] != null) {
		data.xOperator = json['operator'];
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['extend'] != null) {
		data.extend = json['extend'];
	}
	return data;
}

Map<String, dynamic> yBDUserCertificationRecordCertificationInfosToJson(YBDUserCertificationRecordCertificationInfos entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['userId'] = entity.userId;
	data['userName'] = entity.userName;
	data['type'] = entity.type;
	data['icon'] = entity.icon;
	data['title'] = entity.title;
	data['hidUserLevel'] = entity.hidUserLevel;
	data['hidTalentLevel'] = entity.hidTalentLevel;
	data['hidVip'] = entity.hidVip;
	data['hidBadge'] = entity.hidBadge;
	data['hidGender'] = entity.hidGender;
	data['hidTalent'] = entity.hidTalent;
	data['createTime'] = entity.createTime;
	data['updateTime'] = entity.updateTime;
	data['operator'] = entity.xOperator;
	data['status'] = entity.status;
	data['extend'] = entity.extend;
	return data;
}