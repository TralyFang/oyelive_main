import 'package:oyelive_main/module/entity/game_ybd_info_entity.dart';

yBDGameInfoEntityFromJson(YBDGameInfoEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDGameInfoRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameInfoEntityToJson(YBDGameInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDGameInfoRecordFromJson(YBDGameInfoRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['url'] != null) {
		data.url = json['url'].toString();
	}
	if (json['shieldStatus'] != null) {
		data.shieldStatus = json['shieldStatus'] is String
				? int.tryParse(json['shieldStatus'])
				: json['shieldStatus'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGameInfoRecordToJson(YBDGameInfoRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	data['shieldStatus'] = entity.shieldStatus;
	return data;
}