import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_state.dart';
import 'dart:async';
import 'package:get/get.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_config_display_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gift_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gifting_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_status_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/room_ybd_status_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/zego_ybd_code_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';

yBDGameWebLudoConfigFromJson(YBDGameWebLudoConfig data, Map<String, dynamic> json) {
	if (json['gamePlaying'] != null) {
		data.gamePlaying = json['gamePlaying'];
	}
	if (json['battleId'] != null) {
		data.battleId = json['battleId'] is String
				? int.tryParse(json['battleId'])
				: json['battleId'].toInt();
	}
	if (json['battleMode'] != null) {
		data.battleMode = json['battleMode'].toString();
	}
	if (json['chargeCurrency'] != null) {
		data.chargeCurrency = json['chargeCurrency'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameWebLudoConfigToJson(YBDGameWebLudoConfig entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['gamePlaying'] = entity.gamePlaying;
	data['battleId'] = entity.battleId;
	data['battleMode'] = entity.battleMode;
	data['chargeCurrency'] = entity.chargeCurrency;
	return data;
}