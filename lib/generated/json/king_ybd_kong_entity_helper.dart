import 'package:oyelive_main/ui/page/home/entity/king_ybd_kong_entity.dart';
import 'dart:async';

yBDKingKongEntityFromJson(YBDKingKongEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDKingKongRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'];
	}
	return data;
}

Map<String, dynamic> yBDKingKongEntityToJson(YBDKingKongEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDKingKongRecordFromJson(YBDKingKongRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['route'] != null) {
		data.route = json['route'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['rank'] != null) {
		data.rank = json['rank'] is String
				? int.tryParse(json['rank'])
				: json['rank'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDKingKongRecordToJson(YBDKingKongRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['route'] = entity.route;
	data['name'] = entity.name;
	data['img'] = entity.img;
	data['rank'] = entity.rank;
	return data;
}