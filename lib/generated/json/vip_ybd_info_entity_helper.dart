import 'package:oyelive_main/module/entity/vip_ybd_info_entity.dart';

yBDVipInfoEntityFromJson(YBDVipInfoEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDVipInfoRecord().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDVipInfoEntityToJson(YBDVipInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDVipInfoRecordFromJson(YBDVipInfoRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['rights'] != null) {
		data.rights = json['rights'] is String
				? int.tryParse(json['rights'])
				: json['rights'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['modify'] != null) {
		data.modify = json['modify'];
	}
	return data;
}

Map<String, dynamic> yBDVipInfoRecordToJson(YBDVipInfoRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['status'] = entity.status;
	data['level'] = entity.level;
	data['price'] = entity.price;
	data['rights'] = entity.rights;
	data['name'] = entity.name;
	data['modify'] = entity.modify;
	return data;
}