import 'package:oyelive_main/common/room_socket/message/common/gift_ybd_command_message.dart';

yBDGiftCommandMessageFromJson(YBDGiftCommandMessage data, Map<String, dynamic> json) {
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['num'] != null) {
		data.num = json['num'] is String
				? int.tryParse(json['num'])
				: json['num'].toInt();
	}
	if (json['imgUrl'] != null) {
		data.imgUrl = json['imgUrl'].toString();
	}
	if (json['animationUrl'] != null) {
		data.animationUrl = json['animationUrl'].toString();
	}
	if (json['combo'] != null) {
		data.combo = json['combo'] is String
				? int.tryParse(json['combo'])
				: json['combo'].toInt();
	}
	if (json['code'] != null) {
		data.code = json['code'] is String
				? int.tryParse(json['code'])
				: json['code'].toInt();
	}
	if (json['personalId'] != null) {
		data.personalId = json['personalId'] is String
				? int.tryParse(json['personalId'])
				: json['personalId'].toInt();
	}
	if (json['pkId'] != null) {
		data.pkId = json['pkId'] is String
				? int.tryParse(json['pkId'])
				: json['pkId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGiftCommandMessageToJson(YBDGiftCommandMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['name'] = entity.name;
	data['num'] = entity.num;
	data['imgUrl'] = entity.imgUrl;
	data['animationUrl'] = entity.animationUrl;
	data['combo'] = entity.combo;
	data['code'] = entity.code;
	data['personalId'] = entity.personalId;
	data['pkId'] = entity.pkId;
	return data;
}