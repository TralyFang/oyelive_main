import 'package:oyelive_main/ui/page/room/play_center/entity/game_ybd_check_entity.dart';
import 'dart:async';
import 'package:oyelive_main/common/room_socket/message/common/ludo_ybd_status_message_entity.dart';

yBDGameResponseBaseFromJson(YBDGameResponseBase data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDLudoStatusMessageContent().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDGameResponseBaseToJson(YBDGameResponseBase entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDGameResponseResultFromJson(YBDGameResponseResult data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = json['record'];
	}
	return data;
}

Map<String, dynamic> yBDGameResponseResultToJson(YBDGameResponseResult entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record;
	return data;
}