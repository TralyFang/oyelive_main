import 'package:oyelive_main/module/payment/entity/pay_ybd_config_entity.dart';

yBDPayConfigEntityFromJson(YBDPayConfigEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = (json['data'] as List).map((v) => YBDPayConfigData().fromJson(v)).toList();
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDPayConfigEntityToJson(YBDPayConfigEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v?.toJson())?.toList();
	data['success'] = entity.success;
	return data;
}

yBDPayConfigDataFromJson(YBDPayConfigData data, Map<String, dynamic> json) {
	if (json['payload'] != null) {
		data.payload = YBDPayConfigDataPayload().fromJson(json['payload']);
	}
	if (json['nickName'] != null) {
		data.nickName = json['nickName'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['order'] != null) {
		data.order = json['order'] is String
				? int.tryParse(json['order'])
				: json['order'].toInt();
	}
	if (json['child'] != null) {
		data.child = (json['child'] as List).map((v) => YBDPayConfigDataChild().fromJson(v)).toList();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['items'] != null) {
		data.items = (json['items'] as List).map((v) => YBDPayConfigDataItem().fromJson(v)).toList();
	}
	if (json['rate'] != null) {
		data.rate = json['rate'] is String
				? double.tryParse(json['rate'])
				: json['rate'].toDouble();
	}
	if (json['suffixIcon'] != null) {
		data.suffixIcon = json['suffixIcon'].toString();
	}
	return data;
}

Map<String, dynamic> yBDPayConfigDataToJson(YBDPayConfigData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['payload'] = entity.payload?.toJson();
	data['nickName'] = entity.nickName;
	data['name'] = entity.name;
	data['icon'] = entity.icon;
	data['order'] = entity.order;
	data['child'] =  entity.child?.map((v) => v?.toJson())?.toList();
	data['currency'] = entity.currency;
	data['items'] =  entity.items?.map((v) => v?.toJson())?.toList();
	data['rate'] = entity.rate;
	data['suffixIcon'] = entity.suffixIcon;
	return data;
}

yBDPayConfigDataPayloadFromJson(YBDPayConfigDataPayload data, Map<String, dynamic> json) {
	if (json['regex'] != null) {
		data.regex = json['regex'].toString();
	}
	if (json['desc'] != null) {
		data.desc = json['desc'];
	}
	if (json['action'] != null) {
		data.action = json['action'].toString();
	}
	return data;
}

Map<String, dynamic> yBDPayConfigDataPayloadToJson(YBDPayConfigDataPayload entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['regex'] = entity.regex;
	data['desc'] = entity.desc;
	data['action'] = entity.action;
	return data;
}

yBDPayConfigDataChildFromJson(YBDPayConfigDataChild data, Map<String, dynamic> json) {
	if (json['rate'] != null) {
		data.rate = json['rate'] is String
				? double.tryParse(json['rate'])
				: json['rate'].toDouble();
	}
	if (json['payload'] != null) {
		data.payload = YBDPayConfigDataPayload().fromJson(json['payload']);
	}
	if (json['nickName'] != null) {
		data.nickName = json['nickName'].toString();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['items'] != null) {
		data.items = (json['items'] as List).map((v) => YBDPayConfigDataItem().fromJson(v)).toList();
	}
	if (json['order'] != null) {
		data.order = json['order'] is String
				? int.tryParse(json['order'])
				: json['order'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPayConfigDataChildToJson(YBDPayConfigDataChild entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rate'] = entity.rate;
	data['payload'] = entity.payload?.toJson();
	data['nickName'] = entity.nickName;
	data['name'] = entity.name;
	data['icon'] = entity.icon;
	data['currency'] = entity.currency;
	data['items'] =  entity.items?.map((v) => v?.toJson())?.toList();
	data['order'] = entity.order;
	return data;
}

yBDPayConfigDataItemFromJson(YBDPayConfigDataItem data, Map<String, dynamic> json) {
	if (json['money'] != null) {
		data.money = json['money'] is String
				? double.tryParse(json['money'])
				: json['money'].toDouble();
	}
	if (json['productId'] != null) {
		data.productId = json['productId'].toString();
	}
	if (json['value'] != null) {
		data.value = json['value'] is String
				? int.tryParse(json['value'])
				: json['value'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPayConfigDataItemToJson(YBDPayConfigDataItem entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['money'] = entity.money;
	data['productId'] = entity.productId;
	data['value'] = entity.value;
	return data;
}