import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gift_entity.dart';
import 'dart:async';

yBDGameGiftEntityFromJson(YBDGameGiftEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = (json['data'] as List).map((v) => YBDGameGiftData().fromJson(v)).toList();
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDGameGiftEntityToJson(YBDGameGiftEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v?.toJson())?.toList();
	data['success'] = entity.success;
	return data;
}

yBDGameGiftDataFromJson(YBDGameGiftData data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['label'] != null) {
		data.label = json['label'] is String
				? int.tryParse(json['label'])
				: json['label'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['currencyImage'] != null) {
		data.currencyImage = json['currencyImage'].toString();
	}
	if (json['count'] != null) {
		data.count = json['count'] is String
				? int.tryParse(json['count'])
				: json['count'].toInt();
	}
	if (json['earn'] != null) {
		data.earn = json['earn'] is String
				? int.tryParse(json['earn'])
				: json['earn'].toInt();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['display'] != null) {
		data.display = json['display'] is String
				? int.tryParse(json['display'])
				: json['display'].toInt();
	}
	if (json['attribute'] != null) {
		data.attribute = json['attribute'];
	}
	if (json['animation'] != null) {
		data.animation = YBDGameGiftDataAnimation().fromJson(json['animation']);
	}
	if (json['animationFile'] != null) {
		data.animationFile = json['animationFile'].toString();
	}
	if (json['animationDisplay'] != null) {
		data.animationDisplay = json['animationDisplay'] is String
				? int.tryParse(json['animationDisplay'])
				: json['animationDisplay'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGameGiftDataToJson(YBDGameGiftData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['status'] = entity.status;
	data['label'] = entity.label;
	data['index'] = entity.index;
	data['price'] = entity.price;
	data['currency'] = entity.currency;
	data['currencyImage'] = entity.currencyImage;
	data['count'] = entity.count;
	data['earn'] = entity.earn;
	data['img'] = entity.img;
	data['display'] = entity.display;
	data['attribute'] = entity.attribute;
	data['animation'] = entity.animation?.toJson();
	data['animationFile'] = entity.animationFile;
	data['animationDisplay'] = entity.animationDisplay;
	return data;
}

yBDGameGiftDataAnimationFromJson(YBDGameGiftDataAnimation data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameGiftDataAnimationToJson(YBDGameGiftDataAnimation entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	return data;
}