import 'package:oyelive_main/module/room/entity/gift_ybd_package_entity.dart';

yBDGiftPackageEntityFromJson(YBDGiftPackageEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDGiftPackageRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGiftPackageEntityToJson(YBDGiftPackageEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDGiftPackageRecordFromJson(YBDGiftPackageRecord data, Map<String, dynamic> json) {
	if (json['gift'] != null) {
		data.gift = (json['gift'] as List).map((v) => YBDGiftPackageRecordGift().fromJson(v)).toList();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGiftPackageRecordToJson(YBDGiftPackageRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['gift'] =  entity.gift?.map((v) => v?.toJson())?.toList();
	data['index'] = entity.index;
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

yBDGiftPackageRecordGiftFromJson(YBDGiftPackageRecordGift data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['number'] != null) {
		data.number = json['number'] is String
				? int.tryParse(json['number'])
				: json['number'].toInt();
	}
	if (json['personalId'] != null) {
		data.personalId = json['personalId'] is String
				? int.tryParse(json['personalId'])
				: json['personalId'].toInt();
	}
	if (json['conditionType'] != null) {
		data.conditionType = json['conditionType'] is String
				? int.tryParse(json['conditionType'])
				: json['conditionType'].toInt();
	}
	if (json['conditionExtends'] != null) {
		data.conditionExtends = json['conditionExtends'].toString();
	}
	if (json['label'] != null) {
		data.label = json['label'] is String
				? int.tryParse(json['label'])
				: json['label'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['earn'] != null) {
		data.earn = json['earn'] is String
				? int.tryParse(json['earn'])
				: json['earn'].toInt();
	}
	if (json['agencyearn'] != null) {
		data.agencyearn = json['agencyearn'] is String
				? int.tryParse(json['agencyearn'])
				: json['agencyearn'].toInt();
	}
	if (json['expireAfter'] != null) {
		data.expireAfter = json['expireAfter'] is String
				? int.tryParse(json['expireAfter'])
				: json['expireAfter'].toInt();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['animation'] != null) {
		data.animation = json['animation'];
	}
	if (json['display'] != null) {
		data.display = json['display'] is String
				? int.tryParse(json['display'])
				: json['display'].toInt();
	}
	if (json['attribute'] != null) {
		data.attribute = json['attribute'].toString();
	}
	if (json['channel'] != null) {
		data.channel = json['channel'].toString();
	}
	if (json['animationDisplay'] != null) {
		data.animationDisplay = json['animationDisplay'] is String
				? int.tryParse(json['animationDisplay'])
				: json['animationDisplay'].toInt();
	}
	if (json['animationFile'] != null) {
		data.animationFile = json['animationFile'].toString();
	}
	if (json['animationInfo'] != null) {
		data.animationInfo = YBDGiftPackageRecordGiftAnimationInfo().fromJson(json['animationInfo']);
	}
	return data;
}

Map<String, dynamic> yBDGiftPackageRecordGiftToJson(YBDGiftPackageRecordGift entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['status'] = entity.status;
	data['number'] = entity.number;
	data['personalId'] = entity.personalId;
	data['conditionType'] = entity.conditionType;
	data['conditionExtends'] = entity.conditionExtends;
	data['label'] = entity.label;
	data['index'] = entity.index;
	data['price'] = entity.price;
	data['earn'] = entity.earn;
	data['agencyearn'] = entity.agencyearn;
	data['expireAfter'] = entity.expireAfter;
	data['img'] = entity.img;
	data['animation'] = entity.animation;
	data['display'] = entity.display;
	data['attribute'] = entity.attribute;
	data['channel'] = entity.channel;
	data['animationDisplay'] = entity.animationDisplay;
	data['animationFile'] = entity.animationFile;
	data['animationInfo'] = entity.animationInfo?.toJson();
	return data;
}

yBDGiftPackageRecordGiftAnimationInfoFromJson(YBDGiftPackageRecordGiftAnimationInfo data, Map<String, dynamic> json) {
	if (json['animationId'] != null) {
		data.animationId = json['animationId'] is String
				? int.tryParse(json['animationId'])
				: json['animationId'].toInt();
	}
	if (json['animationType'] != null) {
		data.animationType = json['animationType'] is String
				? int.tryParse(json['animationType'])
				: json['animationType'].toInt();
	}
	if (json['foldername'] != null) {
		data.foldername = json['foldername'].toString();
	}
	if (json['framDuration'] != null) {
		data.framDuration = json['framDuration'] is String
				? int.tryParse(json['framDuration'])
				: json['framDuration'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGiftPackageRecordGiftAnimationInfoToJson(YBDGiftPackageRecordGiftAnimationInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['animationId'] = entity.animationId;
	data['animationType'] = entity.animationType;
	data['foldername'] = entity.foldername;
	data['framDuration'] = entity.framDuration;
	data['name'] = entity.name;
	data['version'] = entity.version;
	return data;
}