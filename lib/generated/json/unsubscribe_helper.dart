import 'package:oyelive_main/common/room_socket/message/common/unsubscribe.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';

yBDUnsubscribeFromJson(YBDUnsubscribe data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['desc'] != null) {
		data.desc = json['desc'].toString();
	}
	if (json['detail'] != null) {
		data.detail = json['detail'].toString();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	return data;
}

Map<String, dynamic> yBDUnsubscribeToJson(YBDUnsubscribe entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['desc'] = entity.desc;
	data['detail'] = entity.detail;
	data['msgType'] = entity.msgType;
	data['roomId'] = entity.roomId;
	data['fromUser'] = entity.fromUser;
	data['toUser'] = entity.toUser;
	data['mode'] = entity.mode;
	data['destination'] = entity.destination;
	return data;
}