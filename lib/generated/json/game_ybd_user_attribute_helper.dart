import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_user_attribute.dart';
import 'dart:async';

yBDGameUsersAttributeFromJson(YBDGameUsersAttribute data, Map<String, dynamic> json) {
	if (json['gender'] != null) {
		data.gender = json['gender'].toString();
	}
	if (json['nickName'] != null) {
		data.nickName = json['nickName'].toString();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar'].toString();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['roomLevel'] != null) {
		data.roomLevel = json['roomLevel'] is String
				? int.tryParse(json['roomLevel'])
				: json['roomLevel'].toInt();
	}
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameUsersAttributeToJson(YBDGameUsersAttribute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['gender'] = entity.gender;
	data['nickName'] = entity.nickName;
	data['avatar'] = entity.avatar;
	data['level'] = entity.level;
	data['roomLevel'] = entity.roomLevel;
	data['id'] = entity.id;
	return data;
}