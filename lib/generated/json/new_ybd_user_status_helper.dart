import 'package:oyelive_main/ui/page/home/entity/new_ybd_user_status.dart';
import 'dart:async';

yBDNewUserStatusEntityFromJson(YBDNewUserStatusEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDNewUserStatusRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDNewUserStatusEntityToJson(YBDNewUserStatusEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDNewUserStatusRecordFromJson(YBDNewUserStatusRecord data, Map<String, dynamic> json) {
	if (json['enabled'] != null) {
		data.enabled = json['enabled'];
	}
	if (json['route'] != null) {
		data.route = json['route'].toString();
	}
	if (json['resourceUrl'] != null) {
		data.resourceUrl = json['resourceUrl'].toString();
	}
	if (json['remainingTime'] != null) {
		data.remainingTime = json['remainingTime'] is String
				? int.tryParse(json['remainingTime'])
				: json['remainingTime'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDNewUserStatusRecordToJson(YBDNewUserStatusRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['enabled'] = entity.enabled;
	data['route'] = entity.route;
	data['resourceUrl'] = entity.resourceUrl;
	data['remainingTime'] = entity.remainingTime;
	return data;
}