import 'package:oyelive_main/module/user/entity/mobile_ybd_check_resp_entity.dart';

yBDMobileCheckRespFromJson(YBDMobileCheckResp data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDMobileCheckRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDMobileCheckRespToJson(YBDMobileCheckResp entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDMobileCheckRecordFromJson(YBDMobileCheckRecord data, Map<String, dynamic> json) {
	if (json['tokenId'] != null) {
		data.tokenId = json['tokenId'] is String
				? int.tryParse(json['tokenId'])
				: json['tokenId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDMobileCheckRecordToJson(YBDMobileCheckRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['tokenId'] = entity.tokenId;
	return data;
}