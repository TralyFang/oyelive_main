import 'package:oyelive_main/ui/page/activty/entity/activity_ybd_skin_entity.dart';
import 'dart:async';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

yBDActivitySkinEntityFromJson(YBDActivitySkinEntity data, Map<String, dynamic> json) {
	if (json['folderName'] != null) {
		data.folderName = json['folderName'].toString();
	}
	if (json['zipName'] != null) {
		data.zipName = json['zipName'].toString();
	}
	if (json['zipUrl'] != null) {
		data.zipUrl = json['zipUrl'].toString();
	}
	if (json['beginTime'] != null) {
		data.beginTime = json['beginTime'].toString();
	}
	if (json['endTime'] != null) {
		data.endTime = json['endTime'].toString();
	}
	if (json['template'] != null) {
		data.template = json['template'].toString();
	}
	if (json['textColor'] != null) {
		data.textColor = json['textColor'].toString();
	}
	if (json['homeBgColor'] != null) {
		data.homeBgColor = json['homeBgColor'].toString();
	}
	if (json['myprofileIconListBgColor'] != null) {
		data.myprofileIconListBgColor = json['myprofileIconListBgColor'].toString();
	}
	if (json['myprofileRowColor'] != null) {
		data.myprofileRowColor = json['myprofileRowColor'].toString();
	}
	if (json['myprofileSlogBgColor'] != null) {
		data.myprofileSlogBgColor = json['myprofileSlogBgColor'].toString();
	}
	if (json['roomListItemTextColor'] != null) {
		data.roomListItemTextColor = json['roomListItemTextColor'].toString();
	}
	if (json['popularNavTitleColor'] != null) {
		data.popularNavTitleColor = json['popularNavTitleColor'].toString();
	}
	if (json['activitySeparatorColor'] != null) {
		data.activitySeparatorColor = json['activitySeparatorColor'].toString();
	}
	if (json['exploreRecordBgColor'] != null) {
		data.exploreRecordBgColor = json['exploreRecordBgColor'].toString();
	}
	if (json['userProfileColors'] != null) {
		data.userProfileColors = json['userProfileColors'].toString();
	}
	if (json['userProfileFontColor'] != null) {
		data.userProfileFontColor = json['userProfileFontColor'].toString();
	}
	if (json['userProfileNameColor'] != null) {
		data.userProfileNameColor = json['userProfileNameColor'].toString();
	}
	if (json['userProfileIDColor'] != null) {
		data.userProfileIDColor = json['userProfileIDColor'].toString();
	}
	if (json['indexBgColor'] != null) {
		data.indexBgColor = json['indexBgColor'].toString();
	}
	if (json['borderW'] != null) {
		data.borderW = json['borderW'].toString();
	}
	return data;
}

Map<String, dynamic> yBDActivitySkinEntityToJson(YBDActivitySkinEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['folderName'] = entity.folderName;
	data['zipName'] = entity.zipName;
	data['zipUrl'] = entity.zipUrl;
	data['beginTime'] = entity.beginTime;
	data['endTime'] = entity.endTime;
	data['template'] = entity.template;
	data['textColor'] = entity.textColor;
	data['homeBgColor'] = entity.homeBgColor;
	data['myprofileIconListBgColor'] = entity.myprofileIconListBgColor;
	data['myprofileRowColor'] = entity.myprofileRowColor;
	data['myprofileSlogBgColor'] = entity.myprofileSlogBgColor;
	data['roomListItemTextColor'] = entity.roomListItemTextColor;
	data['popularNavTitleColor'] = entity.popularNavTitleColor;
	data['activitySeparatorColor'] = entity.activitySeparatorColor;
	data['exploreRecordBgColor'] = entity.exploreRecordBgColor;
	data['userProfileColors'] = entity.userProfileColors;
	data['userProfileFontColor'] = entity.userProfileFontColor;
	data['userProfileNameColor'] = entity.userProfileNameColor;
	data['userProfileIDColor'] = entity.userProfileIDColor;
	data['indexBgColor'] = entity.indexBgColor;
	data['borderW'] = entity.borderW;
	return data;
}