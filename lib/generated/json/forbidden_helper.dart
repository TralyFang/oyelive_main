import 'package:oyelive_main/common/room_socket/message/common/forbidden.dart';

yBDForbiddenFromJson(YBDForbidden data, Map<String, dynamic> json) {
	if (json['operType'] != null) {
		data.operType = json['operType'] is String
				? int.tryParse(json['operType'])
				: json['operType'].toInt();
	}
	if (json['period'] != null) {
		data.period = json['period'] is String
				? int.tryParse(json['period'])
				: json['period'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDForbiddenToJson(YBDForbidden entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['operType'] = entity.operType;
	data['period'] = entity.period;
	return data;
}