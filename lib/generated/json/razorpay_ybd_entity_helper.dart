import 'package:oyelive_main/ui/page/recharge/razorpay/entity/razorpay_ybd_entity.dart';
import 'dart:async';

yBDRazorpayResultEntityFromJson(YBDRazorpayResultEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDRazorpayEntity().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDRazorpayResultEntityToJson(YBDRazorpayResultEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['message'] = entity.message;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDRazorpayEntityFromJson(YBDRazorpayEntity data, Map<String, dynamic> json) {
	if (json['amount'] != null) {
		data.amount = json['amount'] is String
				? int.tryParse(json['amount'])
				: json['amount'].toInt();
	}
	if (json['amount_paid'] != null) {
		data.amountPaid = json['amount_paid'] is String
				? int.tryParse(json['amount_paid'])
				: json['amount_paid'].toInt();
	}
	if (json['notes'] != null) {
		data.notes = (json['notes'] as List).map((v) => v).toList().cast<dynamic>();
	}
	if (json['created_at'] != null) {
		data.createdAt = json['created_at'] is String
				? int.tryParse(json['created_at'])
				: json['created_at'].toInt();
	}
	if (json['amount_due'] != null) {
		data.amountDue = json['amount_due'] is String
				? int.tryParse(json['amount_due'])
				: json['amount_due'].toInt();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['receipt'] != null) {
		data.receipt = json['receipt'].toString();
	}
	if (json['id'] != null) {
		data.id = json['id'].toString();
	}
	if (json['entity'] != null) {
		data.entity = json['entity'].toString();
	}
	if (json['offer_id'] != null) {
		data.offerId = json['offer_id'];
	}
	if (json['attempts'] != null) {
		data.attempts = json['attempts'] is String
				? int.tryParse(json['attempts'])
				: json['attempts'].toInt();
	}
	if (json['tradeNo'] != null) {
		data.tradeNo = json['tradeNo'].toString();
	}
	if (json['orderId'] != null) {
		data.orderId = json['orderId'].toString();
	}
	if (json['requestUrl'] != null) {
		data.requestUrl = json['requestUrl'].toString();
	}
	if (json['sign'] != null) {
		data.sign = json['sign'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRazorpayEntityToJson(YBDRazorpayEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['amount'] = entity.amount;
	data['amount_paid'] = entity.amountPaid;
	data['notes'] = entity.notes;
	data['created_at'] = entity.createdAt;
	data['amount_due'] = entity.amountDue;
	data['currency'] = entity.currency;
	data['receipt'] = entity.receipt;
	data['id'] = entity.id;
	data['entity'] = entity.entity;
	data['offer_id'] = entity.offerId;
	data['attempts'] = entity.attempts;
	data['tradeNo'] = entity.tradeNo;
	data['orderId'] = entity.orderId;
	data['requestUrl'] = entity.requestUrl;
	data['sign'] = entity.sign;
	data['status'] = entity.status;
	return data;
}