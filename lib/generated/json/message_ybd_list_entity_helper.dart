import 'package:oyelive_main/module/inbox/entity/message_ybd_list_entity.dart';
import 'dart:convert';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/inbox/entity/conversation_ybd_list_entity.dart';

yBDMessageListEntityFromJson(YBDMessageListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDMessageListData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDMessageListEntityToJson(YBDMessageListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDMessageListDataFromJson(YBDMessageListData data, Map<String, dynamic> json) {
	if (json['pullMessages'] != null) {
		data.pullMessages = YBDPullMessageListData().fromJson(json['pullMessages']);
	}
	return data;
}

Map<String, dynamic> yBDMessageListDataToJson(YBDMessageListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['pullMessages'] = entity.pullMessages?.toJson();
	return data;
}

yBDPullMessageListDataFromJson(YBDPullMessageListData data, Map<String, dynamic> json) {
	if (json['messages'] != null) {
		data.messages = (json['messages'] as List).map((v) => YBDMessageListDataPullMessage().fromJson(v)).toList();
	}
	if (json['end'] != null) {
		data.end = json['end'];
	}
	return data;
}

Map<String, dynamic> yBDPullMessageListDataToJson(YBDPullMessageListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['messages'] =  entity.messages?.map((v) => v?.toJson())?.toList();
	data['end'] = entity.end;
	return data;
}

yBDMessageListDataPullMessageFromJson(YBDMessageListDataPullMessage data, Map<String, dynamic> json) {
	if (json['messageId'] != null) {
		data.messageId = json['messageId'].toString();
	}
	if (json['contentType'] != null) {
		data.contentType = json['contentType'].toString();
	}
	if (json['content'] != null) {
		data.content = YBDMessageListDataPullMessagesContent().fromJson(json['content']);
	}
	if (json['attributes'] != null) {
		data.attributes = json['attributes'];
	}
	if (json['sender'] != null) {
		data.sender = YBDMessageListDataPullMessagesSender().fromJson(json['sender']);
	}
	if (json['receivers'] != null) {
		data.receivers = (json['receivers'] as List).map((v) => YBDMessageListDataPullMessagesReceiver().fromJson(v)).toList();
	}
	if (json['sequenceId'] != null) {
		data.sequenceId = json['sequenceId'] is String
				? int.tryParse(json['sequenceId'])
				: json['sequenceId'].toInt();
	}
	if (json['queueId'] != null) {
		data.queueId = json['queueId'].toString();
	}
	if (json['sendTime'] != null) {
		data.sendTime = json['sendTime'] is String
				? int.tryParse(json['sendTime'])
				: json['sendTime'].toInt();
	}
	if (json['category'] != null) {
		data.category = json['category'].toString();
	}
	if (json['requestId'] != null) {
		data.requestId = json['requestId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDMessageListDataPullMessageToJson(YBDMessageListDataPullMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['messageId'] = entity.messageId;
	data['contentType'] = entity.contentType;
	data['content'] = entity.content?.toJson();
	data['attributes'] = entity.attributes;
	data['sender'] = entity.sender?.toJson();
	data['receivers'] =  entity.receivers?.map((v) => v?.toJson())?.toList();
	data['sequenceId'] = entity.sequenceId;
	data['queueId'] = entity.queueId;
	data['sendTime'] = entity.sendTime;
	data['category'] = entity.category;
	data['requestId'] = entity.requestId;
	return data;
}

yBDMessageListDataPullMessagesContentFromJson(YBDMessageListDataPullMessagesContent data, Map<String, dynamic> json) {
	if (json['text'] != null) {
		data.text = json['text'].toString();
	}
	if (json['queueId'] != null) {
		data.queueId = json['queueId'].toString();
	}
	if (json['principal'] != null) {
		data.principal = YBDConversationListDataPrincipal().fromJson(json['principal']);
	}
	if (json['category'] != null) {
		data.category = json['category'].toString();
	}
	if (json['disturb'] != null) {
		data.disturb = json['disturb'];
	}
	if (json['blockList'] != null) {
		data.blockList = json['blockList'];
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['subtitle'] != null) {
		data.subtitle = json['subtitle'].toString();
	}
	if (json['resource'] != null) {
		data.resource = json['resource'].toString();
	}
	if (json['comment'] != null) {
		data.comment = json['comment'].toString();
	}
	if (json['quoteMessage'] != null) {
		data.quoteMessage = YBDMessageQuoteMessage().fromJson(json['quoteMessage']);
	}
	if (json['attributes'] != null) {
		data.attributes = json['attributes'];
	}
	if (json['click'] != null) {
		data.click = json['click'];
	}
	if (json['layout'] != null) {
		data.layout = json['layout'].toString();
	}
	if (json['extend'] != null) {
		data.extend = json['extend'];
	}
	return data;
}

Map<String, dynamic> yBDMessageListDataPullMessagesContentToJson(YBDMessageListDataPullMessagesContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['text'] = entity.text;
	data['queueId'] = entity.queueId;
	data['principal'] = entity.principal?.toJson();
	data['category'] = entity.category;
	data['disturb'] = entity.disturb;
	data['blockList'] = entity.blockList;
	data['icon'] = entity.icon;
	data['title'] = entity.title;
	data['subtitle'] = entity.subtitle;
	data['resource'] = entity.resource;
	data['comment'] = entity.comment;
	data['quoteMessage'] = entity.quoteMessage?.toJson();
	data['attributes'] = entity.attributes;
	data['click'] = entity.click;
	data['layout'] = entity.layout;
	data['extend'] = entity.extend;
	return data;
}

yBDMessageListDataPullMessagesSenderFromJson(YBDMessageListDataPullMessagesSender data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	return data;
}

Map<String, dynamic> yBDMessageListDataPullMessagesSenderToJson(YBDMessageListDataPullMessagesSender entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['nickname'] = entity.nickname;
	return data;
}

yBDMessageListDataPullMessagesReceiverFromJson(YBDMessageListDataPullMessagesReceiver data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDMessageListDataPullMessagesReceiverToJson(YBDMessageListDataPullMessagesReceiver entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	return data;
}

yBDMessageQuoteMessageFromJson(YBDMessageQuoteMessage data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['text'] != null) {
		data.text = json['text'].toString();
	}
	return data;
}

Map<String, dynamic> yBDMessageQuoteMessageToJson(YBDMessageQuoteMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['text'] = entity.text;
	return data;
}

yBDMessageFontColorEntityFromJson(YBDMessageFontColorEntity data, Map<String, dynamic> json) {
	if (json['target'] != null) {
		data.target = json['target'].toString();
	}
	if (json['fontSize'] != null) {
		data.fontSize = json['fontSize'] is String
				? double.tryParse(json['fontSize'])
				: json['fontSize'].toDouble();
	}
	if (json['fontWeight'] != null) {
		data.fontWeight = json['fontWeight'] is String
				? int.tryParse(json['fontWeight'])
				: json['fontWeight'].toInt();
	}
	if (json['color'] != null) {
		data.color = json['color'].toString();
	}
	return data;
}

Map<String, dynamic> yBDMessageFontColorEntityToJson(YBDMessageFontColorEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['target'] = entity.target;
	data['fontSize'] = entity.fontSize;
	data['fontWeight'] = entity.fontWeight;
	data['color'] = entity.color;
	return data;
}