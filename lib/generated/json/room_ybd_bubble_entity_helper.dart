import 'package:oyelive_main/module/room/entity/room_ybd_bubble_entity.dart';

yBDRoomBubbleEntityFromJson(YBDRoomBubbleEntity data, Map<String, dynamic> json) {
	if (json['BUBBLE'] != null) {
		data.bUBBLE = YBDRoomBubbleBUBBLE().fromJson(json['BUBBLE']);
	}
	return data;
}

Map<String, dynamic> yBDRoomBubbleEntityToJson(YBDRoomBubbleEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['BUBBLE'] = entity.bUBBLE?.toJson();
	return data;
}

yBDRoomBubbleBUBBLEFromJson(YBDRoomBubbleBUBBLE data, Map<String, dynamic> json) {
	if (json['image'] != null) {
		data.image = json['image'].toString();
	}
	if (json['itemId'] != null) {
		data.itemId = json['itemId'] is String
				? int.tryParse(json['itemId'])
				: json['itemId'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['expireAt'] != null) {
		data.expireAt = json['expireAt'] is String
				? int.tryParse(json['expireAt'])
				: json['expireAt'].toInt();
	}
	if (json['animation'] != null) {
		data.animation = json['animation'].toString();
	}
	if (json['cacheTime'] != null) {
		data.cacheTime = json['cacheTime'] is String
				? int.tryParse(json['cacheTime'])
				: json['cacheTime'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDRoomBubbleBUBBLEToJson(YBDRoomBubbleBUBBLE entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['image'] = entity.image;
	data['itemId'] = entity.itemId;
	data['name'] = entity.name;
	data['expireAt'] = entity.expireAt;
	data['animation'] = entity.animation;
	data['cacheTime'] = entity.cacheTime;
	return data;
}