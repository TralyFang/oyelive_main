import 'package:oyelive_main/common/room_socket/message/common/match_ybd_command_message.dart';

yBDMatchCommandMessageFromJson(YBDMatchCommandMessage data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['source'] != null) {
		data.source = json['source'] is String
				? int.tryParse(json['source'])
				: json['source'].toInt();
	}
	if (json['target'] != null) {
		data.target = json['target'] is String
				? int.tryParse(json['target'])
				: json['target'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDMatchCommandMessageToJson(YBDMatchCommandMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['source'] = entity.source;
	data['target'] = entity.target;
	return data;
}