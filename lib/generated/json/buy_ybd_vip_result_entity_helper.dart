import 'package:oyelive_main/module/entity/buy_ybd_vip_result_entity.dart';

yBDBuyVipResultEntityFromJson(YBDBuyVipResultEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDBuyVipResultRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDBuyVipResultEntityToJson(YBDBuyVipResultEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDBuyVipResultRecordFromJson(YBDBuyVipResultRecord data, Map<String, dynamic> json) {
	if (json['pay'] != null) {
		data.pay = json['pay'] is String
				? int.tryParse(json['pay'])
				: json['pay'].toInt();
	}
	if (json['userExp'] != null) {
		data.userExp = json['userExp'] is String
				? int.tryParse(json['userExp'])
				: json['userExp'].toInt();
	}
	if (json['money'] != null) {
		data.money = json['money'] is String
				? int.tryParse(json['money'])
				: json['money'].toInt();
	}
	if (json['earn'] != null) {
		data.earn = json['earn'] is String
				? int.tryParse(json['earn'])
				: json['earn'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDBuyVipResultRecordToJson(YBDBuyVipResultRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['pay'] = entity.pay;
	data['userExp'] = entity.userExp;
	data['money'] = entity.money;
	data['earn'] = entity.earn;
	return data;
}