import 'package:oyelive_main/module/status/entity/comment_ybd_submit_resp_entity.dart';
import 'package:oyelive_main/module/status/entity/comment_ybd_list_entity.dart';

yBDCommentSubmitRespEntityFromJson(YBDCommentSubmitRespEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDReplyData().fromJson(json['data']);
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDCommentSubmitRespEntityToJson(YBDCommentSubmitRespEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	data['success'] = entity.success;
	return data;
}