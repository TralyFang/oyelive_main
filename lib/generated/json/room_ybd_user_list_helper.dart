import 'package:oyelive_main/common/room_socket/entity/room_ybd_user_list.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

yBDRoomUserListFromJson(YBDRoomUserList data, Map<String, dynamic> json) {
	if (json['totalCount'] != null) {
		data.totalCount = json['totalCount'] is String
				? int.tryParse(json['totalCount'])
				: json['totalCount'].toInt();
	}
	if (json['visitorCount'] != null) {
		data.visitorCount = json['visitorCount'] is String
				? int.tryParse(json['visitorCount'])
				: json['visitorCount'].toInt();
	}
	if (json['managerCount'] != null) {
		data.managerCount = json['managerCount'] is String
				? int.tryParse(json['managerCount'])
				: json['managerCount'].toInt();
	}
	if (json['users'] != null) {
		data.users = (json['users'] as List).map((v) => YBDUserInfo().fromJson(v)).toList();
	}
	if (json['managers'] != null) {
		data.managers = (json['managers'] as List).map((v) => YBDUserInfo().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDRoomUserListToJson(YBDRoomUserList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['totalCount'] = entity.totalCount;
	data['visitorCount'] = entity.visitorCount;
	data['managerCount'] = entity.managerCount;
	data['users'] =  entity.users?.map((v) => v?.toJson())?.toList();
	data['managers'] =  entity.managers?.map((v) => v?.toJson())?.toList();
	return data;
}