import 'package:oyelive_main/module/user/entity/agreements_ybd_entity.dart';

yBDAgreementsEntityFromJson(YBDAgreementsEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDAgreementsData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDAgreementsEntityToJson(YBDAgreementsEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDAgreementsDataFromJson(YBDAgreementsData data, Map<String, dynamic> json) {
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	return data;
}

Map<String, dynamic> yBDAgreementsDataToJson(YBDAgreementsData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['status'] = entity.status;
	return data;
}