import 'package:oyelive_main/common/room_socket/message/common/update_ybd_room_bg_publish.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/common/room_socket/message/common/room_ybd_theme.dart';

yBDRoomBgPublishFromJson(YBDRoomBgPublish data, Map<String, dynamic> json) {
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['content'] != null) {
		data.content = YBDRoomThemeContent().fromJson(json['content']);
	}
	return data;
}

Map<String, dynamic> yBDRoomBgPublishToJson(YBDRoomBgPublish entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['msgType'] = entity.msgType;
	data['roomId'] = entity.roomId;
	data['fromUser'] = entity.fromUser;
	data['toUser'] = entity.toUser;
	data['mode'] = entity.mode;
	data['destination'] = entity.destination;
	data['type'] = entity.type;
	data['content'] = entity.content?.toJson();
	return data;
}