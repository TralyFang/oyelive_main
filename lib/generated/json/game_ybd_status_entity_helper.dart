import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_status_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/game_room/entity/enter_ybd_room_body_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_entry_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:collection/collection.dart';

yBDGameStatusEntityFromJson(YBDGameStatusEntity data, Map<String, dynamic> json) {
	if (json['cmd'] != null) {
		data.cmd = json['cmd'].toString();
	}
	if (json['ver'] != null) {
		data.ver = json['ver'].toString();
	}
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['codeMsg'] != null) {
		data.codeMsg = json['codeMsg'].toString();
	}
	if (json['route'] != null) {
		data.route = YBDEnterRoomBodyEntityRoute().fromJson(json['route']);
	}
	if (json['body'] != null) {
		data.body = YBDGameStatusBody().fromJson(json['body']);
	}
	if (json['resources'] != null) {
		data.resources = YBDGameStatusResources().fromJson(json['resources']);
	}
	return data;
}

Map<String, dynamic> yBDGameStatusEntityToJson(YBDGameStatusEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['cmd'] = entity.cmd;
	data['ver'] = entity.ver;
	data['code'] = entity.code;
	data['codeMsg'] = entity.codeMsg;
	data['route'] = entity.route?.toJson();
	data['body'] = entity.body?.toJson();
	data['resources'] = entity.resources?.toJson();
	return data;
}

yBDGameStatusBodyFromJson(YBDGameStatusBody data, Map<String, dynamic> json) {
	if (json['category'] != null) {
		data.category = json['category'].toString();
	}
	if (json['group'] != null) {
		data.group = YBDGameStatusBodyGroup().fromJson(json['group']);
	}
	if (json['groupId'] != null) {
		data.groupId = json['groupId'] is String
				? int.tryParse(json['groupId'])
				: json['groupId'].toInt();
	}
	if (json['members'] != null) {
		data.members = (json['members'] as List).map((v) => YBDGameStatusBodyMembers().fromJson(v)).toList();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['subCategory'] != null) {
		data.subCategory = json['subCategory'].toString();
	}
	if (json['subStatus'] != null) {
		data.subStatus = json['subStatus'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameStatusBodyToJson(YBDGameStatusBody entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['category'] = entity.category;
	data['group'] = entity.group?.toJson();
	data['groupId'] = entity.groupId;
	data['members'] =  entity.members?.map((v) => v?.toJson())?.toList();
	data['roomId'] = entity.roomId;
	data['status'] = entity.status;
	data['subCategory'] = entity.subCategory;
	data['subStatus'] = entity.subStatus;
	return data;
}

yBDGameStatusBodyGroupFromJson(YBDGameStatusBodyGroup data, Map<String, dynamic> json) {
	if (json['amount'] != null) {
		data.amount = json['amount'] is String
				? int.tryParse(json['amount'])
				: json['amount'].toInt();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['groupId'] != null) {
		data.groupId = json['groupId'] is String
				? int.tryParse(json['groupId'])
				: json['groupId'].toInt();
	}
	if (json['minimum'] != null) {
		data.minimum = json['minimum'] is String
				? int.tryParse(json['minimum'])
				: json['minimum'].toInt();
	}
	if (json['maximum'] != null) {
		data.maximum = json['maximum'] is String
				? int.tryParse(json['maximum'])
				: json['maximum'].toInt();
	}
	if (json['model'] != null) {
		data.model = json['model'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameStatusBodyGroupToJson(YBDGameStatusBodyGroup entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['amount'] = entity.amount;
	data['currency'] = entity.currency;
	data['groupId'] = entity.groupId;
	data['minimum'] = entity.minimum;
	data['maximum'] = entity.maximum;
	data['model'] = entity.model;
	data['status'] = entity.status;
	return data;
}

yBDGameStatusBodyMembersFromJson(YBDGameStatusBodyMembers data, Map<String, dynamic> json) {
	if (json['groupId'] != null) {
		data.groupId = json['groupId'] is String
				? int.tryParse(json['groupId'])
				: json['groupId'].toInt();
	}
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['role'] != null) {
		data.role = json['role'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['subStatus'] != null) {
		data.subStatus = json['subStatus'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameStatusBodyMembersToJson(YBDGameStatusBodyMembers entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['groupId'] = entity.groupId;
	data['id'] = entity.id;
	data['index'] = entity.index;
	data['role'] = entity.role;
	data['status'] = entity.status;
	data['subStatus'] = entity.subStatus;
	data['userId'] = entity.userId;
	return data;
}

yBDGameStatusResourcesFromJson(YBDGameStatusResources data, Map<String, dynamic> json) {
	if (json['user'] != null) {
		data.user = YBDGameEntryNotifyResourcesUser().fromJson(json['user']);
	}
	return data;
}

Map<String, dynamic> yBDGameStatusResourcesToJson(YBDGameStatusResources entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['user'] = entity.user?.toJson();
	return data;
}