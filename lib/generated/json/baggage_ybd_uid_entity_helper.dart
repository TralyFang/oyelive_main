import 'package:oyelive_main/module/entity/baggage_ybd_uid_entity.dart';

yBDBaggageUidEntityFromJson(YBDBaggageUidEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDBaggageUidRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDBaggageUidEntityToJson(YBDBaggageUidEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDBaggageUidRecordFromJson(YBDBaggageUidRecord data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['itemList'] != null) {
		data.itemList = (json['itemList'] as List).map((v) => YBDBaggageUidRecordItemList().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDBaggageUidRecordToJson(YBDBaggageUidRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['itemList'] =  entity.itemList?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDBaggageUidRecordItemListFromJson(YBDBaggageUidRecordItemList data, Map<String, dynamic> json) {
	if (json['number'] != null) {
		data.number = json['number'] is String
				? int.tryParse(json['number'])
				: json['number'].toInt();
	}
	if (json['personalId'] != null) {
		data.personalId = json['personalId'] is String
				? int.tryParse(json['personalId'])
				: json['personalId'].toInt();
	}
	if (json['equipped'] != null) {
		data.equipped = json['equipped'];
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['expireAfter'] != null) {
		data.expireAfter = json['expireAfter'] is String
				? int.tryParse(json['expireAfter'])
				: json['expireAfter'].toInt();
	}
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDBaggageUidRecordItemListToJson(YBDBaggageUidRecordItemList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['number'] = entity.number;
	data['personalId'] = entity.personalId;
	data['equipped'] = entity.equipped;
	data['price'] = entity.price;
	data['name'] = entity.name;
	data['currency'] = entity.currency;
	data['expireAfter'] = entity.expireAfter;
	data['id'] = entity.id;
	return data;
}