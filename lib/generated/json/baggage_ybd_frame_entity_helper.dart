import 'package:oyelive_main/ui/page/baggage/entity/baggage_ybd_frame_entity.dart';
import 'dart:async';

yBDBaggageFrameEntityFromJson(YBDBaggageFrameEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDBaggageFrameRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDBaggageFrameEntityToJson(YBDBaggageFrameEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDBaggageFrameRecordFromJson(YBDBaggageFrameRecord data, Map<String, dynamic> json) {
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['itemList'] != null) {
		data.itemList = (json['itemList'] as List).map((v) => YBDBaggageFrameRecordItemList().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDBaggageFrameRecordToJson(YBDBaggageFrameRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['type'] = entity.type;
	data['itemList'] =  entity.itemList?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDBaggageFrameRecordItemListFromJson(YBDBaggageFrameRecordItemList data, Map<String, dynamic> json) {
	if (json['image'] != null) {
		data.image = json['image'].toString();
	}
	if (json['number'] != null) {
		data.number = json['number'] is String
				? int.tryParse(json['number'])
				: json['number'].toInt();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['thumbnail'] != null) {
		data.thumbnail = json['thumbnail'].toString();
	}
	if (json['personalId'] != null) {
		data.personalId = json['personalId'] is String
				? int.tryParse(json['personalId'])
				: json['personalId'].toInt();
	}
	if (json['equipped'] != null) {
		data.equipped = json['equipped'];
	}
	if (json['price'] != null) {
		data.price = json['price'] is String
				? int.tryParse(json['price'])
				: json['price'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['expireAfter'] != null) {
		data.expireAfter = json['expireAfter'] is String
				? int.tryParse(json['expireAfter'])
				: json['expireAfter'].toInt();
	}
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDBaggageFrameRecordItemListToJson(YBDBaggageFrameRecordItemList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['image'] = entity.image;
	data['number'] = entity.number;
	data['img'] = entity.img;
	data['thumbnail'] = entity.thumbnail;
	data['personalId'] = entity.personalId;
	data['equipped'] = entity.equipped;
	data['price'] = entity.price;
	data['name'] = entity.name;
	data['expireAfter'] = entity.expireAfter;
	data['id'] = entity.id;
	return data;
}