import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_category_entity.dart';
import 'dart:async';

yBDGameCategoryEntityFromJson(YBDGameCategoryEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = (json['data'] as List).map((v) => YBDGameCategoryData().fromJson(v)).toList();
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameCategoryEntityToJson(YBDGameCategoryEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] =  entity.data?.map((v) => v?.toJson())?.toList();
	data['success'] = entity.success;
	data['message'] = entity.message;
	return data;
}

yBDGameCategoryDataFromJson(YBDGameCategoryData data, Map<String, dynamic> json) {
	if (json['projectCode'] != null) {
		data.projectCode = json['projectCode'].toString();
	}
	if (json['tenantCode'] != null) {
		data.tenantCode = json['tenantCode'].toString();
	}
	if (json['category'] != null) {
		data.category = json['category'].toString();
	}
	if (json['subCategory'] != null) {
		data.subCategory = json['subCategory'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['appMinVersion'] != null) {
		data.appMinVersion = json['appMinVersion'] is String
				? int.tryParse(json['appMinVersion'])
				: json['appMinVersion'].toInt();
	}
	if (json['selectedImage'] != null) {
		data.selectedImage = json['selectedImage'].toString();
	}
	if (json['image'] != null) {
		data.image = json['image'].toString();
	}
	if (json['redirectUrl'] != null) {
		data.redirectUrl = json['redirectUrl'].toString();
	}
	if (json['property'] != null) {
		data.property = json['property'];
	}
	return data;
}

Map<String, dynamic> yBDGameCategoryDataToJson(YBDGameCategoryData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['projectCode'] = entity.projectCode;
	data['tenantCode'] = entity.tenantCode;
	data['category'] = entity.category;
	data['subCategory'] = entity.subCategory;
	data['status'] = entity.status;
	data['appMinVersion'] = entity.appMinVersion;
	data['selectedImage'] = entity.selectedImage;
	data['image'] = entity.image;
	data['redirectUrl'] = entity.redirectUrl;
	data['property'] = entity.property;
	return data;
}