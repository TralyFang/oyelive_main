import 'package:oyelive_main/ui/page/recharge/easypaisa/entity/easypaisa_ybd_pay_body_entity.dart';
import 'dart:async';

yBDEasypaisaPayBodyEntityFromJson(YBDEasypaisaPayBodyEntity data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['money'] != null) {
		data.money = json['money'] is String
				? int.tryParse(json['money'])
				: json['money'].toInt();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['channel'] != null) {
		data.channel = json['channel'].toString();
	}
	if (json['country'] != null) {
		data.country = json['country'].toString();
	}
	if (json['msisdn'] != null) {
		data.msisdn = json['msisdn'].toString();
	}
	if (json['mobileAccountNo'] != null) {
		data.mobileAccountNo = json['mobileAccountNo'].toString();
	}
	if (json['email'] != null) {
		data.email = json['email'].toString();
	}
	if (json['advertisingId'] != null) {
		data.advertisingId = json['advertisingId'].toString();
	}
	if (json['appsflyerId'] != null) {
		data.appsflyerId = json['appsflyerId'].toString();
	}
	if (json['appVersionName'] != null) {
		data.appVersionName = json['appVersionName'].toString();
	}
	return data;
}

Map<String, dynamic> yBDEasypaisaPayBodyEntityToJson(YBDEasypaisaPayBodyEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['money'] = entity.money;
	data['currency'] = entity.currency;
	data['channel'] = entity.channel;
	data['country'] = entity.country;
	data['msisdn'] = entity.msisdn;
	data['mobileAccountNo'] = entity.mobileAccountNo;
	data['email'] = entity.email;
	data['advertisingId'] = entity.advertisingId;
	data['appsflyerId'] = entity.appsflyerId;
	data['appVersionName'] = entity.appVersionName;
	return data;
}