import 'package:oyelive_main/ui/page/room/teen_patti/entity/tp_ybd_vip_record_entity.dart';
import 'dart:async';

yBDTpVipRecordEntityFromJson(YBDTpVipRecordEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDTpVipRecordEntityRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDTpVipRecordEntityToJson(YBDTpVipRecordEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDTpVipRecordEntityRecordFromJson(YBDTpVipRecordEntityRecord data, Map<String, dynamic> json) {
	if (json['targetBeans'] != null) {
		data.targetBeans = json['targetBeans'] is String
				? int.tryParse(json['targetBeans'])
				: json['targetBeans'].toInt();
	}
	if (json['beans'] != null) {
		data.beans = json['beans'] is String
				? int.tryParse(json['beans'])
				: json['beans'].toInt();
	}
	if (json['context'] != null) {
		data.context = json['context'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	return data;
}

Map<String, dynamic> yBDTpVipRecordEntityRecordToJson(YBDTpVipRecordEntityRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['targetBeans'] = entity.targetBeans;
	data['beans'] = entity.beans;
	data['context'] = entity.context;
	data['userId'] = entity.userId;
	data['status'] = entity.status;
	return data;
}