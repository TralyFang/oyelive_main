import 'package:oyelive_main/module/user/entity/login_ybd_resp_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

yBDLoginRespFromJson(YBDLoginResp data, Map<String, dynamic> json) {
	if (json['record'] != null) {
		data.record = YBDLoginRecord().fromJson(json['record']);
	}
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	return data;
}

Map<String, dynamic> yBDLoginRespToJson(YBDLoginResp entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['record'] = entity.record?.toJson();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	return data;
}

yBDLoginRecordFromJson(YBDLoginRecord data, Map<String, dynamic> json) {
	if (json['userInfo'] != null) {
		data.userInfo = YBDUserInfo().fromJson(json['userInfo']);
	}
	if (json['randomKey'] != null) {
		data.randomKey = json['randomKey'].toString();
	}
	if (json['seed'] != null) {
		data.seed = json['seed'] is String
				? int.tryParse(json['seed'])
				: json['seed'].toInt();
	}
	if (json['newUser'] != null) {
		data.newUser = json['newUser'];
	}
	if (json['type'] != null) {
		data.type = json['type'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['content'] != null) {
		data.content = json['content'].toString();
	}
	if (json['button'] != null) {
		data.button = json['button'].toString();
	}
	if (json['extend'] != null) {
		data.extend = json['extend'].toString();
	}
	return data;
}

Map<String, dynamic> yBDLoginRecordToJson(YBDLoginRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userInfo'] = entity.userInfo?.toJson();
	data['randomKey'] = entity.randomKey;
	data['seed'] = entity.seed;
	data['newUser'] = entity.newUser;
	data['type'] = entity.type;
	data['title'] = entity.title;
	data['content'] = entity.content;
	data['button'] = entity.button;
	data['extend'] = entity.extend;
	return data;
}