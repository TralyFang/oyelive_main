import 'package:oyelive_main/common/room_socket/message/common/pk_ybd_invite_pub.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';

yBDPKInvitePubFromJson(YBDPKInvitePub data, Map<String, dynamic> json) {
	if (json['costTime'] != null) {
		data.costTime = (json['costTime'] as List).map((v) => v).toList().cast<dynamic>();
	}
	if (json['content'] != null) {
		data.content = YBDPKInviteContent().fromJson(json['content']);
	}
	if (json['version'] != null) {
		data.version = json['version'] is String
				? int.tryParse(json['version'])
				: json['version'].toInt();
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPKInvitePubToJson(YBDPKInvitePub entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['costTime'] = entity.costTime;
	data['content'] = entity.content?.toJson();
	data['version'] = entity.version;
	data['destination'] = entity.destination;
	data['fromUser'] = entity.fromUser;
	data['mode'] = entity.mode;
	data['msgType'] = entity.msgType;
	data['roomId'] = entity.roomId;
	data['toUser'] = entity.toUser;
	data['type'] = entity.type;
	return data;
}

yBDPKInviteContentFromJson(YBDPKInviteContent data, Map<String, dynamic> json) {
	if (json['source'] != null) {
		data.source = json['source'] is String
				? int.tryParse(json['source'])
				: json['source'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPKInviteContentToJson(YBDPKInviteContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['source'] = entity.source;
	return data;
}