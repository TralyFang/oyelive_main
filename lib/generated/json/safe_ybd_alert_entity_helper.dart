import 'package:oyelive_main/ui/page/profile/my_profile/entity/safe_ybd_alert_entity.dart';
import 'dart:async';

yBDSafeAlertEntityFromJson(YBDSafeAlertEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = YBDSafeAlertRecord().fromJson(json['record']);
	}
	return data;
}

Map<String, dynamic> yBDSafeAlertEntityToJson(YBDSafeAlertEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record?.toJson();
	return data;
}

yBDSafeAlertRecordFromJson(YBDSafeAlertRecord data, Map<String, dynamic> json) {
	if (json['userInfo'] != null) {
		data.userInfo = YBDSafeAlertRecordUserInfo().fromJson(json['userInfo']);
	}
	if (json['total'] != null) {
		data.total = json['total'] is String
				? int.tryParse(json['total'])
				: json['total'].toInt();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['safety'] != null) {
		data.safety = json['safety'].toString();
	}
	if (json['topTitle'] != null) {
		data.topTitle = json['topTitle'].toString();
	}
	if (json['bottomTitle'] != null) {
		data.bottomTitle = json['bottomTitle'].toString();
	}
	if (json['window'] != null) {
		data.window = json['window'];
	}
	return data;
}

Map<String, dynamic> yBDSafeAlertRecordToJson(YBDSafeAlertRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userInfo'] = entity.userInfo?.toJson();
	data['total'] = entity.total;
	data['level'] = entity.level;
	data['safety'] = entity.safety;
	data['topTitle'] = entity.topTitle;
	data['bottomTitle'] = entity.bottomTitle;
	data['window'] = entity.window;
	return data;
}

yBDSafeAlertRecordUserInfoFromJson(YBDSafeAlertRecordUserInfo data, Map<String, dynamic> json) {
	if (json['country'] != null) {
		data.country = json['country'].toString();
	}
	if (json['password'] != null) {
		data.password = json['password'];
	}
	if (json['phone'] != null) {
		data.phone = json['phone'].toString();
	}
	if (json['sex'] != null) {
		data.sex = json['sex'] is String
				? int.tryParse(json['sex'])
				: json['sex'].toInt();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDSafeAlertRecordUserInfoToJson(YBDSafeAlertRecordUserInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['country'] = entity.country;
	data['password'] = entity.password;
	data['phone'] = entity.phone;
	data['sex'] = entity.sex;
	data['nickname'] = entity.nickname;
	data['userId'] = entity.userId;
	return data;
}