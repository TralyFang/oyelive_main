import 'package:oyelive_main/module/status/entity/dialogue_ybd_list_entity.dart';
import 'package:oyelive_main/module/status/entity/random_ybd_dialogue_entity.dart';

yBDDialogueListEntityFromJson(YBDDialogueListEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDDialogueListData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDDialogueListEntityToJson(YBDDialogueListEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['message'] = entity.message;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDDialogueListDataFromJson(YBDDialogueListData data, Map<String, dynamic> json) {
	if (json['rows'] != null) {
		data.rows = (json['rows'] as List).map((v) => YBDRandomDialogueDataDialogue().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDDialogueListDataToJson(YBDDialogueListData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['rows'] =  entity.rows?.map((v) => v?.toJson())?.toList();
	return data;
}