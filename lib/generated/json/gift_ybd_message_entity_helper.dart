import 'package:oyelive_main/module/inbox/entity/gift_ybd_message_entity.dart';

yBDGiftMessageEntityFromJson(YBDGiftMessageEntity data, Map<String, dynamic> json) {
	if (json['messageId'] != null) {
		data.messageId = json['messageId'].toString();
	}
	if (json['contentType'] != null) {
		data.contentType = json['contentType'].toString();
	}
	if (json['content'] != null) {
		data.content = YBDGiftMessageContent().fromJson(json['content']);
	}
	if (json['sender'] != null) {
		data.sender = json['sender'];
	}
	if (json['receivers'] != null) {
		data.receivers = (json['receivers'] as List).map((v) => YBDGiftMessageReceiver().fromJson(v)).toList();
	}
	if (json['sequenceId'] != null) {
		data.sequenceId = json['sequenceId'] is String
				? int.tryParse(json['sequenceId'])
				: json['sequenceId'].toInt();
	}
	if (json['queueId'] != null) {
		data.queueId = json['queueId'].toString();
	}
	if (json['sendTime'] != null) {
		data.sendTime = json['sendTime'] is String
				? int.tryParse(json['sendTime'])
				: json['sendTime'].toInt();
	}
	if (json['category'] != null) {
		data.category = json['category'].toString();
	}
	if (json['attributes'] != null) {
		data.attributes = YBDGiftMessageAttributes().fromJson(json['attributes']);
	}
	if (json['requestId'] != null) {
		data.requestId = json['requestId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGiftMessageEntityToJson(YBDGiftMessageEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['messageId'] = entity.messageId;
	data['contentType'] = entity.contentType;
	data['content'] = entity.content?.toJson();
	data['sender'] = entity.sender;
	data['receivers'] =  entity.receivers?.map((v) => v?.toJson())?.toList();
	data['sequenceId'] = entity.sequenceId;
	data['queueId'] = entity.queueId;
	data['sendTime'] = entity.sendTime;
	data['category'] = entity.category;
	data['attributes'] = entity.attributes?.toJson();
	data['requestId'] = entity.requestId;
	return data;
}

yBDGiftMessageContentFromJson(YBDGiftMessageContent data, Map<String, dynamic> json) {
	if (json['layout'] != null) {
		data.layout = json['layout'].toString();
	}
	if (json['resource'] != null) {
		data.resource = json['resource'].toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['comment'] != null) {
		data.comment = json['comment'].toString();
	}
	if (json['attributes'] != null) {
		data.attributes = json['attributes'];
	}
	if (json['text'] != null) {
		data.text = json['text'];
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['extend'] != null) {
		data.extend = YBDGiftMessageAttributes().fromJson(json['extend']);
	}
	if (json['click'] != null) {
		data.click = YBDGiftMessageContentClick().fromJson(json['click']);
	}
	return data;
}

Map<String, dynamic> yBDGiftMessageContentToJson(YBDGiftMessageContent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['layout'] = entity.layout;
	data['resource'] = entity.resource;
	data['icon'] = entity.icon;
	data['comment'] = entity.comment;
	data['attributes'] = entity.attributes;
	data['text'] = entity.text;
	data['title'] = entity.title;
	data['extend'] = entity.extend?.toJson();
	data['click'] = entity.click?.toJson();
	return data;
}

yBDGiftMessageContentClickFromJson(YBDGiftMessageContentClick data, Map<String, dynamic> json) {
	if (json['action'] != null) {
		data.action = json['action'].toString();
	}
	if (json['url'] != null) {
		data.url = json['url'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGiftMessageContentClickToJson(YBDGiftMessageContentClick entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['action'] = entity.action;
	data['url'] = entity.url;
	return data;
}

yBDGiftMessageReceiverFromJson(YBDGiftMessageReceiver data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGiftMessageReceiverToJson(YBDGiftMessageReceiver entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	return data;
}

yBDGiftMessageAttributesFromJson(YBDGiftMessageAttributes data, Map<String, dynamic> json) {
	if (json['giftName'] != null) {
		data.giftName = json['giftName'].toString();
	}
	if (json['giftNumber'] != null) {
		data.giftNumber = json['giftNumber'] is String
				? int.tryParse(json['giftNumber'])
				: json['giftNumber'].toInt();
	}
	if (json['gender'] != null) {
		data.gender = json['gender'] is String
				? int.tryParse(json['gender'])
				: json['gender'].toInt();
	}
	if (json['level'] != null) {
		data.level = json['level'] is String
				? int.tryParse(json['level'])
				: json['level'].toInt();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	if (json['vipIcon'] != null) {
		data.vipIcon = json['vipIcon'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGiftMessageAttributesToJson(YBDGiftMessageAttributes entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['giftName'] = entity.giftName;
	data['giftNumber'] = entity.giftNumber;
	data['gender'] = entity.gender;
	data['level'] = entity.level;
	data['userId'] = entity.userId;
	data['vipIcon'] = entity.vipIcon;
	return data;
}