import 'package:oyelive_main/ui/page/room/widget/room_ybd_special_entrance.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_activity_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/generated/json/room_ybd_special_entrance_helper.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/home/widget/popular_ybd_task_float.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_icon_banner.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

yBDSpecialEntranceEntityFromJson(YBDSpecialEntranceEntity data, Map<String, dynamic> json) {
	if (json['route'] != null) {
		data.route = json['route'].toString();
	}
	if (json['pic'] != null) {
		data.pic = json['pic'].toString();
	}
	if (json['remainingTime'] != null) {
		data.remainingTime = json['remainingTime'] is String
				? int.tryParse(json['remainingTime'])
				: json['remainingTime'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDSpecialEntranceEntityToJson(YBDSpecialEntranceEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['route'] = entity.route;
	data['pic'] = entity.pic;
	data['remainingTime'] = entity.remainingTime;
	return data;
}