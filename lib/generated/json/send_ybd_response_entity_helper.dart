import 'package:oyelive_main/module/inbox/entity/send_ybd_response_entity.dart';
import 'package:oyelive_main/module/inbox/entity/conversation_ybd_list_entity.dart';

yBDSendResponseEntityFromJson(YBDSendResponseEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDSendResponseData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDSendResponseEntityToJson(YBDSendResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDSendResponseDataFromJson(YBDSendResponseData data, Map<String, dynamic> json) {
	if (json['messageId'] != null) {
		data.messageId = json['messageId'].toString();
	}
	if (json['requestId'] != null) {
		data.requestId = json['requestId'].toString();
	}
	if (json['newContact'] != null) {
		data.newContact = json['newContact'];
	}
	if (json['contactInfo'] != null) {
		data.contactInfo = YBDConversationListData().fromJson(json['contactInfo']);
	}
	return data;
}

Map<String, dynamic> yBDSendResponseDataToJson(YBDSendResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['messageId'] = entity.messageId;
	data['requestId'] = entity.requestId;
	data['newContact'] = entity.newContact;
	data['contactInfo'] = entity.contactInfo?.toJson();
	return data;
}