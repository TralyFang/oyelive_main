import 'package:oyelive_main/module/entity/query_ybd_server_time_entity.dart';

yBDServerTimeEntityFromJson(YBDServerTimeEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = json['record'] is String
				? int.tryParse(json['record'])
				: json['record'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDServerTimeEntityToJson(YBDServerTimeEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] = entity.record;
	return data;
}