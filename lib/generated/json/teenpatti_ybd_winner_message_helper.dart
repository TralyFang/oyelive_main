import 'package:oyelive_main/common/room_socket/message/common/teenpatti_ybd_winner_message.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/common/room_socket/message/common/winner.dart';

yBDPublishTeenPattiWinnerMessageFromJson(YBDPublishTeenPattiWinnerMessage data, Map<String, dynamic> json) {
	if (json['content'] != null) {
		data.content = (json['content'] as List).map((v) => YBDWinner().fromJson(v)).toList();
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPublishTeenPattiWinnerMessageToJson(YBDPublishTeenPattiWinnerMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['content'] =  entity.content?.map((v) => v?.toJson())?.toList();
	data['destination'] = entity.destination;
	data['fromUser'] = entity.fromUser;
	data['mode'] = entity.mode;
	data['msgType'] = entity.msgType;
	data['roomId'] = entity.roomId;
	data['toUser'] = entity.toUser;
	data['type'] = entity.type;
	return data;
}