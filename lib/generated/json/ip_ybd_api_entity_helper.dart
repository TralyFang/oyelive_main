import 'package:oyelive_main/module/thirdparty/entity/ip_ybd_api_entity.dart';

yBDIpApiEntityFromJson(YBDIpApiEntity data, Map<String, dynamic> json) {
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['country'] != null) {
		data.country = json['country'].toString();
	}
	if (json['countryCode'] != null) {
		data.countryCode = json['countryCode'].toString();
	}
	if (json['region'] != null) {
		data.region = json['region'].toString();
	}
	if (json['regionName'] != null) {
		data.regionName = json['regionName'].toString();
	}
	if (json['city'] != null) {
		data.city = json['city'].toString();
	}
	if (json['zip'] != null) {
		data.zip = json['zip'].toString();
	}
	if (json['lat'] != null) {
		data.lat = json['lat'] is String
				? double.tryParse(json['lat'])
				: json['lat'].toDouble();
	}
	if (json['lon'] != null) {
		data.lon = json['lon'] is String
				? double.tryParse(json['lon'])
				: json['lon'].toDouble();
	}
	if (json['timezone'] != null) {
		data.timezone = json['timezone'].toString();
	}
	if (json['isp'] != null) {
		data.isp = json['isp'].toString();
	}
	if (json['org'] != null) {
		data.org = json['org'].toString();
	}
	if (json['as'] != null) {
		data.xAs = json['as'].toString();
	}
	if (json['query'] != null) {
		data.query = json['query'].toString();
	}
	return data;
}

Map<String, dynamic> yBDIpApiEntityToJson(YBDIpApiEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['status'] = entity.status;
	data['country'] = entity.country;
	data['countryCode'] = entity.countryCode;
	data['region'] = entity.region;
	data['regionName'] = entity.regionName;
	data['city'] = entity.city;
	data['zip'] = entity.zip;
	data['lat'] = entity.lat;
	data['lon'] = entity.lon;
	data['timezone'] = entity.timezone;
	data['isp'] = entity.isp;
	data['org'] = entity.org;
	data['as'] = entity.xAs;
	data['query'] = entity.query;
	return data;
}