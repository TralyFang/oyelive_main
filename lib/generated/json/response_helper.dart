import 'package:oyelive_main/common/room_socket/message/resp/response.dart';

responseFromJson(Response data, Map<String, dynamic> json) {
	if (json['roomId'] != null) {
		data.roomId = json['roomId'];
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['costTime'] != null) {
		data.costTime = (json['costTime'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['sequence'] != null) {
		data.sequence = json['sequence'] is String
				? int.tryParse(json['sequence'])
				: json['sequence'].toInt();
	}
	if (json['commandId'] != null) {
		data.commandId = json['commandId'] is String
				? int.tryParse(json['commandId'])
				: json['commandId'].toInt();
	}
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['desc'] != null) {
		data.desc = json['desc'].toString();
	}
	if (json['result'] != null) {
		data.result = json['result'];
	}
	if (json['content'] != null) {
		data.content = json['content'];
	}
	return data;
}

Map<String, dynamic> responseToJson(Response entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['roomId'] = entity.roomId;
	data['destination'] = entity.destination;
	data['costTime'] = entity.costTime;
	data['msgType'] = entity.msgType;
	data['fromUser'] = entity.fromUser;
	data['toUser'] = entity.toUser;
	data['mode'] = entity.mode;
	data['sequence'] = entity.sequence;
	data['commandId'] = entity.commandId;
	data['code'] = entity.code;
	data['desc'] = entity.desc;
	data['result'] = entity.result;
	data['content'] = entity.content;
	return data;
}