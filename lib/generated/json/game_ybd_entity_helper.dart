import 'package:oyelive_main/ui/page/room/play_center/entity/game_ybd_entity.dart';
import 'dart:async';

yBDGameEntityFromJson(YBDGameEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDGameRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDGameEntityToJson(YBDGameEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDGameRecordFromJson(YBDGameRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon'].toString();
	}
	if (json['url'] != null) {
		data.url = json['url'].toString();
	}
	if (json['platform'] != null) {
		data.platform = json['platform'].toString();
	}
	if (json['appVersion'] != null) {
		data.appVersion = json['appVersion'].toString();
	}
	if (json['display'] != null) {
		data.display = json['display'] is String
				? int.tryParse(json['display'])
				: json['display'].toInt();
	}
	if (json['shieldCountryCode'] != null) {
		data.shieldCountryCode = json['shieldCountryCode'].toString();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['showRoomBottom'] != null) {
		data.showRoomBottom = json['showRoomBottom'] is String
				? int.tryParse(json['showRoomBottom'])
				: json['showRoomBottom'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDGameRecordToJson(YBDGameRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	data['platform'] = entity.platform;
	data['appVersion'] = entity.appVersion;
	data['display'] = entity.display;
	data['shieldCountryCode'] = entity.shieldCountryCode;
	data['index'] = entity.index;
	data['code'] = entity.code;
	data['showRoomBottom'] = entity.showRoomBottom;
	return data;
}