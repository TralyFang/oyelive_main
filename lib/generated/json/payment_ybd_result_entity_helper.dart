import 'package:oyelive_main/module/entity/payment_ybd_result_entity.dart';

yBDPaymentResultEntityFromJson(YBDPaymentResultEntity data, Map<String, dynamic> json) {
	if (json['payResult'] != null) {
		data.payResult = YBDPayResultInfo().fromJson(json['payResult']);
	}
	return data;
}

Map<String, dynamic> yBDPaymentResultEntityToJson(YBDPaymentResultEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['payResult'] = entity.payResult?.toJson();
	return data;
}

yBDPayResultInfoFromJson(YBDPayResultInfo data, Map<String, dynamic> json) {
	if (json['pay'] != null) {
		data.pay = json['pay'] is String
				? int.tryParse(json['pay'])
				: json['pay'].toInt();
	}
	if (json['userExp'] != null) {
		data.userExp = json['userExp'] is String
				? int.tryParse(json['userExp'])
				: json['userExp'].toInt();
	}
	if (json['money'] != null) {
		data.money = json['money'] is String
				? int.tryParse(json['money'])
				: json['money'].toInt();
	}
	if (json['earn'] != null) {
		data.earn = json['earn'] is String
				? int.tryParse(json['earn'])
				: json['earn'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPayResultInfoToJson(YBDPayResultInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['pay'] = entity.pay;
	data['userExp'] = entity.userExp;
	data['money'] = entity.money;
	data['earn'] = entity.earn;
	return data;
}