import 'package:oyelive_main/module/entity/pk_ybd_record_entity.dart';

yBDPKRecordEntityFromJson(YBDPKRecordEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDPKRecord().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDPKRecordEntityToJson(YBDPKRecordEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['recordSum'] = entity.recordSum;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDPKRecordFromJson(YBDPKRecord data, Map<String, dynamic> json) {
	if (json['userId'] != null) {
		data.userId = json['userId'] is String
				? int.tryParse(json['userId'])
				: json['userId'].toInt();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime'] is String
				? int.tryParse(json['createTime'])
				: json['createTime'].toInt();
	}
	if (json['competitor'] != null) {
		data.competitor = json['competitor'] is String
				? int.tryParse(json['competitor'])
				: json['competitor'].toInt();
	}
	if (json['result'] != null) {
		data.result = json['result'] is String
				? int.tryParse(json['result'])
				: json['result'].toInt();
	}
	if (json['gifts'] != null) {
		data.gifts = json['gifts'] is String
				? int.tryParse(json['gifts'])
				: json['gifts'].toInt();
	}
	if (json['competitorGifts'] != null) {
		data.competitorGifts = json['competitorGifts'] is String
				? int.tryParse(json['competitorGifts'])
				: json['competitorGifts'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDPKRecordToJson(YBDPKRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['userId'] = entity.userId;
	data['createTime'] = entity.createTime;
	data['competitor'] = entity.competitor;
	data['result'] = entity.result;
	data['gifts'] = entity.gifts;
	data['competitorGifts'] = entity.competitorGifts;
	return data;
}