import 'package:oyelive_main/common/room_socket/message/common/surrender_ybd_pk_message.dart';

yBDSurrenderPKMessageFromJson(YBDSurrenderPKMessage data, Map<String, dynamic> json) {
	if (json['pkId'] != null) {
		data.pkId = json['pkId'] is String
				? int.tryParse(json['pkId'])
				: json['pkId'].toInt();
	}
	if (json['sourceUserId'] != null) {
		data.sourceUserId = json['sourceUserId'] is String
				? int.tryParse(json['sourceUserId'])
				: json['sourceUserId'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDSurrenderPKMessageToJson(YBDSurrenderPKMessage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['pkId'] = entity.pkId;
	data['sourceUserId'] = entity.sourceUserId;
	data['type'] = entity.type;
	return data;
}