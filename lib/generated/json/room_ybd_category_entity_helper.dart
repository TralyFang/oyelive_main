import 'package:oyelive_main/ui/page/room/start_live/entity/room_ybd_category_entity.dart';
import 'dart:async';

yBDRoomCategoryEntityFromJson(YBDRoomCategoryEntity data, Map<String, dynamic> json) {
	if (json['items'] != null) {
		data.items = (json['items'] as List).map((v) => YBDRoomCategoryItem().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDRoomCategoryEntityToJson(YBDRoomCategoryEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['items'] =  entity.items?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDRoomCategoryItemFromJson(YBDRoomCategoryItem data, Map<String, dynamic> json) {
	if (json['selected'] != null) {
		data.selected = json['selected'];
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDRoomCategoryItemToJson(YBDRoomCategoryItem entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['selected'] = entity.selected;
	data['title'] = entity.title;
	data['img'] = entity.img;
	data['index'] = entity.index;
	return data;
}