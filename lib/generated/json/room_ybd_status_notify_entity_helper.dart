import 'package:oyelive_main/ui/page/game_room/entity/room_ybd_status_notify_entity.dart';
import 'dart:async';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';

yBDRoomStatusNotifyEntityFromJson(YBDRoomStatusNotifyEntity data, Map<String, dynamic> json) {
	if (json['cmd'] != null) {
		data.cmd = json['cmd'].toString();
	}
	if (json['ver'] != null) {
		data.ver = json['ver'].toString();
	}
	if (json['route'] != null) {
		data.route = YBDRoomStatusNotifyRoute().fromJson(json['route']);
	}
	if (json['body'] != null) {
		data.body = YBDRoomStatusNotifyBody().fromJson(json['body']);
	}
	if (json['resources'] != null) {
		data.resources = YBDRoomStatusNotifyResources().fromJson(json['resources']);
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusNotifyEntityToJson(YBDRoomStatusNotifyEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['cmd'] = entity.cmd;
	data['ver'] = entity.ver;
	data['route'] = entity.route?.toJson();
	data['body'] = entity.body?.toJson();
	data['resources'] = entity.resources?.toJson();
	return data;
}

yBDRoomStatusNotifyRouteFromJson(YBDRoomStatusNotifyRoute data, Map<String, dynamic> json) {
	if (json['room'] != null) {
		data.room = json['room'].toString();
	}
	if (json['from'] != null) {
		data.from = json['from'].toString();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusNotifyRouteToJson(YBDRoomStatusNotifyRoute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['room'] = entity.room;
	data['from'] = entity.from;
	data['mode'] = entity.mode;
	return data;
}

yBDRoomStatusNotifyBodyFromJson(YBDRoomStatusNotifyBody data, Map<String, dynamic> json) {
	if (json['category'] != null) {
		data.category = json['category'].toString();
	}
	if (json['game'] != null) {
		data.game = YBDRoomStatusNotifyBodyGame().fromJson(json['game']);
	}
	if (json['subCategory'] != null) {
		data.subCategory = json['subCategory'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['subStatus'] != null) {
		data.subStatus = json['subStatus'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusNotifyBodyToJson(YBDRoomStatusNotifyBody entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['category'] = entity.category;
	data['game'] = entity.game?.toJson();
	data['subCategory'] = entity.subCategory;
	data['roomId'] = entity.roomId;
	data['status'] = entity.status;
	data['subStatus'] = entity.subStatus;
	return data;
}

yBDRoomStatusNotifyBodyGameFromJson(YBDRoomStatusNotifyBodyGame data, Map<String, dynamic> json) {
	if (json['amount'] != null) {
		data.amount = json['amount'] is String
				? int.tryParse(json['amount'])
				: json['amount'].toInt();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['currencyImage'] != null) {
		data.currencyImage = json['currencyImage'].toString();
	}
	if (json['minimum'] != null) {
		data.minimum = json['minimum'] is String
				? int.tryParse(json['minimum'])
				: json['minimum'].toInt();
	}
	if (json['maximum'] != null) {
		data.maximum = json['maximum'] is String
				? int.tryParse(json['maximum'])
				: json['maximum'].toInt();
	}
	if (json['model'] != null) {
		data.model = json['model'].toString();
	}
	if (json['needWinPawnDesc'] != null) {
		data.needWinPawnDesc = json['needWinPawnDesc'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['battleId'] != null) {
		data.battleId = json['battleId'] is String
				? int.tryParse(json['battleId'])
				: json['battleId'].toInt();
	}
	if (json['gameId'] != null) {
		data.gameId = json['gameId'] is String
				? int.tryParse(json['gameId'])
				: json['gameId'].toInt();
	}
	if (json['ticketLevel'] != null) {
		data.ticketLevel = (json['ticketLevel'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	if (json['thirdPartyId'] != null) {
		data.thirdPartyId = json['thirdPartyId'].toString();
	}
	if (json['members'] != null) {
		data.members = (json['members'] as List).map((v) => YBDRoomStatusNotifyBodyGameMembers().fromJson(v)).toList();
	}
	if (json['settlementList'] != null) {
		data.settlementList = (json['settlementList'] as List).map((v) => YBDRoomStatusNotifyBodyGameSettlementList().fromJson(v)).toList();
	}
	if (json['attribute'] != null) {
		data.attribute = YBDRoomStatusNotifyBodyGameAttribute().fromJson(json['attribute']);
	}
	if (json['optionalMode'] != null) {
		data.optionalMode = (json['optionalMode'] as List).map((v) => YBDRoomStatusGameMode().fromJson(v)).toList();
	}
	if (json['image'] != null) {
		data.image = YBDRoomStatusNotifyBodyGameImage().fromJson(json['image']);
	}
	if (json['matchType'] != null) {
		data.matchType = json['matchType'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusNotifyBodyGameToJson(YBDRoomStatusNotifyBodyGame entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['amount'] = entity.amount;
	data['currency'] = entity.currency;
	data['currencyImage'] = entity.currencyImage;
	data['minimum'] = entity.minimum;
	data['maximum'] = entity.maximum;
	data['model'] = entity.model;
	data['needWinPawnDesc'] = entity.needWinPawnDesc;
	data['status'] = entity.status;
	data['battleId'] = entity.battleId;
	data['gameId'] = entity.gameId;
	data['ticketLevel'] = entity.ticketLevel;
	data['thirdPartyId'] = entity.thirdPartyId;
	data['members'] =  entity.members?.map((v) => v?.toJson())?.toList();
	data['settlementList'] =  entity.settlementList?.map((v) => v?.toJson())?.toList();
	data['attribute'] = entity.attribute?.toJson();
	data['optionalMode'] =  entity.optionalMode?.map((v) => v?.toJson())?.toList();
	data['image'] = entity.image?.toJson();
	data['matchType'] = entity.matchType;
	return data;
}

yBDRoomStatusNotifyBodyGameImageFromJson(YBDRoomStatusNotifyBodyGameImage data, Map<String, dynamic> json) {
	if (json['logo'] != null) {
		data.logo = json['logo'].toString();
	}
	if (json['rule'] != null) {
		data.rule = YBDRoomStatusNotifyBodyGameImageRule().fromJson(json['rule']);
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusNotifyBodyGameImageToJson(YBDRoomStatusNotifyBodyGameImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['logo'] = entity.logo;
	data['rule'] = entity.rule?.toJson();
	return data;
}

yBDRoomStatusNotifyBodyGameImageRuleFromJson(YBDRoomStatusNotifyBodyGameImageRule data, Map<String, dynamic> json) {
	if (json['body'] != null) {
		data.body = json['body'].toString();
	}
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusNotifyBodyGameImageRuleToJson(YBDRoomStatusNotifyBodyGameImageRule entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['body'] = entity.body;
	data['title'] = entity.title;
	return data;
}

yBDRoomStatusNotifyBodyGameAttributeFromJson(YBDRoomStatusNotifyBodyGameAttribute data, Map<String, dynamic> json) {
	if (json['minWinPawn'] != null) {
		data.minWinPawn = json['minWinPawn'] is String
				? int.tryParse(json['minWinPawn'])
				: json['minWinPawn'].toInt();
	}
	if (json['url'] != null) {
		data.url = json['url'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusNotifyBodyGameAttributeToJson(YBDRoomStatusNotifyBodyGameAttribute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['minWinPawn'] = entity.minWinPawn;
	data['url'] = entity.url;
	return data;
}

yBDRoomStatusNotifyBodyGameMembersFromJson(YBDRoomStatusNotifyBodyGameMembers data, Map<String, dynamic> json) {
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['role'] != null) {
		data.role = json['role'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'].toString();
	}
	if (json['subStatus'] != null) {
		data.subStatus = json['subStatus'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusNotifyBodyGameMembersToJson(YBDRoomStatusNotifyBodyGameMembers entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['index'] = entity.index;
	data['role'] = entity.role;
	data['status'] = entity.status;
	data['subStatus'] = entity.subStatus;
	data['userId'] = entity.userId;
	return data;
}

yBDRoomStatusNotifyBodyGameSettlementListFromJson(YBDRoomStatusNotifyBodyGameSettlementList data, Map<String, dynamic> json) {
	if (json['gameCurrency'] != null) {
		data.gameCurrency = json['gameCurrency'].toString();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	if (json['result'] != null) {
		data.result = json['result'].toString();
	}
	if (json['userId'] != null) {
		data.userId = json['userId'].toString();
	}
	if (json['winNumber'] != null) {
		data.winNumber = json['winNumber'] is String
				? int.tryParse(json['winNumber'])
				: json['winNumber'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusNotifyBodyGameSettlementListToJson(YBDRoomStatusNotifyBodyGameSettlementList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['gameCurrency'] = entity.gameCurrency;
	data['index'] = entity.index;
	data['result'] = entity.result;
	data['userId'] = entity.userId;
	data['winNumber'] = entity.winNumber;
	return data;
}

yBDRoomStatusNotifyResourcesFromJson(YBDRoomStatusNotifyResources data, Map<String, dynamic> json) {
	if (json['user'] != null) {
		data.user = json['user'];
	}
	if (json['userAttribute'] != null) {
		data.userAttribute = json['userAttribute'];
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusNotifyResourcesToJson(YBDRoomStatusNotifyResources entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['user'] = entity.user;
	data['userAttribute'] = entity.userAttribute;
	return data;
}

yBDRoomStatusGameModeFromJson(YBDRoomStatusGameMode data, Map<String, dynamic> json) {
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['gameCurrency'] != null) {
		data.gameCurrency = (json['gameCurrency'] as List).map((v) => YBDRoomStatusGameModeCurrency().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusGameModeToJson(YBDRoomStatusGameMode entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['mode'] = entity.mode;
	data['gameCurrency'] =  entity.gameCurrency?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDRoomStatusGameModeCurrencyFromJson(YBDRoomStatusGameModeCurrency data, Map<String, dynamic> json) {
	if (json['ticketLevel'] != null) {
		data.ticketLevel = (json['ticketLevel'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	if (json['currency'] != null) {
		data.currency = json['currency'].toString();
	}
	if (json['robotSupportLevel'] != null) {
		data.robotSupportLevel = (json['robotSupportLevel'] as List).map((v) => v is String
				? int.tryParse(v)
				: v.toInt()).toList().cast<int>();
	}
	return data;
}

Map<String, dynamic> yBDRoomStatusGameModeCurrencyToJson(YBDRoomStatusGameModeCurrency entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['ticketLevel'] = entity.ticketLevel;
	data['currency'] = entity.currency;
	data['robotSupportLevel'] = entity.robotSupportLevel;
	return data;
}