import 'package:oyelive_main/common/service/iap_service/entity/deliver_ybd_entity.dart';

yBDDeliverEntityFromJson(YBDDeliverEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['data'] != null) {
		data.data = json['data'].toString();
	}
	if (json['success'] != null) {
		data.success = json['success'];
	}
	return data;
}

Map<String, dynamic> yBDDeliverEntityToJson(YBDDeliverEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['data'] = entity.data;
	data['success'] = entity.success;
	return data;
}