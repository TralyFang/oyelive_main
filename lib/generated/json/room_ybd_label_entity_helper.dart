import 'package:oyelive_main/module/user/entity/room_ybd_label_entity.dart';

yBDRoomLabelEntityFromJson(YBDRoomLabelEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDRoomLabelRecord().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDRoomLabelEntityToJson(YBDRoomLabelEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDRoomLabelRecordFromJson(YBDRoomLabelRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id'] is String
				? int.tryParse(json['id'])
				: json['id'].toInt();
	}
	if (json['name'] != null) {
		data.name = json['name'].toString();
	}
	if (json['img'] != null) {
		data.img = json['img'].toString();
	}
	if (json['parent'] != null) {
		data.parent = json['parent'] is String
				? int.tryParse(json['parent'])
				: json['parent'].toInt();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['display'] != null) {
		data.display = json['display'] is String
				? int.tryParse(json['display'])
				: json['display'].toInt();
	}
	if (json['index'] != null) {
		data.index = json['index'] is String
				? int.tryParse(json['index'])
				: json['index'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDRoomLabelRecordToJson(YBDRoomLabelRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['img'] = entity.img;
	data['parent'] = entity.parent;
	data['type'] = entity.type;
	data['display'] = entity.display;
	data['index'] = entity.index;
	return data;
}