import 'package:oyelive_main/ui/page/profile/my_profile/entity/top_ybd_up_gift_entity.dart';
import 'dart:async';

yBDTopUpGiftEntityFromJson(YBDTopUpGiftEntity data, Map<String, dynamic> json) {
	if (json['returnCode'] != null) {
		data.returnCode = json['returnCode'].toString();
	}
	if (json['returnMsg'] != null) {
		data.returnMsg = json['returnMsg'].toString();
	}
	if (json['record'] != null) {
		data.record = (json['record'] as List).map((v) => YBDTopUpGiftEntityRecord().fromJson(v)).toList();
	}
	if (json['recordSum'] != null) {
		data.recordSum = json['recordSum'].toString();
	}
	return data;
}

Map<String, dynamic> yBDTopUpGiftEntityToJson(YBDTopUpGiftEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['returnCode'] = entity.returnCode;
	data['returnMsg'] = entity.returnMsg;
	data['record'] =  entity.record?.map((v) => v?.toJson())?.toList();
	data['recordSum'] = entity.recordSum;
	return data;
}

yBDTopUpGiftEntityRecordFromJson(YBDTopUpGiftEntityRecord data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title'].toString();
	}
	if (json['status'] != null) {
		data.status = json['status'] is String
				? int.tryParse(json['status'])
				: json['status'].toInt();
	}
	if (json['spendCoins'] != null) {
		data.spendCoins = json['spendCoins'].toString();
	}
	if (json['winCoins'] != null) {
		data.winCoins = json['winCoins'].toString();
	}
	if (json['luckPackage'] != null) {
		data.luckPackage = json['luckPackage'] is String
				? int.tryParse(json['luckPackage'])
				: json['luckPackage'].toInt();
	}
	if (json['rewards'] != null) {
		data.rewards = (json['rewards'] as List).map((v) => YBDTopUpGiftEntityRecordReward().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDTopUpGiftEntityRecordToJson(YBDTopUpGiftEntityRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['status'] = entity.status;
	data['spendCoins'] = entity.spendCoins;
	data['winCoins'] = entity.winCoins;
	data['luckPackage'] = entity.luckPackage;
	data['rewards'] =  entity.rewards?.map((v) => v?.toJson())?.toList();
	return data;
}

yBDTopUpGiftEntityRecordRewardFromJson(YBDTopUpGiftEntityRecordReward data, Map<String, dynamic> json) {
	if (json['image'] != null) {
		data.image = json['image'].toString();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['desc'] != null) {
		data.desc = json['desc'].toString();
	}
	return data;
}

Map<String, dynamic> yBDTopUpGiftEntityRecordRewardToJson(YBDTopUpGiftEntityRecordReward entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['image'] = entity.image;
	data['type'] = entity.type;
	data['desc'] = entity.desc;
	return data;
}