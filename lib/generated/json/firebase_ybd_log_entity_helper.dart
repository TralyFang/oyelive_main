import 'package:oyelive_main/module/entity/firebase_ybd_log_entity.dart';
import 'dart:convert';
import 'package:flutter/cupertino.dart';

yBDFirebaseLogEntityFromJson(YBDFirebaseLogEntity data, Map<String, dynamic> json) {
	if (json['isOpen'] != null) {
		data.isOpen = json['isOpen'];
	}
	if (json['userIds'] != null) {
		data.userIds = (json['userIds'] as List).map((v) => v.toString()).toList().cast<String>();
	}
	if (json['modules'] != null) {
		data.modules = (json['modules'] as List).map((v) => v.toString()).toList().cast<String>();
	}
	return data;
}

Map<String, dynamic> yBDFirebaseLogEntityToJson(YBDFirebaseLogEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['isOpen'] = entity.isOpen;
	data['userIds'] = entity.userIds;
	data['modules'] = entity.modules;
	return data;
}