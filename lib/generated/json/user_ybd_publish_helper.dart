import 'package:oyelive_main/common/room_socket/message/common/user_ybd_publish.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

yBDUserPublishFromJson(YBDUserPublish data, Map<String, dynamic> json) {
	if (json['msgType'] != null) {
		data.msgType = json['msgType'].toString();
	}
	if (json['roomId'] != null) {
		data.roomId = json['roomId'] is String
				? int.tryParse(json['roomId'])
				: json['roomId'].toInt();
	}
	if (json['fromUser'] != null) {
		data.fromUser = json['fromUser'] is String
				? int.tryParse(json['fromUser'])
				: json['fromUser'].toInt();
	}
	if (json['toUser'] != null) {
		data.toUser = json['toUser'] is String
				? int.tryParse(json['toUser'])
				: json['toUser'].toInt();
	}
	if (json['mode'] != null) {
		data.mode = json['mode'].toString();
	}
	if (json['destination'] != null) {
		data.destination = json['destination'].toString();
	}
	if (json['type'] != null) {
		data.type = json['type'] is String
				? int.tryParse(json['type'])
				: json['type'].toInt();
	}
	if (json['content'] != null) {
		data.content = YBDUserListInfo().fromJson(json['content']);
	}
	return data;
}

Map<String, dynamic> yBDUserPublishToJson(YBDUserPublish entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['msgType'] = entity.msgType;
	data['roomId'] = entity.roomId;
	data['fromUser'] = entity.fromUser;
	data['toUser'] = entity.toUser;
	data['mode'] = entity.mode;
	data['destination'] = entity.destination;
	data['type'] = entity.type;
	data['content'] = entity.content?.toJson();
	return data;
}

yBDUserListInfoFromJson(YBDUserListInfo data, Map<String, dynamic> json) {
	if (json['totalCount'] != null) {
		data.totalCount = json['totalCount'] is String
				? int.tryParse(json['totalCount'])
				: json['totalCount'].toInt();
	}
	if (json['visitorCount'] != null) {
		data.visitorCount = json['visitorCount'] is String
				? int.tryParse(json['visitorCount'])
				: json['visitorCount'].toInt();
	}
	if (json['onlineCount'] != null) {
		data.onlineCount = json['onlineCount'] is String
				? int.tryParse(json['onlineCount'])
				: json['onlineCount'].toInt();
	}
	if (json['managerCount'] != null) {
		data.managerCount = json['managerCount'] is String
				? int.tryParse(json['managerCount'])
				: json['managerCount'].toInt();
	}
	if (json['users'] != null) {
		data.users = (json['users'] as List).map((v) => YBDUserInfo().fromJson(v)).toList();
	}
	if (json['managers'] != null) {
		data.managers = (json['managers'] as List).map((v) => YBDUserInfo().fromJson(v)).toList();
	}
	return data;
}

Map<String, dynamic> yBDUserListInfoToJson(YBDUserListInfo entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['totalCount'] = entity.totalCount;
	data['visitorCount'] = entity.visitorCount;
	data['onlineCount'] = entity.onlineCount;
	data['managerCount'] = entity.managerCount;
	data['users'] =  entity.users?.map((v) => v?.toJson())?.toList();
	data['managers'] =  entity.managers?.map((v) => v?.toJson())?.toList();
	return data;
}