import 'package:oyelive_main/ui/page/game_room/entity/quick_ybd_join_entity.dart';
import 'dart:async';

yBDQuickJoinEntityFromJson(YBDQuickJoinEntity data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code'].toString();
	}
	if (json['message'] != null) {
		data.message = json['message'].toString();
	}
	if (json['data'] != null) {
		data.data = YBDQuickJoinData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> yBDQuickJoinEntityToJson(YBDQuickJoinEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['message'] = entity.message;
	data['data'] = entity.data?.toJson();
	return data;
}

yBDQuickJoinDataFromJson(YBDQuickJoinData data, Map<String, dynamic> json) {
	if (json['roomId'] != null) {
		data.roomId = json['roomId'].toString();
	}
	if (json['matchType'] != null) {
		data.matchType = json['matchType'].toString();
	}
	if (json['waitingTime'] != null) {
		data.waitingTime = json['waitingTime'] is String
				? int.tryParse(json['waitingTime'])
				: json['waitingTime'].toInt();
	}
	return data;
}

Map<String, dynamic> yBDQuickJoinDataToJson(YBDQuickJoinData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['roomId'] = entity.roomId;
	data['matchType'] = entity.matchType;
	data['waitingTime'] = entity.waitingTime;
	return data;
}