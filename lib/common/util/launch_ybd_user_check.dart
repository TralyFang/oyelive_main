




import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/constant/i18n_ybd_key.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/set_ybd_ext.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/module/agency/agency_ybd_api.dart';
import 'package:oyelive_main/module/api.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/play_center/entity/game_ybd_check_entity.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/widget/dialog/confirm_ybd_dialog.dart';

// 涉及到用户相关的启动检测
class YBDLaunchUserCheck {

  // static checkUserGameStatus() async {
  //   Future.delayed(Duration(seconds: 10), (){
  //     _checkUserGameStatus();
  //   });
  // }

  // 检测用户游戏状态
  static checkUserGameStatus() async {
    Map<String, dynamic> params = {
      'names': "ludo,gameRoom", // 查询语聊房ludo+游戏房
    };

    YBDGameResponseBase? response = await YBDHttpUtil.getInstance().doGet<YBDGameResponseBase>(Get.context, YBDApi.QUERY_GAME_STATUS, params: params);
    logger.v("game check status resut : ${response?.toJson()}");
    var result = response?.record;
    if (result?.ludo != null && (result!.ludo!.gaming ?? false) && result.ludo!.roomId != null) {
      /// v2.4.14: 如果在游戏房了，那就直接忽略语聊房
      if (YBDObsUtil.instance().routeList.subContain(YBDNavigatorHelper.game_room_page)) return;
      YBDModuleCenter.instance().enterRoom(roomId: result.ludo!.roomId, type: EnterRoomType.OpenLudo);
    }else if (result!.gameRoom != null
        && (result.gameRoom!.gaming ?? false)
        && (result.gameRoom!.roomId != null)
        && (result.gameRoom!.subCategory != null)
    ) {
      /// 进入游戏房,已经在游戏房了就不弹窗了 v2.4.14: 改成弹窗点击确认是否回到游戏房,如果存在语聊房则退出; 取消则投降
      if (YBDObsUtil.instance().routeList.subContain(YBDNavigatorHelper.game_room_page)) return;
      YBDConfirmDialog.show(
          Get.context,
        content: YBDI18nKey.reconnectingEnterRoomTips,
        onSureCallback: (){
          if (YBDObsUtil.instance().routeList.subContain(YBDNavigatorHelper.flutter_room_list_page)) {
            // 在语聊房，去退出，在进入游戏房！
            logger.v("game check status gamerooming in flutter_room_list_page now");
            BlocProvider.of<YBDRoomBloc>(YBDLiveService.instance.data.roomContext!).exitRoom();
            Future.delayed(Duration(milliseconds: 300),(){
              YBDGameSocketApiUtil.enterViaRoute(
                  subCategory: result.gameRoom!.subCategory,
                  roomId: "${result.gameRoom!.roomId}");
            });

          }else {
            logger.v("game check status gamerooming go");
            // 不再语聊房，直接进入游戏房，里面已经判断是否在游戏了
            YBDGameSocketApiUtil.enterViaRoute(
                subCategory: result.gameRoom!.subCategory,
                roomId: "${result.gameRoom!.roomId}");
          }
        },
        onCancelCallback: () async {
          logger.v("game check status gamerooming give up");
            // 取消调用投降的接口
          var giveUpParams = {
            "roomId":result.gameRoom!.roomId,
            "category":result.gameRoom!.subCategory,
          };
          YBDHttpUtil.getInstance().doPost<YBDGameResponseResult>(
              Get.context,
              YBDApi.GAME_GIVE_UP,
              giveUpParams,
            baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null,
              isJsonData: true,
          );
        }
      );

    }
  }
}