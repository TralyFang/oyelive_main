

import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';

import 'common_ybd_util.dart';
import 'log_ybd_util.dart';
import 'numeric_ybd_util.dart';

/// 日期转换工具
class YBDDateUtil {
  /// 一天内只显示时间，否则显示 年/月/日
  static String timeFromMilliseconds(
    int milliseconds, {
    String dayFormat = 'HH:mm',
    String monthFormat = 'MM/dd HH:mm',
    String yearFormat = 'yyyy/MM/dd HH:mm',
  }) {
    if (milliseconds == null || milliseconds <= 0 || !(milliseconds is int)) {
      logger.v('milliseconds is invalid : $milliseconds');
      return '';
    }

    DateTime nowTime = DateTime.now();

    if (sameDateFromMilliseconds(milliseconds)) {
      // 一天内时间显示时间
      return DateFormat(dayFormat).format(DateTime.fromMillisecondsSinceEpoch(milliseconds));
    } else if (nowTime.year == DateTime.fromMillisecondsSinceEpoch(milliseconds).year) {
      // 一年以内的时间
      return DateFormat(monthFormat).format(DateTime.fromMillisecondsSinceEpoch(milliseconds));
    } else {
      // 一年以上的时间
      return DateFormat(yearFormat).format(DateTime.fromMillisecondsSinceEpoch(milliseconds));
    }
  }

  /// 毫秒时间戳转成日期字符串
  static String timeConvert(String? timeString) {
    DateTime dateTime;

    try {
      if (YBDNumericUtil.stringToInt(timeString) > 0) {
        // 时间戳
        dateTime = DateTime.fromMillisecondsSinceEpoch(YBDNumericUtil.stringToInt(timeString));
      } else {
        // 格式化的时间字符串
        dateTime = DateTime.parse(timeString!);
      }
    } catch (e) {
      logger.v('parse time : $timeString error : $e');
      return '';
    }

    DateTime nowTime = DateTime.now();
    int millisecBetween = nowTime.millisecondsSinceEpoch - dateTime.millisecondsSinceEpoch;

    //////一分钟内
    if (millisecBetween < Duration(seconds: 60).inMilliseconds) {
      return 'Just Now';
    }
    //////一小时内
    if (millisecBetween < Duration(hours: 1).inMilliseconds) {
      int min = Duration(milliseconds: millisecBetween).inMinutes;
      return min.toString() + (min == 1 ? ' min' : ' mins');
    }
    //////一天内
    if (millisecBetween < Duration(days: 1).inMilliseconds) {
      int hours = Duration(milliseconds: millisecBetween).inHours;
      return hours.toString() + (hours == 1 ? ' hr' : ' hrs');
    }
    //////一年内
    if (millisecBetween < 31557600000) {
      return DateFormat("MM/dd HH:mm").format(dateTime);
    }

    return DateFormat("MM/dd/yyyy HH:mm").format(dateTime);
  }

  /// 判断当前时间戳是否为同一天
  static bool sameDateFromMilliseconds(int milliseconds) {
    if (milliseconds == null || milliseconds <= 0) {
      logger.v('milliseconds is invalid : $milliseconds');
      return false;
    }

    // 生成格式化日期
    String date = DateFormat("yyyy/MM/dd").format(DateTime.fromMillisecondsSinceEpoch(milliseconds));
    String today = DateFormat("yyyy/MM/dd").format(DateTime.now());
    logger.v('date : $milliseconds, $date, today: ${DateTime.now().millisecondsSinceEpoch}, $today');

    // 判断日期字符串是否相等
    if (date == today) {
      logger.v('is today date : $milliseconds, $date, today : $today');
      return true;
    } else {
      logger.v('is not today date : $milliseconds, $date, today : $today');
      return false;
    }
  }

  /// 当前时间的毫秒时间戳
  static int currentUtcTimestamp() {
    return DateTime.now().millisecondsSinceEpoch;
  }

  /// 过去某天的毫秒时间戳
  static int pastUtcTimestamp(int pastDay) {
    return DateTime.now().millisecondsSinceEpoch - pastDay * 24 * 60 * 60 * 1000;
  }

  /// 当前时间字符串
  static String currentDate({String yearFormat = 'yyyy-MM-dd HH:mm'}) {
    return DateFormat(yearFormat).format(DateTime.fromMillisecondsSinceEpoch(currentUtcTimestamp()));
  }

  /// 当前时间字符串 到毫秒
  static String currentDateSSS({String yearFormat = 'yyyy-MM-dd HH:mm:ss.SSS'}) {
    return DateFormat(yearFormat).format(DateTime.fromMillisecondsSinceEpoch(currentUtcTimestamp()));
  }

  /// 当前日期
  static String dateWithFormat(int? milliseconds, {String yearFormat = 'yyyy-MM-dd HH:mm'}) {
    if (milliseconds == null || milliseconds < 0 || !(milliseconds is int)) {
//      logger.v('milliseconds is invalid : $milliseconds');
      return '';
    }

    return DateFormat(yearFormat).format(DateTime.fromMillisecondsSinceEpoch(milliseconds));
  }

  /// 格式化日期
  static String formatStringWithDate(DateTime date, {String format = 'yyyy-MM-dd'}) {
    return DateFormat(format).format(date);
  }

  /// 根据生日时间戳计算年龄
  static calculateAge(int? timestamp) {
    if (null == timestamp) {
      return "";
    }

    DateTime birthDate = DateTime.fromMillisecondsSinceEpoch(timestamp);
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age.toString();
  }

  static String getExpireAfter(double expireAfter) {
    String txt = 'Expire in ';

    double numberOfDays;
    double numberOfHours;
    double numberOfMinutes;
    double numberOfSeconds;
    numberOfDays = (expireAfter / 86400);
    numberOfHours = ((expireAfter % 86400) / 3600);
    numberOfMinutes = (((expireAfter % 86400) % 3600) / 60);
    numberOfSeconds = (((expireAfter % 86400) % 3600) % 60);
    String times;
    if (expireAfter == -1) {
      txt = 'lifetime';
    } else if (numberOfDays > 0) {
      times = YBDCommonUtil.formatDoubleToint(numberOfDays);
      if (numberOfDays == 1) {
        txt += "$times day";
      } else {
        txt += "$times days";
      }
    } else if (numberOfHours > 0) {
      times = YBDCommonUtil.formatDoubleToint(numberOfHours);
      if (numberOfHours == 1) {
        txt += "$times hr";
      } else {
        txt += "$times hrs";
      }
    } else if (numberOfMinutes > 0) {
      times = YBDCommonUtil.formatDoubleToint(numberOfMinutes);
      if (numberOfMinutes == 1) {
        txt += "$times min";
      } else {
        txt += "$times mins";
      }
    } else {
      txt = "expired";
    }
    return txt;
  }

  /// 超时判断
  /// [oldTime] 较早的毫秒时间戳
  /// [tagTime] 相差的毫秒时间数
  /// true: 当前时间与较早的时间差超过了指定的时间间隔，false：没有超过时间间隔
  static bool overTime(int oldTime, int tagTime) {
    if (currentUtcTimestamp() - oldTime > tagTime) {
      return true;
    }
    return false;
  }

  /// 根据日期字符串获取 DateTime
  static DateTime string2DateTime(String date, {String format = 'yyyy-MM-dd'}) {
    return DateFormat(format).parse(date);
  }

  /// 根据年份的偏移量获取 DateTime
  /// [offset] 年份偏移量
  static String getSpecificYearDate(int offset) {
    DateTime now = DateTime.now();
    DateTime dateTime = DateTime(now.year + offset, now.month, now.day);
    return formatStringWithDate(dateTime, format: 'MM/dd/yyyy');
  }

  /// vip 过期时间
  static String getExpireDate(int? expireTime) {
    if (expireTime == -1) {
      return "Lifetime";
    }

    var date = DateTime.fromMillisecondsSinceEpoch(currentUtcTimestamp() + expireTime! * 1000);
    return formatStringWithDate(date, format: 'dd MMM yyyy');
  }

  /// 秒转换为分
  /// 格式：**m **s
  /// [second] 秒数
  static String secondFormattedToMin(int second) {
    if (second > 60) {
      final secValue = second % 60;
      final minValue = second ~/ 60;
      return '$minValue m $secValue s';
    } else {
      return '$second s';
    }
  }

  /// Discount 过期时间
  static String getDiscountExpireDate(int? expireTime) {
    if (expireTime == null) {
      return '';
    }
    if (expireTime == -1) {
      return "Lifetime";
    }

    return dateWithFormat(expireTime, yearFormat: 'MM/dd/yyyy');
  }

  /// 秒转换为day
  static String getExpireDateDD(int expireTime) {
    if (expireTime >= 0) {
      print('ExpireDate expireTime:$expireTime days:${(expireTime / 86400)}');
      String piAsString = '${Duration(seconds: expireTime).inDays} Days';

      return piAsString;
    } else if (expireTime == -1) {
      return translate('forever');
    } else {
      return '';
    }
  }

  /// 当前时间字符串 到毫秒
  static int timeDateMilli(String? timeString) {
    logger.v('timeDate $timeString');
    if (timeString == null) {
      return 0;
    }
    try {
      DateTime dateTime = DateTime.parse(timeString);
      return dateTime.millisecondsSinceEpoch;
    } catch (e) {
      logger.v('timeDate : $timeString error : $e');
      return 0;
    }
  }
}
