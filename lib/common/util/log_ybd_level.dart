
import 'package:logger/src/logger.dart';

/// 用来配置输出日志的级别
enum Output {
  Verbose,
  Debug,
  Info,
  Warning,
  Error,
  Nothing,
}

/// 根据日志级别输出日志
class YBDTPLogLevel {
  /// 输出日志的级别，默认不输出日志
  static Output output = Output.Nothing;

  /// 根据配置的日志级别判断是否输出[level]级别的日志
  static bool filterWithLevel(Level level) {
    switch (output) {
      case Output.Verbose:
        // 输出全部日志
        return true;
      case Output.Debug:
        return _handleDebug(level);
      case Output.Info:
        return _handleInfo(level);
      case Output.Warning:
        return _handleWarning(level);
      case Output.Error:
        return _handleError(level);
      case Output.Nothing:
        return false;
      default:
        return false;
    }
  }

  /// 输出 debug 级别及以上级别的日志
  static bool _handleDebug(Level level) {
    // 只要屏蔽 verbose 级别的日志
    if (level == Level.verbose) {
      return false;
    }

    return true;
  }

  /// 输出 info 级别及以上级别的日志
  static bool _handleInfo(Level level) {
    if (level == Level.info || level == Level.warning || level == Level.error) {
      return true;
    }

    return false;
  }

  /// 输出 warning 级别及以上级别的日志
  static bool _handleWarning(Level level) {
    if (level == Level.warning || level == Level.error) {
      return true;
    }

    return false;
  }

  /// 输出 error 级别及以上级别的日志
  static bool _handleError(Level level) {
    if (level == Level.error) {
      return true;
    }

    return false;
  }
}
