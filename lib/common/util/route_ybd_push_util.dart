import 'package:oyelive_main/common/abstract/route/imp/enter_ybd_chat_room_route_service.dart';
import 'package:oyelive_main/common/abstract/route/imp/game_ybd_route_service.dart';
import 'package:oyelive_main/common/abstract/route/imp/index_ybd_route_service.dart';
// import 'package:oyelive_main/common/abstract/route/imp/post_ybd_slog_route_service.dart';
import 'package:oyelive_main/common/abstract/route/imp/quick_ybd_access.dart';
import 'package:oyelive_main/common/abstract/route/route_ybd_service.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/widget/dialog_ybd_wsa_support.dart';

class YBDRoutePushUtil {
  ///假路由的形式跳转
  static List<String> routeList = [
    RouteConst.gameRoute,
    RouteConst.whatsAppRoute,
    RouteConst.indexRoute,
    RouteConst.enterChatRoomRoute,
    RouteConst.postSlogRoute,
    RouteConst.gameQuickAccess
  ];

  static String? type = '-1';
  static pushRoute(String path) {
    logger.i('YBDRoutePushUtil path:$path');
    if (path.startsWith(RouteConst.whatsAppRoute))
      YBDCommonUtil.showMyDialog(YBDWSASupportDialog());

    ///跳转游戏大厅、选中指定类目
    ///根据参数 选择是否进入游戏房
    if (path.contains(RouteConst.gameRoute)) YBDGameRouteService().push(path);

    /// 跳转到首页
    if (path.contains(RouteConst.indexRoute)) YBDIndexRouteService().push(path);

    /// 跳转到某个语聊房打开xxx
    if (path.contains(RouteConst.enterChatRoomRoute))
      YBDEnterChatRoomRouteService().push(path);

    // /// 跳转到发布slog
    // if (path.contains(RouteConst.postSlogRoute)) YBDPostSlogRouteService().push(path);

    if (path.contains(RouteConst.gameQuickAccess))
      YBDQuickAccessService().push(path);
  }

  ///一些中间值还原
  static dispose() {
    YBDRoutePushUtil.type = '-1';
  }
}
