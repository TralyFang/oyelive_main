import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;

import 'package:encrypt/encrypt.dart' as v_encrypt;
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:livechat_inc/livechat_inc.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/crashlytics_ybd_util.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/int_ybd_ext.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/util/launch_ybd_user_check.dart';
import 'package:oyelive_main/common/util/uniqueid_ybd_helper.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import 'package:oyelive_main/module/room/entity/game_ybd_query_model.dart';
import 'package:oyelive_main/module/thirdparty/entity/ip_ybd_api_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_file.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_type_ext.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/widget/dialog_ybd_wsa_support.dart';
import 'package:oyelive_main/ui/page/recharge/google/google_ybd_purchase_helper.dart';
import 'package:oyelive_main/ui/page/room/entity/room_ybd_operate_info.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:redux/redux.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vector_math/vector_math.dart' as v_math;

import '../../module/entity/unique_ybd_id_entity.dart';
import '../../module/thirdparty/third_ybd_party_helper.dart';
import '../../module/user/entity/user_ybd_info_entity.dart';
import '../../redux/app_ybd_state.dart';
import '../../ui/widget/bottom_ybd_selector_sheet.dart';
import '../../ui/widget/intl_input/countries.dart';
import '../analytics/analytics_ybd_util.dart';
import '../constant/const.dart';
import '../global/tp_ybd_global.dart';
import '../http/environment_ybd_config.dart';
import '../navigator/navigator_ybd_helper.dart';
import 'config_ybd_util.dart';
import 'date_ybd_util.dart';
import 'image_ybd_util.dart';
import 'log_ybd_util.dart';
import 'numeric_ybd_util.dart';
import 'remote_ybd_config_service.dart';
import 'sp_ybd_util.dart';

class YBDCommonUtil {
  /// 毫秒时间戳转成日期字符串
  static String timeConvert(String? timeString) {
    return YBDDateUtil.timeConvert(timeString);
  }

  static String getZodicaSign(DateTime date) {
    var days = date.day;
    var months = date.month;
    if (months == 1) {
      if (days >= 21) {
        return "Aquarius";
      } else {
        return "Capricorn";
      }
    } else if (months == 2) {
      if (days >= 20) {
        return "Pisces";
      } else {
        return "Aquarius";
      }
    } else if (months == 3) {
      if (days >= 21) {
        return "Aries";
      } else {
        return "Pisces";
      }
    } else if (months == 4) {
      if (days >= 21) {
        return "Taurus";
      } else {
        return "Aries";
      }
    } else if (months == 5) {
      if (days >= 22) {
        return "Gemini";
      } else {
        return "Taurus";
      }
    } else if (months == 6) {
      if (days >= 22) {
        return "Cancer";
      } else {
        return "Gemini";
      }
    } else if (months == 7) {
      if (days >= 23) {
        return "Leo";
      } else {
        return "Cancer";
      }
    } else if (months == 8) {
      if (days >= 23) {
        return "Virgo";
      } else {
        return "Leo";
      }
    } else if (months == 9) {
      if (days >= 24) {
        return "Libra";
      } else {
        return "Virgo";
      }
    } else if (months == 10) {
      if (days >= 24) {
        return "Scorpio";
      } else {
        return "Libra";
      }
    } else if (months == 11) {
      if (days >= 23) {
        return "Sagittarius";
      } else {
        return "Scorpio";
      }
    } else if (months == 12) {
      if (days >= 22) {
        return "Capricorn";
      } else {
        return "Sagittarius";
      }
    }
    return "";
  }

  static final key = v_encrypt.Key.fromUtf8('my 32 length key................');
  static final iv = v_encrypt.IV.fromLength(16);

  static String encrypt(String text) {
    final encrypter = v_encrypt.Encrypter(v_encrypt.AES(key));
    final encrypted = encrypter.encrypt(text, iv: iv);
    logger.v(encrypted.base64);
    return encrypted.base64;
  }

  static String decrypt(String encrypted) {
    final encrypter = v_encrypt.Encrypter(v_encrypt.AES(key));
    final decrypted = encrypter.decrypt64(encrypted, iv: iv);
    return decrypted;
  }

  // static Future<String> getCountryNameByCode(String code) async {
  //   String forSearch;
  //   await YBDSPUtil.get(Const.SP_SELECT_COUNTRY_CODE).then((value) {
  //     if (value != null) {
  //       forSearch = value;
  //     } else {
  //       forSearch = code;
  //     }
  //   });
  //
  //   return getCountryByCode(forSearch);
  // }

  /// 通过国家码 获取国家名称
  static String? getCountryByCode(String? code) {
    try {
      List<Map<String, String>> result = YBDCountries.list()
          .where((element) => code == element['code'])
          .toList();

      if (null == result || result.isEmpty) {
        result = [YBDCountries.emptyCounty];
      }

      return result[0]['name'];
    } catch (e) {
      logger.v(e);
      return "In Space";
    }
  }

  /// 通过国家码 获取国家国旗
  static String? getCountryFlagByCode(String? code) {
    try {
      List<Map<String, String>> result = YBDCountries.list()
          .where((element) => code == element['code'])
          .toList();

      if (null == result || result.isEmpty) {
        result = [YBDCountries.emptyCounty];
      }
      return result[0]['flag'];
    } catch (e) {
      logger.v(e);
      return '';
    }
  }

  /// 输入手机号，默认国家码
  /// #1 SP 获取 sp_select_country_code
  /// #2 通过IP获取
  static Future<String> getDefaultCountryCode(BuildContext? context) async {
    String? defaultCode = await YBDSPUtil.get(Const.SP_SELECT_COUNTRY_CODE);
    defaultCode = defaultCode ?? await getIpCountryCode(context);
    defaultCode = defaultCode;

    return defaultCode;
  }

  /// 输入手机号，默认国家码
  /// #1 SP 获取 sp_ip_country_code
  /// #2 http://geolocation-db.com/json/
  /// #3 http://ip-api.com/json
  static Future<String> getIpCountryCode(BuildContext? context) async {
    String? ipCode = await YBDSPUtil.get(Const.SP_IP_COUNTRY_CODE);
    ipCode = ipCode ?? await YBDThirdParty.getLocation(context);
    ipCode = ipCode ?? 'PK';
    logger.v('ip code is = $ipCode');
    return ipCode;
  }

  /// 查看大图
  /// download  是否显示下载按钮，默认显示
  static String getPhotoViewUrl(List<String> imagePaths, int index,
      {bool? download}) {
    var concatenate = StringBuffer();
    imagePaths.forEach((item) {
      concatenate.write(",");
      concatenate.write(item);
    });

    return YBDNavigatorHelper.photo_viewer +
        "/${concatenate.toString().substring(1).replaceAll("/", "<>")}/$index/${download == null || download}";
  }

  static Future<String> getResourceDir(String? folderName) async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String path = (Platform.isAndroid
            ? (await getExternalStorageDirectory())!.path
            : (await getApplicationDocumentsDirectory()).path) +
        Platform.pathSeparator +
        packageInfo.appName +
        Platform.pathSeparator +
        "$folderName";

    final savedDir = Directory(path);
    if (!await savedDir.exists()) {
      logger.v('directory not exist create path : $path');
      await savedDir.create(recursive: true);
    }

    return path;
  }

  //根据角度计算点在圆弧上点X坐标
  static getXbyAngle(double r, double angle) {
    return r * math.sin(v_math.radians(angle));
  }

  //根据角度计算点在圆弧上点Y坐标

  static getYbyAngle(double r, double angle) {
    return r * math.cos(v_math.radians(angle));
  }

  static getIntFormMap(value) {
    if (value == null || value == "null") {
      return null;
    }
    if (value is int) {
      return value;
    }
    try {
      return int.parse(value);
    } catch (e) {
      print(e);

      return null;
    }
  }

  static getBooleanFormMap(value) {
    if (value == null || value == "null") {
      return null;
    }
    if (value is bool) {
      return value;
    }
    return true.toString() == value;
  }

  static Map<String, String> toStringsMap(Map<String, dynamic> map) {
    final res = <String, String>{};
    map.forEach((String k, dynamic v) => res[k] = "$v");
    return res;
  }

  static String getUnreadCount(int count) {
    if (count > Const.MESSAGE_MAX_COUNT) {
      return "${Const.MESSAGE_MAX_COUNT}+";
    } else {
      return count.toString();
    }
  }

  static String formatNum(num, {point: 3}) {
    if (num != null) {
      String str = double.parse(num.toString()).toString();
      // 分开截取
      List<String> sub = str.split('.');
      // 处理值
      List val = List.from(sub[0].split(''));
      // 处理点
      List<String> points = List.from(sub[1].split(''));
      //处理分割符
      for (int index = 0, i = val.length - 1; i >= 0; index++, i--) {
        // 除以三没有余数、不等于零并且不等于1 就加个逗号
        if (index % 3 == 0 && index != 0) val[i] = val[i] + ',';
      }
      // 处理小数点
      for (int i = 0; i <= point - points.length; i++) {
        points.add('0');
      }
      //如果大于长度就截取
      if (points.length > point) {
        // 截取数组
        points = points.sublist(0, point);
      }
      // 判断是否有长度
      if (points.length > 0) {
        return '${val.join('')}';
      } else {
        return val.join('');
      }
    } else {
      return "0";
    }
  }

  ///保留double 小数个数
  static double formatDoubleNum(double? num, int postion) {
    if ((num.toString().length - num.toString().lastIndexOf(".") - 1) <
        postion) {
      return double.parse(num!
          .toStringAsFixed(postion)
          .substring(0, num.toString().lastIndexOf(".") + postion + 1)
          .toString());
    } else {
      return double.parse(num.toString()
          .substring(0, num.toString().lastIndexOf(".") + postion + 1)
          .toString());
    }
  }

  ///保留double转String 去除小数
  static String formatDoubleToint(double num) {
    return num.toString().substring(0, num.toString().lastIndexOf("."));
  }

  /// 是否为动图
  static bool isSvgaFile(String? imgUrl) {
    int type = getFileType(imgUrl ?? '');

    // GIF：2，svga：3
    if (type == 2 || type == 3) {
      return true;
    }

    return false;
  }

  ///通过文件名后缀判断文件类型 0 未知或文件名为空  1 png、JPG  2 gif  3 svga
  static int getFileType(String? fileName) {
    int type = 0;
    if (fileName == null || fileName.isEmpty) {
      type = 0;
    } else if (fileName.toUpperCase().endsWith('JPG') ||
        fileName.toUpperCase().endsWith('PNG')) {
      type = 1;
    } else if (fileName.toUpperCase().endsWith('GIF')) {
      type = 2;
    } else if (fileName.toUpperCase().endsWith('SVGA')) {
      type = 3;
    }
    print("getFileType type: $type");
    return type;
  }

  /// 从Firebase数据库解析Unique ID有效期
  static String? parseValidity(YBDUniqueIDEntity source) {
    if (source.validity == null || source.validity!.isEmpty) return null;

    try {
      // TEST format validity
      // null 或者 空
      // 28th, April 2020
      // 29,Nov,2019
      // 30, April 2020
      // 8th May, 2020
      String validity = source.validity!
          .replaceAll('st', '') // 1st -> 1
          .replaceAll('nd', '') // 2nd -> 2
          .replaceAll('rd', '') // 3rd -> 3
          .replaceAll('th', '') // 4th -> 4
          .replaceAll('  ', ' ')
          .replaceAll(', ', ' ') // remove ','
          .replaceAll(',', ' ');
      List<String> temp = validity.split(' ');
      validity = '${temp[0]} ${temp[1].substring(0, 3)} ${temp[2]}';

      String pattern = temp[0].length == 1 ? 'd MMM yyyy' : 'dd MMM yyyy';
      String result =
          DateFormat('yyyy-MM-dd').format(DateFormat(pattern).parse(validity));
      return result;
    } catch (e) {
      logger.v(
          "parse unique id validity error: $e, (${source.userId}, ${source.uniqueId}, ${source.validity})");
      return null;
    }
  }

  /// 查询用户的Lucky ID
  static String? getUserLuckyID(BuildContext context, int? userID) {
    Store<YBDAppState>? store =
        YBDCommonUtil.storeFromContext(context: context);
    List<YBDUniqueIDEntity?>? uniques = store?.state.configs?.uniqueIds;

    if (uniques == null || uniques.isEmpty) return null;

    List<YBDUniqueIDEntity?> found =
        uniques.where((YBDUniqueIDEntity? element) {
      return '${element!.userId}' == '$userID';
    }).toList();
    if (found == null || found.isEmpty) {
      return null;
    }

    /// 检查Lucky ID是否过期
    if (DateTime.now()
            .compareTo(YBDDateUtil.string2DateTime(found[0]!.validity!)) >
        0) {
      logger.v(
          "$userID 's Unique ID: ${found[0]!.uniqueId}, validity: ${found[0]!.validity} has expired.");
      return null;
    }

    logger.v(
        "$userID has valid Unique ID: ${found[0]!.uniqueId}, validity: ${found[0]!.validity}");
    return '${found[0]!.uniqueId}';
  }

  /// 通过Lucky ID， 查询用户ID
  static Future<String?> getUserIDByLuckyID(
      BuildContext context, String luckyID) async {
    List<YBDUniqueIDEntity?>? uniques = await ConfigUtil.uniqueIds(context);
    logger.v("Unique ID list from redux: $uniques");

    if (uniques == null || uniques.isEmpty) return null;

    List<YBDUniqueIDEntity?> found =
        uniques.where((YBDUniqueIDEntity? element) {
      return '${element!.uniqueId}' == luckyID;
    }).toList();
    if (found == null || found.isEmpty) {
      logger.v("$luckyID does not exist!");
      return null;
    }

    /// 检查Lucky ID是否过期
    if (DateTime.now()
            .compareTo(YBDDateUtil.string2DateTime(found[0]!.validity!)) >
        0) {
      logger.v(
          "$luckyID belongs to ${found[0]!.userId}, validity: ${found[0]!.validity} has expired.");
      return null;
    }

    logger.v(
        "$luckyID belongs to ${found[0]!.userId}, validity: ${found[0]!.validity}");
    return '${found[0]!.userId}';
  }

  /// 查询登录显示手机短信登录的配置项
  static Future<String?> getSMSLogin(BuildContext context) async {
    String? sms = await ConfigUtil.loginShowSMS(context);
    return sms;
  }

  /// 判断当前用户所处的国家是否为印度
  static Future<bool> _isIndiaUser() async {
    var country;
    logger.v('default country is: $country');

    if ((storeFromContext()!.state.bean?.country ?? '').isNotEmpty) {
      // 优先使用个人信息中的国家码
      country = storeFromContext()!.state.bean!.country;
      logger.v('user country is: $country');
    } else {
      // 获取默认的国家码
      country = await YBDCommonUtil.getDefaultCountryCode(Get.context);
      logger.v('default country is: $country');
    }

    if (country == 'IN') {
      logger.v('open india live chat page');
      // 印度用户
      return true;
    }

    return false;
  }

  static showSupport(BuildContext context) async {
    bool showLevelSupport = canShowWSASupport();
    List<String> opts = Const.TEST_ENV
        ? [
            translate('high_level_exclusive_service'),
            translate('support_livechat'),
            translate('support_facebook'),
            'change_environment'
          ]
        : [
            translate('high_level_exclusive_service'),
            translate('support_livechat'),
            translate('support_facebook'),
          ];
    if (!showLevelSupport) opts.removeAt(0);
    logger.v(
        '11.17-----showLevelSupport:$showLevelSupport,opts length:${opts.length}');
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return YBDBottomSelectorSheet(
            opts: opts,
            onSelectAt: (index) async {
              logger.v('===support selected at $index');

              switch (index) {
                case 0:
                  if (showLevelSupport) {
                    showMyDialog(YBDWSASupportDialog());
                  } else {
                    openLiveChat();
                  }
                  break;
                case 1:
                  if (showLevelSupport) {
                    openLiveChat();
                  } else {
                    openFbSupport();
                  }
                  break;
                case 2:
                  if (showLevelSupport) {
                    openFbSupport();
                  } else {
                    YBDCommonUtil.showCurrentEnvironment(context);
                  }
                  break;
                case 3:
                  if (showLevelSupport) {
                    YBDCommonUtil.showCurrentEnvironment(context);
                  } else {
                    logger.v('===support selected at $index');
                  }
                  break;
              }
            },
          );
        });
  }

  static bool canShowWSASupport() {
    Store<YBDAppState> store = storeFromContext(context: Get.context)!;
    logger.v(
        '11.17-----level:${store.state.bean?.level ?? 0}--showWsaSupportLevel:${YBDCommonUtil.getRoomOperateInfo().showWsaSupportLevel}');
    return (store.state.bean?.level ?? 0) >=
        YBDCommonUtil.getRoomOperateInfo().showWsaSupportLevel;
  }

  static Future<void> openLiveChat() async {
    // 默认打开巴基斯坦的客服页面
    var licence = Const.LIVECHAT_KEY_LICENCE;
    if (await _isIndiaUser()) {
      // 印度用户打开印度客服页面
      licence = Const.LIVECHAT_KEY_LICENCE_INDIA;
      logger.v('open india live chat page');
    }
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
    // YBDUserInfo userInfo = YBDUserUtil.getLoggedUser(Get.context);
    LivechatInc.start_chat(
      licence,
      "",
      userInfo?.nickname ?? 'Visitor',
      userInfo?.email ?? 'No Email',
    );
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
        itemName: 'support_livechat'));
  }

  static Future<void> openFbSupport() async {
    try {
      bool launched =
          await launch(Const.FACEBOOK_APP_IOS, forceSafariVC: false);

      if (!launched) {
        await launch(Const.FACEBOOK_WEB, forceSafariVC: false);
      }
    } catch (e) {
      await launch(Const.FACEBOOK_WEB, forceSafariVC: false);
    }
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
        itemName: 'support_facebook'));
  }

  ///切换当前网络环境
  static showCurrentEnvironment(BuildContext context) async {
    if ((await YBDSPUtil.get(YBDEnvConfig.envKey)).toString() != null) {
      YBDEnvConfig.currentSelectType = EnvTypeExt.getType(
          (await YBDSPUtil.get(YBDEnvConfig.envKey)).toString());
    }
    List<String> opts = [
      "Test 测试: ${YBDEnvConfig.getGreyOrTestPath(EnvType.Test)}",
      "Pet vm测试: ${YBDEnvConfig.getGreyOrTestPath(EnvType.Pet)}",
      "Pre 预生产: ${YBDEnvConfig.getGreyOrTestPath(EnvType.Pre)}",
      "Dev 开发: ${YBDEnvConfig.getGreyOrTestPath(EnvType.Dev)}",
      "Prod 生产: ${YBDEnvConfig.getGreyOrTestPath(EnvType.Prod)}",
      "Current_SelectEnv: ${YBDEnvConfig.getCurrentEnvPath()}"
    ];
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return YBDBottomSelectorSheet(
            opts: opts,
            onSelectAt: (index) async {
              switch (index) {
                case 0:
                  YBDSPUtil.save(YBDEnvConfig.envKey, EnvType.Test.getKey());
                  YBDEnvConfig.currentSelectType = EnvType.Test;
                  print(
                      "currentEnv : ${await YBDSPUtil.get(YBDEnvConfig.envKey)}, ${YBDEnvConfig.getCurrentEnvPath()}");
                  break;
                case 1:
                  YBDSPUtil.save(YBDEnvConfig.envKey, EnvType.Pet.getKey());
                  YBDEnvConfig.currentSelectType = EnvType.Pet;
                  print(
                      "currentEnv1 : ${await YBDSPUtil.get(YBDEnvConfig.envKey)}, ${YBDEnvConfig.getCurrentEnvPath()}");
                  break;
                case 2:
                  YBDSPUtil.save(YBDEnvConfig.envKey, EnvType.Pre.getKey());
                  YBDEnvConfig.currentSelectType = EnvType.Pre;
                  print(
                      "currentEnv1 : ${await YBDSPUtil.get(YBDEnvConfig.envKey)}, ${YBDEnvConfig.getCurrentEnvPath()}");
                  break;
                case 3:
                  YBDSPUtil.save(YBDEnvConfig.envKey, EnvType.Dev.getKey());
                  YBDEnvConfig.currentSelectType = EnvType.Dev;
                  print(
                      "currentEnv1 : ${await YBDSPUtil.get(YBDEnvConfig.envKey)}, ${YBDEnvConfig.getCurrentEnvPath()}");
                  break;
                case 4:
                  YBDSPUtil.save(YBDEnvConfig.envKey, EnvType.Prod.getKey());
                  YBDEnvConfig.currentSelectType = EnvType.Prod;
                  print(
                      "currentEnv1 : ${await YBDSPUtil.get(YBDEnvConfig.envKey)}, ${YBDEnvConfig.getCurrentEnvPath()}");
                  break;
              }
            },
          );
        });
  }

  /// 查询充值活动配置项
  static Future<List<String>> getTopupOffer(BuildContext context) async {
    String? topupOffer = await ConfigUtil.topupOffer(context);
    if (null != topupOffer) {
      List<String> configs = topupOffer.split('|');
      if (configs.length == 5 &&
          YBDNumericUtil.isNumeric(configs[0]) &&
          YBDNumericUtil.isNumeric(configs[1]) &&
          YBDImageUtil.isFullImagePath(configs[2]) &&
          YBDImageUtil.isFullImagePath(configs[3]) &&
          YBDNumericUtil.isNumeric(configs[4])) {
        return configs;
      }
    }
    return [];
  }

  static Future<String> compressImage(String path,
      {int compressWidth: 720}) async {
    File _file = File(path);
    String name = _file.path.toLowerCase();
    if (name.endsWith(".jpg") ||
        name.endsWith(".jpeg") ||
        name.endsWith(".png") ||
        (name.endsWith(".webp") && Platform.isAndroid)) {
      YBDRemoteConfigService? r = await YBDRemoteConfigService.getInstance();
      dynamic quality =
          r?.getConfig()?.getString("status_picture_upload_quality");
      quality = (quality ?? '') == '' ? 90 : int.parse(quality);
      var decodedImage = await decodeImageFromList(_file.readAsBytesSync());
      int minWidth, minHeight;
      if (decodedImage.width <= compressWidth) {
        minWidth = decodedImage.width;
        minHeight = decodedImage.height;
      } else {
        minWidth = compressWidth;
        minHeight = compressWidth * decodedImage.height ~/ decodedImage.width;
      }
      String nowName = _file.path.substring(_file.path.lastIndexOf('/') + 1);
      nowName = '${DateTime.now().toIso8601String()}_$nowName';
      String targetPath = (await YBDCommonUtil.getResourceDir(Const.TEMP)) +
          Platform.pathSeparator +
          nowName;
      try {
        File? compressedFile = await FlutterImageCompress.compressAndGetFile(
            _file.path, targetPath,
            quality: quality,
            minWidth: minWidth,
            minHeight: minHeight,
            format: name.endsWith(".jpg") || name.endsWith(".jpeg")
                ? CompressFormat.jpeg
                : name.endsWith(".png")
                    ? CompressFormat.png
                    : CompressFormat.webp);
        return compressedFile?.path ?? '';
      } catch (e) {
        print(e);
        return path;
      }
    }

    return path;
  }

  /// 查询是否显示首页icon配置项  0-不显示  1-显示
  static bool getShowHomeIcon(BuildContext context) {
    Store<YBDAppState> store =
        YBDCommonUtil.storeFromContext(context: context)!;
    String? showHomeIcon = store.state.configs?.showHomeIcon;
    return showHomeIcon == '1';
  }

  /// userId 用户ID
  /// purchaseUserId 支付用户ID（订单通过透传拿到的，有可能和现在的用户ID不是一个）
  static storeLostGoogleOrder(
      dynamic userId, dynamic purchaseUserId, String orderId, String status,
      {String? signature, String? purchaseToken, String? otOrderId}) async {
    try {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();

      /// abnormal_event /
      ///   google_inapp_purchase /
      ///     / date
      ///           user_id :
      ///           order_status :
      ///           order_id :
      ///           purchase_time :
      ///           app_version :
      DatabaseReference databaseReference = FirebaseDatabase.instance
          .reference()
          .child('abnormal_event')
          .child('google_inapp_purchase');
      databaseReference
          .child(YBDDateUtil.formatStringWithDate(DateTime.now()))
          .push()
          .set({
        'user_id': userId,
        'purchase_user_id': purchaseUserId,
        'order_status': status,
        'order_id': orderId,
        'signature': signature ?? '',
        'app_version': packageInfo.version,
        'purchase_token': purchaseToken ?? '',
        'ot_order_id': otOrderId ?? '',
        'transparent_method': YBDGooglePurchaseHelper.transparentMethod,
      });
    } catch (e) {
      YBDCrashlyticsUtil.instance!
          .report(e, extra: 'firebase database error: $e');
    }
  }

  ////Android其它支付记录（用于收集日志，分析定位）
  static storeOtherOrder(
      int? userId, String orderId, String status, String child,
      {String? otOrderId}) async {
    try {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();

      /// abnormal_event /
      ///   google_inapp_purchase /
      ///     / date
      ///           user_id :
      ///           order_status :
      ///           order_id :
      ///           purchase_time :
      ///           app_version :
      DatabaseReference databaseReference = FirebaseDatabase.instance
          .reference()
          .child('abnormal_event')
          .child(child);
      databaseReference
          .child(YBDDateUtil.formatStringWithDate(DateTime.now()))
          .push()
          .set({
        'user_id': userId,
        'order_status': status,
        'order_id': orderId,
        'app_version': packageInfo.version,
        'ot_order_id': otOrderId ?? ''
      });
    } catch (e) {
      YBDCrashlyticsUtil.instance!
          .report(e, extra: 'firebase database error: $e');
    }
  }

  static Future<void> storeAppleOrder(
    String orderId,
    String? receipt,
    String status, {
    String result = '',
    bool retry = false,
    String purchase = '',
    String productId = '',
    String thirdPartyId = '',
  }) async {
    try {
      int? userId = await YBDUserUtil.id();
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      DatabaseReference databaseReference = FirebaseDatabase.instance
          .reference()
          .child('abnormal_event')
          .child('apple_inapp_purchase');
      databaseReference
          .child(YBDDateUtil.formatStringWithDate(DateTime.now()))
          .push()
          .set({
        'date': YBDDateUtil.currentDateSSS(),
        'result': result,
        'user_id': userId,
        'order_status': status,
        'order_id': orderId,
        'receipt': receipt,
        'app_version': packageInfo.version,
        'retry': retry,
        'thirdPartyId': thirdPartyId,
        'purchase': purchase,
        'productId': productId,
      });
      logger.i('$result, $status, $orderId, $retry, $thirdPartyId, $productId');
    } catch (e) {
      YBDCrashlyticsUtil.instance!.report('apple : ${e.toString()}',
          extra: 'firebase database error: $e');
    }
  }

  /// 保存用户日志
  static void storeUserLog(String content) {
    String userId = '${YBDCommonUtil.storeFromContext()!.state.bean?.id}';
    final databaseReference = FirebaseDatabase.instance
        .reference()
        .child('log_event')
        .child('user_log');
    databaseReference
        .child(YBDDateUtil.formatStringWithDate(DateTime.now()))
        .push()
        .set({
      'log': '$userId $content',
    });
  }

  /// app版本号
  static String? _appVersion;

  static Future<String?> getAppVersion() async {
    if (null == _appVersion) {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      _appVersion = packageInfo.version;
    }

    return _appVersion;
  }

  /// 获取 store
  /// 可以试下 null safety Store<YBDAppState>? store; 避免返回 null 调 disopatch 时报错
  static Store<YBDAppState>? storeFromContext({BuildContext? context}) {
    // assert(null != YBDTPGlobal.store, 'store is null');
    if (null == YBDTPGlobal.store) {
      logger.e('store is null');
    }
    return YBDTPGlobal.store;

    // 取 store 的旧方法
    // Store<YBDAppState> store;
    // try {
    //   // 获取 store 对象
    //   store = StoreProvider.of(context);
    // } catch (e) {
    //   logger.v('get store : $store from context : $context');
    // }
    //
    // return store;
  }

  ///数字大小比较
  static bool numberComparison(int sourceNumber, int tagNumber) {
    logger
        .v("numberComparison sourceNumber:$sourceNumber tagNumber:$tagNumber");
    return sourceNumber > tagNumber;
  }

  static bool isShowPeriodDay(String? timeString, int tagNumber) {
    return numberComparison(YBDDateUtil.timeDateMilli(timeString), tagNumber);
  }

  ////rtm信令与服务端状态差异
  static storeRtmState(
      int? userId,
      int? pkId,
      int? roomId_initiator,
      int? roomId_recipient,
      int? pk_start_time,
      int? pk_end_time,
      int? rtm_state,
      int? service_state) async {
    logger.d(
        "storeRtmState userId:$userId ,pkId:$pkId ,roomId_initiator:$roomId_initiator,roomId_recipient:$roomId_recipient,pk_start_time:$pk_start_time,"
        "pk_end_time:$pk_end_time ,rtm_state:$rtm_state,service_state:$service_state");
    try {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      DatabaseReference databaseReference = FirebaseDatabase.instance
          .reference()
          .child('abnormal_event')
          .child('rtm');
      databaseReference
          .child(YBDDateUtil.formatStringWithDate(DateTime.now()))
          .push()
          .set({
        'user_id': userId,
        'pk_id': pkId,
        'roomId_initiator': roomId_initiator,
        'roomId_recipient': roomId_recipient,
        'pk_start_time': pk_start_time,
        'pk_end_time': pk_end_time,
        'rtm_state': rtm_state,
        'service_state': service_state,
        'app_version': packageInfo.version,
      });
    } catch (e) {
      YBDCrashlyticsUtil.instance!
          .report(e, extra: 'firebase database error: $e');
    }
  }

  /// 声网api调用异常情况
  static storeEngineApiError(int userId, String api, String result) async {
    logger.d("storeEngineApiError userId:$userId ,api:$api ,result:$result");
    try {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      DatabaseReference databaseReference = FirebaseDatabase.instance
          .reference()
          .child('abnormal_event')
          .child('engineApiError');
      databaseReference
          .child(YBDDateUtil.formatStringWithDate(DateTime.now()))
          .push()
          .set({
        'user_id': userId != null ? userId : await YBDSPUtil.getUserId(),
        'api': api,
        'result': result,
        'app_version': packageInfo.version,
      });
    } catch (e) {
      YBDCrashlyticsUtil.instance!.report(e);
    }
  }

  /// 记录下载皮肤报错的日志
  static storeSkinError(String error) async {
    logger.v("store skin error: $error");
    try {
      int? userId = YBDCommonUtil.storeFromContext()!.state.bean?.id;
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      DatabaseReference databaseReference = FirebaseDatabase.instance
          .reference()
          .child('abnormal_event')
          .child('skinError');
      databaseReference
          .child(YBDDateUtil.formatStringWithDate(DateTime.now()))
          .push()
          .set({
        'user_id': userId,
        'folder': YBDActivityFile.instance!.logSkinFolderName(),
        'zip': YBDActivityFile.instance!.logSkinZipName(),
        'files': YBDActivityFile.instance!.logSkinFileNames(),
        'config': YBDActivityFile.instance!.logSkinConfig(),
        'error': error,
        'app_version': packageInfo.version,
      });
    } catch (e) {
      logger.e('store skin firebase log error: $e');
    }
  }

  static Future<Map> generateCommonHeader() async {
    /// 请求Header 增加公共消息头：
    /// ot-lang: language, 用户设置的APP语言，en|zh
    /// ot-c: channel, 应用市场，Google或oppo 等
    /// ot-p: platform, 平台，Android或iOS
    /// ot-vc: APP version code
    /// Cookie: jsessionid; app     鉴权token相关
    String cookieCombine = await YBDUserUtil.userToken();

    // TODO: 测试代码，获取 cookie
    logger.d("cookie combine : $cookieCombine");
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String locale =
        (await YBDSPUtil.get(Const.SP_LOCALE)) ?? Const.DEFAULT_LANG;
    String uId = await YBDUniqueIdHelper.getInstance()!.getUniqId();

    ///TODO 实时定位的位置ip
    var headers = {
      'Accept': 'application/json,*/*',
      'YBDContent-Type': 'application/json',
      'Cookie': cookieCombine,
      'ot-lang': locale,
      'ot-c': 'Google', // TODO channel, 应用市场，Google或oppo 等
      'ot-p': Platform.operatingSystem, //ios
      'ot-vc': packageInfo.buildNumber,
      'ot-v': packageInfo.version,
      'ot-an': 'OL',
      'ot_p': Platform.operatingSystem,
      'ot_vc': packageInfo.buildNumber,
      // 'ot-vc': 200,
      'ot-did': uId,
      'ot-ipl': await YBDSPUtil.get(Const.SP_IP_COUNTRY_CODE), // ip 国家代码
      'Connection': 'keep-alive',
      // 'distinctId': TA.distinctId // 数数访客ID
      'distinctId': await TA.ta?.getDistinctId(), // 数数访客ID
    };
    log("header : \n$headers");
    return headers;
  }

  static bool checkPwdLegal(String pwd,
      {String regex: "^(?:(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])).{8,16}\$"}) {
    return RegExp(regex).hasMatch(pwd);
  }

  static YBDUserCertificationRecordCertificationInfos? getUsersSingleCer(
      List<YBDUserCertificationRecord?>? record, int? userId) {
    if (record != null && record.isNotEmpty) {
      YBDUserCertificationRecord? userCertificationRecord =
          record.firstWhereOrNull(
        (element) => element!.userId == userId, /*orElse: () => null*/
      );
      if (userCertificationRecord != null &&
          userCertificationRecord.certificationInfos != null &&
          userCertificationRecord.certificationInfos!.length != 0) {
        return userCertificationRecord.certificationInfos!.first;
      }
    }
    return null;
  }

  static Future<void> setGreedyGameUrl() async {
    YBDCommonUtil.getDefaultCountryCode(Get.context).then((value) {
      // 获取国家码
      logger.v('getDefaultCountryCode countryCode: $value');
      ApiHelper.queryGame(Get.context, value).then((gameEntity) {
        // 请求游戏数据
        if (gameEntity == null || gameEntity.returnCode != "000000") {
          // 刷新数据失败
          logger.v('11.9----queryGame failed');
        } else {
          logger.v('11.9----queryGame SUCCESS');
          if (gameEntity.record!.length > 0) {
            YBDGameQueryModel.modelAssemble(gameEntity.record);
            // for (int i = 0; i < gameEntity.record.length; i++) {
            //   if (gameEntity.record[i].name.contains('Greedy')) {
            //     YBDTPGlobal.greedyGameUrl = gameEntity.record[i].url ?? '';
            //     YBDTPGlobal.greedyGameIcon = gameEntity.record[i].icon ?? '';
            //     logger.v('11.9---------greedyGameUrl:${YBDTPGlobal.greedyGameUrl}');
            //   } else if (gameEntity.record[i].name.contains('Ludo')) {
            //     YBDTPGlobal.ludoGameUrl = gameEntity.record[i].url ?? '';
            //     YBDTPGlobal.ludoGameIcon = gameEntity.record[i].icon ?? '';
            //     logger.v('12.22---------ludoGameUrl:${YBDTPGlobal.ludoGameUrl}');
            //   }
            // }
            // 开始检测ludo游戏状态
            YBDLaunchUserCheck.checkUserGameStatus();
          } else {
            logger
                .v('11.8-------_gameData.length:${gameEntity.record!.length}');
          }
        }
      });
    });
  }

  static bool couldShowGreedyGame() {
    Store<YBDAppState> store =
        YBDCommonUtil.storeFromContext(context: Get.context)!;
    String showGreedy = store.state.configs?.showGreedy ?? '0';
    if (showGreedy == '1' &&
        (GameType.GameTypeGreedy.info()?.url?.isNotEmpty ?? false) &&
        (GameType.GameTypeGreedy.info()?.icon!.isNotEmpty ?? false)) {
      return true;
    }
    return false;
  }

  static bool shouldShowLudoGame() {
    // ignore: null_aware_in_logical_operator
    if ((GameType.GameTypeLudo.info()?.url?.isNotEmpty ?? false) &&
        (GameType.GameTypeLudo.info()?.icon?.isNotEmpty ?? false)) {
      return true;
    }
    return false;
  }

  static bool shouldShowLucky1000() {
    // ignore: null_aware_in_logical_operator
    logger.v(
        'game type lucky1000 url: ${GameType.GameTypeLucky1000.info()?.url}, ${GameType.GameTypeLucky1000.info()?.icon}');
    if ((GameType.GameTypeLucky1000.info()?.url?.isNotEmpty ?? false) &&
        (GameType.GameTypeLucky1000.info()?.icon?.isNotEmpty ?? false)) {
      return true;
    }
    return false;
  }

  static Map<String, dynamic>? getTpG2BConfig() {
    Map<String, dynamic>? tpG2BConfig;
    Store<YBDAppState> store =
        YBDCommonUtil.storeFromContext(context: Get.context)!;
    String tpG2BConfigStr = store.state.configs!.tpGems2Beans ?? '';
    logger.v('11.22---tpG2BConfigStr:$tpG2BConfigStr');
    if (tpG2BConfigStr.isEmpty) {
      logger.v('11.22---tpG2BConfigStr:empty');
      return Map();
    }
    tpG2BConfig = jsonDecode(tpG2BConfigStr);
    return tpG2BConfig;
  }

  static bool couldShowTPGems2Beans() {
    bool gemsEnough = false;
    bool? userInScope = false;
    Map<String, dynamic> tpG2BConfig = YBDCommonUtil.getTpG2BConfig()!;
    if (tpG2BConfig.isEmpty) return false;

    gemsEnough = YBDUserUtil.getLoggedUser(Get.context)!.gems! >=
        int.parse(tpG2BConfig['least_gem']);
    logger.v('11.17-----scope:${tpG2BConfig['scope']}');
    if (tpG2BConfig['scope'] == Const.TP_G2B_SCOPE_NONE) {
      logger.v('11.17-----scope:TP_G2B_SCOPE_NONE');
      userInScope = false;
    } else if (tpG2BConfig['scope'] == Const.TP_G2B_SCOPE_ONLY_OT) {
      logger.v('11.17-----scope:TP_G2B_SCOPE_ONLY_OT,');
      logger.v('11.17-----isOfficialTalent:${YBDTPGlobal.isOfficialTalent}');
      userInScope = YBDTPGlobal.isOfficialTalent;
    } else if (tpG2BConfig['scope'] == Const.TP_G2B_SCOPE_ALL) {
      logger.v('11.17-----scope:TP_G2B_SCOPE_ALL');
      userInScope = true;
    }
    logger.v('11.17-----gemsEnough:$gemsEnough--userInScope:$userInScope');
    return gemsEnough && userInScope!;
  }

  /// 不重复弹的弹框
  static void showMyDialog(Widget widget, {int? dialogId}) {
    logger.v('dialogId:$dialogId');
    if (!YBDLiveService.instance.dialogManager.isShowing(dialogId)) {
      YBDLiveService.instance.dialogManager.add(dialogId);
      try {
        showDialog(
            context: Get.context!,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return widget;
            }).then((value) {
          YBDLiveService.instance.dialogManager.remove(dialogId);
        });
      } catch (e) {
        YBDLiveService.instance.dialogManager.remove(dialogId);
      }
    }
  }

  static YBDRoomOperateInfo getRoomOperateInfo() {
    final store = YBDCommonUtil.storeFromContext(context: Get.context)!;
    Map? result;
    try {
      result = jsonDecode(store.state.configs!.roomOperate!);
    } on Error catch (error) {
      logger.v('config error');
    }
    YBDRoomOperateInfo info = YBDRoomOperateInfo.get(result);
    return info;
  }

  static get getTermsURL {
    if (Platform.isIOS) {
      return YBDCommonUtil.getRoomOperateInfo().termsIosUrl;
    }
    return YBDCommonUtil.getRoomOperateInfo().termsAndroidUrl;
  }

  // 过滤不符合userid位数的用户
  static List<int> filterAbnormalUserId(List<int> userIds) {
    logger.v("filterAbnormalUserId before : $userIds");

    // 不是pK状态就不过滤了，这个拿不到状态
    // if (YBDLiveAgoraRtmService.instance.agoraRtmInfoStart?.pk_state != 1) {
    //   logger.e("filterAbnormalUserId YBDPkStateBloc is not pking "
    //       "state: ${YBDLiveAgoraRtmService.instance.agoraRtmInfoStart?.pk_state}");
    //   return userIds;
    // }

    // 默认"100000,99999999"
    var values = YBDCommonUtil.getRoomOperateInfo().rightUserIdRange.split(',');
    var beginValue = int.tryParse(values.first);
    var endValue = int.tryParse(values.last);

    if (beginValue != null &&
        endValue != null &&
        userIds != null &&
        beginValue < endValue &&
        userIds.length > 0) {
      var newUserIds = userIds
          .where((element) => (element >= beginValue && element <= endValue))
          .toList();
      // userIds.removeWhere((element) => (element<beginValue || element>endValue));
      logger.v("filterAbnormalUserId after : $newUserIds");
      return newUserIds;
    }
    return userIds;
  }

  static String getUnitName(int? type, String? unit) {
    String unitTxt = '';
    switch (type) {
      case 1: // day
        if (int.parse(unit!) > 1) {
          unitTxt = " days";
        } else {
          unitTxt = " day";
        }
        break;
      case 3: // month
      default:
        if (int.parse(unit!) > 1) {
          unitTxt = " months";
        } else {
          unitTxt = " month";
        }
        break;
    }
    return unitTxt;
  }

  static String getCurrencyImg(String? currency) {
    wlog("getCurrencyImg-currency", currency);
    switch (currency) {
      case Const.CURRENCY_BEAN:
        return 'assets/images/topup/y_top_up_beans@2x.webp';
        break;
      case Const.CURRENCY_GEM:
        return 'assets/images/icon_gems.webp';
        break;
      case Const.CURRENCY_GOLD:
        return 'assets/images/icon_gold.webp';
        break;
      default:
        return 'assets/images/topup/y_top_up_beans@2x.webp';
        break;
    }
  }

  static String getCurrencyGreyImg(String? currency) {
    wlog("getCurrencyGreyImg-currency", currency);
    switch (currency) {
      case Const.CURRENCY_BEAN:
        return 'assets/images/beans_grey.webp';
        break;
      case Const.CURRENCY_GEM:
        return 'assets/images/gems_grey.webp';
        break;
      case Const.CURRENCY_GOLD:
        return 'assets/images/golds_grey.webp';
        break;
      default:
        return 'assets/images/beans_grey.webp';
        break;
    }
  }

  static String getBalanceByCurrency(String? currency) {
    YBDUserInfo? u = YBDUserUtil.getLoggedUser(Get.context);
    switch (currency) {
      case Const.CURRENCY_BEAN:
        wlog("getBalanceByCurrency-money", formatNum(u!.money));
        return formatNum(u.money);
        break;
      case Const.CURRENCY_GEM:
        wlog("getBalanceByCurrency-gems", formatNum(u!.gems));
        return formatNum(u.gems);
        break;
      case Const.CURRENCY_GOLD:
        wlog("getBalanceByCurrency-golds", formatNum(u!.golds));
        return formatNum(u.golds);
        break;
      default:
        return formatNum(u!.money);
        break;
    }
  }

  static bool checkIsEnoughByCurrency(String? currency, String price) {
    wlog("checkIsEnoughByCurrency-currency", currency,
        desc2: 'price', msg2: price);
    YBDUserInfo? u = YBDUserUtil.getLoggedUser(Get.context);
    int p = int.parse(price);
    switch (currency) {
      case Const.CURRENCY_BEAN:
        return u!.money! >= p;
        break;
      case Const.CURRENCY_GEM:
        return u!.gems! >= p;
        break;
      case Const.CURRENCY_GOLD:
        return u!.golds! >= p;
        break;
      default:
        return u!.money! >= p;
        break;
    }
  }

  static void wlog(String desc, String? msg,
      {String desc2 = '', String? msg2 = ''}) {
    DateTime now = DateTime.now();
    if (desc2.isNotEmpty && (msg2?.isNotEmpty ?? false)) {
      print(
          'wlog:${now.year}-${now.month}-${now.day}---$desc:$msg---$desc2:$msg2');
    } else {
      print('wlog:${now.year}-${now.month}-${now.day}---$desc:$msg');
    }
  }

  // 余额不足跳转充值页
  static void gotoRechargePage(String? currency) {
    final Store<YBDAppState>? store =
        YBDCommonUtil.storeFromContext(context: Get.context);
    YBDCommonUtil.wlog("gotoRechargePage-currency", currency);
    String? url;
    switch (currency) {
      case Const.CURRENCY_BEAN:
        YBDNavigatorHelper.openTopUpPage(Get.context);
        break;
      case Const.CURRENCY_GEM:
        url = store!.state.configs!.gemsBeans;
        YBDNavigatorHelper.navigateToWeb(
            Get.context, "?url=${Uri.encodeComponent(url!)}&showNavBar=false");
        break;
      case Const.CURRENCY_GOLD:
        url = store!.state.configs!.beans2GoldsUrl ??
            'http://121.37.214.86/talent/#/exchangeGold';
        YBDNavigatorHelper.navigateToWeb(
            Get.context, "?url=${Uri.encodeComponent(url)}&showNavBar=false");
        break;
      default:
        YBDNavigatorHelper.openTopUpPage(Get.context);
        break;
    }
  }

  /// 是否限制购买
  /// 商品购买限制条件类型  1 不限制购买 2 用户等级限制 3 主播等级限制 4 用户等级&直播等级限制
  static bool stintBuy({int? type = 1, String? lv = '0'}) {
    // 默认不限制购买 限制等级为0
    YBDUserInfo? user = YBDUserUtil.getLoggedUser(Get.context);
    // logger.v('22.5.13----type:${type.stintType()}--stintlv:$lv----userLv:${user.level}');
    switch (type.stintType()) {
      case StintType.none:
        return false;
      case StintType.level:
        bool stint = user!.level! < YBDNumericUtil.stringToInt(lv);
        return stint;
      case StintType.room:
        bool stint = user!.roomlevel! < YBDNumericUtil.stringToInt(lv);
        return stint;
      case StintType.level7Room:
        // no confirm yet
        bool stint = user!.roomlevel! < YBDNumericUtil.stringToInt(lv);
        return stint;
      default:
        return true;
    }
  }

  /// 获取当前用户信息
  static Future<YBDUserInfo?> getUserInfo() async {
    // 从store里取用户信息
    if (null != YBDCommonUtil.storeFromContext()?.state.bean) {
      return YBDCommonUtil.storeFromContext()!.state.bean;
    }

    // 从sp里取用户信息
    var spUserInfo = await YBDSPUtil.getUserInfo();
    if (null != spUserInfo) {
      return spUserInfo;
    }

    return null;
  }

  /// 获取当前用户信息
  static YBDUserInfo? getUserInfoInstanly() {
    // 从store里取用户信息
    if (null != YBDCommonUtil.storeFromContext()?.state.bean) {
      return YBDCommonUtil.storeFromContext()!.state.bean;
    }
  }

  /// 获取IP信息
  static Future<YBDIpApiEntity?> getIp() async {
    YBDIpApiEntity? ipApiEntity = await YBDHttpUtil.getInstance().doGet(
      Get.context,
      'http://ip-api.com/json',
      needErrorToast: false,
    );
    return ipApiEntity;
  }

  /// 领取完更新
  static Future<bool> saveReceiveExtraBeans(BuildContext context) async {
    YBDUserInfo? info = await YBDUserUtil.userInfo();
    if (info?.vipDailyCheckRewardBeans != 0) {
      info?.vipDailyCheckRewardBeans = 0;
      await YBDSPUtil.save(Const.SP_USER_INFO, info);
      final Store<YBDAppState>? store =
          YBDCommonUtil.storeFromContext(context: context);
      store?.dispatch(YBDUpdateUserAction(info));
    }
    return true;
  }
}
