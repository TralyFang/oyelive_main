



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:package_info/package_info.dart';
import '../../constant/const.dart';
import '../../service/task_service/task_ybd_service.dart';
import '../config_ybd_util.dart';
import '../date_ybd_util.dart';
import '../log_ybd_util.dart';
import '../numeric_ybd_util.dart';
import '../sp_ybd_util.dart';
import '../toast_ybd_util.dart';
import '../../widget/update_ybd_dialog.dart';
import '../../../module/entity/query_ybd_configs_resp_entity.dart';

/// 检查版本更新
class YBDCheckUpdateUtil {
  /// 在首页检查 APP 版本更新
  /// [period] 检查版本更新的时间间隔，单位（小时）
  static Future<void> checkUpdateWithPeriod(BuildContext context, int period) async {
    if (await shouldForceUpdate(context)) {
      logger.v('===checkup force update');
      // 配置项为强制更新时要检查更新
      _checkUpdateTask(context);
    } else if (!(await isSupportedVersion(context))) {
      logger.v('===checkup not supported version');
      // 当前版本小于最小支持的版本则检查更新
      _checkUpdateTask(context);
    } else {
      // 距离上次检更新的时间间隔
      final value = await YBDSPUtil.getInt(Const.SP_SHOW_APP_UPDATE);

      if (value == null || YBDDateUtil.currentUtcTimestamp() - value >= period * 60 * 60 * 1000) {
        logger.v('===check update after : $period hour');
        // 距离上次检查更新的时间间隔超过 [period] 则检查更新
        _checkUpdateTask(context);
      } else {
        // 检查版本更新的时间间隔小于[period]则不检查更新
        logger.v('===check update period less than : $period');
        // 已完成版本弹框任务
        YBDTaskService.instance!.end(TASK_CHECK_UPGRADE);
      }
    }
  }

  /// 在首页检查更新要按先后顺序弹框
  static void _checkUpdateTask(BuildContext context) {
    // 超时后自动结束任务
    YBDTaskService.instance!.autoEnd(TASK_CHECK_UPGRADE, timeOut: TASK_CHECK_UPGRADE_TIMEOUT);

    // 按先后顺序显示首页弹框
    YBDTaskService.instance!.onEvent.listen((taskId) async {
      logger.v('===check update listen $taskId');
      // 开始版本更新弹框任务
      if (taskId == TASK_CHECK_UPGRADE) {
        YBDAppVersionConfig? versionConfig = await ConfigUtil.appVersionInfo(context);

        if (null == versionConfig) {
          // 已完成版本弹框任务
          YBDTaskService.instance!.end(TASK_CHECK_UPGRADE);
          logger.w('===app version config is null');
          return;
        }

        PackageInfo packageInfo = await PackageInfo.fromPlatform();
        // [showDialog] 默认显示更新弹框
        if ((versionConfig.showDialog ?? true) &&
            (versionConfig.buildCode ?? 0) > YBDNumericUtil.stringToInt(packageInfo.buildNumber)) {
          logger.v('show update dialog : ${versionConfig.versionCode} ${versionConfig.forceUpdate}');

          // 配置项版本大于当前版本则显示更新弹框
          _showUpdateDialog(context, versionConfig);
        } else {
          logger.v('not show update dialog : ${versionConfig.versionCode} ${versionConfig.forceUpdate}');
          // 已完成版本弹框任务
          YBDTaskService.instance!.end(TASK_CHECK_UPGRADE);
        }
      }
    });

    // 等待显示版本更新弹框
    YBDTaskService.instance!.waiting(TASK_CHECK_UPGRADE);
  }

  /// 是否要强制更新
  static Future<bool> shouldForceUpdate(BuildContext context) async {
    YBDAppVersionConfig? versionConfig = await ConfigUtil.appVersionInfo(context);
    if (versionConfig?.forceUpdate ?? false) {
      return true;
    } else {
      return false;
    }
  }

  /// 是否支持当前版本
  /// 当前版本大于或等于配置项的最小版本返回 true 否则返回 false
  static Future<bool> isSupportedVersion(BuildContext context) async {
    YBDAppVersionConfig? versionConfig = await ConfigUtil.appVersionInfo(context);

    // 没有配置最小支持的版本默认支持
    if (null == versionConfig?.minVersion) {
      return true;
    }

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    if (YBDNumericUtil.stringToInt(packageInfo.buildNumber) >= versionConfig!.minVersion!) {
      // 当前版本大于或等于配置项的最小版本返回 true
      return true;
    } else {
      return false;
    }
  }

  /// 检查版本更新，弹出升级提示框
  /// [showLatestVersionMessage]  已经是最新版本，是否弹出提示 （首页不需要提示）
  static Future<void> checkUpdate(BuildContext context, {bool showLatestVersionMessage = true}) async {
    YBDAppVersionConfig? versionConfig = await ConfigUtil.appVersionInfo(context);
    if (null == versionConfig) {
      if (showLatestVersionMessage) YBDToastUtil.toast(translate('latest_version'));
      return;
    }

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    if (versionConfig.buildCode! > YBDNumericUtil.stringToInt(packageInfo.buildNumber)) {
      // 配置项版本大于当前版本则显示更新弹框
      _showUpdateDialog(context, versionConfig);
    } else {
      // no update
      if (showLatestVersionMessage) YBDToastUtil.toast(translate('latest_version'));
    }
  }

  /// 显示版本更新弹框
  static Future<void> _showUpdateDialog(BuildContext context, YBDAppVersionConfig versionConfig) async {
    YBDSPUtil.save(Const.SP_SHOW_APP_UPDATE, YBDDateUtil.currentUtcTimestamp());
    bool forceUpdate = _shouldForceUpdate(versionConfig.forceUpdate, await isSupportedVersion(context));
    showDialog(
      // 点空白处不关闭弹框
      barrierDismissible: false,
      context: context,
      builder: (BuildContext builder) {
        return YBDUpdateDialog(
          description: versionConfig.description,
          versionName: versionConfig.versionCode,
          forceUpdate: forceUpdate,
        );
      },
    ).then((value) {
      // 已完成版本弹框任务
      YBDTaskService.instance!.end(TASK_CHECK_UPGRADE);
    });
  }

  /// 是否显示强制更新的弹框
  static bool _shouldForceUpdate(bool? forceUpdate, bool isSupportedVersion) {
    logger.v('force update : $forceUpdate, isSupportedVersion : $isSupportedVersion');
    // 强制更新或小于最低支持的版本都会强制更新
    return (forceUpdate ?? false) || !isSupportedVersion;
  }
}
