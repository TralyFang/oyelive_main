


import 'package:characters/characters.dart';

import 'log_ybd_util.dart';

/// 字符串工具类
class YBDStringUtil {
  /// 判断字符串是否以某个字符串开头
  // TODO: 直接用这个方法 String().startWith('aaa')
  static beginWith(String source, String match) {
    if (source is String && match is String) {
      if (null == source || null == match) {
        logger.v('params is null');
        return false;
      }

      if (source.isEmpty || source.length < match.length) {
        logger.v('source string is short than match');
        return false;
      } else {
        if (source.substring(0, match.length) == match) {
          return true;
        } else {
          return false;
        }
      }
    } else {
      logger.v('params is not string');
    }
  }

  /// 移除字符串开头的字母
  static removeBeginMatch(String source, String match) {
    // TODO: 还没有完成
//  String subString = value.replaceFirst(RegExp(r"[0^]*"), "");
  }

  /// 判断空字符串
  static bool isBlank(String? str) {
    if (null == str || str.isEmpty || str.trim().isEmpty) {
      return true;
    } else {
      return false;
    }
  }

  /// 截取 Unicode (extended) grapheme clusters
  /// subLen 截取的字符串长度
  /// 🍎 包含两个 UTF-16 code units：\uD83C \uDF4E
  /// String str = '🍎abc';
  /// Characters substring = str.characters.take(1); // 🍎

  static String? subString(String? source, int subLen, {bool isWithDot: false}) {
    if (null == source) {
      return null;
    }

    String cutName;

    if (source.length > subLen) {
      if (source.length > source.characters.length) {
        // 包含图片字符
        if (source.characters.length > subLen) {
          cutName = source.characters.take(subLen).toString() + "...";
        } else {
          // 图片字符的长度小于 subLen 返回源字符串
          cutName = source;
        }
      } else {
        // 普通字符
        cutName = source.substring(0, subLen) + "...";
      }
    } else {
      // 源字符串小于 subLen 返回源字符串
      cutName = source;
    }

    return isWithDot ? "$cutName..." : cutName;
  }

  /// 从 url 中获取 userId
  static String userIdFromUrl(String? url) {
    if (null == url) {
      return '';
    }

    List<String> components = url.split('userId=');

    if (components.length > 1) {
      return components.last;
    } else {
      return '';
    }
  }

  /// 截取子串
  static String maxSub(
    String? original,
    int maxLength, {
    String overFlow = '',
  }) {
    String strValue = original ?? '';

    if (strValue.length > maxLength) {
      return strValue.substring(0, maxLength) + overFlow;
    } else {
      return strValue;
    }
  }
}
