

import 'dart:async';

import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';

class YBDCertificationUtil {
  static YBDCertificationUtil? _instance;

  static YBDCertificationUtil? getInstance() {
    if (_instance == null) _instance = YBDCertificationUtil();
    return _instance;
  }

  init() {}

  List<int?> userIdInAddOder = [];
  Map<int?, YBDUserCertificationRecordCertificationInfos?> certificationCache = {};
  StreamController _certificationCacheStreamController =
      StreamController<Map<int?, YBDUserCertificationRecordCertificationInfos?>>.broadcast();
  StreamSink<Map<int?, YBDUserCertificationRecordCertificationInfos?>> get _inAdd_certificationCache =>
      _certificationCacheStreamController.sink as StreamSink<Map<int?, YBDUserCertificationRecordCertificationInfos?>>;
  Stream<Map<int?, YBDUserCertificationRecordCertificationInfos?>> get certificationCacheOutStream =>
      _certificationCacheStreamController.stream as Stream<Map<int?, YBDUserCertificationRecordCertificationInfos?>>;

  addUserCache(List<int?> userIds) async {
    if (userIds == null || userIds.isEmpty) {
      return;
    }
    userIdInAddOder.addAll(userIds);
    //大于100清除缓存20条
    if (userIdInAddOder.length > 100) {
      List<int?> expiredCache = userIdInAddOder.sublist(0, 20);
      userIdInAddOder = userIdInAddOder.sublist(20);
      certificationCache.removeWhere((key, value) => expiredCache.contains(key));
    }

    YBDUserCertificationEntity result = await _queryCer(userIds);
    if (result != null) {
      userIds.forEach((element) {
        YBDUserCertificationRecordCertificationInfos? singleInfo = YBDCommonUtil.getUsersSingleCer(result.record, element);
        //null 也要加进去，覆盖
        certificationCache.addAll({element: singleInfo});
      });
      _inAdd_certificationCache.add(certificationCache);
    }
  }

  _queryCer(List<int?> userIds) async {
    YBDUserCertificationEntity? userCertificationEntity = await ApiHelper.queryCertification(null, userIds);
    if (userCertificationEntity?.returnCode == Const.HTTP_SUCCESS) {
      return userCertificationEntity;
    }
  }
}
