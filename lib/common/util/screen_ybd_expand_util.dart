

import 'package:flutter_screenutil/flutter_screenutil.dart';

class YBDScreenExpandUtil {
  static YBDScreenExpandUtil? _instance;
  static YBDScreenExpandUtil getInstance() {
    if (_instance == null) {
      _instance = YBDScreenExpandUtil();
    }
    return _instance!;
  }

  /// 缩放后的尺寸
  static double scaledWidth(num width, {double forceScale = 1}) {
    return ScreenUtil().setWidth(width) * forceScale;
  }

  setScaleWidth(width, {double? forceScale}) {
    if (forceScale != null) {
      return ScreenUtil().setWidth(width) * forceScale;
    }
    return ScreenUtil().setWidth(width) * 1;
  }

  /// 缩放后的字体
  static double scaledSp(sp, {double forceScale = 1}) {
    return ScreenUtil().setSp(sp) * forceScale;
  }

  setScaleSp(sp, {double? forceScale}) {
    if (forceScale != null) {
      return ScreenUtil().setSp(sp) * forceScale;
    }
    return ScreenUtil().setSp(sp) * 1;
  }

  set instance(YBDScreenExpandUtil value) {
    _instance = value;
  }
}
