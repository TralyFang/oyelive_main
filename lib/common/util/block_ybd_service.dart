


import 'package:get/get.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/abstract/base_ybd_refresh_service.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/inbox/entity/block_ybd_list_entity.dart';
import 'package:oyelive_main/module/inbox/inbox_ybd_api_helper.dart';
import 'package:oyelive_main/module/status/entity/slog_ybd_block_list_entity.dart';
import 'package:oyelive_main/module/status/status_ybd_api_helper.dart';
import 'package:oyelive_main/module/user/entity/query_ybd_room_blacklist_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

class YBDBlockService extends BaseRefreshService {
  @override
  void refresh(
      {YBDBaseModel? model,
      int page = 1,
      int pageSize = Const.PAGE_SIZE,
      Function(List? list, bool isSuccess)? block}) async {
    switch (this.type) {
      case HttpType.SlogBlack:
        YBDSlogBlockListEntity? entity = await YBDStatusApiHelper.slogBlockList(Get.context, page: page, pageSize: pageSize);
        if (entity == null || entity.code != Const.HTTP_SUCCESS_NEW) {
          // 加载数据失败
          block!(null, false);
        } else {
          block!(entity.data?.rows, true);
        }
        break;
      case HttpType.InboxBlack:
        YBDBlockListEntity? blockListEntity = await YBDInboxApiHelper.queryInboxBlacklist(Get.context);
        if (blockListEntity == null || blockListEntity.code != Const.HTTP_SUCCESS_NEW) {
          // 加载数据失败
          block!(null, false);
        } else {
          block!(blockListEntity.data!.blockList, true);
        }
        break;
      case HttpType.RoomBlack:
        YBDQueryRoomBlacklistRespEntity? result = await ApiHelper.queryRoomBlacklist(
            Get.context, YBDUserUtil.getLoggedUserID(Get.context),
            start: page > 0 ? (page - 1) * pageSize : page);
        if (result == null || result.returnCode != Const.HTTP_SUCCESS) {
          // 加载数据失败
          block!(null, false);
        } else {
          block!(result.record, true);
        }
        break;
    }
  }
}
