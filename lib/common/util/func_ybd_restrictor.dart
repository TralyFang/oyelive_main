
import 'dart:developer';

import 'package:synchronized/synchronized.dart';

class YBDFuncRestrictor {
  final TAG = "YBDFuncRestrictor:";

  final _lock = Lock();

  final int _maxLength = 10;

  final Duration _restrictDuration = Duration(milliseconds: 2800);

  final Duration _dueDuration = Duration(seconds: 10);

  List<YBDRestrictTask> _tasks = [];

  static YBDFuncRestrictor? _instance;
  static YBDFuncRestrictor? getInstance() {
    if (_instance == null) {
      _instance = YBDFuncRestrictor();
    }
    return _instance;
  }

  void run(YBDRestrictTask restrictTask) {
    _lock.synchronized(() {
      DateTime nowTime = DateTime.now();
      _clearWhenReachLimits(nowTime);
      bool isTaskInList = false;

      _tasks.forEach((inLineTask) {
        if (inLineTask.taskName == restrictTask.taskName) {
          isTaskInList = true;
          log("$TAG new task:${restrictTask.taskName} find oldTask when $nowTime");
          if (nowTime.difference(inLineTask.callTime) > _restrictDuration) {
            log("$TAG new task:${restrictTask.taskName} pass restrict Time call At $nowTime");

            inLineTask.callTime = nowTime;
            restrictTask.function.call();
          }
        }
      });
      if (!isTaskInList) {
        log("$TAG new task:${restrictTask.taskName} never inline call At $nowTime");

        restrictTask.function.call();
        _tasks.add(restrictTask..callTime = nowTime);
      }
    });
  }
  void runPfx1toyelive(YBDRestrictTask restrictTask) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _clearWhenReachLimits(DateTime nowTime) {
    if (_tasks.length >= _maxLength) {
      _tasks.removeWhere((element) => nowTime.difference(element.callTime) > _dueDuration);
    }
  }
  void _clearWhenReachLimitsRFWqzoyelive(DateTime nowTime) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDRestrictTask {
  String taskName;
  late DateTime callTime;
  Function function;

  YBDRestrictTask(this.taskName, this.function);
}

final Task_Send_Emoji = "t_s_emoji";
