
import '../../ui/page/room/entity/mini_ybd_game_info.dart';

class YBDFrameAnimationUtil {
  ///Dice 幀元素
  static List<String> getDiceGameImage(YBDMiniGameInfo miniGameInfo) {
    List<String> images = [];
    for (int i = 1; i < 7; i++) {
      images.add('assets/images/game/games_dice_$i.png');
    }

    for (int i = 5; i > 0; i--) {
      images.add('assets/images/game/games_dice_$i.png');
    }
    images.add('assets/images/game/games_dice_6.png');

    images.add('assets/images/game/games_dice_${miniGameInfo.status}.png');
    return images;
  }

  ///FINGER_CROSS 幀元素
  static List<String> getFingerGameImage(YBDMiniGameInfo miniGameInfo) {
    List<String> images = [];
    for (int i = 1; i < 4; i++) {
      images.add('assets/images/liveroom/games/finger/finger_cross_$i.webp');
    }
    images.add('assets/images/liveroom/games/finger/finger_cross_2.webp');
    images.add('assets/images/liveroom/games/finger/finger_cross_1.webp');
    images.add('assets/images/liveroom/games/finger/finger_cross_3.webp');
    images.add('assets/images/liveroom/games/finger/finger_cross_2.webp');
    images.add('assets/images/liveroom/games/finger/finger_cross_1.webp');
    images.add('assets/images/liveroom/games/finger/finger_cross_${miniGameInfo.status}.webp');
    return images;
  }

  ///FLIP_COIN 幀元素
  static List<String> getFlipCoinGameImage(YBDMiniGameInfo miniGameInfo) {
    List<String> images = [];
    for (int value = 0; value <= 20; value++) {
      if (value < 10)
        images.add('assets/images/liveroom/games/flip_coin_0000$value.webp');
      else
        images.add('assets/images/liveroom/games/flip_coin_000$value.webp');
    }

    images.add('assets/images/${miniGameInfo.status == 0 ? "flip_coin_head" : "flip_coin_tail"}.png');
    return images;
  }

  ///FLIP_CIRCLE 幀元素
  static List<String> getFlipCircleEGameImage(YBDMiniGameInfo miniGameInfo) {
    List<String> images = [];
    for (int value = 0; value <= 17; value++) {
      if (value < 10)
        images.add('assets/images/liveroom/games/flip_circle_0000$value.webp');
      else
        images.add('assets/images/liveroom/games/flip_circle_000$value.webp');
    }

    images.add('assets/images/liveroom/games/flip_circle_${miniGameInfo.status}.png');
    return images;
  }
}
