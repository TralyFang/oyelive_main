import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';

import 'log_ybd_util.dart';

class YBDRemoteConfigService {
  final RemoteConfig? _remoteConfig;
  static YBDRemoteConfigService? _instance;

  /// 异步获取远端配置的实例，解决获取baseUrl配置时阻塞启动页的问题
  static YBDRemoteConfigService? _remote;
  YBDRemoteConfigService({RemoteConfig? remoteConfig}) : _remoteConfig = remoteConfig;

  static Future<YBDRemoteConfigService?> getInstance() async {
    // RemoteConfig插件升级后用的代码
    //   if (_instance == null) {
    //       logger.v('remote config service is empty, create instance');
    //
    //       try {
    //         await Firebase.initializeApp();
    //         RemoteConfig remoteConfig = RemoteConfig.instance;
    //         _instance = YBDRemoteConfigService(remoteConfig: remoteConfig);
    //         // 使用的pub.dev上Example的配置：https://pub.dev/packages/firebase_remote_config/example
    //         // 可以添加和修改配置
    //         await remoteConfig.setConfigSettings(RemoteConfigSettings(
    //           fetchTimeout: const Duration(seconds: 10),
    //           minimumFetchInterval: const Duration(hours: 1),
    //         ));
    //         await remoteConfig.fetch();
    //         await remoteConfig.fetchAndActivate();
    //         print('done create instance');
    //       } catch (e) {
    //         print('remote config service get instance error : $e');
    //       }
    //     }
    //
    //     print('return YBDRemoteConfigService instance');
    //     return _instance;

    if (_instance == null) {
      logger.v('remote config service is empty, create instance');

      try {
        await Firebase.initializeApp();
        RemoteConfig remoteConfig = RemoteConfig.instance;
        _instance = YBDRemoteConfigService(remoteConfig: remoteConfig);
        // 使用的pub.dev上Example的配置：https://pub.dev/packages/firebase_remote_config/example
        // 可以添加和修改配置
        await remoteConfig.setConfigSettings(RemoteConfigSettings(
          fetchTimeout: const Duration(seconds: 10),
          minimumFetchInterval: const Duration(hours: 1),
        ));
        await remoteConfig.fetch();
        await remoteConfig.fetchAndActivate();
        print('done create instance');
      } catch (e) {
        print('remote config service get instance error : $e');
      }
    }

    print('return YBDRemoteConfigService instance');
    return _instance;
  }

  RemoteConfig? getConfig() {
    return _remoteConfig;
  }

  /// 从远端获取baseUrl
  /// 与远端建立连接前返回空值
  /// 与远端建立连接后返回远端配置的baseUrl
  static Future<String?> baseUrlFromRemote(String key) async {
    String? result = '';

    try {
      logger.v('before baseUrlFromRemote: $key');
      // 从上一次获取的配置实例中获取baseUrl
      result = _remote?.getConfig()?.getString(key);
      logger.v('$key : $result');

      getInstance().then((value) {
        // 异步获取配置实例
        _remote = value;
      });
    } catch (e) {
      logger.e('get remote url error : $e');
    }

    logger.v('return baseUrlFromRemote $key result: $result');
    return result;
  }
}
