// import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

import 'log_ybd_util.dart';

AudioCache? assetsPlayer;
AudioPlayer? advancedPlayer;

class YBDPlayUtil {
  /// 播放 Assets mp3
  static Future<void> playAssetsAudio(final String? mp3File) async {
    if (mp3File == null) {
      return;
    }
    logger.v('playAssetsAudio mp3File: $mp3File');
    if (advancedPlayer == null) {
      advancedPlayer = AudioPlayer();
    }
    if (assetsPlayer == null) {
      assetsPlayer = new AudioCache(fixedPlayer: advancedPlayer);
    }

    await advancedPlayer!.stop();
    await advancedPlayer!.release();
    final result = await assetsPlayer!.play(mp3File, mode: PlayerMode.LOW_LATENCY);
    // advancedPlayer!.play(AssetSource(mp3File), mode: PlayerMode.LOW_LATENCY);
    logger.v('playAssetsAudio result: $result');
  }

  /// 清除assetsPlayer缓存
  static void cleanAssetsAudio({final String? mp3File}) {
    logger.v('cleanAssetsAudio mp3File: $mp3File');
    if (mp3File == null) {
      return;
    }
    assetsPlayer?.clear(Uri.file(mp3File));
    assetsPlayer?.fixedPlayer?.dispose();
    advancedPlayer?.dispose();

    // if (assetsPlayer == null) {
    //   return;
    // }
    //
    // if (mp3File != null) {
    //   assetsPlayer.clear(mp3File);
    // } else {
    //   assetsPlayer.clearCache();
    // }
  }
}
