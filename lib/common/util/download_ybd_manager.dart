



import 'package:flutter_downloader/flutter_downloader.dart';
import 'log_ybd_util.dart';
import '../../ui/page/store/entity/downloadbean.dart';

import 'download_ybd_util.dart';

//订阅者回调签名
typedef void EventCallback(YBDDownloadTaskInfo arg, int code);

class YBDDownLoadManager {
  List<YBDDownLoadBean> loadList = [];
  List<YBDDownLoadBean> loadErrorList = [];
  List<YBDDownLoadBean> loadSuccessList = [];
  List<String?> loadUrlList = [];
  String? curLoadFile;

  factory YBDDownLoadManager() => _getInstance()!;

  static YBDDownLoadManager? get instance => _getInstance();
  static YBDDownLoadManager? _instance;

  Function? callback;

  List<Function> callbackList = [];

//  Map<String,Function> callbackMap;
  Map? callbackMap;

  String callBackEntrances = 'entrances';
  String callBackMic = 'mic';
  String callBackRoomTheme = 'roomTheme';
  String callBackRoomEntrances = 'roomEntrances';

  YBDDownLoadManager._internal() {
    // 初始化
    init();
    _listen();
  }

  static YBDDownLoadManager? _getInstance() {
    if (_instance == null) {
      _instance = new YBDDownLoadManager._internal();
    }
    return _instance;
  }

  init() {
    loadList.clear();
    loadErrorList.clear();
    loadSuccessList.clear();
    loadUrlList.clear();
    callbackMap = new Map();
    YBDDownloadUtil.remove();
  }

  clean() {
    loadList.clear();
    loadErrorList.clear();
    loadSuccessList.clear();
    loadUrlList.clear();
    YBDDownloadUtil.remove();
  }

  ///商城使用回调  暂不修改
  addCall(Function callback) {
    this.callback = callback;
  }

  addCallBacks(String key, Function callback) {
//    this.callback = null;
//    callbackList.add(callback);
    if (callbackMap == null) {
      callbackMap = new Map();
    }
    callbackMap!.putIfAbsent(key, () => callback);
  }

  removeCallBack(String key) {
    callbackMap?.remove(key);
  }

  sentEvent(YBDDownloadTaskInfo event) {
    logger.v("sentEvent event----------:${event.progress}");
    callbackMap?.forEach((key, value) {
      logger.v('key $key value :${value.toString()}');
      if (value is Function) {
        value(event);
        logger.v("sentEvent value event----------:${event.progress}");
      }
    });
  }

  ///添加文件到下载队列 并开始下载
  Future<String?>? addDownLoadfile(YBDDownLoadBean downLoadBean) {
    if (downLoadBean.fileUrl == null || loadUrlList.contains(downLoadBean.fileUrl)) {
      ///文件已经在下载列表中  不在重复添加
      logger.v('addDownLoadfile loadUrlList.contains fileUrl: ${downLoadBean.fileUrl}');
      return null;
    }
    loadUrlList.add(downLoadBean.fileUrl);
    logger.v('addDownLoadfile downLoadBean.fileUrl:${downLoadBean.fileUrl}');
    return _downLoading(downLoadBean);
  }

  ///重新下载  下载失败的文件
  Future<String?>? retry(String taskId) {
    if (taskId == null) {
      return null;
    }
    logger.v('retry taskId:$taskId');
    return YBDDownloadUtil.retry(taskId).then((value) => value as String?);
  }

  ///下载
  Future<String?> _downLoading(YBDDownLoadBean downLoadBean) async {
    //'http://s3-us-west-2.amazonaws.com/thankyotest/anim/owl.zip'
    if (downLoadBean != null) {
      curLoadFile = downLoadBean.fileUrl;
      downLoadBean.taskId = await YBDDownloadUtil.createTask(downLoadBean.fileUrl!, savePath: downLoadBean.savePath);
      logger.v('curLoadFile: $curLoadFile downLoadBean.taskId: ${downLoadBean.taskId}');
      logger.v('curLoad savePath: ${downLoadBean.savePath}');
      downLoadBean.state = DownLoadStatus.loading;
      loadList.add(downLoadBean);
      return downLoadBean.taskId;
    } else {
      logger.v('downLoadBean is null');
      return null;
    }
  }

  cancelTask(YBDDownLoadBean downLoadBean) {
    if (downLoadBean != null && downLoadBean.taskId != null) {
      logger.v('cancelTask');
      YBDDownloadUtil.cancelTask(downLoadBean.taskId);
      downLoadBean.state = DownLoadStatus.cancel;
//      loadList.remove(downLoadBean);
    } else {
      logger.v('cancelTask parameter not right!');
    }
  }

  cancelAllTask() {
    YBDDownloadUtil.cancelAllTask();
    loadList.clear();
  }

  pauseTask(YBDDownLoadBean downLoadBean) {
    if (downLoadBean != null && downLoadBean.state == DownLoadStatus.loading && downLoadBean.taskId != null) {
      logger.v('pauseTask');
      YBDDownloadUtil.pauseTask(downLoadBean.taskId);
      downLoadBean.state = DownLoadStatus.pause;
    } else {
      logger.v('pauseTask parameter not right!');
    }
  }

  resumeTask(YBDDownLoadBean downLoadBean) {
    if (downLoadBean != null && downLoadBean.state == DownLoadStatus.pause && downLoadBean.taskId != null) {
      logger.v('resumeTask');
      YBDDownloadUtil.resumeTask(downLoadBean.taskId);
      downLoadBean.state = DownLoadStatus.loading;
    } else {
      logger.v('resumeTask parameter not right!');
    }
  }

  _listen() {
    YBDDownloadUtil.downloadOutStream.listen(onData, onError: onError(), onDone: onDone);
  }

  void onData(YBDDownloadTaskInfo event) {
    logger.v('onData ${event.progress} id:${event.id}');
    if (event.status == DownloadTaskStatus.complete) {
      logger.v("---------Download success:${event.id}");
      _loadSuccessUpdate(event.id);
    } else if (event.status == DownloadTaskStatus.failed) {
      try {
        _loadErrorUpdate(event.id);
        YBDDownloadUtil.removeTask(event.id!);
      } catch (e) {}
    }
//    send("flush", event,0);
//    callback(event);
    sentEvent(event);
  }
  void onDataT36Tdoyelive(YBDDownloadTaskInfo event) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  onError() {
    logger.v("---------Download onError");
//    _loadErrorUpdate();
//    send("flush", null,1);
  }

  void onDone() {
    logger.v("---------Download onDone $curLoadFile");
  }
  void onDoneryL0coyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _loadSuccessUpdate(String? taskId) async {
    if (loadList == null || taskId == null) {
      return;
    }
    for (int i = 0; i < loadList.length; i++) {
      if (taskId == loadList[i].taskId) {
        loadList[i].state = DownLoadStatus.success;
        loadSuccessList.add(loadList[i]);
        break;
      }
    }
  }
  void _loadSuccessUpdateolR9boyelive(String? taskId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _loadErrorUpdate(String? taskId) async {
    logger.v("---------Download _loadErrorUpdate:$taskId}");
    if (loadList == null || taskId == null) {
      return;
    }
    logger.v("---------Download _loadErrorUpdate  01:$taskId");
    for (int i = 0; i < loadList.length; i++) {
      logger.v("---------Download _loadErrorUpdate 02:$taskId");
      if (taskId == loadList[i].taskId) {
        loadList[i].state = DownLoadStatus.failed;
        loadErrorList.add(loadList[i]);
        loadList.remove(loadList[i]);
        logger.v("---------Download _loadErrorUpdate 03:$taskId");
        if (loadUrlList.isNotEmpty && loadList.isNotEmpty && loadUrlList.contains(loadList[i].fileUrl)) {
          loadUrlList.remove(loadList[i].fileUrl);
          logger.v("---------Download _loadErrorUpdate 04:$taskId fileUrl:${loadList[i].fileUrl}");
        }
        break;
      }
    }
  }

/*  _downloadNext() {
    if (loadList.isNotEmpty) {
      logger.v('loadList ${loadList.length}');
      for (int i = 0; i == loadList.length; i++) {
        if (loadList[i].state == DownLoadStatus.normal || loadList[i].state == DownLoadStatus.pause) {
          _downLoading(loadList[i]);
        }
      }
    } else {
      logger.v('all download complete');
      curLoadFile = null;
    }
  }*/

/* /// 保存事件订阅者队列，key:事件名(id)，value: 对应事件的订阅者队列
  var _eMap = new Map<Object, List<EventCallback>>();

  /// 添加订阅者
  void on(eventName, EventCallback f) {
    if (eventName == null || f == null) return;
    _eMap[eventName] ??= new List<EventCallback>();
    _eMap[eventName].add(f);
  }
  void onEhUPMoyelive(eventName, EventCallback f) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 移除订阅者
  void off(eventName, [EventCallback f]) {
    var list = _eMap[eventName];
    if (eventName == null || list == null) return;
    if (f == null) {
      _eMap[eventName] = null;
    } else {
      list.remove(f);
    }
  }

  /// 触发事件，事件触发后该事件所有订阅者会被调用
  void send(eventName, [arg, code]) {
    var list = _eMap[eventName];
    if (list == null) return;
    int len = list.length - 1;
    //反向遍历，防止订阅者在回调中移除自身带来的下标错位
    for (var i = len; i > -1; --i) {
      list[i](arg, code);
    }
  }*/
}
