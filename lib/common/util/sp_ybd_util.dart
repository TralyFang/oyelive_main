

import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/room/entity/emoji_ybd_entity.dart';
import '../constant/const.dart';
import 'common_ybd_util.dart';
import 'log_ybd_util.dart';
import '../../module/entity/deep_ybd_link_param.dart';
import '../../module/inbox/entity/message_ybd_offset_entity.dart';
import '../../module/user/entity/user_ybd_info_entity.dart';
import '../../module/user/util/user_ybd_util.dart';
import '../../ui/page/store/entity/discount_ybd_entity.dart';
import '../../ui/page/store/entity/price_ybd_info_entity.dart';

class YBDSPUtil {
  static save(String key, dynamic value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var spValue = _parseSpValue(value);

    if (spValue is int) {
      prefs.setInt(key, spValue);
    } else if (spValue is String) {
      prefs.setString(key, spValue);
    } else if (spValue is bool) {
      prefs.setBool(key, spValue);
    } else if (spValue is double) {
      prefs.setDouble(key, spValue);
    } else if (spValue is List) {
      prefs.setStringList(key, spValue as List<String>);
    }

    log("sp save : $key, value : $value, spValue : $spValue");
  }

  /// 解析 sp value
  /// value: YBDUserInfo || jsonMap || Locale || YBDMessageOffsetEntity
  static dynamic _parseSpValue(dynamic value) {
    if (value is YBDUserInfo) {
      logger.v('parse sp YBDUserInfo');
      return jsonEncode(value.toJson());
    }

    if (value is Map) {
      logger.v('parse sp Map');
      return jsonEncode(value);
    }

    if (value is Locale) {
      logger.v('parse sp Locale');
      return localeToString(value);
    }

    if (value is YBDMessageOffsetEntity) {
      logger.v('parse sp YBDMessageOffsetEntity');
      return jsonEncode(value.toJson());
    }

    if (value is YBDEmojiEntity) {
      // 存一份到tp
      YBDTPGlobal.emojis = value.toJson();
      return jsonEncode(value.toJson());
    }

    if (value is YBDSelfEmoji) {
      // 存一份到tp
      // YBDTPGlobal.emojis = value.toJson();
      return jsonEncode(value.toJson());
    }

    return value;
  }

  static Future<void> setStringList(String key, List<String> value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList(key, value);
  }

  static Future<List<String>?> getStringList(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      return prefs.getStringList(key);
    } catch (e) {
      logger.e('getStringList error: $e');
      return null;
    }
  }

  static Future get(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var value = prefs.get(key);
    logger.v('get value from sp key : $key value : $value');
    return value;
  }

  /// 获取当前用户的 sp
  /// 先根据 [key] 取出所有用户的键值对 map
  /// 以当前用户为 key 取对应的 value
  static Future<dynamic> getCurrent(String key) async {
    // 所有用户的 sp 值
    String? mapStr = await YBDSPUtil.get(key);
    logger.v('get sp key : $key, all value :  $mapStr');

    if (null == mapStr) {
      logger.v('get null sp key : $key');
      return null;
    }

    // 当前用户 id
    String? userId = await YBDUserUtil.userId();

    try {
      Map<String, dynamic> map = jsonDecode(mapStr);
      logger.v('get sp user : $userId, key : $key, value: ${map[userId!]}');

      // 返回当前用户的 sp 值
      return map[userId];
    } catch (e) {
      logger.v('parse sp error : $e');
      return null;
    }
  }

  /// 保存当前用户的 sp
  /// 先根据 [key] 取出所有用户的键值对 map
  /// 在取出的 map 中插入一条当前用户的键值对
  /// 保存 map
  static Future<void> saveCurrent(String key, dynamic value) async {
    // 所有用户的 sp 值
    String? mapStr = await YBDSPUtil.get(key);
    logger.v('all user sp key : $key, all value : $mapStr');

    // 当前用户
    String? userId = await YBDUserUtil.userId();
    Map<String?, dynamic>? map = {};

    try {
      map = jsonDecode(mapStr ?? '{}');

      // 以当前用户的 id 为 key 保存 sp 值
      map!.addAll({userId: value});

      // 保存所有用户的 sp 值
      await YBDSPUtil.save(key, jsonEncode(map));
      logger.v('sp save current key : $key, all value : ${jsonEncode(map)}');
    } catch (e) {
      logger.v('sp save current error : $e, $mapStr');
    }
  }

//  static getPortaltype() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    int result = prefs.get(Const.SP_KEY_PORTALTYPE);
//    if (result == null) {
//      return 201;
//    }
//    return result;
//  }

  static remove(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(key);

    logger.v("sp remove : $key ");
  }

  /// TODO: 调用新方法 YBDUserUtil.userInfo()
  static Future<YBDUserInfo?> getUserInfo() async {
    /// sp 获取用户信息
    String? spUser = await YBDSPUtil.get(Const.SP_USER_INFO);

    try {
      YBDUserInfo? bean;
      if (null != spUser && spUser.isNotEmpty) {
        bean = YBDUserInfo().fromJson(json.decode(spUser));
        return bean;
      } else {
        logger.v('sp user info empty');
        return null;
      }
    } catch (e) {
      logger.v('get sp user info error : $e');
      return null;
    }
  }

  /// 查看 [YBDUserUtil]的userId方法
  static Future<String?> getUserId() async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();

    if (null != userInfo && null != userInfo.id) {
      logger.v('get sp user id success : ${userInfo.id}');
      return '${userInfo.id}';
    } else {
      logger.v('sp user id empty');
      return null;
    }
  }

  static reload() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    logger.v("reload flutter sp!!!!!!!!!!!!");
    await prefs.reload();
  }

  /// sp 获取DeepLink 信息
  static Future<YBDDeepLinkParam?> getDeepLinkParams() async {
    String? spParams = await YBDSPUtil.get(Const.SP_DEEP_LINK_PARAMS);

    try {
      YBDDeepLinkParam? bean;
      if (null != spParams && spParams.isNotEmpty) {
        bean = YBDDeepLinkParam().fromJson(json.decode(spParams));
        return bean;
      } else {
        logger.v('sp deep link params empty');
        return null;
      }
    } catch (e) {
      logger.v('get sp deep link params error : $e');
      return null;
    }
  }

  /// 保存每日任务的完成记录
  static saveCheckInDate({int? milliseconds}) async {
    String? userId = await getUserId();
    int? date = milliseconds;

    if (null == date) {
      logger.v('milliseconds is null, default use now');
      date = DateTime.now().millisecondsSinceEpoch;
    }

    save('${Const.SP_DAILY_CHECKED_IN}_$userId', date);
  }

  /// 获取每日任务的完成记录
  static Future<int?> getCheckInDate() async {
    String? userId = await getUserId();
    int? date = await get('${Const.SP_DAILY_CHECKED_IN}_$userId');

    if (null == date) {
      logger.v('no daily checked-in date');
      return null;
    }
    logger.v('daily checked-in date : $date');
    return date;
  }

  /// 获取房间名称
  static Future<String> getRoomTitle() async {
    String? userId = await getUserId();
    String? title = await get('${Const.SP_ROOM_TITLE}_$userId');
    return title ?? '';
  }

  // 保存房间名称
  static saveRoomTitle(String? title) async {
    if (null == title || title.isEmpty) {
      return;
    }

    String? userId = await getUserId();
    save('${Const.SP_ROOM_TITLE}_$userId', title);
  }

  // 保存活动弹框弹出的日期
  static saveActivityDialogDate({int? milliseconds}) async {
    String? userId = await getUserId();
    int? date = milliseconds;

    if (null == date) {
      logger.v('milliseconds is null, default use now');
      date = DateTime.now().millisecondsSinceEpoch;
    }

    save('${Const.SP_ACTIVITY_DIALOG_DATE}_$userId', date);
  }

  /// 获取活动弹框的弹出日期
  static Future<int?> activityDialogDate() async {
    String? userId = await getUserId();
    int? date = await get('${Const.SP_ACTIVITY_DIALOG_DATE}_$userId');

    if (null == date) {
      logger.v('no halloween dialog date');
      return null;
    }

    return date;
  }

  static Future<YBDMessageOffsetEntity?> getMessageOffset() async {
    String? spParams = await YBDSPUtil.get(Const.SP_MESSAGE_QUEUE);

    YBDMessageOffsetEntity? result;
    try {
      if (spParams != null && spParams != null) {
        result = YBDMessageOffsetEntity().fromJson(json.decode(spParams));
        return result;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<YBDEmojiEntity?> getEmojiEntity() async {
    String spParams = await YBDSPUtil.get(Const.SP_EMOJI_DATA);

    YBDEmojiEntity result;
    try {
      if (spParams != null && spParams != null) {
        result = YBDEmojiEntity().fromJson(json.decode(spParams));
        return result;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<YBDSelfEmoji?> getMyEmoji() async {
    String spParams = await YBDSPUtil.get(Const.SP_MY_EMOJI);

    YBDSelfEmoji result;
    try {
      if (spParams != null && spParams != null) {
        result = YBDSelfEmoji().fromJson(json.decode(spParams));
        return result;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<bool?> getBool(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? value = prefs.getBool(key);
    return value;
  }

  static Future<int?> getInt(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key);
  }

  /// Frame Discount
  static Future<List<YBDPriceInfoEntity?>?> getFrameDiscount() async {
    String? spParams = await YBDSPUtil.get(Const.SP_KEY_DISCOUNT_MIC_FRAME);
    YBDDiscountRecord? result;
    List<YBDPriceInfoEntity?> priceInfoEntityList = [];
    try {
      if (spParams != null && spParams != null) {
        logger.v('queryDiscountList getFrameDiscount spParams: $spParams');
        result = YBDDiscountRecord().fromJson(json.decode(spParams));
        Map dataMap = Map<String, dynamic>.from(result.priceInfo);
        if (dataMap != null) {
          List<dynamic>? dataList = dataMap['-1'];
          if (dataList != null) {
            YBDPriceInfoEntity? priceInfoEntity;
            for (int i = 0; i < dataList.length; i++) {
              priceInfoEntity = YBDPriceInfoEntity().fromJson(dataList[i]);
              priceInfoEntity.unitType = result.unitType;
              priceInfoEntityList.add(priceInfoEntity);
            }
          }
        }
        return priceInfoEntityList;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  /// Theme Discount
  static Future<List<YBDPriceInfoEntity?>?> getThemeDiscount() async {
    String? spParams = await YBDSPUtil.get(Const.SP_KEY_DISCOUNT_THEME);
    YBDDiscountRecord? result;
    List<YBDPriceInfoEntity?> priceInfoEntityList = [];
    try {
      if (spParams != null && spParams != null) {
        logger.v('queryDiscountList getFrameDiscount spParams: $spParams');
        result = YBDDiscountRecord().fromJson(json.decode(spParams));
        Map dataMap = Map<String, dynamic>.from(result.priceInfo);
        if (dataMap != null) {
          List<dynamic>? dataList = dataMap['-1'];
          if (dataList != null) {
            YBDPriceInfoEntity? priceInfoEntity;
            for (int i = 0; i < dataList.length; i++) {
              priceInfoEntity = YBDPriceInfoEntity().fromJson(dataList[i]);
              priceInfoEntity.unitType = result.unitType;
              priceInfoEntityList.add(priceInfoEntity);
            }
          }
        }
        return priceInfoEntityList;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  /**
   * 单位类型：1.日，2.周，3.月，1001.个 (时间上的定义，跟DateType枚举保持一致)
   */
//  private int unitType;
  /// Car Discount
  static Future<List<YBDPriceInfoEntity?>?> getCarDiscount() async {
    String? spParams = await YBDSPUtil.get(Const.SP_KEY_DISCOUNT_CAR);
    YBDDiscountRecord? result;
    List<YBDPriceInfoEntity?> priceInfoEntityList = [];
    try {
      if (spParams != null && spParams != null) {
        logger.v('queryDiscountList getCarDiscount spParams: $spParams');
        result = YBDDiscountRecord().fromJson(json.decode(spParams));
        Map dataMap = Map<String, dynamic>.from(result.priceInfo);
        if (dataMap != null) {
          List<dynamic>? dataList = dataMap['-1'];
          if (dataList != null) {
            YBDPriceInfoEntity? priceInfoEntity;
            for (int i = 0; i < dataList.length; i++) {
              priceInfoEntity = YBDPriceInfoEntity().fromJson(dataList[i]);
              priceInfoEntity.unitType = result.unitType;
              priceInfoEntityList.add(priceInfoEntity);
            }
          }
        }
        return priceInfoEntityList;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<List<YBDPriceInfoEntity?>?> getUidDiscount() async {
    String? spParams = await YBDSPUtil.get(Const.SP_KEY_DISCOUNT_UID);
    YBDDiscountRecord? result;
    List<YBDPriceInfoEntity?> priceInfoEntityList = [];
    try {
      if (spParams != null && spParams != null) {
        logger.v('queryDiscountList getUidDiscount spParams: $spParams');
        result = YBDDiscountRecord().fromJson(json.decode(spParams));
        Map dataMap = Map<String, dynamic>.from(result.priceInfo);
        if (dataMap != null) {
          List<dynamic>? dataList = dataMap['-1'];
          if (dataList != null) {
            YBDPriceInfoEntity? priceInfoEntity;
            for (int i = 0; i < dataList.length; i++) {
              priceInfoEntity = YBDPriceInfoEntity().fromJson(dataList[i]);
              priceInfoEntity.unitType = result.unitType;
              priceInfoEntityList.add(priceInfoEntity);
            }
          }
        }
        return priceInfoEntityList;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<List<YBDPriceInfoEntity?>?> getDiscount(String key) async {
    String? spParams = await YBDSPUtil.get(key);
    YBDDiscountRecord? result;
    List<YBDPriceInfoEntity?> priceInfoEntityList = [];
    logger.v('query $key getUidDiscount spParams: $spParams');

    try {
      if (spParams != null && spParams != null) {
        logger.v('query $key getUidDiscount spParams: $spParams');
        result = YBDDiscountRecord().fromJson(json.decode(spParams));
        Map dataMap = Map<String, dynamic>.from(result.priceInfo);
        if (dataMap != null) {
          List<dynamic>? dataList = dataMap['-1'];
          if (dataList != null) {
            YBDPriceInfoEntity? priceInfoEntity;
            for (int i = 0; i < dataList.length; i++) {
              priceInfoEntity = YBDPriceInfoEntity().fromJson(dataList[i]);
              priceInfoEntity.unitType = result.unitType;
              priceInfoEntityList.add(priceInfoEntity);
            }
          }
        }
        return priceInfoEntityList;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  /// 本地屏蔽单个用户
  static blockUser(String userId) async {
    if (null != userId && userId.isNotEmpty && userId != 'null') {
      List<String>? blockedUsers = await getBlockedUserList();

      if (null != blockedUsers && blockedUsers.isNotEmpty && !blockedUsers.contains(userId)) {
        blockedUsers.add(userId);
        await YBDSPUtil.save(Const.SP_BLOCKED_USER_LIST, blockedUsers);
      } else {
        await YBDSPUtil.save(Const.SP_BLOCKED_USER_LIST, [userId]);
      }
    } else {
      logger.v('invalid user id');
    }
  }

  /// 获取被屏蔽的用户列表
  static Future<List<String>?> getBlockedUserList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? blockedUsers = prefs.getStringList(Const.SP_BLOCKED_USER_LIST);
    return blockedUsers;
  }

  /// 查询保存的账号密码
  /// 格式：{加密的账号密码1}<credential>{加密的账号密码2}<credential>{加密的账号密码3}
  static Future<List<List<String>>> getSavedCredentials() async {
    List<List<String>> credentials = [];
    String? spValue = await YBDSPUtil.get(Const.SP_SAVE_PASSWORD);
    if (spValue != null && spValue.isNotEmpty) {
      List<String> c = spValue.split('<credential>');
      if (c != null && c.isNotEmpty) {
        List<String> accPwd;
        c.forEach((element) {
          if (element.isNotEmpty) {
            accPwd = YBDCommonUtil.decrypt(element).split("<gap>");
            credentials.add(accPwd);
          }
        });
      }
    }

    return credentials;
  }

  /// 保存账号密码
  /// 最多保存5个账号
  static Future<void> saveCredentials(String account, String password) async {
    await removeSavedCredentials(account); // 如果已经保存，则先删除，再新增

    List<List<String>> credentials = await getSavedCredentials();
    if (credentials == null) credentials = [];
    if (credentials.length == 5) credentials.removeAt(0); // 已经保存5个账号，则移除第1个
    credentials.add([account, password]);

    await _saveCredentials2Sp(credentials);
  }

  static Future<void> _saveCredentials2Sp(List<List<String>> credentials) async {
    if (credentials == null) credentials = [];
    String saveSp = '';
    credentials.forEach((element) {
      if (element.isNotEmpty) {
        saveSp += YBDCommonUtil.encrypt('${element[0]}<gap>${element[1]}');
        saveSp += '<credential>';
      }
    });
    await YBDSPUtil.save(Const.SP_SAVE_PASSWORD, saveSp);
  }

  /// 删除保存的账号密码
  static Future<void> removeSavedCredentials(String account) async {
    List<List<String>> credentials = await getSavedCredentials();
    if (credentials != null && credentials.isNotEmpty) {
      credentials.removeWhere((element) {
        return element[0] == account;
      });
    }
    await _saveCredentials2Sp(credentials);
  }

  /// EP支付账号 本地保存
  static Future<void> saveEPAccountNUmber(String accountNumber) async {
    YBDSPUtil.save(Const.SP_EASY_PAISA_ACCOUNT_NUMBER, accountNumber);
  }

  /// 获取EP支付账号
  static Future getEPAccountNUmber() async {
    return YBDSPUtil.get(Const.SP_EASY_PAISA_ACCOUNT_NUMBER);
  }
}
