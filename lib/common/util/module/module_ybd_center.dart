

import 'package:flutter/foundation.dart';
import 'package:oyelive_main/common/abstract/module_ybd_center.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/room_ybd_operate_imp.dart';

enum EnterRoomType {
  Default,
  OpenTp, //打开tp
  OpenFruit, //打开水果游戏
  OpenGift, //打开礼物框
  GetAMic, // 上麦
  NeedBlc, //需要initState回调时间
  OpenLudo, // 打开ludo
  OpenGameUrl, // 打开h5游戏，需要另外追加gameUrl
  OpenLucky1000, //打开lucky1000
}

class YBDModuleCenter implements RoomOperateCenter {
  static YBDModuleCenter? _instance;

  int? notificationId;
  static YBDModuleCenter instance() {
    if (_instance == null) {
      _instance = YBDModuleCenter._();
    }
    return _instance!;
  }

  YBDModuleCenter._();

  final ObserverList<RoomOperateCenter> _listeners = ObserverList();

  void registerObserver() {
    logger.v("register obs center");
    if (_listeners.length == 0) {
      RoomOperateCenter ls = YBDRoomOperateImp();
      _listeners.add(ls);
    }
  }
  void registerObserverroxovoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void removeObserver(RoomOperateCenter listener) {
    _listeners.remove(listener);
  }
  void removeObserver5T068oyelive(RoomOperateCenter listener) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  static EnterRoomType enterRoomType(int? tag) {
    switch (tag) {
      case 1:
        return EnterRoomType.OpenTp;
        break;
      case 2:
        return EnterRoomType.OpenFruit;
        break;
      case 3:
        return EnterRoomType.OpenGift;
        break;
      case 4:
        return EnterRoomType.GetAMic;
        break;
      case 5:
        return EnterRoomType.NeedBlc;
        break;
      case 6:
        return EnterRoomType.OpenLudo;
        break;
      case 7:
        return EnterRoomType.OpenGameUrl;
        break;
      case 8:
        return EnterRoomType.OpenLucky1000;
        break;
      default:
        return EnterRoomType.Default;
        break;
    }
  }

  @override
  void tpPop() {
    YBDModuleCenter.instance().registerObserver();
    logger.v('tp close method: ${_listeners.length}');
    final List<RoomOperateCenter> localListeners = List<RoomOperateCenter>.from(_listeners);
    localListeners.forEach((element) {
      element.tpPop();
    });
    localListeners.forEach((element) {
      YBDModuleCenter.instance().removeObserver(element);
    });
  }
  void tpPopiVGePoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void enterRoom({
    int? roomId = defaultValue,
    EnterRoomType? type = EnterRoomType.Default,
    int? id,
    int? gameCode,
  }) {
    YBDModuleCenter.instance().registerObserver();
    final List<RoomOperateCenter> localListeners = List<RoomOperateCenter>.from(_listeners);
    localListeners.forEach((element) {
      element.enterRoom(roomId: roomId, type: type, gameCode: gameCode);
    });
    localListeners.forEach((element) {
      YBDModuleCenter.instance().removeObserver(element);
    });
    notificationId = id;
  }

  @override
  showGemsTransBeansView() {
    YBDModuleCenter.instance().registerObserver();
    final List<RoomOperateCenter> localListeners = List<RoomOperateCenter>.from(_listeners);
    localListeners.forEach((element) {
      element.showGemsTransBeansView();
    });
    localListeners.forEach((element) {
      YBDModuleCenter.instance().removeObserver(element);
    });
  }

  @override
  initBlock(String routeName) {
    YBDModuleCenter.instance().registerObserver();
    final List<RoomOperateCenter> localListeners = List<RoomOperateCenter>.from(_listeners);
    localListeners.forEach((element) {
      element.initBlock(routeName);
    });
    localListeners.forEach((element) {
      YBDModuleCenter.instance().removeObserver(element);
    });
  }

  @override
  roomFuncOperation(EnterRoomType? type, {int? gameCode}) {
    YBDModuleCenter.instance().registerObserver();
    final List<RoomOperateCenter> localListeners = List<RoomOperateCenter>.from(_listeners);
    localListeners.forEach((element) {
      element.roomFuncOperation(type, gameCode: gameCode);
    });
    localListeners.forEach((element) {
      YBDModuleCenter.instance().removeObserver(element);
    });
  }

  @override
  updateTPMoney() {
    YBDModuleCenter.instance().registerObserver();
    final List<RoomOperateCenter> localListeners = List<RoomOperateCenter>.from(_listeners);
    localListeners.forEach((element) {
      element.updateTPMoney();
    });
    localListeners.forEach((element) {
      YBDModuleCenter.instance().removeObserver(element);
    });
  }
}
