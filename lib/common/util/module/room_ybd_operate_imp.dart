



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/abstract/module_ybd_center.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/int_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/tp_ybd_save_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/bus_event/event_ybd_fn.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/dialog/dialog_ybd_invite_tp.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/dialog/dialog_ybd_tp_save.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';
import 'package:oyelive_main/ui/page/room/util/tp_ybd_dialog_manager.dart';
import 'package:oyelive_main/ui/page/room/widget/dialog_ybd_gems_exchange_beans.dart';
import 'package:oyelive_main/ui/page/room/widget/dialog_ybd_insufficient_warn.dart';

import '../toast_ybd_util.dart';

const int defaultValue = -1;

class YBDRoomOperateImp implements RoomOperateCenter {
  ///外面进入房间 可能有附带业务  实体还未定义 默认先打开tp
  @override
  enterRoom({int? roomId, defaultVal, EnterRoomType? type, int? gameCode}) {
    logger.v('enter room page: $roomId');
    roomId.enterRoom(type: type, gameCode: gameCode);
  }

  ///新增一个配置  A|B｜C  A判断是否调服务端接口
  /// B 接口超时时间 例如设置2s 那调完接口后 2s 没有回调 直接pop掉
  /// 根据接口直接显示不同 UI
  @override
  tpPop() async {
    logger.v('tp close');

    if (YBDCommonUtil.getRoomOperateInfo().isNeedTpSave == 0) {
      Navigator.pop(Get.context!);
      return;
    }
    YBDTpSaveEntity? _tpSaveEntity = await ApiHelper.isTpSave(Get.context);
    if (_tpSaveEntity?.returnCode == Const.HTTP_SUCCESS && _tpSaveEntity!.record != null) {
      //是否弹出挽留

      showDialog<Null>(
          context: Get.context!, //BuildContext对象
          barrierDismissible: true,
          builder: (BuildContext context) {
            return YBDTpSaveDialog(_tpSaveEntity.record);
          });
    } else {
      Navigator.maybePop(Get.context!);
    }
  }

  /// 上面配置项c 区别是否过滤签约主播
  /// 签约主播没有beans 但是有gems 直接弹转化弹窗   如果也没有gems 直接弹以前的充值弹窗
  @override
  showGemsTransBeansView() {
    if (YBDCommonUtil.couldShowTPGems2Beans()) {
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.CLICK_EVENT,
        location: YBDLocationName.ROOM_PAGE,
        itemName: YBDItemName.TP_G2B_OPEN,
      ));
      YBDCommonUtil.showMyDialog(YBDGemsExchangeBeansDialog(), dialogId: TP_GEM2BEAN_ID);
    } else {
      YBDCommonUtil.showMyDialog(YBDInsufficinetDialog(), dialogId: TP_INSUFFICINET_ID);
    }
  }

  @override
  initBlock(String routeName) async {
    //房间 routeName YBDNavigatorHelper.flutter_room_list_page
    logger.v('init state func: $routeName');

    ///外面打开tp
    // roomFuncOperation(EnterRoomType.Default);
    if (routeName.contains(YBDNavigatorHelper.flutter_room_list_page)) {
      //弹窗

      if (YBDModuleCenter.instance().notificationId != null) {
        YBDTpSaveEntity? tpSaveEntity =
            await ApiHelper.tpInviteOffline(Get.context, YBDModuleCenter.instance().notificationId);

        if (tpSaveEntity?.returnCode == Const.HTTP_SUCCESS && tpSaveEntity!.record != null) {
          showDialog<Null>(
              context: Get.context!, //BuildContext对象
              barrierDismissible: true,
              builder: (BuildContext context) {
                return YBDInviteTpDialog(
                  InviteType.OfflineInvite,
                  tpSaveEntity.record,
                  YBDModuleCenter.instance().notificationId,
                  onSuccess: () {
                    YBDTeenInfo.getInstance().enterType = TPEnterType.TPETBeans;
                    roomFuncOperation(EnterRoomType.OpenTp);
                  },
                );
              });
        } else {
          YBDToastUtil.toast(tpSaveEntity?.returnMsg ?? "Failed!");
        }
      }
    }
  }

  @override
  roomFuncOperation(EnterRoomType? type, {int? gameCode}) {
    logger.v('get current route: ${Get.currentRoute} ${YBDObsUtil.instance().routeSets}');
    if (type == EnterRoomType.Default ||
        type == EnterRoomType.NeedBlc ||
        !YBDObsUtil.instance().routeSets.last.contains('room')) {
      logger.v('the func is unuse');
      return;
    }
    eventBus.fire(YBDOpenTp()..tag = type!.index);
  }

  @override
  updateTPMoney() {
    eventBus.fire(YBDSeatSelectEvent()
      ..money = 0
      ..index = -1);
  }
}
