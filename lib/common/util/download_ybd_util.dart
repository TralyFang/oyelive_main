

import 'dart:async';
import 'dart:isolate';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';

import 'common_ybd_util.dart';
import 'log_ybd_util.dart';

/**
 * 使用的时候在需要下载的页面生命周期init的时候执行bindBackgroundIsolate，dispose的时候unbindBackgroundIsolate
 * downloadOutStream监听所有任务，会返回当前所有任务的状态YBDDownloadTaskInfo，根据id自行判断是当前哪个任务。
 */

class YBDDownloadUtil {
  static ReceivePort _port = ReceivePort();

  static final String portName = "downloader_task";

  static final String defaultSaveFolderName = "downloaded_file";

  // ignore: close_sinks
  static StreamController _downloadStreamController = StreamController<YBDDownloadTaskInfo>.broadcast();

  static StreamSink<YBDDownloadTaskInfo> get _add_download => _downloadStreamController.sink as StreamSink<YBDDownloadTaskInfo>;

  static Stream<YBDDownloadTaskInfo> get downloadOutStream => _downloadStreamController.stream as Stream<YBDDownloadTaskInfo>;

  static void _downloadCallback(String id, DownloadTaskStatus status, int progress) {
    logger.v('Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    final SendPort send = IsolateNameServer.lookupPortByName(portName)!;
    send.send([id, status, progress]);
  }

  static void unbindBackgroundIsolate() {
    logger.v('dispose  YBDDownloadUtil.unbindBackgroundIsolate');
    IsolateNameServer.removePortNameMapping(portName);
    _port.close();
//    logger.v("unbind isolate ${_port.length} ${_port.toList()}");
  }

  static void bindBackgroundIsolate() {
    logger.v('dialogUtil bindBackgroundIsolate');
    bool isSuccess = IsolateNameServer.registerPortWithName(_port.sendPort, portName);
    logger.v('dialogUtil bindBackgroundIsolate: $isSuccess');
    if (!isSuccess) {
      logger.v("bind isolate failed");
      unbindBackgroundIsolate();
      bindBackgroundIsolate();
      return;
    }

    logger.v("dialogUtil bind isolate isSuccess");
    _port.listen((dynamic data) {
      logger.v('UI Isolate Callback: $data');
      String? id = data[0];
      DownloadTaskStatus? status = data[1];
      int progress = data[2];
      double downloadProgress = progress / 100;

      _add_download.add(YBDDownloadTaskInfo()
        ..id = id
        ..status = status
        ..progress = downloadProgress);
    });
    FlutterDownloader.registerCallback(_downloadCallback);
  }

  //创建任务，文件名、保存路径可传可不传
  static Future<String?> createTask(String fileUrl, {String? fileName, String? savePath}) async {
    String _fileName, _downloadPath;
    if (fileName == null) {
      _fileName = fileUrl.split("/").last;
    } else {
      _fileName = fileName;
    }

    if (savePath == null) {
      _downloadPath = await YBDCommonUtil.getResourceDir(defaultSaveFolderName);
    } else {
      //Animation/audi_garage/2  Animation +
//      _downloadPath = savePath;
      _downloadPath = await YBDCommonUtil.getResourceDir(savePath);
    }

    logger.v('_downloadPath :$_downloadPath _fileName:$_fileName fileUrl:$fileUrl');
    return FlutterDownloader.enqueue(
        url: fileUrl,
        savedDir: _downloadPath,
        showNotification: false,
        openFileFromNotification: true,
        fileName: _fileName);
  }

  /// 创建下载皮肤zip的任务
  /// [zipUrl] zip的下载链接
  /// [zipName] 下载后保存在磁盘中的zip名称
  static Future<String?> createSkinTask({
    required String zipUrl,
    required String? zipName,
  }) async {
    // 下载到app的文件目录中
    String skinPath = (await getApplicationDocumentsDirectory()).path;

    logger.v('skinPath: $skinPath, zipPath: $zipName, zipUrl: $zipUrl');
    return FlutterDownloader.enqueue(
      url: zipUrl,
      savedDir: skinPath,
      showNotification: false,
      openFileFromNotification: false,
      fileName: zipName,
    );
  }

  static void cancelTask(String? taskId) {
    if (taskId != null && taskId.isNotEmpty) {
      FlutterDownloader.cancel(taskId: taskId);
    }
  }

  static void cancelAllTask() {
    FlutterDownloader.cancelAll();
  }

  static void pauseTask(String? taskId) {
    if (taskId != null && taskId.isNotEmpty) {
      FlutterDownloader.pause(taskId: taskId);
    }
  }

  static void resumeTask(String? taskId) {
    if (taskId != null && taskId.isNotEmpty) {
      FlutterDownloader.resume(taskId: taskId);
    }
  }

  static Future<List<DownloadTask>?> loadTasks() {
    return FlutterDownloader.loadTasks();
  }

  static Future remove() {
    return FlutterDownloader.remove(taskId: '');
  }

  static Future removeTask(String taskId) {
    return FlutterDownloader.remove(taskId: taskId);
  }

  static Future retry(String taskId) {
    return FlutterDownloader.retry(taskId: taskId);
  }
}

class YBDDownloadTaskInfo {
  String? id;
  DownloadTaskStatus? status;
  double? progress;
}

//import 'dart:async';
//import 'dart:ui';
//
//import 'package:flutter_downloader/flutter_downloader.dart';
//import 'dart:isolate';
//
//import 'package:oyelive_main/common/util/common_ybd_util.dart';
//
///**
// * 使用的时候在需要下载的页面生命周期init的时候执行bindBackgroundIsolate，dispose的时候unbindBackgroundIsolate
// * downloadOutStream监听所有任务，会返回当前所有任务的状态YBDDownloadTaskInfo，根据id自行判断是当前哪个任务。
// */
//
//class YBDDownloadUtil {
//  static ReceivePort _port = ReceivePort();
//
//  static final String portName = "downloader_task";
//
//  static final String defaultSaveFolderName = "Downloaded_File";
//
//  // ignore: close_sinks
//  static StreamController _downloadStreamController = StreamController<YBDDownloadTaskInfo>.broadcast();
//  static StreamSink<YBDDownloadTaskInfo> get _add_download => _downloadStreamController.sink;
//  static Stream<YBDDownloadTaskInfo> get downloadOutStream => _downloadStreamController.stream;
//
//  static void _downloadCallback(String id, DownloadTaskStatus status, int progress) {
//    logger.v('Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
//    final SendPort send = IsolateNameServer.lookupPortByName(portName);
//    send.send([id, status, progress]);
//  }
//
//  static void unbindBackgroundIsolate() {
//    IsolateNameServer.removePortNameMapping(portName);
//  }
//
//  static void bindBackgroundIsolate() {
//    bool isSuccess = IsolateNameServer.registerPortWithName(_port.sendPort, portName);
//    if (!isSuccess) {
//      logger.v("bind isolate failed");
//      unbindBackgroundIsolate();
//      bindBackgroundIsolate();
//      return;
//    }
//    _port.listen((dynamic data) {
//      logger.v('UI Isolate Callback: $data');
//      String id = data[0];
//      DownloadTaskStatus status = data[1];
//      int progress = data[2];
//      double downloadProgress = progress / 100;
//
//      _add_download.add(YBDDownloadTaskInfo()
//        ..id = id
//        ..status = status
//        ..progress = downloadProgress);
//    });
//
//    FlutterDownloader.registerCallback(_downloadCallback);
//  }
//
//  //创建任务，文件名、保存路径可传可不传
//  static Future<String> createTask(String fileUrl, {String fileName, String savePath}) async {
//    String _fileName, _downloadPath;
//    if (fileName = null) {
//      _fileName = fileUrl.split("/").last;
//    } else {
//      _fileName = fileName;
//    }
//    if (_downloadPath = null) {
//      _downloadPath = await YBDCommonUtil.getResourceDir(defaultSaveFolderName);
//    } else {
//      _downloadPath = savePath;
//    }
//    return FlutterDownloader.enqueue(
//        url: fileUrl,
//        savedDir: _downloadPath,
//        showNotification: true,
//        openFileFromNotification: true,
//        fileName: _fileName);
//  }
//}
//
//class YBDDownloadTaskInfo {
//  String id;
//  DownloadTaskStatus status;
//  double progress;
//}
