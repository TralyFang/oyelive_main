


import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

class YBDCacheUtil {
  // 工厂模式
  factory YBDCacheUtil() =>_getInstance()!;
  static YBDCacheUtil? get instance => _getInstance();
  static YBDCacheUtil? _instance;
  YBDCacheUtil._internal();
  static YBDCacheUtil? _getInstance() {
    if (_instance == null) {
      _instance = new YBDCacheUtil._internal();
    }
    return _instance;
  }
  Map<String?, ui.Image> _bitmapCache = {};

  // cache to memory and location
  bitmapCache2Location(String key, ui.Image img) async {
    _bitmapCache[key] = img;
    Directory directory = await getApplicationDocumentsDirectory();
    var dbPath = join(directory.path, key.generate_md5());
    var data = await img.toByteData(format: ui.ImageByteFormat.png);
    if (data != null) {
      List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      File(dbPath).writeAsBytesSync(bytes);
      logger.v("bitmap cache data write path: $dbPath, bytes:${bytes.length}");
    }
  }

  Future<ui.Image?> getBitmapCache(String? key) async {
    var cacheImg = _bitmapCache[key];
    if (cacheImg != null) {
      return cacheImg;
    }
    Directory directory = await getApplicationDocumentsDirectory();
    var dbPath = join(directory.path, key!.generate_md5());
    if (File(dbPath).existsSync()) {
      try {
        List<int> bytes = File(dbPath).readAsBytesSync();
        logger.v("bitmap cache data read path: $dbPath, bytes: ${bytes.length}");
        var img = await decodeImageFromList(bytes as Uint8List);
        _bitmapCache[key] = img;
        return img;
      } catch(e){
        logger.e("bitmap cache data read decode fail: $e");
      }
    }
    logger.v("bitmap cache data read path is not exists");
    return null;
  }

}