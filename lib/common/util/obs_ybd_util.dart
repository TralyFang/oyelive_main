import 'package:get/get.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/abstract/analytics/imp/analytics_ybd_page_imp.dart';
import 'package:oyelive_main/common/constant/short_ybd_route.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';

class YBDObsUtil {
  static YBDObsUtil? _instance;

  static YBDObsUtil instance() {
    if (_instance == null) {
      _instance = YBDObsUtil._();
    }
    return _instance!;
  }

  YBDObsUtil._();

  Set<String> routeSets = Set();

  // 这个组数不具备唯一性，没有处理去重，没有重复压栈建议使用routeSets
  List<String> routeList = [];

  push({String? currentRoute, String? previousRoute}) {
    logger.v('obs push route enter: ${Get.currentRoute} ');
    logger.v(
        'obs push route enter: currentRoute:$currentRoute    previousRoute:$previousRoute');
    String cur = currentRoute ?? Get.currentRoute;
    String pre = previousRoute ?? Get.previousRoute;
    // alog.v('currentRoute:$cur   pre:$pre');
    if (cur.isEmpty) return;
    if (cur == YBDNavigatorHelper.re_login) {
      YBDObsUtil.instance().routeList = [];
      YBDObsUtil.instance().routeSets = Set();
    }
    String subCurRoute = cur.subStringAntIndex('?');
    routeSets.add(subCurRoute);
    logger.v(
        'obs push route end1: $subCurRoute, ${routeList.indexOf(subCurRoute, routeList.length - 1)} ${routeList.length}');
    if (routeList.isEmpty ||
        routeList.indexOf(subCurRoute, routeList.length - 1) !=
            (routeList.length - 1)) {
      routeList.add(subCurRoute);
    }
    logger.v(
        'obs push route end: ${Get.currentRoute}, $routeSets $routeList ${routeList.last}');
    if (currentRoute != null)
      YBDAnalyticsPageImp()
          .page(previousRoute: pre.shortRoute, currentPage: cur.shortRoute);
  }

  pop({required String currentRoute, String? previousRoute}) {
    alog.v('currentRoute:$currentRoute   pre:$previousRoute');
    logger
        .v('obs pop route enter: $currentRoute,$previousRoute,set:$routeSets');
    if (currentRoute.isEmpty) return;
    String cur = currentRoute.subStringAntIndex('?');
    String pre = previousRoute?.subStringAntIndex('?') ?? Get.previousRoute;
    routeSets.remove(cur);
    // if(routeList.last.contains(currentRoute.subStringAntIndex('?'))){
    routeList.remove(cur);
    // }
    logger.v('obs pop route end: $currentRoute, $routeSets $routeList');
    if (previousRoute == null) return;
    YBDAnalyticsPageImp().pop(
        currentPage: currentRoute.shortRoute,
        previousRoute: previousRoute.shortRoute);
  }

  void tabRouteChange(
      {required List<String> routeList,
      required int curIndex,
      required int preIndex,
      String? preRoute}) {
    YBDObsUtil.instance()
        .pop(currentRoute: routeList[preIndex], previousRoute: preRoute);
    YBDObsUtil.instance().push(
        currentRoute: routeList[curIndex], previousRoute: routeList[preIndex]);
  }

  void tabRouteChangexf6PXoyelive(
      {required List<String> routeList,
      required int curIndex,
      required int preIndex,
      String? preRoute}) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 路由简称
  Map<String, String> shortRoutes = {
    YBDNavigatorHelper.login: YBDShortRoute.login,
    '${YBDNavigatorHelper.login_indoor}/false': YBDShortRoute.login_indoor,
    YBDNavigatorHelper.index_page: YBDShortRoute.index_page,
    YBDNavigatorHelper.home_page: YBDShortRoute.home,
    // 'flutter://ybd_index?index=0&sub=0': YBDShortRoute.home_related,
    // 'flutter://ybd_index?index=0&sub=1': YBDShortRoute.home_pop,
    // 'flutter://ybd_index?index=0&sub=2': YBDShortRoute.home_explore,
    // 'flutter://ybd_index?index=0&sub=3': YBDShortRoute.home_game,
    YBDNavigatorHelper.combo_list: YBDShortRoute.home_pop_combo,
    YBDNavigatorHelper.cp_list: YBDShortRoute.home_pop_team,
    YBDNavigatorHelper.leaderboard: YBDShortRoute.home_pop_rank,
    'flutter://leaderboard/0': YBDShortRoute.home_pop_rank_talent,
    'flutter://leaderboard/1': YBDShortRoute.home_pop_rank_gifter,
    'flutter://leaderboard/2': YBDShortRoute.home_pop_rank_follower,
    YBDNavigatorHelper.search: YBDShortRoute.home_search,
    YBDNavigatorHelper.game_room_page: YBDShortRoute.home_game_room,
    YBDNavigatorHelper.room_fans: YBDShortRoute.live_gifter_rank,
    // 'flutter://ybd_index?index=2': YBDShortRoute.inbox,
    YBDNavigatorHelper.inbox_private_message: YBDShortRoute.inbox_fri_msg,
    YBDNavigatorHelper.inbox_setting: YBDShortRoute.inbox_fri_msg_set,
    YBDNavigatorHelper.system_msg_page: YBDShortRoute.inbox_sys_msg,
    YBDNavigatorHelper.like_msg_page: YBDShortRoute.like_msg_page,
    YBDNavigatorHelper.comment_msg_page: YBDShortRoute.comment_msg_page,
    YBDNavigatorHelper.gift_msg_page: YBDShortRoute.gift_msg_page,
    YBDNavigatorHelper.top_up: YBDShortRoute.my_top_up,
    YBDNavigatorHelper.top_up_record: YBDShortRoute.my_top_up_rec,
    YBDNavigatorHelper.daily_task: YBDShortRoute.my_task,
    YBDNavigatorHelper.setting: YBDShortRoute.my_set,
    YBDNavigatorHelper.room_blacklist: YBDShortRoute.my_set_blacklist_room,
    YBDNavigatorHelper.inbox_blacklist: YBDShortRoute.my_set_blacklist_msg,
    // YBDNavigatorHelper.slog_blacklist: YBDShortRoute.my_set_blacklist_slog,
    YBDNavigatorHelper.about: YBDShortRoute.my_set_about,
    YBDNavigatorHelper.lite_version: YBDShortRoute.my_lite_ver,
    YBDNavigatorHelper.pk_record: YBDShortRoute.my_pk,
    YBDNavigatorHelper.edit_profile: YBDShortRoute.my_profile_edit,
    YBDNavigatorHelper.my_invites: YBDShortRoute.my_invites,
    YBDNavigatorHelper.badges: YBDShortRoute.my_badges,
    YBDNavigatorHelper.guardian: YBDShortRoute.my_guardian,
    YBDNavigatorHelper.vip: YBDShortRoute.my_vip,
    YBDNavigatorHelper.store: YBDShortRoute.my_store,
    YBDNavigatorHelper.baggage: YBDShortRoute.my_baggage,
    YBDNavigatorHelper.user_profile: YBDShortRoute.my_profile,
    // YBDNavigatorHelper.status_post_audio_record: YBDShortRoute.new_slog,
    YBDNavigatorHelper.start_live_page: YBDShortRoute.new_live,
    YBDNavigatorHelper.event_msg_page: YBDShortRoute.event_msg_page,
  };

  /// 已经打开路由
  bool containsRoute(String routeName) {
    return routeList.contains(routeName);
  }

  void containsRouteYrV59oyelive(String routeName) {
    int needCount = 0;
    print('input result:$needCount');
  }
}
