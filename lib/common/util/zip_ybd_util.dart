


import 'dart:io';

import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../ui/page/home/entity/car_ybd_list_entity.dart';
import '../../ui/page/home/entity/gift_ybd_list_entity.dart';
import '../../ui/page/store/entity/downloadbean.dart';
import 'common_ybd_util.dart';
import 'log_ybd_util.dart';
import 'upload_ybd_s3_util.dart';

class YBDZipUtil {
  /// fileName：自定义解压后的文件名
  // TODO: jackie - 暂时不支持自定义路径名
  static Future<bool> asyncUnZip(String outPutFile, String? zipFile, {String? fileName}) async {
    if (zipFile == null || zipFile == "") {
      Fluttertoast.showToast(msg: "zipFile is null！");
      logger.v("unZip failed zipFile:$zipFile");
      return false;
    }

    if (!File(zipFile).existsSync()) {
      logger.v("unZip failed zipFile:$zipFile");
      Fluttertoast.showToast(msg: "zipFile not exist！");
      return false;
    }
    // 从磁盘读取Zip文件。
    List<int> bytes = File(zipFile).readAsBytesSync();
    // 解码Zip文件
    Archive archive = ZipDecoder().decodeBytes(bytes);

    // 将Zip存档的内容解压缩到磁盘。
    for (ArchiveFile file in archive) {
      if (file.isFile) {
        List<int> data = file.content;

        // 获取解压文件
        File(outPutFile + "/" + unzipFileName(file.name, customFileName: fileName))
          ..createSync(recursive: true)
          ..writeAsBytesSync(data);
      } else {
        Directory(zipFile + "/" + file.name)..create(recursive: true);
      }
    }
    logger.v("unZip success  delete zip $zipFile start");
    await File(zipFile).delete();
//    Fluttertoast.showToast(msg: "unZip success！");
    return true;
  }

  /// 解压皮肤zip文件，解压成功后删除zip文件
  /// 前提条件：zip中只包含图片文件，不包含文件夹
  /// [targetDir] 解压后存放文件的目标路径
  /// [zipFile] zip文件
  static Future<void> unZipSkin({
    required String targetDir,
    required String zipFile,
  }) async {
    if (!File(zipFile).existsSync()) {
      logger.e('skin zip not exist');
      YBDCommonUtil.storeSkinError('skin zip not exist');
      return;
    }

    try {
      // 从磁盘读取zip文件
      List<int> bytes = File(zipFile).readAsBytesSync();

      // 解码zip文件
      Archive archive = ZipDecoder().decodeBytes(bytes);

      // 将zip存档的内容解压缩到目录
      for (ArchiveFile file in archive) {
        if (file.isFile) {
          List<int> data = file.content;

          logger.v('create target file to store content');
          // 创建目标文件
          File('$targetDir/${file.name}').createSync();

          // 获取目标文件路径
          File targetFile = File('$targetDir/${file.name}');
          logger.v('target file path: ${targetFile.path}');

          // 把内容写入目标文件
          targetFile.writeAsBytesSync(data);
        } else {
          logger.v('unzip skin not support directory');
        }
      }

      logger.v("unzip success delete skin zip");
      // 解压成功后删除zip文件
      await File(zipFile).delete();
    } catch (e) {
      logger.e('unzip skin zip error: $e');
      // 解压报错删除zip文件
      await File(zipFile).delete();
      YBDCommonUtil.storeSkinError('unzip skin zip error: $e');
    }
  }

  /// 把svga的zip文件解压到指定目录
  /// zip文件规定格式：.svga或者.svga + .mp3
  /// [outDir]解压后文件存放的目录
  /// [zip]要解压的zip文件
  /// return: true 解压成功，false 解压错误
  static bool unzipSvgaAnimationZipFile({String? outDir, File? zip}) {
    if (null == outDir || outDir.isEmpty) {
      logger.v('svga outDir not exist');
    }

    if (null == zip || !zip.existsSync()) {
      logger.v('zip file not exist');
      return false;
    }

    List<int> bytes = zip.readAsBytesSync();
    Archive archive = ZipDecoder().decodeBytes(bytes);

    for (ArchiveFile file in archive) {
      if (file.isFile) {
        List<int> data = file.content;
        File(outDir! + "/" + file.name)
          ..createSync(recursive: true)
          ..writeAsBytesSync(data);
      }
    }

    return true;
  }

  /// 获取解压后的文件名
  static String unzipFileName(String oldFileName, {String? customFileName}) {
    // 默认显示原文件名
    String outPutFileName = oldFileName;

    if (null != customFileName) {
      // 后缀名
      String extensionName = YBDUploadS3Util.fileExtension(oldFileName);

      // 拼接自定义文件名
      outPutFileName = '$customFileName$extensionName';
    }

    return outPutFileName;
  }

  /// 解压座驾动画文件
  static Future<bool> unzipSvga(int unzipIndex, List<YBDDownLoadBean> downLoadList, List<YBDCarListRecord?>? carList) async {
    if (unzipIndex >= 0 && downLoadList != null && downLoadList.length > unzipIndex) {
      for (int i = 0; i < carList!.length; i++) {
        if (downLoadList[unzipIndex].fileName == carList[i]!.animationInfo!.name) {
          String path = 'Animation/${carList[i]!.animationInfo!.foldername}/${carList[i]!.animationInfo!.version}';
          path = await YBDCommonUtil.getResourceDir(path);
          String pathZip = path + '/' + carList[i]!.animationInfo!.name!;
          bool unzip = await YBDZipUtil.asyncUnZip(path, pathZip);
          logger.v('unzip :$unzip _carRecordList[i].id:${carList[i]!.id}');
          logger.v("_unzipSvga path:$path pathZip:$pathZip");
          return unzip;
        }
      }
    }
    return false;
  }

  /// 解压礼物动画文件
  static Future<bool> unzipGiftAnimation(
      int unzipIndex, List<YBDDownLoadBean> downLoadList, List<YBDGiftListRecordGift?>? giftList) async {
    if (unzipIndex >= 0 && downLoadList != null && downLoadList.length > unzipIndex) {
      for (int i = 0; i < (giftList?.length ?? 0); i++) {
        if (downLoadList[unzipIndex].fileName == giftList![i]!.animationInfo!.name) {
          String path = 'Animation/${giftList[i]!.animationInfo!.foldername}/${giftList[i]!.animationInfo!.version}';
          path = await YBDCommonUtil.getResourceDir(path);
          String pathZip = path + '/' + giftList[i]!.animationInfo!.name;
          bool unzip = await YBDZipUtil.asyncUnZip(path, pathZip);
          logger.v('unzip :$unzip unzipGiftAnimation[i].id:${giftList[i]!.id}');
          logger.v("unzipGiftAnimation path:$path pathZip:$pathZip");
          return unzip;
        }
      }
    }
    return false;
  }
}
