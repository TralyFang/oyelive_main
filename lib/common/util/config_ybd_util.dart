


import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/module/entity/firebase_ybd_log_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/activty/entity/activity_ybd_skin_entity.dart';

import '../../generated/json/base/json_convert_content.dart';
import '../../module/entity/query_ybd_configs_resp_entity.dart';
import '../../module/entity/unique_ybd_id_entity.dart';
import '../../redux/app_ybd_state.dart';
import '../../ui/page/splash/splash_ybd_util.dart';
import '../constant/const.dart';
import 'common_ybd_util.dart';
import 'file_ybd_util.dart';
import 'log_ybd_util.dart';
import 'numeric_ybd_util.dart';
import 'sp_ybd_util.dart';

YBDConfigUtilX ConfigUtil = YBDConfigUtilX.instance;

/// 处理配置项的工具类
class YBDConfigUtilX {
  YBDConfigUtilX._();
  static YBDConfigUtilX? _instance;
  static YBDConfigUtilX get instance => _instance ??= YBDConfigUtilX._();

  /// mock类调用的构造方法
  YBDConfigUtilX.mock();

  /// 上传日志的配置项
  YBDFirebaseLogEntity? _firebaseLogConfig;
  YBDFirebaseLogEntity? getFirebaseLogConfig() {
    if (null == _firebaseLogConfig) {
      var configStr = YBDCommonUtil.storeFromContext()!.state.configs?.firebaseLog;

      if (null == configStr) {
        // 没有配置默认关闭日志
        return YBDFirebaseLogEntity(isOpen: false);
      }

      // 解析配置
      _firebaseLogConfig = YBDFirebaseLogEntity.fromString(configStr);
    }

    return _firebaseLogConfig;
  }

  /// 是否显示游戏 allPlatform: 所有平台判断
  Future<bool> shouldShowGame({BuildContext? context, bool allPlatform = false}) async {
    if (Platform.isIOS || allPlatform) {
      // iOS 审核期间不显示游戏
      // 配置项中的版本信息
      YBDAppVersionConfig? appConfig = await appVersionInfo(context);

      // 白名单的用户直接展示全部内容
      int? userId = YBDUserUtil.getUserIdSync;
      List<int>? whiteList = appConfig?.whiteList;
      if (whiteList != null && userId != null && whiteList.contains(userId)) {
        return true;
      }

      // 安装包信息
      PackageInfo packageInfo = await PackageInfo.fromPlatform();

      // 从配置项里获取的版本号
      // 默认 0，隐藏游戏
      int configVersionCode = appConfig?.buildCode ?? 0;

      // 安装包版本号大于配置项版本号时隐藏游戏
      if (YBDNumericUtil.stringToInt(packageInfo.buildNumber) > configVersionCode) {
        return false;
      } else {
        return true;
      }
    } else {
      // 其他平台显示游戏
      return true;
    }
  }
  void shouldShowGameBSX82oyelive({BuildContext? context, bool allPlatform = false})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取对应平台的版本配置项
  Future<YBDAppVersionConfig?> appVersionInfo(BuildContext? context) async {
    final configInfo = await loadConfigInfo(context);

    if (Platform.isIOS) {
      logger.v('===ios config : ${configInfo.appVersionConfig?.iosConfig?.toJson()}');
      return configInfo.appVersionConfig?.iosConfig;
    } else {
      logger.v('===android config : ${configInfo.appVersionConfig?.androidConfig?.toJson()}');
      return configInfo.appVersionConfig?.androidConfig;
    }
  }

  /// 从store获取皮肤配置项
  YBDActivitySkinEntity? skinConfig(YBDQueryConfigsRespEntity configsEntity) {
    // 从store获取配置项
    // final configInfo = loadFromStore(YBDCommonUtil.storeFromContext());

    YBDActivitySkinEntity? result = YBDActivitySkinEntity();
    try {
      logger.v('from store skin config : ${configsEntity.record!.skinConfig}');
      result = JsonConvert.fromJsonAsT<YBDActivitySkinEntity>(jsonDecode(configsEntity.record?.skinConfig ?? ''));
    } catch (e) {
      logger.e('===get skin config error: $e');
    }

    return result;
  }

  /// 签约主播 url 配置项
  Future<String?> talentApplyUrl(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config talentApplyUrl : ${configInfo.talentApplyUrl}');
    return configInfo.talentApplyUrl;
  }

  /// 获取预订单 url 配置项
  Future<String> razorpayServerUrl(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config razorpayServerUrl : ${configInfo.razorpay_server_url}');
    String? url = configInfo.razorpay_server_url;
    if (url == null) {
      // 如果取不到 url，取正式服的 url。
      url = Const.RPS_URL;
    }
    return url;
  }
  void razorpayServerUrluVYOhoyelive(BuildContext context)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 代理充值页面的默认地址
  Future<String> resellerUrl(BuildContext context) async {
    String url = YBDCommonUtil.getRoomOperateInfo().resellerIosUrl ?? Const.TOPUP_AGENCY;
    return url;
  }
  void resellerUrl9UNwZoyelive(BuildContext context)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 靓号配置项
  Future<List<YBDUniqueIDEntity?>?> uniqueIds(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config uniqueIds : ${configInfo.uniqueIds}');
    return configInfo.uniqueIds;
  }

  /// 登录页面是否显示手机号登录的配置项
  Future<String?> loginShowSMS(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config loginShowSMS : ${configInfo.loginShowSMS}');
    return configInfo.loginShowSMS;
  }

  /// 充值活动配置项
  Future<String?> topupOffer(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config topupOffer : ${configInfo.topupOffer}');
    return configInfo.topupOffer;
  }

  /// combo 活动配置项
  Future<String?> masterOfCombos(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config masterOfCombos : ${configInfo.masterOfCombos}');
    return configInfo.masterOfCombos;
  }

  /// 送礼活动配置项
  Future<String?> presentActivity(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config presentActivity : ${configInfo.presentActivity}');
    return configInfo.presentActivity;
  }

  /// 开斋节活动配置项
  Future<String?> eidSpecialActivity(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config eidSpecialActivity : ${configInfo.eidSpecialActivity}');
    return configInfo.eidSpecialActivity;
  }

  /// 充值送礼活动配置项
  Future<String?> topUpGiftActivity(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config topUpGiftActivity : ${configInfo.topUpGiftActivity}');
    return configInfo.topUpGiftActivity;
  }

  /// 推送主播配置项
  Future<String?> pushChannelVipTalent(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config pushChannelVipTalent : ${configInfo.pushChannelVipTalent}');
    return configInfo.pushChannelVipTalent;
  }

  /// s3 地址配置项
  Future<String?> amazonS3Address2(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config amazonS3Address2 : ${configInfo.amazonS3Address2}');
    return configInfo.amazonS3Address2;
  }

  /// s3 桶名配置项
  Future<String?> amazonS3BucketName2(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config amazonS3BucketName2 : ${configInfo.amazonS3BucketName2}');
    return configInfo.amazonS3BucketName2;
  }

  /// android 平台版本信息配置项
  Future<String?> appAndroid(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config appAndroid : ${configInfo.appAndroid}');
    return configInfo.appAndroid;
  }

  /// 白名单国家配置项
  Future<String?> whitelistCountries(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config whitelistCountries : ${configInfo.whitelistCountries}');
    return configInfo.whitelistCountries;
  }

  /// 根据用户等级上传头像的配置项
  Future<String?> ulevelPrivilegeAvatar(BuildContext? context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config ulevelPrivilegeAvatar : ${configInfo.ulevelPrivilegeAvatar}');
    return configInfo.ulevelPrivilegeAvatar;
  }

  /// 根据用户等级开播的配置项
  Future<String?> ulevelGoLive(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config ulevelGoLive : ${configInfo.ulevelGoLive}');
    return configInfo.ulevelGoLive;
  }

  /// 根据主播等级开播的配置项
  Future<String?> tlevelGoLive(BuildContext context) async {
    final configInfo = await loadConfigInfo(context);
    logger.v('===config tlevelGoLive : ${configInfo.tlevelGoLive}');
    return configInfo.tlevelGoLive;
  }

  /// 获取配置项
  Future<YBDConfigInfo> loadConfigInfo(BuildContext? context) async {
    YBDConfigInfo? configInfo = loadFromStore(YBDCommonUtil.storeFromContext(context: context));

    // store 取不到时再从文件里读取
    if (null == configInfo) {
      logger.v('===load config from file');

      // 刷新 store
      _refreshStore(context, configInfo);
      configInfo = await loadFromFile();
    }

    // 从文件取不到时再从资源包里读取
    if (null == configInfo) {
      logger.v('===load config from assets');
      configInfo = await loadFromAssets();
    }

    return configInfo!;
  }
  void loadConfigInfoSVDWjoyelive(BuildContext? context)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 刷新 store
  void _refreshStore(BuildContext? context, YBDConfigInfo? configInfo) {
    logger.v('===_refreshStore start');

    try {
      // 从接口获取最新的配置信息然后更新 store
      Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
      YBDSplashUtil.refreshAppConfig(context, store);
    } catch (e) {
      logger.v('===_refreshStore error : $e');
    }

    logger.v('===_refreshStore complete');
  }

  /// 从 assets 获取配置项
  Future<YBDConfigInfo?> loadFromAssets() async {
    final configEntity = await readConfigFromAssets();
    if (null == configEntity) {
      logger.v('asset config is null');
      return null;
    }

    // 从 response 获取配置项
    final configInfo = configEntity.record;
    if (null == configInfo) {
      logger.v('assets config file record is null');
      return null;
    }

    logger.v('===config info from assets : ${configInfo.toJson()}');
    return configInfo;
  }

  /// 从本地文件加载配置项
  Future<YBDConfigInfo?> loadFromFile() async {
    final configEntity = await readConfig();

    if (null == configEntity) {
      logger.v('local config file is null');
      return null;
    }

    // 从 response 获取配置项
    final configInfo = configEntity.record;
    if (null == configInfo) {
      logger.v('local config file record is null');
      return null;
    }

    logger.v('===config info from file : ${configInfo.toJson()}');
    return configInfo;
  }

  /// 从 store 里获取配置项
  YBDConfigInfo? loadFromStore(Store<YBDAppState>? store) {
    final configEntity = store?.state.configs;
    return configEntity;
  }

  /// 从资源包里里读取配置信息
  Future<YBDQueryConfigsRespEntity?> readConfigFromAssets() async {
    logger.v('load app config from assets');
    YBDQueryConfigsRespEntity? localConfigEntity;

    // 如果从接口下载的配置信息为空则从资源文件里获取
    // 一般新用户才会出现这种场景
    String jsonFile = '';
    dynamic isTestEnv = await YBDSPUtil.get(Const.SP_TEST_ENV);
    if (isTestEnv == null) {
      jsonFile = Const.TEST_ENV ? "assets/data/app_config_test_env.json" : "assets/data/app_config.json";
    } else {
      jsonFile = isTestEnv as bool ? "assets/data/app_config_test_env.json" : "assets/data/app_config.json";
    }

    try {
      String jsonStr = await rootBundle.loadString(jsonFile);
      Map? mapData = json.decode(jsonStr);
      localConfigEntity = JsonConvert.fromJsonAsT<YBDQueryConfigsRespEntity>(mapData);
    } catch (e) {
      logger.v('load assets app config failed');
    }

    return localConfigEntity;
  }

  /// 配置信息写到本地文件
  Future<void> writeConfig(YBDQueryConfigsRespEntity queryConfigsResp) async {
    try {
      // 把配置信息保存到本地 json 文件
      YBDFileUtil.writeJsonToFile(json.encode(queryConfigsResp.toJson()), 'appConfig.json');
    } catch (e) {
      logger.e('write app config error : $e');
    }
  }
  void writeConfigrC1S4oyelive(YBDQueryConfigsRespEntity queryConfigsResp)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 从本地读取配置信息
  Future<YBDQueryConfigsRespEntity?> readConfig() async {
    try {
      String? configJson = await YBDFileUtil.readJsonFromFile('appConfig.json');
      if (null == configJson) {
        logger.v('config json is null');
        return null;
      }

      Map? mapData = json.decode(configJson);
      YBDQueryConfigsRespEntity? queryConfigsResp = JsonConvert.fromJsonAsT<YBDQueryConfigsRespEntity>(mapData);
      return queryConfigsResp;
    } catch (e) {
      logger.e('read app config error : $e');
      return null;
    }
  }
}
