
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/date_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/entity/db_ybd_dynamic_link_entity.dart';
import 'package:oyelive_main/module/status/entity/db_ybd_status_entity.dart';
import 'package:oyelive_main/module/status/entity/status_ybd_list_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/recharge/entity/db_ybd_inApp_purchase_entity.dart';
import 'package:oyelive_main/ui/page/recharge/entity/db_ybd_order_purchase_entity.dart';

class YBDDataBaseUtil {
  Database? db;
  int? userId = 0;
  static YBDDataBaseUtil? instance;

  static YBDDataBaseUtil getInstance() {
    logger.v('get data base instance');
    if (instance == null) {
      instance = YBDDataBaseUtil();
    }
    return instance!;
  }

  String sql_createTable =
      'Create Table If Not Exists  `$tableName`(`id` INTEGER  Primary key AUTOINCREMENT,`$columnUserId` INTEGER,`$columnStatusScene` text,`$columnStatusConent` text)';
  String sql_dynamic_link_table =
      'create table if not exists t_dynamic_link (`id` integer primary key autoincrement, `inviter` integer, `spf` text not null, `time` integer, `link` text not null, `type` text not null, `rid` integer, `sid` text, `prefix` text not null)';

  String sql_inApp_purchase_table =
      'Create Table If Not Exists  `$inAppPurchaseTableName`(`id` INTEGER,`$cUserID` INTEGER,`$cPurchaseUserID` text,`$cOrderID` text Primary key,`$cSignedData` text,`$cSignature` text,`$cSPOrderID` text,`$cServiceState` INTEGER,`$cChannel` INTEGER,`$cChannelState` INTEGER,`$cCreateTime` INTEGER)';

  String sql_order_purchase_table =
      'Create Table If Not Exists  `$orderPurchaseTableName`(`$orderUserID` INTEGER,`$orderOrderID` text Primary key,`$orderProductID` text,`$orderCreateTime` INTEGER,`$orderSignedData` text,`$orderSignature` text,`$orderSPOrderID` text,`$orderChannel` INTEGER,`$orderState` INTEGER)';

  ///添加掉单表 字段
  String sql_inApp_purchase_table_update =
      'Alter Table `$inAppPurchaseTableName` drop primary key,add primary key (\'$cOrderID\'), add $cPurchaseUserID` text,`$cServiceState` INTEGER,`$cChannel` INTEGER,`$cChannelState` INTEGER,`$cCreateTime` INTEGER)';

  ///添加预订单表 字段
  String sql_order_purchase_table_add_channel = 'Alter Table `$orderPurchaseTableName`  add $orderChannel INTEGER';
  String sql_order_purchase_table_add_state = 'Alter Table `$orderPurchaseTableName`  add $orderState INTEGER';

  String sql_inApp_purchase_tem_table =
      'Create Table If Not Exists  `$inAppPurchaseTemTableName`(`id` INTEGER,`$cUserID` INTEGER,`$cPurchaseUserID` text,`$cOrderID` text Primary key,`$cSignedData` text,`$cSignature` text,`$cSPOrderID` text,`$cServiceState` INTEGER,`$cChannel` INTEGER,`$cChannelState` INTEGER,`$cCreateTime` INTEGER)';

  Future initDataBase() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, "flutter_oyetalk.db");

    /// version:
    /// 1 - 新增动态页面缓存
    /// 2 - 新增搜索用户页面搜索记录; 新增 生成firebase dynamic 短链记录
    db = await openDatabase(path, version: 5, onCreate: (Database db, int version) async {
      debugPrint('Database onCreate... version: $version');
      debugPrint(
          'Database sql_inApp_purchase_table: $sql_inApp_purchase_table, sql_order_purchase_table: $sql_order_purchase_table sql_inApp_purchase_table_update:$sql_inApp_purchase_table_update');
      /*  sql_inApp_purchase_table =
          'Create Table If Not Exists  `$inAppPurchaseTableName`(`id` INTEGER,`$cUserID` INTEGER,`$cPurchaseUserID` text,`$cOrderID` text Primary key,`$cSignedData` text,`$cSignature` text,`$cSPOrderID` text,`$cServiceState` INTEGER,`$cChannel` INTEGER,`$cChannelState` INTEGER,`$cCreateTime` INTEGER)';

*/
      try {
        /// 动态页面缓存数据
        await db.execute(sql_createTable);

        /// 生成firebase dynamic 短链记录
        await db.execute(sql_dynamic_link_table);

        ///google 充值记录本地保存
        await db.execute(sql_inApp_purchase_table);

        ///google 预订单记录本地保存
        await db.execute(sql_order_purchase_table);
      } catch (e) {
        YBDLogUtil.e('YBDDataBaseUtil openDatabase: e:$e');
      }

//      await db.close();
    }, onUpgrade: (Database db, int oldVersion, int newVersion) async {
      debugPrint('Database onUpgrade... oldVersion: $oldVersion, newVersion: $newVersion');

      try {
        if (newVersion == 2) {
          /// 生成firebase dynamic 短链记录
          await db.execute(sql_dynamic_link_table);
        } else if (newVersion == 3) {
          /// google 异常订单记录表
          await db.execute(sql_inApp_purchase_table);
        } else if (newVersion == 4) {
          /// google 异常订单记录表
          await db.execute(sql_inApp_purchase_table);

          /// google 预订单记录记录表
          await db.execute(sql_order_purchase_table);
        } else if (newVersion == 5) {
          /// google 表结构修改 （苹果支付也需要）
          // await test();
          logger.v("sql_order_purchase_table_add_state : $sql_order_purchase_table_add_state");
          logger.v("sql_order_purchase_table_add_channel : $sql_order_purchase_table_add_channel");

          ///预订单表添加字段
          await db.execute(sql_order_purchase_table_add_state);
          await db.execute(sql_order_purchase_table_add_channel);

          await updateInAppPurchaseTable(db);
        }
      } catch (e) {
        YBDLogUtil.e('YBDDataBaseUtil onUpgrade: e:$e');
      }
    });

    setUserId();
    return db;
  }
  void initDataBaseUTvUCoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  setUserId() async {
    String? spUser = await YBDSPUtil.get(Const.SP_USER_INFO);
    if (null != spUser && spUser.isNotEmpty) {
      userId = YBDUserInfo().fromJson(json.decode(spUser)).id;
    }
  }

  /// 检查用户ID，没有重新取
  checkUser() async {
    if (userId == null || userId == 0) {
      await setUserId();
    }
  }

  // 根据场景查询
  Future<YBDDBStatusEntity?> queryStatus(StatusScene scene) async {
    List<Map> maps;
    try {
      maps = await db!.query(tableName,
          columns: [
            columnId,
            columnUserId,
            columnStatusScene,
            columnStatusConent,
          ],
          //        orderBy: "$columnMessageTime DESC",
          where: '$columnUserId=? and $columnStatusScene=?',
          whereArgs: [userId, scene.toString().split(".").last]);
    } catch (e) {
      logger.v(e);
      return null;
    }

    logger.v("mapLength : ${maps.length} ,map : $maps ,");
    if (maps == null || maps.length == 0) {
      return null;
    }

    List<YBDDBStatusEntity> messages = [];
    for (int i = 0; i < maps.length; i++) {
      messages.add(YBDDBStatusEntity.fromMap(maps[i] as Map<String, dynamic>));
    }
    logger.v("entity : $messages");

    return messages.first;
  }

  deleteAt(StatusScene scene) async {
    await db!.delete(
      tableName,
      where: "$columnUserId = ? and $columnStatusScene = ?",
      whereArgs: [userId, scene.toString().split('.').last],
    );
  }

  // 插入
  insert(YBDStatusListEntity statusListEntity, StatusScene scene) async {
    await deleteAt(scene);
    var result = await db!.insert(
      tableName,
      YBDDBStatusEntity(
        userId,
        scene.toString().split('.').last,
        json.encode(statusListEntity.toJson()),
      ).toMap(),
    );
    return result;
  }

  // 删除表所有数据
  Future clearTable() async {
    return await db!.execute("DELETE FROM : $tableName");
  }
  void clearTablepk0feoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询firebase dynamic 短链
  Future<String?> queryDynamicLink(int inviter, String spf, String type, String? prefix, {int? rid, String? sid}) async {
    List<Map> maps;
    try {
      maps = await db!.query('t_dynamic_link',
          columns: [
            'id',
            'inviter',
            'spf',
            'time',
            'link',
            'type',
            'rid',
            'sid',
            'prefix',
          ],
          limit: 1,
          where: 'inviter=? and spf=? and type=? and rid=? and sid=? and prefix=?',
          whereArgs: [inviter, spf, type, rid ?? -1, sid ?? "-1", prefix]);
    } catch (e) {
      print(e);
      return null;
    }

    print("search dynamic link, data: $maps");
    if (maps == null || maps.isEmpty) {
      return null;
    }

    YBDDBDynamicLinkEntity dynamicLink = YBDDBDynamicLinkEntity.fromMap(maps.first as Map<String, dynamic>);

    /// 动态链接仅缓存一天
    if (DateFormat('yyyy-MM-dd').format(DateTime.fromMillisecondsSinceEpoch(dynamicLink.time!)) !=
        DateFormat('yyyy-MM-dd').format(DateTime.now())) {
      deleteDynamicLink(dynamicLink.id);
    }
    return dynamicLink.link;
  }

  /// 删除firebase dynamic 短链
  Future<int> deleteDynamicLink(int? id) async {
    return await db!.delete('t_dynamic_link', where: "id = ? ", whereArgs: [id]);
  }
  void deleteDynamicLinkzMB4ioyelive(int? id)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 插入firebase dynamic 短链
  /// type: invite , room ,  status
  Future<int> insertDynamicLink(int inviter, String spf, String type, String link, String? prefix,
      {int? rid, String? sid}) async {
    int result = await db!.insert(
        't_dynamic_link',
        YBDDBDynamicLinkEntity(inviter, spf, DateTime.now().millisecondsSinceEpoch, link, type, prefix,
                rid: rid ?? -1, sid: sid ?? "-1")
            .toMap());
    return result;
  }

  /// 清空动态链接
  Future<int> clearDynamicLink() async {
    return await db!.delete('t_dynamic_link');
  }
  void clearDynamicLinkA4dRRoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///----------google充值记录本地操作------start
  inAppAdd(YBDDBInAppPurchaseEntity dbInAppPurchaseEntity) async {
    YBDLogUtil.d("inAppAdd ------------------------------------");
    if (dbInAppPurchaseEntity.orderID == null) {
      YBDLogUtil.d("inAppAdd orderID is null!");
      return;
    }
    var result;
    try {
      result = await db!.insert(
          inAppPurchaseTableName,
          YBDDBInAppPurchaseEntity(
            dbInAppPurchaseEntity.usrId != null ? dbInAppPurchaseEntity.usrId : int.tryParse(await YBDSPUtil.getUserId() ?? ''),
            dbInAppPurchaseEntity.purchaseUserID,
            dbInAppPurchaseEntity.orderID,
            dbInAppPurchaseEntity.signedData,
            dbInAppPurchaseEntity.signature,
            dbInAppPurchaseEntity.spOrderID,
            dbInAppPurchaseEntity.serviceState,
            YBDDateUtil.currentUtcTimestamp(),
            dbInAppPurchaseEntity.channel,
            dbInAppPurchaseEntity.channelState,
          ).toMap());
    } catch (e) {
      YBDLogUtil.e("insert google order: ${dbInAppPurchaseEntity.orderID} error：$e");
    }
    YBDLogUtil.d("inAppAdd orderID : ${dbInAppPurchaseEntity.orderID} result:$result");

    ///测试数据
    // inAppQuery(userId);
    return result;
  }

  ///判断是否有存储有这个订单
  Future<bool> isOrderExist(YBDDBInAppPurchaseEntity dbInAppPurchaseEntity) async {
    List<YBDDBInAppPurchaseEntity>? message =
        await inAppQueryByOrderID(dbInAppPurchaseEntity.orderID, dbInAppPurchaseEntity.usrId);
    if (message == null || message.isEmpty) {
      return false;
    }
    return true;
  }
  void isOrderExist1GDvyoyelive(YBDDBInAppPurchaseEntity dbInAppPurchaseEntity)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///修改订单状态
  /// stateType修改的状态类型 0（默认）我们服务器给的状态 1 第三方支付的订单状态
  inAppUpdate(YBDDBInAppPurchaseEntity dbInAppPurchaseEntity, {int stateType = 0}) async {
    YBDLogUtil.d("inAppUpdate  dbInAppPurchaseEntity:${dbInAppPurchaseEntity.toString()}");
    YBDLogUtil.d("inAppUpdate  stateType:$stateType");
    if (dbInAppPurchaseEntity.orderID == null) {
      YBDLogUtil.d("inAppUpdate orderID is null!");
      return;
    }
    var result;
    try {
      List<YBDDBInAppPurchaseEntity>? message =
          await inAppQueryByOrderID(dbInAppPurchaseEntity.orderID, dbInAppPurchaseEntity.usrId);

      ///正常支付没有回调，有可能就会没有数据
      if (message == null) {
        await inAppAdd(dbInAppPurchaseEntity);
        return;
      }

      var map;
      if (stateType == 0) {
        map = <String, dynamic>{
          cServiceState: dbInAppPurchaseEntity.serviceState,
        };
      } else {
        map = <String, dynamic>{
          cChannelState: dbInAppPurchaseEntity.channelState,
        };
      }

      result = await db!
          .update(inAppPurchaseTableName, map, where: "$cOrderID = ? ", whereArgs: [dbInAppPurchaseEntity.orderID]);
    } catch (e) {
      YBDLogUtil.e("inAppUpdate orderID: ${dbInAppPurchaseEntity.orderID} error");
    }
    YBDLogUtil.d("inAppUpdate orderID : ${dbInAppPurchaseEntity.orderID} result:$result");

    ///测试数据
    // inAppQuery(userId);
    return result;
  }

  ///获取订单状态
  /* Future<int> getPurchaseState(int curState, int oldSate) async {
    YBDLogUtil.d("getPurchaseState curState: $curState oldSate:$oldSate");

    ///新的状态是消耗成功
    if (curState == STATE_CONSUME) {
      ///如果老的状态是校验成功，则订单完成
      if (oldSate == STATE_VERIFY) {
        return STATE_SUCCESS;
      }

      ///否则状态改为消耗成功
      return STATE_CONSUME;
    }

    ///新的状态是校验成功
    if (curState == STATE_VERIFY) {
      ///如果老的状态是消耗成功，则订单完成
      if (oldSate == STATE_CONSUME) {
        return STATE_SUCCESS;
      }

      ///否则状态改为校验成功
      return STATE_VERIFY;
    }

    return curState;
  }*/

  inAppDelete(String orderID, int userId) async {
    var result;
    try {
      result = await db!
          .delete('$inAppPurchaseTableName', where: "$cOrderID = ? and $cUserID = ?", whereArgs: [orderID, userId]);
    } catch (e, s) {
      YBDLogUtil.e('inAppDelete google order: $orderID userId: $userId from db error');
    }
    YBDLogUtil.d("inAppDelete orderID : $orderID userId:$userId result:$result");
    return result;
  }

  inAppDeleteList(List<YBDDBInAppPurchaseEntity> dbInAppPurchaseLists, int userId) async {
    if (dbInAppPurchaseLists == null || dbInAppPurchaseLists.isEmpty) {
      return;
    }
    dynamic sql = 'delete from $inAppPurchaseTableName where $cUserID = $userId and $cOrderID in';
    StringBuffer valueBuffer = new StringBuffer();
    valueBuffer.write(sql);
    valueBuffer.write('(');
    for (int i = 0; i < dbInAppPurchaseLists.length; i++) {
      valueBuffer.write(dbInAppPurchaseLists[i].orderID);
      if (i < dbInAppPurchaseLists.length - 1) {
        valueBuffer.write(',');
      }
    }
    valueBuffer.write(')');
    YBDLogUtil.d("inAppDeleteList valueBuffer:$valueBuffer");

    var result;
    try {
      await db!.execute('$valueBuffer');
    } catch (e, s) {
      YBDLogUtil.e('inAppDeleteList Error: $e ');
    }
    return result;
  }

  List<YBDDBInAppPurchaseEntity>? _mapToPurchase(List<Map> maps) {
    try {
      YBDLogUtil.d("_mapToPurchase mapLength : ${maps.length} ,map : $maps ,");
      if (maps == null || maps.length == 0) {
        return null;
      }

      List<YBDDBInAppPurchaseEntity> messages = [];
      for (int i = 0; i < maps.length; i++) {
        messages.add(YBDDBInAppPurchaseEntity.fromMap(maps[i] as Map<String, dynamic>));
        YBDLogUtil.v("_mapToPurchase entity i:$i: ${messages[i]}");
      }
      return messages;
    } catch (e) {
      print(e);
    }
  }

  ///userId 用户ID  state支付状态  equal与state配合使用：是否取正，默认取正
  Future<List<YBDDBInAppPurchaseEntity>?> inAppQuery(int userId, {int serviceState = -1, bool equal = true}) async {
    YBDLogUtil.d("inAppQuery   ------------------------");
    YBDLogUtil.d("inAppQuery  userId: $userId serviceState:$serviceState equal:$equal");
    YBDLogUtil.d((serviceState != -1) ? '$cUserID=? and $cServiceState ${equal ? '=' : '!='} ?' : '$cUserID=?');
    List<Map> maps;
    try {
      maps = await db!.query(inAppPurchaseTableName,
          columns: [
            cUserID,
            cOrderID,
            cPurchaseUserID,
            cSignature,
            cSignedData,
            cSPOrderID,
            cServiceState,
            cCreateTime,
            cChannel,
            cChannelState,
          ],
          where: (serviceState != -1) ? '$cUserID=? and $cServiceState ${equal ? '=' : '!='} ?' : '$cUserID=?',
          whereArgs: (serviceState != -1) ? [userId, serviceState] : [userId]);
    } catch (e) {
      YBDLogUtil.e('inAppQuery query google order error, userid: $userId');
      return null;
    }

    YBDLogUtil.d("inAppQuery mapLength : ${maps.length} ,map : $maps ,");

    return _mapToPurchase(maps);
  }

  ///获取某段时间之前支付完成的数据
  Future<List<YBDDBInAppPurchaseEntity>?> inAppQueryPast(int userId, int createTime) async {
    YBDLogUtil.d("inAppQueryPast  userId: $userId createTime:$createTime");
    List<Map> maps;
    try {
      maps = await db!.query(inAppPurchaseTableName,
          columns: [
            cUserID,
            cOrderID,
            cPurchaseUserID,
            cSignature,
            cSignedData,
            cSPOrderID,
            cServiceState,
            cCreateTime,
            cChannel,
            cChannelState,
          ],
          where: '$cUserID=? and $cCreateTime<=? and $cServiceState = ? and $cChannelState = ?',
          whereArgs: [userId, createTime, STATE_VERIFY_SUCCESS, CHANNEL_STATE_CONSUME_SUCCESS]);
    } catch (e) {
      YBDLogUtil.e('inAppQueryPast query google order error, userid: $userId');
      return null;
    }

    YBDLogUtil.d("inAppQueryPast mapLength : ${maps.length} ,map : $maps ,");

    return _mapToPurchase(maps);
  }

  ///通过预订单号查询订单
  Future<List<YBDDBInAppPurchaseEntity>?> inAppQueryByOrderID(String? orderID, int? userId) async {
    YBDLogUtil.d("inAppQueryByOrderID  userId: $userId orderID:$orderID");
    List<Map> maps;
    try {
      maps = await db!.query(inAppPurchaseTableName,
          columns: [
            cOrderID,
            cPurchaseUserID,
            cSignature,
            cSignedData,
            cSPOrderID,
            cServiceState,
          ],
          where: '$cOrderID=? and $cUserID=?',
          whereArgs: [
            orderID,
            userId,
          ]);
    } catch (e) {
      YBDLogUtil.e('inAppQueryByOrderID error, userid: $userId orderID:$orderID e:$e');
      return null;
    }

    YBDLogUtil.d("inAppQueryByOrderID mapLength : ${maps.length} ,map : $maps ,");

    return _mapToPurchase(maps);
  }

  ///----------google充值记录本地操作------end ////
  ///
  ///
  ///----------google Order记录本地操作------start
  orderAdd(YBDDBOrderPurchaseEntity dbOrderPurchaseEntity) async {
    YBDLogUtil.d(
        "orderAdd orderID:${dbOrderPurchaseEntity.orderID} ,productID:${dbOrderPurchaseEntity.productID},createTime:${dbOrderPurchaseEntity.createTime}");
    if (dbOrderPurchaseEntity.orderID == null) {
      YBDLogUtil.d("orderAdd orderID is null!");
      return;
    }
    await orderDeleteByOrderId(dbOrderPurchaseEntity.orderID!);
    var result;
    try {
      result = await db!.insert(
          orderPurchaseTableName,
          YBDDBOrderPurchaseEntity(
                  userId ?? -1,
                  dbOrderPurchaseEntity.orderID,
                  dbOrderPurchaseEntity.productID,
                  dbOrderPurchaseEntity.createTime,
                  null,
                  null,
                  null,
                  dbOrderPurchaseEntity.channel,
                  dbOrderPurchaseEntity.state)
              .toMap());
    } catch (e) {
      YBDLogUtil.e("insert orderAdd order: ${dbOrderPurchaseEntity.orderID} error");
    }
    YBDLogUtil.d("orderAdd orderID : ${dbOrderPurchaseEntity.orderID} result:$result");
    return result;
  }

  ///修改预订单状态
  Future<dynamic> inAppOrderUpdate(YBDDBOrderPurchaseEntity dbOrderPurchaseEntity) async {
    YBDLogUtil.d("inAppOrderUpdate  dbInAppPurchaseEntity:${dbOrderPurchaseEntity.toString()}");
    if (dbOrderPurchaseEntity.orderID == null) {
      YBDLogUtil.d("inAppOrderUpdate orderID is null!");
      return;
    }
    var result;
    try {
      List<YBDDBOrderPurchaseEntity>? message = await orderQueryByOderId(dbOrderPurchaseEntity.orderID ?? '');

      if (message == null) {
        await orderAdd(dbOrderPurchaseEntity);
        return;
      }

      var map = <String, dynamic>{
        orderState: dbOrderPurchaseEntity.state,
      };

      result = await db
          ?.update(orderPurchaseTableName, map, where: "$cOrderID = ? ", whereArgs: [dbOrderPurchaseEntity.orderID]);
    } catch (e) {
      YBDLogUtil.e("inAppOrderUpdate orderID: ${dbOrderPurchaseEntity.orderID} error");
    }
    YBDLogUtil.d("inAppOrderUpdate orderID : ${dbOrderPurchaseEntity.orderID} result:$result");
    return result;
  }
  void inAppOrderUpdateah746oyelive(YBDDBOrderPurchaseEntity dbOrderPurchaseEntity)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///预订单绑定第三方订单
  Future<dynamic> inAppBindingOrderId(YBDDBOrderPurchaseEntity dbOrderPurchaseEntity) async {
    YBDLogUtil.d("inAppOrderBind  dbInAppPurchaseEntity:${dbOrderPurchaseEntity.toString()}");
    if (dbOrderPurchaseEntity.orderID == null) {
      YBDLogUtil.d("inAppOrderBind orderID is null!");
      return;
    }
    var result;
    try {
      var map = <String, dynamic>{
        orderSPOrderID: dbOrderPurchaseEntity.spOrderID,
        orderState: '1',
      };

      result = await db
          ?.update(orderPurchaseTableName, map, where: "$cOrderID = ? ", whereArgs: [dbOrderPurchaseEntity.orderID]);
    } catch (e) {
      YBDLogUtil.e("inAppBindingOrderId orderID: ${dbOrderPurchaseEntity.orderID} error");
    }
    YBDLogUtil.d("inAppBindingOrderId orderID : ${dbOrderPurchaseEntity.orderID} result:$result");
    return result;
  }
  void inAppBindingOrderIdc6CbOoyelive(YBDDBOrderPurchaseEntity dbOrderPurchaseEntity)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///通过商品productID And userId 删除记录
  ///废弃inAppOrderUpdate
  orderDelete(String productID) async {
    YBDLogUtil.d("OrderDelete userid: $userId ,productID : $productID");
    var result;
    try {
      await checkUser();
      result = await db!.delete('$orderPurchaseTableName',
          where: "$orderProductID = ? and $orderUserID = ?", whereArgs: [productID, userId]);
    } catch (e, s) {
      YBDLogUtil.e('OrderDelete google productID: $productID from db error');
    }
    YBDLogUtil.d("OrderDelete productID : $productID result:$result");
    return result;
  }

  ///通过服务器生成的orderId And userId 删除记录
  orderDeleteByOrderId(String orderId) async {
    YBDLogUtil.d("orderDeleteByOrderId userid: $userId ,orderId : $orderId");
    var result;
    try {
      await checkUser();
      result = await db!.delete('$orderPurchaseTableName',
          where: "$orderOrderID = ? and $orderUserID = ?", whereArgs: [orderId, userId]);
    } catch (e, s) {
      YBDLogUtil.e('orderDeleteByOrderId google productID: $orderId from db error');
    }
    YBDLogUtil.d("orderDeleteByOrderId productID : $orderId result:$result");
    return result;
  }

  ///根据google商品ID获取记录
  Future<List<YBDDBOrderPurchaseEntity>?> orderQuery(String productID) async {
    YBDLogUtil.d("orderQuery  userid: $userId , productID: $productID db:$db");
    List<Map> maps;
    try {
      await checkUser();
      maps = await db!.query(
        orderPurchaseTableName,
        columns: [
          orderUserID,
          orderOrderID,
          orderProductID,
          orderCreateTime,
          orderSignedData,
          orderSignature,
          orderSPOrderID,
        ],
        where: '$orderProductID=? and $orderUserID = ?',
        whereArgs: [productID, userId],
      );
    } catch (e) {
      YBDLogUtil.e('query google order error, userid: $userId ,productID:$productID e:$e');
      return null;
    }

    YBDLogUtil.d("orderQuery mapLength : ${maps.length} ,map : $maps ,");
    if (maps == null || maps.length == 0) {
      return null;
    }

    List<YBDDBOrderPurchaseEntity> messages = [];
    for (int i = 0; i < maps.length; i++) {
      messages.add(YBDDBOrderPurchaseEntity.fromMap(maps[i] as Map<String, dynamic>));
    }
    YBDLogUtil.d("orderQuery entity : $messages");

    return messages;
  }

  ///根据orderID获取记录
  Future<List<YBDDBOrderPurchaseEntity>?>? orderQueryByOderId(String orderID) async {
    YBDLogUtil.d("orderQueryByOderId  userid: $userId , orderID: $orderID");
    List<Map>? maps;
    try {
      maps = await db?.query(
        orderPurchaseTableName,
        columns: [
          orderUserID,
          orderOrderID,
          orderProductID,
          orderCreateTime,
          orderSignedData,
          orderSignature,
          orderSPOrderID,
          orderChannel,
          orderState,
        ],
        where: '$orderOrderID=? and $orderUserID = ?',
        whereArgs: [orderID, userId],
      );
    } catch (e) {
      YBDLogUtil.e('query order error, userid: $userId ,orderOrderID:$orderOrderID');
      return null;
    }

    YBDLogUtil.d("orderQuery mapLength : ${maps?.length} ,map : $maps ,");
    if (maps == null || maps.length == 0) {
      return null;
    }

    List<YBDDBOrderPurchaseEntity> messages = [];
    for (int i = 0; i < maps.length; i++) {
      messages.add(YBDDBOrderPurchaseEntity.fromMap(maps[i] as Map<String, dynamic>));
    }
    YBDLogUtil.d("orderQuery entity : $messages");

    return messages;
  }

  ///获取预订单数据---测试使用
  Future<List<YBDDBOrderPurchaseEntity>?>? orderQueryAll() async {
    YBDLogUtil.d("orderQueryAll  userid: $userId ");
    List<Map>? maps;
    try {
      maps = await db?.query(
        orderPurchaseTableName,
        columns: [
          orderUserID,
          orderOrderID,
          orderProductID,
          orderCreateTime,
          orderSignedData,
          orderSignature,
          orderSPOrderID,
          orderChannel,
          orderState,
        ],
        where: '$orderUserID = ?',
        whereArgs: [userId],
      );
    } catch (e) {
      YBDLogUtil.e('orderQueryAll order error, userid: $userId');
      return null;
    }

    YBDLogUtil.d("orderQueryAll mapLength : ${maps?.length} ,map : $maps ,");
    if (maps == null || maps.length == 0) {
      return null;
    }

    List<YBDDBOrderPurchaseEntity> messages = [];
    for (int i = 0; i < maps.length; i++) {
      messages.add(YBDDBOrderPurchaseEntity.fromMap(maps[i] as Map<String, dynamic>));
    }
    YBDLogUtil.d("orderQueryAll entity : $messages");

    return messages;
  }

  ///根据苹果商品ID获取预订单记录
  ///获取规则 ios渠道下该用户创建的最近未绑定过的ID
  Future<List<YBDDBOrderPurchaseEntity>?>? iosOrderQuery(String productID) async {
    YBDLogUtil.d("iosOrderQuery  userid: $userId , productID: $productID db:$db");
    List<Map>? maps;
    try {
      maps = await db?.query(
        orderPurchaseTableName,
        columns: [
          orderUserID,
          orderOrderID,
          orderProductID,
          orderCreateTime,
          orderSignedData,
          orderSignature,
          orderSPOrderID,
          orderChannel,
          orderState,
        ],
        orderBy: orderCreateTime,
        where: '$orderProductID=? and $orderUserID = ? and $orderChannel = ? and $orderState = ?',
        whereArgs: [productID, userId, '1', '0'],
      );
    } catch (e) {
      YBDLogUtil.e('query iosOrderQuery order error, userid: $userId ,productID:$productID e:$e');
      return null;
    }

    YBDLogUtil.d("iosOrderQuery mapLength : ${maps?.length} ,map : $maps ,");
    if (maps == null || maps.length == 0) {
      return null;
    }

    List<YBDDBOrderPurchaseEntity> messages = [];
    for (int i = 0; i < maps.length; i++) {
      messages.add(YBDDBOrderPurchaseEntity.fromMap(maps[i] as Map<String, dynamic>));
    }
    YBDLogUtil.d("iosOrderQuery entity : $messages");

    return messages;
  }

  ///根据spOrderID（苹果第三方id）获取orderID
  Future<String?> orderQueryBySpOrderID(String spOrderID) async {
    YBDLogUtil.d("orderQueryBySpOrderID  userId: $userId , spOrderID: $spOrderID");
    List<Map>? maps;
    try {
      maps = await db?.query(
        orderPurchaseTableName,
        columns: [
          orderUserID,
          orderOrderID,
          orderSPOrderID,
          orderChannel,
          orderState,
        ],
        where: '$orderSPOrderID=? and $orderUserID = ? and $orderChannel = ?',
        whereArgs: [spOrderID, userId, '1'],
      );
    } catch (e) {
      YBDLogUtil.e('orderQueryBySpOrderID error, userid: $userId ,orderOrderID:$orderOrderID');
      return null;
    }

    YBDLogUtil.d("orderQueryBySpOrderID mapLength : ${maps?.length} ,map : $maps ,");
    if (maps == null || maps.length == 0) {
      return null;
    }

    List<YBDDBOrderPurchaseEntity> messages = [];
    for (int i = 0; i < maps.length; i++) {
      messages.add(YBDDBOrderPurchaseEntity.fromMap(maps[i] as Map<String, dynamic>));
    }
    YBDLogUtil.d("orderQueryBySpOrderID entity : $messages");

    return messages[0].orderID;
  }

  ///补充预订单 信息（google返回的）
  orderUpdate(String orderID, String signedData, String signature, String spOrderID) async {
    // YBDLogUtil.d(
    //     "orderUpdate userid: $userId ,orderID : $orderID,signedData : $signedData,signature : $signature,spOrderID : $spOrderID");
    var result;
    try {
      await checkUser();
      result = await db!.rawUpdate(
          'UPDATE $orderPurchaseTableName SET $orderSignedData = ?, $orderSignature = ? , $orderSPOrderID = ? WHERE $orderOrderID = ? and $orderUserID = ?',
          ['$signedData', '$signature', '$spOrderID', '$orderID', '$userId']);
    } catch (e, s) {
      YBDLogUtil.e('orderUpdate google orderUserID: $userId ,orderID:$orderID');
    }
    YBDLogUtil.d(
        'orderUpdate google orderUserID: $userId ,orderID:$orderID ,result:$result ,signedData : $signedData,signature : $signature,spOrderID : $spOrderID"');
    return result;
  }

  ///----------google Order充值记录本地操作------end ////
  updateInAppPurchaseTable(Database db) async {
    /// google 表结构修改
    YBDLogUtil.d('test orderUpdate ------------------------------------');
    // await db.execute(sql_inApp_purchase_table_update_Test);
    String sqlBeGin = "BEGIN TRANSACTION;";
    String sqlCreateTemTable = sql_inApp_purchase_tem_table;
    String sqlIInsertToTem =
        "INSERT INTO $inAppPurchaseTemTableName (id,`$cUserID`,`$cOrderID`,`$cSignedData`,`$cSignature`,`$cSPOrderID`) SELECT * FROM $inAppPurchaseTableName;";
    String sqlDropTable = "DROP TABLE $inAppPurchaseTableName;";
    String sqlCreateTable = sql_inApp_purchase_table;
    String sqlTemToPurchaseTable = "INSERT INTO $inAppPurchaseTableName SELECT * FROM $inAppPurchaseTemTableName;";
    String sqlDropTemTable = "DROP TABLE $inAppPurchaseTemTableName;";
    String sqlcommit = "COMMIT;";
    Set<String> statements = {
      sqlBeGin,
      sqlCreateTemTable,
      sqlIInsertToTem,
      sqlDropTable,
      sqlCreateTable,
      sqlTemToPurchaseTable,
      sqlDropTemTable,
      sqlcommit
    };
// then
    for (String sql in statements) {
      YBDLogUtil.d('test orderUpdate --------------sql=$sql');
      await db.execute(sql);
    }
  }

/*
  test11() async {
    YBDLogUtil.d('test test11 --------------sql=$sql_order_purchase_table_add_state');
    YBDLogUtil.d('test test11 --------------sql=$sql_order_purchase_table_add_channel');
    await db.execute(sql_order_purchase_table_add_state);
    await db.execute(sql_order_purchase_table_add_channel);
  }
*/

  /// 测试表数据
/*  Future<List<YBDDBInAppPurchaseEntity>> dateBaseQuery(String tableName) async {
    YBDLogUtil.d("dateBaseQuery   ------------------------tableName：$tableName");
    List<Map> maps;
    try {
      maps = await db.query(tableName);
    } catch (e) {
      YBDLogUtil.e('dateBaseQuery error, userid: $userId');
      return null;
    }

    YBDLogUtil.d("dateBaseQuery mapLength : ${maps.length} ,map : $maps ,");

    return _mapToPurchase(maps);
  }*/
}
