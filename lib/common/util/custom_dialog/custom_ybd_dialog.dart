




/*
 * @Author: William-Zhou
 * @Date: 2022-01-10 18:06:25
 * @LastEditTime: 2022-01-10 18:10:05
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/set_ybd_ext.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';

enum DialogType {
  dialog,    //dialog
  loading, // Loading box
  toastCustom, //custom view
}

class YBDCustomDialog {
  static YBDCustomDialog? _instance;

  /// Helps remove the specified layer
  /// This feature may not work if you create a new child each time
  // List<Map<String, OverlayEntry>> _hashCodeList = [];

  /// the key map widget
  // List<Map<String, String>> _hashKeyList = [];

  /// Non-global popup
  List<Widget?> _nonGlobalPops = [];
  List<Widget?> get nonGlobalPops => _nonGlobalPops;

  /// loading url
  List<String> _loadUrls = [];
  List<String> get loadurls => _loadUrls;

  // List<Map<String, VoidCallback>> _callBackList = [];
  List<YBDDialogInfo> _dialogList = [];

  /// 同一个loading  是否合理？
  YBDLoadingCircle _loading = YBDLoadingCircle();
  YBDLoadingCircle get loading => _loading;

  Future _init() async {
    _instance = await YBDCustomDialog.getInstance();
  }
  void _initdHXwVoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  YBDCustomDialog._();

  static YBDCustomDialog? getInstance() {
    if (_instance == null) {
      var instance = YBDCustomDialog._();
      _instance = instance;
    }
    return _instance;
  }

  static BuildContext? dialogContext = Get.context;

  /// [widget]：custom widget
  /// [AlignmentGeometry]: control the location of the dialog
  /// [barrierDismissible]：true（the dialog will be closed after click background），false（not close）
  /// [maskColor]: the mask color
  /// [valueKey]: default null, if not null the dialog is only show it once at a time (note: use this function ,pls set the custom widget)
  /// [willPopScope]: default（true），true（the back event will close the dialog but not close the page
  /// [onDismiss]：the callback will be invoked when the dialog is closed
  /// [countDown]: Close the pop-up window regularly  Unit: milliseconds
  /// [global]：false = Close the pop-up window when the page pops
  /// [type]: dialog type
  /// [loadUrl]: loading box's url,Use with DialogType.loading.
  Future<void>? showDialog({
    required Widget? child,
    AlignmentGeometry alignmentTemp = Alignment.center,
    bool barrierDismissible = true,
    Color maskColor = Colors.black54,
    ValueKey? valueKey,
    bool willPopScope = true,
    VoidCallback? dismissCallBack,
    int countDown = -1,
    bool global = true,
    DialogType type = DialogType.loading,
    String? loadUrl,
    BuildContext? context,
  }) {
    logger.v('hash code: ${child.hashCode}');

    if (valueKey != null && _dialogList.any((e) => e.valueKey == valueKey.value.toString())) return null;

    OverlayEntry entry = OverlayEntry(
      builder: (context) {
        return Container(
          alignment: Alignment.center,
          // width: MediaQuery.of(context).size.width,
          child: AnimatedOpacity(
            opacity: 1,
            duration: Duration(milliseconds: 200),
            child: GestureDetector(
              onTap: barrierDismissible ? pop : null,
              child: Container(
                alignment: alignmentTemp,
                width: double.infinity,
                height: double.infinity,
                child: child,
              ),
            ),
          ),
        );
      },
    ); // navigatorKey.currentState.overlay.context
    Overlay.of(context ?? dialogContext!)!.insert(entry);
    String childHash = child.hashCode.toString();
    YBDDialogInfo info = YBDDialogInfo()
      ..dialogType = type
      ..childHashCode = childHash
      ..entry = entry
      ..child = child
      ..global = global;
    if (valueKey != null) info.valueKey = valueKey.value.toString();

    if (countDown > 0) {
      Future.delayed(Duration(milliseconds: countDown), () {
        logger.v('delayed:$countDown');
        if (_dialogList
                .firstWhere(
                  (e) => e.childHashCode == childHash,
                  orElse: () => YBDDialogInfo.emptyInfo(),
                )
                .childHashCode !=
            '-1') {
          pop(tagWidge: child);
        } else {
          logger.v('12.22---countDown no child');
        }
      });
    }
    if (!global) _nonGlobalPops.add(child);
    if (dismissCallBack != null) info.dismissCallBack = dismissCallBack;

    _dialogList.add(info);
    logger.v('12.2---_nonGlobalPops:$_nonGlobalPops');
    logger.v('12.2-------lenth:${_dialogList.length} ');
  }

  /// [tagWidge]: pop target widget
  static pop({Widget? tagWidge}) {
    try {
      if (tagWidge != null) {
        logger.v('12.2----tagWidget--close.............');
        YBDDialogInfo info = YBDCustomDialog.getInstance()!._dialogList.firstWhere((e) => e.child == tagWidge);
        logger.v('12.2------childHashCode:${info.childHashCode}');
        logger.v('12.2------ent:${info.entry}');
        closeDialog(info);
        return;
      }
      logger.v('12.2------close.............');
      closeDialog(YBDCustomDialog.getInstance()!._dialogList.last);
    } catch (e) {
      logger.v('dialog pop error: $e');
    }
  }

  static void closeDialog(YBDDialogInfo? info) {
    try {
      if (info != null) {
        info.entry?.remove();
        YBDCustomDialog.getInstance()!.tryCallWhenDismiss(info.childHashCode);
        YBDCustomDialog.getInstance()!._dialogList.remove(info);
      } else {
        logger.v('closeDialog YBDDialogInfo is null');
      }
    } catch (e) {
      logger.v('closeDialog error: $e');
    }
  }

  // Map getCallBack(String hash) {
  //   return YBDDialogUtil.getInstance()._callBackList.firstWhere((e) => e?.keys?.contains(hash), orElse: () => Map());
  // }

  YBDDialogInfo getDialogInfoByHash(String? hash) {
    return YBDCustomDialog.getInstance()!._dialogList.firstWhere((e) => e.childHashCode == hash);
  }

  void tryCallWhenDismiss(String? hash) {
    if (getDialogInfoByHash(hash) != null) {
      logger.v('12.3---tryDismissCallBack--hasCallBack--hash:$hash');
      VoidCallback? cb = getDialogInfoByHash(hash).dismissCallBack;
      cb?.call();
    } else {
      logger.v('12.3---tryDismissCallBack--hasnot--CallBack--hash:$hash');
    }
  }
  void tryCallWhenDismissk1IT7oyelive(String? hash) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 显示网络loading框时可以再展示其他弹窗吗？
  void closeLoading() {
    logger.v('12.6---closeLoading--start-length:${_loadUrls.length}***_loadUrls:$_loadUrls');
    if (_loadUrls.length == 1) {
      pop();
      _loadUrls.clear();
    } else if (_loadUrls.length > 1) {
      _loadUrls.removeAt(0);
    } else {}
  }
  void closeLoadingFKdKhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

Future<T>? showDialogs<T>({
  required Widget? child,
  AlignmentGeometry alignmentTemp = Alignment.center,
  bool barrierDismissible = true,
  Color maskColor = Colors.black54,
  ValueKey? valueKey,
  bool willPopScope = true,
  VoidCallback? dismissCallBack,
  int countDown = -1,
  bool global = true,
  DialogType type = DialogType.dialog,
  String? loadUrl,
  BuildContext? context,
}) {
  // assert(child != null);
  // if (type != DialogType.dialog) child = YBDDialogUtil.getInstance().loading;
  logger.v('12.6-----type:$type');
  if (type == DialogType.loading) {
    if (loadUrl == null || loadUrl.isEmpty) {
      logger.v('12.6-----null');
      return null;
    } else {
      logger.v('12.6-----loading');
      child = YBDCustomDialog.getInstance()!.loading;
      YBDCustomDialog.getInstance()!._loadUrls.add(loadUrl);
      logger.v('12.6-----add:${YBDCustomDialog.getInstance()!._loadUrls}');
    }
  }
  //To prevent the repeat toast
  if (YBDCustomDialog.getInstance()!._dialogList.any((e) => e.childHashCode == child.hashCode.toString())) return null;

  YBDCustomDialog.getInstance()!.showDialog(
    child: child,
    alignmentTemp: alignmentTemp,
    barrierDismissible: barrierDismissible,
    maskColor: maskColor,
    valueKey: valueKey,
    willPopScope: willPopScope,
    dismissCallBack: dismissCallBack,
    countDown: countDown,
    global: global,
    type: type,
    loadUrl: loadUrl,
    context: context,
  );
}

class YBDDialogInfo {
  DialogType? dialogType;
  BuildContext? context;
  String? childHashCode;
  OverlayEntry? entry;
  String? valueKey;
  Widget? child;
  bool? global;
  String? loadUrl;
  VoidCallback? dismissCallBack;

  static YBDDialogInfo emptyInfo() {
    return YBDDialogInfo()..childHashCode = '-1';
  }
}
