


import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

import '../../ui/page/home/entity/car_ybd_list_entity.dart';
import '../../ui/page/home/entity/gift_ybd_list_entity.dart';
import '../../ui/page/store/entity/downloadbean.dart';
import '../constant/const.dart';
import 'common_ybd_util.dart';
import 'log_ybd_util.dart';
import 'zip_ybd_util.dart';

/// 文件操作工具类
class YBDFileUtil {
  /// 保存 json 字符串到文件
  static Future<File?>? writeJsonToFile(String json, String fileName) async {
    if (null != json) {
      final file = await localFile(fileName);
      return file.writeAsString(json);
    } else {
      logger.v('json is null');
      return null;
    }
  }

  /// 从本地文件读取 json 字符串
  static Future<String?> readJsonFromFile(String fileName) async {
    try {
      final file = await localFile(fileName);
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      logger.v('read file error : $e');
      return null;
    }
  }

  /// 获取资源包中的图片路径
  static Future<String> copyFileFromAssets(String assets) async {
    Directory directory = await getApplicationDocumentsDirectory();
    var dbPath = join(directory.path, fileNameFromPath(assets));

    if (FileSystemEntity.typeSync(dbPath) == FileSystemEntityType.notFound) {
      ByteData data = await rootBundle.load(assets);
      List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(dbPath).writeAsBytes(bytes);
    }

    logger.v('file path from assets : $assets, path : $dbPath');
    return dbPath;
  }

  /// 从文件路径中获取文件名
  static String fileNameFromPath(String path) {
    assert((path).isNotEmpty, 'path is empty');
    final name = path.split(Platform.pathSeparator).last;
    logger.v('file name : $name from path : $path');
    return name;
  }

  /// app 外部部存储路径  android
  static Future<String> externalStoragePath() async {
    final directory = await getExternalStorageDirectory();
    return directory?.path ?? '';
  }

  /// app 外部存储文件全路径 android
  static Future<File> externalLocalFile(String fileName) async {
    final path = await externalStoragePath();
    logger.v('externalLocalFile $path');
    return File('$path${Platform.pathSeparator}$fileName');
  }

  /// 创建目录
  static createFolder() async {
    logger.v("createFolder");
    // TODO: jackie - 修改目录名称
    final folderName = "some_name";
    final path = Directory("storage/emulated/0/$folderName");
    if ((await path.exists())) {
      logger.v("exist");
    } else {
      logger.v("not exist");
      path.create();
    }
  }

  /// app 内部存储路径
  static Future<String> localPath() async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  /// app 内部存储文件全路径
  static Future<File> localFile(String fileName) async {
    final path = await localPath();
    return File('$path${Platform.pathSeparator}$fileName');
  }

  ///动画存储目录
  static Future<String> getAnimDir(String foldername) async {
//    String dir = Environment.getExternalStorageDirectory().getPath() + "/OyeVoice/Animation/";  //正式路径
    logger.v('getAnimDir 01 foldername: $foldername');
    final dir1 = await externalLocalFile('Animation/$foldername');
    final dir = await externalLocalFile('Animation');
    var path = new Directory(dir.path);
    if ((await path.exists())) {
      // TODO:
      logger.v("---exist");
      var path2 = new Directory(dir1.path);
      if ((await path2.exists())) {
        logger.v("path2---exist");
      } else {
        logger.v("path2---not exist");
        await path2.create();
      }
    } else {
      // TODO:
      logger.v("---not exist");
      await path.create();
    }
    logger.v('getAnimDir---: ${dir1.path}');
    return dir1.path;
  }

  static Future<String> animationDownloadedPath(animationConfig) async {
    String savePath = 'Animation/${animationConfig.foldername}/${animationConfig.version}';
    String path = await YBDCommonUtil.getResourceDir(savePath);
    return path;
  }

  /// 礼物动画的路径
  // static Future<String> giftSvgaDir({String folderName, String version,}) async {
  //   String savePath = 'Animation/$folderName/$version';
  //   String path = await YBDCommonUtil.getResourceDir(savePath);
  //   return path;
  // }

  static Future<List<dynamic>> getSvgPlayPath(animationConfig) async {
    logger.v('animationConfig: $animationConfig');
    List svgaPath = new List.filled(2, null);
    String path = await animationDownloadedPath(animationConfig);
    logger.v('animationDownloadedPath: $path');

    try {
      var directory = new Directory(path);
      List<FileSystemEntity> files = directory.listSync();
      logger.v('directory.listSync files: $files');

      for (var f in files) {
        var isSync = FileSystemEntity.isFileSync(f.path);
        logger.v("f.path ${f.path}  bool:$isSync");
        if (isSync) {
          if (f.path.endsWith('.svga')) {
            svgaPath[0] = f.path;
            logger.v("svga path: ${f.path}");
          } else if (f.path.endsWith('.mp3') || f.path.endsWith('.MP3')) {
            logger.v("mp3 path: ${f.path}");
            svgaPath[1] = f.path;
          } else if (f.path.endsWith('.mp4') || f.path.endsWith('.MP4')) {
            svgaPath[0] = f.path;
            logger.v("mp4 path: ${f.path}");
          }
        }
      }
    } catch (e) {
      logger.v("path not exist");
    }
    return svgaPath;

    /*path = path + '/'+animationConfig.foldername + '.svga';
    return path;*/
  }

  static Future<List<dynamic>> getSvgPath(YBDDownLoadBean downLoadBean) async {
    List svgaPath = new List.filled(2, null);
    try {
//      String directory = new Directory(downLoadBean.savePath+'/');
      String path = await YBDCommonUtil.getResourceDir(downLoadBean.savePath);
      logger.v('getSvgPath savePath:${downLoadBean.savePath} path:$path');
      var directory = new Directory(path);
      List<FileSystemEntity> files = directory.listSync();
      for (var f in files) {
        var bool = FileSystemEntity.isFileSync(f.path);
        logger.v("f.path ${f.path}  bool:$bool");
        if (bool) {
          logger.v(f.path);
          if (f.path.endsWith('.svga')) {
            svgaPath[0] = f.path;
            logger.v("svga path: ${f.path}");
          } else if (f.path.endsWith('.mp3') || f.path.endsWith('.MP3')) {
            logger.v("mp3 path: ${f.path}");
            svgaPath[1] = f.path;
          }
        }
      }
    } catch (e) {
      logger.v("directory not exist");
    }
    return svgaPath;
  }

  ///判断动画是否已经下载在本本地（foldername + version + 判断文件夹里面是否有文件）
  static Future<bool> isAnimationDownloaded(var animationConfig, {DownLoadType? downLoadType}) async {
    String path = await animationDownloadedPath(animationConfig);
    Directory dir = Directory(path);
    bool hasFile = true;
    if (!await dir.exists()) {
      hasFile = false;
    }

    int len = await dir.list().length;
    if (len <= 0) {
      hasFile = false;
    }

    if (len == 1) {
      String? zipPath;
      try {
        List<FileSystemEntity> files = dir.listSync();
        for (var f in files) {
          logger.v('isAnimationDownloaded f.path:' + f.path);
          if (f.path.endsWith('.zip')) {
            zipPath = f.path;
          }
        }
        logger.v("isAnimationDownloaded zipPath:$zipPath  path:$path");
        if (zipPath != null) {
          await YBDZipUtil.asyncUnZip(path, zipPath);
//            await Lock().synchronized(() async {
//              YBDZipUtil.asyncunZip(path, zipPath);
//            });
        }
      } catch (e) {
        logger.v("isAnimationDownloaded----------------$e");
      }
    }

    logger.v("isAnimationDownloaded hasFile:$hasFile  path:$path");
    return hasFile;
  }

  ///获取本地路径（svg_entrances）
  static Future<List<dynamic>?> getLocalUrl(YBDDownLoadBean downLoadBean, List<YBDCarListRecord?> carList) async {
    if (carList.length <= 0) {
      logger.v('getLocalUrl-----01');
      return null;
    }

    YBDCarListRecord? item;
    for (int i = 0; i < carList.length; i++) {
      item = carList[i];
      if (item!.animationInfo != null) {
        if (item.animationInfo!.name == downLoadBean.fileName) {
          YBDCarListRecordAnimationInfo carListRecordAnimationInfo = YBDCarListRecordAnimationInfo()
            ..version = item.animationInfo!.version
            ..foldername = item.animationInfo!.foldername
            ..name = item.animationInfo!.name
            ..animationId = item.animationInfo!.animationId;

          bool isSave = await YBDFileUtil.isAnimationDownloaded(carListRecordAnimationInfo,
              downLoadType: DownLoadType.svg_entrances);
          logger.v('getLocalUrl-----01 $isSave');
          if (isSave) {
            ///本地要是已经下载好  就直接播放
            List path = await YBDFileUtil.getSvgPlayPath(carListRecordAnimationInfo);
            return path;
          }
        }
      }
    }
    logger.v('getLocalUrl-----05');
    return null;
  }

  ///获取本地路径（YBDGift Animation）
  static Future<List<dynamic>?> getGiftLocalUrl(YBDDownLoadBean downLoadBean, List<YBDGiftListRecordGift?> giftList) async {
    if (giftList.length <= 0) {
      logger.v('getLocalUrl-----01');
      return null;
    }

    YBDGiftListRecordGift? item;
    for (int i = 0; i < giftList.length; i++) {
      item = giftList[i];
      if (item!.animationInfo != null) {
        if (item.animationInfo!.name == downLoadBean.fileName) {
          YBDCarListRecordAnimationInfo carListRecordAnimationInfo = YBDCarListRecordAnimationInfo()
            ..version = item.animationInfo!.version
            ..foldername = item.animationInfo!.foldername
            ..name = item.animationInfo!.name
            ..animationId = item.animationInfo!.animationId;

          bool isSave =
              await YBDFileUtil.isAnimationDownloaded(carListRecordAnimationInfo, downLoadType: DownLoadType.gift);
          logger.v('getGiftLocalUrl-----01 $isSave');
          if (isSave) {
            ///本地要是已经下载好  就直接播放
            List path = await YBDFileUtil.getSvgPlayPath(carListRecordAnimationInfo);
            return path;
          }
        }
      }
    }
    return null;
  }

  /* ///判断动画是否已经下载 边框（name + version + 判断文件夹里面是否有文件）
  static Future<bool> isFrameAnimationDownloaded(YBDCarListRecordAnimationInfo animationConfig) async {
    String path = await animationFameDownloadedPath(animationConfig);
    Directory dir = Directory(path);
    bool hasFile = true;
    if (!await dir.exists()) {
      hasFile = false;
    }
    int len = await dir.list().length;
    if (len <= 0) {
      hasFile = false;
    }
    logger.v("isAnimationDownloaded hasFile:$hasFile  path:${path}");
    return hasFile;
  }

  static Future<String> animationFameDownloadedPath(YBDCarListRecordAnimationInfo animationConfig) async {
    String savePath = 'Animation/${animationConfig.name}/${animationConfig.version}';
    String path = await YBDCommonUtil.getResourceDir(savePath);
    return path;
  }*/

  static clearTempDir() async {
    /// 清空 临时文件夹 压缩文件
    String tempPath = await YBDCommonUtil.getResourceDir(Const.TEMP);
    final tempDir = Directory(tempPath);
    tempDir.deleteSync(recursive: true);
  }


  //循环获取缓存大小
  static Future<int> getTotalSizeOfFilesInDir(final FileSystemEntity file) async {
    //  File
    if (file is File && file.existsSync()) {
      int length = await file.length();
      return length;
    }
    if (file is Directory && file.existsSync()) {
      List children = file.listSync();
      int total = 0;
      if (children.length > 0)
        for (final FileSystemEntity child in children as Iterable<FileSystemEntity>)
          total += await getTotalSizeOfFilesInDir(child);
      return total;
    }
    return 0;
  }

  // 获取图片缓存插件的缓存路径
  static Future<String> getCacheManagerPath() async {
    var directory = await getTemporaryDirectory();
    var path = directory.path +
        "${Platform.pathSeparator}${DefaultCacheManager.key}";
    return path;
  }

  // 清理图片缓存插件的缓存路径下的文件
  static clearCacheManagerPath() async {
    DefaultCacheManager manager = new DefaultCacheManager();
    await manager.emptyCache();
    Directory(await YBDFileUtil.getCacheManagerPath())
        .deleteSync(recursive: true);
  }

  /// 删除文件
  static Future<void> deleteFileWithPath(String? path) async {
    if (null == path || path.isEmpty) {
      logger.w('empty path : $path');
      return;
    }

    if (await File(path).exists()) {
      await File(path).delete();
    }
  }

  /// 移动文件
  /// [desPath] 目标文件路径
  /// 移动成功返回File对象，否则返回null
  static File? moveFileSync(File file, String desPath) {
    if (file == null || desPath == null || !file.existsSync() || desPath.isEmpty) {
      logger.w('invalid file or des path: ${file.path}, $desPath}');
      return null;
    }

    try {
      // 读写文件容易报异常
      List<int> data = file.readAsBytesSync();
      return File(desPath)
        ..createSync(recursive: true)
        ..writeAsBytes(data);
    } catch (e) {
      logger.e('move file error: $e');
    }

    return null;
  }
}
