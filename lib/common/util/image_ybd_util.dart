




import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux/redux.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_svga_theme.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import '../constant/const.dart';
import 'common_ybd_util.dart';
import 'log_ybd_util.dart';
import '../../redux/app_ybd_state.dart';
import 'dart:ui' as ui;

class YBDImageUtil {
  Widget showImageOrSvga(String path, {double? width, double? height, bool showHolder: true, bool fill: false}) {
    path = path.replaceAll("https", "http");

    return path.endsWith('svga')
        ? YBDRoomSvgaTheme(
            path,
            width: width,
            height: height,
            showLoading: showHolder,
            holder: showHolder
                ? Container(
                    width: width,
                    height: height,
                    decoration: BoxDecoration(
                        color: Color(0xff5B02AE).withOpacity(0.3),
                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(24)))),
                  )
                : null,
          )
        : YBDNetworkImage(
            imageUrl: path,
            width: width,
            height: height,
            fit: fill ? BoxFit.fill : BoxFit.fitWidth,
            placeholder: showHolder
                ? (x, d) {
                    return Container(
                      width: width,
                      height: height,
                      decoration: BoxDecoration(
                          color: Color(0xff5B02AE).withOpacity(0.3),
                          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(24)))),
                    );
                  }
                : null,
          );
  }
  void showImageOrSvga2FOBYoyelive(String path, {double? width, double? height, bool showHolder: true, bool fill: false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取图片域名地址
  /// http://d3py0gff3tm44e.cloudfront.net/
  static String? _getBasePath(BuildContext? context) {
    final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;

    // get image from cdn
    String? address = store.state.configs?.amazonS3Address2;
    String? bucket = store.state.configs?.amazonS3BucketName2;

    String basePath = address ?? '';
    if (null != bucket && bucket.isNotEmpty) {
      basePath += bucket + "/";
    }

    return basePath;
  }

  static String? roomImagePath;

  /// 获取图片域名+图片目录
  /// http://d3py0gff3tm44e.cloudfront.net/image/
  static String? _getImagePath(BuildContext? context) {
//    logger.v('_getImagePath roomImagePath:$roomImagePath context:$context');
    if (context == null) {
      return roomImagePath;
    }
    final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;

    String? imagePath = _getBasePath(context) ?? '';
    String? imageFolder = store.state.configs?.pathShareImage;
    if (null != imageFolder && imageFolder.isNotEmpty) {
      imagePath += imageFolder + "/";
    }

    return imagePath;
  }

  static void roomImageBasePath(BuildContext context) {
    roomImagePath = _getImagePath(context);
    logger.v('roomImageBasePath roomImagePath:$roomImagePath');
  }

  /// 获取图片域名+图片各级目录
  /// 非用户相关：http://d3py0gff3tm44e.cloudfront.net/image/{type}/
  /// 头像、海报，切割用户ID为三级目录：http://d3py0gff3tm44e.cloudfront.net/image/{type}/0000/0030/0011/
  static String _getPath(BuildContext? context, String type, int? userId) {
    if (null == _getImagePath(context)) {
      logger.d('_getImagePath null : $context');
      return '';
    }
    String path = _getImagePath(context)! + type + "/";
    if (YBDImageType.AVATAR == type || YBDImageType.COVER == type) {
      String tempId = "$userId";
      String temp = "000000000000";
      temp = temp.substring(0, 12 - tempId.length) + tempId;
      path += temp.substring(0, 4) + "/" + temp.substring(4, 8) + "/" + temp.substring(8, 12) + "/";
    }
    return path;
  }

  /// 获取默认图片
  static String getDefaultImg(BuildContext? context) {
    return '${_getImagePath(context)}default.jpg';
  }

  static Map<String, dynamic>? imageConfigBase;

  /// 获取图片不同场景的尺寸
  static int? _getSize(BuildContext? context, String? scene, String type) {
    Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    int? size = 0;
    dynamic sceneMap;

    if (null != scene && scene.isNotEmpty) {
      Map<String, dynamic>? imageConfig;
      if (context != null) {
        try {
          imageConfig = Map<String, dynamic>.from(store!.state.configs?.imageConfig);
        } catch (e) {
          logger.e('get imageConfig error : $e, imageConfig : ${store!.state.configs?.imageConfig}');
        }
      }
      if (imageConfigBase != null) {
        imageConfig = imageConfigBase;
      }

      if (null != imageConfig && imageConfig.isNotEmpty) {
        sceneMap = imageConfig[type];

        if (null != sceneMap && sceneMap.isNotEmpty && null != sceneMap[scene]) {
          size = sceneMap[scene];
        }
      } else {
        logger.v('image config is null');
      }
    }
    return size;
  }

  static void setImageConfig(BuildContext context) {
    final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    logger.v('imageConfigBase store: ${store.state.configs?.imageConfig}');

    try {
      imageConfigBase = Map<String, dynamic>.from(store.state.configs?.imageConfig);
    } catch (e) {
      logger.v('imageConfigBase errror: $e');
    }
  }

  /// 获取图片名
  static String getName(String img, int? size) {
    if (img.indexOf('.') == -1) {
      return img;
    }
    return img.substring(0, img.indexOf('.')) + '_$size' + img.substring(img.indexOf('.'));
  }

  static String getGifName(String img) {
    if (img.indexOf('.') == -1) {
      return img;
    }
    return img.substring(0, img.indexOf('.')) + img.substring(img.indexOf('.'));
  }

  /// 是否为图片全路径
  static bool isFullImagePath(String img) {
    // 如果字符串以 http 开头则为全路径
    return img.startsWith("http");
  }

  /// 给图片全路径添加 size
  /// [size] 图片大小
  /// [img] 以 http 开头的图片全路径
  /// 示例：
  /// 图片全路径为 http://xxx.xxx.com/img_name.png , size 等于 400
  /// 替换前：http://xxx.xxx.com/img_name.png
  /// 替换后：http://xxx.xxx.com/img_name_400.png
  static String imgUrlWithSize(String img, int? size) {
    var result = '';

    if (img.toLowerCase().endsWith('gif')) {
      // gif 图片不支持多尺寸
      result = img;
    } else if (img.contains("_0") && size != 0) {
      // 兼容老版本：用 [size] 替换 url 中的 ["_0"]
      result = img.replaceAll("_0.", "_$size.");
    } else if (size != 0) {
      // 新版本：向全路径中插入 [size]
      result = img.substring(0, img.lastIndexOf('\.')) + '_$size' + img.substring(img.lastIndexOf('\.'));
    } else {
      // [size] 为 0 返回图片全路径
      result = img;
    }

    return result;
  }

  /// 用户头像
  /// img: 后台返回图片名称
  /// scene: A - 80    B - 128    C - 256    不传时，显示原图
  static String avatar(BuildContext? context, String? img, int? userId, {String? scene}) {
    if (null != img && img.isNotEmpty) {
      int? size = _getSize(context, scene, YBDImageType.AVATAR);

      /// 如果后台返回http(s)图片地址
      if (isFullImagePath(img)) {
        return imgUrlWithSize(img, size);
      } else {
        return _getPath(context, YBDImageType.AVATAR, userId) + getName(img, size);
      }
    } else {
      return '';
    }
  }

  /// 主播海报
  /// img: 后台返回图片名称
  /// scene: A - 80    B - 128    C - 256    D - 640   不传时，显示原图
  static String cover(BuildContext? context, String? img, int? userId, {String? scene}) {
    if (null != img && img.isNotEmpty) {
      int? size = _getSize(context, scene, YBDImageType.COVER);

      /// 如果后台返回http(s)图片地址
      if (isFullImagePath(img)) {
        return imgUrlWithSize(img, size);
      } else {
        return _getPath(context, YBDImageType.COVER, userId) + getName(img, size);
      }
    } else {
      return '';
    }
  }

  /// 动态图片
  /// img: 后台返回图片路径
  /// scene: A - 列表图片 400px   B - 大图 720px     不传时，显示原图
  static String? status(BuildContext context, String? img, {String? scene}) {
    if (null != img && img.isNotEmpty) {
      if (img.toLowerCase().endsWith('gif')) {
        return img;
      }

      int? size = _getSize(context, scene, YBDImageType.STATUS);
      if (size != 0) {
        img = imgUrlWithSize(img, size);
      }
      return img;
    } else {
      return '';
    }
  }

  static String audioBg(BuildContext context, String img, {String? scene}) {
    if (null != img && img.isNotEmpty) {
      if (img.toLowerCase().endsWith('gif')) {
        return img;
      }

      // scene 为空直接返回 img
      if (null == scene) {
        return img;
      } else {
        // scene 不为空添加尺寸
        int? size = _getSize(context, scene, YBDImageType.AUDIO_BG);
        // TODO A场景配置为空时，尺寸取 400
        size = size == 0 && scene == 'A' ? 400 : size;

        if (size != 0) {
          img = imgUrlWithSize(img, size);
        }
        return img;
      }
    } else {
      return '';
    }
  }

  /// 广告图片
  static String ad(BuildContext? context, String? img, String scene) {
    String result;

    if (null == img || img.isEmpty) {
      logger.v('ad img url is empty use default img');
      result = getDefaultImg(context);
    } else {
      int? size = _getSize(context, scene, YBDImageType.AD);

      if (isFullImagePath(img)) {
        result = imgUrlWithSize(img, size);
      } else {
        result = '${_getPath(context, YBDImageType.AD, null)}${getName(img, size)}';
      }
    }

    return result;
  }

  /// 礼物图片
  /// "A": 48, "B": 128
  static String gift(BuildContext context, String? img, String scene) {
    String result;

    if (null == img || img.isEmpty) {
      // logger.v('gift img url is empty use default img');
      result = getDefaultImg(context);
    } else {
      int? size = _getSize(context, scene, YBDImageType.GIFT);

      if (isFullImagePath(img)) {
        result = imgUrlWithSize(img, size);
      } else {
        result = '${_getPath(context, YBDImageType.GIFT, null)}${getName(img, size)}';
      }
    }

    // logger.v("gift url: $result");
    return result;
  }

  /// 汽车图片
  static String car(BuildContext context, String? img, String scene) {
    String result;

    if (null == img || img.isEmpty) {
      // logger.v('gift img url is empty use default img');
      result = getDefaultImg(context);
    } else {
      int? size = _getSize(context, scene, YBDImageType.CAR);

      if (isFullImagePath(img)) {
        result = imgUrlWithSize(img, size);
      } else {
        result = '${_getPath(context, YBDImageType.CAR, null)}${getName(img, size)}';
      }
    }
    if (!result.startsWith('https')) {
      result = result.replaceFirst('http', 'https');
    }
    return result;
  }

  /// 配置活动图片
  static String configActivityBg(BuildContext context, String? img, String? scene) {
    String result;

    if (null == img || img.isEmpty) {
      logger.v('configActivityBg empty use default img');
      result = getDefaultImg(context);
    } else {
      int? size = _getSize(context, scene, YBDImageType.POP_UP);

      if (isFullImagePath(img)) {
        result = imgUrlWithSize(img, size);
      } else {
        result = '${_getPath(context, YBDImageType.POP_UP, null)}${getName(img, size)}';
      }
    }

    return result;
  }

  /// 主题缩略图
  static String themeTh(BuildContext context, String? img, String scene) {
    String result;
    if (null == img || img.isEmpty) {
      result = getDefaultImg(context);
    } else {
      int? size = _getSize(context, scene, YBDImageType.THEME_THUMBNAIL);

      if (isFullImagePath(img)) {
        result = imgUrlWithSize(img, size);
      } else {
        result = '${_getPath(context, YBDImageType.THEME_THUMBNAIL, null)}${getName(img, size)}';
      }
    }
    // print('themeTh url result: $result');
    return result;
  }

  /// 主题图 theme
  /// image:  A - 360 , B - 720
  /// thumbnail: A - 2OO
  static String theme(BuildContext context, String? img, String scene) {
    String result;
    if (null == img || img.isEmpty) {
      result = getDefaultImg(context);
    } else {
      int? size = _getSize(context, scene, YBDImageType.THEME);

      if (isFullImagePath(img)) {
        result = imgUrlWithSize(img, size);
      } else {
        result = '${_getPath(context, YBDImageType.THEME, null)}${getName(img, size)}';
      }
    }
    // print('theme url result: $result');
    return result;
  }

  /// gif && svga完整路径
  static String gif(BuildContext? context, String? img, String imageType) {
    if (null == img || img.isEmpty) {
      return getDefaultImg(context);
    } else {
      if (img.startsWith("http")) {
        return img;
      } else
        return '${_getPath(context, imageType, null)}${getGifName(img)}';
    }
  }

  /// Mig Frame图
  /// A: 100 ; B: 160
  static String frame(BuildContext? context, String? img, String scene) {
    String result;
    if (null == img || img.isEmpty) {
      result = getDefaultImg(context);
    } else {
      int? size = _getSize(context, scene, YBDImageType.FRAME);

      if (isFullImagePath(img)) {
        result = imgUrlWithSize(img, size);
      } else
        result = '${_getPath(context, YBDImageType.FRAME, null)}${getName(img, size)}';
    }

    print('frame url result: $result');
    return result;
  }

  static getFullSize(BuildContext? context, String? img) {
    String result;
    if (null == img || img.isEmpty) {
      result = getDefaultImg(context);
    } else {
      result = '${_getPath(context, YBDImageType.FRAME, null)}$img';
    }

    print('full url result: $result');
    return result;
  }

  ///获取动画下载路径
  static String animZipFile(BuildContext? context, String? stickName) {
    if (null == stickName || stickName.isEmpty) {
      return "";
    }
    // 如果stickName 带了后缀名就不拼接后缀名了。避免地址拼接出错
    if (stickName.toLowerCase().endsWith('.zip') ||
        stickName.toLowerCase().endsWith('.mp4') ||
        stickName.toLowerCase().endsWith('.svga')) {
      return _getBasePath(context)! + "anim/" + stickName;
    }
    return _getBasePath(context)! + "anim/" + stickName + ".zip";
  }

  ///获取动画下载路径
  static String emojieZipFile(BuildContext? context, String stickName) {
    if (null == stickName || stickName.isEmpty) {
      return "";
    }
    return Const.EMOJI_BASE_URL + stickName;
  }

/*  public static String emojieZipFile(Context context, String emojieName) {
    if (null == context || StringUtils.isBlank(emojieName)) {
      return "";
    }

    return Globals.EMOJI_BASE_URL + emojieName;
  }*/

  /// 系统头像库地址
  static String getAvatarPath(BuildContext? context) {
    return _getBasePath(context)! + 'oyetalk_frames/avatar/';
  }

  static String? getDiscountImgUrl(BuildContext context, int? type, String? thumbnail) {
    print('getDiscountImgUrl type:$type ,thumbnail:$thumbnail');
    String? url;
    if (type == null || thumbnail == null) {
      print('getDiscountImgUrl error type:$type ,thumbnail:$thumbnail');
      return null;
    }
    //商品类型,{entrance:2,gift:4,theme:12,frame:13}
    if (type == 2) {
      url = car(context, thumbnail, 'D');
    } else if (type == 4) {
      url = gift(context, thumbnail, 'B');
    } else if (type == 12) {
      url = themeTh(context, thumbnail, 'B');
    } else if (type == 13) {
      url = frame(context, thumbnail, "B");
    } else {
      print('getDiscountImgUrl error type:$type');
      url = null;
    }
    return url;
  }

  // https://www.jianshu.com/p/ffb4b58f0aa9/
  static Future<ui.Image> loadUIImageByProvider(
    ImageProvider provider, {
    ImageConfiguration config = ImageConfiguration.empty,
  }) async {
    Completer<ui.Image> completer = Completer<ui.Image>();
    late ImageStreamListener listener;
    ImageStream stream = provider.resolve(config);
    listener = ImageStreamListener((ImageInfo frame, bool sync) {
      final ui.Image image = frame.image;
      completer.complete(image);
      stream.removeListener(listener);
    });
    stream.addListener(listener);
    return completer.future;
  }

  // 裁剪出一个圆形的图片, 最大的中心区域
  static Future<ui.Image> clipCircularImage(ui.Image img) async {
    var oWidth = img.width.toDouble();
    var oHeight = img.height.toDouble();
    var width = min(oWidth, oHeight);
    var height = max(oWidth, oHeight);
    var start = (height - width) / 2.0;
    // 绘制区域
    var rr = RRect.fromLTRBR(0, 0, width, width, Radius.circular(width / 2));
    // 渲染区域
    var dst = Rect.fromLTRB(0, 0, width, width);
    // 原切割路径区域
    var src = Rect.fromLTRB(0, start, width, width);
    if (oWidth > oHeight) {
      src = Rect.fromLTRB(start, 0, width, width);
    }
    var p = Paint()..filterQuality = FilterQuality.low;

    final recorder = ui.PictureRecorder();
    final canvas = Canvas(recorder, Rect.fromPoints(Offset(0.0, 0.0), Offset(width, width)));
    canvas.clipPath(Path()..addRRect(rr));
    canvas.drawImageRect(img, src, dst, p);
    final picture = recorder.endRecording();
    return picture.toImage(width.toInt(), width.toInt());
  }
}

class YBDImageType {
  static const AVATAR = "7";
  static const COVER = "8";
  static const MIC_FRAME = "13";
  static const STATUS = "15";
  static const AUDIO_BG = "16";
  static const AD = "1";
  static const GIFT = "4";
  static const CAR = "2";
  static const THEME_THUMBNAIL = "11";
  static const THEME = "12";
  static const FRAME = "13";
  static const POP_UP = "18";
}
