


import 'dart:async';
import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';
import 'package:logger_flutter/logger_flutter.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/module/entity/firebase_ybd_log_entity.dart';

import '../../module/user/util/user_ybd_util.dart';
import '../constant/const.dart';
import 'date_ybd_util.dart';
import 'log_ybd_level.dart';
import 'remote_ybd_config_service.dart';
import 'share_ybd_util.dart';
import 'toast_ybd_util.dart';
import 'upload_ybd_s3_util.dart';

/// 打印日志的新方法
/// 用 [YBDTPLogLevel.output] 配置输出日志的级别
YBDMyLogger logger = YBDMyLogger();

/// 自定义的日志打印器
class YBDMyLogger {
  YBDMyLogger() {
    configLogger();
  }

  /// 日志打印器
  Logger? _logger;

  /// 日志中输出的版本号
  String? _buildCode;

  /// 初始化日志打印器和日志版本号
  Future<void> configLogger() async {
    if (null == _logger) {
      _logger = Logger(
        filter: YBDMyFilter(),
        printer: OneLinePrinter(),
        output: (await _multiOutput()),
      );

      // 输出版本号
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      _buildCode = packageInfo.buildNumber;

      // 配置输出日志的级别
      // 上线的时候可以注释这行默认不打印日志或者设置verbose以上级别的日志
      YBDTPLogLevel.output = Output.Verbose;

      // 生产环境要屏蔽 verbose 级别的日志
      // 注释上面一行或设置 LogLevel.output = Output.Nothing
      if (!Const.TEST_ENV && YBDTPLogLevel.output == Output.Verbose) {
        YBDToastUtil.toast('prod env should block verbose log');
      }
    }
  }
  void configLoggerLQavYoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 上传Firebase实时日志
  /// TODO: [module] 上传指定模块的日志
  /// 配置项：firebaseLog:{"isOpen":true, "userIds":["userId_1", "userId_2"]}
  void f(dynamic message) {
    YBDFirebaseLogEntity config = ConfigUtil.getFirebaseLogConfig()!;
    int? currentUser = YBDCommonUtil.storeFromContext()!.state.bean?.id;

    // 检查日志开关
    if (!config.isOpen!) {
      return;
    }

    // 指定用户上传日志
    if (null != config.userIds && config.userIds!.contains('$currentUser')) {
      _uploadFirebase(message);
    }
  }
  void fzxmWeoyelive(dynamic message) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 上传日志到firebase
  void _uploadFirebase(String message) {
    // 上传日志到firebase
    var methodInfo = OneLinePrinter().classAndMethod(StackTrace.current);
    YBDCommonUtil.storeUserLog('${DateTime.now()} $methodInfo |$_buildCode| $message');
  }
  void _uploadFirebasenoNIKoyelive(String message) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// [Level.verbose].
  void v(dynamic message) {
    // print('${DateTime.now()} $message');
    _logger?.v('|$_buildCode| $message');
  }

  /// [Level.verbose].
  /// 打印长文本日志
  void l(dynamic message) {
    try {
      // 屏蔽换行操作，在日志里显示换行符
      final longStr = message.replaceAll("\n", "\\n");
      final maxLength = 800;
      // 800 is the size of each chunk
      if (longStr is String && longStr.length > maxLength) {
        // print('=====fafasd 9000');
        final pattern = RegExp('.{1,$maxLength}');
        pattern.allMatches(longStr).forEach((match) {
          v(match.group(0));
        });
      } else {
        v(longStr);
      }
    } catch (e) {
      print('logger long string error : $e');
    }
  }
  void lDBMhQoyelive(dynamic message) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// [Level.debug].
  void d(dynamic message) {
    _logger?.d('|$_buildCode| $message');
  }

  /// [Level.info].
  void i(dynamic message) {
    _logger?.i('|$_buildCode| $message');
  }

  /// [Level.warning].
  void w(dynamic message) {
    _logger?.w('|$_buildCode| $message');
  }

  /// [Level.error].
  void e(dynamic message) {
    _logger?.e('|$_buildCode| $message');
  }
  void eryVk9oyelive(dynamic message) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 输出日志到多个平台
  Future<MultiOutput> _multiOutput() async {
    final List<LogOutput> value = [YBDMyLogOutput(), await _fileOutput()];
    return MultiOutput(value);
  }

  /// 输出日志到文件
  Future<FileOutput> _fileOutput() async {
    return FileOutput(file: await YBDLogUtil.logFile());
  }
}

/// 配置输出日志的级别
class YBDMyFilter extends LogFilter {
  @override
  bool shouldLog(LogEvent event) {
    // im消息开关
    // im消息开关：sender:firebase,status:open,level:verbose
    // if (im.openLog && im.logLevel > event.level) {
    //    _uploadFirebase(message);
    //    return;
    // }
    ///中文 过滤掉
    // if (!event.CN()) {
    //   print('cn is unuse：${event.message}');
    //   return false;
    // }

    // 根据日志级别输出日志
    if (YBDTPLogLevel.filterWithLevel(event.level)) {
      return true;
    }

    // 不输出日志
    return false;
  }
  void shouldLogK5qRRoyelive(LogEvent event) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

extension LFilter on LogEvent {
  bool CN() {
    if (this.message == null || this.message.runtimeType != ''.runtimeType) return true;
    return !RegExp("[\u4e00-\u9fa5]").hasMatch(this.message);
  }
}

class YBDMyLogOutput extends ConsoleOutput {
  @override
  void output(OutputEvent event) {
    super.output(event);
    // 设置摇一摇显示日志的最大条数
    LogConsole.add(event, bufferSize: 200);
  }
}

/// 打印日志的旧方法
/// logger 是打印日志的新方法
class YBDLogUtil {
  /// 监听手机晃动手势
  static ShakeDetector? _detector;
  static bool _open = false;

  /// 摇动手机弹出日志弹框
  static void popLogViewOnShake(BuildContext context) {
    // 生产环境屏蔽此功能
    if (!Const.TEST_ENV) {
      return;
    }

    // 重置手势监听器
    if (null != _detector) {
      _detector?.stopListening();
      _detector = null;
    }

    _detector = ShakeDetector(onPhoneShake: () async {
      if (_open) return;

      _open = true;
      await LogConsole.open(context, dark: true);
      _open = false;
    });

    _detector!.startListening();
  }

  /// 初始化日志打印器，清理过期的日志文件
  static void init({bool isDebug = false, String tag = ""}) {
    // 初始化日志打印器
    logger.i('init logger');

    // 清理超过一周的日志
    cleanLogDir();
  }

  @Deprecated(
    'Use logger.e instead.'
    'This method was deprecated',
  )
  static void e(Object object, {String? tag, String? className, String? methodName}) {
    logger.e(object);
  }

  @Deprecated(
    'Use logger.d instead.'
    'This method was deprecated',
  )
  static void d(Object object, {String? tag, String? className, String? methodName}) {
    logger.d(object);
  }

  @Deprecated(
    'Use logger.w instead.'
    'This method was deprecated',
  )
  static void w(Object object, {String? tag, String? className, String? methodName}) {
    logger.w(object);
  }

  @Deprecated(
    'Use logger.v instead.'
    'This method was deprecated',
  )
  static void v(Object object, {String? tag, String? className, String? methodName}) {
    logger.v(object);
  }

  /// 根据 Firebase 配置项上传日志到 s3
  static Future<void> uploadLogWithRemoteConfig(YBDRemoteConfigService remoteConfigService) async {
    String userIds = remoteConfigService.getConfig()!.getString(Const.UPLOAD_LOG_USER_ID);
    logger.v('remote config userId : $userIds');
    if (null != userIds && userIds.contains('${await YBDUserUtil.userId()}')) {
      // 上传指定用户 id 的日志
      uploadLogToS3();
    }
  }

  /// 保存日志的文件
  static Future<File> logFile() async {
    String logDir = await logDirectory();
    String separator = Platform.pathSeparator;
    String logFileName = YBDDateUtil.currentDate(yearFormat: 'yyyy-MM-dd-HH');
    String logPath = '$logDir$separator$logFileName.log';
    print('log path : $logPath');

    // 创建新的日志文件
    File logFile = await File(logPath).create(recursive: true);
    return logFile;
  }

  /// 保存日志的目录
  static Future<String> logDirectory() async {
    String baseDir = await _baseDirectory();
    print('base log directory : $baseDir');

    String separator = Platform.pathSeparator;
    String log = 'log';
    String logDir = '$baseDir$separator$log';
    print('log directory : $logDir');
    return logDir;
  }

  /// 声网日志路径
  static Future<String> agoraLogPath({bool isAgora = true}) async {
    String logDir = await logDirectory();
    print('log directory : $logDir');

    String separator = Platform.pathSeparator;
    String agoraFile = isAgora ? 'agorasdk.log' : 'zego.log';
    String path = '$logDir$separator$agoraFile';
    print('agora log path : $path');
    return path;
  }

  /// 日志目录
  static Future<String> _baseDirectory() async {
    String? baseDir;
    if (Platform.isIOS) {
      baseDir = (await getLibraryDirectory()).path;
    } else {
      baseDir = (await getExternalStorageDirectory())?.path;
    }

    return baseDir ?? '';
  }

  /// 日志目录的 zip 文件路径
  static Future<String> logZip() async {
    String baseDir = await _baseDirectory();
    print('base log directory : $baseDir');

    // 用户名作为日志 zip 文件名
    String? zip = await YBDUserUtil.userId();
    String separator = Platform.pathSeparator;
    String zipPath = '$baseDir$separator$zip.zip';
    print('zip log file path : $zipPath');
    return zipPath;
  }

  /// 导出 zip 日志文件
  static Future<File?> exportLogFile() async {
    // var encoder = ZipFileEncoder();

    // 生成的 zip 文件路径
    final zipPath = await logZip();

    // 所有日志文件所在的目录
    final logDir = await logDirectory();

    // encoder.zipDirectory(
    //   Directory(logDir),
    //   filename: zipPath,
    // );
    print('encode zip file compute start');
    await compute(_encodeZipFile, [zipPath, logDir]);
    print('encode zip file compute end');
    final zipFile = File(zipPath);

    if (await zipFile.exists()) {
      print('log zip file exist');
      return zipFile;
    } else {
      print('log zip file is null');
      return null;
    }
  }

  /// 创建压缩文件
  /// [params] 第一个元素为生成的 zip 文件路径，第二个元素要压缩的目录
  static _encodeZipFile(List<String> params) {
    assert(null != params && params.length == 2, 'params illegal');
    final zipPath = params[0];
    final logDir = params[1];
    var encoder = ZipFileEncoder();

    encoder.zipDirectory(
      Directory(logDir),
      filename: zipPath,
    );
  }

  /// 上传到 s3 服务器
  static Future<String> uploadLogToS3() async {
    final zipLog = await YBDLogUtil.exportLogFile();
    // 上传到 s3
    final result = await YBDUploadS3Util.uploadFile(zipLog, type: UploadS3Type.Log);
    logger.v('s3 result : $result');
    return '';
  }

  /// 分享日志
  static Future<void> shareLog() async {
    final File? zipLog = await YBDLogUtil.exportLogFile();
    logger.v('zip log : $zipLog');
    YBDShareUtil.shareFile(zipLog?.path ?? '', '');
  }

  /// 清理日志，保留最近一周的日志文件
  /// [maxDay] 保留日志的天数，默认保留最近 7 天的日志
  static Future<void> cleanLogDir({int maxDay = 7}) async {
    // 删除 zip 文件
    final zipFile = File(await logZip());

    if (await zipFile.exists()) {
      print('delete zip log file');
      zipFile.delete();
    }

    List<FileSystemEntity> fileList = Directory(await logDirectory()).listSync();
    fileList.forEach((element) async {
      if (element is File) {
        final time = await element.lastModified();
        if (DateTime.now().difference(time).inDays > maxDay) {
          print('===delete file  : ${element.path}');
          element.delete();
        }
      }
    });
  }
}
