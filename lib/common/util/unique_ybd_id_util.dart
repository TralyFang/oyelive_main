


import 'dart:async';
import 'dart:core';
import 'dart:io';
import 'dart:math';

import 'package:advertising_id/advertising_id.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:uniqueid/uniqueid.dart';
import '../constant/const.dart';
import 'common_ybd_util.dart';
import 'date_ybd_util.dart';
import 'log_ybd_util.dart';
import 'sp_ybd_util.dart';
import '../../module/api_ybd_helper.dart';
import '../../module/entity/device_ybd_unique_id.dart';
import '../../module/user/entity/user_ybd_info_entity.dart';

/**
 * 上报设备ID类型  激活 、新用户、登录
 */
enum UniqueIDType { ACTIVATION, NEW_USER, NORMAL_LOGIN }

Future<YBDDeviceUniqueID?> getDeviceUniqueIDs(BuildContext? context, var uniqueIDType, {int? timeout}) async {
  String? deviceUniqueID = await YBDSPUtil.get(Const.SP_DEVICE_UNIQUE_ID);
  if (deviceUniqueID == null || deviceUniqueID.isEmpty) {
    DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      print('IOSDevice:identifierForVendor：' + iosInfo.identifierForVendor);
      YBDSPUtil.save(Const.SP_DEVICE_UNIQUE_ID, iosInfo.identifierForVendor);
      return _sentDeviceUniqueID(context, uniqueIDType, iosInfo.identifierForVendor, timeout: timeout);
    } else if (Platform.isAndroid) {
      print('Android Device   001 ${YBDDateUtil.currentUtcTimestamp()}');
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      print("Device androidInfo androidId:" + androidInfo.androidId);
      String? advertisingId;
      try {
        advertisingId = await AdvertisingId.id(true);
      } on PlatformException {
        advertisingId = '';
      }
      String mediaDrmId = await Uniqueid.getAndroidMediaDrm;
      // 当  [mediaDrmId] 等于 null 时用加号组装字符串回报错
      // String androidUniqueID = androidInfo.androidId + "|" + advertisingId + "|" + mediaDrmId;
      String androidUniqueID = '${androidInfo.androidId}|$advertisingId|$mediaDrmId';
      // print(
      //     "Android Device 002 ${YBDDateUtil.currentUtcTimestamp()} Device androidInfo androidUniqueID:" + androidUniqueID);
      YBDSPUtil.save(Const.SP_DEVICE_UNIQUE_ID, androidUniqueID);
      return _sentDeviceUniqueID(context, uniqueIDType, androidUniqueID, timeout: timeout);
    } else {
      assert(false, 'unknown platform');
      return null;
    }
  } else {
    print('Android Device   00100 ${YBDDateUtil.currentUtcTimestamp()}');
    return _sentDeviceUniqueID(context, uniqueIDType, deviceUniqueID, timeout: timeout);
  }
}

getUniqueID(
  BuildContext? context,
  var uniqueIDType,
) async {
  String? deviceUniqueID = await YBDSPUtil.get(Const.SP_DEVICE_UNIQUE_ID);
  if (deviceUniqueID == null || deviceUniqueID.isEmpty) {
    DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      print('IOSDevice:identifierForVendor：' + iosInfo.identifierForVendor);
      YBDSPUtil.save(Const.SP_DEVICE_UNIQUE_ID, iosInfo.identifierForVendor);
      return iosInfo.identifierForVendor;
    } else if (Platform.isAndroid) {
      print('Android Device   001 ${YBDDateUtil.currentUtcTimestamp()}');
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      print("Device androidInfo androidId:" + androidInfo.androidId);
      String? advertisingId;
      try {
        advertisingId = await AdvertisingId.id(true);
      } on PlatformException {
        advertisingId = '';
      }
      String mediaDrmId = await Uniqueid.getAndroidMediaDrm;
      String androidUniqueID = '${androidInfo.androidId}|$advertisingId|$mediaDrmId';
      YBDSPUtil.save(Const.SP_DEVICE_UNIQUE_ID, androidUniqueID);
      return androidUniqueID;
      // print(
      //     "Android Device 002 ${YBDDateUtil.currentUtcTimestamp()} Device androidInfo androidUniqueID:" + androidUniqueID);

    } else {
      assert(false, 'unknown platform');
      return deviceUniqueID;
    }
  }

  return deviceUniqueID;
}

Future<YBDDeviceUniqueID?> _sentDeviceUniqueID(
  BuildContext? context,
  var uniqueIDType,
  String deviceUniqueID, {
  int? timeout,
}) async {
  print("_sentDeviceUniqueID deviceUniqueID:" + deviceUniqueID);
  Map<String, dynamic>? params;
  Map<String, dynamic> uniqueId;
  Map<String, dynamic> device;
  YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
  int? userId;
  String? selectCountry;
  String? country;
  int? loginType;
  if (userInfo != null) {
    userId = userInfo.id;
    country = userInfo.country;
    loginType = userInfo.loginType;

    String? countryLocal = await YBDSPUtil.get(Const.SP_SELECT_COUNTRY_CODE);
    if (selectCountry != countryLocal && countryLocal != null) {
      selectCountry = countryLocal;
    }
  }

  int? type;
  if (uniqueIDType == UniqueIDType.ACTIVATION) {
    type = 1;
  } else if (uniqueIDType == UniqueIDType.NEW_USER) {
    type = 2;
  } else if (uniqueIDType == UniqueIDType.NORMAL_LOGIN) {
    type = 3;
  }
  if (Platform.isIOS) {
    uniqueId = {"identifierForVendor": deviceUniqueID};
    device = {
      "system": "ios",
      "uniqueId": uniqueId,
    };

    params = {
      "userId": userId,
      "loginType": loginType,
      "country": country,
      "selectCountry": selectCountry,
      "type": type,
      "device": device,
    };
  } else if (Platform.isAndroid) {
    List<String>? ids = deviceUniqueID.split('|');
    if (ids != null && ids.length == 3) {
      uniqueId = {
        "androidId": ids[0],
        "advertisingId": ids[1],
        "mediaDrmUniqueId": ids[2],
      };

      device = {
        "system": "Android",
        "uniqueId": uniqueId,
      };

      params = {
        "userId": userId,
        "loginType": loginType,
        "country": country,
        "selectCountry": selectCountry,
        "type": type,
        "device": device,
      };
    }
  }
  print('Android Device   003 ${YBDDateUtil.currentUtcTimestamp()}');
  return await ApiHelper.sentDeviceUniqueID(context, params, timeout: timeout);
}

/// 初始时间戳到SP和文件，用来判断是否为新用户
Future<bool> isNewDevice() async {
  String timestamp = '${DateTime.now().millisecondsSinceEpoch}_${Random().nextInt(10000000)}';
  int initTime = DateTime.now().millisecondsSinceEpoch; // 初始时间

  /// 初始时间戳存入SP
  String? fromSP = await YBDSPUtil.get(Const.SP_INIT_TIMESTAMP);
  if (fromSP == null) {
    YBDSPUtil.save(Const.SP_INIT_TIMESTAMP, timestamp);
  } else {
    try {
      initTime = int.parse(fromSP.substring(0, fromSP.indexOf('_')));
    } catch (e) {
      YBDSPUtil.save(Const.SP_INIT_TIMESTAMP, timestamp);
    }
  }

  try {
    /// timestamp 文件
    String path = await YBDCommonUtil.getResourceDir('timestamp');
    Directory dir = Directory(path);
    if (!await dir.exists()) {
      dir.create(recursive: true);
    }

    bool timestampFileExists = false;
    await dir.list(recursive: false, followLinks: false).toList().then((value) {
      value.forEach((element) {
        if (element.path.contains('_')) {
          String temp = element.path.substring(element.path.lastIndexOf('/') + 1, element.path.lastIndexOf('_'));
          int i = int.parse(temp);
          if (i > 0) {
            timestampFileExists = true;
            initTime = initTime < i ? initTime : i; // 取文件和SP最小初始时间
          }
        }
      });
    });

    if (!timestampFileExists) {
      File tFile = File(path + Platform.pathSeparator + timestamp);
      tFile.create(recursive: true);
    }

    /// log file
    Directory logDir = Directory(await YBDCommonUtil.getResourceDir('log'));
    if (await logDir.exists()) {
      await logDir.list(recursive: false, followLinks: false).toList().then((value) {
        value.forEach((element) {
          if (element.path.contains('.')) {
            String logFileName =
                element.path.substring(element.path.lastIndexOf('/') + 1, element.path.lastIndexOf('.'));
            DateTime logTime = DateTime.parse(logFileName);
            if (logTime.millisecondsSinceEpoch < initTime) {
              initTime = logTime.millisecondsSinceEpoch;

              dir.deleteSync(recursive: true);
              File tFile = File(path + Platform.pathSeparator + '${initTime}_${Random().nextInt(10000000)}');
              tFile.create(recursive: true);
            }
          }
        });
      });
    }
  } catch (e) {}

  bool isNewDevice = YBDDateUtil.sameDateFromMilliseconds(initTime);
  logger.v('isNewDevice : $isNewDevice');
  return isNewDevice;
}
