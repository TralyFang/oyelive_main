



import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

class YBDIntlLangUtil{
/*
    "en": "English",
    "arabic": "العربية",
    "urdu": "اردو",
    "hindi": "हिंदी",
    "turkish": "Türkçe",
    "indonesia": "bahasa Indonesia"
* */
  static const String ar = 'ar';
  static const String ur = 'ur';
  static const String en = 'en';
  static const String id = 'id';
  static const String tr = 'tr';
  static const String hi = 'hi';

  ///类似阿拉伯一样 镜像的国家
  static List<String> internationLangLs = [YBDIntlLangUtil.ar,YBDIntlLangUtil.ur];

   static String getLocal(){
      return Get.locale!.languageCode;
   }

   static bool isArabic(){
     logger.v('lang is ${YBDIntlLangUtil.getLocal()} , ${YBDIntlLangUtil.internationLangLs.contains(YBDIntlLangUtil.getLocal())}');
     return YBDIntlLangUtil.internationLangLs.contains(YBDIntlLangUtil.getLocal());
   }

}