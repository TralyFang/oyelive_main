


import 'dart:convert';

import 'package:flutter/services.dart';
import '../constant/const.dart';
import 'log_ybd_util.dart';
import '../widget/local_ybd_animation_entity.dart';
import '../../generated/json/base/json_convert_content.dart';

///2.1需求 优化座驾辅助类 获取本地svga动画
class YBDLocalAnimationUtil {
  static List<YBDLocalAnimationItems>? _localAnimationItems;

  ///加载动画文件到内存
  // static Future<List<YBDLocalAnimationItems>> getLocalAnimationInfo() async {
  //   if (_localAnimationItems != null) {
  //     return _localAnimationItems;
  //   } else {
  //     String jsonStr = await rootBundle.loadString(
  //         Const.TEST_ENV ? "assets/animation/entrance_local_test.json" : "assets/animation/entrance_local.json");
  //     YBDLogUtil.d('getLocalAnimationInfo jsonStr:$jsonStr');
  //     Map mapData = json.decode(jsonStr);
  //     YBDLocalAnimationEntity data = JsonConvert.fromJsonAsT<YBDLocalAnimationEntity>(mapData);
  //     return data.items;
  //   }
  // }

  // ///判断是否是本地动画
  // static Future<bool> isLocalAnimation(int animationId) async {
  //   if (_localAnimationItems == null) {
  //     _localAnimationItems = await getLocalAnimationInfo();
  //   }
  //
  //   for (int i = 0; i < _localAnimationItems.length; i++) {
  //     if (animationId == _localAnimationItems[i].animationId) {
  //       return true;
  //     }
  //   }
  //   return false;
  // }

  ///通过animationId获取本地动画，没有返回空
  static Future<YBDLocalAnimationItems?> getLocalAnimationById(int? animationId) async {
    // YBDLogUtil.d('getLocalAnimationById animationId:$animationId');
    // if (_localAnimationItems == null) {
    //   _localAnimationItems = await getLocalAnimationInfo();
    // }
    //
    // for (int i = 0; i < _localAnimationItems.length; i++) {
    //   if (animationId == _localAnimationItems[i].animationId) {
    //     YBDLogUtil.d('getLocalAnimationById animationId:$animationId svga:${_localAnimationItems[i].svga}');
    //     return _localAnimationItems[i];
    //   }
    // }
    // 去掉本地座驾文件
    return null;
  }
}
