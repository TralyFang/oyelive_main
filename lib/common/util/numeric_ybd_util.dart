



import 'dart:math';

import "package:intl/intl.dart";
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

import 'log_ybd_util.dart';

/// 处理数字的工具类
class YBDNumericUtil {
  /// 判断字符串能否转成数字
  static bool isNumeric(String? s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null || int.tryParse(s) != null;
  }

  /// 字符串转 int
  static int stringToInt(String? s) {
    int result = 0;

    if (isNumeric(s)) {
      // double 类型包含小数点
      if (s!.contains('.')) {
        result = doubleToInt(double.parse(s));
      } else {
        result = int.parse(s);
      }
    } else if (result is int) {
      // 是整型则返回原来的值
      return result;
    } else {
      // 不能转成整型的字符串默认返回 0
      return 0;
    }

    return result;
  }

  /// double, int, String 类型转 int
  static int dynamicToInt(dynamic s) {
    if (s is int) {
      return s;
    }

    if (s is double) {
      return doubleToInt(s);
    }

    if (s is String) {
      return stringToInt(s);
    }

    logger.w('$s can not convert to int');
    // 不能转成 int 返回默认值
    return 0;
  }

  /// 字符串转 double
  static double stringToDouble(String? s) {
    double result = 0.0;

    if (isNumeric(s)) {
      result = double.parse(s!);
    } else {
      logger.v('string is invalid : $s');
    }

    return result;
  }

  // bool isNumber(String item){
  //   return '0123456789'.split('').contains(item);
  // }

  // static int moneyWithString(String money) {
  //   if (null == money) {
  //     return 0;
  //   }
  //
  //   final  moneyStr = '$money';
  //
  //
  //   //
  //   // int i = 0;
  // }

  /// 数字千分位加逗号，  e.g. 12,120,000
  static String? format(int? n) {
    if (null == n) return null;
    var format = NumberFormat('#,##0');
    return format.format(n);
  }

  /// 数字千分位加逗号，  e.g. 12,120,000
  static String? formatWithDot(num? n) {
    if (null == n) return null;
    var format = NumberFormat('#,##0.00#');
    return format.format(n);
  }

  /// 1, >= 1m, 显示m；>= 1k, 显示1k，四舍五入保留一位小数点;
  //  2, 省略小数点后的 0;
  //  3, 小于 1k 显示原数字；
  //  4, 大于 1b 暂时不考虑;
  /// [numStr] 数字字符串
  static String? compactNumberWithFixedDigits(
    String? numStr, {
    int fixedDigits = 1,
    bool removeZero = true, // 去掉小数点后的0
    bool rounding = true, // 四舍五入
    bool goldType = false, // gold的显示规则 <1w 显示原数字
  }) {
    if (null == numStr) return "0";
    int? num;
    try {
      num = int.parse(numStr);
    } catch (e) {
      logger.e('string to int error : $e');
    }

    if (null == num) {
      logger.v('$numStr to int result is null');
      return "0";
    }

    String suffix = '';
    double shortNum = 0;

    if (goldType && num < 10000) {
      return format(num);
    }

    if (num >= 1000 && num < 1000000) {
      // >= 1k
      shortNum = num / 1000;
      suffix = 'K';
    } else if (num >= 1000000 && num < 1000000000) {
      // >= 1m
      shortNum = num / 1000000;
      suffix = 'M';
    } else {
      // 大于 1,000,000,000 或小于 1,000
      if (goldType) return '1000M';
      return NumberFormat.compact().format(num);
    }

    String fixedStr =
        rounding ? shortNum.toStringAsFixed(fixedDigits) : withoutRoundingToStringAsFixed(shortNum, fixedDigits);
    String removedZero = removeZero ? _removeDecimalLastZero(fixedStr) : fixedStr;
    if (goldType) {
      if (removedZero.endsWith('.00')) {
        removedZero = removedZero.substring(0, removedZero.length - 3);
      } else if (removedZero.endsWith('0')) {
        removedZero = removedZero.substring(0, removedZero.length - 1);
      }
    }
    return '$removedZero$suffix';
  }

  /// 移除小数点和后面的 0
  static String _removeDecimalLastZero(String decimalStr) {
    if (decimalStr.length > 2 && decimalStr.contains('.0')) {
      return decimalStr.substring(0, decimalStr.length - 2);
    } else {
      return decimalStr;
    }
  }

  /// double 转 int
  static int doubleToInt(ranks) {
    double multiplier = 1.0;
    return (multiplier * ranks).round();
  }

  /// 生成小于[range]的随机数
  static int randomInt(int range) {
    final random = Random();
    return random.nextInt(range);
  }

  /// 钻石转豆子
  static String gemsToBeans(String gems) {
    if (gems == '0' || gems.startsWith('0')) return '';
    if (isNumeric(gems) && int.parse(gems).isEven) return (int.parse(gems) ~/ YBDTPGlobal.gemToBeanRate).toString();
    return '';
  }

  /// 计算百分比钻石
  static int? getGemsPercentage(int? gems, double percent) {
    if (percent == 1.0 && gems! > 1) {
      logger.v('11.17-----gems:$gems,percent:$percent');
      if (gems.isEven) return gems;
      return gems - 1;
    }
    if (percent > 0 && percent < 1.0) {
      int floorGems = (gems! * percent).floor();
      if (floorGems.isEven) return floorGems;
      return floorGems + 1;
    }
    return 0;
  }

  /// 不四舍五入 且 保留小数点后 n 位
  static String withoutRoundingToStringAsFixed(double num, int fixedDigits) {
    String numStr = num.toString();
    // 需要补0的情况
    if ((numStr.length - numStr.lastIndexOf(".") - 1) < fixedDigits) {
      return num.toStringAsFixed(fixedDigits);
    } else {
      // 不需要补0直接截取
      return numStr.substring(0, numStr.lastIndexOf(".") + fixedDigits + 1);
    }
  }
}
