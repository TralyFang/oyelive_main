



import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

/*
{
  "af_status" : 'Non_organic',
  "media_source":"",
  "campaign":"",
}
{
  "af_status" : 'organic',
}
*/
const String Af_status = 'af_status'; //organic or Non_organic
const String Media_source = 'media_source';
const String Campaign = 'campaign';

const String Non_organic = 'Non-organic'; //organic

///新的数据格式
/*
  {status: success, payload: {redirect_response_data: null, adgroup_id: null, engmnt_source: null, retargeting_conversion_type: re-attribution, is_incentivized: false, orig_cost: 0.0, is_first_launch: true, af_click_lookback: 7d, af_cpi: null, iscache: true, click_time: 2022-08-25 06:36:18.343, af_android_url: https://www.pgyer.com/dDyK, is_branded_link: null, match_type: probabilistic, adset: null, af_channel: oyetalkchannel, campaign_id: null, shortlink: de09i0qf, install_time: 2022-08-25 06:38:21.869, media_source: Social_facebook, agency: null, af_siteid: null, af_status: Non-organic, af_sub1: null, cost_cents_USD: 0, af_sub5: null, af_adset: adsetname, af_sub4: null, af_sub3: null, af_sub2: null, adset_id: null, esp_name: null, campaign: HDtest, http_referrer: android-app://com.google.android.googlequicksearchbox/, af_ad: adname, is_universal_link: null, is_retargeting: true, adgroup: null}}
* */
const String payload = 'payload';
const String click_time = 'click_time';
const String install_time = 'install_time';
const String af_ad = 'af_ad';

extension StrExt on String {
  String value(dynamic e) {
    if (e is! Map) return "type is not map";
    try {
      Map<String, dynamic> map = e as Map<String, dynamic>;
      if (this != Af_status) {
        var status = Af_status.value(e);
        return status == Non_organic ? (map.keys.contains(this) ? map[this].toString() : this + status) : this + status;
      }
      return map.keys.contains(this) ? map[this].toString() : this;
    } catch (error) {
      return 'data is unusual';
    }
  }
  void valueeZa7moyelive(dynamic e) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String afVal(dynamic e) {
    if (e is! Map) return "af type is not map";
    try {
      Map<String, dynamic> map = e as Map<String, dynamic>;
      if (!map.keys.contains(payload)) return 'payload undefine';
      Map<String, dynamic> payloadMap = e[payload];
      return payloadMap.keys.contains(this) ? map[this].toString() : this;
    } catch (error) {
      return 'af data is unusual';
    }
  }
  void afValQMYa8oyelive(dynamic e) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

///归因转化
void referrer() async {
  logger.v(' install referrer value');
  YBDAnalyticsUtil.appsflyerSdk!.onInstallConversionData((e) {
    logger.i(' install referrer value: $e');
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.REFERRER_INSTALL,
        location: Af_status.value(e), itemName: 'media_source:${Media_source.value(e)}', value: Campaign.value(e)));

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.REFERRER_INSTALL_APPSFLYER,
        location: Af_status.afVal(e) + Media_source.afVal(e) + Campaign.afVal(e),
        itemName: click_time.afVal(e) + af_ad.afVal(e),
        value: install_time.afVal(e)));
  });

  // YBDUserInfo us = await YBDCommonUtil.getUserInfo();
  // AndroidPlayInstallReferrer.installReferrer.then((value) {
  //   logger.v(
  //       ' install referrer value: ${value?.installReferrer}, ${value?.googlePlayInstantParam}, ${value?.installVersion}');
  //   YBDGameRoomConfig config = YBDGameCommomUtil.getGameConfig();
  //   try {
  //     if (config.userIds.contains(us.id.toString())) {
  //       YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.REFERRER_INSTALL,
  //           location: YBDLocationName.MAIN_PAGE,
  //           itemName: 'ref:${value?.installReferrer}/param:${value?.googlePlayInstantParam}',
  //           value: value?.toString()));
  //     }
  //   } catch (e) {
  //     logger.v('referrer: $e');
  //   }
  // });
}
