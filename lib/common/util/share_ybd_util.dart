
import 'dart:async';
import 'dart:io';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:flutter_social_content_share/flutter_social_content_share.dart';
import 'package:share/share.dart';
import 'package:social_share/social_share.dart';
import 'package:sprintf/sprintf.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';

import '../../module/entity/room_ybd_info_entity.dart';
import '../../module/status/entity/status_ybd_entity.dart';
import '../../module/user/entity/user_ybd_info_entity.dart';
import '../../ui/page/home/entity/banner_ybd_advertise_entity.dart';
import '../../ui/widget/loading_ybd_circle.dart';
import 'database_ybd_until.dart';
import 'image_ybd_util.dart';
import 'log_ybd_util.dart';
import 'remote_ybd_config_service.dart';
import 'sp_ybd_util.dart';
import 'toast_ybd_util.dart';

class YBDShareUtil {
  /// 成功
  static const SHARE_SUCCESS = 'success';

  /// 短信分享
  static Future<bool> shareSms(BuildContext context) async {
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.SHARE,
      method: 'sms',
      itemName: YBDItemName.BEFORE_SHARE,
    ));

    final content = await _msg();
    final urlContent = await _url(context, 'Contact');

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.SHARE,
      method: 'sms',
      itemName: YBDItemName.SHARING,
      value: maxParamValue('content: $content urlContent: $urlContent'),
    ));

    final tailingContent = '  #OyeTalk';
    String? result;

    if (Platform.isIOS) {
      // 弹出系统的分享弹框
      await Share.share('$content \n $urlContent \n $tailingContent');
      result = SHARE_SUCCESS;
    } else {
      result = await FlutterShareMe().shareSms(
        await _msg(),
        url: '  ' + await _url(context, 'Contact'),
        trailingText: '  #OyeTalk',
      );
    }

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.SHARE,
      method: 'sms',
      itemName: YBDItemName.SHARE_RESULT,
      value: result,
    ));

    logger.v('Share to contacts... result: $result');
    return true;
  }

  /// 社交软件分享
  static Future<bool> share(
    BuildContext context,
    String spf, {
    YBDStatusInfo? status,
    YBDBannerAdvertiseRecord? advertise,
    YBDRoomInfo? room,
  }) async {
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.SHARE,
      method: spf,
      itemName: YBDItemName.BEFORE_SHARE,
    ));

    String? result;

    // 分享文字和链接
    var msgContent;

    if ((advertise?.adurl ?? '').isNotEmpty) {
      logger.v('share advertise url : ${advertise?.adurl}');
      msgContent = spf == "WhatsApp" ? advertise!.adurl : (advertise!.adname! + " " + advertise.adurl!);
    } else if (status != null) {
      logger.v('share status : ${status.id}');
      msgContent = (await _msg(status: status))! + " " + await _url(context, spf, status: status);
    } else {
      logger.v('share room : ${room!.id}');
      msgContent = (await _msg(room: room))! + " " + await _url(context, spf, room: room);
    }

    // 分享链接
    var urlContent;

    if ((advertise?.adurl ?? '').isNotEmpty) {
      urlContent = advertise!.adurl;
    } else if (status != null) {
      urlContent = await _url(context, spf, status: status);
    } else {
      urlContent = await _url(context, spf, room: room);
    }

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.SHARE,
      method: spf,
      itemName: YBDItemName.SHARING,
      value: maxParamValue('msgContent: $msgContent urlContent: $urlContent'),
    ));

    switch (spf) {
      case 'WhatsApp':
        logger.v('WhatsApp share: $msgContent');
        result = await _shareWithWhatsApp(spf, msgContent);
        break;
      case 'Facebook':
        result = await _shareWithFacebook(spf, urlContent);
        break;
      case 'Instagram':
        result = await _shareWithInstagram(spf, msgContent);
        break;
      case 'Messenger':
        result = await _shareWithMessenger(spf, msgContent);
        break;
      case 'Snapchat':
        result = await _shareWithSnapchat(spf, msgContent);
        break;
      case 'Twitter':
        result = await _shareWithTwitter(spf, msgContent);
        break;
      case 'Skype':
        result = await _shareWithSkype(spf, msgContent);
        break;
      case 'More':
        result = await _shareMore(msgContent);
        break;
      case 'Copy Link':
        result = await _copyLink(msgContent);
        YBDToastUtil.toast('Copied to Clipboard');
        break;
    }

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.SHARE,
      method: spf,
      itemName: YBDItemName.SHARE_RESULT,
      value: result,
    ));

    return result == SHARE_SUCCESS;
  }

  /// 通过 whatsapp 分享
  /// [spf] 分享方式
  /// [content] 分享内容
  static Future<String?> _shareWithWhatsApp(String spf, String? content) async {
    logger.v('share with whatsapp content : $content, spf : $spf');
    var result;
    if (Platform.isIOS) {
      result = await SocialShare.shareWhatsapp(content!);
      logger.v('share with whatsapp await result : $result');

      // 插件返回 sharing，手动设置 success
      result = SHARE_SUCCESS;

      // TODO: 测试代码
      // 分享到 whatsapp 报错：链接无法打开
      // result = await FlutterSocialContentShare.shareOnWhatsapp('000000', content);
    } else {
      if (await FlutterShareMe().isAppInstalled(app: spf)) {
        logger.v('share to WhatsApp');
        result = await FlutterShareMe().shareToWhatsApp(msg: content);
      }
    }

    logger.v('share with whatsapp result : $result');
    return result;
  }

  /// 通过 Facebook 分享
  /// [spf] 分享方式
  /// [content] 分享内容0
  static Future<String?> _shareWithFacebook(String spf, String? content) async {
    logger.v('share with Facebook content : $content, spf : $spf');
    var result;
    if (Platform.isIOS) {
      // 分享 story
      // result = await SocialShare.shareFacebookStory(
      //   await YBDFileUtil.copyFileFromAssets('assets/images/status/status_default_bg_0.webp'),
      //   '#ffffff',
      //   '#000000',
      //   'http://google.com',
      // );

      // 分享引言
      result = await FlutterSocialContentShare.share(
        type: ShareType.facebookWithoutImage,
        url: content,
      );
    } else {
      result = await FlutterShareMe().shareToFacebook(url: content);
    }

    logger.v('share with Facebook result : $result');
    return result;
  }

  /// 通过 Instagram 分享
  /// [spf] 分享方式
  /// [content] 分享内容
  static Future<String?> _shareWithInstagram(String spf, String? content, {String? urlContent}) async {
    logger.v('share with Instagram content : $content, spf : $spf');
    var result;
    if (Platform.isIOS) {
      // TODO: 测试代码
      // 分享时 app 闪退
      // result = await SocialShare.shareInstagramStory(
      //   await YBDFileUtil.copyFileFromAssets('assets/images/status/status_default_bg_0.webp'),
      //   attributionURL: content,
      // );
      // logger.v('share with Instagram await result : $result');
      // result = SHARE_SUCCESS;

      // 弹出系统的分享弹框
      // content 参数为图片时才显示 Instagram 分享方式
      // await Share.shareFiles([content]);
      // result = SHARE_SUCCESS;
    } else {
      if (await FlutterShareMe().isAppInstalled(app: spf)) {
        result = await FlutterShareMe().shareInstagram(msg: content);
      }
    }

    logger.v('share with Instagram result : $result');
    return result;
  }

  /// 通过 Twitter 分享
  /// [spf] 分享方式
  /// [content] 分享内容
  static Future<String?> _shareWithTwitter(String spf, String? content) async {
    logger.v('share with Twitter content : $content, spf : $spf');
    var result;
    if (Platform.isIOS) {
      // TODO: 测试代码 iOS 暂时不支持
      // 不能调起 twitter 分享弹框
      // result = await SocialShare.shareTwitter(
      //   'this is social share twitter example',
      //   hashtags: ['hello', 'world'],
      //   url: 'https://www.apple.com',
      // );
      // logger.v('share with Twitter await result : $result');

      // 弹出系统的分享弹框
      await Share.share(content!);
      result = SHARE_SUCCESS;
    } else {
      result = await FlutterShareMe().shareToTwitter(msg: content);
    }

    logger.v('share with Twitter result : $result');
    return result;
  }

  /// 通过 Messenger 分享
  /// [spf] 分享方式
  /// [content] 分享内容
  static Future<String?> _shareWithMessenger(String spf, String? content) async {
    logger.v('share with Messenger content : $content, spf : $spf');
    var result;
    if (Platform.isIOS) {
      // TODO: 测试代码 iOS 暂时不支持
      // 弹出系统的分享弹框
      await Share.share(content!);
      result = SHARE_SUCCESS;
    } else {
      if (await FlutterShareMe().isAppInstalled(app: spf)) {
        result = await FlutterShareMe().shareMessenger(msg: content);
      }
    }

    logger.v('share with Messenger result : $result');
    return result;
  }

  /// 通过 Snapchat 分享
  /// [spf] 分享方式
  /// [content] 分享内容
  static Future<String?> _shareWithSnapchat(String spf, String? content) async {
    logger.v('share with Snapchat content : $content, spf : $spf');
    var result;
    if (Platform.isIOS) {
      // TODO: 测试代码 iOS 暂时不支持
      // 弹出系统的分享弹框
      await Share.share(content!);
      result = SHARE_SUCCESS;
    } else {
      if (await FlutterShareMe().isAppInstalled(app: spf)) {
        result = await FlutterShareMe().shareSnapchat(msg: content);
      }
    }

    logger.v('share with Snapchat result : $result');
    return result;
  }

  /// 通过 Skype 分享
  /// [spf] 分享方式
  /// [content] 分享内容
  static Future<String?> _shareWithSkype(String spf, String? content) async {
    logger.v('share with Skype content : $content, spf : $spf');
    var result;
    if (Platform.isIOS) {
      // TODO: 测试代码 iOS 暂时不支持
      // 弹出系统的分享弹框
      await Share.share(content!);
      result = SHARE_SUCCESS;
    } else {
      if (await FlutterShareMe().isAppInstalled(app: spf)) {
        result = await FlutterShareMe().shareSkype(msg: content);
      }
    }

    logger.v('share with Skype result : $result');
    return result;
  }

  /// 更多分享方式
  /// [content] 分享内容
  static Future<String?> _shareMore(String? content) async {
    logger.v('share with content : $content');
    var result;
    if (Platform.isIOS) {
      // 弹出系统的分享弹框
      await Share.share(content!);
      result = SHARE_SUCCESS;
    } else {
      result = await FlutterShareMe().shareToSystem(msg: content);
    }

    logger.v('share result : $result');
    return result;
  }

  /// 分享文件
  /// [path] 本地文件路径
  static Future<void> shareFile(String path, String url) async {
    logger.v('share with content : $path, $url');
    // 弹出系统的分享弹框
    Share.shareFiles([path]);
  }

  /// 分享链接
  /// [url] 链接地址
  static Future<void> shareLink(String url) async {
    logger.v('share with link : $url');
    Share.share(url);
  }

  /// 拷贝链接
  /// [content] 分享内容
  static Future<String?> _copyLink(String? content) async {
    logger.v('copy link : $content');
    var result;
    if (Platform.isIOS) {
      // TODO: 测试代码
      // 拷贝链接到系统剪贴板
      // 弹出系统的分享弹框
      // await Share.share(content);
      // result = SHARE_SUCCESS;
      SocialShare.copyToClipboard(content);
    } else {
      result = await FlutterShareMe().copyToClipboard(msg: content);
    }

    logger.v('copy link : $result');
    return result;
  }

  static Future<String?> _msg({YBDStatusInfo? status, YBDBannerAdvertiseRecord? advertise, YBDRoomInfo? room}) async {
    YBDRemoteConfigService? r = await YBDRemoteConfigService.getInstance();
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();

    String? text;
    if (status != null) {
      logger.v('status not null');
      try {
        // 分享动态
        text = r!.getConfig()!.getString("share_text_status");
      } catch (e) {
        logger.v('get share string error: $e');
      }

      logger.v('status text : $text');
      if ((text ?? '').isEmpty || (status.nickName ?? '').isEmpty) {
        text = 'Share your wonderful life now! #OyeTalk';
      } else {
        text = sprintf(text!, [status.nickName]);
      }
    } else if (room != null) {
      try {
        // 分享房间
        text = r!.getConfig()!.getString("share_text_room");
      } catch (e) {
        logger.v('get share_text_room error: $e');
      }

      if ((text ?? '').isEmpty || (room.nickname ?? '').isEmpty) {
        text = 'So exciting! Join party now! #OyeTalk';
      } else {
        text = sprintf(text!, [room.nickname]);
      }
    } else {
      try {
        // 邀请
        text = r!.getConfig()!.getString("share_text_invite");
      } catch (e) {
        logger.v('get share_text_invite error: $e');
      }

      if ((text ?? '').isEmpty || (userInfo?.nickname ?? '').isEmpty) {
        text = 'Join OyeTalk - Let\'s have a party!';
      } else {
        text = sprintf(text!, [userInfo!.nickname]);
      }
    }

    logger.v('msg text: $text');
    return text;
  }

  static Future<String> _url(BuildContext context, String spf, {YBDStatusInfo? status, YBDRoomInfo? room}) async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
    YBDRemoteConfigService? r = await YBDRemoteConfigService.getInstance();
    String? prefix;
    try {
      prefix = r?.getConfig()?.getString("firebase_url_prefix");
    } catch (e) {
      logger.v('get firebase_url_prefix error: $e');
    }

    if ((prefix ?? '').isEmpty) {
      prefix = 'https://oyetalk.page.link';
    }

    /// #1 从数据库获取
    String? result = await YBDDataBaseUtil.instance!.queryDynamicLink(
        userInfo?.id ?? -1,
        spf,
        status != null
            ? 'status'
            : room != null
                ? 'room'
                : 'invite',
        prefix,
        sid: status?.id,
        rid: room?.id);
    if (result != null && result.isNotEmpty) {
      return result;
    }

    // show loading,  从firebase获取动态短链 过程显示loading
    showDialog<Null>(
        context: context, //BuildContext对象
        barrierDismissible: false,
        builder: (BuildContext context) {
          return YBDLoadingCircle();
        });

    /// #2 生成Firebase Dynamic Link
    String? description;
    String? text;
    try {
      description = status != null ? status.content!.text : r?.getConfig()?.getString("share_description");
      text = await _msg(status: status);
    } catch (e) {
      logger.v('get share_description error: $e');
    }
    logger.v(
        "Sharing remote configs, firebase_url_prefix: $prefix , share_description: $description , share_text_invite: $text");

    String? image = 'https://www.oyetalk.live/image/poster.jpg';
    if (status?.content?.images != null) {
      // 动态第一张图片
      List<String> imageList = status!.content!.images!.split(',');
      image = YBDImageUtil.status(context, imageList[0], scene: "B");
    } else if (room?.roomimg != null) {
      // 房间海报
      image = YBDImageUtil.cover(context, room?.roomimg, room?.id);
    } else if (userInfo?.headimg != null) {
      // 用户头像
      image = YBDImageUtil.avatar(context, userInfo?.headimg, userInfo?.id);
    }
    if ((description ?? '').isEmpty) {
      description = 'OyeTalk is the Live Group Voice Talking and Entertaining Community.';
    }

    String link = 'https://www.oyetalk.live/?spf=$spf';
    if (userInfo?.id != null) {
      link += '&inviter=${userInfo!.id}';
    }

    if (status != null) {
      link += '&sid=${status.id}';
    }

    if (room != null) {
      link += '&rid=${room.id}';
    }

    DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: prefix!,
      link: Uri.parse(link),
      androidParameters: AndroidParameters(
        packageName: 'com.oyetalk.tv',
        minimumVersion: 1,
      ),
      iosParameters: IOSParameters(
        bundleId: 'com.oyetalk.tv',
        minimumVersion: '1.0.0',
        appStoreId: '1524257092',
      ),
      navigationInfoParameters: NavigationInfoParameters(forcedRedirectEnabled: true),
      /*
      dynamicLinkParametersOptions:
          DynamicLinkParametersOptions(shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short),

       */
      socialMetaTagParameters:
          SocialMetaTagParameters(title: text, description: description, imageUrl: Uri.parse(image!)),
    );
/*
    final Uri dynamicUrl = await parameters.buildUrl();

 */
    final Uri dynamicUrl = await parameters.link;
    logger.v('generate dynamicUrl: $dynamicUrl');

    ShortDynamicLink? shortDynamicLink;
    try {
      shortDynamicLink = await FirebaseDynamicLinks.instance.buildShortLink(parameters).timeout(Duration(seconds: 10), onTimeout: () {
      /*
      shortDynamicLink = await parameters.buildShortLink().timeout(Duration(seconds: 10), onTimeout: () {

       */
        logger.v('generate shortUrl timeout ... ');
        /*
        ShortDynamicLink sdl;
        return sdl;

         */
        return ShortDynamicLink(shortUrl: Uri.parse(''), type: ShortDynamicLinkType.short);
      });
    } catch (e) {
      logger.v('generate shortUrl error : $e');
    }

    logger.v('generate shortUrl: ${shortDynamicLink?.shortUrl}');
    final Uri shortUrl = shortDynamicLink?.shortUrl ?? dynamicUrl;
    result = shortUrl.toString();

    /// #3 保存Firebase Short Dynamic Link 到数据库
    if (null != shortDynamicLink?.shortUrl) {
      await YBDDataBaseUtil.instance!.insertDynamicLink(
          userInfo?.id ?? -1,
          spf,
          status != null
              ? 'status'
              : room != null
                  ? 'room'
                  : 'invite',
          result,
          prefix,
          sid: status?.id,
          rid: room?.id);
    }

    // dismiss loading
    Navigator.pop(context);

    return result;
  }
}
