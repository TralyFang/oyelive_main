


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'log_ybd_util.dart';

class YBDToastUtil {
  static var toastQue = {};

  static toast(String? msg, {BuildContext? thecontext, int duration = 3, ToastGravity gravity: ToastGravity.CENTER}) {
    logger.v('===toast msg : $msg');
    if (null == msg || msg.isEmpty) {
      return;
    }

    if (toastQue.length > 10) toastQue.clear();
    if (toastQue[msg] == null) {
      toastQue.addAll({msg: DateTime.now().millisecondsSinceEpoch / 1000});
      Fluttertoast.showToast(
        msg: msg,
        // [duration]大于 4 秒延长 toast 时间
        toastLength: duration > 4 ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT,
        gravity: gravity,
        timeInSecForIosWeb: (duration) == 0 ? 1 : duration,
        backgroundColor: Colors.black.withOpacity(0.8),
        textColor: Colors.white,
        fontSize: ScreenUtil().setSp(22),
      );
    } else {
      int nowSecond = DateTime.now().millisecondsSinceEpoch ~/ 1000;
      if (nowSecond - toastQue[msg] > 2) {
        toastQue[msg] = nowSecond;
        Fluttertoast.showToast(
          msg: msg,
          toastLength: Toast.LENGTH_SHORT,
          gravity: gravity,
          timeInSecForIosWeb: (duration) == 0 ? 1 : duration,
          backgroundColor: Colors.black.withOpacity(0.8),
          textColor: Colors.white,
          fontSize: ScreenUtil().setSp(22),
        );
      }
    }

//    showToast(
//      msg,
//      context: thecontext,
//      duration: Duration(seconds: 2),
//      position: ToastPosition.bottom,
//      backgroundColor: Colors.black,
//      radius: 5,
//      textStyle: TextStyle(fontSize: 18.0,color: Colors.white),
//      textDirection: TextDirection.ltr
//    );
  }
}
