

import 'log_ybd_util.dart';

class YBDValidPhoneUtil {
  static String? removeWhiteSpace(str) {
    if (str.length <= 0) return '';
    return str.trim().replaceAll(RegExp(r'\s+'), '');
  }

  static String? removeSpecialChars(str) {
    return str.replaceAll(RegExp(r'[^a-zA-Z 0-9]+'), '');
  }

  /// 是否为纯数字
  static bool allEqual(str) {
    final RegExp re = RegExp(r'^(\d)\1+$');
    return re.hasMatch(str);
  }

  /// 验证手机号是否合法
  static bool validatorPhone(String? value) {
    if (null == value || value.isEmpty) {
      logger.v('value is empty');
      return false;
    } else {
      // TODO: 客户端暂时没做手机号检测
      return true;
    }

    var phone = removeSpecialChars(value)!;
    phone = removeWhiteSpace(phone)!;

    if (allEqual(phone)) return true;
    final RegExp re = RegExp(r'^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})-?(\d{4}))$');

    final bool matches = re.hasMatch(phone);
    return !matches;
  }
}
