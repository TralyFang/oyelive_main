import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart' as g;
import 'package:get/get.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/set_ybd_ext.dart';
import 'package:oyelive_main/common/util/custom_dialog/custom_ybd_dialog.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';
import 'package:oyelive_main/ui/widget/go_ybd_set_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../module/api_ybd_helper.dart';
import '../../module/status/entity/status_ybd_entity.dart';
import '../../ui/page/buy/buy_ybd_popwindow_page.dart';
import '../../ui/page/home/entity/car_ybd_list_entity.dart';
import '../../ui/page/home/widget/combo_ybd_description_dialog.dart';
import '../../ui/page/home/widget/cp/cp_ybd_description_dialog.dart';
import '../../ui/page/home/widget/device_ybd_blocked_dialog.dart';
import '../../ui/page/home/widget/purchase_ybd_dialog.dart';
import '../../ui/page/profile/my_profile/util/top_ybd_up_badges_alert_util.dart';
import '../../ui/page/room/bloc/room_ybd_bloc.dart';
import '../../ui/page/room/define/room_ybd_define.dart';
import '../../ui/page/room/mic/mic_ybd_bloc.dart';
import '../../ui/page/room/pk/pk_ybd_invite_dialog.dart';
import '../../ui/page/room/pk/pk_ybd_normal_dialog.dart';
import '../../ui/page/room/pk/pk_ybd_start_mic_notice.dart';
import '../../ui/page/room/service/live_ybd_service.dart';
import '../../ui/page/room/start_live/widget/complete_ybd_now_dialog.dart';
import '../../ui/page/room/util/tp_ybd_dialog_manager.dart';
import '../../ui/page/room/widget/been_ybd_muted_dialog.dart';
import '../../ui/page/room/widget/leave_ybd_confirm_dialog.dart';
import '../../ui/page/room/widget/mute_ybd_user_dialog.dart';
import '../../ui/page/room/widget/normal_ybd_dialog.dart';
import '../../ui/page/room/widget/set_ybd_cover_dialog.dart';
import '../../ui/page/room/widget/show_ybd_roomlevel_dialog.dart';
import '../../ui/page/room/widget/show_ybd_topup_dialog.dart';
import '../../ui/page/room/widget/talent_ybd_offline_dialog.dart';
import '../../ui/page/store/entity/theme_ybd_entity.dart';
import '../../ui/page/store/widget/svga_ybd_play.dart';
import '../../ui/widget/bottom_ybd_selector_sheet.dart';
import '../../ui/widget/loading_ybd_circle.dart';
import '../../ui/widget/loading_ybd_circle_text.dart';
import '../../ui/widget/profile_ybd_report_dialog.dart';
import '../../ui/widget/sharing_ybd_widget.dart';
import '../constant/const.dart';
import '../navigator/navigator_ybd_helper.dart';
import '../room_socket/message/common/internal.dart';
import '../service/iap_service/iap_ybd_service.dart';
import '../service/task_service/task_ybd_service.dart';
import 'date_ybd_util.dart';
import 'log_ybd_util.dart';
import 'sp_ybd_util.dart';
import 'toast_ybd_util.dart';

/// 对话框工具类
class YBDDialogUtil {
  /// 记录加载框的弹出与隐藏
  static YBDTPDialogManager _dialogManager = YBDTPDialogManager();

  /// 显示加载框
  static Future<dynamic> showLoading(BuildContext? context,
      {barrierDismissible = true}) async {
    if (!_dialogManager.isShowing(LOADING_DIALOG_ID)) {
      logger.v('loading dialog is not showing show dialog');
      _dialogManager.add(LOADING_DIALOG_ID);
      return showDialog(
        barrierDismissible: barrierDismissible,
        context: context!,
        builder: (BuildContext builder) {
          return YBDLoadingCircle();
        },
      ).then((value) {
        logger.v('loading dialog is popped remove dialog id');
        _dialogManager.remove(LOADING_DIALOG_ID);
      });
    }

    logger.v('loading dialog is showing do nothing');
  }

  /// 隐藏加载框
  static hideLoading(BuildContext? context) {
    if (_dialogManager.isShowing(LOADING_DIALOG_ID)) {
      _dialogManager.remove(LOADING_DIALOG_ID);
      logger.v('loading dialog is showing pop dialog');
      YBDNavigatorHelper.popPage(context!);
      return;
    }

    logger.v('loading dialog is not showing do nothing');
  }

  /// 显示支付加载框
  static Future<void> showPurchaseLoading(BuildContext context) async {
    logger.v('===purchase show dialog');
    Future.delayed(Duration(seconds: 45), () {
      hidePurchaseLoading(context, retry: true);
    });

    if (!YBDIapService.instance.dialogManger.isShowing(APPLE_PAY_DIALOG_ID)) {
      YBDIapService.instance.dialogManger.add(APPLE_PAY_DIALOG_ID);
      logger.v('===purchase dialog is showing');
      try {
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext builder) {
            return YBDLoadingCircleText('Submitting');
          },
        ).then((value) {
          YBDIapService.instance.dialogManger.remove(APPLE_PAY_DIALOG_ID);
        });
      } catch (e) {
        YBDIapService.instance.dialogManger.remove(APPLE_PAY_DIALOG_ID);
      }
    }
  }

  /// 隐藏支付加载框
  static Future<void> hidePurchaseLoading(BuildContext context,
      {bool retry = false}) async {
    logger.v('===purchase hide dialog');
    if (YBDIapService.instance.dialogManger.isShowing(APPLE_PAY_DIALOG_ID)) {
      logger.v('===purchase pop dialog');
      if (Navigator.canPop(context)) {
        logger.v('===purchase can pop dialog');
        Navigator.pop(context);
      }

      if (retry) {
        Future.delayed(Duration(seconds: 2), () {
          logger.v('===purchase retry hide dialog');
          // 再次调用，确保弹框已经隐藏
          hidePurchaseLoading(context);
        });
      }
    }
  }

  /// 显示 combo 描述弹框
  static showComboDescriptionDialog(BuildContext context,
      {barrierDismissible = true}) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (BuildContext builder) {
        return YBDComboDescriptionDialog();
      },
    );
  }

  /// 显示 cp 描述弹框
  static showCpDescriptionDialog(BuildContext context,
      {barrierDismissible = true}) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (BuildContext builder) {
        return YBDCpDescriptionDialog();
      },
    );
  }

  /// 显示商城座驾播放弹框
  static showPlayCarDialog(BuildContext context, YBDCarListRecord carInfo,
      {barrierDismissible = true}) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (BuildContext builder) {
        return YBDSvgaPlayerPage(carInfo: carInfo);
      },
    );
  }

  /// 购买主题预览弹框
  static showBuyPreviewDialog(
    BuildContext context,
    List<YBDThemeRecord?>? themeRecordList,
    int curIndex, {
    barrierDismissible = true,
  }) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (BuildContext builder) {
        return YBDBuyPopWindowPage(themeRecordList, curIndex);
      },
    );
  }

  /// 显示举报用户的弹框
  static showReportUserDialog(
    BuildContext context,
    int? userId,
    String? nickName,
  ) {
    logger.v('targetId: $userId, nickName: $nickName');
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return YBDProfileReportDialog((String reportTitle) async {
            logger.v('report dialog ok button callback : $reportTitle');
            // 隐藏举报弹框
            Navigator.pop(context);

            bool result = await ApiHelper.complain(
              context,
              'user',
              userId,
              '$reportTitle|$userId|$nickName',
            );

            if (result) {
              // TODO: 翻译
              YBDToastUtil.toast('Report successful, under review');
            } else {
              // TODO: 翻译
              YBDToastUtil.toast('The system is busy, please try again later');
            }

            logger.v('report user result : $result');
          }, () {
            logger.v('report dialog cancel button callback');
            Navigator.pop(context); // 隐藏举报弹框
          });
        });
  }

  /// 显示禁言弹框
  static showMuteUserDialog(
    BuildContext context, {
    ForbiddenUserType? type,
    VoidCallback? cancelCallback,
    ForbiddenUserCallback? okCallback,
  }) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return YBDMuteUserDialog(
          type: type,
          cancelCallback: () {
            Navigator.pop(context);
          },
          okCallback: (int period) {
            if (null != okCallback) {
              okCallback(period);
            }
            Navigator.pop(context);
          },
        );
      },
    );
  }

  /// 显示被禁言的弹框
  static showBeenMutedDialog(BuildContext context, YBDInternal internal) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return BlocProvider.value(
            value: BlocProvider.of<YBDRoomBloc>(context),
            child: YBDBeenMutedDialog(internal, Duration(seconds: 5)),
          );
        });
  }

  /// 显示房主离线弹框
  static showTalentOfflineDialog(BuildContext context) {
    ///不在语聊房 不能弹出离线框
    if (!YBDObsUtil.instance()
        .routeList
        .subContain(YBDNavigatorHelper.flutter_room_list_page)) return;
    showDialogs(
        child: YBDTalentOfflineDialog(closeCallback: () {
          YBDCustomDialog.pop();
        }),
        valueKey: ValueKey('YBDTalentOfflineDialog'));
    // if (!YBDLiveService.instance.dialogManager.isShowing(TALENT_OFFLINE_DIALOG_ID)) {
    //   YBDLiveService.instance.dialogManager.add(TALENT_OFFLINE_DIALOG_ID);
    //   try {
    //     showDialog(
    //         context: context,
    //         builder: (context) {
    //           return YBDTalentOfflineDialog(closeCallback: () {
    //             Navigator.pop(context);
    //           });
    //         }).then((value) {
    //       YBDLiveService.instance.dialogManager.remove(TALENT_OFFLINE_DIALOG_ID);
    //     });
    //   } catch (e) {
    //     YBDLiveService.instance.dialogManager.remove(TALENT_OFFLINE_DIALOG_ID);
    //   }
    // }
  }

  /// 普通弹框
  static showNormalDialog(BuildContext context,
      {NormalDialogType? type,
      VoidCallback? cancelCallback,
      VoidCallback? okCallback,
      String? content,
      String? title,
      String? ok,
      String? cancel,
      bool showBorder = true,
      double height = 0.0,
      barrierDismissible = true,
      Duration? autoConfirmDuration}) {
    return showDialog(
        barrierDismissible: barrierDismissible,
        context: context,
        builder: (context) {
          return YBDNormalDialog(
            type: type,
            cancelCallback: () {
              Navigator.pop(context);
            },
            okCallback: () {
              if (null != okCallback) {
                okCallback();
              }
            },
            autoConfirmDuration: autoConfirmDuration,
            content: content,
            title: title,
            ok: ok,
            cancel: cancel,
            showBorder: showBorder,
            height: height,
          );
        });
  }

  /// 显示被邀麦弹框
  static showMicInviteDialog(
    BuildContext? context, {
    required VoidCallback okCallback,
    required String content,
    required String ok,
    required String cancel,
  }) {
    logger.v(
        'YBDLiveService.instance.dialogManager.isShowing(MIC_INVITED_DIALOG_ID) 0001:${YBDLiveService.instance.dialogManager.isShowing(MIC_INVITED_DIALOG_ID)}');
    if (!YBDLiveService.instance.dialogManager
        .isShowing(MIC_INVITED_DIALOG_ID)) {
      YBDLiveService.instance.dialogManager.add(MIC_INVITED_DIALOG_ID);
      logger.v(
          'YBDLiveService.instance.dialogManager.isShowing(MIC_INVITED_DIALOG_ID)  0002:${YBDLiveService.instance.dialogManager.isShowing(MIC_INVITED_DIALOG_ID)}');
      try {
        showNormalDialog(
          context!,
          type: NormalDialogType.two,
          okCallback: () {
            Navigator.pop(context);
            okCallback();
          },
          cancelCallback: () {
            Navigator.pop(context);
          },
          content: content,
          ok: ok,
          cancel: cancel,
        ).then((value) {
          logger.v(
              'YBDLiveService.instance.dialogManager.isShowing(MIC_INVITED_DIALOG_ID) 0003: emove(MIC_INVITED_DIALOG_ID)');
          YBDLiveService.instance.dialogManager.remove(MIC_INVITED_DIALOG_ID);
        });
      } catch (e) {
        logger.e(
            'YBDLiveService.instance.dialogManager.isShowing(MIC_INVITED_DIALOG_ID) 0004: e$e');
        YBDLiveService.instance.dialogManager.remove(MIC_INVITED_DIALOG_ID);
      }
    }
  }

  /// 封设备弹出框
  static showDeviceBlock(BuildContext context, String? content,
      {barrierDismissible = true}) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext builder) {
        return YBDDeviceBlockDialog(content);
      },
    );
  }

  /// 显示重新登录弹框
  static showReloginDialog(BuildContext context) {
    if (!_dialogManager.isShowing(RE_LOGIN_DIALOG_ID)) {
      logger.v('Relogin dialog is not showing show dialog');
      _dialogManager.add(RE_LOGIN_DIALOG_ID);
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(translate('logout_error')),
            actions: [
              TextButton(
                child: Text(
                  translate('re_login'),
                  style: TextStyle(color: Color(0xff5E94E7)),
                ),
                onPressed: () {
                  _dialogManager.remove(RE_LOGIN_DIALOG_ID);
                  // 跳转登录页
                  Navigator.pop(context);
                },
              )
            ],
          );
        },
      );
    }
  }

  /// 显示切换 Hindi 语弹框
  /// return : 点 [Change to Hindi Now] 返回 true, 否则返回 null
  static Future<bool?> showChangeToHindiDialog(
    BuildContext context, {
    VoidCallback? changeToHindiCallback,
    VoidCallback? laterCallback,
  }) async {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text(translate('supports_hindi')),
          actions: [
            TextButton(
                child: Text(
                  'Later',
                  style: TextStyle(color: Color(0xff5E94E7)),
                ),
                onPressed: () {
                  if (null != laterCallback) {
                    laterCallback();
                  }
                  Navigator.of(context).pop(false);
                }),
            TextButton(
                child: Text(
                  'Change to Hindi Now',
                  style: TextStyle(color: Color(0xff5E94E7)),
                ),
                onPressed: () {
                  if (null != changeToHindiCallback) {
                    changeToHindiCallback();
                  }

                  Navigator.of(context).pop(true);
                }),
          ],
        );
      },
    );
  }

  /// 确认离开房间弹框
  static showLeaveRoomConfirmDialog(BuildContext context) {
    var mic = BlocProvider.of<YBDMicBloc>(context)
        .state
        .micBeanList
        ?.where((element) => element?.userId == YBDUserUtil.getUserIdSync)
        .toList();
    if (mic?.isEmpty ?? true) {
      print("not on mic clear");
      YBDRoomUtil.liveDurationInMillSecs = null;
    } else {
      print("is on mic ");
    }
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return BlocProvider.value(
            value: BlocProvider.of<YBDRoomBloc>(context),
            child: YBDLeaveConfirmDialog(),
          );
        });
  }

  /// pk中禁止离开房间的弹框
  static showPKNormalDialog(BuildContext context, PKNormalType type) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return YBDPKNormalNotice(type: type);
        });
  }

  /// pk开始后提醒在麦位上的用户的弹框
  static showPKStartMicNoticeDialog(BuildContext? context) {
    if (!YBDLiveService.instance.dialogManager.isShowing(PK_START_MIC_NOTICE) &&
        (g.Get.currentRoute
                .contains(YBDNavigatorHelper.flutter_room_list_page) ||
            g.Get.currentRoute
                .contains(YBDNavigatorHelper.flutter_room_page))) {
      YBDLiveService.instance.dialogManager.add(PK_START_MIC_NOTICE);
      try {
        showDialog(
                barrierDismissible: false,
                context: context!,
                builder: (context) {
                  return YBDPKStartMicNotice();
                })
            .then((value) => YBDLiveService.instance.dialogManager
                .remove(PK_START_MIC_NOTICE));
      } catch (v) {
        YBDLiveService.instance.dialogManager.remove(PK_START_MIC_NOTICE);
      }
    }
  }

  /// 显示pk邀请的弹框
  static showPKInviteDialog(
    BuildContext? context, {
    VoidCallback? okCallback,
    VoidCallback? closeCallback,
    String? bgImg,
    String? okImg,
    String? closeImg,
    bool? haveCountDown,
    int? countNum,
  }) {
    if (!YBDLiveService.instance.dialogManager.isShowing(PK_INVITE_DIALOG_ID)) {
      YBDLiveService.instance.dialogManager.add(PK_INVITE_DIALOG_ID);
      try {
        showDialog(
            barrierDismissible: false,
            context: context!,
            builder: (x) {
              return YBDPKInviteDialog(
                okCallback: okCallback,
                closeCallback: closeCallback,
                bgImg: bgImg,
                okImg: okImg,
                closeImg: closeImg,
                haveCountDown: haveCountDown,
                countNum: countNum,
                micBloc: context.read<YBDMicBloc>(),
              );
            }).then((value) {
          YBDLiveService.instance.dialogManager.remove(PK_INVITE_DIALOG_ID);
        });
      } catch (e) {
        YBDLiveService.instance.dialogManager.remove(PK_INVITE_DIALOG_ID);
      }
    }
  }

//PK_INVITE_DIALOG_ID
  /// 显示开始PK的动画
  // static showPKStartAnimationDialog(
  //   BuildContext context, {
  //   String homeImg,
  //   String homeName,
  //   String awayImg,
  //   String awayName,
  // }) {
  // if (!YBDLiveService.instance.dialogManager.isShowing(PK_START_ANIMATION)) {
  //   YBDLiveService.instance.dialogManager.add(PK_START_ANIMATION);
  //   // YBDLiveService.instance.dialogManager.hide(context, PK_START_MIC_NOTICE);
  //   try {
  //     showDialog(
  //         barrierDismissible: false,
  //         context: context,
  //         builder: (context) {
  //           return YBDPKStartAnimation(
  //             homeImg: homeImg,
  //             homeName: homeName,
  //             awayImg: awayImg,
  //             awayName: awayName,
  //           );
  //         }).then((value) {
  //       YBDLiveService.instance.dialogManager.remove(PK_START_ANIMATION);
  //     });
  //   } catch (e) {
  //     YBDLiveService.instance.dialogManager.remove(PK_START_ANIMATION);
  //   }
  // }
  // }

  /// 显示首充提示框
  static void showTopUpDialog(BuildContext context) {
    if (!YBDLiveService.instance.dialogManager
        .isShowing(ROOM_TOP_UP_DIALOG_ID)) {
      YBDLiveService.instance.dialogManager.add(ROOM_TOP_UP_DIALOG_ID);
      try {
        showDialog(
            context: context,
            builder: (context) {
              return YBDShowTopUpPage(
                okCallback: () {
                  Navigator.pop(context);
                  YBDNavigatorHelper.openTopUpPage(context);
                },
                closeCallback: () {
                  Navigator.pop(context);
                },
              );
            }).then((value) {
          YBDLiveService.instance.dialogManager.remove(ROOM_TOP_UP_DIALOG_ID);
        });
      } catch (e) {
        YBDLiveService.instance.dialogManager.remove(ROOM_TOP_UP_DIALOG_ID);
      }
    }
  }

  /// 显示上传海报对话框
  static showEditInfoAlertDialog(
    BuildContext context,
    String? message, {
    bool goSettings = false,
    bool showLater = false,
    VoidCallback? onPressedLater,
    VoidCallback? onPressConfirm,
  }) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return YBDSetCoverDialog(
          content: message,
          confirmBtn: translate('ok'),
          onConfirm: () {
            logger.v('edit info dialog clicked ok btn');
            Navigator.pop(context);
            if (goSettings) {
              // 跳转编辑页面
              YBDNavigatorHelper.navigateTo(
                  context, YBDNavigatorHelper.edit_profile);
            }

            if (null != onPressConfirm) {
              onPressConfirm();
            }
          },
          laterBtn: showLater ? translate('later') : '',
          onLater: () {
            Navigator.pop(context);
            // 跳过设置海报，开始开播
            onPressedLater?.call();
          },
        );
      },
    );
  }

  /// 显示完善签约主播信息对话框
  static showCompleteNowDialog(
    BuildContext context, {
    VoidCallback? onPressedLater,
    VoidCallback? onPressedComlete,
  }) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return YBDCompleteNowDialog(
            onLater: onPressedLater,
            onCompleteNow: onPressedComlete,
          );
        });
  }

  /// 版本更新弹框
  static showUpdateDialog(
    BuildContext context,
    String versionName,
    String description, {
    bool forceUpdate = false,
  }) {
    YBDSPUtil.save(Const.SP_SHOW_APP_UPDATE, YBDDateUtil.currentUtcTimestamp());
    showDialog(
      // 点空白处不关闭弹框
      barrierDismissible: false,
      context: context,
      builder: (BuildContext builder) {
        return WillPopScope(
            child: Material(
              type: MaterialType.transparency,
              child: Center(
                child: Container(
//                  height: ScreenUtil().setWidth(660),
                    width: ScreenUtil().setWidth(550),
                    color: Colors.transparent,
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: <Widget>[
                        Container(
//                              height: ScreenUtil().setWidth(520),
                            width: ScreenUtil().setWidth(546),
                            margin: EdgeInsets.symmetric(
                                vertical: ScreenUtil().setWidth(40)),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(
                                    ScreenUtil().setWidth(32))),
                            child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
//                                    height: ScreenUtil().setWidth(300),
                                    margin: EdgeInsets.only(
                                        top: ScreenUtil().setWidth(180)),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(44),
                                        vertical: ScreenUtil().setWidth(40)),
                                    child: SingleChildScrollView(
                                        child: Text(
                                            '${description.replaceAll(r'\n', '\n')}',
                                            style: TextStyle(
                                                fontSize:
                                                    ScreenUtil().setSp(26),
                                                color: Color(0xff333333)))),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        bottom: ScreenUtil().setWidth(30)),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          if (!forceUpdate) ...[
                                            Container(
                                              width: ScreenUtil().setWidth(200),
                                              height: ScreenUtil().setWidth(64),
                                              child: TextButton(
                                                  child: Text(
                                                      translate('later'),
                                                      style: TextStyle(
                                                          color:
                                                              Color(0xff484848),
                                                          fontWeight:
                                                              FontWeight.w400)),
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  }),
                                            )
                                          ],
                                          SizedBox(
                                            width: ScreenUtil().setWidth(10),
                                          ),
                                          Container(
                                            width: ScreenUtil().setWidth(200),
                                            height: ScreenUtil().setWidth(64),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(ScreenUtil()
                                                        .setWidth(40))),
                                                gradient: LinearGradient(
                                                    colors: [
                                                      Color(0xff47CDCC),
                                                      Color(0xff5E94E7),
                                                    ],
                                                    begin: Alignment.topCenter,
                                                    end: Alignment
                                                        .bottomCenter)),
                                            child: TextButton(
                                                child: Text(
                                                  translate('update_now'),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: ScreenUtil()
                                                          .setSp(20)),
                                                ),
                                                onPressed: () {
                                                  if (Platform.isIOS) {
                                                    launch(Const.APPLE_STORE);
                                                  } else {
                                                    launch(Const.GOOGLE_STORE);
                                                  }

                                                  // Navigator.of(context).pop();
                                                }),
                                          ),
                                        ]),
                                  )
                                ])),
                        Container(
                          width: ScreenUtil().setWidth(550),
                          height: ScreenUtil().setWidth(234),
                          color: Colors.transparent,
                          child: Stack(children: <Widget>[
                            Positioned(
                                top: 0,
                                child: Image.asset(
                                    'assets/images/new_version_top.webp',
                                    width: ScreenUtil().setWidth(550),
                                    height: ScreenUtil().setWidth(234))),
                            Positioned(
                                top: ScreenUtil().setWidth(136),
                                child: Container(
                                  width: ScreenUtil().setWidth(550),
                                  child: Center(
                                      child: Text('v$versionName',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize:
                                                  ScreenUtil().setSp(24)))),
                                ))
                          ]),
                        )
                      ],
                    )),
              ),
            ),
            onWillPop: () async {
              // 版本更新弹框忽略物理返回键
              return Future.value(false);
            });
      },
    ).then((value) {
      // 已完成版本弹框任务
      YBDTaskService.instance!.end(TASK_CHECK_UPGRADE);
    });
  }

  /// 显示 Alert
  static showAlertDialog(
    BuildContext context,
    String message,
  ) {
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text(message),
          actions: [
            TextButton(
              child: Text(
                translate('ok'),
                style: TextStyle(color: Color(0xff5E94E7)),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  /// 房间等级提示框
  static void showRoomLevelUpDialog(BuildContext? context, int? roomLevel,
      {bool barrierDismissible = true, bool isRoomLevel = false}) {
    YBDLogUtil.v(
        'userInfo.experience 0003: showRoomLevelUpDialog context:$context roomLevel:$roomLevel ROOM_LEVEL_UP_DIALOG_ID:$ROOM_LEVEL_UP_DIALOG_ID');
    if (!YBDLiveService.instance.dialogManager
        .isShowing(ROOM_LEVEL_UP_DIALOG_ID)) {
      YBDLiveService.instance.dialogManager.add(ROOM_LEVEL_UP_DIALOG_ID);
      YBDLogUtil.v(
          'userInfo.experience 0003: showRoomLevelUpDialog context:0002');
      try {
        showDialog(
            context: Get.context!,
            barrierDismissible: barrierDismissible,
            builder: (context) {
              return YBDShowRoomLevelUpPage(
                roomLevel,
                okCallback: () {
                  Navigator.pop(context);
                },
                isRoomLevel: isRoomLevel,
              );
            }).then((value) {
          YBDLiveService.instance.dialogManager.remove(ROOM_LEVEL_UP_DIALOG_ID);
        });
      } catch (e) {
        YBDLiveService.instance.dialogManager.remove(ROOM_LEVEL_UP_DIALOG_ID);
      }
    }
  }

  /// 底部弹出框--EP
  static showEPBottomSheet(
    BuildContext context,
    String optsOne,
    String optsTwo, {
    VoidCallback? itemOneCallback,
    VoidCallback? itemTwoCallback,
  }) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return YBDBottomSelectorSheet(
            // opts: [translate('easypaisa_mobile_account'), translate('easypaisa_shop')],
            opts: [optsOne, optsTwo],
            onSelectAt: (index) async {
              switch (index) {
                case 0:
                  if (null != itemOneCallback) {
                    itemOneCallback();
                  }
                  break;
                case 1:
                  if (null != itemTwoCallback) {
                    itemTwoCallback();
                  }
                  break;
              }
            },
          );
        });
  }

  /// 分享动态弹框
  static void showSharingWidget(
    BuildContext context,
    YBDStatusInfo? statusInfo,
    double radius,
  ) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(radius),
            topRight: Radius.circular(radius),
          ),
        ),
        builder: (BuildContext context) {
          return YBDSharingWidget(
            key: Key(statusInfo!.id!),
            status: statusInfo,
          );
        });
  }

  /// 分享link
  static void showLinkWidget(
      BuildContext context, String? link, String title, double radius,
      {bool shareInside: true}) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(radius),
            topRight: Radius.circular(radius),
          ),
        ),
        builder: (BuildContext context) {
          return YBDSharingWidget(
            adUrl: link,
            // title: title,
            adName: title,
            adImg: '',
            shareInside: shareInside,
          );
        });
  }

  /// 删除动态的确认弹框
  static void showConfirmDialog(
    BuildContext context, {
    required String content,
    VoidCallback? onCancel,
    VoidCallback? onConfirm,
  }) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text(
        translate('cancel'),
        style: TextStyle(color: Color(0xff5E94E7)),
      ),
      onPressed: onCancel,
    );

    Widget continueButton = TextButton(
      child: Text(
        translate('ok'),
        style: TextStyle(color: Color(0xff5E94E7)),
      ),
      onPressed: onConfirm,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text(
        content,
        style: TextStyle(color: Colors.black),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  /// google充值成功弹框
  static showGooglePurchaseDialog(BuildContext context, String coins) {
    YBDTopUpBadgesAlertUtil.changeGoogleAlertState(true);
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext builder) {
        return YBDPurchaseDialog(
          coins: coins,
        );
      },
    );
  }

  static showTransparent(context, page) {
    Navigator.of(context).push(PageRouteBuilder(
        opaque: false,
        pageBuilder: (BuildContext context, _, __) {
          return page;
        }));
  }

  /// ios 跳转设置页面获取麦克风权限
  static goSettingDialog(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return YBDGoSetNowDialog();
        });
  }
}
