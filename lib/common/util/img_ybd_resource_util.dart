


/*
 * @Author: William
 * @Date: 2022-06-09 14:43:09
 * @LastEditTime: 2022-06-10 14:45:31
 * @LastEditors: William
 * @Description: get or update all images in oyetalk-flutter project
 * @FilePath: /oyetalk-flutter/lib/common/util/img_resource_util.dart
 */

import 'package:flutter/material.dart';

class YBDImgDir {
  static const String defaultDir = 'assets/images/';
  static const String tpDir = 'assets/images/teen_patti/';
  static const String tpCardDir = 'assets/images/teen_patti/card/';
  static const String gameDir = 'assets/images/game/';
  static const String imgDefaultDir = 'assets/images/default/';
  static const String level = 'assets/images/level/';
}

extension ImgPathExt on String {
  String get webp {
    return YBDImgDir.defaultDir + this + '.webp';
  }

  String get png {
    return YBDImgDir.defaultDir + this + '.png';
  }

  String get lv {
    return YBDImgDir.level + this + '.png';
  }

  String get svga => this + '.svga';

  Image asset({double? width, double? height, BoxFit fit = BoxFit.cover}) {
    return Image.asset(this, width: width, height: height, fit: fit);
  }
}

class YBDImg {
  /// level images
  static const String level = 'level_';

  //************************************* home page ******************************************/
  static const String explore_tab_bg_img = 'explore_tab_bg_img';
}
