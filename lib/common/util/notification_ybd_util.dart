

import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:math' as Math;

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:redux/redux.dart';

import '../../module/api_ybd_helper.dart';
import '../../module/entity/query_ybd_followed_resp_entity.dart';
import '../../module/inbox/entity/message_ybd_list_entity.dart';
import '../../module/inbox/message_ybd_helper.dart';
import '../../module/user/util/user_ybd_util.dart';
import '../../redux/app_ybd_state.dart';
import '../../ui/page/room/room_ybd_helper.dart';
import '../analytics/analytics_ybd_util.dart';
import '../constant/const.dart';
import '../navigator/navigator_ybd_helper.dart';
import 'common_ybd_util.dart';
import 'config_ybd_util.dart';
import 'log_ybd_util.dart';
import 'module/module_ybd_center.dart';
import 'numeric_ybd_util.dart';
import 'sp_ybd_util.dart';

class YBDNotificationUtil {
  static bool isFirst = true;
  static FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  static FirebaseMessaging? _firebaseMessaging;

  static FirebaseMessaging get instance {
    if (_firebaseMessaging == null) {
      _firebaseMessaging = FirebaseMessaging.instance;
    }

    return _firebaseMessaging!;
  }

  static init(BuildContext context) async {
    String? token = await instance.getToken();

    log("$token istssssss ");
    bool? showVipTalent = await YBDSPUtil.getBool(Const.SP_NOTIFICATION_VIP_TALENT);
    bool? showFollowedTalent = await YBDSPUtil.getBool(Const.SP_NOTIFICATION_FOLLOWED_TALENT);
    bool? showSystem = await YBDSPUtil.getBool(Const.SP_NOTIFICATION_SYSTEM);
    logger.v(
        'notification settings: vip talent: $showVipTalent, followed talents: $showFollowedTalent, system: $showSystem');

    instance.setAutoInitEnabled(true);

    FirebaseMessaging.onMessageOpenedApp.listen((event) {
      logger.v("openedApp ${event.data}");
      if (event.data['body'] != null) {
        Map result = jsonDecode(event.data['body']);
        onOpenTeenPattiInvite(result['recallId']);
      }
    });
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      logger.v("initMessage: ${message?.data}");

      if (message?.data != null && message?.data['body'] != null) {
        Future.delayed(Duration(seconds: 1), () {
          Map result = jsonDecode(message?.data['body']);
          onOpenTeenPattiInvite(result['recallId']);
        });
      }

      return null;
    });
    FirebaseMessaging.onMessage.listen((event) async {
      onReceivedMessage(
        event.data,
        context,
        showVipTalent,
        showFollowedTalent,
        showSystem,
        notification: event.notification,
      );
    });

    instance.requestPermission();

    // instance.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
    //   logger.v("Settings registered: $settings");
    // });

    instance.onTokenRefresh.listen((event) {
      logger.v("Push Messaging onTokenRefresh, new token: $event");
      logger.v('==fcm token : $event');
      ApiHelper.uploadFcmToken(context);
      YBDAnalyticsUtil.appsflyerSdk?.updateServerUninstallToken(event);
    });

    // if (showVipTalent == null || showVipTalent) {
    //   /// vip talent
    //   subscribeVipTalent(context);
    // }
    // if (showFollowedTalent == null || showFollowedTalent) {
    //   /// followed talent
    //   subscribeFollowingTalent(context);
    // }
    // if (showSystem == null || showSystem) {
    //   /// system notification
    //   subscribeGlobal(context);
    // }

    subscribeNotify();

    var initializationSettingsAndroid = AndroidInitializationSettings('ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
      requestSoundPermission: false,
      requestBadgePermission: false,
      requestAlertPermission: false,
    );

    var initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: (String? payload) {
      onLocalNotificationClick(context, payload);
      // return Future.value(null);
    });

    flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails().then((value) {
      logger.v("onLocalClickxx ${value!.payload}");
      onLocalNotificationClick(context, value.payload);
    });
  }

  static Future<dynamic> onLocalNotificationClick(BuildContext context, String? payload) async {
    logger.v("ssxxss");
    if ((payload ?? '').isNotEmpty) {
      logger.v('Firebase messaging, onSelectNotification payload: $payload');
      Map<String, dynamic> message = jsonDecode(payload!);

      if (message.containsKey('userId') && message['userId'] != null) {
        logger.v("go to pm");
        YBDNavigatorHelper.navigateTo(
          context,
          YBDNavigatorHelper.inbox_private_message + "/${message['userId']}/",
        );

        return;
      }
      if (message.containsKey('talentId') && message['talentId'] != null) {
        YBDRoomHelper.enterRoom(context, YBDNumericUtil.stringToInt(message['talentId']), location: 'app_notification');
        return;
      }
      if (message.containsKey('tp_save') && jsonDecode(message['tp_save']).isNotEmpty) {
        Map result = jsonDecode(message['tp_save']);
        logger.v("1111$result  ${result['recallId']}");
        onOpenTeenPattiInvite(int.tryParse((result['recallId'].toString())));
        return;
      }
    }
    isFirst = false;
    return null;
  }

  static onReceivedMessage(
    Map<String, dynamic> message,
    context,
    showVipTalent,
    showFollowedTalent,
    showSystem, {
    RemoteNotification? notification,
  }) async {
    // type=4 显示老用户召回的通知
    logger.i('message data: $message');
    if (YBDNumericUtil.stringToInt('${message['type']}') == 4) {
      Map<String, dynamic> msgData = Map.from(message);
      msgData['title'] = notification!.title;
      msgData['body'] = notification.body;
      // 显示本地通知
      showNotificationLargeIcon(msgData);
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.OPEN_PAGE,
        itemName: YBDItemName.NOTIFICATION_RECALL_4,
      ));
    }

    // Solving Android uninstall push notification not being silent
    logger.v("the message is :\n$message");
    if (message['af-uinstall-tracking'] != null) {
      return;
    }

    if (message != null) {
      int type = int.parse(message['type']);

      /// type==4  TeenPatti save
      ///
      ///
      // if (type == 4) {
      //   createLocalNotification(DateTime.now().second, message['title'], message['message'], null,
      //       jsonEncode({'tp_save': message['body']}));
      //
      //   return;
      // }

      /// type==3  private message
      ///
      ///
      if (type == 3) {
        YBDMessageListDataPullMessage? data = YBDMessageListDataPullMessage().fromJson(json.decode(message['message']));

        if (YBDMessageHelper.READING_CONVERSATION == null ||
            YBDMessageHelper.READING_CONVERSATION?.senderID != data.sender!.id.toString()) {
//              if(data.content.comment.contains('share'))
          createLocalNotification(
              data.sender!.id! + data.sequenceId!,
              json.decode(message['sender'])['nickname'],
              data.content!.comment,
              json.decode(message['sender'])['avatar'],
              jsonEncode({"userId": json.decode(message['sender'])['id'], "talentId": message['talentId']}));
        }
      } else {
        /// system message , followed talent , vip talent
        /// 遍历topic 检查是否设置不接收 （Settings 页面设置）

        if (message['topics'] != null) {
          List<String>? topics = [];
          if (message['topics'] is List) {
            // ["topic_1","topic_2"]
            topics = message['topics'];
          } else if (message['topics'] is String) {
            String temp = message['topics'];
            if (temp.startsWith('[') && temp.endsWith(']')) {
              // ["topic_1","topic_2"]
              temp.substring(1, temp.length - 1).split(',').forEach((element) {
                topics!.add(element.trim());
              });
            } else {
              // topic_1
              topics.add(temp);
            }
          }

          bool showNotification = false;
          String vipTalentTopic = await ConfigUtil.pushChannelVipTalent(context) ?? 'vip-talent';
          topics!.forEach((element) async {
            if (showVipTalent == null || showVipTalent && vipTalentTopic == element) {
              /// vip talent
              showNotification = true;
            } else if (showFollowedTalent == null || showFollowedTalent && element.startsWith('talent_')) {
              /// followed talent
              showNotification = true;
            } else if (showSystem == null || showSystem) {
              /// system notification
              showNotification = true;
            }
          });

          if (showNotification) {
            createLocalNotification(Math.Random().nextInt(10000), message['title'] ?? 'OyeTalk', message['message'],
                null, jsonEncode(message));
          }
        }
      }
    } else if (message['notification'] != null) {
      // normal
      Math.Random random = new Math.Random();
      int randomNumber = random.nextInt(10000);
      createLocalNotification(
          randomNumber, message['notification']['title'], message['notification']['body'], null, jsonEncode(message));
    }
  }

  static onOpenTeenPattiInvite(int? id) async {
    logger.v("enter room with tppppait");
    YBDModuleCenter.instance().enterRoom(type: EnterRoomType.NeedBlc, id: id);
  }

  /// unsubscribe topics except: vip-talent ,  OyeTalk-global  ,  user_$userId
  static unsubscribeAll(BuildContext context) async {
    logger.v('unsubscribeAll...');

    await instance.deleteToken();

    bool? showVipTalent = await YBDSPUtil.getBool(Const.SP_NOTIFICATION_VIP_TALENT);
    bool? showSystem = await YBDSPUtil.getBool(Const.SP_NOTIFICATION_SYSTEM);
    if (showVipTalent == null || showVipTalent) {
      /// vip talent
      subscribeVipTalent(context);
    }
    if (showSystem == null || showSystem) {
      /// system notification
      subscribeGlobal(context);
    }
  }

  /// subscribe user_notify
  static subscribeNotify() async {
    logger.v('subscribe user_notify');
    instance.subscribeToTopic("user_notify");
  }

  /// subscribe topic: OyeTalk-global ,    user_$userId
  static subscribeGlobal(BuildContext context) async {
    logger.v('subscribeGlobal...');

    final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    instance.subscribeToTopic(store?.state.configs?.pushChannelAll ?? "OyeTalk-global");

    String? userId = await YBDUserUtil.userId();
    if (null != userId) instance.subscribeToTopic("user_$userId");
  }

  /// unsubscribe topic: OyeTalk-global ,    user_$userId
  static unsubscribeGlobal(BuildContext context) async {
    logger.v('unsubscribeGlobal...');

    final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    instance.unsubscribeFromTopic(store?.state.configs?.pushChannelAll ?? "OyeTalk-global");

    String? userId = await YBDUserUtil.userId();
    if (null != userId) instance.unsubscribeFromTopic("user_$userId");
  }

  /// subscribe topic: vip-talent
  static subscribeVipTalent(BuildContext context) async {
    logger.v('subscribeVipTalent...');
    final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    instance.subscribeToTopic(store?.state.configs?.pushChannelVipTalent ?? "vip-talent");
  }

  /// unsubscribe topic: vip-talent
  static unsubscribeVipTalent(BuildContext context) async {
    logger.v('unsubscribeVipTalent...');
    final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    instance.unsubscribeFromTopic(store?.state.configs?.pushChannelVipTalent ?? "vip-talent");
  }

  /// subscribe topic: talent_$userId
  static subscribeTalent(BuildContext? context, String? userId) async {
    logger.v('subscribeTalent...$userId');
    instance.subscribeToTopic("talent_$userId");
  }

  /// unsubscribe topic: talent_$userId
  static unsubscribeTalent(BuildContext context, String userId) async {
    logger.v('subscribeTalent...$userId');
    instance.unsubscribeFromTopic("talent_$userId");
  }

  /// subscribe topic: followed talents
  static subscribeFollowingTalent(BuildContext context) async {
    logger.v('subscribeFollowingTalent...');
    YBDQueryFollowedRespEntity? queryFollowedRespEntity =
        await ApiHelper.queryFollowed(context, -1, 0, 0, int.parse((await YBDUserUtil.userId()) ?? '-1'), 1);
    if (queryFollowedRespEntity != null && queryFollowedRespEntity.returnCode == Const.HTTP_SUCCESS) {
      if (queryFollowedRespEntity.record?.followed != null && queryFollowedRespEntity.record!.followed!.length != 0) {
        queryFollowedRespEntity.record!.followed!.forEach((element) {
          instance.subscribeToTopic("talent_$element");
        });
      }
    }
  }

  /// unsubscribe topic: followed talents
  static unsubscribeFollowingTalent(BuildContext context) async {
    logger.v('unsubscribeFollowingTalent...');
    // YBDQueryFollowedRespEntity queryFollowedRespEntity =
    // await ApiHelper.queryFollowed(context, -1, 0, 0, int.parse(await YBDUserUtil.userId()), 1);
    // if (queryFollowedRespEntity != null && queryFollowedRespEntity.returnCode == Const.HTTP_SUCCESS) {
    //   if (queryFollowedRespEntity?.record?.followed != null && queryFollowedRespEntity.record.followed.length != 0) {
    //     queryFollowedRespEntity.record.followed.forEach((element) {
    //       _firebaseMessaging.unsubscribeFromTopic("talent_$element");
    //     });
    //   }
    // }

    unsubscribeAll(context);
  }

  /// 显示带缩略图的本地通知
  static Future<void> showNotificationLargeIcon(Map<String, dynamic> msgData) async {
    logger.i('msg data: $msgData');
    if (null == msgData || msgData.isEmpty) {
      logger.i('no data');
      return;
    }

    String? largeIcon = msgData['image'];
    String? channelId = msgData['channelId'];
    String? channelName = msgData['channelName'];
    String? channelDecs = msgData['channelDecs'];
    String title = msgData['title'] ?? '';
    String body = msgData['body'] ?? '';
    int msgId = DateTime.now().second;

    if (null == largeIcon) {
      logger.i('no largeIcon');
      return;
    }

    if (null == channelId) {
      logger.i('no channelId');
      return;
    }

    if (null == channelName) {
      logger.i('no channelName');
      return;
    }

    // 下载缩略图
    File largeIconFile = await DefaultCacheManager().getSingleFile(largeIcon).catchError((e) {
      logger.e('download large icon error: $e');
    });

    // 可扩展文本
    final bigTextStyleInformation = BigTextStyleInformation(
      body,
    );

    // 设置安卓通知样式
    final androidPlatformChannelSpecifics = AndroidNotificationDetails(
      channelId,
      channelName,
      channelDescription: channelDecs,
      largeIcon: FilePathAndroidBitmap(largeIconFile.path),
      styleInformation: bigTextStyleInformation,
    );

    // 设置苹果通知样式
    final iOSPlatformChannelSpecifics = IOSNotificationDetails(
      attachments: <IOSNotificationAttachment>[IOSNotificationAttachment(largeIconFile.path)],
    );

    // 设置通知样式
    final platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics,
    );

    // 显示通知
    await flutterLocalNotificationsPlugin.show(
      msgId,
      title.isEmpty ? null : title,
      Platform.isAndroid ? null : body,
      platformChannelSpecifics,
    );
  }

  static createLocalNotification(int id, String? title, String? description, String? imagePath, String payload) async {
    late File file;
    if (imagePath != null) {
      file = await DefaultCacheManager().getSingleFile(imagePath);
    }

    logger.v("xaaaaxxx $id $title $description $imagePath ");

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', channelDescription: 'your channel description',
        importance: Importance.max,
        priority: Priority.high,
        ticker: 'ticker',
        largeIcon: imagePath != null ? FilePathAndroidBitmap(file.path) : null);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics, iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(id, title, description, platformChannelSpecifics, payload: payload);
  }
}
