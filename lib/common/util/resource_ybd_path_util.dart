



import 'package:flutter/material.dart';
import 'config_ybd_util.dart';

class YBDResourcePathUtil {
  static Future<String?> _getBasePath(BuildContext context) async {
    // get image from cdn
    String? address = await ConfigUtil.amazonS3Address2(context);
    String? bucket = await ConfigUtil.amazonS3BucketName2(context);

    String basePath = address ?? '';
    if (null != bucket && bucket.isNotEmpty) {
      basePath += bucket + "/";
    }

    return basePath;
  }

  static Future<String> getFullPath(BuildContext context, String? relevantPath) async {
    if (relevantPath == null) {
      return "";
    }
    if (relevantPath.contains("http")) {
      return relevantPath;
    } else {
      String result = (await _getBasePath(context))! + '/' + relevantPath.replaceAll("//", '/');
      return result;
    }
  }
  // 默认性别头像路径，默认男性
  static String defaultMaleAssets({bool male = true}) {
    if (male)
      return "assets/images/profile/avatar_male.webp";
    return "assets/images/profile/avatar_female.webp";
  }
  // 默认性别头像，默认男性
  static Image defaultMaleImage({bool male = true}) {
    return Image.asset(defaultMaleAssets(male: male));
  }
}
