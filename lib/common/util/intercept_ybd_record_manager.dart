




import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:webview_flutter/webview_flutter.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/web_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/page/game_room/widget/media_ybd_cache_widget.dart';

/**
 * 1。 拦截文件的加载 interceptItem [url, time, bytes]
 * 2。 加载webView地址涉及的所有拦截文件 YBDInterceptWebItem [url, [interceptItem], time, bytes]
 * 3。 工程涉及的所有拦截资源 [YBDInterceptWebItem]
 * */
///
///
///
String jsonMapToString(Map data) {
  var jsonString;
  try {
    jsonString = jsonEncode(data);
  } catch (e) {
    jsonString = e.toString();
  }
  // logger.v("YBDInterceptManager jsonMapToString: $jsonString");
  return jsonString;
}

class YBDInterceptItem {
  String? url;
  int? time; // 加载时长ms
  int? bytes; // 加载大小
  bool isCache = false; // 是否已经缓存了

  @override
  String toString() {
    return jsonMapToString(toJson());
  }
  void toStringqc32Toyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = url;
    data['time'] = time;
    data['bytes'] = bytes;
    data['isCache'] = isCache;
    return data;
  }
}

class YBDInterceptWebItem extends YBDInterceptItem {
  List<YBDInterceptItem>? items;
  int? remainTime; // 页面停留时长ms
  @override
  String toString() {
    return jsonMapToString(toJson());
  }
  void toStringXj0cYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Map<String, dynamic> toJson() {
    int? timeItems = 0;
    int? bytesItems = 0;
    int? cacheBytesItems = 0;
    if (items != null && items!.isNotEmpty) {
      timeItems = items?.map((e) => e.time).reduce((value, element) => value! + element!);
      // 已经缓存的数据大小不做统计
      bytesItems = items?.map((e) => e.isCache ? 0 : e.bytes).reduce((value, element) => value! + element!);
      cacheBytesItems = items?.map((e) => e.isCache ? e.bytes : 0).reduce((value, element) => value! + element!);
    }

    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = url;
    data['time'] = '${time}'; // ms
    data['remain_time'] = '${remainTime}'; // ms
    data['items_time'] = '${(time ?? 0) + (timeItems ?? 0)}';// ms
    data['bytes'] = '${(bytes ?? 0) + (bytesItems ?? 0)}'; // 为下载的网络数据大小 bytes
    data['cache_bytes'] = '${cacheBytesItems ?? 0}'; // 缓存读取的资源大小 bytes
    data['items_length'] = items?.length;
    data['items'] = items?.map((e) => e.toJson()).toList();
    return data;
  }

  /// 简化上报参数，避免上报限制异常
  Map<String, dynamic> toSimpleJson() {
    final Map<String, dynamic> data = toJson();
    data.remove('items');
    data.remove('url');
    return data;
  }
}

class YBDInterceptManager {
  // 工厂模式
  factory YBDInterceptManager() => _getInstance()!;
  static YBDInterceptManager? get instance => _getInstance();
  static YBDInterceptManager? _instance;
  YBDInterceptManager._internal();
  static YBDInterceptManager? _getInstance() {
    if (_instance == null) {
      _instance = new YBDInterceptManager._internal();
    }
    return _instance;
  }

  Map<String?, List<YBDInterceptWebItem>> _map = {};

  // 关闭记录
  bool closeRecord = Const.TEST_ENV;

  /**
   * {url:[
   * InterceptWebtem
   * ]}
   * */
  ///
  _addInterceptWebItem(String? url, YBDInterceptWebItem item) {
    // _lock.synchronized(() {
    var items = getInterceptWebItems(url)!;
    items.add(item);
    _map[url] = items;
    // logger.v("YBDInterceptManager addInterceptWebItem: $item");
    // });
  }

  /// url 为key， subUrl为key里面对象单次加载key
  updateInterceptWebItem(String? url, String? subUrl, YBDInterceptWebItem item) {
    // if (closeRecord) return;

    // _lock.synchronized(() {
    var webItem = YBDInterceptManager.instance!.getInterceptWebItemsLast(url, subUrl);
    if (item.url != null) webItem.url = item.url;
    if (item.remainTime != null) webItem.remainTime = item.remainTime;
    if (item.time != null) webItem.time = item.time;
    if (item.bytes != null) webItem.bytes = item.bytes;
    // logger.v("YBDInterceptManager addInterceptWebItem: $item");
    // });
  }

  addInterceptItem(String? url, String? subUrl, YBDInterceptItem item) {
    // if (closeRecord) return;

    // _lock.synchronized(() {
    var webItem = YBDInterceptManager.instance!.getInterceptWebItemsLast(url, subUrl);
    var items = webItem.items ?? [];
    if (webItem.items == null) {
      webItem.items = items;
    }
    items.add(item);
    // logger.v("YBDInterceptManager addInterceptItem: ${item.url}, ${item.isCache}");
    // });
  }

  List<YBDInterceptWebItem>? getInterceptWebItems(String? url) {
    if (!_map.containsKey(url)) {
      List<YBDInterceptWebItem> items = [];
      _map[url] = items;
    }
    // logger.v("YBDInterceptManager getInterceptWebItems: ${_map[url]}");
    return _map[url];
  }

  YBDInterceptWebItem getInterceptWebItemsLast(String? url, String? subUrl) {
    var items = getInterceptWebItems(url)!;
    var last = items.firstWhere((element) => element.url == subUrl, orElse: () {
      var item = YBDInterceptWebItem()..url = subUrl;
      _addInterceptWebItem(url, item);
      return item;
    });
    // logger.v("YBDInterceptManager getInterceptWebItemsLast: ${last.url}, items: ${items.length}");
    return last;
  }

  /// 打印和保存当前拦截记录，并清空
  logMap() {
    // 上传记录
    /*
    * "url":"https://www.oyetalk.tv/event/luckyPatternOneJuly/index.html?isFlutter=1", // web地址
    * "remainTime":10000, 停留时间
    * "time":486, // web加载时间ms
    * "time+items":1462, // web+资源加载时间ms
    * "bytes":2048264, // 网络下载大小，使用了缓存不统计
    * "cacheBytes": // 缓存读取的资源大小
    * "items.length":72 // 资源条数
    *
    * **/

    String? url = _map.keys.last;
    if (url == null) return;
    YBDInterceptWebItem? webItem = getInterceptWebItems(url)?.last;
    if (webItem == null) return;
    var path = Uri.tryParse(url)?.path ?? '';

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.WEB_INTERCEPT_EVENT,
        location: path.substring(0, min(path.length, MAX_PARAM_VALUE_LENGTH)),
        itemName: url.substring(0, min(url.length, MAX_PARAM_VALUE_LENGTH)),
        extParams: webItem.toSimpleJson()));

    try {
      logger.v("YBDInterceptManager map: ${jsonEncode(_map)}");
    } catch (e) {
      logger.v("YBDInterceptManager map error: ${e.toString()}");
    }
    _map = {};
    _filterHosts = null;
    _interceptSuffixes = null;
  }

  static List<String>? _filterHosts;
  static List<String>? _interceptSuffixes;
  static List<String>? _interceptRegExpHosts;

  /*
  *  是否应该拦截
  *
  * */
  static bool shouldIntercept(String url) {

    // 根据正则来匹配
    List<String>? interceptRegExpHosts = _interceptRegExpHosts ??= YBDCommonUtil.getRoomOperateInfo().interceptRegExpHosts;
    for (String host in interceptRegExpHosts ?? []) {
      try {
        if (url.contains(RegExp(host))){
          return true;
        }
      }catch (e) {
        logger.e('interceptRegExpHosts error:${e.toString()}');
      }
    }

    // 1. 域名过滤逻辑
    List<String>? filterHosts = _filterHosts ??= YBDCommonUtil.getRoomOperateInfo().filterHosts;
    // 1.1 如果是有 */* 直接全部过滤掉
    if (filterHosts?.contains('*/*') ?? true) return false;
    // 1.2 如果url包含其中的一个就不拦截了
    var filter = filterHosts?.firstWhereOrNull((element) => url.contains(element));
    if (filter != null) return false;

    // 2. 后缀名过滤逻辑
    List<String>? interceptSuffixes = _interceptSuffixes ??= YBDCommonUtil.getRoomOperateInfo().interceptSuffixes;
    String suffix = url.split(".").last;
    // 2.1 不包含的后缀名过滤掉
    if (!(interceptSuffixes?.contains(".$suffix") ?? false)) {
      return false;
    }
    // 3. 来到这里说明你很尊贵！
    // 这里只支持.js, .css, .png, .jpg, .jpeg, .gif, .webp, .svga;
    // 就算interceptSuffixes 填写了，如果不在上面，也是不起作用的。避免数据传输出错！
    return true;
  }

  // 请求拦截 url: 请求的地址， webUrl：页面地址， webSubUrl：拼接了时间等相关参加的地址
  static Future<Response?> shouldInterceptRequest(String url, String? webUrl, String? webSubUrl) async {
    logger.v("_shouldInterceptRequest _filterHosts: $_filterHosts, _interceptSuffixes: $_interceptSuffixes, url:$url");
    // 缓存文件数据
    Future<Uint8List> interceptResponseUint8List() async {
      var startTime = DateTime.now().millisecondsSinceEpoch;
      var isCache = await YBDWebCacheManager().fileExists(url);
      File file = await YBDWebCacheManager().getWebDownloadFile(url);
      Uint8List bytes = file.readAsBytesSync();
      var endTime = DateTime.now().millisecondsSinceEpoch;

      // 埋点
      YBDInterceptManager.instance!.addInterceptItem(
        webUrl,
        webSubUrl,
        YBDInterceptItem()
          ..url = url
          ..time = endTime - startTime
          ..isCache = isCache
          ..bytes = bytes.length,
      );
      return bytes;
    }
    void interceptResponseUint8ListLrhgAoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

    // 根据类型设置Response
    Future<Response> interceptResponseFile(String mineType) async {
      Uint8List bytes = await interceptResponseUint8List();
      return Response("$mineType", null, bytes);
    }

    String suffix = url.split(".").last;
    MediaCacheWidgetType type = MediaCacheWidgetType.auto.autoType(url);
    if (type == MediaCacheWidgetType.pic) {
      return interceptResponseFile('image/$suffix');
    } else if (url.endsWith(".js") || url.endsWith(".css")) {
      return interceptResponseFile('text/$suffix');
    } else if (url.endsWith(".otf") || url.endsWith(".ttf") || url.endsWith(".svga")) {
      return interceptResponseFile('application/octet-stream');
    }
    return null;
  }
}
