import 'package:flutter/widgets.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/date_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/entity/result_ybd_bean.dart';
import 'package:oyelive_main/module/payment/payment_ybd_api_helper.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/recharge/entity/db_ybd_inApp_purchase_entity.dart';
import 'package:oyelive_main/ui/page/recharge/entity/db_ybd_order_purchase_entity.dart';

import 'database_ybd_until.dart';
import 'log_ybd_util.dart';

/// 支付工具类
class YBDPurchaseUtil {
  ///间隔执行时间 seconds
  static List<int> delayedTimes = [1, 3, 5];

  ///执行最大次数
  static const maxTimes = 3;

  ///  校验google支付 订单（检查支付成功，我们）
  static void googleVerify(BuildContext context, {int times = 0}) async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
    YBDLogUtil.d('googleVerify times:$times');

    ///用户信息为空，间隔多次执行
    if ((userInfo == null || userInfo.id == null)) {
      if (times < maxTimes) {
        //延时执行
        Future.delayed(Duration(seconds: delayedTimes[times]), () {
          YBDLogUtil.d('googleVerify times googleVerify:$times');
          googleVerify(context, times: ++times);
        });
      }
      return;
    }
    YBDLogUtil.d('googleVerify userInfo.id:${userInfo.id}');
    try {
      List<YBDDBInAppPurchaseEntity>? messages =
          await YBDDataBaseUtil.getInstance().inAppQuery(userInfo.id!, serviceState: STATE_VERIFY_SUCCESS, equal: false);
      YBDLogUtil.d('googleVerify messages?.length:${messages?.length}');
      messages?.forEach((element) async {
        await _verifyPurchase(context, element, userInfo);
      });
    } catch (e) {
      print(e);
    }
  }

  ///回调服务器  确认结果
  static _verifyPurchase(BuildContext context, YBDDBInAppPurchaseEntity dbInAppPurchaseEntity, YBDUserInfo userInfo) async {
    YBDLogUtil.d('YBDPurchaseUtil _verifyPurchase orderID:${dbInAppPurchaseEntity.orderID}');
    YBDResultBeanEntity result = await YBDPaymentApiHelper.requestCallbacksPurchaseAll(context,
        orderId: dbInAppPurchaseEntity.orderID!,
        payType: 'google',
        command: 'success',
        signedData: dbInAppPurchaseEntity.signedData,
        signature: dbInAppPurchaseEntity.signature);
    YBDLogUtil.d('YBDPurchaseUtil _verifyPurchase productID:${dbInAppPurchaseEntity.orderID} re:$result');
    if (result.code == Const.HTTP_SUCCESS_NEW) {
      YBDLogUtil.d('YBDPurchaseUtil requestCallbacksPurchaseAll lost_success');
      dbInAppPurchaseEntity.serviceState = STATE_VERIFY_SUCCESS;
      YBDDataBaseUtil.getInstance().inAppUpdate(dbInAppPurchaseEntity, stateType: 0);

      /// firebase database 记录异常订单
      YBDCommonUtil.storeLostGoogleOrder(
        userInfo.id,
        dbInAppPurchaseEntity.usrId,
        dbInAppPurchaseEntity.spOrderID ?? '',
        'lost_success',
        signature: dbInAppPurchaseEntity.signedData,
        otOrderId: dbInAppPurchaseEntity.orderID,
      );
    } else {
      YBDLogUtil.d('YBDPurchaseUtil requestCallbacksPurchaseAll lost_failed_{${result.code}');
      dbInAppPurchaseEntity.serviceState = STATE_VERIFY_FAILED;
      YBDDataBaseUtil.getInstance().inAppUpdate(dbInAppPurchaseEntity, stateType: 0);

      /// firebase database 记录异常订单
      YBDCommonUtil.storeLostGoogleOrder(
        userInfo.id,
        dbInAppPurchaseEntity.usrId,
        dbInAppPurchaseEntity.spOrderID ?? '',
        'lost_failed_${result.code}',
        signature: dbInAppPurchaseEntity.signedData,
        otOrderId: dbInAppPurchaseEntity.orderID,
      );
    }
  }

/*  ///获取预订单ID
  ///透传策略有2种 第二种情况下obfuscatedProfileId为空，obfuscatedAccountId装预订单Id
  static String getOrderID(String obfuscatedProfileId, String obfuscatedAccountId) {
    if (obfuscatedProfileId == null || obfuscatedProfileId.isEmpty) {
      return obfuscatedAccountId;
    }
    return obfuscatedProfileId;
  }*/

  ///透传字段加了前缀 本地存储前需要去除前缀 用户前缀：U  订单前缀：O
  static String removePrefix(String obfuscatedValue) {
    if (obfuscatedValue == null || obfuscatedValue.isEmpty) {
      return obfuscatedValue;
    }
    if (obfuscatedValue.startsWith('U') || obfuscatedValue.startsWith('O')) {
      return obfuscatedValue.substring(1, obfuscatedValue.length);
    }
    return obfuscatedValue;
  }

  ///获取透传的用户ID：U  订单前缀：O
  static String? getAccountId(String obfuscatedAccountId) {
    YBDLogUtil.d("getAccountId obfuscatedAccountId: $obfuscatedAccountId");
    if (obfuscatedAccountId == null || obfuscatedAccountId.isEmpty) {
      return obfuscatedAccountId;
    }

    List<String> obfuscatedValueList = obfuscatedAccountId.split('|');
    YBDLogUtil.d("getAccountId obfuscatedValueList: $obfuscatedValueList");
    if (obfuscatedValueList == null || obfuscatedValueList.isEmpty) {
      return null;
    }
    return removePrefix(obfuscatedValueList[0]);
  }

  ///获取透传预订单ID：U  订单前缀：O
  static String? getProfileId(String obfuscatedAccountId, String obfuscatedProfileId) {
    YBDLogUtil.d("getProfileId obfuscatedAccountId: $obfuscatedAccountId obfuscatedProfileId: $obfuscatedProfileId");

    ///透传数据都为空，返回null
    if (obfuscatedAccountId == null && obfuscatedProfileId == null) {
      return null;
    }


    ///透传obfuscatedProfileId不为空
    if (obfuscatedProfileId != null && obfuscatedProfileId.isNotEmpty) {
      return removePrefix(obfuscatedProfileId);
    }
    List<String> obfuscatedValueList = obfuscatedAccountId.split('|');
    YBDLogUtil.d("getProfileId obfuscatedValueList: $obfuscatedValueList");
    if (obfuscatedValueList == null || obfuscatedValueList.length < 2) {
      return null;
    }

    ///透传obfuscatedProfileId为空 obfuscatedAccountId不为空 (U2099955|O88888)
    return removePrefix(obfuscatedValueList[1]);
  }

  ///删除订单
  static inAppDeleteCompleteOrder() async {
    int userId = (await YBDUserUtil.userIdInt()) ?? -1;
    int pastTime = YBDDateUtil.pastUtcTimestamp(15);
    YBDLogUtil.d('inAppDeleteCompleteOrder pastTime:$pastTime');
    try {
      List<YBDDBInAppPurchaseEntity> messages = (await YBDDataBaseUtil.getInstance().inAppQueryPast(userId, pastTime)) ?? [];
      await YBDDataBaseUtil.getInstance().inAppDeleteList(messages, userId);
    } catch (e, s) {
      YBDLogUtil.e('inAppDeleteCompleteOrder userId: $userId from db error');
    }
    YBDLogUtil.d("inAppDeleteCompleteOrder userId: $userId");
  }

  //根据productID、state、userId获取最近的预订单Id
  static Future<String?> getIosPayOrderId(String productID) async {
    try {
      if (productID == null) {
        YBDLogUtil.d('getIosPayOrderId productID is null');
        return null;
      }
      List<YBDDBOrderPurchaseEntity>? orderMessage = await YBDDataBaseUtil.getInstance().iosOrderQuery(productID);
      if (orderMessage != null) {
        YBDLogUtil.d('getIosPayOrderId orderMessage length:${orderMessage.length}');
        int newestOrderIdIndex = orderMessage.length - 1;
        YBDLogUtil.d('getIosPayOrderId productID is newestOrderIdIndex:$newestOrderIdIndex');
        return orderMessage[newestOrderIdIndex].orderID;
      } else {
        YBDLogUtil.d('getIosPayOrderId orderMessage is null');
        return null;
      }
    } catch (e) {
      YBDLogUtil.e('getIosPayOrderId error: $e');
      return null;
    }
  }

  //根据第三方id获取预订单id
  static Future<String?> getOrderIdByThirdId(String thirdId) async {
    try {
      if (thirdId == null) {
        YBDLogUtil.d('getOrderIdByThirdId thirdId is null');
        return null;
      }
      var order = await YBDDataBaseUtil.getInstance().orderQueryBySpOrderID(thirdId);
      if (order != null && order.isNotEmpty) {
        return order;
      } else {
        YBDLogUtil.d('getOrderIdByThirdId order is null');
        return null;
      }
    } catch (e) {
      YBDLogUtil.e('getOrderIdByThirdId error: $e');
      return null;
    }
  }
}
