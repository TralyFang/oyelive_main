

import 'package:flutter/services.dart';

List<TextInputFormatter> PWD_RESTRICT = [
// FilteringTextInputFormatter.deny(new RegExp(
//     r"\s\b|\b\s'(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])'")),
// FilteringTextInputFormatter.allow(RegExp(r'^[a-zA-Z0-9_\-=@,\\.|\\,]+$')),
  FilteringTextInputFormatter.deny(
      new RegExp('(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])')),
  FilteringTextInputFormatter.deny(new RegExp(r"\s\b|\b\s")),
  FilteringTextInputFormatter.deny(RegExp('[ ]')),
];
