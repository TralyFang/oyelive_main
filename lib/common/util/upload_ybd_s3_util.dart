

import 'dart:async';
import 'dart:io';

import 'package:amazon_s3_cognito/amazon_s3_cognito.dart';
import 'package:amazon_s3_cognito/aws_region.dart';
// import 'package:amazon_s3_cognito/image_data.dart';
import 'package:random_string/random_string.dart';
import '../constant/const.dart';
import '../http/environment_ybd_config.dart';
import 'log_ybd_util.dart';
import 'sp_ybd_util.dart';

enum UploadS3Type {
  Log,
  Image,
  Audio,
}

/// 上传文件到 S3 的工具类
class YBDUploadS3Util {
  /// 上传文件到 S3 并记录已上传文件的文件名
  static Future<String?> uploadFile(File? file, {UploadS3Type type = UploadS3Type.Image}) async {
    if (null == file) {
      logger.v("file is null");
      return null;
    }

    // 检查要上传的文件是否为空
    bool isFileExist = await file.exists();

    if (!isFileExist) {
      logger.v("file is not exist");
      return null;
    }
    logger.v("upload file path ${file.path}");

    String extension = fileExtension(file.path);
    // TODO：放到常量文件中
    String awsFolderPath = "temp";
    String? userId = await YBDSPUtil.getUserId();
    String? s3ImgName;

    switch (type) {
      case UploadS3Type.Audio:
      case UploadS3Type.Image:
        s3ImgName =
            "$awsFolderPath/${userId}_${DateTime.now().millisecondsSinceEpoch}_${randomAlphaNumeric(10)}$extension";
        break;
      case UploadS3Type.Log:
        // https://s3-ap-south-1.amazonaws.com/oyetalk-status/temp/log/<userId>.zip
        // 根据用户 id 来从 s3 下载用户的日志，这里就不加时间戳了
        // String logFileName = '${DateTime.now().millisecondsSinceEpoch}_';
        String fileSuffix = '${file.path.substring(file.path.lastIndexOf('/') + 1)}';
        s3ImgName = "$awsFolderPath/log/$fileSuffix";
        break;
    }

    // 配置上传插件

    String? result = await AmazonS3Cognito.upload(
      file.path,
      Const.TEST_ENV ? YBDEnvConfig.currentConfig().uploadS3 : "oyetalk-status",
      "ap-south-1:c1ad253f-921b-4e75-9581-737ff5a626b4",
      s3ImgName,
      AwsRegion.AP_SOUTH_1,
      AwsRegion.AP_SOUTH_1,
    );

/*
    String? result = await AmazonS3Cognito.upload(
      Const.TEST_ENV ? YBDEnvConfig.currentConfig().uploadS3 : "oyetalk-status",
      "ap-south-1:c1ad253f-921b-4e75-9581-737ff5a626b4",
      AwsRegion.AP_SOUTH_1,
      AwsRegion.AP_SOUTH_1,
      ImageData(s3ImgName, file.path)
    );
 */
    logger.v("upload result : $result");

    /// 监听s3文件上传进度
//    amazonS3Cognito.getProgress.listen((event) {
//      logger.v("uploading progress: $event");
//    });

    if (result?.startsWith("https://") ?? false) {
      logger.v("_upload file return true");

      if (type == UploadS3Type.Log) {
        return result;
      }

      return s3ImgName;
    } else {
      logger.v("_upload file return false");
      return null;
    }
  }

  /// 获取文件扩展名
  static String fileExtension(String path) {
    // 检查参数是否为字符串
    if (null == path || !(path is String)) {
      logger.v('$path is not valid');
    }

    String? extension;
    List arr = path.split('.');

    // 以点结束的最后一部分字符串
    if (null != arr && arr.isNotEmpty) {
      extension = '.${arr.last}';
    } else {
      logger.v('not found do in path string');
    }

    // 检查扩展名是否有效，最长的后缀是 .jpeg
    if (null != extension && extension.isNotEmpty && extension.length < 6) {
      logger.v('file extension : $extension');
      return extension;
    } else {
      logger.v('file format is invalid');
    }

    // 文件扩展名没找到
    logger.v('return default extension : empty string');
    return '';
  }
}
