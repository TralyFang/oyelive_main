

import 'dart:async';

import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/inbox/inbox_ybd_api_helper.dart';
import 'package:oyelive_main/module/inbox/message_ybd_helper.dart';
import 'package:oyelive_main/module/status/entity/block_ybd_user_entity.dart';
import 'package:oyelive_main/module/status/status_ybd_api_helper.dart';

extension BaseStateType on HttpType {
  value() {
    String value = '';
    switch (this) {
      case HttpType.SlogBlack:
        value = translate('slog_blocklist');
        break;
      case HttpType.InboxBlack:
        value = translate('inbox_blocklist');
        break;
      case HttpType.RoomBlack:
        value = translate('room_blocklist');
        break;
      case HttpType.Default:
        break;
    }
    return value;
  }

  operator(int? blackId, Function(bool isSuccess) callBack) async {
    bool success = false;
    switch (this) {
      case HttpType.SlogBlack:
        YBDBlockUserEntity? entity = await YBDStatusApiHelper.blockUserSearch(Get.context, 2, blackId);
        success = entity?.code == Const.HTTP_SUCCESS_NEW;
        break;
      case HttpType.InboxBlack:
        success = await YBDMessageHelper.blockContact(Get.context, blackId.toString(), EditAction.Delete);
        break;
      case HttpType.RoomBlack:
        success = await ApiHelper.deleteRoomBlacklist(Get.context, blackId);
        break;
      case HttpType.Default:
        break;
    }
    success ? YBDToastUtil.toast(translate("block_success")) : YBDToastUtil.toast(translate('block_fail'));
    callBack(success);
  }
}
