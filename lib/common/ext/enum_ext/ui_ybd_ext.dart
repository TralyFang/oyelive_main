



import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/common/util/intl_ybd_lang_util.dart';

extension IntlMAAlignment on MainAxisAlignment{
  //和系统保持一致 使用属性代替 静态方法
    static get end{
      return YBDIntlLangUtil.isArabic() ? MainAxisAlignment.start : MainAxisAlignment.end;
    }

    static get start{
      return YBDIntlLangUtil.isArabic() ? MainAxisAlignment.end : MainAxisAlignment.start;
    }
}

extension IntlAlignment on Alignment{
   static get centerRight{
      return YBDIntlLangUtil.isArabic() ? Alignment.centerLeft : Alignment.centerRight;
   }

   static get centerLeft{
     return YBDIntlLangUtil.isArabic() ? Alignment.centerRight : Alignment.centerLeft;
   }
}