

import 'dart:async';
import 'dart:convert';

import 'package:characters/characters.dart';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/custom_dialog/custom_ybd_dialog.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/page/room/entity/room_ybd_operate_info.dart';

extension StringExt on String {
  String unsubLog({int? step, String? log}) {
    return 'unsubscribeLog:' + '$step:' + this + 'ps: $log';
  }
  void unsubLogLx34Ooyelive({int? step, String? log}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String subLog({int? step, String? log}) {
    return 'subscribeLog:' + '$step:' + this + 'ps: $log';
  }
  void subLogDJLx0oyelive({int? step, String? log}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
  String removeBlankSpace() {
    if (this == null) return "";
    return Characters(this).join("\u{200B}");
    return this.replaceAll("", "\u{200B}");
  }
  void removeBlankSpaceoP2vdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<String> webCookie() async {
    String cookieApp = (await YBDSPUtil.get(Const.SP_COOKIE_APP)) ?? '';
    int? openWebAuth = YBDCommonUtil.getRoomOperateInfo().openWebAuth;
    var append = 'app=$cookieApp&time=${DateTime.now().microsecondsSinceEpoch}';
    if (openWebAuth == 1) {
      String authorization = (await YBDSPUtil.get(Const.SP_HTTP_AUTHORIZATION)) ?? '';
      append += '&auth=$authorization';
    }
    if (contains('?')) {
      return this + '&$append';
    }
    return this + '?$append';
  }
  void webCookiey0Zv7oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  WebEntranceType getType() {
    if (this == null || this.isEmpty) return WebEntranceType.Default;
    switch (int.parse(this)) {
      case 1:
        return WebEntranceType.Banner;
      case 2:
        return WebEntranceType.Slider;
      case 3:
        return WebEntranceType.AlertView;
      case 4:
        return WebEntranceType.EventNotice;
      case 5:
        return WebEntranceType.EventCenter;
      case 6:
        return WebEntranceType.Float;
    }
    return WebEntranceType.Default;
  }

  int? toInt() {
    if (this == null || double.tryParse(this) == null) return -1;
    return this.contains('.') ? double.parse(this).round() : int.tryParse(this);
  }

  String cutSuffix(String exp) {
    if (this.endsWith(exp)) {
      return this.substring(0, this.length - exp.length);
    }
    return this;
  }
  void cutSuffixmqMhioyelive(String exp) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String subStringAntIndex(String exp) {
    logger.v('sub string ${this} exp');
    if (this.contains(exp)) return this.substring(0, this.indexOf(exp));
    return this;
  }

  toBool() {
    return this == null ? false : (this == '1' ? true : false);
  }

  ///遍历this 是否包含ls 子元素
  bool containsListSub(List<String> ls) {
    bool result = false;
    if (ls == null || ls.isEmpty) return result;
    ls.forEach((element) {
      if (this.contains(element)) {
        result = true;
      }
    });
    return result;
  }
  void containsListSubKN9Zcoyelive(List<String> ls) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void showLoading(context) {
    showDialogs(
      child: null,
      type: DialogType.loading,
      loadUrl: this,
      global: false,
      barrierDismissible: false,
      context: context,
    );
  }

  // md5 加密
  String generate_md5() {
    var content = Utf8Encoder().convert(this);
    var digest = md5.convert(content);
    // 这里其实就是 digest.toString()
    return hex.encode(digest.bytes);
  }

  static String md5String(String str) {
    return str.generate_md5();
  }

  // 获取已拼接的图片全链接中的S3key
  String getS3Key() {
    if (YBDTPGlobal.s3Host.isNotEmpty) {
      String fileName = this.substring(YBDTPGlobal.s3Host.length);
      return fileName;
    }
    return '';
  }
  void getS3KeyfzizJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 拼接的图片全链接
  String getS3Path() {
    return 'https://s3-ap-south-1.amazonaws.com/oyevoice-test/' + this;
  }

  String get category {
    String _result = 'Standard';
    switch (this) {
      case '1':
        _result = 'Special';
        break;
      case '2':
        _result = 'Heart';
        break;
      case '3':
        _result = 'Party';
        break;
      case '4':
        _result = 'Friends';
        break;
      case '5':
        _result = 'Exclusive';
        break;
      case '6':
        _result = 'Royal';
        break;
      case '0':
        _result = 'Standard';
        break;
    }
    return _result;
  }

  // 获取url中的key
  String? get key {
    late Uri u;
    try {
      u = Uri.parse(this);
    } catch (e) {
      logger.e('url trans fail : $e');
    }

    String? key = u.queryParameters.keyToString('key');
    return key;
  }

  // 文字溢出
  String overflow(int number) {
    if (this == null) return '';
    if (this.length <= number) {
      return this;
    } else {
      return this.substring(0, number) + '...';
    }
  }
  void overflowL4oOIoyelive(int number) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

extension MapExt on Map {
  int? keyToInt(String key) {
    return this.keys.contains(key) ? int.tryParse(this[key]) : -1;
  }

  String? keyToString(String key) {
    return this.keys.contains(key) ? this[key] : '1';
  }

  /// 追加一个非空值
  appendNotEmpty(String key, String? value) {
    if (value != null && value.isNotEmpty) {
      this[key] = value;
    }
  }

  Map<String, dynamic> allParams(int start, int offset) {
    int upStart = start;
    int upOffset = offset;
    Map upMap = this;
    upMap.addAll({'start': upStart, 'offset': upOffset, 'page': upOffset / upStart, 'pageSize': upOffset});
    return upMap as Map<String, dynamic>;
  }
}
