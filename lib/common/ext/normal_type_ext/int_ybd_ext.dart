

import 'dart:math';
import 'package:redux/redux.dart';

import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/common/util/module/room_ybd_operate_imp.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/query_ybd_label_rooms_resp_entity.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/ui/page/room/entity/room_ybd_operate_info.dart';
import 'package:oyelive_main/ui/page/room/room_ybd_helper.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/set_ybd_ext.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';

const String IN = 'INDIA';

extension IntExt on int? {
  ///全局进入房间业务
  /// funcType  = 1 跳转房间打开teenpatti
  //            = 2 跳转房间打开水果游戏
  //            = 3 跳转房间打开送礼框
  //            = 4 跳转房间上麦
  enterRoom({EnterRoomType? type, int? gameCode}) async {
    ///如果已经在房间
    logger.v(
        'enter room ext: ${YBDObsUtil.instance().routeSets} ${YBDObsUtil.instance().routeSets.contains(YBDNavigatorHelper.flutter_room_list_page)}');
    if (YBDObsUtil.instance().routeSets.contains(YBDNavigatorHelper.flutter_room_list_page)) {
      YBDObsUtil.instance().routeSets.operate(type, gameCode);
      return;
    }

    YBDRoomOperateInfo info = YBDCommonUtil.getRoomOperateInfo();
    logger.v('enter room info : $info');
    //如果当前roomId 不存在 即默认值-1
    if (this == defaultValue) {
      //获取房间列表前randnum的数据 3秒超时
      YBDDialogUtil.showLoading(Get.context);
      YBDQueryLabelRoomsRespEntity? popularListEntity = await ApiHelper.queryPopularRooms(
        Get.context,
        0,
        info.randomNum,
        timeout: info.timeOut * 1000,
      );
      YBDDialogUtil.hideLoading(Get.context);
      //随机进入一个 直播间
      int a = info.randomNum == 0 ? 0 : Random().nextInt(info.randomNum!);
      logger.v('enter room random: $a, ${popularListEntity?.returnCode}');
      try {
        if (popularListEntity?.returnCode == Const.HTTP_SUCCESS && !(popularListEntity?.record?.rank?.isEmpty ?? true)) {
          //随机数大于服务端的list length 就进入list最后一个
          if (a > (popularListEntity?.record?.rank?.length ?? 0)) {
            if (popularListEntity?.record?.rank?.last?.protectMode != 1) {
              popularListEntity?.record?.rank?.last?.id?.enter(type!.index, gameCode: gameCode);
              return;
            }
          } else {
            if ((popularListEntity?.record?.rank?.length ?? 0) > a && popularListEntity?.record?.rank![a]?.protectMode != 1) {
              popularListEntity?.record?.rank![a]?.id?.enter(type!.index, gameCode: gameCode);
              return;
            }
          }
        }
      } catch (e) {
        logger.v('get ip code error: $e');
      }
      //超时 根据ip 进入官方直播间 非印度进巴铁房
      logger.v('enter room timeout');
      String? code = await YBDCommonUtil.getIpCountryCode(Get.context);

      try {
        ///2.4.7 需求确认
        Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: Get.context)!;
        code = YBDCommonUtil.getCountryByCode(store.state.bean!.country);
        logger.v('current code = $code');
      } on Error catch (error) {
        logger.v('get ip code error: $error');
      }
      (code!.toUpperCase() == IN ? info.INID : info.PKID).enter(type!.index, gameCode: gameCode);
      return;
    }
    this.enter(type!.index, gameCode: gameCode);
  }

  enter(int funcType, {int? gameCode}) {
    //走到这里有没有可能存在非法id
    logger.v('enter room id: $this, $funcType');
    YBDRoomHelper.enterRoom(Get.context, this, funcType: funcType, gameCode: gameCode);
  }

  StintType stintType() {
    if (this == 1) {
      return StintType.none;
    } else if (this == 2) {
      return StintType.level;
    } else if (this == 3) {
      return StintType.room;
    } else if (this == 4) {
      return StintType.level7Room;
    } else {
      return StintType.none;
    }
  }
}
