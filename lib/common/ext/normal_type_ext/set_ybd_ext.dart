import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/ui/page/home/entity/king_ybd_kong_entity.dart';

extension SetExt on Set<String> {
  operate(EnterRoomType? type, int? gameCode) {
    ///如果当前处在房间
    logger.v(
        'enter room operate: ${this.last.contains(YBDNavigatorHelper.flutter_room_list_page)}');
    if (last.contains(YBDNavigatorHelper.flutter_room_list_page)) {
      YBDModuleCenter.instance().roomFuncOperation(type, gameCode: gameCode);
      return;
    }

    /// 返回到指定页面 两种场景都黑屏 待解决 产品建议返回上一层 如果是n级页面 只pop 不会弹tp
    try {
      Navigator.of(Get.context!).popUntil((route) {
        logger.v('route settings name: ${route.settings.name}');
        return route.settings.name
                ?.startsWith(YBDNavigatorHelper.flutter_room_list_page) ??
            true;
      });
    } catch (e) {
      logger.v('pop fail');
    }
    // Navigator.pop(Get.context);
    //  Navigator.of(Get.context).popUntil(ModalRoute.withName(YBDNavigatorHelper.flutter_room_list_page));
    Future.delayed(Duration(milliseconds: 200), () {
      YBDModuleCenter.instance().roomFuncOperation(type, gameCode: gameCode);
    });
  }

  bool subContain(String router) {
    return where((String element) => element.contains(router)).isNotEmpty;
    // return map((String e) => e.contains(router)).toList().contains(true);
  }

  void subContainwnhGwoyelive(String router) {
    int needCount = 0;
    print('input result:$needCount');
  }
}

extension ListExt on List<String> {
  bool subContain(String router) {
    return map((String e) => e.contains(router)).toList().contains(true);
  }

  void subContainAScwyoyelive(String router) {
    int needCount = 0;
    print('input result:$needCount');
  }
}

extension LExt on List<YBDKingKongRecord?>? {
  List<YBDKingKongRecord?>? def() {
    if (this != null) return this;
    List<String> iconsTitles = ['activity', 'record', 'cp', 'combo'];
    List<String> pathList = [
      YBDNavigatorHelper.event_center,
      YBDNavigatorHelper.leaderboard + "/2",
      YBDNavigatorHelper.cp_list,
      YBDNavigatorHelper.combo_list,
    ];
    List<YBDKingKongRecord> ls = [];
    for (int i = 0; i < iconsTitles.length; i++) {
      YBDKingKongRecord record = YBDKingKongRecord()
        ..route = pathList[i]
        ..img = 'assets/images/default/popular_default.webp'
        ..name = iconsTitles[i];
      ls.add(record);
    }
    // logger.v('king kong list end $ls');
    return ls;
  }
}
