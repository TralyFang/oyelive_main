

import 'dart:ui';

class YBDHexColor extends Color {
  /*
  Color color3 = YBDHexColor("0xffb74093");
  Color color1 = YBDHexColor("b74093");
  Color color2 = YBDHexColor("#b74093");
  Color color3 = YBDHexColor("#88b74093"); // if you wish to use ARGB format
  https://qa.1r1g.com/sf/ask/3505684941/
  * */
  static int _getColorFromHex(String? hexColor) {
    if (hexColor == null) {
      hexColor = "#FFFFFF";
    }
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    hexColor = hexColor.toUpperCase().replaceAll("0X", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.tryParse(hexColor, radix: 16) ?? 0xFFFFFFFF;
  }

  YBDHexColor(final String? hexColor) : super(_getColorFromHex(hexColor));
}
