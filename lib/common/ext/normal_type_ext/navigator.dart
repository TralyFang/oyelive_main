




import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/set_ybd_ext.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';

class YBDNavigator extends Navigator{
   static pop({required String router}){
     if(router.isEmpty){
       if (!Navigator.canPop(Get.context!)) return;
         Navigator.pop(Get.context!);
       return;
     }
     logger.v('router obs : ${YBDObsUtil.instance().routeList} ${YBDObsUtil.instance().routeList.subContain(router)}');
     if(YBDObsUtil.instance().routeList.subContain(router)){
       logger.v('router obs 1: ${YBDObsUtil.instance().routeList} ${YBDObsUtil.instance().routeList.subContain(router)}, ${Get.currentRoute}');
       if (Get.currentRoute.contains(router))return;
       if (!Navigator.canPop(Get.context!)) return;
         Navigator.pop(Get.context!);
       Future.delayed(Duration(milliseconds: 100),(){
         YBDNavigator.pop(router: router);
       });
     }
   }
}