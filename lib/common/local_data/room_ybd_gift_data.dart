

import 'dart:convert';

import 'package:flutter/cupertino.dart';

import '../../generated/json/base/json_convert_content.dart';
import '../../module/room/entity/gift_ybd_package_entity.dart';
import '../../module/room/room_ybd_api_helper.dart';
import '../constant/const.dart';
import '../util/file_ybd_util.dart';
import '../util/log_ybd_util.dart';
import 'local_ybd_data_def.dart';

/// 本地存放房间礼物数据
class YBDRoomGiftData {
  /// 读取房间礼物信息
  /// 先从本地文件获取
  /// 本地没有数据再从网络获取
  /// 每次读数据都会从网络刷新本地数据
  static Future<YBDGiftPackageEntity?> readRoomGift(BuildContext context) async {
    // 礼物包实体类
    YBDGiftPackageEntity? giftEntity;

    // 本地 json 文件
    String? giftJson = await YBDFileUtil.readJsonFromFile(ROOM_GIFT_JSON);

    // 本地有数据时取本地数据并从网络获取最新数据
    if (null != giftJson) {
      try {
        Map? mapData = json.decode(giftJson);
        giftEntity = JsonConvert.fromJsonAsT<YBDGiftPackageEntity>(mapData);
        YBDRoomGiftData._refreshRoomGift(context);
      } catch (e) {
        logger.e('read room gift data error : $e');
        return null;
      }

      logger.l('read gift file : $ROOM_GIFT_JSON, gift : ${giftEntity.toJson()}');
      return giftEntity;
    }

    // 本地没数据时请求网络
    logger.v('local gift package is null');

    // 从网络取数据
    giftEntity = await _requestRoomGift(context);

    // 把网络数据保存到本地
    _writeRoomGift(giftEntity);

    return giftEntity;
  }

  /// 把数据写到文件
  static Future<void> _writeRoomGift(YBDGiftPackageEntity? giftPackageEntity) async {
    try {
      if (giftPackageEntity?.returnCode == Const.HTTP_SUCCESS) {
        logger.l('write room gift to json file : $ROOM_GIFT_JSON, gift : ${giftPackageEntity!.toJson()}');

        // 把房间礼物数据保存到本地 json 文件
        YBDFileUtil.writeJsonToFile(json.encode(giftPackageEntity.toJson()), ROOM_GIFT_JSON);
      } else {
        logger.w('gift package data code : ${giftPackageEntity?.returnCode}');
      }
    } catch (e) {
      logger.e('write room gift data error : $e');
    }
  }

  /// 从网络刷新数据
  static Future<void> _refreshRoomGift(BuildContext context) async {
    logger.v('before query gift package : $context');
    YBDGiftPackageEntity? giftEntity = await YBDRoomApiHelper.queryGiftPackage(context);
    logger.l('after query gift package');
    _writeRoomGift(giftEntity);
    return;
  }

  /// 取网络数据
  static Future<YBDGiftPackageEntity?> _requestRoomGift(BuildContext context) async {
    YBDGiftPackageEntity? giftEntity = await YBDRoomApiHelper.queryGiftPackage(context);

    if (giftEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.l('query gift package success');
      return giftEntity;
    }

    logger.w('queryGiftPackage error : ${giftEntity?.returnCode}');
    return null;
  }
}
