

import '../constant/const.dart';
import '../http/environment_ybd_config.dart';

// final Room_Socket_Host_Dev = "ws://${YBDEnvConfig.currentConfig(isDevSocket: true).testUrl}:13999/websocket";

final Room_Socket_Host_Prd = "ws://www.oyetalk.live:13999/websocket";

Duration Keep_Live_Interval = Duration(seconds: 30);

Duration Socket_Request_Time_Out = Duration(seconds: 15);
