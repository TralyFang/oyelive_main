



import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

class YBDRoomUserList extends JsonConvert<YBDRoomUserList> {
  /// 总人数
  int? totalCount;

  /// 游客数量
  int? visitorCount;

  /// 管理员人数
  int? managerCount;

  /// 房间在线用户列表
  List<YBDUserInfo?>? users;

  /// 房间管理员
  List<YBDUserInfo?>? managers;
}
