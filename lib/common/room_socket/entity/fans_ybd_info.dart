import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDFansInfo extends JsonConvert<YBDFansInfo> {
  int? senderId;

  String? nickname;

  int? level;

  String? img;

  int? score;

  /// 0.unknown, 1.female, 2.male
  int? sex;


  int? status;

  int? agency;

  int? vip;

  String? vipIcon;
}
