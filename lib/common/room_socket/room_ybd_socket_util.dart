





import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:geolocator/geolocator.dart';
// import 'package:imei_plugin/imei_plugin.dart';
import 'package:package_info/package_info.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/http/environment_ybd_config.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';

import '../../module/user/util/user_ybd_util.dart';
import '../../ui/page/room/service/live_ybd_service.dart';
import '../constant/const.dart';
import '../util/log_ybd_util.dart';
import '../util/sp_ybd_util.dart';
import 'message/base/message.dart';
import 'message/base/send_ybd_with_resp.dart';
import 'message/common/heart.dart';
import 'message/resp/response.dart';
import 'message/room_ybd_message_helper.dart';
import 'room_ybd_socket_api.dart';
import 'room_ybd_socket_config.dart';

final SOCKET_TAG = "==================On Room Socket Action================\r\n";
enum RoomSocketStatus { TryConnecting, Connected, Close }

class YBDRoomSocketUtil {
  static YBDRoomSocketUtil? _socketUtil;
  IOWebSocketChannel? _channel;
  Timer? socketKeeper;
  Timer? socketPingRoutine;
  bool shouldReconnect = true;

  String? redirectUrl;

  int sendHeartCount = 0;
  int receiveHeartCount = 0;
  final maxHeartOffset = 5;
//===========
  String? imei;

  String? versionName;
  int? userId;
  String? cookie;
  int? portal;

  double longitude = 0;
  double latitude = 0;
  int? roomId;

  int? curSequence = -1;

  RoomSocketStatus roomSocketStatus = RoomSocketStatus.TryConnecting;

  //============
  StreamController _socketStatusStreamController = StreamController<RoomSocketStatus>.broadcast();

  StreamSink<RoomSocketStatus> get _inAdd_socketStatus => _socketStatusStreamController.sink as StreamSink<RoomSocketStatus>;

  Stream<RoomSocketStatus> get socketStatusOutStream => _socketStatusStreamController.stream as Stream<RoomSocketStatus>;

  //============
  static YBDRoomSocketUtil getInstance() {
    if (_socketUtil == null) {
      logger.v('socket util init');
      _socketUtil = YBDRoomSocketUtil();
    }
    return _socketUtil!;
  }

  getSocketHost() async {
    if (redirectUrl != null) {
      return redirectUrl;
    }
    dynamic isTestEnv = await YBDSPUtil.get(Const.SP_TEST_ENV);

    if (isTestEnv == null) {
      return Const.TEST_ENV ? YBDEnvConfig.currentConfig(isDevSocket: true).getRoomSocket() : Room_Socket_Host_Prd;
    } else {
      return isTestEnv as bool ? YBDEnvConfig.currentConfig(isDevSocket: true).getRoomSocket() : Room_Socket_Host_Prd;
    }
  }

  Future<Position?> getLocation() async {
    return null;
    Position? result;

    LocationPermission locationPermission = await Geolocator.checkPermission();
    switch (locationPermission) {
      case LocationPermission.denied:
        await Geolocator.requestPermission();
        try {
          result = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
        } catch (e) {
          logger.e('get current position error : $e');
        }
        break;
      case LocationPermission.deniedForever:
        // TODO: Handle this case.
        break;
      case LocationPermission.whileInUse:
        // TODO: Handle this case.

        break;
      case LocationPermission.always:
        // TODO: Handle this case.
        break;
    }
    return result;
  }

  Future<IOWebSocketChannel?> initSocket() async {
    userId = await YBDUserUtil.userIdInt();
    cookie = await YBDSPUtil.get(Const.SP_COOKIE_APP);
    portal = (await YBDSPUtil.get(Const.SP_PORTAL_TYPE)) ?? 201;
    YBDRoomSocketApi.getInstance(Id: userId);
    longitude = 0;
    latitude = 0;
    Position? position = await getLocation();
    if (position != null) {
      latitude = position.latitude;
      longitude = position.longitude;
    }

    imei = "0";
    // imei = await ImeiPlugin.getImei() ?? "0";

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    versionName = packageInfo.version;
    logger.v('socket connect auth channel: $_channel  ${YBDRoomSocketUtil.getInstance().roomId}'.subLog(step: 100));
    if (_channel == null) {
      changeConnectStatus(RoomSocketStatus.TryConnecting);
      logger.i("$SOCKET_TAG================roomsocket init===============");
      _channel = IOWebSocketChannel.connect(await getSocketHost(), pingInterval: Duration(seconds: 10), headers: {});
      _channel!.stream.listen((message) async {
        Map jsonMessage = json.decode(message);
        logger.v("$SOCKET_TAG roomsocket:-- message \n ${jsonMessage['msgType']} \n ");
        log("$SOCKET_TAG roomsocket:-- message \n $message \n ");

        e(message);
        changeConnectStatus(RoomSocketStatus.Connected);
        YBDRoomMessageHelper.getInstance().receiveMessage(jsonMessage);
      }, onDone: () {
        _channel = null;
        logger.i("$SOCKET_TAG roomsocket:-- disconnect");
      }, onError: (error) {
        _channel = null;
        logger.e("$SOCKET_TAG roomsocket:-- error$error");
      }, cancelOnError: true);

      //auth

    }
    await Future.delayed(Duration(seconds: 2), () async {
      await YBDRoomSocketApi.getInstance().auth(
          randomKey: cookie,
          client: portal,
          latitude: latitude,
          longitude: longitude,
          version: versionName,
          unique: imei,
          onSuccess: (Response data) async {
            logger.i("$SOCKET_TAG roomsocket:-- success!x!");
            if (data.code == Const.HTTP_SUCCESS) {
              changeConnectStatus(RoomSocketStatus.Connected);
              logger.i("$SOCKET_TAG roomsocket:-- success!!");

              // ResubscribeRoom
              logger.v('socket connect auth success and subscribe roomId: $roomId'.subLog(step: 100));
              if (roomId != null) {
                YBDRoomSocketApi.getInstance().subscribe(
                  roomId,
                  protectData: YBDLiveService.instance.data.protectData,
                );
              }

              if ('${YBDCommonUtil.storeFromContext()!.state.bean!.id}' == roomId.toString()) {
                logger.i('retry start broadcast result : ${data.code}');
                // 重新开播
                YBDRoomSocketApi.getInstance().startBroadcastTitle(
                  YBDLiveService.instance.data.roomId,
                  screen: 2,
                  title: YBDLiveService.instance.data.roomTitle,
                  channel: YBDLiveService.instance.data.roomCategory,
                  protectMode: YBDLiveService.instance.data.protectMode,
                  protectData: YBDLiveService.instance.data.protectData,
                  onSuccess: (Response data) {
                    if (Const.TEST_ENV) {
                      YBDToastUtil.toast('retry start broadcast result');
                    }
                    logger.i('retry start broadcast result : ${data.code}');
                  },
                );
              }
            } else if (data.code == "900009" || data.code == "900010") {
              redirectUrl = data.result;

              changeConnectStatus(RoomSocketStatus.TryConnecting);

              destorySocket();
              logger.i("$SOCKET_TAG roomsocket:-- redirect");

              initSocket();
            } else {}
          },
          onTimeOut: () {});
    });
    return _channel;
  }

  e(String msg) {
    int strLength = msg.length;
    int start = 0;
    int end = 2000;
    for (int i = 0; i < 100; i++) {
      //剩下的文本还是大于规定长度则继续重复截取并输出
      if (strLength > end) {
        logger.v(msg);
        start = end;
        end = end + 2000;
      } else {
        logger.v(msg.substring(start, strLength));
        break;
      }
    }
  }

  inRoom(int? roomId) {
    this.roomId = roomId;
  }

  outRoom() {
    logger.v("unsubscribe outRoom null before roomId: $roomId,");
    this.roomId = null;
  }

  setPingRoutine(Duration duration) {
    socketPingRoutine?.cancel();
    socketPingRoutine = Timer.periodic(duration, (_) {
      if (sendHeartCount - receiveHeartCount > maxHeartOffset) {
        resetHeart();
        initSocket();
      } else {
        sendHeartCount++;
        sendSocketMessage(YBDHeartBeat()
          ..toUser = userId
          ..fromUser = userId
          ..mode = modeU);
      }
    });
  }

  resetHeart() {
    sendHeartCount = 0;
    receiveHeartCount = 0;
  }

  romovePingRoutine() {
    socketPingRoutine?.cancel();
  }

  sendSocketMessage(YBDMessage message) async {
    logger.v('sendSocketMessage _channel : $_channel, message: $message');

    if (_channel != null) {
      if (message is YBDSendWithResp) {
        YBDSendWithResp sMessage = message;
        if (sMessage.sequence == null) {
          sMessage.sequence = await YBDRoomMessageHelper.getInstance().getSequenceId();
        }
        curSequence = sMessage.sequence;
        YBDRoomMessageHelper.getInstance().addToQueue(message);
      }
      String messageString = json.encode(message.toJsonContent());
      print('22.12.26---encode:$messageString');
      logger.v("$SOCKET_TAG Send message to roomsocket:\n$messageString");
      _channel!.sink.add(messageString);
    }
  }

  setSocketCheckRoutine() {
    if (socketKeeper == null)
      socketKeeper = Timer.periodic(Duration(seconds: 10), (_) {
        if (_channel == null && shouldReconnect) {
          initSocket();
        }
      });
  }

  disconnect() {
    _channel?.sink?.close(status.goingAway);
    _channel = null;
  }

  logOutSocket() {
    logger.v('socket disconnect, util is null');
    disconnect();
    socketKeeper?.cancel();
    _socketUtil = null;
  }

  destorySocket() {
    _channel = null;
  }

  changeConnectStatus(RoomSocketStatus status) {
    if (status != roomSocketStatus) {
      _inAdd_socketStatus.add(status);
      roomSocketStatus = status;
    }
  }
}
