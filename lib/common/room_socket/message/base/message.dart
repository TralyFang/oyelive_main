

//指定用户
final modeU = "u";
//群发
final modeM = "m";

final modeG = 'g';

class YBDMessage {
  String? msgType;
  int? roomId;
  int? fromUser;
  int? toUser;
  String? mode;
  String? destination;

  YBDMessage({this.msgType, this.roomId, this.fromUser, this.toUser, this.mode, this.destination});

  toJsonContent() {
    return {
      "msgType": msgType,
      "roomId": roomId,
      "fromUser": fromUser,
      "toUser": toUser,
      "mode": mode,
      "destination": destination
    };
  }

  YBDMessage? fromMap(Map json) {
    msgType = json['msgType'];
    roomId = json['roomId'];
    fromUser = json['fromUser'];
    toUser = json['toUser'];
    mode = json['mode'];
    destination = json['destination'];
    return this;
  }
}
