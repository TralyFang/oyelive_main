

import 'package:oyelive_main/common/room_socket/message/base/message.dart';

class YBDDisplayMessage extends YBDMessage {
  /**
   * The sender's nickname
   */
  String? sender;
  /**
   * 发送人头像
   */
  String? senderImg;

  /**
   * 发送人等级
   */
  int? senderLevel;

  /**
   * 发送人vip编码
   */
  int? senderVip;

  /**
   * 发送人sex
   */
  int? senderSex;

  /**
   * 接收人昵称
   */
  String? receiver;

  /**
   * 接收人头像
   */
  String? receiverImg;

  /**
   * 接收人等级
   */
  int? receiverLevel;

  int? receiverSex;

  /**
   * 接收人vip编码
   */
  int? receiverVip;

  String? time;

  YBDDisplayMessage({
    this.sender,
    this.senderImg,
    this.senderLevel,
    this.senderVip,
    this.senderSex,
    this.receiver,
    this.receiverImg,
    this.receiverLevel,
    this.receiverSex,
    this.receiverVip,
    this.time,
  });
}
