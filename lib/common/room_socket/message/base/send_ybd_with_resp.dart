

import 'dart:convert';

import '../../../../generated/json/base/json_convert_content.dart';

import 'message.dart';

/**
 * T,发送的消息范型， E 接收的消息类型
 */
class YBDSendWithResp<T, E> extends YBDMessage {
  //不用填，自动生成
  int? sequence;
  //参考原生那边的ID对应
  int? commandId;
  //发送的消息
  T? message;
  //成功匹配消息的回调
  Function? onSuccess;
  //超时的回调
  Function? onTimeOut;
  //不用填自动会生成
  DateTime? sendTime;

  Map? extra;
  YBDSendWithResp(
      {this.sequence,
      this.onSuccess(E data)?,
      this.onTimeOut,
      this.sendTime,
      this.commandId,
      this.message,
      int? roomId,
      var fromUser,
      var toUser,
      String? mode,
      String? destination,
      this.extra})
      : super(
            msgType: "command",
            roomId: roomId,
            fromUser: fromUser,
            toUser: toUser,
            mode: mode,
            destination: destination);

  callSuccess(json) {
    return onSuccess?.call(JsonConvert.fromJsonAsT<E>(json));
  }

  @override
  Map toJsonContent() {
    // TODO: implement toJson
    Map olderJson = super.toJsonContent();

    Map newJson = {"sequence": sequence, "commandId": commandId};
    newJson.addAll(olderJson);
    if (extra != null) {
      newJson.addAll(extra!);
    }
    try {
      if (message != null) {
        newJson.addAll((message as JsonConvert<T>?)?.toJson() ?? {});
      }
      newJson.removeWhere((key, value) => value == null);
      print("Send message:$newJson");
      return newJson;
    } catch (e) {
      print(e);
      newJson.removeWhere((key, value) => value == null);
      return newJson;
    }
  }
  void toJsonContentlLOQDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
