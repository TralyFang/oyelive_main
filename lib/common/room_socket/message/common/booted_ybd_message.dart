

/// 被踢的消息
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDBootedMessage extends JsonConvert<YBDBootedMessage> implements YBDMessage {
  String? code;
  String? desc;
  String? detail;
  String? msgType;
  int? roomId;
  int? fromUser;
  int? toUser;
  String? mode;
  String? destination;

  YBDBootedMessage fromMap(Map json) {
    return fromMap(json);
  }

  toJsonContent() {
    return toJson();
  }

  YBDBootedMessage();
}
