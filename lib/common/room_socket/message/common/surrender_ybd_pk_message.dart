

/*
 * @Author: William-Zhou
 * @Date: 2022-02-17 17:21:10
 * @LastEditTime: 2022-02-17 17:30:38
 * @LastEditors: William-Zhou
 * @Description: surrender PK YBDMessage:pkId  sourceUserId  type
 */
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDSurrenderPKMessage extends JsonConvert<YBDSurrenderPKMessage> {
  int? pkId;
  int? sourceUserId;
  // TypeEnum  none(0),endBeforehand(1)
  int? type;
}
