


import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDLudoStatusMessageEntity extends JsonConvert<YBDLudoStatusMessageEntity> implements YBDPublish {
	YBDLudoStatusMessageContent? content;

  @override
  String? destination;

  @override
  int? fromUser;

  @override
  String? mode;

  @override
  String? msgType;

  @override
  int? roomId;

  @override
  int? toUser;

  @override
  int? type;

  @override
  YBDMessage fromMap(Map json) {
    // TODO: implement fromMap
    throw UnimplementedError();
  }

  @override
  toJsonContent() {
    // TODO: implement toJsonContent
    throw UnimplementedError();
  }
}

class YBDLudoStatusMessageContent with JsonConvert<YBDLudoStatusMessageContent> {
	YBDLudoStatusMessagePk? pk;
	YBDLudoStatusMessageLudo? ludo;
  YBDLudoStatusMessageLudo? gameRoom;
}

class YBDLudoStatusMessagePk with JsonConvert<YBDLudoStatusMessagePk> {
	int? roomId;
	int? userId;
	bool? gaming;
}

class YBDLudoStatusMessageLudo with JsonConvert<YBDLudoStatusMessageLudo> {
	int? roomId;
	int? userId;
	bool? gaming;
	/// 以下游戏房字段
	String? category;
	String? subCategory;
}
