



import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/gift.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDGlobal extends JsonConvert<YBDGlobal> implements YBDDisplayMessage {
  String? contentType;

  String? messageId;

  YBDGift? content;

  YBDGlobal({msgType: "global"}) : super();

  // @override
  YBDGlobal? fromMap(Map json) {
    // TODO: implement fromMap
    return fromJson(json as Map<String, dynamic>);
  }

  // @override
  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }

  @override
  String? destination;

  @override
  int? fromUser;

  @override
  String? mode;

  @override
  String? msgType;

  @override
  String? receiver;

  @override
  String? receiverImg;

  @override
  int? receiverLevel;

  @override
  int? receiverSex;

  @override
  int? receiverVip;

  @override
  int? roomId;

  @override
  String? sender;

  @override
  String? senderImg;

  @override
  int? senderLevel;

  @override
  int? senderSex;

  @override
  int? senderVip;

  @override
  String? time;

  @override
  int? toUser;

  /// 根据socket消息传过来的大礼物消息生成新的大礼物对象
  static copyWith(YBDGlobal value) {
    YBDGlobal global = new YBDGlobal();
    global.roomId = value.roomId;
    global.content = new YBDGift();
    global.content!.imgUrl = value.content!.imgUrl;
    global.content!.fromUser = value.content!.fromUser;
    global.content!.sender = value.content!.sender;
    global.content!.senderImg = value.content!.senderImg;
    global.content!.receiver = value.content!.receiver;
    global.content!.receiverImg = value.content!.receiverImg;
    global.content!.toUser = value.content!.toUser;
    global.content!.num = value.content!.num;
    global.content!.roomId = value.content!.roomId;
    global.content!.name = value.content!.name;

    return global;
  }
}
