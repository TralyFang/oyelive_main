


import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/common/room_socket/message/common/room_ybd_theme.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

/// YBDRoomBgPublish 消息，查询房间在线人数时用到
class YBDRoomBgPublish extends JsonConvert<YBDRoomBgPublish> implements YBDPublish {
  /// YBDMessage 类里的字段
  String? msgType;
  int? roomId;
  int? fromUser;
  int? toUser;
  String? mode;
  String? destination;

  /// YBDPublish 类里的字段
  int? type;

  /// YBDRoomBgPublish 类里的字段
  YBDRoomThemeContent? content;

  YBDRoomBgPublish? fromMap(Map json) {
    return fromJson(json as Map<String, dynamic>);
  }

  toJsonContent() {
    return toJson();
  }
}
