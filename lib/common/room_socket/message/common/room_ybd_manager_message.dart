

import '../../../../generated/json/base/json_convert_content.dart';

/// 设置或取消房间管理员的消息
class YBDRoomManagerMessage with JsonConvert<YBDRoomManagerMessage> {
  /// 1: promote an user to be room manager; 2: demote room manager
  int? operateType;
}
