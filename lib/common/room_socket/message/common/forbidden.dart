

import '../../../../generated/json/base/json_convert_content.dart';

class YBDForbidden with JsonConvert<YBDForbidden> {
  /// operType 1: mute for 5m  2: boot for 1h  -1: forever
  int? operType;
  int? period;
}
