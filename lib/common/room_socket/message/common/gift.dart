import 'dart:convert';

import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDGift extends JsonConvert<YBDGift> implements YBDDisplayMessage {
  int? code;
  String? name;
  int? num;
  String? imgUrl;
  String? animationUrl;

  String? animation;
  String? senderVipIcon;

  int? price;

  int? combo;
  int? comboId;

  String? customized; // jsonString

  // 根据jsonString获取实例，可能为空
  YBDGiftCustomized? customizedEntity() {
    if (customized != null) {
      try {
        Map? jsonMap = jsonDecode(customized!);
        var entity = YBDGiftCustomized().fromJson(jsonMap as Map<String, dynamic>);
        return entity;
      } catch (e) {
        logger.e("customizedEntity error: $e");
      }
    }
    return null;
  }

  YBDGift({msgType: "gift"}) : super();

  // @override
  String? destination;

  // @override
  int? fromUser;

  // @override
  String? mode;

  // @override
  String? msgType;

  // @override
  String? receiver;

  // @override
  String? receiverImg;

  // @override
  int? receiverLevel;

  // @override
  int? receiverSex;

  // @override
  int? receiverVip;

  // @override
  int? roomId;

  // @override
  String? sender;

  // @override
  String? senderImg;

  // @override
  int? senderLevel;

  // @override
  int? senderSex;

  // @override
  int? senderVip;

  // @override
  String? time;

  // @override
  int? toUser;

  // @override
  YBDGift? fromMap(Map json) {
    // TODO: implement fromMap
    return fromJson(json as Map<String, dynamic>);
  }

  // @override
  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}

class YBDGiftCustomized extends JsonConvert<YBDGiftCustomized> {
  List<YBDGiftCustomizedData?>? data;
  String? type; // customized1两个头像

  bool doubleAvatarType() {
    return type == "customized1";
  }
  void doubleAvatarTypehZ9Bxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDGiftCustomizedData extends JsonConvert<YBDGiftCustomizedData> {
  String? imgKey; // 指定替换的图片key
  String? imgPath; // 图片网络路径
}