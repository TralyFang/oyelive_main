


/*
 * @Date: 2021-07-13 17:16:57
 * @LastEditors: William-Zhou
 * @LastEditTime: 2021-07-26 15:18:59
 * @FilePath: \oyetalk_flutter\lib\common\room_socket\message\common\pk_invite_pub.dart
 */
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDPKInvitePub extends JsonConvert<YBDPKInvitePub> implements YBDPublish {
  List<dynamic>? costTime;
  YBDPKInviteContent? content;
  int? version;
  @override
  String? destination;

  @override
  int? fromUser;

  @override
  String? mode;

  @override
  String? msgType;

  @override
  int? roomId;

  @override
  int? toUser;
//* 12是邀请  13是开始匹配
  @override
  int? type;

  @override
  YBDMessage? fromMap(Map json) {
    // TODO: implement fromMap
    // throw UnimplementedError();
    return fromJson(json as Map<String, dynamic>);
  }

  @override
  toJsonContent() {
    // TODO: implement toJsonContent
    // throw UnimplementedError();
    return toJson();
  }
}

class YBDPKInviteContent extends JsonConvert<YBDPKInviteContent> {
  int? source;
}
