


import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDRoomManagerDisplayMessage extends JsonConvert<YBDRoomManagerDisplayMessage> implements YBDDisplayMessage {
  YBDRoomManagerDisplayMessage({msgType: "room_manager"}) : super();

  /// 1 - add someone as a room manager ; 2 - remove room manager
  int? operateType;
  // @override
  String? destination;

  // @override
  int? fromUser;

  // @override
  String? mode;

  // @override
  String? msgType;

  // @override
  String? receiver;

  // @override
  String? receiverImg;

  // @override
  int? receiverLevel;

  // @override
  int? receiverSex;

  // @override
  int? receiverVip;

  // @override
  int? roomId;

  // @override
  String? sender;

  // @override
  String? senderImg;

  // @override
  int? senderLevel;

  // @override
  int? senderSex;

  // @override
  int? senderVip;

  // @override
  String? time;

  // @override
  int? toUser;

  // @override
  YBDRoomManagerDisplayMessage? fromMap(Map json) {
    // TODO: implement fromMap
    return fromJson(json as Map<String, dynamic>);
  }

  // @override
  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}
