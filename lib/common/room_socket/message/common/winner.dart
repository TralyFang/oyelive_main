



import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

class YBDWinner extends JsonConvert<YBDWinner> implements YBDDisplayMessage {
  String? destination;

  int? fromUser;

  String? mode;

  String? msgType;

  String? receiver;

  String? receiverImg;

  int? receiverLevel;

  int? receiverSex;

  int? receiverVip;

  int? roomId;

  String? sender;

  String? senderImg;

  int? senderLevel;

  int? senderSex;

  int? senderVip;

  String? time;

  int? toUser;

  YBDUserInfo? user;

  int? prizeCoins;
  YBDMessage fromMap(Map json) {
    // TODO: implement fromMap
    throw UnimplementedError();
  }

  toJsonContent() {
    // TODO: implement toJsonContent
    throw UnimplementedError();
  }
}
