


import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDInternal extends JsonConvert<YBDInternal> implements YBDDisplayMessage {
  dynamic content;

  /**
   * 操作类型 1-禁言 2-踢人 3-黑名单 4-公告 mode-m:“某某人被禁言”的消息 u:提示被禁言/被踢/已被加入黑名单
   */
  int? type;

  /**
   * 禁言/被踢截止时间。
   */
  int? endTime;

  dynamic record;

  String? destination;

  int? fromUser;

  String? mode;

  String? msgType;

  String? receiver;

  String? receiverImg;

  int? receiverLevel;

  int? receiverSex;

  int? receiverVip;

  int? roomId;

  String? sender;

  String? senderImg;

  int? senderLevel;

  int? senderSex;

  int? senderVip;

  String? time;

  int? toUser;

  bool? playingLudo; // 是否正在ludo游戏中

  YBDInternal fromMap(Map json) {
    // TODO: implement fromMap
    return fromMap(json);
  }

  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}
