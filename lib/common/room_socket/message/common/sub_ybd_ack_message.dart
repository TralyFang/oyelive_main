


import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

/// 订阅成功的消息
class YBDSubAckMessage extends JsonConvert<YBDSubAckMessage> implements YBDMessage {
  String? code;
  String? desc;
  String? msgType;
  int? roomId;
  int? fromUser;
  int? toUser;
  String? mode;
  String? destination;

  YBDSubAckMessage fromMap(Map json) {
    return fromMap(json);
  }

  toJsonContent() {
    return toJson();
  }

  YBDSubAckMessage();
}
