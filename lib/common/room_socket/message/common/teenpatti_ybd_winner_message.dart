


import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/common/room_socket/message/common/winner.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDPublishTeenPattiWinnerMessage extends JsonConvert<YBDPublishTeenPattiWinnerMessage> implements YBDPublish {
//    int type;
  List<YBDWinner?>? content;

  String? destination;

  int? fromUser;

  String? mode;

  String? msgType;

  int? roomId;

  int? toUser;

  int? type;

  YBDPublishTeenPattiWinnerMessage? fromMap(Map json) {
    // TODO: implement fromMap
    return fromJson(json as Map<String, dynamic>);
  }

  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}
