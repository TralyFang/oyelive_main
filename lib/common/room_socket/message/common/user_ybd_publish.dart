


import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

/// YBDUserPublish 消息，查询房间在线人数时用到
class YBDUserPublish extends JsonConvert<YBDUserPublish> implements YBDPublish {
  /// YBDMessage 类里的字段
  String? msgType;
  int? roomId;
  int? fromUser;
  int? toUser;
  String? mode;
  String? destination;

  /// YBDPublish 类里的字段
  int? type;

  /// YBDUserPublish 类里的字段
  YBDUserListInfo? content;

  YBDUserPublish? fromMap(Map json) {
    return fromJson(json as Map<String, dynamic>);
  }

  toJsonContent() {
    return toJson();
  }
}

class YBDUserListInfo with JsonConvert<YBDUserListInfo> {
  ///  总人数
  int? totalCount;

  /// 游客数量
  int? visitorCount;

  /// 在线数量
  int? onlineCount;

  /// 管理员人数
  int? managerCount;
  List<YBDUserInfo?>? users;
  List<YBDUserInfo?>? managers;
}
