


import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';

class YBDTextMessage extends JsonConvert<YBDTextMessage> implements YBDDisplayMessage {
  String? content;

  YBDTextMessage({this.content, msgType = "text"}) : super();

  // @override
  String? destination;

  // @override
  int? fromUser;

  // @override
  String? mode;

  // @override
  String? msgType;

  // @override
  String? receiver;

  // @override
  String? receiverImg;

  // @override
  int? receiverLevel;

  // @override
  int? receiverSex;

  // @override
  int? receiverVip;

  // @override
  int? roomId;

  // @override
  String? sender;

  // @override
  String? senderImg;

  // @override
  int? senderLevel;

  // @override
  int? senderSex;

  // @override
  int? senderVip;

  String? senderVipIcon;

  // @override
  String? time;

  // @override
  int? toUser;

  String? equipGoods;

  List<YBDUserCertificationRecordCertificationInfos?>? senderCerInfos;

  // @override
  YBDTextMessage? fromMap(Map json) {
    // TODO: implement fromMap
    return fromJson(json as Map<String, dynamic>);
  }

  // @override
  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}
