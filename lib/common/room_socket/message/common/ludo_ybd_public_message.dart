


import 'package:oyelive_main/common/room_socket/message/base/message.dart';

class YBDLudoPublicMessage extends YBDMessage {
  List<YBDLudoPublicContent>? content;

  YBDLudoPublicMessage({this.content});

  YBDLudoPublicMessage.fromJson(Map<String, dynamic> json) {
    super.fromMap(json);
    if (json['content'] != null) {
      content = <YBDLudoPublicContent>[];
      json['content'].forEach((v) {
        content!.add(YBDLudoPublicContent.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data.addAll(super.toJsonContent());
    if (this.content != null) {
      data['content'] = this.content!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class YBDLudoPublicContent extends YBDMessage {
  String? userName;
  String? userColor;
  String? gameName;
  String? gameColor;
  String? revenueColor;
  int? revenue;
  String? currency;
  String? currencyColor;
  int? type;
  String? normalText;
  String? normalColor;
  String? link;
  String? linkColor;
  String? linkName;

  YBDLudoPublicContent(
      {this.userName,
        this.userColor,
        this.gameName,
        this.gameColor,
        this.revenueColor,
        this.revenue,
        this.currency,
        this.currencyColor,
        this.type,
        this.normalText,
        this.normalColor,
        this.link,
        this.linkColor,
        this.linkName});

  YBDLudoPublicContent.fromJson(Map json) {
    userName = json['userName'];
    userColor = json['userColor'];
    gameName = json['gameName'];
    gameColor = json['gameColor'];
    revenueColor = json['revenueColor'];
    // revenue = json['revenue'];
    if (json['revenue'] != null) {
      revenue = json['revenue'] is String
          ? int.tryParse(json['revenue'])
          : json['revenue'].toInt();
    }
    currency = json['currency'];
    currencyColor = json['currencyColor'];
    // type = json['type'];
    if (json['type'] != null) {
      type = json['type'] is String
          ? int.tryParse(json['type'])
          : json['type'].toInt();
    }
    normalText = json['normalText'];
    normalColor = json['normalColor'];
    link = json['link'];
    linkColor = json['linkColor'];
    linkName = json['linkName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userName'] = this.userName;
    data['userColor'] = this.userColor;
    data['gameName'] = this.gameName;
    data['gameColor'] = this.gameColor;
    data['revenueColor'] = this.revenueColor;
    data['revenue'] = this.revenue;
    data['currency'] = this.currency;
    data['currencyColor'] = this.currencyColor;
    data['type'] = this.type;
    data['normalText'] = this.normalText;
    data['normalColor'] = this.normalColor;
    data['link'] = this.link;
    data['linkColor'] = this.linkColor;
    data['linkName'] = this.linkName;
    return data;
  }

}
