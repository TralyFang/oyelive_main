

import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDEnter extends JsonConvert<YBDEnter> implements YBDDisplayMessage {
  String? destination;

  int? fromUser;

  String? mode;

  String? msgType;

  String? receiver;

  String? receiverImg;

  int? receiverLevel;

  int? receiverSex;

  int? receiverVip;

  int? roomId;

  String? sender;

  String? senderImg;

  int? senderLevel;

  int? senderSex;

  int? senderVip;

  String? time;
  String? senderVipIcon;


  int? toUser;

  String? carName;
  String? carImg;
  String? carAnimation;
  String? carExtend;

  /// [YBDCarListRecordExtend]json字符串 座驾的svga图片替换的扩展参数
  int? carId;

  String? userLevelEnterTip;
  String? userEnterMedal; // VIP勋章

  YBDEnter? fromMap(Map json) {
    // TODO: implement fromMap
    return fromJson(json as Map<String, dynamic>);
  }

  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}
