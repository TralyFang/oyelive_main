

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDTargetPkOpMessage extends JsonConvert<YBDTargetPkOpMessage> {
  int? type;
  //拒绝时区
  int? source;
  int? target;
  String? timeZone;
  bool? rejectFriends;
}
