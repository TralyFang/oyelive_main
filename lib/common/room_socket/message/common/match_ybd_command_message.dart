

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDMatchCommandMessage extends JsonConvert<YBDMatchCommandMessage> {
  // type=1时,开始匹配,source开始者
  //
  // type=2时,接收匹配,source为发出邀请者,target为接收邀请者
  //
  // type=3时,拒绝匹配,source为发出邀请者,target为拒绝邀请者
  //
  // type=4时,停止匹配,source为停止者
  //
  // type 类型定义 start(1), accept(2), refuse(3), stop(4),
  int? type;
  int? source;
  int? target;
}
