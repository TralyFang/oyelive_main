


import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDTargetPkMessage extends JsonConvert<YBDTargetPkMessage> implements YBDPublish {
  @override
  String? destination;

  @override
  int? fromUser;

  @override
  String? mode;

  @override
  String? msgType;

  @override
  int? roomId;

  @override
  int? toUser;

  @override
  int? type;

  YBDTargetContent? content;

  @override
  YBDMessage? fromMap(Map json) {
    // TODO: implement fromMap
    return fromJson(json as Map<String, dynamic>);
  }

  @override
  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}

class YBDTargetContent extends JsonConvert<YBDTargetContent> {
  String? sourceNickName;
  String? sourceHeadImg;
  String? targetNickName;
  String? targetHeadImg;
  int? target;
  int? source;
}
