

import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDMicRequestMessage with JsonConvert<YBDMicRequestMessage> implements YBDDisplayMessage {
  //麦下标
  int? index;

  //1 -请求上麦 2 -邀请上麦 3 - 同意上麦请求  4 - 同意 邀请上麦请求  50-主播同意请求成功  51-主播请求同意请求失败 ：用户不在房间内   52-主播请求同意请求失败 ：MIC位已经被占用
  int? type;

  String? destination;

  int? fromUser;

  String? mode;

  String? msgType;

  String? receiver;

  String? receiverImg;

  int? receiverLevel;

  int? receiverSex;

  int? receiverVip;

  int? roomId;

  String? sender;

  String? senderImg;

  int? senderLevel;

  int? senderSex;

  int? senderVip;

  String? time;

  int? toUser;

  YBDMessage fromMap(Map json) {
    // TODO: implement fromMap
    throw UnimplementedError();
  }

  toJsonContent() {
    // TODO: implement toJsonContent
    throw UnimplementedError();
  }
}
