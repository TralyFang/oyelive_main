
import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';

class YBDStartLiveMsg implements YBDDisplayMessage {
  String? code;

  String? desc;

  @override
  String? destination;

  @override
  int? fromUser;

  @override
  String? mode;

  @override
  String? msgType;

  @override
  String? receiver;

  @override
  String? receiverImg;

  @override
  int? receiverLevel;

  @override
  int? receiverSex;

  @override
  int? receiverVip;

  @override
  int? roomId;

  @override
  String? sender;

  @override
  String? senderImg;

  @override
  int? senderLevel;

  @override
  int? senderSex;

  @override
  int? senderVip;

  @override
  String? time;

  @override
  int? toUser;

  @override
  YBDMessage fromMap(Map json) {
    // TODO: implement fromMap
    throw UnimplementedError();
  }

  @override
  toJsonContent() {
    // TODO: implement toJsonContent
    throw UnimplementedError();
  }
}
