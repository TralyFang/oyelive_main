


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

/// YBDRoomTheme 内容
class YBDRoomTheme extends JsonConvert<YBDRoomTheme> {
  int? id;
  String? name;
  int? status;
  int? lable;
  int? index;
  int? price;
  String? image;
  int? display;
  String? attribute;
  int? personalId;
  int? expireAfter;


  YBDRoomTheme? fromMap(Map json) {
    return fromJson(json as Map<String, dynamic>);
  }

  toJsonContent() {
    return toJson();
  }
}

class YBDRoomThemeContent extends JsonConvert<YBDRoomThemeContent> {
  YBDRoomTheme? theme;


  YBDRoomThemeContent? fromMap(Map json) {
    return fromJson(json as Map<String, dynamic>);
  }

  toJsonContent() {
    return toJson();
  }
}

