


import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDPkPublish extends JsonConvert<YBDPkPublish> implements YBDPublish {
  List<dynamic>? costTime;

  YBDPkInfoContent? content;
  int? version;

  @override
  String? destination;

  @override
  int? fromUser;

  @override
  String? mode;

  @override
  String? msgType;

  @override
  int? roomId;

  @override
  int? toUser;

  @override
  int? type;

  @override
  YBDMessage? fromMap(Map json) {
    // TODO: implement fromMap
    return fromJson(json as Map<String, dynamic>);
  }

  @override
  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }

  revertContentData() {
    print("pkpublish revert content data!");
    //对调数据
    YBDPkInfoContent cloneData = YBDPkInfoContent().clone(content!);
    content!.sourceNickName = cloneData.targetNickName;
    content!.sourceHeadImg = cloneData.targetHeadImg;
    content!.sourceBanMics = cloneData.targetBanMics;
    content!.sourceTopGifts = cloneData.targetTopGifts;
    content!.sourceOnline = cloneData.targetOnline;
    content!.sourceGifts = cloneData.targetGifts;
    content!.sourceUser = cloneData.targetUser;
    content!.sourceRoom = cloneData.targetRoom;
    content!.sourceToken = cloneData.targetToken;

    content!.targetNickName = cloneData.sourceNickName;
    content!.targetHeadImg = cloneData.sourceHeadImg;
    content!.targetBanMics = cloneData.sourceBanMics;
    content!.targetTopGifts = cloneData.sourceTopGifts;
    content!.targetOnline = cloneData.sourceOnline;
    content!.targetGifts = cloneData.sourceGifts;
    content!.targetUser = cloneData.sourceUser;
    content!.targetRoom = cloneData.sourceRoom;
    content!.targetToken = cloneData.sourceToken;
  }
}

class YBDPkInfoContent with JsonConvert<YBDPkInfoContent> {
  int? pkId;
  int? type;
  int? status;
  int? startTime;
  int? endTime;
  int? remainingTime;
  int? sequelType;
  int? sourceUser;
  int? sourceRoom;
  int? sourceGifts;
  bool? sourceOnline;
  List<YBDPkInfoContentSourceTopGifts?>? sourceTopGifts;
  int? targetUser;
  int? targetRoom;
  int? targetGifts;
  bool? targetOnline;
  List<YBDPkInfoContentSourceTopGifts?>? targetTopGifts;
  // a pk b  a禁麦了b(禁止b 流转发) sourceBanMics.contains(b的id) is true  同理 targetBanMics
  List<int>? sourceBanMics;
  List<int>? targetBanMics;
  String? sourceHeadImg;
  String? sourceNickName;
  String? targetHeadImg;
  String? targetNickName;

  String? targetToken;
  String? sourceToken;
  // 获胜者ID
  int? winner;

  YBDPkInfoContent clone(YBDPkInfoContent origin) {
    return YBDPkInfoContent()
      ..pkId = origin.pkId
      ..type = origin.type
      ..status = origin.status
      ..startTime = origin.startTime
      ..endTime = origin.endTime
      ..remainingTime = origin.remainingTime
      ..sequelType = origin.sequelType
      ..sourceUser = origin.sourceUser
      ..sourceRoom = origin.sourceRoom
      ..sourceGifts = origin.sourceGifts
      ..sourceOnline = origin.sourceOnline
      ..sourceTopGifts = origin.sourceTopGifts
      ..targetUser = origin.targetUser
      ..targetRoom = origin.targetRoom
      ..targetGifts = origin.targetGifts
      ..targetOnline = origin.targetOnline
      ..targetTopGifts = origin.targetTopGifts
      ..sourceBanMics = origin.sourceBanMics
      ..targetBanMics = origin.targetBanMics
      ..sourceHeadImg = origin.sourceHeadImg
      ..sourceNickName = origin.sourceNickName
      ..targetHeadImg = origin.targetHeadImg
      ..targetNickName = origin.targetNickName
      ..targetToken = origin.targetToken
      ..sourceToken = origin.sourceToken
      ..winner = origin.winner;
  }
}

class YBDPkInfoContentSourceTopGifts with JsonConvert<YBDPkInfoContentSourceTopGifts> {
  int? consume;
  YBDPkInfoContentSourceTopGiftsUser? user;
}

class YBDPkInfoContentSourceTopGiftsUser with JsonConvert<YBDPkInfoContentSourceTopGiftsUser> {
  int? id;
  dynamic agency;
  dynamic crest;
  String? nickname;
  String? headimg;
  int? level;
  int? roomlevel;
  int? vip;
  dynamic area;
  int? sex;
  dynamic tags;
}
