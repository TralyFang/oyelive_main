import 'package:oyelive_main/common/room_socket/entity/fans_ybd_info.dart';
import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDSuperFansMessage extends JsonConvert<YBDSuperFansMessage> implements YBDDisplayMessage {
  YBDFansContent? content;

  // @override
  String? destination;

  // @override
  int? fromUser;

  // @override
  String? mode;

  // @override
  String? msgType;

  // @override
  String? receiver;

  // @override
  String? receiverImg;

  // @override
  int? receiverLevel;

  // @override
  int? receiverSex;

  // @override
  int? receiverVip;

  // @override
  int? roomId;

  // @override
  String? sender;

  // @override
  String? senderImg;

  // @override
  int? senderLevel;

  // @override
  int? senderSex;

  // @override
  int? senderVip;

  // @override
  String? time;

  // @override
  int? toUser;

  // @override
  YBDSuperFansMessage? fromMap(Map json) {
    return fromJson(json as Map<String, dynamic>);
  }

  // @override
  toJsonContent() {
    return toJson();
  }
}

class YBDFansContent with JsonConvert<YBDFansContent> {
  List<YBDFansInfo?>? fans;
  List<YBDFansInfo?>? superFans;
  List<YBDFansInfo?>? dailyFans;
  List<YBDFansInfo?>? weeklyFans;
  List<YBDFansInfo?>? monthlyFans;
}
