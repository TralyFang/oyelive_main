
/*
 * @Author: William
 * @Date: 2022-12-22 15:23:34
 * @LastEditTime: 2022-12-26 17:15:29
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/room_socket/message/common/emoji_message.dart
 */
import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';

class YBDEmojiMessage extends JsonConvert<YBDEmojiMessage> implements YBDDisplayMessage {
  String? content;

  YBDEmojiMessage({this.content, msgType = "emoji"}) : super();

  // @override
  String? destination;

  // @override
  int? fromUser;

  // @override
  String? mode;

  // @override
  String? msgType;

  // @override
  String? receiver;

  // @override
  String? receiverImg;

  // @override
  int? receiverLevel;

  // @override
  int? receiverSex;

  // @override
  int? receiverVip;

  // @override
  int? roomId;

  // @override
  String? sender;

  // @override
  String? senderImg;

  // @override
  int? senderLevel;

  // @override
  int? senderSex;

  // @override
  int? senderVip;

  // @override
  String? time;

  // @override
  int? toUser;

  String? equipGoods;

  String? senderVipIcon;

  List<YBDUserCertificationRecordCertificationInfos>? senderCerInfos;

  // @override
  YBDEmojiMessage fromMap(Map json) {
    // TODO: implement fromMap
    return fromJson(json as Map<String, dynamic>);
  }

  // @override
  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}
