

import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDAppMessage extends JsonConvert<YBDAppMessage> implements YBDDisplayMessage {
  String? destination;

  int? fromUser;

  String? mode;

  String? msgType;

  String? receiver;

  String? receiverImg;

  int? receiverLevel;

  int? receiverSex;

  int? receiverVip;

  int? roomId;

  String? sender;

  String? senderImg;

  int? senderLevel;

  int? senderSex;

  int? senderVip;

  String? time;

  int? toUser;

  String? content;

  YBDAppMessage fromMap(Map json) {
    // TODO: implement fromMapr
    return fromMap(json);
  }

  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}
