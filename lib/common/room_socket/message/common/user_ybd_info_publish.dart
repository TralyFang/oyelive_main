


import 'package:oyelive_main/common/room_socket/message/base/publish.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

/// 从房间 scoket 获取粉丝总榜数据
class YBDUserInfoPublish extends JsonConvert<YBDUserInfoPublish> implements YBDPublish {
  // YBDMessage 类里的字段
  String? msgType;
  int? roomId;
  int? fromUser;
  int? toUser;
  String? mode;
  String? destination;

  // YBDPublish 类里的字段
  int? type;

  // TODO: jackie - 添加 content 字段
  dynamic content;

  YBDUserInfoPublish? fromMap(Map json) {
    return fromJson(json as Map<String, dynamic>);
  }

  toJsonContent() {
    return toJson();
  }
}
