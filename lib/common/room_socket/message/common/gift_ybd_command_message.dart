

import '../../../../generated/json/base/json_convert_content.dart';

class YBDGiftCommandMessage extends JsonConvert<YBDGiftCommandMessage> {
  String? name;

  int? num;

  String? imgUrl;

  String? animationUrl;

  int? combo;
  int? code;

  int? personalId;

  int? pkId;
}
