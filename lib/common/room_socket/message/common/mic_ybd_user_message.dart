

import '../../../../generated/json/base/json_convert_content.dart';

class YBDMicUserMessage with JsonConvert<YBDMicUserMessage> {

  ///操作类型:1.上麦 2.下麦 3.强制下麦 4.设置为房间master
  int? operType;

  ///目标编号（麦位下标）
  int? index;
}