

import '../../../../generated/json/base/json_convert_content.dart';

class YBDAuth with JsonConvert<YBDAuth> {
  String? randomKey;
  int? client;
  String? version;
  int? protocol;
  String? unique;
  double? latitude;
  double? longitude;
}
