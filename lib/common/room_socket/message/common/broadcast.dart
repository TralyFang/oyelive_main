

import '../../../../generated/json/base/json_convert_content.dart';

class YBDBroadcast with JsonConvert<YBDBroadcast> {
  int? type;
  int? screen;
  int? protectMode;
  String? protectData;
  String? title;
  String? channel;
  String? tag;
  int? micRequestEnable;
}
