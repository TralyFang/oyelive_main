

import 'dart:async';
import 'dart:developer';

import 'package:synchronized/synchronized.dart';
import 'package:oyelive_main/common/room_socket/message/base/send_ybd_with_resp.dart';
import 'package:oyelive_main/common/room_socket/message/common/Global.dart';
import 'package:oyelive_main/common/room_socket/message/common/PkPublish.dart';
import 'package:oyelive_main/common/room_socket/message/common/booted_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/bullet_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/emoji_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/enter.dart';
import 'package:oyelive_main/common/room_socket/message/common/gift.dart';
import 'package:oyelive_main/common/room_socket/message/common/internal.dart';
import 'package:oyelive_main/common/room_socket/message/common/ludo_ybd_public_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/ludo_ybd_status_message_entity.dart';
import 'package:oyelive_main/common/room_socket/message/common/mic_ybd_request_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/room_ybd_manager_display_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/sub_ybd_ack_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/super_ybd_fans_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/target_ybd_pk_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/teenpatti_ybd_winner_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/text_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/unsubscribe.dart';
import 'package:oyelive_main/common/room_socket/message/common/update_ybd_room_bg_publish.dart';
import 'package:oyelive_main/common/room_socket/message/common/user_ybd_info_publish.dart';
import 'package:oyelive_main/common/room_socket/message/common/user_ybd_publish.dart';
import 'package:oyelive_main/common/room_socket/message/resp/response.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_mute_response_message.dart';
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_user_publish.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_settle_result_entity.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';

import '../room_ybd_socket_config.dart';
import 'base/message.dart';
import 'common/follow_ybd_display_message.dart';
import 'common/like_ybd_message.dart';
import 'common/pk_ybd_invite_pub.dart';

class YBDRoomMessageHelper {
//  T buildMessage<T extends YBDMessage>(T msg, int roomId, int from, int to, String mode) {}
  int sequenceId = 0;
  static final _lock = Lock();

  static Timer? _queueChecker;

  static final Duration _checkQueueInterval = Duration(seconds: 1);

  static List<YBDSendWithResp> messageQueue = [];

  static YBDRoomMessageHelper? _helper;

  //=========================
  StreamController _messageStreamController = StreamController<YBDMessage>.broadcast();

  StreamSink<YBDMessage?> get _inAdd_message => _messageStreamController.sink as StreamSink<YBDMessage?>;

  Stream<YBDMessage> get messageOutStream => _messageStreamController.stream as Stream<YBDMessage>;

  static YBDRoomMessageHelper getInstance() {
    if (_helper == null) {
      _helper = YBDRoomMessageHelper();
    }
    if (_queueChecker == null) {
      _queueChecker = Timer.periodic(_checkQueueInterval, (_) {
        _lock.synchronized(() {
          DateTime now = DateTime.now();

          List<YBDSendWithResp> timeOutList = messageQueue
              .where((element) => now.difference(element.sendTime!).inSeconds > Socket_Request_Time_Out.inSeconds)
              .toList();

          timeOutList.forEach((element) {
            element.onTimeOut?.call();
          });
          messageQueue
              .removeWhere((element) => now.difference(element.sendTime!).inSeconds > Socket_Request_Time_Out.inSeconds);
        });
      });
    }

    return _helper!;
  }

  Future<int> getSequenceId() async {
    await _lock.synchronized(() => {++sequenceId});
    return sequenceId;
  }
  void getSequenceIdy1OiWoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future addToQueue(YBDSendWithResp message) async {
    await _lock.synchronized(() {
      if (message.sendTime == null) message.sendTime = DateTime.now();
      messageQueue.add(message);
    });
  }
  void addToQueueqgcuCoyelive(YBDSendWithResp message)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void receiveMessage(Map message) {
    String? msgType = message['msgType'];
    logger.v('receive message type: ${msgType}');
    switch (msgType) {
      case "text":
        _inAdd_message.add(YBDTextMessage().fromJson(message as Map<String, dynamic>));
        break;
      case "emoji":
        _inAdd_message.add(YBDEmojiMessage().fromJson(message as Map<String, dynamic>));
        break;
      case "bullet":
        _inAdd_message.add(YBDBulletMessage().fromJson(message as Map<String, dynamic>));
        break;
      case "gift":
        _inAdd_message.add(YBDGift().fromJson(message as Map<String, dynamic>));
        break;
      case "global":
        _inAdd_message.add(YBDGlobal().fromJson(message as Map<String, dynamic>));

        break;
      case "enter":
        _inAdd_message.add(YBDEnter().fromJson(message as Map<String, dynamic>));
        log("entering message:\n$message");
        break;
      case "follow":
        _inAdd_message.add(YBDFollowDisplayMessage().fromJson(message as Map<String, dynamic>));
        break;
      case "heart":
//        JsonConvert.fromJsonAsT(json)
        YBDRoomSocketUtil.getInstance().receiveHeartCount++;
        break;
      case "close":
        YBDRoomSocketUtil.getInstance().destorySocket();
        break;
      case "internalnotice":
        YBDInternal? intnal = YBDInternal().fromJson(message as Map<String, dynamic>);
        _inAdd_message.add(intnal);
        break;
      case "like":
        _inAdd_message.add(YBDLikeMessage().fromJson(message as Map<String, dynamic>));
        break;
      case "room_manager":
        _inAdd_message.add(YBDRoomManagerDisplayMessage().fromJson(message as Map<String, dynamic>));
        break;
      case "unsub":
        _inAdd_message.add(YBDUnsubscribe().fromJson(message as Map<String, dynamic>));
        break;
      case "redirect":
        logger.v("$SOCKET_TAG roomsocket:-- redirect");
        YBDRoomSocketUtil.getInstance().redirectUrl = Response().fromJson(message as Map<String, dynamic>).result;

        YBDRoomSocketUtil.getInstance().changeConnectStatus(RoomSocketStatus.TryConnecting);

        YBDRoomSocketUtil.getInstance().destorySocket();

        YBDRoomSocketUtil.getInstance().initSocket();
        break;
      case "mic_req":
        _inAdd_message.add(YBDMicRequestMessage().fromJson(message as Map<String, dynamic>));
        break;
      case "command":
        if (message['code'] == '905004') {
          _inAdd_message.add(YBDBootedMessage().fromJson(message as Map<String, dynamic>));
        }

        if (message['commandId'] == 26) {
          ///静音特殊处理
          _inAdd_message.add(YBDMicMuteResponseMessage().fromJson(message as Map<String, dynamic>));
        }
        if (message["commandId"] == -21) {
          YBDTeenPattiInfo msg = YBDTeenPattiInfo.fromJson(message as Map<String, dynamic>);
          _inAdd_message.add(msg);
        }
        if (message["commandId"] == -18) {
          YBDSubAckMessage? msg = YBDSubAckMessage().fromJson(message as Map<String, dynamic>);
          _inAdd_message.add(msg);
        }
        matchRespMessage(message);
        break;
      case "pub":
        matchPublishMessage(message);
        break;
      default:
        logger.v("+++++not match message+++++\n  $message \n+++++++++\n ");
        break;
    }
  }
  void receiveMessagelSiQ4oyelive(Map message) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  matchRespMessage(Map cmdMessage) {
    logger.v('matchRespMessage : $cmdMessage');
    _lock.synchronized(() {
      List<YBDSendWithResp> result = messageQueue
          .where((element) =>
              element.sequence == cmdMessage["sequence"] && (0 - element.commandId!) == cmdMessage["commandId"])
          .toList();
      if (result != null && result.length != 0) {
        logger.v('matchRespMessage callSuccess: ${result[0].commandId}');

        result[0].callSuccess(cmdMessage);
        messageQueue.remove(result[0]);
      }
    });
  }

  matchPublishMessage(Map publishMessage) {
    int? type = publishMessage['type'];
    switch (type) {
      case 1: // 房间用户
        _inAdd_message.add(YBDUserPublish().fromJson(publishMessage as Map<String, dynamic>));
        break;
      case 2:
        break;
      case 3:
        _inAdd_message.add(YBDMicUserPublish().fromJson(publishMessage as Map<String, dynamic>));
        break;
      case 5: // SuperFansPublish 房间粉丝榜
        _inAdd_message.add(YBDSuperFansMessage().fromJson(publishMessage as Map<String, dynamic>));
        break;
      case 6:
        _inAdd_message.add(YBDUserInfoPublish().fromJson(publishMessage as Map<String, dynamic>));
        break;
      case 7:
        break;
      case 8:
        YBDTeenPattiInfo msg = YBDTeenPattiInfo.fromJson(publishMessage as Map<String, dynamic>);
        _inAdd_message.add(msg);
        break;
      case 9:
        _inAdd_message.add(YBDPublishTeenPattiWinnerMessage().fromJson(publishMessage as Map<String, dynamic>));
        break;
      case 10:
        YBDPkPublish? result = YBDPkPublish().fromJson(publishMessage as Map<String, dynamic>);
        if (YBDLiveService.instance.data.roomId != null &&
            YBDLiveService.instance.data.roomId != result.content!.sourceRoom) {
          //在直播间的时候要判断pk数据，确认主播房间内的数据在哪方然后对掉；
          result.revertContentData();
        }
        _inAdd_message.add(result);
        break;
      case 11:
        _inAdd_message.add(YBDRoomBgPublish().fromJson(publishMessage as Map<String, dynamic>));
        logger.v("+++++ match YBDRoomBgPublish+++++\n  $publishMessage \n+++++++++\n ");
        logger.v("+++++ match YBDRoomBgPublish+++++\n  $publishMessage \n+++++++++\n ");
        break;
      case 12:
        _inAdd_message.add(YBDPKInvitePub().fromJson(publishMessage as Map<String, dynamic>));
        break;
      case 13:
        _inAdd_message.add(YBDPKInvitePub().fromJson(publishMessage as Map<String, dynamic>));
        print("7.23-------publishMessage:$publishMessage");
        break;
      case 14:
        _inAdd_message.add(YBDTeenSettleResultEntity().fromJson(publishMessage as Map<String, dynamic>));
        break;
      case 15: // ludo公聊信息
        logger.v("ludo socket public message\n  $publishMessage \n+++++++++\n ");
        _inAdd_message.add(YBDLudoPublicMessage.fromJson(publishMessage as Map<String, dynamic>));
        break;

      case 16: // 单独搜索邀请
        logger.v("target pk message\n  $publishMessage \n+++++++++\n ");
        _inAdd_message.add(YBDTargetPkMessage().fromJson(publishMessage as Map<String, dynamic>));
        break;

      case 17: // 单独搜索邀请响应
        logger.v("targetxxx pk message\n  $publishMessage \n+++++++++\n ");
        YBDToastUtil.toast(publishMessage['content']['text']);
        // _inAdd_message.add(YBDTargetPkMessage().fromJson(publishMessage));
        break;
      case 18: // 进房打开ludo游戏
        _inAdd_message.add(YBDLudoStatusMessageEntity().fromJson(publishMessage as Map<String, dynamic>));
        break;
      case 19:
        if (publishMessage["roomId"] == YBDUserUtil.getUserIdSync)
          YBDRoomUtil.liveDurationInMillSecs = publishMessage['content'];
        break;
      case 19:
        if (publishMessage["roomId"] == YBDUserUtil.getUserIdSync)
          YBDRoomUtil.liveDurationInMillSecs = publishMessage['content'];
        break;
      case 26:

        ///YBDMicMuteMessage
        _inAdd_message.add(YBDMicMuteResponseMessage().fromJson(publishMessage as Map<String, dynamic>));
        break;
      default:
        logger.v("+++++not match matchPublishMessage+++++\n  $publishMessage \n+++++++++\n ");
        break;
    }
  }
}
