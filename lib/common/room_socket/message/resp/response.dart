

import '../../../../generated/json/base/json_convert_content.dart';

class Response with JsonConvert<Response> {
  dynamic roomId;
  String? destination;
  List<int>? costTime;
  String? msgType;
  int? fromUser;
  int? toUser;
  String? mode;
  int? sequence;
  int? commandId;
  String? code;
  String? desc;
  dynamic result;
  dynamic content;

  T? getResult<T>() {
    if (result is Map) {
      return  JsonConvert.fromJsonAsT<T>(result);
    } else {
      return result;
    }
  }
}
