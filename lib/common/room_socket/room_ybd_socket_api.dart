

import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/room_socket/message/common/broadcast.dart';
import 'package:oyelive_main/common/room_socket/message/common/bullet_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/emoji_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/forbidden.dart';
import 'package:oyelive_main/common/room_socket/message/common/gift_ybd_command_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/match_ybd_command_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/mic_ybd_request_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/room_ybd_manager_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/subscribe.dart';
import 'package:oyelive_main/common/room_socket/message/common/surrender_ybd_pk_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/target_ybd_pk_op_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/text_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/unsubscribe.dart';
import 'package:oyelive_main/common/room_socket/message/resp/response.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_util.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_mute_message.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_join_msg.dart';

import 'message/base/message.dart';
import 'message/base/send_ybd_with_resp.dart';
import 'message/common/auth.dart';
import 'message/common/mic_ybd_user_message.dart';

class YBDRoomSocketApi {
  static YBDRoomSocketApi? _request;

  static var userId;

  ///大转盘在用
  static int? roomId;

  static YBDRoomSocketApi getInstance({Id}) {
    if (_request == null) {
      _request = YBDRoomSocketApi();
    }
    if (Id != null) {
      userId = Id;
    }
    return _request!;
  }

  auth({randomKey, client, latitude, longitude, version, unique, onSuccess(Response data)?, onTimeOut}) async {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDAuth, Response>(
        fromUser: userId,
        toUser: userId,
        mode: modeU,
        commandId: 1,
        extra: await YBDCommonUtil.generateCommonHeader(),
        message: YBDAuth()
          ..randomKey = randomKey
          ..client = client
          ..latitude = latitude
          ..longitude = longitude
          ..version = version
          ..protocol = 2
          ..unique = unique,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  subscribe(int? roomId, {String? protectData, onSuccess(Response data)?, onTimeOut}) {
    logger.v('subscribe roomid $roomId'.subLog(step: 100));
    YBDRoomSocketUtil.getInstance().inRoom(roomId);
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDSubscribe, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: roomId,
        destination: '/room/$roomId',
        mode: modeU,
        commandId: 18,
        message: YBDSubscribe()..protectData = protectData,
        onSuccess: (data) {
          logger.v('subscribe onSuccess roomid $roomId'.subLog(step: 100));
          onSuccess?.call(data);
        },
        onTimeOut: () {
          logger.v('subscribe timeout roomid $roomId'.subLog(step: 100));
          if (null != onTimeOut) {
            onTimeOut();
          }
        }));
  }

  unsubscribe(int? roomId) {
    // TODO: 测试代码
    // 取消订阅的时候 roomId 为 null 会断开 socket 连接
    // 这里是临时方案
    logger.i("unsubscribe roomid $roomId".unsubLog(step: 88));
    final noNullRoomId = roomId ?? 111;
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDUnsubscribe()
      ..roomId = noNullRoomId
      ..fromUser = userId
      ..msgType = 'unsub'
      ..toUser = noNullRoomId
      ..destination = '/room/$noNullRoomId'
      ..mode = modeU);
  }

  /// 获取频道声网 channel token
  getChannelKey(int? roomId, {String? protectData, onSuccess(Response data)?, onTimeOut}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDSubscribe, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: roomId,
        destination: '/room/$roomId',
        mode: modeU,
        commandId: 5,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  /// 获取声网 rtm token
  getRtmKey(int? roomId, {String? protectData, onSuccess(Response data)?, onTimeOut}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDSubscribe, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: roomId,
        destination: '/room/$roomId',
        mode: modeU,
        commandId: 27,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  /// 房间开播
  startBroadcastTitle(
    int? roomId, {
    int? screen,
    String? title,
    String? channel,
    int? protectMode,
    String? protectData,
    String? tag,
    int? micRequestEnable,
    onSuccess(Response data)?,
    onTimeOut,
  }) async {
    await YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDBroadcast, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: userId,
        destination: '/room/$roomId',
        mode: modeU,
        commandId: 7,
        message: YBDBroadcast()
          ..type = 1
          ..channel = channel
          ..title = title
          ..screen = screen
          ..protectMode = protectMode
          ..protectData = protectData
          ..tag = tag
          ..micRequestEnable = micRequestEnable,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  /// 房间停播
  stopBroadcast(
    int? roomId, {
    onSuccess(Response data)?,
    onTimeOut,
  }) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(
      YBDSendWithResp<YBDBroadcast, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: userId,
        destination: '/room/$roomId',
        mode: modeU,
        commandId: 7,
        message: YBDBroadcast()
          ..type = 2
          ..protectMode = 0
          ..screen = 0,
        onSuccess: (Response data) {
          logger.v('stop broadcast success : ${data.code}');
          if (null != onSuccess) {
            onSuccess(data);
          }
        },
        onTimeOut: () {
          logger.v('stop broadcast timeout');
          if (null != onTimeOut) {
            onTimeOut();
          }
        },
      ),
    );
  }

  /// 禁言或拉黑用户
  /// type 1.Mute 2.Boot
  forbiddenUser(
    int type,
    int? roomId,
    int? targetId,
    int period, {
    onSuccess(Response data)?,
  }) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDForbidden, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: targetId,
        destination: '/room/$roomId',
        mode: modeU,
        commandId: 3,
        message: YBDForbidden()
          ..operType = type
          ..period = period,
        onSuccess: (Response data) {
          logger.v('forbidden user result : ${data.code}');
          if (null != onSuccess) {
            onSuccess(data);
          }
        }));
  }

  /// 设置或取消房间管理员
  /// type : 1 设置房间管理员, 2 取消房间管理员
  roomManager(
    int type,
    int? roomId,
    int? targetId, {
    onSuccess(Response data)?,
  }) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDRoomManagerMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: targetId,
        destination: '/room/$roomId',
        mode: modeU,
        commandId: 13,
        message: YBDRoomManagerMessage()..operateType = type,
        onSuccess: (Response data) {
          logger.v('${type == 1 ? 'promote' : 'demote'} room manager result : ${data.code}');
          if (null != onSuccess) {
            onSuccess(data);
          }
        }));
  }

  ///麦位操作
  /// type 1: 上麦，2：下麦
  /// index：麦位编号，比如第一个麦位的index = 0
  micOperation(int? roomId, int? targetId, int? index, int type,
      {String? protectData, onSuccess(Response data)?, onTimeOut}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDMicUserMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: targetId,
        destination: '/room/$roomId',
        mode: modeU,
        commandId: 17,
        message: YBDMicUserMessage()
          ..index = index
          ..operType = type,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  ///静音操作
  micMuteOperation(int? roomId, int type, int? targetId, int index, int direction, int? originSequence,
      {String? protectData, onSuccess(Response data)?, onTimeOut}) {
    logger.v(
      ' micMuteOperation roomId:$roomId  type: $type targetId: $targetId index: $index direction: $direction originSequence: $originSequence, fromUser: $userId',
    );
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDMicMuteMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: targetId,
        destination: '/room/$roomId',
        mode: modeU,
        commandId: 26,
        message: YBDMicMuteMessage()
          ..fromUser = userId
          ..toUser = targetId
          ..destination = '/room/$roomId'
          ..mode = modeU
          ..roomId = roomId
          ..msgType = 'command'
          ..index = index
          ..direction = direction
          ..originSequence = (direction == 1 ? 0 : originSequence)
          ..type = type,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  ///三级以下  请求上麦
  requestMic(int? roomId, int type, int? targetId, int index, {String? protectData, onSuccess(Response data)?, onTimeOut}) {
    logger.v(' requestMic roomId:$roomId  type: $type targetId: $targetId index: $index');
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDMicRequestMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: targetId,
        destination: '/room/$roomId',
        mode: modeU,
        message: YBDMicRequestMessage()
          ..fromUser = userId
          ..toUser = targetId
          ..destination = '/room/$roomId'
          ..mode = modeU
          ..roomId = roomId
          ..msgType = 'mic_req'
          ..index = index
          ..type = type,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  //发送公聊消息
  sendPublicMessage({int? roomId, int? to, String? content}) {
    to = (to == null) ? -1 : to;

    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDTextMessage(content: content)
      ..roomId = roomId
      ..fromUser = userId
      ..toUser = to
      ..msgType = 'text'
      ..destination = '/room/$roomId'
      ..mode = modeM);
  }

  //发送emoji消息
  sendEmojiMessage({String? content}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDEmojiMessage(content: content)
      ..roomId = roomId
      ..fromUser = userId
      ..toUser = -1
      ..msgType = 'emoji'
      ..destination = '/room/$roomId'
      ..mode = modeM);
  }

  //发送弹幕消息
  sendBulletMessage({int? roomId, int? to, String? content}) {
    to = (to == null) ? -1 : to;

    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDBulletMessage(content: content)
      ..roomId = roomId
      ..fromUser = userId
      ..toUser = to
      ..msgType = 'bullet'
      ..destination = '/room/$roomId'
      ..mode = modeM);
  }

  sendGift({int? roomId, int? to, int? code, int? num, int? combo, int? personalId, onSuccess(Response data)?, onTimeOut}) {
    //personalId
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDGiftCommandMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: to,
        mode: modeM,
        commandId: 9,
        destination: '/room/$roomId',
        message: YBDGiftCommandMessage()
          ..code = code
          ..num = num
          ..combo = combo
          ..personalId = personalId
          ..pkId = YBDLiveService.instance.data.pkInfo?.pkId,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  ///获取teenpatti初始化数据
  sendTeenPatti({int? roomId, onSuccess(Response data)?, onTimeOut}) {
    //personalId
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDSubscribe, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: -1,
        mode: modeM,
        commandId: 21,
        destination: '/room/$roomId',
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  ///投注
  joinTeenPatti({int? roomId, int? gameId, int? index, int? coins, onSuccess(Response data)?, onTimeOut}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDTeenJoinMsg, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: -1,
        mode: modeM,
        commandId: 22,
        message: YBDTeenJoinMsg()
          ..coins = coins
          ..gameId = gameId
          ..commandId = 22
          ..index = index,
        destination: '/room/$roomId',
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  ///开始匹配
  startMatch({int? roomId, onSuccess(Response data)?, onTimeOut}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDMatchCommandMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: -1,
        mode: modeM,
        commandId: 28,
        destination: '/room/$roomId',
        message: YBDMatchCommandMessage()
          ..type = 1
          ..source = userId,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  ///中断，退出匹配
  endMatch({int? roomId, onSuccess(Response data)?, onTimeOut}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDMatchCommandMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: -1,
        mode: modeM,
        commandId: 28,
        destination: '/room/$roomId',
        message: YBDMatchCommandMessage()
          ..type = 4
          ..source = userId,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  /// 接受pk邀请
  acceptInvite(int source, {int? roomId, onSuccess(Response data)?, onTimeOut}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDMatchCommandMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: source,
        mode: modeU,
        commandId: 28,
        destination: '/room/$roomId',
        message: YBDMatchCommandMessage()
          ..type = 2
          ..source = source
          ..target = roomId,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  /// 拒绝pk邀请
  rejectInvite(int source, {int? roomId, onSuccess(Response data)?, onTimeOut}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDMatchCommandMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: source,
        mode: modeU,
        commandId: 28,
        destination: '/room/$roomId',
        message: YBDMatchCommandMessage()
          ..type = 3
          ..source = source
          ..target = roomId,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  /**
   *type 1发起 2接受 3拒绝
   */

  targetPkOperation(int? targetUserId, int type,
      {int? sourceUserId, bool? rejectFriends, onSuccess(Response data)?, onTimeOut}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDTargetPkOpMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: type == 1 ? targetUserId : sourceUserId,
        mode: modeU,
        commandId: 29,
        destination: '/room/$roomId',
        message: YBDTargetPkOpMessage()
          ..source = type == 1 ? userId : sourceUserId
          ..type = type
          ..target = targetUserId
          ..rejectFriends = rejectFriends
          ..timeZone = DateTime.now().timeZoneName,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }

  /// 提前结束PK(投降)
  surrenderPK({onSuccess(Response data)?, onTimeOut}) {
    YBDRoomSocketUtil.getInstance().sendSocketMessage(YBDSendWithResp<YBDSurrenderPKMessage, Response>(
        roomId: roomId,
        fromUser: userId,
        toUser: -1,
        mode: modeU,
        commandId: 30,
        destination: '/room/$roomId',
        message: YBDSurrenderPKMessage()
          ..pkId = YBDLiveService.instance.data.pkInfo?.pkId
          ..sourceUserId = userId
          ..type = 1,
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }
}
