


import 'dart:convert';
import 'dart:io';

import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';

import '../../../generated/json/base/json_convert_content.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/user/util/user_ybd_util.dart';
import '../../analytics/analytics_ybd_util.dart';
import '../../util/file_ybd_util.dart';
import '../../util/log_ybd_util.dart';
import 'entity/iap_ybd_order_list_entity.dart';
import 'iap_ybd_util.dart';

/// 订单管理类
class YBDOrderManager {
  /// 订单管理实例
  static YBDOrderManager? _instance;

  static YBDOrderManager? get instance {
    if (null == _instance) {
      _instance = YBDOrderManager();
    }

    return _instance;
  }

  /// 从接口获取订单号
  Future<String> requestOrderId(
    BuildContext? context, {
    required ProductDetails product,
  }) async {
    // 对应购买项目的价格
    final price = YBDIapUtil.orderPriceWithProduct(product);
    final currency = YBDIapUtil.currencyCodeWithProduct(product);
    logger.d('===iap product price $price, currency : $currency');

    String? userId = await YBDUserUtil.userId();
    String type = 'apple';

    if (Platform.isAndroid) {
      // TODO: 测试代码
      // 安卓预下单设置 [type] 参数
      type = 'android';
    }

    final preOrderEntity = await ApiHelper.requestPreOrder(
      context,
      userId: userId,
      money: price,
      currency: currency,
      productId: product.id,
      type: type,
    );

    var result = preOrderEntity?.data?.orderId;

    logger.v('request order id result : $result');
    // print("preOrderEntity?.message ${preOrderEntity?.message} ${preOrderEntity?.code} ${preOrderEntity?.toJson()}");
    if (null == result) {
      // 预下单失败的埋点
      YBDAnalyticsUtil.logEvent(
        YBDAnalyticsEvent(
          YBDEventName.PURCHASE,
          itemID: product.id,
          returnExtra: YBDAnalyticsUtil.ORDER_FAILED,
        ),
      );
      if (preOrderEntity?.message != null) {
        YBDToastUtil.toast(preOrderEntity?.message);
      } else {
        YBDToastUtil.toast(translate('network_error'));
      }
    } else {
      // 预下单成功的埋点
      YBDAnalyticsUtil.logEvent(
        YBDAnalyticsEvent(
          YBDEventName.PURCHASE,
          itemID: product.id,
          returnExtra: YBDAnalyticsUtil.ORDER_SUCCESS,
          transactionId: result,
        ),
      );
    }

    // 调接口获取订单号
    return result ?? '';
  }

  /// 取消预订单
  Future<void> requestCancelOrderId(
    String orderId,
    BuildContext? context, {
    String? productId,
  }) async {
    ApiHelper.requestCancelApplePurchase(context, orderId: orderId);

    // 取消预下单成功的埋点
    YBDAnalyticsUtil.logEvent(
      YBDAnalyticsEvent(
        YBDEventName.PURCHASE,
        itemID: productId,
        returnExtra: 'order_cancel',
        transactionId: orderId,
      ),
    );
  }

  /// 获取当前用户的订单号
  Future<YBDIapOrderListRecord?> recordWithProductId(String productId) async {
    YBDIapOrderListRecord? record;

    // 获取订单列表
    var orderListEntity = await _getOrderListEntity();

    if (null != orderListEntity) {
      // 查询对应产品的订单号
      final orderRecord = orderListEntity.record!.firstWhereOrNull(
        (element) => element!.productId == productId,
        // orElse: () => null,
      );

      record = orderRecord;
    }

    logger.d('product id : $productId, local record : ${record!.toJson()}');
    return record;
  }

  /// 预下单的时候保存（更新）购买产品的订单号
  Future<void> saveRecord(String productId, String orderId) async {
    logger.d('save orderId : $orderId, productId : $productId');

    // 获取订单列表
    var orderListEntity = await _getOrderListEntity();

    if (null == orderListEntity) {
      logger.v('save new order list entity');
      // 构建新的订单记录列表
      orderListEntity = YBDIapOrderListEntity();
      orderListEntity.record = [];

      // 构建新的订单记录
      final record = await _createOrderRecord(productId, orderId);
      orderListEntity.record!.add(record);
    } else {
      // 查询对应产品的订单记录
      final orderRecord = orderListEntity.record!.firstWhereOrNull(
        (element) => element!.productId == productId,
        // orElse: () => null,
      );

      if (null == orderRecord) {
        // 构建新的订单记录
        final record = await _createOrderRecord(productId, orderId);
        orderListEntity.record!.add(record);
        logger.v('product : $productId record is null, save record : ${record.toJson()}');
      } else {
        logger.v('update old : ${orderRecord.toJson()}');
        // 更新订单号
        orderRecord.orderId = orderId;
        logger.v('update record new : ${orderRecord.toJson()}');
      }
    }

    logger.v('save order list : ${orderListEntity.toJson()}');
    await _saveOrderListEntity(orderListEntity);
  }
  void saveRecordm5t9Coyelive(String productId, String orderId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 删除本地订单记录
  Future<void> removeOrderRecord(String productId) async {
    // 获取订单列表
    final orderListEntity = await _getOrderListEntity();

    if (null == orderListEntity || null == orderListEntity.record) {
      logger.v('no order record record list');
      return;
    } else {
      // 查询对应产品的订单记录
      var orderRecord = orderListEntity.record!.firstWhereOrNull(
        (element) => element!.productId == productId,
        // orElse: () => null,
      );

      if (null == orderRecord) {
        logger.v('no order record record');
        return;
      } else {
        logger.v('remove record :${orderRecord.orderId}, productId : $productId');
        orderListEntity.record!.remove(orderRecord);
      }
    }

    await _saveOrderListEntity(orderListEntity);
  }
  void removeOrderRecord9hcHEoyelive(String productId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 构建新的订单记录
  Future<YBDIapOrderListRecord> _createOrderRecord(String productId, String orderId) async {
    var record = YBDIapOrderListRecord();
    record.productId = productId;
    record.orderId = orderId;
    record.userId = await YBDUserUtil.userId();
    return record;
  }
  void _createOrderRecordUfUDtoyelive(String productId, String orderId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 订单记录写到本地文件
  Future<void> _saveOrderListEntity(YBDIapOrderListEntity orderListEntity) async {
    try {
      // 保存到对应用户的订单文件
      final fileName = '${await YBDUserUtil.userId()}_orderList.json';
      // 把订单记录保存到本地 json 文件
      await YBDFileUtil.writeJsonToFile(json.encode(orderListEntity.toJson()), fileName);
    } catch (e) {
      logger.e('write order list error : $e, ${orderListEntity.toJson()}');
    }
  }
  void _saveOrderListEntityPNGxeoyelive(YBDIapOrderListEntity orderListEntity)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 从本地读取订单记录
  Future<YBDIapOrderListEntity?> _getOrderListEntity() async {
    try {
      // 从对应用户的订单文件获取订单信息
      final fileName = '${await YBDUserUtil.userId()}_orderList.json';
      String? jsonStr = await YBDFileUtil.readJsonFromFile(fileName);
      if (null == jsonStr) {
        logger.v('order list json is null');
        return null;
      }

      Map? mapData = json.decode(jsonStr);
      YBDIapOrderListEntity? orderListEntity = JsonConvert.fromJsonAsT<YBDIapOrderListEntity>(mapData);
      return orderListEntity;
    } catch (e) {
      logger.e('read order list error : $e');
      return null;
    }
  }
}
