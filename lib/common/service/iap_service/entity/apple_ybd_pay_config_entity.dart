

import '../../../../generated/json/base/json_convert_content.dart';

class YBDApplePayConfigEntity with JsonConvert<YBDApplePayConfigEntity> {
  String? code;
  List<YBDApplePayConfigData?>? data;
  bool? success;
}

class YBDApplePayConfigData with JsonConvert<YBDApplePayConfigData> {
  String? nickName;
  String? name;
  String? icon;
  List<YBDApplePayConfigDataItem?>? items;
}

class YBDApplePayConfigDataItem with JsonConvert<YBDApplePayConfigDataItem> {
  double? money;
  String? productId;
  int? value;
}
