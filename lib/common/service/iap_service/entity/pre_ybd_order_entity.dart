
import '../../../../generated/json/base/json_convert_content.dart';

class YBDPreOrderEntity with JsonConvert<YBDPreOrderEntity> {
  String? code;
  String? message;
  YBDPreOrderData? data;
}

class YBDPreOrderData with JsonConvert<YBDPreOrderData> {
  int? money;
  String? orderId;
  int? userId;
}
