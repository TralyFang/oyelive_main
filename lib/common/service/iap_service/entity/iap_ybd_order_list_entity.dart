

import '../../../../generated/json/base/json_convert_content.dart';

class YBDIapOrderListEntity with JsonConvert<YBDIapOrderListEntity> {
  List<YBDIapOrderListRecord?>? record;
}

class YBDIapOrderListRecord with JsonConvert<YBDIapOrderListRecord> {
  /// 用[productId]作为键值来存取记录
  String? productId;
  String? orderId;
  String? userId;
}
