
import 'package:flutter/cupertino.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

import 'iap_ybd_def.dart';

/// 处理购买流程的公共部分
class YBDPurchaseHandler {
  /// 连接商店的实例对象
  InAppPurchase? connection;

  /// 调接口时用
  BuildContext? context;

  /// 分发结果回调，把结果返回给页面
  StatusCallback? statusCallback;

  /// 发起购买
  Future<void> purchaseProduct(ProductDetails product) async {}
  void purchaseProductQhUNioyelive(ProductDetails product)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 从商店购买商品
  /// 重新发起购买流程
  Future<void> purchaseProductFromStore(String orderId, ProductDetails product) async {}
  void purchaseProductFromStoreBRsuOoyelive(String orderId, ProductDetails product)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理已经购买过的商品
  /// 给已购商品分发内容
  /// 清除错误的购买记录
  // Future<void> handlePastPurchase() async {}

  /// 处理支付完成的购买
  Future<void> handlePurchased(PurchaseDetails purchaseDetails, {bool retry = false}) async {}
  void handlePurchasedX45HRoyelive(PurchaseDetails purchaseDetails, {bool retry = false})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 调分发购买内容接口
  Future<String> deliverProduct({
    String? productId,
    String? orderId,
    String? receipt,
  }) async {
    return YBDIapPayResult.DELIVER_UNKNOWN;
  }

  /// 调用场景
  /// 1. 完成购买
  ///    完成前要分发购买内容: [_deliverProduct]
  ///    完成后清除本地记录: [YBDOrderManager.instance.removeOrderRecord]
  /// 2. app 启动时检查到上次购买错误时调用
  /// 3. 购买过程发生错误会调用
  ///
  Future<void> completePurchase(
    PurchaseDetails purchaseDetails, {
    bool isError = false,
    String orderId = '',
  }) async {}

  /// 处理挂起状态的购买
  Future<void> handlePurchasePending(PurchaseDetails purchaseDetails) async {}
  void handlePurchasePendingtzSk1oyelive(PurchaseDetails purchaseDetails)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理错误状态的购买
  Future<void> handlePurchaseError(PurchaseDetails purchaseDetails) async {}
}
