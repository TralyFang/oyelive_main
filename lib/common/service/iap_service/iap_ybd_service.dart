
import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:oyelive_main/ui/page/index_ybd_page.dart';

import '../../../ui/page/room/util/tp_ybd_dialog_manager.dart';
import '../../analytics/analytics_ybd_util.dart';
import '../../util/common_ybd_util.dart';
import '../../util/log_ybd_util.dart';
import 'apple_ybd_purchase_handler.dart';
import 'entity/apple_ybd_pay_config_entity.dart';
import 'iap_ybd_def.dart';
import 'iap_ybd_util.dart';
import 'purchase_ybd_handler.dart';

/// 支付服务类
///
/// 获取服务实例的方法: [get instance]
/// 设置网络请求 context: YBDIapService.context = context;
///
/// 初始化服务配置的方法:
/// [initConfig]
/// app 启动时初始化
/// 该方法执行的内容:
/// 1, 监听购买队列
/// 2, 配置商品列表
/// 3, 处理已购买内容
///
/// 配置商品列表: [configProductList]
/// 发起购买: [purchaseProduct]
///
/// 购买状态: [IapStatus]
/// 监听购买状态:
/// [setIapStatusCallback]
/// 移除监听: 设置 null
/// 使用场景举例:
/// 在购买页面的 [initState] 方法里设置回调方法
/// 在购买页面的 [dispose] 方法里设置为 null
/// IMPORTANT! 要在app启动的时候监听支付队列，避免漏掉未完成的交易
class YBDIapService {
  /// 服务类实例
  static YBDIapService? _instance;

  static YBDIapService get instance {
    if (null == _instance) {
      _instance = YBDIapService();
    }

    return _instance!;
  }

  /// 处理苹果购买流程
  late YBDPurchaseHandler _purchaseHandler;

  /// 上下文，发起网络请求要用到
  BuildContext? context;

  /// app已初始化，比如数据库的初始化
  bool _initialized = false;

  /// app启动时监听已完成的购买，完成初始化后走重试流程
  List<PurchaseDetails> _retryPurchaseList = [];

  /// 支付加载框
  YBDTPDialogManager _dialogManger = YBDTPDialogManager();

  YBDTPDialogManager get dialogManger => _dialogManger;

  /// 连接商店的实例对象
  final InAppPurchase _connection = InAppPurchase.instance;

  /// 监听购买队列变化的订阅对象
  late StreamSubscription<List<PurchaseDetails>> _subscription;

  /// 商品详情列表
  List<ProductDetails> _products = [];

  List<ProductDetails> get products => _products;

  /// 从接口获取的商品列表
  List<YBDApplePayConfigDataItem?> _itemList = [];

  /// 分发结果回调，把结果返回给页面
  StatusCallback? _iapStatusCallback;

  /// 初始化设置
  /// app 完成初始化后调用，比如app进入首页[YBDIndexPage]
  void initConfig() {
    logger.i('config iap service');

    // app已完成初始化
    _initialized = true;

    // 监听支付队列
    // 已在main.dart中监听了，这里不重复监听
    // listenPurchaseQueue();

    // 配置商品列表
    configProductList();

    // 配置处理购买的类
    _configPurchaseHandler();

    // app初始化后重试已完成的购买
    _deliverRetryPurchasedOrder();

    // 处理已购买内容
    // 目前的支付流程中不需要恢复已经完成的交易，这行代码要注释掉
    // 参考：https://developer.apple.com/documentation/storekit/skpaymentqueue/1506123-restorecompletedtransactions?changes=la&language=objc
    // _handlePastPurchase();
  }
  void initConfigdlNagoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 监听支付队列
  void listenPurchaseQueue() {
    // 监听购买更新的事件
    _subscription = _connection.purchaseStream.listen((purchaseDetailsList) {
      _listenToPurchaseUpdated(purchaseDetailsList);
    }, onDone: () {
      logger.i('purchase updated stream done');
      _subscription.cancel();
    }, onError: (error) {
      logger.i('purchase updated stream error : $error');
    });
  }
  void listenPurchaseQueue16zg3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 根据不同平台配置处理购买流程的类
  void _configPurchaseHandler() {
    if (Platform.isIOS) {
      _purchaseHandler = YBDApplePurchaseHandler();
    } else {
      // 安卓购买流程处理类
      _purchaseHandler = YBDPurchaseHandler();
    }

    _purchaseHandler.context = context;
    _purchaseHandler.connection = _connection;
    _purchaseHandler.statusCallback = _iapStatusCallback;
  }
  void _configPurchaseHandlerWNy9Foyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 配置商品列表
  Future<void> configProductList() async {
    logger.i('configProductList');

    // 检测是否能连接到商店
    bool isAvailable = await _connection.isAvailable();

    if (!isAvailable) {
      logger.i('can not connect to store');
      _products = [];
      // 无法连接到商店返回空的商品列表，不去商店拉取信息
      return;
    }

    logger.i('connect to store');

    // 清除旧数据
    _itemList.clear();

    // 请求接口获取商品 id 列表
    _itemList.addAll(await YBDIapUtil.requestProductIdList(context));
    YBDIapUtil.printProductItemList(_itemList);

    // 根据产品 id 查询对应的商品信息
    final productList = _itemList.map((e) => e!.productId).toList();
    _products = YBDIapUtil.sortProducts(await YBDIapUtil.queryProductList(_connection, productList));
  }
  void configProductListu2OVWoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理已经购买过的商品
  /// 给已购商品分发内容
  /// 清除错误的购买记录
  // Future<void> _handlePastPurchase() async {
  //   logger.i('handle past purchase after launch app');
  //   _purchaseHandler.handlePastPurchase();
  // }

  /// 设置分发结果回调，把结果回调给页面
  /// [callback] 页面传过来的回调方法
  void setIapStatusCallback(StatusCallback? callback) {
    if (null == callback) {
      logger.i('clear iap callback');
    }

    // 设置新回调
    _iapStatusCallback = null;
    _iapStatusCallback = callback;

    // 在 handler 里触发回调
    _purchaseHandler.statusCallback = _iapStatusCallback;
  }
  void setIapStatusCallbackYJKlzoyelive(StatusCallback? callback) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 发起购买
  Future<void> purchaseProduct(ProductDetails product) async {
    logger.i('purchase product ${product.id}');
    _purchaseHandler.purchaseProduct(product);
  }

  /// 处理挂起状态的购买
  Future<void> _handlePurchasePending(PurchaseDetails purchaseDetails) async {
    if (null != _iapStatusCallback) {
      // 通知页面正在购买中
      _iapStatusCallback!(IapStatus.Pending, purchaseDetails.productID);
    }

    // 处理挂起状态的购买并保存订单到 firebase
    _purchaseHandler.handlePurchasePending(purchaseDetails);

    // pending 支付状态的埋点
    YBDAnalyticsUtil.logEvent(
      YBDAnalyticsEvent(
        YBDEventName.PURCHASE,
        itemID: purchaseDetails.productID,
        returnExtra: YBDAnalyticsUtil.PURCHASE_PENDING,
      ),
    );
  }
  void _handlePurchasePendingGfrU4oyelive(PurchaseDetails purchaseDetails)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理错误状态的购买
  Future<void> _handlePurchaseError(PurchaseDetails purchaseDetails) async {
    // 处理支付错误的购买并保存订单到 firebase
    _purchaseHandler.handlePurchaseError(purchaseDetails);

    // 支付错误的埋点
    YBDAnalyticsUtil.logEvent(
      YBDAnalyticsEvent(
        YBDEventName.PURCHASE,
        itemID: purchaseDetails.productID,
        returnExtra: YBDAnalyticsUtil.PURCHASE_ERROR,
      ),
    );
  }

  /// 处理支付完成的购买
  Future<void> _handlePurchased(PurchaseDetails purchaseDetails, {bool retry = false}) async {
    logger.i('isRetry : $retry');

    // 处理支付过的购买并保存订单到 firebase
    _purchaseHandler.handlePurchased(purchaseDetails, retry: retry);

    // 把订单详情写入到Info级别的日志
    YBDIapUtil.printProductWithPurchaseDetail(purchaseDetails, _products);

    // 支付成功的埋点
    YBDAnalyticsUtil.logEvent(
      YBDAnalyticsEvent(
        YBDEventName.PURCHASE,
        itemID: purchaseDetails.productID,
        returnExtra: YBDAnalyticsUtil.PURCHASE_PURCHASED,
      ),
    );
  }
  void _handlePurchasedxmfkDoyelive(PurchaseDetails purchaseDetails, {bool retry = false})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理未知状态的购买
  Future<void> _handleUnknownPurchased(PurchaseDetails purchaseDetails) async {
    // 理论上不会有这种场景，这里只做记录，不处理
    if (Platform.isIOS) {
      YBDCommonUtil.storeAppleOrder(
        purchaseDetails.productID,
        purchaseDetails.productID,
        YBDIapPurchaseStatus.UNKNOWN,
        thirdPartyId: purchaseDetails.purchaseID ?? '',
        purchase: YBDIapUtil.purchase2String(purchaseDetails),
      );
    }

    // 未知支付状态的埋点
    YBDAnalyticsUtil.logEvent(
      YBDAnalyticsEvent(
        YBDEventName.PURCHASE,
        itemID: purchaseDetails.productID,
        returnExtra: YBDAnalyticsUtil.PURCHASE_UNKNOWN,
      ),
    );
  }

  /// 处理重试订单
  void _deliverRetryPurchasedOrder() {
    _handlePurchaseDetailList(_retryPurchaseList, retry: true);

    // 记录走重试的订单
    _retryPurchaseList.forEach((purchaseDetails) {
      YBDCommonUtil.storeAppleOrder(
        'initialized retry deliver',
        '',
        YBDIapPurchaseStatus.PURCHASED,
        retry: true,
        thirdPartyId: purchaseDetails.purchaseID!,
        purchase: YBDIapUtil.purchase2String(purchaseDetails),
        productId: purchaseDetails.productID,
      );
    });
  }
  void _deliverRetryPurchasedOrderLY0XMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 监听购买更新
  void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList) {
    logger.i('purchaseDetailsList length: ${purchaseDetailsList.length}, initialized: $_initialized');

    if (_initialized) {
      // 已初始化则处理购买队列
      _handlePurchaseDetailList(purchaseDetailsList);
    } else {
      // 没有初始化先保存已完成的购买，在初始化完成后走重试流程
      _retryPurchaseList.addAll(purchaseDetailsList);

      // 记录需要重试的订单
      purchaseDetailsList.forEach((purchaseDetails) {
        YBDCommonUtil.storeAppleOrder(
          'not initialized store purchase details',
          '',
          YBDIapPurchaseStatus.PURCHASED,
          retry: true,
          thirdPartyId: purchaseDetails.purchaseID!,
          purchase: YBDIapUtil.purchase2String(purchaseDetails),
          productId: purchaseDetails.productID,
        );
      });
    }
  }

  /// 处理购买队列
  void _handlePurchaseDetailList(List<PurchaseDetails> purchaseDetailsList, {bool retry = false}) {
    if (null == purchaseDetailsList || purchaseDetailsList.isEmpty) {
      logger.i('purchaseDetailsList is empty');
      return;
    }

    purchaseDetailsList.forEach((PurchaseDetails purchaseDetails) async {
      switch (purchaseDetails.status) {
        case PurchaseStatus.pending:
          logger.i('purchase status pending : ${purchaseDetails.productID}');
          _handlePurchasePending(purchaseDetails);
          break;
        case PurchaseStatus.error:
          logger.i('purchase status error : ${purchaseDetails.productID}');
          logger.e('purchase error : ${purchaseDetails.error!.details}');
          await _handlePurchaseError(purchaseDetails);
          break;
        case PurchaseStatus.purchased:
          logger.i('purchased : : ${purchaseDetails.productID}');
          _handlePurchased(purchaseDetails, retry: retry);
          break;
        default:
          logger.i('purchased unhandled status : ${purchaseDetails.status}');
          _handleUnknownPurchased(purchaseDetails);
          break;
      }
    });
  }
  void _handlePurchaseDetailList6OPHGoyelive(List<PurchaseDetails> purchaseDetailsList, {bool retry = false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
