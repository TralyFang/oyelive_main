
class YBDIapPayResult {
  /// 预订单没有处理但是苹果订单已经处理过 直接返回成功 0
  /// 预订单重复发豆子 直接返回成功 0
  static const DELIVER_SUCCESS = '0';

  /// 收据校验失败
  /// 预订单和第三方订单的金额对不上
  static const DELIVER_FAILED = '13360';

  /// 预订单已经处理过了但是苹果订单没有处理过
  static const INVALID_ORDER_ID = '13380';

  /// 不存在的预订单
  static const NOT_EXIST_ORDER_ID = '13111';

  /// 服务端未知错误
  static const DELIVER_UNKNOWN = '13500';

  /// 用户没有登录
  static const NOT_LOGIN = 'not login';

  /// 无效的收据
  // static const INVALID_RECEIPT = 2;

  /// 验证收据超时
  // static const VERIFICATION_TIMEOUT = 3;

}

/// 页面使用的购买状态
enum IapStatus {
  // 分发完成
  Success,
  // 分发时发生错误，提示用户正在分发
  DeliverError,
  // 正在购买
  Pending,
  // 购买错误
  PurchaseError,
}

/// 分发结果回调
typedef StatusCallback = void Function(IapStatus, String?);

/// 订单支付状态
class YBDIapPurchaseStatus {
  /// 购买未消耗的商品
  static const BUY_UN_CONSUMABLE = 'buy_un_consumable';

  /// 生成预订单
  static const ORDER = 'order';

  /// 启动 app 扫描到的错误并标记为完成的订单
  static const PAST_ERROR_CLEAR = 'past_error_clear';

  /// 支付成功的订单
  static const PURCHASED = 'purchased';

  /// 交易成功的订单
  static const SUCCESS = 'success';

  /// 支付成功分发内容失败的订单
  static const LOST = 'lost';

  /// 支付成功分发内容失败的订单
  static const EMPTY_RECEIPT = 'empty_receipt';

  /// 挂起的订单
  static const PENDING = 'pending';

  /// 购买错误后标记为完成的订单
  static const ERROR_CLEAR = 'error_clear';

  /// 未知状态的订单
  static const UNKNOWN = 'unknown';
}
