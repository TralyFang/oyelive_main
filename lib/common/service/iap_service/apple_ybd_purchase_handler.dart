
import 'package:flutter/cupertino.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:oyelive_main/common/analytics/crashlytics_ybd_util.dart';
import 'package:oyelive_main/common/util/database_ybd_until.dart';
import 'package:oyelive_main/common/util/purchase_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/recharge/entity/db_ybd_order_purchase_entity.dart';
import 'package:in_app_purchase_ios/store_kit_wrappers.dart';

import '../../../module/api_ybd_helper.dart';
import '../../analytics/analytics_ybd_util.dart';
import '../../util/common_ybd_util.dart';
import '../../util/log_ybd_util.dart';
import '../../util/toast_ybd_util.dart';
import 'iap_ybd_def.dart';
import 'iap_ybd_util.dart';
import 'order_ybd_manager.dart';
import 'purchase_ybd_handler.dart';

/// 处理苹果购买流程
class YBDApplePurchaseHandler extends YBDPurchaseHandler {
  /// 连接商店的实例对象
  InAppPurchase? connection;

  /// 调接口时用
  BuildContext? context;

  /// 分发结果回调
  StatusCallback? statusCallback;

  /// 发起购买
  Future<void> purchaseProduct(ProductDetails product) async {
    // 关闭挂起的交易, 只对苹果有效
    // if (Const.TEST_ENV) {
    //   String result = await FlutterInappPurchase.instance.clearTransactionIOS();
    //   logger.i('cleared all pending transaction : $result');
    // }

    // 从接口获取订单号
    String orderId = await YBDOrderManager.instance!.requestOrderId(
      context,
      product: product,
    );

    logger.i('iap order id $orderId');
    if (null == orderId || orderId.isEmpty) {
      logger.i('get order id failed');
      if (null != statusCallback) {
        statusCallback!(IapStatus.PurchaseError, product.id);
      }

      // 没获取到预下单的订单号，终止购买流程
      // YBDToastUtil.toast(translate('network_error'));
      return;
    }

    // // 保存订单记录
    // await YBDOrderManager.instance.saveRecord(product.id, orderId);

    YBDCommonUtil.storeAppleOrder(
      orderId,
      null,
      YBDIapPurchaseStatus.ORDER,
      retry: false,
      productId: product.id,
    );

    ///屏蔽存储本地json 预订单存数据库
    var dbResult = await YBDDataBaseUtil.getInstance().orderAdd(
      YBDDBOrderPurchaseEntity(
          null, orderId, product.id, DateTime.now().millisecondsSinceEpoch, null, null, null, '1', '0'),
    );
    logger.i('orderId : $orderId with product : ${product.id} dbResult:$dbResult');
    await purchaseProductFromStore(orderId, product);
    logger.i('after purchaseProductFromStore $orderId, ${product.id}');
  }
  void purchaseProducttVGExoyelive(ProductDetails product)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 从商店购买商品
  Future<void> purchaseProductFromStore(String orderId, ProductDetails product) async {
    logger.i('purchase product : ${product.id}, orderId : $orderId');
    PurchaseParam purchaseParam = PurchaseParam(
      productDetails: product,
      applicationUserName: orderId,
      // 沙箱测试会弹出家长同意的弹框这里直接用 false
      // sandboxTesting: false,
    );

    try {
      // 购买消耗型产品
      final result = await connection!.buyConsumable(purchaseParam: purchaseParam);
      logger.i('buy consumable result : $result');
    } catch (e) {
      logger.e('iap buy consumable error $e');
      YBDToastUtil.toast(translate('connection_error'));
      YBDCommonUtil.storeAppleOrder(
        orderId,
        e.toString(),
        YBDIapPurchaseStatus.BUY_UN_CONSUMABLE,
        retry: false,
        productId: product.id,
      );

      // 交易失败的订单，需要完成交易才能再次购买同一商品
      SKPaymentQueueWrapper().transactions().then((List<SKPaymentTransactionWrapper> transactions) {
        transactions.forEach((SKPaymentTransactionWrapper element) {
          logger.i('transactions state : ${element.transactionState}, error:${element.error}, '
              'identifier:${element.transactionIdentifier},productIdentifier:${element.payment.productIdentifier}');
          if (element.transactionState != SKPaymentTransactionStateWrapper.purchased) {
            SKPaymentQueueWrapper().finishTransaction(element);
          }
        });
      });

    }
  }
  void purchaseProductFromStoreFeIXDoyelive(String orderId, ProductDetails product)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理已经购买过的商品
  /// 给已购商品分发内容
  /// 清除错误的购买记录
  // 目前的支付流程中不需要恢复已经完成的交易，这个方法要屏蔽掉
  // 参考：https://developer.apple.com/documentation/storekit/skpaymentqueue/1506123-restorecompletedtransactions?changes=la&language=objc
  // @override
  // Future<void> handlePastPurchase() async {
  // 查询上次已购买的商品
  // final purchaseResponse = await connection.queryPastPurchases();
  // logger.i('query past purchases ${purchaseResponse?.pastPurchases?.length}');
  //
  // if (purchaseResponse.error != null) {
  //   logger.i('query past purchases error : ${purchaseResponse.error}');
  //   return;
  // }
  //
  // for (PurchaseDetails purchase in purchaseResponse.pastPurchases) {
  //   logger.i('past purchased status : ${purchase.status}');
  //   switch (purchase.status) {
  //     case PurchaseStatus.purchased:
  //       logger.i('past purchased product : ${purchase.productID}');
  //       // 处理支付完成的购买
  //       handlePurchased(purchase, retry: true);
  //       break;
  //     case PurchaseStatus.error:
  //       logger.e('error purchase : ${purchase.productID}, ${purchase?.skPaymentTransaction?.transactionIdentifier}');
  //       // 清除错误状态的购买
  //       await completePurchase(purchase, isError: true);
  //
  //       // 删除本地订单记录
  //       // YBDOrderManager.instance.removeOrderRecord(purchase.productID);
  //
  //       final error = '_handlePastPurchase: ${purchase.error}';
  //       final productId = 'productId : ${purchase.productID}';
  //       final orderId = await getIosOrderId(purchase);
  //       final receipt = await YBDIapUtil.getReceipt(connection, purchase, orderId, retry: true);
  //       Bugly.postException(
  //         type: Const.BUGLY_EXCEPTION_IAP,
  //         error: '$error, $productId, $receipt',
  //       );
  //
  //       // 启动 app 扫描到的错误并标记为完成的订单
  //       YBDCommonUtil.storeAppleOrder(
  //         orderId,
  //         receipt,
  //         YBDIapPurchaseStatus.PAST_ERROR_CLEAR,
  //         retry: true,
  //         thirdPartyId: purchase?.skPaymentTransaction?.transactionIdentifier,
  //         purchase: YBDIapUtil.purchase2String(purchase),
  //         productId: purchase.productID,
  //       );
  //       break;
  //     default:
  //       logger.i('past purchased ignored status : ${purchase.productID}');
  //   }
  // }
  // }

  /// 处理支付完成的购买
  @override
  Future<void> handlePurchased(PurchaseDetails purchaseDetails, {bool retry = false}) async {
    // 获取自定义字段的订单号（获取不到）
    // var orderId = purchaseDetails.skPaymentTransaction.payment.applicationUsername;
    //final localRecord = await YBDOrderManager.instance.recordWithProductId(purchaseDetails.productID);
    logger.i('handlePurchased transactionIdentifier: ${purchaseDetails.purchaseID}');
    var orderId = await getIosOrderId(purchaseDetails, isPurchase: true);
    logger.i('order id from applicationUsername : $orderId');
    /* if (null == orderId || orderId.isEmpty) {
      // 获取本地订单号
      // orderId = localRecord?.orderId;
      orderId = await YBDPurchaseUtil.getIosPayOrderId(purchaseDetails.productID);
      logger.v('order id from local : $orderId');
    }*/

    // 从购买详情获取收据
    var receipt = await YBDIapUtil.getReceipt(connection, purchaseDetails, orderId);

    /* if (null == receipt || receipt.isEmpty) {
      // 购买详情异常时刷新收据
      receipt = await YBDIapUtil.getReceiptData(connection);

      // 记录收据异常的事件
      YBDCommonUtil.storeAppleOrder(
        YBDCommonUtil.storeFromContext().state.bean.id,
        orderId ?? purchaseDetails.productID,
        receipt,
        YBDIapPurchaseStatus.EMPTY_RECEIPT,
        retry: retry,
        thirdPartyId: purchaseDetails?.skPaymentTransaction?.transactionIdentifier,
        purchase: YBDIapUtil.purchase2String(purchaseDetails),
      );
    }*/

    // 支付成功的订单
    YBDCommonUtil.storeAppleOrder(
      orderId,
      receipt,
      YBDIapPurchaseStatus.PURCHASED,
      retry: retry,
      thirdPartyId: purchaseDetails.purchaseID ?? '',
      purchase: YBDIapUtil.purchase2String(purchaseDetails),
      productId: purchaseDetails.productID,
    );

    // 分发购买内容
    final result = await deliverProduct(
      productId: purchaseDetails.productID,
      orderId: orderId,
      receipt: receipt,
      transactionId: purchaseDetails.purchaseID,
    );

    logger.i('delivered result : $result, $orderId');
    // 处理分发结果
    _handleDeliverResult(result, purchaseDetails, orderId, receipt ?? '', retry: retry);
  }
  void handlePurchased0e1Rfoyelive(PurchaseDetails purchaseDetails, {bool retry = false})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理分发内容的结果
  Future<void> _handleDeliverResult(
    String result,
    PurchaseDetails purchaseDetails,
    String orderId,
    String receipt, {
    bool retry = false,
  }) async {
    logger.i('deliver product : $orderId, ${purchaseDetails.productID} retry:$retry result:$result');
    if (result == YBDIapPayResult.DELIVER_SUCCESS) {
      // 交易成功的订单
      YBDCommonUtil.storeAppleOrder(
        orderId,
        receipt,
        YBDIapPurchaseStatus.SUCCESS,
        thirdPartyId: purchaseDetails.purchaseID ?? '',
        retry: retry,
        purchase: YBDIapUtil.purchase2String(purchaseDetails),
        productId: purchaseDetails.productID,
      );

      // 完成购买
      await completePurchase(purchaseDetails, orderId: orderId);
      //修改预订单状态状态
      await YBDDataBaseUtil.getInstance().inAppOrderUpdate(
        YBDDBOrderPurchaseEntity(null, orderId, purchaseDetails.productID, DateTime.now().millisecondsSinceEpoch, null,
            null, null, '1', '2'),
      );
      // await YBDDataBaseUtil.getInstance().orderQueryAll();
      // 删除本地订单记录
      // YBDOrderManager.instance.removeOrderRecord(purchaseDetails.productID);
    } else {
      // 支付成功分发内容失败的订单
      YBDCommonUtil.storeAppleOrder(
        orderId,
        receipt,
        YBDIapPurchaseStatus.LOST,
        result: '$result',
        retry: retry,
        thirdPartyId: purchaseDetails.purchaseID ?? '',
        purchase: YBDIapUtil.purchase2String(purchaseDetails),
        productId: purchaseDetails.productID,
      );
    }
  }

  /// 调分发购买内容接口
  @override
  Future<String> deliverProduct({
    String? productId,
    String? orderId,
    String? receipt,
    String? transactionId,
  }) async {
    // 发豆子的结果
    String result;

    bool isLogin = await YBDUserUtil.isUserLoggedIn();
    if (isLogin) {
      // 用户登录后才发豆子
      // [orderId] 没有值时设置为空订单，兼容补单的场景
      result = await ApiHelper.requestDeliverApplePurchase(
        context,
        receipt: receipt,
        orderId: orderId ?? '',
        transactionId: transactionId ?? '',
      );
    } else {
      result = YBDIapPayResult.NOT_LOGIN;
    }

    logger.i('iap purchase result $result');
    if (result == YBDIapPayResult.DELIVER_SUCCESS) {
      if (null != statusCallback) {
        statusCallback!(IapStatus.Success, productId);
      }

      // 分发内容成功的埋点
      YBDAnalyticsUtil.logEvent(
        YBDAnalyticsEvent(
          YBDEventName.PURCHASE,
          itemID: productId,
          returnExtra: YBDAnalyticsUtil.DELIVERY_SUCCESS,
          transactionId: orderId,
        ),
      );
    } else {
      if (null != statusCallback) {
        statusCallback!(IapStatus.DeliverError, productId);
      }

      // 分发内容失败的埋点
      YBDAnalyticsUtil.logEvent(
        YBDAnalyticsEvent(
          YBDEventName.PURCHASE,
          itemID: productId,
          returnExtra: YBDAnalyticsUtil.DELIVERY_FAILED,
          transactionId: orderId,
        ),
      );
    }

    return result;
  }

  /// 调用场景
  /// 1. 完成购买
  ///    完成前要分发购买内容: [_deliverProduct]
  ///    完成后清除本地记录: [YBDOrderManager.instance.removeOrderRecord]
  /// 2. app 启动时检查到上次购买错误时调用
  /// 3. 购买过程发生错误会调用
  ///
  Future<void> completePurchase(
    PurchaseDetails purchaseDetails, {
    bool isError = false,
    String orderId = '',
  }) async {
    // TODO: 测试代码 Google 支付验证完之后添加处理逻辑
    // if (Platform.isAndroid) {
    //   if (!_kAutoConsume && purchaseDetails.productID == _kConsumableId) {
    //     await _connection.consumePurchase(purchaseDetails);
    //   }
    // }
    // logger.i(
    // 'completePurchase isError:$isError applicationUsername:${purchaseDetails?.skPaymentTransaction?.payment?.applicationUsername}');
    // 完成交易
    if (purchaseDetails.pendingCompletePurchase) {
      await connection!.completePurchase(purchaseDetails);
    }

    if (isError) {
      // complete 错误订单的埋点
      YBDAnalyticsUtil.logEvent(
        YBDAnalyticsEvent(
          YBDEventName.PURCHASE,
          itemID: purchaseDetails.productID,
          returnExtra: YBDAnalyticsUtil.COMPLETE_ERROR_ORDER,
          transactionId: orderId,
        ),
      );
    } else {
      // complete 成功订单的埋点
      YBDAnalyticsUtil.logEvent(
        YBDAnalyticsEvent(
          YBDEventName.PURCHASE,
          itemID: purchaseDetails.productID,
          returnExtra: YBDAnalyticsUtil.COMPLETE_NORMAL_ORDER,
          transactionId: orderId,
        ),
      );
    }
  }

  /// 处理挂起状态的购买
  Future<void> handlePurchasePending(PurchaseDetails purchaseDetails) async {
    //final localRecord = await YBDOrderManager.instance.recordWithProductId(purchaseDetails.productID);
    // logger.i(
    //     'handlePurchasePending applicationUsername:${purchaseDetails?.skPaymentTransaction?.payment?.applicationUsername}');
    final orderId = await getIosOrderId(purchaseDetails);
    final receipt = await YBDIapUtil.getReceipt(connection, purchaseDetails, orderId);
    // 不处理, 只记录到 firebase 方便跟踪漏单
    // pending 状态的订单
    YBDCommonUtil.storeAppleOrder(
      orderId,
      receipt,
      YBDIapPurchaseStatus.PENDING,
      thirdPartyId: purchaseDetails.purchaseID ?? '',
      purchase: YBDIapUtil.purchase2String(purchaseDetails),
      productId: purchaseDetails.productID,
    );
  }
  void handlePurchasePending8isVDoyelive(PurchaseDetails purchaseDetails)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理错误状态的购买
  Future<void> handlePurchaseError(PurchaseDetails purchaseDetails) async {
    // logger.i(
    //     'handlePurchaseError applicationUsername:${purchaseDetails?.skPaymentTransaction?.payment?.applicationUsername}');
    //final localRecord = await YBDOrderManager.instance.recordWithProductId(purchaseDetails.productID);
    final error = 'requestCancelApplePurchase: ${purchaseDetails.error}';
    final productId = 'productId : ${purchaseDetails.productID}';
    final orderId = await getIosOrderId(purchaseDetails);
    final receipt = 'receipt : ${await YBDIapUtil.getReceipt(connection, purchaseDetails, orderId)}';
    YBDCrashlyticsUtil.instance!.report(
      '$error, $productId, orderId : $orderId, $receipt',
      extra: 'apple purchase error: $error',
      module: YBDCrashlyticsModule.PURCHASE,
    );

    // 调取消购买的接口
    YBDOrderManager.instance!.requestCancelOrderId(
      orderId,
      context,
      productId: purchaseDetails.productID,
    );

    if (null != statusCallback) {
      statusCallback!(IapStatus.PurchaseError, purchaseDetails.productID);
    }

    // 完成错误状态的购买
    await completePurchase(purchaseDetails, isError: true, orderId: orderId);

    // 删除本地订单记录
    // YBDOrderManager.instance.removeOrderRecord(purchaseDetails.productID);

    // 支付错误的订单
    YBDCommonUtil.storeAppleOrder(
      orderId,
      receipt,
      YBDIapPurchaseStatus.ERROR_CLEAR,
      thirdPartyId: purchaseDetails.purchaseID ?? '',
      purchase: YBDIapUtil.purchase2String(purchaseDetails),
      productId: purchaseDetails.productID,
    );
  }
  void handlePurchaseErrorVVBtloyelive(PurchaseDetails purchaseDetails)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///获取ios 预订单ID
  ///策略：先获取透传的，如果透传没有，获取数据库保存的
  Future<String> getIosOrderId(PurchaseDetails purchase, {bool isPurchase = false}) async {
    String transactionIdentifier = purchase.purchaseID ?? '';
    //通过第三方Id获取绑定的预订单id
    var orderID = await YBDPurchaseUtil.getOrderIdByThirdId(transactionIdentifier);
    logger.i('getIosOrderId orderID:$orderID transactionIdentifier:$transactionIdentifier isPurchase:$isPurchase');
    if (orderID != null && orderID.isNotEmpty) {
      return orderID;
    }

    //通过透传方式获取预订单Id
    // orderID = purchase?.skPaymentTransaction?.payment?.applicationUsername;
    logger.i('applicationUsername orderID:$orderID');

    //通过productId获取Id
    if (orderID == null || orderID.isEmpty) {
      orderID = await YBDPurchaseUtil.getIosPayOrderId(purchase.productID);
      logger.i('getIosPayOrderId orderID:$orderID');
    }

    //绑定第三方id 只有Purchase才需要绑定第三方订单
    if (isPurchase == true) {
      bindingOrderId(purchase, orderID!, transactionIdentifier);
    }
    return orderID!;
  }
  void getIosOrderIdT5zZnoyelive(PurchaseDetails purchase, {bool isPurchase = false})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //绑定第三方id
  Future<void> bindingOrderId(PurchaseDetails purchase, String orderID, String transactionIdentifier) async {
    await YBDDataBaseUtil.getInstance().inAppBindingOrderId(
      YBDDBOrderPurchaseEntity(
        null,
        orderID,
        purchase.productID,
        DateTime.now().millisecondsSinceEpoch,
        null,
        null,
        transactionIdentifier,
        null,
        null,
      ),
    );
  }
}
