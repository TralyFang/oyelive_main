
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:in_app_purchase_ios/in_app_purchase_ios.dart';
import 'package:oyelive_main/common/service/iap_service/iap_ybd_def.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';

import '../../../module/api_ybd_helper.dart';
import '../../util/log_ybd_util.dart';
import 'entity/apple_ybd_pay_config_entity.dart';

/// 支付工具类
class YBDIapUtil {
  /// 按产品价格升序排列
  static List<ProductDetails> sortProducts(List<ProductDetails> source) {
    if (null == source || source.isEmpty) {
      return source;
    }

    source.sort((a, b) {
      return double.parse(a.rawPrice.toString()).compareTo(double.parse(b.rawPrice.toString()));
    });
    return source;
    // List<ProductDetails> result = [];
    // result.addAll(source);
    //
    // for (int i = 0; i < result.length; i++) {
    //   for (int j = i; j < result.length; j++) {
    //     int priceA = 0;
    //     int priceB = 0;
    //
    //     try {
    //       if (Platform.isIOS) {
    //         priceA = int.parse(result[i].skProduct.price);
    //         // priceB = int.parse(result[j].skProduct.price);
    //       } else {
    //         priceA = result[i].skuDetail.priceAmountMicros;
    //         priceB = result[j].skuDetail.priceAmountMicros;
    //       }
    //     } catch (e) {
    //       logger.v('get product price error : $e');
    //     }
    //
    //     if (priceA > priceB) {
    //       var tmp = result[i];
    //       result[i] = result[j];
    //       result[j] = tmp;
    //     }
    //   }
    // }

    // return result;
  }

  /// 传给预下单接口的商品价格
  /// 默认值为 1，如果为 0 预下单会失败
  static double orderPriceWithProduct(ProductDetails product) {
    var price = doublePriceWithProduct(product);

    if (null == price || price < 0) {
      price = 1;
      logger.i('===iap default price : $price');
    }

    logger.i('===order price : $price');
    return price;
  }

  /// 从商品详情解析数字格式的价格
  static double? doublePriceWithProduct(ProductDetails product) {
    double? price;
    try {
      if (Platform.isIOS) {
        // 取 AppStore 返回的价格
        price = product.rawPrice;
        logger.i('===iap AppStore price : $price');
      } else {
        // TODO: 测试代码
        // 安卓支付设置 [money] 参数
      }
    } catch (e) {
      logger.i('get product price error : $e');
    }

    logger.i('===iap return price : $price');
    return price;
  }

  /// 从商品详情获取货币码 e.g. USD for US locale
  static String currencyCodeWithProduct(ProductDetails product) {
    String result;
    if (Platform.isIOS) {
      result = product.currencyCode;
    } else {
      // TODO: 测试代码
      // 安卓支付返回 currencyCode
      return '';
    }

    logger.i('===iap AppStore currency code : $result');
    return result;
  }

  /// 生成默认的购买产品列表
  static List<YBDApplePayConfigDataItem?> defaultProductList() {
    var idArr = ['208beans', '1048beans', '2098beans', '6298beans', '10498beans', '20998beans'];

    // 接口没有返回商品列表时时构建默认商品列表
    final List<YBDApplePayConfigDataItem?> result = idArr.map((e) {
      var item = YBDApplePayConfigDataItem();
      item.productId = e;
      return item;
    }).toList();

    logger.i('===iap use default product list');
    return result;
  }

  /// 打印接口返回的产品列表
  static void printProductItemList(List<YBDApplePayConfigDataItem?> items) {
    items.forEach((e) {
      assert(e != null, 'item is null');
      logger.i('===iap id : ${e!.productId}, money : ${e.money}, value : ${e.value}');
    });
  }

  /// 购买成功后把订单详情写入到Info级别的日志
  static void printProductWithPurchaseDetail(PurchaseDetails purchaseDetail, List<ProductDetails> products) {
    if (products == null || products.isEmpty) {
      logger.i('printProductWithPurchaseDetail  products is null');
      return;
    }
    final product = products.firstWhere((element) => element.id == purchaseDetail.productID, orElse: null);

    if (null != product) {
      if (Platform.isIOS) {
        final price = 'formatted price : ${product.price}';
        final skPrice = 'sk price : ${product.rawPrice}';
        final currencySymbol = 'currencySymbol : ${product.currencySymbol}';
        final currencyCode = 'currencyCode : ${product.currencyCode}';
        logger.i('$price, $skPrice, $currencySymbol, $currencyCode');
      } else {
        // TODO: 添加安卓支付的产品信息
        // logger.w('no android pay product info');
      }
    } else {
      logger.e('===not found purchased product info with id : ${purchaseDetail.productID}');
    }
  }

  /// 请求商品 id 列表
  static Future<List<YBDApplePayConfigDataItem?>> requestProductIdList(BuildContext? context) async {
    logger.i('_requestProductIdList');
    List<YBDApplePayConfigDataItem?> result = [];
    if (Platform.isIOS) {
      final YBDApplePayConfigEntity? applePayConfig = await ApiHelper.queryApplePayConfig(context);

      try {
        List<YBDApplePayConfigDataItem?> productIdList = applePayConfig?.data?.first?.items ?? [];
        result.addAll(productIdList);
      } catch (e) {
        logger.i('add product list error : $e');
      }

      if (result.isEmpty) {
        logger.i('===iap result is empty');
        result = YBDIapUtil.defaultProductList();
      }
    } else {
      // TODO: 测试代码
      // 安卓平台获取产品 ID 列表
    }
    logger.i('product id list : $result');
    return result;
  }

  /// 根据 id 列表到商店拉取商品详情列表
  static Future<List<ProductDetails>> queryProductList(
    InAppPurchase connection,
    List<String?> productList,
  ) async {
    if (null == productList || productList.isEmpty) {
      // 返回空的商品列表
      return [];
    }

    List<ProductDetails> result = [];

    // 根据产品 id 查询对应的商品信息
    var productDetailResponse = await connection.queryProductDetails(productList.toSet().removeNulls());

    if (productDetailResponse.error != null) {
      logger.i('product detail response error: ${productDetailResponse.error!.details}');
      // 请求商品错误或请求到空的商品列表，设置空的商品列表
      result = [];
    } else {
      if (productDetailResponse.productDetails.isNotEmpty) {
        // 商店返回商品列表
        result = productDetailResponse.productDetails;
        logger.i('productDetailResponse result : ${result.length}');
      } else {
        // 商店返回空的商品列表
        logger.i('product detail list is empty');
        result = [];
      }
    }

    result.forEach((element) => logger.i('product name : ${element.title}'));
    return result;
  }

  /// 刷新获取收据信息
  static Future<String?> _getReceiptData(InAppPurchase? connection) async {
    if (Platform.isIOS) {
      // 获取最新的验证数据
      InAppPurchaseIosPlatformAddition iosAddition =
          InAppPurchase.instance.getPlatformAddition<InAppPurchaseIosPlatformAddition>();
      final data = await iosAddition.refreshPurchaseVerificationData();
      return data?.serverVerificationData;
    } else if (Platform.isAndroid) {
      assert(false, 'not implement Google Pay');
      // PurchaseVerificationData data = purchaseDetails.verificationData;
      return null;
    } else {
      logger.i('not support platform');
      return null;
    }
  }

  /// 从购买详情获取收据信息
  static String _receiptFromPurchase(PurchaseDetails purchaseDetails) {
    // 获取收据信息
    final data = purchaseDetails.verificationData.localVerificationData;
    return data;
  }

  /// 把[PurchaseDetails]转换为字符串
  static String purchase2String(PurchaseDetails purchase) {
    if (null == purchase) {
      return '';
    }

    Map<String, String?> map = {
      'purchaseID': purchase.purchaseID,
      'productID': purchase.productID,
      'localVerificationData': purchase.verificationData.localVerificationData,
      'transactionDate': purchase.transactionDate,
      // 'skPaymentTransaction': purchase.verificatio,
    };

    return map.toString();
  }

  //获取凭证
  //先从purchaseDetails里面获取，获取不到再从refreshPurchaseVerificationData里面获取
  static Future<String?> getReceipt(InAppPurchase? connection, PurchaseDetails purchaseDetails, String orderId,
      {bool retry = false}) async {
    String? receipt;
    receipt = _receiptFromPurchase(purchaseDetails);
    if (receipt == null || receipt.isEmpty || receipt == '') {
      receipt = await _getReceiptData(connection);
      // 记录收据异常的事件
      YBDCommonUtil.storeAppleOrder(
        orderId,
        receipt,
        YBDIapPurchaseStatus.EMPTY_RECEIPT,
        retry: retry,
        purchase: YBDIapUtil.purchase2String(purchaseDetails),
        productId: purchaseDetails.productID,
      );
    }
    return receipt;
  }
}
