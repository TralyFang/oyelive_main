

import 'dart:async';

import '../../util/log_ybd_util.dart';

/// 首页引导 ID
const TASK_SHOW_HOME_PAGE_GUIDE = 101;

/// 超时时间为 3 分钟
const TASK_SHOW_HOME_PAGE_GUIDE_TIMEOUT = 60 * 3;

/// 版本升级弹框 ID
const TASK_CHECK_UPGRADE = 102;

/// 避免和前一个高优先级的任务同时超时导致一直不执行
const TASK_CHECK_UPGRADE_TIMEOUT = 60 * 3 + 5;

/// 配置活动弹框 ID
const TASK_CONFIG_ACTIVITY = 104;

/// 避免和前一个高优先级的任务同时超时导致一直不执行
const TASK_CONFIG_ACTIVITY_TIMEOUT = 60 * 3 + 10;

/// 签到弹框 ID
const TASK_DAILY_CHECK_IN = 105;

/// 避免和前一个高优先级的任务同时超时导致一直不执行
const TASK_DAILY_CHECK_IN_TIMEOUT = 60 * 3 + 15;

/// 充值送礼活动弹框 ID
const TASK_TOPUP_GIFT_ACTIVITY = 106;

/// 任务超时时间为 2 分钟
/// 弹框任务超时后自动标记为 [_TaskStatus.End]
/// 然后开始下一个弹框任务
const TASK_DEFAULT_TIME_OUT = 60 * 2;

/// 计时器最大轮询次数，超过最大次数后自动销毁计时器
const TASK_TIMER_CANCEL_TIME = 60 * 10;

/// 待处理任务的服务类
/// 按显示优先级调整任务 ID，ID 越小优先级越高
/// 在[_configTaskList]方法里弹框任务
/// [start] 开始广播任务状态，一组任务只需要调用一次
/// [onEvent] 监听任务状态
/// [autoEnd] 设置弹框任务超时时间
/// [waiting] 修改任务状态为 [_TaskStatus.Waiting]
/// [end] 结束单个任务，所有任务都结束时销毁定时器
class YBDTaskService {
  static YBDTaskService? _taskService;

  static YBDTaskService? get instance {
    if (null == _taskService) {
      _taskService = YBDTaskService();
    }
    return _taskService;
  }

  /// 事件广播
  /// 广播可以显示的弹框 ID
  // ignore: close_sinks
  StreamController<int> _controller = StreamController<int>.broadcast();

  /// 任务状态可以执行时会广播该任务的 id
  Stream<int> get onEvent => _controller.stream;

  /// 轮询任务列表的定时器
  /// 任务列表为空时自动销毁定时器
  Timer? _timer;

  /// 计时器轮询次数
  var _count = 0;

  /// 任务列表
  List<_YBDTask> _taskList = [];

  /// 任务改为等待状态
  void waiting(int taskId) {
    _taskList.forEach((element) {
      if (element.taskId == taskId) {
        element.status = _TaskStatus.Waiting;
      }
    });
    _executor();
  }
  void waiting2XcsSoyelive(int taskId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 超时后自动结束任务
  void autoEnd(int taskId, {int timeOut = TASK_DEFAULT_TIME_OUT}) {
    _taskList.forEach((element) {
      if (element.taskId == taskId) {
        // 超时后自动标记为完成
        Future.delayed(Duration(seconds: timeOut), () {
          end(taskId);
        });
      }
    });
  }
  void autoEndp1fI0oyelive(int taskId, {int timeOut = TASK_DEFAULT_TIME_OUT}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 任务改为结束状态
  void end(int taskId) {
    _taskList.removeWhere((element) => element.taskId == taskId);
    logger.v('end task : $taskId taskList: ${_taskList.map((e) => "${e.taskId}:${e.status}").toList()}');
    _executor();
  }
  void endoJ41goyelive(int taskId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 启动服务
  void start() {
    logger.v('start task service');
    // 重新配置任务
    _configTaskList();
  }

  /// 重新配置任务任务队列
  /// 在 [start] 方法里调用
  void _configTaskList() {
    // 清空任务队列
    _taskList.clear();

    // 重新初始化任务
    _taskList = [
      _YBDTask(TASK_SHOW_HOME_PAGE_GUIDE, _TaskStatus.Init),
      _YBDTask(TASK_CHECK_UPGRADE, _TaskStatus.Init),
      _YBDTask(TASK_CONFIG_ACTIVITY, _TaskStatus.Init),
      _YBDTask(TASK_DAILY_CHECK_IN, _TaskStatus.Init),
      _YBDTask(TASK_TOPUP_GIFT_ACTIVITY, _TaskStatus.Init),
    ];
  }
  void _configTaskListbG5xMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 判断是否所有任务都已经结束
  bool _allTaskEnded() {
    bool result = true;

    _taskList.forEach((element) {
      logger.v('===check update ${element.taskId} ${element.status}');
      if (element.status != _TaskStatus.End) {
        result = false;
      }
    });

    return result;
  }

  /// 检查任务是否满足执行条件
  /// 广播可以执行的任务 ID
  void _executor() {
    _taskList.forEach((element) {
      // logger.v('task id : ${element.taskId}, ${element.status}');
      if (element.status == _TaskStatus.Waiting && _isPriorityTaskEnded(element.taskId)) {
        // 标记为正在执行状态
        element.status = _TaskStatus.Executing;
        _controller.sink.add(element.taskId);
        logger.v('can execute task id : ${element.taskId}');
      }
    });
  }
  void _executorp6K59oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 检查比该任务优先级高的任务是否都结束了
  bool _isPriorityTaskEnded(int taskId) {
    bool result = true;

    // ID 小的任务优先级高
    final priorityTask = _taskList.where((element) => element.taskId < taskId);
    priorityTask.forEach((element) {
      // 有优先级高的任务没有结束就不能开始该任务
      if (element.status != _TaskStatus.End) {
        result = false;
      }
    });
    return result;
  }
}

/// 任务状态
enum _TaskStatus {
  // 初始状态
  Init,
  // 等待前一个弹框消失
  Waiting,
  // 正在执行
  Executing,
  // 弹框消失
  End,
  // 未知状态
  Unknown,
}

/// 任务
class _YBDTask {
  int taskId;
  _TaskStatus status;

  _YBDTask(this.taskId, this.status);
}
