

import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/custom_dialog/custom_ybd_dialog.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';

class YBDRouteObserver extends RouteObserver<PageRoute<dynamic>> {
  @override
  void didPush(Route route, Route? previousRoute) {
    super.didPush(route, previousRoute);
    logger.v('obs push setting :${route.settings.name}');
    // if (route.settings?.name != null)
    YBDObsUtil.instance().push(currentRoute: route.settings.name, previousRoute: previousRoute?.settings.name);
  }
  void didPusheDtJboyelive(Route route, Route? previousRoute) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didPop(Route route, Route? previousRoute) {
    super.didPop(route, previousRoute);
    if (route.settings.name == null) return;
    logger.v('obs pop setting :${route.settings.name} ${previousRoute?.settings.name}');
    YBDObsUtil.instance().pop(currentRoute: route.settings.name ?? '', previousRoute: previousRoute?.settings.name);
    List nonGlobalPops = YBDCustomDialog.getInstance()!.nonGlobalPops;
    if (nonGlobalPops.isNotEmpty) {
      nonGlobalPops.forEach((e) {
        YBDCustomDialog.pop(tagWidge: e);
      });
      YBDCustomDialog.getInstance()!.nonGlobalPops.clear();
      logger.v('12.2---_nonGlobalPops:${YBDCustomDialog.getInstance()!.nonGlobalPops}');
    }
  }
  void didPopGS2xUoyelive(Route route, Route? previousRoute) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
