


//sqlcool db
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:oyelive_main/common/analytics/crashlytics_ybd_util.dart';
import 'package:oyelive_main/module/inbox/db/model_ybd_message.dart';
import 'package:oyelive_main/module/user/db/model_ybd_search_history.dart';
import 'package:oyelive_main/module/user/db/serarch_ybd_history_model_v2.dart';

import '../module/inbox/db/model_ybd_conversation.dart';

Db? messageDb;
final Db commonDb = Db();

Future<void> initDb({required Db db, required String userId}) async {
  // initialize the database
  var databasesPath = await getDatabasesPath();
  String dbPath = databasesPath + "/" + "oyetalk_$userId.sqlite";

  await db
      .init(
          path: dbPath,
          schema: [YBDConversationModel().conversationTable, YBDMessageModel().messageTable],
          absolutePath: true,
          verbose: true)
      .catchError((dynamic e) {
    YBDCrashlyticsUtil.instance!.report(
      'Error initializing the database: ${e.message}',
      extra: 'message db error: ${e.message}',
      module: YBDCrashlyticsModule.DB,
    );
    throw Exception("Error initializing the database: ${e.message}");
  });
  db.schema.describe();
}

Future<void> initCommonDb({required Db db}) async {
  // initialize the database
  var databasesPath = await getDatabasesPath();
  String dbPath = databasesPath + "/" + "oyetalk_data.sqlite";

  await db
      .init(
    path: dbPath,
    schema: [
      YBDSearchHistoryModel().searchHistoryTable,
      YBDSearchHistoryModelV2().searchHistoryTableV2,
    ],
    absolutePath: true,
    verbose: true,
  )
      .catchError(
    (dynamic e) {
      YBDCrashlyticsUtil.instance!.report(
        e,
        extra: 'commonDb error: ${e.message}',
        module: YBDCrashlyticsModule.DB,
      );

      throw Exception("Error initializing the database: ${e.message}");
    },
  );
  db.schema.describe();
}
