




/*
 * @Author: William
 * @Date: 2022-05-12 09:41:32
 * @LastEditTime: 2022-06-17 17:12:16
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/widget/red_dot_alert.dart
 */
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

// 2.6.0个人主页编辑按钮红点和等级new标签(下个版本去除)
class YBDRedDotAlert extends StatelessWidget {
  final String? spKey;
  final bool dot;
  bool showAsId;
  YBDRedDotAlert({Key? key, this.spKey, this.dot = true, this.showAsId: false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Widget>(
        future: _redDotAlert(spKey!, dot: dot, showId: showAsId),
        builder: (context, snapshot) {
          return snapshot.data ?? Container();
        });
  }
  void buildAcalfoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<Widget> _redDotAlert(String spKey, {bool dot = true, bool showId = false}) async {
    if (spKey.isEmpty) return Container();
    var result = await YBDSPUtil.get(spKey);
    bool showed = result is bool && (result);

    print("sxdjioooo${result}");
    if (result != null && showId) showed = result.contains(YBDUserUtil.getUserIdSync.toString());
    print("222 ${showed} ${dot}");

    if (showId && showed) return Container();
    if (dot && showed) return Container();
    if (dot)
      return Container(
          width: 12.px,
          height: 12.px,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Color(0xffE6497A),
          ));
    return Container(
        alignment: Alignment.center,
        width: 42.px,
        height: 22.px,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(11.px as double), color: Color(0xffE6497A)),
        child: Text('New', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 16.sp)));
  }
  void _redDotAlert3Gobuoyelive(String spKey, {bool dot = true, bool showId = false})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
