



import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../util/log_ybd_util.dart';

class YBDSquareCheckBox extends StatefulWidget {
  bool? value = false;

  Function(bool?)? onChanged;

  YBDSquareCheckBox({Key? key, required this.value, this.onChanged}) : super(key: key);

  @override
  _YBDSquareCheckBoxState createState() => _YBDSquareCheckBoxState();
}

class _YBDSquareCheckBoxState extends State<YBDSquareCheckBox> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
          onTap: () {
            widget.value = !widget.value!;
            widget.onChanged!(widget.value);
            logger.v("YBDSquareCheckBox check ${widget.value}");
          },
          child: Padding(
              padding: EdgeInsets.only(
                  /* left: ScreenUtil().setWidth(20),
                right: ScreenUtil().setWidth(5),
                top: ScreenUtil().setWidth(10),
                bottom: ScreenUtil().setWidth(10),*/
                  ),
              child: widget.value!
                  ? Image.asset(
                      "assets/images/icon_square_agree.webp",
                      fit: BoxFit.contain,
                      width: ScreenUtil().setWidth(35),
                    )
                  : Image.asset(
                      "assets/images/icon_square.png",
                      fit: BoxFit.contain,
                      width: ScreenUtil().setWidth(35),
                    ))),
    );
  }
  void buildCeYOKoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
