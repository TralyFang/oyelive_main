

import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_entity.dart';

import '../../generated/json/base/json_convert_content.dart';

class YBDLocalAnimationEntity with JsonConvert<YBDLocalAnimationEntity> {
  List<YBDLocalAnimationItems?>? items;
}

class YBDLocalAnimationItems with JsonConvert<YBDLocalAnimationItems> {
  String? svga;
  String? mp3;
  int? animationId;
  int? index;
}