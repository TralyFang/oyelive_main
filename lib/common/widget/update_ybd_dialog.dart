


import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:url_launcher/url_launcher.dart';
import '../constant/const.dart';
import '../util/log_ybd_util.dart';

/// 版本更新弹框

class YBDUpdateDialog extends StatelessWidget {
  final String? versionName;
  final String? description;
  final bool forceUpdate;

  YBDUpdateDialog({
    required this.versionName,
    this.description,
    this.forceUpdate = false,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Material(
        type: MaterialType.transparency,
        child: Center(
          child: Container(
            width: ScreenUtil().setWidth(550),
            color: Colors.transparent,
            child: Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                Container(
                  width: ScreenUtil().setWidth(546),
                  margin: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(40)),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(ScreenUtil().setWidth(32)),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      // 版本更新描述
                      _descriptionContent(context),
                      Container(
                        margin: EdgeInsets.only(bottom: ScreenUtil().setWidth(30)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            // later 按钮
                            _laterBtn(context),
                            SizedBox(width: ScreenUtil().setWidth(10)),
                            // update now 按钮
                            _updateNowBtn(context),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                // 弹框顶部图片
                _dialogTitleImg(context),
              ],
            ),
          ),
        ),
      ),
      onWillPop: () async {
        // 版本更新弹框忽略物理返回键
        return Future.value(false);
      },
    );
  }
  void buildJyNn3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 弹框顶部的图片
  Widget _dialogTitleImg(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(550),
      height: ScreenUtil().setWidth(234),
      color: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            child: Image.asset(
              'assets/images/new_version_top.webp',
              width: ScreenUtil().setWidth(550),
              height: ScreenUtil().setWidth(234),
            ),
          ),
          Positioned(
            top: ScreenUtil().setWidth(136),
            child: Container(
              width: ScreenUtil().setWidth(550),
              child: Center(
                child: Text(
                  'v$versionName',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: ScreenUtil().setSp(24),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void _dialogTitleImgYxmrCoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 版本更新描述
  Widget _descriptionContent(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setWidth(180)),
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(44),
        vertical: ScreenUtil().setWidth(40),
      ),
      child: SingleChildScrollView(
        child: Text(
          '${description!.replaceAll(r'\n', '\n')}',
          style: TextStyle(
            fontSize: ScreenUtil().setSp(26),
            color: Color(0xff333333),
          ),
        ),
      ),
    );
  }
  void _descriptionContenthYe8eoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// later 按钮
  Widget _laterBtn(BuildContext context) {
    if (forceUpdate) {
      // 强制更新时隐藏 later 按钮
      return SizedBox();
    }

    return Container(
      width: ScreenUtil().setWidth(200),
      height: ScreenUtil().setWidth(64),
      child: TextButton(
        child: Text(
          translate('later'),
          style: TextStyle(
            color: Color(0xff484848),
            fontWeight: FontWeight.w400,
          ),
        ),
        onPressed: () {
          logger.v('clicked later btn');
          Navigator.of(context).pop();
        },
      ),
    );
  }
  void _laterBtnhy8Psoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// update now 按钮
  Widget _updateNowBtn(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(240),
      height: ScreenUtil().setWidth(64),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40))),
        gradient: LinearGradient(
          colors: [
            Color(0xff47CDCC),
            Color(0xff5E94E7),
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: TextButton(
        child: Text(
          translate('update_now'),
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
        onPressed: () {
          logger.v('clicked update now btn');
          if (Platform.isIOS) {
            launch(Const.APPLE_STORE);
          } else {
            launch(Const.GOOGLE_STORE);
          }
        },
      ),
    );
  }
  void _updateNowBtnINHbuoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
