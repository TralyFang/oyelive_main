



import 'dart:async';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/widget/marquee/marquee_ybd_translation.dart';

typedef CompleteCallback = void Function(List<Widget> children);

class Marquee extends StatefulWidget {
  final List<Widget> children;
  final Duration? duration;

  /// 切换item的间隔
  final Duration? switchDuration;
  final AxisDirection? direction;

  /// 完成一次轮播回调
  final CompleteCallback? onComplete;

  /// 是否隐藏轮播动画
  final bool hideAnimation;

  /// 只轮播一次
  final bool showOnce;
  Function? onItemTap;

  Marquee({
    Key? key,
    required this.children,
    this.duration,
    this.switchDuration,
    this.direction,
    this.onItemTap,
    this.onComplete,
    this.hideAnimation = false,
    this.showOnce = false,
  });

  @override
  State<StatefulWidget> createState() => _MarqueeState();
}

class _MarqueeState extends State<Marquee> {
  Timer? _timer;
  var _index = 0;
  List<Widget> _children = [];

  @override
  Widget build(BuildContext context) {
    return _itemWid(widget.direction ?? AxisDirection.left);
  }
  void buildrs48eoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    logger.v('widget.children length: ${widget.children.length}');
    _timer = Timer.periodic(widget.duration ?? Duration(milliseconds: 3000), (time) => _indexFun());
  }
  void initStatemx1Hfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _indexFun() {
    ++_index;
    _index = _index % widget.children.length;

    if (_index == 0 && widget.onComplete != null) {
      // 完成一次轮播回调
      widget.onComplete!(widget.children);

      if (widget.showOnce) {
        // 只轮播一次
        return;
      }
    }

    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    }
  }
  void disposeGSF81oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _itemWid(direction) {
    if (_children != null) {
      _children.clear();
    }
    int i = 0;
    _children.addAll(widget.children.map((e) {
      i++;
      return Container(
          key: ValueKey<int>(i),
          child: Listener(
              child: e,
              onPointerDown: (d) {
                logger.v('tp marquee tap click');
                widget.onItemTap != null ? widget.onItemTap!(i) : null;
              }));
    }).toList());
    logger.v('_children length: ${_children.length}, ${widget.children.length}, $_index');
    if (_children.length <= _index) {
      _index = _children.length - 1;
    }

    Widget item = widget.hideAnimation
        ? _children[_index]
        : AnimatedSwitcher(
            duration: widget.switchDuration ?? widget.duration ?? Duration(milliseconds: 500),
            child: _children[_index],
            transitionBuilder: (Widget child, Animation<double> animation) {
              return MarqueeTransition(child: child, direction: widget.direction, position: animation);
            });

    return ClipRRect(borderRadius: BorderRadius.all(Radius.circular(1.0)), child: SizedBox(child: item));
  }
  void _itemWidDREjhoyelive(direction) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
