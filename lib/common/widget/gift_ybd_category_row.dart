



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/room/entity/gift_ybd_package_entity.dart';

/// 选中tab的回调函数
typedef SelectTap = Function(int index);

class YBDGiftCategoryRow extends StatelessWidget {
  YBDGiftCategoryRow({
    Key? key,
    this.giftPages,
    this.selectTap,
    this.viewingTabIndex = 0,
    this.normalColor,
    this.selectedColor,
  }) : super(key: key);

  /// 分类列表，最后一个分类是背包
  /// 数组元素：其他分类礼物+背包礼物
  final List<YBDGiftPackageRecord?>? giftPages;

  /// 选中tab的回调函数
  final SelectTap? selectTap;

  /// 当前选中的tab
  final int? viewingTabIndex;

  /// 标题默认颜色
  final Color? normalColor;

  /// 标题选中颜色
  final Color? selectedColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.px,
      child: Row(
        children: [
          SizedBox(width: 8.px),
          Expanded(
            child: _otherCategoryRow(),
          ),
          SizedBox(width: 8.px),
          Container(
            width: 1.px,
            height: 26.px,
            color: Colors.white.withOpacity(0.3),
          ),
          _packageTab(),
        ],
      ),
    );
  }
  void builddBA85oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 其他分类列表栏
  Widget _otherCategoryRow() {
    // 数组元素：其他分类礼物+背包礼物, 只有一条数据时没有其他分类礼物
    if (null == giftPages || giftPages!.length <= 1) {
      return SizedBox();
    }

    return ListView.separated(
      scrollDirection: Axis.horizontal,
      itemBuilder: (_, index) => GestureDetector(
        onTap: () {
          selectTap?.call(index);
        },
        child: _YBDTitle(
          index: index,
          title: giftPages![index]!.name,
          selectedColor: _titleColor(true),
          normalColor: _titleColor(false),
          selected: index == viewingTabIndex,
        ),
      ),
      separatorBuilder: (_, index) => SizedBox(
        width: 0,
      ),
      itemCount: giftPages!.length - 1,
    );
  }
  void _otherCategoryRowmeZU0oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 背包tab
  Widget _packageTab() {
    return GestureDetector(
      onTap: () {
        logger.v('clicked baggage tab icon');
        selectTap?.call(giftPages!.length - 1);
      },
      child: Container(
        width: 106.px,
        decoration: BoxDecoration(),
        child: Center(
          child: SizedBox(
            width: 26.px,
            child: Image.asset(
              'assets/images/gift_baggage_tab_icon.webp',
              color: _titleColor(viewingTabIndex == giftPages!.length - 1),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
  void _packageTabkzH9uoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 标题颜色，默认房间礼物弹框的样式
  Color _titleColor(bool selected) {
    if (selected) {
      return selectedColor ?? Color(0xff49C9CE);
    }

    return normalColor ?? Colors.white;
  }
}

/// 分类栏的item
class _YBDTitle extends StatelessWidget {
  _YBDTitle({
    Key? key,
    this.index = 0,
    this.title = '',
    this.normalColor,
    this.selectedColor,
    this.selected = false,
  }) : super(key: key);

  /// tab索引
  final int index;

  /// 是否选中tab
  final bool selected;

  /// tab标题
  final String? title;

  /// 标题默认颜色
  final Color? normalColor;

  /// 标题选中颜色
  final Color? selectedColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 120.px,
      decoration: BoxDecoration(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title!,
            style: TextStyle(
              fontSize: 22.sp,
              color: selected ? selectedColor : normalColor,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
  void build8DNG4oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
