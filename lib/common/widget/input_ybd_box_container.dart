



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 输入框
class YBDInputBoxContainer extends StatefulWidget {
  final onChange;
  final IconData? icon;
  final String? hintText;
  final controller;

  YBDInputBoxContainer({Key? key, this.onChange, this.controller, this.icon, this.hintText}) : super(key: key);

  @override
  YBDInputBoxContainerBuilder createState() => new YBDInputBoxContainerBuilder();
}

class YBDInputBoxContainerBuilder extends State<YBDInputBoxContainer> {
  /*TextEditingController emailController = new TextEditingController(); //声明controller*/
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0, bottom: 2.0),
      // margin: EdgeInsets.only(bottom: 30.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        border: Border.all(
          width: 1.0,
          style: BorderStyle.solid,
          color: Colors.white.withOpacity(0.2),
        ),
      ),
      child: Container(
        padding: EdgeInsets.only(
          right: ScreenUtil().setWidth(10),
          left: ScreenUtil().setWidth(10),
        ),
        child: TextField(
          controller: widget.controller != null ? widget.controller : null,
          onChanged: widget.onChange != null ? widget.onChange : null,
          textAlign: TextAlign.left,
          style: TextStyle(
            color: Colors.white,
            fontSize: ScreenUtil().setSp(28),
          ),
          decoration: InputDecoration(
            border: InputBorder.none,
            icon: widget.icon != null ? Icon(widget.icon) : null,
            hintText: widget.hintText != null ? widget.hintText : '',
            hintStyle: TextStyle(
              color: Colors.white.withOpacity(0.2),
              fontSize: ScreenUtil().setSp(28),
            ),
          ),
        ),
      ),
    );
  }
  void buildiQIdKoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
