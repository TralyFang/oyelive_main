


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 网络错误缺页面
class YBDNetworkErrorView extends StatelessWidget {
  /// 缺省图
  final String img;

  /// 缺省图底部的文字
  final String text;

  /// 文字颜色
  final Color textColor;

  YBDNetworkErrorView({
    this.img = 'assets/images/network_error.png',
    this.text = '',
    this.textColor = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    // 宽度和高度默认是父组件的宽度和高度
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            img,
            width: ScreenUtil().setWidth(382),
          ),
          SizedBox(height: ScreenUtil().setWidth(30)),
          Text(
            text,
            style: TextStyle(color: textColor),
          ),
        ],
      ),
    );
  }
  void buildOejb0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
