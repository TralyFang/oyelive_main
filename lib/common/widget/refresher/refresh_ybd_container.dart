


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';

class YBDRefreshContainer extends StatelessWidget {
  final Widget? child;
  final Function? onRefresh;
  final Function? onLoading;
  final bool? noMoreData;
  final bool? enablePullDown;
  final bool? enablePullUp;
  final RefreshController? controller;

  const YBDRefreshContainer({
    this.child,
    this.controller,
    this.onRefresh,
    this.onLoading,
    this.noMoreData,
    this.enablePullDown,
    this.enablePullUp,
    Key? key,
  }) : super(key: key);

  void _onRefresh() async {
    await onRefresh?.call();
    debugPrint('after onRefresh');

    // if failed,use refreshFailed()
    controller!.refreshCompleted();
    controller!.loadComplete();
  }
  void _onRefresh6o7gNoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _onLoading() async {
    await onLoading?.call();
    debugPrint('after onLoading');
    // if failed, use loadFailed(), if no data return, use LoadNoData()
    controller!.loadComplete();
  }
  void _onLoadingrMqFQoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    if (noMoreData == true) {
      controller!.loadNoData();
    }

    return Container(
      child: SmartRefresher(
        enablePullDown: enablePullDown!,
        enablePullUp: enablePullUp!,
        header: YBDMyRefreshIndicator.myHeader,
        footer: YBDMyRefreshIndicator.myFooter,
        controller: controller!,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: child,
      ),
    );
  }
  void buildbzNOdoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
