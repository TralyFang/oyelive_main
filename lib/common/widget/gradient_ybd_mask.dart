




/*
 * @Author: William
 * @Date: 2022-05-12 17:51:51
 * @LastEditTime: 2022-05-13 09:50:18
 * @LastEditors: William
 * @Description: Gradient Mask
 * @FilePath: /oyetalk-flutter/lib/common/widget/gradient_mask.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

class YBDGradientMask extends StatelessWidget {
  /// mask height
  final int? height;

  /// 向下变透明？
  final bool up2down;

  /// 遮罩黑色透明度
  final double? black;

  const YBDGradientMask({Key? key, this.height, this.up2down = true, this.black}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: Container(
        height: height?.px,
        width: ScreenUtil().screenWidth,
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [Colors.black.withOpacity(black!), Colors.transparent],
          begin: up2down ? Alignment.topCenter : Alignment.bottomCenter,
          end: up2down ? Alignment.bottomCenter : Alignment.topCenter,
        )),
      ),
    );
  }
  void buildfJysfoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
