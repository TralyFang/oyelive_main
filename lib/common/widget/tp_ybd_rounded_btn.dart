



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

/// 带点击效果的圆角按钮
/// 支持不可点击的样式
/// 支持渐变色背景和常规背景
class YBDTPRoundedBtn extends StatefulWidget {
  /// 按钮宽度
  final double width;

  /// 按钮高度
  final double height;

  /// 按钮标题
  final String title;

  /// 按钮标题字体
  final double titleSp;

  /// 标题颜色
  Color titleColor;

  /// 不可点击的标题颜色
  Color disableTitleColor;

  /// 可点击状态的渐变背景色
  LinearGradient? enableGradient;

  /// 不可点击状态的渐变背景色
  LinearGradient? disableGradient;

  /// 可点击状态的背景色
  final Color enableBackground;

  /// 不可点击状态的背景色
  final Color disableBackground;

  /// 是否可点击
  final bool enable;

  /// 是否为渐变背景色
  bool isGradient;

  /// 点击事件的回调函数
  final Function? onPressed;

  YBDTPRoundedBtn({
    this.width = 0,
    this.height = 0,
    this.title = '',
    this.titleSp = 28,
    this.titleColor = Colors.black,
    this.disableTitleColor = Colors.black,
    this.enableGradient,
    this.disableGradient,
    this.enableBackground = Colors.white,
    this.disableBackground = Colors.grey,
    this.enable = true,
    this.isGradient = false,
    this.onPressed,
    Key? key,
  }) : super(key: key) {
    if (enable) {
      if (enableGradient != null) {
        isGradient = true;
      }
    } else {
      if (disableGradient != null) {
        isGradient = true;
      }
    }
  }

  @override
  State<YBDTPRoundedBtn> createState() => _YBDTPRoundedBtnState();
}

class _YBDTPRoundedBtnState extends State<YBDTPRoundedBtn> {
  /// 点击状态的标记
  bool _isTapped = false;

  @override
  Widget build(BuildContext context) {
    // 按钮标题
    var title = Text(
      widget.title,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: widget.titleSp,
        color: widget.enable ? widget.titleColor : widget.disableTitleColor,
        fontWeight: FontWeight.w600,
      ),
    );

    Widget resultWidget;

    if (widget.isGradient) {
      // 渐变背景色
      resultWidget = _gradientContainer(title);
    } else {
      // 普通背景色
      resultWidget = _bgColorContainer(title);
    }

    if (_isTapped) {
      logger.v('_tappedMask');
      // 添加遮罩
      resultWidget = _tappedMask(resultWidget);
    }

    if (widget.enable) {
      // 添加手势
      resultWidget = GestureDetector(
        onTapDown: _onTapDown,
        onTapCancel: _onTapCancel,
        onTapUp: _onTapUp,
        onTap: () {
          logger.v('onTap');
          widget.onPressed?.call();
        },
        child: resultWidget,
      );
    }

    return resultWidget;
  }
  void build2ZgQSoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击按钮显示透明样式
  void _onTapDown(TapDownDetails tapDownDetails) {
    logger.v('onTapDown');
    setState(() {
      _isTapped = true;
    });
  }
  void _onTapDownhiFDwoyelive(TapDownDetails tapDownDetails) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消点击显示正常样式
  void _onTapCancel() {
    logger.v('onTapCancel');
    setState(() {
      _isTapped = false;
    });
  }
  void _onTapCanceldrmgooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击完成显示正常样式
  void _onTapUp(TapUpDetails tapUpDetails) {
    logger.v('onTapUp');
    setState(() {
      _isTapped = false;
    });
  }

  /// 点击的遮罩
  Widget _tappedMask(Widget child) {
    return Stack(
      children: [
        child,
        Container(
          alignment: Alignment.center,
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(widget.height)),
            color: Colors.black.withOpacity(0.2),
          ),
        ),
      ],
    );
  }
  void _tappedMask8jykloyelive(Widget child) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 渐变背景
  Widget _gradientContainer(Widget child) {
    return Container(
      alignment: Alignment.center,
      width: widget.width,
      height: widget.height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(widget.height)),
        gradient: widget.enable ? widget.enableGradient : widget.disableGradient,
      ),
      child: child,
    );
  }
  void _gradientContainercnYiWoyelive(Widget child) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 背景色
  Widget _bgColorContainer(Widget child) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(widget.height)),
        color: widget.enable ? widget.enableBackground : widget.disableBackground,
      ),
      width: widget.width,
      height: widget.height,
      child: child,
    );
  }
}
