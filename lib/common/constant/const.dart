


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Const {
  /// 根据 TEST_ENV 判断当前环境 true : 控制测试和灰度   上架包: false
  static const bool TEST_ENV = true;

  /// 生产地址
  static const PROD_URL = 'https://www.oyetalk.live/'; // 默认生产地址
  static const STATUS_PROD_URL = 'https://status.oyetalk.live/'; // 动态生产地址
  static const IM_PROD_URL = 'https://im.oyetalk.live'; // im 生产地址

  /// 定义声网 SDK 的 App ID
  //  测试环境声网 APP ID
  // static const AGORA_APP_ID = '7136fb8346f54384aa9a440281395d95';
  // 生产环境
  // static const AGORA_APP_ID = '4701fc6d91924e2791be9a9cf08dff0c';
  // 服务器已经做了区分测试环境
  static const AGORA_APP_ID = TEST_ENV ? '4d8f08b5fc824eeaadea1359956a9fc5' : '4701fc6d91924e2791be9a9cf08dff0c';

  /// 苹果 appId
  static const String AF_APP_ID_APPLE = '1524257092';
  static const String AF_APP_ID_ANDROID = 'com.oyetalk.tv';

  /// Card 常量
  static const int SWIPE_LEFT = 0;
  static const int SWIPE_RIGHT = 1;
  static const Color CARD_COLOR = Color.fromRGBO(121, 114, 173, 1.0);
  static const Color CARD_PAGE_COLOR = Color.fromRGBO(106, 94, 175, 1.0);
//光标颜色
  static const Color INPUT_CURSOR_COLOR = Color(0xff7dfaff);

  /// sp 常量
  static const SP_INIT_TIMESTAMP = "sp_init_timestamp"; // 记录初始时间戳，判断是否新用户
  static const SP_USER_INFO = "sp_user_info"; // 当前登录用户信息
  static const SP_PORTAL_TYPE = "sp_portal_type"; // portalType  201 - 英语; 210 - 阿拉伯语, 默认201
  static const SP_JSESSION_ID = "sp_jsession_id"; // 用户鉴权token，30分钟失效。 每次调用接口都刷新
  static const SP_COOKIE_APP = "sp_cookie_app"; // socket鉴权token，登录请求返回
  static const SP_HTTP_AUTHORIZATION = "http_header_authorization";

  static const SP_THEME_COLOR = "sp_theme_color";
  static const SP_LOCALE = "sp_locale";
  static const SP_SAVE_PASSWORD = "sp_save_password"; // TODO 用户输入的密码需要加密存储
  static const SP_HOT_MARK = "sp_hot_mark"; // TODO 用户输入的密码需要加密存储
  static const SP_EASY_PAISA_ACCOUNT_NUMBER = "sp_easy_paisa_number";

  ///是否为需要修改密码的用户
  static const SP_NEED_FIX_PWD = "sp_need_fix_pwd";

  // static const SP_SELECT_COUNTRY = "sp_select_country_name";
  static const SP_SELECT_COUNTRY_CODE = "sp_select_country_code"; // 用户选择的国家
  static const SP_IP_COUNTRY_CODE = "sp_ip_country_code"; // IP所在国家
  static const SP_DEVICE_COUNTRY_CODE = "sp_device_country_code"; // 设备所在国家
  static const SP_DEVICE_CITY_NAME = "sp_device_city_name"; // 设备所在城市
  static const SP_TOP_UP_COUNTRY_CODE = "sp_top_up_country_code"; // 充值页选择的国家码

  static const SP_TEST_ENV = "sp_test_env";
  static const SP_DEEP_LINK_PARAMS = "sp_deep_link_params";
  static const SP_DAILY_CHECKED_IN = "sp_daily_checked_in";
  static const SP_ACTIVITY_DIALOG_DATE = "sp_halloween_dialog_date"; // 活动弹框弹出时间

  /// 房间名称
  static const SP_ROOM_TITLE = "sp_room_title";
  static const SP_DEVICE_UNIQUE_ID = "sp_device_unique_id";
  static const SP_KEY_TIP_GO_LIVE = "sp_key_tip_go_live";
  static const SP_NOTIFICATION_VIP_TALENT = "sp_notification_vip_talent"; // APP vip主播开播消息推送   默认 true
  static const SP_NOTIFICATION_FOLLOWED_TALENT = "sp_notification_followed_talent"; // APP 关注主播开播消息推送 默认 true
  static const SP_NOTIFICATION_SYSTEM = "sp_notification_system"; // APP 系统消息推送   默认 true
  static const SP_KEY_TIP_GUIDE = "sp_key_tip_guide";
  static const SP_DISCOUNT_VIP = "sp_key_discount_vip";

  static const SP_NEW_USER = "sp_new_user"; // 记录新用户注册时间
  static const SP_ENTER_ROOM = "sp_enter_room"; // 记录是否进入过房间
  static const SP_SOCKET_SESSION = "sp_socket_session";

  static const SP_LOGIN_PHONE = "sp_login_phone";

  static const SP_LOGIN_PHONE_CODE = "sp_login_phone_code";

  /// 记录用户打开安全弹框的时间
  static const SP_USER_OPEN_SAFE_ALERT_TIME = "sp_user_open_safe_alert_time";

  /// 记录用户打开 app 的时间
  static const SP_USER_OPEN_APP_TIME = "sp_user_open_app_time";

  /// 记录是否为当日启动 app
  static const SP_TODAY_FIRST_OPEN_APP = "sp_today_first_open_app";

  /// 首页Popular呈现样式 true: Grid  false: List
  static const SP_HOME_POPULAR_GRID = "sp_home_popular_grid";

  /// 显示新登录页
  static const SP_LOGIN_NEW = "sp_login_new";

  /// 显示推荐页
  static const SP_SHOW_RECOMMEND_PAGE = "sp_show_recommend_page";

  /// 首页标签栏featured索引
  static const POPULAR_FEATURED_INDEX = 1;

  /// 提醒用户是否切换 Hindi 标识
  /// true 不显示切换 Hindi 语的弹框；false 显示切换 Hindi 语弹框
  static const SP_HINDI_TIP = "sp_hindi_tip";

  //记录消息的偏移量
  static const SP_MESSAGE_QUEUE = "sp_message_queue";
  static const MESSAGE_MAX_COUNT = 99;

  static const SP_KEY_DISCOUNT_CAR = "sp_key_discount_car";
  static const SP_KEY_DISCOUNT_MIC_FRAME = "sp_key_discount_mic_frame";
  static const SP_KEY_DISCOUNT_THEME = "sp_key_discount_theme";
  static const SP_KEY_DISCOUNT_UID = "sp_key_discount_uid";
  static const SP_KEY_DISCOUNT_BUBBLE = "sp_key_discount_bubble";
  static const SP_KEY_DISCOUNT_CARD_FRAME = "sp_key_card_frame";

  static const SP_KEY_LUDO_REDDOTED = "sp_key_ludo_red_doted";

  static const SP_LITE_VERSION = "sp_lite_version"; // 是否支持lite version
  static const SP_SHOW_APP_UPDATE = "sp_show_app_update"; // 新版本更新提示   每48小时仅提示一次

  /// 本地被屏蔽的用户列表
  /// 苹果审核要求有屏蔽用户的功能
  static const SP_BLOCKED_USER_LIST = "sp_blocked_user_list";

  static const SP_SHOW_TOPUP_ACTIVITY_DIALOG = "sp_show_topup_activity_dialog";
  static const FREQUENCY_UNIT_HOUR = 3600000; // 显示频率的单位 1小时  60 * 60 * 1000

  /// 记录combo活动弹框的显示时间
  static const SP_SHOW_COMBO_ACTIVITY_DIALOG = "sp_show_combo_activity_dialog";

  /// 记录送礼活动弹框的显示时间
  static const SP_SHOW_PRESENT_ACTIVITY_DIALOG = "sp_show_present_activity_dialog";

  /// 记录开斋节活动弹框的显示时间
  static const SP_SHOW_EID_ACTIVITY_DIALOG = "sp_show_eid_activity_dialog";

  /// 记录可配置的节活动弹框的显示时间
  static const SP_SHOW_CONFIG_ACTIVITY_DIALOG = "sp_show_config_activity_dialog";

  /// 记录充值送礼活动弹框的显示时间
  static const SP_SHOW_TOPUP_GIFT_ACTIVITY_DIALOG = "sp_show_topup_gift_activity_dialog";

  /// 充值送礼活动弹框
  static const SP_KEY_RECHARGE = "sp_key_recharge";

  static const SP_KEY_READ_LIST = 'event_read_list';
  static const SP_KEY_ONLINE_LIST = 'event_online_list';

  /// 跳转周星活动 h5 页面  2021/7/8 修改
  // "https://www.oyetalk.live/oyetalkWeeklyAr/index.html";
  static const String ROOM_WEEKLY_STAR = 'https://www.oyetalk.tv/event/oyetalkWeeklyAr/index.html';

  /// 生产环境level page
  static const String LEVEL_PAGE_PRD = "http://121.37.214.86/myLevel/#/";

  /// 测试环境level page
  static const String LEVEL_PAGE_DEV = "http://121.37.214.86/myLevel/#/";

  /// 代理充值页面
  static const String TOPUP_AGENCY = 'http://www.oyetalk.live/event/oyetalkReseller/';

  /// razorpay 获取预订单 url
  static const String RPS_URL = 'http://status.oyetalk.tv/oyetalk-pay-center/';

  /// 测试环境 razorpay 获取预订单 url
  static const String TEST_RPS_URL =
      'http://voice-cluster-314173693.cn-northwest-1.elb.amazonaws.com.cn:9020/oyetalk-pay-center/';

  /// http返回码
  static const HTTP_NETWORK_ERROR = "-1";
  static const HTTP_TIMEOUT = "-2";
  static const HTTP_ERROR = "-3";
  static const HTTP_CANCEL = "-4";
  static const HTTP_SUCCESS = "000000";
  static const HTTP_SESSION_TIMEOUT = "900000";
  static const HTTP_SESSION_TIMEOUT_NEW = "0401";
  static const HTTP_SUCCESS_NEW = "0";
  static const Insiffcient_Code = "901001";

  static const PWD_NEED_FIX = "906006";

  /// 麦位相关操作返回码
  static const MAC_OPERATE_SUCCESS = "000000";

  ///折扣商品数量不足
  static const HTTP_DISCOUNT_NUM_NOT_ENOUGH = "903107";

  static const HTTP_MESSAGE_BLOCKED = "15005";
  static const HTTP_MESSAGE_BLOCKING = "15009";

  /// pk成功匹配
  static const PK_MATCH_SUCCESS = "902004";

  /// pk匹配失败，pk邀请已被其他人接收
  static const PK_MATCH_FAIL_OTHER = "902003";

  /// 主播当前已有一场pk未结束,无法开启pk
  static const PK_MATCH_FAIL_NOT_OVER = "905033";

  /// twitter登录相关
  static const TWITTER_CONSUMER_KEY = "JNyJbB2xSiMveGwLzclRH8AyX";
  static const TWITTER_CONSUMER_SECRET = "U0juIf0lQtNfENaOedb7wM7DUHoAPT0rtfvShSAsMQsyHrH7KN";

  /// google登录相关
  static const GOOGLE_SCOPE_EMAIL = "email";
  static const GOOGLE_SCOPE_CONTACTS = "https://www.googleapis.com/auth/contacts.readonly";

  /// sns登录类型
  static const SNS_TYPE_FACEBOOK = "1";
  static const SNS_TYPE_TWITTER = "2";
  static const SNS_TYPE_GOOGLE = "3";
  static const SNS_TYPE_INSTAGRAM = "4";

  /// 登录界面协议
  static const TERMS_URL = "https://www.oyetalk.live/agreement";

  ///WHATSAPP
  static const WHATSAPP_SHARE = "https://go.onelink.me/g2Wx/618e0cd0";
  static const WHATSAPP_CONTACT_ROMAN = "923455213179";

  static const WHATSAPP_CONTACT_ENGLISH = "971554122778"; //"33630595449";

  /// 设计稿尺寸
  static const int DESIGN_WIDTH = 720;
  static const int DESIGN_HEIGHT = 1280;

  /// 设计规范
  static const Color COLOR_BORDER = Color(0x32E6E6E6);
  static const double TITLE_HEIGHT = 93;

  /// 底部标签栏高度
  static const double BOTTOM_BAR_HEIGHT = 96;

  /// 动态相关
  static const STATUS_MAX_LINE = 6;

  /// 动态大于可见百分比时自动播放
  static const STATUS_VISIBLE_FRACTION = 0.8;

  /// 动态背景图的高度和宽度
  static const double STATUS_BG_HEIGHT = 920;
  static const double STATUS_BG_WIDTH = 704;

  /// 动态 item 高度
  // static const double STATUS_ITEM_HEIGHT = 1300;

  /// bugly
  static const String BUGLY_APPID = "957b616d38";
  static const String BUGLY_APPID_iOS = "64a9f38409";

  /// 自定义的 bugly 错误类型
  static const String BUGLY_EXCEPTION_IAP = "ios iap error";
  static const String BUGLY_EXCEPTION_HTTP = "http request error";

  /// support (livechat)
  static const String LIVECHAT_KEY_LICENCE = "11702655";
  static const String LIVECHAT_KEY_LICENCE_INDIA = "13164273";

  ///resource save DIR
  static const String RESOURCE_BACKGROUND_MUSIC = 'bg_music';
  static const String RESOURCE_RECORD_MIXED = 'record_mixed';
  static const String RESOURCE_RECORD_ORIGINAL = 'record_original';
  static const String TEMP = 'temp';
  static const String RESOURCE_AVATAR = 'Avatar';

  /// 每日任务名称
  static const String DAILY_CHECKED_IN = 'Daily checkin';

  /// flutter 和原生的通道名
  static const String FLUTTER_NATIVE_CHANNEL = 'com.oyetalk.flutter_native_channel';

  /// 好友分享类型
  static const SHARE_STATUS = "image_text_status#1";
  static const SHARE_ROOM = "image_text_room#1";
  static const SHARE_EVENT = "image_text_event#1";
  static const SHARE_EVENT_CENTER = "image_text_event_center#1";
  static const FRIEND_GREETING = "text_friend#2";
  static const Share_Badges = "image_text_badge#1";
  static const SHAERE_GAME_ROOM = "image_text_room#2";
  static const SHAERE_TPGO_ROOM = "image_text_room#3";

  /// 引导标识
  static const GUIDE_MAIN = "guide_main";
  static const GUIDE_MOOD = "guide_mood";
  static const GUIDE_SING_DIALOGUE = "guide_sign_dialogue";
  static const GUIDE_UPLOAD_MUSIC = "guide_upload_music";
  static const GUIDE_VOLUME = "guide_volume";
  static const GUIDE_BACKGROUND = "guide_background";
  static const GUIDE_MUSIC = "guide_music";
  static const GUIDE_FEATURED = "guide_featured";
  static const GUIDE_ROOM = "guide_room";
  static const GUIDE_SLOG = "guide_slog";
  static const GUIDE_GAME_ROOM = "guide_game_room";

  /// Firebase Remote Config 配置
  /// 上传所有用户的日志
  static const UPLOAD_LOG_ALL_USER = "upload_log_all_user";

  /// 需要上传日志的用户 id
  static const UPLOAD_LOG_USER_ID = "upload_log_user_ids";

  /// 需要上传的日志文件名称，用逗号分割
  static const UPLOAD_LOG_FILE_NAME = "upload_log_file_name";

  /// 需要上传日志的用户账号
  static const UPLOAD_LOG_USER_ACCOUNT = "upload_log_accounts";

  /// 宰牲节活动皮肤配置
  static const ACTIVITY_CONFIG_INFO = "activity_eid_info";

  /// 是否播放登录背景音乐
  // static const PLAY_LOGIN_BG_MUSIC = "play_login_bg_music";

  /// 是否显示新登录页面
  // static const SHOW_BETTER_LOGIN_PAGE = "show_better_login_page";

  ///麦位小游戏
  static const MINI_GAMES_PATTERN = "<***MINI_GAMES***>";
  static const EMOJIES_PATTERN = "<***EMOJIES***>";
  static const MINI_GAMES_SPLITTER = ":";
  static int DICE_GAME_ID = 7000001;
  static int FINGER_CROSS_GAME_ID = 7000002;
  static int FLIP_CIRCLE_GAME_ID = 7000003;
  static int FLIP_COIN_GAME_ID = 7000004;

  static const String ANIMATION = 'Animation';
  static const String ANIMATION_EMOJI = 'Emojie';
  static const String FIREBASE_BASE_URL =
      "https://oyetalk.s3.ap-south-1.amazonaws.com/oyetalk_frames/"; //"http://www.tkvivo.com/oyetalk/";
  static const String EMOJI_BASE_URL = FIREBASE_BASE_URL + "animatedemojiframes/";

  ///4：PayTm ， 7：google， 9：payermax，10：razorpay
  static const int PAYER_MAX = 9;
  static const int RAZOR_PAY = 10;

  /// razorpay测试key：rzp_test_EIPspdthGjJToV   正式key：rzp_live_kbBVDlwiqbo0IM
  static const String RAZOR_KEY = 'rzp_live_kbBVDlwiqbo0IM';
  static const String RAZOR_TEST_KEY = 'rzp_test_EIPspdthGjJToV';

  /// 根据设备语言需要预设的APP语言，否则默认en
  static const String EXT_LANG = 'hi';

  /// 默认语言 en
  static const String DEFAULT_LANG = 'en';

  //房间配置
  static int MAX_CHAT_LENGTH = 70;
  //房间弹幕字符长度限制
  static int MAX_BARRAGE_LENGTH = 32;
  static const GOOGLE_STORE = 'https://play.google.com/store/apps/details?id=com.oyetalk.tv'; // Play Store 应用详情地址
  static const APPLE_STORE = 'https://apps.apple.com/us/app/id1524257092'; // App Store 应用详情地址
  static const FACEBOOK_APP = 'fb://page/1798861850175813'; // Facebook APP 官方公众号
  static const FACEBOOK_APP_IOS = 'fb://page/?id=1798861850175813'; // Facebook APP 官方公众号
  static const FACEBOOK_WEB = 'https://www.facebook.com/OyeTalk-1798861850175813'; // Facebook website 官方公众号

  // pk资源前缀
  static const PK_RESOURCE_PROFIX = "https://d2zc7tapf7a4we.cloudfront.net/svga/pk/";

  /// 私信消息布局
  static const INBOX_LAYOUT_COMMENT_LIKE = 'image_text_status_comment_like';

  ///teenpatti 规则
  static const String TEEN_PATTI_RULE = "https://www.oyetalk.live/event/teenpatti/index.html";

  /// 代理
  static String PROXY_HOST = '';
  static String PROXY_PORT = '';

  /// 货币类型
  static const String CURRENCY_BEAN = 'bean';
  static const String CURRENCY_GEM = 'gem';
  static const String CURRENCY_GOLD = 'gold';

  // tp钻石换金币使用人群  0-无  1-平台签约主播  2-全体用户
  static const String TP_G2B_SCOPE_NONE = '0';
  static const String TP_G2B_SCOPE_ONLY_OT = '1';
  static const String TP_G2B_SCOPE_ALL = '2';
  // tp 新用户 '1'新用户
  static const String TP_NEW_USER = 'TP_NEW_USER';

  /// 和老版本用户PK投降失败
  static const String PK_SURRENDER_FAILD = '905045';

  static const int PAGE_SIZE = 20;

  /// 2.6.0 红点
  static const String SP_EDIT = '260edit';
  static const String SP_LEVEL = '260level';

  //2.7.0 vip
  static const String SP_ARISTOCRACY = '270aristocracy';

  /// 埋点日志前缀
  static const String ANALYSIS_PREFIX = 'Analysis-----';

  /// 是否开启数数埋点
  static const bool OPEN_TA = true;

  /// 是否显示过授权
  static const String SP_GRANT = 'sp_grant';

  /// 数数测试环境
  static const String TA_TEST_ID = 'a2aff7b4feba43669b8fba338ae1e7fa';

  /// 数数生产环境
  static const String TA_PROD_ID = 'e51f8bf57c17475286b24f1e54ead289';

  /// 数数上报地址
  static const String TA_UPLOAD_URL = 'http://api-ta.oyetalk.tv';

  /// 埋点类型
  /// 埋点开关 0-都不报  1-只报Firebase埋点  2-只报数数埋点  3-都报
  static const String TRACK_TYPE_NONE = '0';
  static const String TRACK_TYPE_FIRE = '1';
  static const String TRACK_TYPE_TA = '2';
  static const String TRACK_TYPE_BOTH = '3';

  /// android sdk 6.0
  static const int ANDROID_6_SDK_API = 23;

  //ulevelPrivilegeAvatar的默认值
  static const int DEFAULT_U_LEVEL_PRIVILEGE = 3;

  /// 封号code
  static const String BAN_ACCOUNT = '905031';

  /// 封设备code
  static const String BAN_DEVICE = '906010';

  /// 记录emoji data
  static const String SP_EMOJI_DATA = 'sp_emoji_data';

  /// 记录myemoji data
  static const String SP_MY_EMOJI = 'sp_my_emoji';
}
