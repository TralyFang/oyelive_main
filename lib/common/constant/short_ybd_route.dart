

/*
 * @Author: William
 * @Date: 2022-07-20 16:05:52
 * @LastEditTime: 2022-10-13 18:09:54
 * @LastEditors: William
 * @Description: https://docs.qq.com/sheet/DZmtRd09UV3ZYUXRz?tab=BB08J2&_t=1658367871972
 * @FilePath: /oyetalk-flutter/lib/common/constant/short_route.dart
 */
class YBDShortRoute {
  /// 登录主页
  static const String login = 'login';

  /// 账号密码登录页
  static const String login_indoor = 'login_pw'; // 账号密码和手机号验证码登录 用参数作区分

  /// 隐私条款页?web容器
  static const String privacy = 'privacy';

  /// 根目录
  static const String index_page = 'index';

  // 活动浮窗页
  static const String activity_pop = 'activity_pop';

  /// home（index=0）
  static const String home = 'home';

  /// flutter://ybd_index?index=0&sub=0
  static const String home_related = 'home_related';

  /// flutter://ybd_index?index=0&sub=1
  static const String home_pop = 'home_pop';

  /// flutter://ybd_index?index=0&sub=2
  static const String home_explore = 'home_explore';

  /// flutter://ybd_index?index=0&sub=3
  static const String home_game = 'home_game';

  /// flutter://combo_list
  static const String home_pop_combo = 'home_pop_combo';

  /// flutter://cp_list
  static const String home_pop_team = 'home_pop_team';

  /// flutter://leaderboard/
  static const String home_pop_rank = 'home_pop_rank';

  /// flutter://leaderboard/0
  static const String home_pop_rank_talent = 'home_pop_rank_talent';

  /// flutter://leaderboard/1
  static const String home_pop_rank_gifter = 'home_pop_rank_gifter';

  /// flutter://leaderboard/2
  static const String home_pop_rank_follower = 'home_pop_rank_follower';

  /// flutter://search
  static const String home_search = 'home_search';

  /// flutter://game_room_page
  static const String home_game_room = 'home_game_room';

  /// /flutter_room_list_page?roomId=2100140&tag=-1&funcType=-1
  static const String live = 'live';

  /// flutter://room_fans
  static const String live_gifter_rank = 'live_gifter_rank';

  /// flutter://ybd_index?index=2
  static const String inbox = 'inbox';

  /// flutter://inbox/private/userid/
  static const String inbox_fri_msg = 'inbox_fri_msg';

  /// flutter://inbox/setting/1/userid
  static const String inbox_fri_msg_set = 'inbox_fri_msg_set';

  /// flutter://system_msg_page/queueId
  static const String inbox_sys_msg = 'inbox_sys_msg';
  static const String comment_msg_page = 'comment_msg_page';
  static const String like_msg_page = 'like_msg_page';
  static const String gift_msg_page = 'gift_msg_page';

  /// flutter://top_up
  static const String my_top_up = 'my_top_up';

  /// my_top_up_rec
  static const String my_top_up_rec = 'my_top_up_rec';

  /// /web_view_page?url=http://121.37.214.86/event/oyetalkReseller/index.html?isFlutter=1
  static const String my_top_up_ag = 'my_top_up_ag';

  /// flutter://web_view_page?url=https://www.oyetalk.live/event/topupoffer/index.html?isFlutter=1
  static const String my_top_up_ag_reward = 'my_top_up_ag_reward';

  /// flutter://daily_task
  static const String my_task = 'my_task';

  /// flutter://setting
  static const String my_set = 'my_set';

  /// flutter://room/blacklist
  static const String my_set_blacklist_room = 'my_set_blacklist_room';

  /// flutter://inbox/black
  static const String my_set_blacklist_msg = 'my_set_blacklist_msg';

  /// flutter://slog_blacklist
  static const String my_set_blacklist_slog = 'my_set_blacklist_slog';

  /// flutter://about
  static const String my_set_about = 'my_set_about';

  /// flutter://lite_version
  static const String my_lite_ver = 'my_lite_ver';

  /// flutter://pk_record
  static const String my_pk = 'my_pk';

  /// flutter://edit_profile
  static const String my_profile_edit = 'my_profile_edit';

  /// /web_view_page?url=
  static const String my_gems_ex = 'my_gems_ex';

  /// /web_view_page?url=
  static const String my_beans_ex = 'my_beans_ex';

  /// flutter://my_invites
  static const String my_invites = 'my_invites';

  /// flutter://badges
  static const String my_badges = 'my_badges';

  /// /web_view_page?url=
  static const String my_level = 'my_level';

  /// flutter://guardian
  static const String my_guardian = 'my_guardian';

  /// flutter://vip
  static const String my_vip = 'my_vip';

  /// flutter://store
  static const String my_store = 'my_store';

  /// flutter://baggage
  static const String my_baggage = 'my_baggage';

  /// flutter://user_profile/userid
  static const String my_profile = 'my_profile';

  /// flutter://follow/userid/0
  static const String my_friends = 'my_friends';

  /// flutter://follow/userid/1
  static const String my_following = 'my_following';

  /// flutter://follow/userid/2
  static const String my_followers = 'my_followers';

  /// flutter://status/post/audio/record
  static const String new_slog = 'new_slog';

  /// flutter://start_live_page?funcType=-1
  static const String new_live = 'new_live';

  /// /web_view_page?url=http://121.37.214.86/talent/#/#/Info?index=1&isFlutter=1
  static const String new_live_rule = 'new_live_rule';

  /// http://121.37.214.86/talent/#/editInfo
  static const String new_live_rule_apply = 'new_live_rule_apply';
  static const String slog = 'slog';
  static const String recommended = 'recommended';
  static const String event_msg_page = 'event_msg_page';
  static const String photo_viewer = 'photo_viewer';
  static const String slog_detail = 'slog_detail';
  static const String game_tp_room_page = 'game_tp_room_page';

  /// 首页上面tab路由列表
  static const List<String> index_tab_list = [home_related, home_pop, home_explore];

  /// 首页下面tab路由列表
  static const List<String> index_list = [home, home_game, inbox, my_profile];

  /// 排行榜路由列表
  static const List<String> rank_list = [home_pop_rank_talent, home_pop_rank_gifter, home_pop_rank_follower];

  /// 非活动webview
  static const List<String> non_event = [my_beans_ex, my_gems_ex, my_level];

  /// 首页点击的名字
  static const List<String> index_names = ['Home', 'Game', 'YBDMessage', 'My'];
}

class YBDWebRoute {
  /// 隐私协议
  static const String privacy = 'agreement';

  /// 兑换金币
  static const String gold = 'exchangeGold';

  /// 兑换钻石
  static const String gem = 'exchange&';

  /// 等级
  static const String level = 'myLevel';

  /// 语音房
  static const String live = 'room_list_page';

  /// 代理充值
  static const String my_top_up_ag = 'oyetalkReseller';

  /// 代理充值奖励明细页
  static const String my_top_up_ag_reward = 'www.oyetalk.live/event/topupoffer';

  /// 官方主播规则页
  static const String new_live_rule = 'talent';

  /// flutter://
  static const String prefix = 'flutter://';
}
