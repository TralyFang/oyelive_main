import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';

/// 所有翻译的对应key，防止key写错
class YBDI18nKey {
  static String get tip => 'tip'.i18n;
  static String get cancel => 'cancel'.i18n;
  static String get confirm => 'ok'.i18n;
  static String get reconnectingEnterRoomTips => 'reconnecting_enter_room_tips'.i18n;

  // tp_room
  static String get insufficientBalance => 'Insufficient balance, bet failed!'.i18n;
  static String get potLimitReached => 'Pot limit reached'.i18n;
}