


/*
 * @Author: William
 * @Date: 2022-09-08 17:22:39
 * @LastEditTime: 2022-09-16 15:00:03
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/abstract/analytics/imp/analytics_page_imp.dart
 */
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/abstract/analytics/page_ybd_time_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_route_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

class YBDAnalyticsPageImp extends AnalyticsService {
  YBDRouteTrack rt = YBDRouteTrack();

  /// eventName: open_page
  /// location: 页面名称
  /// itemId: 当前时间戳base64 字符串
  @override
  void page({String? currentPage, String? previousRoute}) {
    alog.v('currentRoute:$currentPage   pre:$previousRoute');

    ///去重，计时
    if (!YBDPageTimeUtil.instance()!.routeChange(currentPage, previousRoute)) return;

    ///三方平台数数
    if (susu()) {
      // rt.trackRoute(currentRoute: currentPage, previousRoute: previousRoute, routeType: 'push');
      // TA.curRoute = currentPage; // 当前路由
      TA.routeStack.add(currentPage); // 把当前路由加入堆栈
      alog.v(
          '22.7.27----push timeEvent currentPage:$currentPage previousRoute:$previousRoute routeStack:${TA.routeStack}');
      if (TA.startTimeTrack)
        rt.durationTrack(
          YBDTAProps(name: previousRoute),
          currentRoute: currentPage,
          previousRoute: previousRoute,
          routeType: 'push',
        );
      TA.timeEvent(YBDTATrackEvent.route); // 当前路由开始计时
    }
    logger.v('screen view event: $currentPage, $previousRoute');

    ///默认所有的埋点都上报firebase base64Encode(utf8.encode(DateTime.now().millisecondsSinceEpoch.toString())
    // YBDAnalyticsUtil.logEvent(YBDScreenViewEvent(YBDEventName.OPEN_PAGE, screenName: currentPage));
  }
  void pageWQMEjoyelive({String? currentPage, String? previousRoute}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void pop({String? currentPage, String? previousRoute}) {
    YBDPageTimeUtil.instance()!.routeChange(currentPage, previousRoute, isPop: true);

    ///三方平台数数
    if (susu()) {
      // rt.trackRoute(currentRoute: previousRoute, previousRoute: currentPage, routeType: 'pop');
      alog.v('22.7.27----pop timeEvent cur:$currentPage pre:$previousRoute routeStack:${TA.routeStack}');
      if (TA.routeStack.contains(currentPage)) {
        rt.durationTrack(
          YBDTAProps(name: currentPage),
          currentRoute: previousRoute,
          previousRoute: currentPage,
          routeType: 'pop',
        );
        TA.routeStack.remove(currentPage);
        if (TA.routeStack.isNotEmpty) {
          // 路由时长埋点是否要记这么细？
          TA.timeEvent(YBDTATrackEvent.route); // 当前路由开始计时
        }
        // TA.curRoute = currentPage;
      }
    }
  }
  void popLTDo8oyelive({String? currentPage, String? previousRoute}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
