import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:thinking_analytics/thinking_analytics.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';

mixin AnalyticsPage {
  /// 默认埋点所有平台 [].all
  /// [pageName] 页面名称
  /// [YBDAnalyticsEvent] eventName: open_page
  void page({required String currentPage, required String previousRoute}) {}
  void pageTkPJ5oyelive({required String currentPage, required String previousRoute}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// page pop 用于页面计时
  void pop({required String currentPage, required String previousRoute}) {}
  void popdbvVSoyelive({required String currentPage, required String previousRoute}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

mixin AnalyticsClick {
  /// 点击事件埋点
  void click() {}
}

///一些公用 api
mixin AnalyticsCommon {
  /// 时长上传 后期可能涉及到页面停留时长、事件响应时长、接口请求时长 ...
  void timeReport({TimeType type = TimeType.PageOverTime, String? location, int? time}) {}
  void timeReportBt8Fioyelive({TimeType type = TimeType.PageOverTime, String? location, int? time}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 埋点功能 协议
/// 兼容数数埋点 和 firebase
///
abstract class AnalyticsService with AnalyticsPage, AnalyticsClick, AnalyticsCommon {
  ///是否埋点三方平台
  bool susu() {
    return [].all.contains(AnalyticsType.AnalyticsThinking);
  }
}

enum AnalyticsType {
  AnalyticsThinking, //数数
  AnalyticsFirebase //firebase
}

extension AnalyList on List {
  List<AnalyticsType> get all {
    return const [AnalyticsType.AnalyticsThinking, AnalyticsType.AnalyticsFirebase];
  }
}

enum TimeType {
  PageOverTime,

  ///页面停留时长
  EventReponseTime,

  ///事件响应时长
  ///...
}

class TA {
  static ThinkingAnalyticsAPI? ta;
  static String? distinctId;
  static TDPresetProperties? presetProperties;
  static Map<String, dynamic>? eventPresetProperties;
  static List<String?> routeStack = [];
  static bool startTimeTrack = false;
  static String profileUserId = '';
  static String gameRoomId = '';
  static bool startRegTrack = false;

  static Future<void> initThinkingDataState() async {
    // 打开日志
    // ThinkingAnalyticsAPI.enableLog();

    // 初始化 ta 实例
    ta = await ThinkingAnalyticsAPI.getInstance(
      Const.TEST_ENV ? Const.TA_TEST_ID : Const.TA_PROD_ID,
      Const.TA_UPLOAD_URL,
      timeZone: 'UTC',
      mode: ThinkingAnalyticsMode.NORMAL,
      enableEncrypt: false,
      // secretKey: YBDTAUtil.getKey(),
    );
    if (Platform.isAndroid) ta!.enableThirdPartySharing(TAThirdPartyShareType.TA_APPS_FLYER);

    // 校准时间
    YBDTAUtil.calibrateTime();

    // 获取访客id
    distinctId = await ta!.getDistinctId();

    // 获取预置属性对象
    presetProperties = await ta!.getPresetProperties();

    // 生成事件预置属性
    eventPresetProperties = presetProperties!.toEventPresetProperties();

    // 设置公共事件属性
    ta!.setSuperProperties(eventPresetProperties!);

    // 自动采集事件
    ta!.enableAutoTrack([
      ThinkingAnalyticsAutoTrackType.APP_START, // 应用进入前台
      ThinkingAnalyticsAutoTrackType.APP_END, // 应用进入后台
      ThinkingAnalyticsAutoTrackType.APP_INSTALL, // 安装后首次打开应用
      ThinkingAnalyticsAutoTrackType.APP_CRASH, // 出现未捕获异常导致应用闪退
    ]);

    // 自定义采集事件公共属性
    ta!.setAutoTrackProperties([
      ThinkingAnalyticsAutoTrackType.APP_START, // 应用进入前台
      ThinkingAnalyticsAutoTrackType.APP_END, // 应用进入后台
      ThinkingAnalyticsAutoTrackType.APP_INSTALL, // 安装后首次打开应用
      ThinkingAnalyticsAutoTrackType.APP_CRASH, // 出现未捕获异常导致应用闪退
    ], {
      'current_route': Get.currentRoute.shortRoute,
    });
  }

  // 埋点
  static void track(String? eventName, {Map<String, dynamic>? properties, DateTime? dateTime, String? timeZone}) {
    if (ta != null) {
      alog.i('22.8.12----track eventName:$eventName--properties:$properties');
      ta!.track(eventName!, properties: properties, dateTime: dateTime, timeZone: timeZone);
    } else {
      alog.i('22.8.12----track no getInstance');
    }
  }

  // 记录事件时长
  static void timeEvent(String eventName) {
    alog.v('22.7.27----timeEvent eventName:$eventName');
    if (ta != null) {
      if (eventName == YBDTATrackEvent.route) startTimeTrack = true;
      ta!.timeEvent(eventName);
    }
  }

  /// 设置用户属性和动态公共属性, 动态公共属性不支持自动采集事件
  static Future<void> userSet({YBDUserInfo? userInfo}) async {
    alog.v('22.8.4--userSet');
    if (ta != null) {
      Map<String, dynamic> map = await YBDTAUtil.getPresetProperties();
      Map<String, dynamic> userMap = await YBDTAUtil.getUserMap(userInfo: userInfo);
      map.addAll(userMap);
      if (userMap.isNotEmpty) ta?.userSet(map);
      alog.v('22.8.12--setDynamicSuperProperties:$map');
      ta?.setSuperProperties(map);
    }
  }

  /// 用户登录
  static void login({String? accountId}) {
    if (ta != null) {
      alog.i('22.9.22---user log in: $accountId');
      ta!.login(accountId!);
    }
  }

  /// 用户退出登录
  static void logout() {
    YBDTPGlobal.isOfficialTalent = null;
    track(YBDTATrackEvent.account_logout);
    ta?.logout();
    ta?.clearSuperProperties();
  }

  /// 首次事件 设备新增
  static void trackFirstEvent() {
    // 首次事件是指针对某个设备或者其他维度的 ID，只会记录一次的事件。
    // 例如在一些场景下，您可能希望记录在某个设备上第一次发生的事件，则可以用首次事件来上报数据。
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.FIRST_INSTALL));
    var firstModel = TrackFirstEventModel(YBDTATrackEvent.first_device_add, '', {});
    alog.v('22.8.16--trackFirstEvent:first_device_add');
    ta?.trackEventModel(firstModel);
  }
}
