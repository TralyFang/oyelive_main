

/*
 * @Author: William
 * @Date: 2022-07-21 17:51:34
 * @LastEditTime: 2022-07-26 15:02:00
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/abstract/analytics/device_info_util.dart
 */
import 'dart:async';

import 'package:disk_space/disk_space.dart';
import 'package:package_info/package_info.dart';
import 'package:system_info/system_info.dart';
import 'package:oyelive_main/common/util/connectivity_ybd_util.dart';

class YBDDeviceInfoUtil {
  ///已弃用该方法
  // 获取运营商名称
  // static Future<String> getCarrierName() async {
  //   String carrierName;
  //   try {
  //     carrierName = await SimInfo.getCarrierName;
  //   } on Exception catch (e) {
  //     logger.v(e);
  //   }
  //   return carrierName ?? '';
  // }

  // 获取包信息
  static Future<PackageInfo> getPackageInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo;
  }

  // 用户设备的当前剩余内存和总内存，单位GB，如 1.4/2.4
  static Future<String> getRAMInfo() async {
    int bToG = 1024 * 1024 * 1024;
    String totalMemory = (SysInfo.getTotalPhysicalMemory() / bToG).toStringAsFixed(1);
    String freeMemory = (SysInfo.getFreeVirtualMemory() / bToG).toStringAsFixed(1);
    return '$freeMemory/$totalMemory GB';
  }

  // 用户设备的当前剩余存储空间和总存储空间，单位GB，如 30/200
  static Future<String> getDiskInfo() async {
    double totalStorage = (await DiskSpace.getTotalDiskSpace) ?? 0.0;
    double freeStorage = (await DiskSpace.getFreeDiskSpace) ?? 0.0;
    int total = (totalStorage / 1024).floor();
    int free = (freeStorage / 1024).floor();
    return '$free/$total GB';
  }

  // 获取网络状态 wifi mobile none
  static String getNetWorkType() {
    String networkType = YBDConnectivityUtil.getInstance()!.rs.name;
    return networkType;
  }
}
