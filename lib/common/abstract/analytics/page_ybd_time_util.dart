

import 'package:oyelive_main/common/util/log_ybd_util.dart';

///页面时长
class YBDPageTimeUtil {
  static YBDPageTimeUtil? _instance;

  static YBDPageTimeUtil? instance() {
    if (_instance == null) {
      _instance = YBDPageTimeUtil._();
    }
    return _instance;
  }

  YBDPageTimeUtil._();

  ///当前毫秒时间戳
  int time = 0;

  ///当前计时的路由
  String? route = '';

  ///300ms 内相同路由不重复处理
  ///[isPop] :  默认push
  ///[name] : 路由名称
  ///返回值return false 时 不上报埋点
  bool routeChange(String? currentPage, String? previousRoute, {bool isPop = false}) {
    // logger.v('route change: $currentPage, $previousRoute, $isPop，${this.time}');
    int time = DateTime.now().millisecondsSinceEpoch;

    ///push && 300ms内 同一个路由 不处理
    if (!isPop && sameRoute(currentPage) && time - YBDPageTimeUtil.instance()!.time < 300) return false;

    ///pop && 同一个路由 上报时间       push && 不同路由
    if ((isPop && sameRoute(currentPage)) || (!isPop && !sameRoute(currentPage))) {
      // if (YBDPageTimeUtil.instance().time > 0) logger.v('上报时长: ${time - YBDPageTimeUtil.instance().time}');
    }

    ///push 时记录时间
    YBDPageTimeUtil.instance()!.time = time;

    ///pop 时 清除记录, 防止pop完 立即push 同一个页面 出现误差
    YBDPageTimeUtil.instance()!.route = isPop ? '' : currentPage;

    return true;
  }
  void routeChangenBCxRoyelive(String? currentPage, String? previousRoute, {bool isPop = false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// same route at moment
  bool sameRoute(String? name) {
    return route == name;
  }
  void sameRouteD1wOfoyelive(String? name) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
