

import 'package:oyelive_main/common/util/module/module_ybd_center.dart';

abstract class RoomOperateCenter {
  ///teenPatti 关闭弹窗
  tpPop() {}

  ///全局进入房间
  enterRoom({int? roomId, EnterRoomType? type, int? gameCode}) {}

  ///显示gems 转 beans 弹窗
  showGemsTransBeansView() {}

  ///init blc 回调事件
  initBlock(String routeName) {}

  ///直接调用房间的业务
  roomFuncOperation(EnterRoomType? type,{int? gameCode}) {}

  /// 更新Teenpatti遊戲餘額
  updateTPMoney() {}
}
