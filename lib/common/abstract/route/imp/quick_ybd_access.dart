import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/abstract/route/route_ybd_service.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_lobby/enter_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/room/widget/jump_ybd_tpgo_notice.dart';

import '../../../util/dialog_ybd_util.dart';

class YBDQuickAccessService extends RouteService {
  @override
  push(String path) {
    // TODO: implement push

    print("sduuhisd$path");
    Map data = {};
    path.split("?").last.split("&").forEach((element) {
      List reslut = element.split("=");
      data.addAll({reslut.first: reslut.last});
    });
    showDialog<Null>(
        context: Get.context!, //BuildContext对象
        barrierDismissible: false,
        builder: (BuildContext context) {
          return YBDJumpTpgoNoticeDialog(
            onConfirm: data.isEmpty
                ? null
                : () {
                    YBDNavigatorHelper.popPage(context);
                    YBDDialogUtil.showLoading(Get.context);
                    bool isWatch = data["roomUserRole"] == "2";
                    YBDEnterHelper.needSeat = isWatch ? YBDEnterHelper.noNeed : YBDEnterHelper.need;
                    YBDEnterHelper().connectSocketNjoin(data["subType"], data["roomId"], data["matchType"],
                        popFirst: true,
                        enterType: isWatch ? EnterGameRoomType.Watch : EnterGameRoomType.QuickJoin, onSuccess: () {
                      YBDDialogUtil.hideLoading(Get.context);
                    });
                  },
          );
        });
  }
}
