


/*
 * @Author: William
 * @Date: 2022-06-13 20:37:17
 * @LastEditTime: 2022-07-13 18:28:15
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/abstract/route/imp/enter_chat_room_route_service.dart
 */
import 'package:oyelive_main/common/abstract/route/route_ybd_service.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';

class YBDEnterChatRoomRouteService extends RouteService {
  @override
  push(String path) {
    // type: 打开语聊房后再打开啥？
    // roomId: 打开谁的语聊房
    // "flutter://ybd_chat_room?type=7&roomId=-1&gameCode=1005"
    late Uri u;
    try {
      u = Uri.parse(path);
    } catch (e) {
      logger.e('url trans fail : $e');
    }
    int? type = u.queryParameters.keyToInt('type');
    int? roomId = u.queryParameters.keyToInt('roomId');
    int? gameCode = u.queryParameters.keyToInt('gameCode');
    YBDTeenInfo.getInstance().enterType = TPEnterType.TPTask;
    YBDModuleCenter.instance().enterRoom(roomId: roomId, type: YBDModuleCenter.enterRoomType(type), gameCode: gameCode);
  }
}
