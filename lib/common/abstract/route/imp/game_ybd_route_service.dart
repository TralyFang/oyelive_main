



import 'package:get/get.dart';
import 'package:oyelive_main/common/abstract/route/route_ybd_service.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/route_ybd_push_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/go_ybd_tab_event.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/widget/dialog_ybd_wsa_support.dart';

class YBDGameRouteService extends RouteService {
  @override
  push(String path) {
    if (path.startsWith(RouteConst.whatsAppRoute)) {
      YBDCommonUtil.showMyDialog(YBDWSASupportDialog());
      return;
    }

    if (path.contains(RouteConst.gameRoute)) {
      late Uri u;
      try {
        u = Uri.parse(path);
      } catch (e) {
        logger.e('url trans fail : $e');
      }

      String? type = u.queryParameters.keyToString('type');
      String? pos = u.queryParameters.keyToString('postion');
      int? index = u.queryParameters.keyToInt('index');
      int? enterRoom = u.queryParameters.keyToInt('enterRoom');
      int? match = u.queryParameters.keyToInt('match'); // 是否匹配不填默认为-1不匹配，1为匹配

      ///1 表示不进游戏房
      logger.v('daily task page type: $type, pos: $pos, index:$index, enterRoom:$enterRoom ');
      if (pos != 'home') return;
      YBDRoutePushUtil.type = type;
      if (Get.currentRoute == YBDNavigatorHelper.index_page) {
        ///去房间打开ludo
        if (enterRoom != 1) {
          YBDRtcHelper.getInstance().setGameSubType(type);
          YBDGameSocketApiUtil.quickEnterRoom(type: EnterGameRoomType.QuickJoin);
        }
        eventBus.fire(YBDGoTabEvent(1, postion: pos, subIndex: index, type: type, match: match));
        return;
      }
      if (Get.currentRoute == YBDNavigatorHelper.game_room_page) return;

      ///深层级的也适配一下
      while (Get.currentRoute != YBDNavigatorHelper.index_page) {
        YBDNavigatorHelper.popPage(Get.context!);
      }
      eventBus.fire(YBDGoTabEvent(1, postion: pos, subIndex: index, type: type, match: match));
    }
  }
}
