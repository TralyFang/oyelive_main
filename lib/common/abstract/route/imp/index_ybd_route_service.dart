


import 'package:get/get.dart';
import 'package:oyelive_main/common/abstract/route/route_ybd_service.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/go_ybd_tab_event.dart';

class YBDIndexRouteService extends RouteService {
  @override
  push(String path) {
    // "flutter://ybd_index?index=0&sub=0";
    late Uri u;
    try {
      u = Uri.parse(path);
    } catch (e) {
      logger.e('url trans fail : $e');
    }
    Map map = u.queryParameters;
    int? index = map.keys.contains('index') ? int.tryParse(map['index']) : 0;
    int? sub = map.keyToInt('sub');
    if (Get.currentRoute == YBDNavigatorHelper.index_page) return;
    YBDNavigatorHelper.popUntilMain(Get.context!);
    eventBus.fire(YBDGoTabEvent(index, subIndex: sub));
  }
}
