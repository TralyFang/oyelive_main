
class RouteConst {
  ///从其他位置跳转游戏房
  static String gameRoute = "flutter://ybd_game";

  /// 客服弹窗
  static String whatsAppRoute = "flutter://ybd_whatsapp";

  /// 跳转到首页
  static String indexRoute = "flutter://ybd_index";

  /// 跳转到语聊房打开xxx
  static String enterChatRoomRoute = "flutter://ybd_chat_room";

  /// 跳转到发布slog页
  static String postSlogRoute = "flutter://ybd_post_slog";

  static const gameQuickAccess = 'flutter://game_tp_quick_access';
}

abstract class RouteService {
  push(String path);
}
