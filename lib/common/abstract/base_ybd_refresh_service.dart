
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/constant/const.dart';

abstract class BaseRefreshService {
  ///关联页面title 等
  HttpType? type;
  bool needLoadMore = false;
  int pageSize = Const.PAGE_SIZE;
  int page = 1;
  late YBDBaseModel model;

  void refresh(
      {int page = 1, int pageSize = Const.PAGE_SIZE, YBDBaseModel? model, Function(List? list, bool isSuccess)? block});
}

class YBDBaseModel {}
