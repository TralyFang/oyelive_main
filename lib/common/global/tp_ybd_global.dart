

/*
 * @Author: William-Zhou
 * @Date: 2021-12-27 09:43:47
 * @LastEditTime: 2022-12-30 09:43:37
 * @LastEditors: William
 * @Description: global items
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/module/room/entity/game_ybd_query_model.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

import '../../redux/app_ybd_state.dart';
import '../constant/const.dart';

/// 存放全局变量
class YBDTPGlobal {
  /// store 全局变量, 在
  static Store<YBDAppState>? store;

  static String APP_VERSION = 'unknown';

  // app启动的毫秒时间戳
  // static int launchTime;

  /// 活动配置信息
  // static YBDTpActivityEntity activityEntity;

  static final ThemeData DEFAULT_THEME_DATA = ThemeData(
    primaryColor: Colors.white,
    dividerColor: Const.COLOR_BORDER,
    backgroundColor: Colors.white,
    colorScheme: ColorScheme.light(
      primary: YBDTPStyle.heliotrope,
      secondary: YBDTPStyle.gentianBlue,
    ),
    // 动态时间的文字颜色
    textTheme: TextTheme(bodyText2: TextStyle(color: Color(0xffEAEAEA))),
  );

  /// 显示新版登录页
  // static bool SHOW_BETTER_LOGIN_PAGE = true;

  /// 播放登录背景音乐
  // static bool PLAY_LOGIN_BG_MUSIC = true;

  /// 根据本地活动配置信息更新活动配置信息全局变量
  // static Future<void> updateActivityInfoValue(BuildContext context) async {
  //   // 活动配置信息
  //   String configInfo = await YBDSPUtil.get(Const.ACTIVITY_CONFIG_INFO);
  //   try {
  //     YBDTPGlobal.activityEntity = JsonConvert.fromJsonAsT<YBDTpActivityEntity>(json.decode(configInfo));
  //   } catch (e, s) {
  //     YBDLogUtil.e('parse activity entity error ');
  //   }

  //   Store<YBDAppState> appStore = YBDCommonUtil.storeFromContext(context: context);

  //   // 默认主题
  //   appStore.dispatch(YBDUpdateThemeAction(YBDTPGlobal.DEFAULT_THEME_DATA));
  // }
  // static String greedyGameUrl = '';
  // static String greedyGameIcon = '';
  // static String ludoGameUrl = ''; // ludo游戏地址
  // static String ludoGameIcon = '';

  static YBDGameQueryModel? model;

  static final LinearGradient blueGradient = LinearGradient(
    colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
  static final LinearGradient greyGradient = LinearGradient(
    colors: [Color(0xffF0F0F0), Color(0xffF0F0F0)],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );

  static SizedBox hSizedBox(int height) {
    return SizedBox(height: ScreenUtil().setWidth(height));
  }

  static SizedBox wSizedBox(int width) {
    return SizedBox(width: ScreenUtil().setWidth(width));
  }

  static bool? isOfficialTalent = false;
  static final int gemToBeanRate = 2;
  static String nameWarn = '';
  // 滤色器
  static ColorFilter getGreyFilter(bool grey) {
    return ColorFilter.mode(grey ? Colors.white.withOpacity(0.5) : Colors.white, BlendMode.modulate);
  }

  static Widget bgImg({bool isMe = true}) {
    return YBDImage(path: YBDActivitySkinRoot().curAct().profileBg(isMe), fit: BoxFit.cover);
  }

  static String s3Host = '';
  static bool openTrack = true;

  static List<Map> tpgoModels = [];

  static Map emojis = {};
}

class YBDTextCtrl extends GetxController {
  var nameWarn = ''.obs;
  setNameWarn(String warn) {
    nameWarn.value = warn;
  }
}

extension SetPixel on num {
  double get px {
    return dp720;
  }

  double get sp {
    return sp720;
  }

  // A member named 'sp' is defined in extension 'SizeExtension' and extension 'SetPixel'
  double get sp2 {
    return sp720;
  }
}

double kBottomBarHeight = ScreenUtil().bottomBarHeight;
double kScreenWidthDp = ScreenUtil().screenWidth;
