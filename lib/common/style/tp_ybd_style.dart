


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

/// 公共样式
class YBDTPStyle {
  /// 洋红色
  static const Color heliotrope = Color(0xffD251F9);

  /// 蓝品红
  static const Color gentianBlue = Color(0xff32249D);

  /// 青色，标签下划线颜色
  static const Color cyan = Color(0xff7DFAFF);

  /// 渐变颜色数组
  static const List<Color> colorList = [heliotrope, gentianBlue];

  /// 渐变
  static const LinearGradient gradient = LinearGradient(
    colors: colorList,
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );

  /// 渐变背景
  static const BoxDecoration gradientDecoration = BoxDecoration(
    gradient: gradient,
  );

  /// 页面边距
  static EdgeInsets pagePadding = EdgeInsets.symmetric(horizontal: 25.px as double);

  /// 标签样式
  static const Color labelColor = Colors.white;
  static Color unselectedLabelColor = Colors.white.withOpacity(0.49);

  static TextStyle labelStyle = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 28.sp,
    color: labelColor,
  );

  static TextStyle unselectedLabelStyle = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 28.sp,
    color: unselectedLabelColor,
  );

  /// 导航栏标题样式
  static TextStyle navTitle = TextStyle(
    color: Colors.white,
    fontSize: 32.sp,
  );

  /// 导航栏标题 sp 字体大小
  static double spNav = 32.sp as double;

  /// 标题1
  /// 导航栏选中标题
  static const double title1 = 32;

  /// 标题2
  /// 导航栏未选中标题
  /// TabBar 的标签页的标题
  /// 弹框标题
  /// 房间列表 item 的房间标题
  /// 私信页面列表 item 的标题
  static const double title2 = 28;

  /// 标题3
  /// 房间主播头像图标旁边的昵称
  /// TabBar 嵌套的次级的标签页的标题，Leaderboard -> Gifters -> Weeekly
  static const double title3 = 24;

  /// 正文1
  /// 弹框内容
  /// Slog 文本内容
  /// Slog 用户头像图标的昵称
  /// 个人中心页面金币, 豆子，钻石图标旁边的数字
  /// 个人中心头像下方的用户昵称
  static const double body1 = 28;

  /// 正文2
  /// 个人中心九宫格图标说明
  /// 首页金刚区图标说明
  /// 商店页面 Grid item 的商品名称
  /// 首页房间列表分段标题
  /// 首页房间列表 item 的主播昵称
  /// 首页房间列表 item 在线人数动图旁边的文字
  /// 首页 Explore Gifts 和 Receivers 图标旁边的文本
  /// 私信页面列表 item 的内容简介
  /// 个人中心的个人签名文本内容
  static const double body2 = 24;

  /// 正文3
  /// 首页 Explore 房间列表正方形 item 的房间标题，主播昵称
  static const double body3 = 22;

  /// 正文4
  /// Slog 送礼，点赞，评论，分享图标的文字
  /// 首页 Explore 房间列表正方形 item 的在线人数动图旁边的文本
  /// 个人中心的位置图标旁边的位置文本
  static const double body4 = 20;

  /// 标签1
  /// 弹框确认按钮
  static const double label1 = 28;

  /// 标签2
  /// Slog 剩余时长
  /// Slog 发布日期
  /// 首页 Game 列表 item 的操作按钮的文本，Watch，Join
  static const double label2 = 22;

  /// 标签3
  /// 首页房间列表 item 的房间类型图标的文本
  /// 首页 Game 列表 item 的房间类型标签
  /// 私信页面列表 item 的时间
  /// 个人中心用户昵称下面的用户 id
  static const double label3 = 20;

  /// 标签4
  /// 个人中心用户昵称下面的用户幸运 id 标签的文本
  /// 个人中心用户昵称下面的年龄
  static const double label4 = 16;
}
