

import 'package:flutter/material.dart';
import '../util/log_ybd_util.dart';

class YBDNavigationService {
  /// 已经打开启动页的标记位
  static bool splashed = false;
  static GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
  static Future<dynamic> navigateTo(String routeName) {
    logger.v("context${navigatorKey}, $routeName");
    return navigatorKey.currentState!.pushNamedAndRemoveUntil(routeName, (r) => r == null);
  }

  static goBack() {
    navigatorKey.currentState!.pop();
  }
}
