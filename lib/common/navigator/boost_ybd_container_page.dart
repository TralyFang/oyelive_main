

import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'navigation_ybd_service.dart';
import 'navigator_ybd_helper.dart';
import '../util/common_ybd_util.dart';
import '../util/log_ybd_util.dart';
import '../util/sp_ybd_util.dart';
import '../../module/user/entity/user_ybd_info_entity.dart';
import '../../redux/app_ybd_state.dart';
import '../../redux/user_ybd_redux.dart';
import '../../ui/widget/loading_ybd_circle.dart';

class YBDBoostContainerPage extends StatefulWidget {
  String url;

  YBDBoostContainerPage(this.url);

  @override
  YBDBoostContainerPageState createState() => new YBDBoostContainerPageState();
}

class YBDBoostContainerPageState extends State<YBDBoostContainerPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        color: Colors.white,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            YBDLoadingCircle(
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
  void buildZXhNtoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  reloadUserInfoIfNecessary() async {
    /// sp 获取用户信息
    YBDUserInfo? spUser = await YBDSPUtil.getUserInfo();
    final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    if (spUser?.id != store.state.bean?.id) {
      store.dispatch(YBDUpdateUserAction(spUser));
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      await reloadUserInfoIfNecessary();

      if (widget.url == YBDNavigatorHelper.splash) {
        if (YBDNavigationService.splashed) {
          logger.v('splash page already opened');
          // 避免重复打开 splash 页面
          return;
        }
        YBDNavigationService.splashed = true;

        Future.delayed(Duration(seconds: 2), () {
          // 这里还原标记位，切换语言启动 app 能重新打开 splash 页面
          YBDNavigationService.splashed = false;
        });
      }

      YBDNavigatorHelper.navigateTo(context, widget.url, forceFluro: true, replace: true);
    });
  }
  void initState9pRGEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDBoostContainerPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgethpznSoyelive(YBDBoostContainerPage oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
