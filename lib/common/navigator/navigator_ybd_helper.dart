import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_activity_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/common/util/route_ybd_push_util.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/ext/int_ybd_ext.dart';

import '../../ui/page/home/util/home_ybd_util.dart';
import '../../ui/page/not_ybd_found_page.dart';
import '../../ui/page/room/room_ybd_helper.dart';
import '../util/log_ybd_util.dart';
import '../util/numeric_ybd_util.dart';
import 'router_ybd_handlers.dart';

class YBDNavigatorHelper {
  static const TAG = "YBDNavigatorHelper";
  static late FluroRouter _router;

  /// "/"是默认页面
  static const boost_container = 'flutter://boost_container';

  static const splash = "flutter://splash";
  static const index_page = "flutter://index_page";
  static const search = 'flutter://search';
  static const recommended = 'flutter://recommended';
  static const login_indoor = "flutter://login_indoor";
  static const login_indoor_phone = "flutter://login_indoor_phone";
  static const login = "flutter://login";
  static const re_login = "flutter://re_login";
  // static const set_profile = "flutter://set_profile";
  static const myprofile = 'flutter://myprofile';
  static const setting = 'flutter://setting';
  static const account_safe = 'flutter://account_safe';
  static const about = 'flutter://about';
  static const edit_profile = "flutter://edit_profile"; // 编辑个人信息
  static const forgot_password = "flutter://forgot_password";
  static const fix_password = "flutter://fix_password";
  static const account = "flutter://account";
  static const account_unlink = "flutter://account_unlink";
  static const user_profile = 'flutter://user_profile'; // 其他人的用户信息界面
  static const my_invites = 'flutter://my_invites';

  /*
  static const status = 'flutter://status';
  static const status_detail = 'flutter://status/detail';
  static const status_post = 'flutter://status/post';

  static const status_post_audio = "flutter://status/post/audio";
  static const status_post_audio_record = "flutter://status/post/audio/record";
*/
  static const photo_viewer = "flutter://photo_viewer";
  static const status_post_audio_preview = "flutter://status/audio/preview";
  static const status_post_audio_upload = "flutter://status/audio/upload";
  static const search_resource = "flutter://search/resource";
  static const upload_script = "flutter://upload/script";
  static const upload_music = 'flutter://upload_music'; // 上传音乐界面
  static const scan_local_music = 'flutter://scan_local_music'; // 扫描本地音乐界面

  static const inbox = "flutter://inbox";
  static const inbox_private_message = "flutter://inbox/private";
  static const inbox_setting = "flutter://inbox/setting";
  static const inbox_blacklist = "flutter://inbox/black"; //私信黑名单

  static const follow = "flutter://follow";

  static const home_page = 'flutter://home_page'; // 首页
  static const like_msg_page = 'flutter://like_msg_page'; // 点赞消息界面1
  static const comment_msg_page = 'flutter://comment_msg_page'; // 评论消息界面2
  static const event_msg_page = 'flutter://event_msg_page'; // 活动消息界面
  static const system_msg_page = 'flutter://system_msg_page'; // 系统消息界面
  static const gift_msg_page = 'flutter://gift_msg_page'; // Slog礼物消息界面3

  static const web_view_page = 'flutter://web_view_page';

  // 兼容旧版本的路由名称
  static const flutter_web_view_page_v1 = '/web_view_page';
  // 新版本使用带flutter前缀的路由名
  static const flutter_web_view_page = 'flutter://web_view_page_v2';
  static const friend_share_page = 'flutter://friend_share_page'; // 好友分享界面
  static const top_up = "flutter://top_up"; //充值
  static const top_up_location = "flutter://top_up_location"; //充值
  static const top_up_type = "flutter://top_up_type"; //充值类型 活动
  static const top_up_record = "flutter://top_up/record"; //充值记录页
  static const combo_list = "flutter://combo_list"; // combo 列表页
  static const cp_list = "flutter://cp_list"; // cp 列表页
  static const top_up_gift = "flutter://top_up_gift"; // 首充礼物列表页
  static const top_up_gift_location =
      "flutter://top_up_gift_location"; // 带页面路径的首充礼物列表页
  static const daily_task = "flutter://daily_task"; // 每日任务页
  static const event_center = "flutter://event_center"; // 活动中心
  static const start_live_page = "flutter://start_live_page"; // 开播页面
  static const leaderboard = "flutter://leaderboard"; // 榜单页
  static const vip = "flutter://vip"; // combo 列表页
  static const store = "flutter://store"; //商店

  /// 房间页面，兼容旧版本的路由名称
  static const flutter_room_page_v1 = '/flutter_room_page';

  /// 房间页面，新版本使用带flutter前缀的路由名
  static const flutter_room_page = 'flutter://room_page';

  // 房间列表页面，兼容旧版本的路由名称
  static const flutter_room_list_page_v1 = '/flutter_room_list_page';
  // 房间列表页面，新版本使用带flutter前缀的路由名
  static const flutter_room_list_page = 'flutter://room_list_page';

  static const game_room_page = 'flutter://game_room_page'; // 游戏房间
  static const game_tp_room_page = 'flutter://game_tp_room_page'; // 游戏tp房间
  // 游戏tp房间

  // static const room_fans = 'flutter://room_fans'; // 房间
  static const purchase_detail = "flutter://purchase_detail"; // 购买详情页
  static const purchase_detail_location =
      "flutter://purchase_detail_location"; // 购买详情页，带页面路径
  static const baggage =
      "flutter://baggage"; // 背包 需要携带参数(/userId)，没有就拼接"/"，后面拼接参数"?tabIndex=1&title=你好呀"
  static const razor_pay = "flutter://razor_pay"; //YBDRazorpayPage支付
  static const room_blacklist = "flutter://room/blacklist"; // 房间黑名单页面
  static const guardian = "flutter://guardian"; // 守护页面
  static const lite_version = "flutter://lite_version"; // Lite Version说明页面

  //------------------------1.16.0------------------------------------------
  static const search_music =
      "flutter://status/post/search_music"; // Lite Version说明页面
  static const slog_upload = "flutter://status/post/upload"; // Lite Version说明页面

  static const teenpatti_history = "flutter://teenpatti_history";
  static const teenpatti_record = "flutter://teenpatti_record";

  static const badges = 'flutter://badges';

  static const pk_record = 'flutter://pk_record'; // pk记录页
  static const bill = 'flutter://bill'; // 账单列表页
  static const currency_record = 'flutter://currency_record'; // 货币记录页(豆子，钻石，金币)

  static const search_pk = 'flutter://search_pk';

  static const once_pk = 'flutter://once_pk';

  // slog black list
  // static const slog_blacklist = 'flutter://slog_blacklist';
  static const room_fans = 'flutter://room_fans';

  static const game_room_list = 'flutter://game_room_list';

  static void configureRoutes(FluroRouter router) {
    _router = router;
    _router.notFoundHandler = Handler(
        handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
      YBDLogUtil.e("route not found...");
      return YBDNotFoundPage();
    });

    /*
    _router.define(status, handler: statusHandler);
    _router.define(status_detail + "/:id", handler: statusDetailHandler);
    _router.define(status_post, handler: statusPostHandler);
    _router.define(status_post_audio_record, handler: postAudioHandler);
    _router.define(status_post_audio + "/:index", handler: postAudioIndexHandler);
    */
    _router.define(photo_viewer + "/:images/:index/:download",
        handler: photoViewerHandler);
    // _router.define(status_post_audio_preview, handler: previewAudioHandler);
    // _router.define(status_post_audio_upload, handler: uploadAudioHandler);
    // _router.define(search_resource + "/:index", handler: searchResourceHandler);
    // _router.define(upload_script, handler: uploadScriptHandler);
    // 上传音乐界面
    _router.define(upload_music, handler: uploadMusicHandler);
    // 扫描本地音乐界面
    _router.define(scan_local_music, handler: scanLocalMusicHandler);

    _router.define(login_indoor + "/:showSMS", handler: loginIndoorHandler);
    _router.define(login_indoor_phone, handler: loginIndoorPhoneHandler);
    _router.define(login, handler: loginThirdHandler);
    _router.define(re_login, handler: reLoginHandler);
    // _router.define(set_profile + "/:token/:tokenId/:isEdit", handler: setProfileHandler);
    _router.define(edit_profile, handler: editProfileHandler);
    _router.define(
        forgot_password + '/:phoneNumber/:phoneNumberComplete/:country',
        handler: forgotPwdHandler);
    _router.define(myprofile, handler: myprofileHandler);
    _router.define(user_profile + "/:userId", handler: userProfileHandler);
    _router.define(setting, handler: settingHandler);
    _router.define(account_safe, handler: accountSafeHandler);
    _router.define(about, handler: aboutHandler);
    _router.define(boost_container + "/:url", handler: boostContainerHandler);
    _router.define(search, handler: searchHandler);
    _router.define(my_invites, handler: myInvitesHandler);
    _router.define(fix_password + "/:navigate/:location",
        handler: fixPwdHandler);
    _router.define(account, handler: accountHandler);
    _router.define(account_unlink, handler: accountUnlinkHandler);

    _router.define(inbox, handler: inboxHandler);
    _router.define(inbox_private_message + "/:userId/:queueId",
        handler: privateMessageHandler);
    _router.define(inbox_setting + "/:gender/:userId",
        handler: privateMessageSettingHandler);
    _router.define(inbox_blacklist, handler: inboxBlackHandler);

    // 点赞消息页面
    _router.define(like_msg_page + "/:queueId/:unreadAmount",
        handler: likeMsgPageHandler);

    // 评论消息页面
    _router.define(comment_msg_page + "/:queueId/:unreadAmount",
        handler: commentMsgPageHandler);

    // 活动消息页面
    _router.define(event_msg_page + "/:queueId",
        handler: eventNoticeMsgPageHandler);

    // 系统消息页面
    _router.define(system_msg_page + "/:queueId",
        handler: systemMsgPageHandler);

    // 兼容 flutter boost 网页路由
    _router.define(web_view_page, handler: webViewPageHandler);

    // 纯 flutter 版本网页路由
    _router.define(flutter_web_view_page, handler: webViewPageHandler);
    _router.define(flutter_web_view_page_v1, handler: webViewPageHandler);

    // 礼物消息页面
    _router.define(gift_msg_page + "/:queueId", handler: giftMsgPageHandler);

    // 首页
    _router.define(home_page, handler: homePageHandler);

    // combo 列表页
    _router.define(combo_list, handler: comboListPageHandler);

    // cp 列表页
    _router.define(cp_list, handler: cpListPageHandler);

    // 首充礼物列表页
    // [fromTopUp] true: 从充值页面跳转到该页面, false: 非充值页面跳转到该页面
    _router.define(top_up_gift + "/:fromTopUp", handler: topUpGiftPageHandler);
    _router.define(top_up_gift_location + "/:fromTopUp/:location",
        handler: topUpGiftLocationPageHandler);

    // 每日任务页
    _router.define(daily_task, handler: dailyTaskPageHandler);

    // 活动中心
    _router.define(event_center, handler: eventCenterPageHandler);

    // 开播页面
    _router.define(start_live_page, handler: startLivePageHandler);

    // 榜单页
    _router.define(leaderboard + "/:category", handler: leaderboardPageHandler);
    _router.define(purchase_detail, handler: purchaseDetailPageHandler);
    _router.define(purchase_detail_location + "/:location",
        handler: purchaseDetailLocationPageHandler);

    _router.define(splash, handler: splashHandler);
    _router.define(index_page, handler: mainHandler);
    _router.define(recommended, handler: recommendedHandler);
    _router.define(follow + "/:userId/:tabIndex", handler: followHandler);

    // 好友和活动分享页面
    // 好友分享：type：0，id: 房间 id，nickName：主播昵称，roomImg：房间海报
    // 活动分享：type：1，id: 活动链接，nickName：活动名称，roomImg：活动图片
    _router.define(friend_share_page + "/:type/:id/:nickName/:roomImg",
        handler: friendShareHandler);
    _router.define(top_up, handler: topUpHandler);
    _router.define(top_up_location + "/:location",
        handler: topUpLocationHandler);
    _router.define(top_up_type + "/:topUpType", handler: topUpTypeHandler);

    _router.define(bill, handler: billHandler);
    _router.define(top_up_record, handler: topUpRecordHandler);
    _router.define(store, handler: storeHandler);
    _router.define(store + '/:index', handler: storeHandler); // 指定初始标签页
    _router.define(baggage + "/:userId", handler: baggageHandler);

    _router.define(vip, handler: vipHandler);
    _router.define(vip + '/:index', handler: vipHandler);

    _router.define(flutter_room_page_v1, handler: roomHandler);
    _router.define(flutter_room_page, handler: roomHandler);
    _router.define(flutter_room_list_page, handler: roomListHandler);
    _router.define(flutter_room_list_page_v1, handler: roomListHandler);
    _router.define(game_room_page, handler: gameRoomHandler);
    _router.define(game_tp_room_page, handler: gameTPRoomHandler);

    _router.define(room_fans, handler: roomFansHandler);
    _router.define(razor_pay + "/:amount/:currency/:order_id/:order_ybd_id",
        handler: razorPayHandler);

    _router.define(room_blacklist, handler: roomBlacklistHandler);
    _router.define(guardian, handler: guardianHandler);

    _router.define(lite_version, handler: liteVersionHandler);

    // _router.define(search_music + "/:index", handler: searchMusicHandler);
    // _router.define(slog_upload, handler: slogUploadHandler);
    //teenpatti histroy 和record

    _router.define(teenpatti_history, handler: teenPattiHistoryHandler);
    _router.define(teenpatti_record, handler: teenPattiRecordHandler);

    //勋章
    _router.define(badges, handler: badgesHandler);
    // PK记录
    _router.define(pk_record, handler: pkRecordHandler);
    _router.define(currency_record + "/:currency",
        handler: currencyRecordPageHandler);

    _router.define(search_pk, handler: searchPkHandler);

    _router.define(once_pk, handler: oncePkHandler);

    // _router.define(slog_blacklist, handler: slogBlacklistHandler);

    _router.define(game_room_list + "/:title/:subCategory",
        handler: gameRoomlistHandler);
  }

  /// forceFluro:强行走flutter的fluro路由
  /// clearStack,replace只有在fluro下有作用
  /// transition：flutter路由时的过度动画
  static Future navigateTo(
    BuildContext? context,
    String? path, {
    bool replace = false,
    TransitionType transition: TransitionType.cupertino,
    bool clearStack: false,
    bool forceFluro: false,
    RouteSettings? routeSettings,
  }) async {
    logger.v(
        "context : $context, \n path : $path, , ${Get.currentRoute},preRoute:${Get.previousRoute}");

    // if (path == null || path.isEmpty || path.trim() == "#") {
    //   return;
    // }
    ///特殊路由 做拦截
    if (path?.containsListSub(YBDRoutePushUtil.routeList) ?? false) {
      YBDRoutePushUtil.pushRoute(path ?? '');
      return;
    }

    if (_router.match(path ?? '') == null) return;

    var repeatRoute = TeenConfigInfo.seatBetConfig().repeatRoute;
    if (repeatRoute) {
      logger.v("TeenConfigInfo.seatBetConfig().repeatRoute");
      if (Get.currentRoute == path && Get.previousRoute.isNotEmpty) return;
    }
    return await _router.navigateTo(
      context!,
      path ?? '',
      replace: replace,
      transition: forceFluro ? TransitionType.fadeIn : transition,
      clearStack: clearStack,
      routeSettings: routeSettings,
    );
  }

  /// 跳转到充值页面
  static Future<void> openTopUpPage(BuildContext? context,
      {String location = ''}) async {
    logger.v('topup gift location topup gift page : $location');
    if (Platform.isIOS) {
      if (location.isNotEmpty) {
        return await YBDNavigatorHelper.navigateTo(
            context, YBDNavigatorHelper.purchase_detail);
      } else {
        return await YBDNavigatorHelper.navigateTo(context,
            YBDNavigatorHelper.purchase_detail_location + "/$location");
      }
    } else {
      if (location.isNotEmpty) {
        logger.v('topup gift location topup gift page if : $location');
        // 带页面路径的充值页面路由
        return await YBDNavigatorHelper.navigateTo(
            context, YBDNavigatorHelper.top_up_location + "/$location");
      } else {
        // 不带页面路径的充值页面路由
        return await YBDNavigatorHelper.navigateTo(
            context, YBDNavigatorHelper.top_up);
      }
    }
  }

  /// 跳转到网页
  /// [url] 网页地址,
  /// [shareName] 分享标题,
  /// [shareImg] 分享图片,
  /// [title] 网页默认标题,
  /// [showNavBar] 是否显示网页标题,
  static navigateToWeb(BuildContext? context, String params) {
    YBDNavigatorHelper.navigateTo(
      context,
      YBDNavigatorHelper.flutter_web_view_page + params,
    );
  }

  // 用 YBDRoomHelper.enterRoom
  /// 跳转到房间
  /// [_roomId] 房间 id
  /// [type] 主播房间页面或观众房间页面
  /// [tag] 房间分类
  // static navigateToRoom(
  //   BuildContext context,
  //   String params, {
  //   bool replace = false,
  // }) {
  // YBDNavigatorHelper.navigateTo(
  //   context,
  //   YBDNavigatorHelper.flutter_room_page + params,
  //   replace: replace,
  // );
  // }

  static remoteNavigator(BuildContext? context, String url) {
    print("url:$url");
    if (url.startsWith("flutter")) {
      navigateTo(context, url);
    } else if (url.startsWith("http")) {
      String encodedImg = '';
      String title = '';
      String encodeUrl = Uri.encodeComponent(url);

      YBDNavigatorHelper.navigateToWeb(
        context,
        "?url=$encodeUrl&title=$title&shareImg=$encodedImg",
      );
    } else if (url.startsWith("game")) {
      String gameType = url.split("//").last;
      // YBDGameSocketApiUtil.enterViaRoute(subCategory: gameType);
    }
  }

  /// 退出当前页面，如果当前是最后一个页面则不退出
  static popPage(BuildContext context) {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      logger.v('last page not pop');
    }
  }

  /// 回到首页
  static popUntilMain(BuildContext context) {
    Navigator.popUntil(context, (route) => route.isFirst);
  }

  /// 回到指定路由页面
  static popUntilPage(BuildContext context, String routeName) {
    Navigator.popUntil(context, (route) => route.settings.name == routeName);
  }

  // TODO: 暂时没用到的方法
  /// 跳到下一个页面
  static nextPage(BuildContext context, String path) async {
    logger.v('next page context : $context, path : $path');
    await _router.navigateTo(context, path,
        replace: false, transition: TransitionType.cupertino);
  }

  // TODO: 暂时没用到的方法
  /// 替换当前页面
  static replacePage(BuildContext context, String path) async {
    logger.v('replace page context : $context, path : $path');
    await _router.navigateTo(context, path,
        replace: true, transition: TransitionType.cupertino);
  }

  /// 导航栏返回到上一个页面
  static backAction(BuildContext context) {
    logger.v('pop page context : $context');
    _router.pop(context);
  }

  /// 发布音频动态成功后退出发布页面
  static popAudioStatusPages(BuildContext context) async {
    logger.v('pop to root with context : $context');
    Navigator.popUntil(context, (route) => route.isFirst);
  }

  /// 清理聊天记录后关闭页面
  static popClearChatPage(BuildContext context) async {
    Navigator.popUntil(context, (route) => route.isFirst);
  }

  /// 发布音频动态成功后退出发布页面
  static popSlogPage(BuildContext context, {bool pop: false}) async {
    logger.v('pop to root with context : $context');
    logger.v('full flutter env pop');
    Navigator.popUntil(context, (route) => route.isFirst);
  }

  /// 通过解析 json 字符串跳转到对应的页面
  // TODO: 后台返回来的消息格式和分享消息格式不一样，先用 apiMsg 字段区分
  static onTapShare(BuildContext context, var jsonData, {bool apiMsg = false}) {
    var click = jsonData['content']['click'];
    if (click != null) {
      if (click['action'] != null && click['action'] == "redirect") {
        String? url = click['url'];

        // TODO: 测试代码 全部切完 flutter 后移除这个判断
        // v1.12.0 已经支持跳转到 flutter 版充值记录页面
        if (url == "native://transaction_record") {
          url = 'flutter://top_up/record';
        }

        if (url == "flutter://top_up") {
          if (Platform.isIOS) {
            url = "flutter://purchase_detail";
          }
        }

        if (url != null) {
          if (url.startsWith("flutter")) {
            YBDNavigatorHelper.navigateTo(context, url);
          } else if (url.startsWith("http")) {
            String? encodedImg = '';
            String? title = '';
            String encodeUrl = Uri.encodeComponent(url);

            if (apiMsg) {
              try {
                encodedImg =
                    Uri.encodeComponent(jsonData['content']['resource']);
                title = jsonData['content']['title'];
              } catch (e) {
                logger.e('==jsonData error : $e');
              }
              alog.v('22.8.1---json:$jsonData');
              // 进入活动埋点
              YBDActivityInTrack().activityIn(YBDTAProps(
                  location: YBDTAEventLocation.event_notice, url: url));
              YBDNavigatorHelper.navigateToWeb(
                context,
                "?url=$encodeUrl&title=$title&shareImg=$encodedImg&entranceType=${WebEntranceType.EventNotice.index}",
              );

              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                YBDEventName.CLICK_EVENT,
                location: YBDLocationName.INBOX_PAGE,
                itemName: YBDItemName.INBOX_MSG,
                adUnitName: title,
              ));
            } else {
              try {
                encodedImg = jsonData['content']['attributes']['adImg'];
                title = jsonData['content']['attributes']['adName'];
              } catch (e) {
                logger.e('==jsonData error : $e');
              }

              YBDNavigatorHelper.navigateToWeb(
                context,
                "?url=$encodeUrl&title=${Uri.encodeComponent(title!)}&shareImg=${Uri.encodeComponent(encodedImg!)}",
              );
            }
          } else if (url.startsWith("native")) {
            // openNativePage(context, url, location: 'inbox_message_item');
            // YBDNavigatorHelper.navigateTo(context, url.replaceAll("native", "flutter"));
            if (url.contains("room?roomId=")) {
              YBDRoomHelper.enterRoom(
                context,
                YBDNumericUtil.stringToInt(url.split("roomId=").last),
                replace: true,
              );
              // navigateToRoom(
              //   context,
              //   '?roomId=${url.split("roomId=").last}&type=1',
              //   replace: true,
              // );
            } else if (url.contains("game_room_page?roomId=")) {
              // YBDGameSocketApiUtil.quickEnterRoom(
              //     type: EnterGameRoomType.Join,
              //     roomId: url.split("roomId=").last);
            }
          } else {
            logger.v('unknown url : $url');
          }
        } else {
          logger.v('url is null');
        }
      } else {
        logger.v('acton is invalid');
      }
    } else {
      logger.v('click is null');
    }
  }

  /// 解析 url 跳转到 h5 或 app 里的页面
  /// http 开头打开网页
  /// flutter 开头打开 app 页面
  static openUrl(BuildContext? context, String url,
      {String? title, WebEntranceType? type}) {
    if (url.startsWith("http")) {
      // FlutterBoost.singleton.open(getNativePageUrl(NativePageName.WEB_VIEW), urlParams: {
      //   'EVENT_KEY': 'event',
      //   'TITLE_KEY': title,
      //   'URL_KEY': url,
      // });

      // 解析出url的参数
      late Uri u;
      try {
        u = Uri.parse(url);
      } catch (e) {
        logger.e('openUrl parse fail : $e');
      }
      title = title ?? u.queryParameters['title'];

      YBDNavigatorHelper.navigateToWeb(
        context,
        "?url=${Uri.encodeComponent(url)}&title=$title&entranceType=${type!.index}",
      );
    } else if (url.startsWith("flutter")) {
      YBDNavigatorHelper.navigateTo(context, url);
    } else {
      YBDLogUtil.w('can not parse url : $url');
    }
  }

  /// 通过解析跳转到对应的原生页面
  static openNativePage(BuildContext? context, String jsonStr,
      {String? location}) async {
    Map<String, dynamic>? pathAndParams =
        YBDHomeUtil.parseNativePathWithUrlParams(jsonStr);

    if (null != pathAndParams && pathAndParams is Map) {
      String? path = pathAndParams['path'];
      Map<String, dynamic>? params = pathAndParams['params'];

      if (null == params) {
        // 无参数的页面跳转
        if (path!.contains('native://live_now')) {
          // 开播跳转
          YBDHomeUtil.goLive(context, location: location);
        } else if (path.contains('native://room')) {
          // 跳转房间
          YBDRoomHelper.enterRoom(context, 0, location: location);
        } else {
          // 跳转到其他页面
          flutterBoostOpen(path);
        }
      } else {
        // 有参数的页面跳转
        if (path!.contains('native://room')) {
          // 跳转房间
          int roomId = YBDNumericUtil.stringToInt(params['roomId']);
          YBDRoomHelper.enterRoom(context, roomId, location: location);
        } else {
          // 跳转到其他页面
          logger.v('native path : $path, params : ${json.encode(params)}');
          flutterBoostOpen(path, urlParams: params);
        }
      }
    } else {
      logger.v('can not parse native path : $pathAndParams');
    }
  }

  /// FlutterBoost 打开页面
  static Future<void> flutterBoostOpen(String? path,
      {Map<String, dynamic>? urlParams}) async {
    //
  }

  /// FlutterBoost 关闭当前页面
  static flutterBoostCloseCurrent() {
    //
  }

  /// 添加 FlutterBoost 容器生命周期监听器
  // TODO: jackie - 重新实现该方法
  static addFlutterBoostObserver(dynamic callback) {
//  FlutterBoost.singleton.addBoostContainerLifeCycleObserver(
//  (state, settings) {
//    print("boost lifecycle ${settings.params}" + state.toString());
//
//    switch (state) {
//      case ContainerLifeCycle.Init:
//      // TODO: Handle this case.
//        break;
//      case ContainerLifeCycle.Appear:
//      // TODO: Handle this case.
//        break;
//      case ContainerLifeCycle.WillDisappear:
//      // TODO: Handle this case.
//        break;
//      case ContainerLifeCycle.Disappear:
//      // TODO: Handle this case.
//        print("boost lifecycle ${settings.params} : stop music");
//
//        context.read<StatusPlayerBloc>().stopAll();
//
//        break;
//      case ContainerLifeCycle.Destroy:
//      // TODO: Handle this case.
//        break;
//      case ContainerLifeCycle.Background:
//      // TODO: Handle this case.
//        break;
//      case ContainerLifeCycle.Foreground:
//      // TODO: Handle this case.
//        break;
//    }
//  });
  }

  /// 返回目标路由的上一页, 即回到进来的页面, 可以有效pop目标页面上层的所有页面
  static popUntilPre(String route) {
    //  YBDNavigatorHelper.game_tp_room_page
    int index = YBDObsUtil.instance().routeSets.toList().indexOf(route);
    logger.v(
        "popUntilPre routeSets: ${YBDObsUtil.instance().routeSets}, index: $index");
    if (index > 0) {
      // 获取前一个界面
      String preRouteName = YBDObsUtil.instance().routeSets.toList()[index - 1];
      Navigator.popUntil(Get.context!, (route) {
        logger.v(
            "popUntilPre route: ${route.settings.name}, pre: $preRouteName}");
        if (route.isFirst) {
          return true; // 已经是最底层了，不能再往下了。
        }
        if (route.settings.name?.startsWith(preRouteName) ?? false) {
          return true;
        }
        return false;
      });
    } else {
      logger.v("popUntilPre route: $route, is not exist. index: $index");
    }
  }
}
