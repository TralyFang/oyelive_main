import 'dart:io';

import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_lobby/room_ybd_list_page.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_bloc.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/game_ybd_room_page.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/tp_ybd_room_page.dart';
import 'package:oyelive_main/ui/page/home/event_ybd_center_page.dart';
import 'package:oyelive_main/ui/page/profile/account_safe/account_ybd_safe_page.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/aristocracy_ybd_page.dart';
import 'package:oyelive_main/ui/page/profile/setting/account_ybd_page.dart';
import 'package:oyelive_main/ui/page/profile/setting/account_ybd_unlink_page.dart';
import 'package:oyelive_main/ui/page/profile/setting/fix_ybd_pwd_page.dart';
import 'package:oyelive_main/ui/page/recharge/purchase_ybd_detail_page.dart';
import 'package:oyelive_main/ui/page/recharge/record/bill_ybd_page.dart';
import 'package:oyelive_main/ui/page/recharge/record/currency_ybd_record_page.dart';
import 'package:oyelive_main/ui/page/room/pk/once_ybd_pk_page.dart';
import 'package:oyelive_main/ui/page/room/pk/search_ybd_pk_page.dart';
import 'package:oyelive_main/ui/page/room/room_ybd_fans_page.dart';
import 'package:oyelive_main/ui/page/room/room_ybd_page.dart';

import '../../base/page_ybd_bloc_provider.dart';
import '../../ui/page/baggage/baggage_ybd_page.dart';
import '../../ui/page/follow/follow_ybd_details.dart';
import '../../ui/page/home/combo_ybd_list_page.dart';
import '../../ui/page/home/cp_ybd_list_page.dart';
import '../../ui/page/home/home_ybd_page.dart';
import '../../ui/page/home/widget/tp_ybd_web_view.dart';
import '../../ui/page/inbox/black/inbox_ybd_blacklist.dart';
import '../../ui/page/inbox/gift_message/gift_ybd_message_page.dart';
import '../../ui/page/inbox/inbox_ybd_page.dart';
import '../../ui/page/inbox/private_message/contact_setting/contact_ybd_setting_page.dart';
import '../../ui/page/inbox/private_message/private_ybd_message_page.dart';
import '../../ui/page/inbox/system_msg/comment_ybd_msg_page.dart';
import '../../ui/page/inbox/system_msg/event_ybd_notice_page.dart';
import '../../ui/page/inbox/system_msg/like_ybd_msg_page.dart';
import '../../ui/page/inbox/system_msg/system_ybd_msg_page.dart';
import '../../ui/page/index_ybd_page.dart';
import '../../ui/page/leaderboard/define/leaderboard_ybd_def.dart';
import '../../ui/page/leaderboard/leaderboard_ybd_page.dart';
import '../../ui/page/login/bloc/phonenumber_ybd_input_bloc.dart';
import '../../ui/page/login/forgot_ybd_password_page.dart';
import '../../ui/page/login/login_ybd_indoor_page.dart';
import '../../ui/page/login/login_ybd_indoor_phone_page.dart';
import '../../ui/page/login/login_ybd_page.dart';
import '../../ui/page/profile/guardian/guardian_ybd_page.dart';
import '../../ui/page/profile/invite_friends/invite_ybd_friends_page.dart';
import '../../ui/page/profile/my_invites/my_ybd_invites_page.dart';
import '../../ui/page/profile/my_invites/my_ybd_invites_page_bloc.dart';
import '../../ui/page/profile/my_profile/badge_ybd_page.dart';
import '../../ui/page/profile/my_profile/daily_ybd_task_page.dart';
import '../../ui/page/profile/my_profile/my_ybd_profile_page.dart';
import '../../ui/page/profile/my_profile/pk_ybd_record_page.dart';
import '../../ui/page/profile/my_profile/top_ybd_up_gift_page.dart';
import '../../ui/page/profile/profile_ybd_edit_page.dart';
import '../../ui/page/profile/setting/about_ybd_page.dart';
import '../../ui/page/profile/setting/lite_ybd_version_page.dart';
import '../../ui/page/profile/setting/setting_ybd_page.dart';
import '../../ui/page/profile/user_profile/user_ybd_profile_page.dart';
import '../../ui/page/profile/vip/vip_ybd_page.dart';
import '../../ui/page/recharge/razorpay/razorpay_ybd_page.dart';
import '../../ui/page/recharge/recharge_ybd_page.dart';
import '../../ui/page/recharge/record/topup_ybd_record_page.dart';
import '../../ui/page/recommended/recommended_ybd_page.dart';
import '../../ui/page/room/blacklist/room_ybd_blacklist_page.dart';
import '../../ui/page/room/bloc/combo_ybd_bloc.dart';
import '../../ui/page/room/bloc/music_ybd_player_bloc.dart';
import '../../ui/page/room/bloc/pk_ybd_state_bloc.dart';
import '../../ui/page/room/bloc/room_ybd_bloc.dart';
import '../../ui/page/room/define/room_ybd_define.dart';
import '../../ui/page/room/mic/mic_ybd_bloc.dart';
import '../../ui/page/room/room_ybd_list_page.dart';
import '../../ui/page/room/start_live/start_ybd_live_page.dart';
import '../../ui/page/room/teen_patti/pages/teen_ybd_history_page.dart';
import '../../ui/page/room/teen_patti/pages/teen_ybd_record_page.dart';
import '../../ui/page/search/bloc/search_ybd_page_bloc.dart';
import '../../ui/page/search/search_ybd_page.dart';
import '../../ui/page/share/friend_ybd_share_page.dart';
import '../../ui/page/share/util/friend_ybd_share_util.dart';
import '../../ui/page/splash/splash_ybd_page.dart';
import '../../ui/page/status/local_audio/local_ybd_music_list_page.dart';
import '../../ui/page/status/local_audio/upload_ybd_music_page.dart';
// import '../../ui/page/status/post/music_ybd_resource_page.dart';
// import '../../ui/page/status/post/post_ybd_pre_bloc.dart';
// import '../../ui/page/status/post/post_ybd_pre_page.dart';
// import '../../ui/page/status/post/post_ybd_upload_page.dart';
// import '../../ui/page/status/post/tips_ybd_bloc.dart';
// import '../../ui/page/status/post_audio/post_ybd_audio_page.dart';
// import '../../ui/page/status/post_audio/preview_ybd_audio_page.dart';
// import '../../ui/page/status/post_audio/upload_ybd_audio_page.dart';
// import '../../ui/page/status/post_audio/upload_ybd_script_page.dart';
// import '../../ui/page/status/post_picture_text/status_ybd_post_page.dart';
// import '../../ui/page/status/status_detail/status_ybd_detail_page.dart';
// import '../../ui/page/status/status_ybd_list_page.dart';
import '../../ui/page/store/store_ybd_page.dart';
import '../../ui/widget/photo_ybd_viewer.dart';
import '../analytics/analytics_ybd_util.dart';
import '../util/log_ybd_util.dart';
import '../util/numeric_ybd_util.dart';
import 'boost_ybd_container_page.dart';

// var statusHandler = Handler(
//     handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   return YBDStatusListPage();
// });
//
// var statusDetailHandler = Handler(
//     handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   return MultiBlocProvider(providers: [
//     BlocProvider<YBDCommentBloc>(
//       create: (_) => YBDCommentBloc(YBDCommentState()),
//     ),
//   ], child: YBDStatusDetailPage(params['id']![0]));
// });

// var statusPostHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   return YBDStatusPostPage();
// });

var loginIndoorHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  bool showSMS = false;
  if (null != params['showSMS']?.first) {
    showSMS = params['showSMS']![0] == 'true';
  } else {
    logger.v('showSMS is empty');
  }

  return MultiBlocProvider(
    providers: [
      BlocProvider<YBDPhoneNumberInputBloc>(
        create: (_) => YBDPhoneNumberInputBloc(),
      ),
    ],
    child: YBDLoginIndoorPage(showSMS),
  );
});

var loginIndoorPhoneHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return MultiBlocProvider(
    providers: [
      BlocProvider<YBDPhoneNumberInputBloc>(
        create: (_) => YBDPhoneNumberInputBloc(),
      ),
    ],
    child: YBDLoginIndoorPhonePage(),
  );
});

var loginThirdHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDLoginPage();
});

var reLoginHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDLoginPage(reLogin: 'reLogin');
});

// var setProfileHandler = Handler(handlerFunc: (BuildContext context, Map<String, List<String>> params) {
//   return YBDSetProfilePage(params['token'][0], params['tokenId'][0], isEdit: params['isEdit'][0] == 'true');
// });

/// 编辑个人信息
var editProfileHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDProfileEditPage();
});

var forgotPwdHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDForgotPasswordPage(
    initCountry: params['country']![0],
    phoneNumber: params['phoneNumber']![0],
    phoneNumberComplete: params['phoneNumberComplete']![0],
  );
});

var myprofileHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDMyProfilePage();
});

var userProfileHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  int userId = YBDNumericUtil.stringToInt(params['userId']?.first);
  return YBDUserProfilePage(userId);
});

var settingHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDSettingPage();
});

var accountSafeHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDAccountSafePage();
});

var aboutHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDAboutPage();
});

// 上传音乐界面
var uploadMusicHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDUploadMusicPage();
});

// 扫描本地音乐界面
var scanLocalMusicHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDLocalMusicListPage();
});

var boostContainerHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDBoostContainerPage(params['url']![0]);
});

var photoViewerHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  int index = YBDNumericUtil.stringToInt(params['index']?.first);
  return YBDPhotoViewer(
    params['images']![0].replaceAll("<>", "/").split(","),
    index,
    download: params['download']![0] == 'true',
  );
});

var searchHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return MultiBlocProvider(providers: [
    BlocProvider<YBDSearchPageBloc>(
      create: (_) => YBDSearchPageBloc(),
    ),
  ], child: YBDSearchPage());
});

// var postAudioHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   return MultiBlocProvider(providers: [
//     BlocProvider<YBDPostPreBloc>(
//       create: (_) => YBDPostPreBloc(YBDPostPreState()),
//     ),
//     BlocProvider<YBDTipsBloc>(
//       create: (_) => YBDTipsBloc(TipsEvent.Show),
//     ),
//   ], child: YBDPostPrePage());
// });
//
// var postAudioIndexHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   int index = YBDNumericUtil.stringToInt(params['index']?.first);
//   return YBDPostAudioPage(index: index);
// });
//
// var previewAudioHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   return YBDPreviewAudioPage();
// });
//
// var uploadAudioHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   return YBDUploadAudioPage();
// });
//
// var searchResourceHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   int index = YBDNumericUtil.stringToInt(params['index']?.first);
//   return YBDSearchResourcePage(index);
// });
//
// var uploadScriptHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   return YBDUploadScriptPage();
// });

var myInvitesHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDInviteFriendsPage();
  return YBDPageBlocProvider<YBDMyInvitesPageBloc>(
    bloc: YBDMyInvitesPageBloc(),
    child: YBDMyInvitesPage(),
  );
});

var inboxHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDInboxPage();
});

var fixPwdHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  bool navigateToHome = params['navigate']![0] == 'true';
  String location = params['location']?.first ?? '';
  return YBDFixPwdPage(navigateToHome, location: location);
});
var accountHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDAccountPage();
});
var accountUnlinkHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDAccountUnlinkPage(
      params['title']?.first ?? '', params['type']?.first ?? '0');
});

var privateMessageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  int userId = YBDNumericUtil.stringToInt(params['userId']?.first);
  return YBDPrivateMessagePage(userId, params['queueId']![0]);
});

var inboxBlackHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDInboxBlacklistPage();
});

var privateMessageSettingHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  // 0 未知性别
  int gender = YBDNumericUtil.stringToInt(params['gender']?.first);
  int userId = YBDNumericUtil.stringToInt(params['userId']?.first);
  return YBDContactSettingPage(gender, userId);
});

/// 首页
var homePageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDHomePage();
});

/// combo 列表页
var comboListPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDComboListPage();
});

/// cp 列表页
var cpListPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDCpListPage();
});

/// 首充礼物列表页
var topUpGiftPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  logger.v(
      'topup gift location topUpGiftPageHandler : ${params['fromTopUp']![0]}');
  return YBDTopUpGiftPage(fromTopUp: params['fromTopUp']![0]);
});

/// 带页面路径的首充礼物列表路由
var topUpGiftLocationPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  logger.v(
      'topup gift location topUpGiftLocationPageHandler : ${params['location']![0]}');
  return YBDTopUpGiftPage(
      fromTopUp: params['fromTopUp']![0], location: params['location']![0]);
});

/// 每日任务页
var dailyTaskPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
    YBDEventName.OPEN_PAGE,
    location: YBDLocationName.TASK_PAGE,
    // itemName: YBDItemName.,
  ));
  return YBDDailyTaskPage();
});

/// 活动中心页面
var eventCenterPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDEventCenterPage();
});

/// 开播页面
var startLivePageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  int? funcType = int.tryParse(params['funcType']?.first ?? "-1");
  logger.v("start page functype: $funcType");
  return YBDStartLivePage(funcType: funcType);
});

/// 榜单页
var leaderboardPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  LeaderboardCategory category = LeaderboardCategory.Streamers;

  if (params['category']![0] == '1') {
    category = LeaderboardCategory.Gifts;
  } else if (params['category']![0] == '2') {
    category = LeaderboardCategory.Followers;
  }

  return YBDLeaderboardPage(category);
});

/// 购买详情页
var purchaseDetailPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDPurchaseDetailPage();
});

/// 购买详情页
var purchaseDetailLocationPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDPurchaseDetailPage(location: params['location']![0]);
});

/// 点赞消息页面
var likeMsgPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  int amountInt = YBDNumericUtil.stringToInt(params['unreadAmount']?.first);
  return YBDLikeMsgPage(params['queueId']![0], unreadAmount: amountInt);
});

/// 评论消息页面
var commentMsgPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  int amountInt = YBDNumericUtil.stringToInt(params['unreadAmount']?.first);
  return YBDCommentMsgPage(params['queueId']![0], unreadAmount: amountInt);
});

/// 活动消息页面
var eventNoticeMsgPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDEventNoticePage(params['queueId']![0]);
});

/// 系统消息页面
var systemMsgPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDSystemMessagePage(params['queueId']![0]);
});

/// 礼物消息页面
var giftMsgPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDGiftMessagePage(params['queueId']![0]);
});

/// 网页容器页面
var webViewPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  String url = Uri.decodeComponent(params['url']?.first ?? '');
  if (url.contains('?')) {
    url += '&isFlutter=1';
  } else {
    url += '?isFlutter=1';
  }
  logger.v("on router url $url, params: $params");
  Uri? u;
  try {
    u = Uri.parse(url);
  } catch (e) {
    logger.e('url trans fail : $e');
  }
  bool showNavBar = params['showNavBar'] != null
      ? (params['showNavBar']?.first == 'false' ? false : true)
      : (u!.queryParameters.keys.contains('showNavBar')
          ? (u.queryParameters.keyToInt('showNavBar') == 1)
          : true);
  logger.v('showNavBar: ${u!.queryParameters}');

  return YBDTPWebView(
    url,
    shareName: _paramWithKey(params, u, 'shareName'),
    shareImg: _paramWithKey(params, u, 'shareImg'),
    pageTitle: Uri.decodeComponent(_paramWithKey(params, u, 'title')),
    showNavBar: showNavBar,
    entranceType: _paramWithKey(params, u, 'entranceType').getType(),
  );
});

String _paramWithKey(
    Map<String, List<String>> params, Uri? uri, String paramKey) {
  String? paramValue = params[paramKey]?.first;

  if (null == paramValue || paramValue.isEmpty) {
    paramValue = uri!.queryParameters[paramKey] ?? '';
    if (paramValue == null || paramValue.isEmpty) {
      // 兼容处理带#的地址：http://121.37.214.86/h5/talent/#/ExchangeGold?title=aaa
      List<String> list = uri.fragment.split('?'); // /ExchangeGold?title=aaa
      String keyValue = list.firstWhere(
          (String element) => element.contains(paramKey),
          orElse: () => '');
      if (keyValue.isNotEmpty) {
        paramValue = keyValue.split('=').last;
      }
    }
  }

  return paramValue;
}

var splashHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDSplashPage();
});

var mainHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  int? index;
  try {
    index = int.parse(params['index']![0]);
    YBDSPUtil.remove(Const.SP_NEED_FIX_PWD);
  } catch (e) {
    print(e);
  }
  return YBDIndexPage(
    defaultIndex: index ?? 0,
  );
});

var recommendedHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDRecommendedPage();
});

var followHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  int userId = YBDNumericUtil.stringToInt(params['userId']?.first);
  int tabIndex = YBDNumericUtil.stringToInt(params['tabIndex']?.first);
  return YBDFollowDetailsListPage(userId, tabIndex);
});

// 好友分享页面
var friendShareHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  if ('0' == params['type']![0]) {
    int roomId = YBDNumericUtil.stringToInt(params['id']?.first);
    FriendShareType type = FriendShareType.Unknown;

    type = FriendShareType.ShareRoom;
    return YBDFriendSharePage(
      type,
      roomId: roomId,
      nickName: params['nickName']![0],
      roomImg: Uri.decodeComponent(params['roomImg']![0]),
    );
  } else if ('1' == params['type']![0]) {
    FriendShareType type = FriendShareType.ShareAd;
    return YBDFriendSharePage(
      type,
      adUrl: Uri.decodeComponent(params['id']![0]),
      adName: params['nickName']![0],
      adImg: Uri.decodeComponent(params['roomImg']![0]),
    );
  } else if ('2' == params['type']![0]) {
    //nickName/:roomImg
    FriendShareType type = FriendShareType.ShareBadge;
    return YBDFriendSharePage(
      type,
      adName: params['nickName']![0],
      adImg: Uri.decodeComponent(params['roomImg']![0]),
    );
  } else {
    FriendShareType type = FriendShareType.Unknown;
    return YBDFriendSharePage(type);
  }
});

var topUpHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDRechargePage();
});

/// 带页面路径的充值页面路由
var topUpLocationHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDRechargePage(
    location: params['location']![0],
  );
});

/// 充值活动
var topUpTypeHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  if (Platform.isIOS)
    return YBDPurchaseDetailPage(topUpType: params['topUpType']![0]);
  return YBDVipPage();
});

/// bill页
var billHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDBillPage();
});

var topUpRecordHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDTopupRecordPage();
});

var vipHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  if (params['index'] == null) return YBDAristocracyPage(-1);
  int index = YBDNumericUtil.stringToInt(params['index']?.first);
  return YBDAristocracyPage(index);
  return YBDVipPage();
});

var storeHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDStorePage(
      initIndex: YBDNumericUtil.stringToInt(params['index']?.first));
});

@Deprecated(
  'Use YBDRoomHelper.enterRoom and roomListHandler'
  'This handler was deprecated',
)
var roomHandler = Handler(handlerFunc: (
  BuildContext? context,
  Map<String, List<String>> params,
) {
  Widget widget;
  int roomId = YBDNumericUtil.stringToInt(params['roomId']?.first);
  String tag = params['tag']?.first ?? ROOM_DEFAULT_TAG;
  // RoomPageType type = RoomPageType.User;

  // if ((params['type']?.first ?? '0') == '1') {
  //   type = RoomPageType.Talent;
  // }

  // 从开播页面进入房间
  widget = YBDRoomPage();
  // widget = GameRoomPageTest();

  YBDPkStateBloc pkStateBloc = new YBDPkStateBloc(context);
  return MultiBlocProvider(
    providers: [
      BlocProvider<YBDMicBloc>(
        create: (context) {
          return YBDMicBloc(context, roomIdx: roomId);
        },
      ),
      BlocProvider<YBDRoomBloc>(
        create: (context) {
          return YBDRoomBloc(
            context: context,
            roomId: roomId,
            roomTag: tag,
            pkStateBloc: pkStateBloc,
          );
        },
      ),
      BlocProvider<YBDPkStateBloc>(
        create: (context) {
          return pkStateBloc;
        },
      ),
      BlocProvider<YBDMusicPlayerBloc>(
        create: (context) {
          return YBDMusicPlayerBloc();
        },
      ),
      BlocProvider<YBDRoomComboBloc>(
        create: (context) {
          return YBDRoomComboBloc();
        },
      ),
    ],
    child: widget,
  );
});

/// 游戏房间路由处理
var gameRoomHandler = Handler(handlerFunc: (
  BuildContext? context,
  Map<String, List<String>> params,
) {
  return MultiBlocProvider(providers: [
    BlocProvider<YBDGameMicBloc>(
        create: (_) => YBDGameMicBloc(YBDGameMicBlocState(), context))
  ], child: YBDGameRoomPage());
});

/// 游戏TP房间路由处理
var gameTPRoomHandler = Handler(handlerFunc: (
  BuildContext? context,
  Map<String, List<String>> params,
) {
  String? roomId = params['roomId']?.first;
  String socketUrl = params['socketUrl']?.first ?? '';
  socketUrl = Uri.decodeComponent(socketUrl);
  return YBDTPRoomPage(roomId: roomId, socketUrl: socketUrl);
});

/// 房间列表页面路由处理器
var roomListHandler = Handler(handlerFunc: (
  BuildContext? context,
  Map<String, List<String>> params,
) {
  int roomId = YBDNumericUtil.stringToInt(params['roomId']?.first);
  String tag = params['tag']?.first ?? ROOM_DEFAULT_TAG;
  int? funcType = int.tryParse(params['funcType']?.first ?? "-1");
  int? gameCode = int.tryParse(params['gameCode']?.first ?? "-1");
  YBDPkStateBloc pkStateBloc = YBDPkStateBloc(context);

  return MultiBlocProvider(
    providers: [
      BlocProvider<YBDRoomBloc>(
        create: (_) {
          return YBDRoomBloc(
            context: context,
            roomId: roomId,
            roomTag: tag,
            pkStateBloc: pkStateBloc,
            funcType: funcType,
            gameCode: gameCode,
          );
        },
      ),
      BlocProvider<YBDMicBloc>(
        create: (_) {
          return YBDMicBloc(context, roomIdx: roomId);
        },
      ),
      BlocProvider<YBDPkStateBloc>(
        create: (context) {
          return pkStateBloc;
        },
      ),
      BlocProvider<YBDMusicPlayerBloc>(
        create: (_) {
          return YBDMusicPlayerBloc();
        },
      ),
      BlocProvider<YBDRoomComboBloc>(
        create: (_) {
          return YBDRoomComboBloc();
        },
      ),
    ],
    child: YBDRoomListPage(
      roomId: roomId,
      tag: tag,
      // roomPageType: type,
    ),
  );
});

var roomFansHandler = Handler(handlerFunc: (context, params) {
  final args = context!.settings!.arguments as YBDRoomFansPage?;
  return args;
});
var razorPayHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDRazorpayPage(
      params['amount']![0],
      params['currency']![0],
      params['order_id']![0],
      params['order_ybd_id']![0]); //targetId, this.money
});

var baggageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  int? userId;
  if (null != params['userId']?.first) {
    if (YBDNumericUtil.isNumeric(params['userId']!.first)) {
      userId = int.parse(params['userId']!.first);
    }
  } else {
    logger.v('user id is invalid : ${params['userId']?.first}');
  }
  int tabIndex = int.tryParse(params['tabIndex']?.first ?? "0") ?? 0;
  return YBDBaggagePage(userId: userId, tabIndex: tabIndex);
});

/*var razorPayHandler = Handler(handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return YBDRazorpayPage(params['targetId'][0], params['money'][0]); //targetId, this.money
});*/

/// 发 v1.17.0 版本时要去掉黑名单页面（混合包没有房间重构代码）
var roomBlacklistHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDRoomBlacklistPage();
});

var guardianHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDGuardianPage();
});

var liteVersionHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDLiteVersionPage();
});

// var searchMusicHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   return YBDMusicResourcePage(int.parse(params['index']![0]));
// });
// var slogUploadHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   return YBDPostUploadPage();
// });

var teenPattiHistoryHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDTeenHistoryPage();
});

var teenPattiRecordHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDTeenRecordPage();
});

var badgesHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDBadgePage();
});
var pkRecordHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDPKRecordPage();
});

/// 货币获得记录页
var currencyRecordPageHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDCurrencyRecord(currency: params['currency']![0]);
});

var searchPkHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDSearchPkPage();
});

var oncePkHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDOncePkPage();
});

// var slogBlacklistHandler = Handler(
//     handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
//   return YBDSlogBlackListPage();
// });

var gameRoomlistHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return YBDGameRoomListPage(params['title']![0], params['subCategory']![0]);
});
