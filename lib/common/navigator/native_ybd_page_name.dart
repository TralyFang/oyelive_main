

import '../util/log_ybd_util.dart';
import '../../module/user/entity/user_ybd_info_entity.dart';

const NATIVE_USER_TOKEN = "userToken";
const NATIVE_SESSION_ID = "sessionId";
const NATIVE_COOKIE_APP = "cookieApp";
const NATIVE_USER_INFO = "userInfo";

enum NativePageName {
  LOGOUT,

  /// 从原生弹出勋章弹框
  BADGE_DIALOG,

  /// 从原生弹出充值送礼弹框
  TOPUP_GIFT_DIALOG,

  // EDIT_PROFILE,
  // SETTING,
  // SET_LOCATION,

  // BALANCE,
  LEVEL_INTRO,
  TOP_UP,
  // BAGGAGE,

  // GUARDIAN,

  // TALENT_TIME,

  // 进入房间
  ROOM,

  // 跳转到 MainActivity
  HOME,

  // 广告 banner 跳转到原生的 web view
  WEB_VIEW,

  // 支付跳转原生WebView
  WEB_VIEW_PAY,

  // 每日任务弹框更多奖励跳转到原生
  // MORE_REWARD,

  // 主页右下角 悬浮按钮跳转原生
  // LIVE_FOLLOW,

  // 首页切换语言
  CHANGE_LANGUAGE,
}

String getNativePageUrl(NativePageName name) {
  String pageUrl = "native://" + name.toString().split(".").last.toLowerCase();
  logger.v('native page name : $pageUrl');
  return pageUrl;
}

/// 打开 MainActivity
void openNativeHomePage(String cookieApp, String sessionId, String userToken, YBDUserInfo userInfo) {
  // print('====openNativeHomePage : $cookieApp');
  // print('====openNativeHomePage : $sessionId');
  // print('====openNativeHomePage : $userToken');
  // print('====openNativeHomePage : ${userInfo.toJson()}');
  // final Map<String, dynamic> params = {
  //   NATIVE_USER_TOKEN: userToken,
  //   NATIVE_COOKIE_APP: cookieApp,
  //   NATIVE_SESSION_ID: sessionId,
  // };
  // FlutterBoost.singleton.open(getNativePageUrl(NativePageName.HOME), urlParams: params, exts: userInfo.toJson());
}
