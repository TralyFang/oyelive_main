

/*
 * @Author: William
 * @Date: 2022-08-11 14:32:46
 * @LastEditTime: 2022-09-30 11:25:13
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/analytics/thinking/ta_loading_track.dart
 */
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/short_ybd_route.dart';

class YBDLoadTrack extends TATrackBase {
  void loadingTrack(YBDTAProps prop) {
    if (openTrack) track(YBDTATrackEvent.generic_loading_time, properties: prop.toJson());
  }
  void loadingTracktYiSgoyelive(YBDTAProps prop) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool canTrack(String? eventName, {Map<String, dynamic>? properties}) {
    if (properties!['type'] == YBDTATimeTrackType.event_load && YBDShortRoute.non_event.contains(TA.routeStack.last))
      return false;
    return true;
  }
  void canTracku4RbAoyelive(String? eventName, {Map<String, dynamic>? properties}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDStayTrack extends TATrackBase {
  void stayTrack(String event, YBDTAProps prop) {
    if (openTrack) track(event, properties: prop.toJson());
  }
  void stayTrackZtGmPoyelive(String event, YBDTAProps prop) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
