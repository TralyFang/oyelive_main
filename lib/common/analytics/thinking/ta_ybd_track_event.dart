
/*
 * @Author: William
 * @Date: 2022-08-01 10:44:33
 * @LastEditTime: 2022-10-09 15:51:38
 * @LastEditors: William
 * @Description: thinking analytics track event
 * @FilePath: /oyetalk-flutter/lib/common/analytics/thinking/ta_track_event.dart
 */

/// 埋点事件名
class YBDTATrackEvent {
  /// 路由埋点事件
  static const String route = 'route';

  /// 签到事件
  static const String sign_complete = 'sign_complete';

  /// 进入活动
  static const String activity_in = 'activity_in';

  /// 活动加载失败
  static const String activity_load_fail = 'activity_load_fail';

  /// 活动加载完成
  static const String activity_load_finish = 'activity_load_finish';

  /// 退出活动
  static const String activity_out = 'activity_out';

  /// 进入语音房
  static const String enter_liveroom = 'enter_liveroom';

  /// 离开语音房
  static const String exit_liveroom = 'exit_liveroom';

  /// 语音房上麦
  static const String liveroom_mic_take = 'liveroom_mic_take';

  /// 语音房下麦
  static const String liveroom_mic_drop = 'liveroom_mic_drop';

  /// 点击语音房游戏
  static const String liveroom_game_click = 'liveroom_game_click';

  /// 语音房游戏加载完毕
  static const String liveroom_game_load_finish = 'liveroom_game_load_finish';

  /// 语音房游戏加载失败
  static const String liveroom_game_load_fail = 'liveroom_game_load_fail';

  /// 关闭语音房游戏
  static const String liveroom_game_close = 'liveroom_game_close';

  /// 进入游戏房
  static const String gameroom_enter = 'gameroom_enter';

  /// 退出游戏房
  static const String gameroom_exit = 'gameroom_exit';

  /// 游戏开始
  static const String game_start = 'game_start';

  /// 游戏退出
  static const String game_quit = 'game_quit';

  /// 游戏加载完成
  static const String game_load_finish = 'game_load_finish';

  /// 游戏加载失败
  static const String game_load_fail = 'game_load_fail';

  /// 加载时长
  static const String generic_loading_time = 'generic_loading_time';

  /// 页面停留时长
  // static const String generic_stay_time = 'generic_stay_time';

  /// 点击事件
  @Deprecated(
    'Use [YBDEventName.CLICK_EVENT] instead.'
    'This event name was deprecated',
  )
  static const String common_click = 'common_click';

  /// 停留时长
  static const String duration = 'duration';

  /// 设备新增
  static const String first_device_add = 'first_device_add';

  /// 退出登录
  static const String account_logout = 'account_logout';

  /// 账号登录
  static const String account_login = 'account_login';

  /// 注册
  static const String register = 'register';

  /// 第一次授权
  static const String grant = 'grant';

  /// 创建liveroom
  static const String create_liveroom_click = 'create_liveroom_click';

  /// 弹窗曝光
  static const String alert_expose = 'alert_expose';

  /// 网络错误
  static const String net_error = 'net_error';

  /// 将用户踢出房间
  static const String liveroom_kick_by_guardians = 'liveroom_kick_by_guardians';

  /// 分享
  static const String share = 'share';

  /// 消息界面查看消息
  static const String inbox_message_click = 'inbox_message_click';

  /// 消息界面查看消息返回
  static const String inbox_message_close = 'inbox_message_close';

  /// 游戏匹配
  static const String game_match = 'game_match';

  /// 游戏取消匹配
  static const String game_match_cancel = 'game_match_cancel';

  /// 游戏房上麦
  static const String gameroom_mic_take = 'gameroom_mic_take';

  /// 游戏房下麦
  static const String gameroom_mic_drop = 'gameroom_mic_drop';

  /// FB登录失败
  static const String login_fb_fail = 'login_fb_fail';
}

/// 埋点属性名
class YBDTAPropertiesName {
  /// 当前路由名
  static const String currentRoute = 'current_route';

  /// 前一个路由名
  static const String previousRoute = 'previous_route';

  /// profileUserId
  static const String profileUserId = 'profile_user_id';

  /// 路由操作类型
  static const String routeType = 'route_type';
}

/// 埋点位置名
class YBDTAEventLocation {
  /// 首页banner
  static const String popular_banner = 'popular_banner';

  /// 首活动弹窗
  static const String popular_alert_view = 'popular_alert_view';

  /// 房间slider
  static const String slider = 'slider';

  /// 私信活动消息
  static const String event_notice = 'event_notice';

  /// 创建语音房页
  static const String new_live = 'new_live';

  /// 新人活动浮窗
  static const String new_bie = 'newbie';
}

class YBDTATimeTrackType {
  /// 活动加载
  static const String event_load = 'event_load';

  /// 语音房游戏加载时长
  static const String liveroom_game_load = 'liveroom_game_load';

  /// 游戏加载时长
  static const String game_load = 'game_load';

  /// 活动停留时长
  static const String event_stay = 'event_stay';

  /// 语音房停留时长
  static const String liveroom_stay = 'liveroom_stay';

  /// 游戏房停留时长
  static const String gameroom_stay = 'gameroom_stay';

  /// 上麦时长
  static const String liveroom_mic_stay = 'liveroom_mic_stay';

  /// 注册到推荐页加载时长
  static const String register_loading = 'register_loading';

  /// 匹配成功时长
  static const String match_success_time = 'match_success_time';

  /// 匹配取消等待时长
  static const String match_cancel_time = 'match_cancel_time';

  /// 语音房上麦时长
  static const String live_take_mic_time = 'live_take_mic_time';

  /// 游戏房上麦时长
  static const String game_take_mic_time = 'game_take_mic_time';

  /// 语音房停留时长
  static const String liveroom_stay_time = 'liveroom_stay_time';

  /// 游戏房停留时长
  static const String gameroom_stay_time = 'gameroom_stay_time';

  /// 活动停留时长
  static const String activity_stay_time = 'activity_stay_time';
}

class YBDTAClickName {
  /// 新手引导关注主播页面点击关注全部
  static const String guide_follow_all = 'guide_follow_all';

  /// 新手引导关注主播页面点击关注
  static const String guide_follow = 'guide_follow';

  /// 新手引导关注主播页面点击下一步
  static const String guide_skip = 'guide_skip';

  /// 新手引导欢迎弹窗点击关闭
  static const String guide_wc_close = 'guide_wc_close';

  /// 新手引导欢迎弹窗点击Go
  static const String guide_wc_go = 'guide_wc_go';

  /// 新手引导语音房送礼引导
  static const String guide_send_gift = 'guide_send_gift';

  /// 新手引导语音房上麦引导
  static const String guide_take_mic = 'guide_take_mic';

  /// 点击game图标
  static const String liveroom_game = 'liveroom_game';

  /// 点击finger cross（语音房）
  static const String liveroom_finger_cross = 'liveroom_finger_cross';

  /// 点击flip circle（语音房）
  static const String liveroom_flip_circle = 'liveroom_flip_circle';

  /// 点击flip coin（语音房）
  static const String liveroom_flip_coin = 'liveroom_flip_coin';

  /// 点击创建房间
  static const String create_gameroom = 'create_gameroom';

  /// 点击快速加入
  static const String quickly_join = 'quickly_join';

  /// 点击ludo时上报（游戏tab）
  static const String gametab_ludo = 'gametab_ludo';

  /// 点击bumper blaster时上报（游戏tab）
  static const String gametab_bb = 'gametab_bb';

  /// 点击knife challenge时上报（游戏tab）
  static const String gametab_knife = 'gametab_knife';

  /// 打开充值页面（打开popup界面则视为点击充值，充值订单事件则指的是发起订单）
  static const String topup = 'topup';

  static const Map<String, String> gameNameMap = {
    'Finger Cross': liveroom_finger_cross,
    'Flip Circle': liveroom_flip_circle,
    'Flip Coin': liveroom_flip_coin,
  };
  static const Map<String, String> gameTabMap = {
    'ludo': gametab_ludo,
    'bumper_car': gametab_bb,
    'fly_cutter': gametab_knife,
  };
}
