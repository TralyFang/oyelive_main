



/*
 * @Author: William
 * @Date: 2022-08-11 15:03:56
 * @LastEditTime: 2023-01-10 14:35:19
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/analytics/thinking/ta_common_track.dart
 */
import 'dart:async';

import 'package:permission_handler/permission_handler.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

class YBDCommonTrack extends TATrackBase {
  /// 常规点击事件埋点
  void commonTrack(YBDTAProps prop) {
    if (openTrack) track(YBDEventName.CLICK_EVENT, properties: prop.toJson());
  }
  void commonTrackFisQ7oyelive(YBDTAProps prop) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 登录事件埋点
  void loginTrack({bool? firstLogin, String? loginWay}) {
    if (openTrack)
      track(YBDTATrackEvent.account_login, properties: {
        'first_login': firstLogin ?? false,
        'login_way': loginWay,
        'device_id': TA.presetProperties?.deviceId,
      });
  }
  void loginTrackXZO2Noyelive({bool? firstLogin, String? loginWay}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// facebook登录失败埋点
  void fbLoginFail({String? errorCode, String? message}) {
    if (errorCode != null && errorCode.isNotEmpty && !errorCode.contains('success'))
      YBDTATrack().trackEvent(YBDTATrackEvent.login_fb_fail,
          prop: YBDTAProps(
            type: errorCode,
            reason: message,
            extParams: {'status': YBDTAUtil.pingFb, 'oye_status': YBDTAUtil.pingOyetalk},
          ));
  }
  void fbLoginFailBtF8Hoyelive({String? errorCode, String? message}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 注册事件埋点
  void regTrack({String? regType, String? adSource}) {
    if (openTrack) {
      track(YBDTATrackEvent.register, properties: {
        'user_id': YBDUserUtil.getUserIdSync?.toString(),
        'reg_type': regType,
        'ad_source': adSource, // 下次再传
        'device_id': TA.presetProperties?.deviceId,
      });
      // TA.startRegTrack = true;
      // TA.timeEvent(YBDTATrackEvent.generic_loading_time);
    }
  }
  void regTrackrLzC8oyelive({String? regType, String? adSource}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 新下载APP用户权限埋点
  /// undetermined-尚未请求权限
  /// granted-用户授予对所请求功能的访问权限
  /// denied-用户拒绝访问请求的功能
  /// restricted-操作系统拒绝访问请求的功能。  用户无法更改此应用程序的状态，可能是由于存在家长控制等主动限制。* 仅在 iOS 上支持。*
  /// limited-用户已授权此应用程序进行有限访问。 * 仅在 iOS (iOS14+) 上支持。*
  /// permanentlyDenied-用户拒绝访问所请求的功能并选择不再显示对该权限的请求。  用户仍然可以在设置中更改权限状态。* 仅在 Android 上支持。*
  /// 只要安装后上报一次
  Future<void> permissionTrack(Map<Permission, PermissionStatus> value) async {
    if (openTrack) {
      String location = value[Permission.location].toString().substring(17);
      String storage = value[Permission.storage].toString().substring(17);
      YBDTAProps prop = YBDTAProps(position: location, storage: storage);
      bool grant = (await YBDSPUtil.get(Const.SP_GRANT)) ?? false;
      if (!grant) {
        track(YBDTATrackEvent.grant, properties: prop.toJson());
        YBDSPUtil.save(Const.SP_GRANT, true);
      }
    }
  }
  void permissionTrack19Mueoyelive(Map<Permission, PermissionStatus> value)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 弹框曝光
  /// [name] 弹窗名
  void alertExpose(String name) {
    if (openTrack) {
      YBDTAProps prop = YBDTAProps(name: name);
      track(YBDTATrackEvent.alert_expose, properties: prop.toJson());
    }
  }

  /// 网络错误埋点
  void netErrorTrack(YBDTAProps prop) {
    // 过滤手机号登录(非网络异常的code)
    if (openTrack) {
      List<String> whitelist = ['902068', '00211'];
      if (!whitelist.contains(prop.reason)) {
        track(YBDTATrackEvent.net_error, properties: prop.toJson());
      }
    }
  }

  /// 登录页面的点击事件埋点
  void loginClick(String location, {bool? installed}) {
    if (openTrack) {
      YBDTAProps p = YBDTAProps(location: location, module: YBDTAModule.login);
      if (installed != null) p.extParams = {'installed': installed};
      commonTrack(p);
      // 第三方登录时长埋点
      if (location == 'facebook' || location == 'more_opt_facebook') TA.timeEvent('login_fb_fail');
      if (location == 'google' || location == 'more_opt_google') TA.timeEvent('login_google_fail');
      if (location == 'twitter' || location == 'more_opt_twitter') TA.timeEvent('login_twitter_fail');
      if (location == 'apple' || location == 'phone_apple') TA.timeEvent('login_apple_fail');
    }
  }
  void loginClickj5ikRoyelive(String location, {bool? installed}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 游戏大厅埋点
  void gameLobby(String? location) {
    if (openTrack) {
      commonTrack(YBDTAProps(location: location, module: YBDTAModule.game));
    }
  }

  /// tpgo 埋点
  void tpgo(String location) {
    if (openTrack) {
      commonTrack(YBDTAProps(location: location, module: YBDTAModule.TPGO));
    }
  }
}

/// 点击事件所在的模块
class YBDTAModule {
  /// 登录注册-login
  static const String login = 'login';

  /// 新手引导-guide
  static const String guide = 'guide';

  /// 游戏大厅-game
  static const String game = 'game';

  /// 语音房-liveroom
  static const String liveroom = 'liveroom';

  /// emoji
  static const String emoji = 'emoji';

  /// vip
  static const String vip = 'VIP';

  /// my-my
  static const String my = 'my';

  /// 金刚区-kingkong
  static const String kingkong = 'kingkong';

  /// 消息页-message
  static const String message = 'message';

  /// 设置-settings
  static const String settings = 'settings';

  /// TP GO 房间 - TPGO
  static const String TPGO = 'TPGO';

  /// 活动 - event
  static const String event = 'event';

  /// 首页
  static const String home = 'home';

  /// common
  static const String common = 'common';

  /// index
  static const String index = 'Index';

  /// plus 开播和slog
  static const String plus = 'Plus';
}
