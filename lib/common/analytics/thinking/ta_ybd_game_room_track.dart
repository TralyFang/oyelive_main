

/*
 * @Author: William
 * @Date: 2022-08-11 16:02:14
 * @LastEditTime: 2022-11-24 17:55:45
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/analytics/thinking/ta_game_room_track.dart
 */
import 'package:get/get.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_loading_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/room_ybd_status_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';

class YBDGameRoomTrack extends TATrackBase {
  /// 游戏房间埋点
  /// [event] 埋点事件名
  /// [addMap] 需要额外添加的属性 如点击位置、失败原因等
  /// [roomStatus] 游戏房游戏数据
  /// [count] 在线人数
  void gameRoomTrack({String? event}) {
    if (openTrack) {
      YBDGameRoomGetLogic? logic;
      try {
        logic = Get.find<YBDGameRoomGetLogic>();
      }catch (e) {
        logger.e('YBDGameRoomGetLogic" not found.');
      }
      YBDGameRoomGetState? state = logic?.state;
      YBDTAProps prop;
      if (state != null) {
        // 房间状态信息
        YBDRoomStatusNotifyBody? roomStatus = state.roomStatusNotify.body;
        prop = YBDTAProps(
          name: roomStatus?.subCategory ?? YBDRtcHelper.getInstance().selectedGame?.subCategory,
          mode: roomStatus?.game?.model,
          currency: roomStatus?.game?.currency,
          bet: roomStatus?.game?.amount,
          id: roomStatus?.roomId,
          num: state.roomInfo?.body?.userCount,
        );
        if (event == YBDTATrackEvent.gameroom_enter) {
          prop.previous_route = Get.previousRoute.shortRoute;
          TA.timeEvent(YBDTATimeTrackType.gameroom_stay_time);
        }
        // 进房时游戏加载完成
        if (event == YBDTATrackEvent.game_load_finish) {
          // YBDLoadTrack().loadingTrack(YBDTAProps(
          //   type: YBDTATimeTrackType.game_load,
          //   id: roomStatus?.game?.gameId?.toString(),
          //   name: roomStatus?.subCategory ?? '',
          // ));
        }
        if (event == YBDTATrackEvent.game_start) {
          String type = 'audience';
          List<YBDRoomStatusNotifyBodyGameMembers?> users = state.roomStatusNotify.body?.game?.members ?? [];
          users.forEach((e) {
            if (e?.userId == YBDRtcHelper.getInstance().info?.id?.toString()) type = 'player';
          });
          prop.type = type;
          YBDLoadTrack().loadingTrack(YBDTAProps(
            type: YBDTATimeTrackType.game_load,
            id: roomStatus?.game?.gameId?.toString(),
            name: roomStatus?.subCategory ?? '',
          ));
        }
        if (event == YBDTATrackEvent.game_load_fail) {
          //todo---Can't get the reason for the failure
          prop.reason = '';
        }
        if (event == YBDTATrackEvent.gameroom_exit) {
          TA.gameRoomId = '';
          // 游戏房停留时长埋点
          YBDStayTrack().stayTrack(
              YBDTATimeTrackType.gameroom_stay_time,
              YBDTAProps(
                id: roomStatus?.roomId,
                name: roomStatus?.subCategory,
              ));
        }
        // 上麦计时
        if (event == YBDTATrackEvent.gameroom_mic_take) {
          TA.timeEvent(YBDTATimeTrackType.game_take_mic_time);
        }
        // 游戏上麦时长
        if (event == YBDTATrackEvent.gameroom_mic_drop) {
          YBDStayTrack().stayTrack(
              YBDTATimeTrackType.game_take_mic_time,
              YBDTAProps(
                id: roomStatus?.roomId,
                name: roomStatus?.subCategory,
              ));
        }
        track(event, properties: prop.toJson());
      }
    }
  }
  void gameRoomTrack4Nl5Ooyelive({String? event}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// TPGO 匹配
  void match({bool cancel = false, String name = 'TPGO'}) {
    if (openTrack) {
      YBDTAProps tp = YBDTAProps(name: name);
      if (!cancel) {
        TA.timeEvent(YBDTATimeTrackType.match_success_time);
        TA.timeEvent(YBDTATimeTrackType.match_cancel_time);
      }
      track(cancel ? YBDTATrackEvent.game_match_cancel : YBDTATrackEvent.game_match, properties: tp.toJson());
    }
  }
  void matchw88k0oyelive({bool cancel = false, String name = 'TPGO'}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool canTrack(String? eventName, {Map<String, dynamic>? properties}) {
    if (eventName == YBDTATrackEvent.gameroom_enter && TA.gameRoomId.isNotEmpty) return false;
    return true;
  }
  void canTrackcR81royelive(String? eventName, {Map<String, dynamic>? properties}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
