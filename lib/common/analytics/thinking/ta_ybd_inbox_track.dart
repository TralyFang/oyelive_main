

/*
 * @Author: William
 * @Date: 2022-09-28 19:46:24
 * @LastEditTime: 2022-09-28 20:13:51
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/analytics/thinking/ta_inbox_track.dart
 */
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';

class YBDInboxTrack extends TATrackBase {
  static String curType = '';

  /// 点击消息
  void clickMessage({String? type}) {
    if (openTrack && type != null) {
      curType = type;
      YBDTAProps tp = YBDTAProps(type: type);
      track(YBDTATrackEvent.inbox_message_click, properties: tp.toJson());
      TA.timeEvent(YBDTATrackEvent.inbox_message_close);
    }
  }
  void clickMessagernNXGoyelive({String? type}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 返回Inbox首页
  /// 只有curType有值才会上报
  void backToInbox() {
    if (openTrack && curType.isNotEmpty) {
      YBDTAProps tp = YBDTAProps(type: curType);
      track(YBDTATrackEvent.inbox_message_close, properties: tp.toJson());
      curType = '';
    }
  }
  void backToInboxpoK9poyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
