



/*
 * @Author: William
 * @Date: 2022-08-11 14:14:45
 * @LastEditTime: 2022-10-27 14:40:15
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/analytics/thinking/ta_route_track.dart
 */
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_loading_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/short_ybd_route.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

class YBDRouteTrack extends TATrackBase {
  // 路由埋点  已合并至停留时长埋点
  // void trackRoute({String currentRoute, String previousRoute, String routeType}) {
  //   if (openTrack) {
  //     alog.v('YBDRouteTrack currentRoute:$currentRoute-previousRoute:$previousRoute--profileUserId:${TA.profileUserId}');
  //     // 注册到推荐页加载时长埋点
  //     if (currentRoute == YBDShortRoute.recommended && TA.startRegTrack) {
  //       TA.startRegTrack = false;
  //       YBDLoadTrack().loadingTrack(YBDTAProps(name: YBDTATimeTrackType.register_loading));
  //     }
  //     Map<String, dynamic> baseMap = getMap(cur: currentRoute, pre: previousRoute, type: routeType);
  //     track(YBDTATrackEvent.route, properties: baseMap);
  //   }
  // }

  // 获取属性map
  Map<String, dynamic> getMap({String? cur, String? pre, String? type}) {
    Map<String, dynamic> baseMap = {
      'user_id': YBDUserUtil.getUserIdSync?.toString(),
      YBDTAPropertiesName.currentRoute: cur,
      YBDTAPropertiesName.previousRoute: pre,
      YBDTAPropertiesName.routeType: type,
    };
    if (TA.profileUserId.isNotEmpty) baseMap.addAll({YBDTAPropertiesName.profileUserId: TA.profileUserId});
    return baseMap;
  }

  /// 路由停留时长
  void durationTrack(YBDTAProps prop, {String? currentRoute, String? previousRoute, String? routeType}) {
    if (openTrack) {
      TA.startTimeTrack = false;
      alog.v('YBDRouteTrack currentRoute:$currentRoute-previousRoute:$previousRoute--profileUserId:${TA.profileUserId}');
      // 注册到推荐页加载时长埋点
      registerLoadTimeTrack(currentRoute);
      Map<String, dynamic> baseMap = getMap(cur: currentRoute, pre: previousRoute, type: routeType);
      prop.extParams = baseMap;
      track(YBDTATrackEvent.route, properties: prop.toJson());
    }
  }
  void durationTrack1lkYnoyelive(YBDTAProps prop, {String? currentRoute, String? previousRoute, String? routeType}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 注册到推荐页加载时长埋点
  void registerLoadTimeTrack(String? route) {
    if (route == YBDShortRoute.recommended) {
      YBDLoadTrack().loadingTrack(YBDTAProps(name: YBDTATimeTrackType.register_loading, type: 'register_load'));
    }
  }
  void registerLoadTimeTrackanZ31oyelive(String? route) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
