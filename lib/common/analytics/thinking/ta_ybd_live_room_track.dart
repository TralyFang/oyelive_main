

/*
 * @Author: William
 * @Date: 2022-08-11 15:33:18
 * @LastEditTime: 2022-10-26 17:25:35
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/analytics/thinking/ta_live_room_track.dart
 */
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_loading_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc_state.dart';
import 'package:oyelive_main/ui/page/room/play_center/entity/game_ybd_entity.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';

class YBDTALiveRoomTrack extends TATrackBase {
  static String? roomCategory = '';

  /// 房间埋点
  /// [event] 埋点事件名
  /// [addMap] 需要额外添加的属性 如点击位置、失败原因等
  /// [record] 语音房游戏数据
  /// [reason] 语音房游戏加载失败原因
  /// [target] 被踢者ID
  void taRoom({String? event, String? location, YBDGameRecord? record, String? reason, String? target}) {
    alog.v('ta room event:$event ');
    YBDRoomState? state;
    try {
      if (openTrack) {
        // 房间信息
        if (YBDLiveService.instance.data.roomContext != null) {
          state = BlocProvider.of<YBDRoomBloc>(YBDLiveService.instance.data.roomContext!).state;
        }
        // 房间标题
        String titleNum = state?.roomInfo?.title?.substring(0, 1) ?? YBDLiveService.instance.data.roomCategory!;
        // 房间ID
        String? uploadRoomId = YBDLiveService.instance.data.roomId?.toString();
        // 游戏名
        String? gameName;
        YBDTAProps prop;
        prop = YBDTAProps(id: uploadRoomId, type: titleNum.category);
        prop.num = state?.curOnLineUserCount;
        if (roomCategory!.isNotEmpty) prop.type = roomCategory?.category;
        if (event == YBDTATrackEvent.enter_liveroom || event == YBDTATrackEvent.exit_liveroom) {
          if (event == YBDTATrackEvent.exit_liveroom) {
            // 房间停留时长埋点
            YBDStayTrack().stayTrack(
                YBDTATimeTrackType.liveroom_stay_time,
                YBDTAProps(
                  id: uploadRoomId,
                  name: titleNum.category,
                ));
          }
          if (event == YBDTATrackEvent.enter_liveroom) TA.timeEvent(YBDTATimeTrackType.liveroom_stay_time);
          YBDLiveService.instance.data.gameRecord = null;
          roomCategory = '';
          gameName = '';
          prop.owner_id = uploadRoomId;
        }
        if (location != null) prop.previous_route = location;
        if (reason != null) prop.reason = reason;
        if (target != null) prop.extParams = {'target': target}; // 踢人
        if (record != null) {
          alog.v('22.8.3--ta record:${record.toJson()}');
          YBDLiveService.instance.data.gameRecord = record;
          alog.v('22.8.3--data record:${YBDLiveService.instance.data.gameRecord?.toJson()}');
          prop.name = record.name;
          prop.game = record.id?.toString();
        }
        if (event == YBDTATrackEvent.liveroom_game_click) {
          gameName = record?.name;
          TA.timeEvent(YBDTATrackEvent.generic_loading_time);
        }
        if (event == YBDTATrackEvent.liveroom_game_load_finish && record!.name == gameName) {
          gameName = '';
          // Deprecated
          // 语音房游戏加载时长埋点
          // YBDLoadTrack().loadingTrack(YBDTAProps(
          //   type: YBDTATimeTrackType.liveroom_game_load,
          //   id: record?.id?.toString(),
          //   name: record?.name,
          // ));
        }
        if (event == YBDTATrackEvent.liveroom_mic_take) {
          TA.timeEvent(YBDTATimeTrackType.live_take_mic_time);
        }
        if (event == YBDTATrackEvent.liveroom_mic_drop) {
          YBDStayTrack().stayTrack(
              YBDTATimeTrackType.live_take_mic_time,
              YBDTAProps(
                id: uploadRoomId,
                name: titleNum.category,
              ));
        }
        track(event, properties: prop.toJson());
      }
    } catch (e) {
      alog.v('ta room event error:$e ');
    }
  }
  void taRoomsExN2oyelive({String? event, String? location, YBDGameRecord? record, String? reason, String? target}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
