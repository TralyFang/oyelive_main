

/*
 * @Author: William
 * @Date: 2022-08-11 14:21:47
 * @LastEditTime: 2022-12-16 15:44:51
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/analytics/thinking/ta_activity_track.dart
 */
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';

class YBDActivityInTrack extends TATrackBase {
  // 进入活动埋点
  void activityIn(YBDTAProps prop) {
    if (openTrack) {
      alog.v('22.8.1----activityIn location:${prop.location} url:${prop.url} title:${prop.name}');
      if (prop.url != null) prop.name = prop.url!.key;
      track(YBDTATrackEvent.activity_in, properties: prop.toJson());
      TA.timeEvent(YBDTATrackEvent.generic_loading_time);
      TA.timeEvent(YBDTATimeTrackType.activity_stay_time);
    }
  }
  void activityIn4le7joyelive(YBDTAProps prop) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 活动分享埋点
  void shareAct({String? url, String? target}) {
    alog.v('22.9.28--url:$url target:$target');
    if (openTrack && url != null) {
      YBDTAProps tp = YBDTAProps(name: 'activity', id: url.key, target: target);
      track(YBDTATrackEvent.share, properties: tp.toJson());
    }
  }
  void shareActtgOzRoyelive({String? url, String? target}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
