



/*
 * @Author: William
 * @Date: 2022-08-11 17:09:03
 * @LastEditTime: 2022-12-19 11:21:32
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/analytics/thinking/ta_util.dart
 */
import 'package:thinking_analytics/thinking_analytics.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/query_ybd_server_time_entity.dart';
import 'package:oyelive_main/module/thirdparty/entity/ip_ybd_api_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

class YBDTAUtil {
  // facebook 能否ping通
  static bool pingFb = false;
  // twitter 能否ping通
  static bool pingTwitter = false;
  // google 能否ping通
  static bool pingGoogle = false;
  // Oyetalk 能否ping通
  static bool pingOyetalk = false;

  /// 获取预置属性map
  static Future<Map<String, dynamic>> getPresetProperties() async {
    Map<String, dynamic> map = {};
    TDPresetProperties? presetProperties = await TA.ta?.getPresetProperties();
    if (presetProperties != null) {
      map.addAll({
        'device_id': presetProperties.deviceId,
        'client_version': presetProperties.appVersion,
        'network_type': presetProperties.networkType,
        'platform': presetProperties.os,
        'channel_id': presetProperties.os == 'Android' ? 'Google Play' : 'App Store',
      });
    }
    YBDIpApiEntity? entity = await YBDCommonUtil.getIp();
    if (entity != null) map.addAll({'ip': entity.query});
    map.removeWhere((key, value) => value == null);
    return map;
  }

  /// 获取用户信息map
  static Future<Map<String, dynamic>> getUserMap({YBDUserInfo? userInfo}) async {
    if (userInfo?.id == null) return {};
    Map<String, dynamic> userMap = {
      'nickname': userInfo!.nickname,
      'sex': userInfo.sex,
      'birth': DateTime.fromMillisecondsSinceEpoch(userInfo.birthday!),
      'age': YBDUserUtil.calculateAge(userInfo.birthday),
      'country': await YBDSPUtil.get(Const.SP_IP_COUNTRY_CODE), // ip 国家代码
      'choose_country': userInfo.country,
      'user_level': userInfo.level,
      'talent_level': userInfo.roomlevel,
      'is_official_talent': YBDTPGlobal.isOfficialTalent,
      'phone_number': userInfo.cellphone,
      'email': userInfo.email,
      'current_gold': userInfo.golds,
      'current_gems': userInfo.gems,
      'current_beans': userInfo.money,
      'follows': userInfo.concern,
      'followers': userInfo.fans,
      'friends': userInfo.friends,
      'account': userInfo.account,
    };
    // vip信息
    if (userInfo.tags!.isNotEmpty) {
      for (var e in userInfo.tags!) {
        if (e!.name == 'VIP') userMap.addAll({'vip_type': e.value, 'vip_duration': e.expireAt});
      }
    }
    userMap.removeWhere((key, value) => value == null);
    return userMap;
  }

  /// 获取密钥
  static TASecretKey getKey() {
    // 加密
    var sKey = TASecretKey();
    sKey.publicKey =
        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCzAKEGsq67Yd03/RF77VKJ/cQ3zfSboK1wzlQfH2E1fr504WCJHHL/UVgjfUGUjMLIN15FNEelp7TXLToqtYlqqMbEXCfSc14ulRatKQioYnJ8EzgUhG0HcRlulni6vxGJHR9iq4weDNyJFRaZuwIQSrUzIaiVq/3hYijxxhhFqQIDAQAB";
    sKey.version = 1;
    sKey.symmetricEncryption = "AES";
    sKey.asymmetricEncryption = "RSA";
    return sKey;
  }

  /// 时间校验
  static void calibrateTime() async {
    YBDServerTimeEntity? time = await ApiHelper.getServerTime();
    if (time != null && time.record != null) {
      alog.i('22.9.15--get server time:${time.record}');
      ThinkingAnalyticsAPI.calibrateTime(time.record!);
    } else {
      alog.i('22.9.15--get server time error!');
    }
  }
}
