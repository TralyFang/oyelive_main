



/*
 * @Author: William
 * @Date: 2022-08-11 14:14:05
 * @LastEditTime: 2022-10-13 15:25:38
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/analytics/thinking/ta_track_base.dart
 */
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

/// 所有埋点的入口
/// 可根据配置项决定是否上报数数或firebase埋点
abstract class TATrackBase {
  /// TATrackBase的子类必须判断[openTrack]再走业务逻辑!用来防止无意义的计算消耗资源
  bool openTrack = YBDTPGlobal.openTrack;

  // [eventName] 事件名
  // [properties] 属性
  void track(String? eventName, {Map<String, dynamic>? properties}) {
    // print('22.8.12--event:$eventName--properties:$properties');
    // 是否可以埋点
    if (canTrack(eventName, properties: properties)) {
      // 是否开启数数埋点
      if (YBDAnalyticsUtil.openTaTrack()) TA.track(eventName, properties: properties);
      // 是否开启firebase埋点
      if (YBDAnalyticsUtil.openFbTrack()) {
        // print('22.8.17--event:$eventName--properties:$properties');
        YBDAnalyticsUtil.logNewEvent(eventName!, properties: properties);
      }
    }
  }
  void trackZh2seoyelive(String? eventName, {Map<String, dynamic>? properties}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 是否可以上传事件
  bool canTrack(String? eventName, {Map<String, dynamic>? properties}) => true;
}

class YBDTAProps {
  String? id;
  String? user;
  String? game;
  String? type;
  int? num;
  String? location;
  String? url;
  String? name;
  String? reason;
  String? currency;
  int? bet;
  String? position; // 位置
  String? storage; // 存储权限
  String? current_route; // 当前路由
  String? previous_route; // 上一个路由
  String? owner_id; // 房主ID
  String? game_id; // 游戏ID
  String? mode; // 游戏模式
  String? target;
  String? module; // 点击事件的模块名
  Map<String, dynamic>? extParams; // 扩展参数

  YBDTAProps({
    this.id,
    this.user,
    this.game,
    this.type,
    this.num,
    this.location,
    this.url,
    this.name,
    this.reason,
    this.currency,
    this.bet,
    this.position,
    this.storage,
    this.current_route,
    this.previous_route,
    this.owner_id,
    this.game_id,
    this.mode,
    this.target,
    this.module,
    this.extParams,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['user'] = user;
    data['game'] = game;
    data['type'] = type;
    data['num'] = num;
    data['location'] = location;
    data['url'] = url;
    data['name'] = name;
    data['reason'] = reason;
    data['currency'] = currency;
    data['bet'] = bet;
    data['position'] = position;
    data['storage'] = storage;
    data['current_route'] = current_route;
    data['previous_route'] = previous_route;
    data['owner_id'] = owner_id;
    data['game_id'] = game_id;
    data['mode'] = mode;
    data['target'] = target;
    data['module'] = module;
    if (extParams != null) data.addAll(extParams!);
    data.removeWhere((key, value) => value == null);
    return data;
  }
}
