

/*
 * @Author: William
 * @Date: 2022-07-21 14:21:52
 * @LastEditTime: 2022-07-21 14:22:01
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/common/analytics/ta_route_event.dart
 */
class YBDTARouterEvent {
  /// 当前页面路由名(简称)
  String? route;

  /// 上一个页面的路由名
  String? preRoute;
}
