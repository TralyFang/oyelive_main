


import 'dart:developer';
import 'dart:isolate';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:package_info/package_info.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';

/// 异常原因
enum CrashlyticsReason {
  /// 已捕获的异常，比如try-catch捕获异常后上报到firebase
  caughtManually,

  /// flutter捕获的异常
  caughtFlutter,

  /// zone中捕获的异常
  caughtZone,

  /// isolate捕获的异常
  caughtIsolate,
}

/// 添加自定义信息
/// 最多限制64个key
class YBDCrashlyticsKey {
  /// 当前用户id
  static const USER_ID = 'user_id';

  /// 模块
  static const MODULE_TAG = 'module_tag';
}

class YBDCrashlyticsModule {
  static const LOGIN = 'module_login'; // 登录模块
  static const NETWORK = 'module_network'; // 网络
  static const WEB_VIEW = 'module_webview'; // 网页
  static const DB = 'module_db'; // 数据库
  static const PURCHASE = 'module_purchase'; // 支付
  static const DEFAULT = 'module_default'; // 默认
}

/// 统计错误信息的工具类
class YBDCrashlyticsUtil {
  YBDCrashlyticsUtil._();

  static YBDCrashlyticsUtil? _instance;

  static YBDCrashlyticsUtil? get instance {
    if (null == _instance) {
      _instance = YBDCrashlyticsUtil._();
    }

    return _instance;
  }

  /// app版本号
  static String? _version;

  /// 上报异常
  /// [error] 不同类型的异常信息 String或[FlutterErrorDetails]
  /// [stack] 异常堆栈
  /// [reason] 异常原因，默认[CrashlyticsReason.caughtManually]
  /// [extra] 额外信息
  Future<void> report(
    dynamic error, {
    StackTrace? stack,
    dynamic extra,
    String module = YBDCrashlyticsModule.DEFAULT,
    CrashlyticsReason reason = CrashlyticsReason.caughtManually,
  }) async {
    var userId = await _getUserId();
    FirebaseCrashlytics.instance.setUserIdentifier(userId);
    setCustomKey(YBDCrashlyticsKey.USER_ID, userId);
    setCustomKey(YBDCrashlyticsKey.MODULE_TAG, module);

    if (reason == CrashlyticsReason.caughtFlutter) {
      // flutter框架捕获的异常
      return FirebaseCrashlytics.instance.recordFlutterError(error);
    }

    if (null != extra) {
      recordLog('$extra');
    }

    // 其他异常：zone，isolate，try-catch异常
    return FirebaseCrashlytics.instance.recordError(
      error,
      stack,
      reason: reason.toString().split('.').last,
    );
  }

  /// 记录错误日志, 关联下一条错误
  Future<void> recordLog(String message) async {
    FirebaseCrashlytics.instance.log(message);
  }
  void recordLogpt7L1oyelive(String message)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 添加自定义信息, 关联下一条错误
  Future<void> setCustomKey(String key, dynamic value) async {
    String keyStr = key.toString().split('.').last;
    FirebaseCrashlytics.instance.setCustomKey(keyStr, value);
  }
  void setCustomKeyLZlYVoyelive(String key, dynamic value)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 监听Flutter错误
  void listenFlutterError() {
    FlutterError.onError = (FlutterErrorDetails details) {
      log('fffxxxx${details.toString()}');
      report(details, reason: CrashlyticsReason.caughtFlutter);
    };
  }
  void listenFlutterErrorzvqfGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 监听Isolate错误
  void listenIsolateError() {
    Isolate.current.addErrorListener(RawReceivePort((pair) async {
      final List<dynamic> errorAndStacktrace = pair;
      await report(
        errorAndStacktrace.first,
        stack: errorAndStacktrace.last,
        reason: CrashlyticsReason.caughtIsolate,
      );
    }).sendPort);
  }
  void listenIsolateError1DZoyoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取当前用户的id
  Future<String> _getUserId() async {
    var userInfo = await YBDCommonUtil.getUserInfo();
    return '${userInfo?.id}';
  }

  /// 获取当前用户昵称
  Future<String> _getUserName() async {
    var userInfo = await YBDCommonUtil.getUserInfo();
    return userInfo?.account ?? 'userNameX';
  }

  /// app版本号
  static Future<String?> _getAppVersion() async {
    if (null == _version) {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      _version = packageInfo.buildNumber;
    }

    return _version;
  }
}
