


import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';

class YBDScreenViewEvent extends YBDAnalyticsEvent {
  String? screenName;
  String screenClass;

  YBDScreenViewEvent(String eventName, {this.screenName, this.screenClass = ""}) : super(eventName);

  @override
  Map<String, dynamic> getParams() {
    // TODO: implement getParams
    Map<String, dynamic> params = {};
    if (screenName != null) {
      params.addAll(<String, dynamic>{"firebase_screen": screenName});
    }
    if (screenClass != null) {
      params.addAll(<String, dynamic>{"firebase_screen_class": screenClass ?? "MainActivity"});
    }
    params.addAll(super.getParams());
    return params;
  }
}
