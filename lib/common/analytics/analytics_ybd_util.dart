



import 'dart:io';
import 'dart:math';

import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

import '../constant/const.dart';
import 'performance.dart' as apiPerformance;

class YBDAnalyticsUtil {
  /// 未知支付状态的埋点
  static const String PURCHASE_UNKNOWN = 'purchase_unknown';

  /// 支付错误的埋点
  static const String PURCHASE_ERROR = 'purchase_error';

  /// 支付成功的埋点
  static const String PURCHASE_PURCHASED = 'purchase_purchased';

  /// pending 支付状态的埋点
  static const String PURCHASE_PENDING = 'purchase_pending';

  /// complete 成功订单的埋点
  static const String COMPLETE_NORMAL_ORDER = 'complete_normal_order';

  /// complete 错误订单的埋点
  static const String COMPLETE_ERROR_ORDER = 'complete_error_order';

  /// 分发内容失败的埋点
  static const String DELIVERY_FAILED = 'delivery_failed';

  /// 分发内容成功的埋点
  static const String DELIVERY_SUCCESS = 'delivery_success';

  /// 预下单失败的埋点
  static const String ORDER_FAILED = 'order_failed';

  /// 预下单成功的埋点
  static const String ORDER_SUCCESS = 'order_success';

  static late FirebaseAnalytics _analytics;
  static String _userId = "-1";
  static FirebaseAnalyticsObserver? _observer;

  // static int usrId = -1;

  static AppsFlyerOptions? _options;
  static AppsflyerSdk? appsflyerSdk;

  /// 统计http和socket接口请求
  static final apiPerformance.YBDPerformance _performance = apiPerformance.YBDPerformance();

  static void init(FirebaseAnalytics analytics, FirebaseAnalyticsObserver observer) {
    _analytics = analytics;
    _observer = observer;

    String appId;
    if (Platform.isIOS) {
      appId = Const.AF_APP_ID_APPLE;
    } else {
      appId = Const.AF_APP_ID_ANDROID;
    }

    _options = AppsFlyerOptions(afDevKey: 'JRA7UhXd8ngM47bE3ENA3f', showDebug: true, appId: appId);
    appsflyerSdk = AppsflyerSdk(_options);
    appsflyerSdk!.initSdk(registerConversionDataCallback: true, registerOnAppOpenAttributionCallback: true);
  }

  static Future<void> logEvent(YBDAnalyticsEvent event) async {
    if (_ignoreAnalyticsData()) return;

    await _analytics.logEvent(
      name: event.eventName,
      parameters: await event.getParams(),
    );
  }

  /// 是否开启firebase埋点
  static bool openFbTrack() {
    String type = YBDCommonUtil.storeFromContext()!.state.configs?.trackSwitch ?? Const.TRACK_TYPE_BOTH;
    bool open = type == Const.TRACK_TYPE_FIRE || type == Const.TRACK_TYPE_BOTH;
    return open && !_ignoreAnalyticsData();
  }

  /// 是否开启thinking analytics埋点
  static bool openTaTrack() {
    String type = YBDCommonUtil.storeFromContext()!.state.configs?.trackSwitch ?? Const.TRACK_TYPE_BOTH;
    return type == Const.TRACK_TYPE_TA || type == Const.TRACK_TYPE_BOTH;
  }

  /// 是否开启埋点
  static bool openTrack() {
    return openFbTrack() || openTaTrack();
  }

  static Future<void> logNewEvent(String eventName, {Map<String, dynamic>? properties}) async {
    Map<String, dynamic> params = <String, dynamic>{};
    params.addAll(properties ?? {});
    // 追加用户平台信息
    params.addAll(
      <String, dynamic>{
        'user_id': YBDAnalyticsUtil._userId,
        'platform': Platform.operatingSystem,
      },
    );
    await _analytics.logEvent(name: eventName, parameters: params);
  }

  static Future<void> logAFEvent(String eventName, Map eventValues) async {
    await appsflyerSdk!.logEvent(eventName, eventValues);
  }

  /// 用户进入首页后记录用户ID
  /// [YBDAnalyticsEvent.getParams]方法中设置事件的用户id
  static Future<void> setUserId(String userId) async {
    _userId = userId;

    // TODO: 根据文档设置后BigQuery中的user_id=null, 可能要升级插件
    // https://firebase.google.com/docs/analytics/userid
    await _analytics.setUserId(id: userId);
  }

  /// 发出http请求的事件
  /// [packageId] http：请求透传字段，匹配request和response
  static void httpStartRequest(String path, String? packageId) {
    if (_closeAnalysisApiDuration()) {
      logger.v('configs analysisApiDuration No');
      return;
    }

    _performance.startRequest(
      path,
      apiPerformance.RequestType.HTTP,
      packageId: packageId,
    );
  }

  /// 获得http响应的事件
  /// [statusCode] 请求状态码，默认0表示未知
  /// [packageId] http：请求透传字段，匹配request和response
  static void httpStopRequest(String path, String? packageId, {String statusCode = '0'}) {
    if (_closeAnalysisApiDuration()) {
      logger.v('configs analysisApiDuration No');
      return;
    }

    _performance.stopRequest(
      path,
      apiPerformance.RequestType.HTTP,
      statusCode: statusCode,
      packageId: packageId,
    );
  }

  /// 发出socket请求的事件
  static void socketStartRequest(String msgStr) {
    if (_closeAnalysisApiDuration()) {
      logger.v('configs analysisApiDuration No');
      return;
    }

    _performance.startRequest(
      msgStr,
      apiPerformance.RequestType.SOCKET,
    );
  }

  /// 获得socket响应的事件
  /// [statusCode] 请求状态码，默认0表示未知
  static void socketStopRequest(String? msgStr) {
    if (_closeAnalysisApiDuration()) {
      logger.v('configs analysisApiDuration No');
      return;
    }

    _performance.stopRequest(
      msgStr,
      apiPerformance.RequestType.SOCKET,
    );
  }

  /// 统计接口时长的开关
  static bool _closeAnalysisApiDuration() {
    return YBDCommonUtil.storeFromContext()!.state.configs?.analysisApiDuration == 'No';
  }

  /// 屏蔽分析数据
  static bool _ignoreAnalyticsData() {
    // 屏蔽测试环境的数据
    return Const.TEST_ENV;
  }
}

class YBDAnalyticsEvent {
  String eventName;
  String? location;
  String? searchTerm; // 搜索内容
  String? itemID;
  String? itemName;
  String? adUnitName;
  String? method;
  String? returnExtra;
  bool? loggedIn;
  String? value;
  String? transactionId;
  String? pt;

  /// 这个字段的使用查看[YBDEventName]配置限制
  Map<String, dynamic>? extParams;

  /// 数数埋点属性
  YBDTAProps? ta;

  ///页面时长

  YBDAnalyticsEvent(
    this.eventName, {
    this.location,
    this.searchTerm,
    this.itemID,
    this.itemName,
    this.method,
    this.returnExtra,
    this.loggedIn,
    this.value,
    this.adUnitName,
    this.transactionId,
    this.pt,
    this.extParams,
    this.ta,
  });

  Map<String, dynamic> getParams() {
    Map<String, dynamic> params = {};
    if (location != null) {
      params.addAll(<String, dynamic>{"location": location});
    }
    if (searchTerm != null) {
      params.addAll(<String, dynamic>{"searchTerm": searchTerm});
    }
    if (itemID != null) {
      params.addAll(<String, dynamic>{"item_id": itemID});
    }
    if (itemName != null) {
      params.addAll(<String, dynamic>{"item_name": itemName});
    }
    if (method != null) {
      params.addAll(<String, dynamic>{"method": method});
    }
    if (returnExtra != null) {
      params.addAll(<String, dynamic>{"return": returnExtra});
    }
    if (loggedIn != null) {
      params.addAll(<String, dynamic>{"logged_in": loggedIn});
    }
    if (transactionId != null) {
      params.addAll(<String, dynamic>{"transaction_id": transactionId});
    }
    if (value != null) {
      params.addAll(<String, dynamic>{"value": value});
    }
    if (adUnitName != null) {
      params.addAll(<String, dynamic>{"ad_unit_name": adUnitName});
    }
    if (pt != null) {
      params.addAll(<String, dynamic>{"pt": pt});
    }
    if (extParams != null) {
      extParams!.removeWhere((String key, value) => value == null);
      params.addAll(extParams!);
    }
    if (ta != null) params.addAll(ta!.toJson());
    if (ta == null)
      params.addAll(
        <String, dynamic>{
          "user_id": YBDAnalyticsUtil._userId,
          "platform": Platform.operatingSystem,
        },
      );

    return params;
  }
}

/// 参数长度限制
const int MAX_PARAM_VALUE_LENGTH = 99;
String maxParamValue(String? itemName) {
  String strValue = itemName ?? '';
  String maxStrValue = strValue.substring(
    0,
    min(strValue.length, MAX_PARAM_VALUE_LENGTH),
  );

  return maxStrValue;
}

/// 自定义事件有数量限制
/// 定义前先看是否有已定义的事件可以用
/// 收集和配置限制：https://support.google.com/firebase/answer/9237506?hl=zh-Hans
/// 自动收集的事件：https://support.google.com/firebase/answer/9234069?visit_id=637940797093631043-3924067323&rd=1
/// 建议的事件：https://support.google.com/firebase/answer/9267735?hl=en&ref_topic=6317484#
class YBDEventName {
  static final String OPEN_PAGE = 'open_page';
  static final String SEARCH = 'search';
  static final String SHARE = 'share';
  static final String CLICK_EVENT = 'click_event';
  static final String LOGIN_EVENT = 'login_event';
  static final String REPORT_TIME = 'report_time';
  static final String SCREEN_VIEW = 'screen_view';

  /// 归因转化
  static final String REFERRER_INSTALL = 'referrer_install';
  static final String REFERRER_INSTALL_APPSFLYER = 'referrer_install_appsflyer';

  /// 打开页面
  static final String OPEN_PAGE_SPLASH = 'open_page_splash';
  static final String OPEN_PAGE_LOGIN = 'open_page_login';

  /// 点击活动对话框的 GO 按钮
  static final String ACTIVITY_GO_CLICK = 'activity_go_click';

  /// 显示活动对话框
  static final String ACTIVITY_DIALOG_IMPRESSION = 'activity_dialog_impression';

  /// 新设备进入登录页面
  static const String OPEN_PAGE_MAIN = 'open_page_main';
  static const String OPEN_PAGE_SET_PROFILE = 'open_page_set_profile';
  static const String OPEN_PAGE_RECOMMENDED = 'open_page_recommended';

  static const String OPEN_PAGE_STATUS_FEATURED = 'open_page_status_featured';
  static const String OPEN_PAGE_STATUS_FOLLOWING = 'open_page_status_following';

  static const String OPEN_PAGE_RELATED_RECENTLY = 'open_page_related_recently';
  static const String OPEN_PAGE_RELATED_FOLLOWING = 'open_page_related_following';

  static final String HOME_INVITE_IMPRESSION = 'home_invite_impression';
  static final String HOME_INVITE_CLICK = 'home_invite_click';

  /// 成功发布SLog
  static final String POST_SLOG = 'post_slog';

  /// 完成任务，领取任务奖励
  ///
  /// Params：
  ///   item_id：任务ID
  ///   item_name: 任务名称
  static final String COMPLETE_TASK = "complete_task";

  /// 完成注册
  static final String COMPLETE_REGISTRATION = "complete_registration";

  /// 首次下载安装
  static final String FIRST_INSTALL = "first_install";

  /// 支付
  static final String PURCHASE = 'purchase';
  static final String SLOG_SEND_GIFT = 'slog_send_gift';

  /// APP异常事件埋点
  ///
  /// 例如：
  /// Google支付：记录掉单、pending单
  /// Param:
  /// item_name: google_inapp_purchase
  /// transaction_id: Google Order ID Google订单号
  /// return: 'lost'、'lost_success'、'pending'、'lost_failed_{error code}'
  /// item_id: User ID
  // static final ABNORMAL_EVENT = 'abnormal_event';

  /// ---------------- AppsFlyer YBDEventName   -----------------
  static final String AF_COMPLETE_REGISTRATION = "af_complete_registration";

  ///
  static final String RESPONSE_EVENT = 'response_event';
  static final String OFFLINE_EVENT = 'offline_event';

  /// teenPatti 挽留
  static const String TEEN_PATTI_SAVE_EVENT = "teen_patti_save_event";

  /// teenPatti 在线邀请
  static const String TEEN_PATTI_ONLINE_INVITE_EVENT = "teen_patti_online_invite_event";

  /// teenPatti 在线邀请
  static const String TEEN_PATTI_ONLINE_CLICK_EVENT = "teen_patti_online_click_event";

  /// socket响应
  static const String SOCKET_RESPONSE_EVENT = "socket_response_event";

  /// 发送短信
  static const String SMS_POST = "sms_post";

  /// 短信获取异常上报
  static const String SMS_POST_EXCEPTION = "sms_post_exception";

  /// web资源拦截数据上报
  static const String WEB_INTERCEPT_EVENT = "web_intercept_event";

  /// 上报异常信息
  static final String REPORT_EXCEPTION = 'report_exception';

}

class YBDLocationName {
  /// 首页
  static const String HOME_PAGE = 'home_page';
  static const String MAIN_PAGE = 'main_page';
  static const String INBOX_PAGE = 'inbox_page';
  static const String SEARCH_PAGE = 'search_page';

  static const String GAME_HALL_PAGE = 'game_hall_page';

  static const String GAME_ROOM_PAGE = 'game_room_page';

  /// 个人中心页面
  static const String MY_PROFILE_PAGE = 'my_profile_page';

  /// 安全设置页面
  static const String ACCOUNT_SAFE_PAGE = 'account_safe_page';

  /// 商城主题列表页面
  static const String STORE_THEMES_LIST_PAGE = 'store_themes_list_page';

  /// 房间个人主题列表页面
  static const String ROOM_PERSONAL_THEMES_LIST_PAGE = 'room_personal_themes_list_page';

  /// 房间商城主题列表页面
  static const String ROOM_STORE_THEMES_LIST_PAGE = 'room_store_themes_list_page';

  /// 主题预览页面
  static const String THEMES_PREVIEW_DIALOG = 'themes_preview_dialog';

  /// 底部购买弹框
  static const String BOTTOM_BUY_DIALOG = 'bottom_buy_dialog';

  /// 商城麦位边框列表页面
  static const String STORE_MIC_FRAMES_LIST_PAGE = 'store_mic_frame_list_page';

  /// 房间个人边框列表页面
  static const String ROOM_PERSONAL_MIC_LIST_PAGE = 'room_personal_mic_list_page';

  /// 房间商城边框列表页面
  static const String ROOM_STORE_MIC_LIST_PAGE = 'room_store_mic_list_page';

  /// 账号信息页面
  static const String ACCOUNT_PAGE = 'account_page';

  /// 修改密码页面
  static const String FIX_PWD_PAGE = 'fix_pwd_page';

  /// 登录页面的风险弹框
  static const String RISK_DIALOG_WARN = 'risk_dialog_warn';

  /// 房间里
  static final String ROOM_PAGE = 'room_page';

  /// event notice 消息列表页面
  static const String EVENT_NOTICE_PAGE = 'event_notice_page';

  /// 活动中心页面
  static const String EVENT_CENTER_PAGE = 'event_center_page';

  /// Home - related page
  static const String HOME_RELATED_PAGE = 'home_related_page';
  static const String HOME_POPULAR_PAGE = 'home_popular_page';
  static const String HOME_EXPLORE_PAGE = 'home_explore_page';

  /// 动态详情页
  static const String STATUS_DETAIL_PAGE = "status_detail_page";

  /// 动态列表页面
  static const String SLOG_LIST_PAGE = 'slog_list_page';

  /// 发布动态页面
  static const String POST_SLOG_PAGE = 'post_slog_page';

  /// 购买详情页面
  static const String PURCHASE_DETAIL_PAGE = 'purchase_detail_page';

  /// 任务界面
  static const String TASK_PAGE = "task_page";

  /// 充值界面
  static const String RECHARGE_PAGE = "recharge_page";

  /// 谷歌充值界面
  static const String GOOGLE_PURCHASE_DETAIL_PAGE = "google_purchase_detail_page";

  /// 邀请好友界面
  static const String INVITE_PAGE = "invite_page";

  /// setting 界面
  static const String SETTING_PAGE = 'setting_page';

  /// 动态列表 Feature 页面
  static const String STATUS_FEATURED_PAGE = 'featured_status_page';

  /// 动态列表 FOLLOWING 页面
  static const String STATUS_FOLLOWING_PAGE = 'following_status_page';

  static const String INDEX_PK_PAGE = 'Index_pk_page';

  /// 宰牲节皮肤
  static const String SKIN_PAGE = 'skin_page';

  /// 他人主页 进背包
  static const String USER_PROFILE_BAGGAGE = 'user_profile_baggage';

  /// WEB界面
  static const String WEB_PAGE = 'web_page';

  /// TPGO游戏
  static const String TPGO = 'tpgo';
}

class YBDItemName {
  /// 首页 banner
  static const String HOME_PAGE_BANNER = 'banner';

  /// 系统消息
  static const String INBOX_MSG = 'inbox_msg';

  /// 房间 slder
  static final String ROOM_PAGE_SLIDER = 'slider';

  /// Game event
  static final String GAME_EVENT = 'game_event';

  /// 点赞
  static const String LIKE = 'like';

  /// 点击安全设置弹框的improve按钮
  static const String SAFE_ALERT_IMPROVE = 'safe_alert_improve';

  /// 显示安全设置弹框的事件
  static const String SAFE_ALERT_SHOW = 'safe_alert_show';

  /// 推荐页面批量关注按钮的埋点
  static const String RECOMMENDED_FOLLOW_ALL = 'recommended_follow_all';

  /// 显示老用户召回的本地通知, 通知类型为4
  static const String NOTIFICATION_RECALL_4 = 'notification_recall_4';

  /// 忘记密码
  static const String FORGET_OLD_PWD = 'forget_old_pwd';

  /// 取消点赞
  static const String UNLIKE = 'unlike';

  /// 评论
  static const String COMMENT = 'comment';

  /// 猎游游戏
  static const String HOT_GAMES = 'hot_games';

  /// 房间送礼Sheet
  static final String ROOM_PAGE_SHEET = 'sheet';

  /// 送礼弹框上的周星活动
  static final String SHEET_WEEKLY_STAR = 'sheet_weekly_star';

  /// 送礼
  static final String SEND_GIFT = 'send_gift';

  /// 改变背景图
  static const String CHANGE_BACKGROUND = 'change_background';

  /// 发布动态
  static const String POST_SLOG = 'post_slog';

  /// 分享动态
  static const String SHARE_SLOG = 'share_slog';

  /// 发布 voice slog
  static const String VOICE_SLOG = 'Voice SLog';

  /// 发布 text slog
  static const String TEXT_SLOG = 'Text SLog';

  /// 点击发布按钮发布 text slog
  static const String POST_BTN_TEXT = 'post_btn_text';

  /// 点击发布按钮发布 void slog
  static const String POST_BTN_VOICE = 'post_btn_voice';

  /// 送礼活动弹框活动
  static const String PRESENT_ACTIVITY = 'present_activity';

  /// 开斋节活动弹框活动
  static const String EID_SPECIAL_ACTIVITY = 'eid_special_activity';

  /// 充值送礼活动弹框活动
  static const String TOPUP_GIFT_ACTIVITY = 'topup_gift_activity';

  /// combo 活动弹框活动
  static const String MASTER_COMBOS_ACTIVITY = 'master_combos_activity';

  /// teenPatti 点击事件
  static const String TEEN_PATTI = "teen_patti_activity";

  /// 送礼
  static final String SEND_GIFT_SUCCESS = 'send_gift_success';

  /// 领取每日任务奖励
  static final String CLAIM_DAILY_TASK = 'claim_daily_task';

  /// 领取每日任务奖励
  static final String CLAIM_DAILY_TASK_SUCCESS = 'claim_daily_task_success';
  static final String CLAIM_DAILY_TASK_ERROR = 'claim_daily_task_error';

  /// teenPatti vip 点击事件
  static const String TEEN_PATTI_VIP = "teen_patti_vip";

  /// 系统干预“挑战书”应战
  static const String PK_INVITE_ACCEPT = "pk_invite_accept";

  /// 系统干预“挑战书”拒绝
  static const String PK_INVITE_REJECT = "pk_invite_reject";

  /// 匹配超时
  static const String PK_MATCH_TIMEOUT = "pk_match_timeout";

  /// PK中主播点击退出按钮弹出提示
  static const String PK_TALENT_EXIT_ROOM = "pk_talent_exit_room";

  ///pk中主播屏蔽对手主播语音
  static const String PK_MUTE_TALENT = "pk_mute_talent";

  ///pk中主播屏蔽对手主播语音失败
  static const String PK_MUTE_TALENT_FAIL = "pk_mute_talent_fail";

  ///pk中主播转发流 失败
  static const String PK_CHANNEL_MEDIA_RELAY_FAIL = "pk_channel_media_relay_fail";

  /// room 周期函数 异常
  static const ROOM_LIFE_CIR_EXT = "room_life_cir_ext";

  /// TEENPATTI触发钻石兑换金币弹窗
  static const String TP_G2B_OPEN = "tp_g2b_open";

  /// TEENPATTI钻石兑换金币各个档位的点击次数
  static const String TP_G2B_PERCENT = "tp_g2b_percent";

  /// TEENPATTI钻石兑换金币兑换按钮的点击次数
  static const String TP_G2B_EXCHANGE = "tp_g2b_exchange";

  /// teenPatti Record 点击事件
  static const String TEEN_PATTI_RECORD = "teen_patti_record";

  /// teenPatti HISTORY 点击事件
  static const String TEEN_PATTI_HISTORY = "teen_patti_history";

  /// 新用户 更改国家code
  static const String NEW_USER_SET_COUNTRY_CODE = "new_user_set_country_code";

  /// tp get free
  static const String TEEN_PATTI_GET_FREE = "teen_patti_get_free";

  /// 提前结束pk surrender
  static const String SURRENDER_PK = "surrender_PK";

  /// 打开新手引导语音房页面
  static const String GUIDE_LIVE_ROOM = "guide_live_room";

  /// 新手引导语音房上麦引导
  static const String GUIDE_LIVE_ROOM_TAKE_MIC = "guide_live_room_take_mic";

  /// 新手引导语音房送礼引导
  static const String GUIDE_LIVE_ROOM_SEND_GIFT = "guide_live_room_send_gift";

  /// 创建语音房
  static const String PLUS_LIVE = "plus_live";

  /// 打开首页签到弹窗
  static const String CHECKIN_POPUP = "checkin_popup";

  /// 关闭首页签到弹窗
  static const String CHECKIN_POPUP_CLOSE = "checkin_popup_close";

  /// 首页签到弹窗签到
  static const String CHECKIN_POPUP_CHECKIN = "checkin_popup_checkin";

  /// 首页活动弹窗点击关闭
  static const String EVENT_POPUP_CLOSE = "event_popup_close";

  /// 首页活动弹窗点击切换
  static const String EVENT_POPUP_SWIPE = "event_popup_swipe";

  /// 分享状态
  static const String BEFORE_SHARE = 'before_share';
  static const String SHARING = 'sharing';
  static const String SHARE_RESULT = 'share_result';
}
