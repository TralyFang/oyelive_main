

import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';

/// "cmd": "Entry 1.0"
/// "cmd": "EntryNotify 1.0"
///	"cmd": "Exit 1.0"
///	"cmd": "ExitNotify 1.0"
///	"cmd": "RoomInfoNotify 1.0"
///	"cmd": "MicNotify 1.0"
/// "cmd": "GrabMic 1.0"
///	"cmd": "DropMic 1.0"
///	"cmd": "LockMic 1.0"
///	"cmd": "UnlockMic 1.0"
///	"cmd": "MuteMic 1.0"
///	"cmd": "UnMuteMic 1.0"
///	"cmd": "Notice 1.0"
/// "cmd": "Chat 1.0"
///	"cmd": "ChatNotify 1.0"
///	"cmd": "Block 1.0"
///	"cmd": "RoomStatus 1.0"
///	"cmd": "Leave 1.0"
///	"cmd": "RoomStatusNotify 1.0"
///	"cmd": "TakeSeat 1.0"
///	"cmd": "LeaveSeat 1.0"
///	"cmd": "ReadyGame 1.0"
///	"cmd": "ReadyGame 1.0"
///	"cmd": "CancelReadyGame 1.0"
///	"cmd": "UpdateGameConfig 1.0"
///	"cmd": "UpdateGameConfig 1.0"
///	"cmd": "KickOutPlayer 1.0"
///	"cmd": "StartGame 1.0"
///	"cmd": "GameStartAni 1.0"
///	"cmd": "GameStartResult 1.0"
///	"cmd": "YBDGameSettlement 1.0"
///	"cmd": "Gifting 1.0"
///	"cmd": "GiftingNotify 1.0"
/// 消息类型：CONNECT，CONNACK，SUBSCRIBE，SUBACK，UNSUBSCRIBE，PUBLISH，PINGREQ，PINGRESP
/// 消息中包含cmd则用cmd当作路径，cmd为空则用消息类型当作路径
/// CONNECT，CONNACK的cmd为CONNECT
/// SUBSCRIBE，SUBACK的cmd为SUBSCRIBE
/// PINGREQ，PINGRESP返回空串
String pathWithMsg(Map<String, dynamic> msg) {
  if (msg == null || !(msg is Map<String, dynamic>)) {
    logger.w('invalid msg: $msg');
    return '';
  }

  String? cmd = msg['cmd'];

  // cmd当作路径
  if (cmd != null && cmd.isNotEmpty) {
    logger.v('msg cmd: $cmd');
    return cmd;
  }

  // 消息类型当作路径
  var type = msg['type'] ?? '';
  logger.v('msg type: $type');
  switch (type) {
    case 'CONNECT':
    case 'CONNACK':
      return 'CONNECT';
    case 'SUBSCRIBE':
    case 'SUBACK':
      return 'SUBSCRIBE';
    default:
      // 忽略其他消息类型
      return '';
  }
}

/// packageId
String? idWithMsg(Map<String, dynamic> msg) {
  var packageId;
  try {
    packageId = msg['packet-id'];
  } catch (e) {
    logger.e('get packageId error: $e');
  }
  return packageId;
}

/// 消息字符串转成map
/// 两个回车换行分割header和body："header \r\n\r\n body"
/// header中的键值对用回车换行分割："type \r\n key:value \r\n"
/// body为json字符串
Map<String, dynamic> fromStr(String str) {
  // 用两个回车换行分割header和body
  List<String> headerAndBody = str.split('\r\n\r\n');

  // 包含header和body的map对象
  Map<String, dynamic> result = {};

  // 消息类型和header
  if (headerAndBody.length > 0) {
    List<String> header = headerAndBody[0].split('\r\n');

    // header
    for (var keyAndValue in header) {
      if (keyAndValue.contains(':')) {
        // header中的键值对
        var key = keyAndValue.split(':')[0];
        var value = keyAndValue.split(':')[1];
        result[key] = value;
      } else {
        // header中的消息类型
        result['type'] = keyAndValue;
      }
    }
  }

  // body
  if (headerAndBody.length > 1) {
    String body = headerAndBody[1];
    logger.v('body: $body');
    try {
      var bodyMap = json.decode(body);

      if (bodyMap is Map<String, dynamic>) {
        result.addAll(bodyMap);
      }
    } catch (e) {
      logger.e('decode body error: $e');
    }
  }

  return result;
}

/// 请求在一天中的时间段
int requestDayTime() {
  var hour = DateFormat('HH').format(DateTime.now());
  logger.v('_pathHour: $hour');
  return YBDNumericUtil.stringToInt(hour);
}

/// 请求开始时间与[initialTime]的时间差
int diffTime(DateTime initialTime) {
  final diff = DateTime.now().difference(initialTime).inSeconds;
  logger.v('request time diff $initialTime time: $diff');
  return diff;
}
