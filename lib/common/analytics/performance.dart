



import 'dart:collection';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:firebase_performance/firebase_performance.dart';
import 'package:oyelive_main/common/analytics/performance_ybd_util.dart' as performanceUtil;
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

/// 接口请求类型
enum RequestType {
  HTTP,
  SOCKET,
}

/// Trace事件
class YBDTraceEvent {
  /// firebase记录性能数据的对象
  Trace? trace;

  /// firebase控制台显示的名称
  String? traceName;

  /// 用来匹配开始请求和结束请求
  String? traceId;

  /// 与[YBDPerformance]初始化时间的时间差
  int? requestTime;

  /// 一天的时间段
  int? dayTime;

  /// 接口的请求次数
  int? count;

  /// 根据请求类型创建实例
  /// [packageId] http：请求透传字段，匹配request和response，socket：忽略该参数
  static YBDTraceEvent getTraceEvent(
    String? request,
    RequestType type, {
    String? packageId,
  }) {
    switch (type) {
      case RequestType.HTTP:
        return YBDHttpTraceEvent.fromRequest(request!, packageId);
      case RequestType.SOCKET:
        return YBDSocketTraceEvent.fromRequest(request!);
      default:
        return YBDTraceEvent();
    }
  }

  /// 设置性能指标
  void setMetrics({
    int? time,
    int? dayTime,
    int? count,
  }) {
    this.requestTime = time;
    this.dayTime = dayTime;
    this.count = count;
  }

  /// [traceId]或[traceName]为空
  bool isEmpty() {
    if ((traceId ?? '').isEmpty || (traceName ?? '').isEmpty) {
      logger.w('$this is empty');
      return false;
    }

    return true;
  }
  void isEmptyuVemmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// [trace]是否已经初始化
  bool isTraced() {
    if (trace == null) {
      logger.w('trace is null');
      return false;
    }

    return true;
  }
  void isTracedgn5VDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  String toString() {
    return 'traceName: $traceName, traceId: $traceId, hour: $dayTime, startTime: $requestTime, count: $count';
  }
  void toStringAsFDLoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDHttpTraceEvent extends YBDTraceEvent {
  /// [packageId] http：请求透传字段，匹配request和response
  YBDHttpTraceEvent.fromRequest(String request, String? packageId) {
    // 去掉path中当前用户的id
    var path = request.replaceAll(RegExp(r'[0-9]{4,20}'), 'X');

    traceName = path;
    traceId = packageId;
  }
}

class YBDSocketTraceEvent extends YBDTraceEvent {
  YBDSocketTraceEvent.fromRequest(String request) {
    Map<String, dynamic> msgMap = performanceUtil.fromStr(request);
    traceName = performanceUtil.pathWithMsg(msgMap);
    traceId = performanceUtil.idWithMsg(msgMap) ?? traceName;
  }
}

class YBDPerformance {
  /// 类初始化的时间
  DateTime initialTime = DateTime.now();

  /// 请求队列
  ListQueue<YBDTraceEvent> traceEventBuffer = ListQueue();

  /// 当前用户 id
  String _userId = '';

  String get userId {
    if (_userId.isEmpty) {
      YBDCommonUtil.getUserInfo().then((value) {
        _userId = '${value?.id ?? ''}';
      });
    }

    return _userId;
  }

  /// 接口的请求次数：{接口名: 请求次数}
  Map<String?, int> requestCountMap = {};

  /// 同一个接口的请求次数
  int? getRequestCount(String? name) {
    if (requestCountMap.containsKey(name)) {
      int count = requestCountMap[name] ?? 0;
      count += 1;
      requestCountMap[name] = count;
    } else {
      requestCountMap[name] = 1;
    }

    final count = requestCountMap[name];
    logger.v('count: $count, requestName: $name');
    return count;
  }

  /// 请求开始的事件
  /// [type] 请求类型
  /// [request] http：http请求路径，socket：socket消息字符串
  /// [packageId] http：请求透传字段，匹配request和response，socket：忽略该参数
  void startRequest(
    String request,
    RequestType type, {
    String? packageId,
  }) {
    logger.v('startRequest: $request');
    YBDTraceEvent startEvent = YBDTraceEvent.getTraceEvent(
      request,
      type,
      packageId: packageId,
    );

    startEvent.setMetrics(
      time: performanceUtil.diffTime(initialTime),
      dayTime: performanceUtil.requestDayTime(),
      count: getRequestCount(startEvent.traceId),
    );

    if (!startEvent.isEmpty()) {
      return;
    }

    // 清理队列
    while (traceEventBuffer.length >= (200)) {
      traceEventBuffer.removeFirst();
    }

    // 新建Trace
    startEvent.trace = FirebasePerformance.instance.newTrace(startEvent.traceName!);
    startEvent.trace!.start();

    // 缓存Trace
    traceEventBuffer.add(startEvent);
    logger.v('startRequest: $startEvent');
  }

  /// 请求的响应的事件
  /// [statusCode] 请求状态码，默认0表示未知
  /// [type] socket或http
  /// [request] http：请求路径，socket：请求消息字符串
  /// [packageId] http：请求透传字段，匹配request和response，socket：忽略该参数
  void stopRequest(
    String? request,
    RequestType type, {
    String? statusCode,
    String? packageId,
  }) {
    logger.v('stopRequest: $request');
    YBDTraceEvent stopEvent = YBDTraceEvent.getTraceEvent(
      request,
      type,
      packageId: packageId,
    );

    if (!stopEvent.isEmpty()) {
      return;
    }

    // 匹配缓存的事件
    final matchedTraceEvent = traceEventBuffer.firstWhereOrNull(
      (element) => element.traceId == stopEvent.traceId && element.traceName == stopEvent.traceName,
    );

    if (null == matchedTraceEvent || !matchedTraceEvent.isTraced()) {
      logger.w('matchedTraceEvent not traced');
      return;
    }

    matchedTraceEvent.trace!.putAttribute('statusCode', statusCode ?? 'unknown');
    matchedTraceEvent.trace!.putAttribute('userId', userId);
    matchedTraceEvent.trace!.stop();

    // 停止记录后清理队列
    traceEventBuffer.remove(matchedTraceEvent);
    logger.v('stopRequest matchedTraceEvent $matchedTraceEvent');
  }
}
