
enum ReceivedMessageType { CONNACK, SUBACK, PINGREQ, PUBLISH, DISCONNECT, NONE, UNSUBSCRIBE }

ReceivedMessageType parseReceivedMessageType(String type) {
  for (ReceivedMessageType rMessage in ReceivedMessageType.values) {
    if (rMessage.toString().split(".").last == type) {
      return rMessage;
    }
  }
  return ReceivedMessageType.NONE;
}
