

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:oyelive_main/common/web_socket/receive/received_ybd_socket_message.dart';

/// 解析活动消息活动消息返回活动ID列表
String? parseEventSocketMsg(YBDReceivedSocketMessage receivedSocketMessage) {
  String? eventKey = '';

  try {
    final msgMap = json.decode(receivedSocketMessage.data);
    debugPrint('msgList: $msgMap');

    if (msgMap is Map) {
      eventKey = msgMap['eventKey'];
    }
  } catch (e) {
    debugPrint('parse event socket msg error: $e');
  }

  return eventKey;
}
