

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/module/entity/tp_ybd_save_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/dialog/dialog_ybd_invite_tp.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/dialog/dialog_ybd_tp_save.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';

import '../received_ybd_socket_message.dart';

class YBDPopUp {
  static bool isShowing = false;
  static onPopup(YBDReceivedSocketMessage receivedSocketMessage) {
    YBDTpSaveRecord? popData = YBDTpSaveRecord().fromJson(json.decode(receivedSocketMessage.data));

    if (!isShowing)
      switch (popData.layout) {
        case "teenpatti_online_recall":
          onTpOnlineSave(popData);
          break;
        default:
          isShowing = false;
          break;
      }
  }

  static onTpOnlineSave(YBDTpSaveRecord? popData) {
    if (YBDTeenInfo.getInstance().open) return;
    isShowing = true;
    showDialog<Null>(
        context: Get.context!, //BuildContext对象
        barrierDismissible: true,
        builder: (BuildContext context) {
          return YBDInviteTpDialog(InviteType.OnlineInvite, popData, 0);
        }).then((value) {
      isShowing = false;
    });
  }
}
