

import 'dart:convert';

import 'package:oyelive_main/common/util/log_ybd_util.dart';

import '../../../../module/inbox/entity/message_ybd_list_entity.dart';
import '../../../../module/inbox/message_ybd_helper.dart';
import '../../socket_ybd_util.dart';
import '../received_ybd_socket_message.dart';

onImTopic(YBDReceivedSocketMessage receivedSocketMessage) {
  if (receivedSocketMessage.data != null) {
    logger.v("$SOCKET_TAG im message from socket: ${receivedSocketMessage.data}");

    YBDMessageHelper.singleMessageAdd(
        null, YBDMessageListDataPullMessage().fromJson(json.decode(receivedSocketMessage.data)));
  }
}
