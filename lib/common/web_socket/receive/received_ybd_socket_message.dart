

import 'received_ybd_message_type.dart';

class YBDReceivedSocketMessage<T> {
  ReceivedMessageType? receivedMessageType;
  late Map<String, String> headers;
  T? data;
}
