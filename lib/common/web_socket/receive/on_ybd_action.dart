


/*
 * @Author: William-Zhou
 * @Date: 2022-03-11 15:04:08
 * @LastEditTime: 2022-03-23 18:28:12
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/receive/on_topic/popup.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/topic/tpg_ybd_topic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_socket_helper/game_ybd_suback_helper.dart';
import 'package:oyelive_main/ui/page/home/controller/event_ybd_dot_controller.dart';

import '../../constant/const.dart';
import '../../util/sp_ybd_util.dart';
import '../send/send_ybd_action.dart';
import '../socket_ybd_config.dart';
import '../socket_ybd_util.dart';
import 'on_topic/im.dart';
import 'received_ybd_msg_parser.dart';
import 'received_ybd_socket_message.dart';

CONNACK(YBDReceivedSocketMessage receivedSocketMessage) async {
  if (receivedSocketMessage.headers['reason-code'] != null && receivedSocketMessage.headers['reason-code'] == "0") {
    //keep-alive: 15
    //session:
    //reason-code:

    await YBDSPUtil.save(Const.SP_SOCKET_SESSION, receivedSocketMessage.headers['session']);
    if (receivedSocketMessage.headers['keep-alive'] != null) {
      Keep_Live_Interval = Duration(seconds: int.parse(receivedSocketMessage.headers['keep-alive']!));
      logger.v("$SOCKET_TAG set Socket ping interval to ${Keep_Live_Interval.inSeconds}s");
    }
    await YBDSendAction.SUBSCRIBE();
    YBDSocketUtil.getInstance()!.setPingRoutine(Keep_Live_Interval);
  } else {
    YBDSocketUtil.getInstance()!.disconnect();
  }
}

DISCONNECT(YBDReceivedSocketMessage receivedSocketMessage) {
  //暂时无动作
  YBDSocketUtil.getInstance()!.disconnect();
  //YBDSocketUtil.getInstance().romovePingRoutine();
}

SUBACK(YBDReceivedSocketMessage receivedSocketMessage) {
  //暂时无动作
  subAck(receivedSocketMessage: receivedSocketMessage);
}

PUBLISH(YBDReceivedSocketMessage receivedSocketMessage) {
  subAck(receivedSocketMessage: receivedSocketMessage);
  if (receivedSocketMessage.headers['topic'] != null) {
    if (receivedSocketMessage.headers['topic']!.startsWith("/im")) {
      onImTopic(receivedSocketMessage);
    } else if (receivedSocketMessage.headers['topic']!.startsWith("/notify")) {
      YBDPopUp.onPopup(receivedSocketMessage);
    } else if (receivedSocketMessage.headers['topic']!.startsWith("/message")) {
      if (receivedSocketMessage.headers['type'] == 'event') {
        try {
          Get.find<YBDEventDotController>();
        } catch (e) {
          logger.e('find EventIconController error: $e');
          // 获取控制器异常时不刷新数据
          return;
        }

        String eventKey = parseEventSocketMsg(receivedSocketMessage)!;

        if (eventKey.isNotEmpty) {
          Get.find<YBDEventDotController>().addPushEvent(['$eventKey']);
        }
      }
    }
    if (receivedSocketMessage.headers['topic']!.startsWith(tgp_topic_name)) {
      YBDTpgTopic.onTpgMessage(receivedSocketMessage);
    }
  }
}
