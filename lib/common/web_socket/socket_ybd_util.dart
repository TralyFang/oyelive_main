






import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:get/get.dart';
import 'package:synchronized/synchronized.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/http/environment_ybd_config.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/send/send_ybd_headers.dart';
import 'package:oyelive_main/common/web_socket/send/send_ybd_socket_message.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base_send_resp.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import '../constant/const.dart';
import '../http/http_ybd_header.dart';
import '../util/log_ybd_util.dart';
import '../util/sp_ybd_util.dart';
import 'message_ybd_handler.dart';
import 'receive/on_ybd_action.dart';
import 'receive/received_ybd_message_type.dart';
import 'receive/received_ybd_socket_message.dart';
import 'send/send_ybd_action.dart';
import 'socket_ybd_config.dart';

final SOCKET_TAG = "==================On Socket Action================\r\n";
enum SocketStatus { TryConnecting, Connected, Close }
Duration Game_Socket_Request_Time_Out = Duration(seconds: 15);

class YBDSocketUtil {
  static YBDSocketUtil? _socketUtil;
  IOWebSocketChannel? _channel;
  Timer? socketKeeper;
  Timer? socketPingRoutine;
  bool shouldReconnect = true;

  int packet_id = 0;

  static final _lock = Lock();
  static final _initLock = Lock();

  static Timer? _queueChecker;
  static final Duration _checkQueueInterval = Duration(seconds: 1);
  static List<YBDGameBaseSendResp> messageQueue = [];

  SocketStatus socketStatus = SocketStatus.TryConnecting;
  // ignore: close_sinks
  StreamController _socketStatusStreamController = StreamController<SocketStatus>.broadcast();
  StreamSink<SocketStatus> get _inAddSocketStatus => _socketStatusStreamController.sink as StreamSink<SocketStatus>;
  Stream<SocketStatus> get socketStatusOutStream => _socketStatusStreamController.stream as Stream<SocketStatus>;

  static YBDSocketUtil? getInstance() {
    if (_socketUtil == null) {
      print("new socketUtil!");
      _socketUtil = YBDSocketUtil();
    }
    if (_queueChecker == null) {
      _queueChecker = Timer.periodic(_checkQueueInterval, (_) {
        _lock.synchronized(() {
          DateTime now = DateTime.now();

          List<YBDGameBaseSendResp> timeOutList = messageQueue
              .where((element) => now.difference(element.sendTime!).inSeconds > Game_Socket_Request_Time_Out.inSeconds)
              .toList();

          timeOutList.forEach((element) {
            // roomId过滤，不同房间的超时不做处理。
            if (element.roomId != YBDRtcHelper.getInstance().roomId) return;
            YBDDialogUtil.hideLoading(Get.context);
            element.onTimeOut?.call();
          });
          messageQueue.removeWhere(
              (element) => now.difference(element.sendTime!).inSeconds > Game_Socket_Request_Time_Out.inSeconds);
        });
      });
    }
    return _socketUtil;
  }

  getSocketHost() async {
    dynamic isTestEnv = await YBDSPUtil.get(Const.SP_TEST_ENV);

    if (isTestEnv == null) {
      return Const.TEST_ENV ? YBDEnvConfig.currentConfig().getWebSocket() : Socket_Host_Prd;
    } else {
      return isTestEnv as bool ? YBDEnvConfig.currentConfig().getWebSocket() : Socket_Host_Prd;
    }
  }

  void onMessageSort() {}
  void onMessageSort2yNfjoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<IOWebSocketChannel?> initSocket() async {
    await _initLock.synchronized(() async {
      if (_channel == null) {
        log("$SOCKET_TAG================websocket init=============== ${await getSocketHost()}");
        changeConnectStatus(SocketStatus.TryConnecting);
        _channel = IOWebSocketChannel.connect(await getSocketHost(),
            pingInterval: Duration(seconds: 10), headers: await getHeader());
        _channel!.stream.listen((message) async {
          log("$SOCKET_TAG receive websocket:-- message:\n$message");
          changeConnectStatus(SocketStatus.Connected);
          YBDReceivedSocketMessage receivedSocketMessage = await Future.value(messageDecode(message));
          switch (receivedSocketMessage.receivedMessageType) {
            case ReceivedMessageType.CONNACK:
              CONNACK(receivedSocketMessage);
              break;
            case ReceivedMessageType.SUBACK:
            case ReceivedMessageType.UNSUBSCRIBE:
              SUBACK(receivedSocketMessage);
              break;
            case ReceivedMessageType.PINGREQ:
              break;
            case ReceivedMessageType.PUBLISH:
              PUBLISH(receivedSocketMessage);
              break;
            case ReceivedMessageType.DISCONNECT:
              DISCONNECT(receivedSocketMessage);
              break;
            case ReceivedMessageType.NONE:
              // no furthur action
              break;
          }

          // 添加游戏房socket埋点
          YBDAnalyticsUtil.socketStopRequest(message);

          // return 1;
        }, onDone: () {
          logger.i("$SOCKET_TAG websocket:-- disconnect");
          _channel = null;
        }, onError: (error) {
          logger.e("$SOCKET_TAG websocket:-- error: $error");
          _channel = null;
        }, cancelOnError: false);

        await YBDSendAction.CONNECT();
      }
    });

    return _channel;
  }

  setPingRoutine(Duration duration) {
    socketPingRoutine?.cancel();
    socketPingRoutine = Timer.periodic(duration, (_) {
      YBDSendAction.PINGREQ();
    });
  }

  romovePingRoutine() {
    socketPingRoutine?.cancel();
  }

  sendSocketMessage(String message) {
    if (_channel != null) {
      log("socket sendxx\n $message");
      logger.v("$SOCKET_TAG Send message to socket:\n$message");
      _channel!.sink.add(message);
    }

    // 添加游戏房socket埋点
    YBDAnalyticsUtil.socketStartRequest(message);
  }

  sendSocketMsg(YBDGameBaseSendResp msg) async {
    msg.packetId = await getPacketId();
    addToQueue(msg);
    sendSocketMessage(await YBDSendSocketMessage(
        sendMessageType: msg.msgType.type,
        body: msg.toJsonContent(),
        sendHeaders: YBDSendHeaders(extra: {
          "topic": "/room/${YBDRtcHelper.getInstance().roomId}",
          'packet-id': msg.packetId.toString(),
          'route': 'mode=upstream,rpc=req',
        })).getMessage());
  }

  setSocketCheckRoutine() {
    if (socketKeeper == null)
      socketKeeper = Timer.periodic(Duration(seconds: 10), (_) {
        if (_channel == null && shouldReconnect) {
          print("self reconnecting");
          initSocket();
        }
      });
  }

  changeConnectStatus(SocketStatus status) {
    if (status != socketStatus) {
      _inAddSocketStatus.add(status);
      socketStatus = status;
      if (status == SocketStatus.Connected && YBDRtcHelper.getInstance().roomId != GameConst.defaultValue) {
        ///房间存在 重新订阅
        YBDGameRoomSocketApi.getInstance().enterRoom(type: EnterGameRoomType.Watch);
      }
    }
  }

  Future addToQueue(YBDGameBaseSendResp message) async {
    await _lock.synchronized(() {
      if (message.sendTime == null) message.sendTime = DateTime.now();
      messageQueue.add(message);
    });
  }
  void addToQueuep1bMIoyelive(YBDGameBaseSendResp message)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  matchRespMessage(YBDReceivedSocketMessage receivedSocketMessage) {
    String? cmd = receivedSocketMessage.headers[GameConst.cmd];
    String? packId = receivedSocketMessage.headers[GameConst.packetId];
    logger.v('matchRespMessage : $cmd, packId:$packId');
    _lock.synchronized(() {
      List<YBDGameBaseSendResp> result =
          messageQueue.where((element) => element.packetId.toString() == packId && element.cmd == cmd).toList();
      if (result != null && result.length != 0) {
        logger.v('matchRespMessage callSuccess: ${result[0].cmd}');
        YBDDialogUtil.hideLoading(Get.context);
        result[0].callSuccess(json.decode(receivedSocketMessage.data));
        messageQueue.remove(result[0]);
      }
      logger.v('message queue: ${messageQueue}');
    });
  }

  Future<int> getPacketId() async {
    await _lock.synchronized(() => {++packet_id});
    return packet_id;
  }
  void getPacketIdb9Lymoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  bool checkConnection() {
    if (_channel == null) {
      // YBDToastUtil.toast("Please wait for connection!");
      initSocket();
      return false;
    }
    return true;
  }

  disconnect() {
    print("web socket disconnect");
    logger.i('web socket disconnect');
    _channel?.sink.close(status.goingAway);
    _channel = null;
  }

  logOutSocket() {
    logger.i('web socket logOutSocket');
    _channel?.sink.close(status.goingAway);
    _channel = null;
    socketKeeper?.cancel();
    _socketUtil = null;
  }
}
