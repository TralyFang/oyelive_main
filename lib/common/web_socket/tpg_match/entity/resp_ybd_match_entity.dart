


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/entity/match_ybd_cmd_base.dart';

class YBDRespMatchEntity extends YBDMatchCmdBase with JsonConvert<YBDRespMatchEntity> {
  String? code;
  String? command;
  String? matchId;
  String? matchStatus;
  String? matchType;
  String? message;
  num? operationTime;
  num? version;
}
