

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDMatchCmdBase {
  String? command;
  String? matchType;
  num? operationTime;
  num? version;
  num? gameId;
  String? extent;
}
