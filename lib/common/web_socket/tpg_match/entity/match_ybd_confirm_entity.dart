

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDMatchConfirmEntity with JsonConvert<YBDMatchConfirmEntity> {
  String? collectionId;
  String? connectionRoomUrl;
  String? command;
  int? confirmCommandTime;
  int? confirmTimeout;
  int? finalConfirmStatus;
  String? gameId;
  String? matchId;
  String? matchStatus;
  int? matchSuccessTime;
  String? matchType;
  int? needUserConfirm;
  int? operationTime;
  int? userConfirmStatus;
  YBDMatchConfirmUserInfo? userInfo;
  int? version;
}

class YBDMatchConfirmUserInfo with JsonConvert<YBDMatchConfirmUserInfo> {
  String? country;
  int? gems;
  int? id;
  int? matchOperationTime;
  String? nickname;
  int? sex;
}
