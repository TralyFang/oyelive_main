



import 'package:oyelive_main/common/web_socket/tpg_match/entity/match_ybd_cmd_base.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

class YBDMatchCmd extends YBDMatchCmdBase with JsonConvert<YBDMatchCmd> {
  String? command;
  String? matchType;
  num? operationTime;
  num? version;
  num? gameId;
  String? extent;

  YBDUserInfo? userInfo;

  @override
  Map<String, dynamic> toJson() {
    // TODO: implement toJson
    if (command == null) command = "matchRequest";
    if (matchType == null) matchType = "tp";
    if (operationTime == null) operationTime = DateTime.now().millisecondsSinceEpoch;
    return super.toJson();
  }
}
