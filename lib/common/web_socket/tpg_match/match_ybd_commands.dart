



import 'dart:async';
import 'dart:convert';

import 'package:oyelive_main/common/web_socket/receive/received_ybd_socket_message.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/entity/cancel_ybd_match_cmd.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/entity/match_ybd_confirm_entity.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/topic/tpg_ybd_topic.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';

import '../send/send_ybd_headers.dart';
import '../send/send_ybd_message_type.dart';
import '../send/send_ybd_socket_message.dart';
import '../socket_ybd_util.dart';
import 'entity/cmd_ybd_match.dart';
import 'entity/match_ybd_cmd_base.dart';
import 'entity/resp_ybd_match_entity.dart';

class YBDMatchCommands {
  static int packetId = 0;

  static String? _curMatchId = "";

  static String? get curMatchId => _curMatchId;

  static Duration TIME_OUT = Duration(seconds: 30);

  static set curMatchId(String? value) {
    _curMatchId = value;
  }

  static num getPacketId() {
    return ++packetId;
  }

  static num getVersion() {
    return packetId;
  }

  static getExtra(curReqId) {
    return {"topic": tgp_topic_name, 'packet-id': curReqId.toString(), 'route': 'mode=upstream,rpc=req'};
  }

  static YBDReceivedSocketMessage onTimeOut() {
    print("tpg timeout!!");
    return YBDReceivedSocketMessage()..data = "{\"code\":\"90099\",\"message\":\"Time out!\"}";
  }

  static final YBDRespMatchEntity timeOut = YBDRespMatchEntity()
    ..code = "90099"
    ..message = "Time out!";

  static final YBDRespMatchEntity connectionBroken = YBDRespMatchEntity()
    ..code = "90100"
    ..message = "Connection broken, Please try again Later!";

  // send() {
  //   if (YBDSocketUtil.getInstance().socketStatus != SocketStatus.Connected) {}
  // }

  static Future<YBDRespMatchEntity?> Match({String? matchType}) async {
    bool isConnecting = YBDSocketUtil.getInstance()!.checkConnection();
    if (!isConnecting) {
      return null;
    }
    YBDMatchCmd matchCmd = YBDMatchCmd()..matchType = matchType;

    /// 心跳添加roomId
    int curReqId = getPacketId() as int;
    matchCmd.userInfo = await YBDUserUtil.userInfo();

    YBDSocketUtil.getInstance()!.sendSocketMessage(await YBDSendSocketMessage(
            sendMessageType: SendMessageType.PUBLISH,
            sendHeaders: YBDSendHeaders(
              extra: getExtra(curReqId),
            ),
            body: (matchCmd..version = curReqId).toJson())
        .getMessage());

    YBDRespMatchEntity? result;

    // if (YBDSocketUtil.getInstance().socketStatus != SocketStatus.Connected) {
    //   return connectionBroken;
    // }

    await YBDTpgTopic.tgpTopicOutStream.firstWhere((element) {
      YBDRespMatchEntity? entity = YBDRespMatchEntity().fromJson(json.decode(element.data));

      bool findResp = entity.version == (curReqId + 1);

      if (findResp) {
        result = entity;
      }

      return findResp;
    }).timeout(TIME_OUT, onTimeout: () {
      result = timeOut;
      // FutureOr<YBDReceivedSocketMessage<dynamic>>
      return YBDReceivedSocketMessage();
    });

    return result;
  }

  static Future<YBDRespMatchEntity?> QuitMatch() async {
    bool isConnecting = YBDSocketUtil.getInstance()!.checkConnection();
    if (!isConnecting) {
      return null;
    }
    YBDCancelMatchCmd cancelMatchCmd = YBDCancelMatchCmd();

    /// 心跳添加roomId
    int curReqId = getPacketId() as int;
    cancelMatchCmd.userInfo = await YBDUserUtil.userInfo();

    await YBDSocketUtil.getInstance()!.sendSocketMessage(await YBDSendSocketMessage(
            sendMessageType: SendMessageType.PUBLISH,
            sendHeaders: YBDSendHeaders(
              extra: getExtra(curReqId),
            ),
            body: (cancelMatchCmd
                  ..version = curReqId
                  ..matchId = _curMatchId)
                .toJson())
        .getMessage());

    YBDRespMatchEntity? result;

    await YBDTpgTopic.tgpTopicOutStream.firstWhere((element) {
      YBDRespMatchEntity entity = YBDRespMatchEntity().fromJson(json.decode(element.data));
      bool findResp = entity.version == (curReqId + 1);

      if (findResp) {
        result = entity;
      }

      return findResp;
    }).timeout(TIME_OUT, onTimeout: () {
      result = timeOut;
      return YBDReceivedSocketMessage();
    });
    return result;
  }

  static void EnterBussiness(YBDMatchConfirmEntity original) async {
    /// 心跳添加roomId
    int curReqId = getPacketId() as int;

    //默认接受
    original = handleMatch(original);

    YBDSocketUtil.getInstance()!.sendSocketMessage(await YBDSendSocketMessage(
            sendMessageType: SendMessageType.PUBLISH,
            sendHeaders: YBDSendHeaders(
              extra: getExtra(curReqId),
            ),
            body: (original..version = curReqId).toJson())
        .getMessage());
  }

  static YBDMatchConfirmEntity handleMatch(YBDMatchConfirmEntity original, {bool acceptGame = true}) {
    //"int 最终确认状态 0 待确认 1 确认 2 拒绝"
    original.finalConfirmStatus = 1;
    original.operationTime = DateTime.now().millisecondsSinceEpoch;
    return original;
  }
}
