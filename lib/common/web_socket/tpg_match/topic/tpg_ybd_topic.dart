

import 'dart:async';
import 'dart:convert';

import 'package:oyelive_main/common/web_socket/tpg_match/match_ybd_commands.dart';

import '../../receive/received_ybd_socket_message.dart';

class YBDTpgTopic {
  static StreamController _tgpTopicStreamController = StreamController<YBDReceivedSocketMessage>.broadcast();
  static StreamSink<YBDReceivedSocketMessage> get _inAdd_tgpTopic => _tgpTopicStreamController.sink as StreamSink<YBDReceivedSocketMessage<dynamic>>;
  static Stream<YBDReceivedSocketMessage> get tgpTopicOutStream => _tgpTopicStreamController.stream as Stream<YBDReceivedSocketMessage<dynamic>>;

  static onTpgMessage(YBDReceivedSocketMessage receivedSocketMessage) {
    _inAdd_tgpTopic.add(receivedSocketMessage);
    try {
      YBDMatchCommands.packetId = json.decode(receivedSocketMessage.data)['version'];
    } catch (e) {}
  }
}
