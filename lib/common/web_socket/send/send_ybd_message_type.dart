
enum SendMessageType {
  CONNECT, //初始化链接
  SUBSCRIBE, //订阅
  UNSUBSCRIBE, //取消订阅
  PINGREQ, //心跳
  PUBLISH, //pub
  NONE,
}

SendMessageType parseSendMessageType(String type) {
  for (SendMessageType sMessage in SendMessageType.values) {
    if (sMessage.toString().split(".").last == type) {
      return sMessage;
    }
  }
  return SendMessageType.NONE;
}
