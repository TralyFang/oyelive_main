



import 'dart:convert';
import 'dart:io';

import 'package:package_info/package_info.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/socket_ybd_config.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket.dart';

import 'send_ybd_headers.dart';
import 'send_ybd_message_type.dart';

class YBDSendSocketMessage {
  SendMessageType? sendMessageType;
  //version: 1.0
  //keep-alive: 10
  //client-id:
  //client-type: app / webview / web
  //session:
  //language:
  //content-type: applicaiont/json
  YBDSendHeaders? sendHeaders;
  Map? body;

  YBDSendSocketMessage({this.sendMessageType, this.sendHeaders, this.body});

  Future<String> getMessage() async {
    String message = "${sendMessageType.toString().split(".").last}\r\n";
    Map<String, String?> headers = {};
    if (sendHeaders == null) {
      headers = await YBDSendHeaders().getSendHeader();
    } else {
      headers = await sendHeaders!.getSendHeader();
    }
    headers.forEach((key, value) {
      message += "$key:$value\r\n";
    });
    message += "\r\n${json.encode(body)}";
    return message;
  }
  void getMessageOD1cgoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  



  ///**************默认的消息组装*************////

  /// socket连接上报信息
  static Future<String> connect() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return YBDSendSocketMessage(sendMessageType: SendMessageType.CONNECT, body: {
      "ot-lang": await YBDSPUtil.get(Const.SP_LOCALE) ?? Const.DEFAULT_LANG,
      "ot-c": Application_Channel,
      "ot-p": Platform.operatingSystem,
      "ot-vc": packageInfo.version,
      "portalType": await YBDSPUtil.get(Const.SP_PORTAL_TYPE) ?? 201
    }).getMessage();
  }

  /// socket订阅上报
  static Future<String> subscribe({int? userId}) async {
    return YBDSendSocketMessage(
        sendMessageType: SendMessageType.SUBSCRIBE,
        sendHeaders: YBDSendHeaders(extra: {"topic": "/im/USER_$userId,/notify,/notify_$userId", "rpc": "true"}))
        .getMessage();
  }

  /// socket心跳上报
  static Future<String> pingreq({String? roomId}) async {
    return YBDSendSocketMessage(
        sendMessageType: SendMessageType.PINGREQ,
        sendHeaders: YBDSendHeaders(extra: {
          "topic": "/room/$roomId",
        })).getMessage();
  }
}
