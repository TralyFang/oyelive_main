


import 'dart:io';

import 'package:package_info/package_info.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import '../../constant/const.dart';
import '../../util/sp_ybd_util.dart';
import 'send_ybd_message_type.dart';
import 'send_ybd_socket_message.dart';
import '../socket_ybd_util.dart';
import '../../../module/user/util/user_ybd_util.dart';

import '../socket_ybd_config.dart';
import 'send_ybd_headers.dart';

class YBDSendAction {
  static CONNECT() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    await YBDSocketUtil.getInstance()!
        .sendSocketMessage(await YBDSendSocketMessage(sendMessageType: SendMessageType.CONNECT, body: {
      "ot-lang": await YBDSPUtil.get(Const.SP_LOCALE) ?? Const.DEFAULT_LANG,
      "ot-c": Application_Channel,
      "ot-p": Platform.operatingSystem,
      "ot-vc": packageInfo.version,
      "portalType": await YBDSPUtil.get(Const.SP_PORTAL_TYPE) ?? 201
    }).getMessage());
  }

  static SUBSCRIBE() async {
    var userId = await YBDUserUtil.userId();
    await YBDSocketUtil.getInstance()!.sendSocketMessage(await YBDSendSocketMessage(
        sendMessageType: SendMessageType.SUBSCRIBE,
        sendHeaders: YBDSendHeaders(
            extra: {"topic": "/im/USER_$userId,/message,/message_$userId,/notify,/notify_$userId,$tgp_topic_name", "rpc": "true"})).getMessage());
  }

  static PINGREQ() async {
    /// 心跳添加roomId
    var roomId = YBDRtcHelper.getInstance().roomId;
    if (roomId == GameConst.defaultValue) roomId = null;
    await YBDSocketUtil.getInstance()!.sendSocketMessage(await YBDSendSocketMessage(
        sendMessageType: SendMessageType.PINGREQ,
        sendHeaders: YBDSendHeaders(extra: {
          "topic": "/room/$roomId",
        })).getMessage());
  }
}
