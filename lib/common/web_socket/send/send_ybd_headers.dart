/*
 * @Author: William-Zhou
 * @Date: 2022-03-11 11:30:27
 * @LastEditTime: 2022-11-29 14:56:41
 * @LastEditors: William
 * @Description: 
 */
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';

import '../../constant/const.dart';
import '../../util/sp_ybd_util.dart';
import '../../../module/user/util/user_ybd_util.dart';

import '../socket_ybd_config.dart';

class YBDSendHeaders {
  String? version = "1.0", client_id, client_type = "app", session, language, content_type = "application/json";
  Duration? keep_alive;

  Map<String, String>? extra;

  YBDSendHeaders(
      {this.version: "1.0",
      this.client_id,
      this.client_type: "app",
      this.session,
      this.language,
      this.content_type: "application/json",
      this.keep_alive,
      this.extra});

  Future<Map<String, String?>> getSendHeader() async {
    if (client_id == null) {
      String? userId = await YBDUserUtil.userId();
      client_id = "$userId"; //USER_
    }
    if (language == null) {
      language = (await YBDSPUtil.get(Const.SP_LOCALE)) ?? Const.DEFAULT_LANG;
    }
    if (session == null) {
      session = await YBDSPUtil.get(Const.SP_SOCKET_SESSION);
    }
    if (keep_alive == null) {
      keep_alive = Keep_Live_Interval;
    }

    var result = {
      "version": version,
      "client-id": client_id,
      "client-type": client_type,
      "session": session,
      "language": language,
      "content-type": content_type,
      "keep-alive": keep_alive!.inSeconds.toString()
    }..removeWhere((key, value) => value == null);
    Map commonHeader = await YBDCommonUtil.generateCommonHeader();
    result.addAll(Map<String, String>.from(commonHeader));
    if (extra != null) {
      result.addAll(extra!);
    }
    logger.v('3.23---result:$result');
    return result;
  }
}
