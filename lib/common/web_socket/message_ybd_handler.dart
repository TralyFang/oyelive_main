

import 'dart:developer';

import 'package:oyelive_main/common/util/log_ybd_util.dart';

import 'receive/received_ybd_message_type.dart';
import 'receive/received_ybd_socket_message.dart';
import '../../generated/json/base/json_convert_content.dart';

YBDReceivedSocketMessage messageDecode(String rawMessage) {
  List<String> firstSplit = rawMessage.split("\r\n\r\n");

  String tops = firstSplit[0];

  List<String> secondSplit = tops.split("\r\n");
  String command = secondSplit[0];
  secondSplit.removeAt(0);

  Map<String, String> headers = {};
  secondSplit.forEach((element) {
    List<String> headInfo = element.split(":");
    headers.addAll({headInfo[0]: headInfo[1]});
  });

  if (firstSplit.length > 1) {
    String body = firstSplit[1];
    return YBDReceivedSocketMessage()
      ..receivedMessageType = parseReceivedMessageType(command)
      ..headers = headers
      ..data = body;
  }

  return YBDReceivedSocketMessage()
    ..receivedMessageType = parseReceivedMessageType(command)
    ..headers = headers;
}
