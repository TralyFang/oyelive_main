
/// 关注用户成功的事件
class YBDFollowBusEvent {
  int userId;
  YBDFollowBusEvent(this.userId);
}

/// 取关用户成功的事件
class YBDUnFollowBusEvent {
  int userId;
  YBDUnFollowBusEvent(this.userId);
}
