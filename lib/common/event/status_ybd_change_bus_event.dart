
import 'package:oyelive_main/module/status/entity/status_ybd_entity.dart';

/// 动态翻页的事件
/// 触发场景：
/// 1. 刷新动态列表数据，只会广播非空的动态数据
/// 2. 换Featured和Following标签
/// 3. 滑动动态列表
// TODO: 测试代码
// 这里有bug，当Feature数据为空，Following有数据时会显示图片背景，应该显示渐变背景
// 出现机率小这里忽略掉了，v2.3.0版本后会改需求
class YBDStatusChangeBusEvent {
  YBDStatusInfo? statusInfo;
  YBDStatusChangeBusEvent(this.statusInfo);
}
