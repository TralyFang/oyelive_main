

import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
// import 'package:firebase_performance/firebase_performance.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/crashlytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/http/error_ybd_helper.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/custom_dialog/custom_ybd_dialog.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

import '../../generated/json/base/json_convert_content.dart';
import '../../module/api.dart';
import '../../module/inbox/inbox_ybd_api_helper.dart';
import '../../module/inbox/queue_ybd_sync.dart';
import '../../module/user/entity/user_ybd_info_entity.dart';
import '../../redux/app_ybd_state.dart';
import '../../redux/followed_ybd_ids_redux.dart';
import '../../redux/user_ybd_redux.dart';
import '../../ui/page/status/widget/upgrade_ybd_notice_dialog.dart';
import '../constant/const.dart';
import '../util/common_ybd_util.dart';
import '../util/log_ybd_util.dart';
import '../util/sp_ybd_util.dart';
import '../util/toast_ybd_util.dart';
import 'custom_ybd_interceptor.dart';

/// 网络请求
/// 建议所有请求写 get, post
/// 传入泛型 T 前，需要在 [JsonConvert] 这个类里写对应 T 的 case
/// needErrorToast 默认弹 dio 错误 toast
class YBDHttpUtil {
  static const String TAG = "YBDHttpUtil";
  static YBDHttpUtil? _instance;

  static YBDHttpUtil getInstance() {
    if (_instance == null) {
      _instance = YBDHttpUtil();
    }
    return _instance!;
  }

  /// Get 请求
  /// [connectTimeout] 连接超时毫秒
  Future<T?> doGet<T>(
    BuildContext? context,
    String? path, {
    Map<String, dynamic>? params,
    String? baseUrl,
    bool isJsonData = false,
    bool needErrorToast = false,
    int? connectTimeout,
    int? sendTimeout,
    int? receiveTimeout,
    bool? showLoading = false,
  }) {
    return _request<T>(
      context,
      path,
      params,
      baseUrl: baseUrl,
      method: "GET",
      needErrorToast: needErrorToast,
      isJsonData: isJsonData,
      connectTimeout: connectTimeout,
      sendTimeout: sendTimeout,
      receiveTimeout: receiveTimeout,
      showLoading: showLoading,
    );
  }

  /// Post 请求
  /// [connectTimeout] 连接超时毫秒
  Future<T?> doPost<T>(
    BuildContext? context,
    String? url,
    Map<String, dynamic>? params, {
    String? baseUrl,
    bool isJsonData = false,
    bool needErrorToast = false,
    int? connectTimeout,
    int? sendTimeout,
    int? receiveTimeout,
    bool? showLoading = false,
  }) {
    return _request<T>(
      context,
      url,
      params,
      baseUrl: baseUrl,
      method: "POST",
      needErrorToast: needErrorToast,
      isJsonData: isJsonData,
      connectTimeout: connectTimeout,
      sendTimeout: sendTimeout,
      receiveTimeout: receiveTimeout,
      showLoading: showLoading,
    );
  }

  /// Patch 请求
  /// [connectTimeout] 连接超时毫秒
  Future<T?> doPatch<T>(
    BuildContext? context,
    String url,
    Map<String, dynamic>? params, {
    String? baseUrl,
    bool isJsonData = false,
    bool needErrorToast = false,
    int? connectTimeout,
    int? sendTimeout,
    int? receiveTimeout,
  }) {
    return _request<T>(
      context,
      url,
      params,
      baseUrl: baseUrl,
      method: "PATCH",
      needErrorToast: needErrorToast,
      isJsonData: isJsonData,
      connectTimeout: connectTimeout,
      sendTimeout: sendTimeout,
      receiveTimeout: receiveTimeout,
    );
  }

  /// Delete 请求
  /// [connectTimeout] 连接超时毫秒
  Future<T?> doDelete<T>(
    BuildContext? context,
    String? url,
    Map<String, dynamic> params, {
    String? baseUrl,
    bool isJsonData = false,
    bool needErrorToast = false,
    int? connectTimeout,
    int? sendTimeout,
    int? receiveTimeout,
  }) {
    return _request<T>(
      context,
      url,
      params,
      baseUrl: baseUrl,
      method: "DELETE",
      needErrorToast: needErrorToast,
      isJsonData: isJsonData,
      connectTimeout: connectTimeout,
      sendTimeout: sendTimeout,
      receiveTimeout: receiveTimeout,
    );
  }

  /// Put 请求
  /// [connectTimeout] 连接超时毫秒
  Future<T?> doPut<T>(
    BuildContext? context,
    String url,
    Map<String, dynamic> params, {
    String? baseUrl,
    bool isJsonData = false,
    bool needErrorToast = false,
    int? connectTimeout,
    int? sendTimeout,
    int? receiveTimeout,
  }) {
    return _request<T>(
      context,
      url,
      params,
      baseUrl: baseUrl,
      method: "PUT",
      needErrorToast: needErrorToast,
      isJsonData: isJsonData,
      connectTimeout: connectTimeout,
      sendTimeout: sendTimeout,
      receiveTimeout: receiveTimeout,
    );
  }

  /// 请求网络
  Future<T?> _request<T>(
    BuildContext? context,
    String? path,
    Map<String, dynamic>? paramsMap, {
    String? baseUrl,
    String? method,
    bool? needErrorToast,
    bool? isJsonData,
    int? connectTimeout,
    int? sendTimeout,
    int? receiveTimeout,
    bool? showLoading,
  }) async {
    if (showLoading ?? false) path!.showLoading(context);
    Map<String, dynamic> params = _paramsWith(paramsMap);
    BaseOptions options = await _optionsWith(
      baseUrl: baseUrl,
      method: method,
      connectTimeout: connectTimeout,
      sendTimeout: sendTimeout,
      receiveTimeout: receiveTimeout,
    );
    Dio dio = _dioWithOptions(options);
    // 打印调试信息
    assert(() {
      YBDUserUtil.userToken().then((value) => print(value));
      print('request info : ${dio.options.method} ${options.baseUrl} $path $params');
      return true;
    }());

    // flutter 暂时不支持自动收集http请求的数据
    // final HttpMetric metric = FirebasePerformance.instance.newHttpMetric(
    //   (options.baseUrl ?? '') + (path ?? ''),
    //   _httpMethodWithString(method),
    // );
    // await metric.start();

    try {
      Response response = await _requestWith(dio, path, params, isJsonData);
      logger.v('response info : ${dio.options.method} ${options.baseUrl} $path $params ${response.data}');
      log("${options}full response data:\n${response.data}");
      _codeHandle(context, response: response, path: path, dio: dio);
      if (showLoading ?? false) YBDCustomDialog.getInstance()!.closeLoading();
      return await _responseHandle(context, response, needErrorToast: needErrorToast);
    } on DioError catch (e) {
      logger.e('response dio error : ${dio.options.method} ${options.baseUrl} $path $params $e');
      if (showLoading ?? false) YBDCustomDialog.getInstance()!.closeLoading();
      _codeHandle(context, response: e.response, path: path, dio: dio);
      _postBugly(response: e.response, dio: dio, path: path!);
      return await _dioErrorHandle<T>(context, e, needErrorToast: needErrorToast, path: path);
    } catch (e) {
      logger.e('response error : $e');
      if (showLoading ?? false) YBDCustomDialog.getInstance()!.closeLoading();
      // flutter 暂时不支持自动收集http请求的数据
      // await metric.stop();
      return null;
    }
  }

  /// 根据返回结果返回数据实体类
  Future<T?> _responseHandle<T>(
    BuildContext? context,
    Response response, {
    String? path,
    bool? needErrorToast,
  }) async {
    if (null != response.data && response.data is Map) {
      logger.v('handle response map data $T');
      // String type = T.toString();
      // if (type == (YBDGameResponseBase).toString()) {
      //   return await Future.value(YBDGameResponseBase.fromJson(response.data) as T);
      // }
      return await Future.value(JsonConvert.fromJsonAsT<T>(response.data));
    } else if (null != response.data && response.data is String && response.data.isNotEmpty) {
      logger.v('handle response string data');
      return await Future.value(JsonConvert.fromJsonAsT<T>(jsonDecode(response.data)));
    } else {
      logger.e("response is null");
      return null;
    }
  }

  /// 错误码处理器
  _codeHandle(
    BuildContext? context, {
    Response? response,
    String? path,
    Dio? dio,
  }) async {
    logger.v('dio code path : $path');
    final isImUrl = _isImPath(path);
    final responseCode = YBDHttpUtil.codeFromResponse(response);

    if (null == responseCode) {
      logger.v('response code is null');
      if (response != null && response.statusCode != 200) {
        // 网络请求错误
        _postBugly(response: response, dio: dio, path: path!, responseCode: responseCode);
      }

      return;
    }

    logger.v('dio error type code : $responseCode');
    try {
      final store = YBDCommonUtil.storeFromContext(context: context);
      if (store?.state.configs?.errorModal == "1" && !ERROR_CODE.contains(responseCode)) {
        YBDErrorHelper.showCode(context, responseCode);
      }
    } catch (e) {
      logger.e('YBDErrorHelper showCode error: $e');
    }

    if (responseCode == Const.HTTP_SUCCESS_NEW || responseCode == Const.HTTP_SUCCESS) {
      // 请求成功
      logger.i('http path: $path');
    } else if (responseCode == '00211') {
      logger.v('toast upgrade notice');
      _postBugly(response: response, dio: dio, path: path!, responseCode: responseCode);
      // YBDToastUtil.toast(translate('upgrade_notice'));
      showDialog(
          barrierDismissible: false,
          context: context!,
          builder: (context) {
            return YBDUpgradeNoticeDialog();
          });
    } else if (responseCode == '0401') {
      _postBugly(response: response, dio: dio, path: path!, responseCode: responseCode);
      try {
        // 清空数据
        await YBDSPUtil.remove(Const.SP_USER_INFO);
        await YBDSPUtil.remove(Const.SP_COOKIE_APP);
        await YBDSPUtil.remove(Const.SP_JSESSION_ID);
        Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
        store.dispatch(YBDUpdateUserAction(YBDUserInfo()));
        store.dispatch(YBDUpdateFollowedIdsAction(Set()));
      } catch (e) {
        logger.e('remove store error : $e');
      }

      try {
        // if (context != null) YBDDialogUtil.showReloginDialog(context);
        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.re_login, clearStack: true, forceFluro: true);
      } catch (e) {
        logger.e('show relogin dialog error : $e');
        YBDCrashlyticsUtil.instance!.report('show relogin dialog error : $e', module: YBDCrashlyticsModule.NETWORK);
      }
    } else if (responseCode == "15004" && isImUrl) {
      logger.e("im not login, retry");
      _postBugly(response: response, dio: dio, path: path!, responseCode: responseCode);
      YBDInboxApiHelper.imLogin(context).then((value) {
        fetchConversationList(context);
      });
    } else {
      // 上报http响应错误的请求
      _postBugly(
        response: response,
        dio: dio,
        path: path!,
        responseCode: responseCode,
      );
    }
  }

  /// 上报错误到 bugly
  void _postBugly({
    Response? response,
    String? responseCode,
    required String path,
    Dio? dio,
  }) {
    final userId = YBDCommonUtil.storeFromContext()!.state.bean?.id;
    String? dioResponseCode = response?.statusCode?.toString();
    if (YBDCustomInterceptor.isLoginPath(path))
      YBDCommonTrack().netErrorTrack(YBDTAProps(
        reason: responseCode ?? dioResponseCode,
        name: response?.data?.toString(),
        url: path,
      ));
    String? code = (responseCode ?? 'null') == 'null' ? dioResponseCode : responseCode;
    YBDCrashlyticsUtil.instance!.report(
      '$userId, ${dio?.options.baseUrl}, $path, $code',
      extra: 'path: $path, code: $code',
      module: YBDCrashlyticsModule.NETWORK,
    );
    logger.e('$userId, ${response?.data}, ${dio?.options.baseUrl}, $path, $responseCode');
  }

  /// dio 错误
  Future<T?> _dioErrorHandle<T>(
    BuildContext? context,
    DioError e, {
    String? path,
    bool? needErrorToast,
  }) async {
    logger.v('22.10.29--DioError type: ${e.type}');
    switch (e.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.receiveTimeout:
      case DioErrorType.sendTimeout:
        return await _timeoutHandle(needErrorToast);
      case DioErrorType.other:
      case DioErrorType.response:
        return await _incorrectStatusHandle(context, e, needErrorToast: needErrorToast, path: path);
      case DioErrorType.cancel:
        return await _cancelDioHandle(needErrorToast);
      default:
        logger.e('unknown dio error : $e');
        return await _unknownErrorHandle(needErrorToast, Const.HTTP_ERROR);
    }
  }

  /// 网络错误, 404, 503 等错误
  Future<T?> _incorrectStatusHandle<T>(
    BuildContext? context,
    DioError e, {
    String? path,
    bool? needErrorToast,
  }) async {
    dynamic responseCode;

    try {
      responseCode = YBDHttpUtil.codeFromResponse(e.response);
      logger.v('response error code : $responseCode');
    } catch (e) {
      logger.v('get response data code error : $e');
    }

    Map<String, dynamic> responseData = {
      'message': "Network error! Please try again later!",
      'returnMessage': "Network error! Please try again later!",
    };

    switch (responseCode ?? '') {
      case '0401':
      case '15004':
        responseData["code"] = responseCode;
        responseData["returnCode"] = responseCode;
        logger.v('incorrect status error : $responseCode');
        try {
          return await Future.value(JsonConvert.fromJsonAsT<T>(responseData));
        } catch (e) {
          logger.v('JsonConvert error : $e');
          return null;
        }
        break;
      default:
        logger.v('incorrect status unknown error : $responseCode, path: $path');
        return await _unknownErrorHandle(needErrorToast, responseCode ?? e.response?.statusCode?.toString());
    }
  }

  /// 请求超时
  Future<T?> _timeoutHandle<T>(bool? needErrorToast) async {
    Map<String, dynamic> responseData = {
      "code": Const.HTTP_TIMEOUT,
      'returnCode': Const.HTTP_TIMEOUT,
      'message': "Request timeout! Please check your network!",
      'returnMessage': "Request timeout! Please check your network!",
    };
    logger.v('request time out');
    if (needErrorToast ?? false) _toastDioErrorMsg(responseData['returnMessage']);
    try {
      return await Future.value(JsonConvert.fromJsonAsT<T>(responseData));
    } catch (e) {
      logger.v('JsonConvert error : $e');
      return null;
    }
  }

  /// 取消请求
  Future<T?> _cancelDioHandle<T>(bool? needErrorToast) async {
    Map<String, dynamic> responseData = {
      "code": Const.HTTP_CANCEL,
      'returnCode': Const.HTTP_CANCEL,
      'message': "Request cancelled!",
      'returnMessage': "Request cancelled!",
    };
    logger.v('cancelled dio request');
    // if (needErrorToast ?? false) _toastDioErrorMsg(responseData['returnMessage']);
    try {
      return await Future.value(JsonConvert.fromJsonAsT<T>(responseData));
    } catch (e) {
      logger.v('JsonConvert error : $e');
      return null;
    }
  }

  Future<T?> _unknownErrorHandle<T>(bool? needErrorToast, String? responseCode) async {
    Map<String, dynamic> responseData = {
      "code": responseCode,
      'returnCode': Const.HTTP_ERROR,
      'message': translate('unknown_error'),
      'returnMessage': translate('unknown_error'),
    };

    logger.v('unknown error');
    if (needErrorToast ?? false) _toastDioErrorMsg(responseData['returnMessage']);
    try {
      return await Future.value(JsonConvert.fromJsonAsT<T>(responseData));
    } catch (e) {
      logger.v('JsonConvert error : $e');
      return null;
    }
  }

  /// 返回后台接口的错误码
  /// 其他接口返回null，比如解析ip的接口http://ip-api.com/json
  static String? codeFromResponse(Response? response) {
    if (null == response?.data) {
      logger.v('response data is null');
      return null;
    }

    Map<String, dynamic>? result;

    if (response!.data is Map) {
      result = response.data;
    } else if (response.data is String && response.data.isNotEmpty) {
      try {
        result = jsonDecode(response.data);
      } catch (e) {
        logger.v('parse string to json error : $e');
      }
    }

    if (null == result) {
      logger.v('response data is null');
      return null;
    } else if (null != result['returnCode']) {
      return result['returnCode'];
    } else if (null != result['code']) {
      return result['code'];
    } else {
      logger.v('not found error code');
      return null;
    }
  }

  /// 弹错误提示
  /// 屏蔽包含服务器地址的错误信息
  void _toastDioErrorMsg(String msg) {
    if (!msg.startsWith('http')) {
      logger.v('dio error msg : $msg');
      YBDToastUtil.toast(msg);
    } else {
      logger.v('not show http:// and https:// toast string');
    }
  }
  void _toastDioErrorMsggnxTboyelive(String msg) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 创建 dio
  Dio _dioWithOptions(BaseOptions options) {
    Dio dio = Dio(options);
    dio.interceptors
      // 开启请求日志
      ..add(LogInterceptor(
        requestBody: kDebugMode,
        responseBody: kDebugMode,
        requestHeader: kDebugMode,
        responseHeader: kDebugMode,
      ))
      ..add(YBDCustomInterceptor(dio));
    if (Const.TEST_ENV && Const.PROXY_HOST.isNotEmpty && Const.PROXY_PORT.isNotEmpty) {
      print('9.6---go to proxy--PROXY_HOST:${Const.PROXY_HOST}----PROXY_PORT:${Const.PROXY_PORT}');
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (client) {
        client.findProxy = (uri) {
          return "PROXY ${Const.PROXY_HOST}:${Const.PROXY_PORT}";
        };
      };
    }
    return dio;
  }

  /// 配置默认参数
  Map<String, dynamic> _paramsWith(Map<String, dynamic>? params) {
    Map<String, dynamic> portalType = {"portalType": 201};

    if (params != null) {
      if (params is Map) {
        Map<String, dynamic> result = Map.from(params);
        // TODO  params.addAll(<String, dynamic>{"portalType": await YBDSPUtil.get(Const.SP_PORTAL_TYPE) ?? 201});
        result.addAll(portalType);
        return result;
      } else {
        assert(false, 'params is not map');
        return portalType;
      }
    } else {
      return portalType;
    }
  }

  /// 配置默认可选项
  Future<BaseOptions> _optionsWith({
    String? baseUrl,
    String? method,
    int? connectTimeout,
    int? sendTimeout,
    int? receiveTimeout,
  }) async {
    BaseOptions options = BaseOptions();
    options = BaseOptions();
    options.method = method ?? "GET";
    options.baseUrl = baseUrl ?? (await YBDApi.getBaseUrl ?? '');
    options.connectTimeout = connectTimeout ?? 60 * 1000;
    options.sendTimeout = sendTimeout ?? 60 * 1000;
    options.receiveTimeout = receiveTimeout ?? 30 * 1000;
    return options;
  }

  /// 发起 dio 请求返回 response
  Future<Response> _requestWith(
    Dio dio,
    String? path,
    Map<String, dynamic> params,
    bool? isJsonData,
  ) async {
    if (dio.options.method == "GET" || dio.options.method == "DELETE") {
      return await dio.request(path!, queryParameters: params);
    } else {
      return await dio.request(
        path!,
        data: isJsonData! ? json.encode(params) : FormData.fromMap(params),
      );
    }
  }

  Future<T?> upload<T>(
    BuildContext context,
    path,
    String filePath, {
    ProgressCallback? onSendProgress,
  }) async {
    BaseOptions options = BaseOptions();
    options.connectTimeout = 30 * 1000;
    options.sendTimeout = 60 * 1000;
    options.receiveTimeout = 30 * 1000;
    options.baseUrl = await YBDApi.getBaseUrl ?? '';

    Dio dio = _dioWithOptions(options);
    FormData formData = FormData.fromMap({"file": await MultipartFile.fromFile(filePath)});
    try {
      Response response = await dio.post(path, data: formData, onSendProgress: (int sent, int total) {
        onSendProgress?.call(sent, total);
      });
      _codeHandle(context, response: response, path: path, dio: dio);
      return await _responseHandle(context, response);
    } on DioError catch (e) {
      _codeHandle(context, response: e.response, path: path, dio: dio);
      return await _dioErrorHandle<T>(context, e, path: path);
    }
  }

  /// 下载网络资源
  static download(url, savePath, progress) async {
    try {
      Dio _dio = Dio();
      Response response = await _dio.download(url, savePath, onReceiveProgress: progress);
      logger.v(response.statusCode);
    } catch (e) {
      logger.e('$url download error');
    }
  }

  /// 根据字符串返回请求方法类型
  // flutter 暂时不支持自动收集http请求的数据
  // HttpMethod _httpMethodWithString(String name) {
  //   switch (name) {
  //     case 'POST':
  //       return HttpMethod.Post;
  //     case 'DELETE':
  //       return HttpMethod.Delete;
  //     case 'PUT':
  //       return HttpMethod.Put;
  //     default:
  //       return HttpMethod.Get;
  //   }
  // }

  bool _isImPath(String? path) {
    final pathStr = path ?? '';

    if (pathStr.contains('im')) {
      logger.v('im url : $pathStr');
      return true;
    }

    return false;
  }
  void _isImPathWMPt1oyelive(String? path) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
