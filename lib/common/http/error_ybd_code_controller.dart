

import 'package:get/get.dart';

class YBDErrorCodeController extends GetxController {
  RxList errorCodes = [].obs;

  addCode(String code) {
    if (errorCodes.length < 3 && !errorCodes.contains(code)) {
      errorCodes.add(code);
      // update();
    }
  }
}
