

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/http/error_ybd_code_controller.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';

class YBDErrorDialog extends StatefulWidget {
  @override
  YBDErrorDialogState createState() => new YBDErrorDialogState();
}

class YBDErrorDialogState extends BaseState<YBDErrorDialog> {
  printErrorCodes(var codes) {
    String result = '';
    codes.forEach((element) {
      result = result + "$element, ";
    });
    if (result.length > 0) {
      result = result.substring(0, result.length - 2);
    }
    print("add code codes:$codes \n errorCode reusult $result");
    return result;
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    final YBDErrorCodeController c = Get.find<YBDErrorCodeController>();
    return Center(
      child: Container(
        width: ScreenUtil().setWidth(500),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))), color: Colors.white),
        child: Material(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: ScreenUtil().setWidth(182),
                width: ScreenUtil().setWidth(500),
                decoration: BoxDecoration(image: DecorationImage(image: AssetImage("assets/images/error_bg.webp"))),
                padding:
                    EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(32), vertical: ScreenUtil().setWidth(30)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Sorry",
                      style:
                          TextStyle(fontSize: ScreenUtil().setSp(32), color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: ScreenUtil().setWidth(12),
                    ),
                    Text(
                      "unable to connect\n to server",
                      style: TextStyle(fontSize: ScreenUtil().setSp(20), color: Colors.white),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(26),
              ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.only(left: ScreenUtil().setWidth(32), right: ScreenUtil().setWidth(74)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Please try the following：",
                      style:
                          TextStyle(fontSize: ScreenUtil().setSp(26), color: Colors.black, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: ScreenUtil().setWidth(20),
                    ),
                    Text.rich(TextSpan(
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(24), color: Color(0xffEE5480), fontWeight: FontWeight.w400),
                        children: [
                          TextSpan(text: "* "),
                          TextSpan(
                              text: "Try this action again later",
                              style: TextStyle(color: Colors.black.withOpacity(0.7))),
                        ])),
                    SizedBox(
                      height: ScreenUtil().setWidth(20),
                    ),
                    Text.rich(TextSpan(
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(24), color: Color(0xffEE5480), fontWeight: FontWeight.w400),
                        children: [
                          TextSpan(text: "* "),
                          TextSpan(
                              text: "Take a screenshot and contact our\n   Customer service",
                              style: TextStyle(color: Colors.black.withOpacity(0.7))),
                        ])),
                    SizedBox(
                      height: ScreenUtil().setWidth(20),
                    ),
                    Obx(
                      () => Text(
                        "   Error code ${printErrorCodes(c.errorCodes)}",
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(24), color: Color(0xffEE5480), fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(42),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  height: ScreenUtil().setWidth(64),
                  width: ScreenUtil().setWidth(300),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
                      gradient: LinearGradient(
                          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter)),
                  child: Center(
                    child: Text(
                      "Got it",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(28),
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(30),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void myBuildsKLA7oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStateDuXlroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    Get.delete<YBDErrorCodeController>();
  }
  void dispose5SuhFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDErrorDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesMnpohoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
