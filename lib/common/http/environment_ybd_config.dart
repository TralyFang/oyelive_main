

import 'package:oyelive_main/common/util/log_ybd_util.dart';

class YBDEnvConfig {
  ///当前环境的key
  static const String envKey = "currentEnv";

  //测试
  static String test = "test";
  //预生产
  static String pre = "pre";

  //当前选中的环境 可以设置一个默认的环境
  static EnvType currentSelectType = EnvType.Test;

  /*
  *  显示某个环境的路径
  */
  static String getGreyOrTestPath(EnvType type) {
    return '${YBDEnvConfig.getProfix(type: type)}://${YBDHttpConfig.getConfig(type)?.testUrl}';
  }

  /*
  *  显示当前选中环境的路径  === const.TEST_URL
  */
  static String getCurrentEnvPath() {
    return getGreyOrTestPath(currentSelectType);
  }

  /*
  *  当前选中环境的config 对象
  */
  static YBDHttpConfig currentConfig({bool? isDevSocket}) {
    YBDHttpConfig config = YBDHttpConfig.getConfig(currentSelectType)!;

    /// 目前没有自己转发 Dev写死的ip 已经放弃110？
    // config.testUrl = currentSelectType == EnvType.Dev ? "192.168.10.111" : config.testUrl;
    logger.v("current config: ${config.testUrl}");
    return config;
  }

  static String getProfix({EnvType? type}) {
    return (type ?? YBDEnvConfig.currentSelectType) == EnvType.Pre ? "https" : "http";
  }

  //inbox baseUrl
  static String inboxBasePath() {
    logger.v('index current profix: ${YBDEnvConfig.getProfix()}');
    return '${YBDEnvConfig.getProfix()}://${YBDEnvConfig.currentConfig().statusTestIp}/';
  }
}

enum EnvType {
  //测试
  Test, //voice-cluster-314173693.cn-northwest-1.elb.amazonaws.com.cn:8090    9020     121.37.214.86
  //预生产
  Pre, // 34.221.72.57:8080                                                   9020      34.221.72.57
  //开发
  Dev, //http://ybd.k8s.dev.com/                                                 80      http://ybd.k8s.dev.com/  oyevoice-test
  //压测
  Pet,
  // 生产
  Prod
}

extension EnvTypeExt on EnvType {
  YBDHttpConfig? getValue() {
    YBDHttpConfig? config;
    switch (this) {
      case EnvType.Test:
        config = YBDHttpConfig("voice-cluster-314173693.cn-northwest-1.elb.amazonaws.com.cn", "8090", "8080", "9020",
            'oyetalk-pay-center', '121.37.214.86', 'oyevoice-test');
        break;
      case EnvType.Pet: //目前没配置
        config =
            YBDHttpConfig("ybd.k8s.test.live", "", "", "", 'oyetalk-pay-center', 'ybd.k8s.test.live', 'oyevoice-test');
        break;
      case EnvType.Pre:
        config =
            YBDHttpConfig("pre.oyetalk.tv", "", "80", "9020", 'oyetalk-pay-center', 'pre.oyetalk.tv', 'oyevoice-test');
        // YBDHttpConfig("34.221.72.57", "8080", "80", "9020", 'oyetalk-pay-center', '34.221.72.57', 'oyetalk-status');
        break;
      case EnvType.Dev:
        config =
            YBDHttpConfig("192.168.10.121", "8090", "", "9020", 'oyetalk-pay-center', '192.168.10.120', 'oyevoice-test');
        break; //ybd.k8s.dev.live
      case EnvType.Prod:
        config = YBDHttpConfig(
            "www.oyetalk.live", "8080", "80", "9020", 'oyetalk-pay-center', 'status.oyetalk.live', 'oyetalk-status');
        break;
    }
    return config;
  }

  String getKey() {
    switch (this) {
      case EnvType.Test:
        return "test";
      case EnvType.Pet: //目前没配置
        return "pet";
        break;
      case EnvType.Pre:
        return "pre";
        break;
      case EnvType.Dev:
        return "dev";
        break;
      case EnvType.Prod:
        return "prod";
        break;
      default:
        return '';
        break;
    }
  }
  void getKeynAQFqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  static EnvType getType(String value) {
    switch (value) {
      case "test":
        return EnvType.Test;
      case "pet": //目前没配置
        return EnvType.Pet;
        break;
      case "pre":
        return EnvType.Pre;
        break;
      case "dev":
        return EnvType.Dev;
        break;
      case "prod":
        return EnvType.Prod;
        break;
      default: // 默认值
        return YBDEnvConfig.currentSelectType;
        break;
    }
  }
}

class YBDHttpConfig {
  String testUrl;
  String testPort;
  String testPortAgency;
  String testPortPayment;
  String urlPathPayment;
  String statusTestIp;
  String uploadS3;

  YBDHttpConfig(this.testUrl, this.testPort, this.testPortAgency, this.testPortPayment, this.urlPathPayment,
      this.statusTestIp, this.uploadS3);

  getRoomSocket() {
    if (testPort.isEmpty) {
      return "ws://$testUrl/websocket";
    }
    return "ws://$testUrl:13999/websocket";
  }

  getWebSocket() {
    if (testPort.isEmpty) {
      return "ws://$testUrl/oyetalk-socket";
    }
    if ('$testUrl'.contains('live')) return "ws://wsocket.oyetalk.live:80/oyetalk-socket";
    return "ws://$statusTestIp:${statusTestIp.startsWith('34') ? "23999" : "9300"}/oyetalk-socket";
  }

  static YBDHttpConfig? getConfig(EnvType type) {
    return type.getValue();
  }
}
