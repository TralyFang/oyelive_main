

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'dialog_ybd_error.dart';
import 'error_ybd_code_controller.dart';

class YBDErrorHelper {
  static bool showed = false;
  static late YBDErrorCodeController c;
  static showCode(BuildContext? context, String errorCode) {
    c = Get.put(YBDErrorCodeController());
    if (!showed) {
      showed = true;
      showDialog<Null>(
          context: context!, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return YBDErrorDialog();
          }).then((value) {
        showed = false;
      });
    }
    c.addCode(errorCode);
  }
}

const ERROR_CODE = [
  "00001",
  "00002",
  "00003",
  "00004",
  "00005",
  "00006",
  "00100",
  "00104",
  "00105",
  "00107",
  "00108",
  "00109",
  "00210",
  "00211",
  "10002",
  "10003",
  "10004",
  "10005",
  "10006",
  "10007",
  "10008",
  "10009",
  "10010",
  "10011",
  "10012",
  "10013",
  "10014",
  "000000",
  "000002",
  "000001",
  "900000",
  "900001",
  "900002",
  "900003",
  "900004",
  "900005",
  "900006",
  "900007",
  "900008",
  "900009",
  "900010",
  "900011",
  "900012",
  "900013",
  "900014",
  "900015",
  "900016",
  "900101",
  "900102",
  "900103",
  "900104",
  "900105",
  "900106",
  "900107",
  "900201",
  "900202",
  "900203",
  "900301",
  "900401",
  "900402",
  "900501",
  "901001",
  "902001",
  "902002",
  "902003",
  "902004",
  "902005",
  "902006",
  "902007",
  "902008",
  "902101",
  "902102",
  "902009",
  "902010",
  "902011",
  "902017",
  "902018",
  "902020",
  "902021",
  "902024",
  "902026",
  "902031",
  "902032",
  "902033",
  "902041",
  "902050",
  "902051",
  "902052",
  "902053",
  "902054",
  "902055",
  "902056",
  "902057",
  "902157",
  "902058",
  "902059",
  "902061",
  "902062",
  "902063",
  "902064",
  "902065",
  "902066",
  "902067",
  "902068",
  "902069",
  "902070",
  "902071",
  "903001",
  "903002",
  "903003",
  "903004",
  "903005",
  "903101",
  "903102",
  "903103",
  "903104",
  "903105",
  "903107",
  "903200",
  "904001",
  "904002",
  "904003",
  "904004",
  "904005",
  "904101",
  "904102",
  "904201",
  "904202",
  "904301",
  "904302",
  "904303",
  "904304",
  "904305",
  "904306",
  "904308",
  "905001",
  "905002",
  "905003",
  "905004",
  "905005",
  "905006",
  "905007",
  "905008",
  "905009",
  "905010",
  "905011",
  "905012",
  "905013",
  "905014",
  "905015",
  "905016",
  "905017",
  "905018",
  "905019",
  "905020",
  "905021",
  "905022",
  "905023",
  "905024",
  "905025",
  "905026",
  "905027",
  "905028",
  "905029",
  "905030",
  "905031",
  "905032",
  "905033",
  "902003",
  "902004",
  "906001",
  "906002",
  "906003",
  "909786",
  "909787",
  "903106",
  "905110",
  "905111",
  "907001",
  "907002",
  "907003",
  "907004",
  "908001",
  "908002",
  "908003",
  "908004",
  "908005",
  "909001",
  "909002",
  "909003",
  "0",
  "0400",
  "0500",
  "0401",
  "14999",
  "14004",
  "00211",
  "906000",
  "906006",
];
