

import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:uuid/uuid.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/module/api.dart';

import '../constant/const.dart';
import '../util/log_ybd_util.dart';
import '../util/sp_ybd_util.dart';

/// url中包含多个斜杠
/// 不处理带http://的路径
bool hasMultipleSlashes({String base = '', String path = ''}) {
  var hasPathScheme = Uri.parse(path).hasScheme;

  if (hasPathScheme) {
    logger.v('path is full url, base: $base, path: $path');
    return false;
  }

  var fullUrl = '$base$path';
  logger.v('hasMultipleSlashes fullUrl: $fullUrl');

  // 移除url中的http://或https://
  final domainAndPath = removeHttpScheme(fullUrl: fullUrl);
  logger.v('hasMultipleSlashes domainAndPath: $domainAndPath');

  // 路径中包含两个或两个以上的斜杠
  if (domainAndPath.contains(RegExp(r'\/\/+'))) {
    logger.v('hasMultipleSlashes true');
    return true;
  }

  logger.v('hasMultipleSlashes false');
  return false;
}

/// 移除url中的http://或https://
String removeHttpScheme({String fullUrl = ''}) {
  logger.v('removeHttpScheme fullUrl: $fullUrl');
  var result = fullUrl.replaceFirst(RegExp(r'^(http:\/\/)|^(https:\/\/)'), '');
  logger.v('removeHttpScheme result: $result');
  return result;
}

/// 移除路中的多个斜杠
/// 比如"/oyetalk-room//room/page" 中间的两个斜杠改成一个 "/oyetalk-room/room/page";
/// 比如"//oyetalk-room///room/page" 开头的两个斜杠和中间的三个斜杠改成一个 "/oyetalk-room/room/page";
String removeMultipleSlashes({String path = ''}) {
  var newPath = path.replaceAll(RegExp(r'\/\/+'), '/');
  logger.v('removeMultipleSlashes: $newPath');
  return newPath;
}

/// 移除路径开头的斜杠
/// 比如"/oyetalk-room/room/page" 去掉开头的一个斜杠 "oyetalk-room/room/page";
/// 比如"//oyetalk-room/room/page" 去掉开头的两个斜杠 "oyetalk-room/room/page";
String removeStartSlashes({String path = ''}) {
  var newPath = path.replaceFirst(RegExp(r'^\/+'), '');
  logger.v('removeStartSlashes: $newPath');
  return newPath;
}

class YBDCustomInterceptor extends InterceptorsWrapper {
  final Dio _dio;

  YBDCustomInterceptor(this._dio);

  @override
  Future<void> onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    /// check network
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      logger.e("${options.uri} request failed, network error.");
      // return _dio.resolve("");
      handler.next(options);
      return;
    }

    options.headers = await YBDCommonUtil.generateCommonHeader() as Map<String, dynamic>;

    // url中包含多个斜杠
    if (hasMultipleSlashes(base: options.baseUrl, path: options.path)) {
      // 移除路径的多个斜杠
      options.path = removeMultipleSlashes(path: options.path);

      // 移除路径开头的斜杠
      options.path = removeStartSlashes(path: options.path);

      // TODO: 移除[options.baseUrl]中的多个斜杠
    }

    // 请求透传字段
    options.extra = {'packageId': Uuid().v4()};
    logger.v('onRequest url: ${options.baseUrl}${options.path}');
    YBDAnalyticsUtil.httpStartRequest(options.path, options.extra['packageId']);
    // return super.onRequest(options);
    handler.next(options);
  }
  void onRequest6ILQooyelive(RequestOptions options, RequestInterceptorHandler handler)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
//    logger.v('interceptor on response');
    // 检查 null
    if (null != response) {
      String? jsessionId = getCookieByName(response, "JSESSIONID");
      String? authorization = getHeaderByName(response, "Authorization");
      YBDSPUtil.save(Const.SP_HTTP_AUTHORIZATION, authorization);

      // 检查返回数据是否为空
      if (null == response.data) {
        logger.v('response data is empty');
      }

      // 检查返回数据是否为 map 对象
      if (!(response.data is Map)) {
        logger.v('response data is not map');
      }

      // TODO: 测试代码 在 [YBDHttpUtil] 里处理登录过期
      if (null != response.data && response.data is Map && response.data['returnCode'] == Const.HTTP_SESSION_TIMEOUT) {
        logger.w('session timeout clean user info');
        // YBDSPUtil.remove(Const.SP_USER_INFO);
        //处理身份过期,跳转到login
// TODO: 跳转报错 The method 'pushNamedAndRemoveUntil' was called on null
//        YBDNavigationService.navigateTo('/login');
      }

      /// 用户鉴权token，30分钟失效。 每次调用接口都刷新
      if (null != jsessionId && jsessionId.isNotEmpty) {
        YBDSPUtil.save(Const.SP_JSESSION_ID, jsessionId);

        /// app cookie socket鉴权token, 登录请求返回，刷新SP
        if (_isLogin(response)) {
          try {
            int? userId = response.data?['record']?['userInfo']?['id'];
            YBDSPUtil.save(Const.SP_COOKIE_APP, userId.toString() + "_" + jsessionId);
          } catch (e) {
            logger.v(e);
          }
        }
      }
    } else {
      logger.v('response is null');
    }

    YBDAnalyticsUtil.httpStopRequest(
      response.requestOptions.path,
      response.requestOptions.extra['packageId'],
      // 首页获取后台接口返回码，如果为null再获取http请求的状态码
      statusCode: YBDHttpUtil.codeFromResponse(response) ?? '${response.statusCode}',
    );
    // return super.onResponse(response);
    handler.next(response);
  }
  void onResponseHot4Ooyelive(Response response, ResponseInterceptorHandler handler) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  static String? getHeaderByName(Response response, String keyName) {
    try {
      var value = response.headers.value(keyName);
      logger.v("getHeaderByName: $value");
      return value;
    } catch (e) {
      logger.e("getHeaderByName error: $e");
      return "";
    }
  }

  static String? getCookieByName(Response response, String cookieName) {
    List<String>? setCookies;
//      if (null != response && response.headers is Map) {
    if (null != response && response.headers != null) {
      // TODO: 要测试，可能 headers 为 list
      setCookies = response.headers['set-cookie'];
    } else {
      logger.v('response is null');
    }

    String? cookieValue;
    if (setCookies != null) {
      for (var setCookie in setCookies) {
        var cookies = setCookie.split(';');

        for (var rawCookie in cookies) {
          if (rawCookie.length > 0) {
            var keyValue = rawCookie.split('=');
            if (keyValue.length == 2) {
              var key = keyValue[0].trim();
              var value = keyValue[1];

              if (key == cookieName) {
                cookieValue = value;
              }
            }
          }
        }
      }
    }

    logger.v('cookie value : $cookieValue');
    return cookieValue;
  }

  /// 检测是否为登录相关的接口地址
  static bool _isLogin(Response response) {
    return isLoginPath(response.requestOptions.path);
  }

  /// 是否为登录接口地址
  static bool isLoginPath(String path) {
    return path.contains("um/applogin") ||
        path.contains("sns/applogin") ||
        path.contains("um/appregister") ||
        path.contains("um/mobileSignIn") ||
        path.contains("um/processToken");
  }
}
