

import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:package_info/package_info.dart';
import '../constant/const.dart';
import '../util/log_ybd_util.dart';
import '../util/sp_ybd_util.dart';
import '../../module/user/util/user_ybd_util.dart';

Future<Map<String, dynamic>> getHeader() async {
  /// check network
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.none) {
    YBDLogUtil.e(" request failed, network error.");
    return <String, dynamic>{};
  }

  /// 请求Header 增加公共消息头：
  /// ot-lang: language, 用户设置的APP语言，en|zh
  /// ot-c: channel, 应用市场，Google或oppo 等
  /// ot-p: platform, 平台，Android或iOS
  /// ot-vc: APP version code
  /// Cookie: jsessionid; app     鉴权token相关
  String cookieCombine = await YBDUserUtil.userToken();

  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  String locale = (await YBDSPUtil.get(Const.SP_LOCALE)) ?? Const.DEFAULT_LANG;
  return <String, dynamic>{
    'Accept': 'application/json,*/*',
    'YBDContent-Type': 'application/json',
    'Cookie': cookieCombine,
    'ot-lang': locale,
    'ot-c': 'Google', // TODO channel, 应用市场，Google或oppo 等
    'ot-p': Platform.operatingSystem,
    'ot-vc': packageInfo.buildNumber,
    'Connection': 'keep-alive',
  };
}
