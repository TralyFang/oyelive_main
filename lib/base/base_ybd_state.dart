import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../common/constant/const.dart';
import '../common/navigator/navigator_ybd_helper.dart';
import '../common/util/log_ybd_util.dart';
import '../ui/widget/dialog_ybd_lock.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T> with WidgetsBindingObserver {
  // 是否用YBDLockDialog锁定操作
  bool isLocking = false;

  List<StreamSubscription?> subs = [];
  @protected
  Widget myBuild(BuildContext context);

  onAddSubscription() {}

  @override
  @mustCallSuper
  Widget build(BuildContext context) {
    initScreenUtil();

    return myBuild(context);
  }
  void buildiAcmRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  @mustCallSuper
  void initState() {
    super.initState();
    onAddSubscription();
    WidgetsBinding.instance?.addObserver(this);
  }
  void initStateg8FCPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  @mustCallSuper
  void dispose() {
    subs.forEach((StreamSubscription? element) {
      element?.cancel();
      element = null;
    });
    subs.clear();
    super.dispose();
    WidgetsBinding.instance?.removeObserver(this);
  }
  void disposeOQgKpoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 初始化适配框架
  initScreenUtil() {
    if (ScreenUtil().statusBarHeight == 0) {
      ScreenUtil.init(context, designSize: Size(Const.DESIGN_WIDTH.toDouble(), Const.DESIGN_HEIGHT.toDouble()));
    }
  }

  BuildContext? dialogContext;

  /// 锁定屏幕dialog
  showLockDialog(
      {String info: '',
      bool barrierDismissible: false,
      int timeout: 0,
      VoidCallback? timeoutCallback,
      ValueNotifier<String>? infoController}) {
    // TODO: 超时隐藏弹框功能没做好，暂时屏蔽掉
//    if (timeout > 0) {
//      Timer(Duration(seconds: timeout), () {
//        if (mounted) {
//          logger.v('timeout hide dialog');
//          dismissLockDialog();
//
//          // 超时回调
//          if (null != timeoutCallback) {
//            timeoutCallback();
//          }
//        } else {
//          logger.v('widget is unmounted ignore hide dialog timer');
//        }
//      });
//    }

    if (!isLocking) {
      logger.v('show lock dialog');
      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: barrierDismissible,
          builder: (BuildContext x) {
            dialogContext = x;
            return ValueListenableBuilder<String>(
              builder: (BuildContext context, String value, Widget? child) {
                return YBDLockDialog(() {
                  dismissLockDialog();
                }, info: value, barrierDismissible: barrierDismissible);
              },
              valueListenable: infoController ?? ValueNotifier<String>(''),
              child: YBDLockDialog(() {
                dismissLockDialog();
              }, info: info, barrierDismissible: barrierDismissible),
            );
          });
      isLocking = true;
    } else {
      logger.v('not show lock dialog');
    }
  }

  addSub(StreamSubscription sub) {
    subs.add(sub);
  }

  /// 清除锁屏dialog
  dismissLockDialog() {
    if (isLocking && mounted) {
      YBDNavigatorHelper.popPage(context);
      isLocking = false;
    }
  }

  popOut() {
    YBDNavigatorHelper.popPage(context);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (Platform.isIOS) lifeChangeNative(state);
  }
  void didChangeAppLifecycleStatevHrVzoyelive(AppLifecycleState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  lifeChangeNative(AppLifecycleState state) {}


  String? getSettingsTitle() {
    Object? arguments = ModalRoute.of(context)?.settings.arguments;
    if (arguments != null) return (arguments as Map)['title'];
    return null;
  }
}
