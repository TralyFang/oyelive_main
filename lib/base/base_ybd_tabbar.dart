import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/base/base_ybd_scaffold.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/widget/my_ybd_underline_tabIndicator.dart';
import 'package:oyelive_main/ui/page/store/widget/bubble_ybd_content.dart';
import 'package:oyelive_main/ui/page/store/widget/card_ybd_frame_content.dart';
import 'package:oyelive_main/ui/page/store/widget/discount_ybd_content.dart';
import 'package:oyelive_main/ui/page/store/widget/entrances_ybd_content.dart';
import 'package:oyelive_main/ui/page/store/widget/frame_ybd_content.dart';
import 'package:oyelive_main/ui/page/store/widget/gifts_ybd_content.dart';
import 'package:oyelive_main/ui/page/store/widget/themes_ybd_content.dart';
import 'package:oyelive_main/ui/page/store/widget/unique_ybd_content.dart';
import 'package:oyelive_main/ui/widget/my_ybd_app_bar.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';

import '../ui/page/baggage/widget/baggage_ybd_bubble_content.dart';
import '../ui/page/baggage/widget/baggage_ybd_entrance_content.dart';
import '../ui/page/baggage/widget/baggage_ybd_gifts_content.dart';
import '../ui/page/baggage/widget/baggage_ybd_micframe_content.dart';
import '../ui/page/baggage/widget/baggage_ybd_themes_content.dart';
import '../ui/page/baggage/widget/baggage_ybd_uniqueid_content.dart';
import '../ui/page/baggage/widget/baggage_ybd_vip_content.dart';
import '../ui/page/store/bloc/entrances_ybd_block.dart';

main() {
  runApp(MaterialApp(home: YBDTabBarTest1Page()));
}

class YBDTabBarTest1Page extends StatefulWidget {
  @override
  YBDTabBarTest1PageState createState() => YBDTabBarTest1PageState();
}

class YBDTabBarTest1PageState extends BaseState<YBDTabBarTest1Page> {
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return YBDTabBar(
      title: 'store',
      tabArr: YBDTabBarModel.store(),
      rightItem: Padding(
        padding: EdgeInsets.all(ScreenUtil().setWidth(24)),
        child: Image.asset(
          "assets/images/shortcut_store.webp",
          width: ScreenUtil().setWidth(48),
        ),
      ),
      rightItemCallBack: () {
        print('click right item');
      },
      switchPageCallBack: (index) {
        print('scroll to $index page');
      },
    );
  }
  void myBuildN9iHuoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDTabBarModel {
  final String title;
  final Widget widget; //对应的widget

  const YBDTabBarModel({
    required this.title,
    required this.widget,
  });

  static String kDiscount = translate("discount");
  static String kGift = translate("gifts");
  static String kEntrance = translate("entrances");
  static String kThemes = translate("themes");
  static String kMicFrame = translate("mic_frames");
  static String kBubble = translate("bubble");
  static String kUniqueId = translate("unique_id");
  static String kCardFrame = translate("card_frame");

  static List<String> storeTitles() {
    return <String>[
      kDiscount,
      kGift,
      kEntrance,
      kThemes,
      kMicFrame,
      kBubble,
      kUniqueId,
      kCardFrame,
    ];
  }

  static List<YBDTabBarModel> store() {
    List<YBDTabBarModel> tabBarModels = <YBDTabBarModel>[];

    for (String title in storeTitles()) {
      if (title == kDiscount) {
        tabBarModels.add(YBDTabBarModel(title: title, widget: YBDDiscountContent()));
      } else if (title == kGift) {
        tabBarModels.add(YBDTabBarModel(title: title, widget: YBDGiftsContent()));
      } else if (title == kEntrance) {
        tabBarModels.add(YBDTabBarModel(title: title, widget: YBDEntrancesContent()));
      } else if (title == kThemes) {
        tabBarModels.add(YBDTabBarModel(title: title, widget: YBDThemesContent()));
      } else if (title == kMicFrame) {
        tabBarModels.add(YBDTabBarModel(title: title, widget: YBDFrameContent()));
      } else if (title == kBubble) {
        tabBarModels.add(YBDTabBarModel(title: title, widget: YBDBubbleContent()));
      } else if (title == kUniqueId) {
        tabBarModels.add(YBDTabBarModel(title: title, widget: YBDUniqueContent()));
      } else if (title == kCardFrame) {
        tabBarModels.add(YBDTabBarModel(title: title, widget: YBDCardFrameContent()));
      }
    }

    return tabBarModels;
  }

  static List<YBDTabBarModel> baggage(userInfo, BuildContext c) {
    return <YBDTabBarModel>[
      YBDTabBarModel(title: translate('aristocracy'), widget: YBDBaggageVipContent(userInfo.id)),
      YBDTabBarModel(title: kGift, widget: YBDBaggageGiftContent(userInfo)..storeTitle = kGift),
      YBDTabBarModel(
          title: kEntrance,
          widget: MultiBlocProvider(providers: [
            BlocProvider<YBDEntrancesListBloc>(create: (context) {
              // logger.v('create entrance list bloc');
              return YBDEntrancesListBloc(c);
            }),
          ], child: YBDBaggageEntranceContent(userInfo)..storeTitle = kEntrance)),
      YBDTabBarModel(title: kThemes, widget: YBDBaggageThemesContent(userInfo)..storeTitle = kThemes),
      YBDTabBarModel(title: kMicFrame, widget: YBDBaggageMicframeContent(userInfo)..storeTitle = kMicFrame),
      YBDTabBarModel(title: kBubble, widget: YBDBaggageBubbleContent(userInfo)..storeTitle = kBubble),
      YBDTabBarModel(title: kUniqueId, widget: YBDBaggageUniqueIdContent(userInfo)..storeTitle = kUniqueId),
      YBDTabBarModel(title: kCardFrame, widget: YBDCardFrameContent(userId: userInfo.id)..storeTitle = kCardFrame),
    ];
  }
}

///红点功能未实现
class YBDTabBar extends StatefulWidget {
  final String title; // tab标题
  final List<YBDTabBarModel> tabArr; // tab对象数组
  final Color? bgColor; // TabBar背景颜色，
  final TabController? tabController; // TabController对象
  final Color? indicatorColor; // 指示器颜色
  final Color labelColor; // 选中label颜色
  final Color? unselectedLabelColor; // 未选中label颜色
  final double indicatorWeight; // 指示器 高度 默认2
  final double? height; // tab高度 默认35
  final TextStyle? labelStyle; // tab字体样式
  final TextStyle? unselectedLabelStyle; // 未选中时的tab字体样式
  final Decoration? indicator; // 指示器样式
  final Widget? rightItem; // tab栏右边的widget(如背包按钮)
  final Widget? leftItem; // tab栏左边的widget
  final bool haveBackBtn; // appBar是否有返回箭头
  final double elevation; // appBar的阴影
  final bool isScrollable; // tab栏是否可以左右滑动
  final Function? rightItemCallBack; // tab栏右边的widget的点击事件
  final Function? leftItemCallBack; // tab栏左边的widget的点击事件
  final Function? switchPageCallBack; // 页面切换的回调，返回index
  final int? initIndex;
  const YBDTabBar(
      {Key? key,
      required this.title,
        required this.tabArr,
        this.bgColor,
        this.tabController,
        this.indicatorColor,
      this.labelColor: Colors.white,
        this.unselectedLabelColor,
      this.indicatorWeight: 2,
        this.height,
        this.labelStyle,
        this.unselectedLabelStyle,
        this.indicator,
        this.rightItem,
        this.leftItem,
      this.haveBackBtn: true,
      this.elevation: 3,
      this.isScrollable: true,
        this.rightItemCallBack,
        this.leftItemCallBack,
        this.switchPageCallBack,
        this.initIndex})
      : super(key: key);

  @override
  _YBDTabBarState createState() => _YBDTabBarState();
}

class _YBDTabBarState extends State<YBDTabBar> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _tabController = TabController(length: widget.tabArr.length, vsync: this);
    _tabController.addListener(() {
      if (widget.switchPageCallBack != null) {
        if (_tabController.index.toDouble() == _tabController.animation?.value) {
          widget.switchPageCallBack!(_tabController.index);
        }
      }
    });
    if (widget.initIndex != null && widget.initIndex! >= 0 && widget.initIndex! < _tabController.length) {
      _tabController.index = widget.initIndex!;
      setState(() {});
    }
  }
  void initStateFTT5Xoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, designSize: Size(Const.DESIGN_WIDTH.toDouble(), Const.DESIGN_HEIGHT.toDouble()));

    return YBDBaseScaffold(
      appBar: YBDMyAppBar(
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.white, fontSize: YBDTPStyle.spNav),
        ),
        backgroundColor: YBDTPStyle.heliotrope,
        leading: widget.haveBackBtn ? YBDNavBackButton(color: Colors.white) : null,
        // 去掉导航底部投影
        elevation: 0.0,
        actions: [
          if (widget.rightItem != null)
            GestureDetector(
              onTap: () {
                widget.rightItemCallBack?.call();
              },
              child: widget.rightItem,
            )
        ],
      ),
      padding: EdgeInsets.all(0),
      expandedChild: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: ScreenUtil().setWidth(50),
            child: TabBar(
              controller: _tabController,
              indicatorSize: TabBarIndicatorSize.label,
              indicatorColor: widget.indicatorColor,
              indicatorWeight: widget.indicatorWeight,
              labelColor: widget.labelColor,
              isScrollable: widget.isScrollable,
              unselectedLabelColor: widget.unselectedLabelColor ?? Colors.white.withOpacity(0.49),
              labelStyle: widget.labelStyle ??
                  TextStyle(
                    fontWeight: FontWeight.w400,
                    height: 1,
                    fontSize: ScreenUtil().setSp(24),
                  ),
              unselectedLabelStyle: widget.unselectedLabelStyle ??
                  TextStyle(
                    fontWeight: FontWeight.w400,
                    height: 1,
                    fontSize: ScreenUtil().setSp(24),
                  ),
              indicator: widget.indicator ??
                  YBDMyUnderlineTabIndicator(
                    borderSide: BorderSide(
                      width: 2.0,
                      color: Color(0xff7DFAFF),
                    ),
                  ),
              tabs: widget.tabArr.map((item) => Tab(text: item.title != null ? item.title : null, icon: null)).toList(),
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: widget.tabArr.map((item) => item.widget).toList(),
            ),
          )
        ],
      ),
    );
  }
  void buildaAozxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
