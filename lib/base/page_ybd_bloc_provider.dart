
import 'package:flutter/cupertino.dart';

import '../common/util/log_ybd_util.dart';
import 'base_ybd_bloc.dart';

class YBDPageBlocProvider<T extends BaseBloc> extends StatefulWidget {
  final T bloc;
  final Widget child;

  YBDPageBlocProvider({Key? key, required this.child, required this.bloc}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _YBDBlocProviderState<T>();
  }

  static T of<T extends BaseBloc>(BuildContext context) {
    YBDPageBlocProvider<T> provider = context.findAncestorWidgetOfExactType() as YBDPageBlocProvider<T>;
    return provider.bloc;
  }
}

class _YBDBlocProviderState<T> extends State<YBDPageBlocProvider<BaseBloc>> {
  @override
  void initState() {
    super.initState();
    logger.v('initState ' + T.toString());
  }
  void initStateR8mDIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    logger.v('build ' + T.toString());
    return widget.child;
  }
  void buildRTsFLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    logger.v('dispose ' + T.toString());
    widget.bloc.dispose();
  }
  void disposeTrS5boyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
