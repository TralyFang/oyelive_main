import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/ui/widget/scaffold/bg_ybd_container.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';

import '../ui/widget/scaffold/bg_ybd_container.dart';

class YBDBaseScaffold extends Scaffold {
  ///body 填充的widget
  Widget expandedChild;

  ///虚拟导航title 当设置里appBar时 不需要设置这个
  String? title;

  ///body 整体背景色 不传默认[Color(0xffD251F9), Color(0xff32249D)]
  List<Color> colors;

  ///appbar 存在时 title 不需要设置
  PreferredSizeWidget? appBar;

  /// padding
  EdgeInsetsGeometry? padding;

  YBDBaseScaffold({
    this.title = null,
    required this.expandedChild,
    this.colors = YBDTPStyle.colorList,
    this.appBar = null,
    this.padding = null,
  }) : super(
            appBar: appBar,
            body: YBDBgContainer(
              padding: padding ?? EdgeInsets.only(top: ScreenUtil().statusBarHeight),
              child: appBar != null
                  ? expandedChild
                  : Column(children: [
                      Container(
                        width: ScreenUtil().screenWidth,
                        height: ScreenUtil().setWidth(116),
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                                // 导航栏返回按钮
                                top: ScreenUtil().setWidth(15),
                                left: 0,
                                child: YBDNavBackButton()),
                            Center(
                              child: Text(title ?? '', style: TextStyle(fontSize: YBDTPStyle.spNav, color: Colors.white)),
                            ),
                          ],
                        ),
                      ),
                      Expanded(child: expandedChild),
                    ]),
              colors: colors,
            ));
}
