
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/base/base_ybd_scaffold.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/abstract/base_ybd_refresh_service.dart';
import 'package:oyelive_main/common/ext/enum_ext/type_ybd_ext.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';

import '../common/navigator/navigator_ybd_helper.dart';

enum HttpType {
  SlogBlack,
  InboxBlack,
  RoomBlack,
  Default,
  QueryCardFrameStore,
  QueryCardFrameBag,
  QueryStoreUId,
  QueryStoreBubble,
  QueryStoreFrame,
  QueryStoreTheme,
  QueryStoreGift,
  QueryStoreDiscount,
  QueryBaggageVip,
  QueryBaggageGift,
  QueryBaggageTheme,
  QueryBaggageFrame,
  QueryBaggageBubble,
  QueryBaggageUId,
}

abstract class BaseRefreshListState<T extends StatefulWidget> extends BaseState<T>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  //list data
  List list = [];

  /// 是否为初始状态
  bool _isInitState = true;

  RefreshController _refreshController = RefreshController();

  bool asFragmentPage = false;

  @protected
  Widget baseBuild(BuildContext context, int index);

  @protected
  refresh();

  HttpType type = HttpType.Default;

  ///是否需要加载更多
  bool needLoadMore = false;

  ///分页加载
  int currentPage = 1;

  ///如果分页参数不是page这种标准模式，记得请求时转换
  ///eg: start/offset start = page-1这种方式
  ///如果最终服务端这几种分页方式无法修改，客户端直接每次使用到分页的接口param参数 直接传几种全量模式
  ///eg: param: {'page': page, 'pageSize': 10, 'start': (page-1)*offset, offset: 10 .....}
  service(BaseRefreshService service) {
    this.type = service.type ?? HttpType.Default;
    this.needLoadMore = service.needLoadMore;
    service.refresh(
        page: currentPage,
        block: (list, isSuccess) {
          isSuccess ? _refreshController.refreshCompleted() : _refreshController.loadFailed();
          // ignore: unnecessary_statements
          currentPage > 1 ? this.list.addAll(list!) : (this.list = list ?? []);
          logger.v('base page state list length: ${this.list}');
          loadData(list!.length < service.pageSize);
        });
  }

  loadData(bool noMoreData) {
    logger.v('refresh state: ${_refreshController.isRefresh}, $needLoadMore ${noMoreData}');
    _refreshController.resetNoData();
    if (_refreshController.isRefresh) _refreshController.refreshCompleted();
    if (_refreshController.isLoading) _refreshController.loadComplete();
    if (needLoadMore && noMoreData) {
      _refreshController.loadNoData();
    }
    _isInitState = false;
    if (mounted) setState(() {});
  }

  customItemLayout(List<Widget> children) {
    return null;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    refresh();
  }
  void initStateHjcL1oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    throw UnimplementedError();
  }
  void myBuild1KZ9Poyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @mustCallSuper
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return asFragmentPage
        ? childW()
        : YBDBaseScaffold(
            title: type.value(),
            expandedChild: childW(),
          );
  }
  void buildnwqB9oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget emptyView() {
    return YBDPlaceHolderView(text: 'No Data');
  }

  Widget baggageEmptyView({int index = 0, bool isVip = false}) {
    return YBDPlaceHolderView(
      text: translate("no_item"),
      touchLister: () {
        if (isVip) {
          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.vip);
        } else {
          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.store + '/$index');
        }
      },
    );
  }
  void baggageEmptyView1nn5Woyelive({int index = 0, bool isVip = false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget childW() {
    return list.isEmpty
        ? (_isInitState ? YBDLoadingCircle() : emptyView())
        : SmartRefresher(
            controller: _refreshController,
            header: YBDMyRefreshIndicator.myHeader,
            footer: YBDMyRefreshIndicator.myFooter,
            enablePullDown: true,
            enablePullUp: this.needLoadMore,
            onRefresh: () {
              logger.v("base page state pull down refresh");
              currentPage = 1;
              refresh();
            },
            onLoading: () {
              logger.v("base page state pull up load more");
              currentPage++;
              refresh();
            },
            child: customItemLayout(List.generate(list.length/* ?? 0*/, (index) => baseBuild(context, index))) ??
                ListView.separated(
                  padding: lvPadding(),
                  itemCount: list.length,// ?? 0, // 数据的数量
                  itemBuilder: (BuildContext context, int index) {
                    return baseBuild(context, index);
                  }, // 类似 cellForRow 函数
                  separatorBuilder: (BuildContext context, int index) => separatorItem(index),
                ));
  }
  void childWnyY5doyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget separatorItem(int index) {
    return SizedBox();
  }

  EdgeInsetsGeometry lvPadding() {
    return EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(13));
  }

  @override
  bool get wantKeepAlive => true;
}
