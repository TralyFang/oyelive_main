

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import '../common/constant/const.dart';
import '../common/global/tp_ybd_global.dart';
import '../common/util/log_ybd_util.dart';
import '../common/util/sp_ybd_util.dart';
import '../module/user/entity/user_ybd_info_entity.dart';
import 'theme_ybd_redux.dart';
import 'user_ybd_redux.dart';

import 'app_ybd_state.dart';
import 'init_ybd_redux.dart';
import 'locale_ybd_redux.dart';

class YBDInitMiddleware extends MiddlewareClass<YBDAppState> {
  @override
  call(Store<YBDAppState> store, action, NextDispatcher next) async {
    if (action is YBDInitAction) {
      logger.v("listened YBDInitAction");

      /// sp 获取主题 theme : int
      Color color = (await YBDSPUtil.get(Const.SP_THEME_COLOR)) ?? Colors.white;
      next(
        YBDUpdateThemeAction(
          YBDTPGlobal.DEFAULT_THEME_DATA.copyWith(primaryColor: color),
        ),
      );

      /// sp 获取语言 Locale
      String locale = (await YBDSPUtil.get(Const.SP_LOCALE)) ?? Const.DEFAULT_LANG;
      logger.v("-- sp locale: $locale");
      next(YBDUpdateLocaleAction(localeFromString(locale)));

      /// sp 获取用户信息
      String? spUser = await YBDSPUtil.get(Const.SP_USER_INFO);
      logger.v("-- sp user: $spUser");
      try {
        YBDUserInfo? bean;
        if (null != spUser && spUser.isNotEmpty) {
          bean = YBDUserInfo().fromJson(json.decode(spUser));
        }

        /// 同步用户信息到redux
        next(YBDUpdateUserAction(bean));
      } catch (e) {
        YBDLogUtil.e('error : $e');
      }
    }
    next(action);
  }
}
