
import 'package:redux/redux.dart';
import '../common/util/log_ybd_util.dart';
import '../module/entity/query_ybd_configs_resp_entity.dart';

final configsReducer = combineReducers<YBDConfigInfo?>([TypedReducer<YBDConfigInfo?, YBDUpdateConfigsAction>(_refresh)]);

YBDConfigInfo? _refresh(YBDConfigInfo? configs, action) {
  logger.v("YBDUpdateConfigsAction: ${action.configs}");
  configs = action.configs;
  return configs;
}

class YBDUpdateConfigsAction {
  final YBDConfigInfo configs;

  YBDUpdateConfigsAction(this.configs);
}
