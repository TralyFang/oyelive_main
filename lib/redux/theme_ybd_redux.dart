
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

final themeDataReducer = combineReducers<ThemeData?>([TypedReducer<ThemeData?, YBDUpdateThemeAction>(_refresh)]);

ThemeData? _refresh(ThemeData? themeData, action) {
  themeData = action.themeData;
  return themeData;
}

class YBDUpdateThemeAction {
  final ThemeData themeData;

  YBDUpdateThemeAction(this.themeData);
}
