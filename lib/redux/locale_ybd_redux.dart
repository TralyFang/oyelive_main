
import 'package:flutter/material.dart';
import '../common/util/log_ybd_util.dart';
import 'package:redux/redux.dart';

final localeReducer = combineReducers<Locale?>([TypedReducer<Locale?, YBDUpdateLocaleAction>(_refresh)]);

Locale? _refresh(Locale? locale, action) {
  logger.v("YBDUpdateLocaleAction: ${action.locale}");
  locale = action.locale;
  return locale;
}

class YBDUpdateLocaleAction {
  final Locale? locale;

  YBDUpdateLocaleAction(this.locale);
}
