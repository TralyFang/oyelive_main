

import 'package:flutter/material.dart';
import '../module/entity/query_ybd_configs_resp_entity.dart';
import '../module/user/entity/user_ybd_info_entity.dart';
import 'configs_ybd_redux.dart';
import 'followed_ybd_ids_redux.dart';
import 'theme_ybd_redux.dart';
import 'user_ybd_redux.dart';

import 'locale_ybd_redux.dart';

class YBDAppState {
  /// 当前登录的用户
  YBDUserInfo? bean;

  /// 当前登录用户关注的所有用户ID
  Set<int>? followedIds;

  /// APP主题
  ThemeData? themeData;

  /// APP语言
  Locale? locale;

  /// 后台配置
  YBDConfigInfo? configs;

  YBDAppState({this.bean, this.followedIds, this.themeData, this.locale, this.configs});

  factory YBDAppState.initial() => YBDAppState(
      bean: null,
      followedIds: Set(),
      themeData: ThemeData.light(),
      locale: Locale("en", "US"),
      configs: YBDConfigInfo.initial());
}

YBDAppState appReducer(YBDAppState state, action) {
  return YBDAppState(
    bean: userReducer(state.bean, action),
    followedIds: followedIdsReducer(state.followedIds, action),
    themeData: themeDataReducer(state.themeData, action),
    locale: localeReducer(state.locale, action),
    configs: configsReducer(state.configs, action),
  );
}
