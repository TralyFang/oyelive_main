
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import '../common/util/log_ybd_util.dart';
import '../module/user/entity/user_ybd_info_entity.dart';

final userReducer = combineReducers<YBDUserInfo?>(
  [
    TypedReducer<YBDUserInfo?, YBDUpdateUserAction>(_refresh),
  ],
);

YBDUserInfo? _refresh(YBDUserInfo? bean, action) {
  logger.v("YBDUpdateUserAction: ${action.bean?.toJson()}");
  bean = action.bean;
  TA.userSet(userInfo: bean);
  return bean;
}

class YBDUpdateUserAction {
  final YBDUserInfo? bean;

  YBDUpdateUserAction(this.bean);
}
