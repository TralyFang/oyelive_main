
import 'package:redux/redux.dart';
import '../common/util/log_ybd_util.dart';

final followedIdsReducer = combineReducers<Set<int>?>([TypedReducer<Set<int>?, YBDUpdateFollowedIdsAction>(_refresh)]);

Set<int>? _refresh(Set<int>? followedIds, action) {
  logger.v("YBDUpdateFollowedIdsAction: ${action.followedIds}");
  followedIds = action.followedIds;
  return followedIds;
}

class YBDUpdateFollowedIdsAction {
  final Set<int?> followedIds;

  YBDUpdateFollowedIdsAction(this.followedIds);
}
