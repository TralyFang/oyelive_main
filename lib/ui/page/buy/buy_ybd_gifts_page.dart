
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/widget/tp_ybd_rounded_btn.dart';
import 'package:oyelive_main/ui/page/buy/amount_ybd_item.dart';
import 'package:oyelive_main/ui/page/buy/manual_ybd_amount_item.dart';

import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/date_ybd_util.dart';
import '../../../common/util/image_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../home/entity/discount_ybd_list_entity.dart';
import '../home/entity/gift_ybd_list_entity.dart';
import 'buy_ybd_helper.dart';
import 'buy_ybd_info_item.dart';

enum BuyType {
  gifts,
  discount,
}

/// 购买数量的索引 [_amountItem]
const _OneAmountIndex = 0;
const _FiveAmountIndex = 1;
const _TwentyAmountIndex = 2;
const _ManualAmountIndex = 3;

/// 购买底部弹框
class YBDBuyGiftsBottomWindowPage extends StatefulWidget {
  final BuyType type;
  final YBDGiftListRecordGift? giftListRecordGift;
  final YBDDiscountListRecord? discountListRecord;
  final Function? onBuyCallBack;
  final bool stintBuy;

  YBDBuyGiftsBottomWindowPage(
    this.type, {
    this.giftListRecordGift,
    this.discountListRecord,
    this.onBuyCallBack,
    this.stintBuy = false,
  });

  @override
  _YBDBuyGiftsBottomWindowPageState createState() => _YBDBuyGiftsBottomWindowPageState();
}

class _YBDBuyGiftsBottomWindowPageState extends State<YBDBuyGiftsBottomWindowPage> {
  /// 商品图片
  String? _imageUrl;
  int _currentAmountIndex = 0;
  List<int> _amountItem = [1, 5, 20, DefaultCustomizeNumber];
  FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        logger.v("focus manual amount text field");
        _choseItem(_ManualAmountIndex);
      }
    });

    logger.v('initState giftListRecordGift: ${widget.giftListRecordGift?.img}');
    _initImgUrl();
  }
  void initStatessIT4oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 选中数量 item
  void _choseItem(int index) {
    _currentAmountIndex = index;

    // 隐藏键盘
    if (_currentAmountIndex != _ManualAmountIndex) {
      FocusScope.of(context).requestFocus(FocusNode());
    }

    setState(() {});
  }
  void _choseItem813ORoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 余额不足
  bool _insufficientBalance() {
    int? price = 0;

    if (widget.type == BuyType.gifts) {
      price = widget.giftListRecordGift!.price;
    } else {
      price = widget.discountListRecord!.discountPrice;
    }

    if (price! * _amountItem[_currentAmountIndex] > (YBDCommonUtil.storeFromContext()!.state.bean?.money ?? 0)) {
      return true;
    } else {
      return false;
    }
  }
  void _insufficientBalanceOvAH6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 根据传进来的数据类型
  void _initImgUrl() async {
    if (widget.type == BuyType.gifts) {
      _imageUrl = YBDImageUtil.gift(context, widget.giftListRecordGift?.img, 'B');
    } else if (widget.type == BuyType.discount) {
      _imageUrl = YBDImageUtil.getDiscountImgUrl(
        context,
        widget.discountListRecord!.itemType,
        widget.discountListRecord?.thumbnail,
      );
    } else {
      logger.v('buy_gifts type error');
      return;
    }
  }
  void _initImgUrloZpcdoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 固定档位的数量
  Widget _amountItemWithIndex(int index) {
    return GestureDetector(
      onTap: () {
        logger.v('clicked amount, previous item: $_currentAmountIndex');
        _choseItem(index);
      },
      child: YBDAmountItem(
        isSelected: index == _currentAmountIndex,
        amount: _amountItem[index],
      ),
    );
  }
  void _amountItemWithIndexXYrgyoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    if (widget.type == BuyType.gifts && widget.giftListRecordGift == null) {
      logger.v("-------------giftListRecordGift == null");
      return Container();
    }

    if (widget.type == BuyType.discount && widget.discountListRecord == null) {
      logger.v("-------------discountListRecord == null");
      return Container();
    }

    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        height: 650.px,
        padding: EdgeInsets.all(20.px as double),
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        child: Column(
          children: [
            SizedBox(height: 10.px),
            Row(
              children: [
                SizedBox(width: 10.px),
                _discountEndTime(),
                Expanded(child: SizedBox()),
                _closeBtn(),
                SizedBox(width: 10.px),
              ],
            ),
            SizedBox(height: 40.px),
            YBDBuyInfoItem(
              buyType: widget.type,
              imageUrl: _imageUrl,
              giftListRecordGift: widget.giftListRecordGift,
              discountListRecord: widget.discountListRecord,
            ),
            SizedBox(height: 70.px),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _amountItemWithIndex(_OneAmountIndex),
                SizedBox(width: 44.px),
                _amountItemWithIndex(_FiveAmountIndex),
                SizedBox(width: 44.px),
                _amountItemWithIndex(_TwentyAmountIndex),
                SizedBox(width: 44.px),
                YBDManualAmountItem(
                  // 折扣商品的限制数量；礼物商品无限制
                  maxAmount: widget.discountListRecord?.stock ?? 9999,
                  isSelected: _ManualAmountIndex == _currentAmountIndex,
                  onMinus: (int amount) {
                    _amountItem[_ManualAmountIndex] = amount;
                    _choseItem(_ManualAmountIndex);
                  },
                  onPlus: (int amount) {
                    _amountItem[_ManualAmountIndex] = amount;
                    _choseItem(_ManualAmountIndex);
                  },
                  onFocus: () {
                    _choseItem(_ManualAmountIndex);
                  },
                  onChanged: (int amount) {
                    _amountItem[_ManualAmountIndex] = amount;
                    _choseItem(_ManualAmountIndex);
                  },
                ),
              ],
            ),
            SizedBox(height: 80.px),
            YBDTPRoundedBtn(
              width: 440.px as double,
              height: 72.px as double,
              enable: !widget.stintBuy,
              title: translate('buy'),
              titleSp: 32.sp as double,
              titleColor: Colors.white,
              disableTitleColor: Color(0xff717171),
              enableGradient: YBDTPGlobal.blueGradient,
              disableGradient: YBDTPGlobal.greyGradient,
              onPressed: _onPressedBuyBtn,
            ),
          ],
        ),
      ),
    );
  }
  void buildMWjeXoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击购买按钮
  void _onPressedBuyBtn() {
    if (widget.stintBuy) {
      logger.v('stint buy!');
      return;
    }

    var number = _amountItem[_currentAmountIndex];

    if (number <= 0) {
      YBDToastUtil.toast(translate('enter_correct_number'));
      return;
    }

    if (_insufficientBalance()) {
      YBDToastUtil.toast(translate('insufficient_balance'));
    } else if (widget.type == BuyType.gifts) {
      YBDBuyHelper.buyGift(
        context,
        widget.giftListRecordGift?.id,
        number,
        YBDCommonUtil.storeFromContext()!.state.bean,
        price: widget.giftListRecordGift?.price,
      );
    } else if (widget.type == BuyType.discount) {
      if (number > (widget.discountListRecord?.stock ?? 0)) {
        YBDToastUtil.toast(translate('enter_correct_number'));
        return;
      }

      YBDBuyHelper.buyDiscount(
        context,
        widget.discountListRecord?.itemId,
        number,
        YBDCommonUtil.storeFromContext()!.state.bean,
        widget.discountListRecord?.itemType,
        price: widget.discountListRecord?.discountPrice,
        onBuycallBack: widget.onBuyCallBack,
      );
    } else {
      logger.v('buy type error: ${widget.type}');
    }
  }
  void _onPressedBuyBtnjA6Qdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 折扣结束时间
  Widget _discountEndTime() {
    var inPeriod = !YBDCommonUtil.isShowPeriodDay(widget.discountListRecord?.endTime, 29345252839000);
    var endTime = YBDDateUtil.getDiscountExpireDate(widget.discountListRecord?.endTimestamp);

    if (widget.type == BuyType.discount && inPeriod) {
      return Text(
        'End time：$endTime',
        style: TextStyle(
          fontSize: 24.sp,
          color: Color(0xff0FA7AD),
          fontWeight: FontWeight.w400,
        ),
      );
    }

    return SizedBox();
  }

  /// 关闭按钮
  Widget _closeBtn() {
    return GestureDetector(
      onTap: () {
        logger.v('close------');
        Navigator.pop(context);
      },
      child: Icon(
        Icons.clear,
        color: Colors.black,
      ),
    );
  }
  void _closeBtnHjSzaoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
