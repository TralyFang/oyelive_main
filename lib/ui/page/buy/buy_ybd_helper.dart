import 'dart:async';


import 'package:flutter/cupertino.dart';
import '../../../common/constant/const.dart';
import '../../../common/util/dialog_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/user/entity/user_ybd_info_entity.dart';

class YBDBuyHelper {
  ///礼物购买
  static buyGift(BuildContext context, int? goodsId, int number, YBDUserInfo? userInfo,
      {int? price = 0, bool isSpecial = false}) {
    YBDDialogUtil.showLoading(context);

    ///礼物数量填number字段
    ApiHelper.purchase(context, goodsId, number, '1', isSpecial: isSpecial).then((result) async {
      if (result?.returnCode == Const.HTTP_SUCCESS) {
        logger.v('purchase success');
        YBDToastUtil.toast('success');
        logger.v(
            'buy YBDGift success money:${userInfo!.money} curBuyInfo.price ${price! * number} price:${(userInfo.money! - price * number)}');
        userInfo.money = (userInfo.money! - price * number);
        YBDSPUtil.save(Const.SP_USER_INFO, userInfo);
        Navigator.pop(context);
      } else if (isSpecial && result?.returnCode == Const.HTTP_DISCOUNT_NUM_NOT_ENOUGH) {
        logger.v('purchase failed');
        YBDToastUtil.toast('${result?.returnMsg ?? 'purchase failed'}');
      } else {
        YBDToastUtil.toast('${result?.returnMsg ?? 'purchase failed'}');
      }
      YBDDialogUtil.hideLoading(context);
    });
  }

  ///Discount购买  {entrance:2,gift:4,theme:12,frame:13}
  static buyDiscount(BuildContext context, int? goodsId, int number, YBDUserInfo? userInfo, int? type,
      {int? price = 0, Function? onBuycallBack}) {
    ///Theme Frame
    if (type == 4 || type == 12 || type == 13) {
      YBDDialogUtil.showLoading(context);
      ApiHelper.purchase(context, goodsId, (type == 4) ? number : 1, (type == 4) ? '1' : '$number', isSpecial: true)
          .then((result) async {
        if (result?.returnCode == Const.HTTP_SUCCESS) {
          logger.v('buyTheme_Frame success');
          YBDToastUtil.toast('success');
          logger.v(
              'buyTheme_Frame success money:${userInfo!.money} curBuyInfo.price ${price! * number} price:${(userInfo.money! - price * number)}');
          onBuycallBack?.call();
          userInfo.money = (userInfo.money! - price * number);
          YBDSPUtil.save(Const.SP_USER_INFO, userInfo);
          Navigator.pop(context);
        } else if (result?.returnCode == Const.HTTP_DISCOUNT_NUM_NOT_ENOUGH) {
          logger.v('buyTheme_Frame failed');
          YBDToastUtil.toast('${result?.returnMsg ?? 'purchase failed'}');
          onBuycallBack?.call();
          Navigator.pop(context);
        } else {
          YBDToastUtil.toast('${result?.returnMsg ?? 'purchase failed'}');
          logger.v('buyTheme_Frame failed');
        }
        YBDDialogUtil.hideLoading(context);
      });
    } else if (type == 2) {
      YBDDialogUtil.showLoading(context);
      ApiHelper.buyCar(context, goodsId, '$number', isSpecial: true).then((result) async {
        if (result?.returnCode == Const.HTTP_SUCCESS) {
          logger.v('buyCar success');
          YBDToastUtil.toast('success');
          logger.v(
              'buyCar success money:${userInfo!.money} curBuyInfo.price ${price! * number} price:${(userInfo.money! - price * number)}');
          onBuycallBack?.call();
          userInfo.money = (userInfo.money! - price * number);
          YBDSPUtil.save(Const.SP_USER_INFO, userInfo);
          Navigator.pop(context);
        } else if (result?.returnCode == Const.HTTP_DISCOUNT_NUM_NOT_ENOUGH) {
          logger.v('buyCar failed');
          YBDToastUtil.toast('${result?.returnMsg ?? 'failed'}');
          onBuycallBack?.call();
          Navigator.pop(context);
        } else {
          YBDToastUtil.toast('${result?.returnMsg ?? 'failed'}');
          logger.v('buyCar failed');
        }
      });
      YBDDialogUtil.hideLoading(context);
    }

    // else if (type == 4) {
    //   buyGift(context, goodsId, number, userInfo, price: price, isSpecial: true);
    // }
  }
}
