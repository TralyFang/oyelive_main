import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

import '../../../common/constant/const.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/entity/buy_ybd_vip_result_entity.dart';
import '../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../module/user/util/user_ybd_util.dart';
import '../../../redux/app_ybd_state.dart';
import '../../../redux/user_ybd_redux.dart';
import 'buy_ybd_step.dart';

//"priceInfo": {
//            "1": [
//                {
//                    "unit": "1",
//                    "discount": "1.0"
//                },
//                {
//                    "unit": "3",
//                    "discount": "1.0"
//                },
//                {
//                    "unit": "6",
//                    "discount": "1.0"
//                },
//                {
//                    "unit": "12",
//                    "price": "100000"
//                }
//            ],
//            "2": [
//                {
//                    "unit": "1",
//                    "discount": "1.0"
//                },
//                {
//                    "unit": "3",
//                    "discount": "1.0"
//                },
//                {
//                    "unit": "6",
//                    "discount": "1.0"
//                },
//                {
//                    "unit": "12",
//                    "price": "200000"
//                }
//            ]
//        }
class YBDBuyVipStep extends BuyStep {
  List? discountData;

  int? price;

  String imagePath;

  int? vipId;

  String productName;
  YBDBuyVipStep(this.vipId, this.discountData, this.price, this.productName, this.imagePath);

  @override
  YBDBuySheetData makeData() {
    List<YBDDisplayItemData> items = [];
    discountData!.forEach((element) {
      YBDDisplayItemData data = YBDDisplayItemData();
      int unitCount = int.parse(element['unit']);
      if (unitCount > 1) {
        data.unitName = "Months";
      } else {
        data.unitName = "Month";
      }

      data.price = (unitCount * price!).toInt().toString();
      if (element['price'] != null) {
        data.discountPrice = element['price'];
      } else {
        data.discountPrice = (unitCount * price! * double.parse(element['discount'])).toInt().toString();
      }
      data.itemName = "$unitCount ${data.unitName}";
      items.add(data);
    });

    return YBDBuySheetData()
      ..productName = this.productName
      ..items = items
      ..showImage = YBDImage(
        path: imagePath,
        width: 200,
        fit: BoxFit.fitWidth,
      );
  }

  @override
  Future<bool> onBuy(BuildContext context, int index, {String? currency = Const.CURRENCY_BEAN}) async {
    YBDBuyVipResultEntity? resultEntity = await ApiHelper.buyVip(context, vipId, discountData![index]['unit']);

    if (resultEntity == null || resultEntity.returnCode != Const.HTTP_SUCCESS) {
      YBDToastUtil.toast(resultEntity!.returnMsg);
      return false;
    } else {
      YBDUserInfo info = (await YBDUserUtil.userInfo()) ?? YBDUserInfo();
      info.money = resultEntity.record!.money;
      await YBDSPUtil.save(Const.SP_USER_INFO, info);
      final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
      store.dispatch(YBDUpdateUserAction(info));
      return true;
    }
  }
  void onBuy39e04oyelive(BuildContext context, int index, {String? currency = Const.CURRENCY_BEAN})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget? makeLeftWidget(bool hasDisCount, discountPrice, originalPrice) {
    // TODO: implement makeLeftWidget
    return null;
  }

  @override
  InfoLayout getInfoLayout() {
    // TODO: implement getInfoLayout
    return InfoLayout.classic;
  }
}
