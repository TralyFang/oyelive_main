import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../redux/app_ybd_state.dart';
import 'buy_ybd_step.dart';
import '../splash/splash_ybd_util.dart';

class YBDBuySheet extends StatefulWidget {
  BuyStep buyStep;
  Function? onSuccessBuy;

  YBDBuySheet(this.buyStep, {this.onSuccessBuy});

  @override
  YBDBuySheetState createState() => new YBDBuySheetState();
}

class YBDBuySheetState extends BaseState<YBDBuySheet> {
  int selectingIndex = 0;
  List<YBDDisplayItemData>? items;
  YBDBuySheetData? buySheetData;

  Widget _getItemWidget(int i) {
    return GestureDetector(
        onTap: () {
          selectingIndex = i;
          setState(() {});
        },
        child: Container(
            alignment: Alignment.center,
            width: ScreenUtil().setWidth(155),
            height: ScreenUtil().setWidth(60),
            decoration: (i == selectingIndex)
                ? const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    gradient: LinearGradient(
                      colors: [
                        Color(0xFFC84BEE),
                        Color(0xFF9209FF),
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  )
                : BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    color: Color(0xfff0f0f0),
                  ),
            child: Text(items![i].itemName,
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(24),
                    color: (i == selectingIndex) ? Colors.white : Colors.grey,
                    fontWeight: FontWeight.w400))));
  }
  void _getItemWidgetLqtLnoyelive(int i) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    return Container(
      height: ScreenUtil().setWidth(650),
      padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
      decoration: new BoxDecoration(
        //背景Colors.transparent 透明
        color: Colors.transparent,
        //设置四周圆角 角度
        borderRadius: BorderRadius.only(topLeft: Radius.circular(4.0), topRight: Radius.circular(4.0)),
      ),
      child: Column(
        children: [
          SizedBox(
            width: ScreenUtil().setWidth(10),
          ),
          Row(
            children: [
              Expanded(
                child: Container(),
              ),
              GestureDetector(
                onTap: () {
                  print('close------');
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.clear,
                  color: Colors.black,
                ),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                height: ScreenUtil().setWidth(200),
//                width: ScreenUtil().setWidth(200),
                decoration: new BoxDecoration(
                  //背景Colors.transparent 透明
                  color: Colors.transparent,
                  //设置四周圆角 角度
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(4.0), topRight: Radius.circular(4.0)),
                ),
                child: buySheetData!.showImage,
              ),
              SizedBox(
                width: ScreenUtil().setWidth(40),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: ScreenUtil().setWidth(15),
                  ),
                  Container(
                      child: Row(
                    children: [
                      Image.asset(
                        'assets/images/topup/y_top_up_beans@2x.webp',
                        width: ScreenUtil().setWidth(45),
                        height: ScreenUtil().setWidth(50),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(15),
                      ),
                      Text(
                        YBDCommonUtil.formatNum(num.parse(items![selectingIndex].discountPrice!)),
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(36), color: Colors.amberAccent, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(10),
                      ),
                      Container(
                        child: items![selectingIndex].discountPrice != items![selectingIndex].price
                            ? Text(
                                YBDCommonUtil.formatNum(num.parse(items![selectingIndex].price!)),
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(28),
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400,
                                  decoration: TextDecoration.lineThrough,
                                ),
                              )
                            : Container(),
                      ),
                    ],
                  )),
                  SizedBox(
                    width: ScreenUtil().setWidth(15),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(300),
                    child: Text(
                      buySheetData!.productName!,
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.black, fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: Container(),
              ),
              Text('${translate('balance')}:',
                  style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.black, fontWeight: FontWeight.w400)),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
              Text(
                '${YBDCommonUtil.formatNum(store.state.bean!.money)}',
                style:
                    TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.amberAccent, fontWeight: FontWeight.w400),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
              Image.asset(
                'assets/images/topup/y_top_up_beans@2x.webp',
                width: ScreenUtil().setWidth(35),
                height: ScreenUtil().setWidth(40),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
            ],
          ),
          SizedBox(
            height: ScreenUtil().setWidth(70),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _getItemWidget(0),
              SizedBox(
                width: ScreenUtil().setWidth(20),
              ),
              _getItemWidget(1),
              SizedBox(
                width: ScreenUtil().setWidth(20),
              ),
              _getItemWidget(2),
              SizedBox(
                width: ScreenUtil().setWidth(20),
              ),
              _getItemWidget(3),
            ],
          ),
          SizedBox(
            height: ScreenUtil().setWidth(80),
          ),
          GestureDetector(
            onTap: () async {
              showLockDialog(info: "Buying");
              bool isSuccess = await widget.buyStep.onBuy(context, selectingIndex);
              dismissLockDialog();
              if (isSuccess) {
                YBDToastUtil.toast(translate('success'));
                Navigator.pop(context);
                widget.onSuccessBuy?.call();
              }
            },
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(600),
              height: ScreenUtil().setWidth(75),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                gradient: LinearGradient(
                  colors: [
                    Color(0xFFC84BEE),
                    Color(0xFF9209FF),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
              child: Text(translate('buy_now'),
                  style: TextStyle(fontSize: ScreenUtil().setSp(32), color: Colors.white, fontWeight: FontWeight.w600)),
            ),
          ),
        ],
      ),
    );
  }
  void myBuildBrZR7oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    buySheetData = widget.buyStep.makeData();
    items = buySheetData!.items;
  }
  void initStateDNNXcoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDBuySheet oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
  void didChangeDependenciesy5MI3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
