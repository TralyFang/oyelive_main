import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:path/path.dart' as p;
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_svga_theme.dart';

import '../../../common/util/image_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../follow/widget/place_ybd_holder_view.dart';
import '../store/entity/theme_ybd_entity.dart';
import 'buy_ybd_page.dart';

/// 购买预览页面
class YBDBuyPopWindowPage extends StatefulWidget {
  /// 数据源
  final List<YBDThemeRecord?>? themeRecordList;

  /// 初始图片下标
  final int curIndex;

  YBDBuyPopWindowPage(this.themeRecordList, this.curIndex);

  @override
  _YBDBuyPopWindowPageState createState() => _YBDBuyPopWindowPageState();
}

class _YBDBuyPopWindowPageState extends State<YBDBuyPopWindowPage> with AutomaticKeepAliveClientMixin {
  /// 当前图片下标
  int? _curImgIndex;
  // 可以购买
  late bool _canBuy;
  YBDThemeRecord? _themeRecord;
  @override
  void initState() {
    super.initState();
    _curImgIndex = widget.curIndex;
    _themeRecord = widget.themeRecordList![_curImgIndex!];
    _canBuy = !YBDCommonUtil.stintBuy(type: _themeRecord!.conditionType, lv: _themeRecord!.conditionExtends);
  }
  void initStateMVy7ioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == widget.themeRecordList || widget.themeRecordList!.isEmpty) {
      logger.v('BuyData list is empty');
      return YBDPlaceHolderView(text: translate('no_data'));
    }

    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      SizedBox(
        height: ScreenUtil().setWidth(956),
        width: ScreenUtil().setWidth(404),
        child: Swiper(
          // 滑动预览
          scrollDirection: Axis.horizontal,
          itemCount: widget.themeRecordList!.length,
          autoplay: false,
          onIndexChanged: (int changedIndex) {
            _curImgIndex = changedIndex;
            _canBuy = !YBDCommonUtil.stintBuy(type: _themeRecord!.conditionType, lv: _themeRecord!.conditionExtends);
            print('22.5.6-----_curImgIndex:$_curImgIndex--_canBuy:$_canBuy');
            setState(() {});
          },
          outer: false,
          index: _curImgIndex,
          pagination: SwiperPagination(
              alignment: Alignment.bottomCenter,
              // margin: EdgeInsets.only(top: ScreenUtil().setWidth(50)),
              builder: FractionPaginationBuilder(
                color: Color(0x9FFFFFFF),
                fontSize: ScreenUtil().setSp(26),
                activeColor: Color(0xFFFFFFFF),
                activeFontSize: ScreenUtil().setSp(26),
              )),
          itemBuilder: (BuildContext context, int index) {
            // 当前图片
            return Padding(
              padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(60)),
              child: ClipRect(
                child: _themeImage(widget.themeRecordList![index]),
              ),
            );
          },
        ),
      ),
      SizedBox(height: ScreenUtil().setWidth(10)),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          // 关闭按钮
          alignment: Alignment.center,
          width: ScreenUtil().setWidth(180),
          height: ScreenUtil().setWidth(70),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            gradient: LinearGradient(
              colors: [
                Color(0xfff0f0f0),
                Color(0xfff0f0f0),
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: TextButton(
            child: Text(
              translate('close'),
              style: TextStyle(
                fontSize: ScreenUtil().setSp(28),
                color: Colors.black,
                fontWeight: FontWeight.w600,
              ),
            ),
            onPressed: () {
              logger.v('clicked close btn');
              Navigator.pop(context);
            },
          ),
        ),
        SizedBox(width: ScreenUtil().setWidth(30)),
        Container(
          // 购买按钮
          alignment: Alignment.center,
          width: ScreenUtil().setWidth(300),
          height: ScreenUtil().setWidth(70),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            gradient: YBDTPGlobal.blueGradient,
          ),
          child: TextButton(
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(300),
              height: ScreenUtil().setWidth(70),
              child: Text(
                translate('buy'),
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(28),
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            onPressed: () {
              logger.v('clicked buy btn');
              // 关闭弹框
              Navigator.pop(context);
              // 弹出底部购买弹框
              showModalBottomSheet(
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8.0),
                    topRight: Radius.circular(8.0),
                  ),
                  side: BorderSide(style: BorderStyle.none),
                ),
                builder: (context) => YBDBuyBottomWindowPage(
                  BuyType.theme,
                  null,
                  null,
                  widget.themeRecordList![_curImgIndex!],
                  null,
                  currency: widget.themeRecordList![_curImgIndex!]!.currency,
                  stintBuy: !_canBuy,
                ),
              );

              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                YBDEventName.CLICK_EVENT,
                location: YBDLocationName.THEMES_PREVIEW_DIALOG,
                itemName: 'buy',
                value: '${p.extension(widget.themeRecordList![_curImgIndex!]!.image ?? '')}',
              ));
            },
          ),
        ),
      ]),
    ]);
  }
  void buildKxjt0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 主题图片
  Widget _themeImage(YBDThemeRecord? themeRecord) {
    String imgUrl;
    if (null != themeRecord) {
      imgUrl = YBDImageUtil.car(context, themeRecord.image, 'D');

      if (YBDCommonUtil.isSvgaFile(themeRecord.image)) {
        imgUrl = YBDImageUtil.gif(context, themeRecord.image, YBDImageType.THEME);
      } else {
        imgUrl = YBDImageUtil.theme(context, themeRecord.image, "A");
      }
    } else {
      logger.v('ad is null show default ad img');
      imgUrl = YBDImageUtil.getDefaultImg(context);
    }

    return _itemImg(imgUrl);
  }
  void _themeImageqgiAPoyelive(YBDThemeRecord? themeRecord) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item图片
  Widget _itemImg(String imgUrl) {
    // 用 svgaUrl 显示动画图片
    return YBDRoomSvgaTheme(
      imgUrl,
    );
  }
}
