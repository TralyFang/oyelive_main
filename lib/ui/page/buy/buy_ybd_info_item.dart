import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/home/entity/discount_ybd_list_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/gift_ybd_list_entity.dart';

import '../store/widget/level_ybd_stint_buy_tag.dart';
import 'buy_ybd_gifts_page.dart';

/// 商品信息 item
class YBDBuyInfoItem extends StatelessWidget {
  final BuyType? buyType;
  final YBDGiftListRecordGift? giftListRecordGift;
  final YBDDiscountListRecord? discountListRecord;
  final String? imageUrl;

  const YBDBuyInfoItem({
    this.buyType,
    this.imageUrl,
    this.giftListRecordGift,
    this.discountListRecord,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          _itemImg(),
          SizedBox(width: 44.px),
          _itemInfo(),
        ],
      ),
    );
  }
  void buildiXiO9oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 商品图片
  Container _itemImg() {
    return Container(
      padding: EdgeInsets.only(
        right: 16.px as double,
        left: 16.px as double,
      ),
      height: 180.px,
      width: 220.px,
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(4.0),
          topRight: Radius.circular(4.0),
        ),
      ),
      child: YBDNetworkImage(
        imageUrl: imageUrl ?? '',
        width: 140.px,
        height: 140.px,
        fit: BoxFit.contain,
      ),
    );
  }

  /// 商品信息和用户余额
  Container _itemInfo() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Row(
              children: [
                Image.asset(
                  'assets/images/topup/y_top_up_beans@2x.webp',
                  width: 29.px,
                  fit: BoxFit.cover,
                ),
                SizedBox(width: 5.px),
                _originalPrice(),
                // 折扣价格
                _discountPrice(),
                SizedBox(width: 20.px),
                // 折扣商品剩余数量
                _discountNumber(),
              ],
            ),
          ),
          SizedBox(height: 16.px),
          Row(
            children: [
              _itemName(),
              YBDTPGlobal.wSizedBox(10),
              YBDLevelStintTag(
                stintType: giftListRecordGift?.conditionType,
                stintLv: giftListRecordGift?.conditionExtends,
              ),
            ],
          ),
          SizedBox(height: 10.px),
          _userMoney(),
        ],
      ),
    );
  }

  /// 折扣商品的剩余数量
  Widget _discountNumber() {
    if (buyType == BuyType.discount) {
      return Text(
        'Number: ${discountListRecord?.stock}',
        style: TextStyle(
          fontSize: 24.sp,
          color: Color(0xff717171),
          fontWeight: FontWeight.w400,
        ),
      );
    }

    return Container();
  }
  void _discountNumberdx7Gdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 折扣价格
  Container _discountPrice() {
    if (buyType == BuyType.discount) {
      return Container(
        margin: EdgeInsets.only(left: 15.px as double),
        child: Row(
          children: [
            Image.asset(
              'assets/images/beans_grey.webp',
              width: 18.px,
              fit: BoxFit.cover,
            ),
            Text(
              '${YBDCommonUtil.formatNum(discountListRecord?.originalPrice)}',
              style: TextStyle(
                fontSize: 22.sp,
                color: Colors.grey,
                fontWeight: FontWeight.w400,
                decoration: TextDecoration.lineThrough,
                decorationColor: Colors.grey,
              ),
            ),
          ],
        ),
      );
    }

    return Container();
  }

  /// 商品名
  Widget _itemName() {
    String? name = '';

    if (buyType == BuyType.gifts) {
      name = giftListRecordGift?.name;
    } else {
      name = discountListRecord?.name;
    }

    return Text(
      '  $name',
      textAlign: TextAlign.left,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        fontSize: 24.sp,
        color: Colors.black,
        fontWeight: FontWeight.w400,
      ),
    );
  }
  void _itemNamezkaIFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 当前用户余额
  Widget _userMoney() {
    var money = YBDCommonUtil.formatNum(
      YBDCommonUtil.storeFromContext()!.state.bean?.money,
    );

    return Row(
      children: [
        Text(
          '${translate('balance')}:',
          style: TextStyle(
            fontSize: 24.sp,
            color: Color(0xff717171),
            fontWeight: FontWeight.w400,
          ),
        ),
        SizedBox(width: 10.px),
        Text(
          '$money',
          style: TextStyle(
            fontSize: 24.sp,
            color: Color(0xffEBB01E),
            fontWeight: FontWeight.w400,
          ),
        )
      ],
    );
  }
  void _userMoneyjADN3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 原价
  Widget _originalPrice() {
    int? price = 0;

    if (buyType == BuyType.gifts) {
      price = giftListRecordGift?.price;
    } else {
      price = discountListRecord?.discountPrice;
    }

    return Text(
      '${YBDCommonUtil.formatNum(price)}',
      style: TextStyle(
        fontSize: 36.sp,
        color: Color(0xffEBB01E),
        fontWeight: FontWeight.w600,
      ),
    );
  }
  void _originalPricef9Z7koyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
