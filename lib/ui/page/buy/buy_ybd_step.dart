import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/common/constant/const.dart';

import '../store/entity/price_ybd_info_entity.dart';

enum InfoLayout {
  classic, //原版
  tight //收缩
}

abstract class BuyStep {
  YBDBuySheetData? data;
  YBDBuySheetData? makeData();

  InfoLayout? infoLayout;
  InfoLayout getInfoLayout();

  Future<bool> onBuy(BuildContext context, int index, {String? currency = Const.CURRENCY_BEAN});

  Widget? makeLeftWidget(bool hasDisCount, discountPrice, originalPrice);
}

class YBDBuySheetData {
  List<YBDDisplayItemData>? items;
  //产品名称
  String? productName;
  //显示的缩略图
  Widget? showImage;

  List<YBDPriceInfoEntity?>? priceInfo;

  bool bigLayout = false;

  // 限制购买信息
  int? conditionType;
  String? conditionExtends;
}

class YBDDisplayItemData {
  //选项名称 ，为数量加单位
  late String itemName;
  //原价

  String? price;
  //特价

  String? discountPrice;

  // 单位
  String? unitName;
}
