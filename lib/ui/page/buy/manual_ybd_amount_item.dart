import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/ExtraHittest/gesture_ybd_detector.dart';

const int DefaultCustomizeNumber = 1;

typedef OnAmountChanged = void Function(int amont);

/// 手动输入商品数量
class YBDManualAmountItem extends StatefulWidget {
  final OnAmountChanged? onMinus;
  final bool isSelected;
  final int maxAmount;
  final OnAmountChanged? onPlus;
  final Function? onFocus;
  final OnAmountChanged? onChanged;

  const YBDManualAmountItem({
    required this.maxAmount,
    this.isSelected = false,
    this.onMinus,
    this.onPlus,
    this.onFocus,
    this.onChanged,
    Key? key,
  }) : super(key: key);

  @override
  State<YBDManualAmountItem> createState() => _YBDManualAmountItemState();
}

class _YBDManualAmountItemState extends State<YBDManualAmountItem> {
  var _numberController = TextEditingController(text: '$DefaultCustomizeNumber');
  int _amount = DefaultCustomizeNumber;
  FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        //有焦点
        logger.v("---_focusNode hasFocus: 001");
        widget.onFocus?.call();
      } else {
        //失去焦点
        logger.v("---_focusNode.hasFocus:002");
      }
    });
  }
  void initStateLGA9ooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _numberController.dispose();
    super.dispose();
  }
  void disposesaxcIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _minusBtn(),
          SizedBox(width: 10.px),
          _manualInputAmount(),
          SizedBox(width: 10.px),
          _plusBtn(),
        ],
      ),
    );
  }
  void buildkpMiAoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 减号按钮
  GestureDetector _minusBtn() {
    var icon = 'assets/images/login/number_less.png';

    if (_amount <= DefaultCustomizeNumber) {
      icon = 'assets/images/login/number_less_gray.png';
    }

    return GestureDetector(
      onTap: () {
        logger.v('clicked minus btn');
        if (_amount> 1) {
          _amount--;
          _numberController.text = '$_amount';
          logger.v('customizeNumber --:$_amount');
          widget.onMinus?.call(_amount);
          setState(() {});
        }
      },
      child: Container(
        decoration: BoxDecoration(),
        width: 50.px,
        child: Image.asset(
          icon,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  /// 加号按钮
  GestureDetector _plusBtn() {
    var icon = 'assets/images/login/number_plus.png';

    if (_amount >= widget.maxAmount) {
      icon = 'assets/images/login/number_plus_gray.png';
    }

    return GestureDetector(
      onTap: () {
        logger.v('clicked plus btn');
        if (_amount < widget.maxAmount) {
          _amount++;
          _numberController.text = '$_amount';
          widget.onPlus?.call(_amount);
          setState(() {});
        }
      },
      child: Container(
        decoration: BoxDecoration(),
        width: 50.px,
        child: Image.asset(
          icon,
        ),
      ),
    );
  }

  /// 手动输入数量
  Container _manualInputAmount() {
    var borderColor = widget.isSelected ? Color(0xff6EA8E6) : Color(0xfff0f0f0);

    return Container(
      alignment: Alignment.center,
      width: ScreenUtil().setWidth(100),
      height: ScreenUtil().setWidth(56),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
        border: Border.all(color: borderColor, width: 1),
        color: widget.isSelected ? Color(0xffE9FBF8) : Color(0xfff0f0f0),
      ),
      child: TextField(
        inputFormatters: [
          LengthLimitingTextInputFormatter(11),
          FilteringTextInputFormatter.digitsOnly,
        ],
        keyboardType: TextInputType.number,
        controller: _numberController,
        textInputAction: TextInputAction.done,
        textAlign: TextAlign.center,
        focusNode: _focusNode,
        style: TextStyle(
          color: Color(0xff333333),
          fontSize: ScreenUtil().setSp(24),
          fontWeight: FontWeight.w400,
        ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(bottom: ScreenUtil().setWidth(35)),
          border: InputBorder.none,
        ),
        onChanged: (text) {
          logger.v('input text changed: $text');
          if (_numberController.text.startsWith('0')) {
            _numberController.text = _numberController.text.substring(1, _numberController.text.length);
            logger.v('input text remove prefix zero: ${_numberController.text}');
          }

          _amount = int.parse(_numberController.text.isEmpty ? 0 as String : _numberController.text);

          // 设置最小值
          if (_amount < DefaultCustomizeNumber) {
            _amount = DefaultCustomizeNumber;
            _numberController.text = '$DefaultCustomizeNumber';
          }

          // 设置最大值
          if (_amount > widget.maxAmount) {
            _amount = widget.maxAmount;
            _numberController.text = '${widget.maxAmount}';
          }

          widget.onChanged?.call(_amount);
        },
      ),
    );
  }
}
