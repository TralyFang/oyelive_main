import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

/// 商品数量 item
class YBDAmountItem extends StatelessWidget {
  final int amount;
  final bool isSelected;

  const YBDAmountItem({
    this.amount = 1,
    this.isSelected = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: ScreenUtil().setWidth(85),
      height: ScreenUtil().setWidth(56),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8.px as double)),
        color: _bgColor(),
        border: Border.all(color: Color(0xff6EA8E6), width: 1.px as double),
      ),
      child: Text(
        '$amount',
        style: TextStyle(
          fontSize: ScreenUtil().setSp(24),
          color: Color(0xff333333),
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
  void build8ty2Goyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 背景色
  Color _bgColor() {
    return isSelected ? Color(0xffD7F5FB) : Colors.white;
  }
}
