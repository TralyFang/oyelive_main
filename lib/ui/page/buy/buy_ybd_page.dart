import 'dart:async';



import 'dart:developer';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:svgaplayer_flutter/player.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';

import '../../../common/constant/const.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/dialog_ybd_util.dart';
import '../../../common/util/image_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/entity/discount_ybd_info_entity.dart';
import '../../../module/user/entity/user_ybd_info_entity.dart';
import '../../widget/dialog_ybd_lock.dart';
import '../room/play_center/entity/personal_ybd_frame_entity.dart';
import '../room/widget/normal_ybd_dialog.dart';
import '../store/entity/mic_ybd_frame_entity.dart';
import '../store/entity/price_ybd_info_entity.dart';
import '../store/entity/theme_ybd_entity.dart';
import 'package:redux/redux.dart';

enum BuyType {
  car,
  frame,
  theme,
  vip,
}

class YBDBuyInfo {
  int? id;
  String? img;
  String? name;

  ///价格
  String? price;

  ///没有折扣的时候和price一个价格  有折扣的时候discountPrice表原价
  String? discountPrice;
  String? item;
  // 限制购买信息
  int? conditionType;
  String? conditionExtends;
}

final int DISPLAY_NUMBER = 4;

//YBDCarListRecord carListRecord;
//YBDMicFrameRecord micFrameRecord;

// 购买页面
class YBDBuyBottomWindowPage extends StatefulWidget {
  BuyType type;
  var carListRecord;
  YBDMicFrameRecord? micFrameRecord;
  YBDThemeRecord? themeRecord;
  YBDThemeRecord? vipData;
  int? roomId;
  int source;
  Function? onBuySuccess;
  String? currency; // 货币类型
  bool stintBuy; // 是否限制购买

  YBDBuyBottomWindowPage(
    this.type,
    this.carListRecord,
    this.micFrameRecord,
    this.themeRecord,
    YBDThemeRecord? vipData, {
    this.roomId,
    this.source = 0,
    this.onBuySuccess,
    this.currency = Const.CURRENCY_BEAN,
    this.stintBuy = false,
  });

  @override
  _YBDBuyBottomWindowPageState createState() => _YBDBuyBottomWindowPageState();
}

class _YBDBuyBottomWindowPageState extends State<YBDBuyBottomWindowPage> {
  String? imageUrl;
  List<YBDPriceInfoEntity?>? priceInfoEntityList;
  YBDBuyInfo? curBuyInfo;
  int curItem = 0;
  List<YBDBuyInfo> buyInfoList = [];
  YBDUserInfo? userInfo;
  late bool insufficientBalance;
  late bool isLocking;

  @override
  void initState() {
    super.initState();
    logger.v("---personalFram roomId:${widget.roomId}");
    Future.delayed(Duration.zero, () {
      _initData();
    });
  }
  void initStater416doyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _choseItem(int i) {
    curItem = i;
    curBuyInfo = buyInfoList[i];
    insufficientBalance = !YBDCommonUtil.checkIsEnoughByCurrency(widget.currency, curBuyInfo!.price!);
    YBDCommonUtil.wlog('insufficientBalance', insufficientBalance.toString());
    setState(() {});
  }

  bool isPNG = false;

  ///根据传进来的数据类型  重新封装
  _initData() async {
    userInfo = await YBDSPUtil.getUserInfo();
    if (userInfo == null) {
      return;
    }
    if (widget.type == BuyType.car) {
      if (widget.carListRecord == null) {
        logger.v('modalButtomSheet carListRecord == null');
        return;
      }
      imageUrl = YBDImageUtil.car(context, widget.carListRecord.img, 'A');
      YBDBuyInfo buyInfo;
      priceInfoEntityList = await YBDSPUtil.getCarDiscount();

      for (int i = 0; i < DISPLAY_NUMBER; i++) {
        buyInfo = new YBDBuyInfo();
        if ("-1" == priceInfoEntityList![i]!.unit) {
          buyInfo.item = 'Lifetime';
        } else {
          String unitTxt = "";
          switch (priceInfoEntityList![i]!.unitType) {
            case 1: // day
              if (int.parse(priceInfoEntityList![i]!.unit!) > 1) {
                unitTxt = " days";
              } else {
                unitTxt = " day";
              }
              break;
            case 3: // month
            default:
              if (int.parse(priceInfoEntityList![i]!.unit!) > 1) {
                unitTxt = " months";
              } else {
                unitTxt = " month";
              }
              break;
          }
          buyInfo.item = priceInfoEntityList![i]!.unit! + unitTxt;
          buyInfo.name = widget.carListRecord.name;
          buyInfo.id = widget.carListRecord.id;
          buyInfo.conditionType = widget.carListRecord.conditionType ?? 0;
          buyInfo.conditionExtends = widget.carListRecord.conditionExtends;
          String? normalPrice;
          if (priceInfoEntityList![i]!.discount == null) {
            if (priceInfoEntityList![i]!.price != null) {
              normalPrice = priceInfoEntityList![i]!.price;
            } else {
              normalPrice = (widget.carListRecord.price * int.parse(priceInfoEntityList![i]!.unit!)).toString();
            }
          } else {
            normalPrice = (widget.carListRecord.price *
                    int.parse(priceInfoEntityList![i]!.unit!) *
                    double.parse(priceInfoEntityList![i]!.discount!))
                .toInt()
                .toString();
            if (double.parse(priceInfoEntityList![i]!.discount!) < 1) {
              ///折扣少于1  折扣价和原价
              /*     mOriginalPriceTv.setVisibility(View.VISIBLE);
            mOriginalPriceTv.setText(NumberUtil.format(mCarInfo.getPrice() * Integer.parseInt(priceInfos.get(i).getUnit())));*/
              buyInfo.discountPrice = (widget.carListRecord.price * int.parse(priceInfoEntityList![i]!.unit!)).toString();
            } else {
              ///没有折扣  显示原价
//            mOriginalPriceTv.setVisibility(View.GONE);
              buyInfo.discountPrice = null;
            }
          }
          buyInfo.price = normalPrice;
        }
        buyInfoList.add(buyInfo);
        if (i == 0) {
          curBuyInfo = buyInfo;
        }
      }
    } else if (widget.type == BuyType.frame) {
      if (widget.micFrameRecord == null) {
        logger.v('modalButtomSheet carListRecord == null');
        return;
      }
      isPNG =
          widget.micFrameRecord!.animationFile!.endsWith("png") || widget.micFrameRecord!.animationFile!.endsWith("jpg");
      if (isPNG)
        imageUrl = YBDImageUtil.frame(context, widget.micFrameRecord!.image, "B");
      else
        imageUrl = YBDImageUtil.gif(context, widget.micFrameRecord!.animationFile, YBDImageType.FRAME);
      YBDBuyInfo buyInfo;
      priceInfoEntityList = await YBDSPUtil.getFrameDiscount();

      for (int i = 0; i < DISPLAY_NUMBER; i++) {
        buyInfo = new YBDBuyInfo();
        if ("-1" == priceInfoEntityList![i]!.unit) {
          buyInfo.item = 'Lifetime';
        } else {
          String unitTxt = "";
          switch (priceInfoEntityList![i]!.unitType) {
            case 1: // day
              if (int.parse(priceInfoEntityList![i]!.unit!) > 1) {
                unitTxt = " days";
              } else {
                unitTxt = " day";
              }
              break;
            case 3: // month
            default:
              if (int.parse(priceInfoEntityList![i]!.unit!) > 1) {
                unitTxt = " months";
              } else {
                unitTxt = " month";
              }
              break;
          }
          buyInfo.item = priceInfoEntityList![i]!.unit! + unitTxt;
          buyInfo.name = widget.micFrameRecord!.name;
          buyInfo.id = widget.micFrameRecord!.id;
          buyInfo.conditionType = widget.micFrameRecord!.conditionType;
          buyInfo.conditionExtends = widget.micFrameRecord!.conditionExtends;
          String? normalPrice;
          if (priceInfoEntityList![i]!.discount == null) {
            if (priceInfoEntityList![i]!.price != null) {
              normalPrice = priceInfoEntityList![i]!.price;
            } else {
              normalPrice = (widget.micFrameRecord!.price! * int.parse(priceInfoEntityList![i]!.unit!)).toString();
            }
          } else {
            normalPrice = (widget.micFrameRecord!.price! *
                    int.parse(priceInfoEntityList![i]!.unit!) *
                    double.parse(priceInfoEntityList![i]!.discount!))
                .toInt()
                .toString();
            if (double.parse(priceInfoEntityList![i]!.discount!) < 1) {
              ///折扣少于1  折扣价和原价
              /*     mOriginalPriceTv.setVisibility(View.VISIBLE);
            mOriginalPriceTv.setText(NumberUtil.format(mCarInfo.getPrice() * Integer.parseInt(priceInfos.get(i).getUnit())));*/
              buyInfo.discountPrice = (widget.micFrameRecord!.price! * int.parse(priceInfoEntityList![i]!.unit!)).toString();
            } else {
              ///没有折扣  显示原价
//            mOriginalPriceTv.setVisibility(View.GONE);
              buyInfo.discountPrice = null;
            }
          }
          buyInfo.price = normalPrice;
        }
        buyInfoList.add(buyInfo);
        if (i == 0) {
          curBuyInfo = buyInfo;
        }
      }
    } else if (widget.type == BuyType.theme) {
      if (widget.themeRecord == null) {
        logger.v('modalButtomSheet themeRecord == null');
        return;
      }
      if (widget.themeRecord!.image!.endsWith("gift") ||
          widget.themeRecord!.image!.endsWith("gif") ||
          widget.themeRecord!.image!.endsWith("svga")) {
        imageUrl = YBDImageUtil.themeTh(context, widget.themeRecord!.thumbnail, "A");
      } else {
        imageUrl = YBDImageUtil.theme(context, widget.themeRecord!.image, "A");
      }
      YBDBuyInfo buyInfo;
      priceInfoEntityList = await YBDSPUtil.getThemeDiscount();

      for (int i = 0; i < DISPLAY_NUMBER; i++) {
        buyInfo = new YBDBuyInfo();
        if ("-1" == priceInfoEntityList![i]!.unit) {
          buyInfo.item = 'Lifetime';
        } else {
          String unitTxt = "";
          switch (priceInfoEntityList![i]!.unitType) {
            case 1: // day
              if (int.parse(priceInfoEntityList![i]!.unit!) > 1) {
                unitTxt = " days";
              } else {
                unitTxt = " day";
              }
              break;
            case 3: // month
            default:
              if (int.parse(priceInfoEntityList![i]!.unit!) > 1) {
                unitTxt = " months";
              } else {
                unitTxt = " month";
              }
              break;
          }
          buyInfo.item = priceInfoEntityList![i]!.unit! + unitTxt;
          buyInfo.name = widget.themeRecord!.name;
          buyInfo.id = widget.themeRecord!.id;
          buyInfo.conditionType = widget.themeRecord!.conditionType;
          buyInfo.conditionExtends = widget.themeRecord!.conditionExtends;
          String? normalPrice;
          if (priceInfoEntityList![i]!.discount == null) {
            if (priceInfoEntityList![i]!.price != null) {
              normalPrice = priceInfoEntityList![i]!.price;
            } else {
              normalPrice = (widget.themeRecord!.price! * int.parse(priceInfoEntityList![i]!.unit!)).toString();
            }
          } else {
            normalPrice = (widget.themeRecord!.price! *
                    int.parse(priceInfoEntityList![i]!.unit!) *
                    double.parse(priceInfoEntityList![i]!.discount!))
                .toInt()
                .toString();
            if (double.parse(priceInfoEntityList![i]!.discount!) < 1) {
              ///折扣少于1  折扣价和原价
              /*     mOriginalPriceTv.setVisibility(View.VISIBLE);
            mOriginalPriceTv.setText(NumberUtil.format(mCarInfo.getPrice() * Integer.parseInt(priceInfos.get(i).getUnit())));*/
              buyInfo.discountPrice = (widget.themeRecord!.price! * int.parse(priceInfoEntityList![i]!.unit!)).toString();
            } else {
              ///没有折扣  显示原价
//            mOriginalPriceTv.setVisibility(View.GONE);
              buyInfo.discountPrice = null;
            }
          }
          buyInfo.price = normalPrice;
        }
        buyInfoList.add(buyInfo);
        if (i == 0) {
          curBuyInfo = buyInfo;
        }
      }
    } else if (widget.type == BuyType.vip) {
      // TODO: VIP

      if (widget.vipData == null) {
        logger.v('modalButtomSheet vipRecord == null');
        return;
      }
    }
    insufficientBalance = !YBDCommonUtil.checkIsEnoughByCurrency(widget.currency, curBuyInfo!.price!);
    setState(() {});
  }

  Widget _getItemWidget(int i) {
    return GestureDetector(
      onTap: () {
        _choseItem(i);
      },
      child: (i == curItem)
          ? Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(155),
              height: ScreenUtil().setWidth(60),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8.px as double)),
                border: Border.all(color: Color(0xff5B9CE3), width: 1.px as double),
                color: Color(0xffD7F5FB),
              ),
              child: Text('${buyInfoList[i].item}',
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(24),
                    color: Color(0xff333333),
                    fontWeight: FontWeight.w400,
                  )))
          : Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(155),
              height: ScreenUtil().setWidth(60),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8.px as double)),
                border: Border.all(color: Color(0xff5B9CE3), width: 1.px as double),
              ),
              child: Text(
                '${buyInfoList[i].item}',
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  color: Color(0xff333333),
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
    );
  }
  void _getItemWidget51dDwoyelive(int i) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    if (curBuyInfo == null) {
      return Container();
    }
    final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    return Container(
      height: ScreenUtil().setWidth(650),
      padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
      decoration: new BoxDecoration(
        //背景Colors.transparent 透明
        color: Colors.transparent,
        //设置四周圆角 角度
        borderRadius: BorderRadius.only(topLeft: Radius.circular(4.0), topRight: Radius.circular(4.0)),
      ),
      child: Column(
        children: [
          SizedBox(
            width: ScreenUtil().setWidth(10),
          ),
          Row(
            children: [
              Expanded(
                child: Container(),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.clear,
                  color: Colors.black,
                ),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
            ],
          ),
          SizedBox(
            height: ScreenUtil().setWidth(40),
          ),
          Row(
            children: [
              Container(
                height: ScreenUtil().setWidth(180),
                width: ScreenUtil().setWidth(220),
                decoration: new BoxDecoration(
                  //背景Colors.transparent 透明
                  color: Colors.transparent,
                  //设置四周圆角 角度
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(4.0), topRight: Radius.circular(4.0)),
                ),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    if (widget.type == BuyType.frame)
                      ClipOval(
                        child: YBDNetworkImage(
                          imageUrl: YBDImageUtil.avatar(context, store!.state.bean!.headimg, store.state.bean!.id),
                          width: ScreenUtil().setWidth(140),
                          height: ScreenUtil().setWidth(140),
                          fit: BoxFit.contain,
                          placeholder: (context, url) =>
                              YBDResourcePathUtil.defaultMaleImage(male: store.state.bean!.sex == 2),
                        ),
                      ),
                    imageUrl!.endsWith('svga')
                        ? SizedBox(
                            width: ScreenUtil().setWidth(220),
                            height: ScreenUtil().setWidth(180),
                            child: SVGASimpleImage(
                              resUrl: imageUrl,
                            ))
                        : ClipRRect(
                            borderRadius: BorderRadius.circular(ScreenUtil().setWidth(18)),
                            child: YBDNetworkImage(
                              imageUrl: imageUrl!,
                              width: ScreenUtil().setWidth(220),
                              height: ScreenUtil().setWidth(180),
                              fit: BoxFit.contain,
                            ),
                          ),
                  ],
                ),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(44),
              ),
              Container(
                height: ScreenUtil().setWidth(180),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Spacer(
                      flex: 46,
                    ),
                    Container(
                        // width: ScreenUtil().setWidth(400),
                        child: Row(
                      children: [
                        Image.asset(
                          YBDCommonUtil.getCurrencyImg(widget.currency),
                          width: ScreenUtil().setWidth(29),
                          height: ScreenUtil().setWidth(31),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(1),
                        ),
                        Text(
                          '${YBDCommonUtil.formatNum(curBuyInfo!.price)}',
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(36), color: Color(0xffEBB01E), fontWeight: FontWeight.w600),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(10),
                        ),
                        curBuyInfo!.discountPrice != null
                            ? Row(children: [
                                Image.asset(
                                  YBDCommonUtil.getCurrencyGreyImg(widget.currency),
                                  width: ScreenUtil().setWidth(18),
                                  height: ScreenUtil().setWidth(18),
                                ),
                                Text(
                                  '${YBDCommonUtil.formatNum(curBuyInfo!.discountPrice)}',
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(22),
                                    color: Colors.grey,
                                    // color: Color(0xffEBB01E),
                                    fontWeight: FontWeight.w400,
                                    decoration: TextDecoration.lineThrough,
                                    decorationColor: Colors.grey,
                                  ),
                                ),
                              ])
                            : Container()
                      ],
                    )),
                    Spacer(
                      flex: 20,
                    ),
                    Row(
                      children: [
                        Text(
                          ' ${curBuyInfo!.name}',
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(24), color: Colors.black, fontWeight: FontWeight.w400),
                        ),
                        YBDTPGlobal.wSizedBox(10),
                        YBDLevelStintTag(stintType: curBuyInfo!.conditionType, stintLv: curBuyInfo!.conditionExtends),
                      ],
                    ),
                    Spacer(
                      flex: 10,
                    ),
                    Row(
                      children: [
                        Text('${translate('balance')}:',
                            style: TextStyle(
                                fontSize: ScreenUtil().setSp(24),
                                color: Color(0xff717171),
                                fontWeight: FontWeight.w400)),
                        SizedBox(
                          width: ScreenUtil().setWidth(10),
                        ),
                        Text(
                          YBDCommonUtil.getBalanceByCurrency(widget.currency),
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(24), color: Color(0xffEBB01E), fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: ScreenUtil().setWidth(70),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _getItemWidget(0),
              SizedBox(
                width: ScreenUtil().setWidth(20),
              ),
              _getItemWidget(1),
              SizedBox(
                width: ScreenUtil().setWidth(20),
              ),
              _getItemWidget(2),
              SizedBox(
                width: ScreenUtil().setWidth(20),
              ),
              _getItemWidget(3),
            ],
          ),
          SizedBox(
            height: ScreenUtil().setWidth(80),
          ),
          Container(
            alignment: Alignment.center,
            width: ScreenUtil().setWidth(600),
            height: ScreenUtil().setWidth(75),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              gradient: widget.stintBuy ? YBDTPGlobal.greyGradient : YBDTPGlobal.blueGradient,
            ),
            child: TextButton(
                child: Container(
                  alignment: Alignment.center,
                  width: ScreenUtil().setWidth(600),
                  height: ScreenUtil().setWidth(75),
                  child: Text(
                    translate('buy_now'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(32),
                      color: widget.stintBuy ? Color(0xff717171) : Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                onPressed: () {
                  if (widget.stintBuy) {
                    logger.v('stint buy!');
                    return;
                  }
                  if (insufficientBalance) {
                    YBDCommonUtil.gotoRechargePage(widget.currency);
                    YBDToastUtil.toast(translate('insufficient_balance'));
                    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                      YBDEventName.CLICK_EVENT,
                      location: YBDLocationName.BOTTOM_BUY_DIALOG,
                      itemName: 'insufficientBalance',
                    ));
                  } else if (widget.type == BuyType.car) {
                    YBDDialogUtil.showLoading(context);
                    ApiHelper.buyCar(context, curBuyInfo!.id, priceInfoEntityList![curItem]!.unit).then((result) async {
                      bool success = result?.returnCode == Const.HTTP_SUCCESS;
                      if (!success) {
                        logger.v('buyCar failed');
                        YBDToastUtil.toast('failed');
                      } else {
                        widget.onBuySuccess?.call();
                        YBDToastUtil.toast('success');
                        _updateUserBalance();
                        store!.dispatch(YBDUpdateUserAction(userInfo));
                        YBDSPUtil.save(Const.SP_USER_INFO, userInfo);
                        Navigator.pop(context);
                      }
                      YBDDialogUtil.hideLoading(context);
                    });
                    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                      YBDEventName.CLICK_EVENT,
                      location: YBDLocationName.BOTTOM_BUY_DIALOG,
                      itemName: 'buyCar',
                    ));
                  } else if (widget.type == BuyType.frame || widget.type == BuyType.theme) {
                    YBDDialogUtil.showLoading(context);
                    bool purchasesu = false;
                    ApiHelper.purchase(context, curBuyInfo!.id, 1, priceInfoEntityList![curItem]!.unit)
                        .then((result) async {
                      bool success;
                      if (result?.returnCode == Const.HTTP_SUCCESS) {
                        logger.v('purchase success');
                        success = true;
                      } else {
                        logger.v('purchase failed');
                        success = false;
                      }

                      if (!success) {
                        logger.v('buy Frame failed');
                        YBDToastUtil.toast('failed');
                      } else {
                        widget.onBuySuccess?.call();
                        YBDToastUtil.toast('purchase success');
                        _updateUserBalance();
                        Future.delayed(const Duration(milliseconds: 1500), () {
                          store!.dispatch(YBDUpdateUserAction(userInfo));
                        });
                        YBDSPUtil.save(Const.SP_USER_INFO, userInfo);
                        purchasesu = true;
                      }
                      YBDDialogUtil.hideLoading(context);
                      logger.v('---personalFrames widget.source:${widget.source}');
                      if (purchasesu && widget.source == 1) {
                        await Future.delayed(const Duration(milliseconds: 500), () {
                          equipFrame(curBuyInfo!.name, 'FRAME');
                        });
                      } else if (purchasesu) {
                        Navigator.pop(context);
                      }
                    });
                    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                      YBDEventName.CLICK_EVENT,
                      location: YBDLocationName.BOTTOM_BUY_DIALOG,
                      itemName: '${widget.type == BuyType.frame ? 'buyFrame' : 'buyTheme'}',
                    ));
                  } else if (widget.type == BuyType.vip) {
                    // TODO: VIP
                  }
                }),
          ),
        ],
      ),
    );
  }
  void buildQMg1Voyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 更新余额
  void _updateUserBalance() {
    YBDCommonUtil.wlog("_updateUserInfo-currency", widget.currency, desc2: '-', msg2: curBuyInfo!.price);
    int reduce = int.parse(curBuyInfo!.price!);
    switch (widget.currency) {
      case Const.CURRENCY_BEAN:
        int money = userInfo?.money ?? 0;
        money -= reduce;
        userInfo!.money = money;
        break;
      case Const.CURRENCY_GEM:
        int gems = userInfo!.gems ?? 0;
        gems -= reduce;
        userInfo!.gems = gems;
        break;
      case Const.CURRENCY_GOLD:
        int golds = userInfo!.golds ?? 0;
        golds -= reduce;
        userInfo!.golds = golds;
        break;
      default:
        int money = userInfo!.money ?? 0;
        money -= reduce;
        userInfo!.money = money;
        break;
    }
  }
  void _updateUserBalancevRy6Joyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<bool?>? equipFrame(String? name, String goodsType) async {
    logger.v('---personalFrames equipFrame name:$name goodsType:$goodsType');
    YBDDialogUtil.showNormalDialog(
      context,
      type: NormalDialogType.two,
      barrierDismissible: false,
      okCallback: () {
        Navigator.pop(context);
        YBDDialogUtil.showLoading(context);
        ApiHelper.queryPersonalFrame(context, userInfo!.id, goodsType).then((personalFrameEntity) async {
          // 刷新数据失败
          if (personalFrameEntity == null || personalFrameEntity.returnCode != "000000") {
            logger.v('queryMyFrame failed ${personalFrameEntity?.returnCode ?? 'personalFrameEntity==null'}');
          } else {
            logger.v('queryMyFrame SUCCESS');
            if (personalFrameEntity.record != null) {
              List<YBDPersonalFrameRecordItemList?>? personalFrames;
              for (int i = 0; i < personalFrameEntity.record!.length; i++) {
                if (personalFrameEntity.record![i]!.type == "FRAME") {
                  personalFrames = personalFrameEntity.record![i]!.itemList;
                  break;
                }
              }
              logger.v('---personalFrames:${personalFrames?.length}');
              if (personalFrames != null) {
                for (int i = 0; i < personalFrames.length; i++) {
                  logger.v('---personalFrames personalFrames[i]?.name:${personalFrames[i]?.name}  name:$name');
                  if (personalFrames[i]?.name == name) {
                    YBDDiscountInfoEntity? discountInfoEntity =
                        await ApiHelper.equip(context, personalFrames[i]!.personalId, widget.roomId, goodsType);
                    if (discountInfoEntity?.returnCode == "000000") {
                      YBDToastUtil.toast(translate('equip success'));
                    } else {
                      YBDToastUtil.toast(translate('failed'));
                    }
                    YBDDialogUtil.hideLoading(context);
                    Navigator.pop(context);
                    return Future<bool>.value(false);
                  }
                }
              }
            }
          }
          YBDDialogUtil.hideLoading(context);
          Navigator.pop(context);
        });
        // return Future<bool>.value(false);
      },
      content: 'equip $name',
      ok: translate('equip'),
      cancel: translate('cancel'),
    );
  }

  /// 锁定屏幕dialog
  showLockDialog(
      {String info: '',
      bool barrierDismissible: false,
      int timeout: 0,
      VoidCallback? timeoutCallback,
      ValueNotifier<String>? infoController}) {
    if (!isLocking) {
      logger.v('show lock dialog');
      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: barrierDismissible,
          builder: (BuildContext context) {
            return ValueListenableBuilder(
              builder: (context, dynamic value, child) {
                return YBDLockDialog(() {
                  dismissLockDialog();
                }, info: value, barrierDismissible: barrierDismissible);
              },
              valueListenable: infoController ?? ValueNotifier<String>(''),
              child: YBDLockDialog(() {
                dismissLockDialog();
              }, info: info, barrierDismissible: barrierDismissible),
            );
          });
      isLocking = true;
    } else {
      logger.v('not show lock dialog');
    }
  }

  /// 清除锁屏dialog
  dismissLockDialog() {
    if (isLocking) {
      YBDNavigatorHelper.popPage(context);
      isLocking = false;
    }
  }
}
