import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_item.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';

import '../../../../common/util/common_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../bloc/mini_ybd_operation_def.dart';
import '../entity/mic_ybd_bean.dart';

/// mini profile 工具类
class YBDMiniProfileUtil {
  static List<YBDOperationItem> operationList(
    int? myId,
    int roomId,
    int userId,
    bool? isFriend, {
    List<YBDUserInfo?>? roomManagers,
    List<YBDMicBean?>? micBeanS,
    bool isEnemy: false,
    bool isEnemyMuted: false,
    bool gameRoomType: false,
    // List<GameMicUser> gameUsers,
  }) {
    bool isPking = YBDLiveService().data.pkInfo?.pkId != null;

    assert(null != roomId);
    assert(null != userId);

    /// 操作列表
    List<YBDOperationItem> operationList = [];

    // 默认操作列表
    operationList.addAll(_defaultOperations(isFriend ?? false, gameRoomType));
    if (null == myId) {
      operationList.clear();
    } else if (myId == userId) {
      // 自己的 profile 不显示操作按钮
      operationList.clear();
    } else if (myId == roomId) {
      // 主播查看其他人的 profile
      if (isEnemy) {
        //查看的是对手的
        operationList.clear();
        operationList.addAll(_talentCheckEnemy(isMuted: isEnemyMuted, gameRoomType: gameRoomType));
      } else {
        operationList.addAll(_talentOperationList(
            roomManagers: roomManagers ?? [],
            micBeanS: micBeanS ?? [],
            userId: userId,
            isPking: isPking,
            gameRoomType: gameRoomType));
      }
    } else if (_isRooManagerUser(roomManagers ?? [], myId)) {
      // 管理员查看其他人的 profile
      ///管理员不能禁言 邀请 主播
      if (userId != roomId) {
        operationList.addAll(_managerOperationList(userId, roomId, isPking, gameRoomType));
      }
    }

    return operationList;
  }

  /// 判断用户是否为管理员
  static bool _isRooManagerUser(List<YBDUserInfo?> sourceList, int checkId) {
    bool result = false;

    if (null != sourceList) {
      sourceList.forEach((element) {
        if (element?.id == checkId) {
          result = true;
        }
      });
    }

    return result;
  }

  /// 管理员查看其他人时 profile 的操作列表
  static List<YBDOperationItem> _managerOperationList(int userId, int roomId, bool isPking, bool gameRoomType) {
    List<YBDOperationItem> opList = [];

    // 邀麦
    YBDOperationItem inviteMicItem = YBDOperationItem(
      _operationImage(OperationType.InviteMic),
      _operationName(OperationType.InviteMic),
      OperationType.InviteMic,
    );
    //pk时不可以邀请上麦
    if (!isPking && !gameRoomType) opList.add(inviteMicItem);

    // 禁言
    YBDOperationItem muteItem = YBDOperationItem(
      _operationImage(OperationType.Mute),
      _operationName(OperationType.Mute),
      OperationType.Mute,
    );
    opList.add(muteItem);

    return opList;
  }

  /// 主播查看其他人时 profile 的操作列表
  static List<YBDOperationItem> _talentOperationList({
    required List<YBDUserInfo?> roomManagers,
    required List<YBDMicBean?> micBeanS,
    required int userId,
    required bool isPking,
    required bool gameRoomType,
  }) {
    List<YBDOperationItem> opList = [];
    // boot guardian block invite ban

    // 踢人
    YBDOperationItem bootItem = YBDOperationItem(
      _operationImage(OperationType.Boot),
      _operationName(OperationType.Boot),
      OperationType.Boot,
    );
    opList.add(bootItem);

    if (_isRooManagerUser(roomManagers, userId)) {
      // 取消管理员
      YBDOperationItem guardianItem = YBDOperationItem(_operationImage(OperationType.CancelGuardian),
          _operationName(OperationType.CancelGuardian), OperationType.CancelGuardian,
          enable: !isPking, imgDisable: "assets/images/guardian_disable.webp");
      if (!gameRoomType) opList.add(guardianItem);
    } else {
      // 添加管理员
      YBDOperationItem guardianItem = YBDOperationItem(
          _operationImage(OperationType.Guardian), _operationName(OperationType.Guardian), OperationType.Guardian,
          enable: !isPking, imgDisable: "assets/images/guardian_disable.webp");
      if (!gameRoomType) opList.add(guardianItem);
    }

    // 拉黑
    YBDOperationItem blockItem = YBDOperationItem(
      _operationImage(OperationType.Block),
      _operationName(OperationType.Block),
      OperationType.Block,
    );
    if (!gameRoomType) opList.add(blockItem);

    YBDMicBean? mic;
    if (null != micBeanS) {
      mic = micBeanS.firstWhereOrNull(
        (element) => element!.userId == userId,
        // orElse: () => null,
      );
    }

    if (null != mic) {
      // 用户在麦位上
      if (mic.mute!) {
        // 被静音 显示取消静音按钮
        YBDOperationItem unMuteMicItem = YBDOperationItem(
            _operationImage(OperationType.UnMuteMic), _operationName(OperationType.UnMuteMic), OperationType.UnMuteMic,
            enable: !isPking, imgDisable: "assets/images/mute_disable.webp");
        if (!gameRoomType) opList.add(unMuteMicItem);
      } else {
        // 没静音 显示静音按钮
        YBDOperationItem muteMicItem = YBDOperationItem(
            _operationImage(OperationType.MuteMic), _operationName(OperationType.MuteMic), OperationType.MuteMic,
            enable: !isPking, imgDisable: "assets/images/mute_disable.webp");
        if (!gameRoomType) opList.add(muteMicItem);
      }
    } else {
      // 用户不在麦位显示邀麦按钮
      YBDOperationItem inviteMicItem = YBDOperationItem(
        _operationImage(OperationType.InviteMic),
        _operationName(OperationType.InviteMic),
        OperationType.InviteMic,
      );
      //pk时不可以邀请上麦
      if (!isPking && !gameRoomType) opList.add(inviteMicItem);
    }

    // 禁言
    YBDOperationItem muteItem = YBDOperationItem(
      _operationImage(OperationType.Mute),
      _operationName(OperationType.Mute),
      OperationType.Mute,
    );
    if (!gameRoomType) opList.add(muteItem);



    return opList;
  }

  static List<YBDOperationItem> _talentCheckEnemy({
    required bool isMuted,
    bool? gameRoomType,
  }) {
    List<YBDOperationItem> opList = [];
    if (isMuted) {
      YBDOperationItem unMuteMicItem = YBDOperationItem(_operationImage(OperationType.UnMuteEnemyMic),
          _operationName(OperationType.UnMuteEnemyMic), OperationType.UnMuteEnemyMic,
          enable: true);
      if (!gameRoomType!) opList.add(unMuteMicItem);
    } else {
      YBDOperationItem MuteMicItem = YBDOperationItem(_operationImage(OperationType.MuteEnemyMic),
          _operationName(OperationType.MuteEnemyMic), OperationType.MuteEnemyMic,
          enable: true);
      if (!gameRoomType!) opList.add(MuteMicItem);
    }

    return opList;
  }

  /// 默认操作列
  static List<YBDOperationItem> _defaultOperations(bool isFriend, bool gameRoomType) {
    List<YBDOperationItem> defaultList = [];

    // YBDOperationItem sayHiItem = YBDOperationItem(
    //   _operationImage(isFriend ? OperationType.Chat : OperationType.SayHi),
    //   _operationName(isFriend ? OperationType.Chat : OperationType.SayHi),
    //   isFriend ? OperationType.Chat : OperationType.SayHi,
    // );
    // defaultList.add(sayHiItem);

    return defaultList;
  }

  /// 操作图片
  static String _operationImage(OperationType type) {
    switch (type) {
      case OperationType.SayHi:
        return 'assets/images/mini/mini_say_hi.webp';
      case OperationType.Chat:
        return 'assets/images/mini/mini_chat.webp';
      case OperationType.Block:
        return 'assets/images/liveroom/icon_block@2x.webp';
      case OperationType.Guardian:
        return 'assets/images/liveroom/icon_guardian@2x.webp';
      case OperationType.CancelGuardian:
        return 'assets/images/liveroom/icon_no_guardian@2x.webp';
      case OperationType.InviteMic:
        return 'assets/images/liveroom/icon_take_mic@2x.webp';
      case OperationType.UnMuteMic:
      case OperationType.UnMuteEnemyMic:
        return 'assets/images/liveroom/icon_mic@2x.webp';
      case OperationType.MuteMic:
      case OperationType.MuteEnemyMic:
        return 'assets/images/liveroom/icon_mute_mic2@2x.webp';
      case OperationType.Mute:
        return 'assets/images/liveroom/icon_ban@2x.webp';
      case OperationType.Boot:
        return 'assets/images/liveroom/icon_boot@2x.webp';
      default:
        return '';
    }
  }

  /// 操作名称
  static String _operationName(OperationType type) {
    // TODO: 翻译
    switch (type) {
      case OperationType.SayHi:
        return 'Say Hi';
      case OperationType.Chat:
        return 'Chat';
      case OperationType.Block:
        return 'Block';
      case OperationType.Guardian:
        return 'Guardian';
      case OperationType.CancelGuardian:
        return 'Guardian';
      case OperationType.InviteMic:
        return 'Invite';
      case OperationType.UnMuteMic:
      case OperationType.UnMuteEnemyMic:
        return 'UnMute';
      case OperationType.MuteMic:
      case OperationType.MuteEnemyMic:
        return 'Mute';
      case OperationType.Mute:
        return 'Ban';
      case OperationType.Boot:
        return 'Boot';
      default:
        return '-';
    }
  }

  /// 查询守护者最大数量
  static int getMaxGuardianCount(BuildContext context) {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    String count = store.state.configs?.maxRoomManager ?? '15';
    return int.parse(count);
  }
}
