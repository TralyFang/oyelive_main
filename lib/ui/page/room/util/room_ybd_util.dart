import 'dart:async';


import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart' as g;
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_live_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/common/util/download_ybd_manager.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/room/entity/gift_ybd_package_entity.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_data.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';

import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/room_socket/entity/fans_ybd_info.dart';
import '../../../../common/room_socket/message/resp/response.dart';
import '../../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../home/widget/tp_ybd_web_view.dart';
import '../bloc/combo_ybd_bloc.dart';
import '../bloc/gift_ybd_sheet_bloc.dart';
import '../bloc/mini_ybd_profile_bloc.dart';
import '../bloc/room_ybd_bloc.dart';
import '../mic/mic_ybd_bloc.dart';
import '../play_center/play_ybd_center.dart';
import '../room_ybd_fans_page.dart';
import '../room_ybd_helper.dart';
import '../service/live_ybd_service.dart';
import '../teen_patti/pages/teen_ybd_patti_picker_view.dart';
import '../teen_patti/pages/teen_ybd_vip_view.dart';
import '../widget/gift_ybd_receiver.dart';
import '../widget/gift_ybd_sheet.dart';
import '../widget/mic_ybd_request_sheet.dart';
import '../widget/mini_ybd_profile_dialog.dart';
import '../widget/room_ybd_pwd_dialog.dart';
import '../widget/room_ybd_viewers_sheet.dart';
import 'tp_ybd_dialog_manager.dart';

/// 房间工具类
class YBDRoomUtil {
  static int? _liveDurationInMillSecs;

  static int? get liveDurationInMillSecs => _liveDurationInMillSecs;

  static set liveDurationInMillSecs(int? value) {
    _liveDurationInMillSecs = value;
  }

  /// 跳转到私信页面
  static jumpToInboxPage(BuildContext context, int? userId) {
    YBDNavigatorHelper.navigateTo(
      context,
      YBDNavigatorHelper.inbox_private_message + "/$userId/}",
    );
  }

  /// 跳转到粉丝列表
  static jumpToFansListPage(
    BuildContext context,
    int? roomId,
    List<YBDFansInfo>? totalRanking,
    List<YBDFansInfo>? currentRanking,
  ) {
    // Navigator.push(
    //   context,
    //   CupertinoPageRoute(
    //     builder: (context) => YBDRoomFansPage(
    //       roomId,
    //       currentRanking,
    //       totalRanking,
    //     ),
    //   ),
    // );

    YBDNavigatorHelper.navigateTo(
      context,
      YBDNavigatorHelper.room_fans,
      routeSettings: RouteSettings(
        arguments: YBDRoomFansPage(
          roomId,
          currentRanking,
          totalRanking,
        ),
      ),
    );
  }

  /// 弹出底部在线用户列表或管理员列表
  /// selectedTab 0, 在线用户列表； 1，管理员列表
  static showViewersBottomSheet(
    BuildContext context,
    int? roomId,
    int selectedTab,
  ) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(ScreenUtil().setWidth(32)),
        ),
      ),
      builder: (_) {
        return MultiBlocProvider(
          providers: [
            BlocProvider.value(value: BlocProvider.of<YBDRoomBloc>(context)),
            BlocProvider.value(value: BlocProvider.of<YBDMicBloc>(context)),
            BlocProvider.value(value: BlocProvider.of<YBDRoomComboBloc>(context)),
          ],
          child: YBDRoomViewersSheet(
            roomId,
            index: selectedTab,
          ),
        );
      },
    );
  }

  static bool isOpenH5Game = false;

  /// 显示 h5 游戏
  static void showH5Game(
    BuildContext buildContext,
    String? url, {
    WebGameType type = WebGameType.Greedy,
  }) {
    if (isOpenH5Game) return;

    /// 游戏已经展示了就不用再展示了
    isOpenH5Game = true;
    if (url!.contains('?')) {
      url += '&isFlutter=1';
    } else {
      url += '?isFlutter=1';
    }
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: buildContext,
      isScrollControlled: true,
      enableDrag: false,
      isDismissible: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(ScreenUtil().setWidth(16)),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
          color: Colors.transparent,
          height: ScreenUtil().screenHeight,
          child: YBDTPWebView(
            url,
            showNavBar: false,
            opaque: false,
            gameType: type,
            closeCallback: () {
              isOpenH5Game = false;
            },
          ),
        );
      },
    ).then((value) => isOpenH5Game = false);
  }

  /// 显示 turnTable 游戏
  static void showTurnTableGame(BuildContext buildContext, String url) {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: buildContext,
      isScrollControlled: true,
      enableDrag: false,
      isDismissible: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(ScreenUtil().setWidth(16)),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
          color: Colors.transparent,
          height: ScreenUtil().screenHeight,
          child: YBDTPWebView(
            url,
            showNavBar: false,
            opaque: false,
            gameType: WebGameType.TurnTable,
          ),
        );
      },
    );
  }

  /// 显示 ludo 游戏
  static void showLudoGame(BuildContext buildContext, String url) {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: buildContext,
      isScrollControlled: true,
      enableDrag: false,
      isDismissible: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(ScreenUtil().setWidth(16)),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
          color: Colors.transparent,
          height: ScreenUtil().screenWidth * 1.25,
          child: YBDTPWebView(
            url,
            showNavBar: false,
            opaque: false,
            gameType: WebGameType.Ludo,
          ),
        );
      },
    );
  }

  /// 弹出底部 PlayCenter
  static showPlayCenterSheet(
    int? roomId, {
    int? defaultTab,
  }) {
    var context = YBDLiveService.instance.data.roomContext!;
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(16))),
      ),
      builder: (_) {
        return MultiBlocProvider(
          providers: [
            BlocProvider.value(value: BlocProvider.of<YBDMicBloc>(context)),
          ],
          child: YBDPlayCenterPage(
            roomId,
            defaultTab: defaultTab,
          ),
        );
      },
    );
  }

  ///显示teen_patti 弹窗
  static showTeenPattiPickerView(BuildContext context) {
    showGeneralDialog(
        context: context,
        barrierDismissible: false,
        barrierLabel: '',
        barrierColor: Colors.transparent,
        transitionDuration: Duration(milliseconds: 200),
        pageBuilder: (context, animation, sectionAnimation) {
          return Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    color: Colors.transparent,
                  ),
                ),
                YBDTeenPickerView()
              ],
            ),
          );
        });
  }

  ///显示teen_patti VipModel弹窗
  static showTeenPattiVipView(BuildContext? context, bool isShowModeView) {
    showGeneralDialog(
        context: g.Get.context!,
        barrierDismissible: false,
        barrierLabel: '',
        barrierColor: Colors.transparent,
        transitionDuration: Duration(milliseconds: 200),
        pageBuilder: (context, animation, sectionAnimation) {
          return Center(
            child: YBDTeenVipView(isShowModeView),
          );
        });
  }

  static YBDGiftPackageState giftPackageState = YBDGiftPackageState(
    SheetAction.Init,
    viewingTabIndex: 0,
    viewingPageIndex: 0,
    multiple: 1,
  );

  static YBDGiftSheetBloc? giftPackageBloc;
  static int combo = 1;
  static YBDGiftPackageRecordGift? roomGuideGiftInfo;

  static resetGiftPackageState() {
    giftPackageState = YBDGiftPackageState(
      SheetAction.Init,
      viewingTabIndex: 0,
      viewingPageIndex: 0,
      multiple: 1,
    );
  }

  /// 弹出底部送礼弹框
  /// [tabIndex] 选中的标签栏索引
  /// [giftIndex] 选中的礼物索引
  static showGiftSheet({
    List<YBDReceiverUser>? specUsers,
    int? tabIndex,
    int? giftIndex,
    YBDGiftPackageRecordGift? giftInfo,
    VoidCallback? send,
    bool showPackage = false,
  }) {
    logger.v('_giftSheetBloc showGiftSheet');
    final context = YBDLiveService.instance.data.roomContext!;

    if (null != giftPackageBloc) {
      // 关闭前一个bloc
      logger.v('close giftPackageBloc');
      giftPackageBloc!.close();
      giftPackageBloc = null;
    }

    giftPackageBloc = YBDGiftSheetBloc(giftPackageState);

    // 选中礼物标签
    if (null != tabIndex) {
      giftPackageBloc!.add(
        YBDGiftPackageState(
          SheetAction.ChangeTab,
          viewingTabIndex: tabIndex,
        ),
      );
    }

    // 选中礼物
    if (null != giftIndex) {
      if (null != giftInfo) {
        giftPackageBloc!.add(
          YBDGiftPackageState(
            SheetAction.SelectGift,
            selectingGiftIndex: 0,
            giftInfo: giftInfo,
          ),
        );
      } else {
        giftPackageBloc!.add(
          YBDGiftPackageState(
            SheetAction.SelectGift,
            selectingGiftIndex: 0,
          ),
        );
      }
    }

    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(ScreenUtil().setWidth(8)),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (_) {
        logger.v('_giftSheetBloc build BottomSheet');
        return MultiBlocProvider(
          providers: [
            BlocProvider<YBDGiftSheetBloc>.value(
              value: giftPackageBloc ?? YBDGiftSheetBloc(giftPackageState),
            ),
            BlocProvider.value(
              value: BlocProvider.of<YBDRoomComboBloc>(context),
            ),
            BlocProvider.value(
              value: BlocProvider.of<YBDRoomBloc>(context),
            ),
            BlocProvider.value(
              value: BlocProvider.of<YBDMicBloc>(context),
            ),
          ],
          child: YBDGiftSheet(
            roomInfo: BlocProvider.of<YBDRoomBloc>(context).state.roomInfo,
            talentInfo: BlocProvider.of<YBDRoomBloc>(context).state.talentInfo,
            specUsers: specUsers,
            showPackage: showPackage,
            onSend: (info) {
              combo = 1;
              BlocProvider.of<YBDRoomComboBloc>(context).add(
                YBDComboInfo(
                  comboStatus: ComboStatus.Counting,
                  info: info,
                ),
              );
            },
          ),
        );
      },
    );
  }

  static sendCombo(BuildContext context) {
    giftPackageBloc!.sendCombo(++combo, context);

    return combo;
  }

  /// 弹出底部麦位申请列表
  static showMicRequestBottomSheet(BuildContext context, int roomId) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(ScreenUtil().setWidth(16)),
        ),
      ),
      builder: (_) {
        return MultiBlocProvider(
          providers: [
            BlocProvider.value(value: BlocProvider.of<YBDRoomBloc>(context)),
            BlocProvider.value(value: BlocProvider.of<YBDMicBloc>(context)),
          ],
          child: YBDMicRequestSheet(roomId),
        );
      },
    );
  }

  /// 显示房间 mini profile 对话框
  static showMiniProfileDialog({
    int? roomId,
    int? userId,
    bool clickEnemy: false,
    bool isEnemyMuted: false,
    bool isEnemyGuest: false,
  }) {
    final context = YBDLiveService.instance.data.roomContext ?? g.Get.context!;
    showDialog(
      context: context,
      builder: (BuildContext builder) {
        return MultiBlocProvider(
          providers: [
            BlocProvider.value(
              value: BlocProvider.of<YBDRoomBloc>(context),
            ),
            BlocProvider.value(
              value: BlocProvider.of<YBDMicBloc>(context),
            ),
            BlocProvider.value(
              value: BlocProvider.of<YBDRoomComboBloc>(context),
            ),
            BlocProvider<YBDMiniProfileBloc>(
              create: (_) {
                return YBDMiniProfileBloc(context, userId, roomId);
              },
            ),
          ],
          child: YBDMiniProfileDialog(
            userId,
            roomId,
            isClickOnPkEnemy: clickEnemy,
            isEnemyMuted: isEnemyMuted,
            isEnemyGuest: isEnemyGuest,
          ),
        );
      },
    );
  }

  /// 显示房间密码弹框
  static showRoomPwdDialog(
    BuildContext context, {
    RoomPwdDialogCallback? callback,
  }) {
    if (!YBDLiveService.instance.dialogManager.isShowing(ROOM_PWD_DIALOG_ID)) {
      YBDLiveService.instance.dialogManager.add(ROOM_PWD_DIALOG_ID);
      YBDLiveService.instance.dialogManager.hide(context, TALENT_OFFLINE_DIALOG_ID);
      try {
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext builder) {
            return WillPopScope(
              onWillPop: () async => false,
              child: BlocProvider.value(
                value: BlocProvider.of<YBDRoomBloc>(context),
                child: YBDRoomPwdDialog(enterPwdCallback: callback),
              ),
            );
          },
        ).then((value) {
          YBDLiveService.instance.dialogManager.remove(ROOM_PWD_DIALOG_ID);
        });
      } catch (e) {
        YBDLiveService.instance.dialogManager.remove(ROOM_PWD_DIALOG_ID);
      }
    }
  }

  /// 禁言前先检查
  static Future<bool> checkForbiddenCondition(int? roomId, int? userId) async {
    YBDUserInfo? userInfo = await YBDUserUtil.userInfo();

    if (userId == roomId) {
      YBDToastUtil.toast(translate('not_allow_boot_talent'));
      return false;
    } else if (userId == userInfo!.id) {
      YBDToastUtil.toast(translate('not_allow_boot_self'));
      return false;
    } else if (roomId != userInfo.id) {
      YBDToastUtil.toast(translate('not_allow_action'));
      return false;
    } else {
      logger.v('check forbidden condition passed');
      return true;
    }
  }

  /// 踢人
  static bootUser(int? roomId, int? userId, int period) {
    YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_kick_by_guardians, target: userId.toString());
    YBDRoomSocketApi.getInstance().forbiddenUser(
      2,
      roomId,
      userId,
      period,
      onSuccess: (Response data) {
        _forbiddenUserSuccess(data);
      },
    );
  }

  /// 禁言或拉黑接口的返回成功的处理方法
  static _forbiddenUserSuccess(Response data) {
    if (data.code == '000000') {
      YBDToastUtil.toast(translate('success'));
    } else if (data.code == '900012') {
      // TODO: 弹框提示，跳转到购买页面
      YBDToastUtil.toast(translate('purchase_vip'));
    } else {
      if (null != data.result) {
        YBDToastUtil.toast(data.result);
      }
    }
  }

  /// 禁言
  static muteUser(int? roomId, int? userId, int period) {
    YBDRoomSocketApi.getInstance().forbiddenUser(
      1,
      roomId,
      userId,
      period,
      onSuccess: (Response data) {
        _forbiddenUserSuccess(data);
      },
    );
  }

  /// 拉黑
  static blockUser(int? roomId, int? userId) {
    YBDRoomSocketApi.getInstance().forbiddenUser(
      2,
      roomId,
      userId,
      -1,
      onSuccess: (Response data) {
        _forbiddenUserSuccess(data);
      },
    );
  }

  /// 设置管理员
  static addGuardian(int? roomId, int? userId, VoidCallback successCallback) {
    YBDRoomSocketApi.getInstance().roomManager(
      1,
      roomId,
      userId,
      onSuccess: (Response data) {
        if (data.code == '000000') {
          successCallback();
        } else {
          logger.v('promote room manager result : ${data.toJson()}');
          YBDToastUtil.toast('The number of guardian is full !');
        }
      },
    );
  }

  /// 取消管理员
  static cancelGuardian(int? roomId, int? userId, VoidCallback successCallback) {
    YBDRoomSocketApi.getInstance().roomManager(
      2,
      roomId,
      userId,
      onSuccess: (Response data) {
        if (null != successCallback) {
          successCallback();
        }
      },
    );
  }

  /// 根据[roomId]判断当前用户是否为主播
  static bool isTalent(int? roomId) {
    if (null == roomId || roomId <= 0) {
      logger.v('roomId is invalid: $roomId');
      return false;
    }

    int? currentId = YBDCommonUtil.storeFromContext()!.state.bean!.id;
    logger.v('roomId: $roomId, currentId: $currentId');
    return currentId == roomId;
  }

  static checkIsLudoRoom(BuildContext context, String roomId) async {
    print("checkkk ludo");
    try {
      final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
      List<String> ludoRooms = store.state.configs!.ludoTestRooms!.split(',');
      if (ludoRooms.contains(roomId)) {
        showLudoGame(context, ludoRooms.last);
        print("checkkk ludo ttt");
      }
      print("checkkk ludo fff");
      print(ludoRooms.toString());
    } catch (e) {
      print(e);
      print("checkkk ludo fff");
      return false;
    }
  }

  /// 获取房间麦位的数量
  /// [roomTag]房间类型
  /// [roomInfo]房间信息
  static int? getMicSize({
    required String? roomTag,
    required YBDRoomInfo? roomInfo,
  }) {
    int? curMicIndexSize;
    String? tag = roomInfo?.tag;
    int? category;

    try {
      if (tag != null) {
        ///新版本
        category = int.parse(tag);
      } else {
        ///兼容老版本
        List<String>? oldTitle = roomInfo?.title?.split(',');
        logger.v('_getMicSize category title  01:${roomInfo?.title}');
        if (oldTitle == null || oldTitle.isEmpty) {
          category = int.parse(roomTag ?? 0 as String);
        }
        if (oldTitle != null && oldTitle.length >= 2) {
          category = int.parse(oldTitle[0]);
          logger.v('_getMicSize category title 02:$category');
        }
      }
    } catch (e) {
      category = 0;
    }

    if (category == 0 || category == 1 || category == 4 || category == 2) {
      curMicIndexSize = 8;
    } else if (category == 3) {
      curMicIndexSize = 1;
    } else if (category == 5) {
      curMicIndexSize = 9;
    } else if (category == 6) {
      curMicIndexSize = 10;
    }

    logger.v('_getMicSize category:$category curMicIndexSize:$curMicIndexSize');
    return curMicIndexSize;
  }

  /// 进房间上麦
  static void takeAMic() {
    int id = YBDCommonUtil.storeFromContext()!.state.bean!.id ?? 0; // 获取用户ID
    if (YBDLiveService.instance.data != null && id != 0) {
      YBDLiveData data = YBDLiveService.instance.data;
      BuildContext ctx = data.roomContext!;
      var micBloc = ctx.read<YBDMicBloc>();
      YBDMicBlocState micState = micBloc.state; // 获取麦位信息
      List micList = micState.micBeanList!;
      int? curMicSize = YBDRoomUtil.getMicSize(
        roomTag: data.roomCategory,
        roomInfo: ctx.read<YBDRoomBloc>().state.roomInfo,
      ); // 当前房间类型最大麦位数
      List lockedMicList = micList.where((e) => e.lock).toList();
      logger.v('takeAMic---lockedMicList length:${lockedMicList.length}');

      if (micState.gotMic!) {
        logger.v('takeAMic---already on mic!');
        YBDToastUtil.toast('already on mic!'); // 该用户已经在麦位上
      } else if (micState.micIds!.length == curMicSize) {
        logger.v('takeAMic---All mics occupied!');
        YBDToastUtil.toast('All mics occupied!'); // 所有麦位都有人
      } else if (lockedMicList.length + micState.micIds!.length >= curMicSize!) {
        logger.v('takeAMic---No mic can use!');
        YBDToastUtil.toast('No mic can use!'); // 所有麦位都有人或被锁
      } else {
        for (int i = 0; i < curMicSize; i++) {
          // 遍历当前房间类型的所有麦位，找到第一个没有人且没有锁的麦位
          if ((micList[i]?.userId ?? -1) <= 0 && !(micList[i]?.lock ?? false)) {
            logger.v('takeAMic---roomId:${data.roomId},userId:$id,index:$i');
            micBloc.micOperation(data.roomId, 1, id, i);
            break; // 上麦
          }
        }
      }
    } else {
      logger.v('takeAMic fail---YBDLiveService.instance.data is null! & id:$id');
    }
  }

  /// 输入框底部间距
  static double bottomNavBarPadding({bool isKeyboardVisible = false}) {
    if (isKeyboardVisible) return 0.0;

    // ScreenUtil().setWidth(28)
    final value = ScreenUtil().bottomBarHeight > 0 ? ScreenUtil().bottomBarHeight : 0.0;
    logger.v('bottomNavBarPadding value: $value');
    return value;
  }

  /// 清理房间资源
  static cleanRoom(int? currentRoom) async {
    logger.v("cleanRoom resouce and endmatch to closeroom");

    // 退出房间停止匹配
    YBDRoomSocketApi().endMatch(
      roomId: currentRoom,
      onSuccess: (Response data) {},
    );

    // 清理送礼弹框的数据
    YBDRoomUtil.resetGiftPackageState();

    // 清理房间下载数据
    YBDDownLoadManager.instance!.clean();
    YBDDownLoadManager.instance!.removeCallBack(
      YBDDownLoadManager.instance!.callBackRoomEntrances,
    );

    // 清理teenpatti数据
    YBDTeenInfo.getInstance().clean();

    // 离开socket和声网频道
    await YBDLiveService.instance.leaveRoomV2(roomId: currentRoom);

    // 清理直播数据
    YBDLiveService.instance.cleanLive();

    // 关闭房间页面，还原状态
    YBDLiveService.instance.bootedLiveRoomLimit = false;
    YBDRoomHelper.closeRoom();
  }

  // 将ludo红点设置为已查看了
  static ludoRedDoted() {
    YBDSPUtil.save(Const.SP_KEY_LUDO_REDDOTED, true);
  }

  // 是否展示ludo红点提示
  static Widget ludoRedDot({bool space = false, bool isDot = false}) {
    return FutureBuilder<Widget>(
        future: YBDRoomUtil._ludoRedDotWidget(space: space, isDot: isDot),
        builder: (context, snapshot) {
          if (snapshot.data != null) {
            return snapshot.data!;
          }
          return Container();
        });
  }

  static Future<Widget> _ludoRedDotWidget({bool space = false, bool isDot = false}) async {
    var showed = await YBDSPUtil.get(Const.SP_KEY_LUDO_REDDOTED) ?? false;
    var shouldShowGame = await ConfigUtil.shouldShowGame(context: g.Get.context);
    // 显示过了 || 地址不存在 || 审核期间 ==> 都不给予展示
    if (showed || !YBDCommonUtil.shouldShowLudoGame() || !shouldShowGame) {
      return Container();
    }
    var dot = Container(
        alignment: Alignment.center,
        width: ScreenUtil().setWidth(29),
        height: ScreenUtil().setWidth(29),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: YBDHexColor('#E6497A'),
        ),
        child: Text('1',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w400, height: 1, fontSize: ScreenUtil().setSp(18))));
    if (isDot) {
      dot = Container(
          alignment: Alignment.center,
          width: ScreenUtil().setWidth(8),
          height: ScreenUtil().setWidth(8),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: YBDHexColor('#E6497A'),
          ));
    }
    if (space) {
      return Row(children: [dot, SizedBox(width: ScreenUtil().setWidth(3))]);
    }
    return dot;
  }
}
