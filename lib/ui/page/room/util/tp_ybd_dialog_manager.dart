import 'dart:async';


import 'package:flutter/cupertino.dart';

/// 密码输入框 ID
const ROOM_PWD_DIALOG_ID = 1000;

/// 主播离线对话框 ID
const TALENT_OFFLINE_DIALOG_ID = 1001;

/// loading 框 ID
const LOADING_DIALOG_ID = 1002;

/// 房间首充弹框 ID
const ROOM_TOP_UP_DIALOG_ID = 1003;

/// 被邀麦弹框 ID
const MIC_INVITED_DIALOG_ID = 1004;

/// 苹果支付弹框 ID
const APPLE_PAY_DIALOG_ID = 1005;

/// 房间升级弹框 ID
const ROOM_LEVEL_UP_DIALOG_ID = 1006;

/// PK匹配麦位提醒弹框
const PK_START_MIC_NOTICE = 1007;

/// PK开始动画弹框
const PK_START_ANIMATION = 1008;

/// PK邀请弹框
const PK_INVITE_DIALOG_ID = 1009;

/// PK邀请已接受弹框
const PK_INVITE_ACCEPT_DIALOG_ID = 1010;

/// 登出弹框弹框
const RE_LOGIN_DIALOG_ID = 1011;

/// TP余额不足弹框Insufficinet
const TP_INSUFFICINET_ID = 1012;

/// TP钻石换豆子
const TP_GEM2BEAN_ID = 1013;

///  对话框管理器
class YBDTPDialogManager {
  /// 对话框 id 列表
  List<int?> _dialogIdList = [];

  void removeAllDialogID() {
    _dialogIdList = [];
  }
  void removeAllDialogIDRlFMYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  int? remove(int? dialogId) {
    if (_dialogIdList.contains(dialogId)) {
      _dialogIdList.remove(dialogId);
      return dialogId;
    } else {
      return 0;
    }
  }

  void add(int? dialogId) {
    if (!_dialogIdList.contains(dialogId)) {
      _dialogIdList.add(dialogId);
    }
  }
  void addfclq7oyelive(int? dialogId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 弹框为显示状态
  bool isShowing(int? dialogId) {
    if (_dialogIdList.contains(dialogId)) {
      return true;
    } else {
      return false;
    }
  }
  void isShowingpMUZgoyelive(int? dialogId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 使用场景：弹出对话框之前先关闭前一个对话框
  void hide(BuildContext context, int dialogId) {
    if (_dialogIdList.contains(dialogId)) {
      Navigator.pop(context);
      _dialogIdList.remove(dialogId);
    }
  }
}
