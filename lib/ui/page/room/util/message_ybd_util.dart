import 'dart:async';


import 'package:flutter/cupertino.dart';
import '../../../../common/room_socket/message/common/internal.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/string_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';

/// 处理房间消息的工具类
class YBDMessageUtil {
  /// 禁言/踢人/拉黑操作时长
  static String _forbiddenTimeWithInternal(YBDInternal internal) {
    String time = '';
    switch (internal.record) {
      case -1:
        time = '';
        break;
      case 5:
        // TODO: 测试代码 弹框显示有错误日志
        // time = translate('for_five_min');
        time = 'for 5 minutes';
        break;
      case 60:
        // time = translate('for_one_h');
        time = 'for 1 hour';
        break;
      default:
        break;
    }

    return time;
  }

  /// 操作类型：禁言/踢人/拉黑
  static String _forbiddenActionWithInternal(YBDInternal internal) {
    String action = '';

    // 操作类型 1-禁言 2-踢人 3-黑名单 4-公告
    switch (internal.type) {
      case 1:
        // TODO: 测试代码 弹框显示有错误日志
        // action = translate('been_muted');
        action = 'You have been muted';
        break;
      case 2:
        if (internal.record == -1) {
          // action = translate('been_blocked');
          action = 'You have been blocked';
        } else {
          // action = translate('been_booted');
          action = 'You have been booted';
        }
        break;
      default:
        break;
    }

    return action;
  }

  /// 收信人名称
  /// type 0, 公聊区消息; 1, 弹框消息;
  static String _receiverName(
    YBDInternal internal,
    YBDUserInfo? mUserInfo, {
    int type = 0,
  }) {
    String name = '';

    if (type == 1 && internal.toUser == mUserInfo!.id) {
      name = '';
    } else {
      name = internal.receiver ?? '';
    }

    return name;
  }

  /// 发送人名称
  static String _senderName(YBDInternal internal) {
    if (YBDStringUtil.isBlank(internal.sender)) {
      return '';
    } else {
      return 'by ${internal.sender}';
    }
  }

  /// 组装 禁言/踢人/拉黑操作的消息
  /// type 0, 公聊区消息; 1, 弹框消息;
  static String forbiddenTextWithInternal(
    YBDInternal internal,
    YBDUserInfo? mUserInfo, {
    int type = 0,
  }) {
    String result = '';
    String action = _forbiddenActionWithInternal(internal);
    String time = _forbiddenTimeWithInternal(internal);
    String receiver = _receiverName(internal, mUserInfo, type: type);
    String sender = _senderName(internal);
    result = '$receiver $action $sender $time';
    return result;
  }

  /// 显示弹框
  static void showDialogWithInternal(
    BuildContext context,
    YBDInternal internal,
    YBDUserInfo? mUserInfo,
    int? roomId,
  ) {
    if (internal.type == 1 || internal.type == 2) {
      if (internal.toUser == mUserInfo!.id) {
        // 当前用户被屏蔽的提示弹框
        YBDDialogUtil.showBeenMutedDialog(context, internal);
      } else {
        // 屏蔽其他人不显示弹框
      }
    } else if (internal.type == 8) {
      if (roomId != mUserInfo!.id) {
        // 主播离线提示弹框
        YBDDialogUtil.showTalentOfflineDialog(context);
      }
    } else {
      // 其他 internal 消息不显示弹框
    }
  }

  /// 禁止用户的消息是否显示公聊区
  static bool shouldShowMsgWithInternal(YBDInternal internal) {
    if (null == internal.sender) {
      return false;
    } else {
      return true;
    }
  }

  /// 是否离开房间
  static bool shouldLeaveRoomWithInternal(YBDInternal internal, YBDUserInfo mUserInfo) {
    if (internal.toUser == mUserInfo.id && internal.type == 2) {
      return true;
    } else {
      return false;
    }
  }
}
