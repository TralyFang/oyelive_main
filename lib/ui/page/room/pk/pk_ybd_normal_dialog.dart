import 'dart:async';


/*
 * @Date: 2021-07-06 11:28:52
 * @LastEditors: William-Zhou
 * @LastEditTime: 2022-01-20 14:10:04
 * @FilePath: \oyetalk_flutter\lib\ui\page\room\widget\pk_exit_dialog.dart
 */
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

/// pk时禁止主播退出房间的弹框
/// 3种类型：1.PK时主播不能退出直播间 2.匹配10分钟后结束匹配 3.点击匹配按钮时没有上麦
enum PKNormalType { Exit, NoTalent, TakeMic }

class YBDPKNormalNotice extends StatefulWidget {
  final PKNormalType type;

  const YBDPKNormalNotice({Key? key, required this.type}) : super(key: key);

  @override
  _YBDPKNormalNoticeState createState() => _YBDPKNormalNoticeState();
}

class _YBDPKNormalNoticeState extends State<YBDPKNormalNotice> {
  // 倒计时
  int _time = 0;
  Timer? _timer;

  //* 还剩多少秒进入清算
  int _restDuration = 0;
  @override
  void initState() {
    super.initState();
    if (YBDLiveService.instance.data.pkInfo != null) {
      _restDuration =
          Duration(milliseconds: YBDLiveService.instance.data.pkInfo!.endTime! - DateTime.now().millisecondsSinceEpoch)
              .inSeconds;
      //* 修复主播在pk最后5秒内退出房间按钮弹出此弹框可能会出现的bug
      if (_restDuration< 6 && _restDuration>= 0) {
        _time = _restDuration;
      } else {
        _time = 5;
      }
    } else {
      _time = 5;
    }

    const repeatPeriod = const Duration(seconds: 1);
    _timer = Timer.periodic(repeatPeriod, (timer) {
      if (_time == 0) {
        _disposeTimer();
        return;
      }
      setState(() {
        _time--;
      });
    });
  }
  void initStateA6Qc7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _disposeTimer();
    super.dispose();
  }
  void dispose6B96yoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _disposeTimer() {
    if (_timer != null) {
      if (_timer!.isActive) {
        _timer!.cancel();
        _timer = null;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_time == 0 && widget.type == PKNormalType.Exit) {
      _disposeTimer();
      logger.v('Automatically close the bullet box');
      Navigator.pop(context);
    }
    return WillPopScope(
      onWillPop: () async {
        // 屏蔽物理返回键
        return Future.value(false);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    width: ScreenUtil().setWidth(500),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(ScreenUtil().setWidth(32)),
                        ),
                        color: Colors.white),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        // SizedBox(height: ScreenUtil().setWidth(10)),
                        // _closeBtn(),
                        SizedBox(height: ScreenUtil().setWidth(30)),
                        _pkLabel(),
                        SizedBox(height: ScreenUtil().setWidth(10)),
                        _content(widget.type),
                        SizedBox(height: ScreenUtil().setWidth(37)),
                        _bottomBtn(),
                        SizedBox(height: ScreenUtil().setWidth(20)),
                        widget.type == PKNormalType.Exit ? _countdown() : Container(),
                        SizedBox(height: ScreenUtil().setWidth(30)),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              top: ScreenUtil().screenHeight / 2 - ScreenUtil().setWidth(_closeBtnOffset(widget.type)),
              right: ScreenUtil().setWidth(110),
              child: _closeBtn(),
            )
          ],
        ),
      ),
    );
  }
  void buildABKewoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// pk标签
  Widget _pkLabel() {
    return Row(
      children: [
        SizedBox(width: ScreenUtil().setWidth(35)),
        Container(
          width: ScreenUtil().setWidth(400),
          margin: EdgeInsets.only(right: ScreenUtil().setWidth(20)),
          child: Image.asset('assets/images/liveroom/pk/pk_attention.webp', fit: BoxFit.cover),
        ),
      ],
    );
  }
  void _pkLabelW7i7doyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 内容
  Widget _content(PKNormalType type) {
    String _text;
    switch (type) {
      case PKNormalType.Exit:
        _text = translate('pk_exit_room_notice');
        break;
      case PKNormalType.NoTalent:
        _text = translate('pk_match_no_talent');
        break;
      case PKNormalType.TakeMic:
        _text = translate('pk_match_take_mic');
        break;
      default:
        _text = translate('pk_exit_room_notice');
    }
    return Container(
        margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(70)),
        child: Center(
          child: Text(
            _text,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(28),
              // fontWeight: FontWeight.w500,
              height: 1.5,
              color: Color.fromRGBO(0, 0, 0, 0.85),
            ),
          ),
        ));
  }
  void _content9ZIccoyelive(PKNormalType type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部按钮
  Widget _bottomBtn() {
    return YBDScaleAnimateButton(
      onTap: () {
        logger.v('clicked ok btn');
        _disposeTimer();
        Navigator.pop(context);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
          gradient: LinearGradient(
            colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        width: ScreenUtil().setWidth(300),
        height: ScreenUtil().setWidth(64),
        alignment: Alignment.center,
        child: Text(
          translate(widget.type == PKNormalType.TakeMic ? 'go' : 'ok'),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
  void _bottomBtnMCfJKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部倒计时
  Widget _countdown() {
    String noticeText = translate('pk_exit_room_accept');
    String timeText = '($_time)';
    return Container(
      width: ScreenUtil().setWidth(330),
      // margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(70)),
      child: Text(
        '$noticeText$timeText',
        style: TextStyle(
          height: 1.5,
          fontSize: ScreenUtil().setSp(24),
          color: Colors.black.withOpacity(0.50),
        ),
      ),
    );
  }

  /// 关闭按钮
  Widget _closeBtn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        GestureDetector(
          onTap: () {
            logger.v('clicked close btn');
            _disposeTimer();
            Navigator.pop(context);
          },
          child: Container(
            width: ScreenUtil().setWidth(58),
            // height: ScreenUtil().setWidth(58),
            child: Image.asset(
              'assets/images/dc/daily_check_close_btn.png',
              color: Color(0xff626262),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(width: ScreenUtil().setWidth(10)),
      ],
    );
  }
  void _closeBtnepaDOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关闭按钮偏移量
  int _closeBtnOffset(PKNormalType type) {
    switch (type) {
      case PKNormalType.Exit:
        return 267;
        break;
      case PKNormalType.NoTalent:
        return 252;
        break;
      case PKNormalType.TakeMic:
        return Platform.isIOS ? 240 : 205;
        break;
      default:
        return 267;
    }
  }
}
