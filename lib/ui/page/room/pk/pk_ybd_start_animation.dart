import 'dart:async';


/*
 * @Date: 2021-07-08 09:34:02
 * @LastEditors: William-Zhou
 * @LastEditTime: 2021-07-28 14:41:44
 * @FilePath: \oyetalk_flutter\lib\ui\page\room\widget\pk_start_animation.dart
 */
import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';

/// PK开始的动画
class YBDPKStartAnimation extends StatefulWidget {
  /// 主场头像
  final String homeImg;

  /// 主场昵称
  final String homeName;

  /// 客场头像
  final String awayImg;

  /// 客场昵称
  final String awayName;

  const YBDPKStartAnimation(
      {Key? key, required this.homeImg, required this.homeName, required this.awayImg, required this.awayName})
      : super(key: key);

  @override
  _YBDPKStartAnimationState createState() => _YBDPKStartAnimationState();
}

class _YBDPKStartAnimationState extends State<YBDPKStartAnimation> with TickerProviderStateMixin {
  late AnimationController _homePartCtr;
  late AnimationController _awayPartCtr;
  late SVGAAnimationController _animationController;
  bool _bgColorBlack = true;
  _play() async {
    _animationController = SVGAAnimationController(vsync: this);
    final videoItem = await SVGAParser.shared.decodeFromAssets('assets/animation/vs.svga');
    _animationController.videoItem = videoItem;
    this._animationController.repeat().whenComplete(() => this._animationController.videoItem = null);
  }

  @override
  void initState() {
    super.initState();
    _homePartCtr = AnimationController(vsync: this, duration: const Duration(milliseconds: 2000));
    _awayPartCtr = AnimationController(vsync: this, duration: const Duration(milliseconds: 2000));
    _homePartCtr.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        print('7.28----status == AnimationStatus.completed');
        Future.delayed(Duration(milliseconds: 1100), () {
          _bgColorBlack = false;
          setState(() {});
        });
      }
    });
    _homePartCtr.forward(from: 0.0);
    _awayPartCtr.forward(from: 0.0);
    _play();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      Future.delayed(Duration(seconds: 3), () {
        if (!mounted) return;
        _homePartCtr.reset();
        _awayPartCtr.reset();
        _animationController.stop();
        _animationController.videoItem = null;
        // Navigator.pop(context); //route.settings.name.contains('/flutter_room_page')
      });
    });
  }
  void initStateb24imoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    //* AnimationController.dispose() called more than once.
    _animationController.dispose();
    super.dispose();
  }
  void dispose03Dxroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return Future.value(true);
      },
      child: Material(
        color: _bgColorBlack ? Colors.black.withOpacity(0.7) : Colors.transparent,
        child: _bgColorBlack
            ? Container(
                width: ScreenUtil().screenWidth,
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: [
                    SVGAImage(_animationController),
                    Container(
                      width: double.infinity,
                      child: Column(
                        // mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: ScreenUtil().setWidth(250)),
                            child: BounceInLeft(
                              from: ScreenUtil().setWidth(300),
                              duration: Duration(milliseconds: 1000),
                              controller: (controller) => _homePartCtr = controller,
                              child: _homePart(),
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil().setWidth(400),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: ScreenUtil().setWidth(250)),
                            child: BounceInRight(
                              from: ScreenUtil().setWidth(300),
                              duration: Duration(milliseconds: 1000),
                              controller: (controller) => _awayPartCtr = controller,
                              child: _awayPart(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            : Container(width: 0),
      ),
    );
  }
  void buildSVfWcoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  TextStyle _nameStyle = TextStyle(
    color: Color(0xffFFFFFF),
    fontWeight: FontWeight.bold,
    fontSize: ScreenUtil().setSp(28),
  );

  //* 左上角部分
  Widget _homePart() {
    return Container(
      height: ScreenUtil().setWidth(300),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            bottom: ScreenUtil().setWidth(20),
            right: ScreenUtil().setWidth(0),
            child: Image.asset(
              'assets/images/liveroom/pk/pk_left_bottom.png',
              width: ScreenUtil().setWidth(500),
            ),
          ),
          Positioned(
            bottom: ScreenUtil().setWidth(40),
            left: ScreenUtil().setWidth(0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/liveroom/pk/pk_red_wing.png',
                  width: ScreenUtil().setWidth(400),
                  fit: BoxFit.cover,
                ),
                SizedBox(height: ScreenUtil().setWidth(50)),
                // Padding(
                //   padding: EdgeInsets.only(left: ScreenUtil().setWidth(8)),
                //   child: Text(widget.homeName ?? '111', style: _nameStyle),
                // )
              ],
            ),
          ),
          Positioned(
            bottom: ScreenUtil().setWidth(80),
            left: ScreenUtil().setWidth(140),
            child: Container(
              width: ScreenUtil().setWidth(127),
              height: ScreenUtil().setWidth(127),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(80))),
                color: Colors.white,
              ),
              child: Center(
                child: YBDRoundAvatar(
                  widget.homeImg,
                  avatarWidth: 124,
                  needNavigation: false,
                  showVip: false,
                ),
              ),
            ),
          ),
          //*名字
          Positioned(
              bottom: ScreenUtil().setWidth(27),
              left: ScreenUtil().setWidth(100),
              child: Container(
                  width: ScreenUtil().setWidth(200),
                  height: ScreenUtil().setWidth(50),
                  child: Center(
                      child: Text(widget.homeName,
                          maxLines: 1, overflow: TextOverflow.ellipsis, style: _nameStyle))))
        ],
      ),
    );
  }
  void _homePart6aBkNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //* 右下角部分
  Widget _awayPart() {
    return Container(
      height: ScreenUtil().setWidth(300),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            bottom: ScreenUtil().setWidth(20),
            left: ScreenUtil().setWidth(0),
            child: Image.asset(
              'assets/images/liveroom/pk/pk_right_bottom.png',
              width: ScreenUtil().setWidth(500),
              fit: BoxFit.fitWidth,
            ),
          ),
          Positioned(
            bottom: ScreenUtil().setWidth(40),
            right: ScreenUtil().setWidth(0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/liveroom/pk/pk_blue_wing.png',
                  width: ScreenUtil().setWidth(400),
                  fit: BoxFit.cover,
                ),
                SizedBox(height: ScreenUtil().setWidth(50)),
                // Text(widget.awayName, style: _nameStyle)
              ],
            ),
          ),
          Positioned(
            bottom: ScreenUtil().setWidth(80),
            right: ScreenUtil().setWidth(140),
            child: Container(
              width: ScreenUtil().setWidth(127),
              height: ScreenUtil().setWidth(127),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(80))),
                color: Colors.white,
              ),
              child: Center(
                child: YBDRoundAvatar(
                  widget.awayImg,
                  avatarWidth: 124,
                  needNavigation: false,
                  showVip: false,
                ),
              ),
            ),
          ),
          //*名字
          Positioned(
              bottom: ScreenUtil().setWidth(27),
              right: ScreenUtil().setWidth(105),
              child: Container(
                  width: ScreenUtil().setWidth(200),
                  height: ScreenUtil().setWidth(50),
                  child: Center(
                      child: Text(widget.awayName,
                          maxLines: 1, overflow: TextOverflow.ellipsis, style: _nameStyle))))
        ],
      ),
    );
  }
  void _awayPartz0G6Qoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
