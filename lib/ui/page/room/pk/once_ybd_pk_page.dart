import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/follow/entity/friend_ybd_list_entity.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/my_ybd_app_bar.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';

import 'item_ybd_search_pk.dart';

class YBDOncePkPage extends StatefulWidget {
  @override
  YBDOncePkPageState createState() => new YBDOncePkPageState();
}

class YBDOncePkPageState extends BaseState<YBDOncePkPage> {
  int page = 0, pageSize = 20;
  List<YBDRoomInfo?>? normalList;

  bool hasNext = false;
  getInitList() async {
    page = 0;
    _refreshController.resetNoData();
    //普通列表
    YBDFriendListEntity? normalListEntity = await ApiHelper.getTargetPkList(context, false, start: 0, offset: 20);
    if (_refreshController.isRefresh) {
      _refreshController.refreshCompleted();
    }
    if (normalListEntity != null && normalListEntity.returnCode == Const.HTTP_SUCCESS) {
      normalList = normalListEntity.record;
      hasNext = normalList!.length >= 20;
      if (!hasNext) _refreshController.loadNoData();
    } else {
      YBDToastUtil.toast(normalListEntity?.returnMsg ?? translate("failed"));
      normalList = [];
    }
    setState(() {});
  }

  nextPage() async {
    if (normalList != null && hasNext) {
      page++;
    }
    YBDFriendListEntity? normalListEntity =
        await ApiHelper.getTargetPkList(context, false, start: pageSize * page, offset: pageSize);
    _refreshController.loadComplete();
    if (normalListEntity != null && normalListEntity.returnCode == Const.HTTP_SUCCESS) {
      normalList!.addAll(normalListEntity.record!);
      hasNext = normalListEntity.record!.length >= 20;
      if (!hasNext) _refreshController.loadNoData();
    } else {
      YBDToastUtil.toast(normalListEntity?.returnMsg ?? translate("failed"));
    }

    setState(() {});
  }

  RefreshController _refreshController = RefreshController();

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return new Scaffold(
      appBar: YBDMyAppBar(
        title: Text(
          'Previous PK',
          style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(32)),
        ),
        backgroundColor: YBDTPStyle.heliotrope,
        leading: YBDNavBackButton(color: Colors.white),
        elevation: 0.0,
      ),
      body: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: normalList == null
            ? Center(child: YBDLoadingCircle())
            : SmartRefresher(
                controller: _refreshController,
                enablePullDown: true,
                header: YBDMyRefreshIndicator.myHeader,
                onRefresh: () {
                  getInitList();
                },
                enablePullUp: true,
                onLoading: nextPage,
                child: ListView.separated(
                  padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                  itemBuilder: (_, index) => YBDSearchPkItem(normalList![index]),
                  separatorBuilder: (_, index) => Container(
                    height: ScreenUtil().setWidth(1),
                    color: Colors.white.withOpacity(0.15),
                  ),
                  itemCount: normalList!.length,
                  shrinkWrap: true,
                ),
              ),
      ),
    );
  }
  void myBuildKhAGEoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getInitList();
  }
  void initState2JKRDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDOncePkPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetBA7NPoyelive(YBDOncePkPage oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
