import 'dart:async';


/*
 * @Date: 2021-07-05 16:19:32
 * @LastEditors: William-Zhou
 * @LastEditTime: 2022-01-13 16:16:17
 * @FilePath: \oyetalk_flutter\lib\ui\page\room\pk\pk_invite_dialog.dart
 */
import 'dart:async';
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_data.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_define.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';

/// pk邀请弹框
class YBDPKInviteDialog extends StatefulWidget {
  /// 点确认按钮的回调
  final VoidCallback? okCallback;

  /// 点关闭按钮的回调
  final VoidCallback? closeCallback;

  /// 背景图片
  final String? bgImg;

  /// ok图片
  final String? okImg;

  /// close图片
  final String? closeImg;

  /// 是否有倒计时
  final bool? haveCountDown;

  /// 倒计时
  final int? countNum;
  final YBDMicBloc micBloc;

  const YBDPKInviteDialog({
    Key? key,
    required this.okCallback,
    required this.closeCallback,
    required this.bgImg,
    required this.okImg,
    required this.closeImg,
    required this.haveCountDown,
    this.countNum,
    required this.micBloc,
  }) : super(key: key);

  @override
  _YBDPKInviteDialogState createState() => _YBDPKInviteDialogState();
}

class _YBDPKInviteDialogState extends State<YBDPKInviteDialog> {
  late int _time;
  Timer? _timer;
  bool _cancel = false;
  @override
  void initState() {
    super.initState();
    _time = widget.countNum ?? 0;
    const repeatPeriod = const Duration(seconds: 1);
    _timer = Timer.periodic(repeatPeriod, (timer) {
      setState(() {
        _time--;
      });
    });
  }
  void initStateptB7Poyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _disposeTimer();
    super.dispose();
  }
  void dispose8V0C6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _disposeTimer() {
    if (_timer != null) {
      if (_timer!.isActive) {
        _timer!.cancel();
        _timer = null;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_time == 0) {
      logger.v('Automatically close the bullet box');
      _disposeTimer();
      Navigator.pop(context);
      if (null != widget.okCallback && !_cancel) {
        widget.okCallback!();
      }
    }
    return WillPopScope(
      onWillPop: () async {
        return Future.value(false);
      },
      child: Material(
        color: Colors.black.withOpacity(0.4),
        child: Container(
          width: ScreenUtil().screenWidth,
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              _bgView(),
              Positioned(
                bottom: ScreenUtil().screenHeight / 2 - ScreenUtil().setWidth(270),
                child: _btnRow(),
              ),
              widget.haveCountDown!
                  ? Positioned(
                      bottom: ScreenUtil().screenHeight / 2 - ScreenUtil().setWidth(300),
                      child: _countdown(),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
  void buildAjwXNoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 背景图片
  Widget _bgView() {
    return GestureDetector(
      onTap: () {},
      child: Container(
        width: ScreenUtil().setWidth(700),
        child: Image.asset(widget.bgImg!, fit: BoxFit.cover),
      ),
    );
  }
  void _bgViewFsPBQoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 按钮组
  Widget _btnRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(width: ScreenUtil().setWidth(10)),
        _closeBtn(),
        SizedBox(width: ScreenUtil().setWidth(20)),
        _okBtn(),
      ],
    );
  }

  // 倒计时文字
  Widget _countdown() {
    String noticeText = translate('pk_invite_accept');
    String timeText = '($_time)';
    return Text(
      '$noticeText$timeText',
      style: TextStyle(
        height: 1.5,
        fontSize: ScreenUtil().setSp(24),
        color: Colors.white.withOpacity(0.50),
      ),
    );
  }
  void _countdown3ZgqToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // OK按钮
  Widget _okBtn() {
    return BlocBuilder<YBDMicBloc, YBDMicBlocState>(
      bloc: widget.micBloc,
      builder: (context, state) {
        return GestureDetector(
          onTap: () async {
            int? roomId = YBDLiveService.instance.data.roomId;
            _disposeTimer();
            logger.v('clicked ok btn');
            Navigator.pop(context);
            if (null != state.micBeanList && null != widget.okCallback && null != widget.closeCallback) {
              // 1. 如果主播在麦位上且未静音
              if (state.micBeanList!.any((e) => e!.userId == roomId && !e.mute!)) widget.okCallback!();

              // 2. 如果主播在麦位且静音提示Please unmute the mic to start!
              if (state.micBeanList!.any((e) => e!.userId == roomId && e.mute!)) {
                YBDToastUtil.toast(translate('pk_match_unmute_mic'));
                widget.closeCallback!();
              }

              // 3. 如果主播不在麦位上先让主播尝试上麦接受PK邀请
              if (!state.micBeanList!.any((e) => e!.userId == roomId)) {
                int id = YBDCommonUtil.storeFromContext()!.state.bean!.id ?? 0;
                if (YBDLiveService.instance.data != null && id != 0) {
                  YBDLiveData data = YBDLiveService.instance.data;
                  BuildContext ctx = data.roomContext!;
                  var micBloc = ctx.read<YBDMicBloc>();
                  YBDMicBlocState micState = micBloc.state;
                  List micList = micState.micBeanList!;
                  int curMicSize = YBDRoomUtil.getMicSize(
                    roomTag: data.roomCategory,
                    roomInfo: ctx.read<YBDRoomBloc>().state.roomInfo,
                  )!; // 当前房间类型最大麦位数
                  List lockedMicList = micList.where((e) => e.lock).toList();
                  logger.v('takeAMic---lockedMicList length:${lockedMicList.length}');

                  if (lockedMicList.length + micState.micIds!.length >= curMicSize) {
                    logger.v('takeAMic---No mic can use!');
                    YBDToastUtil.toast(translate('pk_invite_mic_full')); // 所有麦位都有人或被锁
                    widget.closeCallback!();
                  } else {
                    for (int i = 0; i < curMicSize; i++) {
                      // 遍历当前房间类型的所有麦位，找到第一个没有人且没有锁的麦位
                      if ((micList[i]?.userId ?? -1) <= 0 && !(micList[i]?.lock ?? false)) {
                        logger.v('takeAMic---roomId:${data.roomId},userId:$id,index:$i');
                        await micBloc.micOperation(data.roomId, 1, id, i); // 上麦
                        // 检查是否已经上麦
                        Future.delayed(Duration(milliseconds: 1000), () {
                          if (state.micBeanList!.any((e) => e!.userId == roomId)) {
                            logger.v('22.1.11---pk invite take mic success');
                            widget.okCallback!();
                          } else {
                            // 上麦时，声网处理失败
                            logger.v('22.1.11---pk invite take mic fail');
                            YBDToastUtil.toast(translate('pk_invite_net_error'));
                            widget.closeCallback!();
                          }
                        });
                        break;
                      }
                    }
                  }
                } else {
                  logger.v('takeAMic fail---YBDLiveService.instance.data is null! & id:$id');
                  widget.closeCallback!();
                }
              }
            }
          },
          child: Container(
            width: ScreenUtil().setWidth(300),
            child: Image.asset(widget.okImg!, fit: BoxFit.cover),
          ),
        );
      },
    );
  }
  void _okBtnGXpBXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // close按钮
  Widget _closeBtn() {
    return GestureDetector(
      onTap: () {
        _cancel = true;
        _disposeTimer();
        logger.v('clicked close btn');
        Navigator.pop(context);
        if (null != widget.closeCallback) {
          widget.closeCallback!();
        }
      },
      child: Container(
        width: ScreenUtil().setWidth(200),
        child: Image.asset(widget.closeImg!, fit: BoxFit.cover),
      ),
    );
  }
}
