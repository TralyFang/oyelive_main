import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:svgaplayer_flutter/player.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/ext/enum_ext/ui_ybd_ext.dart';
import 'package:oyelive_main/common/room_socket/message/common/PkPublish.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/ui/widget/intl_ybd_positioned.dart';

class YBDValueBar extends StatefulWidget {
  YBDPkPublish? data;

  YBDValueBar(this.data);

  @override
  YBDValueBarState createState() => new YBDValueBarState();
}

class YBDValueBarState extends BaseState<YBDValueBar> {
  final animateDuration = Duration(milliseconds: 300);
  int flex = 1;

  getPercentageWidth() {
    double percentage = 0.5;
    if (widget.data != null && widget.data?.content?.sourceGifts != widget.data?.content?.targetGifts) {
      int numerator = widget.data?.content?.sourceGifts ?? 0;
      int denominator = (widget.data?.content?.sourceGifts ?? 0) + (widget.data?.content?.targetGifts ?? 0);
      if (denominator != 0) percentage = numerator / denominator;
    }

    return 416 * percentage;
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: ScreenUtil().setWidth(616),
          height: ScreenUtil().setWidth(30),
          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
          child: Row(
            children: [
              Container(
                width: ScreenUtil().setWidth(100),
                height: ScreenUtil().setWidth(30),
                decoration: BoxDecoration(
                    borderRadius: YBDIntlBorderRadius.horizontal(left: Radius.circular(ScreenUtil().setWidth(16))),
                    color: Color(0xffFB6191)),
                alignment: IntlAlignment.centerLeft,
                child: Text.rich(
                  TextSpan(
                      style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(18), height: 1),
                      children: [
                        WidgetSpan(
                          child: SizedBox(
                            width: ScreenUtil().setWidth(10),
                          ),
                        ),
                        WidgetSpan(
                            child: Image.asset(
                              'assets/images/value_gift.webp',
                              width: ScreenUtil().setWidth(18),
                            ),
                            alignment: PlaceholderAlignment.middle),
                        WidgetSpan(
                            child: SizedBox(
                          width: ScreenUtil().setWidth(8),
                        )),
                        TextSpan(
                            text:
                                "${YBDNumericUtil.compactNumberWithFixedDigits(widget.data?.content?.sourceGifts?.toString()) ?? 0}")
                      ]),
                  maxLines: 1,
                ),
              ),
              AnimatedContainer(
                width: ScreenUtil().setWidth(getPercentageWidth()),
                duration: animateDuration,
                height: ScreenUtil().setWidth(30),
                color: Color(0xffFB6191),
              ),
              Expanded(
                child: AnimatedContainer(
                  duration: animateDuration,
                  height: ScreenUtil().setWidth(30),
                  color: Color(0xff70D5FD),
                ),
                flex: 1,
              ),
              Container(
                width: ScreenUtil().setWidth(100),
                height: ScreenUtil().setWidth(30),
                decoration: BoxDecoration(
                    borderRadius: YBDIntlBorderRadius.horizontal(right: Radius.circular(ScreenUtil().setWidth(16))),
                    color: Color(0xff70D5FD)),
                alignment: IntlAlignment.centerRight,
                child: Text.rich(
                  TextSpan(
                      style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(18), height: 1),
                      children: [
                        TextSpan(
                            text:
                                "${YBDNumericUtil.compactNumberWithFixedDigits(widget.data?.content?.targetGifts?.toString()) ?? 0}"),
                        WidgetSpan(
                            child: SizedBox(
                          width: ScreenUtil().setWidth(8),
                        )),
                        WidgetSpan(
                            child: Image.asset(
                              'assets/images/value_gift.webp',
                              width: ScreenUtil().setWidth(18),
                            ),
                            alignment: PlaceholderAlignment.middle),
                        WidgetSpan(
                          child: SizedBox(
                            width: ScreenUtil().setWidth(10),
                          ),
                        ),
                      ]),
                  maxLines: 1,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: ScreenUtil().setWidth(80),
          width: ScreenUtil().setWidth(456),
          child: Row(
            children: [
              AnimatedContainer(
                duration: animateDuration,
                width: ScreenUtil().setWidth(getPercentageWidth()),
              ),
              Container(
                  width: ScreenUtil().setWidth(40),
                  height: ScreenUtil().setWidth(80),
                  child: SVGASimpleImage(
                    assetsName: "assets/animation/glow.svga",
                  )),
              Spacer()
            ],
          ),
        )
      ],
    );
  }
  void myBuildLcwBhoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatemJXaUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDValueBar oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetNmr6joyelive(YBDValueBar oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
