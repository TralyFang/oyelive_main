import 'dart:async';


import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:svgaplayer_flutter/player.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/common/PkPublish.dart';
import 'package:oyelive_main/common/room_socket/message/resp/response.dart';
import 'package:oyelive_main/common/room_socket/message/room_ybd_message_helper.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_util.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/ui/page/room/bloc/pk_ybd_state_bloc.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc_state.dart';
import 'package:oyelive_main/ui/page/room/define/room_ybd_define.dart';
import 'package:oyelive_main/ui/page/room/entity/agora_ybd_rtm_info.dart';
import 'package:oyelive_main/ui/page/room/pk/counting_ybd_header.dart';
import 'package:oyelive_main/ui/page/room/pk/pk_ybd_mic.dart';
import 'package:oyelive_main/ui/page/room/pk/shinning_ybd_star.dart';
import 'package:oyelive_main/ui/page/room/pk/top_ybd_fans.dart';
import 'package:oyelive_main/ui/page/room/pk/value_ybd_bar.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_agora_rtm_service.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';
import 'package:oyelive_main/ui/widget/dialog/confirm_ybd_dialog.dart';

/// 等于0 只有重进房间 没有pk的时候
const SERVER_PK_SATUS_NONE = 0;
const SERVER_PK_SATUS_IN_PK = 1;
const SERVER_PK_SATUS_ENDED = 2;

/// 异常退出 老王说 不会推这个
const SERVER_PK_SATUS_ERROR = 3;

/// 提前结束(投降) bug:3192 新版本(2.4.12后)推此状态
const SERVER_PK_STATUS_SURRENDED = 5;

class YBDPkBoard extends StatefulWidget {
  @override
  YBDPkBoardState createState() => new YBDPkBoardState();
}

class YBDPkBoardState extends State<YBDPkBoard> {
  YBDPkPublish? data;

  @override
  Widget build(BuildContext context) {
    final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    bool _isStreamer = (YBDLiveService.instance.data.roomId ?? 0) == (store?.state.bean?.id ?? 0);
    return StreamBuilder<YBDMessage>(
        // initialData: YBDRoomSocketUtil.getInstance().roomSocketStatus,
        stream: YBDRoomMessageHelper.getInstance().messageOutStream,
        builder: (context, snapshot) {
          bool hasData = snapshot.hasData &&
              snapshot.data is YBDPkPublish &&
              (snapshot.data as YBDPkPublish?)?.content != null &&
              (snapshot.data as YBDPkPublish?)?.content?.status != SERVER_PK_SATUS_NONE;

          if (hasData) {
            print("pk hasData");
            data = snapshot.data as YBDPkPublish?;
          }

          // hasData = true;
          return Container(
            width: double.infinity,
            // height: ScreenUtil().setWidth(400),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  width: ScreenUtil().setWidth(680),
                  height: ScreenUtil().setWidth(384),
                  decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage('assets/images/liveroom/pk/pk_bg.webp'), fit: BoxFit.fill)),
                  child: data != null
                      // child: false
                      ? Stack(
                          children: [
                            //计时板
                            Positioned(
                                left: ScreenUtil().setWidth(230),
                                right: ScreenUtil().setWidth(230),
                                top: 0,
                                child: YBDCountingHeader(data!)),
                            //host
                            Positioned(
                                top: ScreenUtil().setWidth(50),
                                left: ScreenUtil().setWidth(60),
                                child: StreamBuilder<RoomSocketStatus>(
                                    initialData: YBDRoomSocketUtil.getInstance().roomSocketStatus,
                                    stream: YBDRoomSocketUtil.getInstance().socketStatusOutStream,
                                    builder: (context, snapshot) {
                                      print(data.toString());
                                      bool isOnline;
                                      if (BlocProvider.of<YBDRoomBloc>(context).roomId == store?.state.bean?.id) {
                                        //is user's own room
                                        isOnline = snapshot.data == RoomSocketStatus.Connected;
                                        if (!isOnline) {
                                          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                                            YBDEventName.OFFLINE_EVENT,
                                            location: YBDLocationName.ROOM_PAGE,
                                          ));
                                        }
                                      } else {
                                        isOnline = data?.content?.sourceOnline ?? true;
                                      }
                                      return GestureDetector(
                                        onTap: () {
                                          YBDRoomUtil.showMiniProfileDialog(
                                            roomId: context.read<YBDRoomBloc>().roomId,
                                            userId: data!.content!.sourceUser,
                                            clickEnemy: false,
                                            //TODO pk对方是否被禁止转发？？
                                          );
                                        },
                                        child: BlocBuilder<YBDRoomBloc, YBDRoomState>(
                                          buildWhen: (prev, current) => current.type == RoomStateType.TalentInfoUpdated,
                                          builder: (context, state) {
                                            return YBDPkMic(
                                              name: state.talentInfo?.nickname ?? "",
                                              isOnline: isOnline,
                                              avatarUrl: YBDImageUtil.avatar(
                                                  context, state.talentInfo?.headimg ?? "", state.talentInfo?.id ?? 0,
                                                  scene: 'A'),
                                            );
                                          },
                                        ),
                                      );
                                    })),
                            //enemy
                            Positioned(
                                top: ScreenUtil().setWidth(50),
                                right: ScreenUtil().setWidth(60),
                                child: GestureDetector(
                                    onTap: () {
                                      //TODO pk对方是否被禁止转发？？
                                      YBDRoomUtil.showMiniProfileDialog(
                                        roomId: context.read<YBDRoomBloc>().roomId,
                                        userId: data!.content!.targetUser,
                                        clickEnemy: true,
                                        isEnemyMuted:
                                            data!.content!.sourceBanMics?.contains(data!.content!.targetUser) ?? false,
                                      );
                                    },
                                    child: YBDPkMic(
                                      isOnline: data?.content?.targetOnline ?? true,
                                      avatarUrl: data?.content?.targetHeadImg ?? '',
                                      name: data?.content?.targetNickName ?? '',
                                    ))),
                            Positioned(
                              child: YBDValueBar(data),
                              bottom: ScreenUtil().setWidth(62),
                              left: ScreenUtil().setWidth(30),
                              right: ScreenUtil().setWidth(30),
                            ),
                            Positioned(
                              child: YBDTopFans(data!.content!.sourceTopGifts, homeAnchor: true),
                              bottom: ScreenUtil().setWidth(22),
                              left: ScreenUtil().setWidth(30),
                            ),
                            Positioned(
                              child: YBDTopFans(data!.content!.targetTopGifts, homeAnchor: false),
                              bottom: ScreenUtil().setWidth(22),
                              right: ScreenUtil().setWidth(30),
                            ),

                            Positioned(
                              child: YBDShinningStar(
                                12,
                                angle: 36,
                                delay: Duration(milliseconds: 138),
                              ),
                              top: ScreenUtil().setWidth(15),
                              left: ScreenUtil().setWidth(15),
                            ),

                            Positioned(
                              child: YBDShinningStar(
                                24,
                                angle: 30,
                                delay: Duration(milliseconds: 338),
                              ),
                              top: ScreenUtil().setWidth(2),
                              left: ScreenUtil().setWidth(110),
                            ),

                            Positioned(
                              child: YBDShinningStar(
                                16,
                                angle: 60,
                                delay: Duration(milliseconds: 638),
                              ),
                              top: ScreenUtil().setWidth(24),
                              left: ScreenUtil().setWidth(210),
                            ),

                            Positioned(
                              child: YBDShinningStar(
                                12,
                                angle: 8,
                                delay: Duration(milliseconds: 838),
                              ),
                              top: ScreenUtil().setWidth(42),
                              right: ScreenUtil().setWidth(160),
                            ),

                            Positioned(
                              child: YBDShinningStar(20, angle: 70),
                              top: ScreenUtil().setWidth(8),
                              right: ScreenUtil().setWidth(116),
                            ),

                            Positioned(
                              child: YBDShinningStar(
                                12,
                                angle: 19,
                                delay: Duration(milliseconds: 538),
                              ),
                              top: ScreenUtil().setWidth(20),
                              right: ScreenUtil().setWidth(14),
                            ),
                            // close pk btn
                            Positioned(
                              child: BlocBuilder<YBDPkStateBloc, PkState>(
                                  bloc: context.read<YBDPkStateBloc>(),
                                  builder: (_, PkState state) {
                                    String _key = (data?.content?.pkId ?? 0).toString();
                                    context.read<YBDPkStateBloc>().stream.listen((st) {
                                      if (_isStreamer && st == PkState.End) {
                                        logger.v('22.3.6----close pk surrend dialog key:$_key state:$st');
                                        YBDConfirmDialog.closeKey(key: _key);
                                      }
                                    });
                                    return _isStreamer
                                        ? GestureDetector(
                                            onTap: () {
                                              YBDConfirmDialog.show(this.context, title: '', sureTitle: translate('ok'),
                                                  content: translate('stop_pk_notice'), onSureCallback: () {
                                                print('click confirm surrender pk btn');
                                                YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                                                  YBDEventName.CLICK_EVENT,
                                                  location: YBDLocationName.ROOM_PAGE,
                                                  itemName: YBDItemName.SURRENDER_PK,
                                                ));
                                                YBDRoomSocketApi().surrenderPK(onSuccess: (Response data) {
                                                  if (data.code == Const.HTTP_SUCCESS_NEW) {
                                                    logger.v('surrender pk success');
                                                  } else if (data.code == Const.PK_SURRENDER_FAILD) {
                                                    logger.v('surrender pk failed! desc:${data.desc ?? '-1'}');
                                                    YBDToastUtil.toast(data.desc ?? translate('failed'));
                                                  } else {
                                                    logger.v('surrender pk error!code:${data.code ?? '-1'}');
                                                    YBDToastUtil.toast(translate('failed'));
                                                  }
                                                }, onTimeOut: () {
                                                  logger.v('surrender pk timeout!');
                                                });
                                              }, type: ConfirmDialogType.key, key: _key);
                                            },
                                            child: Image.asset(
                                              'assets/images/icon_close.png',
                                              width: ScreenUtil().setWidth(50),
                                              color: Colors.white,
                                            ),
                                          )
                                        : Container(width: 0);
                                  }),
                              top: ScreenUtil().setWidth(1),
                              right: ScreenUtil().setWidth(1),
                            )
                          ],
                        )
                      : Container(),
                ),
                BlocBuilder<YBDPkStateBloc, PkState>(
                    bloc: context.read<YBDPkStateBloc>(),
                    builder: (_, PkState state) {
                      if (state == PkState.CountDown)
                        return IgnorePointer(
                          child: Transform.scale(
                            scale: 1.23,
                            child: SizedBox(
                              height: ScreenUtil().setWidth(384),
                              child: SVGASimpleImage(
                                assetsName: "assets/animation/lightening.svga",
                              ),
                            ),
                          ),
                        );

                      return Container();
                    }),
                StreamBuilder(
                    initialData: YBDPkMediaMute(ChannelMediaRelayState.Running),
                    stream: YBDLiveAgoraRtmService.instance.onEvent,
                    builder: (context, snapshot) {
                      if (snapshot.hasData &&
                          snapshot.data is YBDPkMediaMute &&
                          (snapshot.data as YBDPkMediaMute).state == ChannelMediaRelayState.Connecting)
                        return Padding(
                          padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(80)),
                          child: SizedBox(
                            width: ScreenUtil().setWidth(270),
                            height: ScreenUtil().setWidth(50),
                            child: SVGASimpleImage(
                              assetsName: 'assets/animation/rtm_reconnecting.svga',
                            ),
                          ),
                        );
                      return Container();
                    })
              ],
            ),
          );
        });
  }
  void buildlzvNIoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    if (data == null) {
      data = YBDPkPublish()..content = YBDLiveService.instance.data.pkInfo;
    }
    super.initState();
  }
  void initStaterLVaFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDPkBoard oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetvY2Qyoyelive(YBDPkBoard oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
