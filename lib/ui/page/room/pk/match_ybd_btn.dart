import 'dart:async';


import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/room_socket/message/resp/response.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/bloc/pk_ybd_state_bloc.dart';
import 'package:oyelive_main/ui/page/room/entity/agora_ybd_rtm_info.dart';
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_bean.dart';
import 'package:oyelive_main/ui/page/room/ext/rtm_ybd_type_ext.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_agora_rtm_service.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/widget/glow_ybd_widget.dart';

import 'dialog_ybd_pk_opt.dart';

class YBDMatchButton extends StatefulWidget {
  @override
  YBDMatchButtonState createState() => new YBDMatchButtonState();
}

class YBDMatchButtonState extends BaseState<YBDMatchButton> {
  YBDAgoraRtmInfo? servicePkState;

  bool isPending = false;

  showOpt() {
    showDialog<Null>(
        context: context, //BuildContext对象
        barrierDismissible: false,
        builder: (BuildContext x) {
          return YBDPkOptDialog(
            micBloc: context.read<YBDMicBloc>(),
            onStart: (List<YBDMicBean> micBeanS) {
              isPending = true;
              setState(() {});
              YBDRoomSocketApi().startMatch(
                  roomId: YBDLiveService.instance.data.roomId,
                  onSuccess: (Response data) {
                    isPending = false;
                    setState(() {});
                    //开始匹配时如果 立马能匹配上 返回902004 如果当前没人 排队匹配的话返回000000
                    logger.v("start match with code: ${data.code}");
                    if (data.code == Const.HTTP_SUCCESS) {
                      YBDLiveService.instance.data.matchEndTime = json.decode(data.result)['endTime'] ~/ 1000;
                    }
                    if (data.code == Const.PK_MATCH_SUCCESS || data.code == Const.HTTP_SUCCESS) {
                      context.read<YBDPkStateBloc>().add(PkState.Matching);
                      for (YBDMicBean recipient in micBeanS) {
                        servicePkState = YBDAgoraRtmInfo(
                          RTMType.RTMTypeStartPKMicNotice,
                          pk_id: YBDLiveService.instance.data.roomId,
                          pk_state: 0,
                          pk_start_time: 0,
                          pk_end_time: 0,
                          roomId_initiator: YBDLiveService.instance.data.roomId,
                          roomId_recipient: recipient.userId,
                        );
                        if (YBDLiveService.instance.data.roomId != recipient.userId && recipient.userId != 0) {
                          print('9.22-----send.userId: ${recipient.userId}');
                          YBDLiveAgoraRtmService.instance.rtmSendChannelMessage(servicePkState);
                        }
                      }
                    } else {
                      YBDToastUtil.toast(translate('failed'));
                    }
                  },
                  onTimeOut: () {
                    isPending = false;
                    setState(() {});
                  });
            },
          );
        });
  }

  quitMatching() {
    isPending = true;
    setState(() {});
    YBDRoomSocketApi().endMatch(
        roomId: YBDLiveService.instance.data.roomId,
        onSuccess: (Response data) {
          isPending = false;
          setState(() {});
          if (data.code == Const.HTTP_SUCCESS) {
            context.read<YBDPkStateBloc>().add(PkState.None);
            setState(() {});
          } else {
            YBDToastUtil.toast(translate('failed'));
          }
        },
        onTimeOut: () {
          isPending = false;
          setState(() {});
        });
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild

    return BlocBuilder<YBDPkStateBloc, PkState>(
      bloc: context.read<YBDPkStateBloc>(),
      builder: (_, PkState state) {
        bool isInPk = state.index > 1;
        return YBDGlowWidget(
          shape: BoxShape.rectangle,
          showGlows: state == PkState.Matching,
          // showGlows: true,

          endRadius: ScreenUtil().setWidth(30),
          child: GestureDetector(
            onTap: () {
              if (isPending) {
                return;
              }
              switch (state) {
                case PkState.None:
                case PkState.End:
                  // TODO: Handle this case.
                  showOpt();
                  break;
                case PkState.Matching:
                  // TODO: Handle this case.
                  quitMatching();

                  break;
                case PkState.PKing:
                case PkState.Closing:
                case PkState.CountDown:

                  // TODO: Handle this case.
                  break;
              }
            },
            child: Stack(
              alignment: Alignment.center,
              children: [
                Image.asset(
                  isInPk ? 'assets/images/liveroom/icon_in_pk@2x.webp' : 'assets/images/liveroom/icon_pk@2x.webp',
                  height: ScreenUtil().setWidth(40),
                ),
                if (isPending)
                  Align(
                    child: SizedBox(
                        height: ScreenUtil().setWidth(22),
                        width: ScreenUtil().setWidth(33),
                        child: CircularProgressIndicator(
                          strokeWidth: ScreenUtil().setWidth(1.5),
                          backgroundColor: Colors.white,
                        )),
                  )
              ],
            ),
          ),
        );
      },
    );
  }
  void myBuildjiI2foyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatema9BOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDMatchButton oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgeti7wVJoyelive(YBDMatchButton oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
