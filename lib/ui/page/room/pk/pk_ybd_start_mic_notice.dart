import 'dart:async';


/*
 * @Date: 2021-07-06 15:03:52
 * @LastEditors: William-Zhou
 * @LastEditTime: 2021-09-18 11:40:00
 * @FilePath: \oyetalk_flutter\lib\ui\page\room\widget\pk_start_mic_notice.dart
 */
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

/// 房间开始pk时在麦位上的用户收到的提醒弹窗
class YBDPKStartMicNotice extends StatefulWidget {
  const YBDPKStartMicNotice({Key? key}) : super(key: key);

  @override
  _YBDPKStartMicNoticeState createState() => _YBDPKStartMicNoticeState();
}

class _YBDPKStartMicNoticeState extends State<YBDPKStartMicNotice> {
  // 倒计时5秒自动关闭弹框
  int _time = 5;
  Timer? _timer;
  @override
  void initState() {
    super.initState();
    const repeatPeriod = const Duration(seconds: 1);
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _timer = Timer.periodic(repeatPeriod, (timer) {
        if (_time == 0) {
          _disposeTimer();
          return;
        }
        setState(() {
          _time--;
        });
      });
    });
    Future.delayed(Duration(seconds: 5), () {
      _disposeTimer();
      Navigator.pop(context);
    });
  }
  void initState9MmfZoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _disposeTimer();
    super.dispose();
  }
  void disposeDl3dSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _disposeTimer() {
    if (_timer != null && _timer!.isActive) {
      _timer!.cancel();
      _timer = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_time == 0) {
      _disposeTimer();
      logger.v('Automatically close the pk start mic notice box');
      Navigator.pop(context);
    }
    return WillPopScope(
      onWillPop: () async {
        // 屏蔽物理返回键
        return Future.value(false);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    width: ScreenUtil().setWidth(500),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(ScreenUtil().setWidth(32)),
                        ),
                        color: Colors.white),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        // SizedBox(height: ScreenUtil().setWidth(10)),
                        // _closeBtn(),
                        SizedBox(height: ScreenUtil().setWidth(30)),
                        _pkLabel(),
                        // SizedBox(height: ScreenUtil().setWidth(5)),
                        _content(),
                        SizedBox(height: ScreenUtil().setWidth(20)),
                        _countdown(),
                        SizedBox(height: ScreenUtil().setWidth(30)),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              top: ScreenUtil().screenHeight / 2 - ScreenUtil().setWidth(320),
              right: ScreenUtil().setWidth(110),
              child: _closeBtn(),
            )
          ],
        ),
      ),
    );
  }
  void build8mzHEoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关闭按钮
  Widget _closeBtn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        GestureDetector(
          onTap: () {
            _disposeTimer();
            logger.v('clicked close btn');
            Navigator.pop(context);
          },
          child: Container(
            width: ScreenUtil().setWidth(58),
            // height: ScreenUtil().setWidth(58),
            child: Image.asset(
              'assets/images/dc/daily_check_close_btn.png',
              color: Color(0xff626262),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(width: ScreenUtil().setWidth(10)),
      ],
    );
  }
  void _closeBtnX4Ttioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// pk标签
  Widget _pkLabel() {
    return Row(
      children: [
        SizedBox(width: ScreenUtil().setWidth(35)),
        Container(
          width: ScreenUtil().setWidth(400),
          margin: EdgeInsets.only(right: ScreenUtil().setWidth(20)),
          child: Image.asset('assets/images/liveroom/pk/pk_attention.webp', fit: BoxFit.cover),
        ),
      ],
    );
  }
  void _pkLabel7WWOFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 内容
  Widget _content() {
    return Container(
      // margin: EdgeInsets.all(ScreenUtil().setWidth(30)),
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(50)),
      child: Center(
        child: Text(
          translate('pk_start_mic_notice'),
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            // fontWeight: FontWeight.w500,
            height: 1.5,
            color: Color.fromRGBO(0, 0, 0, 0.85),
          ),
        ),
      ),
    );
  }
  void _contentpfWKcoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部倒计时
  Widget _countdown() {
    String noticeText = translate('pk_start_mic_notice_close');
    String timeText = '($_time)';
    return Container(
      width: ScreenUtil().setWidth(400),
      // margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(70)),
      child: Text(
        '$noticeText$timeText',
        style: TextStyle(
          height: 1.5,
          fontSize: ScreenUtil().setSp(24),
          color: Colors.black.withOpacity(0.50),
        ),
      ),
    );
  }
}
