
import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/room_socket/message/common/PkPublish.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/bloc/pk_ybd_state_bloc.dart';
import 'package:oyelive_main/ui/page/room/pk/dialog_ybd_closing.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';

class YBDCountingHeader extends StatefulWidget {
  YBDPkPublish data;

  YBDCountingHeader(this.data);

  @override
  YBDCountingHeaderState createState() => new YBDCountingHeaderState();
}

class YBDCountingHeaderState extends BaseState<YBDCountingHeader> {
  Duration? restDuration;

  Timer? _countingTimer;
  bool isActive = true; // 应用在活动中

  setCountingTimer(int? endTime) {
    if (_countingTimer == null)
      _countingTimer = Timer.periodic(Duration(seconds: 1), (timer) {
        if (restDuration! <= Duration(seconds: 12)) {
          //ui适配所以才12秒
          context.read<YBDPkStateBloc>().add(PkState.CountDown);
        }
        if (restDuration! > Duration(seconds: 0)) {
          restDuration = Duration(milliseconds: endTime! - DateTime.now().millisecondsSinceEpoch);
          logger.v("showingRematingTime:${restDuration!.inSeconds}");
          setState(() {});
        } else {
          logger.v('pk counting end, isActive: $isActive');
          if (isActive) {
            // If in the background, the settlement pop-up window will not be prompted
            context.read<YBDPkStateBloc>().add(PkState.Closing);
            showDialog<Null>(
                context: context, //BuildContext对象
                barrierDismissible: false,
                barrierColor: Colors.transparent,
                builder: (BuildContext x) {
                  return YBDDialogClosing(context.read<YBDPkStateBloc>());
                });
          } else {
            // If in the background, the pk countdown is over, it will be restored
            if (YBDLiveService.instance.data.pkInfo?.pkId != null) {
              YBDLiveService.instance.data.pkIdEnded = YBDLiveService.instance.data.pkInfo?.pkId;
            }
            YBDLiveService.instance.data.pkInfo = null;
            context.read<YBDPkStateBloc>()..add(PkState.End);
            Future.delayed(Duration(milliseconds: 300), () {
              context.read<YBDPkStateBloc>()..add(PkState.None);
            });
          }

          timer.cancel();
        }
      });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        isActive = true;
        break;
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
        isActive = false;
        break;
    }
  }
  void didChangeAppLifecycleStatei1wJuoyelive(AppLifecycleState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  estimateRestTime() {
    if (restDuration != null) {
      return;
    }
    try {
      //本地时间和服务端截止时间计算的结果
      int result = widget.data.content!.endTime! - DateTime.now().millisecondsSinceEpoch;

      int diffBetweenSNC = (result - widget.data.content!.remainingTime!).abs(); //服务器与客户端计算的毫秒差

      if (widget.data != null) {
        //算出来结果小于0或者秒差大于15秒统一使用服务器数据
        bool isOffsetOutOutRange = result <= 0 || diffBetweenSNC > 15000;

        restDuration = Duration(milliseconds: isOffsetOutOutRange ? widget.data.content!.remainingTime! : result);

        setCountingTimer(isOffsetOutOutRange
            ? (widget.data.content!.remainingTime! + DateTime.now().millisecondsSinceEpoch)
            : widget.data.content!.endTime);
        logger.v(
            "result:$result,diffBetweenSNC$diffBetweenSNC,restDuration$restDuration,now:${DateTime.now().millisecondsSinceEpoch},\n"
            "content\n${widget.data.content!.toJson()}\n"
            "isOffsetOutOutRange:$isOffsetOutOutRange");
      }
    } catch (e) {
      print(e);
    }
  }

  format(Duration? d) => d.toString().split('.').first.padLeft(8, "0").substring(3);

  logInfo() {
    logger.v("the newxx::: ${Duration(minutes: 5, seconds: 3)}");

    logger.v("the new::: ${format(restDuration)}");
    logger.v(
        "the old::: ${DateFormat("mm:ss").format(DateTime.fromMillisecondsSinceEpoch(restDuration!.inMilliseconds))}");
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    estimateRestTime();

    var countingDown;
    logInfo();
    //大于11秒都是普通ui
    if (restDuration! > Duration(seconds: 11) || restDuration == Duration.zero) {
      countingDown =
          Text.rich(TextSpan(style: TextStyle(fontSize: ScreenUtil().setSp(22), color: Colors.white), children: [
        TextSpan(text: "${translate('match')} "),
        TextSpan(text: "${format(restDuration)}", style: TextStyle(color: Color(0xff40FFF6))),
      ]));
    } else {
      countingDown = Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        // TextSpan(
        //   text: "${translate('Countdown')} ",
        // ),
        Text(
          "${translate('countdown')} ",
          style: TextStyle(fontSize: ScreenUtil().setSp(22), color: Colors.white, height: 1),
        ),
        Text(
          "${restDuration!.inSeconds >= 0 ? restDuration!.inSeconds : 0}",
          style: TextStyle(color: Color(0xffF7D642), fontSize: ScreenUtil().setSp(50), fontFamily: 'baloo', height: 1),
        )
      ]);
    }

    return Container(
      width: ScreenUtil().setWidth(220),
      height: ScreenUtil().setWidth(36),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.black, borderRadius: BorderRadius.vertical(bottom: Radius.circular(ScreenUtil().setWidth(22)))),
      child: restDuration == null ? SizedBox() : countingDown,
    );
  }
  void myBuildqDfdLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    estimateRestTime();
    WidgetsBinding.instance?.addObserver(this);
  }
  void initStateHX0tqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    _countingTimer?.cancel();
    _countingTimer = null;
    restDuration = null;
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDCountingHeader oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgeteYlWaoyelive(YBDCountingHeader oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
