import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/bloc/pk_ybd_state_bloc.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class YBDDialogClosing extends StatefulWidget {
  YBDPkStateBloc bloc;

  YBDDialogClosing(this.bloc);

  @override
  YBDDialogClosingState createState() => new YBDDialogClosingState();
}

class YBDDialogClosingState extends BaseState<YBDDialogClosing> {

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Center(
      child: Container(
        decoration: BoxDecoration(
            color: Colors.black, borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8)))),
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(32), vertical: ScreenUtil().setWidth(18)),
        child: Text(
          translate('clearing'),
          style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Color(0xffD9D9D9)),
        ),
      ),
    );
  }
  void myBuildkqYpLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 30), () {
      logger.v('pk closing alert timeout end, mounted:$mounted');
      if (mounted) {
        //30s 无反应手动结束
        if (YBDLiveService.instance.data.pkInfo?.pkId != null) {
          YBDLiveService.instance.data.pkIdEnded = YBDLiveService.instance.data.pkInfo?.pkId;
        }
        YBDLiveService.instance.data.pkInfo = null;
        ///没收到结束报文时 还原pkboard UI
        widget.bloc.add(PkState.End);
        Future.delayed(Duration(milliseconds: 300), () {
          widget.bloc.add(PkState.None);
        });
        YBDAnalyticsUtil.logEvent(
            YBDAnalyticsEvent(YBDEventName.RESPONSE_EVENT, location: YBDLocationName.ROOM_PAGE, itemName: 'time_out'));
        // Navigator.pop(context);
      }
    });
  }
  void initStateLvtz3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  onAddSubscription() {
    addSub(widget.bloc.listen((PkState state) {
      if (state == PkState.End || state == PkState.None) {
        logger.v('pk closing alert pop $mounted');
        if(!mounted) return;
        logger.v('pk closing alert pop mounting');
        Navigator.popUntil(context, (route) {
          logger.v('pk closing alert popUntil ${route.settings.name}');
          if (route.isFirst) {
            return true; // 已经是最底层了，不能再往下了。
          }
          if (utilRoomPageRoute(route.settings.name ?? '')) {
            return true;
          }
          return false;
        });
        /* 目前仅限测试机opop，真我出现问题，改成上面直接回到直播间
        Navigator.pop(context);
         */
      }
    }));
  }

  // 当前路由是否是房间了
  bool utilRoomPageRoute(String route) {
    if (route == null || route.isEmpty) return false;
    List<String> roomPageRoutes = <String>[
      YBDNavigatorHelper.flutter_room_list_page,
      YBDNavigatorHelper.flutter_room_page,
      YBDNavigatorHelper.flutter_room_page_v1,
      YBDNavigatorHelper.flutter_room_list_page_v1];
    for (String roomRoute in roomPageRoutes) {
      if (route.startsWith(roomRoute)) {
        logger.v('pk closing alert popUntil $route, $roomRoute');
        return true;
      }
    }
    return false;
  }
  void utilRoomPageRouteL4694oyelive(String route) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDDialogClosing oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetAnRMXoyelive(YBDDialogClosing oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
