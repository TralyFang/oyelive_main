import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/room_socket/message/common/PkPublish.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/pk/pk_ybd_guest_sheet.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';

class YBDTopFans extends StatefulWidget {
  List<YBDPkInfoContentSourceTopGifts?>? data;
  bool? homeAnchor;

  YBDTopFans(this.data, {this.homeAnchor});

  @override
  YBDTopFansState createState() => new YBDTopFansState();
}

class YBDTopFansState extends BaseState<YBDTopFans> {
  final List<Color> tagColor = [
    Color(0xffEEBE5A),
    Color(0xffD2D2D2),
    Color(0xffBE987B),
  ];

  topFan(int index) {
    return Container(
      width: ScreenUtil().setWidth(40),
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(6)),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Container(
                width: ScreenUtil().setWidth(40),
                padding: EdgeInsets.all(ScreenUtil().setWidth(1)),
                decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                alignment: Alignment.center,
                child: YBDRoundAvatar(
                  YBDImageUtil.avatar(context, widget.data![index]!.user!.headimg, widget.data![index]!.user!.id),
                  avatarWidth: 38,
                  needNavigation: false,
                )),
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: ScreenUtil().setWidth(28),
              ),
              Container(
                width: ScreenUtil().setWidth(28),
                height: ScreenUtil().setWidth(16),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: tagColor[index], borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8)))),
                child: Text(
                  "${index + 1}",
                  style: TextStyle(fontSize: ScreenUtil().setSp(14), color: Colors.white),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    Widget result;
    if (widget.data == null || widget.data!.length == 0)
      result = Padding(
        child: Image.asset(
          "assets/images/chair.webp",
          width: ScreenUtil().setWidth(32),
        ),
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10), vertical: ScreenUtil().setWidth(8)),
      );
    else
      result = Row(
        children: List.generate(widget.data!.length > 3 ? 3 : widget.data!.length, (index) => topFan(index)),
      );

    return GestureDetector(
        onTap: () {
          // 弹出PK贵宾席排行榜用户
          // 贵宾席最多显示前10位赠礼最多的玩家
          logger.v('22.1.4------YBDPkInfoContentSourceTopGifts length:${widget.data!.length}');
          showModalBottomSheet(
            backgroundColor: Colors.transparent,
            context: context,
            isScrollControlled: true,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(16))),
            ),
            builder: (_) {
              return YBDPKGuestSheet(data: widget.data, homeAnchor: widget.homeAnchor);
            },
          );
        },
        child: result);
  }
  void myBuild3gzu3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatetugnyoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDTopFans oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetlw8Vtoyelive(YBDTopFans oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
