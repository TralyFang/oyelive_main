import 'dart:async';


import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/room_socket/message/common/PkPublish.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/page/room/bloc/combo_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/bloc/mini_ybd_profile_bloc.dart';
import 'package:oyelive_main/ui/page/room/bloc/pk_ybd_state_bloc.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/widget/mini_ybd_profile_dialog.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';
import 'package:animate_do/animate_do.dart';

enum PkResultType { PkResultTypeWin, PkResultTypeLose, PkResultTypeDraw }

extension PkResultTypeExt on PkResultType? {
  static String pk_profix = 'assets/images/pk/';
  String getPath() {
    String a = '';
    switch (this) {
      case PkResultType.PkResultTypeWin:
        a = 'win';
        break;
      case PkResultType.PkResultTypeLose:
        a = 'lose';
        break;
      case PkResultType.PkResultTypeDraw:
        a = 'draw';
        break;
    }
    var result = pk_profix + 'pk_$a.svga';
    print("will display pk result: $result");
    return result;
  }
  void getPathYUonZoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
  /// 获取网络地址
  String getURL() {
    String a = '';
    switch (this) {
      case PkResultType.PkResultTypeWin:
        a = 'win';
        break;
      case PkResultType.PkResultTypeLose:
        a = 'lose';
        break;
      case PkResultType.PkResultTypeDraw:
        a = 'draw';
        break;
    }
    var result = YBDCommonUtil.getRoomOperateInfo().pk_resource_profix + 'pk_$a.svga';
    print("will display pk result: $result");
    return result;
  }
  void getURLMpotWoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 提前下载所有pk资源包
  static predownloadPKSvgas() {
    final svgaTypes = [
      PkResultType.PkResultTypeWin,
      PkResultType.PkResultTypeLose,
      PkResultType.PkResultTypeDraw,
    ];
    svgaTypes.forEach((element) {
      YBDDownloadCacheManager.instance.downloadURL(element.getURL(), (path) { });
    });

  }

  // static String vsSvgaURL() {
  //   // vs
  //   String vs = YBDCommonUtil.getRoomOperateInfo().pk_resource_profix + 'vs.svga';
  //   return vs;
  // }
  // static String lighteningSvgaURL() {
  //   // 跑马灯
  //   String lightening = YBDCommonUtil.getRoomOperateInfo().pk_resource_profix + 'lightening.svga';
  //   return lightening;
  // }

}

class YBDPKAShow {
  static showOpt(
      BuildContext? context, int? sourceGifts, int? targetGifts, List<YBDPkInfoContentSourceTopGifts?>? sourceTopGifts,
      {YBDPkStateBloc? pkStateBloc, YBDPkPublish? data}) {
    PkResultType? type;
    if (!(Get.currentRoute.contains(YBDNavigatorHelper.flutter_room_list_page) ||
        Get.currentRoute.contains(YBDNavigatorHelper.flutter_room_page))) return;
    if (data!.content!.winner != null) {
      logger.v('22.3.4----winner:${data.content!.winner}');
      if (data.content!.winner == data.content!.sourceUser) type = PkResultType.PkResultTypeWin;
      if (data.content!.winner == data.content!.targetUser) type = PkResultType.PkResultTypeLose;
    } else {
      type = sourceGifts! > targetGifts!
          ? PkResultType.PkResultTypeWin
          : (sourceGifts < targetGifts ? PkResultType.PkResultTypeLose : PkResultType.PkResultTypeDraw);
    }

    // Avoid multiple bullet windows, close the problem of confusion, and use OverlayEntry instead
    Function? callback;
    OverlayEntry entry = OverlayEntry(
        builder: (context) => YBDPkResultAnimationView(
              type,
              pkStateBloc,
              context,
              function: callback,
              sourceTopGifts: sourceTopGifts,
            ));
    callback = () {
      logger.v('pk result dialog entry remove');
      entry.remove();
    };
    logger.v('pk result dialog showOpt Overlay $entry====${Overlay.of(YBDLiveService.instance.mainContext!)}');
    // Get.content initialized Overlay is empty
    Overlay.of(YBDLiveService.instance.mainContext!)!.insert(entry);
  }
}

class YBDPkResultAnimationView extends StatefulWidget {
  PkResultType? type;
  YBDPkStateBloc? stateBloc;
  BuildContext ctx;
  Function? function;
  List<YBDPkInfoContentSourceTopGifts?>? sourceTopGifts;
  YBDPkResultAnimationView(this.type, this.stateBloc, this.ctx, {this.function, this.sourceTopGifts});

  @override
  _YBDPkResultAnimationViewState createState() => _YBDPkResultAnimationViewState();
}

class _YBDPkResultAnimationViewState extends State<YBDPkResultAnimationView> with TickerProviderStateMixin {
  /// 动画控制器
  late SVGAAnimationController _animationController;
  OverlayEntry? miniProfile;
  int _second = 7;
  bool _avatarExist = true;
  bool _lose = false;

  playAnimation() async {
    _animationController = SVGAAnimationController(vsync: this);
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      if (mounted) {
        /// 使用本地下载的问题
        YBDDownloadCacheManager.instance.downloadURL(widget.type.getURL(), (path) {
          Future decode = File(path!).readAsBytes().then((bytes) {
            return SVGAParser.shared.decodeFromBuffer(bytes);
          });
          decode.then((videoItem) {
            _animationController.videoItem = videoItem;
            _animationController.repeat();
          });
        });
        /// v2.4.14之前使用的assets文件
        // var videoItem = await SVGAParser.shared.decodeFromAssets(widget.type.getPath());
        // _animationController.videoItem = videoItem;
        // _animationController.repeat(); //.whenComplete(() => _animationController.stop());
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    playAnimation();
    Future.delayed(Duration(seconds: _second), () {
      logger.v('pk result dialog end, mounted:$mounted, context:${widget.ctx}');
      if (mounted) {
        _animationController.videoItem = null;
        _animationController.stop();
      }
      if (this.widget.function != null) {
        this.widget.function!();
      }
      // Navigator.pop(widget.ctx);
      widget.stateBloc?.add(PkState.None);

      // Navigator.popUntil(
      //     context,
      //     (route) =>
      //         route.settings.name.contains("flutter_room_page") ||
      //         route.settings.name.contains("flutter_room_list_page"));
    });
    _lose = widget.type == PkResultType.PkResultTypeLose;
    if (_lose)
      Future.delayed(Duration(milliseconds: 6200), () {
        setState(() {
          _avatarExist = false;
        });
      });
  }
  void initStatevfkzAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: double.infinity,
            child: SVGAImage(_animationController),
          ),
          Padding(
              padding: EdgeInsets.only(
                top: ScreenUtil().setWidth(_lose ? 371 : 280),
                right: ScreenUtil().setWidth(_lose ? 1 : 0),
              ),
              child: _avatarExist
                  ? FadeIn(
                      delay: Duration(milliseconds: 1700), // 1750
                      duration: Duration(milliseconds: 500),
                      child: _buildAllAvatars(context, widget.sourceTopGifts!),
                    )
                  : SizedBox()),
        ],
      ),
    );
  }
  void buildItsvvoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _miniAvatar({BuildContext? context, String? img, int? userId, int width = 58}) {
    return GestureDetector(
      onTap: () {
        _setEntry(roomId: YBDLiveService.instance.data.roomId, userId: userId);
        Overlay.of(context!)!.insert(miniProfile!);
      },
      child: YBDRoundAvatar(img, avatarWidth: width, needNavigation: false, showVip: false),
    );
  }
  void _miniAvatarWOkzUoyelive({BuildContext? context, String? img, int? userId, int width = 58}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _setEntry({int? roomId, int? userId}) {
    BuildContext? ctx = YBDLiveService.instance.data.roomContext;
    miniProfile = OverlayEntry(
        builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider.value(value: BlocProvider.of<YBDRoomBloc>(ctx!)),
                BlocProvider.value(value: BlocProvider.of<YBDMicBloc>(ctx)),
                BlocProvider.value(value: BlocProvider.of<YBDRoomComboBloc>(ctx)),
                BlocProvider<YBDMiniProfileBloc>(
                  create: (_) {
                    return YBDMiniProfileBloc(ctx, userId, roomId);
                  },
                ),
              ],
              child: YBDMiniProfileDialog(
                userId,
                roomId,
                closeCallback: () {
                  try {
                    miniProfile?.remove();
                  } catch (e) {
                    logger.v(' miniProfile?.remove error: $e');
                  }
                },
                isClickOnPkEnemy: true,
                isEnemyGuest: true,
              ),
            ));
  }
  void _setEntryEGTasoyelive({int? roomId, int? userId}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _buildAllAvatars(BuildContext context, List<YBDPkInfoContentSourceTopGifts?> info) {
    if (info.length > 3) info = info.sublist(0, 3);
    return _rowAvatar(info.length, context, info);
  }

  /// contributors num type = [0,1,2,3]
  Widget _rowAvatar(int contributors, BuildContext context, List<YBDPkInfoContentSourceTopGifts?> info) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        contributors < 2
            ? _emptyAvatar()
            : _miniAvatar(context: context, userId: info[1]!.user!.id, img: info[1]!.user!.headimg),
        SizedBox(width: ScreenUtil().setWidth(87)),
        Padding(
          padding: EdgeInsets.only(top: ScreenUtil().setWidth(110)),
          child: contributors < 1
              ? _emptyAvatar()
              : _miniAvatar(context: context, userId: info.first!.user!.id, img: info.first!.user!.headimg, width: 80),
        ),
        SizedBox(width: ScreenUtil().setWidth(_lose ? 85 : 85)),
        contributors < 3
            ? _emptyAvatar()
            : _miniAvatar(context: context, userId: info.last!.user!.id, img: info.last!.user!.headimg),
      ],
    );
  }
  void _rowAvatarKOoBaoyelive(int contributors, BuildContext context, List<YBDPkInfoContentSourceTopGifts?> info) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _emptyAvatar() {
    return Container(width: ScreenUtil().setWidth(58), height: ScreenUtil().setWidth(58));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }
}
