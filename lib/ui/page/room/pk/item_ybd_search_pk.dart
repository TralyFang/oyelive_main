import 'dart:async';


import 'dart:developer';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/module/room/room_ybd_api_helper.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/widget/level_ybd_tag.dart';
import 'package:oyelive_main/ui/widget/room_ybd_category_widget.dart';
import 'package:oyelive_main/ui/widget/room_ybd_level_tag.dart';

import 'invite_ybd_cooldown_util.dart';

enum BtnState { Enabled, Counting, Disabled }

class YBDSearchPkItem extends StatefulWidget {
  YBDRoomInfo? roomInfo;

  YBDSearchPkItem(this.roomInfo);

  @override
  YBDSearchPkItemState createState() => new YBDSearchPkItemState();
}

class YBDSearchPkItemState extends BaseState<YBDSearchPkItem> {
  getButton() {
    return StreamBuilder<List<YBDInviteInfo>>(
        stream: YBDInviteCoolDownUtil.getInstance()!.inviteCoolDownOutStream,
        initialData: YBDInviteCoolDownUtil.getInstance()!.getCurrentList(),
        builder: (context, snapshot) {
          List<YBDInviteInfo> info = snapshot.data!;
          List<YBDInviteInfo> result = info.where((element) => element.userId == widget.roomInfo!.id).toList();
          BtnState state;
          if (result != null && result.length != 0) {
            //有相同的
            state = BtnState.Counting;
          } else {
            state = widget.roomInfo!.live! && !widget.roomInfo!.pk! ? BtnState.Enabled : BtnState.Disabled;
          }

          Widget btn;

          switch (state) {
            case BtnState.Enabled:
              // TODO: Handle this case.
              btn = GestureDetector(
                onTap: () {
                  YBDRoomSocketApi.getInstance().targetPkOperation(widget.roomInfo!.id, 1, onSuccess: (data) {
                    if (data.code == Const.HTTP_SUCCESS) {
                      YBDInviteCoolDownUtil.getInstance()!.click(YBDInviteInfo(widget.roomInfo!.id));
                    } else {
                      YBDToastUtil.toast(data.desc);
                    }
                  });
                },
                child: Container(
                  width: ScreenUtil().setWidth(130),
                  height: ScreenUtil().setWidth(48),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(1)),
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40))),
                    gradient: LinearGradient(
                      colors: [
                        Color(0xff47CDCC),
                        Color(0xff5E94E7),
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  child: Center(
                    child: Image.asset(
                      "assets/images/liveroom/pk/pk_t.webp",
                      width: ScreenUtil().setWidth(50),
                    ),
                  ),
                ),
              );
              break;
            case BtnState.Counting:
              // TODO: Handle this case.

              int remainTime = 60 - DateTime.now().difference(result[0].clickTime).inSeconds;
              btn = Container(
                width: ScreenUtil().setWidth(130),
                height: ScreenUtil().setWidth(48),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white.withOpacity(0.85), width: ScreenUtil().setWidth(1)),
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40))),
                  gradient: LinearGradient(
                    colors: [
                      Color(0xff47CDCC),
                      Color(0xff5E94E7),
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                ),
                child: Center(
                  child: Text(
                    "${remainTime}s",
                    style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white.withOpacity(0.85)),
                  ),
                ),
              );
              break;
            case BtnState.Disabled:
              // TODO: Handle this case.
              btn = Container(
                width: ScreenUtil().setWidth(130),
                height: ScreenUtil().setWidth(48),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40))),
                    color: Colors.white.withOpacity(0.5)),
                child: Center(
                  child: Image.asset(
                    "assets/images/liveroom/pk/pk_t.webp",
                    width: ScreenUtil().setWidth(50),
                  ),
                ),
              );
              break;
            default:
              btn = Container();
          }
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              btn,
              if (state != BtnState.Disabled)
                Padding(
                  padding: EdgeInsets.only(top: ScreenUtil().setWidth(20)),
                  child: Row(
                    children: <Widget>[
                      Image.asset(
                        // 音乐动图
                        'assets/images/icon_live.webp',
                        width: ScreenUtil().setWidth(36),
                        height: ScreenUtil().setWidth(33),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: ScreenUtil().setWidth(8)),
                        child: Text(
                          widget.roomInfo!.count?.toString() ?? '-',
                          // 可以限制最大宽度
                          // constraints: BoxConstraints(
                          // maxHeight: maxWidth: ScreenUtil().setWidth(450),
                          style: TextStyle(
                            color: Colors.white.withOpacity(0.7),
                            fontSize: ScreenUtil().setSp(24),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
            ],
          );
        });
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return GestureDetector(
      onTap: () {
        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.user_profile + "/${widget.roomInfo!.id}");
      },
      child: Container(
        decoration: BoxDecoration(),
        height: ScreenUtil().setWidth(200),
        child: Center(
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(18))),
                child: YBDNetworkImage(
                  imageUrl: YBDImageUtil.cover(
                        context,
                        widget.roomInfo!.roomimg,
                        widget.roomInfo!.id,
                        scene: 'C',
                      ),
                  width: ScreenUtil().setWidth(160),
                  height: ScreenUtil().setWidth(160),
                  fit: BoxFit.cover,
                  errorWidget: (context, url, d) => Container(
                      // 房间图像占位图
                      width: ScreenUtil().setWidth(160),
                      height: ScreenUtil().setWidth(160),
                      color: YBDActivitySkinRoot().curAct().exploreRecordBgColor(),
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(50), vertical: ScreenUtil().setWidth(49)),
                      child: Image.asset('assets/images/rabbit_head.png')),
                  placeholder: (context, url) => Container(
                      // 房间图像占位图
                      width: ScreenUtil().setWidth(160),
                      height: ScreenUtil().setWidth(160),
                      color: YBDActivitySkinRoot().curAct().exploreRecordBgColor(),
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(50), vertical: ScreenUtil().setWidth(49)),
                      child: Image.asset('assets/images/rabbit_head.png')),
                ),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(26),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(230)),
                      child: Text(
                        widget.roomInfo!.roomTitle ?? "",
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: ScreenUtil().setSp(28),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: ScreenUtil().setWidth(22),
                    ),
                    YBDRoomCategoryWidget(widget.roomInfo!.roomCategory),
                    SizedBox(
                      height: ScreenUtil().setWidth(28),
                    ),
                    Row(
                      children: [
                        Container(
                          constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(230)),
                          child: Text(
                            widget.roomInfo!.nickname ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: Colors.white.withOpacity(0.7),
                              fontSize: ScreenUtil().setSp(24),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(10),
                        ),
                        YBDRoomLevelTag(widget.roomInfo!.roomlevel)
                      ],
                    ),
                  ],
                ),
              ),
              getButton(),
            ],
          ),
        ),
      ),
    );
  }
  void myBuild6snhSoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStateavI6Foyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDSearchPkItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetSkIKWoyelive(YBDSearchPkItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
