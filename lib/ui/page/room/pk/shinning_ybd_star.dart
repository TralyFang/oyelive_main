import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'dart:math' as math;

class YBDShinningStar extends StatefulWidget {
  int size;
  double angle;
  Duration? delay;
  YBDShinningStar(this.size, {this.angle: 0, this.delay});

  @override
  YBDShinningStarState createState() => new YBDShinningStarState();
}

class YBDShinningStarState extends BaseState<YBDShinningStar> with SingleTickerProviderStateMixin {
  late Animation shinningAnimation;
  late AnimationController shinningController;
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return AnimatedBuilder(
      animation: shinningAnimation,
      builder: (c, x) {
        return Container(
          width: ScreenUtil().setWidth(widget.size * 1.2),
          height: ScreenUtil().setWidth(widget.size * 1.2),
          alignment: Alignment.center,
          child: Transform.scale(
            scale: shinningAnimation.value,
            child: Transform.rotate(
              angle: math.pi / widget.angle,
              child: Icon(
                Icons.star,
                color: Color(0xffEBD3AD),
                size: ScreenUtil().setWidth(widget.size),
              ),
            ),
          ),
        );
      },
    );
  }
  void myBuildwgJMYoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    shinningController = AnimationController(vsync: this, duration: Duration(milliseconds: 1000));
    shinningAnimation = Tween<double>(begin: 0, end: 1).animate(shinningController);
    shinningController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        shinningController.reverse();
      }

      if (status == AnimationStatus.dismissed) {
        shinningController.forward();
      }
    });
    if (widget.delay != null) {
      Future.delayed(widget.delay!, () {
        shinningController.forward();
      });
    } else {
      shinningController.forward();
    }
  }
  void initStateJ3uddoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    shinningController.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDShinningStar oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetIsdUpoyelive(YBDShinningStar oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
