import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/follow/entity/friend_ybd_list_entity.dart';
import 'package:oyelive_main/ui/page/room/pk/item_ybd_search_pk.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';

import 'invite_ybd_cooldown_util.dart';

class YBDSearchPkPage extends StatefulWidget {
  @override
  YBDSearchPkPageState createState() => new YBDSearchPkPageState();
}

class YBDSearchPkPageState extends BaseState<YBDSearchPkPage> {
  TextEditingController _textEditingController = new TextEditingController();

  bool isLoading = false;

  title(String title, {String? more}) {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setWidth(20)),
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff63FFDF), fontWeight: FontWeight.bold),
          ),
          Spacer(),
          if (more != null)
            GestureDetector(
              onTap: () {
                YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.once_pk);
              },
              child: Text(
                more,
                style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white.withOpacity(0.8)),
              ),
            ),
        ],
      ),
    );
  }

  YBDRoomInfo? result;

  emptyView() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            "assets/images/empty/no_data.webp",
            width: ScreenUtil().setWidth(288),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(30),
          ),
          Text(
            "No data",
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }

  resultList() {
    return YBDSearchPkItem(result);
    // return ListView.separated(
    //   padding: EdgeInsets.all(0),
    //   itemBuilder: (_, index) => YBDSearchPkItem(list[index]),
    //   separatorBuilder: (_, index) => Container(
    //     height: ScreenUtil().setWidth(1),
    //     color: Colors.white.withOpacity(0.15),
    //   ),
    //   itemCount: list.length,
    //   shrinkWrap: true,
    //   physics: NeverScrollableScrollPhysics(),
    // );
  }

  queryRoomInfo() async {
    if (_textEditingController.text.isEmpty) {
      setState(() {});
      return;
    }
    isLoading = true;
    setState(() {});
    YBDRoomInfo? roomInfo = await ApiHelper.queryOnlineInfo(context, int.tryParse(_textEditingController.text));
    isLoading = false;
    if (roomInfo == null) {
      result = null;
      YBDToastUtil.toast(translate('no_result'));
    } else {
      result = roomInfo;
    }
    setState(() {});
  }

  List<YBDRoomInfo?>? friendList, normalList;

  getInitList() async {
    page = 0;
    _refreshController.resetNoData();
    //朋友列表
    isLoading = true;
    setState(() {});
    YBDFriendListEntity? friendListEntity = await ApiHelper.getTargetPkList(context, true, start: 0, offset: pageSize);
    if (friendListEntity != null && friendListEntity.returnCode == Const.HTTP_SUCCESS) {
      friendList = friendListEntity.record;
      hasNext = friendList!.length >= pageSize;
      if (!hasNext) _refreshController.loadNoData();
    } else {
      YBDToastUtil.toast(friendListEntity?.returnMsg ?? translate("failed"));
      friendList = [];
    }

    //普通列表
    YBDFriendListEntity? normalListEntity = await ApiHelper.getTargetPkList(context, false, start: 0, offset: 5);
    if (normalListEntity != null && normalListEntity.returnCode == Const.HTTP_SUCCESS) {
      normalList = normalListEntity.record;
    } else {
      YBDToastUtil.toast(normalListEntity?.returnMsg ?? translate("failed"));
      normalList = [];
    }
    isLoading = false;
    setState(() {});
  }

  RefreshController _refreshController = RefreshController();

  int page = 0, pageSize = 10;
  bool hasNext = false;

  nextPage() async {
    if (friendList != null && hasNext) {
      page++;
    }
    YBDFriendListEntity? friendListEntity =
        await ApiHelper.getTargetPkList(context, true, start: pageSize * page, offset: pageSize);

    _refreshController.loadComplete();
    if (friendListEntity != null && friendListEntity.returnCode == Const.HTTP_SUCCESS) {
      friendList!.addAll(friendListEntity.record!);
      hasNext = friendListEntity.record!.length >= pageSize;
      if (!hasNext) _refreshController.loadNoData();
    } else {
      YBDToastUtil.toast(friendListEntity?.returnMsg ?? translate("failed"));
    }

    setState(() {});
  }

  getContent(bool isSearch) {
    print("xxx${isSearch} ${normalList!.length} ${friendList!.length}");
    return ((isSearch && result == null) || (!isSearch && normalList!.length == 0 && friendList!.length == 0))
        ? emptyView()
        : SmartRefresher(
            controller: _refreshController,
            enablePullDown: false,
            header: YBDMyRefreshIndicator.myHeader,
            enablePullUp: true,
            onLoading: nextPage,
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                child: Column(
                  children: [
                    if (isSearch) resultList(),
                    if (!isSearch && normalList != null && normalList!.isNotEmpty) ...[
                      title("Previous PK", more: "See more >"),
                      ListView.separated(
                        padding: EdgeInsets.all(0),
                        itemBuilder: (_, index) => YBDSearchPkItem(normalList![index]),
                        separatorBuilder: (_, index) => Container(
                          height: ScreenUtil().setWidth(1),
                          color: Colors.white.withOpacity(0.15),
                        ),
                        itemCount: normalList!.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                      ),
                      Container(
                        height: ScreenUtil().setWidth(1),
                        color: Colors.white.withOpacity(0.15),
                      )
                    ],
                    if (!isSearch && friendList != null && friendList!.isNotEmpty) ...[
                      title(
                        "My friends",
                      ),
                      ListView.separated(
                        padding: EdgeInsets.all(0),
                        itemBuilder: (_, index) => YBDSearchPkItem(friendList![index]),
                        separatorBuilder: (_, index) => Container(
                          height: ScreenUtil().setWidth(1),
                          color: Colors.white.withOpacity(0.15),
                        ),
                        itemCount: friendList!.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                      )
                    ],
                  ],
                ),
              ),
            ),
          );
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    bool isSearch = _textEditingController.text.isNotEmpty;
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: [
            Container(
              height: ScreenUtil().setWidth(93),
              child: Row(
                children: [
                  SizedBox(
                    width: ScreenUtil().setWidth(10),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(88),
                    child: TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                      style: TextButton.styleFrom(padding: EdgeInsets.all(ScreenUtil().setWidth(20))),
                      // padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
                    ),
                  ),
                  Expanded(
                    child: Container(),
                    flex: 1,
                  ),
                  Container(
                    width: ScreenUtil().setWidth(520),
                    height: ScreenUtil().setWidth(76),
                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
                    decoration: BoxDecoration(
                        color: Color(0x32ffffff),
                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(38)))),
                    child: Row(
                      children: [
                        Expanded(
                          child: TextField(
                            style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                            textInputAction: TextInputAction.search,
                            controller: _textEditingController,
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                            // Only numbers can be entered

                            onSubmitted: (text) {
                              queryRoomInfo();
                            },
                            decoration: InputDecoration(
                                isDense: true,
                                // contentPadding: EdgeInsets.only(bottom: ScreenUtil().setWidth(8)),
                                border: InputBorder.none,
                                hintText: "User ID",
                                counterText: '',
                                hintStyle: TextStyle(color: Color(0x88ffffff))),
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(),
                    flex: 4,
                  ),
                  GestureDetector(
                    onTap: () {
                      queryRoomInfo();
                    },
                    child: Image.asset(
                      "assets/images/icon_search.png",
                      width: ScreenUtil().setWidth(40),
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(26),
                  )
                ],
              ),
            ),
            Expanded(
              child: isLoading ? YBDLoadingCircle() : getContent(isSearch),
            )
          ],
        ),
      ),
    );
  }
  void myBuild29oFyoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    YBDInviteCoolDownUtil.getInstance()!.init();
    getInitList();
  }
  void initStatepPmnooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    YBDInviteCoolDownUtil.getInstance()!.destroy();
    super.dispose();
  }
  void disposecaxPhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDSearchPkPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesDs1jwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
