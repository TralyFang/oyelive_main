import 'dart:async';


import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/room_socket/message/common/PkPublish.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/result_ybd_bean.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart' as user;
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/ui/page/room/entity/agora_ybd_rtm_info.dart';
import 'package:oyelive_main/ui/page/room/ext/rtm_ybd_type_ext.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_agora_rtm_service.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/service/pk_ybd_prohibit_service.dart';

import '../room_ybd_helper.dart';
import 'pk_ybd_board.dart';

class YBDPKRTMHelper {
  ///服务端下发pk状态
  static YBDAgoraRtmInfo? servicePkState;

  ///Rtm状态
  static YBDAgoraRtmInfo? rtmState;

  static int? user_id;

  //处理RTM 点对点消息
  static reciveRTMMsg(YBDAgoraRtmInfo agoraRtmInfo) {
    switch (agoraRtmInfo.rtmType) {
      case RTMType.RTMTypeUnknow:
        logger.v('rtm message type is unknow');
        break;
      case RTMType.RTMTypePauseMuteB:
        //禁言对手主播
        //有没有必要 进行id 校验
        YBDPkProhibitService.pauseAllChannelMediaRelay();
        logger.v('recive rtm message type is RTMTypePauseMuteB');
        // YBDToastUtil.toast('自己被禁言了');
        break;
      case RTMType.RTMTypeResumeMuteB:
        //恢复对手主播流转发
        //恢复对手主播流转发
        logger.v('recive rtm message type is RTMTypeResumeMuteB');
        YBDPkProhibitService.resumeAllChannelMediaRelay();
        // YBDToastUtil.toast('自己被恢复禁言');
        break;
    }
  }

  //A主播发送 禁言/恢复 B主播转发消息  默认禁言
  static sendRTMMsgMuteB(int? to, {RTMType type = RTMType.RTMTypePauseMuteB}) {
    logger.v('send rtm message userId is $to');
    if (to == null) return;
    final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: Get.context)!;
    logger.v('send rtm message type is $type');
    if (type == RTMType.RTMTypePauseMuteB) {
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.CLICK_EVENT,
        location: YBDLocationName.ROOM_PAGE,
        itemName: YBDItemName.PK_MUTE_TALENT,
      ));
    }
    YBDLiveAgoraRtmService.instance.rtmSendPeerMessage(
        to.toString(), YBDAgoraRtmInfo(type, roomId_initiator: store.state.bean!.id ?? -1, roomId_recipient: to));
  }

  ///处理 跨通道 音频 转发结果
  static void eventStateHandle(ChannelMediaRelayState state, ChannelMediaRelayError code) {
    if (code == ChannelMediaRelayError.None) {
      switch (state) {
        case ChannelMediaRelayState.Idle:
          // pk 结束 或者 退出房间 触发 stopChannelMediaRelay 失败
          logger.v('start channel media stop success');
          break;
        case ChannelMediaRelayState.Running: //startChannelMediaRelay 成功
          logger.v('start channel media relay success');
          YBDLiveAgoraRtmService.instance.sink.add(YBDPkMediaMute(ChannelMediaRelayState.Running));
          break;
        case ChannelMediaRelayState.Failure: //startChannelMediaRelay 失败
          logger.v('start channel media relay failure');
          //失败状态不需要处理
          // YBDLiveAgoraRtmService.instance.sink.add(YBDPkMediaMute(ChannelMediaRelayState.Failure));
          YBDPKRTMHelper.circulate();
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.ROOM_PAGE,
            itemName: YBDItemName.PK_CHANNEL_MEDIA_RELAY_FAIL,
          ));
          break;
      }
    } else if (code == ChannelMediaRelayError.ServerNoResponse || code == ChannelMediaRelayError.ServerConnectionLost) {
      // pk 结束 或者 退出房间 触发 stopChannelMediaRelay 失败
      logger.v('start channel media stop failure');
      // if (YBDLiveService.instance.getEngine() == null)return;
      // YBDLiveService.instance.getEngine()?.stopChannelMediaRelay();
    } else {
      logger.v('start channel media error $code');
    }
  }

  /*
  * 转发流失败的情况 轮询去start pk结束/退出房间会停止
  */
  static circulate() async {
    //start 想再次被调用 需要先stop 停止当前转发状态
    if (YBDLiveService.instance.getEngine() == null) return;
    await YBDLiveService.instance.getEngine()?.stopChannelMediaRelay();
    Future.delayed(Duration(seconds: 2), () {
      YBDPkProhibitService.startChannelMediaRelay();
    });
  }

  /*
   * 处理 禁对方麦 -- 流转发的 结果 成功后上传服务端同步状态
   * 服务端状态同步失败 就以客户端为主 不进一步处理了
  */
  static void eventHandle(ChannelMediaRelayEvent e) {
    switch (e) {
      case ChannelMediaRelayEvent.PauseSendPacketToDestChannelSuccess: //暂停成功
        logger.v('channel media relay event pause success');
        // YBDToastUtil.toast('停止成功');
        //通知服务端去更新状态  index  -1 没有意义  注意sourceId 和targetId
        //已和产品确认 这里不关心服务端 是否记录成功 以本地状态为主 如果杀死进程重新进来 拉取的状态不对 让主播自己操作
        YBDRoomSocketApi.getInstance().micMuteOperation(
            YBDLiveService.instance.data.pkInfo!.sourceUser, 1, YBDLiveService.instance.data.pkInfo!.targetUser, -1, 3, -1);
        break;
      case ChannelMediaRelayEvent.PauseSendPacketToDestChannelFailed: //暂停失败
        logger.v('channel media relay event pause failure');
        YBDToastUtil.toast('YBDOperation Failed');
        //加个埋点 看看出现的概率大不大
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.ROOM_PAGE,
          itemName: YBDItemName.PK_MUTE_TALENT_FAIL,
        ));
        break;
      case ChannelMediaRelayEvent.ResumeSendPacketToDestChannelSuccess: //恢复成功
        logger.v('channel media relay event resume success');
        //通知服务端去更新状态
        //已和产品确认 这里不关心服务端 是否记录成功 以本地状态为主 如果杀死进程重新进来 拉取的状态不对 让主播自己操作
        YBDRoomSocketApi.getInstance().micMuteOperation(
            YBDLiveService.instance.data.pkInfo!.sourceUser, 2, YBDLiveService.instance.data.pkInfo!.targetUser, -1, 3, -1);
        // YBDToastUtil.toast('恢复成功');
        break;
      case ChannelMediaRelayEvent.ResumeSendPacketToDestChannelFailed: //恢复失败
        logger.v('channel media relay event resume failure');
        YBDToastUtil.toast('YBDOperation Failed');
        break;
    }
  }

  static Future<RTMType> getPkPublishType(YBDPkPublish pkPublish) async {
    if (pkPublish.content?.status == 1) {
      return RTMType.RTMTypePauseMuteL;
    }

    if (pkPublish.content?.status == 0 || pkPublish.content?.status == 2) {
      return RTMType.RTMTypeResumeMuteL;
    }
    return RTMType.RTMTypeUnknow;
  }

  ///pk状态
  static updateServicePkState(YBDPkPublish pkPublish) async {
    servicePkState = YBDAgoraRtmInfo(
      await getPkPublishType(pkPublish),
      pk_id: pkPublish.content?.pkId,
      pk_state: pkPublish.content?.status,
      pk_start_time: pkPublish.content?.startTime,
      pk_end_time: pkPublish.content?.endTime,
      roomId_initiator: pkPublish.content?.sourceRoom,
      roomId_recipient: pkPublish.content?.targetRoom,
    );
    if (user_id == null) {
      user_id = await YBDUserUtil.userIdInt();
    }
    logger.v('updateServicePkState servicePkState:${servicePkState!.pk_state} user_id:$user_id');

    ///用户是两个主播之一，需要发rtm静音信息到频道
    if (isPkTalent(user_id) && servicePkState?.pk_id != null) {
      logger.v('updateServicePkState rtmSendChannelMessage:${isPkTalent(user_id)} pk_id:${servicePkState?.pk_id}');
      YBDLiveAgoraRtmService.instance.rtmSendChannelMessage(servicePkState);
      return;
    }

    ///不是主播 并且pk_id != null且 pk_state = 1就是观众在pk房，都需要静音
    if (servicePkState?.pk_id != null && servicePkState!.pk_state == 1) {
      logger.v('updateServicePkState muteLocalAudioStream(true) pk_id:${servicePkState?.pk_id}');
      YBDLiveService.instance.muteLocalAudioStream(true);
    } else {
      YBDLiveService.instance.muteLocalAudioStream(false);
    }
  }

  ///pking 下发channel 消息
  ///Rtm 状态
  static updateRtmState(YBDAgoraRtmInfo agoraRtmInfo, bool micOnline) async {
    rtmState = agoraRtmInfo;
    if (user_id == null) {
      user_id = await YBDUserUtil.userIdInt();
    }
    logger.v(
        'updateRtmState rtmState?.pk_state:${rtmState?.pk_state} servicePkState?.pk_state:${servicePkState?.pk_state}');
    logger.v('updateRtmState isPkTalent:${isPkTalent(user_id)} micOnline：$micOnline');

    ///主播收到不处理
    if (isPkTalent(user_id)) return;

    ///rtm显示pk中
    if (rtmState?.pk_state == SERVER_PK_SATUS_IN_PK) {
      logger.v('updateRtmState 001:${rtmState?.pk_state}');

      /// 静音
      YBDLiveService.instance.muteLocalAudioStream(true);

      /// TODO test
      // rtmState?.pk_state = 2;
      logger.v('updateRtmState 002: rtmState:${rtmState?.pk_state} servicePkState:${servicePkState?.pk_state}');

      ///rtm和服务端状态不一致，12秒后再次比较 并且是在麦位上用户
      if (servicePkState?.pk_state != rtmState?.pk_state && micOnline) {
        delayCompare();
      }
      return;
    }

    ///rtm显示pk结束
    ///pk 中禁掉麦位上 观众的麦   pk结束恢复麦位
    if (rtmState?.pk_state == SERVER_PK_SATUS_ENDED || rtmState?.pk_state == SERVER_PK_STATUS_SURRENDED) {
      ///rtm和服务端状态不一致，12秒后再次比较 并且是在麦位上用户
      if (servicePkState?.pk_state == SERVER_PK_SATUS_IN_PK && micOnline) {
        logger.v('updateRtmState 002:${servicePkState?.pk_state}');
        YBDLiveService.instance.muteLocalAudioStream(true);
        delayCompare();
        return;
      }
      YBDLiveService.instance.muteLocalAudioStream(false);
      return;
    }
  }

  ///服务器状态与rtm状态比较
  static delayCompare() {
    logger.v('delayCompare()');
    Future.delayed(Duration(seconds: 12), () {
      logger.v('delayCompare rtmState:${rtmState?.pk_state} ,servicePkState${servicePkState?.pk_state}');

      ///rtm与service不一样，报firebase
      ///针对麦克用户 操作
      ///过滤掉 网络延迟
      ///数据写入定位到 广播信息错乱时
      if (rtmState?.pk_state != servicePkState?.pk_state &&
          servicePkState != null &&
          servicePkState?.pk_id != null &&
          servicePkState?.roomId_initiator != null) {
        ///TODO firebase
        YBDCommonUtil.storeRtmState(
            user_id,
            servicePkState?.pk_id,
            servicePkState?.roomId_initiator,
            servicePkState?.roomId_recipient,
            servicePkState?.pk_start_time,
            servicePkState?.pk_end_time,
            rtmState?.pk_state,
            servicePkState?.pk_state);
        return;
      }

      ///rtm与service一样，并且是pk中，静音
      if (servicePkState?.pk_state == SERVER_PK_SATUS_IN_PK) {
        YBDLiveService.instance.muteLocalAudioStream(true);
        return;
      }

      ///其它
      YBDLiveService.instance.muteLocalAudioStream(false);
    });
  }

  ///判断是否是pk主播
  static bool isPkTalent(int? userId) {
    if (userId == null) {
      return false;
    }
    if ((servicePkState?.roomId_recipient == userId) || servicePkState?.roomId_initiator == userId) {
      return true;
    }
    return false;
  }

  /// 检查用户是否是PK房的主播
  static Future<void> isInPk(BuildContext context) async {
    YBDResultBeanEntity? resultBeanEntity = await ApiHelper.getPkState(context);
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      if (resultBeanEntity!.record == true) {
        user.YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
        if (userInfo == null) {
          return;
        }
        YBDRoomHelper.enterRoom(
          context,
          userInfo.id,
          forceEnter: true,
          location: YBDLocationName.INDEX_PK_PAGE,
        );
      }
    }
    /* YBDUserInfo userInfo = await YBDSPUtil.getUserInfo();
    YBDRoomHelper.enterRoom(
      context,
      userInfo.id,
      forceEnter: true,
      location: YBDLocationName.INDEX_PAGE,
    );*/
  }
}
