import 'dart:async';


/*
 * @Author: William-Zhou
 * @Date: 2022-01-04 10:09:42
 * @LastEditTime: 2022-01-20 17:59:42
 * @LastEditors: William-Zhou
 * @Description: pk guest sheet and Show up to the top 10 players with the most gifts
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/room_socket/message/common/PkPublish.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';
import 'package:oyelive_main/ui/widget/colored_ybd_safe_area.dart';
import 'package:oyelive_main/ui/widget/level_ybd_tag.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';

class YBDPKGuestSheet extends StatefulWidget {
  final List<YBDPkInfoContentSourceTopGifts?>? data;
  final bool? homeAnchor;
  const YBDPKGuestSheet({Key? key, this.data, this.homeAnchor}) : super(key: key);

  @override
  _YBDPKGuestSheetState createState() => _YBDPKGuestSheetState();
}

class _YBDPKGuestSheetState extends State<YBDPKGuestSheet> {
  @override
  Widget build(BuildContext context) {
    return YBDColoredSafeArea(
      child: Container(
        height: ScreenUtil().screenHeight * 0.57,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(top: Radius.circular(px(16))), color: Color(0xb2000000)),
          child: Column(
            children: [
              Container(
                  width: ScreenUtil().screenWidth,
                  height: px(90),
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(px(25), px(30), 0, px(20)),
                    child: Text(translate('pk_contribution_list'), style: _28style),
                  )),
              _splitLine(),
              Expanded(child: SingleChildScrollView(physics: BouncingScrollPhysics(), child: _pkGuest()))
            ],
          ),
        ),
      ),
    );
  }
  void buildalePUoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _pkGuest() {
    bool isAnchor = YBDUserUtil.getLoggedUserID(context) == YBDLiveService.instance.data.roomId;
    List<YBDPkInfoContentSourceTopGifts?>? myData = widget.data;
    if (widget.data == null || widget.data!.length == 0) {
      return Column(
        children: [
          YBDTPGlobal.hSizedBox(300),
          Image.asset('assets/images/icon_trophy.png', width: px(100)),
          YBDTPGlobal.hSizedBox(35),
          Text(translate('pk_contribution_list_empty'), style: _28style),
          if (widget.homeAnchor! && !isAnchor) ...[YBDTPGlobal.hSizedBox(102), _giftBtn()]
        ],
      );
    }
    if (widget.data!.length > 10) myData = widget.data!.sublist(0, 10);
    switch (myData!.length) {
      case 1:
        return _topItem(1, myData.first!);
        break;
      case 2:
        return Column(
          children: [_topItem(1, myData.first!), _topItem(2, myData.last!)],
        );
        break;
      case 3:
        return Column(
          children: [_topItem(1, myData.first!), _topItem(2, myData[1]!), _topItem(3, myData.last!)],
        );
        break;
      default:
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _topItem(1, myData.first!),
            _topItem(2, myData[1]!),
            _topItem(3, myData[2]!),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: myData.length - 3,
              itemBuilder: (_, index) {
                return _pkGuestItem(
                  index: index + 4,
                  user: myData![index + 3]!.user!,
                  consume: myData[index + 3]!.consume,
                );
              },
            ),
          ],
        );
        break;
    }
  }
  void _pkGuesteOkDwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _topItem(int index, YBDPkInfoContentSourceTopGifts data) {
    return _pkGuestItem(
      index: index,
      user: data.user!,
      consume: data.consume,
      badge: Image.asset('assets/images/liveroom/pk/pk_guest_top$index.png', width: px(70)),
    );
  }
  void _topItemzzNpHoyelive(int index, YBDPkInfoContentSourceTopGifts data) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _pkGuestItem({int? index, required YBDPkInfoContentSourceTopGiftsUser user, int? consume, Widget? badge}) {
    return Container(
      constraints: BoxConstraints(minHeight: px(90)),
      height: px(90),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              logger
                  .v('22.1.5---showMiniProfileDialog---roomId:${YBDLiveService.instance.data.roomId}---userId:${user.id}');
              // 根据homeAnchor展示不同的miniprofile
              // 22.1.20修改：两边展示相同操作的miniprofile
              Navigator.pop(context);
              YBDRoomUtil.showMiniProfileDialog(
                roomId: YBDLiveService.instance.data.roomId,
                userId: user.id,
                clickEnemy: true,
                isEnemyGuest: true,
              );
            },
            child: Row(
              children: [
                YBDTPGlobal.wSizedBox(23),
                Container(width: px(30), child: Center(child: Text('$index', style: _24style))),
                YBDTPGlobal.wSizedBox(20),
                _avatar(id: user.id, img: user.headimg, badge: badge),
                _level(user.level),
                Container(width: px(300), child: Text(user.nickname!, style: _24style, overflow: TextOverflow.ellipsis)),
                Spacer(),
                Padding(
                  padding: EdgeInsets.only(bottom: px(3)),
                  child: Image.asset('assets/images/icon_hot.png', width: px(23)),
                ),
                YBDTPGlobal.wSizedBox(10),
                Container(width: px(100), child: Text(YBDNumericUtil.format(consume)!, style: _24style)),
                YBDTPGlobal.wSizedBox(2),
              ],
            ),
          ),
          _splitLine()
        ],
      ),
    );
  }
  void _pkGuestItemQ2Xyooyelive({int? index, required YBDPkInfoContentSourceTopGiftsUser user, int? consume, Widget? badge}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _avatar({int? id, String? img, Widget? badge}) {
    return Container(
      width: px(88),
      height: px(88),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: px(62),
            height: px(62),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(px(40)), color: Colors.white),
            child: Center(
                child: YBDRoundAvatar(
              img ?? '',
              userId: id ?? 0,
              avatarWidth: 60,
              showVip: false,
              needNavigation: false,
            )),
          ),
          Positioned(bottom: px(7), child: badge ?? Container())
        ],
      ),
    );
  }
  void _avatarCaeH9oyelive({int? id, String? img, Widget? badge}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _level(int? level) {
    return YBDLevelTag(level);
  }

  Widget _splitLine() {
    return Container(color: Colors.white.withOpacity(0.2), height: px(1));
  }

  Widget _giftBtn() {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
        YBDRoomUtil.showGiftSheet();
      },
      child: Container(
        height: px(64),
        width: px(300),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(px(32))),
            gradient: LinearGradient(
                colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter)),
        child: Center(child: Text(translate('pk_contribution_list_gift'), style: _28style)),
      ),
    );
  }
  void _giftBtnLz2SUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  double px(int px) {
    return ScreenUtil().setWidth(px);
  }

  TextStyle _28style = TextStyle(
    fontSize: ScreenUtil().setSp(28),
    color: Colors.white,
  );
  TextStyle _24style = TextStyle(
    fontSize: ScreenUtil().setSp(24),
    color: Colors.white,
  );
}
