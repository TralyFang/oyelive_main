import 'dart:async';


import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/room_ybd_message_helper.dart';
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_user_publish.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_event.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/service/volume_ybd_event.dart';
import 'package:oyelive_main/ui/widget/glow_ybd_widget.dart';

class YBDPkMic extends StatefulWidget {
  bool isOnline;
  String avatarUrl, name;
  int? micUserId;
  YBDPkMic(
      {this.isOnline: false,
      this.avatarUrl: 'https://img1.baidu.com/it/u=2180744820,3495432104&fm=26&fmt=auto&gp=0.jpg',
      this.name: 'streamer'});

  @override
  YBDPkMicState createState() => new YBDPkMicState();
}

class YBDPkMicState extends BaseState<YBDPkMic> {
  getMicSoundStatus() {}
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return StreamBuilder<YBDLiveEvent>(
        // initialData: YBDRoomSocketUtil.getInstance().roomSocketStatus,
        stream: YBDLiveService.instance.onEvent,
        builder: (context, snapshot) {
          bool isTalking = false;
          if (snapshot.hasData && snapshot.data is YBDVolumeEvent) {
            YBDVolumeEvent volumeEvent = snapshot.data as YBDVolumeEvent;
            /*
            List<AudioVolumeInfo> result = volumeEvent.speakers
              ..retainWhere((element) => element.uid == widget.micUserId);

             */
            volumeEvent.speakers?.retainWhere((element) => element.uid == widget.micUserId);
            List<AudioVolumeInfo>? result = volumeEvent.speakers;
            if (result != null && result.length != 0) {
              int volume = result[0].volume;
              isTalking = volume > 20;
            }
          }
          return Stack(
            alignment: Alignment.topCenter,
            children: [
              YBDGlowWidget(
                endRadius: ScreenUtil().setWidth(86),
                showGlows: isTalking,
                glowColor: Colors.white.withOpacity(0.1),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: ScreenUtil().setWidth(120),
                      height: ScreenUtil().setWidth(120),
                      padding: EdgeInsets.all(ScreenUtil().setWidth(2)),
                      decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                      child: ClipOval(
                        child: widget.isOnline
                            ? YBDNetworkImage(
                                imageUrl: widget.avatarUrl,
                                fit: BoxFit.cover,
                                placeholder: (context, url) => Image.asset("assets/images/liveroom/pk/pk_default_head.webp"),
                              )
                            : Container(
                                foregroundDecoration: BoxDecoration(
                                  color: Colors.grey,
                                  backgroundBlendMode: BlendMode.saturation,
                                ),
                                child: YBDNetworkImage(
                                  imageUrl: widget.avatarUrl,
                                  fit: BoxFit.cover,
                                  placeholder: (context, url) => Image.asset("assets/images/liveroom/pk/pk_default_head.webp"),
                                ),
                              ),
                      ),
                    ),
                    if (!widget.isOnline)
                      SizedBox(
                        width: ScreenUtil().setWidth(65),
                        height: ScreenUtil().setWidth(65),
                        child: SVGASimpleImage(
                          assetsName: 'assets/animation/loading.svga',
                        ),
                      )
                  ],
                ),
              ),
              Container(
                  width: ScreenUtil().setWidth(180),
                  margin: EdgeInsets.only(top: ScreenUtil().setWidth(160)),
                  alignment: Alignment.center,
                  child: Text(
                    widget.name,
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.visible,
                  ))
            ],
          );
        });
  }
  void myBuild9Shr3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStateTo3bAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDPkMic oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidget88QqEoyelive(YBDPkMic oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
