import 'dart:async';


import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/src/store.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/agency/agency_ybd_api_helper.dart';
import 'package:oyelive_main/module/status/status_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/ui/page/home/util/home_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_bean.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/pk/pk_ybd_normal_dialog.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/widget/dialog_ybd_pk_finvite.dart';
import 'package:oyelive_main/ui/page/room/widget/dialog_ybd_pk_setting.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';

class YBDPkOptDialog extends StatefulWidget {
  Function? onStart;
  YBDMicBloc? micBloc;
  YBDPkOptDialog({this.onStart, this.micBloc});

  @override
  YBDPkOptDialogState createState() => new YBDPkOptDialogState();
}

class YBDPkOptDialogState extends BaseState<YBDPkOptDialog> {
  bool init = true;

  Duration duration = Duration(milliseconds: 200);

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    return Material(
      color: Colors.black.withOpacity(0.3),
      child: GestureDetector(
        onTap: () {
          setState(() {
            init = true;
          });
        },
        child: Stack(
          children: <Widget>[
            Positioned.fill(
                child: Container(
              decoration: BoxDecoration(),
            )),
            Positioned(
              top: ScreenUtil().statusBarHeight + ScreenUtil().setWidth(20),
              right: ScreenUtil().setWidth(24),
              child: Container(
                width: ScreenUtil().setWidth(650),
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.4),
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    BlocBuilder<YBDMicBloc, YBDMicBlocState>(
                        bloc: widget.micBloc,
                        builder: (BuildContext context, YBDMicBlocState state) {
                          return GestureDetector(
                            onTap: () {
                              List<YBDMicBean>? result = state.micBeanList?.where((element) => element?.userId == YBDLiveService.instance.data.roomId)
                                  .whereNotNull().toList();
                              if (result?.isNotEmpty ?? false) {
                                if (result![0].mute!) {
                                  YBDToastUtil.toast(translate('pk_match_unmute_mic'));
                                  return;
                                }
                                Navigator.pop(context);
                                widget.onStart?.call(state.micBeanList?.whereNotNull().toList());
                              } else {
                                YBDDialogUtil.showPKNormalDialog(context, PKNormalType.TakeMic);
                              }
                              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
                                  location: YBDLocationName.ROOM_PAGE, itemName: 'pk_match'));
                            },
                            child: AnimatedContainer(
                              width: init ? 0 : ScreenUtil().setWidth(130),
                              height: init ? 0 : ScreenUtil().setWidth(166),
                              onEnd: () {
                                if (init) Navigator.pop(context);
                              },
                              duration: duration,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    'assets/images/liveroom/pk/pk_matching.webp',
                                    width: ScreenUtil().setWidth(70).floorToDouble(),
                                  ),
                                  SizedBox(
                                    height: ScreenUtil().setWidth(14).floorToDouble(),
                                  ),
                                  Container(
                                    height: ScreenUtil().setWidth(50).floorToDouble(),
                                    alignment: Alignment.center,
                                    child: AnimatedDefaultTextStyle(
                                      duration: duration,
                                      style: init
                                          ? TextStyle(fontSize: 0)
                                          : TextStyle(fontSize: ScreenUtil().setSp(22), color: Colors.white),
                                      textAlign: TextAlign.center,
                                      child: Text(
                                        translate('start_matching'),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                    GestureDetector(
                      onTap: () async {
                        Navigator.pop(context);
                        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.pk_record);
                      },
                      child: AnimatedContainer(
                        width: init ? 0 : ScreenUtil().setWidth(130),
                        height: init ? 0 : ScreenUtil().setWidth(166),
                        duration: duration,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              'assets/images/liveroom/pk/pk_record.webp',
                              width: ScreenUtil().setWidth(70).floorToDouble(),
                            ),
                            SizedBox(
                              height: ScreenUtil().setWidth(14).floorToDouble(),
                            ),
                            Container(
                              height: ScreenUtil().setWidth(50).floorToDouble(),
                              alignment: Alignment.center,
                              child: AnimatedDefaultTextStyle(
                                duration: duration,
                                style: init
                                    ? TextStyle(fontSize: 0)
                                    : TextStyle(fontSize: ScreenUtil().setSp(22), color: Colors.white),
                                textAlign: TextAlign.center,
                                child: Text(
                                  translate('pk_record_room'),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    BlocBuilder<YBDMicBloc, YBDMicBlocState>(
                        bloc: widget.micBloc,
                        builder: (BuildContext context, YBDMicBlocState state) {
                          return GestureDetector(
                            onTap: () async {
                              List<YBDMicBean?> result = state.micBeanList!
                                  .where((YBDMicBean? element) => element?.userId == YBDLiveService.instance.data.roomId)
                                  .toList();
                              if (result.isNotEmpty) {
                                if (result[0]!.mute!) {
                                  YBDToastUtil.toast(translate('pk_match_unmute_mic'));
                                  return;
                                }
                                Navigator.pop(context);
                                YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.search_pk);
                              } else {
                                YBDDialogUtil.showPKNormalDialog(context, PKNormalType.TakeMic);
                              }
                            },
                            child: AnimatedContainer(
                              width: init ? 0 : ScreenUtil().setWidth(130),
                              height: init ? 0 : ScreenUtil().setWidth(166),
                              duration: duration,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    'assets/images/btn_search_f.webp',
                                    width: ScreenUtil().setWidth(70).floorToDouble(),
                                  ),
                                  SizedBox(
                                    height: ScreenUtil().setWidth(14).floorToDouble(),
                                  ),
                                  Container(
                                    height: ScreenUtil().setWidth(50).floorToDouble(),
                                    alignment: Alignment.center,
                                    child: AnimatedDefaultTextStyle(
                                      duration: duration,
                                      style: init
                                          ? TextStyle(fontSize: 0)
                                          : TextStyle(fontSize: ScreenUtil().setSp(22), color: Colors.white),
                                      textAlign: TextAlign.center,
                                      child: Text(
                                        translate('pk_friend'),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                    GestureDetector(
                      onTap: () async {
                        Navigator.pop(context);
                        // YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.pk_record);
                        showDialog<Null>(
                            context: context, //BuildContext对象
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return YBDPkSettingDialog();
                            });
                      },
                      child: AnimatedContainer(
                        width: init ? 0 : ScreenUtil().setWidth(130),
                        height: init ? 0 : ScreenUtil().setWidth(166),
                        duration: duration,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              'assets/images/images/setting/btn_pk_setting.webp',
                              width: ScreenUtil().setWidth(70).floorToDouble(),
                            ),
                            SizedBox(
                              height: ScreenUtil().setWidth(14).floorToDouble(),
                            ),
                            Container(
                              height: ScreenUtil().setWidth(50).floorToDouble(),
                              alignment: Alignment.center,
                              child: AnimatedDefaultTextStyle(
                                duration: duration,
                                style: init
                                    ? TextStyle(fontSize: 0)
                                    : TextStyle(fontSize: ScreenUtil().setSp(22), color: Colors.white),
                                textAlign: TextAlign.center,
                                child: Text(
                                  translate('Setting'),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        // Navigator.pop(context);
                        // YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.pk_record);
                        print("xws${store!.state.configs!.pkHelp}");
                        YBDNavigatorHelper.navigateToWeb(
                          context,
                          "?url=${Uri.encodeComponent(store.state.configs!.pkHelp ?? '')}&title=Pk Help",
                        );
                      },
                      child: AnimatedContainer(
                        width: init ? 0 : ScreenUtil().setWidth(130),
                        height: init ? 0 : ScreenUtil().setWidth(166),
                        duration: duration,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              'assets/images/btn_help.webp',
                              width: ScreenUtil().setWidth(70).floorToDouble(),
                            ),
                            SizedBox(
                              height: ScreenUtil().setWidth(14).floorToDouble(),
                            ),
                            Container(
                              height: ScreenUtil().setWidth(50).floorToDouble(),
                              alignment: Alignment.center,
                              child: AnimatedDefaultTextStyle(
                                duration: duration,
                                style: init
                                    ? TextStyle(fontSize: 0)
                                    : TextStyle(fontSize: ScreenUtil().setSp(22), color: Colors.white),
                                textAlign: TextAlign.center,
                                child: Text(
                                  translate('Help'),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildAUolfoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      init = false;
      setState(() {});
    });
  }
  void initStateyswjboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDPkOptDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetbKoUPoyelive(YBDPkOptDialog oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
