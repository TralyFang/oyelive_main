import 'dart:async';


import 'emoji_ybd_info.dart';
import 'mini_ybd_game_info.dart';

class YBDMicBean {
  ///true 有人在麦  false 无人在麦上
  bool? micOnline;

  ///true 锁麦  false 未锁麦
  bool? lock;

  ///true（被静音 不显示说话动效控件） false（未被静音 按需显示说话动效控件）
  bool? mute;

  ///0没有边框  1 png边框  2 gif边框  3 svg边框
  int? frameType;

  /// 边框动画 url(拼接后的下载地址)
  String? frameUrl;

  /// 边框动画 svg下载到本地的地址
  String? frameSvgLocalPath;

  ///true（游戏动画） false（不显示控件）
  bool? game;

  ///true（emoji动画） false（不显示控件）
  bool emoji;

  ///用户Id
  int? userId;

  ///麦位Id
  int? micIndex;

  ///头像
  String? img;

  ///声音大小
  int? volume;

  ///昵称
  String? nikName;

  ///麦位游戏
  YBDMiniGameInfo? miniGameInfo;

  ///麦位emoji
  YBDEmojiInfo? emojiInfo;

  ///
  double? volumeOpacity;

  String? vipIcon;

  YBDMicBean(
      {this.micOnline = false,
      this.lock = false,
      this.mute = false,
      this.frameType = 0,
      this.frameUrl,
      this.frameSvgLocalPath,
      this.game = false,
      this.emoji = false,
      this.userId,
      this.micIndex,
      this.img = '',
      this.volume = 0,
      this.nikName,
      this.miniGameInfo,
      this.emojiInfo,
      this.volumeOpacity,
      this.vipIcon});

  @override
  String toString() {
    return 'YBDMicBean{micOnline: $micOnline, lock: $lock, mute: $mute, frameType: $frameType, frameUrl: $frameUrl, game: $game, userId: $userId, micIndex: $micIndex, img: $img, volume: $volume, nikName: $nikName}';
  }
  void toStringY0Hxqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/*
{
micOnline:true 有人在麦  false 无人在麦上
  micOnline_false{
    lock:true  true 锁麦  false 麦空位
  }
  micOnline_true{
    mute：true（） false（不显示说话动效控件）
    frame：0没有边框  1 png边框  2 gif边框  3 svg边框
    game：true（游戏动画） false（不显示控件）
  }
}*/

/*
{
lock:true 有人在麦  false 无人在麦上
  lock_true{
    lock:true  true 锁麦
  }
  lock_false{
    micOnline_true{
      mute：true（） false（不显示说话动效控件）
      frame：0没有边框  1 png边框  2 gif边框  3 svg边框
      game：true（游戏动画） false（不显示控件）
    }
    micOnline_false{
      normal 无人在麦上
    }

  }
}*/
