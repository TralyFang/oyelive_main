
class YBDEmojiInfo {
  ///本地地址
  String? emojiLocalPath;

  ///下载地址
  String? emojiPath;

  ///名字
  String? emojiName;

  ///价格
  String? emojiPrice;

  ///缩略图
  String? emojiThumbnail;
  bool? locked;
  bool? selected;

  ///发送者
  int? userId;

  ///麦位下标
  int? micIndex;

  ///版本
  int? version;

  ///播放时长
  int? frameDuration;

  YBDEmojiInfo({
    this.emojiLocalPath,
    this.emojiPath,
    this.emojiName,
    this.emojiPrice,
    this.emojiThumbnail,
    this.locked,
    this.selected,
    this.userId,
    this.micIndex,
    this.version,
    this.frameDuration,
  });

  @override
  String toString() {
    return 'YBDEmojiInfo{emojiPath: $emojiPath, emojiName: $emojiName, emojiPrice: $emojiPrice, emojiThumbnail: $emojiThumbnail, locked: $locked, selected: $selected, userId: $userId, micIndex: $micIndex, version: $version, frameDuration: $frameDuration}';
  }
  void toStringqn806oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
