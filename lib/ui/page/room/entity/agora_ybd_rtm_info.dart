import 'dart:async';


//指定用户
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:oyelive_main/ui/page/room/ext/rtm_ybd_type_ext.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_event.dart';

///2.3需求：rtm信令传递的消息
class YBDAgoraRtmInfo extends YBDLiveEvent {
  RTMType? rtmType;

  ///pk房pk_id  每一场pk的唯一Id
  int? pk_id;

  ///消息类型  0，匹配状态  1， 进入pk状态  2，pk结束状态
  int? pk_state;

  ///创建时间
  dynamic pk_start_time;

  ///结束时间
  dynamic pk_end_time;

  ///房间Id 发起者
  int? roomId_initiator;

  ///房间Id 接受者
  int? roomId_recipient;

  YBDAgoraRtmInfo(this.rtmType,
      {this.pk_id, this.pk_state, this.pk_start_time, this.pk_end_time, this.roomId_initiator, this.roomId_recipient});

  toJsonContent() {
    return {
      'rtm_type': rtmType.val(),
      "pk_id": pk_id,
      "pk_state": pk_state,
      "pk_start_time": pk_start_time,
      "pk_end_time": pk_end_time,
      "roomId_initiator": roomId_initiator,
      "roomId_recipient": roomId_recipient
    };
  }

  YBDAgoraRtmInfo.fromMap(Map<String, dynamic> map) {
    rtmType = RTMTypeExt.type(map['rtm_type'] ?? 0);
    pk_id = map['pk_id'];
    pk_state = map['pk_state'];
    pk_start_time = map['pk_start_time'];
    pk_end_time = map['pk_end_time'];
    roomId_initiator = map['roomId_initiator'];
    roomId_recipient = map['roomId_recipient'];
  }
}

class YBDPkMediaMute extends YBDLiveEvent {
  //Connecting 正在连接 running 连接成功 Failure连接失败
  ChannelMediaRelayState state;

  YBDPkMediaMute(this.state);
}
