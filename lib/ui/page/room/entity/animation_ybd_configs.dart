import 'dart:async';


class YBDAnimationConfigs {
  int? animationId = 0; // 礼物、座驾  ID
  int? duration = 0; // unused
  int? framDuration = 0; //
  String? name;
  String? foldername;
  int? animationType = 0;
  String? version;

  YBDAnimationConfigs(
      {this.animationId,
      this.duration,
      this.framDuration,
      this.name,
      this.foldername,
      this.animationType,
      this.version});
}
