import 'dart:async';


import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/tp_ybd_cache.dart';

class YBDRoomOperateInfo {
  late int timeOut;
  int? PKID; //官方账号
  int? INID;
  int? randomNum;
  int? audioProfile; //声网通道场景 1 设置成stand
  int? isNeedTpSave; //是否开启tp挽留
  late int showWsaSupportLevel;
  String? resellerAndroidUrl; // 代理充值页面的 h5 地址 android
  String? resellerIosUrl; // 代理充值页面的 h5 地址 ios
  String? termsAndroidUrl; // 协议 h5 地址 android
  String? termsIosUrl; // 协议 h5 地址 ios
  String? wsaRoman; // WHATSAPP Roman
  String? wsaEnglish; // WHATSAPP English
  String? muteChangeRole; //1 切 0 不切
  int? isOpenAccountDelete; // 是否开启账号注销入口 1 开启 0 不开启(默认)
  late String rightUserIdRange; // 用于过滤不合法用户，正确的用户区间 默认"100000,99999999"
  int? gameRoomShowLudo; // 测试游戏房是否显示ludo游戏界面 默认显示
  late String pk_resource_profix; // pk结果svga资源前缀
  //web需要拦截的资源格式后缀名(支持中括号中的备选项，其他无效)：
  List<String>? interceptSuffixes;//：[".png", ".jpeg", ".gif", ".svga", ".webp", ".js", ".css", ".ttf", ".otf"]

  //web需要过滤的包含的域名或包括的字符串，默认全部不拦截[*/*]（如果包括*/*，则全部过滤掉不拦截）：
  List<String>? filterHosts; //：["host/path", "*/*"]
  List<String>? interceptRegExpHosts; // 默认空[] ['[^\\s]+(ludo/tool/game)[^\\s]+(.js|.css)'];
  int? openWebAuth; // 1：追加web.url的auth，0： 不追加(默认)
  int? launchDownloadTpgo; // 是否启动就开始下载tpgo资源包 1:是(默认) 0:否
  String? tpgoResourceProfix; // tpgo资源前缀
  List<dynamic>? wsaConfig; // whatsapp 客服弹框


  String? emojiRestrictionTips; // 表情包不可用提示
  String? enterDuration; // 不同用户进场时间
  String? enterLength; // 不同用户进场队列长度
  int? vipBullet; // vip 几能开弹幕
  List? specialEntrance; // 房间特殊入口
  YBDRoomOperateInfo();

  static YBDRoomOperateInfo get(Map? result) {
    logger.v('room_operate: ${result}');
    return result == null ? YBDRoomOperateInfo.def() : YBDRoomOperateInfo.from(result);
  }

  YBDRoomOperateInfo.from(Map result) {
    YBDRoomOperateInfo def = YBDRoomOperateInfo.def();
    logger.v(
        "roomOperateInfo map : $result，${result['timeOut']}, ${result['PKID']} ,${result['INID']}, ${result['randomNum']} ");
    this.timeOut = int.tryParse(result['timeOut'] ?? '3') ?? 3;
    this.PKID = int.tryParse(result['PKID'] ?? '5485978') ?? 5485978;
    this.INID = int.tryParse(result['INID'] ?? '6407103') ?? 6407103;
    this.randomNum = int.tryParse(result['randomNum'] ?? '6') ?? 6;
    this.audioProfile = int.tryParse(result['audioProfile'] ?? '0') ?? 0;
    this.isNeedTpSave = int.tryParse(result['isNeedTpSave'] ?? '1') ?? 1;
    this.showWsaSupportLevel = int.tryParse(result['showWsaSupportLevel'] ?? '15') ?? 15;
    this.resellerAndroidUrl = result['resellerAndroidUrl']?.toString();
    this.resellerIosUrl = result['resellerIosUrl']?.toString() ?? Const.TOPUP_AGENCY;
    this.termsAndroidUrl = result['termsAndroidUrl']?.toString() ?? Const.TERMS_URL;
    this.termsIosUrl = result['termsIosUrl']?.toString() ?? Const.TERMS_URL;
    this.wsaRoman = result['wsaRoman']?.toString() ?? Const.WHATSAPP_CONTACT_ROMAN;
    this.wsaEnglish = result['wsaEnglish']?.toString() ?? Const.WHATSAPP_CONTACT_ENGLISH;
    this.muteChangeRole = result['muteChangeRole']?.toString() ?? '1';
    this.isOpenAccountDelete = int.tryParse(result['isOpenAccountDelete'] ?? '0') ?? 0;
    this.vipBullet = int.tryParse(result['vipBullet'] ?? '2') ?? 2;
    this.rightUserIdRange = result['rightUserIdRange']?.toString() ?? '100000,99999999';
    this.gameRoomShowLudo = int.tryParse(result['gameRoomShowLudo'] ?? '1') ?? 1;
    this.pk_resource_profix = result['pk_resource_profix']?.toString() ?? Const.PK_RESOURCE_PROFIX;

    this.interceptSuffixes = result['interceptSuffixes'] != null
        ? (result['interceptSuffixes'] as List).map((e) => e.toString()).toList()
        : [];
    this.filterHosts =
        result['filterHosts'] != null ? (result['filterHosts'] as List).map((e) => e.toString()).toList() : ['*/*'];
    this.interceptRegExpHosts = result['interceptRegExpHosts'] != null
        ? (result['interceptRegExpHosts'] as List).map((e) => e.toString()).toList()
        : [];
    this.openWebAuth = getMapIntValue(result, 'open_web_auth');
    this.launchDownloadTpgo = getMapIntValue(result, 'launchDownloadTpgo', defualtInt: 1);
    this.tpgoResourceProfix = result['tpgoResourceProfix']?.toString() ?? YBDTPCache.defaultProfix;
    this.wsaConfig = result['wsaConfig'] ?? def.wsaConfig;
    this.specialEntrance = result['specialEntrance'] ?? def.specialEntrance;
    this.emojiRestrictionTips = result['emojiRestrictionTips']?.toString() ?? def.emojiRestrictionTips;
    this.enterDuration = result['enterDuration']?.toString() ?? def.enterDuration;
    this.enterLength = result['enterLength']?.toString() ?? def.enterLength;
  }

  /// 从map中获取int 类型的的值，key：map中的key
  int? getMapIntValue(Map result, String key, {int defualtInt = 0}) {
    var value = result[key];
    if (value != null) {
      return value is String ? int.tryParse(value) : value.toInt();
    }
    return defualtInt;
  }

  static YBDRoomOperateInfo? _def;

  static YBDRoomOperateInfo def() {
    if (_def != null) return _def!;
    _def = YBDRoomOperateInfo()
      ..timeOut = 3
      ..PKID = 5485978
      ..INID = 6407103
      ..randomNum = 6
      ..audioProfile = 0
      ..isNeedTpSave = 1
      ..showWsaSupportLevel = 15
      ..termsAndroidUrl = Const.TERMS_URL
      ..termsIosUrl = Const.TERMS_URL
      ..resellerIosUrl = Const.TOPUP_AGENCY
      ..wsaRoman = Const.WHATSAPP_CONTACT_ROMAN
      ..wsaEnglish = Const.WHATSAPP_CONTACT_ENGLISH
      ..isOpenAccountDelete = 0
      ..vipBullet = 2
      ..rightUserIdRange = '100000,99999999'
      ..gameRoomShowLudo = 1
      ..pk_resource_profix = Const.PK_RESOURCE_PROFIX
      ..interceptSuffixes = []
      ..filterHosts = ['*/*']
      ..interceptRegExpHosts = []
      ..openWebAuth = 0
      ..launchDownloadTpgo = 1
      ..tpgoResourceProfix = YBDTPCache.defaultProfix
      ..wsaConfig = [
        {"name": "roman_english", "level": 15, "number": 923455213179},
        {"name": "english", "level": 15, "number": 971554122778}
      ]
      ..muteChangeRole = '1'
      ..emojiRestrictionTips = 'You can use this sticker when you get VIP2 or above.'
      ..enterDuration = '1.5/2.0/2.3/2.6/2.9/3.2'
      ..enterLength = '8/5'
      ..specialEntrance = [
        {
          "route": "https://www.bilibili.com/",
          "pic": "https://img.zcool.cn/community/01c8f15aeac135a801207fa16836ae.jpg@1280w_1l_2o_100sh.jpg",
          "remainingTime": 1672430644000
        }
      ];
    return _def!;
  }
}
