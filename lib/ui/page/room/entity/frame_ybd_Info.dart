import 'dart:async';



import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDFrameInfo extends JsonConvert<YBDFrameInfo> {
  String? image;
  int? personalId;
  String? name;

  YBDFrameInfo({this.image, this.personalId, this.name});

  YBDFrameInfo.fromMap(Map<String, dynamic> json) {
    // TODO: implement fromMap
    image = json['image'];
    personalId = json['personalId'];
    name = json['name'];
  }

  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}

/*
"frame":{
"image":"1591697490103.png",
"personalId":2376,
"price":30,
"name":"Spectacles",
"index":3,
"expireAfter":29590548,
"id":1010002,
"label":1
}*/
