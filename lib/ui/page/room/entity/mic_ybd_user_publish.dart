import 'dart:async';


import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_item.dart';

///麦位信息
class YBDMicUserPublish extends JsonConvert<YBDMicUserPublish> implements YBDDisplayMessage {
  List<YBDMicItem?>? content;

  List<int>? masters;

  String? destination;

  int? fromUser;

  String? mode;

  String? msgType;

  String? receiver;

  String? receiverImg;

  int? receiverLevel;

  int? receiverSex;

  int? receiverVip;

  int? roomId;

  String? sender;

  String? senderImg;

  int? senderLevel;

  int? senderSex;

  int? senderVip;

  String? time;

  int? toUser;

  YBDMessage? fromMap(Map json) {
    return fromJson(json as Map<String, dynamic>);
  }

  toJsonContent() {
    return toJson();
  }
}
