import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/room/entity/frame_ybd_Info.dart';
import 'package:oyelive_main/ui/page/room/entity/mute_ybd_Info.dart';

class YBDMicItem extends JsonConvert<YBDMicItem> {
  int? status; //
  int? index;
  int? userId;
  String? nickname;
  String? img;
  double? time;
  int? period;
  YBDAttributes? attributes;
  String? vipIcon;

  YBDMicItem({
    this.status,
    this.index,
    this.userId,
    this.nickname,
    this.img,
    this.time,
    this.period,
    this.attributes,
    this.vipIcon,
  });
}

class YBDAttributes extends JsonConvert<YBDAttributes> {
  YBDFrameInfo? frame;
  YBDMuteInfo? muteInfo;

  YBDAttributes({this.frame, this.muteInfo});
}
