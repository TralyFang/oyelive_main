
/*
 * @Author: William
 * @Date: 2022-12-21 17:05:03
 * @LastEditTime: 2022-12-27 15:46:33
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/room/entity/emoji_entity.dart
 */
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDEmojiEntity extends JsonConvert<YBDEmojiEntity> {
  String? returnCode;
  String? returnMsg;
  String? timestamp;
  List<YBDEmoji>? record;
}

class YBDEmoji extends JsonConvert<YBDEmoji> {
  String? id;
  String? name;
  String? coverImg;
  String? price;
  late List<YBDEmojiData> emojiDatas;
  List<YBDUseCondition>? useCondition;
  String? updateTime;
  String? status;
}

class YBDEmojiData extends JsonConvert<YBDEmojiData> {
  String? id;
  String? pic;
  String? svga;
  String? index;
  String? name;
}

class YBDUseCondition extends JsonConvert<YBDUseCondition> {
  String? type;
  String? condition;
}

class YBDSelfEmoji with JsonConvert<YBDSelfEmoji> {
  String? returnCode;
  String? returnMsg;
  YBDSelfEmojiRecord? record;
}

class YBDSelfEmojiRecord with JsonConvert<YBDSelfEmojiRecord> {
  int? userId;
  List<int>? emojiIds;
}
