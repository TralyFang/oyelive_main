import 'dart:async';


import '../../../../module/user/entity/user_ybd_info_entity.dart';

class YBDMicRequestInfo {
  YBDUserInfo? userInfo; //用户
  int? time; //请求时间
  int type; //请求类型  默认上麦请求
  int? micIndex; //麦位*/

  YBDMicRequestInfo({
    this.userInfo,
    this.type = 1,
    this.time,
    this.micIndex,
  });
}
