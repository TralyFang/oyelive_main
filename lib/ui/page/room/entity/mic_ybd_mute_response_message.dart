import 'dart:async';


import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

///静音消息
class YBDMicMuteResponseMessage extends JsonConvert<YBDMicMuteResponseMessage> implements YBDDisplayMessage {
  ///操作类型:1.静音 2. 解除静音
  int? type;

  ///mic下标
  int? index;

  ///源序列号
  int? originSequence;

  ///1 - 静音、解除请求 2- 静音、解除完成
  int? direction;

  String? destination;

  int? fromUser;

  String? mode;

  String? msgType;

  String? receiver;

  String? receiverImg;

  int? receiverLevel;

  int? receiverSex;

  int? receiverVip;

  int? roomId;

  String? sender;

  String? senderImg;

  int? senderLevel;

  int? senderSex;

  int? senderVip;

  String? time;

  int? toUser;

  int? sequence;

  int? commandId;

  YBDMessage fromMap(Map json) {
    // TODO: implement fromMap
    assert(false, 'implement forMap');
    return YBDMessage();
  }

  toJsonContent() {
    // TODO: implement toJsonContent
    assert(false, 'implement toJsonContent');
  }
}
