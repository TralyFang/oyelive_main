import 'dart:async';



import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDMuteInfo extends JsonConvert<YBDMuteInfo> {
  String? status;
  String? muteUserId;

  YBDMuteInfo({this.status, this.muteUserId});

  YBDMuteInfo.fromMap(Map<String, dynamic> json) {
    // TODO: implement fromMap
    status = json['status'];
    muteUserId = json['muteUserId'];
  }

  toJsonContent() {
    // TODO: implement toJsonContent
    return toJson();
  }
}
