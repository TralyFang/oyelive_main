import 'dart:async';


import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
// import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:sprintf/sprintf.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_live_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc_state.dart';
import 'package:oyelive_main/ui/page/room/gift_btn/gift_ybd_btn.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_emoji_page.dart';
import 'package:oyelive_main/ui/page/room/widget/svga_ybd_play_frame.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/constant/const.dart';
import '../../../common/db_ybd_config.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/dialog_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../module/inbox/db/model_ybd_conversation.dart';
import '../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../module/user/util/user_ybd_util.dart';
import '../../../redux/app_ybd_state.dart';
import '../../widget/sharing_ybd_widget.dart';
import 'bloc/room_ybd_bloc.dart';
import 'define/room_ybd_define.dart';
import 'service/live_ybd_service.dart';
import 'util/room_ybd_util.dart';
import 'widget/inbox_ybd_sheet.dart';
import 'widget/normal_ybd_dialog.dart';
import 'package:collection/collection.dart';

class YBDBottomNav extends StatefulWidget {
  final int? roomId;

  /// 显示底部输入框
  final bool? popInput;

  /// 底部输入框控制器
  final TextEditingController? textEditingController;

  /// 底部输入框 FocusNode
  final FocusNode? focusNode;

  // final YBDMicBloc micBloc;

  YBDBottomNav({
    this.roomId: 0,
    this.popInput,
    this.textEditingController,
    this.focusNode,
    // this.micBloc,
  });

  @override
  YBDBottomNavState createState() => new YBDBottomNavState();
}

class YBDBottomNavState extends BaseState<YBDBottomNav> with TickerProviderStateMixin {
  bool isMuteAll = false;
  SelectBloc? bloc;

  int unreadCount = 0;

  ///是否发送弹幕模式
  bool _isBarrage = false;

  // YBDQueryConfigsRespEntity _localConfigEntity;

  double iconWidth = 64.px;

  late SVGAAnimationController _animationController;

  normalLine() {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    return Row(
      children: [
        SizedBox(width: 25.px),
        // 静音
        GestureDetector(
          onTap: () {
            YBDCommonTrack().commonTrack(YBDTAProps(location: 'sound', module: YBDTAModule.liveroom));
            if (isMuteAll) {
              YBDLiveService.instance.adjustPlaybackSignalVolume(100);
            } else {
              YBDLiveService.instance.adjustPlaybackSignalVolume(0);
            }
            setState(() {
              isMuteAll = !isMuteAll;
            });
          },
          child: Image.asset(
            isMuteAll ? 'assets/images/liveroom/icon_no_voice@2x.webp' : 'assets/images/liveroom/icon_voice@2x.webp',
            width: iconWidth,
          ),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              // 发公聊区信息
              GestureDetector(
                onTap: () {
                  logger.v('clicked type a message btn');
                  // Still kicked out of the room in the game, can't send a message, give a hint
                  if (YBDLiveService.instance.bootedLiveRoomLimit) {
                    YBDToastUtil.toast(translate('permissions_restricted'));
                    return;
                  }
                  context.read<YBDRoomBloc>().add(RoomEvent.ShowKeyBoard);
                  widget.focusNode?.requestFocus();
                },
                child: Container(
                  width: ScreenUtil().setWidth(150),
                  height: ScreenUtil().setWidth(64),
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.symmetric(horizontal: 5.px),
                  padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.1),
                      borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(36)))),
                  child: Text(
                    translate('type_message'),
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xffEBEBEB),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              ),
              // emoji
              YBDDelayGestureDetector(
                onTap: () {
                  YBDCommonTrack().commonTrack(YBDTAProps(location: 'emoji', module: YBDTAModule.liveroom));
                  if (BlocProvider.of<YBDMicBloc>(context).state.gotMic ?? false) {
                    ApiHelper.getSelfEmoji();
                    showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      enableDrag: false,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(32))),
                      ),
                      barrierColor: Colors.transparent,
                      backgroundColor: Colors.transparent,
                      builder: (_) {
                        return YBDEmojiPage();
                      },
                    );
                  } else {
                    YBDToastUtil.toast('Please take the mic to use the emoji.');
                  }
                },
                child: Image.asset('assets/images/liveroom/icon_emoji@2x.webp', width: iconWidth),
              ),
              // 私信
              Container(
                  width: ScreenUtil().setWidth(76),
                  child: TextButton(
                      style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.zero)),
                      onPressed: () {
                        showModalBottomSheet(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(16)))),
                            context: context,
                            isScrollControlled: true,
                            backgroundColor: Colors.transparent,
                            builder: (BuildContext context) {
                              return YBDInboxSheet();
                            });
                      },
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Image.asset('assets/images/liveroom/icon_chat@2x.webp', width: iconWidth),
                          ),
                          unreadCount != 0
                              ? Align(
                                  alignment: Alignment.center,
                                  child: Transform.translate(
                                    offset: Offset(17.px, -ScreenUtil().setWidth(14)),
                                    child: Container(
                                      height: ScreenUtil().setWidth(24),
                                      padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(6),
                                      ),
                                      decoration: BoxDecoration(
                                          color: Color(0xffE6497A),
                                          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(12)))),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            YBDCommonUtil.getUnreadCount(unreadCount),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: ScreenUtil().setSp(18), color: Colors.white, height: 1),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              : Container()
                        ],
                      ))),
              // 配置图标
              _configGameIcon(),
              // 游戏
              Container(
                  width: ScreenUtil().setWidth(76),
                  child: TextButton(
                      style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.zero)),
                      onPressed: () {
                        logger.v('clicked play center btn');
                        YBDCommonTrack().commonTrack(YBDTAProps(
                          location: YBDTAClickName.liveroom_game,
                          module: YBDTAModule.liveroom,
                        ));
                        YBDRoomUtil.showPlayCenterSheet(widget.roomId, defaultTab: 1);
                      },
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Image.asset('assets/images/liveroom/icon_all@2x.webp', width: iconWidth),
                          Positioned(right: 0, top: 0, child: YBDRoomUtil.ludoRedDot())
                        ],
                      ))),
            ],
          ),
        ),
        // SizedBox(width: 20.px),

        // SizedBox(width: 20.px),

        // SizedBox(width: 8.px),

        // SizedBox(width: ScreenUtil().setWidth(store.state.configs.lotteryPageDisplay == '0' ? 0 : 8)),
        // _configGameIcon(),
        // SizedBox(width: 8.px),

        // SizedBox(width: 20.px),
        _sendGiftBtn(),
        SizedBox(width: 25.px),
      ],
    );
  }

  inputLine() {
    return Row(
      children: [
        SizedBox(
          width: ScreenUtil().setWidth(1),
        ),
        GestureDetector(
            onTap: () async {
              logger.v('clicked YBDBarrage');
              _changeToBarrageType();
            },
            child: Image.asset(
              "assets/images/liveroom/icon_danmu_${_isBarrage ? 'on' : 'off'}.png",
              height: ScreenUtil().setWidth(64),
            )),
        SizedBox(
          width: ScreenUtil().setWidth(1),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
            alignment: AlignmentDirectional.centerStart,
            height: ScreenUtil().setWidth(72),
            decoration: BoxDecoration(
                color: !_isBarrage ? Color(0x2bffffff) : Color(0xff3ED5CD),
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(36)))),
            child: TextField(
              style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
              textAlign: TextAlign.start,
              textInputAction: TextInputAction.send,
              controller: widget.textEditingController,
              focusNode: widget.focusNode,
              maxLength: !_isBarrage ? Const.MAX_CHAT_LENGTH : Const.MAX_BARRAGE_LENGTH,
              decoration: InputDecoration(
                  isDense: true,
                  counterText: '',
                  contentPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
                  border: InputBorder.none,
                  hintStyle: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xffEBEBEB)),
                  hintText: translate('type_bullet_message')),
              onChanged: (x) {
                if (x.length >= (!_isBarrage ? Const.MAX_CHAT_LENGTH : Const.MAX_BARRAGE_LENGTH)) {
                  YBDToastUtil.toast(translate('chat_max_length'));
                }
              },
              onSubmitted: (x) async {
                YBDCommonTrack().commonTrack(YBDTAProps(
                  location: 'chat_send',
                  module: YBDTAModule.liveroom,
                  id: widget.roomId?.toString(),
                ));
                var connectivityResult = await (Connectivity().checkConnectivity());
                if (connectivityResult == ConnectivityResult.none) {
                  YBDToastUtil.toast(translate('no_network'));
                  return;
                }
                if (x.trim().isEmpty) {
                  YBDToastUtil.toast(translate('say_something'));
                  return;
                }

                if (x.isNotEmpty && !_isBarrage) {
                  YBDRoomSocketApi.getInstance().sendPublicMessage(roomId: widget.roomId, content: x);
                  widget.textEditingController!.clear();
                  widget.focusNode!.unfocus();
                  context.read<YBDRoomBloc>().add(RoomEvent.HideKeyBoard);
                } else if (x.isNotEmpty && _isBarrage) {
                  YBDCommonTrack().commonTrack(YBDTAProps(location: 'bullet_message', module: YBDTAModule.liveroom));

                  ///TODO 发送弹幕  需要条件判断
                  YBDRoomSocketApi.getInstance().sendBulletMessage(roomId: widget.roomId, content: x);
                  widget.textEditingController!.clear();
                  widget.focusNode!.unfocus();
                  context.read<YBDRoomBloc>().add(RoomEvent.HideKeyBoard);
                } else {
                  logger.v('can not sent message');
                }
              },
            ),
          ),
        ),
        Container(
            width: ScreenUtil().setWidth(76),
            // color: Colors.green,
            child: TextButton(
                style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.zero)),
                onPressed: () async {
                  // If there's still a roomid here, but you can't leave a message, it's not an unsubscribe problem.
                  YBDCommonTrack().commonTrack(YBDTAProps(
                    location: 'chat_send',
                    module: YBDTAModule.liveroom,
                    id: widget.roomId?.toString(),
                  ));
                  logger.v('clicked send btn roomId: ${YBDRoomSocketUtil.getInstance().roomId}');
                  var connectivityResult = await (Connectivity().checkConnectivity());
                  if (connectivityResult == ConnectivityResult.none) {
                    YBDToastUtil.toast(translate('no_network'));
                    return;
                  }
                  if (widget.textEditingController!.text.trim().isEmpty) {
                    YBDToastUtil.toast(translate('say_something'));
                    return;
                  }
                  if (widget.textEditingController!.text.isNotEmpty && !_isBarrage) {
                    YBDRoomSocketApi.getInstance()
                        .sendPublicMessage(roomId: widget.roomId, content: widget.textEditingController?.text);
                    widget.textEditingController?.clear();
                    widget.focusNode?.unfocus();
                    context.read<YBDRoomBloc>().add(RoomEvent.HideKeyBoard);
                  } else if (widget.textEditingController?.text.isNotEmpty ?? false && _isBarrage) {
                    YBDCommonTrack().commonTrack(YBDTAProps(location: 'bullet_message', module: YBDTAModule.liveroom));

                    ///TODO 发送弹幕  需要条件判断
                    YBDRoomSocketApi.getInstance()
                        .sendBulletMessage(roomId: widget.roomId, content: widget.textEditingController!.text);
                    widget.textEditingController!.clear();
                    widget.focusNode!.unfocus();
                    context.read<YBDRoomBloc>().add(RoomEvent.HideKeyBoard);
                  } else {
                    logger.v('can not sent message');
                  }
                },
                child: Icon(
                  Icons.send,
                  color: Colors.white,
                ))),
      ],
    );
  }

  @override
  Widget myBuild(BuildContext context) {
    return SafeArea(
      child: Container(
        height: ScreenUtil().setWidth(96),
        width: ScreenUtil().setWidth(720),
        margin: EdgeInsets.only(bottom: 10.px),
        // color: Colors.blue,
        // decoration: BoxDecoration(
        //   color: Color(0x19000000),
        //   // color: Colors.white,
        //   // color: Colors.amber[400],
        // ),
        child: (widget.popInput ?? false) ? inputLine() : normalLine(),
      ),
    );
  }
  void myBuildoCvvnoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();

    try {
      bloc = SelectBloc(
        database: messageDb!,
        table: YBDConversationModel.conversationTableName,
        reactive: true,
      );
    } catch (e) {
      logger.v('init SelectBloc error : $e');
    }

    if (null != bloc) {
      bloc!.items.listen((data) {
        if (data != null && mounted) {
          List<YBDConversationModel> updateConversation =
              List.generate(data.length, (index) => YBDConversationModel().fromDb(data[index]));
          updateConversation.removeWhere((element) => element.unreadCount == 0 || element.lastMessage!.isEmpty);
          int count = 0;
          updateConversation.forEach((element) {
            if (element.conversationType == 'friend') count += element.unreadCount!;
          });

          unreadCount = count;
          setState(() {});
        }
      });
    } else {
      logger.v('bloc is null');
    }
    logger.v('roomid is : ${widget.roomId}');
    //后期ludo游戏需要roomid 提前埋进去
    YBDRoomSocketApi.roomId = widget.roomId;
    _playGiftAnimation();
  }
  void initStatetWbQwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///切换到弹幕模式
  void _changeToBarrageType() async {
    YBDUserInfo? userInfo = await YBDUserUtil.userInfo();
    if (_isBarrage) {
      setState(() {
        _isBarrage = false;
      });
    } else {
      int vipBullet = YBDCommonUtil.getRoomOperateInfo().vipBullet ?? 0;
      if ((userInfo?.vip ?? 0) >= vipBullet) {
        setState(() {
          _isBarrage = true;
        });
      } else {
        YBDDialogUtil.showNormalDialog(
          context,
          type: NormalDialogType.two,
          showBorder: false,
          height: 200.px,
          okCallback: () {
            Navigator.pop(context);
            YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.vip + "/1");
          },
          // 弹幕提示买VIP弹框
          content: translate('bullet_str_new'),
          ok: translate('go_to_buy'),
          cancel: translate('cancel'),
        );
      }
    }
  }
  void _changeToBarrageTypehUAo4oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 可配置的游戏图标
  Widget _configGameIcon() {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
      builder: (context, state) {
        var configs = YBDCommonUtil.storeFromContext()?.state.configs;
        double iconWidth = 64.px;

        // 配置项据控制是否显示图标
        if (configs?.lotteryPageDisplay == '0') {
          return SizedBox(width: iconWidth);
        }

        // 房间底部的游戏数据
        var configGame = state.gameData?.firstWhereOrNull(
          (element) => element!.showConfigGame(),
          /*orElse: () => null,*/
        );

        if (null == configGame || configGame.url!.isEmpty) {
          // 游戏链接为空隐藏图标
          return SizedBox(width: iconWidth);
        }

        return Container(
          width: iconWidth,
          child: TextButton(
            style: ButtonStyle(
              padding: MaterialStateProperty.all(EdgeInsets.zero),
            ),
            onPressed: () {
              logger.v('clicked turn table: ${widget.roomId}');
              YBDCommonTrack().commonTrack(YBDTAProps(location: 'recommend', module: YBDTAModule.liveroom));
              YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_game_click, record: configGame);
              YBDRoomSocketApi.roomId = widget.roomId;
              YBDRoomUtil.showH5Game(
                context,
                configGame.url,
                type: WebGameType.RoomBottom,
              );
            },
            child: YBDNetworkImage(
              width: iconWidth,
              imageUrl: configGame.icon ?? '',
              fit: BoxFit.contain,
              placeholder: (a, b) => Image.asset('assets/images/liveroom/icon_game@2x.webp', width: iconWidth),
              errorWidget: (context, url, error) =>
                  Image.asset('assets/images/liveroom/icon_game@2x.webp', width: iconWidth),
            ),
          ),
        );
      },
    );
  }
  void _configGameIconXJTicoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 播放礼物动画
  void _playGiftAnimation() async {
    _animationController = SVGAAnimationController(vsync: this);
    final videoItem = await SVGAParser.shared.decodeFromAssets('assets/images/liveroom/live_gift_normal.svga');
    _animationController.videoItem = videoItem;
    _animationController.repeat();
  }

  /// 送礼按钮
  Widget _sendGiftBtn() {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
        buildWhen: (prev, current) => current.type == RoomStateType.RoomInfoUpdated,
        builder: (_, state) {
          if (null == state.roomInfo) {
            // 房间信息为空时隐藏combo按钮
            return SizedBox();
          }

          return Container(
            // width: ScreenUtil().setWidth(110),
            // height: ScreenUtil().setWidth(110),
            // color: Colors.pink.withOpacity(0.5),
            margin: EdgeInsets.only(bottom: 10.px),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    onTap: () {
                      logger.v('clicked send gift btn');
                      YBDCommonTrack().commonTrack(YBDTAProps(location: 'gift', module: YBDTAModule.liveroom));
                      YBDRoomUtil.showGiftSheet();
                    },
                    // child: Image.asset('assets/images/liveroom/icon_gift@2x.webp', width: 80.px),
                    child: Container(width: 80.px, height: 80.px, child: SVGAImage(_animationController)),
                  ),
                ),
                Align(alignment: Alignment.center, child: YBDGiftButton()),
                // ComboBtn()
              ],
            ),
          );
        });
  }
  void _sendGiftBtnjskhJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
