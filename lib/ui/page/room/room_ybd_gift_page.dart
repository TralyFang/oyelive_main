import 'dart:async';


import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:oyelive_main/common/room_socket/message/common/enter.dart';
import 'package:oyelive_main/common/room_socket/message/common/gift.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_record_extend.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/constant/const.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/download_ybd_manager.dart';
import '../../../common/util/download_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/zip_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../home/entity/car_ybd_list_entity.dart';
import '../store/entity/downloadbean.dart';
import '../store/widget/svga_ybd_play.dart';
import 'bloc/room_ybd_bloc.dart';
import 'service/live_ybd_service.dart';

/// 礼物、座驾动画
class YBDRoomGiftage extends StatefulWidget {
  ///
  List<String?>? roomAnimationPath = List.filled(3, '');

  // YBDRoomBloc roomBloc;
  YBDEnter? enter;// 进房模型
  YBDGift? gift; // 送礼模型

  YBDRoomGiftage({
    this.roomAnimationPath,
    this.enter,
    this.gift,
  });

  @override
  _YBDRoomGiftPageState createState() => _YBDRoomGiftPageState();
}

class _YBDRoomGiftPageState extends BaseState<YBDRoomGiftage> {
  ///进房动画地址
  List<String?> enterRoomLocalPath = List.filled(2, '');

  ///进房动画地址集
  Map<String, List<String>> enterRoomMap = new Map<String, List<String>>();

  ///下载列表
  List<YBDDownLoadBean> downLoadList = [];

  ///座驾动画 --所有
  List<YBDCarListRecord?>? carEnterRoomList = [];

  ///进房用户
  int? curEnterRoomUser;

  /// 引用[YBDRoomBloc]，[playerStateCallBack]是在dispose中被回调的这里提前保存roomBloc
  late YBDRoomBloc _roomBloc;

  @override
  void initState() {
    super.initState();
    _requestCarList();
    YBDDownLoadManager.instance!.addCallBacks(YBDDownLoadManager.instance!.callBackRoomTheme, downloadCallback);
  }
  void initStateG2Ymroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求座驾列表数据(用于动画解析)
  _requestCarList() async {
    if ((YBDLiveService.instance.carList ?? []).isNotEmpty) {
      carEnterRoomList = YBDLiveService.instance.carList;
      return;
    }

    YBDCarListEntity? carListEntity = await ApiHelper.carList(context, queryAll: true);
    if (null != carListEntity && carListEntity.returnCode == Const.HTTP_SUCCESS) {
      logger.v('room bg queryCAR SUCCESS');
      carEnterRoomList = carListEntity.record;
    } else {
      logger.v('room bg queryCAR failed');
    }
  }

  ///下载进度回调
  Function? downloadCallback(YBDDownloadTaskInfo event) {
    logger.v('downloadCallback updateProgress');
    if (event == null) {
      logger.v('downloadCallback task info arg is null');
      return null;
    }

    logger.v('downloadCallback----');

    for (int i = 0; i < downLoadList.length; i++) {
      logger.v('downloadCallback i:$i taskId:${downLoadList[i].taskId}');

      if (downLoadList[i].taskId != null && downLoadList[i].taskId!.compareTo(event.id!) == 0) {
        if (event.status == DownloadTaskStatus.complete) {
          logger.v('downloadCallback----complete taskID:${event.id}');
          if (downLoadList[i].type == DownLoadType.svg_room_bg) {
            if (mounted) {
              setState(() {});
            }
          } else if (downLoadList[i].type == DownLoadType.svg_entrances) {
            _unzipSvga(i);

            ///座驾 有压缩
          }
        } else if (event.status == DownloadTaskStatus.failed) {
          logger.v('downloadCallback----failed taskID:${event.id}');
        } else if (event.status == DownloadTaskStatus.running) {
          double progress = YBDCommonUtil.formatDoubleNum(event.progress, 2);
          logger.v('downloadCallback taskID:${event.id}  progress :$progress  event.progress:$event.progress');
        } else {}
        break;
      }
    }

    return null;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _roomBloc = BlocProvider.of<YBDRoomBloc>(context);
  }
  void didChangeDependenciesbHGvYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    logger.v('room_bg animation myBuild');
    return _roomAnimation();
  }
  void myBuildn96vxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _roomAnimation() {
    bool local = false;
    if (widget.roomAnimationPath!.length > 2 &&
        widget.roomAnimationPath![2] != null &&
        widget.roomAnimationPath![2] == 'true') {
      logger.v('roomAnimationPath local : $local');
      local = true;
    }
    return Stack(children: [
      _enterRoomAnimation(
        path: widget.roomAnimationPath![0],
        mp3file: widget.roomAnimationPath![1],
        isLocal: local,
      ),
    ]);
  }
  void _roomAnimationB1TK8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///进房座驾动画
  Widget _enterRoomAnimation({String? path, String? mp3file, bool isLocal = false}) {
    YBDCarListRecordExtend? extend;
    if (widget.enter?.carExtend != null) {
      try {
        Map? json = jsonDecode(widget.enter?.carExtend ?? '');
        extend = YBDCarListRecordExtend().fromJson(json as Map<String, dynamic>);
      } on Error catch (error) {
        logger.v('room enter YBDCarListRecordExtend error: $error');
      }
    }
    logger.v(
        '_enterRoomAnimation path:$path mp3file:$mp3file isLocal:$isLocal extend:${widget
            .enter?.carExtend}, gift.customized: ${widget.gift?.customized}');
    return (path == null || path.isEmpty)
        ? SizedBox()
        : YBDSvgaPlayerPage(url: path,
        mp3File: mp3file,
        callBack: playerStateCallBack,
        mud: 1,
        isLocal: isLocal,
        replaceSvgaEntitys: widget.gift?.customizedEntity()?.data,
        replaceSvgaEntity: extend);
  }
  void _enterRoomAnimation7xHD9oyelive({String? path, String? mp3file, bool isLocal = false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 动画播放状态
  Future playerStateCallBack(bool isPlaying) async {
    logger.v('playerStateCallBack isPlaying:$isPlaying');
    _roomBloc.showEnterRoomAnimation(isPlaying);
    if (!isPlaying) {
      ///播放结束 把路劲清空
      try {
        enterRoomLocalPath[0] = null;
        enterRoomLocalPath[1] = null;
      } catch (e) {}
    }
  }

  /// 解压座驾动画文件
  _unzipSvga(int unzipIndex) async {
    if (unzipIndex >= 0 && downLoadList != null && downLoadList.length > unzipIndex) {
      for (int i = 0; i < carEnterRoomList!.length; i++) {
        if (downLoadList[unzipIndex].fileName == carEnterRoomList![i]!.animationInfo!.name) {
          String path =
              'Animation/${carEnterRoomList![i]!.animationInfo!.foldername}/${carEnterRoomList![i]!.animationInfo!.version}';
          path = await YBDCommonUtil.getResourceDir(path);
          String pathZip = path + '/' + carEnterRoomList![i]!.animationInfo!.name!;
          bool unzip = await YBDZipUtil.asyncUnZip(path, pathZip);
          logger.v('unzip :$unzip _carRecordList[i].id:${carEnterRoomList![i]!.id}');
          logger.v("_unzipSvga path:$path pathZip:$pathZip");

          ///解压完才通知页面  状态修改
          if (unzip) {
            setState(() {});
          }
          break;
        }
      }
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    YBDDownLoadManager.instance!.removeCallBack(YBDDownLoadManager.instance!.callBackRoomTheme);
  }
  void disposeNJeMYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
