import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/room_socket/message/common/follow_ybd_display_message.dart';

class YBDItemFollow extends StatefulWidget {
  YBDFollowDisplayMessage data;

  YBDItemFollow(this.data);

  @override
  YBDItemFollowState createState() => new YBDItemFollowState();
}

class YBDItemFollowState extends BaseState<YBDItemFollow> {
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Container(
      margin: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(12), horizontal: ScreenUtil().setWidth(14)),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
          color: Colors.black.withOpacity(0.4)),
      child: Text.rich(TextSpan(children: [
        TextSpan(text: widget.data.sender),
        TextSpan(text: ' '),
        TextSpan(text: translate("follow"), style: TextStyle(color: Colors.white)),
        TextSpan(text: ' '),
        TextSpan(text: widget.data.receiver),
      ], style: TextStyle(fontSize: ScreenUtil().setSp(22), color: Color(0xffFFE824)))),
    );
  }
  void myBuild62MSRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatesolZHoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDItemFollow oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgethJQZQoyelive(YBDItemFollow oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
