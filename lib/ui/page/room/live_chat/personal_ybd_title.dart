import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../common/util/string_ybd_util.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/round_ybd_avatar.dart';
import '../bloc/room_ybd_bloc.dart';
import '../util/room_ybd_util.dart';

class YBDPersonalTitle extends StatelessWidget {
  var img, userId, level, vipLevel;
  String? name;
  Color? nameColor;
  TextSpan? extra;
  bool sign;

  ///弹幕标示
  bool barrage;

  YBDUserCertificationRecordCertificationInfos? cerInfo;

  // 是否显示头像
  bool showAvatar;

  // 是否显示VIP图标
  bool showVIP;

  String? vipIcon;
  YBDPersonalTitle(
      {this.img,
      this.name,
      this.userId,
      this.level,
      this.vipLevel,
      this.nameColor,
      this.extra,
      this.sign: true,
      this.cerInfo,
      this.barrage: false,
      this.showAvatar: true,
      this.showVIP: true,
      this.vipIcon});

  getName() {
    if (name == null) return '';
    String cutName = YBDStringUtil.subString(name, 10)!;
    return cutName + (sign ? ": " : " ");
  }

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          showAvatar
              ? WidgetSpan(
                  child: GestureDetector(
                    onTap: () {
                      YBDRoomUtil.showMiniProfileDialog(
                        roomId: context.read<YBDRoomBloc>().roomId,
                        userId: userId,
                      );
                    },
                    child: GestureDetector(
                      onLongPress: () {
                        logger.v('long pressed user avatar: $name, $userId');
                        context.read<YBDRoomBloc>().setAtUserNickName(name);
                        YBDAnalyticsUtil.logEvent(
                          YBDAnalyticsEvent(
                            YBDEventName.CLICK_EVENT,
                            itemName: 'long_pressed_avatar',
                            location: YBDLocationName.ROOM_PAGE,
                          ),
                        );
                      },
                      child: YBDRoundAvatar(
                        img,
                        avatarWidth: 50,
                        needNavigation: false,
                        userId: userId,
                        scene: "C",
                        sideBorder: false,
                      ),
                    ),
                  ),
                  alignment: PlaceholderAlignment.middle)
              : WidgetSpan(child: SizedBox()),
          if (showVIP)
            WidgetSpan(
                child: SizedBox(
                  height: 48.px,
                  child: YBDVipIcon(
                    vipIcon,
                    padding: EdgeInsets.all(0),
                  ),
                ),
                alignment: PlaceholderAlignment.middle),
          if (cerInfo != null) ...[
            WidgetSpan(
                child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                YBDNetworkImage(
                  imageUrl: cerInfo!.icon!,
                  width: ScreenUtil().setWidth(34),
                )
              ],
            ))
          ],
          cerInfo == null && !barrage
              ? WidgetSpan(
                  child: Padding(
                    padding: EdgeInsets.only(left: ScreenUtil().setWidth(1)),
                    child: YBDLevelTag(level),
                  ),
                  alignment: PlaceholderAlignment.middle)
              : TextSpan(text: " "),
          TextSpan(text: " "),
          TextSpan(
            text: getName(),
            style: TextStyle(
              fontSize: ScreenUtil().setSp(24),
              color: nameColor,
            ),
          ),
          extra ?? TextSpan(text: '')
        ],
        style: TextStyle(
          fontSize: ScreenUtil().setSp(22),
          color: Colors.white,
//          height: 1.8,
        ),
      ),
    );
  }
  void builduvcxcoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
