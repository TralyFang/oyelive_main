import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/room_socket/message/common/emoji_ybd_message.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import 'package:oyelive_main/module/room/entity/room_ybd_bubble_entity.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/entity/emoji_ybd_entity.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/room_socket/message/common/text_ybd_message.dart';
import 'personal_ybd_title.dart';

typedef BoxDecoration bubbleDecorationFunc(String url);

class YBDItemText extends StatefulWidget {
  var data;
  YBDItemText(this.data);

  @override
  YBDItemTextState createState() => new YBDItemTextState();
}

class YBDItemTextState extends BaseState<YBDItemText> {
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    YBDRoomBubbleEntity? roomBubbleEntity;
    if (widget.data.equipGoods != null) {
      Map maps = json.decode(widget.data.equipGoods!);
      if (maps["BUBBLE"] != null) {
        roomBubbleEntity = YBDRoomBubbleEntity().fromJson(maps as Map<String, dynamic>);
      }
    }
    if (widget.data.content!.startsWith('<***')) {
      return Container();
    }
    YBDUserCertificationRecordCertificationInfos? cerInfo;
    try {
      cerInfo = widget.data.senderCerInfos!.first;
    } catch (e) {
      print(e);
    }

    final String? text = widget.data.content;
    final TextStyle textStyle = TextStyle(
      fontSize: ScreenUtil().setSp(24),
      color: Colors.white,
    );
    final Size txtSize = _textSize(text, textStyle);
    // print('22.12.277--_getEmoji():${_getEmoji()}');
    BoxDecoration defaultDecoration = BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
        color: Colors.black.withOpacity(0.4));
    bubbleDecorationFunc? bubbleDecoration;
    if (roomBubbleEntity != null) {
      bubbleDecoration = (String url) {
        logger.v('bubbleDecoration.message.url:$url');
        return BoxDecoration(
            // color: Colors.red.withOpacity(0.3),
            image: DecorationImage(
                scale: 4,
                // fit: BoxFit.contain,
                //6ff55d86-6b7e-4d17-8103-748e06bde709.png

                // image: CachedNetworkImageProvider(
                //     YBDImageUtil.frame(context, roomBubbleEntity.bUBBLE.animation, "A")),

                //https://s3.bmp.ovh/imgs/2022/04/02/b2a3e7cd5d08fffa.png big
                //https://s3.bmp.ovh/imgs/2022/04/07/d8af1f0633239791.png  44
                //https://s3.bmp.ovh/imgs/2022/04/07/fdf6b679e5e8adc2.png 88
                // image: CachedNetworkImageProvider(
                //   YBDImageUtil.getFullSize(context, roomBubbleEntity.bUBBLE.animation),
                //   // cacheManager: dc,
                // ),
                // scale: 1,
                image: FileImage(File(url)),
                // image: AssetImage("assets/images/item_text_default.webp"),
                // centerSlice: Rect.fromCenter(
                //     center: Offset.zero, width: ScreenUtil().setWidth(20), height: ScreenUtil().setWidth(8)))),
                // 220 180
                // centerSlice: Rect.fromLTRB(18, 16, 37, 29)));
                centerSlice: Rect.fromLTWH(25, 20, 5, 5)));
      };
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // 头像
        GestureDetector(
          onTap: () {
            YBDRoomUtil.showMiniProfileDialog(
              roomId: widget.data.roomId,
              userId: widget.data.fromUser,
            );
          },
          child: GestureDetector(
            onLongPress: () {
              logger.v('long pressed user avatar: ${widget.data.sender}, ${widget.data.fromUser}');
              BlocProvider.of<YBDRoomBloc>(context).setAtUserNickName(widget.data.sender);
              // context.bloc<YBDRoomBloc>().setAtUserNickName(widget.data.sender);
              YBDAnalyticsUtil.logEvent(
                YBDAnalyticsEvent(
                  YBDEventName.CLICK_EVENT,
                  itemName: 'long_pressed_avatar',
                  location: YBDLocationName.ROOM_PAGE,
                ),
              );
            },
            child: Container(
              margin: EdgeInsets.only(top: 10.px),
              child: YBDRoundAvatar(
                widget.data.senderImg,
                avatarWidth: 70,
                needNavigation: false,
                userId: widget.data.fromUser,
                scene: 'C',
                sideBorder: false,
              ),
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: ScreenUtil().setWidth(10),
            ),
            Container(
              margin: EdgeInsets.only(left: 10.px),
              child: YBDPersonalTitle(
                img: widget.data.senderImg,
                userId: widget.data.fromUser,
                level: widget.data.senderLevel,
                vipLevel: widget.data.senderVip,
                name: widget.data.sender,
                cerInfo: cerInfo,
                nameColor: Color(0xffD5D5D5),
                showAvatar: false,
                vipIcon: widget.data.senderVipIcon,
                sign: false,
              ),
            ),
            SizedBox(height: ScreenUtil().setWidth(0)),
            // 消息内容
            Container(
              child: Stack(
                children: [
                  StreamBuilder<String>(
                      stream: YBDDownloadCacheManager.instance.downloadStreamURL(roomBubbleEntity?.bUBBLE != null
                          ? YBDImageUtil.getFullSize(context, roomBubbleEntity!.bUBBLE?.animation)
                          : ''),
                      builder: (context, snapshot) {
                        return Container(
                          // width: ScreenUtil().setWidth(200),
                          // height: ScreenUtil().setWidth(100),
                          width: widget.data is YBDEmojiMessage
                              ? 120.px
                              : (txtSize.width + ScreenUtil().setWidth(roomBubbleEntity != null ? 80 : 50)),
                          constraints:
                              BoxConstraints(maxWidth: ScreenUtil().setWidth(460), minWidth: 52, minHeight: 42),
                          margin: EdgeInsets.only(left: ScreenUtil().setWidth(roomBubbleEntity != null ? 4 : 16)),
                          padding: EdgeInsets.symmetric(
                              vertical: ScreenUtil().setWidth(roomBubbleEntity != null ? 22 : 12),
                              horizontal: ScreenUtil().setWidth(roomBubbleEntity != null ? 30 : 22)),
                          alignment: Alignment.center,
                          decoration: (roomBubbleEntity != null && snapshot.data != null)
                              ? bubbleDecoration?.call(snapshot.data!)
                              : defaultDecoration,

                          child: widget.data is YBDTextMessage
                              ? Text(widget.data.content,
                                  // softWrap: true,
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(24),
                                    color: Colors.white,
                                    //              height: 1.8,
                                  ))
                              : FutureBuilder<String>(
                                  future: _getEmoji(),
                                  builder: (BuildContext context, AsyncSnapshot<String> data) {
                                    if (data.hasData) {
                                      // logger.v('22.12.27--data.data._getEmoji:${data.data}');
                                      return YBDImage(
                                        path: data.data,
                                        width: 55,
                                        height: 55,
                                        fit: BoxFit.cover,
                                        errorWidget: (_, a, b) =>
                                            Container(width: 55.px, height: 55.px, color: Colors.transparent),
                                      );
                                    }
                                    return Container(width: 55.px, height: 55.px, color: Colors.transparent);
                                  }),
                        );
                      }),
                ],
              ),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(10),
            ),
          ],
        ),
      ],
    );
  }
  void myBuild2tHaUoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // Here it is!
  Size _textSize(String? text, TextStyle style) {
    final TextPainter textPainter =
        TextPainter(text: TextSpan(text: text, style: style), maxLines: 2, textDirection: TextDirection.ltr)
          ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  Future<String> _getEmoji() async {
    // "{id:1002 , emojiCode:1}"
    String path = '';
    try {
      Map<String, int> map = Map<String, int>.from(json.decode(widget.data.content));
      if (map != null && map.isNotEmpty) {
        // print('22.12.27---map:$map--id:${map['id']}---emojiCode:${map['emojiCode']}');
        YBDEmojiEntity? spEmojis = await YBDSPUtil.getEmojiEntity();
        // print('22.12.27---record:${spEmojis.record}');
        for (var e in spEmojis?.record ?? []) {
          if (e.id == map['id'].toString()) {
            for (var a in e.emojiDatas) {
              if (a.id == map['emojiCode'].toString()) {
                // print('22.12.27---pic1:${a.pic}');
                path = a.pic;
                break;
              }
            }
          }
        }
      }
    } catch (e) {
      // return null;
    }
    // print('22.12.27---path:$path');
    return path;
  }
  void _getEmojiyvCe4oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  void disposeUtg1voyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDItemText oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesKOjDboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
