import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/room_socket/message/common/enter.dart';
import '../../../../common/util/image_ybd_util.dart';
import 'personal_ybd_title.dart';

class YBDItemEnter extends StatefulWidget {
  YBDEnter data;

  YBDItemEnter(this.data);

  @override
  YBDItemEnterState createState() => new YBDItemEnterState();
}

class YBDItemEnterState extends BaseState<YBDItemEnter> {
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    // logger.v('22.12.27--userLevelEnterTip:${widget.data?.userLevelEnterTip}');
    // logger.v('22.12.27--userEnterMedal:${widget.data?.userEnterMedal}');
    List<String> imageList = (widget.data.userLevelEnterTip ?? '').split(',');
    String avatarBg = imageList.last;
    String wholeBg = imageList.first;
    // logger.v('22.12.27--wholeBg1:$wholeBg---vip:${widget.data.senderVip}--lv:${widget.data.senderLevel}');
    return Container(
      constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(540)),
      margin: EdgeInsets.only(left: ScreenUtil().setWidth(0)),
      // padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(6), horizontal: ScreenUtil().setWidth(0)),
      child: Stack(
        children: [
          YBDNetworkImage(
            imageUrl: wholeBg,
            width: ScreenUtil().setWidth(550),
            fit: BoxFit.fitWidth,
            placeholder: (_, a) => Container(
              height: 60.px,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))), color: Color(0x19000000)),
            ),
            errorWidget: (_, a, b) => Container(
              height: 60.px,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))), color: Color(0x19000000)),
            ),
          ),
          Positioned(
            left: ScreenUtil().setWidth(1),
            right: 0,
            top: 0,
            bottom: 0,
            child: Row(
              children: [
                if (widget.data.userEnterMedal != null && widget.data.userEnterMedal!.isNotEmpty)
                  Container(
                    // width: 80.px,
                    height: 80.px,
                    child: YBDNetworkImage(
                      imageUrl: widget.data.userEnterMedal!,
                      fit: BoxFit.fitHeight,
                      errorWidget: (_, a, b) => Container(),
                    ),
                  ),
                SizedBox(
                  width: ScreenUtil().setWidth((widget.data.senderVip == 0 && (widget.data.senderLevel ?? 0) < 80) ? 8 : 0),
                ),
                YBDPersonalTitle(
                  img: widget.data.senderImg,
                  userId: widget.data.fromUser,
                  level: widget.data.senderLevel,
                  vipLevel: widget.data.senderVip,
                  name: widget.data.sender,
                  nameColor: Colors.white,
                  sign: false,
                  showAvatar: false,
                  showVIP: false,
                  vipIcon: widget.data.senderVipIcon,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(20),
                ),
                Text(
                  translate('entered'),
                  style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                ),
                // V2.7.4去除座驾图片
                // widget.data.carName != null && widget.data.carName.isNotEmpty
                //     ? YBDNetworkImage(
                //         imageUrl: YBDImageUtil.car(
                //           context,
                //           widget.data.carImg,
                //           'A',
                //         ),
                //         height: ScreenUtil().setWidth(58),
                //         errorWidget: (_, a, b) => Container(),
                //       )
                //     : Container()
              ],
            ),
          ),
          // if (avatarBg != null && avatarBg.isNotEmpty)
          //   Positioned(
          //     left: ScreenUtil().setWidth(6),
          //     top: ScreenUtil().setWidth(6),
          //     child: IgnorePointer(
          //       child: YBDNetworkImage(
          //         imageUrl: avatarBg ?? "",
          //         width: ScreenUtil().setWidth(64),
          //         errorWidget: (_, a, b) => Container(),
          //       ),
          //     ),
          //   ),
        ],
      ),
    );
  }
  void myBuildddDojoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatehAfXDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDItemEnter oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetTqgk6oyelive(YBDItemEnter oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
