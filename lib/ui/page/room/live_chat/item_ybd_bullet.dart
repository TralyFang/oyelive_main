import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/room_socket/message/common/bullet_ybd_message.dart';
import 'personal_ybd_title.dart';

class YBDItembullet extends StatefulWidget {
  YBDBulletMessage data;

  YBDItembullet(this.data);

  @override
  YBDItembulletState createState() => new YBDItembulletState();
}

// {"roomId":2100212,"destination":"/room/2100212","costTime":[38],"msgType":"bullet","fromUser":2100212,"toUser":-1,"mode":"m","filterVersion":null,"sender":"😦🤕🤕🤣👻🥰","senderImg":"1670380703694.jpeg","senderLevel":84,"senderVip":2,"senderVipIcon":"https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/icon_vip2_mini.webp","senderSex":2,"receiver":null,"receiverImg":null,"receiverLevel":0,"receiverVip":null,"receiverSex":null,"time":"05:58","millisTime":1673157495865,"senderCerInfos":[],"equipGoods":"{}","userLevelEnterTip":null,"userEnterMedal":null,"content":"555522"}
class YBDItembulletState extends BaseState<YBDItembullet> {
  @override
  Widget myBuild(BuildContext context) {
    print('sender:${widget.data.sender!.length} content:${widget.data.content!.length}');
    return Container(
      height: 60.px,
      constraints: BoxConstraints(maxHeight: 60.px),
      padding: EdgeInsets.only(left: 6.px, right: 20.px),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(50.px)),
        //背景
        // color: _getColor(),
        gradient: LinearGradient(colors: _getColor(), begin: Alignment.topCenter, end: Alignment.bottomCenter),
        //设置四周边框
        border: Border.all(width: _getWidth(), color: _getBorder()),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          // SizedBox(width: 6.px),
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(50.px)),
            child: YBDRoundAvatar(
              widget.data.senderImg ?? '',
              avatarWidth: 50,
              needNavigation: false,
              userId: widget.data.fromUser,
              scene: 'C',
              sideBorder: false,
            ),
          ),
          SizedBox(width: 10.px),
          Text(
            widget.data.content?.removeBlankSpace() ?? '',
            style: TextStyle(color: Colors.white, fontSize: 24.sp2),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          // SizedBox(width: 20.px),
        ],
      ),
    );
  }
  void myBuild5yDJeoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Color> _getColor() {
    int vip = widget.data.senderVip ?? 0;
    if (vip == 2) return [Color(0xff077349).withOpacity(0.9), Color(0xff077349).withOpacity(0.9)];
    if (vip == 3) return [Color(0xff0247B6), Color(0xff0247B6)];
    if (vip == 4) return [Color(0xffAA4DEB), Color(0xff5010A1)];
    if (vip == 5) return [Color(0xffFF7518), Color(0xffB20A09)];
    return [Color(0xff077349).withOpacity(0.9), Color(0xff077349).withOpacity(0.9)];
  }
  void _getColor3E2Nmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  double _getWidth() {
    int vip = widget.data.senderVip ?? 0;
    if (vip == 3) return 1.px;
    if (vip == 4) return 2.px;
    if (vip == 5) return 2.px;
    return 0;
  }

  Color _getBorder() {
    int vip = widget.data.senderVip ?? 0;
    if (vip == 3) return Color(0xff36C2FF);
    if (vip == 4) return Color(0xff36C2FF);
    if (vip == 5) return Color(0xffFFC336);
    return Color(0xff077349).withOpacity(0.9);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatejYUPwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDItembullet oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidget9XMcEoyelive(YBDItembullet oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
