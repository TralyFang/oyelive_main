
/*
 * @Author: William
 * @Date: 2022-12-26 11:34:49
 * @LastEditTime: 2023-01-11 11:40:24
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/room/live_chat/task_queue_util.dart
 */
import 'dart:async';
import 'package:synchronized/synchronized.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

typedef TaskCallback = void Function(bool success, dynamic result);
typedef TaskFutureFuc = Future Function();

///队列任务，先进先出，一个个执行
class YBDTaskQueueUtils {
  bool _isTaskRunning = false;
  List<YBDTaskItem> _taskList = [];
  final Lock _lock = Lock();

  bool get isTaskRunning => _isTaskRunning;

  void endTask() {
    _taskList.first.completer!.complete(true);
    _taskList.removeAt(0);
    _isTaskRunning = false;
    logger.v('23.1.11---endTask _isTaskRunning false ${_lock.locked}, ${_taskList.length}');
    //递归任务
    _doTask();
  }
  void endTaskm0vVYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void endTaskItem(YBDTaskItem task) {
    _taskList.remove(task);
    _isTaskRunning = false;
    logger.v('23.1.11---endTask _isTaskRunning false ${_lock.locked}');
    //递归任务
    _doTask();
  }
  void endTaskItemE5amooyelive(YBDTaskItem task) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<YBDTaskItem> get taskItems => _taskList;

  Future addTask(TaskFutureFuc futureFunc, {dynamic param}) async {
    // return _lock.synchronized(() async {
    Completer completer = Completer();
    YBDTaskItem taskItem = YBDTaskItem(
      futureFunc,
      (bool success, result) {
        if (success) {
          logger.v('23.1.11---completer.complete ${_lock.locked}');
          completer.complete(result);
        } else {
          completer.completeError(result);
        }
        _taskList.removeAt(0);
        _isTaskRunning = false;
        logger.v('23.1.11---_isTaskRunning false ${_lock.locked}');
        //递归任务
        _doTask();
      },
      param: param,
      completer: completer,
    );
    _lock.synchronized(() {
      _taskList.add(taskItem);
    });

    await _doTask();
    return completer.future;
    // });
  }
  void addTasklgqd5oyelive(TaskFutureFuc futureFunc, {dynamic param})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<void> _doTask() async {
    if (_isTaskRunning) return;
    if (_taskList.isEmpty) return;

    //获取先进入的任务
    YBDTaskItem task = _taskList[0];
    _isTaskRunning = true;
    logger.v('23.1.11---_isTaskRunning true');
    try {
      //执行任务
      var result = await task.futureFun();
      logger.v('23.1.11---futureFun');
      //完成任务
      task.callback(true, result);
    } catch (_) {
      logger.v('23.1.11---task.callback fail:${_.toString()}');
      task.callback(false, _.toString());
    }
  }
  void _doTaskIZJZuoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

///任务封装
class YBDTaskItem {
  final TaskFutureFuc futureFun;
  final TaskCallback callback;
  final dynamic param;
  final Completer? completer;
  const YBDTaskItem(this.futureFun, this.callback, {this.param, this.completer});
}

/// -----------------------------------new
typedef TaskEventFunction = Future Function();

class YBDTaskQueue {
  /// 任务编号队列
  List<String> _actionQueue = [];

  /// 任务队列
  Map<String, TaskEventFunction> _actionMap = {};

  bool dealing = false;

  /// 指定taskId 为 -1 则不用去重
  String add(TaskEventFunction task, {String? taskId}) {
    String? id = taskId;
    id ??= task.hashCode.toString();
    bool isContains = false;

    if (taskId != '-1') {
      isContains = _actionQueue.contains(id);
    } else {
      id = task.hashCode.toString();
    }

    if (!isContains) {
      _actionQueue.add(id);
      _actionMap[id] = task;
    }
    return id;
  }
  void adddNOOaoyelive(TaskEventFunction task, {String? taskId}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 这里需注意，add的时taskId为-1，那就直接把task传入，add时有返回，可以对应
  bool remove(TaskEventFunction task, {String? taskId}) {
    String? id = taskId;
    id ??= task.hashCode.toString();
    if (_actionQueue.contains(id)) {
      _actionMap.remove(id);
      return _actionQueue.remove(id);
    }
    return false;
  }

  /// 是否队列中包含该任务
  /// [task] 与 [taskId] 应该只传一个
  bool containers({TaskEventFunction? task, String? taskId}) {
    assert(task != null || taskId != null);
    String? id = taskId;
    id ??= task.hashCode.toString();
    return _actionQueue.contains(taskId);
  }
  void containersX0FBboyelive({TaskEventFunction? task, String? taskId}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void startLoop() async {
    if (dealing || _actionQueue.isEmpty) {
      return;
    }
    dealing = true;
    String taskId = _actionQueue.first;
    TaskEventFunction? callback = _actionMap[taskId];
    if (callback == null) {
      _actionQueue.remove(taskId);
      return;
    }

    try {
      await callback();
    } catch (e) {
      logger.v('_actionQueue 出错 $e');
    } finally {
      _actionQueue.remove(taskId);
      _actionMap.remove(taskId);
      dealing = false;
      if (_actionQueue.isNotEmpty) {
        startLoop();
      }
    }
  }
}
