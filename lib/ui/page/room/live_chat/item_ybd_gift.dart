import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/ext/enum_ext/ui_ybd_ext.dart';
import 'package:oyelive_main/common/util/intl_ybd_lang_util.dart';
import 'package:oyelive_main/ui/widget/intl_ybd_positioned.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/room_socket/message/common/enter.dart';
import '../../../../common/room_socket/message/common/gift.dart';
import '../../../../common/room_socket/room_ybd_socket_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/string_ybd_util.dart';
import 'personal_ybd_title.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

class YBDItemGift extends StatefulWidget {
  YBDGift data;

  YBDItemGift(this.data);

  @override
  YBDItemGiftState createState() => new YBDItemGiftState();
}

class YBDItemGiftState extends BaseState<YBDItemGift> {
  getName(name) {
    String? cutName = YBDStringUtil.subString(name, 10);
    return cutName;
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Stack(
      alignment: IntlAlignment.centerRight,
      children: <Widget>[
        Container(
          width: ScreenUtil().setWidth(540),
          margin: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
          padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(9), horizontal: ScreenUtil().setWidth(12)),
          decoration: BoxDecoration(
//              boxShadow: [BoxShadow(blurRadius: ScreenUtil().setWidth(11), color: Color(0x7fffffff))],
              border: Border.all(color: Color(0x4c47CDCC), width: ScreenUtil().setWidth(2)),
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
              color: Colors.black.withOpacity(0.4)),
          child: Row(
            children: [
              Container(
                constraints: BoxConstraints(
                  maxWidth: ScreenUtil().setWidth(368),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: ScreenUtil().setWidth(50),
                      child: YBDPersonalTitle(
                        img: widget.data.senderImg,
                        userId: widget.data.fromUser,
                        name: widget.data.sender,
                        level: widget.data.senderLevel,
                        vipLevel: widget.data.senderVip,
                        nameColor: Color(0xffD1D1D1),
                        sign: false,
                        vipIcon: widget.data.senderVipIcon,
                      ),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          width: ScreenUtil().setWidth(54),
                        ),
                        Text.rich(
                          TextSpan(
                              children: [
                                TextSpan(text: translate("send_to"), style: TextStyle(color: Color(0xffD9D9D9))),
                                TextSpan(text: translate(" "), style: TextStyle(color: Color(0xffD9D9D9))),
                                TextSpan(
                                  text: getName(widget.data.receiver),
                                ),
                              ],
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(22),
                                color: Colors.white,
                              )),
                          maxLines: 1,
                          overflow: TextOverflow.clip,
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(10),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(120),
              )
            ],
          ),
        ),
        Transform.translate(
          offset: YBDIntlOffset(ScreenUtil().setWidth(90), 0),
          child: Container(
            width: ScreenUtil().setWidth(220),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.network(
                  YBDImageUtil.gift(context, widget.data.imgUrl, "B"),
                  width: ScreenUtil().setWidth(134),
                  fit: BoxFit.fitHeight,
                  height: ScreenUtil().setWidth(80),
                ),
                Text(
                  "x ${widget.data.num}",
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(28), fontWeight: FontWeight.w600, color: Color(0xffFFD87A)),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
  void myBuildDXB8Poyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatejdEHwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDItemGift oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetNdWifoyelive(YBDItemGift oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
