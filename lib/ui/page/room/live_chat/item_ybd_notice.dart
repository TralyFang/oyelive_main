/*
 * @Author: William
 * @Date: 2022-03-28 10:02:47
 * @LastEditTime: 2022-12-25 16:31:54
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/room/live_chat/item_notice.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux/redux.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/room_socket/message/common/internal.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../util/message_ybd_util.dart';

class YBDItemNotice extends StatefulWidget {
  YBDInternal? internal;
  String messageContent;
  YBDItemNotice({
    this.messageContent: '',
    this.internal,
  });

  @override
  YBDItemNoticeState createState() => new YBDItemNoticeState();
}

class YBDItemNoticeState extends BaseState<YBDItemNotice> {
  String makeMessage() {
    Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);

    logger.v("makeNotice");
    if (widget.messageContent.isNotEmpty || (widget.internal!.type != 1 && widget.internal!.type != 2))
      return widget.messageContent;
    //* 操作类型 1-禁言 2-踢人 3-黑名单 4-公告 mode-m:“某某人被禁言”的消息 u:提示被禁言/被踢/已被加入黑名单
    return YBDMessageUtil.forbiddenTextWithInternal(widget.internal!, store!.state.bean);
  }
  void makeMessageSsgakoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return makeMessage().isEmpty
        ? Container()
        : Container(
            constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(540)),
            margin: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(12), horizontal: ScreenUtil().setWidth(14)),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
                color: Colors.black.withOpacity(0.4)),
            child: Text.rich(TextSpan(children: [
              TextSpan(text: makeMessage()),
            ], style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff7DFAFF)))),
          );
  }
  void myBuildiqibjoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
