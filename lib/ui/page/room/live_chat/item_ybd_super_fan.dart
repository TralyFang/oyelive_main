import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/room_socket/message/common/super_ybd_fans_message.dart';
import '../../../../common/util/string_ybd_util.dart';

class YBDItemSuperFan extends StatefulWidget {
  YBDSuperFansMessage data;

  YBDItemSuperFan(this.data);

  @override
  YBDItemSuperFanState createState() => new YBDItemSuperFanState();
}

class YBDItemSuperFanState extends BaseState<YBDItemSuperFan> with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation<double> animation;

  getName(name) {
    String? cutName = YBDStringUtil.subString(name, 10);
    return cutName;
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    if (widget.data.content?.fans!.length == 0) {
      return Container();
    }
    return Container(
      constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(540)),
      margin: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
      padding: EdgeInsets.only(top: 20.px, bottom: 20.px, left: 20.px),
      decoration: BoxDecoration(
          // border: Border.all(color: Color(0x4c47CDCC), width: ScreenUtil().setWidth(2)),
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
          color: Colors.black.withOpacity(0.4)),
      child: Row(
        children: [
          // SizedBox(
          //   height: ScreenUtil().setWidth(60),
          // ),
          Image.asset('assets/images/liveroom/icon_Trophy_big@2x.webp', width: 48.px),
          SizedBox(
            width: ScreenUtil().setWidth(10),
          ),
          Container(
            constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(400)),
            child: Text.rich(
              TextSpan(style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff7DFAFF)), children: [
                TextSpan(text: 'The top 1 gifter of the room is '),
                TextSpan(
                    text: '${getName(widget.data.content?.fans![0]?.nickname)} '.removeBlankSpace(),
                    style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xffFFE824))),
                TextSpan(text: 'now. Go to surpass~'),
              ]),
              // overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }
  void myBuildb6ACgoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animationController = AnimationController(duration: Duration(milliseconds: 500), vsync: this);
    animation = new Tween(begin: 1.0, end: 0.0).animate(animationController)
      ..addListener(() {
        if (mounted) setState(() {});
      });
    animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animationController.reverse();
      }
      if (status == AnimationStatus.dismissed) {
        animationController.forward();
      }
    });
    animationController.forward();
  }
  void initStatexBPd5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    animationController.dispose();
    super.dispose();
  }
  void disposeAZFP6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDItemSuperFan oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesECwEOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
