import 'dart:async';


import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/room_socket/message/common/ludo_ybd_public_message.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/room/entity/game_ybd_query_model.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_type_ext.dart';
import 'package:oyelive_main/ui/page/room/room_ybd_helper.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';

class YBDLudoNoticeItem extends StatelessWidget {
  YBDLudoPublicContent message;

  int roomId;
  YBDLudoNoticeItem(this.message, this.roomId);

  @override
  Widget build(BuildContext context) {
    Widget textRich;

    if (message.type == 0) {
      textRich = Text.rich(
        TextSpan(children: [
          TextSpan(text: "${message.normalText ?? ""}", style: TextStyle(color: YBDHexColor(message.normalColor))),
          TextSpan(text: "${message.linkName ?? ""}", style: TextStyle(color: YBDHexColor(message.linkColor))),
        ]),
        style: TextStyle(fontSize: ScreenUtil().setSp(22), color: YBDHexColor(message.normalColor)),
      );
    } else if (message.type == 1) {
      textRich = Text.rich(
        TextSpan(children: [
          TextSpan(text: "Congratulations to "),
          TextSpan(text: "${message.userName ?? ""}", style: TextStyle(color: YBDHexColor(message.userColor))),
          TextSpan(text: " for his "),
          TextSpan(
              text: "${YBDNumericUtil.format(message.revenue ?? 0)}",
              style: TextStyle(color: YBDHexColor(message.revenueColor))),
          TextSpan(text: " ${message.currency ?? ""}", style: TextStyle(color: YBDHexColor(message.currencyColor))),
          TextSpan(text: "  award in the "),
          TextSpan(text: "${message.gameName}", style: TextStyle(color: YBDHexColor(message.gameColor))),
          TextSpan(text: "  "),
          TextSpan(text: "${message.linkName ?? ""}", style: TextStyle(color: YBDHexColor(message.linkColor))),
        ]),
        style: TextStyle(fontSize: ScreenUtil().setSp(22), color: YBDHexColor(message.normalColor)),
      );
    } else {
      textRich = Text.rich(
        TextSpan(children: [
          TextSpan(text: "[${message.userName ?? ""}]", style: TextStyle(color: YBDHexColor(message.userColor))),
          TextSpan(text: " ${translate('ludo_playing')} "),
          TextSpan(text: "${message.gameName}", style: TextStyle(color: YBDHexColor(message.gameColor))),
          TextSpan(text: ", ${translate('ludo_has_won')} "),
          TextSpan(
              text: "${YBDNumericUtil.format(message.revenue ?? 0)}",
              style: TextStyle(color: YBDHexColor(message.revenueColor))),
          TextSpan(text: " ${message.currency ?? ""}", style: TextStyle(color: YBDHexColor(message.currencyColor))),
          TextSpan(text: "... "),
          TextSpan(text: "${message.linkName ?? ""}", style: TextStyle(color: YBDHexColor(message.linkColor))),
        ]),
        style: TextStyle(fontSize: ScreenUtil().setSp(22), color: YBDHexColor(message.normalColor)),
      );
    }

    return GestureDetector(
      onTap: () {
        logger.v("chat notice ludo link ${message.link}, ludoweburl: ${GameType.GameTypeLudo.info()?.url}");
        if (message.link != null && message.link!.isNotEmpty) {
          if (message.link!.contains("flutter://room")) {
            Uri? u;
            try {
              u = Uri.parse(message.link!);
            } catch (e) {
              logger.e('message.link parse fail : $e');
            }
            if (u?.queryParameters == null) {
              return;
            }
            int? roomId = u!.queryParameters.keyToInt('roomId');
            int? tag = u.queryParameters.keyToInt('funcType');
            String? gameUrl = u.queryParameters['url'];
            if (gameUrl?.isNotEmpty ?? false) {
              // Open gameUrl from the room
              YBDRoomUtil.showH5Game(context, gameUrl);
            } else if (roomId != -1) {
              YBDModuleCenter.instance().enterRoom(roomId: roomId, type: YBDModuleCenter.enterRoomType(tag));
            }
          } else {
            print("sdihi${message.link}");
            if (message.type == 1 && roomId == YBDUserUtil.getUserIdSync) {
              //tpgo消息，主播不能跳转
              YBDToastUtil.toast("Only audience can jump to TP GO");
              return;
            }
            print("sdihixxx${message.link}");

            YBDNavigatorHelper.navigateTo(context, message.link);
          }
        }
      },
      child: Container(
        constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(500)),
        margin: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14), vertical: ScreenUtil().setWidth(20)),
        decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.4),
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
        child: textRich,
      ),
    );
  }
  void buildauCr1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
