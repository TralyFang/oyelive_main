import 'dart:async';


import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:redux/redux.dart';
import 'package:synchronized/synchronized.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/room_socket/message/common/emoji_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/ludo_ybd_public_message.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/live_chat/controller/tp_ybd_msg_controller.dart';
import 'package:oyelive_main/ui/page/room/live_chat/item_ybd_ludo_notice.dart';
import 'package:oyelive_main/ui/page/room/live_chat/task_ybd_queue_util.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/room_socket/message/base/message.dart';
import '../../../../common/room_socket/message/common/bullet_ybd_message.dart';
import '../../../../common/room_socket/message/common/enter.dart';
import '../../../../common/room_socket/message/common/follow_ybd_display_message.dart';
import '../../../../common/room_socket/message/common/gift.dart';
import '../../../../common/room_socket/message/common/internal.dart';
import '../../../../common/room_socket/message/common/room_ybd_manager_display_message.dart';
import '../../../../common/room_socket/message/common/super_ybd_fans_message.dart';
import '../../../../common/room_socket/message/common/teenpatti_ybd_winner_message.dart';
import '../../../../common/room_socket/message/common/text_ybd_message.dart';
import '../../../../common/room_socket/message/room_ybd_message_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/barrage/barrage.dart';
import '../util/message_ybd_util.dart';
import '../util/room_ybd_util.dart';
import 'item_ybd_bullet.dart';
import 'item_ybd_enter.dart';
import 'item_ybd_follow.dart';
import 'item_ybd_follow_tips.dart';
import 'item_ybd_gift.dart';
import 'item_ybd_notice.dart';
import 'item_ybd_super_fan.dart';
import 'item_ybd_text.dart';

/// 房间公聊列表页面事件的回调
typedef LiveChatUserItemCallback = Function(YBDUserInfo);

class YBDLiveChat extends StatefulWidget {
  int? roomId;
  String? talentPic;
  YBDUserInfo? mUserInfo;

  /// 点击用户列表项头像事件的回调
  final LiveChatUserItemCallback? userItemCallback;

  final double? height;

  YBDLiveChat({
    this.roomId,
    this.talentPic,
    this.userItemCallback,
    this.mUserInfo,
    this.height,
  });

  @override
  YBDLiveChatState createState() => new YBDLiveChatState();
}

class YBDLiveChatState extends BaseState<YBDLiveChat> with TickerProviderStateMixin {
  /// 键盘高度
  double _keyboardHeight = 0.0;

  List<Widget> messageItems = [];

  ScrollController _scrollController = new ScrollController();

  Timer? followtips;

  int? currentTopsFansId;

  GlobalKey<YBDBarrageState> _barrageKey = GlobalKey<YBDBarrageState>();

  final YBDReadCtrl rc = Get.put(YBDReadCtrl());
  Timer? textRoutine;
  final Lock _lock = Lock();

  // 当前消息
  YBDMessage? curMsg;

  // 普通用户进场
  late AnimationController _userInController;

  // VIP用户进场
  late AnimationController _vipInController;

  // 普通用户进场队列
  List<YBDMessage> _userList = [];
  YBDTaskQueueUtils _userQueue = YBDTaskQueueUtils();

  // VIP用户进场队列
  List<YBDMessage> _vipList = [];
  YBDTaskQueueUtils _vipQueue = YBDTaskQueueUtils();

  // 主播
  YBDEnter? hostEnter;

  StreamSubscription<YBDMessage>? _listenMessage;

  @override
  Widget myBuild(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(720),
      child: _content(),
    );
  }
  void myBuild5Zq00oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _content() {
    return Listener(
      onPointerDown: (PointerDownEvent event) {
        rc.setTouch(true);
      },
      onPointerUp: (PointerUpEvent event) {
        rc.setTouch(false);
      },
      child: Stack(
        children: <Widget>[
          ShaderMask(
            shaderCallback: (Rect rect) {
              return LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.purple, Colors.transparent, Colors.transparent, Colors.transparent],
                stops: [0.0, 0.05, 0.9, 1.0], // 10% purple, 80% transparent, 10% purple
              ).createShader(rect);
            },
            blendMode: BlendMode.dstOut,
            child: ListView.builder(
              padding: EdgeInsets.only(bottom: _bottomPadding(_keyboardHeight)),
              // physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              controller: _scrollController,
              itemBuilder: (_, int index) => Row(
                children: <Widget>[messageItems[index]],
              ),
              itemCount: messageItems.length,
              // 填充容器空间，拦截 PageView 的滑动事件
              shrinkWrap: true,
            ),
          ),
          _enterBarrage(),
          // 跳到最底部
          Positioned(bottom: ScreenUtil().setWidth(5), left: ScreenUtil().setWidth(10), child: _goToNewMsgBtn())
        ],
      ),
    );
  }
  void _content7ZON4oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void msgHandler(YBDMessage event) {
    Widget? item;
    logger.v('message event is ${event}');
    curMsg = event;
    if (event is YBDInternal) {
      if (YBDMessageUtil.shouldShowMsgWithInternal(event)) {
        // 发送人为 null 时不显示
        item = YBDItemNotice(
          internal: event,
        );
      }

      if (mounted) {
        // 被禁言弹框, 主播离线弹框
        YBDMessageUtil.showDialogWithInternal(context, event, widget.mUserInfo, widget.roomId);
      }
    }
    if (event is YBDEnter) {
      logger.v('22.12.25---_getLength1:${_getLength(0)}---_userList:$_userList}');
      logger.v('22.12.25---_getLength2:${_getLength(1)}---_vipList:$_vipList}');
      logger.v('22.12.29-----id:${event.fromUser} vip:${event.senderVip}');
      if ((event.senderVip ?? 0) == 0) {
        if (_userList.length < _getLength(0)) {
          _userList.add(curMsg!);
          _userQueue.addTask(_playUserInAnimation);
        }
      } else {
        if (_vipList.length < _getLength(1)) {
          // if (widget.roomId == widget.mUserInfo.id) hostEnter = event;
          if (mounted) {
            // _vipInController = AnimationController(
            //   duration: Duration(milliseconds: _getDuration(event.senderVip)),
            //   vsync: this,
            // );
            _vipInController.duration = Duration(milliseconds: _getDuration(event.senderVip ?? 0));
            // setState(() {});
          }
          _vipList.add(curMsg!);
          logger.v('22.12.25---added---_vipList:$_vipList}');
          _vipQueue.addTask(_playVIPInAnimation);
        }
      }
      // item = YBDItemEnter(event);
    }
    if (event is YBDFollowDisplayMessage) {
      item = YBDItemFollow(
        event,
      );
    }
    if (event is YBDGift) {
      item = GestureDetector(
        onTap: () {
          _clickedItemUserAvatar(event.fromUser);
        },
        child: YBDItemGift(event),
      );
    }
    if (event is YBDTextMessage) {
      item = GestureDetector(
        onLongPress: () {
          logger.v('long press live chat');
          YBDDialogUtil.showReportUserDialog(
            context,
            event.fromUser,
            event.sender,
          );
        },
        child: YBDItemText(event),
      );
    }
    if (event is YBDBulletMessage) {
      logger.v('get YBDBulletMessage message ${event.content}');
      YBDItembullet itemBullet = YBDItembullet(event);
      if (mounted) _barrageKey.currentState?.addBarrageWidget(itemBullet);
    }
    if (event is YBDSuperFansMessage) {
      try {
        if ((event.content?.fans?.length ?? 0) > 0 && currentTopsFansId != event.content?.fans?[0]?.senderId) {
          currentTopsFansId = event.content?.fans?[0]?.senderId;
          item = YBDItemSuperFan(event);
        }
      } catch (e) {
        logger.v(e);
      }
    }
    if (event is YBDRoomManagerDisplayMessage) {
      /**
         * 1: promote an user to be room manager; 2: demote room manager
         */
      if (event.operateType == 1) {
        item = YBDItemNotice(
          messageContent: "${event.receiver} ${translate('become_guardian')}",
        );
      }
    }
    if (event is YBDPublishTeenPattiWinnerMessage) {
      Get.find<YBDTpMsgController>().addPattiWinners(event.content);
    }
    if (event is YBDLudoPublicMessage) {
      List<YBDLudoPublicContent>? pattiWinners = event.content;
      item = Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(pattiWinners?.length ?? 0, (int index) => YBDLudoNoticeItem(pattiWinners![index], widget.roomId ?? -1)),
      );
    }
    if (event is YBDEmojiMessage) {
      logger.v('22.12.29---event:${event.toJson()}');
      item = YBDItemText(event);
    }
    if (item != null) {
      rc.setUnRead(true);
      messageItems.add(item);
      if (mounted) {
        // 判断当前是否在最底部  若不在则展示[下滑到底部]按钮  若是则按原逻辑处理  是否有新消息加入且不在最底部
        setState(() {});
        if (rc.isInBottom.value) {
          Future.delayed(Duration(milliseconds: 10), () {
            try {
              _lock.synchronized(() {
                _scrollController.animateTo(
                  _scrollController.position.maxScrollExtent,
                  duration: Duration(milliseconds: 250),
                  curve: Curves.ease,
                );
              });
            } catch (e) {
              logger.e('scroll live chat error : $e');
            }
          });
        }
      }
    }
  }

  void setListener() {
    _listenMessage = YBDRoomMessageHelper.getInstance().messageOutStream.listen((YBDMessage event) {
      msgHandler(event);
    });
    // 只添加主播开播时的进场消息
    Future.delayed(Duration(milliseconds: 3000), () {
      YBDLiveService.instance.data.startPageMsgList.forEach((element) {
        if (element is YBDEnter) {
          msgHandler(element);
        }
      });
    });
    // 滚动监听
    _scrollController.addListener(() {
      logger.v(
          '22.2.16--cur:${_scrollController.offset.floor()}--max:${_scrollController.position.maxScrollExtent.floor()}');
      if (_scrollController.position.extentAfter < ScreenUtil().setWidth(180)) {
        rc.setUnRead(false);
        rc.setBottom(true);
      } else {
        rc.setBottom(false);
      }
    });
    textRoutine = Timer.periodic(Duration(milliseconds: 5000), (_) {
      if (!rc.onTouch.value) _scrollController.jumpTo(_scrollController.offset + 0.0001);
    });
  }
  void setListenerNpZoeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击列表项头像的处理方法
  void _clickedItemUserAvatar(int? userId) {
    logger.v('clicked user : $userId');

    if (null != widget.userItemCallback) {
      YBDUserInfo fromUser = YBDUserInfo();
      fromUser.id = userId;
      widget.userItemCallback!(fromUser);
    }
  }
  void _clickedItemUserAvatartQvyWoyelive(int? userId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void setFollowTips() {
    followtips = Timer.periodic(Duration(minutes: 10), (Timer timer) {
      if (!queryUserIsFollowing()) {
        messageItems.add(YBDItemFollowTips(
          taltentId: widget.roomId,
          talentHead: widget.talentPic,
        ));

        setState(() {});
      }
    });
  }

  addWarningTips() {
    messageItems.add(YBDItemNotice(
      messageContent: translate('app_waring'),
    ));
  }

  bool queryUserIsFollowing() {
    if (!mounted) return false;
    final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    if (store.state.bean!.id == widget.roomId) {
      return true;
    }
    return store.state.followedIds!.contains(widget.roomId);
  }
  void queryUserIsFollowingiYk0ooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    setUserController();
    setVIPController();
    setListener();
    setFollowTips();
    addWarningTips();
    // addHostEnter();
  }

  @override
  void dispose() {
    followtips?.cancel();
    textRoutine?.cancel();
    Get.delete<YBDReadCtrl>();
    _scrollController.dispose();
    _userInController.dispose();
    _vipInController.dispose();
    _listenMessage?.cancel();
    _listenMessage = null;
    super.dispose();
  }
  void disposeAJv2Toyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeMetrics() {
    final double bottomInset = WidgetsBinding.instance!.window.viewInsets.bottom;
    // 过滤负数
    final double bottomInsetDp = _nonNegative(bottomInset / (ScreenUtil().pixelRatio ?? 0));

    // 弹出键盘时隐藏tp消息
    Get.find<YBDTpMsgController>().updateKeyBoard(bottomInsetDp > 0);

    logger.v('window bottomInset: $bottomInset, $bottomInsetDp');
    rc.setBottom(true);
    setState(() => _keyboardHeight = bottomInsetDp);
    // 显示底部消息
    _scrollToBottom();
  }

  /// 显示底部消息
  void _scrollToBottom() {
    if (!mounted) return;
    Future.delayed(Duration(milliseconds: 3), () {
      try {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: Duration(milliseconds: 250),
          curve: Curves.ease,
        );
      } catch (e) {
        logger.e('scroll live chat error : $e');
      }
    });
  }
  void _scrollToBottomSDTN8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击查看最新消息的按钮
  Widget _goToNewMsgBtn() {
    double extraHeight = Platform.isIOS ? 0 : (ScreenUtil().screenHeight * 2);
    return GestureDetector(
      onTap: () {
        logger.v('22.2.16--tap the gotobottom btn');
        _scrollController.animateTo(
          // The maximum in-range value for [pixels].
          // The actual [pixels] value might be [outOfRange].
          _scrollController.position.maxScrollExtent + extraHeight,
          duration: Duration(milliseconds: 1000),
          curve: Curves.ease,
        );
        rc.setBottom(true);
      },
      child: Obx(() => AnimatedContainer(
            duration: Duration(milliseconds: 300),
            width: ScreenUtil().setWidth(120),
            height: (!rc.isInBottom.value && rc.haveUnreadMsg.value) ? ScreenUtil().setWidth(40) : 0,
            child: Center(child: Image.asset('assets/images/new_msg.webp')),
          )),
    );
  }
  void _goToNewMsgBtnocVkRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 进场特效和弹幕
  Widget _enterBarrage() {
    return Container(
      width: 720.px,
      // height: 770.px,
      // color: Colors.pink,
      child: Column(
        children: [
          // 普通用户
          _userIn(),
          // VIP用户
          _vipUserIn(),
          // 弹幕
          Container(
            height: 255.px,
            // color: Colors.green,
            child: YBDBarrage(key: _barrageKey),
          ),
        ],
      ),
    );
  }

  // 普通用户进场
  Widget _userIn() {
    return Transform.translate(
      offset: TweenSequence<Offset>(
        <TweenSequenceItem<Offset>>[
          TweenSequenceItem<Offset>(
            tween: Tween<Offset>(begin: Offset(0.0, 20.px), end: Offset.zero).chain(CurveTween(curve: Curves.ease)),
            weight: 1,
          ),
          TweenSequenceItem<Offset>(
            tween: ConstantTween<Offset>(Offset.zero),
            weight: 3,
          ),
          TweenSequenceItem<Offset>(
            tween:
                Tween<Offset>(begin: Offset.zero, end: Offset(-720.px, 0.0)).chain(CurveTween(curve: Curves.easeOut)),
            weight: 1,
          ),
        ],
      ).animate(_userInController).value,
      child: Container(
        width: 720.px,
        height: 70.px,
        child: (_userList.isNotEmpty && (_userList.first is YBDEnter)) ? YBDItemEnter(_userList.first as YBDEnter) : Container(),
      ),
    );
  }
  void _userInLAm0moyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<void> _playUserInAnimation() async {
    logger.v('23.1.6---user in :${_userList.length}---status:${_userInController.status} mounted:$mounted');
    logger.v('22.12.25---curMsg:${curMsg?.msgType}');
    if (mounted && _userInController.status == AnimationStatus.dismissed) {
      logger.v('23.1.5---user forward---status:${_userInController.status}');
      await _userInController.forward().whenComplete(() {
        logger.v('23.1.5---user reset---status:${_userInController.status}');
        return _userInController.reset();
      });
    }
  }

  Future<void> _playVIPInAnimation() async {
    logger.v(
        '23.1.5---_playVIPInAnimation vipList:${_vipList.length}--first:${_vipList.first.fromUser}---status:${_vipInController.status}');
    if (mounted && _vipInController.status == AnimationStatus.dismissed) {
      logger.v('23.1.5---_vip forward---status:${_vipInController.status}');
      await _vipInController.forward().whenComplete(() {
        logger.v('23.1.5---_vip reset---status:${_vipInController.status}');
        return _vipInController.reset();
      });
    }
  }
  void _playVIPInAnimationgO4GRoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // vip用户进场
  Widget _vipUserIn() {
    return Transform.translate(
      offset: TweenSequence<Offset>(
        <TweenSequenceItem<Offset>>[
          TweenSequenceItem<Offset>(
            tween: Tween<Offset>(begin: Offset(720.px, 0), end: Offset.zero).chain(CurveTween(curve: Curves.ease)),
            weight: 1,
          ),
          TweenSequenceItem<Offset>(
            tween: ConstantTween<Offset>(Offset.zero),
            weight: 3,
          ),
          TweenSequenceItem<Offset>(
            tween:
                Tween<Offset>(begin: Offset.zero, end: Offset(-720.px, 0.0)).chain(CurveTween(curve: Curves.easeOut)),
            weight: 1,
          ),
        ],
      ).animate(_vipInController).value,
      child: Container(
        width: 720.px,
        height: 70.px,
        child: (_vipList.isNotEmpty && (_vipList.first is YBDEnter)) ? YBDItemEnter(_vipList.first as YBDEnter) : Container(),
      ),
    );
  }

  // 获取进场动画时长
  int _getDuration(int index) {
    String? enterDuration = YBDCommonUtil.getRoomOperateInfo().enterDuration;
    List<String> props = enterDuration?.split('/') ?? [];
    int userInDuration = YBDNumericUtil.doubleToInt(YBDNumericUtil.stringToDouble(props[index]) * 1000);
    logger.v('22.12.26---userInDuration:$userInDuration');
    return userInDuration;
  }
  void _getDuration2Ylv2oyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 获取进场队列长度
  int _getLength(int index) {
    String? enterLength = YBDCommonUtil.getRoomOperateInfo().enterLength;
    List<String> props = enterLength?.split('/') ?? [];
    int length = YBDNumericUtil.stringToInt(props[index]);
    logger.v('22.12.26---enterLength:$enterLength');
    return length;
  }

  // 添加主播进场消息
  void addHostEnter() {
    // 主播插入一条进房间消息
    // 从开播页进入房间页漏掉了

    Future.delayed(Duration(seconds: 2));
    if (widget.roomId == widget.mUserInfo?.id && hostEnter != null) {
      if ((widget.mUserInfo?.vip ?? 0) == 0) {
        _userList.add(hostEnter!);
        _userQueue.addTask(_playUserInAnimation).then((result) {
          _userList.removeAt(0);
        });
      } else {
        if (mounted)
          _vipInController = AnimationController(
            duration: Duration(milliseconds: _getDuration(widget.mUserInfo?.vip ?? 0)),
            vsync: this,
          );
        _vipList.add(hostEnter!);
        _vipQueue.addTask(_playVIPInAnimation).then((result) {
          _vipList.removeAt(0);
        });
      }
    }
  }

  // 设置普通用户进场控制器
  void setUserController() {
    _userInController = AnimationController(duration: Duration(milliseconds: _getDuration(0)), vsync: this);
    _userInController.addListener(() {
      if (mounted) setState(() {});
    });
    _userInController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        if (_userList.isNotEmpty) _userList.removeAt(0);
        logger.v('22.12.26--addTask:_userList isNotEmpty:${_userList.length}');
      }
    });
  }
  void setUserController7tRCXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 设置VIP用户进场控制器
  void setVIPController() {
    _vipInController = AnimationController(duration: Duration(milliseconds: _getDuration(1)), vsync: this);
    _vipInController.addListener(() {
      if (mounted) setState(() {});
    });
    _vipInController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        if (_vipList.isNotEmpty) _vipList.removeAt(0);
        logger.v('22.12.26--addTask:_vipList isNotEmpty:${_vipList.length}');
      }
    });
  }

  /// 弹出键盘时底部缩进
  double _bottomPadding(double keyboardHeight) {
    // 没显示键盘
    if (null == keyboardHeight) {
      return 0.0;
    }

    // 显示音乐播放器时的缩进
    if (Platform.isAndroid && YBDRoomUtil.isTalent(widget.roomId)) {
      // 聊天列表的底部padding: 键盘高度减播放器的高度
      // 过滤负数
      return _nonNegative(keyboardHeight - ScreenUtil().setWidth(124));
    }

    // 过滤负数
    return _nonNegative(keyboardHeight);
  }

  /// 过滤负数
  double _nonNegative(double value) {
    double result = value;

    if (null == result || result < 0.0) {
      // 过滤负数
      result = 0.0;
    }

    return result;
  }
}

class YBDReadCtrl extends GetxController {
  RxBool isInBottom = true.obs;
  RxBool haveUnreadMsg = false.obs;
  RxBool onTouch = false.obs;
  setBottom(bool v) {
    isInBottom.value = v;
  }

  setUnRead(bool v) {
    haveUnreadMsg.value = v;
  }

  setTouch(bool v) {
    onTouch.value = v;
  }
}
