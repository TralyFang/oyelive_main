
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/common/gift.dart';
import 'package:oyelive_main/common/room_socket/message/room_ybd_message_helper.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/combo/combo_ybd_tips.dart';
import 'package:oyelive_main/ui/page/room/live_chat/task_ybd_queue_util.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';

class YBDRoomGiftSlideOut extends StatefulWidget {
  const YBDRoomGiftSlideOut({Key? key}) : super(key: key);

  @override
  State<YBDRoomGiftSlideOut> createState() => _YBDRoomGiftSlideOutState();
}

class _YBDRoomGiftSlideOutState extends State<YBDRoomGiftSlideOut> with TickerProviderStateMixin {
  // 上礼物控制器
  AnimationController? _upGiftController;
  AnimationController? _upGiftMissController;

  // 下礼物控制器
  AnimationController? _downGiftController;
  AnimationController? _downGiftMissController;

  // 礼物消息队列
  List<YBDGift> _giftList = [];
  YBDTaskQueueUtils _upGiftQueue = YBDTaskQueueUtils();
  YBDTaskQueueUtils _downGiftQueue = YBDTaskQueueUtils();
  StreamSubscription<YBDMessage>? _listenMessage;
  Tween<Offset> _slideTween = Tween<Offset>(begin: Offset(-720.px, 0.0), end: Offset.zero);
  TweenSequence<double> _opacityTween = TweenSequence<double>(
    <TweenSequenceItem<double>>[
      TweenSequenceItem<double>(tween: ConstantTween<double>(1.0), weight: 7),
      TweenSequenceItem<double>(
          tween: Tween<double>(begin: 1.0, end: 0.0).chain(CurveTween(curve: Curves.ease)), weight: 3),
    ],
  );
  @override
  void initState() {
    super.initState();
    setGiftController();
    _listenMessage = YBDRoomMessageHelper.getInstance().messageOutStream.listen((YBDMessage event) {
      if (event is YBDGift) {
        _giftList.add(event);
        logger.v('23.1.7---YBDGift sender:${event.sender}-combo:${event.combo}---_giftList:${_giftList.length}');
        addGiftTask(event);
      }
    });
  }
  void initStateDk4vkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return _gift();
  }
  void buildkFbOPoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // gift
  Widget _gift() {
    return Container(
        width: 720.px,
        height: 370.px,
        // color: Colors.blue,
        alignment: Alignment.topLeft,
        child: Column(
          // mainAxisAlignment: ,
          children: [_upGift(), SizedBox(height: 10.px), _downGift()],
        ));
  }
  void _giftOKvQNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // up gift
  Widget _upGift() {
    return Transform.translate(
      offset: _slideTween.animate(CurvedAnimation(parent: _upGiftController!, curve: Curves.easeIn)).value,
      child: Opacity(
        opacity: _opacityTween.animate(_upGiftMissController!).value,
        child: _giftItem(nextGiftMessage(true), true),
      ),
    );
  }
  void _upGiftrsi0toyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // down gift
  Widget _downGift() {
    return Transform.translate(
      offset: _slideTween.animate(CurvedAnimation(parent: _downGiftController!, curve: Curves.easeIn)).value,
      child: Opacity(
        opacity: _opacityTween.animate(_downGiftMissController!).value,
        child: _giftItem(nextGiftMessage(false), false),
      ),
    );
  }
  void _downGiftZsWNloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // gift item  need combo
  Widget _giftItem(YBDMessage? message, bool up) {
    // logger.v('_giftItem _upGiftQueue.taskItems:${_upGiftQueue.taskItems.length}, ${_downGiftQueue.taskItems.length}');
    if (message is YBDGift) {
      YBDGift gift = message;
      return Row(
        children: [
          Container(
            width: 480.px,
            height: 88.px,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(44.px),
                color: Colors.black.withOpacity(0.8),
                border: Border.all(color: Color(0xffFF7D7D), width: 1.px)),
            child: Row(children: [
              SizedBox(width: 10.px),
              YBDRoundAvatar(
                gift.senderImg ?? '',
                avatarWidth: 70,
                needNavigation: false,
                userId: gift.fromUser,
                scene: 'C',
              ),
              SizedBox(width: 15.px),
              Container(
                width: 220.px,
                height: 63.px,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      constraints: BoxConstraints(maxWidth: 220.px),
                      child: Text(
                        gift.sender?.removeBlankSpace() ?? '',
                        style: TextStyle(color: Colors.white, fontSize: 20.sp2),
                        overflow: TextOverflow.ellipsis,
                        // textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: 6.px),
                    Row(
                      children: [
                        Text('Send to', style: TextStyle(color: Colors.white, fontSize: 20.sp2)),
                        SizedBox(width: 15.px),
                        Container(
                          width: 120.px,
                          height: 30.px,
                          child: Text(
                            gift.receiver?.removeBlankSpace() ?? '',
                            style: TextStyle(color: Color(0xffFFE824), fontSize: 20.sp2),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              // SizedBox(width: 15.px),
              Container(
                width: 160.px,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.network(
                      YBDImageUtil.gift(context, gift.imgUrl, 'B'),
                      width: ScreenUtil().setWidth(80),
                      fit: BoxFit.fitHeight,
                      height: ScreenUtil().setWidth(80),
                    ),
                    Text(
                      'x ${gift.num}',
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(28), fontWeight: FontWeight.w600, color: Color(0xffFFD87A)),
                    )
                  ],
                ),
              ),
            ]),
          ),
          Transform.translate(offset: Offset(-50.px, 0), child: YBDComboTips(gift: gift))
        ],
      );
    }

    return Container(width: 480.px, height: 88.px);
  }
  void _giftItemJ1tQloyelive(YBDMessage? message, bool up) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 播放上礼物消息
  Future<void> _playUpGift({bool combo = false, YBDGift? giftMsg}) async {
    logger.v('23.1.4---_giftList:${_giftList.first.fromUser}');
    logger.v('23.1.4---status:${_upGiftController!.status}');
    if (_giftList.isEmpty) return;

    if (mounted && _upGiftController!.status == AnimationStatus.dismissed) {
      await _upGiftController?.forward().whenComplete(() async {
        logger.v('23.1.8---_upGiftController Completed');
        await upDismiss();
      });
    } else {
      await upDismiss(combo: combo);
    }
  }
  void _playUpGiftFLVT8oyelive({bool combo = false, YBDGift? giftMsg})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 上礼物消失
  Future<void> upDismiss({bool combo = false, YBDGift? giftMsg}) async {
    // if (_upGiftMissController.status != AnimationStatus.dismissed) {
    // if (combo) _upGiftMissController.forward(from: 1.0);
    logger.v('23.1.8---_upGiftMissController upDismiss $combo');
    // _upGiftMissController.stop();
    _upGiftMissController!.reset();
    // }
    await _upGiftMissController!.forward().whenComplete(() {
      logger.v('23.1.8---_upGiftMissController Completed');

      if (combo) _upGiftQueue.endTask();
      _upGiftController?.reset();
      // if (combo) {
      //   var preTaskItem =
      //     _upGiftQueue.taskItems.firstWhere((element) => (element.param as YBDGift).comboId == giftMsg.comboId);
      // _upGiftQueue.endTaskItem(preTaskItem);
      // }
      return _upGiftMissController!.reset();
    });
  }
  void upDismissQYtO4oyelive({bool combo = false, YBDGift? giftMsg})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 播放下礼物消息
  Future<void> _playDownGift({bool combo = false}) async {
    logger.v('23.1.4---_giftList:${_giftList.first.fromUser}');
    if (_giftList.isEmpty) return;
    if (mounted && _downGiftController!.status == AnimationStatus.dismissed) {
      await _downGiftController?.forward().whenComplete(() async {
        // await Future.delayed(Duration(milliseconds: 1200));
        // YBDGift curDownMsg = _downGiftQueue.taskItems.isEmpty ? YBDGift() : _downGiftQueue.taskItems.first.param as YBDGift;
        await downDismiss();
      });
    } else {
      await downDismiss(combo: combo);
    }
  }

// 下礼物消失
  Future<void> downDismiss({bool combo = false}) async {
    // if (_downGiftMissController.status != AnimationStatus.dismissed) {
    // if (combo) _downGiftMissController.forward(from: 1.0);
    _downGiftMissController!.reset();
    // }
    await _downGiftMissController!.forward().whenComplete(() {
      _downGiftController?.reset();
      if (combo) _downGiftQueue.endTask();
      return _downGiftMissController!.reset();
    });
  }
  void downDismissfasgFoyelive({bool combo = false})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  YBDMessage? nextGiftMessage(bool up) {
    if (up) {
      if (_upGiftQueue.taskItems.isEmpty) return null;
      return _upGiftQueue.taskItems.first.param as YBDGift?;
    } else {
      if (_downGiftQueue.taskItems.isEmpty) return null;
      return _downGiftQueue.taskItems.first.param as YBDGift?;
    }
  }

  void addGiftTask(YBDGift message) {
    YBDGift? curUpMsg = _upGiftQueue.taskItems.isEmpty ? YBDGift() : _upGiftQueue.taskItems.first.param as YBDGift?;
    YBDGift? curDownMsg = _downGiftQueue.taskItems.isEmpty ? YBDGift() : _downGiftQueue.taskItems.first.param as YBDGift?;
    if (message.combo == 1 && curUpMsg!.fromUser != message.fromUser && curDownMsg!.fromUser != message.fromUser) {
      // _giftList.add(message);
      logger.v('23.1.9---up:${_upGiftQueue.isTaskRunning}--down:${_downGiftQueue.isTaskRunning}');
      if (!_upGiftQueue.isTaskRunning || (_upGiftQueue.isTaskRunning && _downGiftQueue.isTaskRunning)) {
        logger.v('23.1.11---run up gift');
        _upGiftQueue.addTask(_playUpGift, param: message);
      } else {
        logger.v('23.1.11---run down gift');
        _downGiftQueue.addTask(_playDownGift, param: message);
      }
    } else {
      if (message.fromUser == curUpMsg!.fromUser) {
        _playUpGift(combo: true, giftMsg: curUpMsg);
      }
      if (message.fromUser == curDownMsg!.fromUser) {
        _playDownGift(combo: true);
      }
    }
  }
  void addGiftTaskdtzewoyelive(YBDGift message) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // void giftQueueTaskHandler(YBDGift message) {
  //   // bool giftControllerAvailable = _upGiftController.status == AnimationStatus.dismissed;
  //   logger.v('23.1.8---combo:${message.combo}');
  //   if (message.combo > 1) {
  //     YBDGift curUpMsg = _upGiftQueue.taskItems.isEmpty ? YBDGift() : _upGiftQueue.taskItems.first.param as YBDGift;
  //     YBDGift curDownMsg = _downGiftQueue.taskItems.isEmpty ? YBDGift() : _downGiftQueue.taskItems.first.param as YBDGift;
  //     logger.v('23.1.8---sender:${message.sender}--curUpMsg:${curUpMsg.sender}');
  //     // 如果是上面的combo
  //     if (message.sender == curUpMsg.sender) {
  //       logger.v('23.1.8---= curUpMsg.sender _upGiftController.status:${_upGiftController.status}');
  //       if (_upGiftController.status == AnimationStatus.completed) {
  //       } else {
  //         addGiftTask(message);
  //       }
  //     } else if (message.sender == curDownMsg.sender) {
  //       if (_downGiftController.status == AnimationStatus.forward) {
  //         _downGiftController.stop();
  //         Future.delayed(Duration(milliseconds: 2000), () async {
  //           await _downGiftController?.forward()?.whenComplete(() {
  //             return _downGiftController?.reset();
  //           });
  //         });
  //       } else {
  //         addGiftTask(message);
  //       }
  //     }
  //   } else {
  //     addGiftTask(message);
  //   }
  // }

  // 设置送礼控制器
  void setGiftController() {
    _upGiftController = AnimationController(duration: Duration(milliseconds: 400), vsync: this);
    _upGiftMissController = AnimationController(duration: Duration(milliseconds: 3000), vsync: this);
    _downGiftController = AnimationController(duration: Duration(milliseconds: 400), vsync: this);
    _downGiftMissController = AnimationController(duration: Duration(milliseconds: 3000), vsync: this);
    _upGiftController!.addListener(() {
      // logger.v('23.1.8---${_upGiftController.value}');
      if (mounted) setState(() {});
    });
    _downGiftController!.addListener(() {
      if (mounted) setState(() {});
    });
    _upGiftMissController!.addListener(() {
      if (mounted) setState(() {});
    });
    _downGiftMissController!.addListener(() {
      if (mounted) setState(() {});
    });
    _upGiftController!.addStatusListener((status) {
      // logger.v('23.1.8---${_upGiftController.value}');
      if (status == AnimationStatus.completed) {
        if (_giftList.isNotEmpty) _giftList.removeAt(0);
      }
    });
    _downGiftController!.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        if (_giftList.isNotEmpty) _giftList.removeAt(0);
      }
    });
  }
  void setGiftControllerII8Pmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _upGiftController?.dispose();
    _upGiftMissController?.dispose();
    _downGiftController?.dispose();
    _downGiftMissController?.dispose();
    _listenMessage!.cancel();
    _listenMessage = null;
    super.dispose();
  }
}
