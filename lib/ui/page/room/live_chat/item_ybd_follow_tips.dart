import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../widget/round_ybd_avatar.dart';

class YBDItemFollowTips extends StatefulWidget {
  String? talentHead;
  int? taltentId;

  YBDItemFollowTips({this.talentHead, this.taltentId});

  @override
  YBDItemFollowTipsState createState() => new YBDItemFollowTipsState();
}

class YBDItemFollowTipsState extends BaseState<YBDItemFollowTips> {
  bool isFollow = false;
  bool isSuccess = false;
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return isSuccess
        ? Container()
        : Container(
            margin: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10), horizontal: ScreenUtil().setWidth(18)),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
                color: Colors.black.withOpacity(0.4)),
            child: Row(
              children: [
                YBDRoundAvatar(
                  widget.talentHead,
                  avatarWidth: 50,
                  needNavigation: false,
                  userId: widget.taltentId,
                  sideBorder: false,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Container(
                  // width: ScreenUtil().setWidth(200),
                  child: Text(
                    translate('follow_tip'),
                    style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff7DFAFF)),
                  ),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(32),
                ),
                GestureDetector(
                  onTap: () async {
                    YBDCommonTrack().commonTrack(
                        YBDTAProps(location: 'follow', module: YBDTAModule.liveroom, id: widget.taltentId.toString()));
                    isFollow = !isFollow;
                    setState(() {});
                    bool isSuccess = await ApiHelper.followUser(context, widget.taltentId.toString());
                    if (isSuccess) {
                      this.isSuccess = true;
                    }
                    setState(() {});
                  },
                  child: Image.asset(
                    isFollow
                        ? 'assets/images/liveroom/icon_following@2x.webp'
                        : 'assets/images/liveroom/icon_follow@2x.webp',
                    width: ScreenUtil().setWidth(40),
                  ),
                )
              ],
            ),
          );
  }
  void myBuildDRubYoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStateQEjcZoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDItemFollowTips oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidget9Lnfvoyelive(YBDItemFollowTips oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
