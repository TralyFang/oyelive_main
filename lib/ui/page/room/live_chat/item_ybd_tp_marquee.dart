import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/room_socket/message/common/winner.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/util/string_ybd_util.dart';
import 'package:oyelive_main/common/widget/marquee/marquee.dart';
import 'package:oyelive_main/ui/page/room/live_chat/controller/tp_ybd_msg_controller.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

/// tp推送消息轮播
class YBDItemTpMarquee extends StatefulWidget {
  const YBDItemTpMarquee({Key? key}) : super(key: key);

  @override
  State<YBDItemTpMarquee> createState() => _YBDItemTpMarqueeState();
}

class _YBDItemTpMarqueeState extends State<YBDItemTpMarquee> with TickerProviderStateMixin {
  late AnimationController _upController;
  late AnimationController _leftController;
  final YBDTpMsgController tp = Get.put(YBDTpMsgController());
  @override
  void initState() {
    super.initState();
    _upController = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);
    _leftController = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);
    tp.addListener(() {
      if (tp.pattiWinners.isNotEmpty) {
        if (mounted) {
          _upController.forward();
        }
      }
    });

    _upController.addListener(() {
      if (mounted) setState(() {});
    });
    _leftController.addListener(() {
      if (mounted) setState(() {});
    });
  }
  void initStateENdbIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _upController.dispose();
    _leftController.dispose();
    super.dispose();
  }
  void disposezkDHzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return GetBuilder<YBDTpMsgController>(builder: (YBDTpMsgController tpMsgCtr) {
      if (tpMsgCtr.pattiWinners.isEmpty || tpMsgCtr.hasKeyBoard) {
        return Container(
          width: 510.px,
          height: 88.px,
          margin: EdgeInsets.only(bottom: 20.px),
        );
      }

      List<Widget> msgs = tpMsgCtr.pattiWinners.map((YBDWinner? e) => msgText(e!)).toList();

      return SlideTransition(
        position: Tween<Offset>(begin: Offset(0.0, 1.0), end: Offset.zero).animate(
          CurvedAnimation(parent: _upController, curve: Curves.ease),
        ),
        child: SlideTransition(
          position: Tween<Offset>(begin: Offset.zero, end: Offset(-2.0, 0.0)).animate(
            CurvedAnimation(parent: _leftController, curve: Curves.bounceOut),
          ),
          child: YBDDelayGestureDetector(
            onTap: () {
              YBDCommonTrack().commonTrack(YBDTAProps(
                location: 'TP_message',
                module: YBDTAModule.liveroom,
                id: tpMsgCtr.roomId?.toString(),
              ));
              if (YBDTeenInfo.getInstance().pattiInfo == null) {
                YBDRoomSocketApi.getInstance().sendTeenPatti(roomId: tpMsgCtr.roomId);
              }

              YBDTeenInfo.getInstance().enterType = TPEnterType.TPETBigEvent;
              YBDRoomUtil.showTeenPattiPickerView(context);
            },
            child: Container(
              width: 510.px,
              height: 88.px,
              margin: EdgeInsets.only(bottom: 20.px),
              decoration: BoxDecoration(
                image: DecorationImage(
                  // image: AssetImage('assets/images/item_tp_marquee_bg.webp'),
                  image: AssetImage('assets/images/liveroom/pattern_notice@2x.webp'),
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(8.px),
                ),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.black.withOpacity(0.3),
                    offset: Offset(0, 10.px),
                    blurRadius: 8.px,
                    spreadRadius: -2.px,
                  ),
                ],
                // color: Colors.white,
              ),
              child: Row(
                children: <Widget>[
                  SizedBox(width: 100.px),
                  Container(
                    height: 88.px,
                    width: 400.px,
                    child: Marquee(
                      children: msgs,
                      direction: AxisDirection.up,
                      duration: Duration(milliseconds: 2000),
                      switchDuration: Duration(milliseconds: 1000),
                      hideAnimation: tpMsgCtr.pattiWinners.length == 1,
                      showOnce: true,
                      onComplete: (List<Widget> children) {
                        if (msgs == children) {
                          _leftController.forward().whenComplete(() {
                            _leftController.reset();
                            _upController.reset();
                            tpMsgCtr.cleanPattiWinners();
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
  void build220mpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Container msgText(YBDWinner winner) {
    String count = winner.prizeCoins! < 1000 ? '${winner.prizeCoins}' : NumberFormat('0,000').format(winner.prizeCoins);

    return Container(
      alignment: Alignment.center,
      child: RichText(
        text: TextSpan(
          children: <InlineSpan>[
            TextSpan(
              text: '[${YBDStringUtil.maxSub(winner.user!.nickname, 20)}]',
              style: TextStyle(
                color: Colors.white.withOpacity(0.9),
                fontWeight: FontWeight.normal,
                fontSize: 22.sp,
              ),
            ),
            TextSpan(
              text: ' ${translate('won')}',
              style: TextStyle(
                color: Colors.white.withOpacity(0.9),
                fontWeight: FontWeight.normal,
                fontSize: 22.sp,
              ),
            ),
            WidgetSpan(
              child: SizedBox(
                width: 26.px,
                height: 30.px,
                child: Image.asset(
                  'assets/images/topup/y_top_up_beans@2x.webp',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            TextSpan(
              text: count,
              style: TextStyle(
                color: Color(0xFFFFE824),
                fontWeight: FontWeight.normal,
                fontSize: 24.sp,
              ),
            ),
            TextSpan(
              text: ' ${translate('in_royal_pattern')} ',
              style: TextStyle(
                color: Colors.white.withOpacity(0.9),
                fontWeight: FontWeight.normal,
                fontSize: 22.sp,
              ),
            ),
            TextSpan(
              text: translate('join_win'),
              style: TextStyle(
                color: Color(0xFFFFE824),
                fontWeight: FontWeight.normal,
                fontSize: 22.sp,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
