import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/room_socket/message/common/winner.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';

class YBDTpWonItem extends StatelessWidget {
  YBDWinner message;

  YBDTpWonItem(this.message);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(500)),
      margin: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14), vertical: ScreenUtil().setWidth(20)),
      decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.4),
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
      child: Text.rich(
        TextSpan(children: [
          TextSpan(text: "[${message.user!.nickname}] ${translate('ludo_playing')} "),
          TextSpan(text: translate("pattern"), style: TextStyle(color: Color(0xff24FFEF))),
          TextSpan(text: ", ${translate('ludo_has_won')} "),
          TextSpan(text: "${YBDNumericUtil.format(message.prizeCoins)}", style: TextStyle(color: Color(0xff24FFEF))),
          TextSpan(text: " ${translate('beans')}... "),
          TextSpan(text: "${translate("to_join")} >>", style: TextStyle(color: Color(0xff24FFEF))),
        ]),
        style: TextStyle(fontSize: ScreenUtil().setSp(22), color: Color(0xffFFE824)),
      ),
    );
  }
  void buildRYIDQoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
