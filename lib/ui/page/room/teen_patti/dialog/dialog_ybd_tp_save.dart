import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/module/entity/tp_ybd_save_entity.dart';

class YBDTpSaveDialog extends StatelessWidget {
  YBDTpSaveRecord? record;

  YBDTpSaveDialog(this.record);

  // List<TextSpan> _getTextSpans() {
  //   String result = record.content.replaceAll(record?.attrs?.name?.key ?? '', record?.attrs?.name?.value ?? '');
  //   List<TextSpan> textSpans = [];
  //
  //   List<String> slices = result.split(record.attrs.beans.key);
  //   slices.forEach((element) {
  //     textSpans.add(TextSpan(text: element));
  //     textSpans.add(TextSpan(
  //         text: YBDNumericUtil.format(int.tryParse(record.attrs.beans.value) ?? 0),
  //         style: TextStyle(color: Color(0xffCA2630))));
  //   });
  //   if (textSpans.length > 0) {
  //     textSpans.removeLast();
  //   }
  //   YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
  //     YBDEventName.TEEN_PATTI_SAVE_EVENT,
  //     itemName: record?.content ?? "null",
  //   ));
  //   return textSpans;
  // }

  _getTextSpans() {
    String? result = record!.content;
    List<TextSpan> textSpans = [];
    if (record!.attrs!.name != null) {
      result = result!.replaceAll(record!.attrs!.name!.key!, "##${record!.attrs!.name!.key}##");
    }
    if (record!.attrs!.beans != null) {
      result = result!.replaceAll(record!.attrs!.beans!.key!, "##${record!.attrs!.beans!.key}##");
    }

    List<String> slices = result!.split("##");
    slices.forEach((element) {
      if (element == record?.attrs?.name?.key) {
        print("name xx ss ${record?.attrs?.name?.value}");
        textSpans.add(TextSpan(text: record?.attrs?.name?.value, style: TextStyle(color: Color(0xffCA2630))));
      } else if (element == record?.attrs?.beans?.key) {
        print("beans xx ss ${record?.attrs?.beans?.value}");
        textSpans.add(TextSpan(
            text: YBDNumericUtil.format(int.tryParse(record!.attrs!.beans!.value!) ?? 0),
            style: TextStyle(color: Color(0xffCA2630))));
      } else {
        textSpans.add(TextSpan(text: element));
      }
    });
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.TEEN_PATTI_SAVE_EVENT,
      itemName: record?.content ?? "null",
    ));
    return textSpans;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        YBDNavigatorHelper.popPage(context);
      },
      child: Material(
          color: Colors.transparent,
          child: GestureDetector(
            onTap: () {},
            child: Center(
                child: Container(
              width: ScreenUtil().setWidth(500),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32)))),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: ScreenUtil().setWidth(4),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        child: Padding(
                          padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
                          child: Image.asset(
                            "assets/images/icon_close_grey.png",
                            width: ScreenUtil().setWidth(32),
                            color: Color(0xff626262),
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(4),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(34),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(34)),
                    child: Text.rich(
                      TextSpan(children: _getTextSpans()),
                      style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.black.withOpacity(0.85)),
                    ),
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(34),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          //quit

                          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
                              location: YBDEventName.TEEN_PATTI_SAVE_EVENT,
                              itemName: "btn1: ${record!.buttons![0]!.name}",
                              returnExtra: record!.content));

                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: ScreenUtil().setWidth(64),
                          width: ScreenUtil().setWidth(200),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Color(0xffcccccc),
                              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(34)))),
                          child: Text(
                            record!.buttons![0]!.name!,
                            style: TextStyle(color: Colors.white.withOpacity(0.85), fontSize: ScreenUtil().setSp(28)),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(32),
                      ),
                      GestureDetector(
                        onTap: () {
                          //stay

                          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
                              location: YBDEventName.TEEN_PATTI_SAVE_EVENT,
                              itemName: "btn2: ${record!.buttons![1]!.name}",
                              returnExtra: record!.content));
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: ScreenUtil().setWidth(64),
                          width: ScreenUtil().setWidth(200),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter),
                              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(34)))),
                          child: Text(
                            record!.buttons![1]!.name!,
                            style: TextStyle(color: Colors.white.withOpacity(0.85), fontSize: ScreenUtil().setSp(28)),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(50),
                  ),
                ],
              ),
            )),
          )),
    );
  }
  void buildai7Xboyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
