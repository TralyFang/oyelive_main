import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/result_ybd_bean.dart';
import 'package:oyelive_main/module/entity/tp_ybd_save_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

enum InviteType {
  OnlineInvite, //在线socket邀请弹窗
  OfflineInvite //离线推送邀请 }
}

class YBDInviteTpDialog extends StatefulWidget {
  InviteType inviteType;

  int? dialogId;

  YBDTpSaveRecord? record;

  Function? onSuccess;

  YBDInviteTpDialog(this.inviteType, this.record, this.dialogId, {this.onSuccess});

  @override
  YBDInviteTpDialogState createState() => new YBDInviteTpDialogState();
}

class YBDInviteTpDialogState extends State<YBDInviteTpDialog> with SingleTickerProviderStateMixin {
  double? _scale;
  late AnimationController _controller;

  List<String> topicImages = [
    "assets/images/tpi_tpt@2x.webp",
    "assets/images/tpi_congras.webp",
  ];
  List<String> iconImages = [
    "assets/images/tpi_poker.webp",
    "assets/images/tpi_box.webp",
  ];

  List<String> buttonText = [
    translate("to_join"),
    translate("claim_play"),
  ];

  List<TextSpan> _getTextSpans() {
    switch (widget.inviteType) {
      case InviteType.OnlineInvite:
        // TODO: Handle this case.

        return [TextSpan(text: widget.record!.content)];
        break;
      case InviteType.OfflineInvite:
        // TODO: Handle this case.

        String result = widget.record!.content!;

        List<TextSpan> textSpans = [];
        List<String> slices = result.split(widget.record!.attrs!.beans!.key!);
        slices.forEach((element) {
          textSpans.add(TextSpan(text: element));
          textSpans.add(TextSpan(
              text: YBDNumericUtil.format(int.tryParse(widget.record!.attrs!.beans!.value!) ?? 0),
              style: TextStyle(color: Color(0xffCA2630), fontSize: ScreenUtil().setSp(32))));
        });
        if (textSpans.length > 0) {
          textSpans.removeLast();
        }
        return textSpans;
        break;
    }
  }
  void _getTextSpansapJV9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  claimOffline() async {
    YBDResultBeanEntity? resultBeanEntity = await ApiHelper.tpInviteOfflineRetrieve(context, widget.dialogId);
    YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      Navigator.pop(context);
      widget.onSuccess?.call();
    }
  }

  @override
  Widget build(BuildContext context) {
    _scale = 1 - _controller.value;

    return GestureDetector(
      onTap: () {
        YBDNavigatorHelper.popPage(context);
      },
      child: Material(
        color: Colors.transparent,
        child: GestureDetector(
          onTap: () {},
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: ScreenUtil().setWidth(606),
                  height: ScreenUtil().setWidth(864),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                    "assets/images/tpi_bg.webp",
                  ))),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: ScreenUtil().setWidth(215),
                      ),
                      Image.asset(
                        topicImages[widget.inviteType.index],
                        height: ScreenUtil().setWidth(58),
                      ),
                      SizedBox(
                        height: ScreenUtil().setWidth(30),
                      ),
                      Image.asset(
                        iconImages[widget.inviteType.index],
                        height: ScreenUtil().setWidth(300),
                      ),
                      Expanded(
                          child: Center(
                        child: SizedBox(
                          width: ScreenUtil().setWidth(500),
                          child: Text.rich(
                            TextSpan(children: _getTextSpans()),
                            style: TextStyle(
                                color: Colors.black, fontWeight: FontWeight.w500, fontSize: ScreenUtil().setSp(28)),
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      )),
                      YBDDelayGestureDetector(
                        onTap: () {
                          switch (widget.inviteType) {
                            case InviteType.OnlineInvite:
                              // TODO: Handle this case.

                              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                                YBDEventName.TEEN_PATTI_ONLINE_INVITE_EVENT,
                                location: "Join Now",
                                itemName: YBDObsUtil.instance().routeSets.last.contains('room') ? "InRoom" : "NotInRoom",
                              ));

                              Navigator.pop(context);
                              YBDTeenInfo.getInstance().enterType = TPEnterType.TPETInvite;
                              YBDModuleCenter.instance().enterRoom(type: EnterRoomType.OpenTp);
                              break;
                            case InviteType.OfflineInvite:
                              // TODO: Handle this case.
                              claimOffline();
                              break;
                          }
                        },
                        onTapDown: _onTapDown,
                        onTapCancel: _onTapCancel,
                        onTapUp: _onTapUp,
                        child: Transform.scale(
                          scale: _scale,
                          child: Container(
                            width: ScreenUtil().setWidth(490),
                            height: ScreenUtil().setWidth(122),
                            decoration:
                                BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/tpi_btn.webp'))),
                            child: Center(
                              child: Text(
                                buttonText[widget.inviteType.index],
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(30),
                                    color: Color(0xff20585D),
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setWidth(30),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setWidth(28),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Image.asset(
                    "assets/images/white_cancel.png",
                    width: ScreenUtil().setWidth(60),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  void buildJ51qdoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState

    if (widget.inviteType == InviteType.OnlineInvite)
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.TEEN_PATTI_ONLINE_INVITE_EVENT,
        location: YBDObsUtil.instance().routeSets.last,
        itemName: YBDObsUtil.instance().routeSets.last.contains('room') ? "InRoom" : "NotInRoom",
      ));

    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 200,
      ),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });

    super.initState();
  }
  void initStateY93PAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _onTapDown(TapDownDetails details) {
    _controller.forward();
  }

  void _onTapUp(TapUpDetails details) {
    _controller.reverse();
  }

  void _onTapCancel() {
    _controller.reverse();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    YBDModuleCenter.instance().notificationId = null;
  }
  void dispose2Mlcloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDInviteTpDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
