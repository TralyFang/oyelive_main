import 'dart:async';


import 'dart:async';
import 'dart:math';

import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

import '../../../../../main.dart';
import '../bus_event/event_ybd_fn.dart';
import '../constant/teen_ybd_patti_const.dart';
import '../controller/teen_ybd_patti_ctrl.dart';
import '../entity/teen_ybd_image_util.dart';
import '../entity/teen_ybd_patti_info.dart';
import '../enum/teen_ybd_state.dart';
import '../ext/int_ybd_ext.dart';

class YBDTeenCardView extends StatefulWidget {
  ///开牌了 可能加个边框
  YBDTeenState state;

  int index;

  YBDTeenCardView(this.index, {this.state = YBDTeenState.lottery});

  @override
  YBDTeenCardViewState createState() => YBDTeenCardViewState();
}

class YBDTeenCardViewState extends State<YBDTeenCardView> {
  List<YBDHands>? hands;

  YBDHands get hand {
    return hands![widget.index];
  }

  GlobalKey<FlipCardState> cardKey = GlobalKey();

  late StreamSubscription sSubscription;
  int mill = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sSubscription = eventBus.on<YBDEventFn>().listen((event) {
      if ((event.hands?.isEmpty ?? true) && this.mill != 0) {
        ///下一局开始前还有未翻牌展示的，那就延迟同样的时间之后恢复初始状态。
        Future.delayed(Duration(milliseconds: this.mill), () {
          logger.i('Royal Pattern card transition delayed next continue index: '
              '${widget.index}, '
              'mounted:$mounted, '
              'hands:${event.hands?.hashCode}, '
              'milliseconds:${widget.index.indexMilliseconds(event.hands)}');
          this.mill = 0;
          hands = event.hands;
          if (!mounted) return;
          setState(() {});
        });
        return;
      }
      this.mill = widget.index.indexMilliseconds(event.hands);
      Future.delayed(Duration(milliseconds: this.mill), () {
        logger.i('Royal Pattern card transition delayed index: '
            '${widget.index}, '
            'mounted:$mounted, '
            'hands:${event.hands?.hashCode}, '
            'milliseconds:${widget.index.indexMilliseconds(event.hands)}');

        this.mill = 0;
        hands = event.hands;
        if (!mounted) return;
        setState(() {});
      });
    });
  }
  void initStateu4Sqaoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    sSubscription.cancel();
    super.dispose();
  }
  void disposey8Qlzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return backWidget();
    // return BlocBuilder<YBDTeenPattiBloc,YBDTeenPattiBlocState>(
    //   builder: (context,state){
    //     if (state.transforCard ?? false){
    //       if (state.cleanSeat){
    //         hands = null;
    //         if (!cardKey.currentState.isFront){
    //             cardKey.currentState.toggleCard();
    //             // cardKey.currentState.controller.reverse();
    //             cardKey.currentState.isFront = true;
    //         }
    //       }
    //     }
    //     return FlipCard(
    //       key: cardKey,
    //        pDirection.HORIZONTAL,
    //       front: frontWidget(),
    //       back: backWidget(),
    //     );
    // },
    // );
  }
  void buildmD8n6oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget backWidget() {
    if (hands == null || hands?.length == 0) {
      return frontWidget();
    }
    return Opacity(
      opacity: hand.winner! ? 1 : 0.35,
      child: Container(
        child: Stack(
          children: [
            Container(
              width: YBDConst.frontCardW,
              height: YBDConst.frontCardH,
              alignment: Alignment.topCenter,
              child: Container(
                alignment: Alignment.center,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      left: YBDConst.cardPosLeft,
                      top: 0,
                      bottom: 0,
                      child: Image.asset(
                        'assets/images/teen_patti/card/card_' +
                            '${hand.cards!.first.type}_' +
                            '${hand.cards!.first.number}.png',
                        width: YBDConst.cardW,
                        height: YBDConst.cardH,
                      ),
                    ),
                    Positioned(
                      left: YBDConst.cardPosW + YBDConst.cardPosLeft,
                      top: 0,
                      bottom: 0,
                      child: Image.asset(
                        'assets/images/teen_patti/card/card_' +
                            '${hand.cards![1].type}_' +
                            '${hand.cards![1].number}.png',
                        width: YBDConst.cardW,
                        height: YBDConst.cardH,
                      ),
                    ),
                    Positioned(
                      left: YBDConst.cardPosW * 2 + YBDConst.cardPosLeft,
                      top: 0,
                      bottom: 0,
                      child: Image.asset(
                        'assets/images/teen_patti/card/card_' +
                            '${hand.cards!.last.type}_' +
                            '${hand.cards!.last.number}.png',
                        width: YBDConst.cardW,
                        height: YBDConst.cardH,
                      ),
                    ),
                    Positioned(
                      top: 0,
                      left: 0,
                      bottom: 0,
                      right: 0,
                      child: Container(
                        color: Colors.white.withAlpha(0), //100
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              right: 0,
              left: 0,
              child: Obx(
                () => Image.asset(
                  '${YBDConst.iconPrefix}${getCardHandRanking()}${Get.find<YBDTeenPattiCtrl>().isSelectVip.value ? "_vip" : ''}@2x.png',
                  width: YBDConst.frontCardW,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void backWidgetG1qOPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getCardHandRanking() {
    /// 豹子 Trail 同花顺 pure 同花 color  顺子 seq  pair对子 普通highCard
    String card = "highCard";
    List<int> nums = [];
    List<int> types = [];
    bool isPair = false;
    hand.cards?.forEach((YBDCards element) {
      if (nums.contains(element.number)) {
        isPair = true;
      }
      nums.add(element.number ?? -1);
      types.add(element.type ?? -1);
    });
    if (nums.reduce(min) == nums.reduce(max)) {
      card = "Trail";
    } else if (nums.reduce(max)- nums.reduce(min)== 2) {
      ///num 最值差距2 不是pair就是seq/pure
      card = isPair ? "pair" : ((types.reduce(max) == types.reduce(min) ? "pure" : "seq"));
    } else if (isPair) {
      card = "pair";
    } else {
      card = (types.reduce(max) == types.reduce(min) ? "color" : "highCard");
    }

    return card;
  }

  Widget frontWidget() {
    return Stack(children: [
      Container(
        width: YBDConst.frontCardW,
        height: YBDConst.frontCardH,
        // color: Colors.amberAccent,
        child: Obx(
          () => Image.asset(
            YBDTeenImageUtil.getTeenCardFront(Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
      // Positioned(
      //   top: 0,
      //   left: 0,
      //   bottom: 0,
      //   right: 0,
      //   child: Container(
      //     color: Colors.white.withAlpha(100),
      //   ),
      // ),
    ]);
  }
  void frontWidgetNbwrRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // transf() {
  //   cardKey.currentState.toggleCard();
  // }
}
