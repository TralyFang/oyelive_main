import 'dart:async';


import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:svgaplayer_flutter/player.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/ext/int_ybd_ext.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';
import 'package:oyelive_main/ui/page/room/widget/beans_ybd_bet_animation.dart';
import 'package:oyelive_main/ui/page/room/widget/svga_ybd_ctrl_player.dart';

import '../../../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../../../common/util/toast_ybd_util.dart';

import '../bus_event/event_ybd_fn.dart';
import '../controller/teen_ybd_patti_ctrl.dart';
import '../entity/teen_ybd_image_util.dart';
import '../entity/teen_ybd_result.dart';
import '../entity/ybd_teen_info.dart';
import '../ext/teen_ybd_ext.dart';
import '../bloc/teen_ybd_patti_bloc.dart';
import '../constant/teen_ybd_patti_const.dart';
import '../entity/teen_ybd_patti_info.dart';
import '../enum/teen_ybd_state.dart';

import '../../../../../main.dart';

class YBDTeenSeat extends StatefulWidget {
  YBDTeenSeatType type;
  bool isSelect;
  GlobalKey? seatKey;
//this.seatKey,
  ///选中的钱币类型
  YBDMoneyType? _currenSelectMoneyType;
  YBDTeenSeat({required this.type, this.isSelect = false});

  @override
  YBDTeenSeatState createState() => YBDTeenSeatState();
}

class YBDTeenSeatState extends State<YBDTeenSeat>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;

  ///处在点击区域
  bool istransfor = false;

  bool someBodyPost = false;

  int pot = 0;
  int you = 0;

  List<YBDHands>? hands;

  StreamSubscription? streamSubscription;
  StreamSubscription? sSubscription;

  // 座位上赢钱的冒豆子动画key
  GlobalKey<YBDSVGACtrlPlayImageState> seatDanceKey =
      GlobalKey<YBDSVGACtrlPlayImageState>();

  final GlobalKey seatKey = GlobalKey();

  Widget? _seatWinnerBeansWidget;

  YBDHands get hand {
    if (hands == null || hands!.length == 0) {
      return YBDHands()
        ..normal = true
        ..winner = true;
    }
    return hands![widget.type.index];
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(
        duration: Duration(milliseconds: 200),reverseDuration: Duration(milliseconds: 100), lowerBound: 0.6, vsync: this);
    controller.addStatusListener((status) {
      switch (status) {
        case AnimationStatus.completed:

        ///正向动画结束
          break;
        case AnimationStatus.dismissed:

        ///反向动画结束
          break;
      }
    });
    controller.forward();
    streamSubscription = eventBus.on<YBDEventFn>().listen((event) {
      if (event.transforCard ?? false) {
        // if (event.hands.isNotEmpty) {
        hands = event.hands;
        if (!mounted) return;
        setState(() {});
        // }
      }
    });
    logger.v("*****>>>>seat-init===");

    sSubscription = eventBus.on<YBDToatlMoneyEvent>().listen((event) {
      // pot = widget.type.index == 0
      //     ? (event.rs.totalBidValue1 ?? 0)
      //     : (widget.type.index == 1
      //         ? (event.rs.totalBidValue2 ?? 0)
      //         : (event.rs.totalBidValue3 ?? 0));
      // print("event: ${event.rs.totalBidValue1}, ${event.rs.totalBidValue2}, ${event.rs.totalBidValue3}");
      widget.type.index == 0
          ? (YBDTeenInfo.getInstance().potA = (event.rs!.totalBidValue1 ?? 0))
          : (widget.type.index == 1
              ? (YBDTeenInfo.getInstance().potB =
                  (event.rs!.totalBidValue2 ?? 0))
              : (YBDTeenInfo.getInstance().potC =
                  (event.rs!.totalBidValue3 ?? 0)));
      someBodyPost = true;
      if (!mounted) return;
      setState(() {});
    });
  }
  void initStateBXq1yoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  bool issetstate = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BlocBuilder<YBDTeenPattiBloc, YBDTeenPattiBlocState>(
      builder: (context, state) {
        transfor(state.currentSelectSeatType == widget.type);
        if (state.cleanSeat != null &&
            state.cleanSeat! &&
            !someBodyPost &&
            !issetstate) {
          you = 0;
          pot = 0;
          // hands = null;
        }
        if (YBDTeenInfo.getInstance().type != null) {
          state.currentClickType = YBDTeenInfo.getInstance().type;
          widget._currenSelectMoneyType = YBDTeenInfo.getInstance().type;
        }
        if (state.currentClickType != null) {
          widget._currenSelectMoneyType = state.currentClickType;
        }
        you = YBDTeenInfo.getInstance().getMoney(widget.type, YBDTeenMType.you);
        pot = YBDTeenInfo.getInstance().getMoney(widget.type, YBDTeenMType.pot);
        logger.v("update seat logger: ${state.seatType} ${widget.type} $you $pot, ${state.currentClickType}");
        issetstate = false;
        someBodyPost = false;
        eventBus.fire(YBDYouEvent()
          ..index = widget.type.index
          ..money = you);
        return ScaleTransition(
          key: seatKey,
          alignment: Alignment.center,
          scale: controller,
          child: GestureDetector(
            onTapUp: (d) {
              Future.delayed(Duration(milliseconds: 300), () {
                controller.forward();
              });
            },
            onTapDown: (detail) {
              //beans 不够啦
              if (widget._currenSelectMoneyType == null ||
                  YBDTeenInfo.getInstance().money<
                      widget._currenSelectMoneyType
                          .mValue(Get.find<YBDTeenPattiCtrl>().isSelectVip.value))
                return;

              //限制100ms
              if (YBDTeenInfo.getInstance().getMulClick(widget.type)) {
                // YBDToastUtil.toast('bet too fast');
                return;
              }
              YBDTeenInfo.getInstance().setMulClick(widget.type, true);
              Future.delayed(Duration(milliseconds: TeenConfigInfo.seatBetConfig().interval), () {
                YBDTeenInfo.getInstance().setMulClick(widget.type, false);
              });
              YBDRoomSocketApi.getInstance().joinTeenPatti(
                  roomId: YBDTeenInfo.getInstance().roomId,
                  gameId: YBDTeenInfo.getInstance().gameId,
                  index: widget.type.index,
                  coins: state.currentClickType
                      .mValue(Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
                  onSuccess: (data) async {
                    if (data.code == "000000") {
                      YBDResult msg = YBDResult.fromJson(data.result);
                      Get.find<YBDTeenPattiCtrl>().makeABet.value = true;
                      logger.v('tp bet success : ${await Get.find<YBDTeenPattiCtrl>().reloadMoney(
                          msg,
                          null)}');
                      eventBus.fire(YBDSeatSelectEvent()
                        ..index = widget.type.index
                        ..money = -(await Get.find<YBDTeenPattiCtrl>().reloadMoney(
                            msg,
                            null))); //-state.currentClickType.mValue(Get.find<YBDTeenPattiCtrl>().isSelectVip.value));
                      YBDTeenInfo.getInstance().setMoney(
                          widget.type,
                          await Get.find<YBDTeenPattiCtrl>()
                              .reloadMoney(msg, widget.type));
                      issetstate = true;

                      ///网速慢导致异常的 不操作 防止出现负数
                      // if (YBDTeenInfo.getInstance().money -
                      //         widget._currenSelectMoneyType.mValue(
                      //             Get.find<YBDTeenPattiCtrl>().isSelectVip.value) >
                      //     0) {
                      //   YBDTeenInfo.getInstance().money -=
                      //       widget._currenSelectMoneyType.mValue(
                      //           Get.find<YBDTeenPattiCtrl>().isSelectVip.value);
                      // }
                      setState(() {});
                    }else {
                      if(data.code == '901001'){
                        //随便写的 -2 值  仅仅用于beans 不够的时候刷新下面的bean图标
                        logger.v('beans is insufficient');
                        eventBus.fire(YBDSeatSelectEvent()
                          ..index = -2
                          ..money = 0);
                        YBDModuleCenter.instance().showGemsTransBeansView();
                      }else {
                        YBDToastUtil.toast(data.desc ?? "",
                            duration: 3, gravity: ToastGravity.BOTTOM);
                      }
                  }
                  });
              transfor(true);
              Future.delayed(Duration(milliseconds: 100), ()
              {
                transfor(false);
                // controller.forward();

              });

              // 需要在这里做下注动画
              GlobalKey key = YBDTeenInfo.getInstance().beanKey!;
              Widget icon = Image.asset(
                state.currentClickType.value(true, Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
                width: YBDConst.bottom_Coin_W,//+2.5,
                height: YBDConst.bottom_Coin_W,
              );
              YBDBeansBetAnimateWidget.beansBetAnimation(beginKey: key, endKey: seatKey, animationWidget: icon);

              // setState(() {});
            },
            child: Container(
              width: YBDConst.seatC,
              height: YBDConst.seat_H +
                  (MediaQuery.of(context).padding.top == 0
                      ? YBDConst.potCH
                      : 0),
              // color: Colors.cyan,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: <Widget>[
                      Container(
                        height: YBDConst.seatH +
                            YBDConst.potCH *
                                (MediaQuery.of(context).padding.top == 0
                                    ? 2
                                    : 1),
                        width: YBDConst.seatC,
                        // color: Colors.purple,
                        child: Stack(
                          children: <Widget>[
                            Column(
                              children: [
                                Opacity(
                                  opacity: hand.winner! ? 1 : 0.35,
                                  child: Container(
                                    alignment: Alignment.center,
                                    // color: Colors.orange,
                                    child: Obx(
                                      () => Image.asset(
                                        YBDTeenImageUtil.getSeat(
                                            widget.type,
                                            Get.find<YBDTeenPattiCtrl>()
                                                .isSelectVip
                                                .value),
                                        fit: BoxFit.fitHeight,
                                        height: YBDConst.seatH,
                                        // width: YBDConst.seatW,
                                      ),
                                    ),
                                  ),
                                ),
                                // Container(height: YBDConst.potCH,),
                              ],
                            ),
                            Positioned(
                                width: YBDConst.seatC,
                                bottom: 0,
                                child: getContainer(YBDTeenMType
                                    .pot)), //PotContainer(YBDTeenMType.pot, pot, widget.type)
                          ],
                        ),
                      ),
                      getContainer(YBDTeenMType.you),
                      // PotContainer(YBDTeenMType.you, you, widget.type)
                    ],
                  ),
                  seatWinnerBeansWidget()!,
                ],
              ),
            ),
          ),
        );
      },
    );
  }
  void build2vPHroyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 赢钱的座位，冒豆子
  Widget? seatWinnerBeansWidget() {
    YBDTeenPattiCtrl teenPattiCtrl = Get.find<YBDTeenPattiCtrl>();

    if (_seatWinnerBeansWidget == null) {
      _seatWinnerBeansWidget = GetBuilder<YBDTeenPattiCtrl>(
        id: 'seatDance',
        builder: (teenCtrl) {
          // 该座位赢钱了，而且我有下注，冒豆子吧。
          if (hand.winner! && !hand.normal && teenPattiCtrl.isSeatWin) {
            seatDanceKey.currentState?.play();
          } else {
            seatDanceKey.currentState?.stop();
          }
          logger.v("*****>>>>seatbean===${(hand.winner! && !hand.normal &&
              teenPattiCtrl.isSeatWin)}==${teenPattiCtrl.isSeatWin}==${hand.winner}==${!hand.normal}====${seatDanceKey.currentState}");
          return Offstage(
            offstage: !(hand.winner! && !hand.normal &&
                teenPattiCtrl.isSeatWin),
            child: Container(
              margin: EdgeInsets.only(top: ScreenUtil().setWidth(10)),
              width: ScreenUtil().setWidth(100),
              height: ScreenUtil().setWidth(150),
              child: YBDSVGACtrlPlayImage(
                key: seatDanceKey,
                startPlay: (hand.winner! && !hand.normal &&
                    teenPattiCtrl.isSeatWin),
                assetsName: "assets/animation/teen_patti_seat_dance.svga",
                playStateListener: (status) {
                  // if (status == AnimationStatus.completed ||
                  //     status == AnimationStatus.dismissed) {
                  //   teenPattiCtrl.isSeatWin.value = false;
                  // }
                },
              ),
            ),
          );
        },
      );
    }
    return _seatWinnerBeansWidget;
  }

  String getSuffix() {
    return widget.isSelect ? "" : "_fadded";
  }
  void getSuffixVcx9foyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget getContainer(YBDTeenMType type) {
    return Container(
      alignment: Alignment.center,
      width: YBDConst.seatC,
      // color: Colors.red,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            child: Obx(
              () => Image.asset(
                YBDTeenImageUtil.getYPContainer(type == YBDTeenMType.pot,
                    Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
                // fit: BoxFit.fill,
                width: YBDConst.seatC,
              ),
            ),
          ),
          Positioned(
            child: Container(
              padding: EdgeInsets.only(left: 5, top: 2),
              height: YBDConst.potH * 2 / 3.0,
              width: YBDConst.seatC,
              alignment: Alignment.centerLeft,
              child: Obx(
                () => Text(
                  "${type == YBDTeenMType.pot ? "  Pot" : "  You"}  ${Get.find<YBDTeenPattiCtrl>().isSelectVip.value ? (getNumBeans(type) + 'K') : (type == YBDTeenMType.pot ? pot : you) < 1000 ? (type == YBDTeenMType.pot ? pot : you) : NumberFormat('0,000').format(type == YBDTeenMType.pot ? pot : you)}",
                  style: TextStyle(
                      color: Get.find<YBDTeenPattiCtrl>().isSelectVip.value
                          ? Colors.black
                          : Colors.white,
                      fontSize: ScreenUtil().setSp(16),
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
  void getContainerWJI6Aoyelive(YBDTeenMType type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String getNumBeans(YBDTeenMType type) {
    double a = (type == YBDTeenMType.pot ? pot : you) / 1000;
    int b = (type == YBDTeenMType.pot ? pot : you) % 1000;
    String textNum = a.toStringAsFixed(b % 1000 == 0 ? 0 : 2);
    if (b % 1000 != 0 && textNum.substring(textNum.length - 1) == '0') {
      textNum = a.toStringAsFixed(b % 1000 == 0 ? 0 : 1);
    }
    return textNum;
  }
  void getNumBeanspRvmaoyelive(YBDTeenMType type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///isform 当处在椅子范围执行正向动画 反之就反向动画
  transfor(bool isform) {
    if (istransfor == isform) {
      return;
    }
    istransfor = isform;
    if (isform) {
      controller.reverse();
    }else {
      controller.forward();
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    streamSubscription?.cancel();
    sSubscription?.cancel();
    super.dispose();
  }
}

// // ignore: must_be_immutable
// class PotContainer extends StatefulWidget{
//   YBDTeenMType Mtype;
//   int money;
//   YBDTeenSeatType type;
//   PotContainer(this.Mtype, this.money,this.type);
//
//   @override
//   _PotContainerState createState() => _PotContainerState();
// }
//
// class _PotContainerState extends State<PotContainer> {
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     eventBus.on<YBDToatlMoneyEvent>().listen((event) {
//       int a = widget.type.index == 0
//           ? (event.rs.totalBidValue1 ?? 0)
//           : (widget.type.index == 1
//           ? (event.rs.totalBidValue2 ?? 0)
//           : (event.rs.totalBidValue3 ?? 0));
//       if (widget.Mtype == YBDTeenMType.pot ){
//         widget.money = a;
//       }
//       if (!mounted) return;
//       setState(() {});
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Container(
//       alignment: Alignment.center,
//       width: YBDConst.seatC,
//       // color: Colors.red,
//       child: Stack(
//         children: [
//           Container(
//             width: double.infinity,
//             child: Image.asset(
//               '${YBDConst.iconPrefix}${widget.Mtype == YBDTeenMType.pot ? "pot" : "you"}@2x.png',
//               // fit: BoxFit.fill,
//               width:YBDConst.seatC,
//             ),
//           ),
//           Positioned(
//             child: Container(
//               padding: EdgeInsets.only(left: 5,top: 2),
//               height: YBDConst.potH * 2 / 3.0,
//               width: YBDConst.seatC,
//               alignment: Alignment.centerLeft,
//               child: Text(
//                 "${widget.Mtype == YBDTeenMType.pot ? "  Pot" : "  You"}  ${(widget.money < 1000 ? widget.money : NumberFormat('0,000').format(widget.money))}",
//                 style: TextStyle(
//                     color: Colors.white,
//                     fontSize: ScreenUtil().setSp(16),
//                     decoration: TextDecoration.none,
//                     fontWeight: FontWeight.bold),
//               ),
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
