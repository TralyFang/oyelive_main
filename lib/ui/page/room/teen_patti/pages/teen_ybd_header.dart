import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

import '../constant/teen_ybd_patti_const.dart';
import '../enum/teen_ybd_state.dart';

import 'package:get/get.dart';
import '../constant/teen_ybd_patti_const.dart';
import '../controller/teen_ybd_patti_ctrl.dart';
import '../entity/teen_ybd_image_util.dart';
import '../enum/teen_ybd_state.dart';

class YBDTeenHeader extends StatelessWidget {
  Function(YBDTeenHeadClickType type)? clickBlc;

  YBDTeenHeader({this.clickBlc});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Size size = MediaQuery.of(context).size;
    return Container(
      height: YBDConst.headerH - Get.find<YBDTeenPattiCtrl>().getH(),
      width: double.infinity,
      child: Stack(children: [
        Positioned(
          child: Obx(
            () => Get.find<YBDTeenPattiCtrl>().isSelectVip.value
                ? Container(
                    width: 0,
                  )
                : Container(
                    width: double.infinity,
                    height: double.infinity,
                    padding: EdgeInsets.only(bottom: ScreenUtil().setHeight(120), top: 0),
                    child: Image.asset(
                      YBDTeenImageUtil.TeenLogo(Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
                      // fit: BoxFit.fitHeight,
                    ),
                  ),
          ),
        ),
        Container(
          height: double.infinity,
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 3),
          child: Obx(
            () => Image.asset(
              YBDTeenImageUtil.getHeadBg(Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        Positioned(
          // left: (size.width - YBDConst.headerLogo) / 2.0,
          // top: 3,
          child: Obx(
                () => !Get.find<YBDTeenPattiCtrl>().isSelectVip.value
                ? Container(
              width: 0,
            )
                : Container(
              width: double.infinity,
              height: double.infinity,
              padding: EdgeInsets.only(bottom: ScreenUtil().setHeight(75), top: 0),
              color: Colors.transparent,
              child: Image.asset(
                YBDTeenImageUtil.TeenLogo(Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
                // height: YBDConst.headerLogo,
              ),
            ),
          ),
        ),
        Positioned(
          left: 15,
          top: (YBDConst.headerH - YBDConst.headerSmallIconW) / 2.0 + 8,
          child: YBDDelayGestureDetector(
            onTap: () {
              clickBlc!(YBDTeenHeadClickType.question);
            },
            child: Image.asset(
              YBDTeenImageUtil.TeenQuestion,
              width: YBDConst.headerSmallIconW,
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        Positioned(
          right: 15,
          top: (YBDConst.headerH - YBDConst.headerSmallIconW) / 2.0 + 8,
          child: YBDDelayGestureDetector(
            onTap: () {
              clickBlc!(YBDTeenHeadClickType.close);
            },
            child: Obx(
              () => Image.asset(
                YBDTeenImageUtil.TeenClose(Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
                width: YBDConst.headerSmallIconW,
              ),
            ),
          ),
        ),
      ]),
    );
  }
  void buildZnz1Goyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
