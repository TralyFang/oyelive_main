import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/common/widget/marquee/marquee.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/constant/teen_ybd_patti_const.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/controller/teen_ybd_patti_ctrl.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_image_util.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/enum/teen_ybd_state.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_header.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_card_view.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_seat.dart';
import 'package:oyelive_main/ui/page/room/widget/svga_ybd_ctrl_player.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';

//窗帘
class YBDTeenCurtain extends StatelessWidget {
  bool isLeft;
  YBDTeenCurtain(this.isLeft);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (isLeft)
      return Positioned(
        top: YBDConst.leftTop,
        left: 0,
        child: Obx(
          () => Get.find<YBDTeenPattiCtrl>().isSelectVip.value
              ? Image.asset(
                  YBDTeenImageUtil.TeenCurtainLeft,
                  width: YBDConst.leftTopW,
                  fit: BoxFit.fitWidth,
                )
              : Container(
                  width: 0,
                ),
        ),
      );
    return Positioned(
      top: YBDConst.leftTop,
      right: 0,
      child: Obx(
        () => Get.find<YBDTeenPattiCtrl>().isSelectVip.value
            ? Image.asset(
                YBDTeenImageUtil.TeenCurtainRight,
                width: YBDConst.rightTopW,
                fit: BoxFit.fitWidth,
              )
            : Container(
                width: 0,
              ),
      ),
    );
  }
  void buildSN1QCoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

//vip 图标
class YBDTeenVipItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: YBDConst.vipTop,
      left: YBDConst.vipLeft,
      child: GestureDetector(
        onTap: () async {
          Get.find<YBDTeenPattiCtrl>().tapClick();
        },
        child: Obx(
          () => Container(
            padding: EdgeInsets.all(5),
            child: Image.asset(
              Get.find<YBDTeenPattiCtrl>().isSelectVip.value ? YBDTeenImageUtil.TeenVipNormal : YBDTeenImageUtil.TeenVip,
              width: YBDConst.vipW,
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
      ),
    );
  }
  void build4Gm1zoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

//背景
class YBDTeenBgItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Positioned(
      top: YBDConst.headerH / 2.0,
      left: 0,
      right: 0,
      bottom: 0,
      child: Obx(
        () => Image.asset(
          YBDTeenImageUtil.getTeenBg(Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
          width: double.infinity,
          fit: BoxFit.fill,
        ),
      ),
    );
  }
  void buildCIBeKoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

enum TeenRHItemType { TeenVip, TeenRecords, TeenHistory }

//records and history 图标
class YBDTeenRHItem extends StatelessWidget {
  TeenRHItemType type;
  YBDTeenRHItem(this.type);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Positioned(
      top: YBDConst.leftTopItem(type),
      left: 0,
      child: GestureDetector(
        onTap: () {
          if (type == TeenRHItemType.TeenVip) {
            Get.find<YBDTeenPattiCtrl>().tapClick();
            return;
          }
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.ROOM_PAGE,
            itemName: type == TeenRHItemType.TeenRecords ? YBDItemName.TEEN_PATTI_RECORD : YBDItemName.TEEN_PATTI_HISTORY,
          ));
          YBDNavigatorHelper.navigateTo(
              context,
              type == TeenRHItemType.TeenRecords
                  ? YBDNavigatorHelper.teenpatti_record
                  : YBDNavigatorHelper.teenpatti_history);
        },
        child: Obx(
              () => Image.asset(
            YBDTeenImageUtil.getTeenRH(type, Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
            width: YBDConst.leftW,
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
    );
  }
  void buildmcLGYoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDTeenBodyItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        YBDTeenHeader(
          clickBlc: (type) {
            if (type == YBDTeenHeadClickType.close) {
              // Navigator.pop(context);
              YBDModuleCenter.instance().tpPop();
            } else {
              YBDNavigatorHelper.navigateToWeb(
                context,
                "?url=${Uri.encodeComponent(Const.TEEN_PATTI_RULE)}&title=${translate("pattern")} Rules",
              );
            }
          },
        ),
        Container(
          // padding: EdgeInsets.only(left: YBDConst.leftW),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              YBDTeenCardView(0),
              YBDTeenCardView(1),
              YBDTeenCardView(2),
            ],
          ),
        ),
        Container(
          // padding: EdgeInsets.only(left: YBDConst.leftW),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              YBDTeenSeat(
                type: YBDTeenSeatType.A,
              ),
              YBDTeenSeat(
                type: YBDTeenSeatType.B,
              ),
              YBDTeenSeat(
                type: YBDTeenSeatType.C,
                isSelect: true,
              ),
            ],
          ),
        ),
        YBDMoneyContainer(),
      ],
    );
  }
  void buildOt3Zroyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDTeanBeansDanceWidget extends StatelessWidget {
  YBDTeanBeansDanceWidget({Key? key}) : super(key: key);

  // 余额beans的动画key
  GlobalKey<YBDSVGACtrlPlayImageState> balanceBeansDanceKey = GlobalKey<YBDSVGACtrlPlayImageState>();

  @override
  Widget build(BuildContext context) {
    // logger.v("*****>>>>bean-init");
    return beansDanceWidget();
  }

  // 跳动的金币
  Widget beansDanceWidget() {
    YBDTeenPattiCtrl teenPattiCtrl = Get.find<YBDTeenPattiCtrl>();

    return Obx(() {
      bool isWin = teenPattiCtrl.isWin.value;
      if (isWin) {
        balanceBeansDanceKey.currentState?.play();
      } else {
        balanceBeansDanceKey.currentState?.stop();
      }
      logger.v("*****>>>>bean===$isWin====${balanceBeansDanceKey.currentState}");
      // 这里需要转换下余额金豆的坐标。
      RenderBox renderBox = balanceBeansKey.currentContext!.findRenderObject() as RenderBox;
      var offset = renderBox.localToGlobal(Offset.zero);
      double dx = -ScreenUtil().setWidth(3);
      if (offset.dx > YBDConst.bottom_top) {
        // 只有没有挨边才做计算
        dx = offset.dx - ScreenUtil().setWidth(35);
      }
      return Offstage(
        offstage: !teenPattiCtrl.isWin.value,
        child: Stack(
          alignment: Alignment.bottomLeft,
          children: [
            Positioned(
              bottom: ScreenUtil().setWidth(73),
              left: dx,
              child: Container(
                width: ScreenUtil().setWidth(100),
                height: ScreenUtil().setWidth(150),
                child: YBDSVGACtrlPlayImage(
                  key: balanceBeansDanceKey,
                  startPlay: teenPattiCtrl.isWin.value,
                  assetsName: "assets/animation/teen_patti_balance_dance.svga",
                  playStateListener: (status) {
                    // if (status == AnimationStatus.completed || status == AnimationStatus.dismissed) {
                    //   teenPattiCtrl.isWin.value = false;(status == AnimationStatus.completed || status == AnimationStatus.dismissed) {
                    //                     //   teenPattiCtrl.isWin.value = false;
                    //                     // }
                    // }
                  },
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
  void beansDanceWidget7Diluoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

// 等待下一局开启的动画提示
class YBDTeenWaitingWidget extends StatelessWidget {
  YBDTeenWaitingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return waitingWidget();
  }

  // 等待动画
  Widget waitingWidget() {
    YBDTeenPattiCtrl teenPattiCtrl = Get.find<YBDTeenPattiCtrl>();

    return Obx(() {
      return Offstage(
        offstage: !teenPattiCtrl.isWaiting.value,
        child: Container(
          padding: EdgeInsets.only(top: YBDConst.headerH),
          // height: YBDConst.headerH / 2.0 + YBDConst.bodyH,
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.transparent, Colors.black.withOpacity(0.3), Colors.black.withOpacity(0.3)],
              ),
            ),
            alignment: Alignment.topCenter,
            child: Container(
              width: ScreenUtil().setWidth(280),
              height: ScreenUtil().setWidth(320),
              child: YBDSVGACtrlPlayImage(
                repeatPlay: true,
                assetsName: "assets/animation/teen_patti_waiting.svga",
              ),
            ),
          ),
        ),
      );
    });
  }
  void waitingWidgetyhDy6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

//marquee
class YBDTeenMarquee extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    logger.v(
        'Royal Pattern record: ${Get.find<YBDTeenPattiCtrl>().rank},');
    return Obx(() {
      if ((Get.find<YBDTeenPattiCtrl>().showMarquee.value || !Get.find<YBDTeenPattiCtrl>().showMarquee.value) && Get.find<YBDTeenPattiCtrl>()
          .rank!.isEmpty)
        return Container();
      logger.v('record isreload');
      return Positioned(
        top: YBDConst.vipTop,
        left: YBDConst.vipLeft,
        right: YBDConst.vipLeft,
        child: YBDDelayGestureDetector(
          onTap: pushToRecord,
          child: Container(
              padding: EdgeInsets.only(left: 10,right: 10),
              height: ScreenUtil().setHeight(44),
              decoration: BoxDecoration(
                  color: Color(Get.find<YBDTeenPattiCtrl>().isSelectVip.value ? 0xff282303 : 0xff158182).withOpacity(0.29),
                  borderRadius: BorderRadius.circular(17.5)),
              child: Marquee(
                  children: Get.find<YBDTeenPattiCtrl>()
                      .rank!
                      .map((e) =>
                          Container(height: ScreenUtil().setHeight(44), alignment: Alignment.centerLeft, child: richText(e)))
                      .toList(),
                  direction: AxisDirection.left,
                  duration: Duration(milliseconds: 6000),
                  onItemTap: (index) {})),
        ),
      );
    });
  }
  void buildMUhFFoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  richText(YBDTeenPattiRankEntity? ent) {
    return RichText(
      text: TextSpan(children: [
        WidgetSpan(
          alignment: PlaceholderAlignment.middle,
          child: YBDRoundAvatar(
            ent?.user?.headimg,
            avatarWidth: ScreenUtil().setHeight(70),
            needNavigation: false,
            userId: ent?.user?.id,
            scene: "C",
          ),
        ),
        TextSpan(
            text: '  ' + translate('has_won_total'),
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: ScreenUtil().setSp(22))),
        TextSpan(
            text: ' '
                '${(ent?.count ?? 0) < 1000 ? ent?.count : NumberFormat('0,000').format(ent?.count)}'
                ' ',
            style:
                TextStyle(color: Color(0xFFFFD989), fontWeight: FontWeight.normal, fontSize: ScreenUtil().setSp(24))),
        TextSpan(
            text: translate('beans'),
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: ScreenUtil().setSp(22))),
      ]),
    );
  }

  pushToRecord(){
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: YBDLocationName.ROOM_PAGE,
      itemName: YBDItemName.TEEN_PATTI_RECORD + 'Marquee',
    ));
    YBDNavigatorHelper.navigateTo(
        Get.context,
        YBDNavigatorHelper.teenpatti_record).whenComplete(() => Get.find<YBDTeenPattiCtrl>().getTeenRecord());
  }
}
