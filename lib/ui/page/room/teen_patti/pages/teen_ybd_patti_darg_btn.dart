import 'dart:async';


import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/ext/int_ybd_ext.dart';

import '../../../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../main.dart';
import '../bloc/teen_ybd_patti_bloc.dart';
import '../bus_event/event_ybd_fn.dart';
import '../constant/teen_ybd_patti_const.dart';
import '../controller/teen_ybd_patti_ctrl.dart';
import '../entity/teen_ybd_result.dart';
import '../entity/ybd_teen_info.dart';
import '../enum/teen_ybd_state.dart';
import '../ext/teen_ybd_ext.dart';

class YBDDargBtn extends StatefulWidget {
  GlobalKey<YBDDargBtnState>? gKey;

  YBDMoneyType type;

  ///是否拖拽的icon
  bool isdarg;
  bool isSelect = false;

  YBDDargBtn({required this.type, this.gKey, this.isdarg = false});

  ///默认没选中任何位置
  YBDTeenSeatType currentSeatType = YBDTeenSeatType.D;

  @override
  YBDDargBtnState createState() => YBDDargBtnState();
}

class YBDDargBtnState extends State<YBDDargBtn>
    with SingleTickerProviderStateMixin {
  late double dx;
  double dy = 0.93;
  Alignment? _dargAlignment;

  late AnimationController _animationController;
  late Animation<Alignment> _animation;

  late StreamSubscription _subscriber;

  final GlobalKey beanKey = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _animationController = AnimationController.unbounded(vsync: this);

    ///加个监听
    _animationController.addListener(() {
      _dargAlignment = _animation.value;
      setState(() {});
    });

    _subscriber = eventBus.on<YBDSeatSelectEvent>().listen((event) {
      if (event.money == 0 && event.index == -1) {
        return;
      }
      // YBDTeenInfo.getInstance().money += event.money;
      if (!mounted) return;
      logger.i('Royal Pattern darg btn is reload money: ${YBDTeenInfo.getInstance().money}');
      //延迟让本地money 更新后  及时禁灰bean 按钮
      Future.delayed(Duration(milliseconds: event.index == -1 ? 0: 200),(){
        setState(() {});
      });

    });
  }
  void initStateUlJmgoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    _subscriber.cancel();
    super.dispose();
  }
  void disposewg6Xhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // joinTeen(){
  //   YBDRoomSocketApi.getInstance().joinTeenPatti(roomId: YBDTeenInfo.getInstance().roomId, gameId: YBDTeenInfo.getInstance().gameId,index: widget.currentSeatType.index,coins: widget.type.mValue());
  // }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    if (_dargAlignment == null) {
      dx = widget.type.x(size);
      dy = MediaQuery.of(context).padding.bottom > 0 ? 0.93 : 0.95;
      _dargAlignment = Alignment(dx, dy);
    }
    // YBDMoneyType type = Provider.of<YBDTeenProvider>(context,).currentClickType;
    // widget.isSelect = (type != null && type.index == widget.type.index && !widget.isdarg);
    return BlocBuilder<YBDTeenPattiBloc, YBDTeenPattiBlocState>(
        builder: (context, state) {
      if (state.currentClickType != null) {
        widget.isSelect = (state.currentClickType != null &&
            state.currentClickType!.index == widget.type.index &&
            !widget.isdarg);
      }

      if (YBDTeenInfo.getInstance().type != null) {
        widget.isSelect =
            (YBDTeenInfo.getInstance().type!.index == widget.type.index &&
                !widget.isdarg);
      }
      return GestureDetector(
        onPanStart: (details) {
          if (YBDTeenInfo.getInstance().money<
              widget.type.mValue(Get.find<YBDTeenPattiCtrl>().isSelectVip.value))
            return;
          YBDTeenInfo.getInstance().type = widget.type;
          YBDTeenInfo.getInstance().beanKey = this.beanKey;
          context
              .read<YBDTeenPattiBloc>()
              .add(YBDTeenPattiEventObject(currentClickType: widget.type));
          _animationController.stop(canceled: true);
        },
        onPanUpdate: (details) {
          if (YBDTeenInfo.getInstance().money<
              widget.type.mValue(Get.find<YBDTeenPattiCtrl>().isSelectVip.value))
            return;
          if (details.globalPosition.dx > 0 &&
              details.globalPosition.dx < size.width) {
            var alig = _dargAlignment ?? Alignment(0.0, 0.0);
            alig += Alignment(details.delta.dx / size.width * 2,
                details.delta.dy / size.height * 2);
            _dargAlignment = alig;
          }
          ;
          providerNoctifToReloadSeatAnimation(details.globalPosition, size);

          // final RenderBox box = context.findRenderObject();
          // Offset l = box.localToGlobal(details.globalPosition);
          // Offset g = box.globalToLocal(details.globalPosition);
          setState(() {});
        },
        onPanEnd: (details) {
          if (YBDTeenInfo.getInstance().money<
              widget.type.mValue(Get.find<YBDTeenPattiCtrl>().isSelectVip.value))
            return;
          if (widget.currentSeatType == YBDTeenSeatType.D) {
            startAnimation(details.velocity.pixelsPerSecond, size);
            context.read<YBDTeenPattiBloc>().add(YBDTeenPattiEventObject()
              ..currentSelectSeatType = YBDTeenSeatType.D
              ..currentClickType = widget.type);
          } else {
            ///选中了哪个进行投注
            _dargAlignment = Alignment(dx, dy);
            setState(() {});

            ///如果收不到回调 座位动画无法恢复的 问题  先恢复一下
            context.read<YBDTeenPattiBloc>().add(YBDTeenPattiEventObject()
              ..currentSelectSeatType = YBDTeenSeatType.D
              ..currentClickType = widget.type);


            //限制100ms
            if (YBDTeenInfo.getInstance().getMulClick(widget.currentSeatType)) {
              // YBDToastUtil.toast('bet too fast');
              return;
            }
            YBDTeenInfo.getInstance().setMulClick(widget.currentSeatType, true);
            Future.delayed(Duration(milliseconds: TeenConfigInfo.seatBetConfig().interval), () {
              YBDTeenInfo.getInstance().setMulClick(widget.currentSeatType, false);
            });


            YBDRoomSocketApi.getInstance().joinTeenPatti(
                roomId: YBDTeenInfo.getInstance().roomId,
                gameId: YBDTeenInfo.getInstance().gameId,
                index: widget.currentSeatType.index,
                coins: widget.type
                    .mValue(Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
                onSuccess: (data) async {
                  if (data.code == "000000") {
                    YBDResult msg = YBDResult.fromJson(data.result);
                    Get.find<YBDTeenPattiCtrl>().makeABet.value = true;
                    eventBus.fire(YBDSeatSelectEvent()
                      ..index = widget.currentSeatType.index
                      ..money = -(await Get.find<YBDTeenPattiCtrl>().reloadMoney(
                          msg,
                          null))); //-widget.type.mValue(Get.find<YBDTeenPattiCtrl>().isSelectVip.value));
                    // YBDTeenInfo.getInstance().money -= widget.type.mValue(Get.find<YBDTeenPattiCtrl>().isSelectVip.value);
                    YBDTeenInfo.getInstance().money -=
                        await Get.find<YBDTeenPattiCtrl>()
                            .reloadMoney(msg, widget.currentSeatType);

                    ///双层保证 后台有数据 就以后台为主
                    if ((data.result["bidValue1"] ?? 0) > 0 ||
                        (data.result["bidValue2"] ?? 0) > 0 ||
                        (data.result["bidValue3"] ?? 0) > 0) {
                      YBDTeenInfo.getInstance()
                          .setYou(widget.currentSeatType, data.result);
                    } else {
                      YBDTeenInfo.getInstance().setMoney(
                          widget.currentSeatType,
                          widget.type.mValue(
                              Get.find<YBDTeenPattiCtrl>().isSelectVip.value));
                    }
                    context
                        .read<YBDTeenPattiBloc>()
                        .add(YBDTeenPattiEventObject()
                          ..currentSelectSeatType = YBDTeenSeatType.D
                          ..currentClickType = widget.type
                          ..seatType = widget.currentSeatType);
                  } else {
                    if(data.code == '901001'){
                      //随便写的 -2 值  仅仅用于beans 不够的时候刷新下面的bean图标
                      logger.i('Royal Pattern beans is insufficient');
                      eventBus.fire(YBDSeatSelectEvent()
                        ..index = -2
                        ..money = 0);
                      YBDModuleCenter.instance().showGemsTransBeansView();
                    }else {
                      // translate(key)
                      YBDToastUtil.toast(data.desc ?? "",
                          duration: 3, gravity: ToastGravity.BOTTOM);
                    }
                  }
                });
          }
        },
        child: Align(
          alignment: _dargAlignment!,
          child: Obx(() {
            logger.i('Royal Pattern beans reload : ${YBDTeenInfo.getInstance().money}');
            return Container(
              // padding: EdgeInsets.only(bottom: 4),
              padding: EdgeInsets.only(left: 1),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: (widget.isSelect &&
                          YBDTeenInfo.getInstance().money>=
                              widget.type.mValue(
                                  Get.find<YBDTeenPattiCtrl>().isSelectVip.value))
                      ? (Get.find<YBDTeenPattiCtrl>().isSelectVip.value
                          ? [
                              const Color(0xFFE0CC96),
                              const Color(0xFFF7DB9D),
                              const Color(0xFFF3D390),
                              const Color(0xFFDEC18C)
                            ]
                          : [
                              const Color(0xFFFFC3EC),
                              const Color(0xFFFF745C),
                            ])
                      : [
                          Colors.transparent,
                          Colors.transparent,
                        ],
                ),
                borderRadius:
                    BorderRadius.circular(YBDConst.bottom_Coin_W / 2.0),
              ),
              child: Image.asset(
                widget.type
                    .value(true, Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
                key: beanKey,
                width: YBDConst.bottom_Coin_W, //+2.5,
                height: YBDConst.bottom_Coin_W,
              ),
            );
          }),
        ),
      );
    });
  }
  void buildG8eKsoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  providerNoctifToReloadSeatAnimation(Offset offset, Size size) {
    ///  先随便写一个范围 后面再考虑怎么计算具体的数值
    ///等分后的padding_X
    // double padding_X = (size.width - YBDConst.leftW - YBDConst.seatW *3)/3.0;
    // double x =  YBDConst.leftW + padding_X/2.0; //size.width - (leftW + seatW *3 + midPx *2);
    double padding_X = (size.width - YBDConst.seatW * 3) / 4.0;
    double x = padding_X; //size.width - (leftW + seatW *3 + midPx *2);
    if (offset.dy <
            (size.height - YBDConst.bottom_vH - YBDConst.bottom_Coin_W) &&
        offset.dy > (YBDConst.bodyH - YBDConst.cardH)) {
      if (offset.dx > x && offset.dx < x + YBDConst.seatW) {
        context.read<YBDTeenPattiBloc>().add(YBDTeenPattiEventObject()
          ..currentSelectSeatType = YBDTeenSeatType.A
          ..currentClickType = widget.type);
        widget.currentSeatType = YBDTeenSeatType.A;
      } else if (offset.dx > x + padding_X + YBDConst.seatW &&
          offset.dx < x + padding_X + YBDConst.seatW * 2) {
        context.read<YBDTeenPattiBloc>().add(YBDTeenPattiEventObject()
          ..currentSelectSeatType = YBDTeenSeatType.B
          ..currentClickType = widget.type);
        widget.currentSeatType = YBDTeenSeatType.B;
      } else if (offset.dx > x + padding_X * 2 + YBDConst.seatW * 2 &&
          offset.dx < x + padding_X * 2 + YBDConst.seatW * 3) {
        context.read<YBDTeenPattiBloc>().add(YBDTeenPattiEventObject()
          ..currentSelectSeatType = YBDTeenSeatType.C
          ..currentClickType = widget.type);
        widget.currentSeatType = YBDTeenSeatType.C;
      } else {
        context.read<YBDTeenPattiBloc>().add(YBDTeenPattiEventObject()
          ..currentSelectSeatType = YBDTeenSeatType.D
          ..currentClickType = widget.type);
        widget.currentSeatType = YBDTeenSeatType.D;
      }
    } else {
      // Provider.of<YBDTeenProvider>(context, listen: false).setSeatType(YBDTeenSeatType.D);
      context.read<YBDTeenPattiBloc>().add(YBDTeenPattiEventObject()
        ..currentSelectSeatType = YBDTeenSeatType.D
        ..currentClickType = widget.type);
      widget.currentSeatType = YBDTeenSeatType.D;
    }
  }

  startAnimation(Offset pixelsPerSecond, Size size) {
    ///控制器关联Tween动画
    // _dargAlignment = Alignment(dx, dy);
    // setState(() {});
    _animation = _animationController
        .drive(AlignmentTween(begin: _dargAlignment, end: Alignment(dx, dy)));

    ///加个弹簧
    SpringSimulation springSimulation = SpringSimulation(
        SpringDescription(
            mass: 10,

            ///质量
            stiffness: 1000,

            ///硬度
            damping: 0.75

            ///阻尼系数
            ),
        0,
        1,
        -0.5 *
            Offset(pixelsPerSecond.dx / size.width,
                    pixelsPerSecond.dy / size.height)
                .distance);
    _animationController.animateWith(springSimulation);
  }

  setSelect(bool isSelect) {
    widget.isSelect = isSelect;
  }
}
