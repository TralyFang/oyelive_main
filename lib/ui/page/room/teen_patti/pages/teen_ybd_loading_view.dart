import 'dart:async';


import 'package:flustars/flustars.dart' as s;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constant/teen_ybd_patti_const.dart';

class YBDTeenLoadingView extends StatefulWidget {
  @override
  _YBDTeenLoadingViewState createState() => _YBDTeenLoadingViewState();
}

class _YBDTeenLoadingViewState extends State<YBDTeenLoadingView> {
  double radio = 0;
  late s.TimerUtil _timer;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _timer = s.TimerUtil(mTotalTime: 6 * 1000);
    _timer.setOnTimerTickCallback((millisUntilFinished) {
      // print("radio: $radio");
      if (radio > 0.99) {
        _timer.cancel();
        return;
      }
      radio += 0.99 / 6.0;
      if (!mounted) return;
      setState(() {});
    });
    Future.delayed(Duration(milliseconds: 500), () {
      _timer.startCountDown();
    });
  }
  void initStateOcQYZoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    _timer.cancel();
    super.dispose();
  }
  void disposeu5Razoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pop();
      },
      child: Container(
        width: double.infinity,
        height: ScreenUtil().setHeight(666),
        color: Colors.black.withOpacity(0.49),
        child: Center(
          child: Container(
            width: ScreenUtil().setWidth(200),
            height: ScreenUtil().setHeight(170),
            child: Column(
              children: [
                Image.asset(
                  "${YBDConst.iconPrefix}loading@2x.png",
                  // width: ScreenUtil().setWidth(118),
                  height: ScreenUtil().setHeight(106),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(10),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(ScreenUtil().setHeight(14) / 2.0),
                  child: LinearProgressIndicator(
                    backgroundColor: Colors.white,
                    value: radio,
                    minHeight: ScreenUtil().setHeight(14),
                    valueColor: AlwaysStoppedAnimation(Color(0xff47CDCC)),
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(20),
                ),
                Container(
                  child: Text("loading ${(radio * 100).toStringAsFixed(0)}%",
                      style: TextStyle(
                          decoration: TextDecoration.none,
                          color: Colors.white,
                          fontSize: ScreenUtil().setSp(20),
                          fontWeight: FontWeight.normal)),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  void buildKug7woyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
