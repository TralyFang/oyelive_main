import 'dart:async';


import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' as U;
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_loading_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/enum_ext/ui_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/room_ybd_message_helper.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/bloc/teen_ybd_patti_bloc.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/bus_event/event_ybd_fn.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/constant/teen_ybd_patti_const.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/controller/teen_ybd_patti_ctrl.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_image_util.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_settle_result_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/enum/teen_ybd_state.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/ext/int_ybd_ext.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/game_ybd_code_item.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_curtain.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_loading_view.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_card_view.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_darg_btn.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_seat.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_time_view.dart';
import 'package:oyelive_main/ui/page/room/widget/number_ybd_dance_widget.dart';
import 'package:oyelive_main/ui/widget/colored_ybd_safe_area.dart';

enum TPEnterType {
  TPETGameCenter, //gamecenter
  TPETRoomIcon, // room
  TPETBigEvent, //大事件
  TPETInvite, //邀请弹窗
  TPETBeans, //领取beans 入口弹窗
  TPETActive, //活动入口
  TPTask, //任务入口
}

class YBDTeenPickerView extends StatefulWidget {
  GlobalKey<YBDTeenCardViewState> gKey = GlobalKey();

  @override
  _YBDTeenPickerViewState createState() => _YBDTeenPickerViewState();
}

GlobalKey balanceBeansKey = GlobalKey(debugLabel: "beans");

class _YBDTeenPickerViewState extends State<YBDTeenPickerView> with AutomaticKeepAliveClientMixin {
  GlobalKey<YBDTeenSeatState> aKey = GlobalKey(debugLabel: "a");
  bool isLoading = true;

  YBDTeenPattiInfo? _info;

  List<int?> moneyArray = [0, 0, 0];

  late YBDTeenPattiCtrl _tpCtrl;

  late StreamSubscription<YBDMessage> _listen;

  Widget? _teanBeansDanceWidget;
  Widget? _teanBodyItem;
  Widget? _teenMarquee;

  StreamSubscription? sSubscription;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    TA.timeEvent(YBDTATrackEvent.generic_loading_time);

    _tpCtrl = Get.put(YBDTeenPattiCtrl());

    _tpCtrl.getTeenRecord();

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: YBDLocationName.ROOM_PAGE,
      itemName: '${YBDItemName.TEEN_PATTI} + ${YBDTeenInfo.getInstance().enterType?.toString() ?? ''}',
    ));

    YBDTeenInfo.getInstance().getNew();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      // if (YBDTeenInfo.getInstance().pattiInfo != null) {
      //   _info = YBDTeenInfo.getInstance().pattiInfo;
      //   dealWithNotif();
      // }
      addListener();
    });
    ApiHelper.getBalance(context, () {
      // ApiHelper.checkLogin(context).then((value) {
      // if (!mounted)return;
      eventBus.fire(YBDSeatSelectEvent()
        ..money = 0
        ..index = -1);
    });
    YBDSPUtil.getUserInfo().then((value) {
      YBDTeenInfo.getInstance().money = value!.money ?? 0;
      logger.i('Royal Pattern picker view init money: ${value.money}');
    });

    // eventBus.on<YBDSeatSelectEvent>().listen((event) {
    //     if (event.index == -1)return;
    //     selectSeatArray[event.index] = 1;
    // });
    sSubscription = eventBus.on<YBDYouEvent>().listen((event) {
      moneyArray[event.index] = event.money;
    });

    // eventBus.on<YBDReloadLoadingEvent>().listen((event) {
    //   dealWithNotif();
    // });

    YBDTeenInfo.getInstance().open = true;
  }

  addListener() {
    _listen = YBDRoomMessageHelper.getInstance().messageOutStream.listen((YBDMessage event) {
      Widget item;
      if (event is YBDTeenPattiInfo) {
        _info = event;
        dealWithNotif();
        //仅仅ui展示 没有涉及业务
        _tpCtrl.gameId.value = _info!.gameCode.toString();
      } else if (event is YBDTeenSettleResultEntity) {
        // 只有下注了，并且赢了，才会收到这个报文。
        _tpCtrl.isWaiting.value = false; // 关闭
        // 结算赢钱了，提示下
        if ((event.content is YBDTeenSettleResultContent) && event.content?.winCoins != null) {
          showWinOrFailTip(true, event.content!.winCoins!.toDouble());
        }
        // Future.delayed(Duration(seconds: 20), () {
        //event?.content?.gameId ==
        _tpCtrl.didReceiveSocketBalance(
            event,
            (event.content?.gameId ?? -1) == (int.tryParse(_tpCtrl.gameId.value) ?? -2)
                ? 0
                : moneyArray.reduce((value, element) => value! + element!));
        // });

      }
    });
  }

  dealWithNotif() {
    ///会随机的出现初始化数据失败的情况
    ///{"roomId":2099955,"destination":"/room/2099955","costTime":[6],"msgType":"command","fromUser":-1,"toUser":2099997,"mode":"u","sequence":5,"commandId":-21,"code":"000000","desc":"Success!","result":null}
    if (_info!.result == null && _info!.content == null && YBDTeenInfo.getInstance().pattiInfo == null) {
      ///如果后台有bug  就一直loading过不去
      ///延迟一下 可能一段时间疯狂输出日志 造成卡死
      Future.delayed(Duration(seconds: 3), () {
        YBDRoomSocketApi.getInstance().sendTeenPatti(roomId: _info!.roomId);
      });
      return;
    }

    // 刚开始拉取初始化teenpatti信息
    if (_info!.commandId == -21) {
      if (_info!.result != null) {
        // 拿到初始化数据的，可下注的
        YBDTeenInfo.getInstance().setMoneyWithResult(_info!.result!);
        _tpCtrl.isWaiting.value = false;
      } else {
        // 结算中
        _tpCtrl.isWaiting.value = true;
      }
    }
    YBDTeenInfo.getInstance().roomId = _info!.roomId;
    YBDTeenInfo.getInstance().gameId = _info!.gameCode;
    if (_info!.content == null || (_info!.content != null && isLoading)) {
      isLoading = false;
      YBDLoadTrack().loadingTrack(YBDTAProps(type: YBDTATimeTrackType.liveroom_game_load, name: 'Royal Pattern'));
      if (!mounted) return;
      setState(() {});
      if (_info!.content == null) {
        return;
      }
      logger.v("tp init socket");
    }

    ///{"roomId":2099955,"destination":"/room/2099955","costTime":[41],"msgType":"command","fromUser":-1,"toUser":2099997,"mode":"u","sequence":148,"commandId":-22,"code":"900002","desc":"Database operation failed!","result":null}
    // print("teenPatti: ${_info.content.totalBidValue1},${_info.content.totalBidValue2},${_info.content.totalBidValue3}");
    if (_info!.content!.status == 2) {
      //已经结束，等待开局+等待下一盘开局
      // 还原初始状态，可以切换vip
      Get.find<YBDTeenPattiCtrl>().makeABet.value = false;
      YBDTeenInfo.getInstance().hasResult = true;
      _tpCtrl.isWaiting.value = false; // 开牌了
      logger.v("Royal Pattern open card : ${_info!.remainSecToNextStatus}, ${_info!.content!.hands}");
      eventBus.fire(YBDToatlMoneyEvent()..rs = _info!.content);

      ///先看看是第几个位置赢了
      int index = -1;
      double win = 0;
      bool isWin = false;
      _info!.content!.hands!.forEach((e) {
        index++;
        if (e.winner!) {
          // isWin = selectSeatArray[index] > 0;
          isWin = moneyArray[index]! > 0;
          win = moneyArray[index]! * _info!.content!.prizeTimes!; //event.content.getWinM(index);
        }
        logger.v("Royal Pattern event hand : ${e.winner}, $index $isWin $win, $moneyArray");
      });
      eventBus.fire(YBDEventFn()
        ..time = _info!.remainSecToNextStatus
        ..hands = _info!.content!.hands
        ..transforCard = true
        ..needstage = false);

      ///当前赢的+余额
      ///这一步计算有没有必要 还是完全等接口刷新
      YBDSPUtil.getUserInfo().then((value) {
        double a = value!.money! + win;
      });

      ///记录当前局是否win
      _tpCtrl.setWin(isWin);
      _tpCtrl.makeLastIsWin = isWin;
      if (!isWin) {
        // 没有赢，就直接延迟1s显示加载动画（便于用户开到开奖结果,或等翻牌结束），否则需要等落豆子动画结束之后再显示
        int transform = TeenConfigInfo.teenCardTransform() * 2;
        Duration duration = Duration(milliseconds: transform + 500);
        Future.delayed(duration, () {
          _tpCtrl.isWaiting.value = true;
        });
        // 下注了，没赢。
        if (moneyArray.map((e) => e! > 0).toList().contains(true) && !isWin) {
          showWinOrFailTip(false, 0.0);
        }
      } else {
        // 赢了，等待结算报文
        _tpCtrl.openCardMilliseconds = DateTime.now().millisecondsSinceEpoch;
      }

      ///刷新余额
      Future.delayed(Duration(seconds: 3), () {
        if (mounted) reloadMoney(isWin);
      });

      // if (moneyArray.map((e) => e > 0).toList().contains(true)) {
      //   showWinOrFailTip(isWin, win);
      // }

      moneyArray = [0, 0, 0];
    } else if (_info!.content!.status == 1) {
      logger.v(
          "Royal Pattern  pickerview status 1 , time:${YBDTeenInfo.getInstance().time}, gameid is not equal: ${_info?.gameCode != int.tryParse(_tpCtrl.gameId.value)}, _info.gameCode:${_info?.gameCode}, _tpCtrl.gameid:${_tpCtrl.gameId.value}");

      ///等待中
      if (YBDTeenInfo.getInstance().time == 0 && _info?.gameCode != int.tryParse(_tpCtrl.gameId.value)) {
        //测试环境30 灰度以后25 //_info.remainSecToNextStatus >= 25
        _tpCtrl.setWin(false); // 还原初始状态
        _tpCtrl.isWaiting.value = false; // 还原初始状态
        YBDTeenInfo.getInstance().hasResult = false;
        _tpCtrl.socketSyncWin = false; // 不是同一局游戏了，还原初始状态
        logger.v("Royal Pattern timer over : ${YBDTeenInfo.getInstance().time}");
        YBDTeenInfo.getInstance().cleanMoney();
        reloadMoney(false);
        eventBus.fire(YBDEventFn()
          ..time = _info!.remainSecToNextStatus
          ..hands = _info!.content!.hands
          ..transforCard = true
          ..needstage = false);
      }
      if (_info!.content != null &&
          (_info!.content!.totalBidValue1! > 0 || _info!.content!.totalBidValue2! > 0 || _info!.content!.totalBidValue3! > 0)) {
        eventBus.fire(YBDToatlMoneyEvent()..rs = _info!.content);
      }
    }
  }

  reloadMoney(bool isWin) {
    ApiHelper.getBalance(context, () {
      YBDTeenInfo.getInstance().betValue = 0;
      if (isWin) {
        if (!TeenConfigInfo.teenCheckBalance()) {
          return; // 不需要http拉取，仅仅 YBDTeenSettleResultEntity 更新余额
        }
        _tpCtrl.canAnimationd();
      } else {
        // 没赢钱也要做余额更新
        eventBus.fire(YBDSeatSelectEvent()
          ..money = 0
          ..index = -1);
      }

      // });
    });
  }
  void initState7vFVhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Size size = MediaQuery.of(context).size;
    if (isLoading) {
      return YBDTeenLoadingView();
    }
    // ;
    // super.build(context);
    logger.v('Royal Pattern picker view setState: ${_info?.remainSecToNextStatus}');
    return BlocProvider<YBDTeenPattiBloc>(
      create: (context) => YBDTeenPattiBloc(
        YBDTeenPattiBlocState()..currentSelectSeatType = YBDTeenSeatType.D,
      ),
      child: YBDColoredSafeArea(
        child: Container(
            color: Colors.transparent,
            width: double.infinity,
            height: YBDConst.headerH / 2.0 + YBDConst.bodyH,
            child: Stack(
              children: <Widget>[
                YBDTeenBgItem(),
                YBDTeenCurtain(true),
                YBDTeenCurtain(false),
                Container(
                    color: Colors.transparent, width: double.infinity, height: double.infinity, child: teanBodyItem()),
                // YBDTeenVipItem(),
                ...rhList(),
                ...getBtnList(),
                teenMarquee()!,
                YBDTeenTimerView(),
                YBDTeenTimeTipLab(
                    YBDTeenInfo.getInstance().time! > 0 ? YBDTeenInfo.getInstance().time : _info!.remainSecToNextStatus),

                ///不知道为什么 -2
                YBDGameCodeItem(),
                teanBeanDanceWidget()!,
                YBDTeenWaitingWidget(),
              ],
            )),
      ),
    );
  }
  void buildRfdN0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///RH item
  List<Widget> rhList() {
    return [TeenRHItemType.TeenVip, TeenRHItemType.TeenRecords, TeenRHItemType.TeenHistory]
        .map((e) => YBDTeenRHItem(e))
        .toList();
  }
  void rhList0hkRroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget? teenMarquee() {
    if (_teenMarquee == null) {
      _teenMarquee = YBDTeenMarquee();
    }
    return _teenMarquee;
  }

  Widget? teanBeanDanceWidget() {
    if (_teanBeansDanceWidget == null) {
      _teanBeansDanceWidget = YBDTeanBeansDanceWidget();
    }
    return _teanBeansDanceWidget;
  }

  Widget? teanBodyItem() {
    if (_teanBodyItem == null) {
      _teanBodyItem = YBDTeenBodyItem();
    }
    return _teanBodyItem;
  }

  void showWinOrFailTip(bool isWin, double win) {
    OverlayEntry entry = OverlayEntry(
      builder: (context) => Positioned(
        child: Container(
          alignment: Alignment.center,
          width: MediaQuery.of(context).size.width,
          child: AnimatedOpacity(
            opacity: 1,
            duration: Duration(milliseconds: 200),
            child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(color: Color(0xff6DB5C2), borderRadius: BorderRadius.circular(5)),
              width: U.ScreenUtil().setWidth(500),
              height: U.ScreenUtil().setHeight(60),
              child: Text(
                isWin
                    ? (translate("congrats_you_won") + " ${win.toStringAsFixed(0)} " + translate("beans"))
                    : translate("better_luck_next_time"),
                style:
                    TextStyle(fontSize: U.ScreenUtil().setSp(25), color: Colors.white, decoration: TextDecoration.none),
              ),
            ),
          ),
        ),
        bottom: 30 + U.ScreenUtil().bottomBarHeight,
      ),
    );
    Overlay.of(context)!.insert(entry);
    Future.delayed(Duration(seconds: 2), () async {
      entry.remove();
    });
  }
  void showWinOrFailTipLLKmfoyelive(bool isWin, double win) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///拖拽显示钱币图标
  List<Widget> getBtnList() {
    List<YBDMoneyType> typeList = [
      YBDMoneyType.ten,
      YBDMoneyType.ten,
      YBDMoneyType.fifty,
      YBDMoneyType.fifty,
      YBDMoneyType.hundred,
      YBDMoneyType.hundred,
      YBDMoneyType.thousand,
      YBDMoneyType.thousand
    ];
    int index = -1;
    return typeList.map((e) {
      index++;
      return Positioned(
        child: Container(
          child: YBDDargBtn(
            type: e,
            isdarg: index % 2 != 0,
          ),
        ),
      );
    }).toList();
  }
  void getBtnListm0kCWoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    YBDTeenInfo.getInstance().clean();
    Get.delete<YBDTeenPattiCtrl>();
    _listen.cancel();
    sSubscription?.cancel();
    super.dispose();
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class YBDMoneyContainer extends StatefulWidget {
  @override
  _YBDMoneyContainerState createState() => _YBDMoneyContainerState();
}

class _YBDMoneyContainerState extends State<YBDMoneyContainer> {
  int? money;

  late int m;

  StreamSubscription? streamSubscription;

  // 余额数值的动画key
  GlobalKey<YBDNumberDancefulWidgetState> balanceDanceKey = GlobalKey<YBDNumberDancefulWidgetState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getM();
    logger.v('Royal Pattern  bean view init');
    streamSubscription = eventBus.on<YBDSeatSelectEvent>().listen((event) {
      if (event.money == 0 && event.index == -1) {
        //更新了本地beans 下次全部换成get
        logger.v('Royal Pattern  bean view reload');
        getM(); // 这里是结算赢钱了,也可能是同一局之前有下注初始化的
        return;
      }
      if (event.money == 0 && event.index == -2) {
        //方便实时禁灰按钮
        YBDTeenInfo.getInstance().money = money ?? 0;
        logger.i('Royal Pattern index=-2 money: $money');
        return;
      }
      // money += event.money;
      money = m + event.money!;
      if (!mounted) return;
      // setState(() {});
      logger.i(
          'Royal Pattern  current money: $money , $m, ${YBDTeenInfo.getInstance().betValue}, ${balanceDanceKey.currentState}');
      balanceDanceKey.currentState!.toValue(money ?? 0, duration: Duration(milliseconds: 300));
    });
  }
  void initState0vdT0oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getM() {
    YBDSPUtil.getUserInfo().then((value) {
      money = value!.money ?? 0;
      m = money! + YBDTeenInfo.getInstance().betValue;
      YBDTeenInfo.getInstance().money = money ?? 0;

      logger.i(
          'Royal Pattern money: $money, $m , ${YBDTeenInfo.getInstance().betValue}, ${balanceDanceKey.currentState}');
      if (!mounted) return;
      if (balanceDanceKey.currentState!.lastValue == 0) {
        // 初始化余额
        balanceDanceKey.currentState!.resetValue(money ?? 0, duration: Duration(milliseconds: 1));
      } else {
        // 不要从零开始跳动，只在下注赢钱的时候才跳动。豆子动画需要在座位豆子动画之后，也就是结算到我的余额之后才做的动画
        balanceDanceKey.currentState!.resetValue(money ?? 0, duration: Duration(milliseconds: 1000));
      }
      // setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    money = (money ?? 0) < 0 ? 0 : money;
    return Container(
      height: YBDConst.bottom_vH,
      color: Colors.transparent,
      padding: EdgeInsets.only(
        left: YBDConst.bottom_top,
      ),
      child: Row(
        mainAxisAlignment: IntlMAAlignment.start,
        children: <Widget>[
          // SizedBox(
          //   width: YBDConst.bottom_top,
          // ),
          Column(
            children: [
              // SizedBox(height: 10,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Stack(
                    children: [
                      Image.asset(
                        '${YBDConst.iconPrefix}beans@2x.png',
                        key: balanceBeansKey,
                        height: U.ScreenUtil().setWidth(26),
                        fit: BoxFit.fitHeight,
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      height: YBDConst.bottom_Money_H,
                      alignment: Alignment.bottomCenter,
                      child: YBDNumberDancefulWidget(
                        key: balanceDanceKey,
                        duration: Duration(milliseconds: 700),
                        childBlock: (value) {
                          return Text(
                            value < 1000
                                ? value.toString()
                                : NumberFormat('0,000').format(value).toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: U.ScreenUtil().setSp(28),
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.none),
                          );
                        },
                      )),
                ],
              ),
              GestureDetector(
                onTap: () async {
                  if ((YBDTeenInfo.getInstance().isNew ?? '0') == '1') {
                    YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.daily_task);
                    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                      YBDEventName.CLICK_EVENT,
                      location: YBDLocationName.ROOM_PAGE,
                      itemName: YBDItemName.TEEN_PATTI_GET_FREE,
                    ));
                    return;
                  }
                  YBDNavigatorHelper.openTopUpPage(context).then((value) {
                    ApiHelper.getBalance(context, () {
                      // ApiHelper.checkLogin(context).then((value) {
                      if (!mounted) return;
                      eventBus.fire(YBDSeatSelectEvent()
                        ..money = 0
                        ..index = -1);
                    });
                  });
                },
                child: Container(
                  child: Obx(
                    () => Image.asset(
                      YBDTeenImageUtil.getTeenTopUp(Get.find<YBDTeenPattiCtrl>().isSelectVip.value),
                      height: YBDConst.bottom_TopUP_H,
                      width: YBDConst.bottom_TopUP_W,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    streamSubscription?.cancel();
    YBDSPUtil.save(Const.TP_NEW_USER, '0');
    super.dispose();
  }
}
  void buildubFZ3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
