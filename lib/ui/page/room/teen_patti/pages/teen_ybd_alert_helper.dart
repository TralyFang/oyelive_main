import 'dart:async';


import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../entity/ybd_teen_info.dart';

class YBDTeenAlertHelper {
  OverlayEntry? overlay;

  showTip(BuildContext context, int money) {
    // print("moeny compare: $money, ${YBDTeenInfo.getInstance().money}, ${YBDTeenInfo.getInstance().youA}, ${YBDTeenInfo.getInstance().youB},${YBDTeenInfo.getInstance().youC}");
    if (money ==
        (YBDTeenInfo.getInstance().money! +
            YBDTeenInfo.getInstance().youA +
            YBDTeenInfo.getInstance().youB +
            YBDTeenInfo.getInstance().youC)) return;
    bool isShow = true;
    overlay = OverlayEntry(
      builder: (context) => Positioned(
        child: Container(
          alignment: Alignment.center,
          width: MediaQuery.of(context).size.width,
          child: AnimatedOpacity(
            opacity: isShow ? 1 : 0,
            duration: Duration(milliseconds: 200),
            child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xff47CDCC), width: 3),
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white),
              width: ScreenUtil().setWidth(500),
              height: ScreenUtil().setHeight(300),
              child: Column(children: [
                Container(
                  padding: EdgeInsets.only(top: ScreenUtil().setWidth(10), right: ScreenUtil().setWidth(10)),
                  height: ScreenUtil().setHeight(80),
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                    onTap: () {
                      isShow = false;
                      overlay!.markNeedsBuild();
                      // await Future.delayed(Duration(milliseconds: 200));
                      overlay!.remove();
                      overlay = null;
                    },
                    child: Container(
                      // 关闭按钮
                      width: ScreenUtil().setWidth(48),
                      height: ScreenUtil().setWidth(48),
                      child: Image.asset(
                        'assets/images/dc/daily_check_close_btn.png',
                        color: Color(0xff626262),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: ScreenUtil().setWidth(38), right: ScreenUtil().setWidth(38)),
                  child: Text(
                    'Due to network delays, your beans will arrive within 10 minutes.',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(28),
                        color: Colors.black,
                        decoration: TextDecoration.none,
                        fontWeight: FontWeight.normal),
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setWidth(38),
                ),
                GestureDetector(
                  onTap: () {
                    isShow = false;
                    overlay!.markNeedsBuild();
                    // await Future.delayed(Duration(milliseconds: 200));
                    overlay!.remove();
                    overlay = null;
                  },
                  child: Container(
                    width: ScreenUtil().setWidth(283),
                    height: ScreenUtil().setWidth(67),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(ScreenUtil().setWidth(33.5)),
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          const Color(0xFF5E94E7),
                          const Color(0xFF47CDCC),
                        ],
                      ),
                    ),
                    child: Text(
                      'OK',
                      style: TextStyle(
                          color: Colors.white, fontSize: ScreenUtil().setSp(28), decoration: TextDecoration.none),
                    ),
                  ),
                )
              ]),
            ),
          ),
        ),
        // bottom: 30 + ScreenUtil().bottomBarHeight,
      ),
    );
    Overlay.of(context)!.insert(overlay!);
  }
}
