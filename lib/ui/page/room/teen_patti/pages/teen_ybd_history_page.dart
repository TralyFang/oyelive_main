import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/constant/const.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../widget/colored_ybd_safe_area.dart';
import '../../../../widget/loading_ybd_circle.dart';
import '../../../../widget/my_ybd_refresh_indicator.dart';
import '../../../../widget/scaffold/nav_ybd_back_button.dart';
import '../constant/teen_ybd_patti_const.dart';
import '../entity/teen_ybd_patti_entity.dart';

class YBDTeenHistoryPage extends StatefulWidget {
  YBDTeenHistoryPage();

  @override
  _YBDTeenHistoryPageState createState() => _YBDTeenHistoryPageState();
}

class _YBDTeenHistoryPageState extends BaseState<YBDTeenHistoryPage> {
  List<YBDTeenPattiHistoryRecordEntity?> _historyList = [];

  /// 是否为初始状态
  bool _isInitState = true;

  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _onRefresh();
    });
  }
  void initStateGkLFAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
          body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xff6DB5C2),
              Color(0xff6DB5C2),
              Color(0xff55CBDD),
              Color(0xff55CBDD),
              Color(0xff55CBDD),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(children: [
          YBDColoredSafeArea(
            bottom: false,
            child: Container(
              width: ScreenUtil().screenWidth,
              height: ScreenUtil().setHeight(96),
              child: Stack(
                children: <Widget>[
                  Positioned(
                      // 导航栏返回按钮
                      bottom: 0, // ScreenUtil().setHeight(13),
                      left: 0,
                      child: YBDNavBackButton()),
                ],
              ),
            ),
          ),
          Container(
            child: Image.asset(YBDConst.iconPrefix + "h_top@2x.png"),
          ),
          seatsContainer(),
          Expanded(
              child: (_historyList.isEmpty || _historyList.isEmpty)
                  ? (_isInitState ? YBDLoadingCircle() : placeholderView('No Data'))
                  : SmartRefresher(
                      controller: _refreshController,
                      header: YBDMyRefreshIndicator.myHeader,
                      footer: YBDMyRefreshIndicator.myFooter,
                      enablePullDown: true,
                      enablePullUp: false,
                      onRefresh: () {
                        logger.v("user profile page pull down refresh");
                        _onRefresh();
                      },
                      child: ListView.builder(
                          padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(13)),
                          itemCount: _historyList.length, // 数据的数量
                          itemBuilder: (BuildContext context, int index) {
                            return listItam(_historyList[index]!);
                          } // 类似 cellForRow 函数
                          )))
        ]),
      )),
    );
  }
  void myBuildefrPyoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<bool> _onBackPressed() {
    Navigator.pop(context);
    return Future<bool>.value(true);
  }

  void _onRefresh() async {
    YBDTeenPattiHistoryEntity? entity = await ApiHelper.teenPattiHistory(context);
    if (entity == null || entity.returnCode != Const.HTTP_SUCCESS) {
      // 加载数据失败
      _refreshController.loadFailed();
    } else {
      _historyList = entity.record!.where((element) => element!.winIndex != 0).toList();
      _refreshController.refreshCompleted();
    }

    _isInitState = false;
    if (mounted) setState(() {});
  }
  void _onRefreshPOf3uoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 缺省图
  Widget placeholderView(String text) {
    return Container(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Image.asset("assets/images/empty/empty_my_profile.webp", width: ScreenUtil().setWidth(382)),
      SizedBox(height: ScreenUtil().setWidth(30)),
      Text(text, style: TextStyle(color: Colors.white))
    ]));
  }
  void placeholderView8e6I2oyelive(String text) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget seatsContainer() {
    return Container(
      padding: EdgeInsets.only(top: ScreenUtil().setHeight(36), bottom: ScreenUtil().setHeight(27)),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
              width: MediaQuery.of(context).size.width / 3,
              alignment: Alignment.center,
              child: Image.asset(YBDConst.iconPrefix + "h_a@2x.png",
                  width: ScreenUtil().setWidth(120))), //height: ScreenUtil().setHeight(120),
          Container(
              width: MediaQuery.of(context).size.width / 3,
              alignment: Alignment.center,
              child: Image.asset(
                YBDConst.iconPrefix + "h_b@2x.png",
                width: ScreenUtil().setWidth(120),
              )),
          Container(
              width: MediaQuery.of(context).size.width / 3,
              alignment: Alignment.center,
              child: Image.asset(
                YBDConst.iconPrefix + "h_c@2x.png",
                width: ScreenUtil().setWidth(120),
              ))
        ],
      ),
    );
  }
  void seatsContainerFFbqxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget listItam(YBDTeenPattiHistoryRecordEntity entity) {
    return Container(
      padding: EdgeInsets.only(bottom: ScreenUtil().setHeight(20)),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
              width: MediaQuery.of(context).size.width / 3,
              alignment: Alignment.center,
              child: Image.asset(
                YBDConst.iconPrefix + (entity.winIndex == 1 ? "win" : "fail") + ".png",
                width: ScreenUtil().setWidth(entity.winIndex == 1 ? 62 : 58),
              )),
          Container(
              width: MediaQuery.of(context).size.width / 3,
              alignment: Alignment.center,
              child: Image.asset(
                YBDConst.iconPrefix + (entity.winIndex == 2 ? "win" : "fail") + ".png",
                width: ScreenUtil().setWidth(entity.winIndex == 2 ? 62 : 58),
              )),
          Container(
              width: MediaQuery.of(context).size.width / 3,
              alignment: Alignment.center,
              child: Image.asset(
                YBDConst.iconPrefix + (entity.winIndex == 3 ? "win" : "fail") + ".png",
                width: ScreenUtil().setWidth(entity.winIndex == 3 ? 62 : 58),
              ))
        ],
      ),
    );
  }
  void listItamLj1qKoyelive(YBDTeenPattiHistoryRecordEntity entity) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
