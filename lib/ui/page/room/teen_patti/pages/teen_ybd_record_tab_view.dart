import 'dart:async';



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

class YBDTeenRecordTabView extends StatefulWidget{

  void Function(bool isSelect)? callBack;

  YBDTeenRecordTabView({this.callBack});
  @override
  _YBDTeenRecordTabViewState createState() => _YBDTeenRecordTabViewState();
}

class _YBDTeenRecordTabViewState extends State<YBDTeenRecordTabView> {

  bool isSelect = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return getTopBtn();
  }
  void buildt5zxAoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget getTopBtn(){
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          rankingContainer(translate("daily_ranking"), isSelect),
          rankingContainer(translate('weekly_ranking'), !isSelect),
        ],
      ),
    );
  }

  Widget rankingContainer(String text, bool isSelect){
    return Opacity(
      opacity: isSelect ? 1 : 0.79,
      child: GestureDetector(
        onTap: (){
          if (isSelect)return;
          this.isSelect = !this.isSelect;
          setState(() {});
          widget.callBack!(this.isSelect);
        },
        child: Column(
          children: [
            Text(text, style: TextStyle(fontSize: isSelect ? ScreenUtil().setSp(32):ScreenUtil().setSp(28)),),
            Container(
              padding: EdgeInsets.only(top:  ScreenUtil().setHeight(20)),
              child: Container(
                width: ScreenUtil().setWidth(30),
                height: ScreenUtil().setHeight(4),
                decoration: BoxDecoration(
                    color: isSelect ? Color(0xff63FFDF) : Colors.transparent,
                    borderRadius: BorderRadius.circular(ScreenUtil().setHeight(2.5))
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class YBDTeenRecordTypetabView extends StatefulWidget{

  void Function(bool isSelect)? callBack;

  YBDTeenRecordTypetabView({this.callBack});
  @override
  _YBDTeenRecordTypetabViewState createState() => _YBDTeenRecordTypetabViewState();
}

class _YBDTeenRecordTypetabViewState extends State<YBDTeenRecordTypetabView> {

  bool isSelect = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return getRichest();
  }
  void buildnWILtoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget getRichest(){
    return Container(
      padding: EdgeInsets.only(top: ScreenUtil().setHeight(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          richestContainer(translate('richest_users'), isSelect),
          SizedBox(width: ScreenUtil().setWidth(30),),
          richestContainer(translate('luckiest_users'), !isSelect),
        ],
      ),
    );
  }

  Widget richestContainer(String text, bool isSelect){
    return GestureDetector(
      onTap: (){
        if (isSelect)return;
        this.isSelect = !this.isSelect;
        setState(() {});
        widget.callBack!(this.isSelect);
      },
      child: Container(
        width: ScreenUtil().setWidth(230),
        height: ScreenUtil().setHeight(64),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: isSelect ? Colors.white : Colors.transparent,
            borderRadius: BorderRadius.circular(ScreenUtil().setHeight(32)),
            border: Border.all(color: Colors.white.withOpacity(0.85),width: 1)
        ),
        child: Container(
          child: Text(text, style: TextStyle(fontSize: ScreenUtil().setSp(24),color: !isSelect ? Colors.white.withOpacity(0.85) : Color(0xff5CC4D4)),),
        ),
      ),
    );
  }
}