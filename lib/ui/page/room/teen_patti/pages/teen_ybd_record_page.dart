import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/constant/const.dart';
import '../../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../../common/util/image_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/sp_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../../module/user/entity/user_ybd_info_entity.dart';
import '../constant/teen_ybd_patti_const.dart';
import '../entity/teen_ybd_patti_entity.dart';
import 'teen_ybd_record_header_view.dart';
import 'teen_ybd_record_tab_view.dart';
import '../../../../widget/colored_ybd_safe_area.dart';
import '../../../../widget/loading_ybd_circle.dart';
import '../../../../widget/my_ybd_refresh_indicator.dart';
import '../../../../widget/scaffold/nav_ybd_back_button.dart';

class YBDTeenRecordPage extends StatefulWidget {
  YBDTeenRecordPage();

  @override
  _YBDTeenRecordPageState createState() => _YBDTeenRecordPageState();
}

class _YBDTeenRecordPageState extends BaseState<YBDTeenRecordPage> {
  List<YBDTeenPattiRankEntity?>? _list = [];

  /// 是否为初始状态
  bool _isInitState = true;

  RefreshController _refreshController = RefreshController();

  int _dateType = 1;
  bool _isRich = true;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _onRefresh();
    });
  }
  void initStateKxDtkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
          body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xff6DB5C2),
              Color(0xff6DB5C2),
              Color(0xff55CBDD),
              Color(0xff55CBDD),
              Color(0xff55CBDD),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(children: [
          SafeArea(
            bottom: false,
            child: Container(
              width: ScreenUtil().screenWidth,
              height: ScreenUtil().setWidth(116),
              child: Stack(
                children: <Widget>[
                  Positioned(
                      // 导航栏返回按钮
                      top: ScreenUtil().setWidth(24),
                      left: 0,
                      child: YBDNavBackButton()),
                  Center(
                    child: Text(translate("ranking_record"),
                        style: TextStyle(fontSize: ScreenUtil().setSp(32), color: Colors.white)),
                  ),
                ],
              ),
            ),
          ),
          YBDTeenRecordTabView(
            callBack: (a) {
              _dateType = a ? 1 : 2;
              _onRefresh();
            },
          ),
          YBDTeenRecordTypetabView(
            callBack: (a) {
              _isRich = a;
              _onRefresh();
            },
          ),
          getContainer(),
          Expanded(
              child: (_list!.isEmpty || _list!.isEmpty)
                  ? (_isInitState ? YBDLoadingCircle() : placeholderView('No Data'))
                  : Container(
                      padding: EdgeInsets.only(
                          top: ScreenUtil().setHeight(30),
                          left: ScreenUtil().setWidth(46),
                          right: ScreenUtil().setWidth(39),
                          bottom: ScreenUtil().bottomBarHeight),
                      child: Container(
                        decoration: BoxDecoration(
                            color: _list!.length > 3 ? Colors.white.withOpacity(0.1) : Colors.transparent,
                            borderRadius: BorderRadius.circular(3)),
                        child: ListView.builder(
                            // physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(13)),
                            itemCount: _list!.length > 3 ? _list!.length - 3 : 0, // 数据的数量
                            itemBuilder: (BuildContext context, int index) {
                              return listItam(_list![index + 3]!, index);
                            } // 类似 cellForRow 函数
                            ),
                      ),
                    ))
        ]),
      )),
    );
  }
  void myBuildZNCADoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<bool> _onBackPressed() {
    Navigator.pop(context);
    return Future<bool>.value(true);
  }

  void _onRefresh() async {
    YBDTeenPattiREntity? entity = await ApiHelper.teenPattiRecord(context, isRich: _isRich, dateType: _dateType);
    if (entity == null || entity.returnCode != Const.HTTP_SUCCESS) {
      // 加载数据失败
      _refreshController.loadFailed();
    } else {
      _list = entity.record!.rank;
      _refreshController.refreshCompleted();
    }

    _isInitState = false;
    if (mounted) setState(() {});
  }
  void _onRefreshERxmYoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 缺省图
  Widget placeholderView(String text) {
    return Container(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Image.asset("assets/images/empty/empty_my_profile.webp", width: ScreenUtil().setWidth(382)),
      SizedBox(height: ScreenUtil().setWidth(30)),
      Text(text, style: TextStyle(color: Colors.white))
    ]));
  }
  void placeholderViewNjUVRoyelive(String text) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget listItam(YBDTeenPattiRankEntity entity, int index) {
    return Container(
      padding: EdgeInsets.only(
          bottom: ScreenUtil().setHeight(20), left: ScreenUtil().setWidth(18), top: ScreenUtil().setWidth(32)),
      child: Row(
        children: [
          Container(
            child: Text("${index + 4}"),
            width: ScreenUtil().setWidth(40),
          ),
          SizedBox(
            width: 10,
          ),
          GestureDetector(
            onTap: () {
              doAvatarClick(entity.user!.id);
            },
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(ScreenUtil().setWidth(84) / 2.0),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(ScreenUtil().setWidth(84) / 2.0),
                  child: YBDNetworkImage(
                    imageUrl: YBDImageUtil.avatar(context, entity.user?.headimg, entity.user?.id),
                    width: ScreenUtil().setWidth(84),
                    height: ScreenUtil().setWidth(84),
                    fit: BoxFit.cover,
                    placeholder: (context, url) => Image.asset(
                      YBDResourcePathUtil.defaultMaleAssets(),
                      width: ScreenUtil().setWidth(84),
                      height: ScreenUtil().setWidth(84),
                      fit: BoxFit.fill,
                    ),
                  ),
                )),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(child: Text(entity.user!.nickname!)),
          Container(
            width: ScreenUtil().setWidth(130),
            // padding: EdgeInsets.only(right: ScreenUtil().setWidth(43)),
            alignment: Alignment.center,
            child: Column(
              children: [
                Image.asset(
                  YBDConst.iconPrefix + (_isRich ? "beans@2x.png" : "hg@2x.png"),
                  width: ScreenUtil().setWidth(_isRich ? 23 : 46),
                  height: ScreenUtil().setWidth(_isRich ? 25 : 38),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(7),
                ),
                Text(
                  entity.count.toString(),
                  style: TextStyle(fontSize: ScreenUtil().setSp(24)),
                ),
              ],
            ),
          ),
          // SizedBox(width: ScreenUtil().setWidth(43),)
        ],
      ),
    );
  }
  void listItamjSM7toyelive(YBDTeenPattiRankEntity entity, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget getContainer() {
    if (_list!.isEmpty) {
      return Container();
    }
    return Container(
      padding: EdgeInsets.only(top: ScreenUtil().setHeight(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: getListW(),
      ),
    );
  }

  getListW() {
    List<YBDTeenRecordHeaderView> ls = [];
    int index = 1;
    // _list.removeLast();
    for (var value in _list!) {
      if (ls.length < 3) {
        ls.add(YBDTeenRecordHeaderView(
          value,
          index == 1 ? 2 : (index == 2 ? 1 : index),
          _isRich,
          callBack: () {
            doAvatarClick(value!.user!.id);
          },
        ));
        index++;
      }
    }
    ls.sort((a, b) => (a.index.compareTo(b.index)));
    if (index < 4 && index > 1) {
      if (index == 2) {
        ls.insert(0, YBDTeenRecordHeaderView(null, index == 1 ? 2 : (index == 2 ? 1 : index), _isRich));
        ls.add(YBDTeenRecordHeaderView(null, index == 1 ? 2 : (index == 2 ? 1 : index), _isRich));
      } else {
        ls.add(YBDTeenRecordHeaderView(null, index == 1 ? 2 : (index == 2 ? 1 : index), _isRich));
      }
    } else {
      // ls.sort((a,b) => (a.index.compareTo(b.index)));
    }

    return ls;
  }

  doAvatarClick(int? userId) async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();

    logger.v('clicked round avatar button');

    if (null != userInfo && '${userInfo.id}' == '$userId') {
      logger.v('jump to my profile page');

      YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.myprofile);
    } else {
      // YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
      //     location: widget.locationForAnalytics, itemName: "user_profile"));
      logger.v('jump to other user profile page : $userId');
      YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.user_profile + "/$userId");
    }
  }
}
