import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import '../../../../../common/util/image_ybd_util.dart';
import '../../../../../module/user/entity/user_ybd_info_entity.dart';
import '../constant/teen_ybd_patti_const.dart';
import '../entity/teen_ybd_patti_entity.dart';

class YBDTeenRecordHeaderView extends StatelessWidget {
  YBDTeenPattiRankEntity? entity;
  int index;
  bool isRich;
  void Function()? callBack;
  YBDTeenRecordHeaderView(this.entity, this.index, this.isRich, {this.callBack});
  @override
  Widget build(BuildContext context) {
    if (entity == null) {
      return Container(
        height: 50,
        width: ScreenUtil().setWidth(index == 2 ? 192 : 172),
      );
    }
    return Container(
      padding: EdgeInsets.only(top: index == 2 ? 0 : ScreenUtil().setHeight(72)),
      width: ScreenUtil().setWidth(index == 2 ? 192 : 172),
      child: Stack(
        children: [
          Column(
            children: [
              SizedBox(
                height: index != 2 ? 40 : 20,
              ),
              ClipOval(
                child: GestureDetector(
                  onTap: () {
                    callBack!();
                  },
                  child: Container(
                      padding: EdgeInsets.all(2),
                      color: Colors.white,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(ScreenUtil().setWidth(index == 2 ? 137 : 108) / 2.0),
                          child: YBDNetworkImage(
                              imageUrl: YBDImageUtil.avatar(context, entity!.user!.headimg, entity!.user!.id, scene: "A"),
                              width: ScreenUtil().setWidth(index == 2 ? 137 : 108),
                              height: ScreenUtil().setWidth(index == 2 ? 137 : 108),
                              fit: BoxFit.cover,
                              placeholder: (context, url) => YBDResourcePathUtil.defaultMaleImage()))),
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: Container(
                  width: ScreenUtil().setWidth(146),
                  padding: EdgeInsets.only(bottom: ScreenUtil().setHeight(13)),
                  // height: ScreenUtil().setHeight(index == 2 ? 102 : 112),
                  decoration:
                      BoxDecoration(color: Colors.white.withOpacity(0.09), borderRadius: BorderRadius.circular(5)),
                  child: getContainer(),
                ),
              ),
            ],
          ),
          Positioned(
            child: GestureDetector(
                onTap: () {
                  callBack!();
                },
                child: Image.asset(YBDConst.iconPrefix + "num${index == 1 ? 2 : (index == 2 ? 1 : index)}@2x.png",
                    width: ScreenUtil().setWidth(index == 2 ? 192 : 172),
                    height: ScreenUtil().setHeight(index == 2 ? 143 : 124))),
            top: index == 2 ? 25 : 45,
          ),
          Positioned(
            child: Image.asset(YBDConst.iconPrefix + "hg@2x.png", width: ScreenUtil().setWidth(index == 2 ? 98 : 0)),
            top: 0,
            left: ScreenUtil().setWidth(49),
          )
        ],
      ),
    );
  }
  void builduBusxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget getContainer() {
    return Container(
      padding: EdgeInsets.only(top: ScreenUtil().setHeight(index == 2 ? 20 : 30)),
      child: Column(
        children: [
          Container(
              height: ScreenUtil().setHeight(30),
              child: Text(
                entity!.user!.nickname!,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: ScreenUtil().setSp(19.8), color: Colors.white),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              )),
          // SizedBox(height: ScreenUtil().setHeight(8),),
          Container(
            width: ScreenUtil().setWidth(73),
            height: ScreenUtil().setHeight(21),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Color(0xff18EAE4),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Text(
              "Lv" + entity!.user!.level.toString(),
              style: TextStyle(fontSize: ScreenUtil().setSp(13.2), color: Colors.white),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(8),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  YBDConst.iconPrefix + (isRich ? "beans@2x.png" : "hg@2x.png"),
                  width: ScreenUtil().setWidth(isRich ? 15 : 20),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(5),
                ),
                Text(
                  entity!.count.toString(),
                  style: TextStyle(fontSize: ScreenUtil().setSp(16.2), color: Colors.white),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
  void getContaineroEW3foyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
