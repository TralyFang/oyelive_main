import 'dart:async';



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/controller/teen_ybd_patti_ctrl.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_image_util.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/tp_ybd_vip_record_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';

class YBDTeenVipView extends StatelessWidget{
  bool isShowModeView;

  YBDTeenVipView(this.isShowModeView);

    @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return isShowModeView ? YBDTeenVipModeView(): YBDTeenVipSeatView();
  }
  void buildSkcoVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDTeenVipModeView extends StatelessWidget{
    @override
  Widget build(BuildContext context) {
    // TODO: implement build
      YBDTpVipRecordEntityRecord? record =  Get.find<YBDTeenPattiCtrl>().recordResult;
      int beans = record?.beans == null ? 0 : (record?.beans ?? 0);
      int max = record?.targetBeans == null ? 0 : (record?.targetBeans ?? 0);
      double pix = (max == 0) ? 0 : (beans / max);
    return Container(
        decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(ScreenUtil().setWidth(16))
      ),
      width: ScreenUtil().setWidth(580),
      height: ScreenUtil().setWidth(405),
      child: Column(
      children: [
         Container(
           padding: EdgeInsets.all(ScreenUtil().setWidth(40)),
           width: double.infinity,
           alignment: Alignment.center,
           child: Text('VIP Mode',style: TextStyle(color: Color(0xff080808),fontSize: ScreenUtil().setSp(28),decoration: TextDecoration.none),),
         ),
        Container(
          padding: EdgeInsets.only(left: ScreenUtil().setWidth(45),right:  ScreenUtil().setWidth(45)),
          child: Row(
            children: [
              Text("${beans < 1000 ? "$beans" : NumberFormat('0,000').format(beans).toString()}",style: TextStyle(color: Color.fromRGBO(143, 143, 143, 1),fontSize: ScreenUtil().setSp(24),decoration: TextDecoration.none),),
              Expanded(child: Container()),
              Text("${max < 1000 ? "$max" : NumberFormat('0,000').format(max).toString()}",style: TextStyle(color: Color.fromRGBO(143, 143, 143, 1),fontSize: ScreenUtil().setSp(24),decoration: TextDecoration.none),),
            ],
          ),
        ),
        SizedBox(height: ScreenUtil().setWidth(15),),
        Container(
          padding: EdgeInsets.only(left: ScreenUtil().setWidth(45),right:  ScreenUtil().setWidth(45)),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(ScreenUtil().setHeight(20) / 2.0),
              child: Container(
                width: double.infinity,
                height: ScreenUtil().setWidth(20),
                color: Color(0xffD9D9D9),
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(ScreenUtil().setHeight(20) / 2.0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(ScreenUtil().setWidth(20)),
                          gradient: LinearGradient(
                              colors:[
                                Color(0xffE0CC96),
                                Color(0xffF7DB9D),
                                Color(0xffF3D390),
                                Color(0xffDEC18C),
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter),
                        ),
                        width: ScreenUtil().setWidth(580 - 90) *pix,
                      ),
                    )
                  ],
                ),
              )
          ),
        ),
        SizedBox(height: ScreenUtil().setWidth(23),),
        Container(
          padding: EdgeInsets.only(left: ScreenUtil().setWidth(63),right: ScreenUtil().setWidth(43)),
          width: double.infinity,
          alignment: Alignment.center,
          child: Text('${translate("teenpatti_vip_mode_one")} ${(max - beans) < 1000 ? "${max - beans}" : NumberFormat('0,000').format(max - beans).toString()} ${translate("teenpatti_vip_mode_two")}',style: TextStyle(color: Color(0xff080808),fontSize: ScreenUtil().setSp(24),fontWeight: FontWeight.normal,decoration: TextDecoration.none,),textAlign: TextAlign.center,),
        ),
        SizedBox(height: ScreenUtil().setWidth(34),),
        GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Container(
            width: ScreenUtil().setWidth(300),
            height: ScreenUtil().setWidth(67),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(37.5)),
              gradient: LinearGradient(
                  colors:[
                    Color(0xff47CDCC),
                    Color(0xff3E76CC),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter),
            ),
            alignment: Alignment.center,
            child: Text('Ok',style: TextStyle(color: Colors.white ,fontSize: ScreenUtil().setSp(28),fontWeight: FontWeight.normal,decoration: TextDecoration.none),),
          ),
        ),
      ],
    ));
  }
  void buildtkoamoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}


class YBDTeenVipSeatView extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ///弹出VIP 默认 teenpatti 入口更改成vip
    YBDTeenInfo.getInstance().recordResult?.beans = YBDTeenInfo.getInstance().recordResult?.targetBeans;
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: ScreenUtil().setWidth(500)),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            children:[
              SizedBox(height: ScreenUtil().setWidth(100),),
              Stack(
                children: [
                  ClipRRect(child: Container(
                      color: Colors.white,
                      child: ClipRRect(borderRadius: BorderRadius.circular(16),child: Image.asset(YBDTeenImageUtil.TeenAlertBg,width: ScreenUtil().setWidth(580),height: ScreenUtil().setWidth(500),fit: BoxFit.fill,))),borderRadius: BorderRadius.circular(16),),
                  Container(
                    padding: EdgeInsets.only(top: ScreenUtil().setWidth(60),left: ScreenUtil().setWidth(43),right: ScreenUtil().setWidth(43)),
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(ScreenUtil().setWidth(16))
                    ),
                    width: ScreenUtil().setWidth(580),
                    height: ScreenUtil().setWidth(500),
                    child: Column(
                      children: [
                        SizedBox(height: ScreenUtil().setWidth(41),),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset(YBDTeenImageUtil.TeenSeatA, width: ScreenUtil().setWidth(110),),
                            Image.asset(YBDTeenImageUtil.TeenSeatB, width: ScreenUtil().setWidth(110),),
                            Image.asset(YBDTeenImageUtil.TeenSeatC, width: ScreenUtil().setWidth(110),),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setWidth(23),),
                        Container(
                          padding: EdgeInsets.only(left: ScreenUtil().setWidth(10),right: ScreenUtil().setWidth(10)),
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Text(translate("teenpatti_vip_tips"),style: TextStyle(color: Color(0xff080808),fontSize: ScreenUtil().setSp(24),fontWeight: FontWeight.normal,decoration: TextDecoration.none),textAlign: TextAlign.center,),
                        ),
                        SizedBox(height: ScreenUtil().setWidth(34),),
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Container(
                            width: ScreenUtil().setWidth(300),
                            height: ScreenUtil().setWidth(67),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(37.5)),
                              gradient: LinearGradient(
                                  colors:[
                                    Color(0xff47CDCC),
                                    Color(0xff3E76CC),
                                  ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter),
                            ),
                            alignment: Alignment.center,
                            child: Text('Ok',style: TextStyle(color: Colors.white ,fontSize: ScreenUtil().setSp(28),fontWeight: FontWeight.normal,decoration: TextDecoration.none),),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ) 
            ],
          ),
          Positioned(top: 0, child: Container(child: Image.asset(YBDTeenImageUtil.TeenAlertHead,width: ScreenUtil().setWidth(666),))),
        ],
      ),
    );
      
  }
  void build20V1Aoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}