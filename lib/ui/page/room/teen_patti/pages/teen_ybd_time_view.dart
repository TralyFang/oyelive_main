import 'dart:async';


import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' as s;
import 'package:get/get.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/controller/teen_ybd_patti_ctrl.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_image_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../main.dart';
import '../bloc/teen_ybd_patti_bloc.dart';
import '../bus_event/event_ybd_fn.dart';
import '../constant/teen_ybd_patti_const.dart';
import '../entity/ybd_teen_info.dart';

class YBDTeenTimerView extends StatefulWidget {
  @override
  _YBDTeenTimerViewState createState() => _YBDTeenTimerViewState();
}

class _YBDTeenTimerViewState extends State<YBDTeenTimerView> {
  bool needstage = !(YBDTeenInfo.getInstance().pattiInfo!.content != null &&
      YBDTeenInfo.getInstance().pattiInfo!.content!.remainSecToNextStatus == 0);

  final int alp = 30;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // print("YB: ${YBDTeenInfo.getInstance().pattiInfo.result}, ${YBDTeenInfo.getInstance().pattiInfo.content}");
    return BlocBuilder<YBDTeenPattiBloc, YBDTeenPattiBlocState>(
      builder: (context, state) {
        if (state.needstage != null) {
          needstage = state.needstage ?? false;
        }
        return Positioned(
          top: YBDConst.headerH - 20,
          left: 0,
          right: 0,
          bottom: 0,
          child: Offstage(
            offstage: needstage,
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.white.withAlpha(0),
                    Colors.white.withAlpha(20),
                    Colors.white.withAlpha(20),
                    Colors.white.withAlpha(20),
                    Colors.white.withAlpha(30),
                    Colors.white.withAlpha(30),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
  void buildjGFnLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDTeenTimeTipLab extends StatefulWidget {
  int? m;

  YBDTeenTimeTipLab(this.m);

  @override
  _YBDTeenTimeTipLabState createState() => _YBDTeenTimeTipLabState();
}

class _YBDTeenTimeTipLabState extends State<YBDTeenTimeTipLab> {
  TimerUtil? timer;
  late var fn;

  bool isCancle = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    logger.v('Royal Pattern time lab init');

    createTimer();

    fn = eventBus.on<YBDEventFn>().listen((event) {
      logger.v('Royal Pattern time lab upload: ${event.time}, ${event.hands}, ${YBDTeenInfo.getInstance().hasResult}');
      // if ((event.hands ?? []).isNotEmpty){ ///如果这个时候已经翻牌 停到定时器  网络延迟 给到客户端的时长已经多了
      //   logger.v('Royal Pattern time is cancel, and update again');
      //   timer.updateTotalTime(0);
      //   // timer.cancel();
      // }
      // if(YBDTeenInfo.getInstance().hasResult)return;
      widget.m = event.time;
      setState(() {});
    });
  }
  void initStateDa3NPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  createTimer() {
    if (timer != null) return;
    YBDTeenInfo.getInstance().time = widget.m;
    timer = TimerUtil(mTotalTime: widget.m! * 1000);
    timer!.setOnTimerTickCallback((millisUntilFinished) {
      widget.m = millisUntilFinished ~/ 1000;
      YBDTeenInfo.getInstance().time = widget.m;
      logger.v("Royal Pattern time : ${YBDTeenInfo.getInstance().time}");
      setState(() {});
      if (widget.m! <= 0) {
        // logger.v("Royal Pattern time1 : ${YBDTeenInfo.getInstance().time}");
        YBDTeenInfo.getInstance().time = 0;
        timer!.cancel();
        isCancle = true;
        Future.delayed(Duration(seconds: 3), () {
          isCancle = false;
        });
        BlocProvider.of<YBDTeenPattiBloc>(context).add(YBDTeenPattiEventCover()..needstage = false);
        Get.find<YBDTeenPattiCtrl>().isWaiting.value = true; // 倒计时结束，等待开牌
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    timer!.cancel();
    fn.cancel();
    super.dispose();
  }
  void disposebrhEXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Size size = MediaQuery.of(context).size;
    if (widget.m! > 0 && !isCancle) {
      logger.v("timer.mTotalTime : ${timer!.mTotalTime}");
      if (timer!.mTotalTime == 0) {
        timer!.mTotalTime = widget.m! * 1000;

        ///倒计时重新开始的时候清空 原下注信息
        logger.v("timer.mTotalTime1 : ${timer!.mTotalTime}");
        BlocProvider.of<YBDTeenPattiBloc>(context).add(YBDTeenPattiEventCover()
          ..cleanSeat = true
          ..needstage = true);
      }
      timer!.startCountDown();
    }
    return Positioned(
      top: YBDConst.headerH + s.ScreenUtil().setHeight(30),
      left: (size.width - YBDConst.tipH) / 2.0,
      right: (size.width - YBDConst.tipH) / 2.0,
      bottom: YBDConst.bodyH - YBDConst.headerH / 2.0 - YBDConst.tipH - s.ScreenUtil().setHeight(30),
      child: Offstage(
        offstage: widget.m! <= 0 || YBDTeenInfo.getInstance().hasResult,
        child: Obx(
            () {
              // logger.v("timer.Offstage : ${widget.m}, ${widget.m <= 0 || YBDTeenInfo.getInstance().hasResult},");
              return Get.find<YBDTeenPattiCtrl>().isSelectVip.value ? getIcon(): getContainer();
            })
      ),
    );
  }
  void build9N4rVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget getIcon(){
    return Stack(
      alignment: Alignment.center,
      children: [
        Image.asset(YBDTeenImageUtil.TeenTimer),
        Text(
          "${widget.m}",
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white, decoration: TextDecoration.none),
        ),
      ],
    );
  }

  Widget getContainer(){
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(YBDConst.tipH / 2.0),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffEF49BC),
            Color(0xffFF745C),
          ],
        ),
      ),
      child: Text(
        "${widget.m}",
        style: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white, decoration: TextDecoration.none),
      ),
    );
  }
}
