import 'dart:async';


import '../entity/teen_ybd_patti_info.dart';
import '../entity/teen_ybd_result.dart';

class YBDEventFn {
  List<YBDHands>? hands;
  int? time;

  ///当前遮罩是否需要显示出来 true 不展示
  bool? needstage;

  ///翻牌
  bool? transforCard;
}

class YBDSeatSelectEvent {
  int? index;
  int? money;
}

class YBDToatlMoneyEvent {
  YBDResult? rs;
}

class YBDYouEvent {
  late int index;
  int? money;
}

class YBDReloadLoadingEvent {}


class YBDOpenTp{
  int? tag;
}
