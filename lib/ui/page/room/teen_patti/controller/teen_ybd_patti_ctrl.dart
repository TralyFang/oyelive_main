import 'dart:async';


import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/bus_event/event_ybd_fn.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_result.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_settle_result_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/tp_ybd_vip_record_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_join_msg.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/enum/teen_ybd_state.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/ext/int_ybd_ext.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';

class YBDTeenPattiCtrl extends GetxController {
  var gameId = ''.obs;
  //vip 进行全局换皮肤
  var isSelectVip = false.obs;

  //下注报文
  var makeABet = false.obs;

  int getH() {
    return 0; //this.isSelectVip.value ? 40:0;
  }
  void getHhf3ONoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //
  var isWin = false.obs; // 绑定余额豆子动画

  var isSeatWin = false; // 座位有赢了

  var lastIsWin = false; // 当局是否赢了，仅仅是记录
  var makeLastIsWin = false; // 记录上一局是否赢。

  /// 等待下一局中
  var isWaiting = false.obs;
  var socketSyncWin = false; // 记录socket的结算余额与http拉取的余额状态。
  var openCardMilliseconds = 0; // 赢局是否还在开牌中，如果是那就等待，完了在冒豆子

  // YBDTeenPattiREntity entity;
  List<YBDTeenPattiRankEntity?>? rank = [];
  var showMarquee = false.obs;

  getTeenRecord() async {
    logger.v('tp record data is request');
    YBDTeenPattiREntity? entity = await ApiHelper.teenPattiRecord(Get.context, isRich: true, dateType: 1);
    if ((entity?.record?.rank?.isNotEmpty ?? false) && entity?.record?.rank?.length == 1) {
      rank!.add(entity?.record?.rank?.first);
      rank!.add(entity?.record?.rank?.first);
    } else {
      rank = entity?.record?.rank;
    }
    showMarquee.value = !showMarquee.value; //必须刷新
    logger.v('show marquee value: ${showMarquee.value}');
    update();
  }

  /// 椅子beans生成动画 + beans下落动画
  void setWin(bool iswin) {
    logger.v("*****>>>>setWin===$iswin");
    lastIsWin = iswin;
    if (!iswin) {
      // 没有赢钱就关闭图层了吧
      this.isSeatWin = false;
      this.isWin.value = false;
      update(['seatDance']);
    }
  }
  void setWinQgIAjoyelive(bool iswin) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 开始执行座位动画
  bool seatWinUpdateAnimation() {
    logger.v("*****>>>>seatWin===$lastIsWin");
    if (TeenConfigInfo.teenSeatDance()) {
      this.isSeatWin = lastIsWin;
      update(['seatDance']);
      return lastIsWin;
    }
    this.isSeatWin = false;
    update(['seatDance']);
    return false;
  }
  void seatWinUpdateAnimationxnahfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 延迟更新，只是为了确保时机。这个应该是不受下盘开始的影响
  void winUpdateAnimation() {
    logger.v("*****>>>>iswin===$lastIsWin");

    /// beans下落,
    /// 1. 还在当前局，赢的状态，执行动画 --
    /// 2. 如果下一盘还没下注，上一盘是赢得状态，且两次余额不同，执行动画
    this.isWin.value = isBalanceWin;
  }
  void winUpdateAnimationt0BoZoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  bool get isBalanceWin {
    return (lastIsWin ||
        (makeLastIsWin && !makeABet.value && YBDTeenInfo.getInstance().balance != YBDTeenInfo.getInstance().money));
  }

  List<Color> dargBtnColors() {
    return this.isSelectVip.value
        ? [const Color(0xFFE0CC96), const Color(0xFFF7DB9D), const Color(0xFFF3D390), const Color(0xFFDEC18C)]
        : [
            const Color(0xFFFFC3EC),
            const Color(0xFFFF745C),
          ];
  }
  void dargBtnColorspGrTxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    if (YBDTeenInfo.getInstance().recordResult != null) {
      isSelectVip.value =
          YBDTeenInfo.getInstance().recordResult?.beans == YBDTeenInfo.getInstance().recordResult?.targetBeans;
    }
  }

  YBDTpVipRecordEntityRecord? recordResult = null;

  Future<YBDTpVipRecordEntity?> queryTeenPattiRecord() async {
    YBDDialogUtil.showLoading(Get.context);
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: Get.context)!;
    Map<String, dynamic> params = {"userId": store.state.bean!.id};
    YBDTpVipRecordEntity? result = await YBDHttpUtil.getInstance().doGet<YBDTpVipRecordEntity>(
      Get.context,
      YBDApi.QUERY_TEENPATTI_VIP,
      params: params,
    );
    YBDDialogUtil.hideLoading(Get.context);
    recordResult = result?.record;
    if (result == null || result.returnCode != Const.HTTP_SUCCESS) {
      logger.v("get vip info fail");
      YBDToastUtil.toast('Network error!');
      return null;
    } else {
      return result;
    }
  }

  tapClick() async {
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: YBDLocationName.ROOM_PAGE,
      itemName: YBDItemName.TEEN_PATTI_VIP,
    ));
    if (this.isSelectVip.value) {
      if (this.makeABet.value) {
        YBDToastUtil.toast('Don’t allowed to switch modes during betting');
        return;
      }
      this.isSelectVip.value = !(this.isSelectVip.value);
      update();
      return;
    }
    YBDTpVipRecordEntity? result = await Get.find<YBDTeenPattiCtrl>().queryTeenPattiRecord();
    if (result == null) return;
    //如果是vip 下注了 就显示toast 没下注 直接换肤
    //不是vip的话 直接弹进度条
    if ((result.record?.beans ?? 0).toInt() != (result.record?.targetBeans ?? 0).toInt()) {
      YBDRoomUtil.showTeenPattiVipView(Get.context, true);
    } else {
      if (this.makeABet.value) {
        YBDToastUtil.toast('Don’t allowed to switch modes during betting');
        return;
      }
      this.isSelectVip.value = !(this.isSelectVip.value);
      update();
    }
  }

  // 接收到socket的余额更新 "msgType":"pub" "type":14, YBDTeenPattiCtrl
  // countMoney 当前局下注的总额
  void didReceiveSocketBalance(YBDTeenSettleResultEntity entity, int? countMoney) {
    // debugPrint('*****>>>>$entity===${entity.content.gameId}===${this.gameId.value}===${entity.content.userId}===${YBDRoomSocketApi.userId}');
    if (!(entity.content is YBDTeenSettleResultContent)) {
      return;
    }
    if (entity.content!.userId == YBDRoomSocketApi.userId && entity.content!.balance != null) {
      // logger.v('*****>>>>resultSocket');
      // 同一个用户，同一局游戏， 也有余额
      YBDUserUtil.updateUserMoney(entity.content!.balance! - countMoney!);
      YBDTeenInfo.getInstance().balance = entity.content!.balance! - countMoney;
      this.canAnimationd();
    }
  }
  void didReceiveSocketBalanceLuct5oyelive(YBDTeenSettleResultEntity entity, int? countMoney) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 判断是否可以直接执行毛豆动画
  void canAnimationd() {
    int nowMilliseconds = DateTime.now().millisecondsSinceEpoch;
    int waitMilliseconds = nowMilliseconds - openCardMilliseconds;
    int transform = TeenConfigInfo.teenCardTransform() * 2 + 500;
    int delayed = transform - waitMilliseconds;
    if (delayed <= 0) {
      // 翻牌动作已经结束了，可以直接冒豆了
      didUpdateBalanceToAnimation();
    } else {
      Future.delayed(Duration(milliseconds: delayed), () {
        didUpdateBalanceToAnimation();
      });
    }
  }

  // 已经接收到新的余额，需要去动画更新UI bean
  void didUpdateBalanceToAnimation() {
    // logger.v('*****>>>>animation====>>>${this.socketSyncWin}====${this.lastIsWin}');
    if (!(!this.socketSyncWin && isBalanceWin)) {
      return;
    }
    if (this.lastIsWin) {
      // 仅限同一局
      this.isWaiting.value = false;
    }
    this.socketSyncWin = true;
    // logger.v('*****>>>>animation.next====>>>${this.socketSyncWin}====${this.lastIsWin}');
    // 执行过动画了，就不用了。 座位没有冒豆子就不要延迟了
    bool dance = this.seatWinUpdateAnimation(); // 2.1s
    Future.delayed((dance ? Duration(milliseconds: 1500) : Duration(milliseconds: 1)), () {
      // 需要延迟1.5s去余额冒豆子
      this.winUpdateAnimation(); // 1.3s
      eventBus.fire(YBDSeatSelectEvent()
        ..money = 0
        ..index = -1);
    });
    // 让余额冒豆子之后，开启下一局等待动画。
    Future.delayed((dance ? Duration(milliseconds: 3300) : Duration(milliseconds: 1800)), () {
      if (this.lastIsWin) {
        // 只有上一局还在赢的状态才展示加载动画，下一局开始了，自动进入false状态
        this.isWaiting.value = true;
      }
    });
  }
  void didUpdateBalanceToAnimationU9xAkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///处理下注成功获取的报文更新 you
  /// seatType == null 的时候 就计算所有的下注金额
  Future<int> reloadMoney(YBDResult msg, YBDTeenSeatType? seatType) async {
    if (seatType == null) {
      return (msg.bidValue1 ?? 0).toInt() + (msg.bidValue2 ?? 0).toInt() + (msg.bidValue3 ?? 0).toInt();
    }
    int m = 0;
    switch (seatType) {
      case YBDTeenSeatType.A:
        m = (msg.bidValue1 ?? 0).toInt();
        break;
      case YBDTeenSeatType.B:
        m = (msg.bidValue2 ?? 0).toInt();
        break;
      case YBDTeenSeatType.C:
        m = (msg.bidValue3 ?? 0).toInt();
        break;
      case YBDTeenSeatType.D:
        break;
    }
    return m;
  }
}
