import 'dart:async';


enum YBDTeenState {
  ///  下注时
  lottery,

  /// 下注结束  执行开牌动画的中间过渡态
  end,

  /// 选中状态
  select,
}

///  座位枚举
enum YBDTeenSeatType { A, B, C, D}

///钱币枚举
enum YBDMoneyType {
  ten,
  fifty,
  hundred,
  thousand,
}

enum YBDTeenHeadClickType{
  question, close
}

enum YBDTeenMType{
  pot, you
}