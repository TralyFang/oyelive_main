import 'dart:async';




import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/entity/query_ybd_configs_resp_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_info.dart';

extension IntExt on int{
   int indexMilliseconds(List<YBDHands>? hands){
     if (hands == null || hands.length == 0)return 0;
     int s = TeenConfigInfo.teenCardTransform();
     return s * this;
   }
}


extension TeenConfigInfo on YBDConfigInfo {

  static int teenCardTransform() {
    final store = YBDCommonUtil.storeFromContext(context: Get.context)!;
    logger.v('tpCardtransform: ${store.state.configs?.tpCardTransform}');
    int s = int.tryParse(store.state.configs?.tpCardTransform ?? "0") ?? 0;
    if (s > 1000) {
      return 1000; // 限制最大值
    }
    return s;
  }

  static bool teenSeatDance() {
    final store = YBDCommonUtil.storeFromContext(context: Get.context)!;
    String? org = store.state.configs?.tpSeatDance;
    logger.v('tpSeatDance: $org');
    String res = org ?? "0";
    if (res == "1") { // 等于 "1" 的情况下才做座位冒豆子动画
      return true;
    }
    return false;
  }

  static bool teenCheckBalance() {
    final store = YBDCommonUtil.storeFromContext(context: Get.context)!;
    String? org = store.state.configs?.tpCheckBalance;
    logger.v('tpCheckBalance: $org');
    String res = org ?? "0";
    // if (res == "1") { // 等于 "1" 的情况下同步socket开局结果与http同步
    //   return true;
    // }
    return true;
  }

  static YBDTPSeatBetConfig seatBetConfig() { // 200,0,0 : 连续点击的时间间隔(毫秒),是否区分座位（1：区分多个不同位置，0：不区分）,路由跳转（1：使用重复判断，0：忽略判断）
    final store = YBDCommonUtil.storeFromContext(context: Get.context)!;
    String org = store.state.configs?.tpConfig ?? "200,0,0"; // 默认值设置
    logger.v('tpConfig: $org');
    var configs = org.split(",");
    var interval = 200; // 默认值设置
    var multiple = false; // 默认值设置
    var repeatRoute = false; // 默认值设置
    if (configs.length > 0) interval = int.tryParse(configs[0]) ?? interval;
    if (configs.length > 1) multiple = configs[1] == "1" ? true : false;
    if (configs.length > 2) repeatRoute = configs[2] == "1" ? true : false;
    return YBDTPSeatBetConfig(interval, multiple, repeatRoute);
  }
}

class YBDTPSeatBetConfig {
  int interval;// 点击间隔
  bool multiple;// 区分多个不同位置
  bool repeatRoute; // 判断是否是重复路由

  YBDTPSeatBetConfig(this.interval, this.multiple, this.repeatRoute);

}

