import 'dart:async';


import 'package:flutter/cupertino.dart';
import '../entity/teen_ybd_image_util.dart';

import '../entity/teen_ybd_result.dart';
import '../entity/ybd_teen_info.dart';
import '../enum/teen_ybd_state.dart';

final double YBDMoneyBtnWidth = 45;

extension YBDSeatValue on YBDTeenSeatType {
  String getValue() {
    String value = '';
    switch (this) {
      case YBDTeenSeatType.A:
        value = 'a';
        break;
      case YBDTeenSeatType.B:
        value = 'b';
        break;
      case YBDTeenSeatType.C:
        value = 'c';
        break;
    }
    return value;
  }
  void getValue1jwtroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  int getTotalM(YBDResult rs){
    if (rs == null){return 0;}
    return this.index == 0 ? (rs.totalBidValue1 ?? 0) : (this.index == 1 ?(rs.totalBidValue2 ?? 0) : (rs.totalBidValue3 ?? 0));
  }

  int? getPersonBindM(YBDResult rs){
    return this.index == 0 ? rs.bidValue1 : (this.index == 1 ?rs.bidValue2 : rs.bidValue3);
  }
}

extension YBDMoneyIconValue on YBDMoneyType? {
  String value(bool isSelect, bool isT) {
    String value = '';
    switch (this) {
      case YBDMoneyType.ten:
        value = 'fivehundred_' + (YBDTeenInfo.getInstance().money< YBDMoneyType.ten.mValue(isT) ? 'unselected' : (isSelect ? "selected" : 'normal'));
        break;
      case YBDMoneyType.fifty:
        value = 'fifty_' + (YBDTeenInfo.getInstance().money< YBDMoneyType.fifty.mValue(isT) ? 'unselected' : (isSelect ? "selected" : 'normal'));
        break;
      case YBDMoneyType.hundred:
        value = 'onek_'+ (YBDTeenInfo.getInstance().money< YBDMoneyType.hundred.mValue(isT) ? 'unselected' : (isSelect ? "selected" : 'normal'));
        break;
      case YBDMoneyType.thousand:
        value = 'fivek_'+ (YBDTeenInfo.getInstance().money< YBDMoneyType.thousand.mValue(isT) ? 'unselected' : (isSelect ? "selected" : 'normal'));
        break;
    }
    return YBDTeenImageUtil.getBeans(value, isT);
  }
  void valueEdej7oyelive(bool isSelect, bool isT) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  double x(Size size) {
    double x = 0;
    switch (this) {
      case YBDMoneyType.ten:
        x = 0.01;
        break;
      case YBDMoneyType.fifty:
        x = 0.35;
        break;
      case YBDMoneyType.hundred:
        x = 0.68;
        break;
      case YBDMoneyType.thousand:
        x = 1;
        break;
    }
    return x -0.05 ;
  }

  int mValue(bool isT) {
    int x = 0;
    switch (this) {
      case YBDMoneyType.ten:
        x = isT? 500:10;
        break;
      case YBDMoneyType.fifty:
        x = isT? 1000 :100;
        break;
      case YBDMoneyType.hundred:
        x = isT? 10000 :1000;
        break;
      case YBDMoneyType.thousand:
        x = isT? 30000 :10000;
        break;
    }
    return x;
  }
  void mValuefVSKcoyelive(bool isT) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

}
