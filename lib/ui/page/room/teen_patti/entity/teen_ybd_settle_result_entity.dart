import 'dart:async';


import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';


class YBDTeenSettleResultEntity extends YBDMessage with JsonConvert<YBDTeenSettleResultEntity> {
	int? roomId;
	List<dynamic>? costTime;
	String? msgType;
	int? fromUser;
	int? toUser;
	String? mode;
	int? type;
	YBDTeenSettleResultContent? content;
	int? version;
}

class YBDTeenSettleResultContent with JsonConvert<YBDTeenSettleResultContent> {
	int? userId;
	int? balance;
	int? winCoins;
	int? gameId;
}
