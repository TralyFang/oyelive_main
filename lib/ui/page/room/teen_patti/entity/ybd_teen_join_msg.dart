import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_result.dart';

class YBDTeenJoinMsg extends JsonConvert<YBDTeenJoinMsg> {
  int? index;
  int? coins;
  int? gameId;

  int? roomId;
  int? commandId;
  String? code;
  String? desc;
  YBDResult? result;
}

Map<String, dynamic> joinTeenInfoToJson(YBDTeenJoinMsg entity) {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['index'] = entity.index;
  data['coins'] = entity.coins;
  data['gameId'] = entity.gameId;
  return data;
}

joinTeenInfoFromJson(YBDTeenJoinMsg data, Map<String, dynamic> json) {
  if (json != null) {
    data.result = YBDResult.fromJson(json);
  }
  return data;
}
