import 'dart:async';


import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/constant/teen_ybd_patti_const.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/controller/teen_ybd_patti_ctrl.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/enum/teen_ybd_state.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/ext/teen_ybd_ext.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_curtain.dart';

///Royal Pattern 所有的 图片资源 放在这里 可能下次有需要 换皮肤
class YBDTeenImageUtil{
    //vip 左右两个窗帘
    static String TeenCurtainLeft = '${YBDConst.iconPrefix}head_l.png';
    static String TeenCurtainRight = '${YBDConst.iconPrefix}head_r.png';

    //vip 切换按钮
    static String TeenVip = '${YBDConst.iconPrefix}vip.png';
    static String TeenVipNormal = '${YBDConst.iconPrefix}normal.png';

    //head
    static String TeenLogo(bool isT) {return '${YBDConst.iconPrefix}logo${isT ? '_vip' : ''}@2x.png';}
    static String TeenQuestion = '${YBDConst.iconPrefix}q@2x.png';
    static String TeenClose(bool isT) {return'${YBDConst.iconPrefix}close${isT ? '_vip' : ''}@2x.png';}
    static String getHeadBg(bool isT){return '${YBDConst.iconPrefix}head_bg${isT ? '_vip': ''}@2x.png';}
    static String getHeadA(bool isT){return '${YBDConst.iconPrefix}aaa${isT ? '_vip': ''}@2x.png';}

    //背景
    static String getTeenBg(bool isT){return '${YBDConst.iconPrefix}bg${isT ? '_vip': ''}@2x.png';}

    //record and history
    static String getTeenRH(TeenRHItemType type, bool isVip){return '${YBDConst.iconPrefix}${type == TeenRHItemType.TeenVip? "vip${isVip ? '_normal': ""}" : (type == TeenRHItemType.TeenRecords ? 'records': 'history')}@2x.png';}

    //card
    static String getTeenCardFront(bool isT){return '${YBDConst.iconPrefix}card${isT ? '_vip': ''}@2x.png';}

    //seat
    static String getSeat(YBDTeenSeatType type,bool isT){return '${YBDConst.iconPrefix}${type.getValue()}${isT ? '_vip': ''}@2x.png';}
    //you and pot
    static String getYPContainer(bool isPot,bool isT){
        if (isT) return '${YBDConst.iconPrefix}pot_vip@2x.png';
        return '${YBDConst.iconPrefix}${isPot ? "pot" : "you"}@2x.png';
    }

    //topUp
    static String getTeenTopUp(bool isT) {
        if (( YBDTeenInfo.getInstance().isNew ?? '0') == '1')return '${YBDConst.iconPrefix}topUp_free@2x.png';
        return '${YBDConst.iconPrefix}topUp${isT ? '_vip': ''}@2x.png';
    }

    //beans
    static String getBeans(String value, bool isT){
        return '${YBDConst.iconPrefix}$value${isT?"_vip":''}.png';
    }

    static String TeenTimer = '${YBDConst.iconPrefix}timer.png';


    static String TeenAlertBg = '${YBDConst.iconPrefix}alert_bg.png';
    static String TeenAlertHead = '${YBDConst.iconPrefix}head_alert@2x.png';

    static String TeenSeatA = '${YBDConst.iconPrefix}a_vip@2x.png';
    static String TeenSeatB = '${YBDConst.iconPrefix}b_vip@2x.png';
    static String TeenSeatC = '${YBDConst.iconPrefix}c_vip@2x.png';

}