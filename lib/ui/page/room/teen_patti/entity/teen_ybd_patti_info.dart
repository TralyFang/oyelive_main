import 'dart:async';


import 'teen_ybd_result.dart';

import '../../../../../common/room_socket/message/base/display_ybd_message.dart';
import '../../../../../common/room_socket/message/base/message.dart';

class YBDTeenPattiInfo implements YBDDisplayMessage {
  int? roomId;
  String? destination;
  List<int>? costTime;
  String? msgType;
  int? fromUser;
  int? toUser;
  String? mode;
  int? sequence;
  int? commandId;
  String? code;
  String? desc;
  YBDResult? result;
  YBDResult? content;

  int? index;
  int? coins;
  int? gameId;

  int get remainSecToNextStatus {
    // print("remainSecToNextStatus : ${result}, ${content} ${result == null ? content.remainSecToNextStatus : result.remainSecToNextStatus}");
    int a = result == null ? (content?.remainSecToNextStatus ?? 0) : (result?.remainSecToNextStatus ?? 0);
    return a;
  }

  int get gameCode {
    return result == null ? (content?.gameId ?? 0) : (result?.gameId ?? 0);
  }

  YBDTeenPattiInfo(
      {this.roomId,
      this.destination,
      this.costTime,
      this.msgType,
      this.fromUser,
      this.toUser,
      this.mode,
      this.sequence,
      this.commandId,
      this.code,
      this.desc,
      this.result,
      this.content});

  YBDTeenPattiInfo.fromJson(Map<String, dynamic> json) {
    roomId = json['roomId'];
    destination = json['destination'];
    costTime = json['costTime'].cast<int>();
    msgType = json['msgType'];
    fromUser = json['fromUser'];
    toUser = json['toUser'];
    mode = json['mode'];
    sequence = json['sequence'];
    commandId = json['commandId'];
    code = json['code'];
    desc = json['desc'];
    result = json['result'] != null ? new YBDResult.fromJson(json['result']) : null;
    content = json['content'] != null ? new YBDResult.fromJson(json['content']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['roomId'] = this.roomId;
    data['destination'] = this.destination;
    data['costTime'] = this.costTime;
    data['msgType'] = this.msgType;
    data['fromUser'] = this.fromUser;
    data['toUser'] = this.toUser;
    data['mode'] = this.mode;
    data['sequence'] = this.sequence;
    data['commandId'] = this.commandId;
    data['code'] = this.code;
    data['desc'] = this.desc;
    if (this.result != null) {
      data['result'] = this.result!.toJson();
    }
    return data;
  }

  @override
  String? receiver;

  @override
  String? receiverImg;

  @override
  int? receiverLevel;

  @override
  int? receiverSex;

  @override
  int? receiverVip;

  @override
  String? sender;

  @override
  String? senderImg;

  @override
  int? senderLevel;

  @override
  int? senderSex;

  @override
  int? senderVip;

  @override
  String? time;

  @override
  YBDMessage fromMap(Map json) {
    // TODO: implement fromMap
    throw UnimplementedError();
  }

  @override
  toJsonContent() {
    // TODO: implement toJsonContent
    // throw UnimplementedError();
  }
}

class YBDHands {
  bool normal = false; // 闲置的状态，可下注
  bool? winner;
  String? pattern;
  List<YBDCards>? cards;

  YBDHands({this.winner, this.pattern, this.cards});

  YBDHands.fromJson(Map<String, dynamic> json) {
    winner = json['winner'];
    pattern = json['pattern'];
    if (json['cards'] != null) {
      cards = <YBDCards>[];
      json['cards'].forEach((v) {
        cards!.add(new YBDCards.fromJson(v));
      });
    }
  }

  fromJson(Map<String, dynamic> json) {
    winner = json['winner'];
    pattern = json['pattern'];
    if (json['cards'] != null) {
      cards = <YBDCards>[];
      json['cards'].forEach((v) {
        cards!.add(new YBDCards.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['winner'] = this.winner;
    data['pattern'] = this.pattern;
    if (this.cards != null) {
      data['cards'] = this.cards!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class YBDCards {
  int? number;
  int? type;
  String? name;

  YBDCards({this.number, this.type, this.name});

  YBDCards.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    type = json['type'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number'] = this.number;
    data['type'] = this.type;
    data['name'] = this.name;
    return data;
  }
}
