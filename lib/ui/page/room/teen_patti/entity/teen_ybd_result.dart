import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_info.dart';

class YBDResult with JsonConvert<YBDResult> {
  int? gameId;
  int? status;

  ///1下注 2end 3不知道
  int? remainSecToNextStatus;
  int? startTime;
  int? endTime;
  int? lastStatusTime;
  int? bidLimit;
  double? prizeTimes;
  int? bidValue1;
  int? bidValue2;
  int? bidValue3;
  int? balance;
  int? totalBidValue1;
  int? totalBidValue2;
  int? totalBidValue3;
  List<YBDHands>? hands;

  ///获取这局赢了多少
  double getWinM(int index) {
    double win = 0;
    switch (index) {
      case 0:
        win = (totalBidValue1 ?? 0) * prizeTimes!;
        break;
      case 1:
        win = (totalBidValue2 ?? 0) * prizeTimes!;
        break;
      case 2:
        win = (totalBidValue3 ?? 0) * prizeTimes!;
        break;
    }
    return win;
  }

  YBDResult(
      {this.gameId,
      this.status,
      this.remainSecToNextStatus,
      this.startTime,
      this.endTime,
      this.lastStatusTime,
      this.bidLimit,
      this.prizeTimes,
      this.bidValue1,
      this.bidValue2,
      this.bidValue3,
      this.balance,
      this.totalBidValue1,
      this.totalBidValue2,
      this.totalBidValue3,
      this.hands});

  YBDResult.fromJson(Map<String, dynamic>? json) {
    gameId = json!['gameId'];
    status = json['status'];
    remainSecToNextStatus = json['remainSecToNextStatus'];
    startTime = json['startTime'];
    endTime = json['endTime'];
    lastStatusTime = json['lastStatusTime'];
    bidLimit = json['bidLimit'];
    prizeTimes = json['prizeTimes'];
    bidValue1 = json['bidValue1'];
    bidValue2 = json['bidValue2'];
    bidValue3 = json['bidValue3'];
    balance = json['balance'];
    totalBidValue1 = json['totalBidValue1'] ?? 0;
    totalBidValue2 = json['totalBidValue2'] ?? 0;
    totalBidValue3 = json['totalBidValue3'] ?? 0;
    if (json['hands'] != null) {
      hands = <YBDHands>[];
      json['hands'].forEach((v) {
        hands!.add(new YBDHands.fromJson(v));
      });
    } else {
      hands = [];
    }
  }

  YBDResult fromJson(Map<String, dynamic> json) {
    gameId = json['gameId'];
    status = json['status'];
    remainSecToNextStatus = json['remainSecToNextStatus'];
    startTime = json['startTime'];
    endTime = json['endTime'];
    lastStatusTime = json['lastStatusTime'];
    bidLimit = json['bidLimit'];
    prizeTimes = json['prizeTimes'];
    bidValue1 = json['bidValue1'];
    bidValue2 = json['bidValue2'];
    bidValue3 = json['bidValue3'];
    balance = json['balance'];
    totalBidValue1 = json['totalBidValue1'] ?? 0;
    totalBidValue2 = json['totalBidValue2'] ?? 0;
    totalBidValue3 = json['totalBidValue3'] ?? 0;
    if (json['hands'] != null) {
      hands = <YBDHands>[];
      json['hands'].forEach((v) {
        hands!.add(new YBDHands.fromJson(v));
      });
    } else {
      hands = [];
    }
    return this;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['gameId'] = this.gameId;
    data['status'] = this.status;
    data['remainSecToNextStatus'] = this.remainSecToNextStatus;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    data['lastStatusTime'] = this.lastStatusTime;
    data['bidLimit'] = this.bidLimit;
    data['prizeTimes'] = this.prizeTimes;
    data['bidValue1'] = this.bidValue1;
    data['bidValue2'] = this.bidValue2;
    data['bidValue3'] = this.bidValue3;
    data['balance'] = this.balance;
    data['totalBidValue1'] = this.totalBidValue1;
    data['totalBidValue2'] = this.totalBidValue2;
    data['totalBidValue3'] = this.totalBidValue3;
    if (this.hands != null) {
      data['hands'] = this.hands!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
