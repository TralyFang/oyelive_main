import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

class YBDTeenPattiHistoryEntity with JsonConvert<YBDTeenPattiHistoryEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDTeenPattiHistoryRecordEntity?>? record;
  YBDTeenPattiHistoryEntity();

  YBDTeenPattiHistoryEntity.fromJson(Map<String, dynamic> json) {
    returnCode = json['returnCode'];
    returnMsg = json['returnMsg'];
    if (json['record'] != null) {
      record = [];
      json['record'].forEach((v) {
        record!.add(new YBDTeenPattiHistoryRecordEntity.fromJson(v));
      });
    } else {
      record = [];
    }
  }
}

class YBDTeenPattiHistoryRecordEntity with JsonConvert<YBDTeenPattiHistoryRecordEntity> {
  int? gameId;
  int? date;
  int? winIndex;
  String? status;

  YBDTeenPattiHistoryRecordEntity();
  YBDTeenPattiHistoryRecordEntity.fromJson(Map<String, dynamic> json) {
    gameId = json['gameId'];
    date = json['date'];
    winIndex = json['winIndex'];
    status = json['status'];
  }
}

class YBDTeenPattiREntity with JsonConvert<YBDTeenPattiREntity> {
  String? returnCode;
  String? returnMsg;
  YBDTeenPattiRecordEntity? record;
  YBDTeenPattiREntity();
  YBDTeenPattiREntity.fromJson(Map<String, dynamic> json) {
    returnCode = json['returnCode'];
    returnMsg = json['returnMsg'];
    record = YBDTeenPattiRecordEntity.fromJson(json['record']);
  }
}

class YBDTeenPattiRecordEntity with JsonConvert<YBDTeenPattiRecordEntity> {
  String? name;
  int? updateTime;
  List<YBDTeenPattiRankEntity?>? rank;
  YBDTeenPattiRecordEntity();
  YBDTeenPattiRecordEntity.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    updateTime = json['updateTime'];
    if (json['rank'] != null) {
      rank = [];
      json['rank'].forEach((v) {
        rank!.add(new YBDTeenPattiRankEntity.fromJson(v));
      });
    } else {
      rank = [];
    }
  }
}

class YBDTeenPattiRankEntity with JsonConvert<YBDTeenPattiRankEntity> {
  int? count;
  YBDUserInfo? user;
  YBDTeenPattiRankEntity();

  YBDTeenPattiRankEntity.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    user = YBDUserInfo().fromJson(json['user']);
  }
}
