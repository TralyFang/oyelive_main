import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDTpVipRecordEntity with JsonConvert<YBDTpVipRecordEntity> {
	String? returnCode;
	String? returnMsg;
	YBDTpVipRecordEntityRecord? record;
}

class YBDTpVipRecordEntityRecord with JsonConvert<YBDTpVipRecordEntityRecord> {
	int? targetBeans;
	int? beans;
	String? context;
	int? userId;
	String? status;
}
