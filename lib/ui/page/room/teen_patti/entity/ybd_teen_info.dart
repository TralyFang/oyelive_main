import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_live_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/api.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_result.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/tp_ybd_vip_record_entity.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/enum/teen_ybd_state.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/ext/int_ybd_ext.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';

class YBDTeenInfo {
  static YBDTeenInfo? _instance;

  int? roomId;
  int? gameId;

  //记录一下 选中的货币
  YBDMoneyType? type;
  GlobalKey? beanKey; // 下注的bean数值
  int balance = 0; // 下注赢钱的报文余额

  //预加载
  YBDTeenPattiInfo? pattiInfo;

  //上局有下注 中途关闭了teenpatti 重新进来，上局未结束的场景
  int betValue = 0;

  YBDTeenInfo._();

  int money = 0;

  int youA = 0;
  int potA = 0;
  int youB = 0;
  int potB = 0;
  int youC = 0;
  int potC = 0;

  //Timer cureent time
  int? time = 0;

  bool open = false;

  //是否是100ms内点击
  bool mutClickA = false;
  bool mutClickB = false;
  bool mutClickC = false;

  ///是否开牌中
  bool hasResult = false;

  String? isNew;

  Future _init() async {
    _instance = await YBDTeenInfo.getInstance();
  }
  void _initJLOf9oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  static YBDTeenInfo getInstance() {
    if (_instance == null) {
      var instance = YBDTeenInfo._();
      // await instance._init();
      _instance = instance;
    }
    return _instance!;
  }

  //入口参数
  TPEnterType? enterType;
  YBDTpVipRecordEntityRecord? recordResult = null;

  queryTeenPattiRecord(BuildContext context) async {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    Map<String, dynamic> params = {"userId": store.state.bean!.id};
    YBDTpVipRecordEntity? result = await YBDHttpUtil.getInstance().doGet<YBDTpVipRecordEntity>(
      context,
      YBDApi.QUERY_TEENPATTI_VIP,
      params: params,
    );
    recordResult = result?.record;
  }

  getNew() {
    YBDSPUtil.get(Const.TP_NEW_USER).then((value) {
      isNew = value;
    });
  }

  setMoney(YBDTeenSeatType type, int money) {
    logger.v('tp pot or you change begin: ${this.potB} ${this.youB}, ${money}');
    switch (type) {
      case YBDTeenSeatType.A:
        potA += (money - youA);
        youA = money;
        break;
      case YBDTeenSeatType.B:
        potB += (money - youB);
        youB = money;
        break;
      case YBDTeenSeatType.C:
        potC += (money - youC);
        youC = money;
        break;
      case YBDTeenSeatType.D:
        break;
    }
    logger.v('tp pot or you change end: ${this.potB} ${this.youB}, ${money}');
  }

  setYou(YBDTeenSeatType type, dynamic result) {
    switch (type) {
      case YBDTeenSeatType.A:
        youA = result["bidValue1"] ?? 0;
        potA = result["totalBidValue1"] ?? 0;
        break;
      case YBDTeenSeatType.B:
        youB = result["bidValue2"] ?? 0;
        ;
        potB = result["totalBidValue2"] ?? 0;
        ;
        break;
      case YBDTeenSeatType.C:
        youC = result["bidValue3"] ?? 0;
        ;
        potC = result["totalBidValue3"] ?? 0;
        ;
        break;
      case YBDTeenSeatType.D:
        break;
    }
  }

  setMoneyWithResult(YBDResult result) {
    youA = result.bidValue1 ?? 0;
    potA = result.totalBidValue1 ?? 0;
    youB = result.bidValue2 ?? 0;
    potB = result.totalBidValue2 ?? 0;
    youC = result.bidValue3 ?? 0;
    potC = result.totalBidValue3 ?? 0;

    ///重新进入的时候 当前局有下注 当第二局开始的时候设置false
    betValue = youA + youB + youC;
  }

  getMoney(YBDTeenSeatType type, YBDTeenMType mType) {
    int a = 0;
    switch (type) {
      case YBDTeenSeatType.A:
        a = (mType == YBDTeenMType.you ? youA : potA);
        break;
      case YBDTeenSeatType.B:
        a = (mType == YBDTeenMType.you ? youB : potB);
        break;
      case YBDTeenSeatType.C:
        a = (mType == YBDTeenMType.you ? youC : potC);
        break;
      case YBDTeenSeatType.D:
        break;
    }
    logger.v('tp get money: $a $type');
    return a;
  }

  setMulClick(YBDTeenSeatType type, bool mul) {
    if (!TeenConfigInfo.seatBetConfig().multiple) {
      // 不区分座位，则共用一个下注时间间隔
      mutClickA = mutClickB = mutClickC = mul;
      return;
    }

    switch (type) {
      case YBDTeenSeatType.A:
        mutClickA = mul;
        break;
      case YBDTeenSeatType.B:
        mutClickB = mul;
        break;
      case YBDTeenSeatType.C:
        mutClickC = mul;
        break;
      case YBDTeenSeatType.D:
        break;
    }
  }

  getMulClick(YBDTeenSeatType type) {
    print("bool : ${mutClickA}, $mutClickB, $mutClickC");
    return type == YBDTeenSeatType.A ? mutClickA : (type == YBDTeenSeatType.B ? mutClickB : mutClickC);
  }

  clean() {
    logger.v("Royal Pattern close");
    if (YBDLiveService.instance.data.gameRecord != null)
      YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_game_close, record: YBDLiveService.instance.data.gameRecord);
    YBDTeenInfo.getInstance().gameId = null;
    YBDTeenInfo.getInstance().roomId = null;
    YBDTeenInfo.getInstance().type = null;
    YBDTeenInfo.getInstance().pattiInfo = null;
    YBDTeenInfo.getInstance().time = 0;
    YBDTeenInfo.getInstance().beanKey = null;
    YBDTeenInfo.getInstance().betValue = 0;
    YBDTeenInfo.getInstance().hasResult = false;
    YBDTeenInfo.getInstance().open = false;
    cleanMoney();
  }

  cleanMoney() {
    logger.v("Royal Pattern clean bet info");
    YBDTeenInfo.getInstance().youA = 0;
    YBDTeenInfo.getInstance().potA = 0;
    YBDTeenInfo.getInstance().youB = 0;
    YBDTeenInfo.getInstance().potB = 0;
    YBDTeenInfo.getInstance().youC = 0;
    YBDTeenInfo.getInstance().potC = 0;
  }
}
