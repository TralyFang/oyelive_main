import 'dart:async';



import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_curtain.dart';

class YBDConst{
  ///teen_patti icon前缀
  static String iconPrefix = "assets/images/teen_patti/teen_patti_";
  /*****************header相关常量*********************/
  ///头部总高度
  static double headerH =  ScreenUtil().setHeight(238); //238
  ///aaa宽高
  static double aaaW = ScreenUtil().setWidth(234);
  static double aaaH = ScreenUtil().setHeight(238);
  ///头部小图标宽度
  static double headerSmallIconW =  ScreenUtil().setWidth(62);
  ///头部logo高度
  static double headerLogo =  ScreenUtil().setHeight(18.8);//38.8

  /*****************card相关常量*********************/
  ///扑克牌反面宽高
  static double frontCardW = ScreenUtil().setWidth(149);
  static double frontCardH = ScreenUtil().setHeight(130);

  static double cardW = ScreenUtil().setWidth(78);
  static double cardH = ScreenUtil().setHeight(80);
  static double cardPosW = ScreenUtil().setWidth(20);
  static double cardPosLeft = ScreenUtil().setWidth(15.5);

  /*****************左耳两个图标相关常量*********************/

  static double leftW = ScreenUtil().setWidth(54);
  static double leftTop = ScreenUtil().setHeight(135);
  static double leftTopW = ScreenUtil().setHeight(100);
  static double rightTopW = ScreenUtil().setHeight(82);
  static double leftTopItem(TeenRHItemType type){
    return type == TeenRHItemType.TeenVip ? leftFristTop : (type == TeenRHItemType.TeenRecords ? leftSecondTop : (leftSecondTop + leftPadding) );
  }
  static double leftPadding = leftW * 70/ 27 - 10;
  static double leftFristTop = ScreenUtil().setHeight(137 + 112);
  static double leftSecondTop = leftFristTop + leftPadding;

  static double vipTop = ScreenUtil().setHeight(145 + 40);
  static double vipLeft = ScreenUtil().setHeight(30);
  static double vipW = ScreenUtil().setHeight(40);


  ///椅子高度度 icon: 154*200
  ///椅子容器宽度 目的和字牌居中对齐
  static double seatC = ScreenUtil().setWidth(165);
  static double seatW = ScreenUtil().setWidth(134.4);
  static double seatH = ScreenUtil().setHeight(150);

/*
* Pot 高度
* 148 * 37
*/
  static double potH = ScreenUtil().setHeight(47);
  ///重叠多余的高度
  static double potCH = 5;
  static double youH = ScreenUtil().setHeight(47);
  ///abc 小图标宽度
  // static double seat_ABCW = 25;
  static double seat_H = YBDConst.seatH + YBDConst.youH + YBDConst.potCH;

  ///底部货币容器高度
  static double bottom_top = ScreenUtil().setHeight(25);
  static double bottom_Money_H = ScreenUtil().setHeight(45);
  static double bottom_TopUP_W = ScreenUtil().setWidth(149);
  static double bottom_TopUP_H = ScreenUtil().setHeight(62);
  static double bottom_Coin_W = 50;
  static double bottom_vH = bottom_top + bottom_Money_H + bottom_TopUP_H;

  ///总高度
  static double bodyH = bottom_vH + seat_H + frontCardH + headerH/2.0 + YBDConst.potCH;


  ///浮动的tip高度
  static double tipH = ScreenUtil().setWidth(81);

}

