import 'dart:async';


import 'package:flutter_bloc/flutter_bloc.dart';
import '../entity/teen_ybd_patti_info.dart';
import '../enum/teen_ybd_state.dart';

abstract class YBDTeenPattiEvent{
  const YBDTeenPattiEvent();
}

class YBDTeenPattiEventObject extends YBDTeenPattiEvent{
  ///当前选中的货币面值 用在点击事件
  YBDMoneyType? currentClickType;
  ///当前选中的座位 用在缩放动画上
  YBDTeenSeatType currentSelectSeatType;

  ///下注选中的座位
  YBDTeenSeatType? seatType;

  YBDTeenPattiEventObject({this.currentSelectSeatType = YBDTeenSeatType.D,this.currentClickType,this.seatType = null});
}

class YBDTeenPattiEventCover extends YBDTeenPattiEvent{
  ///当前遮罩是否需要显示出来 true 不展示
  bool? needstage;
  ///翻牌
  bool? transforCard;

  List<YBDHands>? hands;

  int? time;

  bool? cleanSeat;
}

class YBDTeenPattiEventMoney extends YBDTeenPattiEvent{
  int? pot;
  int? you;
}

class YBDTeenPattiBloc extends Bloc<YBDTeenPattiEvent, YBDTeenPattiBlocState>{
  YBDTeenPattiBloc(YBDTeenPattiBlocState initialState) : super(initialState);

  @override
  Stream<YBDTeenPattiBlocState> mapEventToState(YBDTeenPattiEvent event) async *{
    // TODO: implement mapEventToState
    if (event is YBDTeenPattiEventObject){
      // print('aaaa: ${state.currentSelectSeatType}, ${state.currentClickType}');
      yield YBDTeenPattiBlocState()..currentClickType = event.currentClickType
        ..currentSelectSeatType = event.currentSelectSeatType
        ..seatType = event.seatType;
    }else if(event is YBDTeenPattiEventCover){
      // print("event.hands: ${event.hands} ,${event.time}");
      yield YBDTeenPattiBlocState()
        ..needstage = (event.needstage ?? false)
        ..transforCard = (event.transforCard ?? true)
        ..hands = (event.hands ?? [])
        ..cleanSeat = (event.cleanSeat ?? false)
        ..time = event.time ?? 0;
    }else if (event is YBDTeenPattiEventMoney){
      yield YBDTeenPattiBlocState()..you = event.you
          ..pot = event.pot;
    }
  }
}


class YBDTeenPattiBlocState{
  ///当前选中的货币面值
  YBDMoneyType? currentClickType;

  ///当前选中的座位
  YBDTeenSeatType? currentSelectSeatType;

  YBDTeenSeatType? seatType;

  ///当前遮罩是否需要显示出来 true 不展示
  bool? needstage;
  ///翻牌动作
  bool? transforCard;

  List<YBDHands>? hands;

  int? time;

  bool? cleanSeat;

  int? pot;
  int? you;
}
