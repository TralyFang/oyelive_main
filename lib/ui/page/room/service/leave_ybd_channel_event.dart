import 'dart:async';


import 'package:agora_rtc_engine/rtc_engine.dart';

import 'live_ybd_event.dart';

class YBDLeaveChannelEvent extends YBDLiveEvent {
  RtcStats? stats;

  YBDLeaveChannelEvent({this.stats});
}
