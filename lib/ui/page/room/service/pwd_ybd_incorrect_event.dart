import 'dart:async';


import 'package:flutter/cupertino.dart';

import 'live_ybd_event.dart';

class YBDPwdIncorrectEvent extends YBDLiveEvent {
  int roomId;
  String? pwd;
  YBDPwdIncorrectEvent({required this.roomId, this.pwd});
}
