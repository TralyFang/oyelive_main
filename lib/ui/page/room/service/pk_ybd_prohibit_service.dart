import 'dart:async';



import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart' as user;
import 'package:oyelive_main/ui/page/room/entity/agora_ybd_rtm_info.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_agora_rtm_service.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';

class YBDPkProhibitService{
  /*
  *  跨频道转发流
  *  note:
  *   1. 不支持String 类型的UID
  *   2. 方法调用必须在joinchannel之后
  *   3. 该方法仅适用直播场景下的主播角色
  *   4. 调用成功后，再次调用 需要先stopChannelMediaRelay
  *
  *   ChanelMediaInfo :
  *     channelName :The name of the source channel. The default value is NULL, which means the SDK applies the name of the current channel
  *     uId: ID of the broadcaster whose media stream you want to relay. The default value is 0, which means the SDK generates a random UID. You must set it as 0
  *     token: The token for joining the source channel. It is generated with the `channelName` and `uid` you set in `srcInfo`
  *
  *   sourceChannelName : 主动转发流 的主播 roomid
  * */
  
  static void startChannelMediaRelay() async{
    logger.v(' start Channel media relay : begin ---');
    if (YBDLiveService.instance.getEngine() == null){
      logger.v('please join channel first');
      return;
    }

    user.YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
    //观众进来 不需要转发流
    if (userInfo?.id != YBDLiveService.instance.data.pkInfo?.sourceRoom && userInfo?.id != YBDLiveService.instance.data.pkInfo?.targetRoom){
      logger.v(' start Channel media userInfoId: ${userInfo?.id}, ${YBDLiveService.instance.data.pkInfo?.sourceRoom} ${YBDLiveService.instance.data.pkInfo?.targetRoom}');
      return;
    }

    //如果pk 已经结束
    if (YBDLiveService.instance.data.pkInfo == null)return;

    //开始连接
    YBDLiveAgoraRtmService.instance.sink.add(YBDPkMediaMute(ChannelMediaRelayState.Connecting));
    /*
    //sourceInfo 配置源频道信息
    ChannelMediaInfo sourceInfo = ChannelMediaInfo(0, channelName: YBDLiveService.instance.data.pkInfo?.sourceUser.toString(), token: YBDLiveService.instance.data.pkInfo?.sourceToken);
    //targetInfo 转发到哪里
    ChannelMediaInfo targetInfo = ChannelMediaInfo(0,channelName: YBDLiveService.instance.data.pkInfo?.targetUser.toString(), token:YBDLiveService.instance.data.pkInfo?.targetToken);


     */
    //sourceInfo 配置源频道信息
    ChannelMediaInfo sourceInfo = ChannelMediaInfo(YBDLiveService.instance.data.pkInfo?.sourceUser.toString() ?? '', 0, token: YBDLiveService.instance.data.pkInfo?.sourceToken);
    //targetInfo 转发到哪里
    ChannelMediaInfo targetInfo = ChannelMediaInfo(YBDLiveService.instance.data.pkInfo?.targetUser.toString() ?? '', 0, token:YBDLiveService.instance.data.pkInfo?.targetToken);

    ChannelMediaRelayConfiguration config = ChannelMediaRelayConfiguration(sourceInfo, [targetInfo]);
    YBDLiveService.instance.getEngine()!.startChannelMediaRelay(config);
    logger.v('channel media info has start relay : ${YBDLiveService.instance.data.pkInfo?.sourceUser}, ${YBDLiveService.instance.data.pkInfo?.sourceToken}');
  }

  ///暂停发流 不会退出通道
  static void pauseAllChannelMediaRelay(){
    YBDLiveService.instance.getEngine()!.pauseAllChannelMediaRelay();
    logger.v('channel media info has relay');
  }

  ///恢复发流
  static void resumeAllChannelMediaRelay(){
     YBDLiveService.instance.getEngine()!.resumeAllChannelMediaRelay();
     logger.v('channel media info has resume');
  }

  ///pk结束的时候 停止流转发  停止后 会退出所有的目标通道
  static void stopChannelMediaRelay(){
    logger.v('pk is end, stop channel media');
    YBDLiveService.instance.getEngine()!.stopChannelMediaRelay();
  }

}