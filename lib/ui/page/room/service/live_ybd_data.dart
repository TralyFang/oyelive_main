import 'dart:async';


import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/ui/page/room/play_center/entity/game_ybd_entity.dart';

import '../../../../common/room_socket/message/base/message.dart';
import '../../../../common/room_socket/message/common/PkPublish.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import 'live_ybd_define.dart';

/// 直播数据
class YBDLiveData {
  var _pageData = _YBDPageData();
  var _socketData = _YBDSocketData();
  var _engineData = _YBDEngineData();

  YBDLiveData();

  /// 加入房间的历史记录
  List<YBDEnterHistory> _history = [];

  List<YBDEnterHistory> get history {
    if (_history.length > 10) {
      _history.removeAt(0);
    }

    return _history;
  }

  /// 频道名称
  String? get channelName => _engineData.channelName;

  set channelName(String? value) {
    _engineData.channelName = value;
  }

  /// 用户角色
  ClientRole? get role => _engineData.role;

  set role(ClientRole? value) {
    _engineData.role = value;
  }

  /// 频道用户
  List<int> get users => _engineData.users;

  /// 频道speakers的uid
  List<int> speakers = <int>[];

  set users(List<int> value) {
    _engineData.users = [];
    _engineData.users.addAll(value);
  }

  /// 是否静麦
  bool get muted => _engineData.muted;

  set muted(bool value) {
    _engineData.muted = value;
  }

  /// 是否加入频道
  var isJoined = false;
  // bool get isJoined => _engineData.isJoined;
  //
  // set isJoined(bool value) {
  //   _engineData.isJoined = value;
  // }

  /// 记录从哪个页面进入房间的
  String? get prePage => _pageData.preRouteName;

  set prePage(String? value) {
    _pageData.preRouteName = value;
  }

  /// 房间 context
  /// 在 [YBDRoomBloc.setRoomPageContext]中赋值，在[cleanData]中置空
  /// 使用场景举例：
  /// 从miniProfile弹出礼物弹框时会将miniProfile的context传给礼物弹框
  /// 当miniProfile被pop时礼物弹框用到的context为不稳定状态导致报错
  /// 这里直接用[roomContext]代替miniProfile的context来查询widget树中的bloc
  BuildContext? roomContext;
  // 用[YBDLiveService.instance.mainContext]
  // 要找roomBloc可以从widget树中遍历
  // BuildContext get context => _pageData.roomPageContext;
  // set context(BuildContext value) {
  //   _pageData.roomPageContext = value;
  // }

  /// 房间页面类型，主播房间或观众房间
  // RoomPageType get pageType => _pageData.roomPageType;

  // set pageType(RoomPageType value) {
  //   _pageData.roomPageType = value;
  // }

  /// 是否在房间
  bool get isInRoom => _pageData.isInRoom;

  set isInRoom(bool value) {
    _pageData.isInRoom = value;
  }

  /// 房间 id
  /// 使用场景
  /// 1. 保存加入房间的历史记录
  /// 2. 加入频道前判断要加入的房间id与该[roomId]相等才允许加入
  /// 3. [YBDRoomSocketApi.getInstance().auth]socket鉴权后重新开播
  /// 赋值
  /// 1. 在开播页面订阅房间时赋值
  /// 2. 导航到[YBDRoomPage]前赋值
  /// 3. 加入声网频道成功后赋值
  /// 4. 切换到新房间前赋值
  int? roomId;

  /// 进入房间时的在线房间列表
  /// 进入房间列表页面前赋值，作为房间列表页面的参数
  /// 退出房间列表时清空[roomList]
  List<YBDRoomInfo?>? roomList;

  YBDPkInfoContent? get pkInfo => _pageData.pkInfo;

  set pkInfo(YBDPkInfoContent? info) {
    _pageData.pkInfo = info;
  }

  /// 记录房间结束的PkId
  int? get pkIdEnded => _pageData.pkIdEnded;

  set pkIdEnded(int? value) {
    _pageData.pkIdEnded = value;
  }

  /// 匹配超时时间
  int? get matchEndTime => _pageData.matchEndTime;

  set matchEndTime(int? value) {
    _pageData.matchEndTime = value;
  }

  /// 房间名称
  String? get roomTitle => _socketData.roomTitle;

  set roomTitle(String? value) {
    _socketData.roomTitle = value;
  }

  /// 房间分类
  String? get roomCategory => _socketData.roomCategory;

  set roomCategory(String? value) {
    _socketData.roomCategory = value;
  }

  /// 0 为公开房间，1 为私密房间
  int? get protectMode => _socketData.protectMode;

  set protectMode(int? value) {
    _socketData.protectMode = value;
  }

  /// 房间密码
  String? get protectData => _socketData.protectData;

  set protectData(String? value) {
    _socketData.protectData = value;
  }

  /// 重试输入密码次数
  int get retryPwd => _socketData.retryPwd;

  set retryPwd(int value) {
    _socketData.retryPwd = value;
  }

  /// 开播页面的消息
  List<YBDMessage> get startPageMsgList => _socketData.startPageMsgList;

  /// 游戏信息
  YBDGameRecord? get gameRecord => _pageData.gameRecord;

  set gameRecord(YBDGameRecord? v) {
    _pageData.gameRecord = v;
  }

  /// 在线用户消息
  // YBDUserPublish _viewerMsg;

  // set viewerMsg(YBDUserPublish value) {
  //   _viewerMsg = value;
  // }

  // YBDUserPublish get viewerMsg {
  //   if (null != _viewerMsg && roomId == _viewerMsg?.roomId) {
  //     return _viewerMsg;
  //   } else {
  //     return null;
  //   }
  // }

  /// 保存进入房间的历史记录
  keepHistory() {
    assert(null != _socketData, '_socketData is null');
    if (null == roomId) {
      logger.w('room id is null');
      return;
    }

    final room = YBDEnterHistory(
      roomId: roomId,
      isCurrent: false,
      subscribed: true,
    );

    _history.add(room);
  }

  /// 清除数据
  void cleanData() {
    // comboBloc.close();
    // comboBloc = null;
    roomContext = null;
    roomId = 0;
    roomList?.clear();
    speakers.clear();
    startPageMsgList.clear();
    isJoined = false;

    _socketData = _YBDSocketData();
    _engineData = _YBDEngineData();
    _pageData = _YBDPageData();
  }
  void cleanDatacpijhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

@Deprecated(
  '直接在YBDLiveData里添加字段'
  'This method was deprecated',
)
class _YBDPageData {
  /// 记录从哪个页面进入房间的
  String? preRouteName;

  /// 房间页面类型
  // RoomPageType roomPageType = RoomPageType.User;

  /// 房间 context
  // BuildContext roomPageContext;

  /// 是否在房间
  bool isInRoom = false;

  /// 房间 bloc
  // YBDRoomComboBloc comboBloc;

  // YBDRoomComboBloc comboBloc;

  /// pk礼物相关
  // int pkId;
  YBDPkInfoContent? pkInfo;

  /// 已结束pkId
  int? pkIdEnded;

  /// 匹配超时时间,默认10分钟，600s
  int? matchEndTime = 600;

  YBDGameRecord? gameRecord;
}

@Deprecated(
  '直接在YBDLiveData里添加字段'
  'This method was deprecated',
)
class _YBDEngineData {
  /// 频道名称
  String? channelName;

  /// 用户角色
  ClientRole? role;

  /// 频道用户
  var users = <int>[];

  /// 是否静麦
  var muted = false;

  /// 是否加入频道
  // var isJoined = false;
}

@Deprecated(
  '直接在YBDLiveData里添加字段'
  'This method was deprecated',
)
class _YBDSocketData {
  /// 房间 id
  // TODO: 测试代码 删除
  // int roomId;

  /// 房间名称
  String? roomTitle;

  /// 房间分类
  String? roomCategory;

  /// 0 为公开房间，1 为私密房间
  int? protectMode;

  /// 房间密码
  String? protectData;

  /// 重试输入密码次数
  int retryPwd = 0;

  /// 开播页面的消息
  List<YBDMessage> startPageMsgList = [];

  /// 粉丝榜消息
  // YBDSuperFansMessage _superFansMsg;

  // set superFansMsg(YBDSuperFansMessage value) {
  //   _superFansMsg = value;
  // }

  // YBDSuperFansMessage get superFansMsg {
  //   if (null != _superFansMsg && roomId == _superFansMsg?.roomId) {
  //     return _superFansMsg;
  //   } else {
  //     return null;
  //   }
  // }

  /// 在线用户消息
  // YBDUserPublish _viewerMsg;

  // set viewerMsg(YBDUserPublish value) {
  //   _viewerMsg = value;
  // }

  // YBDUserPublish get viewerMsg {
  //   if (null != _viewerMsg && roomId == _viewerMsg?.roomId) {
  //     return _viewerMsg;
  //   } else {
  //     return null;
  //   }
  // }
}
