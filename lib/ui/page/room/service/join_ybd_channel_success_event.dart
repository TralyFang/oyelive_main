import 'dart:async';


import 'live_ybd_event.dart';

class YBDJoinChannelSuccessEvent extends YBDLiveEvent {
  String? channel;
  int? uid;
  int? elapsed;

  YBDJoinChannelSuccessEvent({this.channel, this.uid, this.elapsed});
}
