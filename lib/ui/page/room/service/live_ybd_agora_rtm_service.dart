import 'dart:async';


import 'dart:async';
import 'dart:convert';

import 'package:agora_rtm/agora_rtm.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/entity/agora_ybd_rtm_info.dart';
import 'package:oyelive_main/ui/page/room/ext/rtm_ybd_type_ext.dart';
import 'package:oyelive_main/ui/page/room/pk/pk_ybd_rtm_helper.dart';
import 'package:get/get.dart';
import 'live_ybd_event.dart';

/// 2.3 需求 声网 RTM 信令封装
class YBDLiveAgoraRtmService {
  static YBDLiveAgoraRtmService? _instance;

  static YBDLiveAgoraRtmService get instance => _getInstance();

  factory YBDLiveAgoraRtmService() => _getInstance();

  static YBDLiveAgoraRtmService _getInstance() {
    if (_instance == null) {
      _instance = YBDLiveAgoraRtmService._();
    }
    return _instance!;
  }

  /// 私有构造函数
  YBDLiveAgoraRtmService._() {
    // initConfig();
  }

  /// 事件广播
  /// 调用 onEvent.listen() 接收广播
  // ignore: close_sinks
  StreamController<YBDLiveEvent> _controller = StreamController<YBDLiveEvent>.broadcast();

  Stream<YBDLiveEvent> get onEvent => _controller.stream;

  StreamSink<YBDLiveEvent> get sink => _controller.sink;

  ///信令对象
  AgoraRtmClient? _client;

  ///频道对象
  AgoraRtmChannel? _channel;

  ///登录状态
  bool _isLogin = false;

  ///是否在频道
  bool _isInChannel = false;

  ///pk创建时的状态
  YBDAgoraRtmInfo? agoraRtmInfoStart;

  ///创建信令对象
  Future<void> createClient() async {
    _client = await AgoraRtmClient.createInstance(Const.AGORA_APP_ID);
    //点对点消息
    _client!.onMessageReceived = (AgoraRtmMessage message, String peerId) {
      logger.v("Peer msg: " + peerId + ", msg: " + (message.text));
      Map<String, dynamic> map = jsonDecode(message.text);
      YBDAgoraRtmInfo agoraRtmInfo = YBDAgoraRtmInfo.fromMap(map);
      YBDPKRTMHelper.reciveRTMMsg(agoraRtmInfo);
    };
    _client!.onConnectionStateChanged = (int state, int reason) {
      logger.v('Connection state changed: ' + state.toString() + ', reason: ' + reason.toString());
      if (state == 5) {
        _client!.logout();
        logger.v('--------Logout.');
        _isLogin = false;
      }
    };
  }
  void createClientrKsb0oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///销毁rtm服务，释放资源
  Future<void> _destroyRtm() async {
    await _client?.destroy();
    if (_client != null) {
      _client = null;
    }
  }
  void _destroyRtmVoyvEoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///创建频道
  Future<AgoraRtmChannel?> createChannel(String name) async {
    AgoraRtmChannel? channel = await _client!.createChannel(name);
    if (channel != null) {
      channel.onMemberJoined = (AgoraRtmMember member) {
        logger.v("Member joined: " + member.userId + ', channel: ' + member.channelId);
      };
      channel.onMemberLeft = (AgoraRtmMember member) {
        logger.v("Member left: " + member.userId + ', channel: ' + member.channelId);
      };
      //通道内消息
      channel.onMessageReceived = (AgoraRtmMessage message, AgoraRtmMember member) {
        logger.v("Channel msg: " + member.userId + ", msg: " + (message.text));

/*        YBDAgoraRtmInfo agoraRtmInfo = YBDAgoraRtmInfo(RTMType.RTMTypePauseMuteL, pk_id: 1111, pk_state: 2);
        dynamic test = agoraRtmInfo.toJsonContent();
        var jsonString = jsonEncode(test);
        Map<String, dynamic> map = jsonDecode(jsonString);
        YBDAgoraRtmInfo agoraRtmInfo1 = YBDAgoraRtmInfo.fromMap(map);*/

        ///TODO 接收的消息处理
        Map<String, dynamic> map = jsonDecode(message.text);
        YBDAgoraRtmInfo agoraRtmInfo = YBDAgoraRtmInfo.fromMap(map);

        ///TODO 分发
        receivedRtmMessage(agoraRtmInfo);
      };
    }
    return channel;
  }

  ///加入频道
  Future rtmJoinChannel(String channelId) async {
    if (_isInChannel) {
      try {
        await _channel?.leave();
        logger.v('Leave channel success.');
        if (_channel != null) {
          _client!.releaseChannel(_channel!.channelId!);
        }
        _isInChannel = false;
      } catch (errorCode) {
        logger.v('Leave channel error: ' + errorCode.toString());
      }
    } else {
      if (channelId.isEmpty) {
        logger.v('Please input channel id to join.');
        return;
      }

      try {
        _channel = await createChannel(channelId);
        await _channel!.join();
        logger.v('Join channel success.');

        _isInChannel = true;
      } catch (errorCode) {
        logger.v('Join channel error: ' + errorCode.toString() + '，channelId：$channelId');
      }
    }
  }
  void rtmJoinChannelGDXluoyelive(String channelId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///离开频道
  Future rtmLeaveChannel() async {
    try {
      await _channel?.leave();
      logger.v('Leave channel success.');
      if (_channel != null) {
        _client!.releaseChannel(_channel!.channelId!);
      }
      _isInChannel = false;
    } catch (errorCode) {
      logger.v('Leave channel error: ' + errorCode.toString());
    }
  }
  void rtmLeaveChannelKPmavoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///登录
  ///注意：token不能和语音使用同一个
  Future rtmLogin(String token) async {
    YBDLogUtil.v('------_toggleLogin. token:$token');
    if (_isLogin) {
      try {
        await _client!.logout();
        YBDLogUtil.v('Logout success.');

        _isLogin = false;
        _isInChannel = false;
      } catch (errorCode) {
        YBDLogUtil.v('Logout error: ' + errorCode.toString());
      }
    } else {
      try {
        String? userId = await YBDSPUtil.getUserId();
        if (userId == null) {
          YBDLogUtil.v('rtmLogin failed. userId is null');
          return;
        }
        await _client!.login(token, userId);
        YBDLogUtil.v('rtmLogin success: ');
        _isLogin = true;
      } catch (errorCode) {
        YBDLogUtil.v('rtmLogin error: ' + errorCode.toString());
      }
    }
  }
  void rtmLoginHo5oqoyelive(String token)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///发送消息给其他用户
  void rtmSendPeerMessage(String userId, YBDAgoraRtmInfo agoraRtmInfo) async {
    if (userId.isEmpty) {
      logger.v('rtmSendPeerMessage user id is null.');
      return;
    }

    if (agoraRtmInfo == null) {
      logger.v('rtmSendPeerMessage Please input text to send.');
      return;
    }

    try {
      AgoraRtmMessage message = AgoraRtmMessage.fromText(jsonEncode(agoraRtmInfo.toJsonContent()));
      logger.v(message.text);
      await _client?.sendMessageToPeer(userId, message, false);
      logger.v('rtmSendPeerMessage Send peer message success.');
    } catch (errorCode) {
      logger.v('rtmSendPeerMessage Send peer message error: ' + errorCode.toString());
    }
  }
  void rtmSendPeerMessage24zP3oyelive(String userId, YBDAgoraRtmInfo agoraRtmInfo)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 发送消息到频道
  void rtmSendChannelMessage(YBDAgoraRtmInfo? agoraRtmInfo) async {
    if (agoraRtmInfo == null) {
      logger.v('rtmSendChannelMessage Please input text to send. agoraRtmInfo is null');
      return;
    }
    try {
      var jsonString = jsonEncode(agoraRtmInfo.toJsonContent());
      await _channel?.sendMessage(AgoraRtmMessage.fromText(jsonString));
      logger.v('rtmSendChannelMessage Send channel message success.');
    } catch (errorCode) {
      logger.v('rtmSendChannelMessage Send channel message error: ' + errorCode.toString());
    }
  }
  void rtmSendChannelMessage6lTXIoyelive(YBDAgoraRtmInfo? agoraRtmInfo)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///通道内接收到Rtm消息 处理
  receivedRtmMessage(YBDAgoraRtmInfo agoraRtmInfo) {
    logger.v('receivedRtmMessage agoraRtmInfo:${agoraRtmInfo.rtmType}');
    switch (agoraRtmInfo.rtmType) {
      case RTMType.RTMTypeUnknow:
        logger.v('rtm message type is unknow');
        break;
      case RTMType.RTMTypePauseMuteL:
        logger.v('rtm message type is RTMTypePauseMuteL');

        ///静音本通道所有麦位
        _controller.sink.add(agoraRtmInfo);
        break;
      case RTMType.RTMTypeStartPKMicNotice:
        //* 7.27修改：监听服务端发来的提醒，不用主动发送频道信息
        // print('7.23-----showPKStartMicNoticeDialog');
        // YBDDialogUtil.showPKStartMicNoticeDialog(Get.context);
        break;
    }
  }

  ///pk结束后，清理缓存数据
  cleanData() async {
    try {
      await rtmLeaveChannel();
      await _client?.logout();
      YBDLogUtil.v('Logout success.');
      _isLogin = false;
      _isLogin = false;
      _isInChannel = false;
      agoraRtmInfoStart = null;
      await _destroyRtm();
    } catch (errorCode) {
      YBDLogUtil.v('Logout error: ' + errorCode.toString());
    }
  }

  ///测试代码
  Future test(String token, String userId) async {
    YBDLogUtil.v('rtm test: token:$token ,userId:$userId');
    /* await createClient();
    await rtmLogin(token, userId);
    await rtmJoinChannel('aA_302');*/
    receivedRtmMessage(YBDAgoraRtmInfo(RTMType.RTMTypePauseMuteL, pk_id: 111, pk_state: 2));
  }
  void testg22mvoyelive(String token, String userId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 初始化配置
  Future<void> initConfig(String? token, int? roomId) async {
    if (token == null || roomId == null) {
      logger.v('YBDLiveAgoraRtmService initConfig failed :data invalid .token:$token roomId:$roomId');
      return;
    }

    if (_isInChannel) {
      logger.v('YBDLiveAgoraRtmService user is in channel._isInChannel:$_isInChannel');
      return;
    }
    await createClient();
    await rtmLogin(token);
    await rtmJoinChannel('$roomId');
    // _listenRtmEvent();
  }

  /// test 监听直播事件
  _listenRtmEvent() {
    logger.v('_listenRtmEvent :initRtmLister');
    YBDLiveAgoraRtmService.instance.onEvent.listen((event) {
      if (event is YBDAgoraRtmInfo) {
        logger.v('_listenRtmEvent YBDAgoraRtmInfo pk_id: ${event.pk_id}, pk_state:${event.pk_state}');
      }
    });
  }
}
