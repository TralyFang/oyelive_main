import 'dart:async';


import 'package:flutter/cupertino.dart';

import 'live_ybd_event.dart';

class YBDPwdPassEvent extends YBDLiveEvent {
  int roomId;
  String? pwd;
  YBDPwdPassEvent({required this.roomId, this.pwd});
}
