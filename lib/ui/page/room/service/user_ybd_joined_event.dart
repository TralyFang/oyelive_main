import 'dart:async';


import 'live_ybd_event.dart';

class YBDUserJoinedEvent extends YBDLiveEvent {
  int? uid;
  int? elapsed;

  YBDUserJoinedEvent({this.uid, this.elapsed});
}
