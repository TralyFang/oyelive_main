import 'dart:async';


import 'package:flutter/cupertino.dart';

import 'live_ybd_event.dart';

enum LiveErrorType {
  SdkError,
  SubscribeRoomError,
}

class YBDErrorEvent extends YBDLiveEvent {
  String error;
  LiveErrorType type;

  YBDErrorEvent({required this.error, required this.type});
}
