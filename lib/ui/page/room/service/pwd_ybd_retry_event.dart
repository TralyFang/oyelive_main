import 'dart:async';


import 'package:flutter/cupertino.dart';

import 'live_ybd_event.dart';

class YBDPwdRetryEvent extends YBDLiveEvent {
  int roomId;
  String? pwd;
  YBDPwdRetryEvent({required this.roomId, this.pwd});
}
