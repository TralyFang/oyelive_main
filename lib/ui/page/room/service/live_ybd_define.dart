import 'dart:async';


import 'package:agora_rtc_engine/rtc_engine.dart';

/// 直播服务相关的自定义类型
/// SDK 发生错误的回调
/// code: 错误码
typedef void OnError(ErrorCode code);

/// 加入频道成功的回调
/// channel: 频道名称
/// uid: 用户 id
/// elapsed: 从调用 RtcEngine.joinChannel 到触发该回调的时长
typedef void JoinChannelSuccess(String channel, int uid, int elapsed);

/// 离开频道的回调
/// stats: 引擎统计信息
typedef void LeaveChannel(RtcStats stats);

/// 远端用户加入频道的回调
/// uid: 用户 id
/// elapsed: 从调用 RtcEngine.joinChannel 到触发该回调的时长
typedef void UserJoined(int uid, int elapsed);

/// 远端用户离开频道的回调
/// uid: 用户 id
/// reason: 用户下线的原因
typedef void UserOffline(int uid, UserOfflineReason reason);

/// 本地用户的音量信息或远端所有用户的音量信息回调
/// [speakers] 每个说话者的用户 ID 和音量信息的数组
///
/// 在本地用户的回调中，此数组中包含以下成员:
/// uid = 0;
/// volume 等于 totalVolume，报告本地用户混音后的音量;
/// vad，报告本地用户人声状态。
///
/// 在远端用户的回调中，此数组中包含以下成员：
/// uid，表示每位说话者的用户 ID；
/// volume，表示各说话者混音后的音量；
/// vad = 0，人声检测对远端用户无效。
/// 如果报告的 speakers 数组为空，则表示远端此时没有人说话
///
/// totalVolume：（混音后的）总音量（0~255）
/// 在本地用户的回调中，totalVolume 为本地用户混音后的音量
/// 在远端用户的回调中，totalVolume 为所有说话者混音后的总音量
typedef void AudioVolumeIndication(List<AudioVolumeInfo> speakers, int totalVolume);

/// 频道场景
enum LiveProfile {
  // 通信场景（默认）
  Communication,
  // 直播场景
  LiveBroadcasting,
  // 游戏语音场景
  Game,
}

/// 直播场景下的用户角色
enum LiveRole {
  // 直播主播
  Broadcaster,
  // 直播观众
  Audience,
}

/// 保存加入房间的历史记录
/// 退出房间时要用到
class YBDEnterHistory {
  // 房间 id
  int? roomId;

  // 正在直播的房间
  // 进入房间时设置为 true
  // 离开房间时设置为 false
  bool? isCurrent;

  // 是否已订阅房间
  // [isCurrent] 等于 false 的房间要取消订阅
  bool? subscribed;

  YBDEnterHistory({
    this.roomId,
    this.isCurrent,
    this.subscribed,
  });
}
