import 'dart:async';


import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/room_socket/message/common/start_ybd_live_msg.dart';

import '../../../../common/room_socket/message/common/internal.dart';
import '../../../../common/room_socket/message/resp/response.dart';
import '../../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../define/room_ybd_define.dart';
import '../notice/notice_ybd_type.dart';
import '../util/room_ybd_util.dart';
import 'join_ybd_channel_success_event.dart';
import 'live_ybd_agora_rtm_service.dart';
import 'live_ybd_service.dart';

/// 加入房间，切房间，退出房间，保留房间的管理类
class YBDRoomManager {
  OnJoinChannelSuccess? onJoinChannelSuccess;

  void listenLiveEvent() {
    YBDLiveService.instance.onEvent.listen((event) {
      if (event is YBDJoinChannelSuccessEvent) {
        if (null != onJoinChannelSuccess) {
          // onJoinChannelSuccess(event.channel, event.uid, event.elapsed);
        }
      }
    });
  }
  void listenLiveEventw8ulRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 加入 scoket
  void joinSocket({
    required int? roomId,
    String? pwd,
    OnPwdRetry? onPwdRetry,
    OnNoticeMessage? onNoticeMessage,
    OnSubscribeSuccess? onSubscribeSuccess,
    OnChannelToken? onChannelToken,
    OnJoinChannelSuccess? onJoinChannelSuccess,
    OnSubscribeError? onSubscribeError,
  }) {
    this.onJoinChannelSuccess = onJoinChannelSuccess;
    logger.v('subscribe room: $roomId, pwd: $pwd'.subLog(step: 3));
    YBDRoomSocketApi.getInstance().subscribe(
      roomId,
      protectData: pwd ?? '',
      onSuccess: (Response data) async {
        logger.v('subscribe room response : $roomId, pwd: $pwd'.subLog(step: 3));

        if (data.code == '000000') {
          logger.v('subscribe room success : $roomId'.subLog(step: 3));

          if (null != onSubscribeSuccess) {
            onSubscribeSuccess();
          }

          // 获取 channel token
          YBDRoomSocketApi.getInstance().getChannelKey(
            roomId,
            onSuccess: (Response data) async {
              logger.v('get channel token : ${data.result}');
              if (null != onChannelToken) {
                onChannelToken(data.result);
              }
            },
            onTimeOut: () {
              logger.e('get channel token timeout');
            },
          );

          // 获取 RTM token
          YBDRoomSocketApi.getInstance().getRtmKey(
            roomId,
            onSuccess: (Response data) async {
              logger.v('get Rtm token : ${data.result}');
              YBDLiveAgoraRtmService.instance.initConfig(data.result, roomId);
            },
            onTimeOut: () {
              logger.e('get Rtm token timeout');
            },
          );
        } else if (data.code == '905019' || data.code == '905018') {
          logger.e('subscribe room pwd error : $roomId, $pwd, ${data.desc}'.subLog(step: 3));

          if (null != onPwdRetry) {
            logger.v('retry pwd : $pwd');
            onPwdRetry();
          }
        } else if (data.code == '905031') {
          // 主播限播
          logger.e("subscribeRoom to room 905031");
          YBDInternal noticeMessage = YBDInternal()
            ..type = YBDNoticeType.TYPE_LIMIT
            ..content = data.desc;

          // 限播 (通过组装成 NoticeMessage消息  发送)
          if (null != onNoticeMessage) {
            onNoticeMessage(noticeMessage);
          }
        } else {
          logger.e('subscribe room error : ${data.code}'.subLog(step: 3));
          if (null != onSubscribeError) {
            onSubscribeError('subscribe room error');
          }
        }
      },
      onTimeOut: () {
        logger.e('subscribe room timeout'.subLog(step: 3));
      },
    );
  }

  /// 离开 socket
  void leaveSocket({required int? roomId}) {
    var duration = Duration(milliseconds: 0);
    if (YBDRoomUtil.isTalent(roomId)) {
      duration = Duration(milliseconds: 100);
      // 主播房间停播
      logger.v('stop broadcast roomId : $roomId'.unsubLog(step: 87));
      YBDRoomSocketApi.getInstance().stopBroadcast(
        roomId,
        onSuccess: (Response data) {
          logger.v('stop broadcast success code : ${data.code}, roomId: $roomId'.unsubLog(step: 87));
          // YBDRoomSocketApi.getInstance().unsubscribe(roomId);
        },
        onTimeOut: () {
          logger.v('stop broadcast timeout : $roomId'.unsubLog(step: 87));
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.SOCKET_RESPONSE_EVENT,
              location: YBDLocationName.ROOM_PAGE,
              itemName: 'stop broadcast timeout'));
          // 超时就不要再处理取消订阅事件了，反之重复开房发不了消息的问题
          // YBDRoomSocketApi.getInstance().unsubscribe(roomId);
        },
      );
    }
    logger.v('unsubscribe room : $roomId'.unsubLog(step: 88));
    Future.delayed(duration, (){
      YBDRoomSocketApi.getInstance().unsubscribe(roomId);
    });
  }
  void leaveSocketNsNuEoyelive({required int? roomId}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 通知服务端已经在开播
  void startLiveNotice({
    int? roomId,
    String? pwd,
    String? title,
    String? category,
    int? micEnable,
    OnNoticeMessage? onNoticeMessage,
  }) {
    logger.v('_startLiveNotice category:$category title:$title ');
    YBDRoomSocketApi.getInstance().startBroadcastTitle(
      roomId,
      screen: 2,
      title: '$category,$title',
      channel: '$category',
      protectMode: (pwd ?? '').isEmpty ? 0 : 1,
      protectData: (pwd ?? '').isEmpty ? null : pwd,
      tag: '$category',
      micRequestEnable: micEnable,
      onSuccess: (Response data) {
        logger.v('start broadcast result : ${data.code}'.subLog(step: 4));
        YBDStartLiveMsg liveMsg  = YBDStartLiveMsg()
          ..code = data.code
          ..desc = data.desc;
          // 限播 (通过组装成 NoticeMessage消息  发送)
          if (null != liveMsg) {
            onNoticeMessage!(liveMsg);
          }
      },
    );
  }

  // /// 订阅房间
  // Future<String> subscribeRoom({
  //   @required int roomId,
  //   String pwd,
  // }) async {
  //   final completer = Completer<String>();
  //   logger.v('begin subscribe : $roomId');
  //   YBDRoomSocketApi.getInstance().subscribe(
  //     roomId,
  //     protectData: '',
  //     onSuccess: (Response data) async {
  //       logger.v('subscribe room response code : ${data.code}');

  //       if (data.code == '000000') {
  //         // 订阅房间成功
  //         logger.v('subscribe room success : $roomId');
  //         completer.complete(ROOM_SUCCESS);
  //       } else if (data.code == '905019' || data.code == '905018') {
  //         // 密码错误
  //         logger.v('subscribe room pwd error : $roomId, $pwd, ${data.desc}');
  //         completer.complete(ROOM_PWD_ERROR);
  //       } else if (data.code == '905031') {
  //         // 主播限播
  //         logger.e("subscribeRoom to room error : 905031");
  //         completer.complete(TALENT_LIMIT);
  //       } else {
  //         // 订阅房间错误
  //         logger.e('subscribe room error : ${data.code}');
  //         completer.complete(SUBSCRIBE_ROOM_ERROR);
  //       }
  //     },
  //   );

  //   return completer.future;
  // }

  // /// 切房间时调socket接口获取token
  // Future<String> channelToken({
  //   @required int roomId,
  //   String pwd,
  // }) async {
  //   final completer = Completer<String>();
  //   logger.v('begin subscribe: $roomId');
  //   YBDRoomSocketApi.getInstance().subscribe(
  //     roomId,
  //     protectData: '',
  //     onSuccess: (Response data) async {
  //       logger.v('subscribe room response: $roomId, pwd: $pwd');
  //       if (data.code == '000000') {
  //         logger.v('subscribe room success : $roomId');

  //         // 获取 channel token
  //         YBDRoomSocketApi.getInstance().getChannelKey(
  //           roomId,
  //           onSuccess: (Response data) async {
  //             logger.v('get channel token : ${data?.result}');
  //             logger.v('before completer: $roomId');
  //             completer.complete(data?.result);
  //             logger.v('after completer: $roomId');
  //           },
  //           onTimeOut: () {
  //             completer.completeError(GET_CHANNEL_KEY_TIMEOUT);
  //             logger.v('get channel token timeout');
  //           },
  //         );

  //         // 获取 RTM token
  //         YBDRoomSocketApi.getInstance().getRtmKey(
  //           roomId,
  //           onSuccess: (Response data) async {
  //             logger.v('get Rtm token : ${data?.result}');
  //             YBDLiveAgoraRtmService.instance.initConfig(data?.result, roomId);
  //           },
  //           onTimeOut: () {
  //             logger.e('get Rtm token timeout');
  //           },
  //         );
  //       } else if (data.code == '905019' || data.code == '905018') {
  //         logger.e('subscribe room pwd error : $roomId, $pwd, ${data.desc}');

  //         if (null != onPwdRetry) {
  //           logger.v('retry pwd : $pwd');
  //           onPwdRetry();
  //         }
  //       } else if (data.code == '905031') {
  //         // 主播限播
  //         logger.e("subscribeRoom to room 905031");
  //         YBDInternal noticeMessage = YBDInternal()
  //           ..type = YBDNoticeType.TYPE_LIMIT
  //           ..content = data.desc;

  //         // 限播 (通过组装成 NoticeMessage消息  发送)
  //         if (null != onNoticeMessage) {
  //           onNoticeMessage(noticeMessage);
  //         }
  //       } else {
  //         logger.e('subscribe room error : ${data.code}');
  //         if (null != onSubscribeError) {
  //           onSubscribeError('subscribe room error');
  //         }
  //       }
  //     },
  //     onTimeOut: () {
  //       logger.e('subscribe room timeout');
  //       completer.completeError('result error: timeout');
  //     },
  //   );

  //   logger.v('end subscribe: $roomId');
  //   return completer.future;
  // }
}
