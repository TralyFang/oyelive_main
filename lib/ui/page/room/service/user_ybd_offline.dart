import 'dart:async';


import 'package:agora_rtc_engine/rtc_engine.dart';
import 'live_ybd_event.dart';

class YBDUserOfflineEvent extends YBDLiveEvent {
  int? uid;
  UserOfflineReason? reason;

  YBDUserOfflineEvent({this.uid, this.reason});
}
