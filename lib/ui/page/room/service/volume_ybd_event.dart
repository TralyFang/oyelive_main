import 'dart:async';


import 'package:agora_rtc_engine/rtc_engine.dart';
import 'live_ybd_event.dart';

class YBDVolumeEvent extends YBDLiveEvent {
  List<AudioVolumeInfo>? speakers;
  int? totalVolume;

  YBDVolumeEvent({this.speakers, this.totalVolume});
}
