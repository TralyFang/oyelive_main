import 'dart:async';


import 'dart:async';
import 'dart:convert';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/event/JoinChannelBusEvent.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/room/entity/room_ybd_operate_info.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_operat_halper.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/room_socket/message/room_ybd_message_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../home/entity/car_ybd_list_entity.dart';
import '../../home/entity/gift_ybd_list_entity.dart';
import '../define/room_ybd_define.dart';
import '../pk/pk_ybd_rtm_helper.dart';
import '../util/tp_ybd_dialog_manager.dart';
import 'error_ybd_event.dart';
import 'join_ybd_channel_success_event.dart';
import 'leave_ybd_channel_event.dart';
import 'live_ybd_agora_rtm_service.dart';
import 'live_ybd_data.dart';
import 'live_ybd_define.dart';
import 'live_ybd_event.dart';
import 'message_ybd_event.dart';
import 'pwd_ybd_incorrect_event.dart';
import 'pwd_ybd_pass_event.dart';
import 'pwd_ybd_retry_event.dart';
import 'room_ybd_manager.dart';
import 'user_ybd_joined_event.dart';
import 'user_ybd_offline.dart';
import 'volume_ybd_event.dart';

/// 声网 SDK 服务类
class YBDLiveService {
  static YBDLiveService? _instance;

  static YBDLiveService get instance => _getInstance();

  factory YBDLiveService() => _getInstance();

  static YBDLiveService _getInstance() {
    if (_instance == null) {
      _instance = YBDLiveService._();
    }
    return _instance!;
  }

  // 进房异常 需要退出房间
  bool enterRoomEnable = true;

  // 游戏中被踢出，限制功能开关
  bool bootedLiveRoomLimit = false;

  /// 私有构造函数
  YBDLiveService._() {
    //
  }

  List<YBDCarListRecord?>? carList;
  List<YBDGiftListRecordGift?>? giftList;
  BuildContext? mainContext;

  /// RTC 引擎
  RtcEngine? _engine;

  /// 直播信息
  YBDLiveData _liveData = YBDLiveData();

  YBDLiveData get data => _liveData;

  /// 事件广播
  /// 调用 onEvent.listen() 接收广播
  // ignore: close_sinks
  StreamController<YBDLiveEvent> _controller = StreamController<YBDLiveEvent>.broadcast();

  Stream<YBDLiveEvent> get onEvent => _controller.stream;

  /// 房间对话框管理器
  YBDTPDialogManager _dialogManger = YBDTPDialogManager();

  YBDTPDialogManager get dialogManager {
    if (null == _dialogManger) {
      _dialogManger = YBDTPDialogManager();
    }
    return _dialogManger;
  }

  /// 加入房间，切房间，退出房间，保留房间的管理对象
  final _roomManager = YBDRoomManager();

  /// 初始化配置
  Future<void> initConfig() async {
    _listenSocketMsg();
    await _startEngine();
    _setEngineEventHandler();
    // _roomManager.listenLiveEvent();
  }
  void initConfighgAysoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  RtcEngine? getEngine() {
    return _engine;
  }

  /// 监听房间 socket 消息
  void _listenSocketMsg() {
    YBDRoomMessageHelper.getInstance().messageOutStream.listen((message) {
      _controller.sink.add(YBDMessageEvent(message));

      // 保留消息防止从开播页面进到房间页面丢消息
      // if (message is YBDSuperFansMessage) {
      //   logger.v('received YBDSuperFansMessage');
      //   _liveData.superFansMsg = message;
      // } else if (message is YBDUserPublish) {
      //   logger.v('received YBDUserPublish msg');
      //   _liveData.viewerMsg = message;
      // }
    });
  }
  void _listenSocketMsgSyfr8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _setEngineEventHandler() {
    logger.i('add engine event handler');
    assert(_engine != null, 'engine is null');
    _engine?.setEventHandler(RtcEngineEventHandler(
      tokenPrivilegeWillExpire: (token) {
        // 需要重新从服务器获取token, 然后同步到SDK
        logger.i('===engine tokenPrivilegeWillExpire $token');
        // _engine.renewToken(token)
      },
      connectionStateChanged: (type, reason) {
        logger.i('===engine connectionStateChanged $reason, ${YBDRoomSocketUtil.getInstance().roomId}');
        // token已经过期了，需要重新从服务器获取token，并且重新joinChannel
        if (reason == ConnectionChangedReason.TokenExpired) {}

        if (reason == ConnectionChangedReason.Interrupted) {
          YBDRoomSocketUtil.getInstance().disconnect();
        }
      },
      error: (ErrorCode code) {
        logger.e('sdk error : $code');
        _controller.sink.add(YBDErrorEvent(error: '$code', type: LiveErrorType.SdkError));
      },
      joinChannelSuccess: (channel, uid, elapsed) async {
        logger.i('join channel success : $channel, uid: $uid');
        _liveData.isJoined = true;
        // 广播加入频道的事件
        eventBus.fire(YBDJoinChannelBusEvent());
        _controller.sink.add(YBDJoinChannelSuccessEvent(channel: channel, uid: uid, elapsed: elapsed));
      },
      clientRoleChanged: (ClientRole previous, ClientRole current) {
        logger.i('client role changed previous : $previous, current : $current');
        YBDMicOperateHelper.getInstance()!.checkMicLegal(previous, current);
      },
      leaveChannel: (stats) {
        logger.i('leave channel');
        _controller.sink.add(YBDLeaveChannelEvent(stats: stats));
      },
      userJoined: (uid, elapsed) {
        logger.i('remote user : $uid join channel');
        _liveData.users.add(uid);
        _controller.sink.add(YBDUserJoinedEvent(uid: uid, elapsed: elapsed));
      },
      userOffline: (uid, reason) {
        logger.i('remote user : $uid offline, reason : $reason');
        _liveData.users.remove(uid);
        _controller.sink.add(YBDUserOfflineEvent(uid: uid, reason: reason));
      },
      audioVolumeIndication: (speakers, totalVolume) {
        // logger.v('speakers length: ${speakers.length}');
        // speakers.forEach((element) {
        //   logger.v('speakers: ${element.uid}');
        // });

        _controller.sink.add(YBDVolumeEvent(speakers: speakers, totalVolume: totalVolume));
      },
      channelMediaRelayStateChanged: (state, code) {
        YBDPKRTMHelper.eventStateHandle(state, code);
      },
      channelMediaRelayEvent: (e) {
        YBDPKRTMHelper.eventHandle(e);
      },
      apiCallExecuted: (ErrorCode error, String api, String result) {
        /*logger.i('apiCallExecuted -> error:$error api:$api, result : $result');
        //rtc.client_role 角色转换
        //che.audio.enable.recording.device audio使用开关
        //rtc.audio.mute_peer 静音
        if (error != ErrorCode.NoError) {
          YBDCommonUtil.storeEngineApiError(null, api, result);
        }*/
      },
      localAudioStateChanged: (AudioLocalState state, AudioLocalError error) {
        logger.i('localAudioStateChanged -> state:$state error:$error');
      },
      remoteAudioStateChanged: (int uid, AudioRemoteState state, AudioRemoteStateReason reason, int elapsed) {
        String? roomAbuserEnable = YBDCommonUtil.storeFromContext()!.state.configs!.roomAbuserEnable;
        // 记录Broadcaster角色的uid
        if (roomAbuserEnable == '1' && (state == AudioRemoteState.Starting || state == AudioRemoteState.Stopped)) {
          state == AudioRemoteState.Starting ? _liveData.speakers.add(uid) : _liveData.speakers.remove(uid);
        }

        logger.v('speakers callback: $uid, $state');
      },
      microphoneEnabled: (bool enabled) {
        logger.i('microphoneEnabled -> enabled:$enabled');
      },
      userMuteAudio: (int uid, bool muted) {
        logger.i('userMuteAudio -> uid:$uid muted:$muted');
      },
    ));
  }
  void _setEngineEventHandlersUNdooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 初始化引擎
  Future<void> _startEngine() async {
    if (null == _engine) {
      logger.i('init engine');
      try {
        // 创建实例
        final agoraPath = await YBDLogUtil.agoraLogPath();
        // final level = Const.TEST_ENV ? LogLevel.Info : LogLevel.Info;
        // 声网Info级别的日志不多，可以保留来定位问题
        LogConfig logConfig = LogConfig(filePath: agoraPath, level: LogLevel.Info);
        logger.v('agora LogConfig path: $agoraPath');
        _engine = await RtcEngine.createWithConfig(RtcEngineConfig(Const.AGORA_APP_ID, logConfig: logConfig));
        // _engine = await RtcEngine.createWithConfig(RtcEngineConfig(Const.AGORA_APP_ID));
      } catch (e) {
        logger.e('create engine error : $e');
      }

      // 启用说话者音量提示，false 关闭本地人声检测
      _engine?.enableAudioVolumeIndication(500, 3, false);

      // 设置频道属性
      setChannelProfile(LiveProfile.LiveBroadcasting);

      // _engine.setParameters("{\"che.audio.mixable.option\":true}");
      ///声网2021/8/3 沟通完 这种场景下更利于 用户频繁上下麦 切换角色
      // _engine.setAudioProfile(AudioProfile.SpeechStandard, AudioScenario.ChatRoomGaming);
      setAudioProfile();

      // 设置用户角色
      await _engine?.setClientRole(ClientRole.Audience);
    } else {
      logger.i('engine already init');
    }
  }
  void _startEngineWf9u3oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///set audioProflie
  setAudioProfile() {
    final store = YBDCommonUtil.storeFromContext(context: Get.context)!;
    Map? result;
    try {
      result = jsonDecode(store.state.configs!.roomOperate!);
    } on Error catch (error) {
      logger.v('config error');
    }
    YBDRoomOperateInfo info = YBDRoomOperateInfo.get(result);
    // if (info?.audioProfile == 1 || info?.audioProfile == 2) {
    //等于1 设置的媒体音量  2 通话音量
    logger.v('set audio profile :${info.audioProfile}');
    switch (info.audioProfile) {
      case 1:
      case 2:
        _engine!.setAudioProfile(AudioProfile.SpeechStandard,
            info.audioProfile == 1 ? AudioScenario.GameStreaming : AudioScenario.ChatRoomGaming);
        break;
      case 3:
      case 4:
        _engine!.setAudioProfile(
            AudioProfile.Default, info.audioProfile == 3 ? AudioScenario.GameStreaming : AudioScenario.ChatRoomGaming);
        break;
    }
    // }
  }

  /// 重启引擎
  Future<void> restartEngine() async {
    try {
      await _engine?.destroy();
      _engine = null;
      logger.i('done destroy engine');
      await _startEngine();
      logger.i('done start engine');
    } catch (e) {
      logger.e('room restart engine error: $e');
    }
  }
  void restartEnginezdviIoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 设置直播场景下的用户角色
  Future<void> setClientRole(LiveRole role, {bool? runTimemute = false}) async {
    assert(_engine != null, 'live engine is null');
    logger.i('change client role : $role');
    switch (role) {
      case LiveRole.Broadcaster:
        _engine?.setClientRole(ClientRole.Broadcaster);

        ///声网 3.5.0 之前修复的bug 先加入通道 后申请权限 可能会导致声网 获取权限失败或者被其他占用
        // _engine.enableLocalAudio(false);
        // _engine.enableLocalAudio(true);

        ///禁言功能
        if (runTimemute!) {
          logger.i('change client role ClientRole.Broadcaster: ---mute');
          muteLocalAudioStream(true);
        } else {
          logger.i('change client role ClientRole.Broadcaster: ---unmute');
          muteLocalAudioStream(false);
        }
        break;
      case LiveRole.Audience:
        logger.i('change client role ClientRole.Audience:');
        _engine?.setClientRole(ClientRole.Audience);
        muteLocalAudioStream(true);
        break;
    }
  }
  void setClientRolenDAZuoyelive(LiveRole role, {bool? runTimemute = false})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 停止/恢复接收所有音频流
  /// muted 设置是否默认不接收所有远端音频
  /// true：不接收所有远端音频流
  /// false：（默认）接收所有远端音频流
  Future<void> muteAllRemoteAudioStreams(bool muted) async {
    _engine?.muteAllRemoteAudioStreams(muted);
  }

  /// 添加当前浏览的房间到历史记录
  void _updateHistoryWithCurrent(int roomId) {
    assert(roomId != null, 'roomId is null');

    // 退出历史房间
    _leaveHistorySocket(roomId);

    // 历史房间改成非当前状态
    _liveData.history.forEach((element) {
      assert(element != null, 'history room item is null');
      element.isCurrent = false;
    });
  }

  /// 退出历史房间
  void _leaveHistorySocket(int currentRoom) {
    _liveData.history.forEach((element) {
      assert(element != null, 'history room item is null');
      if (element.roomId != currentRoom) {
        // 取消订阅
        logger.v("leave history socket roomId ${element.roomId}, currentRoomId: $currentRoom".unsubLog(step: 86));
        _roomManager.leaveSocket(roomId: element.roomId);
      }
    });
  }
  void _leaveHistorySocketoBJ5Toyelive(int currentRoom) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 创建房间
  /// 进入房间页面后还会调一次 [joinRoom]
  Future<void> createRoom({
    String? pwd,
    String? title,
    String? category,
    int? micEnable,
  }) async {
    await _startEngine();
    _setEngineEventHandler();
    logger.i('engine started: $_engine'.subLog(step: 2));

    final userInfo = YBDCommonUtil.storeFromContext()!.state.bean!;
    _roomManager.joinSocket(
      roomId: userInfo.id,
      onChannelToken: (token) async {
        logger.i('join channel with token and startLiveNotice'.subLog(step: 4));
        // 加入频道
        await joinChannel(token, userInfo.id);

        // 向服务器发出开播通知
        _roomManager.startLiveNotice(
          roomId: userInfo.id,
          pwd: pwd,
          title: title,
          category: category,
          micEnable: micEnable,
          onNoticeMessage: (notice) {
            logger.e('create room error ');
            _controller.sink.add(YBDMessageEvent(notice));
          },
        );

        // 记录房间信息
        _liveData.roomId = userInfo.id;
        _liveData.protectMode = (pwd ?? '').isEmpty ? 0 : 1;
        _liveData.protectData = (pwd ?? '').isEmpty ? null : pwd;
        _liveData.roomTitle = '$category,$title';
        _liveData.roomCategory = '$category';

        // 保存房间名
        YBDSPUtil.saveRoomTitle(title);
      },
      onJoinChannelSuccess: (String channel, int uid, int elapsed) {
        logger.i('start live :  $channel, $uid, $elapsed');
      },
      onNoticeMessage: (notice) {
        logger.i('create room limit notice: $userInfo.id, $pwd');
        _controller.sink.add(YBDMessageEvent(notice));
      },
    );
  }

  /// 加入房间
  Future<void> joinRoom({required int roomId, String? pwd}) async {
    logger.v('join room');
    assert(null != roomId, 'roomId is null');
    await _startEngine();
    _setEngineEventHandler();

    logger.v("joinRoom roomId $roomId".unsubLog(step: 85));
    // 更新进房间历史记录
    _updateHistoryWithCurrent(roomId);

    _roomManager.joinSocket(
      roomId: roomId,
      pwd: pwd,
      onChannelToken: (token) async {
        logger.i('onChannelToken : $token');
        if (!_liveData.isJoined) {
          // 开播时调用[createRoom]方法加入声网，这里避免重复加入
          if (roomId != YBDCommonUtil.storeFromContext()!.state.bean!.id) {
            // 加入声网
            await joinChannel(token, roomId);
          }
        }
      },
      onSubscribeSuccess: () {
        if (null != pwd) {
          logger.i('room pwd passed: $roomId, $pwd}');
          _controller.sink.add(YBDPwdPassEvent(roomId: roomId, pwd: pwd));
        }

        // 添加当前房间
        _liveData.history.add(YBDEnterHistory(roomId: roomId, isCurrent: true, subscribed: true));
      },
      onPwdRetry: () {
        _liveData.retryPwd += 1;

        if (_liveData.retryPwd < MAX_RETRY_PWD) {
          logger.i('room pwd retry: $roomId, $pwd}');
          _controller.sink.add(YBDPwdRetryEvent(roomId: roomId, pwd: pwd));
        } else {
          logger.i('room pwd incorrect: $roomId, $pwd}');
          _controller.sink.add(YBDPwdIncorrectEvent(roomId: roomId, pwd: pwd));
        }
      },
      onNoticeMessage: (notice) {
        logger.i('talent limit notice: $roomId, $pwd');
        _controller.sink.add(YBDMessageEvent(notice));
      },
      onSubscribeError: (error) {
        logger.e('talent limit notice: $roomId, $pwd}');
        _controller.sink.add(YBDErrorEvent(error: error, type: LiveErrorType.SubscribeRoomError));
      },
    );
  }

  void ludoplayingExitRoom(int? roomId) {
    logger.i('ludoplayingExitRoom leaveChannel roomId: $roomId');
    try {
      // 离开频道
      _leaveChannel();
    } catch (e) {
      logger.e('ludoplayingExitRoom leaveChannel error : $e');
    }
  }
  void ludoplayingExitRoompK2Ieoyelive(int? roomId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 离开房间
  /// 调socket接口取消房间订阅
  /// 调声网方法离开频道
  /// 和[cleanLive]方法分开调用
  Future<void> leaveRoomV2({
    required int? roomId,
    // @required RoomPageType roomType,
  }) async {
    logger.i('leave room : $roomId');

    try {
      // 离开 socket
      _roomManager.leaveSocket(roomId: roomId);

      // 离开频道
      await _leaveChannel();

      // 离开房间清除 rtm数据
      await YBDLiveAgoraRtmService.instance.cleanData();
    } catch (e) {
      logger.e('leave room v2 error : $e');
    }
  }

  /// 切换房间
  /// 步骤： 1. 取消房间订阅 2. 清除直播数据 3. 离开声网频道
  /// [sourceRoomId]离开的房间
  Future<void> changeRoom({required int sourceRoomId}) async {
    logger.i('changeRoom room : $sourceRoomId');

    try {
      // 离开 socket
      _roomManager.leaveSocket(roomId: sourceRoomId);

      // 清除直播数据
      _liveData.cleanData();

      // 离开频道
      // await _leaveChannel();
    } catch (e) {
      logger.e('leave room v2 error : $e');
    }
  }
  void changeRoomi0Eixoyelive({required int sourceRoomId})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 快速切换房间时调用:
  /// 1.取消房间订阅
  /// 2.清除上一个房间的数据
  Future<void> cleanOldRoom({required int? roomId}) async {
    logger.i('unsubscribe room : $roomId');
    // 还原状态
    bootedLiveRoomLimit = false;
    // 取消房间订阅
    _roomManager.leaveSocket(roomId: roomId);

    // 清除直播数据
    _liveData.cleanData();
  }

  /// 快速切换房间
  /// 新版流程：先在[cleanOldRoom]清上个房间的数据然后在[switchRoom]里switchChannel
  /// 旧版流程：先在[changeRoom]里调声网的leaveChannel然后[joinRoom]里joinChannel
  /// [roomId]加入新的房间ID
  /// [pwd]房间密码，切房间时忽略这个参数
  Future<void> switchRoom({required int roomId, String pwd = ''}) async {
    assert(null != roomId, 'roomId is null');
    logger.i('switch new room: $roomId');

    // 切房间时检查是否已经开启引擎
    await _startEngine();

    logger.v("switchRoom roomId $roomId".unsubLog(step: 100));
    // 更新进房间历史记录
    _updateHistoryWithCurrent(roomId);

    // final result = await _roomManager.subscribeRoom(roomId: roomId, pwd: pwd);

    // if (result == ROOM_SUCCESS) {
    //   // 加入声网
    //   await _switchChannel(token, roomId);
    // }

    // TODO: 测试代码
    // 这部分代码和[joinRoom]方法里的代码重合了，要优化
    _roomManager.joinSocket(
      roomId: roomId,
      pwd: pwd,
      onChannelToken: (token) async {
        logger.v('onChannelToken : $token');
        logger.i("switchRoom: ${await _engine?.getConnectionState()}, ${_liveData.isJoined}");

        // 开播时调用[createRoom]方法加入声网，这里避免重复加入
        if (roomId != YBDCommonUtil.storeFromContext()!.state.bean!.id) {
          // 切房间时先检查是否加入过频道
          if (await _engine?.getConnectionState() == ConnectionStateType.Disconnected) {
            // 加入声网频道
            await joinChannel(token, roomId);
          } else {
            // 切换声网频道
            await _switchChannel(token, roomId);
          }
        }
      },
      onSubscribeSuccess: () {
        if (null != pwd) {
          logger.i('room pwd passed: $roomId, $pwd}');
          _controller.sink.add(YBDPwdPassEvent(roomId: roomId, pwd: pwd));
        }

        // 添加当前房间
        _liveData.history.add(YBDEnterHistory(roomId: roomId, isCurrent: true, subscribed: true));
      },
      onPwdRetry: () {
        _liveData.retryPwd += 1;

        if (_liveData.retryPwd < MAX_RETRY_PWD) {
          logger.i('room pwd retry: $roomId, $pwd}');
          _controller.sink.add(YBDPwdRetryEvent(roomId: roomId, pwd: pwd));
        } else {
          logger.i('room pwd incorrect: $roomId, $pwd}');
          _controller.sink.add(YBDPwdIncorrectEvent(roomId: roomId, pwd: pwd));
        }
      },
      onNoticeMessage: (notice) {
        logger.i('talent limit notice: $roomId, $pwd');
        _controller.sink.add(YBDMessageEvent(notice));
      },
      onSubscribeError: (error) {
        logger.e('talent limit notice: $roomId, $pwd}');
        _controller.sink.add(YBDErrorEvent(error: error, type: LiveErrorType.SubscribeRoomError));
      },
    );
  }

  /// 离开房间
  Future<void> leaveRoom({
    required int? roomId,
  }) async {
    logger.i('leave room : $roomId');

    try {
      // 离开 socket
      _roomManager.leaveSocket(roomId: roomId);

      // 离开频道
      await _leaveChannel();

      // 清除直播数据
      cleanLive();

      // 离开房间清除 rtm数据
      await YBDLiveAgoraRtmService.instance.cleanData();
    } catch (e) {
      logger.e('===live service leave channel failed');

      // 清除直播数据
      cleanLive();
    }
  }

  /// 异常离开房间  在房间被挤出到登录面
  Future<void> leaveRoomInvalid() async {
    logger.i('leave leaveRoomInvalid ');

    try {
      // 离开频道
      await _leaveChannel();

      // 清除直播数据
      cleanLive();

      // 离开房间清除 rtm数据
      await YBDLiveAgoraRtmService.instance.cleanData();
    } catch (e) {
      logger.e('===live service leave leaveRoomInvalid failed');

      // 清除直播数据
      cleanLive();
    }
  }
  void leaveRoomInvalidv6AS7oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 清理直播资源
  void cleanLive() {
    logger.i('clean live resource');
    _liveData.keepHistory();
    _liveData.cleanData();
    _dialogManger.removeAllDialogID();
    _engine?.destroy();
    _engine = null;
  }

  /// 切换房间
  // Future<void> changeRoom({@required int roomId, @required String pwd}) async {
  //   // 离开当前房间
  //   // await leaveRoom(roomId: _liveData.roomId, roomType: _liveData.pageType);

  //   // 加入新房间
  //   joinRoom(roomId: roomId, pwd: pwd);
  // }

  /// 加入频道
  /// token: 在 App 服务器端生成的用于鉴权的 Token
  /// roomId 对应声网的 [channelName] 频道名
  /// optionalInfo: 附加信息，一般可设置为空
  /// optionalUid: 用户 ID，设置为 0 时 SDK 会自动分配一个
  Future<void> joinChannel(String? token, int? roomId) async {
    logger.i('join channel roomId : $roomId, token : $token');

    // 和进入房间页面时的 roomId 一致才加入频道，避免串房间
    if (_liveData.roomId == roomId) {
      // 获取当前用户的信息
      final userInfo = YBDCommonUtil.storeFromContext()!.state.bean!;

      // 加入频道时添加的额外信息
      String? optionalInfo = userInfo.id == roomId ? "{'owner':true,'bitrate':100};" : null;

      assert(_engine != null, 'should init engine');
      logger.i('===engine join channel : $token, $roomId, $optionalInfo, ${userInfo.id}');
      await _engine?.joinChannel(token, '$roomId', optionalInfo, userInfo.id ?? 0);
    } else {
      logger.e('roomId not equal to _liveData.roomId : $roomId != ${_liveData.roomId}');
    }
  }
  void joinChannel7B9bPoyelive(String? token, int? roomId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 切换频道
  /// token 在 App 服务器端生成的用于鉴权的 Token
  /// roomId 对应声网的 [channelName] 频道名
  Future<void> _switchChannel(String? token, int roomId) async {
    logger.i('switch channel roomId : $roomId, token : $token');
    assert(roomId == _liveData.roomId, '$roomId != ${_liveData.roomId}');
    assert(_engine != null, 'should init engine');

    // 和页面记录的 roomId 一致才加入频道，避免串房间
    if (_liveData.roomId == roomId) {
      logger.i('switch channel : $token, $roomId');
      // 切频道后自动订阅音频流，不自动订阅视频流
      final options = ChannelMediaOptions(autoSubscribeAudio: true, autoSubscribeVideo: false);
      await _engine?.switchChannel(token, '$roomId', options);
    }
  }

  /// 离开频道
  Future<void> _leaveChannel() async {
    logger.i('leave channel');
    await _engine?.leaveChannel();
  }

  /// 停止/恢复发送本地音频流
  void muteLocalAudioStream(bool muted) {
    logger.i('mute local audio stream muted : $muted');
    _engine?.muteLocalAudioStream(muted);

    ///开启/关闭本地音频采集。(为了解决偶发下麦或静麦的用户还发出声音)
    _engine?.enableLocalAudio(!muted);
  }

  /// 调节本地播放的所有远端用户音量
  void adjustPlaybackSignalVolume(int volume) {
    logger.v('set play back volume : $volume');
    _engine?.adjustPlaybackSignalVolume(volume);
    YBDMicOperateHelper.getInstance()!.reloadMuteWithVolumn(volume);
  }
  void adjustPlaybackSignalVolumez1PTnoyelive(int volume) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 停止/恢复接收指定音频流
  Future<void> muteRemoteAudioStream(int uid, bool muted) async {
    logger.v('mute remote audio stream uid : $uid, muted : $muted');
    _engine?.muteRemoteAudioStream(uid, muted);
  }

  /// 设置频道场景
  void setChannelProfile(LiveProfile profile) {
    logger.i('set channel profile : $profile');

    switch (profile) {
      case LiveProfile.Communication:
        _engine?.setChannelProfile(ChannelProfile.Communication);
        break;
      case LiveProfile.LiveBroadcasting:
        _engine?.setChannelProfile(ChannelProfile.LiveBroadcasting);
        break;
      case LiveProfile.Game:
        _engine?.setChannelProfile(ChannelProfile.Game);
        break;
    }
  }

  /// 启用说话者音量提示
  void enableAudioVolumeIndication(int interval, int smooth, bool reportVad) {
    logger.v('enable audio volume indication : $interval, $smooth, $reportVad');
    _engine?.enableAudioVolumeIndication(interval, smooth, reportVad);
  }

  /// 恢复播放音乐文件
  void resumeAudioMixing() {
    logger.v('resume audio mixing');
    _engine?.resumeAudioMixing();
  }

  /// 开始播放音乐文件
  void startAudioMixing(String filePath, bool loopback, bool replace, int cycle) {
    logger.v('start audio mixing');
    _engine?.startAudioMixing(filePath, loopback, replace, cycle);
  }

  /// 获取音乐文件的播放进度
  Future<int?> getAudioMixingCurrentPosition() async {
    // logger.v('get audio mixing current position');

    try {
      return await _engine?.getAudioMixingCurrentPosition();
    } catch (e) {
      // logger.v('get audio mixing current position error : $e');
      return 0;
    }
  }

  /// 调节音乐文件播放音量
  Future<int?> getAudioMixingDuration() async {
    // logger.v('get audio mixing duration');
    try {
      return await _engine?.getAudioMixingDuration();
    } catch (e) {
      // logger.v('get audio mixing duration error : $e');
      return 0;
    }
  }

  /// 调节混音音量
  void adjustAudioMixingVolume(int volume) {
    logger.v('adjust audio mixing volume');
    _engine?.adjustAudioMixingVolume(volume);
  }
  void adjustAudioMixingVolumemNNo4oyelive(int volume) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 暂停播放音乐文件
  void pauseAudioMixing() {
    logger.v('pause audio mixing');
    _engine?.pauseAudioMixing();
  }

  /// 调节录音音量
  void adjustRecordingSignalVolume(int volume) {
    logger.v('adjust recording signal volume');
    _engine?.adjustRecordingSignalVolume(volume);
  }
}
