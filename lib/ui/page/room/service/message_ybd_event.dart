import 'dart:async';


import '../../../../common/room_socket/message/base/message.dart';
import 'live_ybd_event.dart';

class YBDMessageEvent extends YBDLiveEvent {
  YBDMessage message;
  YBDMessageEvent(this.message);
}
