import 'dart:async';


import '../../../../../generated/json/base/json_convert_content.dart';

class YBDGameEntity with JsonConvert<YBDGameEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDGameRecord?>? record;
  String? recordSum;
}

class YBDGameRecord with JsonConvert<YBDGameRecord> {
  int? id;
  String? name;
  String? icon;
  String? url;
  String? platform;
  String? appVersion;
  int? display;
  String? shieldCountryCode;
  int? index;
  String? code;

  /// 0：隐藏房间底部游戏图标
  /// 1：显示房间底部游戏图标
  int? showRoomBottom;

  /// 显示房间底部可配置的游戏图标
  bool showConfigGame() {
    if (showRoomBottom == 1) {
      return true;
    }

    return false;
  }
  void showConfigGameR9P10oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
