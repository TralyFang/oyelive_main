import 'dart:async';


import '../../../../../generated/json/base/json_convert_content.dart';

class YBDPersonalFrameEntity with JsonConvert<YBDPersonalFrameEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDPersonalFrameRecord?>? record;
  String? recordSum;
}

class YBDPersonalFrameRecord with JsonConvert<YBDPersonalFrameRecord> {
  String? type;
  List<YBDPersonalFrameRecordItemList?>? itemList;
}

class YBDPersonalFrameRecordItemList with JsonConvert<YBDPersonalFrameRecordItemList> {
  String? image;
  int? number;
  String? img;
  String? thumbnail;
  int? personalId;
  bool? equipped;
  int? price;
  String? name;
  double? expireAfter;
  int? id;
}
