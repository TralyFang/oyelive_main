import 'dart:async';


import 'package:oyelive_main/common/room_socket/message/common/ludo_ybd_status_message_entity.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDGameResponseBase with JsonConvert<YBDGameResponseBase> {
  String? returnCode;
  String? returnMsg;
  YBDLudoStatusMessageContent? record;
}

class YBDGameResponseResult with JsonConvert<YBDGameResponseResult> {
  String? returnCode;
  String? returnMsg;
  var record;
}
