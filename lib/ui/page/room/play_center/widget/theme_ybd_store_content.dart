import 'dart:async';


import 'dart:convert' as convert;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../common/constant/const.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/sp_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../widget/loading_ybd_circle.dart';
import '../../../follow/widget/place_ybd_holder_view.dart';
import '../../../store/entity/theme_ybd_entity.dart';
import 'room_ybd_themes_list_item.dart';

///2.3 playCenter
/// 模块 room--playCenter -theme
class YBDRoomThemesContent extends StatefulWidget {
  YBDRoomThemesContent();

  @override
  _YBDRoomThemesContentState createState() => _YBDRoomThemesContentState();
}

class _YBDRoomThemesContentState extends State<YBDRoomThemesContent> with AutomaticKeepAliveClientMixin {
  /// 列表数据源
  List<YBDThemeRecord?>? _data = [];

  /// 是否为初始状态
  bool _isInitState = true;

  @override
  void initState() {
    super.initState();

    // 获取页面数据
    _getData();
  }
  void initStateV364eoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == _data || _data!.isEmpty) {
      logger.v('theme list is empty');
      return _isInitState
          ? YBDLoadingCircle()
          : YBDPlaceHolderView(
              text: 'No Data',
              textColor: Colors.black,
            );
    }

    return Container(
      height: ScreenUtil().setWidth(500),
      child: GridView.builder(
        shrinkWrap: true,
        itemCount: _data!.length,
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(10),
          vertical: ScreenUtil().setWidth(10),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: ScreenUtil().setWidth(8),
          crossAxisSpacing: ScreenUtil().setWidth(8),
          childAspectRatio: 0.72,
        ),
        itemBuilder: (context, index) {
          return YBDRoomThemesListItem(index, _data);
        },
      ),
    );
  }
  void buildYTYd2oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  /// 获取页面数据
  _getData() async {
    ApiHelper.queryDiscountList(context, 'THEME').then((discountEntity) async {
      if (discountEntity == null) {
        // 刷新数据失败
        logger.v('queryThemeDiscount failed');
      } else {
        logger.v('queryThemeDiscount SUCCESS');
        String frameJson = convert.jsonEncode(discountEntity.record);
        YBDSPUtil.save(Const.SP_KEY_DISCOUNT_THEME, frameJson);
      }
    });

    ApiHelper.queryThemeList(context, offset: 100).then((themeEntity) {
      if (themeEntity == null) {
        // 刷新数据失败
        logger.v('queryTheme failed');
      } else {
        logger.v('queryTheme SUCCESS');
        _data = themeEntity.record;
      }
      // 取消初始状态并刷新页面
      _cancelInitState();
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }
}
