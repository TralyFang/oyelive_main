import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';

import 'mic_ybd_frame_my_content.dart';
import 'mic_ybd_frame_store_content.dart';

///2.3需求：补充playCenter
///模块：边框
class YBDPlayCenterFrameContent extends StatefulWidget {
  final int? roomId;

  YBDPlayCenterFrameContent(this.roomId);

  @override
  _YBDRoomFrameState createState() => _YBDRoomFrameState();
}

class _YBDRoomFrameState extends State<YBDPlayCenterFrameContent> with AutomaticKeepAliveClientMixin {
  List<String> frameType = [translate('mine'), translate('store')];
  int curItem = 0;

  @override
  void initState() {
    super.initState();
  }
  void initStateCIlBVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _themeChoseWidget(0),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
              _themeChoseWidget(1),
            ],
          ),
          curItem == 0 ? _myFrame() : YBDRoomFrameContent(widget.roomId),
        ],
      ),
    );
  }
  void build4u9Jkoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _myFrame() {
    return Column(
      children: [
        YBDRoomMyFrameContent(
          'FRAME',
          widget.roomId,
          touchLister: _noDataCallBack,
        )
      ],
    );
  }
  void _myFramegT9EBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _themeChoseWidget(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          curItem = index;
        });
      },
      child: Material(
        child: Container(
          height: ScreenUtil().setWidth(85),
          width: ScreenUtil().setWidth(130),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: ScreenUtil().setWidth(30),
              ),
              Text(
                // 名称
                frameType[index],
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  color: curItem == index ? Colors.black : Colors.black38,
                  fontWeight: FontWeight.w400,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(
                height: ScreenUtil().setWidth(5),
              ),
              Container(
                alignment: Alignment.topCenter,
                height: ScreenUtil().setWidth(4),
                width: ScreenUtil().setWidth(24),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(5))),
                  color: curItem == index ? Color(0xff7dfaff) : Colors.transparent,
                ),
                child: Container(),
              ),
              Expanded(child: SizedBox(width: 1)),
            ],
          ),
        ),
      ),
    );
  }
  void _themeChoseWidgetcqTZGoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  noDataFunc? _noDataCallBack() {
    setState(() {
      curItem = 1;
    });
    return null;
  }

  @override
  bool get wantKeepAlive => true;
}
