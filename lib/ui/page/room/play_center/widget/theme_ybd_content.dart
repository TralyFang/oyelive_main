import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../follow/widget/place_ybd_holder_view.dart';
import 'theme_ybd_my_content.dart';
import 'theme_ybd_store_content.dart';

///2.3需求：补充playCenter
///模块：主题
class YBDThemeContent extends StatefulWidget {
  final int? roomId;

  YBDThemeContent(this.roomId);

  @override
  _YBDThemeState createState() => _YBDThemeState();
}

class _YBDThemeState extends State<YBDThemeContent> with AutomaticKeepAliveClientMixin {
  List<String> themeType = [translate('mine'), translate('store')];
  int curItem = 0;

  @override
  void initState() {
    super.initState();
  }
  void initStatejSKvXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _themeChoseWidget(0),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
              _themeChoseWidget(1),
            ],
          ),
          curItem == 0
              ? YBDRoomMyThemeContent(
                  'THEME',
                  widget.roomId,
                  touchLister: noDataCallBack,
                )
              : YBDRoomThemesContent(),
        ],
      ),
    );
  }
  void buildfxfGeoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _themeChoseWidget(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          curItem = index;
        });
      },
      child: Container(
        height: ScreenUtil().setWidth(85),
        width: ScreenUtil().setWidth(130),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: ScreenUtil().setWidth(30),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              child: Text(
                // 名称
                themeType[index],
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  color: curItem == index ? Colors.black : Colors.black38,
                  fontWeight: FontWeight.w400,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(5),
            ),
            Container(
              alignment: Alignment.topCenter,
              height: ScreenUtil().setWidth(4),
              width: ScreenUtil().setWidth(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(5))),
                color: curItem == index ? Color(0xff7dfaff) : Colors.transparent,
              ),
              child: Container(),
            ),
            Expanded(child: SizedBox(width: 1)),
          ],
        ),
      ),
    );
  }
  void _themeChoseWidgetItjqhoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  noDataFunc? noDataCallBack() {
    setState(() {
      curItem = 1;
    });
    return null;
  }

  @override
  bool get wantKeepAlive => true;
}
