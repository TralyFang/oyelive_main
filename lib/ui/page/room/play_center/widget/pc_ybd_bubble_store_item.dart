import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';

import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/constant/const.dart';
import '../../../../../common/util/image_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/sp_ybd_util.dart';
import '../../../../../module/entity/bubble_ybd_entity.dart';
import '../../../../widget/svga_ybd_icon.dart';
import '../../../store/entity/price_ybd_info_entity.dart';
import '../../../store/widget/bs_ybd_buy.dart';
import '../../../store/widget/bubble_ybd_buy_step.dart';

class YBDBubbleStoreItem extends StatefulWidget {
  YBDBubbleRecord? data;
  Function? onSuccessBuy;

  YBDBubbleStoreItem(this.data, {this.onSuccessBuy});

  @override
  YBDBubbleStoreItemState createState() => new YBDBubbleStoreItemState();
}

class YBDBubbleStoreItemState extends BaseState<YBDBubbleStoreItem> {
  bool _stint = false;
  @override
  Widget myBuild(BuildContext context) {
    if (widget.data == null) {
      return Container(
        child: Text('nodata'),
      );
    }

    String url = YBDImageUtil.frame(context, widget.data!.image, "B");
    logger.v('_getUrl imageUrl room url：$url');
    return Container(
      child: Column(
        children: [
          GestureDetector(
            onTap: () async {
              List<YBDPriceInfoEntity?>? bubbleDiscout = await YBDSPUtil.getDiscount(Const.SP_KEY_DISCOUNT_BUBBLE);

              YBDBubbleBuyStep bubbleBuyStep = new YBDBubbleBuyStep(bubbleDiscout, widget.data);
              showModalBottomSheet(
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8.0),
                    topRight: Radius.circular(8.0),
                  ),
                  side: BorderSide(style: BorderStyle.none),
                ),
                builder: (context) => YBDBuySheet(
                    buyStep: bubbleBuyStep,
                    onSuccessBuy: () {
                      widget.onSuccessBuy?.call();
                    },
                    currency: widget.data!.currency,
                    stintBuy: _stint),
              );
            },
            child: Stack(
              children: [
                Container(
                  // color: Colors.black,
                  width: ScreenUtil().setWidth(210),
                  child: _itemImg(url),
                ),
                Positioned(
                  top: 0,
                  right: ScreenUtil().setWidth(15),
                  child: YBDSvgaIcon(imgUrl: widget.data!.animation),
                ),
                YBDLevelStintTag(
                  stintType: widget.data?.conditionType,
                  stintLv: widget.data?.conditionExtends,
                  blueLock: _stint,
                  scale: 0.8,
                ),
              ],
            ),
          ),
          Container(
            height: ScreenUtil().setWidth(110),
            child: Column(
              children: [
                SizedBox(height: ScreenUtil().setWidth(10)),
                Container(
                  alignment: Alignment.centerLeft,
                  height: ScreenUtil().setWidth(40),
                  padding: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
                  child: Text(
                    // 名称
                    widget.data!.name ?? '',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xFF4F4F4F),
                      fontWeight: FontWeight.w600,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(height: ScreenUtil().setWidth(4)),
                Container(
                  alignment: Alignment.topCenter,
                  height: ScreenUtil().setWidth(38),
                  width: ScreenUtil().setWidth(200),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(19))),
                    color: Color(0xffF0F0F0),
                  ),
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(5),
                    right: ScreenUtil().setWidth(5),
                  ),
                  child: Row(
                    children: [
                      SizedBox(width: ScreenUtil().setWidth(8)),
                      Image.asset(
                        YBDCommonUtil.getCurrencyImg(widget.data!.currency),
                        width: ScreenUtil().setWidth(30),
                        height: ScreenUtil().setWidth(34),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(2),
                      ),
                      Text(
                        // 价格
                        '${widget.data!.price! * 10}',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(26),
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Expanded(child: Container()),
                      Text(
                        // 有效天数
                        '10 days',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          color: Colors.black38,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(20)),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setWidth(10)),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void myBuildRS3Kpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item图片
  Widget _itemImg(String imgUrl) {
    return Stack(
      children: [
        Center(
          child: ColorFiltered(
            colorFilter: YBDTPGlobal.getGreyFilter(_stint),
            child: YBDNetworkImage(
              // item 图片
              width: ScreenUtil().setWidth(160),
              height: ScreenUtil().setWidth(160),
              imageUrl: imgUrl,
              fit: BoxFit.contain,
            ),
          ),
        ),
      ],
    );
  }
  void _itemImgamwEloyelive(String imgUrl) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _stint = YBDCommonUtil.stintBuy(type: widget.data!.conditionType, lv: widget.data!.conditionExtends);
  }
  void initStatekuFwzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDBubbleStoreItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesoEkTSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
