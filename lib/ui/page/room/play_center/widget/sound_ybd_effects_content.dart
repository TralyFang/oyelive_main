import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../entity/access_ybd_game_bean.dart';
import 'access_ybd_game_item.dart';

///2.3需求：补充playCenter
///模块：音效
class YBDSoundEffectsContent extends StatefulWidget {
  YBDSoundEffectsContent();

  @override
  _YBDSoundEffectsContentState createState() => _YBDSoundEffectsContentState();
}

class _YBDSoundEffectsContentState extends State<YBDSoundEffectsContent> with AutomaticKeepAliveClientMixin {
  /// 列表数据源
  List<YBDAccessGameBean> _data = [
    YBDAccessGameBean(translate("laughter"), 'assets/images/icon_laughter.webp'),
    YBDAccessGameBean(translate("applaud"), 'assets/images/icon_applaud.png'),
    YBDAccessGameBean(translate("cheer"), 'assets/images/icon_cheer.webp'),
    YBDAccessGameBean(translate("kiss"), 'assets/images/icon_kiss.webp'),
    YBDAccessGameBean(translate("dj"), 'assets/images/icon_dj.webp'),
    YBDAccessGameBean(translate("uhoh"), 'assets/images/icon_uh_oh.png'),
    YBDAccessGameBean(translate("boo"), 'assets/images/icon_boo.png'),
    YBDAccessGameBean(translate("snigger"), 'assets/images/icon_snigger.webp'),
  ];

  @override
  void initState() {
    super.initState();
  }
  void initStateUCSlSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: GridView.builder(
        shrinkWrap: true,
        itemCount: _data.length,
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(32),
          vertical: ScreenUtil().setWidth(32),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4,
          mainAxisSpacing: ScreenUtil().setWidth(14),
          crossAxisSpacing: ScreenUtil().setWidth(14),
          childAspectRatio: 0.7,
        ),
        itemBuilder: (context, index) {
          return YBDAccessGameListItem(index, _data, 1);
        },
      ),
    );
  }
  void buildzbQtsoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;
}
