import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/play_ybd_util.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../entity/access_ybd_game_bean.dart';

///2.3需求：补充playCenter
///模块：音效和游戏 item
class YBDAccessGameListItem extends StatefulWidget {
  /// item 在列表中的位置
  final int curIndex;

  /// 列表数据源
  final List<YBDAccessGameBean> accessGameList;

  ///0  Game   1 Sound Effect
  final int type;

  YBDAccessGameListItem(this.curIndex, this.accessGameList, this.type);

  @override
  _YBDAccessGameItemState createState() => _YBDAccessGameItemState();
}

class _YBDAccessGameItemState extends BaseState<YBDAccessGameListItem> {
  // item 数据
  YBDAccessGameBean? _accessGameBean;

  //声音路径
  List<String> audioFiles = [
    'sound/haha.mp3',
    'sound/applause.mp3',
    'sound/cheer.mp3',
    'sound/kiss.mp3',
    'sound/dj.mp3',
    'sound/quack.mp3',
    'sound/boo.mp3',
    'sound/applause.mp3'
  ];

  @override
  void initState() {
    super.initState();
    _accessGameBean = widget.accessGameList[widget.curIndex];
  }
  void initStatea1AvFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // Play.cleanAssetsAudio();
    super.dispose();
  }
  void disposei55egoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    if (widget.accessGameList == null || _accessGameBean == null) {
      return Container();
    }

    return Container(
      child: GestureDetector(
        onTap: () {
          // Item Event
          logger.v('_accessGameBean name access :${_accessGameBean!.name} index:${widget.curIndex}');
          if (widget.type == 0) {
            if (widget.curIndex == 0) {
              YBDNavigatorHelper.openTopUpPage(context);
            } else if (widget.curIndex == 1) {
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.store);
            } else if (widget.curIndex == 2) {
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.vip);
            } else {
              YBDLogUtil.d('curIndex is invalid! type:${widget.type} curIndex:${widget.curIndex}');
            }
          } else if (widget.type == 1) {
            try {
              YBDPlayUtil.playAssetsAudio(audioFiles[widget.curIndex]);
            } catch (e) {
              YBDLogUtil.e(e);
            }
          } else {
            logger.d('type is invalid! type:${widget.type}');
          }
        },
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: ScreenUtil().setWidth(120),
              child: YBDImage(
                path: '${_accessGameBean?.icon}',
                fit: BoxFit.contain,
                width: 120,
              ),
            ),
            Container(
              height: ScreenUtil().setWidth(50),
              alignment: Alignment.center,
              child: Text(
                '${_accessGameBean?.name}',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.black, fontWeight: FontWeight.w400),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildIwM4Koyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
