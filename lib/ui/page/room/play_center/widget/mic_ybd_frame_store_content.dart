import 'dart:async';


import 'dart:convert' as convert;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../common/constant/const.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/sp_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../widget/loading_ybd_circle.dart';
import '../../../follow/widget/place_ybd_holder_view.dart';
import '../../../store/entity/mic_ybd_frame_entity.dart';
import 'room_ybd_frame_list_item.dart';

/// 2.3 需求 playCenter
/// 模块 房间 商店 Frame
class YBDRoomFrameContent extends StatefulWidget {
  final int? roomId;

  YBDRoomFrameContent(this.roomId);

  @override
  _YBDRoomFrameContentState createState() => _YBDRoomFrameContentState();
}

class _YBDRoomFrameContentState extends State<YBDRoomFrameContent> with AutomaticKeepAliveClientMixin {
  /// 列表数据源
  List<YBDMicFrameRecord?>? _data = [];

  /// 是否为初始状态
  bool _isInitState = true;

  @override
  void initState() {
    super.initState();

    // 加载页面数据
    _getData();
  }
  void initStateqIySSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == _data || _data!.isEmpty) {
      logger.v('room mic frame list is empty');
      return _isInitState
          ? YBDLoadingCircle()
          : YBDPlaceHolderView(
              text: 'No Data',
              textColor: Colors.black,
            );
    }

    return Container(
      height: ScreenUtil().setWidth(500),
      child: GridView.builder(
        shrinkWrap: true,
        itemCount: _data!.length,
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(5),
          vertical: ScreenUtil().setWidth(5),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: ScreenUtil().setWidth(10),
          crossAxisSpacing: ScreenUtil().setWidth(10),
          childAspectRatio: 0.72,
        ),
        itemBuilder: (context, index) {
          return YBDRoomFrameListItem(
            _data![index],
            roomId: widget.roomId,
          );
        },
      ),
    );
  }
  void build2huECoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  /// 加载页面数据
  _getData() async {
    ApiHelper.queryDiscountList(context, 'FRAME').then((discountEntity) async {
      if (discountEntity == null) {
        // 刷新数据失败
        logger.v('queryFrameDiscount failed');
      } else {
        logger.v('queryFrameDiscount SUCCESS');
        String frameJson = convert.jsonEncode(discountEntity.record);
        YBDSPUtil.save(Const.SP_KEY_DISCOUNT_MIC_FRAME, frameJson);
      }
    });

    ApiHelper.queryFrameList(context, offset: 100).then((micFrameEntity) {
      // 刷新数据失败
      if (micFrameEntity == null) {
        logger.v('queryFrame failed');
      } else {
        logger.v('queryFrame SUCCESS');
        _data = micFrameEntity.record;
      }

      // 取消初始状态并刷新页面
      _cancelInitState();
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }
}
