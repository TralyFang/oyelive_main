import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/int_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';
import 'package:oyelive_main/ui/widget/svga_ybd_icon.dart';
import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../../common/util/dialog_ybd_util.dart';
import '../../../../../common/util/image_ybd_util.dart';
import '../../../buy/buy_ybd_page.dart';
import '../../../buy/buy_ybd_popwindow_page.dart';
import '../../../store/entity/theme_ybd_entity.dart';
import 'package:path/path.dart' as p;

///2.3需求 playCenter 补充
/// 座驾列表 store - item
class YBDRoomThemesListItem extends StatefulWidget {
  /// item 在列表中的位置
  final int curIndex;

  /// 列表数据源
  final List<YBDThemeRecord?>? themeRecordList;

  YBDRoomThemesListItem(this.curIndex, this.themeRecordList);

  @override
  _YBDRoomThemesListItemState createState() => _YBDRoomThemesListItemState();
}

class _YBDRoomThemesListItemState extends BaseState<YBDRoomThemesListItem> {
  // item 数据
  YBDThemeRecord? _themeRecord;
  bool _stint = false;
  @override
  void initState() {
    super.initState();
    _themeRecord = widget.themeRecordList![widget.curIndex];
    _stint = YBDCommonUtil.stintBuy(type: _themeRecord!.conditionType, lv: _themeRecord!.conditionExtends);
  }
  void initStatezChxXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    if (widget.themeRecordList == null) {
      return Container();
    }

    String url;
    // if (_themeRecord.image.endsWith("gift") ||
    //     _themeRecord.image.endsWith("gif") ||
    //     _themeRecord.image.endsWith("svga")) {
    //   url = YBDImageUtil.themeTh(context, _themeRecord.thumbnail, "A");
    // } else {
    //   url = YBDImageUtil.theme(context, _themeRecord.image, "A");
    // }
    url = YBDImageUtil.themeTh(context, _themeRecord!.thumbnail, "A");

    return Container(
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              // 弹出预览弹框
              YBDDialogUtil.showBuyPreviewDialog(context, widget.themeRecordList, widget.curIndex);
              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                YBDEventName.CLICK_EVENT,
                location: YBDLocationName.ROOM_STORE_THEMES_LIST_PAGE,
                itemName: 'preview',
                value: '${p.extension(_themeRecord!.image ?? '')}',
              ));
            },
            child: Container(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(ScreenUtil().setWidth(18)),
                child: _itemImg(url, _themeRecord!.image),
              ),
            ),
          ),
          Container(
            height: ScreenUtil().setWidth(90),
            child: Column(
              children: [
                SizedBox(height: ScreenUtil().setWidth(10)),
                Container(
                  height: ScreenUtil().setWidth(35),
                  padding: EdgeInsets.only(left: ScreenUtil().setWidth(13)),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    // 名称
                    _themeRecord!.name ?? '',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xFF4F4F4F),
                      fontWeight: FontWeight.w600,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(height: ScreenUtil().setWidth(0)),
                Row(
                  children: [
                    SizedBox(width: ScreenUtil().setWidth(6)),
                    Image.asset(
                      YBDCommonUtil.getCurrencyImg(_themeRecord!.currency),
                      width: ScreenUtil().setWidth(30),
                      height: ScreenUtil().setWidth(40),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(7),
                    ),
                    Text(
                      // 价格
                      '${_themeRecord!.price! * 10}',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(26),
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Expanded(child: Container()),
                    Text(
                      // 有效天数
                      '10 days',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(20),
                        color: Colors.black38,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(20)),
                  ],
                ),
                SizedBox(height: ScreenUtil().setWidth(1)),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void myBuildUxN4Voyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item图片
  /// [url] 静态图链接
  /// [image] 原图链接
  Widget _itemImg(String url, String? image) {
    return Stack(
      children: [
        YBDNetworkImage(
          width: ScreenUtil().setWidth(210),
          height: ScreenUtil().setWidth(210),
          imageUrl: url,
          fit: BoxFit.cover,
        ),
        Positioned(
          top: ScreenUtil().setWidth(10),
          right: ScreenUtil().setWidth(10),
          child: YBDSvgaIcon(imgUrl: image),
        ),
        _stint
            ? Container(
                width: ScreenUtil().setWidth(210),
                height: ScreenUtil().setWidth(210),
                color: Colors.black.withOpacity(0.5))
            : Container(),
        Positioned(
          left: 7.px,
          child: YBDLevelStintTag(
            stintType: _themeRecord?.conditionType,
            stintLv: _themeRecord?.conditionExtends,
            lock: _stint,
            scale: 0.8,
          ),
        ),
      ],
    );
  }
  void _itemImgWSb1woyelive(String url, String? image) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
