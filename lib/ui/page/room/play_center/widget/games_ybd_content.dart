import 'dart:async';


import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/define/room_ybd_define.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';
import 'package:oyelive_main/ui/page/room/widget/jump_ybd_tpgo_notice.dart';

import '../../../../../common/constant/const.dart';
import '../../../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../../../common/util/common_ybd_util.dart';
import '../../../../../common/util/config_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../widget/loading_ybd_circle.dart';
import '../../../follow/widget/place_ybd_holder_view.dart';
import '../../mic/mic_ybd_bloc.dart';
import '../../teen_patti/entity/ybd_teen_info.dart';
import '../../util/room_ybd_util.dart';
import '../entity/game_ybd_entity.dart';
import 'game_ybd_item.dart';

///2.3需求：补充playCenter
///模块：游戏
class YBDGameContent extends StatefulWidget {
  final int? roomId;
  // final YBDMicBloc micBloc;

  YBDGameContent(this.roomId);

  @override
  _YBDGameState createState() => _YBDGameState();
}

class _YBDGameState extends State<YBDGameContent> with AutomaticKeepAliveClientMixin {
  /// 列表数据源
  List<YBDGameRecord?>? _data = [];

  /// 是否为初始状态
  bool _isInitState = true;

  var rng;

  /// 是否显示游戏
  bool _shouldShowGame = true;

  bool? _isPlayMicGame = false;
  bool? _isGotMic = false;
  String _runTimeFlag = '';

  @override
  void initState() {
    super.initState();
    // 获取页面数据
    _getData();
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      // 根据配置项判断是否显示游戏，直接由后台过滤审核版本
      // _shouldShowGame = await ConfigUtil.shouldShowGame(context: context);
      _isPlayMicGame = BlocProvider.of<YBDMicBloc>(context).state.isPlayMicGame;
      _isGotMic = BlocProvider.of<YBDMicBloc>(context).state.gotMic;
      _runTimeFlag = BlocProvider.of<YBDMicBloc>(context).state.runTimeFlag.toString();
      setState(() {});
    });
  }
  void initState1j4iKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == _data || _data!.isEmpty) {
      logger.v('YBDGameRecord list is empty');
      return _isInitState
          ? YBDLoadingCircle()
          : YBDPlaceHolderView(
              text: 'No Data',
              textColor: Colors.black,
            );
    }

    return _playCenterFrameContent();
  }
  void buildx0kDKoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 游戏列表
  Widget _playCenterFrameContent() {
    if (_shouldShowGame) {
      logger.v('===game center show game');
      return Container(
        child: GridView.builder(
          shrinkWrap: true,
          itemCount: _data!.length,
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(32),
            vertical: ScreenUtil().setWidth(32),
          ),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
            mainAxisSpacing: ScreenUtil().setWidth(14),
            crossAxisSpacing: ScreenUtil().setWidth(14),
            childAspectRatio: 0.7,
          ),
          itemBuilder: (context, index) {
            return YBDGameListItem(index, _data, (String gameName, String url) {
              Navigator.pop(context);

              _choseGame(_data?[index]?.code ?? '', url);
            });
          },
        ),
      );
    } else {
      logger.v('===game center hide game');
      return YBDPlaceHolderView(
        text: 'No Data',
        textColor: Colors.black,
      );
    }
  }
  void _playCenterFrameContentQG7Tzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  /// 获取页面数据
  _getData() async {
    YBDCommonUtil.getDefaultCountryCode(context).then((value) {
      //获取国家码

      logger.v('getDefaultCountryCode countryCode: $value');
      ApiHelper.queryGame(context, value).then((gameEntity) {
        //请求游戏数据
        if (gameEntity == null || gameEntity.returnCode != "000000") {
          // 刷新数据失败
          logger.v('queryGame failed');
        } else {
          logger.v('queryGame SUCCESS: ${gameEntity.record!.length}');
          _data = gameEntity.record;
        }
        // 取消初始状态并刷新页面
        _cancelInitState();
      });
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }

  _choseGame(String code, String url) {
    if (url != null && url.isNotEmpty && code != TPGO_CODE) {
      logger.v('H5 Game!');
      YBDRoomUtil.showH5Game(context, url);
      return;
    }

    logger.v('_choseGame gameName isPlayMicGame:$_isPlayMicGame');
    switch (code) {
      case TEENPATTI_CODE:
        if (YBDTeenInfo.getInstance().pattiInfo == null) {
          YBDRoomSocketApi.getInstance().sendTeenPatti(roomId: widget.roomId);
        }
        YBDTeenInfo.getInstance().enterType = TPEnterType.TPETGameCenter;
        YBDRoomUtil.showTeenPattiPickerView(context);
        break;
      case GuessNumber_CODE:
        break;
      case FingerCross_CODE:
        if (_isPlayMicGame == false) {
          _playGame(1, 3, Const.FINGER_CROSS_GAME_ID);
          logger.v('isPlayMicGame Finger Cross $_isPlayMicGame');
        } else {
          logger.v('isPlayMicGame Finger Cross no:$_isPlayMicGame');
        }
        break;
      case FlipCircle_CODE:
        if (_isPlayMicGame == false) {
          _playGame(1, 9, Const.FLIP_CIRCLE_GAME_ID);
        }
        break;
      case FlipCoin_CODE:
        if (_isPlayMicGame == false) {
          _playGame(0, 1, Const.FLIP_COIN_GAME_ID);
        }
        break;
      case Dice_CODE:
        if (_isPlayMicGame == false) {
          _playGame(1, 6, Const.DICE_GAME_ID);
        }
        break;
      case TPGO_CODE:
        if (widget.roomId == YBDUserUtil.getUserIdSync) {
          YBDToastUtil.toast("Only audience can jump to TP GO");
          return;
        }
        showDialog<Null>(
            context: context, //BuildContext对象
            barrierDismissible: false,
            builder: (BuildContext context) {
              return YBDJumpTpgoNoticeDialog();
            });
        break;
    }
  }

  _playGame(int min, int max, int gameId) {
    logger.v("_sentGameMsg gotMic $_isGotMic");
    if (!_isGotMic!) {
      YBDToastUtil.toast('Take a Mic first and play the Game');
      return;
    }
    String number = getRandomNumber(min, max).toString();
    logger.v("_sentGameMsg number $number");
    String content = Const.MINI_GAMES_PATTERN +
        Const.MINI_GAMES_SPLITTER +
        gameId.toString() +
        Const.MINI_GAMES_SPLITTER +
        _runTimeFlag +
        Const.MINI_GAMES_SPLITTER +
        number;
    _sentGameMsg(content);
  }

  _sentGameMsg(String content) {
    logger.v("_sentGameMsg content: $content");
    YBDRoomSocketApi.getInstance().sendPublicMessage(roomId: widget.roomId, content: content);
  }

  int? getRandomNumber(int min, int max) {
    if (rng == null) {
      rng = new Random(); //随机数生成类
    }
    return rng.nextInt(max - min + 1) + min;
  }
}
