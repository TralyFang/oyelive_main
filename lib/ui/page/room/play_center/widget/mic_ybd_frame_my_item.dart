import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/widget/svga_ybd_icon.dart';

import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/util/common_ybd_util.dart';
import '../../../../../common/util/date_ybd_util.dart';
import '../../../../../common/util/image_ybd_util.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../../module/entity/discount_ybd_info_entity.dart';
import '../entity/personal_ybd_frame_entity.dart';
import 'package:path/path.dart' as p;

/// 2.3 需求 playCenter
/// 模块 房间 背包 Frame - item
class YBDFrameMyListItem extends StatefulWidget {
  final YBDPersonalFrameRecordItemList? personalFrame;

  final String? goodsType;

  int? roomId;

  final VoidCallback successCallback;

  YBDFrameMyListItem(this.personalFrame, this.successCallback, {this.goodsType, this.roomId});

  @override
  _YBDFrameMyListItemState createState() => _YBDFrameMyListItemState();
}

class _YBDFrameMyListItemState extends BaseState<YBDFrameMyListItem> {
  String? imageUrl;
  String? expireTime;

  /// 是否为初始状态
  bool _isInitState = true;

  @override
  void initState() {
    super.initState();
    /* WidgetsBinding.instance?.addPostFrameCallback((_) {
      _getUrl();
    });*/
  }
  void initStatejesNooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _getUrl() async {
    if (widget.goodsType == "FRAME") {
      //FRAME 缩略图
      if (YBDCommonUtil.getFileType(widget.personalFrame!.image) != 1) {
        imageUrl = YBDImageUtil.frame(context, widget.personalFrame!.thumbnail, "B");
      } else {
        imageUrl = YBDImageUtil.frame(context, widget.personalFrame!.image, "B");
      }
    } else {
      //Theme 缩略图
      imageUrl = YBDImageUtil.themeTh(context, widget.personalFrame!.thumbnail, "A");
    }
    expireTime = YBDDateUtil.getExpireAfter(widget.personalFrame!.expireAfter!);
  }
  void _getUrlkvIvnoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    if (widget.personalFrame == null) {
      return Container();
    }

    _getUrl();

    // 装配按钮的边距
    double equipPadding = 10;

    return Container(
//      width: ScreenUtil().setWidth(350),
//       decoration: _containerDecoration(),
      color: Colors.white,
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              // 弹出底部购买弹框
              logger.v('------_apply()');
              _apply(!widget.personalFrame!.equipped!);
              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                YBDEventName.CLICK_EVENT,
                location: YBDLocationName.ROOM_PERSONAL_MIC_LIST_PAGE,
                itemName: 'preview',
                value: '${p.extension(widget.personalFrame!.image ?? '')}',
              ));
            },
            child: Stack(
              children: [
                Container(
                  // item 图片
                  width: ScreenUtil().setWidth(210),
                  child: Center(
                    child: ClipRRect(
                      // 房间图像
                      borderRadius: BorderRadius.circular(ScreenUtil().setWidth(18)),
                      child: _itemImg(),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: ScreenUtil().setWidth(equipPadding),
                  child: _equipIcon(widget.personalFrame!.equipped),
                ),
                Positioned(
                  top: 0,
                  right: ScreenUtil().setWidth(equipPadding),
                  child: YBDSvgaIcon(imgUrl: widget.personalFrame!.image),
                ),
              ],
            ),
          ),
          Container(
            // 图片底部部分
            decoration: BoxDecoration(
              color: Color(0x33FFFFFF),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
              ),
            ),
            child: Column(
              children: [
                SizedBox(height: ScreenUtil().setWidth(1)),
                Container(
                  alignment: Alignment.center,
                  height: ScreenUtil().setWidth(40),
                  child: Text(
                    // 名称
                    widget.personalFrame!.name ?? '',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xFF4F4F4F),
                      fontWeight: FontWeight.w600,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(height: ScreenUtil().setWidth(10)),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    // 有效时间
                    '$expireTime',
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(20),
                      color: Colors.black26,
                      fontWeight: FontWeight.w400,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(height: ScreenUtil().setWidth(5)),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void myBuildVFoY9oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///装备、取消装备
  void _apply(bool equip) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8.0),
          topRight: Radius.circular(8.0),
        ),
        side: BorderSide(style: BorderStyle.none),
      ),
      builder: (context) => GestureDetector(
        onTap: () async {
          YBDDiscountInfoEntity? discountInfoEntity;
          if (equip) {
            showLockDialog(info: "Apply");
            discountInfoEntity = await ApiHelper.equip(
              context,
              widget.personalFrame!.personalId,
              widget.roomId,
              widget.goodsType,
            );
          } else {
            showLockDialog(info: "Remove");
            discountInfoEntity = await ApiHelper.unEquip(
              context,
              widget.personalFrame!.personalId,
              widget.roomId,
              widget.goodsType,
            );
          }
          dismissLockDialog();
          if (discountInfoEntity!.returnCode == "000000") {
            YBDToastUtil.toast(translate('success'));
            widget.successCallback();
          } else {
            YBDToastUtil.toast(translate('failed'));
          }
          Navigator.pop(context);
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.ROOM_PERSONAL_MIC_LIST_PAGE,
            itemName: '${equip ? 'apply' : 'remove'}',
            value: '${p.extension(widget.personalFrame!.image ?? '')}',
          ));
        },
        child: Container(
          height: ScreenUtil().setWidth(200),
          child: Column(
            children: [
              Expanded(
                child: Container(),
              ),
              Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(500),
                height: ScreenUtil().setWidth(75),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  gradient: LinearGradient(
                    colors: [
                      Color(0xff47CDCC),
                      Color(0xff5E94E7),
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                ),
                child: Text(
                  equip ? 'Apply' : 'Remove',
                  style: TextStyle(fontSize: ScreenUtil().setSp(32), color: Colors.white, fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(100),
              )
            ],
          ),
        ),
      ),
    );
  }
  void _applyv2YKPoyelive(bool equip) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// equip图标
  Widget _equipIcon(bool? equipped) {
    if (equipped ?? false) {
      return Container(
        height: ScreenUtil().setWidth(26),
        width: ScreenUtil().setWidth(26),
        child: Image.asset(
          'assets/images/select_theme.png',
          fit: BoxFit.cover,
        ),
      );
    }

    return SizedBox();
  }
  void _equipIconUpQZ2oyelive(bool? equipped) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item图片
  Widget _itemImg() {
    return YBDNetworkImage(
      width: ScreenUtil().setWidth(160),
      height: ScreenUtil().setWidth(160),
      imageUrl: imageUrl ?? '',
      fit: BoxFit.cover,
    );
  }
}
