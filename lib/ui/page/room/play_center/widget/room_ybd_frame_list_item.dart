import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';
import 'package:oyelive_main/ui/widget/svga_ybd_icon.dart';
import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/util/image_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../buy/buy_ybd_page.dart';
import '../../../store/entity/mic_ybd_frame_entity.dart';
import 'package:path/path.dart' as p;

///2.3需求 playCenter 补充
/// 边框列表 store - item
class YBDRoomFrameListItem extends StatefulWidget {
  final YBDMicFrameRecord? micFrameRecord;
  final int? roomId;

  YBDRoomFrameListItem(this.micFrameRecord, {this.roomId});

  @override
  _YBDRoomFrameListItemState createState() => _YBDRoomFrameListItemState();
}

class _YBDRoomFrameListItemState extends BaseState<YBDRoomFrameListItem> {
  bool _stint = false;
  @override
  void initState() {
    super.initState();
    logger.v('YBDRoomFrameListItem roomId:$widget.roomId');
    _stint = YBDCommonUtil.stintBuy(
      type: widget.micFrameRecord!.conditionType,
      lv: widget.micFrameRecord!.conditionExtends,
    );
  }
  void initStatewwLotoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    if (widget.micFrameRecord == null) {
      return Container(
        child: Text('nodata'),
      );
    }

    String url = YBDImageUtil.frame(context, widget.micFrameRecord!.image, "B");
    logger.v('_getUrl imageUrl room url：$url');
    return Container(
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              showModalBottomSheet(
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8.0),
                    topRight: Radius.circular(8.0),
                  ),
                  side: BorderSide(style: BorderStyle.none),
                ),
                builder: (context) => YBDBuyBottomWindowPage(
                  BuyType.frame,
                  null,
                  widget.micFrameRecord,
                  null,
                  null,
                  roomId: widget.roomId,
                  source: 1,
                  currency: widget.micFrameRecord!.currency,
                  stintBuy: _stint,
                ),
              );
              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                YBDEventName.CLICK_EVENT,
                location: YBDLocationName.ROOM_STORE_MIC_LIST_PAGE,
                itemName: 'preview',
                value: '${p.extension(widget.micFrameRecord!.image ?? '')}',
              ));
            },
            child: Stack(
              children: [
                Container(
                  width: ScreenUtil().setWidth(210),
                  child: _itemImg(url),
                ),
                Positioned(
                  top: 0,
                  right: ScreenUtil().setWidth(15),
                  child: YBDSvgaIcon(imgUrl: widget.micFrameRecord!.animationFile),
                ),
                YBDLevelStintTag(
                  stintType: widget.micFrameRecord?.conditionType,
                  stintLv: widget.micFrameRecord?.conditionExtends,
                  blueLock: _stint,
                  scale: 0.8,
                ),
              ],
            ),
          ),
          Container(
            height: ScreenUtil().setWidth(110),
            child: Column(
              children: [
                SizedBox(height: ScreenUtil().setWidth(10)),
                Container(
                  alignment: Alignment.centerLeft,
                  height: ScreenUtil().setWidth(40),
                  padding: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
                  child: Text(
                    // 名称
                    widget.micFrameRecord!.name ?? '',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xFF4F4F4F),
                      fontWeight: FontWeight.w600,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(height: ScreenUtil().setWidth(4)),
                Container(
                  alignment: Alignment.topCenter,
                  height: ScreenUtil().setWidth(38),
                  width: ScreenUtil().setWidth(200),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(19))),
                    color: Color(0xffF0F0F0),
                  ),
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(5),
                    right: ScreenUtil().setWidth(5),
                  ),
                  child: Row(
                    children: [
                      SizedBox(width: ScreenUtil().setWidth(8)),
                      Image.asset(
                        YBDCommonUtil.getCurrencyImg(widget.micFrameRecord!.currency),
                        width: ScreenUtil().setWidth(30),
                        height: ScreenUtil().setWidth(34),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(2),
                      ),
                      Text(
                        // 价格
                        '${widget.micFrameRecord!.price! * 10}',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(26),
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Expanded(child: Container()),
                      Text(
                        // 有效天数
                        '10 days',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          color: Colors.black38,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(20)),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setWidth(10)),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void myBuildrSTqQoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item图片
  Widget _itemImg(String imgUrl) {
    return Stack(
      children: [
        Center(
          child: ColorFiltered(
            colorFilter: YBDTPGlobal.getGreyFilter(_stint),
            child: YBDNetworkImage(
              // item 图片
              width: ScreenUtil().setWidth(160),
              height: ScreenUtil().setWidth(160),
              imageUrl: imgUrl,
              fit: BoxFit.contain,
            ),
          ),
        ),
      ],
    );
  }
  void _itemImgsCm5Aoyelive(String imgUrl) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
