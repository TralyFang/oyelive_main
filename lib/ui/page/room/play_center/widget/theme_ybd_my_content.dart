import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../../module/user/util/user_ybd_util.dart';
import '../../../../widget/loading_ybd_circle.dart';
import '../../../follow/widget/place_ybd_holder_view.dart';
import '../entity/personal_ybd_frame_entity.dart';
import 'theme_ybd_my_item.dart';

///2.3需求：补充playCenter
///模块：主题 - 背包
class YBDRoomMyThemeContent extends StatefulWidget {
  final String goodsType;
  final int? roomId;
  final noDataFunc? touchLister;

  YBDRoomMyThemeContent(this.goodsType, this.roomId, {this.touchLister});

  @override
  _YBDRoomMyFrameState createState() => _YBDRoomMyFrameState();
}

class _YBDRoomMyFrameState extends State<YBDRoomMyThemeContent> with AutomaticKeepAliveClientMixin {
  /// 列表数据源
  List<YBDPersonalFrameRecordItemList?>? _data = [];

  /// 是否为初始状态
  bool _isInitState = true;

  YBDUserInfo? _userInfo;

  @override
  void initState() {
    super.initState();

    // 加载页面数据
    _getData();
  }
  void initStateQ4JOyoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == _data || _data!.isEmpty) {
      logger.v('room mic frame list is empty');
      return _isInitState
          ? YBDLoadingCircle()
          : Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setWidth(40),
              ),
              child: YBDPlaceHolderView(
                img: 'assets/images/empty/no_item_baggage.webp',
                text: translate('no_item_to_store'),
                textColor: Color(0xff333333),
                touchLister: widget.touchLister,
              ),
            );
    }

    return Container(
      height: ScreenUtil().setWidth(500),
      child: GridView.builder(
        shrinkWrap: true,
        itemCount: _data!.length,
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(5),
          vertical: ScreenUtil().setWidth(5),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: ScreenUtil().setWidth(10),
          crossAxisSpacing: ScreenUtil().setWidth(10),
          childAspectRatio: 0.72,
        ),
        itemBuilder: (context, index) {
//          return Text('4556');
          return YBDThemeMyListItem(
            _data![index],
            () {
              _getData();
            },
            goodsType: widget.goodsType,
            roomId: widget.roomId,
          );
        },
      ),
    );
  }
  void buildAUkAPoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  /// 加载页面数据
  _getData() async {
    _userInfo = await YBDUserUtil.userInfo();
    if (_userInfo == null) {
      // 取消初始状态并刷新页面
      _cancelInitState();
      return;
    }
    ApiHelper.queryPersonalFrame(context, _userInfo!.id, widget.goodsType).then((personalFrameEntity) {
      // 刷新数据失败
      if (personalFrameEntity == null || personalFrameEntity.returnCode != "000000") {
        logger.v('queryMyFrame failed ${personalFrameEntity?.returnCode ?? 'personalFrameEntity==null'}');
      } else {
        logger.v('queryMyFrame SUCCESS');
        if (personalFrameEntity.record != null) {
          for (int i = 0; i < personalFrameEntity.record!.length; i++) {
            if (personalFrameEntity.record![i]!.type == "FRAME" || personalFrameEntity.record![i]!.type == "THEME") {
              _data = personalFrameEntity.record![i]!.itemList;
              sortMyFrame();
              break;
            }
          }
        }
      }
      // 取消初始状态并刷新页面
      _cancelInitState();
    });
  }

  ///将list里的选中的装备排列在list第一个位置
  sortMyFrame() {
    if (null != _data && _data!.length > 0) {
      for (int index = 0, length = _data!.length; index < length; index++) {
        if (_data![index]!.equipped!) {
          YBDPersonalFrameRecordItemList? temp = _data![0];
          _data![0] = _data![index];
          _data![index] = temp;
          break;
        }
      }
    }
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }
}
