import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/module/entity/result_ybd_bean.dart';

import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/constant/const.dart';
import '../../../../../common/util/date_ybd_util.dart';
import '../../../../../common/util/image_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../widget/svga_ybd_icon.dart';
import '../../../baggage/entity/baggage_ybd_bubble_entity.dart';

class YBDBubbleMineItem extends StatefulWidget {
  YBDBaggageBubbleRecordItemList? data;

  Function? onSuccess;

  YBDBubbleMineItem(this.data, {this.onSuccess});

  @override
  YBDBubbleMineItemState createState() => new YBDBubbleMineItemState();
}

class YBDBubbleMineItemState extends BaseState<YBDBubbleMineItem> {
  @override
  Widget myBuild(BuildContext context) {
    if (widget.data == null) {
      return Container();
    }

    // _getUrl();

    // 装配按钮的边距
    double equipPadding = 10;

    return Container(
//      width: ScreenUtil().setWidth(350),
//       decoration: _containerDecoration(),
      color: Colors.white,
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              // 弹出底部购买弹框
              logger.v('------_apply()');
              _apply(!widget.data!.equipped!);
            },
            child: Stack(
              children: [
                Container(
                  // item 图片
                  width: ScreenUtil().setWidth(210),
                  child: Center(
                    child: ClipRRect(
                      // 房间图像
                      borderRadius: BorderRadius.circular(ScreenUtil().setWidth(18)),
                      child: _itemImg(),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: ScreenUtil().setWidth(equipPadding),
                  child: _equipIcon(widget.data!.equipped),
                ),
                Positioned(
                  top: 0,
                  right: ScreenUtil().setWidth(equipPadding),
                  child: YBDSvgaIcon(imgUrl: widget.data!.image),
                ),
              ],
            ),
          ),
          Container(
            // 图片底部部分
            decoration: BoxDecoration(
              color: Color(0x33FFFFFF),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
              ),
            ),
            child: Column(
              children: [
                SizedBox(height: ScreenUtil().setWidth(1)),
                Container(
                  alignment: Alignment.center,
                  height: ScreenUtil().setWidth(40),
                  child: Text(
                    // 名称
                    widget.data!.name ?? '',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xFF4F4F4F),
                      fontWeight: FontWeight.w600,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(height: ScreenUtil().setWidth(10)),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    // 有效时间
                    '${YBDDateUtil.getExpireAfter(widget.data!.expireAfter!.toDouble())}',
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(20),
                      color: Colors.black26,
                      fontWeight: FontWeight.w400,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(height: ScreenUtil().setWidth(5)),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void myBuildVXzuRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<bool> equipBubble() async {
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.equipItem(context, personalId: widget.data!.personalId, goodsType: "BUBBLE");
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      return true;
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
      return false;
    }
  }
  void equipBubbleDQAVDoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<bool> unequipBubble() async {
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.unequipItem(context, personalId: widget.data!.personalId, goodsType: "BUBBLE");
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      return true;
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
      return false;
    }
  }
  void unequipBubbleH7PDloyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///装备、取消装备
  void _apply(bool equip) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8.0),
          topRight: Radius.circular(8.0),
        ),
        side: BorderSide(style: BorderStyle.none),
      ),
      builder: (context) => GestureDetector(
        onTap: () async {
          bool isOk;
          if (equip) {
            showLockDialog(info: "Apply");
            isOk = await equipBubble();
          } else {
            showLockDialog(info: "Remove");
            isOk = await unequipBubble();
          }
          dismissLockDialog();

          Navigator.pop(context);
          if (isOk) widget.onSuccess?.call();
        },
        child: Container(
          height: ScreenUtil().setWidth(200),
          child: Column(
            children: [
              Expanded(
                child: Container(),
              ),
              Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(500),
                height: ScreenUtil().setWidth(75),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  gradient: LinearGradient(
                    colors: [
                      Color(0xff47CDCC),
                      Color(0xff5E94E7),
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                ),
                child: Text(
                  equip ? 'Apply' : 'Remove',
                  style: TextStyle(fontSize: ScreenUtil().setSp(32), color: Colors.white, fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(100),
              )
            ],
          ),
        ),
      ),
    );
  }
  void _apply1aXgloyelive(bool equip) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// equip图标
  Widget _equipIcon(bool? equipped) {
    if (equipped ?? false) {
      return Container(
        height: ScreenUtil().setWidth(26),
        width: ScreenUtil().setWidth(26),
        child: Image.asset(
          'assets/images/select_theme.png',
          fit: BoxFit.cover,
        ),
      );
    }

    return SizedBox();
  }
  void _equipIconCzC72oyelive(bool? equipped) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item图片
  Widget _itemImg() {
    return YBDNetworkImage(
      width: ScreenUtil().setWidth(160),
      height: ScreenUtil().setWidth(160),
      imageUrl: YBDImageUtil.frame(context, widget.data!.thumbnail, "B"),
      fit: BoxFit.contain,
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDBubbleMineItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetyE6I9oyelive(YBDBubbleMineItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
