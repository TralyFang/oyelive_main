import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/page/room/play_center/widget/pc_ybd_bubble_store_item.dart';
import 'dart:convert' as convert;

import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/constant/const.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/sp_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../../module/entity/bubble_ybd_entity.dart';
import '../../../../widget/loading_ybd_circle.dart';
import '../../../follow/widget/place_ybd_holder_view.dart';

class YBDPcBubbleStore extends StatefulWidget {
  @override
  YBDPcBubbleStoreState createState() => new YBDPcBubbleStoreState();
}

class YBDPcBubbleStoreState extends BaseState<YBDPcBubbleStore> {
  /// 列表数据源
  List<YBDBubbleRecord?>? _data = [];

  /// 是否为初始状态
  bool _isInitState = true;

  @override
  void initState() {
    super.initState();

    // 加载页面数据
    _getData();
  }
  void initStateO65vhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == _data || _data!.isEmpty) {
      logger.v('bubble  list is empty');
      return _isInitState
          ? YBDLoadingCircle()
          : YBDPlaceHolderView(
              text: 'No Data',
              textColor: Colors.black,
            );
    }

    return Container(
      height: ScreenUtil().setWidth(500),
      child: GridView.builder(
        shrinkWrap: true,
        itemCount: _data!.length,
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(5),
          vertical: ScreenUtil().setWidth(5),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: ScreenUtil().setWidth(10),
          crossAxisSpacing: ScreenUtil().setWidth(10),
          childAspectRatio: 0.72,
        ),
        itemBuilder: (context, index) {
          return YBDBubbleStoreItem(
            _data![index],
          );
        },
      ),
    );
  }
  void build7fou2oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  /// 加载页面数据
  _getData() async {
    ApiHelper.queryDiscountList(context, 'BUBBLE').then((discountEntity) async {
      if (discountEntity == null) {
        // 刷新数据失败
        logger.v('queryFrameDiscount failed');
      } else {
        logger.v('queryFrameDiscount SUCCESS');
        String bubbleJson = convert.jsonEncode(discountEntity.record);
        YBDSPUtil.save(Const.SP_KEY_DISCOUNT_BUBBLE, bubbleJson);
      }
    });

    ApiHelper.queryBubbleList(context).then((bubbleEntity) {
      // 刷新数据失败
      if (bubbleEntity == null) {
        logger.v('querybubble failed');
      } else {
        logger.v('queryBubble SUCCESS');
        _data = bubbleEntity.record;
      }

      // 取消初始状态并刷新页面
      _cancelInitState();
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Container();
  }
  void myBuildIR0dIoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
