import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_live_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../entity/game_ybd_entity.dart';

typedef GameCallback = Function(String, String);

///2.3需求：补充playCenter
///模块：Game item
class YBDGameListItem extends StatefulWidget {
  /// item 在列表中的位置
  final int curIndex;

  /// 列表数据源
  final List<YBDGameRecord?>? gameList;

  final GameCallback gameCallback;

  YBDGameListItem(this.curIndex, this.gameList, this.gameCallback);

  @override
  _YBDGameItemState createState() => _YBDGameItemState();
}

class _YBDGameItemState extends BaseState<YBDGameListItem> {
  // item 数据
  YBDGameRecord? _gameBean;

  @override
  void initState() {
    super.initState();
    _gameBean = widget.gameList![widget.curIndex];
    logger.v('YBDGameListItem name:${_gameBean!.name} index:${widget.curIndex} icon:${_gameBean?.icon}');
  }
  void initState1DHvroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    if (widget.gameList == null || _gameBean == null) {
      return Container();
    }

    return Container(
      child: GestureDetector(
        onTap: () {
          logger.v('_accessGameBean name game item:${_gameBean!.name} index:${widget.curIndex}');
          YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_game_click, record: _gameBean);
          // 点击事件埋点
          if (YBDTAClickName.gameNameMap.containsKey(_gameBean!.name)) {
            YBDCommonTrack().commonTrack(YBDTAProps(
              location: YBDTAClickName.gameNameMap[_gameBean!.name!],
              module: YBDTAModule.liveroom,
            ));
          }
          widget.gameCallback(_gameBean!.name ?? '', _gameBean!.url ?? '');
          if (_gameBean?.name == "Ludo") {
            YBDRoomUtil.ludoRedDoted();
          }
        },
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(120),
              height: ScreenUtil().setWidth(120),
              /* decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
//                border: Border.all(color: Colors.black12.withOpacity(0.1), width: ScreenUtil().setWidth(4)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey,
                      offset: Offset(-3.0, 3.0), //阴影x轴偏移量
                      blurRadius: 3, //阴影模糊程度
                      spreadRadius: 2 //阴影扩散程度
                      )
                ],
              ),*/
              child: Stack(
                children: [
                  Container(
                    alignment: Alignment.center,
                    width: ScreenUtil().setWidth(120),
                    height: ScreenUtil().setWidth(120),
                    // decoration: BoxDecoration(
                    //   borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
                    //   color: Colors.white,
                    // ),
                    child: YBDNetworkImage(
                      width: ScreenUtil().setWidth(80),
//                height: ScreenUtil().setWidth(micWidth),
                      imageUrl: '${_gameBean?.icon}',
                      fit: BoxFit.contain,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(15),
            ),
            Container(
              height: ScreenUtil().setWidth(50),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Builder(builder: (context) {
                    if (_gameBean?.name == "Ludo") {
                      return YBDRoomUtil.ludoRedDot(isDot: true, space: true);
                    }
                    return Container();
                  }),
                  Text(
                    '${_gameBean?.name}',
                    overflow: TextOverflow.ellipsis,
                    style:
                        TextStyle(fontSize: ScreenUtil().setSp(20), color: Colors.black, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildfQzJ1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
