import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/room/play_center/widget/pc_ybd_bubble_mine_item.dart';

import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../../module/user/util/user_ybd_util.dart';
import '../../../../widget/loading_ybd_circle.dart';
import '../../../baggage/entity/baggage_ybd_bubble_entity.dart';
import '../../../follow/widget/place_ybd_holder_view.dart';

class YBDPcBubbleMine extends StatefulWidget {
  final noDataFunc touchLister;

  YBDPcBubbleMine(this.touchLister);

  @override
  YBDPcBubbleMineState createState() => new YBDPcBubbleMineState();
}

class YBDPcBubbleMineState extends BaseState<YBDPcBubbleMine> with AutomaticKeepAliveClientMixin {
  /// 列表数据源
  List<YBDBaggageBubbleRecordItemList?>? _data = [];

  /// 是否为初始状态
  bool _isInitState = true;

  YBDUserInfo? _userInfo;

  @override
  void initState() {
    super.initState();

    // 加载页面数据
    _getData();
  }
  void initStateo7aSBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 加载页面数据
  _getData() async {
    _userInfo = await YBDUserUtil.userInfo();
    if (_userInfo == null) {
      // 取消初始状态并刷新页面
      _cancelInitState();
      return;
    }
    ApiHelper.queryBaggageBubbleList(context).then((personalBubbleEntity) {
      // 刷新数据失败
      if (personalBubbleEntity == null || personalBubbleEntity.returnCode != "000000") {
        logger.v('queryBubble failed ${personalBubbleEntity?.returnCode ?? 'personalBubbleEntity==null'}');
      } else {
        logger.v('queryBubble SUCCESS');

        if (personalBubbleEntity.record != null && personalBubbleEntity.record!.length != 0) {
          _data = personalBubbleEntity.record?.first?.itemList;
        }
      }
      // 取消初始状态并刷新页面
      _cancelInitState();
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == _data || _data!.isEmpty) {
      logger.v(' bubble list is empty');
      return _isInitState
          ? YBDLoadingCircle()
          : Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setWidth(40),
              ),
              child: YBDPlaceHolderView(
                img: 'assets/images/empty/no_item_baggage.webp',
                text: translate('no_item_to_store'),
                textColor: Color(0xff333333),
                touchLister: widget.touchLister,
              ),
            );
    }

    return Container(
      height: ScreenUtil().setWidth(500),
      child: GridView.builder(
        shrinkWrap: true,
        itemCount: _data!.length,
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(5),
          vertical: ScreenUtil().setWidth(5),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: ScreenUtil().setWidth(10),
          crossAxisSpacing: ScreenUtil().setWidth(10),
          childAspectRatio: 0.9,
        ),
        itemBuilder: (context, index) {
//          return Text('4556');
          return YBDBubbleMineItem(
            _data![index],
            onSuccess: () {
              _getData();
            },
          );
        },
      ),
    );
  }
  void buildB590soyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(''),
      ),
    );
  }
  void myBuildJrwY4oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDPcBubbleMine oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidget7fFlYoyelive(YBDPcBubbleMine oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
