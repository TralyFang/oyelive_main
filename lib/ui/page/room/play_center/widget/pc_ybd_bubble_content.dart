import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';
import 'package:oyelive_main/ui/page/room/play_center/widget/pc_ybd_bubble_mine.dart';
import 'package:oyelive_main/ui/page/room/play_center/widget/pc_ybd_bubble_store.dart';

///2.3需求：补充playCenter
///模块：边框
class YBDPlayCenterBubbleContent extends StatefulWidget {
  final int? roomId;

  YBDPlayCenterBubbleContent(this.roomId);

  @override
  _YBDRoomBubbleState createState() => _YBDRoomBubbleState();
}

class _YBDRoomBubbleState extends State<YBDPlayCenterBubbleContent> with AutomaticKeepAliveClientMixin {
  List<String> BubbleType = [translate('mine'), translate('store')];
  int curItem = 0;

  @override
  void initState() {
    super.initState();
  }
  void initState9q8b9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _themeChoseWidget(0),
              SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
              _themeChoseWidget(1),
            ],
          ),
          curItem == 0 ? YBDPcBubbleMine(_noDataCallBack) : YBDPcBubbleStore(),
        ],
      ),
    );
  }
  void buildlZ2cJoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _myBubble() {
    return Column(
      children: [
        // RoomMyBubbleContent(
        //   'Bubble',
        //   widget.roomId,
        //   touchLister: _noDataCallBack,
        // )
      ],
    );
  }
  void _myBubbletWH5Noyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _themeChoseWidget(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          curItem = index;
        });
      },
      child: Material(
        child: Container(
          height: ScreenUtil().setWidth(85),
          width: ScreenUtil().setWidth(130),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: ScreenUtil().setWidth(30),
              ),
              Text(
                // 名称
                BubbleType[index],
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  color: curItem == index ? Colors.black : Colors.black38,
                  fontWeight: FontWeight.w400,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(
                height: ScreenUtil().setWidth(5),
              ),
              Container(
                alignment: Alignment.topCenter,
                height: ScreenUtil().setWidth(4),
                width: ScreenUtil().setWidth(24),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(5))),
                  color: curItem == index ? Color(0xff7dfaff) : Colors.transparent,
                ),
                child: Container(),
              ),
              Expanded(child: SizedBox(width: 1)),
            ],
          ),
        ),
      ),
    );
  }
  void _themeChoseWidgetBz6CToyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  noDataFunc? _noDataCallBack() {
    setState(() {
      curItem = 1;
    });
    return null;
  }

  @override
  bool get wantKeepAlive => true;
}
