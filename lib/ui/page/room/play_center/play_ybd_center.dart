import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/room/play_center/widget/pc_ybd_bubble_content.dart';
import 'package:oyelive_main/ui/page/room/play_center/widget/quick_ybd_access_game_content.dart';
import 'package:oyelive_main/ui/page/room/play_center/widget/sound_ybd_effects_content.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/config_ybd_util.dart';
import 'widget/games_ybd_content.dart';
import 'widget/mic_ybd_frame_content.dart';
import 'widget/theme_ybd_content.dart';

///2.3需求：补充playCenter
///模块：主入口
class YBDPlayCenterPage extends StatefulWidget {
  final int? roomId;
  final int? defaultTab;

  ///0--Game  1--Theme  其它--Frame
  // final YBDMicBloc micBloc;

  YBDPlayCenterPage(
    this.roomId, {
    this.defaultTab = 1,
    // this.micBloc,
  });

  @override
  _YBDPlayCenterState createState() => _YBDPlayCenterState();
}

class _YBDPlayCenterState extends BaseState<YBDPlayCenterPage> with SingleTickerProviderStateMixin {
  TabController? _tabController;

  /// 是否显示游戏
  bool _shouldShowGame = false;

  @override
  void initState() {
    super.initState();
    print("YBDPlayCenterPage initState");

    // 一共5个 tab 标签
    _tabController = TabController(length: 6, vsync: this);
    _tabController!.animateTo(widget.defaultTab != null ? widget.defaultTab! : 1);

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      // 根据配置项判断是否显示游戏
      _shouldShowGame = await ConfigUtil.shouldShowGame(context: context);
      setState(() {});
    });
  }
  void initStateFKdWHoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    _tabController!.dispose();
  }
  void disposedgalroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(700),
      padding: EdgeInsets.only(
        left: ScreenUtil().setWidth(15),
        right: ScreenUtil().setWidth(15),
      ),
      decoration: new BoxDecoration(
        //背景Colors.transparent 透明
        color: Colors.white,
        //设置四周圆角 角度
        borderRadius: BorderRadius.only(topLeft: Radius.circular(4.0), topRight: Radius.circular(4.0)),
      ),
      child: Column(
        children: [
          // tab 和导航栏的间距
//          SizedBox(height: ScreenUtil().setWidth(10)),
          // tab 标签
          _topTabView(),
          rowDivider(),
          Expanded(
            // tab 内容页面
            child: TabBarView(
              controller: _tabController,
              children: <Widget>[
                // widget.defaultTab == 0
                //     ? YBDGameContent(
                //         widget.roomId,
                //         micBloc: widget.micBloc,
                //       )
                //     : widget.defaultTab == 1
                //         ? YBDThemeContent(widget.roomId)
                //         : _playCenterFrameContent(),
                YBDQuickAccessGameContent(),
                YBDGameContent(
                  widget.roomId,
                  // micBloc: widget.micBloc,
                ),
                YBDThemeContent(widget.roomId),
                YBDPlayCenterFrameContent(widget.roomId),
                YBDPlayCenterBubbleContent(widget.roomId),

                YBDSoundEffectsContent(),

                /* YBDGameContent(),
                  YBDThemeContent(),
                  YBDPlayCenterFrameContent(),
                  YBDSoundEffectsContent(),*/
              ],
            ),
          ),
        ],
      ),
    );
  }
  void myBuildQw9T3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 游戏列表
  /*Widget _playCenterFrameContent() {
    if (_shouldShowGame) {
      logger.v('===play center show game');
      return YBDPlayCenterFrameContent(widget.roomId);
    } else {
      logger.v('===play center hide game');
      return YBDPlaceHolderView(
        text: 'No Data',
        textColor: Colors.black,
      );
    }
  }*/

  /// tab 选项卡
  Widget _topTabView() {
    return Container(
//      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
      height: ScreenUtil().setWidth(70),
      child: Stack(
        children: [
          TabBar(
            indicator: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
              gradient: LinearGradient(
                colors: [
                  Color(0xff47CDCC),
                  Color(0xff5E94E7),
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            labelPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(0)),
            labelColor: Colors.white,
            unselectedLabelColor: Colors.black54.withOpacity(0.49),
            controller: _tabController,
            labelStyle: TextStyle(
              fontWeight: FontWeight.w400,
              height: 1,
              fontSize: ScreenUtil().setSp(20),
            ),
            unselectedLabelStyle: TextStyle(
              fontWeight: FontWeight.w400,
              height: 1,
              fontSize: ScreenUtil().setSp(20),
            ),
            tabs: [
              /* Text(
                              '${_gameBean?.name}',
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.black, fontWeight: FontWeight.w400),
                            ),*/
//          Tab(text: translate("quickaccess")),
              Tab(text: translate("quickaccess")),
              Tab(text: translate("games")),
              Tab(text: translate("themes")),
              Tab(text: translate("mic_frames")),
              Tab(text: translate("bubble")),

              Tab(text: translate("sound_effects")),
            ],
          ),
          Positioned(
            /// 这里的6需要与上面tabs长度对应
            left: ScreenUtil().screenWidth * 2 / 6.0 - ScreenUtil().setWidth(40),
            top: 0,
            child: YBDRoomUtil.ludoRedDot(),
          )
        ],
      ),
    );
  }
  void _topTabViewbitImoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 分割线
  Divider rowDivider() {
    return Divider(
      height: ScreenUtil().setWidth(1),
      thickness: ScreenUtil().setWidth(1),
      color: Color(0xffDEDCDC),
    );
  }
}
