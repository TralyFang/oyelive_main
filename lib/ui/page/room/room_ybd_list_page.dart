import 'dart:async';


import 'dart:async';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/event/JoinChannelBusEvent.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_operat_halper.dart';
import 'package:oyelive_main/ui/page/room/room_ybd_page.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/widget/tp_ybd_page_view.dart' as TPPageView;

import '../../../common/event/change_ybd_room_bus_event.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../main.dart';
import '../../../module/entity/room_ybd_info_entity.dart';
import 'bloc/room_ybd_bloc.dart';
import 'room_ybd_helper.dart';
import 'service/live_ybd_service.dart';

class YBDRoomListPage extends StatefulWidget {
  /// 房间列表第一个显示的房间ID
  final int roomId;

  /// 房间分类
  final String tag;

  /// 进入房间类型
  // final RoomPageType roomPageType;

  const YBDRoomListPage({
    Key? key,
    required this.roomId,
    required this.tag,
    // this.roomPageType,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _YBDRoomListPageState();
}

class _YBDRoomListPageState extends State<YBDRoomListPage> with WidgetsBindingObserver {
  /// 初始化控制器
  TPPageView.PageController? _pageController;

  /// 房间列表
  // List<YBDRoomInfo> _roomList = RandomRoomList.randomRoomIdList();
  List<YBDRoomInfo>? _roomList;

  /// 当前页面
  int _currentPage = 0;

  /// 是否翻页，记录房间翻页事件
  bool _pageChanged = false;

  /// 进房间或切房间过程中禁止滑动
  bool _canChangeRoom = false;

  /// 房间滑动的定时器, 禁止滑动后10秒钟自动开启房间滑动
  Timer? _changeRoomTimer;
  final int CHANGE_ROOM_DURATION = 10;

  /// 键盘是否可见
  bool _isKeyboardVisible = false;

  /// 是否手动跳转页面
  /// 从第一个房间跳转到最后一个房间或从最后一个房间跳转到第一个房间
  bool _jumpPage = false;

  /// 订阅切房间的事件
  late StreamSubscription<YBDChangeRoomBusEvent> _changeRoomSubscription;

  /// 订阅加入声网频道的事件
  late StreamSubscription<YBDJoinChannelBusEvent> _joinChannelSubscription;

  @override
  void initState() {
    super.initState();
    logger.i('room list page initState: ${widget.roomId}');
    _roomList = _loopRoomList(YBDLiveService.instance.data.roomList);
    _currentPage = YBDRoomHelper.roomPageIndex(widget.roomId, _roomList);
    logger.v('roomlistpage length : ${_roomList!.length}, init page : $_currentPage');
    _pageController = TPPageView.PageController(initialPage: _currentPage);

    // 开启滑动房间定时器
    _enableChangeRoomTimer();

    // 监听切房间事件，跳转到对应的房间
    _onEventBus();

    // 监听键盘事件
    WidgetsBinding.instance?.addObserver(this);
  }
  void initStateBbWrWoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    logger.v('page dispose');
    // _roomBloc?.close();
    _changeRoomSubscription.cancel();
    _joinChannelSubscription.cancel();
    WidgetsBinding.instance?.removeObserver(this);

    // 清理房间资源
    YBDRoomUtil.cleanRoom(_roomList![_currentPage].id);
    YBDMicOperateHelper.getInstance()!.cleanStatus();
    super.dispose();
  }
  void disposeUr88Eoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeMetrics() {
    final bottomInset = WidgetsBinding.instance?.window.viewInsets.bottom ?? 0.0;
    final newValue = bottomInset > 0.0;
    logger.i('_isKeyboardVisible: $newValue, bottomInset: $bottomInset');

    if (newValue != _isKeyboardVisible) {
      setState(() {
        _isKeyboardVisible = newValue;
      });
    }
  }
  void didChangeMetricsvorQloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return NotificationListener(
      onNotification: (dynamic scrollNotification) {
        if (scrollNotification is ScrollEndNotification) {
          // 房间列表翻页动作完成后再跳转到对应的房间
          if (_pageChanged) {
            logger.v('NotificationListener scroll end : $scrollNotification');
            _pageChanged = false;
            logger.v('currentPage: $_currentPage, roomId: ${_roomList![_currentPage].id}');

            if (_jumpPage) {
              _jumpPage = false;
              logger.i('jump change: $_currentPage');
              // 跳转到房间页面刷新页面
              _pageController!.jumpToPage(_currentPage);
            }

            logger.i('scroll change: $_currentPage');
            // 翻页动作完成跳转到房间
            setState(() {});

            if (_canChangeRoom) {
              setState(() {
                _canChangeRoom = false;
              });

              // 开启滑动房间定时器
              _enableChangeRoomTimer();

              BlocProvider.of<YBDRoomBloc>(context).changeRoom(
                targetRoom: _roomList![_currentPage].id,
                roomTag: _roomList![_currentPage].tag,
              );
            } else {
              logger.i('not _enableChangeRoom');
            }
          }
        }

        return true;
      },
      child: TPPageView.PageView.builder(
        scrollDirection: Axis.vertical,
        physics: _canSwipe() ? AlwaysScrollableScrollPhysics() : NeverScrollableScrollPhysics(),
        controller: _pageController,
        itemCount: _roomList!.length,
        onPageChanged: (int index) {
          if (_currentPage == index) {
            // 页面没有变化不用切房间
            return;
          }

          logger.v('onPageChanged: $index');
          _currentPage = index;
          _pageChanged = true;

          if (_roomList![index].id == -1) {
            // 从第一个房间跳转到最后一个房间
            logger.v('first to last room');
            _jumpPage = true;
            _currentPage = _roomList!.length - 2;
          } else if (_roomList![index].id == -100) {
            // 从最后一个房间跳转到第一个房间
            logger.v('last to first room');
            _jumpPage = true;
            _currentPage = 1;
          }
        },
        itemBuilder: (_, int index) {
          if (index == _currentPage) {
            logger.v('index: $index, roomPage: $_currentPage');
            // return GameRoomPageTest(
            //     // key: ValueKey(index),
            //     );
            return YBDRoomPage(
                // key: ValueKey(index),
                );
          } else {
            return Container(
              child: Image.asset(
                'assets/images/liveroom/room_star_bg.webp',
                fit: BoxFit.cover,
              ),
            );
          }
        },
      ),
    );
  }
  void buildVSD12oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是否能滑动房间列表
  bool _canSwipe() {
    // 自己是主播不能滑动
    if (YBDCommonUtil.storeFromContext()!.state.bean!.id == widget.roomId) return false;

    // 进房或切房操作中不能滑动
    if (!_canChangeRoom) return false;

    // 显示键盘时不能滑动
    if (_isKeyboardVisible) return false;

    return true;
  }

  /// 监听切房间事件
  void _onEventBus() {
    _changeRoomSubscription = eventBus.on<YBDChangeRoomBusEvent>().listen((YBDChangeRoomBusEvent event) {
      _onChangeRoomBusEvent(event);
    });

    _joinChannelSubscription = eventBus.on<YBDJoinChannelBusEvent>().listen((YBDJoinChannelBusEvent event) {
      _onJoinChannelBusEvent(event);
    });
  }
  void _onEventBusJ1Sgroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 开启滑动房间定时器
  void _enableChangeRoomTimer() {
    _disableChangeRoomTimer();

    _changeRoomTimer = Timer(Duration(seconds: CHANGE_ROOM_DURATION), () {
      logger.i('_changeRoomTimer _enableChangeRoom');
      if (mounted)
        setState(() {
          _canChangeRoom = true;
        });
    });
  }

  /// 取消滑动房间定时器
  void _disableChangeRoomTimer() {
    _changeRoomTimer?.cancel();
    _changeRoomTimer = null;
  }

  /// 处理加入频道的事件
  void _onJoinChannelBusEvent(YBDJoinChannelBusEvent event) {
    logger.i('_onJoinChannelBusEvent _enableChangeRoom');
    // 加入房间后允许滑动房间
    setState(() {
      _canChangeRoom = true;
    });

    // 已经能滑动房间了，取消定时器
    _disableChangeRoomTimer();
  }
  void _onJoinChannelBusEventnJRRloyelive(YBDJoinChannelBusEvent event) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理切房间的事件
  void _onChangeRoomBusEvent(YBDChangeRoomBusEvent event) {
    if (event.roomId == _roomList![_currentPage].id) {
      logger.v('target room equal current room : ${event.roomId}');
      return;
    }

    if (YBDRoomUtil.isTalent(_roomList![_currentPage].id)) {
      // 禁止当前房间的主播跳转房间
      logger.v('talent not jump : ${_roomList![_currentPage].id}');
      return;
    }

    // 检查房间列表是否包含目标房间
    var targetRoom = _roomList!.firstWhereOrNull(
      (roomInfo) => roomInfo.id == event.roomId,
    );

    if (null != targetRoom) {
      // 跳转到目标房间
      var targetIndex = YBDRoomHelper.roomPageIndex(event.roomId, _roomList);
      logger.v('room list containe target : $targetIndex');
      _pageController!.jumpToPage(targetIndex);
      _currentPage = targetIndex;
      BlocProvider.of<YBDRoomBloc>(context).changeRoom(targetRoom: _roomList![_currentPage].id);
    } else {
      // 在当前房间后面添加目标房间
      var roomInfo = YBDRoomInfo();
      roomInfo.id = event.roomId;
      _currentPage += 1;
      _roomList!.insert(_currentPage, roomInfo);
      logger.v('room list insert target : $_currentPage');
      _pageController!.jumpToPage(_currentPage);

      // 跳转到目标房间
      BlocProvider.of<YBDRoomBloc>(context).changeRoom(targetRoom: _roomList![_currentPage].id);

      if (mounted) {
        setState(() {});
      }
    }
  }
  void _onChangeRoomBusEventzoxxYoyelive(YBDChangeRoomBusEvent event) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 生成可循环滑动的房间列表数组
  /// 数组的第一个元素设置为roomId等于-1，最后一个元素设置为roomId等于-100
  /// 当滑动到roomId等于-1的房间时pageView跳转到最后一个房间
  /// 当滑动到roomId等于-100的房间时pageView跳转到第一个房间
  List<YBDRoomInfo> _loopRoomList(List<YBDRoomInfo?>? sourceList) {
    // 列表为空时返回空数组
    if (null == sourceList || sourceList.isEmpty) return [];

    List<YBDRoomInfo> result = List.from(sourceList);

    if (result.length < 2) {
      // 只有一个房间时不用循环滚动
      return result;
    }

    var first = YBDRoomInfo();
    first.id = -1;
    result.insert(0, first);

    var last = YBDRoomInfo();
    last.id = -100;
    result.add(last);

    return result;
  }
}
