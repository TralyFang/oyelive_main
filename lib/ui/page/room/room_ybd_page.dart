import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart' as g;
import 'package:intl/intl.dart';
import 'package:oye_tool_package/widget/should_rebuild.dart';
import 'package:redux/redux.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import 'package:wakelock/wakelock.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_live_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/event/change_ybd_room_bus_event.dart';
// import 'package:oyelive_main/common/util/app_life_log_util.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/room/entity/game_ybd_query_model.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_type_ext.dart';
import 'package:oyelive_main/ui/page/room/entity/emoji_ybd_entity.dart';
import 'package:oyelive_main/ui/page/room/live_chat/controller/tp_ybd_msg_controller.dart';
import 'package:oyelive_main/ui/page/room/live_chat/item_ybd_tp_marquee.dart';
import 'package:oyelive_main/ui/page/room/live_chat/room_ybd_gift_slide_out.dart';
import 'package:oyelive_main/ui/page/room/pk/pk_ybd_start_animation.dart';
import 'package:oyelive_main/ui/page/room/play_center/entity/game_ybd_entity.dart';
import 'package:oyelive_main/ui/page/room/room_ybd_gift_page.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/bus_event/event_ybd_fn.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';
import 'package:oyelive_main/ui/page/room/widget/jump_ybd_tpgo_notice.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_dialog_guide.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_special_entrance.dart';
import 'package:oyelive_main/ui/widget/intl_ybd_positioned.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/constant/const.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/room_socket/message/resp/response.dart';
import '../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../common/room_socket/room_ybd_socket_util.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/config_ybd_util.dart';
import '../../../common/util/dialog_ybd_util.dart';
import '../../../common/util/image_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../main.dart';
import '../../../redux/app_ybd_state.dart';
import '../../widget/colored_ybd_safe_area.dart';
import 'bloc/pk_ybd_state_bloc.dart';
import 'bloc/room_ybd_bloc.dart';
import 'bloc/room_ybd_bloc_state.dart';
import 'bottom_ybd_nav.dart';
import 'define/room_ybd_define.dart';
import 'gift_btn/gift_ybd_fly_out.dart';
import 'live_chat/live_ybd_chat.dart';
import 'mic/mic_ybd_bloc.dart';
import 'mic/mic_ybd_page.dart';
import 'music_player/music_ybd_player.dart';
import 'notice/notice_ybd_type.dart';
import 'pk/pk_ybd_board.dart';
import 'pk/pk_ybd_normal_dialog.dart';
import 'room_ybd_bg_page.dart';
import 'room_ybd_helper.dart';
import 'service/live_ybd_service.dart';
import 'teen_patti/entity/ybd_teen_info.dart';
import 'util/room_ybd_util.dart';
import 'widget/global_ybd_gift_banners.dart';
import 'widget/mic_ybd_request_amount.dart';
import 'widget/normal_ybd_dialog.dart';
import 'widget/room_ybd_icon_banner.dart';
import 'widget/room_ybd_title.dart';
import 'widget/slider.dart';

class YBDRoomPage extends StatefulWidget {
  YBDRoomPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _YBDRoomPageState();
}

class _YBDRoomPageState extends BaseState<YBDRoomPage> with TickerProviderStateMixin, WidgetsBindingObserver {
  /// 键盘高度
  var _keyboardHeight = 0.0;

  /// 底部消息输入框控制器
  TextEditingController _bottomInputController = TextEditingController();

  /// 底部消息输入 focusNode
  FocusNode _focusNode = FocusNode();

  /// combo礼物动画控制器
  SVGAAnimationController? _comboSvgaController;

  /// 大事件SVG动画控制器
  late SVGAAnimationController _bigEventSVGController;

  /// 给 [YBDMicRequestAmount] 底部弹框提供上下文
  final GlobalKey<_YBDRoomPageState> _roomPageStateKey = GlobalKey<_YBDRoomPageState>();

  /// 是否显示过封号、限播弹框
  bool _showedLimitDialog = false;

  /// 是否显示游戏
  bool _shouldShowGame = false;

  /// 房间标题和大礼物横幅是否在显示
  // bool _showBanners = false;

  /// 大礼物
  // List<YBDGlobal> _globalList = [];

  // int _giftX = -200;

  /// 销毁定时器
  // Timer _bigGiftTimer;

  // int _timerNum = 0;

  /// 是否已经清空了房间数据，在大事件跳转时避免重复清空数据
  bool _isRoomCleaned = false;

  GlobalKey _key = new GlobalKey();
  Size? _size;

  StreamSubscription<YBDOpenTp>? streamSubscription;

  @override
  void initState() {
    super.initState();

    logger.v('_isRoomCleaned: $_isRoomCleaned');
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      logger.v('initState : ${_bloc()?.roomId}');
      g.Get.put(YBDTpMsgController()..roomId = _bloc()?.roomId);

      // 初始化房间数据
      _initRoomData();
      logger.v('current page router = ${g.Get.currentRoute}, ${g.Get.previousRoute}  ${g.Get.rawRoute!.currentResult}');
      // 进房埋点
      YBDTALiveRoomTrack().taRoom(
        event: YBDTATrackEvent.enter_liveroom,
        location: YBDLiveService.instance.data.prePage,
      );
      // 异常退出房间
      if (!YBDLiveService.instance.enterRoomEnable) {
        Future.delayed(Duration(milliseconds: 300), () {
          logger.v('close room');
          _bloc()?.exitRoom();
        });
      }
    });

    streamSubscription = eventBus.on<YBDOpenTp>().listen((event) {
      logger.v('open tp');
      _funcInit(event.tag);
    });

    ApiHelper.getSelfEmoji();
  }
  void initState2CHJmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeMetrics() {
    final bottomInset = WidgetsBinding.instance?.window.viewInsets.bottom ?? 0.0;
    final bottomInsetDb = bottomInset / ScreenUtil().pixelRatio!;
    logger.v('window bottomInset: $bottomInset, $bottomInsetDb');
    setState(() => _keyboardHeight = bottomInsetDb);
  }
  void didChangeMetricsItFgkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 初始化房间数据
  /// 页面渲染完后在调用该方法
  /// 在[addPostFrameCallback]方法里调用
  void _initRoomData() {
    logger.v('combo svga controller is : $_comboSvgaController');

    // 保存房间context
    _bloc()?.setRoomPageContext(context);

    // 监听app生命周期事件
    WidgetsBinding.instance?.addObserver(this);

    // 保持屏幕常亮
    Wakelock.enable();

    // 配置combo动图
    // _configComboSvg();

    // 配置房间大礼物动图
    _configRoomBigGiftSvg();

    // 根据配置项判断是否显示游戏
    _queryGameConfig();

// 获取表情包信息
    _getEmoji();
    // 在[YBDRoomBloc]的构造方法里调用RoomEvent.Init
    // _bloc().add(RoomEvent.Init);

    //初始化VIP 信息
    YBDTeenInfo.getInstance().queryTeenPattiRecord(context);

    //打开一些功能
    _funcInit(_bloc()!.funcType, code: _bloc()!.gameCode);
  }
  void _initRoomDataaqcl5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  onAddSubscription() {
    // 修改了房间跳转的方式，在[YBDRoomListPage._onEventBus]中跳转
    // addSub(eventBus.on<YBDGoToRoomEvent>().listen((event) {
    //   if (event != null && event.userId != null) {
    //     _goToBigGiftRoom(event.userId, instantly: true);
    //   }
    // }));
    return super.onAddSubscription();
  }

  /// 房间引导提示
  void _showRoomGuide() async {
    // 房间引导
    await YBDRoomDialogGuide.instance.showRoomGuide(
      takeMicCallback: () {
        // TODO: 测试代码
        // 添加上麦的代码
        // 有空麦位时随机取一个麦位，没有麦位时 do nothing
        logger.v('take mic guide call back');
      },
      comboCallback: () {
        logger.v('send combo gift guide call back');
      },
      sendCallback: () {
        logger.v('send bottom gift guide call back');
        _showBackPack();
      },
    );
  }
  void _showRoomGuide4F5dtoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 选中背包的第一个礼物
  void _showBackPack() {
    YBDRoomUtil.showGiftSheet(
      showPackage: true,
      giftInfo: YBDRoomUtil.roomGuideGiftInfo,
    );
  }

  /// combo动图
  Widget _comboSvga() {
    if (null == _comboSvgaController) {
      logger.v('combo svga controller is null');
      _configComboSvg();
      return SizedBox();
    }

    return Container(
      width: ScreenUtil().setWidth(160),
      height: ScreenUtil().setWidth(160),
      child: SVGAImage(
        _comboSvgaController!,
        clearsAfterStop: false,
      ),
    );
  }
  void _comboSvgaetJ2Soyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 配置combo动图
  Future<void> _configComboSvg() async {
    _comboSvgaController = SVGAAnimationController(vsync: this);
    final videoItem = await SVGAParser.shared.decodeFromAssets('assets/animation/gift.svga');
    _comboSvgaController!.videoItem = videoItem;
    _comboSvgaController!.repeat();

    // 配置svga后刷新页面
    setState(() {});
  }

  /// 配置房间大礼物动图
  Future<void> _configRoomBigGiftSvg() async {
    _bigEventSVGController = SVGAAnimationController(vsync: this);
    final bigEventVideoItem = await SVGAParser.shared.decodeFromAssets('assets/animation/big_event_bg.svga');
    _bigEventSVGController.videoItem = bigEventVideoItem;
    _bigEventSVGController.repeat();
  }
  void _configRoomBigGiftSvgYleytoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    logger.v('room page didChangeAppLifecycleState : $state');
    switch (state) {
      case AppLifecycleState.resumed:
        YBDRoomSocketUtil.getInstance().initSocket();
        logger.v('room page didChangeAppLifecycleState resumend subscribe roomId'.subLog(step: 100));
        YBDRoomSocketApi.getInstance().subscribe(_bloc()?.roomId);
        Future.delayed(Duration(seconds: 2), () {
          YBDRoomSocketApi.getInstance().sendTeenPatti(
            roomId: _bloc()?.roomId,
          );
        });
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
    }
  }
  void didChangeAppLifecycleState9KAkFoyelive(AppLifecycleState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    logger.v('page dispose');
    g.Get.delete<YBDTpMsgController>();
    // 大礼物跳转房间时避免重复清空直播数据
    logger.v('_isRoomCleaned: $_isRoomCleaned');
    streamSubscription?.cancel();
    if (!_isRoomCleaned) {
      _cleanRoomDataV2();
    }

    // 取消监听 [didChangeAppLifecycleState]
    WidgetsBinding.instance?.removeObserver(this);
    Wakelock.disable();
    YBDRoomUtil.liveDurationInMillSecs = null;

    super.dispose();
  }

  /// v2: 清除房间数据和直播数据数据
  Future<void> _cleanRoomDataV2() async {
    logger.v('_cleanRoomDataV2');
    _comboSvgaController?.dispose();
    _bigEventSVGController.dispose();
    // _titleController = null;
    // _bigGiftTimer?.cancel();
  }
  void _cleanRoomDataV2UJQujoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _getSize() {
    try {
      RenderBox _cardBox = _key.currentContext!.findRenderObject() as RenderBox;
      _size = _cardBox.size;
      logger.v("currentContext RenderObject: $_size");
      // logger.v(
      //     "chat live cover size : ${ScreenUtil().uiHeightPx - ScreenUtil().bottomBarHeight - ScreenUtil().statusBarHeight - _size.height} ");
    } catch (e) {
      logger.e('get size error :$e');
    }
  }

  Future<bool> _onBackPressed() {
    if (YBDRoomUtil.isTalent(_bloc()?.roomId)) {
      logger.v('_onBackPressed is talent');
      //*  pk中主播不能退出房间
      if (YBDLiveService.instance.data.pkInfo?.status == 1) {
        YBDDialogUtil.showPKNormalDialog(context, PKNormalType.Exit);
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.ROOM_PAGE,
          itemName: YBDItemName.PK_TALENT_EXIT_ROOM,
        ));
      } else {
        YBDDialogUtil.showNormalDialog(
          context,
          type: NormalDialogType.two,
          okCallback: () {
            _bloc()?.exitRoom();
            logger.v('close room');
          },
          content: translate('exit_room_notice'),
          ok: translate('ok'),
          cancel: translate('no'),
        );
      }
    } else {
      logger.v('_onBackPressed not talent');
      YBDDialogUtil.showLeaveRoomConfirmDialog(context);
    }

    return Future<bool>.value(false);
  }
  void _onBackPressedncwb8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    logger.v('didChangeDependencies');
    super.didChangeDependencies();
  }

  @override
  Widget myBuild(BuildContext context) {
    YBDImageUtil.roomImageBasePath(context);
    YBDImageUtil.setImageConfig(context);
    // Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context);
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      if (null == _size) {
        _getSize();
      }
    });
    return BlocListener<YBDRoomBloc, YBDRoomState>(
        listener: (_, state) {
          if (state.roomInfo != null) {
            // 房间引导
            _showRoomGuide();
          }

          // 检查房间密码
          if (state.checkPwdStatus == CheckPwdStatus.NeedCheck) {
            _bloc()?.changeCheckPwdStatus(CheckPwdStatus.Checking);
            // 弹出密码输入框
            YBDRoomUtil.showRoomPwdDialog(context, callback: (pwd) {
              //
            });
          }

          // 弹出聊天输入框
          if (null != state.atUserNickName) {
            logger.v('at user : ${state.atUserNickName}');
            _bottomInputController.text = '@${state.atUserNickName} ';
            _focusNode.requestFocus();
          }

          if (!_showedLimitDialog &&
              (state.noticeMessage?.type == YBDNoticeType.TYPE_LIMIT ||
                  state.noticeMessage?.type == YBDNoticeType.TYPE_TITLE)) {
            // 标记已显示避免重复弹框
            _showedLimitDialog = true;
            //限播 封号都要断开声网
            YBDLiveService.instance.leaveRoomInvalid();
            YBDDialogUtil.showNormalDialog(context, type: NormalDialogType.one, okCallback: () {
              // logger.v(
              //     'noticeMessage.type：${state.noticeMessage.type}  noticeMessage.desc：${state.noticeMessage.content}');
              if (state.noticeMessage!.type == YBDNoticeType.TYPE_LIMIT) {
                // 退出房间
                _bloc()?.exitRoom();
              } else if (state.noticeMessage?.type == YBDNoticeType.TYPE_TITLE) {
                // 跳转登录页
                logger.v('noticeMessage.type：Navigator.pop(context) LOGOUT');
                YBDNavigatorHelper.navigateTo(
                  context,
                  YBDNavigatorHelper.login,
                  clearStack: true,
                  forceFluro: true,
                );
              }
            },
                content: '${state.noticeMessage?.content}',
                ok: 'OK',
                barrierDismissible: false,
                autoConfirmDuration: Duration(seconds: 5));
          } else if (state.showLevelUpDialog ?? false) {
            logger.v('roomLevelUpHint');
            _bloc()?.showedLevelUpDialog();
            YBDDialogUtil.showRoomLevelUpDialog(context, state.talentInfo?.roomlevel,
                barrierDismissible: false, isRoomLevel: true);
          }
        },
        child: BlocBuilder<YBDPkStateBloc, PkState>(
            bloc: context.read<YBDPkStateBloc>(),
            builder: (_, PkState pkState) {
              return BlocBuilder<YBDRoomBloc, YBDRoomState>(
                // 切房间刷新房间页面
                buildWhen: (prev, current) => current.type == RoomStateType.RoomChanged,
                builder: (BuildContext context, YBDRoomState state) {
                  bool isPk = false;
                  Widget content = Container();
                  switch (pkState) {
                    case PkState.None:
                    case PkState.Matching:
                    case PkState.End:
                      //正常
                      isPk = false;

                      content = _micContainer();
                      break;
                    case PkState.PKing:
                    case PkState.CountDown:
                    case PkState.Closing:
                    default:
                      //正在pk的widget
                      isPk = true;
                      content = YBDPkBoard();
                      break;
                  }
                  Widget micParts = Column(
                    key: _key,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                          height: ScreenUtil().statusBarHeight > 0
                              ? ScreenUtil().statusBarHeight
                              : ScreenUtil().statusBarHeight * 1.5),
                      // 房间顶部 UI
                      YBDRoomTitle(),

                      // 麦位 UI
                      // _micContainer(),
                      content
                    ],
                  );
                  // 聊天区
                  Widget chatParts =
                      UpdateStateWidget(builder: (BuildContext context, UpdateStateWidgetState updateState) {
                    return Container(
                      height: _chartPartHeight(state.roomTag ?? '', state.dynamicHeight ?? 0, isPk, updateState),
                      // height: 700.px,
                      width: ScreenUtil().setWidth(720),
                      // color: Colors.green,
                      child: Stack(
                        children: <Widget>[
                          Positioned.fill(
                            left: ScreenUtil().setWidth(30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                _shouldShowGame ? YBDItemTpMarquee() : SizedBox(),
                                Expanded(
                                  child: YBDLiveChat(
                                    // 公聊区列表
                                    roomId: _bloc()?.roomId,
                                    talentPic: state.roomInfo?.headimg ?? '',
                                    mUserInfo: YBDCommonUtil.storeFromContext()?.state.bean,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  });

                  return WillPopScope(
                    onWillPop: _onBackPressed,
                    child: Scaffold(
                      key: _roomPageStateKey,
                      resizeToAvoidBottomInset: false,
                      body: GestureDetector(
                        onTap: () {
                          logger.v('clicked hide keyboard gesture');
                          // 隐藏键盘
                          FocusScope.of(context).requestFocus(FocusNode());
                          _bloc()?.add(RoomEvent.HideKeyBoard);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color(0xff6C6CDF),
                                Color(0xff6C6CDF),
                                Color(0xff4A7AB7),
                                Color(0xff6C6CDF),
                                Color(0xff3D0B8D),
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                            ),
                          ),
                          child: YBDColoredSafeArea(
                            top: false,
                            bottom: false,
                            child: Stack(alignment: Alignment.center, children: [
                              // 房间背景
                              BlocBuilder<YBDRoomBloc, YBDRoomState>(
                                buildWhen: (prev, current) => (current.roomTheme != prev.roomTheme),
                                builder: (_, state) {
                                  logger.v('roomAnimationPath builder: ${state.roomAnimationPath}');
                                  return YBDRoomBgPage(state.roomTheme);
                                },
                              ),
                              Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  // 麦位UI
                                  micParts,
                                  // SizedBox(height: 30.px),
                                  chatParts,
                                  Spacer(),
                                  SizedBox(
                                      height: YBDRoomUtil.bottomNavBarPadding(isKeyboardVisible: _keyboardHeight > 0.0)),
                                ],
                              ),
                              // pk匹配时间
                              Positioned(
                                  right: ScreenUtil().setWidth(160),
                                  top: ScreenUtil().statusBarHeight + ScreenUtil().setWidth(70),
                                  child: _PkMatchCounting()),

                              ///座驾 gift动画
                              BlocBuilder<YBDRoomBloc, YBDRoomState>(
                                buildWhen: (prev, current) => (current.roomAnimationPath != prev.roomAnimationPath),
                                builder: (_, state) {
                                  logger.v('roomAnimationPath builder: ${state.roomAnimationPath} ');
                                  if (state.roomAnimationPath == null || state.roomAnimationPath?[0] == null) {
                                    return Container();
                                  } else {
                                    return YBDRoomGiftage(
                                      roomAnimationPath: state.roomAnimationPath,
                                      enter: state.enter,
                                      gift: state.gift,
                                    );
                                  }
                                },
                              ),
                              // room slider
                              _slider(isPk),
                              YBDGiftFlyOut(),
                              // YBDComboTips(),
                              _giftSlideOut(isPk),

                              ///底部栏和送礼栏
                              Positioned(
                                bottom: _keyboardHeight,
                                child: BlocBuilder<YBDRoomBloc, YBDRoomState>(
                                  buildWhen: (prev, current) => current.type == RoomStateType.KeyboardUpdated,
                                  builder: (_, state) {
                                    // 底部栏
                                    return YBDBottomNav(
                                      roomId: context.read<YBDRoomBloc>().roomId,
                                      popInput: state.showKeyboard ?? false,
                                      textEditingController: _bottomInputController,
                                      focusNode: _focusNode,
                                      // micBloc: _micBloc,
                                    );
                                  },
                                ),
                              ),
                              // 房间大礼物
                              _bigGift(),
                              //* PK开始动画
                              _pkStartAnimationWidget(),

                              StreamBuilder<RoomSocketStatus>(
                                  initialData: YBDRoomSocketUtil.getInstance().roomSocketStatus,
                                  stream: YBDRoomSocketUtil.getInstance().socketStatusOutStream,
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData && snapshot.data == RoomSocketStatus.TryConnecting)
                                      return Positioned(
                                        // left: ScreenUtil().setWidth(242),
                                        // right: ScreenUtil().setWidth(242),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(5))),
                                              color: Colors.black.withOpacity(0.5)),
                                          child: Row(
                                            children: [
                                              Image.asset(
                                                'assets/images/status/status_upload_progress.webp',
                                                height: ScreenUtil().setWidth(42),
                                              ),
                                              SizedBox(
                                                width: ScreenUtil().setWidth(4),
                                              ),
                                              Text(
                                                "Connecting…",
                                                style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                                              )
                                            ],
                                          ),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: ScreenUtil().setWidth(24),
                                              vertical: ScreenUtil().setWidth(4)),
                                        ),
                                        bottom: ScreenUtil().setWidth(140) + ScreenUtil().bottomBarHeight,
                                      );
                                    return SizedBox();
                                  })
                            ]),
                          ),
                        ),
                      ),
                    ),
                  );
                },
              );
            }));
  }
  void myBuildKybBroyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _pkStartAnimationWidget() {
    return BlocBuilder<YBDPkStateBloc, PkState>(
        bloc: context.read<YBDPkStateBloc>(),
        builder: (_, PkState state) {
          if ((state == PkState.PKing || state == PkState.CountDown)) {
            return YBDPKStartAnimation(
                homeImg: YBDLiveService.instance.data.pkInfo?.sourceHeadImg ?? '',
                homeName: YBDLiveService.instance.data.pkInfo?.sourceNickName ?? '',
                awayImg: YBDLiveService.instance.data.pkInfo?.targetHeadImg ?? '',
                awayName: YBDLiveService.instance.data.pkInfo?.targetNickName ?? '');
          } else {
            return Container(width: 0);
          }
        });
  }

  /// 麦位 UI
  Widget _micContainer() {
    return BlocBuilder<YBDMicBloc, YBDMicBlocState>(
      builder: (context, state) {
//        logger.v('YBDMicUserPublish _micContainer ');
        return YBDMicPage(
          roomId: _bloc()?.roomId,
          // roomType: 1,
          // micBeans: state?.micBeanList ?? null,
        );
      },
    );
  }
  void _micContainerrzV3aoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 上麦请求 UI
  Widget _micRequestContainer() {
    return BlocBuilder<YBDMicBloc, YBDMicBlocState>(
      // bloc: _micBloc,
      builder: (context, state) {
        return (state.hasMicRequest! && YBDRoomUtil.isTalent(_bloc()?.roomId))
            ? YBDMicRequestAmount(_roomPageStateKey.currentContext)
            : SizedBox();
      },
    );
  }

  int _pkMatchingTime = 0;
  Timer? _pkCoutingTimer;
  int? _expireTime = 600;

  /// Pk matching
  Widget _PkMatchCounting() {
    return BlocBuilder<YBDPkStateBloc, PkState>(
      bloc: context.read<YBDPkStateBloc>(),
      builder: (_, PkState state) {
        if (state == PkState.Matching) {
          if (YBDLiveService.instance.data.matchEndTime != null) {
            _expireTime = YBDLiveService.instance.data.matchEndTime;
          }

          log('set expireTime to $_expireTime');
          if (_pkCoutingTimer == null) {
            _pkCoutingTimer?.cancel();
            _pkCoutingTimer = Timer.periodic(Duration(seconds: 1), (timer) {
              _pkMatchingTime += 1;
              // context.read<YBDPkStateBloc>().add(PkState.Matching);
              if (_pkMatchingTime > _expireTime!) {
                //超时退出匹配
                timer.cancel();
                _pkMatchingTime = 0;
                quitMatching();
              }
              setState(() {});
              print("sijoidjois$_pkMatchingTime");
            });
          }

          return Text(
            DateFormat('mm:ss').format(DateTime(
              0,
              0,
              0,
              0,
              0,
              _pkMatchingTime,
              0,
              0,
            )),
            style: TextStyle(fontSize: ScreenUtil().setSp(18), color: Colors.white.withOpacity(0.85)),
          );
        } else {
          _pkMatchingTime = 0;
          _pkCoutingTimer?.cancel();
          _pkCoutingTimer = null;
          return Container();
        }
      },
    );
  }
  void _PkMatchCounting02yHJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  quitMatching() {
    YBDRoomSocketApi().endMatch(
      roomId: YBDLiveService.instance.data.roomId,
      onSuccess: (Response data) {
        if (data.code == Const.HTTP_SUCCESS) {
          context.read<YBDPkStateBloc>().add(PkState.None);
          YBDDialogUtil.showPKNormalDialog(context, PKNormalType.NoTalent);
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.ROOM_PAGE,
            itemName: YBDItemName.PK_MATCH_TIMEOUT,
          ));
          setState(() {});
        } else {
          YBDToastUtil.toast(translate('failed'));
        }
      },
    );
  }

  /// 水果游戏图标
  Widget _greedyGameIcon(String? roomTag) {
    if (!(roomTag == "0" || roomTag == "3")) {
      // 用banner显示[YBDRoomIconBanner]
      return SizedBox();
    }

    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
      buildWhen: (prev, current) => current.type == RoomStateType.GameDataUpdated,
      builder: (context, state) {
        Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
        String showGreedy = store.state.configs?.showGreedy ?? '0';
        YBDGameRecord? record = GameType.GameTypeGreedy.info();
        String gameUrl;
        String gameIcon;
        gameUrl = record?.url ?? '';
        gameIcon = record?.icon ?? '';
        logger.v('===greedy game :$showGreedy gameUrl:$gameUrl  gameIcon:$gameIcon');

        if (showGreedy == '1' && gameIcon != null && gameUrl != null) {
          return GestureDetector(
            onTap: () {
              logger.v('open Greedy------');
              YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_game_click, record: record);
              YBDRoomUtil.showH5Game(context, gameUrl);
            },
            child: YBDNetworkImage(
              width: ScreenUtil().setWidth(100),
              imageUrl: gameIcon,
              fit: BoxFit.contain,
            ),
          );
        } else {
          return SizedBox();
        }
      },
    );
  }
  void _greedyGameIconF9TsBoyelive(String? roomTag) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// teenpatti游戏图标或游戏的banner
  /// 如果 roomTag == '0' || state.roomTag == "3"
  /// 否则显示teenpatti图标
  Widget _teenPattiGameIcon() {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
        buildWhen: (prev, current) =>
            current.type == RoomStateType.GameDataUpdated || current.type == RoomStateType.RoomTagUpdated,
        builder: (context, state) {
          Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;

          ///teenpatti 显示 有没有权限判断
          String showGreedy = store.state.configs?.showGreedy ?? '0';
          String? greedyUrl;
          String? teenpattiIcon;
          String? greedyIcon;

          String? tpgoUrl;
          String? tpgoIcon;
          YBDGameRecord? tpRecord;
          for (int i = 0; i < (state.gameData?.length ?? 0); i++) {
            var code = state.gameData![i]?.code;
            logger.v('game data code$code');
            if (code == TEENPATTI_CODE) {
              teenpattiIcon = state.gameData![i]!.icon;
              tpRecord = state.gameData![i];
            } else if (code == GREEDY_CODE) {
              greedyUrl = GameType.GameTypeGreedy.info()?.url ?? '';
              greedyIcon = GameType.GameTypeGreedy.info()?.icon ?? '';
            } else if (code == TPGO_CODE) {
              tpgoUrl = GameType.GameTypeTpGo.info()?.url ?? '';
              tpgoIcon = GameType.GameTypeTpGo.info()?.icon ?? '';
            }
          }

          // teenpatti和水果游戏图标
          List<String?> bannerIcons = [teenpattiIcon, tpgoIcon, greedyIcon];

          logger.v('===Royal Pattern game or greedy game :$showGreedy gameUrl:$greedyUrl tpgourl: $tpgoUrl ');
          if (showGreedy == '1' && bannerIcons.length > 0 && greedyUrl != null) {
            return YBDRoomIconBanner(bannerIcons, (index) {
              if (index == 2) {
                logger.v('open Greedy------');
                YBDGameRecord? record = GameType.GameTypeGreedy.info();
                alog.v('22.8.2--name:${record?.name ?? 'name'},id:${record?.id ?? 'id'},code:${record?.code}');
                YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_game_click, record: record);
                YBDRoomUtil.showH5Game(context, greedyUrl);
                return;
              }
              if (index == 1) {
                if (state.roomId == YBDUserUtil.getUserIdSync) {
                  YBDToastUtil.toast("Only audience can jump to TP GO");

                  return;
                }
                showDialog<Null>(
                    context: context, //BuildContext对象
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return YBDJumpTpgoNoticeDialog();
                    });
                return;
              }

              logger.v('open teenPatti------');
              if (tpRecord != null) YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_game_click, record: tpRecord);
              YBDTeenInfo.getInstance().enterType = TPEnterType.TPETRoomIcon;
              openTp();
            });
            // 显示teenpatti图标
            // return Container(
            //     width: ScreenUtil().setWidth(105),
            //     height: ScreenUtil().setWidth(125),
            //     child: GestureDetector(
            //       onTap: () {
            //         logger.v('open teenPatti------');
            //         YBDTeenInfo.getInstance().enterType = TPEnterType.TPETRoomIcon;
            //         if (tpRecord != null)
            //           YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_game_click, record: tpRecord);
            //         openTp();
            //       },
            //       child: YBDNetworkImage(
            //         width: ScreenUtil().setWidth(105),
            //         imageUrl: teenpattiIcon ?? '',
            //         fit: BoxFit.contain,
            //       ),
            //     ));
          } else {
            return Container();
          }
        });
  }
  void _teenPattiGameIconGyiJSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 根据配置项判断是否显示游戏，每个房间都一样
  Future<void> _queryGameConfig() async {
    _shouldShowGame = await ConfigUtil.shouldShowGame(context: context);
    setState(() {});
  }

  /// 房间右侧的Slider
  Widget _slider(bool isPk, {double? maxHeight}) {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
      buildWhen: (prev, current) => (current.roomTag != prev.roomTag),
      builder: (_, state) {
        double top = ScreenUtil().setWidth(YBDRoomHelper.getAdTop(state.roomTag));

        if (isPk) {
          double pkHeight = ScreenUtil().setWidth(384); // pk.contentHeight;
          double barHeight =
              ScreenUtil().statusBarHeight > 0 ? ScreenUtil().statusBarHeight : ScreenUtil().statusBarHeight * 1.5;
          double titleHeight = ScreenUtil().setWidth(200);

          double pkBottom = pkHeight + barHeight + titleHeight;
          logger.v('pkBottom:$pkBottom, top:$top');
          if (top < pkBottom) top = pkBottom;
        }

        double maxSafeHeight =
            ScreenUtil().screenHeight - top - ScreenUtil().setWidth(106) - ScreenUtil().bottomBarHeight;
        logger.v(
            '_micRequestContainer roomTag: ${state.roomTag} maxSafeHeight:$maxSafeHeight, ${ScreenUtil().screenHeight},${ScreenUtil().screenHeight}, top:$top');

        return YBDIntlPositioned(
          // slider
          top: top, //+ (isPk ? 80 : 0)
          right: 6.px,
          child: Container(
            // color: Colors.red,
            // height: 700.px,
            constraints: BoxConstraints(maxHeight: maxHeight ?? maxSafeHeight),
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  // music
                  if (Platform.isAndroid) YBDMusicPlayer(),
                  // 上麦申请通知
                  _micRequestContainer(),
                  YBDRoomSlider(),
                  SizedBox(height: ScreenUtil().setWidth(9)),
                  _shouldShowGame ? _teenPattiGameIcon() : SizedBox(),
                  Offstage(
                    offstage: !(state.roomTag == '0' || state.roomTag == '3'),
                    child: SizedBox(height: 20.px),
                  ),
                  _shouldShowGame ? _greedyGameIcon(state.roomTag) : SizedBox(),
                  YBDSpecialEntrance(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
  void _slidersTobjoyelive(bool isPk, {double? maxHeight}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 房间大礼物
  Widget _bigGift() {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
      buildWhen: (prev, current) => current.type == RoomStateType.BigGiftUpdated,
      builder: (context, state) {
        logger.v('BigGiftUpdated');

        if (state.bannerType == 0) {
          logger.v('BigGiftUpdated bannerType == 0');
          return SizedBox();
        }

        if (null == state.bigGifts || state.bigGifts!.isEmpty) {
          logger.v('BigGiftUpdated bigGifts is empty');
          return SizedBox();
        }

        return Positioned(
          top: ScreenUtil().setWidth(state.bigGiftTop),
          child: Container(
            height: ScreenUtil().setWidth(300),
            width: ScreenUtil().screenWidth,
            child: Stack(children: [
              Center(child: SVGAImage(_bigEventSVGController)),
              Padding(
                  padding: EdgeInsets.only(top: ScreenUtil().setWidth(85)),
                  child: Container(
                    alignment: Alignment.center,
                    // width: ScreenUtil().setWidth(750),
                    height: ScreenUtil().setWidth(148),
                    width: ScreenUtil().screenWidth,
                    child: Center(
                      child: YBDGlobalGiftBanners(
                        state.bigGifts!.first,
                        (roomId) {
                          logger.v('enter :$roomId gif');
                          //* PK中主播禁止跳转房间
                          if (YBDLiveService.instance.data.pkInfo?.status == 1) {
                            YBDDialogUtil.showPKNormalDialog(context, PKNormalType.Exit);
                          } else {
                            _goToBigGiftRoom(roomId ?? -1);
                          }
                          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                            YBDEventName.CLICK_EVENT,
                            location: YBDLocationName.ROOM_PAGE,
                            itemName: 'global_gift',
                          ));
                        },
                      ),
                    ),
                  )),
              // ),
            ]),
          ),
        );
      },
    );
  }
  void _bigGiftM3qOuoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击房间大礼物跳转
  Future<void> _goToBigGiftRoom(int bigRoomId, {bool instantly: false}) async {
    logger.v('_goToBigGiftRoom: $bigRoomId');
    // 不能获取房间id或大礼物房间为当前房间时不跳转
    // if (bigRoomId == null || bigRoomId == _bloc()?.roomId) return;

    // 跳转房间
    eventBus.fire(YBDChangeRoomBusEvent(bigRoomId));
    // TODO: 测试代码
    // eventBus.fire(YBDChangeRoomBusEvent(2000343));

    // // 标记房间数据被清空
    // _isRoomCleaned = true;
    // logger.v('_isRoomCleaned: $_isRoomCleaned');
    // // 清空当前房间数据
    // _cleanRoomDataV2();
    // // await _cleanRoomData();

    // // 显示加载框
    // YBDDialogUtil.showLoading(context, barrierDismissible: false);

    // // 这里要清空数据，延迟跳转到另一个房间
    // Future.delayed(Duration(seconds: instantly ? 1 : 3), () {
    //   // 隐藏加载框
    //   YBDDialogUtil.hideLoading(context);

    //   // 跳到大礼物房间
    //   YBDRoomHelper.enterRoom(
    //     context,
    //     bigRoomId,
    //     replace: true,
    //     forceEnter: true,
    //   );
    // });
  }

  // 聊天区以上的大小
  Size chartTopSize() {
    Size? boxSize = Size(0.0, 0.0);
    if ((_key.currentContext?.findRenderObject() as RenderFlex).hasSize) {
      boxSize = (_key.currentContext?.findRenderObject() as RenderFlex).size;
    }
    return boxSize;
  }

  /// 获取公聊区高度
  double _chartPartHeight(String tag, int dynamicHeight, bool isPk, UpdateStateWidgetState updateState) {
    // 麦位以上的大小,
    Size _cardBoxSize = chartTopSize();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      // findRenderObject 不能立即获取到大小，需要下一帧才能正确获取
      Size newCardBoxSize = chartTopSize();
      logger.v('_chartPartHeight addPostFrameCallback: ${_cardBoxSize}, $isPk');
      // 当两次获取的差值大于1.0才需要刷新
      if ((newCardBoxSize.height - _cardBoxSize.height).abs() > 1.0) updateState.update();
    });
    if (Platform.isIOS) {
      // 下一帧iOS还获取不到变化之后的size，所以延迟了
      Future.delayed(Duration(milliseconds: 100), () {
        Size newCardBoxSize = chartTopSize();
        if ((newCardBoxSize.height - _cardBoxSize.height).abs() > 1.0 && mounted) updateState.update();
      });
    }
    double bottomBarHeight = ScreenUtil().setWidth(106) + ScreenUtil().bottomBarHeight;
    double pkOffset = 0.0; //isPk ? 50.px : 0;
    double maxSafeHeight = ScreenUtil().screenHeight - _cardBoxSize.height - bottomBarHeight - pkOffset;
    logger.v('_chartPartHeight maxSafeHeight:$maxSafeHeight, ${_cardBoxSize}, $bottomBarHeight,$isPk');
    return math.max(0.0, maxSafeHeight);
/*
    double multiple = 1;
    double addHeight = 48.px;
    if (ScreenUtil().bottomBarHeight > 0) {
      multiple = 1;
    } else {
      multiple = 2.3;
    }

    logger.v(
        'before size: $_size, dynamicHeight: $dynamicHeight, multiple: $multiple');
    _getSize();
    logger.v(
        'after size: $_size, dynamicHeight: $dynamicHeight, multiple: $multiple');
    double height = _size != null
        ? ScreenUtil().screenHeightDp -
        ScreenUtil().bottomBarHeight -
        ScreenUtil().statusBarHeight * multiple -
        _size.height -
        ((tag == '-1' || tag == 'null')
            ? (dynamicHeight == 30
            ? _size.height * 2 / 6
            : dynamicHeight == 15
            ? _size.height * 1 / 6
            : ScreenUtil().setWidth(25))
            : ScreenUtil().setWidth(25))
        : 0;

    logger.v('chartPart height: $height');
    return height + addHeight;

 */
  }

  ///一些功能操作
  _funcInit(int? tag, {int? code}) {
    if (tag == -1) return;
    switch (tag) {
      case 1:
        openTp();
        break;
      case 2:
        openGreedy();
        break;
      case 3:
        YBDRoomUtil.showGiftSheet();
        break;
      case 4:
        Future.delayed(Duration(seconds: 3), YBDRoomUtil.takeAMic);
        break;
      case 5:
        YBDModuleCenter.instance().initBlock(YBDNavigatorHelper.flutter_room_list_page);
        break;
      case 6: // 打开ludo
        openLudo();
        break;
      case 7: // 打开webview游戏
        openWebViewGame(code!);
        break;
      case 8: // 打开lucky1000
        openLucky1000();
        break;
    }
  }

  void openWebViewGame(int code) {
    if (code > 0) {
      if (YBDTPGlobal.model != null) {
        for (YBDGameRecord? a in YBDTPGlobal.model!.record!) {
          if (a!.code == code.toString()) YBDRoomUtil.showH5Game(context, a.url);
        }
      }
    }
  }
  void openWebViewGameQGbbIoyelive(int code) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  openTp() {
    if (YBDTeenInfo.getInstance().open) return;
    YBDRoomSocketApi.getInstance().sendTeenPatti(roomId: _bloc()!.state.roomId);
    YBDRoomUtil.showTeenPattiPickerView(context);
  }

  void openGreedy() {
    if (YBDCommonUtil.couldShowGreedyGame()) YBDRoomUtil.showH5Game(context, GameType.GameTypeGreedy.info()?.url);
  }

  void openLudo() {
    if (YBDCommonUtil.shouldShowLudoGame()) YBDRoomUtil.showH5Game(context, GameType.GameTypeLudo.info()?.url);
  }

  void openLucky1000() {
    if (YBDCommonUtil.shouldShowLucky1000()) YBDRoomUtil.showH5Game(context, GameType.GameTypeLucky1000.info()?.url);
  }

  /// 获取[YBDRoomBloc]
  YBDRoomBloc? _bloc() {
    try {
      if (!mounted) {
        return null;
      }

      return BlocProvider.of<YBDRoomBloc>(context);
    } catch (e) {
      logger.l('get bloc error : $e');
      return null;
    }
  }

  double getSentGiftBottom() {
    if (Platform.isIOS) {
      logger.v("ScreenUtil().statusBarHeight ${ScreenUtil().statusBarHeight}");
      return ScreenUtil().statusBarHeight + 110;
    } else {
      return 120;
    }
  }

  Widget _giftSlideOut(bool isPk) {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
        buildWhen: (prev, current) => (current.roomTag != prev.roomTag),
        builder: (_, state) {
          double top = YBDRoomHelper.getAdTop(state.roomTag).px;

          if (isPk) {
            double pkHeight = ScreenUtil().setWidth(384);
            double barHeight =
                ScreenUtil().statusBarHeight > 0 ? ScreenUtil().statusBarHeight : ScreenUtil().statusBarHeight * 1.5;
            double titleHeight = ScreenUtil().setWidth(200);

            double pkBottom = pkHeight + barHeight + titleHeight;
            if (top < pkBottom) top = pkBottom;
            logger.v('pkBottom:$pkBottom, top:$top');
          }
          top -= isPk ? 440.px : 330.px;
          return Container(
            width: 720.px,
            margin: EdgeInsets.only(left: 30.px),
            padding: EdgeInsets.only(top: top),
            child: YBDRoomGiftSlideOut(),
          );
        });
  }
  void _giftSlideOutXuewnoyelive(bool isPk) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _getEmoji() async {
    YBDEmojiEntity? spEmojis = await YBDSPUtil.getEmojiEntity();
    if (spEmojis == null) {
      YBDEmojiEntity? emojiEntity = await ApiHelper.getAllEmoji();
      logger.v('22.12.22---emojiEntity:${emojiEntity?.toJson()}');
      await YBDSPUtil.save(Const.SP_EMOJI_DATA, emojiEntity);
    } else {
      logger.v('22.12.22---spEmojis:${spEmojis.toJson()}');
      YBDEmojiEntity? emojiEntity = await ApiHelper.getAllEmoji(updateTime: spEmojis.record?.first?.updateTime ?? '');
      logger.v('22.12.22---emojiEntity:${emojiEntity?.toJson()}');
      if (spEmojis.record == null) {
        await YBDSPUtil.save(Const.SP_EMOJI_DATA, emojiEntity);
      } else {
        List<String> spIds = [];
        // 更新
        for (var a in spEmojis.record ?? []) {
          spIds.add(a.id);
          for (var b in emojiEntity?.record ?? []) {
            if (a.id == b.id) a = b;
          }
        }
        // 添加
        logger.v('22.12.29---spIds:$spIds');
        for (var c in emojiEntity?.record ?? []) {
          if (!spIds.contains(c.id)) {
            spEmojis.record?.add(c);
          }
        }
        logger.v('22.12.22---spEmojis:${spEmojis.toJson()}');
        await YBDSPUtil.save(Const.SP_EMOJI_DATA, spEmojis);
      }
    }
  }
}
