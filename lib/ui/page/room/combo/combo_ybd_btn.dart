import 'dart:async';


// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_translate/flutter_translate.dart';
// import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
//
// import '../../../../base/base_state.dart';
// import '../../home/widget/bubble_widget.dart';
// import '../bloc/combo_bloc.dart';
// import '../util/room_util.dart';
//
// class ComboBtn extends StatefulWidget {
//   @override
//   ComboBtnState createState() => new ComboBtnState();
// }
//
// class ComboBtnState extends BaseState<ComboBtn> with TickerProviderStateMixin {
//   AnimationController animationController;
//   AnimationController bouncingController;
//
//   ComboStatus _comboStatus = ComboStatus.None;
//   @override
//   Widget myBuild(BuildContext context) {
//     if (_comboStatus == ComboStatus.None) return Container();
//     return Column(
//       children: <Widget>[
//         SizedBox(
//           height: (1 - bouncingController.value) * ScreenUtil().setWidth(14),
//         ),
//         YBDBubbleWidget(
//           ScreenUtil().setWidth(80),
//           ScreenUtil().setWidth(50),
//           Colors.purple,
//           BubbleArrowDirection.bottom,
//           innerPadding: 0.0,
//           arrHeight: ScreenUtil().setWidth(10),
//           child: Center(
//             child: Text(
//               translate('combo'),
//               style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(18)),
//             ),
//           ),
//         ),
//         SizedBox(
//           height: bouncingController.value * ScreenUtil().setWidth(14),
//         ),
//         GestureDetector(
//           onTap: () {
//             YBDRoomUtil.sendCombo(context);
//             context.read<YBDRoomComboBloc>().add(ComboStatus.Counting);
//           },
//           child: Container(
//             width: ScreenUtil().setWidth(100),
//             height: ScreenUtil().setWidth(100),
//             child: LiquidCircularProgressIndicator(
//               value: 1 - animationController.value,
//               //当前进度 0-1
//               valueColor: AlwaysStoppedAnimation(Color(0x9972E3F2)),
//               // 进度值的颜色.
//               backgroundColor: Colors.transparent,
//
//               direction: Axis.vertical, // 进度方向 (Axis.vertical = 从下到上, Axis.horizontal = 从左到右). 默认：Axis.vertical
//             ),
//           ),
//         ),
//         SizedBox(
//           height: ScreenUtil().setWidth(21),
//         ),
//       ],
//     );
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     animationController = AnimationController(vsync: this, duration: Duration(seconds: 3));
//     bouncingController = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
//     bouncingController.addStatusListener((status) {
//       if (status == AnimationStatus.completed) {
//         bouncingController.reverse();
//       }
//       if (status == AnimationStatus.dismissed) {
//         bouncingController.forward();
//       }
//     });
//     animationController.addListener(() {
//       setState(() {});
//     });
//     animationController.addStatusListener((status) {
//       if (status == AnimationStatus.completed || status == AnimationStatus.dismissed) {
//         context.read<YBDRoomComboBloc>().add(ComboStatus.None);
//         _comboStatus = ComboStatus.None;
//       } else {
//         _comboStatus = ComboStatus.Counting;
//       }
//       setState(() {});
//     });
//     context.read<YBDRoomComboBloc>().listen((ComboStatus status) {
//       print("rrrrrxxxxx${status.toString()}");
//       if (status != null) if (status == ComboStatus.Counting) {
//         animationController.reset();
//         animationController.forward();
//         bouncingController.forward();
//       } else {
//         bouncingController.reset();
//       }
//     });
//   }
// }
