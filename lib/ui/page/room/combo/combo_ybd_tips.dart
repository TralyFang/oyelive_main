import 'dart:async';


import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/widget/intl_ybd_container.dart';
import 'package:oyelive_main/ui/widget/intl_ybd_positioned.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/room_socket/message/base/message.dart';
import '../../../../common/room_socket/message/common/gift.dart';
import '../../../../common/room_socket/message/room_ybd_message_helper.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/string_ybd_util.dart';
import '../../../widget/round_ybd_avatar.dart';

class YBDComboTips extends StatefulWidget {
  final Function? onFadeCompleted;
  final YBDGift? gift;

  const YBDComboTips({Key? key, this.onFadeCompleted, this.gift}) : super(key: key);
  @override
  YBDComboTipsState createState() => new YBDComboTipsState();
}

class YBDComboTipsState extends BaseState<YBDComboTips> with TickerProviderStateMixin {
  YBDGift? comboGift;
  StreamSubscription<YBDMessage>? msgSubscription;

  late AnimationController controller;

  double comboScale = 0.0, numberScale = 0.0;

  final double COMBO_SCALE_DESITINATION = 1.01, NUMBER_SCALE_DESITINATION = 1.03;

  Animation<double>? comboAnimation, numberAnimation, showComboAnimation, showNumberAnimation;
  AnimationController? preShowComboController, comboFadeController;

  getColor() {
    int money = comboGift!.num! * comboGift!.price!;
    if (money <= 999) return Color(0x7f000000);
    if (money <= 9999) return Color(0x99FEE571);
    if (money <= 49999) return Color(0x993079FF);
    if (money <= 99999) return Color(0x99FFA0D3);
    return Color(0x99eeeeee);
  }

  getOpacity() {
    var result = 1 - controller.value;
    if (result > 0.8) {
      return 1.0;
    } else
      return result / 0.8;
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    if (comboGift == null) {
      return Container();
    }
    return IgnorePointer(
      child: Opacity(
        opacity: getOpacity(),
        child: Stack(
          // mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            // YBDIntlContainer(
            // height: ScreenUtil().setWidth(82),
            // padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(12)),
            // margin: EdgeInsets.only(left: ScreenUtil().setWidth(25), right: ScreenUtil().setWidth(128)),
            // decoration: BoxDecoration(
            // borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(200))), color: getColor()),
            // child: Row(
            //   mainAxisSize: MainAxisSize.min,
            //   children: [
            //     YBDRoundAvatar(
            //       comboGift.senderImg,
            //       avatarWidth: 58,
            //       needNavigation: false,
            //       userId: comboGift.fromUser,
            //       scene: "C",
            //     ),
            //     SizedBox(
            //       width: ScreenUtil().setWidth(18),
            //     ),
            //     Text(
            //       '${YBDStringUtil.subString(comboGift.sender, 18)}\n${translate('send_to')} ${YBDStringUtil.subString(comboGift.receiver, 10)}',
            //       style: TextStyle(fontSize: ScreenUtil().setSp(18), color: Colors.white),
            //       overflow: TextOverflow.ellipsis,
            //     ),
            //     SizedBox(
            //       width: ScreenUtil().setWidth(20),
            //     ),
            //     Image.network(
            //       YBDImageUtil.gift(context, comboGift.imgUrl, "B"),
            //       fit: BoxFit.fitHeight,
            //       height: ScreenUtil().setWidth(68),
            //     ),
            //     Column(
            //       crossAxisAlignment: CrossAxisAlignment.start,
            //       children: <Widget>[
            //         SizedBox(
            //           height: ScreenUtil().setWidth(8),
            //         ),
            //         Text(
            //           ' x ${comboGift.num}',
            //           style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
            //         ),
            //         Spacer(),
            //         Container(
            //           width: ScreenUtil().setWidth(100),
            //         )
            //       ],
            //     ),
            //   ],
            // ),
            // ),
            Container(
              height: ScreenUtil().setWidth(82),
              // width: 200.px,
              child: Text.rich(TextSpan(children: [
                TextSpan(
                  children: [
                    WidgetSpan(
                      child: Transform.scale(
                        scale: comboScale,
                        child: Text(
                          'combo',
                          style: TextStyle(
                              fontSize: ScreenUtil().setWidth(36), color: Color(0xffFFEE31), fontFamily: 'Combo'),
                        ),
                      ),
                    ),
                    TextSpan(
                      text: ' ',
                      style: TextStyle(
                        fontSize: ScreenUtil().setWidth(30),
                        color: Color(0xffFFEE31),
                      ),
                    ),
                    WidgetSpan(
                      child: Transform.scale(
                        scale: comboScale,
                        child: Text(
                          'x',
                          style: TextStyle(
                              fontSize: ScreenUtil().setWidth(36), color: Color(0xffFFEE31), fontFamily: 'Combo'),
                        ),
                      ),
                    ),
                    TextSpan(
                      text: ' ',
                      style: TextStyle(
                        fontSize: ScreenUtil().setWidth(30),
                        color: Color(0xffFFEE31),
                      ),
                    ),
                    WidgetSpan(
                      child: Transform.scale(
                        scale: numberScale,
                        child: Text(
                          '${comboGift?.combo}',
                          style: TextStyle(
                              fontSize: ScreenUtil().setWidth(60), color: Color(0xffFFEE31), fontFamily: 'Combo'),
                        ),
                      ),
                    ),
                  ],
                )
              ])),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildok8CWoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  setListener() {
    msgSubscription = YBDRoomMessageHelper.getInstance().messageOutStream.listen((YBDMessage event) {
      if (event is YBDGift) {
        if (!(event.fromUser == widget.gift?.fromUser)) return;
        comboGift = event;
        if (controller.status != AnimationStatus.dismissed) {
          controller.reset();
        }
        controller.forward();

        if (comboGift!.combo == 2) {
          runShowComboAnimate();
        } else if (comboGift!.combo! > 2) {
          runFadeComboAnimate();
        }
      }
    });
  }

  var listener;

  getResetAnimate() {}

  setShowAnimate() {
    preShowComboController = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);

    showComboAnimation = Tween<double>(begin: 0, end: COMBO_SCALE_DESITINATION)
        .chain(CurveTween(curve: Curves.ease))
        .animate(preShowComboController!);
    showNumberAnimation = Tween<double>(begin: 0, end: NUMBER_SCALE_DESITINATION)
        .chain(CurveTween(curve: Curves.ease))
        .animate(preShowComboController!);

    preShowComboController!.addListener(() {
      comboScale = showComboAnimation?.value ?? 0.0;
      numberScale = showNumberAnimation?.value ?? 0.0;
      if (comboScale == COMBO_SCALE_DESITINATION) {
        runFadeComboAnimate();
      }
    });
  }

  setFadeAnimate() {
    comboFadeController = AnimationController(duration: Duration(milliseconds: 2300), vsync: this);

    comboAnimation = TweenSequence<double>(
      <TweenSequenceItem<double>>[
        TweenSequenceItem<double>(
          tween: Tween<double>(begin: COMBO_SCALE_DESITINATION, end: 1.0).chain(CurveTween(curve: Curves.ease)),
          weight: 5,
        ),
        TweenSequenceItem<double>(
          tween: Tween<double>(begin: 1.0, end: 0).chain(CurveTween(curve: Curves.ease)),
          weight: 3,
        ),
      ],
    ).animate(comboFadeController!);

    numberAnimation = TweenSequence<double>(
      <TweenSequenceItem<double>>[
        TweenSequenceItem<double>(
          tween: Tween<double>(begin: NUMBER_SCALE_DESITINATION, end: 1.0).chain(CurveTween(curve: Curves.ease)),
          weight: 5,
        ),
        TweenSequenceItem<double>(
          tween: Tween<double>(begin: 1.0, end: 0).chain(CurveTween(curve: Curves.ease)),
          weight: 4,
        ),
      ],
    ).animate(comboFadeController!);

    comboFadeController!.addListener(() {
      numberScale = numberAnimation?.value ?? 0.0;
      comboScale = comboAnimation?.value ?? 0.0;
    });
    comboFadeController?.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        widget.onFadeCompleted?.call();
      }
    });
  }

  runFadeComboAnimate() {
    comboFadeController!.reset();
    comboFadeController!.forward();
  }

  runShowComboAnimate() {
    preShowComboController!.reset();
    preShowComboController!.forward();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setListener();
    controller = AnimationController(duration: Duration(milliseconds: 5000), vsync: this);
    controller.addListener(() {
      if (mounted) setState(() {});
    });
    setShowAnimate();
    setFadeAnimate();
  }
  void initStateyuRqVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    comboFadeController!.dispose();
    preShowComboController?.dispose();
    msgSubscription?.cancel();
    super.dispose();
  }
  void disposeNrDOKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDComboTips oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependencieszIjRRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
