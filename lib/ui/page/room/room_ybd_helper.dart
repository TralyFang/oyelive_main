import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import 'define/room_ybd_define.dart';
import 'service/live_ybd_service.dart';

class YBDRoomHelper {
  /// roomId 等于当前用户 id 进入开播页面, 否则进入房间
  /// [forceEnter] 从首页或开播按钮进房间时进入房间
  static Future<void> enterRoom(
    BuildContext? context,
    int? roomId, {
    bool forceEnter = false,
    String? tag = ROOM_DEFAULT_TAG,
    String? location,
    bool replace = false,
    List<YBDRoomInfo?>? roomList,
    int funcType = -1,
    int? gameCode,
  }) async {
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      itemName: 'room',
      location: location,
    ));

    if (YBDLiveService.instance.data.isInRoom && !forceEnter) {
      logger.w('already in room');
      return;
    }

    int? userId = YBDCommonUtil.storeFromContext()!.state.bean!.id;

    // 记录进入房间的页面
    YBDLiveService.instance.data.prePage = location ?? '';
    logger.v('userId: $userId, roomId: $roomId');

    if (userId == roomId) {
      logger.v('go to start live page funcType: $funcType');
      YBDNavigatorHelper.navigateTo(
        context,
        '${YBDNavigatorHelper.start_live_page}?funcType=$funcType',
      );
    } else {
      logger.v('go to room');
      // 标记已进入房间页面
      YBDLiveService.instance.data.isInRoom = true;

      // 进入房间页面时记录 roomId，作为加入频道的判断条件
      // YBDLiveService.instance.data.roomId = roomId;
      // YBDNavigatorHelper.navigateToRoom(
      //   context,
      //   '?roomId=$roomId&tag=$tag',
      //   replace: replace,
      // );

      // TODO: 测试代码 跳到房间列表页面
      // final randomRoom = RandomRoomList.randomRoomIdList().first;
      // 标记要进入的房间id，加入声网频道时要判断是否为当前要加入的房间
      YBDLiveService.instance.data.roomId = roomId;

      // 在线房间列表，房间滑动列表页面要用到
      YBDLiveService.instance.data.roomList = YBDRoomHelper.onLineRoomList(roomId, roomList);

      // 跳到房间列表页面
      YBDNavigatorHelper.navigateTo(
        context,
        '${YBDNavigatorHelper.flutter_room_list_page}?roomId=$roomId&tag=$tag&funcType=$funcType&gameCode=$gameCode',
        replace: replace,
      );
    }
  }

  /// 在线房间列表
  /// [currentRoom] 当前房间id
  /// [roomList]上个页面传过来的房间列表
  static List<YBDRoomInfo?> onLineRoomList(int? currentRoom, List<YBDRoomInfo?>? roomList, {int funcType = -1}) {
    List<YBDRoomInfo?> result = [];

    // 房间列表为空返回当前的房间
    if (null == roomList || roomList.isEmpty) {
      var roomInfo = YBDRoomInfo();
      roomInfo.id = currentRoom;
      result.add(roomInfo);
      return result;
    }

    // 当前房间索引
    var curIndex = YBDRoomHelper.roomPageIndex(currentRoom, roomList);
    var before = roomList.sublist(0, curIndex).where((element) => element!.live!).toList();
    var after = roomList.sublist(curIndex + 1, roomList.length).where((element) => element!.live!).toList();
    logger.v('current : $curIndex, before : ${before.length}, after : ${after.length}');

    // 在线房间列表
    result.addAll(before);
    result.add(roomList[curIndex]);
    result.addAll(after);

    // 屏蔽加密房间
    result.removeWhere((element) {
      // 保留当前房间
      return element!.isProtectRoom() && currentRoom != element.id;
    });

    return result;
  }

  /// 当前房间的索引
  static int roomPageIndex(int? roomId, List<YBDRoomInfo?>? roomList) {
    // 房间列表为空，默认返回0
    if (null == roomList || roomList.isEmpty) {
      return 0;
    }

    // 当前房间索引，默认为0
    var curIndex = 0;

    for (int i = 0; i < roomList.length; i++) {
      if (roomList[i]!.id == roomId) {
        curIndex = i;
        break;
      }
    }

    return curIndex;
  }

  /// 从开播页进到房间
  static Future<void> enterFromStartPage(BuildContext context,
      {String tag = ROOM_DEFAULT_TAG, int? funcType = -1}) async {
    logger.v('room start live funcType: $funcType'.subLog(step: 200, log: 'enterFromStartPage'));
    // 标记已进入房间页面
    YBDLiveService.instance.data.isInRoom = true;

    // 主播id
    int? roomId = YBDCommonUtil.storeFromContext()!.state.bean!.id;
    YBDLiveService.instance.data.roomId = roomId;

    // 在线房间列表，房间滑动列表页面要用到
    YBDLiveService.instance.data.roomList = YBDRoomHelper.onLineRoomList(roomId, null);

    // 跳到房间列表页面
    YBDNavigatorHelper.navigateTo(
      context,
      '${YBDNavigatorHelper.flutter_room_list_page}?roomId=$roomId&tag=$tag&funcType=$funcType',
      replace: true,
    );

    // YBDNavigatorHelper.navigateToRoom(
    //   context,
    //   '?roomId=$roomId&tag=$tag&type=1',
    //   replace: true,
    // );
  }

  /// 退出房间
  static void closeRoom() {
    // 标记已离开房间页面
    YBDLiveService.instance.data.isInRoom = false;
    // final popContext = YBDLiveService.instance.mainContext ?? context;
    logger.v(
        'close context : ${Get.context} routeSets: ${YBDObsUtil.instance().routeSets}, currentRoute: ${Get.currentRoute}');
    if (!Get.currentRoute.contains(YBDNavigatorHelper.flutter_room_list_page)) return;
    try {
      // 退回到上一个页面
      if (null == YBDLiveService.instance.data.prePage) {
        logger.v('prePage is null');
        YBDNavigatorHelper.popPage(Get.context!);
      }

      // 退到进入房间的页面
      Navigator.popUntil(Get.context!, (route) {
        if (route.isFirst) {
          logger.v('pop to first page : ${route.settings.name}');
          return true;
        }

        if (route.settings.name == YBDLiveService.instance.data.prePage) {
          logger.v('pop to page : ${route.settings.name}');
          return true;
        }

        return false;
      });
    } catch (e) {
      logger.e('pop room page error : $e');
    }
  }

  ///不同的类型 麦位高度不一样，广告的top也不一样
  static double getAdTop(String? tag) {
    double top = 580;
    switch (tag) {
      case '0':
        top = 635;
        break;
      case '1':
        top = 780;
        break;
      case '2':
        top = 740;
        break;
      case '3':
        top = 600;
        break;
      case '4':
        top = 810;
        break;
      case '5':
        top = 760;
        break;
      case '6':
        top = 770;
        break;
      default:
        top = 635;
        break;
    }
    print('getAdTop top:$top tag:$tag');
    return top;
  }
}
