import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:volume_watcher/volume_watcher.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../bloc/music_ybd_player_bloc.dart';

class YBDPlayerConsoleSheet extends StatefulWidget {
  YBDMusicPlayerBloc? bloc;

  YBDPlayerConsoleSheet(this.bloc);

  @override
  YBDPlayerConsoleSheetState createState() => new YBDPlayerConsoleSheetState();
}

class YBDPlayerConsoleSheetState extends BaseState<YBDPlayerConsoleSheet> {
  double musicVolume = 0.0;
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return BlocBuilder<YBDMusicPlayerBloc, YBDMusicPlayerStatus>(
      bloc: widget.bloc,
      builder: (_, YBDMusicPlayerStatus state) {
        return Container(
          margin: EdgeInsets.all(ScreenUtil().setWidth(20)),
          height: ScreenUtil().setWidth(380),
//      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(22)),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
          child: Column(
            children: [
              SizedBox(
                height: ScreenUtil().setWidth(28),
              ),
              Row(
                children: [
                  SizedBox(
                    width: ScreenUtil().setWidth(10),
                  ),
                  Image.asset(
                    'assets/images/icon_player.webp',
                    width: ScreenUtil().setWidth(88),
                    fit: BoxFit.fitWidth,
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(12),
                  ),
                  Container(
                    constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(400)),
                    child: Text(
                      state.selectedSong!.displayName!.replaceAll(".mp3", ''),
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontSize: ScreenUtil().setSp(36), color: Colors.black, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.scan_local_music);
                    },
                    child: Image.asset(
                      'assets/images/btn_music_list.webp',
                      width: ScreenUtil().setWidth(54),
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(22),
                  ),
                ],
              ),
              SizedBox(
                height: ScreenUtil().setWidth(42),
              ),
              Row(
                children: [
                  SizedBox(
                    width: ScreenUtil().setWidth(22),
                  ),
                  Image.asset(
                    'assets/images/player_volume.webp',
                    width: ScreenUtil().setWidth(48),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(20),
                  ),
                  VolumeWatcher(
                    onVolumeChangeListener: (double volume) {
                      musicVolume = volume;
                      setState(() {});
                    },
                    child: Text(
                      '${(musicVolume * 100).round()}%',
                      style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Color(0xff717171)),
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(50),
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(40),
                    width: ScreenUtil().setWidth(470),
                    child: FlutterSlider(
                      values: [musicVolume * 100],
                      handlerWidth: ScreenUtil().setWidth(28),
                      trackBar: FlutterSliderTrackBar(
                          inactiveDisabledTrackBarColor: Colors.white,
                          activeTrackBar: BoxDecoration(
                              color: Color(0xFF4EBCD4),
                              borderRadius: BorderRadius.horizontal(left: Radius.circular(ScreenUtil().setWidth(5)))),
                          inactiveTrackBar: BoxDecoration(
                              color: Color(0xFFD8D8D8),
                              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(5))))),
                      handler: FlutterSliderHandler(
                          child: Container(
                              width: ScreenUtil().setWidth(8),
                              height: ScreenUtil().setWidth(8),
                              decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xff5E95E7))),
                          decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white, boxShadow: [
                            BoxShadow(
                                offset: Offset(0, ScreenUtil().setWidth(2)),
                                blurRadius: ScreenUtil().setWidth(4),
                                color: Color(0x7f000000))
                          ])),
                      min: 0,
                      max: 100,
                      rangeSlider: false,
                      onDragging: (int handlerIndex, dynamic lowerValue, dynamic upperValue) {
//                    musicVolume = lowerValue / 100;
//
//                    setState(() {});
                        VolumeWatcher.setVolume(lowerValue / 100);
                      },
                      onDragCompleted: (int handlerIndex, dynamic lowerValue, dynamic upperValue) async {},
                    ),
                  )
                ],
              ),
              SizedBox(
                height: ScreenUtil().setWidth(48),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      widget.bloc!.add(PlayerAction.Pause);
                      widget.bloc!.add(PlayerAction.Pre);
                    },
                    child: Image.asset(
                      'assets/images/play_prev.webp',
                      width: ScreenUtil().setWidth(38),
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(140),
                  ),
                  GestureDetector(
                    onTap: () {
                      widget.bloc!.add(state.isPlaying! ? PlayerAction.Pause : PlayerAction.Play);
                    },
                    child: Image.asset(
                      state.isPlaying! ? 'assets/images/btn_pause.webp' : 'assets/images/btn_play.webp',
                      width: ScreenUtil().setWidth(104),
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(140),
                  ),
                  GestureDetector(
                    onTap: () {
                      widget.bloc!.add(PlayerAction.Pause);

                      widget.bloc!.add(PlayerAction.Next);
                    },
                    child: Image.asset(
                      'assets/images/player_next.webp',
                      width: ScreenUtil().setWidth(38),
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }
  void myBuild4PQo0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStateBKiv5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDPlayerConsoleSheet oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetekNsloyelive(YBDPlayerConsoleSheet oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
