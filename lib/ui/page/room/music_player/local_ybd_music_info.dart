import 'dart:async';


import 'package:flutter_audio_query/flutter_audio_query.dart';

class YBDLocalMusicInfo {
  List<SongInfo> songList;
  SongInfo selectedSong;

  YBDLocalMusicInfo(this.songList, this.selectedSong);
}
