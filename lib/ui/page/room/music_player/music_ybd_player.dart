import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../main.dart';
import '../../../../redux/app_ybd_state.dart';
import '../bloc/music_ybd_player_bloc.dart';
import '../bloc/room_ybd_bloc.dart';
import '../mic/mic_ybd_bloc.dart';
import '../service/live_ybd_service.dart';
import 'local_ybd_music_info.dart';
import 'player_ybd_console_sheet.dart';

class YBDMusicPlayer extends StatefulWidget {
  final int? roomId;
  YBDMusicPlayer({this.roomId});

  @override
  YBDMusicPlayerState createState() => new YBDMusicPlayerState();
}

class YBDMusicPlayerState extends BaseState<YBDMusicPlayer> with SingleTickerProviderStateMixin {
  AnimationController? controller;

  YBDMusicPlayerBloc? _bloc;
  late StreamSubscription<YBDMusicPlayerStatus> _blocSubscription;
  Store<YBDAppState>? _store;

  @override
  Widget myBuild(BuildContext context) {
    if (context.read<YBDRoomBloc>().roomId == _store!.state.bean!.id)
      return BlocBuilder<YBDMusicPlayerBloc, YBDMusicPlayerStatus>(builder: (_, YBDMusicPlayerStatus state) {
        return GestureDetector(
            onTap: () {
              YBDCommonTrack().commonTrack(YBDTAProps(location: 'music', module: YBDTAModule.liveroom));
              var onlineList = context
                  .read<YBDMicBloc>()
                  .state
                  .micBeanList!
                  .where((element) => element!.userId == _store!.state.bean!.id && element.micOnline!)
                  .toList();
              if (onlineList != null && onlineList.length != 0) {
                if (_bloc!.state.songList!.length == 0)
                  YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.scan_local_music);
                else
                  showModalBottomSheet(
                      backgroundColor: Colors.transparent,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(ScreenUtil().setWidth(8)),
                        ),
                      ),
                      context: context,
                      isScrollControlled: true,
                      builder: (BuildContext context) {
                        return YBDPlayerConsoleSheet(_bloc);
                      });
              } else {
                YBDToastUtil.toast(translate('take_mic_warning'));
              }
            },
            child: Container(
              margin: EdgeInsets.only(bottom: 20.px),
              child: (state.isPlaying ?? false)
                  ? Image.asset(
                      'assets/images/liveroom/icon_music_stop@2x.webp',
                      width: ScreenUtil().setWidth(80),
                    )
                  : Image.asset(
                      'assets/images/liveroom/icon_music_play@2x.webp',
                      width: ScreenUtil().setWidth(80),
                    ),
            ));
      });
    else
      return Container();
  }
  void myBuildULTdZoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _store = YBDCommonUtil.storeFromContext();
    _bloc = context.read<YBDMusicPlayerBloc>();
    _blocSubscription = _bloc!.stream.listen((state) async {
      if (null == controller) {
        logger.v('controller is null');
        return;
      }

      if (state.isPlaying!) {
        controller!.forward();
      } else {
        controller!.stop();
      }
    });

    controller = AnimationController(duration: const Duration(seconds: 4), vsync: this);
    controller!.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller!.repeat();
      }
    });

    eventBus.on<YBDLocalMusicInfo>().listen((YBDLocalMusicInfo event) {
      _bloc!.addSongList(event.songList);
      _bloc!.playIndex(event.songList.indexOf(event.selectedSong));
    });

    _timer = Timer.periodic(Duration(seconds: 1), (_) async {
      int duration = await YBDLiveService.instance.getAudioMixingDuration() ?? 0;
      int position = await YBDLiveService.instance.getAudioMixingCurrentPosition() ?? 0;
      // print('$position/$duration llxxll');
      if (duration != 0 && position != 0 && (position - duration).abs() < 1000) {
        // print('xxxNExt llxxll');
        _bloc!.add(PlayerAction.Next);
      }
    });

    context.read<YBDMicBloc>().stream.listen((state) {
      var onlineList;
      if (null != state.micBeanList) {
        onlineList = state.micBeanList!
            .where(
              (element) => element != null && element.userId == _store!.state.bean!.id && element.micOnline!,
            )
            .toList();
      }
      if (onlineList != null && onlineList.length != 0) {
      } else {
        _bloc!.add(PlayerAction.Pause);
      }
    });
  }
  void initStateo8O61oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    logger.v('page dispose');
    _timer?.cancel();
    controller!.dispose();
    _blocSubscription.cancel();
    _bloc!.close();
    super.dispose();
  }
  void disposesf9CJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
