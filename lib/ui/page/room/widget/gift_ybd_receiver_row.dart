import 'dart:async';


import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/bloc/gift_ybd_sheet_bloc.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc_state.dart';
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_bean.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/widget/gift_ybd_receiver.dart';

/// 房间礼物弹框顶部礼物接收者列表
class YBDGiftReceiverRow extends StatelessWidget {
  YBDGiftReceiverRow({
    Key? key,
    List<YBDReceiverUser>? specUsers,
  }) : super(key: key) {
    if (null != specUsers) {
      _receiverList = List.from(specUsers);
      _specifiedReceiver = true;
    }

    // 是否为pk房
    _isPkRoom = YBDLiveService.instance.data.pkInfo != null;
  }

  /// 礼物接收者列表
  /// 上麦用户或者指定的用户
  /// pk时隐藏礼物接收列表
  List<YBDReceiverUser> _receiverList = [];

  /// 是否指定礼物接收者
  /// 指定礼物接收者时只显示指定的接受者
  /// 未指定礼物接收者时显示上麦用户
  bool _specifiedReceiver = false;

  /// 是否正在pk
  /// pk时隐藏礼物接收列表
  late bool _isPkRoom;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          SizedBox(width: ScreenUtil().setWidth(50)),
          Text(
            translate('to'),
            style: TextStyle(
              fontSize: ScreenUtil().setSp(24),
              color: Colors.white,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(10)),
          Builder(builder: (context) {
            if (_specifiedReceiver) {
              // 从接收列表中移除当前用户，不能给自己送礼
              _removeCurrentUser();

              // 设置默认礼物接收者
              _setDefaultReceiver(context);

              // 指定收礼者的接收者列表
              return _receiverRow(context);
            }

            // 给麦位用户送礼
            return _onMicReceiverRow(context);
          }),
        ],
      ),
    );
  }
  void buildzUXg0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// [_receiverList]接收者列表
  Widget _receiverRow(BuildContext context) {
    return Expanded(
      child: Container(
        height: ScreenUtil().setWidth(66),
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemBuilder: (_, index) => YBDGiftReceiver(_receiverList[index]),
          separatorBuilder: (_, index) => SizedBox(width: ScreenUtil().setWidth(20)),
          itemCount: _receiverList.length,
        ),
      ),
    );
  }
  void _receiverRowmzwuEoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// PK和麦位用户接收者列表
  Widget _onMicReceiverRow(BuildContext context) {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
      builder: (_, YBDRoomState roomState) {
        return BlocBuilder<YBDMicBloc, YBDMicBlocState>(
          builder: (_, YBDMicBlocState state) {
            // 用麦位列表刷新接收列表
            _refreshReceiver(state, roomState);

            // 从接收列表中移除当前用户，不能给自己送礼
            _removeCurrentUser();

            // 设置默认礼物接收者
            _setDefaultReceiver(context);

            if (_isPkRoom) {
              // pk隐藏礼物接收者列表
              return SizedBox();
            }

            return _receiverRow(context);
          },
        );
      },
    );
  }
  void _onMicReceiverRowxe0fRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 用麦位列表刷新接收列表
  void _refreshReceiver(YBDMicBlocState state, YBDRoomState roomState) {
    List<YBDMicBean?>? micBeanS = state.micBeanList;
    if (micBeanS?.isEmpty ?? true) {
      // 没有麦位数据
      return;
    }

    // 清空旧数据
    _receiverList.clear();

    // 主播放到列表第一位
    _receiverList.add(YBDReceiverUser(
      userId: roomState.roomInfo?.id,
      img: roomState.roomInfo?.headimg,
      position: "Owner",
      name: roomState.talentInfo?.nickname ?? '',
    ));

    // 遍历麦位用户，把符合条件的麦位用户加到接收者列表
    for (var onMicUser in micBeanS!) {
      // 用户是否在麦位上
      if (onMicUser!.micOnline == true) {
        // 接收列表是否包含麦位用户，不包含则加到接收者列表
        if (!_containMicUser(onMicUser)) {
          // 添加麦位用户
          _receiverList.add(YBDReceiverUser(
            img: onMicUser.img,
            userId: onMicUser.userId,
            position: (onMicUser.micIndex! + 1).toString(),
            name: onMicUser.nikName,
          ));
        }
      }
    }
  }
  void _refreshReceiver6QDKEoyelive(YBDMicBlocState state, YBDRoomState roomState) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 接收列表是否包含麦位用户
  bool _containMicUser(YBDMicBean? micUser) {
    final result = _receiverList.firstWhereOrNull(
      (receiver) => receiver.userId == micUser!.userId,
    );

    return result != null;
  }

  /// 从接收列表中移除当前用户，不能给自己送礼
  void _removeCurrentUser() {
    _receiverList.removeWhere((element) => element.userId == YBDCommonUtil.storeFromContext()!.state.bean?.id);
  }
  void _removeCurrentUsergknzsoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 设置默认礼物接收者
  /// 当前没有选中接收者或当前接收者不在接收者列表中时默认选中第一个用户
  void _setDefaultReceiver(BuildContext context) {
    if (_receiverList.isEmpty) {
      // 礼物接收者置空
      context.read<YBDGiftSheetBloc>().state.receiverUser = null;
      return;
    }

    // 当前选中的接收者
    final currentReceiver = context.read<YBDGiftSheetBloc>().state.receiverUser;

    // 选中收礼者
    final selectDefault = () {
      context.read<YBDGiftSheetBloc>().add(YBDGiftPackageState(
            SheetAction.SelectReceiver,
            receiverUser: _receiverList.first,
          ));
    };

    // 当前收礼者为空
    if (null == currentReceiver) {
      selectDefault();
      return;
    }

    // 当前选中的收礼者是否在接收者列表中，返回null表示不在列表中
    final checkedCurrentReceiver = _receiverList.firstWhereOrNull(
      (element) => currentReceiver.userId == element.userId,
    );

    // 当前收礼者不在列表中重新选中收礼者
    if (null == checkedCurrentReceiver) {
      selectDefault();
    }
  }
  void _setDefaultReceiveruAcRsoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
