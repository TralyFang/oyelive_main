import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/module/room/entity/my_ybd_gift_entity.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../room_ybd_api_helper.dart';

/// 房间引导
class YBDRoomGuide {
  BuildContext context;

  /// 实例对象
  static YBDRoomGuide? _instance;

  /// 是否显示 send 引导
  var _shouldShowSend = false;

  /// 蒙板
  late OverlayEntry _maskOverlay;

  /// combo 按钮引导
  late OverlayEntry _comboOverlay;

  /// send 按钮引导
  late OverlayEntry _sendOverlay;

  /// 上麦按钮引导
  OverlayEntry? _takeMicOverlay;

  /// 点击上麦按钮的回调
  VoidCallback? _takeMicCallback;

  /// 点击 combo 按钮的回调
  VoidCallback? _comboCallback;

  /// 点击 send 按钮的回调
  VoidCallback? _sendCallback;

  YBDRoomGuide(this.context);

  /// 显示新手指引
  /// 背包有礼物：显示 send 引导 > 上麦引导
  /// 无礼物用户：显示 combo 按钮引导 > 上麦引导
  /// [takeMicCallback] 点击上麦按钮的回调
  /// [comboCallback] 点击 combo 按钮的回调
  /// [sendCallback] 点击 send 按钮的回调
  static Future<bool> showRoomGuide(
    BuildContext context, {
    VoidCallback? takeMicCallback,
    VoidCallback? comboCallback,
    VoidCallback? sendCallback,
  }) async {
    if (null != _instance) {
      // 这个方法只运行一次
      return false;
    }

    _instance = YBDRoomGuide(context);

    if (await _instance!._shouldShowGuide()) {
      _instance!._takeMicCallback = takeMicCallback;
      _instance!._comboCallback = comboCallback;
      _instance!._sendCallback = sendCallback;

      // 显示 send 引导
      _instance!._shouldShowSend = await _instance!._queryBackpack();
      _instance!._showMaskOverlay();

      if (_instance!._shouldShowSend) {
        if (null != _instance!._sendCallback) {
          // 显示礼物弹框
          _instance!._sendCallback!();
        }

        _instance!._showSendOverlay();
      } else {
        _instance!._showComboOverlay();
      }

      await _instance!._saveTipGuideDisplayRecord();
      return true;
    }

    return false;
  }

  /// 查询背包数据
  /// true 背包有数据，false 背包没数据
  Future<bool> _queryBackpack() async {
    YBDMyGiftEntity? entity = await YBDRoomApiHelper.queryMyGift(context);

    if (entity?.returnCode == Const.HTTP_SUCCESS) {
      return entity?.record?.isNotEmpty ?? false;
    }

    return false;
  }
  void _queryBackpackzA1Fioyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是否需要显示新手指引
  Future<bool> _shouldShowGuide() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);

    if (guide != null && guide.contains(Const.GUIDE_ROOM)) {
      logger.v('=================guide false ');
      return false;
    } else {
      logger.v('=================guide true ');
      return true;
    }
  }
  void _shouldShowGuideFYyXuoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 保存显示记录
  /// 安装 app 后只需要显示一次
  Future<void> _saveTipGuideDisplayRecord() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);

    if (guide == null || guide.isEmpty) {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_ROOM);
    } else {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_ROOM);
    }
  }
  void _saveTipGuideDisplayRecord4kqQJoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏新手指引
  void _hideGuide() {
    _hideComboOverlay();
    _hideSendOverlay();
    _hideTakeMicOverlay();
    _hideMaskOverlay();
  }

  /// 蒙板
  void _showMaskOverlay() {
    _maskOverlay = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              if (null == _takeMicOverlay) {
                // 隐藏 send 或 combo 引导
                // 只会显示其中的一个
                _hideSendOverlay();
                _hideComboOverlay();

                // 显示上麦引导
                _showTakeMicOverlay();
              } else {
                _hideGuide();
              }
            },
            child: Container(color: Colors.black.withOpacity(0.8)),
          )
        ],
      );
    });

    // 显示蒙板
    Overlay.of(context)!.insert(_maskOverlay);
  }
  void _showMaskOverlayensXxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏蒙板
  void _hideMaskOverlay() {
    try {
      _maskOverlay.remove();
    } catch (e) {
      logger.v('remove mask overlay error : $e');
    }
  }

  /// 上麦引导
  void _showTakeMicOverlay() {
    _takeMicOverlay = OverlayEntry(builder: (context) {
      return Container(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              left: ScreenUtil().setWidth(380),
              top: ScreenUtil().setWidth(386),
              child: Container(
                // 麦位图标
                width: ScreenUtil().setWidth(100),
                child: Image.asset(
                  'assets/images/liveroom/mic_unlock.webp',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              top: ScreenUtil().setWidth(374),
              left: ScreenUtil().setWidth(479),
              child: Container(
                // 大星星图标
                width: ScreenUtil().setWidth(24),
                child: Image.asset(
                  'assets/images/liveroom/room_guide_star.webp',
                ),
              ),
            ),
            Positioned(
              top: ScreenUtil().setWidth(403),
              left: ScreenUtil().setWidth(501),
              child: Container(
                // 小星星图标
                width: ScreenUtil().setWidth(13),
                child: Image.asset(
                  'assets/images/liveroom/room_guide_star.webp',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              left: ScreenUtil().setWidth(370),
              top: ScreenUtil().setWidth(386),
              child: GestureDetector(
                // 点击麦位的手势
                onTap: () {
                  logger.v('clicked take mic guide btn');
                  if (null != _takeMicCallback) {
                    _takeMicCallback!();
                  }

                  // 上麦引导为最后显示的引导，点完之后隐藏引导
                  _hideGuide();
                },
                child: Container(
                  decoration: BoxDecoration(),
                  width: ScreenUtil().setWidth(140),
                  height: ScreenUtil().setWidth(140),
                ),
              ),
            ),
            Positioned(
              // 文字部分
              top: ScreenUtil().setWidth(520),
              left: ScreenUtil().setWidth(124),
              child: GestureDetector(
                onTap: () {
                  logger.v('clicked take mic guide img');

                  // 隐藏引导
                  _hideGuide();
                },
                child: Container(
                  width: ScreenUtil().setWidth(437),
                  child: Image.asset(
                    'assets/images/liveroom/room_guide_mic.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    });

    /// 显示上麦指引
    Overlay.of(context)!.insert(_takeMicOverlay!);
  }
  void _showTakeMicOverlayzPv1joyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏上麦指引
  void _hideTakeMicOverlay() {
    try {
      _takeMicOverlay!.remove();
    } catch (e) {
      logger.v('remove follow overlay error : $e');
    }
  }

  /// 显示 combo 引导
  void _showComboOverlay() {
    _comboOverlay = OverlayEntry(builder: (context) {
      return Container(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              // 文字部分
              right: ScreenUtil().setWidth(56),
              bottom: ScreenUtil().setWidth(264),
              child: GestureDetector(
                onTap: () {
                  logger.v('clicked combo gift guide img');

                  // 隐藏 combo 指引
                  _hideComboOverlay();

                  // 显示上麦引导
                  _showTakeMicOverlay();
                },
                child: Container(
                  width: ScreenUtil().setWidth(448),
                  child: Image.asset(
                    'assets/images/liveroom/room_guide_combo.png',
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
            ),
            Positioned(
              //  combo 图标部分
              right: ScreenUtil().setWidth(18),
              bottom: ScreenUtil().setWidth(128),
              child: GestureDetector(
                onTap: () {
                  logger.v('clicked combo gift guide btn');

                  // 隐藏 combo 指引
                  _hideComboOverlay();

                  if (_shouldShowSend) {
                    // 显示 send 引导
                    _showSendOverlay();
                  } else {
                    _showTakeMicOverlay();
                  }
                },
                child: Container(
                  width: ScreenUtil().setWidth(140),
                  child: Image.asset(
                    'assets/images/liveroom/room_guide_combo_icon.webp',
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    });

    // 显示 combo 引导
    Overlay.of(context)!.insert(_comboOverlay);
  }
  void _showComboOverlayLjsb2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏 combo 引导
  void _hideComboOverlay() {
    try {
      _comboOverlay.remove();
    } catch (e) {
      logger.v('remove combo overlay error : $e');
    }
  }

  // 显示 send 引导
  void _showSendOverlay() {
    _sendOverlay = OverlayEntry(builder: (context) {
      return Container(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              // 文字部分
              right: ScreenUtil().setWidth(17),
              bottom: ScreenUtil().setWidth(95),
              child: GestureDetector(
                onTap: () {
                  logger.v('clicked send guide img');

                  // 隐藏 send 指引
                  _hideSendOverlay();

                  // 显示上麦引导
                  _showTakeMicOverlay();
                },
                child: Container(
                  width: ScreenUtil().setWidth(448),
                  child: Image.asset(
                    'assets/images/liveroom/room_guide_gift.png',
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
            ),
            Positioned(
              // send 按钮
              right: ScreenUtil().setWidth(24),
              bottom: ScreenUtil().setWidth(12),
              child: GestureDetector(
                onTap: () {
                  logger.v('clicked send guide btn');

                  // 隐藏送礼指引
                  _hideSendOverlay();

                  // 显示上麦引导
                  _showTakeMicOverlay();
                },
                child: Container(
                  width: ScreenUtil().setWidth(180),
                  height: ScreenUtil().setWidth(64),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Color(0xff47CDCC), Color(0xff5E94E7)]),
                    borderRadius: BorderRadius.all(
                      Radius.circular(ScreenUtil().setWidth(60)),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      translate('send'),
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.white,
                        fontSize: ScreenUtil().setSp(28),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    });

    // 显示 send 引导
    Overlay.of(context)!.insert(_sendOverlay);
  }
  void _showSendOverlayDzPsaoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏 send 引导
  void _hideSendOverlay() {
    try {
      _sendOverlay.remove();
    } catch (e) {
      logger.v('remove send overlay error : $e');
    }
  }
}
