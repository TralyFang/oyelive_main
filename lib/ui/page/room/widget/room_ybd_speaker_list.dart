import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_speaker_item.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';

/// 房间speaker列表
class YBDRoomSpeakerList extends StatefulWidget {
  /// 房间ID
  final int? roomId;

  YBDRoomSpeakerList({this.roomId});

  @override
  _YBDRoomSpeakerListState createState() => _YBDRoomSpeakerListState();
}

class _YBDRoomSpeakerListState extends BaseState<YBDRoomSpeakerList> {
  /// 列表刷新控制器
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
  }
  void initStateVEySboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }
  void disposecPNw4oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return SmartRefresher(
      controller: _refreshController,
      header: YBDMyRefreshIndicator.blackHeader,
      enablePullDown: true,
      enablePullUp: false,
      onRefresh: () {
        logger.v("refresh speakers list");
        // 刷新界面
        setState(() {});
        _refreshController.refreshCompleted();
        // _onRefresh();
      },
      child: _listContent(),
    );
  }
  void myBuildWVJRfoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _listContent() {
    final speakers = YBDCommonUtil.filterAbnormalUserId(YBDLiveService.instance.data.speakers);

    if (null == speakers || speakers.isEmpty) {
      return YBDPlaceHolderView(img: 'assets/images/empty/empty_my_profile.webp', text: 'No speaker', textColor: Colors.black87,);
    }

    return ListView.builder(
      itemCount: speakers.length,
      itemBuilder: (_, index) {
        return Container(
          height: ScreenUtil().setWidth(130),
          child: YBDRoomSpeakerItem(
            uid: speakers[index],
            roomId: widget.roomId ?? 0,
          ),
        );
      },
    );
  }
  void _listContentBFGmFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
