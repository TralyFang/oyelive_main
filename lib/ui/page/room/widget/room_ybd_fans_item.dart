import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/room_socket/entity/fans_ybd_info.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

class YBDRoomFansItem extends StatelessWidget {
  final int _index;
  final YBDFansInfo _fansInfo;
  final double _height;
  bool isStreamerViewing;
  YBDRoomFansItem(this._index, this._fansInfo, this._height, this.isStreamerViewing);

  @override
  Widget build(BuildContext context) {
    return _index == 0
        ? YBDtopOneWidget(
            height: _height,
            fansInfo: _fansInfo,
            isStreamerViewing: isStreamerViewing,
          )
        : Container(
            decoration: BoxDecoration(
              color: Colors.white10,
            ),
            padding: EdgeInsets.only(
              left: ScreenUtil().setWidth(22),
              bottom: ScreenUtil().setWidth(22),
              right: ScreenUtil().setWidth(22),
            ),
            child: Column(
              children: [
                _index == 1
                    ? Container()
                    : Container(
                        width: ScreenUtil().setWidth(600),
                        height: ScreenUtil().setWidth(1),
                        color: Const.COLOR_BORDER,
                      ),
                SizedBox(height: ScreenUtil().setWidth(8)),
                Row(
                  children: <Widget>[
                    Container(
                      height: ScreenUtil().setWidth(100),
                      child: Stack(
                        children: <Widget>[
                          Center(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(18)),
                              child:
                                  YBDRoundAvatar(_fansInfo.img, scene: "B", avatarWidth: 82, userId: _fansInfo.senderId),
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            child: Container(
                              width: ScreenUtil().setWidth(82),
                              alignment: Alignment.center,
                              child: Container(
                                alignment: Alignment.center,
                                width: ScreenUtil().setWidth(50),
                                height: ScreenUtil().setWidth(20),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(10))),
                                    gradient: LinearGradient(colors: [
                                      Color(0xffF1BE61),
                                      Color(0xffCA9E3C),
                                    ], begin: Alignment.topLeft, end: Alignment.bottomRight)),
                                child: Text('${_index + 1}',
                                    style: TextStyle(fontSize: ScreenUtil().setSp(18), color: Colors.white)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(370)),
                              child: Text(
                                _fansInfo.nickname ?? '-',
                                overflow: TextOverflow.ellipsis,
                                style:
                                    TextStyle(color: Colors.white.withOpacity(0.7), fontSize: ScreenUtil().setSp(24)),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: ScreenUtil().setWidth(20))),
                            Row(
                              children: <Widget>[
                                YBDLevelTag(_fansInfo.level),
                                SizedBox(width: ScreenUtil().setWidth(10)),
                                Container(
                                  // 性别标签
                                  height: ScreenUtil().setWidth(20),
                                  child: Image.asset(
                                    _fansInfo.sex == 2
                                        ? "assets/images/tag_male.webp"
                                        : "assets/images/tag_female.webp",
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    _index == 1
                        ? Image.asset('assets/images/icon_top2.webp',
                            width: ScreenUtil().setWidth(68), height: ScreenUtil().setWidth(78))
                        : _index == 2
                            ? Image.asset('assets/images/icon_top3.webp',
                                width: ScreenUtil().setWidth(68), height: ScreenUtil().setWidth(78))
                            : Container(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Image.asset(
                          isStreamerViewing ? 'assets/images/icon_gems.webp' : 'assets/images/topup/y_top_up_beans@2x.webp',
                          width: ScreenUtil().setWidth(26),
                          height: ScreenUtil().setWidth(26),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(4)),
                        Text(YBDNumericUtil.format(_fansInfo.score)!,
                            style: TextStyle(color: Color(0xffFDE48B), fontSize: ScreenUtil().setSp(24)))
                      ],
                    )
                  ],
                ),
              ],
            ),
          );
  }
  void build4F04hoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDtopOneWidget extends StatelessWidget {
  const YBDtopOneWidget({
    Key? key,
    required double height,
    required YBDFansInfo fansInfo,
    required bool isStreamerViewing,
  })  : _height = height,
        _fansInfo = fansInfo,
        _isStreamerViewing = isStreamerViewing,
        super(key: key);
  final bool _isStreamerViewing;
  final double _height;
  final YBDFansInfo _fansInfo;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: _height,
      // constraints: BoxConstraints(
      //   minHeight: ScreenUtil().setWidth(1140),
      // ),
      // color: Colors.cyan,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: ScreenUtil().setWidth(164)),
            child: Container(
                decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
              color: Colors.white10,
            )),
          ),
          Positioned(
              child: Image.asset('assets/images/star.png', width: ScreenUtil().setWidth(60), fit: BoxFit.cover),
              top: ScreenUtil().setWidth(90),
              left: ScreenUtil().setWidth(198)),
          Positioned(
              child: Image.asset('assets/images/star.png', width: ScreenUtil().setWidth(80), fit: BoxFit.cover),
              top: ScreenUtil().setWidth(156),
              left: ScreenUtil().setWidth(40)),
          Positioned(
              child: Image.asset('assets/images/star.png', width: ScreenUtil().setWidth(60), fit: BoxFit.cover),
              top: ScreenUtil().setWidth(128),
              left: ScreenUtil().setWidth(500)),
          Positioned(
              child: Image.asset('assets/images/star.png', width: ScreenUtil().setWidth(120), fit: BoxFit.cover),
              top: ScreenUtil().setWidth(220),
              left: ScreenUtil().setWidth(530)),
          Column(
            children: <Widget>[
              SizedBox(height: ScreenUtil().setWidth(30)),
              Container(
                width: ScreenUtil().screenWidth,
                height: ScreenUtil().setWidth(280),
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: YBDRoundAvatar(
                        YBDImageUtil.avatar(context, _fansInfo.img ?? '', _fansInfo.senderId, scene: 'C'),
                        avatarWidth: 194,
                        userId: _fansInfo.senderId,
                      ),
                    ),
                    Positioned(
                      top: 0,
                      child: Container(
                        width: ScreenUtil().screenWidth - ScreenUtil().setWidth(60),
                        alignment: Alignment.center,
                        child: Image.asset('assets/images/icon_crown.webp',
                            width: ScreenUtil().setWidth(122), fit: BoxFit.cover),
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      child: Container(
                        width: ScreenUtil().screenWidth - ScreenUtil().setWidth(60),
                        alignment: Alignment.center,
                        child: Image.asset('assets/images/topup/y_top1.webp',
                            width: ScreenUtil().setWidth(260), fit: BoxFit.cover),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(8)),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(_fansInfo.nickname!, style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28))),
                    SizedBox(width: ScreenUtil().setWidth(10)),
                    YBDLevelTag(_fansInfo.level),
                    SizedBox(width: ScreenUtil().setWidth(10)),
                    Container(
                      // 性别标签
                      height: ScreenUtil().setWidth(20),
                      child: Image.asset(
                        _fansInfo.sex == 2 ? "assets/images/tag_male.webp" : "assets/images/tag_female.webp",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(28)),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Sent', style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(20))),
                    SizedBox(width: ScreenUtil().setWidth(6)),
                    Image.asset(
                      _isStreamerViewing ? 'assets/images/icon_gems.webp' : 'assets/images/topup/y_top_up_beans@2x.webp',
                      width: ScreenUtil().setWidth(26),
                      height: ScreenUtil().setWidth(26),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(6)),
                    Text(YBDNumericUtil.format(_fansInfo.score)!,
                        style: TextStyle(
                            color: Color(0xffFDE48B), fontSize: ScreenUtil().setSp(28), fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(60)),
            ],
          ),
        ],
      ),
    );
  }
  void build0HYAyoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
