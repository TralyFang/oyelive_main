import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/room/entity/my_ybd_gift_entity.dart';
import 'package:oyelive_main/ui/page/room/room_ybd_api_helper.dart';

class YBDRoomDialogGuide {
  YBDRoomDialogGuide._();
  static final YBDRoomDialogGuide _instance = YBDRoomDialogGuide._();
  static YBDRoomDialogGuide get instance => _instance;

  /// 是否显示过弹框
  var _showed = false;

  /// 点击上麦按钮的回调
  VoidCallback? _takeMicCallback;

  /// 点击 combo 按钮的回调
  VoidCallback? _comboCallback;

  /// 点击 send 按钮的回调
  VoidCallback? _sendCallback;

  /// 显示新手指引
  /// 背包有礼物：显示 send 引导 > 上麦引导
  /// 无礼物用户：显示 combo 按钮引导 > 上麦引导
  /// [takeMicCallback] 点击上麦按钮的回调, 暂时没用到
  /// [comboCallback] 点击 combo 按钮的回调, 暂时没用到
  /// [sendCallback] 点击 send 按钮的回调, 弹出房间送礼弹框
  Future<void> showRoomGuide({
    VoidCallback? takeMicCallback,
    VoidCallback? comboCallback,
    VoidCallback? sendCallback,
  }) async {
    // app启动后只显示一次引导
    if (_showedGuide()) return;
    _showed = true;

    if (!(await _shouldShowGuide())) {
      logger.v('_shouldShowGuide: false');
      // 不显示引导
      return;
    }

    _takeMicCallback = takeMicCallback;
    _comboCallback = comboCallback;
    _sendCallback = sendCallback;

    // 显示 send 引导
    var shouldShowSend = await _queryBackpack();

    if (shouldShowSend) {
      // 房间底部弹出礼物弹框
      _sendCallback?.call();
    }
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.OPEN_PAGE,
      location: YBDLocationName.ROOM_PAGE,
      itemName: YBDItemName.GUIDE_LIVE_ROOM,
    ));
    showDialog(
      barrierDismissible: false,
      context: Get.context!,
      builder: (BuildContext builder) {
        return _YBDRoomGuide(
          shouldShowSend: shouldShowSend,
        );
      },
    );

    await _saveTipGuideDisplayRecord();
  }

  /// 已经展示过房间引导
  bool _showedGuide() {
    return _showed;
  }
  void _showedGuideAexutoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 测试用：显示房间引导
  /// 进房间时调用
  void clearDisplayRecord() {
    _showed = false;
  }
  void clearDisplayRecordCNPQ2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是否需要显示新手指引
  Future<bool> _shouldShowGuide() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);
    if (guide != null && guide.contains(Const.GUIDE_ROOM)) {
      logger.v('=================guide false ');
      return false;
    } else {
      logger.v('=================guide true ');
      return true;
    }
  }
  void _shouldShowGuideRQvHcoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询背包数据
  /// true 背包有数据，false 背包没数据
  Future<bool> _queryBackpack() async {
    YBDMyGiftEntity? entity = await YBDRoomApiHelper.queryMyGift(Get.context);

    if (entity?.returnCode == Const.HTTP_SUCCESS) {
      return entity?.record?.isNotEmpty ?? false;
    }

    return false;
  }
  void _queryBackpackiIyMvoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 保存显示记录
  /// 安装 app 后只需要显示一次
  Future<void> _saveTipGuideDisplayRecord() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);

    if (guide == null || guide.isEmpty) {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_ROOM);
    } else {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_ROOM);
    }
  }
  void _saveTipGuideDisplayRecordUSHgBoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class _YBDRoomGuide extends StatefulWidget {
  final bool shouldShowSend;

  _YBDRoomGuide({this.shouldShowSend = false});

  @override
  State<StatefulWidget> createState() => _YBDRoomGuideState();
}

class _YBDRoomGuideState extends State<_YBDRoomGuide> {
  /// 要显示的引导队列
  List<Widget> _guideQueue = [];

  @override
  void initState() {
    super.initState();

    if (widget.shouldShowSend) {
      _guideQueue.add(_sendGiftView());
    } else {
      _guideQueue.add(_comboView());
    }

    _guideQueue.add(_takeMicView());
  }
  void initStateMxsPjoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the room dialog bg");
      },
      child: Material(
        color: Colors.black.withOpacity(0.2),
        child: Container(
          width: ScreenUtil().screenWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              WillPopScope(
                onWillPop: () async {
                  logger.v("clicked the room guide back btn");
                  _popGuide();
                  return false;
                },
                child: Expanded(
                  child: Container(
                    width: double.infinity,
                    // height: ScreenUtil().screenHeight - ScreenUtil().setWidth(64),
                    // color: Colors.green,
                    child: Stack(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            logger.v("clicked the room guide bg");
                            _popGuide();
                          },
                          child: Container(
                            color: Colors.black.withOpacity(0.6),
                            child: Container(
                              child: _currentGuideWidget(),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildbvFaloyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _currentGuideWidget() {
    if (_guideQueue.isNotEmpty) {
      return _guideQueue.first;
    }
    // 避免空的存在
    logger.e("room guide empty page abnormal: $_guideQueue");
    return _takeMicView();
  }

  // 显示 send 引导
  Widget _sendGiftView() {
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.OPEN_PAGE,
      location: YBDLocationName.ROOM_PAGE,
      itemName: YBDItemName.GUIDE_LIVE_ROOM_SEND_GIFT,
    ));
    YBDCommonTrack().commonTrack(YBDTAProps(
      location: YBDTAClickName.guide_send_gift,
      module: YBDTAModule.guide,
    ));
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            // 文字部分
            // right: ScreenUtil().setWidth(17),
            bottom: ScreenUtil().setWidth(30),
            child: GestureDetector(
              onTap: () {
                logger.v('clicked send guide img');
                _popGuide();
              },
              child: Container(
                width: ScreenUtil().setWidth(720),
                child: Image.asset(
                  'assets/images/liveroom/live_2@2x.webp',
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void _sendGiftViewcvZV8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 上麦引导
  Widget _takeMicView() {
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.OPEN_PAGE,
      location: YBDLocationName.ROOM_PAGE,
      itemName: YBDItemName.GUIDE_LIVE_ROOM_TAKE_MIC,
    ));
    YBDCommonTrack().commonTrack(YBDTAProps(
      location: YBDTAClickName.guide_take_mic,
      module: YBDTAModule.guide,
    ));
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            // 文字部分
            top: ScreenUtil().setWidth(350),
            left: ScreenUtil().setWidth(140),
            child: GestureDetector(
              onTap: () {
                logger.v('clicked take mic guide img');
                _popGuide();
              },
              child: Container(
                width: ScreenUtil().setWidth(384),
                child: Image.asset(
                  'assets/images/liveroom/live_3@2x.webp',
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void _takeMicViewD5scGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 显示 combo 引导
  Widget _comboView() {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            //  combo 图标部分
            right: ScreenUtil().setWidth(18),
            bottom: ScreenUtil().setWidth(28),
            child: GestureDetector(
              onTap: () {
                logger.v('clicked combo gift guide btn');
                _popGuide();
              },
              child: Container(
                width: ScreenUtil().setWidth(386),
                child: Image.asset(
                  'assets/images/liveroom/live_1@2x.webp',
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// 移除已显示的引导
  void _popGuide() {
    if (_guideQueue.isNotEmpty) {
      // 移除已显示的引导
      _guideQueue.removeAt(0);
    }

    if (_guideQueue.isEmpty) {
      // 退出引导
      Navigator.pop(context);
    } else {
      // 显示下一个引导
      setState(() {});
    }
  }
  void _popGuideEDHycoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
