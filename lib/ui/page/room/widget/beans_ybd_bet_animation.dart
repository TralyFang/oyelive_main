import 'dart:async';


import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/constant/teen_ybd_patti_const.dart';


class YBDBeansBetAnimateWidget extends StatefulWidget{
  final Offset startOffset;
  final Offset endOffset;
  final Widget animateWidget;
  final Function? animateCallback;
  final int duration;
  final double? controlRatio;


  YBDBeansBetAnimateWidget(this.startOffset,this.endOffset,this
      .animateWidget,this.animateCallback,{int duration = -1,double?
  controlRatio})
      : this.duration = duration,this.controlRatio = controlRatio,assert(animateWidget
      != null);


  @override
  State<StatefulWidget> createState() {
    return YBDBeansBetAnimateWidgetState();
  }

  static void beansBetAnimation({required GlobalKey beginKey, required GlobalKey endKey, Widget? animationWidget, Function()? completedCallback}) {

    // debugPrint("****>>>>>$beginKey=====$endKey=====${beginKey.currentContext}=====$animationWidget====");

    // 需要在这里做下注动画
    // 这里需要转换下余额金豆的坐标。
    RenderBox renderBox = beginKey.currentContext!.findRenderObject() as RenderBox;
    Offset beginfOfset =  renderBox.localToGlobal(Offset.zero);

    RenderBox endRenderBox = endKey.currentContext!.findRenderObject() as RenderBox;
    Offset endfOfset =  endRenderBox.localToGlobal(Offset(endRenderBox.size.width/4, endRenderBox.size.height/4));

    // ===*****>>>Offset(15.7, 735.1)====Offset(138.8, 586.4)

    Function? callback;
    OverlayEntry entry = OverlayEntry(
        builder: (ctx) {
          return YBDBeansBetAnimateWidget(
            beginfOfset, // 开始坐标
            endfOfset, // 结束坐标
            animationWidget!, // 动画widget
            callback, // 回调
          );
        });
    callback = (status) {
      if (status == AnimationStatus.completed) {
        completedCallback?.call();
        entry.remove();
      }
    };
    // 需要移动的widget key
    Overlay.of(endKey.currentContext!)!.insert(entry);
  }

}

class YBDBeansBetAnimateWidgetState extends State<YBDBeansBetAnimateWidget> with
    SingleTickerProviderStateMixin {

  YBDBeansBetAnimateWidgetState();

  AnimationController? _controller;
  late Animation _animation;

  late Animation<double> scaleAnimation;
  late Animation<double> fadeAnimation;


  double widgetLeft = 0.0;
  double widgetTop = 0.0;

  double animateLen = 0.0;
  late PathMetric animatePM ;



  @override
  void initState() {

    super.initState();
    _controller = AnimationController(vsync: this,duration: Duration(milliseconds:
    widget.duration <=0 ? 300 : widget.duration));

    scaleAnimation = Tween(begin: 1.0,  end: 0.3).animate(_controller!);
    fadeAnimation = Tween(begin: 1.0, end: 0.6).animate(
        CurvedAnimation(
          parent: _controller!,
          curve: Interval(
            0.8, 1.0, //间隔，后40%的动画时间
            curve: Curves.easeOut)
        )
    );



    WidgetsBinding.instance?.addPostFrameCallback((_){
      if(widget.startOffset == null || widget.endOffset == null){
        widget.animateCallback!(AnimationStatus.completed);
        return;
      }
      generateAnimateAttr();
      startAnimation();
    });
    widgetLeft = widget.startOffset.dx ==null ? widgetLeft : widget.startOffset.dx;
    widgetTop = widget.startOffset.dy == null ? widgetTop : widget.startOffset.dy;

  }
  void initStateUHNfKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  generateAnimateAttr(){
    Path path = getPath();
    PathMetrics pms = path.computeMetrics();
    animatePM = pms.elementAt(0);
    animateLen = animatePM.length;

  }

  Path getPath() {

    double controlPointX = generateControlPointX();
    Path path = Path();
    path.moveTo(widget.startOffset.dx,widget.startOffset.dy);
    path.lineTo(widget
        .endOffset.dx, widget.endOffset.dy);
    // path.quadraticBezierTo(controlPointX,
    //     widget.startOffset.dy, widget
    //         .endOffset.dx, widget.endOffset.dy);

    return path;
  }


  double generateControlPointX(){
    double controlRatio = widget.controlRatio ?? 2;
    double sX = widget.startOffset.dx;
    double eX = widget.endOffset.dx;
    double largeX = sX > eX ? sX : eX;
    double smallX = sX < eX ? sX : eX;
    double controlX = largeX / controlRatio;
    if(controlX < smallX){
      controlX = ((largeX - smallX) / controlRatio) + smallX;
    }
    return controlX;
  }



  @override
  Widget build(BuildContext context) {

    return Positioned(
      left: widgetLeft,
      top: widgetTop,
      child: Opacity(
        opacity: fadeAnimation.value,
          child: Transform.scale(
            scale: scaleAnimation.value,
            child: widget.animateWidget
          )
      ),
    );
  }
  void builduMkhWoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  


  startAnimation(){

    // 位移+缩放+渐变
    _animation = Tween(begin: 0.0,end: animateLen).animate(_controller!)
      ..addListener((){
        double tempStart = _animation.value;
        Tangent? t = animatePM.getTangentForOffset(tempStart);
        setState(() {
          widgetLeft = t!.position.dx;
          widgetTop = t.position.dy;
        });
      })
      ..addStatusListener((status){
        widget.animateCallback!(status);
        if(status == AnimationStatus.completed){
          _controller?.stop();
          _controller?.dispose();
        }
      });

    _controller!.forward();
  }

}