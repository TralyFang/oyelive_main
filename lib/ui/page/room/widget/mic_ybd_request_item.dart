import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/widget/level_ybd_tag.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../widget/round_ybd_avatar.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import '../entity/mic_ybd_request_info.dart';
import '../mic/mic_ybd_bloc.dart';
import '../util/room_ybd_util.dart';

/// 麦位申请列表项
class YBDMicRequestItem extends StatefulWidget {
  /// 房间ID
  final int roomId;

  /// 用户信息
  final YBDMicRequestInfo _micRequestInfo;

  final int index;

  YBDMicRequestItem(this.index, this.roomId, this._micRequestInfo);

  @override
  State<StatefulWidget> createState() => _YBDMicRequestItemState();
}

class _YBDMicRequestItemState extends State<YBDMicRequestItem> {
  late YBDMicBloc _micBloc;

  @override
  void initState() {
    super.initState();
    _micBloc = context.read<YBDMicBloc>();
  }
  void initStategFS7aoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(130),
      width: ScreenUtil().screenWidth,
      child: Row(
        children: <Widget>[
          SizedBox(width: ScreenUtil().setWidth(30)),
          Container(
            width: ScreenUtil().setWidth(37),
            height: ScreenUtil().setWidth(37),
            child: Center(
              child: Text(
                '${widget.index}',
                style: TextStyle(
                  color: Colors.black.withOpacity(0.7),
                  fontSize: ScreenUtil().setSp(24),
                ),
              ),
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(30)),
          BlocBuilder<YBDMicBloc, YBDMicBlocState>(
            builder: (context, state) {
              return GestureDetector(
                onTap: () {
                  logger.v('clicked ${widget._micRequestInfo.userInfo!.id}');
                  YBDRoomUtil.showMiniProfileDialog(
                    roomId: widget.roomId,
                    userId: widget._micRequestInfo.userInfo!.id,
                  );
                },
                child: YBDRoundAvatar(
                  widget._micRequestInfo.userInfo!.headimg,
                  scene: "B",
                  sex: widget._micRequestInfo.userInfo!.sex,
                  avatarWidth: 80,
                  userId: widget._micRequestInfo.userInfo!.id,
                  lableSrc: (widget._micRequestInfo.userInfo?.vip ?? 0) == 0
                      ? ""
                      : "assets/images/vip_lv${widget._micRequestInfo.userInfo!.vip}.png",
                  labelWitdh: 28,
                  needNavigation: false,
                ),
              );
            },
          ),
          SizedBox(width: ScreenUtil().setWidth(26)),
          // 用户信息
          _infoContainer(),
          Expanded(child: Container()),
          _rounderBtn(1),
          SizedBox(width: ScreenUtil().setWidth(30)),
          _rounderBtn(0),
          SizedBox(width: ScreenUtil().setWidth(30)),
        ],
      ),
    );
  }
  void buildjZBRkoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 昵称，性别标签，用户等级
  Widget _infoContainer() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: [
              Container(
                // 昵称
                constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(190)),
                child: Text(
                  widget._micRequestInfo.userInfo!.nickname ?? '',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Color(0xff3A3A3A),
                    fontSize: ScreenUtil().setSp(28),
                  ),
                ),
              ),
              SizedBox(width: 10.px),
              Container(
                // 性别标签
                height: ScreenUtil().setWidth(23),
                child: Image.asset(
                  widget._micRequestInfo.userInfo!.sex == 2
                      ? 'assets/images/liveroom/icon_male@2x.webp'
                      : 'assets/images/liveroom/icon_female@2x.webp',
                  fit: BoxFit.cover,
                ),
              ),
            ],
          ),
          SizedBox(height: ScreenUtil().setWidth(11)),
          Row(
            children: <Widget>[
              // VIP
              // SizedBox(width: ScreenUtil().setWidth(20)),
              YBDLevelTag(widget._micRequestInfo.userInfo!.level),
            ],
          )
        ],
      ),
    );
  }
  void _infoContainerFqYXFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 同意/拒绝按钮
  /// type 0, 同意；1，拒绝
  Widget _rounderBtn(int type) {
    Function onTap;

    if (type == 0) {
      onTap = () {
        _micBloc.requestMic(BlocProvider.of<YBDRoomBloc>(context).roomId ?? -1, 3, widget._micRequestInfo.userInfo!.id ?? -1,
            widget._micRequestInfo.micIndex!);
        Navigator.pop(context);
      };
    } else {
      onTap = () {
        // 调拒绝接口
        _micBloc.rejectMicApply(widget._micRequestInfo);
        _micBloc.add(MicEvent.Refresh);
        Navigator.pop(context);
      };
    }

    return YBDScaleAnimateButton(
      onTap: onTap as void Function()?,
      child: Container(
        child: Center(
          child: Image.asset(
            type == 1 ? 'assets/images/liveroom/icon_reject@2x.webp' : 'assets/images/liveroom/icon_agree@2x.webp',
            width: 40.px,
            height: 40.px,
          ),
        ),
      ),
    );
  }
  void _rounderBtnAzDnWoyelive(int type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
