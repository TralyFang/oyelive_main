import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/wear_ybd_badge_item.dart';
import 'package:oyelive_main/ui/page/room/widget/fans_ybd_sheet.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_agent_share.dart';
import 'package:oyelive_main/ui/widget/sharing_ybd_widget.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/room_ybd_level_tag.dart';
import '../../profile/user_profile/bloc/follow_ybd_user_bloc.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import '../bloc/room_ybd_bloc.dart';
import '../bloc/room_ybd_bloc_state.dart';
import '../define/room_ybd_define.dart';
import '../pk/match_ybd_btn.dart';
import '../pk/pk_ybd_normal_dialog.dart';
import '../service/live_ybd_service.dart';
import '../util/room_ybd_util.dart';
import 'room_ybd_rank_avatar.dart';

/// 房间顶部的 UI
class YBDRoomTitle extends StatefulWidget {
  /// 总粉丝榜
  // final List<YBDFansInfo> totalRanking;

  // /// 房间在线用户数
  // final int onLineUserCount;

  // /// 房间分数
  // final int roomScore;

  // /// 是否上锁
  // final isLocked;

  // YBDRoomTitle({
  //   this.totalRanking,
  //   this.onLineUserCount,
  //   this.roomScore,
  //   // this.followBloc,
  //   this.isLocked,
  // });

  @override
  _YBDRoomTitleState createState() => _YBDRoomTitleState();
}

class _YBDRoomTitleState extends State<YBDRoomTitle> {
  /// 主播才显示加锁图标
  // bool _isTalent = false;

  /// 房间主播的用户信息
  // YBDUserInfo _talentInfo;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      // if (BlocProvider.of<YBDRoomBloc>(context).state.roomId == YBDCommonUtil.storeFromContext().state.bean.id) {
      //   setState(() {
      //     _isTalent = true;
      //   });
      // }
    });
  }
  void initStateOgfwwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposeRTnBuoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        var bloc = YBDFollowUserBloc(
          context,
          '${BlocProvider.of<YBDRoomBloc>(context).roomId}',
          type: FollowType.Hide,
        );

        bloc.add(FollowUserEvent.Request);
        return bloc;
      },
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: ScreenUtil().screenWidth,
              decoration: BoxDecoration(
                color: Colors.transparent,
              ),
              child: Row(
                children: <Widget>[
                  SizedBox(width: 20.px),
                  GestureDetector(
                    onTap: () {
                      logger.v('clicked talent info area');
                      YBDRoomUtil.showMiniProfileDialog(
                        roomId: BlocProvider.of<YBDRoomBloc>(context).roomId,
                        userId: BlocProvider.of<YBDRoomBloc>(context).roomId,
                      );
                    },
                    child: Row(
                      children: [
                        // 主播头像
                        _talentAvatar(),
                        SizedBox(width: 16.px),
                        // 主播昵称和 ID
                        _talentInfoColumn(),
                      ],
                    ),
                  ),
                  Spacer(),
                  // 关闭房间按钮
                  _closeRoomBtnRow(), SizedBox(width: 20.px),
                ],
              ),
            ),
            SizedBox(height: ScreenUtil().setWidth(25)),
            // 排行榜，用户等级，在线用户人数
            _roomInfoRow(),
          ],
        ),
      ),
    );
  }
  void build6WxmJoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 主播头像
  Widget _talentAvatar() {
    return Container(
      // 主播头像
      width: ScreenUtil().setWidth(90),
      height: ScreenUtil().setWidth(90),
      // decoration: BoxDecoration(
      //   color: Colors.white,
      //   borderRadius: BorderRadius.all(Radius.circular(70)),
      // ),
      padding: EdgeInsets.all(ScreenUtil().setWidth(1)),
      child: ClipOval(
        // 用户头像
        child: BlocBuilder<YBDRoomBloc, YBDRoomState>(
          buildWhen: (previous, current) => current.type == RoomStateType.TalentInfoUpdated,
          builder: (context, state) {
            return YBDNetworkImage(
              imageUrl: YBDImageUtil.avatar(
                    context,
                    state.talentInfo?.headimg,
                    state.roomId,
                    scene: 'B',
                  ),
              fit: BoxFit.cover,
              placeholder: (context, url) {
                return YBDResourcePathUtil.defaultMaleImage(male: state.talentInfo?.sex == 2);
              },
              errorWidget: (context, error, url) => YBDResourcePathUtil.defaultMaleImage(male: state.talentInfo?.sex == 2),
              width: ScreenUtil().setWidth(90),
              height: ScreenUtil().setWidth(90),
            );
          },
        ),
      ),
    );
  }
  void _talentAvatar9qwYyoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 房间加密锁
  Widget _roomLockIcon() {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
      buildWhen: (prev, current) => current.type == RoomStateType.RoomInfoUpdated,
      builder: (context, state) {
        if ((state.roomInfo?.protectMode == 1 ? true : false) && YBDRoomUtil.isTalent(state.roomId)) {
          return Padding(
            padding: EdgeInsets.only(left: 10.px),
            child: Image.asset(
              'assets/images/liveroom/room_icon_lock.png',
              width: ScreenUtil().setWidth(45),
              height: ScreenUtil().setWidth(45),
            ),
          );
        }

        return SizedBox();
      },
    );
  }
  void _roomLockIconO3D2Toyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 主播ID
  Widget _idWidget() {
    bool hasUid = context.read<YBDRoomBloc>().state.talentInfo?.uniqueNum != null;
    return hasUid
        ? Container(
            width: ScreenUtil().setWidth(124),
            // color: Colors.amber,
            child: Row(
              children: [
                Stack(
                  alignment: Alignment.centerLeft,
                  children: [
                    Container(
                      height: ScreenUtil().setWidth(24),
                      padding: EdgeInsets.only(right: 4.px),
                      margin: EdgeInsets.only(left: ScreenUtil().setWidth(2)),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(1)),
                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
                        gradient: LinearGradient(
                          colors: [Color(0xffFBBD1A), Color(0xffFCD323)],
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                        ),
                      ),
                      child: Row(
                        children: [
                          SizedBox(width: 30.px),
                          Text(
                            'ID: ${BlocProvider.of<YBDRoomBloc>(context).state.talentInfo?.uniqueNum}',
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(16),
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Image.asset(
                      'assets/images/u_crown.webp',
                      width: ScreenUtil().setWidth(24),
                      fit: BoxFit.fitWidth,
                    ),
                  ],
                ),
                // Spacer()
              ],
            ),
          )
        : Container(
            // 主播 ID
            // width: ScreenUtil().setWidth(180),
            child: Row(
              children: [
                Text(
                  'ID: ${BlocProvider.of<YBDRoomBloc>(context).roomId}',
                  style: TextStyle(
                    color: Color(0xffC2C2C2),
                    fontSize: ScreenUtil().setSp(24),
                  ),
                ),
              ],
            ),
          );
  }
  void _idWidgetiWsWeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 主播昵称和ID
  Widget _talentInfoColumn() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: [
            Container(
              // 主播昵称
              width: ScreenUtil().setWidth(180),
              child: BlocBuilder<YBDRoomBloc, YBDRoomState>(
                buildWhen: (previous, current) => current.type == RoomStateType.TalentInfoUpdated,
                builder: (context, state) {
                  return Text(
                    state.talentInfo?.nickname ?? '',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: ScreenUtil().setSp(28),
                    ),
                  );
                },
              ),
            ),
            // 加密房间
            _roomLockIcon(),
            // 关注按钮
            _followBtn(),
          ],
        ),
        SizedBox(height: ScreenUtil().setWidth(10)),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            _idWidget(),
            SizedBox(width: 5.px),
            BlocBuilder<YBDRoomBloc, YBDRoomState>(
              buildWhen: (prev, current) =>
                  current.type == RoomStateType.RoomLevelUpdated || current.type == RoomStateType.TalentInfoUpdated,
              builder: (context, state) {
                return Row(
                  children: [
                    YBDRoomLevelTag(state.talentInfo?.roomlevel),
                    YBDWearBadgeItem(
                      state.talentInfo?.badges,
                      needReload: true,
                    ),

                    // YBDVipIcon(state.talentInfo?.vipIcon)
                  ],
                );
              },
            ),
          ],
        ),
      ],
    );
  }
  void _talentInfoColumn0TUxQoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关注按钮
  Widget _followBtn() {
    return BlocBuilder<YBDFollowUserBloc, YBDFollowUserBlocState>(
      builder: (_, state) {
        Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
        // 已关注主播或者是主播自己不显示关注按钮  [followedIds] 存在为空的情况 多加个校验
        if (state.status == FollowStatus.Loading ||
            state.status == FollowStatus.Followed ||
            (store!.state.followedIds ?? Set()).contains(BlocProvider.of<YBDRoomBloc>(context).roomId) ||
            YBDRoomUtil.isTalent(BlocProvider.of<YBDRoomBloc>(context).roomId)) {
          // 加载是否关注的状态时不显示关注按钮
          // 已关注不显示关注按钮
          return SizedBox();
        } else {
          return Builder(builder: (buildContext) {
            return YBDScaleAnimateButton(
              onTap: () {
                logger.v('clicked follow talent btn');
                YBDCommonTrack().commonTrack(YBDTAProps(
                    location: 'follow',
                    module: YBDTAModule.liveroom,
                    id: BlocProvider.of<YBDRoomBloc>(context).roomId?.toString()));
                BlocProvider.of<YBDFollowUserBloc>(buildContext).add(FollowUserEvent.Follow);
              },
              child: Container(
                // follow 按钮
                // width: ScreenUtil().setWidth(130),
                // height: ScreenUtil().setWidth(54),
                margin: EdgeInsets.only(left: 30.px),
                alignment: Alignment.center,
                child: Image.asset(
                    state.status == FollowStatus.Following
                        ? 'assets/images/liveroom/icon_following@2x.webp'
                        : 'assets/images/liveroom/icon_follow@2x.webp',
                    width: 40.px,
                    height: 40.px),
              ),
            );
          });
        }
      },
    );
  }
  void _followBtnFsmJToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 排行榜，用户等级，在线用户人数
  Widget _roomInfoRow() {
    return Container(
      width: ScreenUtil().screenWidth,
      // width: 600.px,
      // color: Colors.pink,
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.start,
        // mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // SizedBox(width: ScreenUtil().setWidth(30)),
          GestureDetector(
            onTap: () {
              logger.v('clicked total rank area');
              // YBDRoomUtil.jumpToFansListPage(
              //   context,
              //   BlocProvider.of<YBDRoomBloc>(context).roomId,
              //   BlocProvider.of<YBDRoomBloc>(context).state.totalRanking,
              //   BlocProvider.of<YBDRoomBloc>(context).state.currentRanking,
              // );
              showModalBottomSheet(
                context: context,
                isScrollControlled: true,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(32))),
                ),
                builder: (_) {
                  return YBDFansSheet(
                    dailyFans: BlocProvider.of<YBDRoomBloc>(context).state.dailyFans,
                    weeklyFans: BlocProvider.of<YBDRoomBloc>(context).state.weeklyFans,
                    monthlyFans: BlocProvider.of<YBDRoomBloc>(context).state.monthlyFans,
                  );
                },
              );
            },
            child: Container(
              // width: 150.px,
              height: ScreenUtil().setWidth(50),
              padding: EdgeInsets.symmetric(horizontal: 10.px),
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.1),

                // color: Colors.pink,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(100.px),
                  bottomRight: Radius.circular(100.px),
                ),
              ),
              child: Row(
                children: <Widget>[
                  SizedBox(width: ScreenUtil().setWidth(20)),
                  Container(
                    child: Container(
                      height: ScreenUtil().setWidth(28),
                      child: Image.asset(
                        'assets/images/liveroom/icon_Trophy_big@2x.webp',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(10)),
                  Container(
                    child: Container(
                      padding: EdgeInsets.only(top: 4.px, right: 20.px),
                      child: BlocBuilder<YBDRoomBloc, YBDRoomState>(
                        buildWhen: (prev, current) => current.type == RoomStateType.ScoreUpdated,
                        builder: (context, state) {
                          return Row(
                            children: [
                              Text(
                                '${YBDNumericUtil.compactNumberWithFixedDigits(
                                      state.roomScore.toString(),
                                      fixedDigits: 2,
                                      removeZero: false,
                                      rounding: false,
                                    ) ?? 0}',
                                style: TextStyle(
                                  color: Color(0xffFFDC40),
                                  fontSize: ScreenUtil().setSp(24),
                                ),
                              ),
                              SizedBox(width: 10.px),
                              Image.asset('assets/images/liveroom/live_icon_more_rank.webp', width: 9.px, height: 15.px)
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(width: 20.px),
          YBDRoomAgentShare(BlocProvider.of<YBDRoomBloc>(context).state.roomInfo),
          Spacer(),
          // Expanded(child: Container()),
          _rankRow(),
          SizedBox(width: 10.px),
          // 在线人数
          GestureDetector(
            onTap: () {
              logger.v('clicked online user area');
              YBDRoomUtil.showViewersBottomSheet(
                context,
                BlocProvider.of<YBDRoomBloc>(context).roomId,
                0,
              );
            },
            child: Container(
              width: 50.px,
              height: 50.px,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.px),
                color: Colors.white.withOpacity(0.1),
              ),
              child: BlocBuilder<YBDRoomBloc, YBDRoomState>(
                buildWhen: (prev, current) => current.type == RoomStateType.UserCountUpdated,
                builder: (context, state) {
                  final userCount = state.onLineUserCount ?? 0;
                  logger.v('update online user count : $userCount');
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/images/liveroom/icon_uesr@2x.webp', width: 20.px, height: 20.px),
                      Text(
                        '${YBDNumericUtil.format(userCount) ?? 0}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: ScreenUtil().setSp(16),
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
          SizedBox(width: 20.px),
        ],
      ),
    );
  }
  void _roomInfoRowAFeOMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 分享，关闭房间按钮
  Widget _closeRoomBtnRow() {
    final store = YBDCommonUtil.storeFromContext(context: context)!;
    return Container(
      // width: ScreenUtil().setWidth(180),
      child: Row(
        children: <Widget>[
          // 匹配按钮
          BlocProvider.of<YBDRoomBloc>(context).roomId == store.state.bean?.id ? YBDMatchButton() : SizedBox(),
          SizedBox(width: ScreenUtil().setWidth(25)),
          // 分享
          YBDScaleAnimateButton(
            extraHitTestArea: EdgeInsets.all(10.px.toDouble()),
            onTap: () {
              YBDCommonTrack().commonTrack(YBDTAProps(location: 'share', module: YBDTAModule.liveroom));
              showModalBottomSheet(
                  context: context,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(16))),
                  ),
                  builder: (BuildContext context1) {
                    return YBDSharingWidget(
                      key: Key('room_${BlocProvider.of<YBDRoomBloc>(context).roomId}'),
                      room: BlocProvider.of<YBDRoomBloc>(context).state.roomInfo,
                    );
                  });

            },
            child: Image.asset('assets/images/liveroom/icon_share@2x.webp', width: 36.px),
          ),
          SizedBox(width: ScreenUtil().setWidth(30)),
          YBDScaleAnimateButton(
            extraHitTestArea: EdgeInsets.all(10.px.toDouble()),
            // 关闭房间按钮
            onTap: () {
              logger.v('clicked close room btn');
              //* pk时房主不能退出直播间
              if (BlocProvider.of<YBDRoomBloc>(context).roomId == store.state.bean!.id &&
                  YBDLiveService.instance.data.pkInfo?.status == 1) {
                YBDDialogUtil.showPKNormalDialog(context, PKNormalType.Exit);
                YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                  YBDEventName.CLICK_EVENT,
                  location: YBDLocationName.ROOM_PAGE,
                  itemName: YBDItemName.PK_TALENT_EXIT_ROOM,
                ));
              } else {
                YBDDialogUtil.showLeaveRoomConfirmDialog(context);
              }
            },
            child: Container(
              decoration: BoxDecoration(color: Colors.transparent),
              // padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
              child: Image.asset(
                'assets/images/liveroom/icon_close@2x.webp',
                width: 40.px,
              ),
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(5)),
        ],
      ),
    );
  }
  void _closeRoomBtnRowaBJk9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 排行榜前三名
  Widget _rankRow() {
    return GestureDetector(
      onTap: () {
        logger.v('clicked rank first three area');
        // YBDRoomUtil.jumpToFansListPage(
        //   context,
        //   BlocProvider.of<YBDRoomBloc>(context).roomId,
        //   BlocProvider.of<YBDRoomBloc>(context).state.totalRanking,
        //   BlocProvider.of<YBDRoomBloc>(context).state.currentRanking,
        // );
        showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(32))),
          ),
          builder: (_) {
            return YBDFansSheet(
              dailyFans: BlocProvider.of<YBDRoomBloc>(context).state.dailyFans,
              weeklyFans: BlocProvider.of<YBDRoomBloc>(context).state.weeklyFans,
              monthlyFans: BlocProvider.of<YBDRoomBloc>(context).state.monthlyFans,
            );
          },
        );
      },
      child: Container(
        color: Colors.transparent,
        child: BlocBuilder<YBDRoomBloc, YBDRoomState>(
            buildWhen: (prev, current) => current.type == RoomStateType.TotalRankUpdated,
            builder: (context, state) {
              return Row(
                children: <Widget>[
                  (state.monthlyFans?.length ?? 0) > 0 ? YBDRoomRankAvatar(state.monthlyFans![0], 1) : SizedBox(),
                  (state.monthlyFans?.length ?? 0) > 1 ? YBDRoomRankAvatar(state.monthlyFans![1], 2) : SizedBox(),
                  (state.monthlyFans?.length ?? 0) > 2 ? YBDRoomRankAvatar(state.monthlyFans![2], 3) : SizedBox(),
                ],
              );
            }),
      ),
    );
  }
  void _rankRowdwc7Foyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
