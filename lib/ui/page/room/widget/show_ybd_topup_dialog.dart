import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 首充提示弹框
class YBDShowTopUpPage extends StatelessWidget {
  /// 点确认按钮的回调
  final VoidCallback okCallback;

  /// 点关闭按钮的回调
  final VoidCallback closeCallback;

  YBDShowTopUpPage({required this.okCallback, required this.closeCallback});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(700),
      child: Center(
        child: Stack(
          children: [
            Positioned(
              child: Image.asset(
                'assets/images/recharge_hint_bg.webp',
                fit: BoxFit.contain,
              ),
            ),
            Positioned(
              top: ScreenUtil().setWidth(470),
              child: Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(700),
                child: Image.asset(
                  'assets/images/topup/y_top_up_message.webp',
                  fit: BoxFit.contain,
                  width: ScreenUtil().setWidth(450),
                ),
              ),
            ),
            Positioned(
              top: ScreenUtil().setWidth(20),
              right: ScreenUtil().setWidth(40),
              child: GestureDetector(
                onTap: () {
                  closeCallback();
                },
                child: Image.asset(
                  'assets/images/hide_recharge.webp',
                  width: ScreenUtil().setWidth(50),
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Positioned(
              top: ScreenUtil().setWidth(530),
              child: GestureDetector(
                onTap: () {
                  okCallback();
                },
                child: Container(
                  alignment: Alignment.center,
                  width: ScreenUtil().setWidth(700),
                  child: Image.asset(
                    'assets/images/recharge_hint_button.webp',
                    width: ScreenUtil().setWidth(300),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void build7njVjoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
