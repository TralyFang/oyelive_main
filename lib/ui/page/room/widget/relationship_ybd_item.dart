import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../common/util/numeric_ybd_util.dart';

/// mini profile 好友，关注，粉丝 item
class YBDRelationshipItem extends StatelessWidget {
  /// item 名称
  final String name;

  // item 数据
  final String data;

  // 点击 item 的回调
  final VoidCallback callback;

  double? height;

  YBDRelationshipItem(this.name, this.data, this.callback, {this.height});

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        onTap: callback,
        child: Container(
          width: ScreenUtil().setWidth(120),
          height: height ??= ScreenUtil().setWidth(80),
          // color: Colors.red.withOpacity(0.3),
          alignment: Alignment.center,
          child: Column(
            children: <Widget>[
              Text(
                YBDNumericUtil.compactNumberWithFixedDigits(data) ?? '0',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: ScreenUtil().setSp(30),
                  // fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                name,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  color: Colors.black.withOpacity(0.5),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildkmypfoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
