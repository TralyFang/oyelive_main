import 'dart:async';


import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/widget/icon_ybd_gender.dart';
import 'package:oyelive_main/ui/widget/level_ybd_tag.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/round_ybd_avatar.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import '../util/room_ybd_util.dart';

typedef AvatarCallback = Function(YBDUserInfo);

class YBDRoomViewerItem extends StatefulWidget {
  /// 房间ID
  final int? roomId;

  /// 用户信息
  final YBDUserInfo? userInfo;

  YBDRoomViewerItem(this.userInfo, this.roomId);

  @override
  _YBDRoomViewerItemState createState() => _YBDRoomViewerItemState();
}

class _YBDRoomViewerItemState extends BaseState<YBDRoomViewerItem> {
  /// 是否正在请求关注该用户的接口
  bool _isFollowing = false;

  @override
  Widget myBuild(BuildContext context) {
    log("userPublish info :${widget.userInfo!.toJson()}");
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: ScreenUtil().setWidth(20),
        horizontal: ScreenUtil().setWidth(30),
      ),
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              logger.v('clicked ${widget.userInfo!.id}');
              YBDRoomUtil.showMiniProfileDialog(
                roomId: widget.roomId,
                userId: widget.userInfo!.id,
              );
            },
            child: YBDRoundAvatar(
              widget.userInfo!.headimg,
              scene: "B",
              sex: widget.userInfo!.sex,
              avatarWidth: 80,
              userId: widget.userInfo!.id,
              lableSrc: (widget.userInfo?.vip ?? 0) == 0 ? "" : "assets/images/vip_lv${widget.userInfo!.vip}.png",
              labelWitdh: 28,
              needNavigation: false,
              levelFrame: widget.userInfo!.headFrame,
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: [
                      Container(
                        // 昵称
                        constraints:
                            BoxConstraints(maxWidth: ScreenUtil().setWidth(370), maxHeight: ScreenUtil().setWidth(55)),
                        child: Text(
                          widget.userInfo?.nickname ?? '',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Color(0xff3A3A3A),
                            fontSize: ScreenUtil().setSp(28),
                          ),
                        ),
                      ),
                      YBDGenderIcon(widget.userInfo?.sex ?? 0),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setWidth(widget.userInfo?.vipIcon != null ? 0 : 6)),
                  Row(
                    children: <Widget>[
                      YBDVipIcon(widget.userInfo?.vipIcon),
                      // SizedBox(width: ScreenUtil().setWidth(20)),
                      YBDLevelTag(widget.userInfo?.level),
                    ],
                  )
                ],
              ),
            ),
          ),
          _followBtn(),
        ],
      ),
    );
  }
  void myBuildRe2Qboyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关注按钮
  _followBtn() {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    if ((store.state.followedIds ?? Set()).contains(widget.userInfo!.id) || store.state.bean?.id == widget.userInfo!.id) {
      // 已关注或者为自己不显示关注按钮
      return Container();
    } else {
      return YBDScaleAnimateButton(
          onTap: () {
            if (!_isFollowing) {
              logger.v('clicked follow user btn : ${widget.userInfo!.id}');
              _requestUnFollow('${widget.userInfo!.id}');
            }
          },
          child: Container(
            // follow 按钮
            // margin: EdgeInsets.only(left: 30.px),
            // alignment: Alignment.center,
            child: Image.asset(
                _isFollowing
                    ? 'assets/images/liveroom/icon_following@2x.webp'
                    : 'assets/images/liveroom/icon_follow@2x.webp',
                width: 40.px,
                height: 40.px),
          ));
    }
  }

  /// 请求关注接口
  _requestUnFollow(String userId) async {
    setState(() {
      _isFollowing = true;
    });
    ApiHelper.followUser(context, userId).then((result) {
      logger.v('follow user : $userId result : $result');
      setState(() {
        _isFollowing = false;
      });
    });
  }
}
