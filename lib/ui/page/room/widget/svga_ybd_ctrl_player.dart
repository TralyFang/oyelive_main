import 'dart:async';



import 'dart:io';

import 'package:flutter/material.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

class YBDSVGACtrlPlayImage extends StatefulWidget {
  final String? resUrl;
  final String? assetsName;
  final File? file;
  final bool? startPlay; // 开始就播放一次
  final bool? repeatPlay; // 重复播放
  final AnimationStatusListener? playStateListener;
  final BoxFit? fit;

  YBDSVGACtrlPlayImage({Key? key, this.resUrl, this.assetsName, this.file, this.startPlay, this.repeatPlay, this.playStateListener, this.fit = BoxFit.contain})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return YBDSVGACtrlPlayImageState();
  }
}

class YBDSVGACtrlPlayImageState extends State<YBDSVGACtrlPlayImage>
    with SingleTickerProviderStateMixin {
  SVGAAnimationController? animationController;

  @override
  void initState() {
    super.initState();
    animationController = SVGAAnimationController(vsync: this);
    late Future decode;
    if (widget.assetsName != null) {
      decode = SVGAParser.shared.decodeFromAssets(widget.assetsName!);
    } else if (widget.file != null) {
      decode = widget.file!.readAsBytes().then((bytes) {
        return SVGAParser.shared.decodeFromBuffer(bytes);
      });
    } else if (widget.resUrl != null) {
      decode = SVGAParser.shared.decodeFromURL(widget.resUrl!);
    }
    decode.then((videoItem) {
      logger.v("svga_ctrl_player.videoItem:$videoItem");
      if (mounted) {
        animationController!
        ..reset()
          ..videoItem = videoItem;
        if (widget.startPlay ?? false) {
          play();
        }
        if (widget.repeatPlay ?? false) {
          repeat();
        }
      }
    });

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      if (widget.startPlay ?? false) {
        play();
      }
      if (widget.repeatPlay ?? false) {
        repeat();
      }
    });

    if (widget.playStateListener != null) {
      animationController!.addStatusListener((status) {
        logger.v("svga_ctrl_player.addStatusListener:$status");
        widget.playStateListener!(status);
      });
    }
    // logger.v("*****>>>>player-init-start:${widget.startPlay}");
  }
  void initStateoweWVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void play() {
    if (!mounted || animationController!.videoItem == null) return;
    // logger.v("*****>>>>player");
    animationController!.reset();
    animationController!.forward();
  }
  void playYYlERoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void stop() {
    if (!mounted || animationController!.videoItem == null) return;
    animationController!.reset();
    animationController!.stop();
  }
  void stopdYD8foyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void repeat() {
    if (!mounted || animationController!.videoItem == null) return;
    animationController!.repeat();
  }

  @override
  Widget build(BuildContext context) {
    return SVGAImage(animationController!, fit: widget.fit!,);
  }
  void build1Cfv5oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    animationController!.videoItem = null;
    animationController!.dispose();
    super.dispose();
  }
}
