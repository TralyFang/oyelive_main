import 'dart:async';


import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';

typedef WidgetValueCallback = Widget Function(int);
class YBDNumberDancefulWidget extends StatefulWidget {

  WidgetValueCallback? childBlock;
  final Duration? duration;

  YBDNumberDancefulWidget({Key? key, this.childBlock, this.duration}) : super(key: key);

  @override
  YBDNumberDancefulWidgetState createState() => YBDNumberDancefulWidgetState();
}

class YBDNumberDancefulWidgetState extends State<YBDNumberDancefulWidget> with SingleTickerProviderStateMixin {

  late AnimationController _controller;
  late Animation<int> _animation;

  int _endValue = 0;
  int _lastValue = 0;
  bool _isForward = false;

  int get lastValue {
    return _lastValue;
  }

  void toValue(int value, {Duration? duration}) {
    resetValue(value, duration: duration);
  }
  void toValuevtPpqoyelive(int value, {Duration? duration}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void addValue(int value, {Duration? duration}) {
    _endValue += value;
    _updateValue();
  }
  void addValueKAbB2oyelive(int value, {Duration? duration}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void resetValue(int value, {Duration? duration}) {
    if (duration != null) {
      _controller.duration = duration;
    }
    _endValue = value;
    if(_endValue< 0){
      _endValue = 0;
    }
    _updateValue();
  }
  void resetValue0fP1hoyelive(int value, {Duration? duration}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _updateValue() {
    if (_isForward) {
      _isForward = false;
      _animation =
          IntTween(begin: _endValue, end: _lastValue).animate(_controller);
      _controller.reverse();
    } else {
      _isForward = true;
      _animation =
          IntTween(begin: _lastValue, end: _endValue).animate(_controller);
      _controller.forward();
    }
    logger.v('tp animation update value: $_endValue $_lastValue ,$_isForward, ${_controller.isAnimating} , ${_controller.status} ${_animation.status}, ${_animation.value}');
    _lastValue = _endValue;
    if (!_controller.isAnimating) {
      // 如果动画没开始，那就重来一次吧
      logger.v('tp animation dismissed reset value');
      _lastValue = _endValue-1;
      _controller.duration = Duration(milliseconds: 100);
      _updateValue();
    }
  }
  void _updateValue8P6HAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  



  @override
  void initState() {

    super.initState();

    _controller = AnimationController(
      vsync: this, // the SingleTickerProviderStateMixin
      duration: widget.duration,
    );
    _animation =
        IntTween(begin: _lastValue, end: _endValue).animate(_controller);

    logger.v('tp number widget init: ${_animation.value}');
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  void disposevfdkmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animation,
      builder: (BuildContext context, Widget? child) {
        return widget.childBlock!(_animation.value);
      },
    );
  }
}