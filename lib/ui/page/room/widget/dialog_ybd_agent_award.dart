import 'dart:async';


import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/module/status/entity/agent_ybd_share_entity.dart';
import 'package:oyelive_main/module/status/status_ybd_api_helper.dart';
import 'package:oyelive_main/ui/page/share/util/friend_ybd_share_util.dart';
import 'package:oyelive_main/ui/page/share/widget/friend_ybd_list_dialog.dart';
import 'package:oyelive_main/common/constant/const.dart';

class YBDAgentAwardDialog extends StatefulWidget {
  YBDRoomInfo? room;

  YBDAgentAwardDialog(this.room);

  @override
  YBDAgentAwardDialogState createState() => new YBDAgentAwardDialogState();
}

class YBDAgentAwardDialogState extends BaseState<YBDAgentAwardDialog> {
  int days = 0;
  int hours = 0;
  int minutes = 0;
  int secs = 0;
  getText(int index) {
    switch (index) {
      case 0:
        return "Day";
        break;
      case 2:
        return "Hours";
        break;
      case 4:
        return "Min";
        break;
      case 6:
        return "Sec";
        break;
    }
  }

  getTime(int index) {
    switch (index) {
      case 0:
        return days.toString();
        break;
      case 2:
        return hours.toString();
        break;
      case 4:
        return minutes.toString();
        break;
      case 6:
        return secs.toString();
        break;
    }
  }

  YBDAgentShareEntity? agentShareEntity;
  getData() async {
    YBDAgentShareEntity? result = await YBDStatusApiHelper.getAgentShare(context);
    if (result?.code == Const.HTTP_SUCCESS_NEW && result?.data != null) {
      agentShareEntity = result;
      setTimer(DateTime.fromMillisecondsSinceEpoch(result!.data!.endTime!));
      setState(() {});
    }
  }

  Timer? _timer;
  setTimer(DateTime endTime) {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      DateTime nowTime = DateTime.now();
      if (nowTime.millisecondsSinceEpoch - endTime.millisecondsSinceEpoch < 0) {
        Duration diff = endTime.difference(nowTime);
        days = diff.inDays;
        hours = diff.inHours - days * 24;
        minutes = diff.inMinutes - diff.inHours * 60;
        secs = diff.inSeconds - diff.inMinutes * 60;
      } else {
        days = 0;
        hours = 0;
        minutes = 0;
        secs = 0;
        _timer!.cancel();
      }

      setState(() {});
    });
  }

  /// 弹出好友列表
  showShareToFriendsDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext builder) {
        return YBDFriendListDialog((selectedFriends) async {
          FriendShareType type;
          dynamic data;

          // 分享房间
          type = FriendShareType.ShareRoom;
          data = widget.room;

          // 分享给好友
          bool isShared = await YBDFriendShareUtil.shareToFriends(context, type, data, selectedFriends);
          if (isShared) {
            YBDToastUtil.toast(translate("sharing_succeeded"));
            if (Navigator.canPop(context)) Navigator.pop(context);
          }
        });
      },
    ).then((value) {
      if (value != null && value) {
        Navigator.pop(context);
      }
    });
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Material(
      color: Colors.transparent,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: ScreenUtil().setWidth(700),
              height: ScreenUtil().setWidth(830),
              decoration: BoxDecoration(image: DecorationImage(image: AssetImage("assets/images/agent/bg_agent.webp"))),
              child: Column(
                children: [
                  SizedBox(
                    height: ScreenUtil().setWidth(215),
                  ),
                  Image.asset(
                    "assets/images/agent/agent_title@2x.webp",
                    width: ScreenUtil().setWidth(488),
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(34),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(7, (index) {
                      if (index % 2 == 0) {
                        return Container(
                          width: ScreenUtil().setWidth(128),
                          height: ScreenUtil().setWidth(108),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
                              color: Colors.white.withOpacity(0.6)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                getTime(index),
                                style: TextStyle(
                                    color: Color(0xff067DF9),
                                    fontSize: ScreenUtil().setSp(42),
                                    fontWeight: FontWeight.w600),
                              ),
                              Text(
                                getText(index),
                                style: TextStyle(color: Color(0xff056574), fontSize: ScreenUtil().setSp(20)),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return SizedBox(
                          width: ScreenUtil().setWidth(10),
                        );
                      }
                    }),
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(48),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: ScreenUtil().setWidth(84),
                      ),
                      Image.asset(
                        "assets/images/agent/agent_chip.webp",
                        width: ScreenUtil().setWidth(78),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(20),
                      ),
                      Text(
                        "Bet Amount",
                        style: TextStyle(
                            color: Color(0xff08426F), fontSize: ScreenUtil().setSp(28), fontWeight: FontWeight.w600),
                      ),
                      Spacer(),
                      Image.asset(
                        "assets/images/topup/y_top_up_beans@2x.webp",
                        width: ScreenUtil().setWidth(32),
                      ),
                      Text(
                        agentShareEntity?.data != null
                            ? YBDNumericUtil.format(agentShareEntity!.data!.totalConsumption)!
                            : "",
                        style: TextStyle(
                            color: Color(0xff08426F), fontSize: ScreenUtil().setSp(28), fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(78),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(32),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: ScreenUtil().setWidth(84),
                      ),
                      Image.asset(
                        "assets/images/agent/agent_box.webp",
                        width: ScreenUtil().setWidth(74),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(24),
                      ),
                      Text(
                        "Sharing",
                        style: TextStyle(
                            color: Color(0xff08426F), fontSize: ScreenUtil().setSp(28), fontWeight: FontWeight.w600),
                      ),
                      Spacer(),
                      Image.asset(
                        "assets/images/topup/y_top_up_beans@2x.webp",
                        width: ScreenUtil().setWidth(32),
                      ),
                      Text(
                        agentShareEntity?.data != null ? YBDNumericUtil.format(agentShareEntity!.data!.layeredBeans)! : "",
                        style: TextStyle(
                            color: Color(0xffFFEA00), fontSize: ScreenUtil().setSp(36), fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(78),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(24),
                  ),
                  GestureDetector(
                    onTap: () {
                      showShareToFriendsDialog(context);
                    },
                    child: Container(
                      width: ScreenUtil().setWidth(490),
                      height: ScreenUtil().setWidth(122),
                      decoration:
                          BoxDecoration(image: DecorationImage(image: AssetImage("assets/images/agent/agent_btn.webp"))),
                      alignment: Alignment.center,
                      child: Text(
                        "Share room and Earn more",
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(26), color: Color(0xff08426F), fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(10),
                  ),
                  Text(
                    "The clearing time is based on UTC time",
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xff08426F),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(28),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Image.asset(
                "assets/images/white_cancel.png",
                width: ScreenUtil().setWidth(60),
              ),
            )
          ],
        ),
      ),
    );
  }
  void myBuildzI1x0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }
  void initStategJfQNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    _timer?.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDAgentAwardDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetD1nKeoyelive(YBDAgentAwardDialog oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
