import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../bloc/room_ybd_bloc.dart';
import '../mic/mic_ybd_bloc.dart';
import '../util/room_ybd_util.dart';

/// 主播收到的请求上麦的个数按钮
class YBDMicRequestAmount extends StatelessWidget {
  final BuildContext? roomPageContext;
  YBDMicRequestAmount(this.roomPageContext);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<YBDMicBloc, YBDMicBlocState>(
      builder: (context, state) {
        return Container(
          width: ScreenUtil().setWidth(80),
          height: ScreenUtil().setWidth(80),
          margin: EdgeInsets.only(bottom: 14.px),
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/images/liveroom/icon_request@2x.webp')),
          ),
          child: Material(
            type: MaterialType.transparency,
            child: InkWell(
              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(40)),
              onTap: () {
                logger.v('clicked mic request amount btn');
                // 弹出麦位请求底部弹框
                YBDRoomUtil.showMicRequestBottomSheet(
                  roomPageContext!,
                  0,
                );
              },
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.topCenter,
                    child: Transform.translate(
                      offset: Offset(ScreenUtil().setWidth(27), 0),
                      child: Container(
                        height: ScreenUtil().setWidth(28),
                        constraints: BoxConstraints(minWidth: ScreenUtil().setWidth(28)),
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(6)),
                        decoration: BoxDecoration(
                          color: Color(0xffE6497A),
                          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              '${state.micRequestlist?.length ?? 0}',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: ScreenUtil().setSp(18), color: Colors.white, height: 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
  void buildPQbD8oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
