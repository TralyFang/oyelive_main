import 'dart:developer' as dev;
import 'dart:io';
import 'dart:math';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/activty/widget/gif_ybd_image.dart';
import 'package:collection/collection.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_gradient_dialog.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:sprintf/sprintf.dart';
import 'package:svgaplayer_flutter/player.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_bloc.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_svga_theme.dart';
import 'package:oyelive_main/ui/widget/dialog/confirm_ybd_dialog.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/event/change_ybd_room_bus_event.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/room_ybd_level_tag.dart';
import '../../../widget/round_ybd_avatar.dart';
import '../../profile/my_profile/wear_ybd_badge_item.dart';
import '../bloc/mini_ybd_operation_def.dart';
import '../bloc/mini_ybd_profile_bloc.dart';
import '../bloc/room_ybd_bloc.dart';
import '../bloc/room_ybd_bloc_state.dart';
import '../define/room_ybd_define.dart';
import '../entity/mic_ybd_bean.dart';
import '../ext/rtm_ybd_type_ext.dart';
import '../mic/mic_ybd_bloc.dart';
import '../pk/pk_ybd_rtm_helper.dart';
import '../util/mini_ybd_profile_util.dart';
import '../util/room_ybd_util.dart';
import 'gift_ybd_receiver.dart';
import 'mini_ybd_profile_follow.dart';
import 'mini_ybd_profile_gift.dart';
import 'mini_ybd_profile_id.dart';
import 'mini_ybd_profile_sex.dart';
import 'relationship_ybd_item.dart';
import 'dart:ui' as ui;

class YBDMiniProfileDialog extends StatefulWidget {
  /// 展示游戏房 mini profile
  static showGame(BuildContext context, {int? roomId, int? userId}) {
    showDialog(
      context: context,
      builder: (BuildContext builder) {
        return MultiBlocProvider(
          providers: [
            BlocProvider<YBDGameMicBloc>(create: (_) => YBDGameMicBloc(YBDGameMicBlocState(), context)),
            BlocProvider<YBDMiniProfileBloc>(create: (_) => YBDMiniProfileBloc(context, userId, roomId)),
          ],
          child: YBDMiniProfileDialog(userId, roomId, gameRoomType: true),
        );
      },
    );
  }

  /// 用户ID
  final int? userId;

  /// 主播 id
  final int? roomId;

  final bool isClickOnPkEnemy, isEnemyMuted, isEnemyGuest, gameRoomType;

  final Function? closeCallback;

  YBDMiniProfileDialog(
    this.userId,
    this.roomId, {
    this.isClickOnPkEnemy: false,
    this.isEnemyMuted: false,
    this.isEnemyGuest: false,
    this.closeCallback,
    this.gameRoomType: false,
  });

  @override
  State<StatefulWidget> createState() => _YBDMiniProfileDialogState();
}

class _YBDMiniProfileDialogState extends BaseState<YBDMiniProfileDialog> {
  /// 页面 bloc
  YBDMiniProfileBloc? _miniProfileBloc;

  /// 我的用户 id
  int? _myId;

  /// 是否为我的好友
  bool? _isFriend;

  /// 管理员列表
  List<YBDUserInfo?>? _roomManagers;

  /// 麦位用户
  List<YBDMicBean?>? _micBeanS;

  /// 游戏房麦位用户
  // List<GameMicUser> _gameUsers;

  /// 用户昵称
  String _nickName = '';

  @override
  void initState() {
    super.initState();
    _miniProfileBloc = context.read<YBDMiniProfileBloc>();
    _miniProfileBloc!.add(MiniProfileEvent.Request);

    _roomManagers = widget.gameRoomType ? [] : context.read<YBDRoomBloc>().state.roomManagers;
    _micBeanS = widget.gameRoomType ? [] : context.read<YBDMicBloc>().state.micBeanList;
    // _gameUsers = widget.gameRoomType ? context.read<YBDGameMicBloc>().state.micUserList : [];
    _myId = YBDCommonUtil.storeFromContext()!.state.bean!.id;
    _queryCer();
  }
  void initStateSxekxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _miniProfileBloc!.close();
    super.dispose();
  }
  void disposeBglkqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Widget> _getCertified() {
    if (_certificationInfos != null)
      return [
        Transform.translate(
          offset: Offset(0, -35.px),
          child: Text(
            _certificationInfos!.title ?? '',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(24),
              color: Color(0xff1FBFC9),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(
          height: ScreenUtil().setWidth(10),
        ),
      ];

    return [];
  }
  void _getCertifiedC7MEMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  YBDUserCertificationRecordCertificationInfos? _certificationInfos;
  bool isCerRequested = false;

  void _queryCer() async {
    final userCertificationEntity = await ApiHelper.queryCertification(
      context,
      [widget.userId],
    );
    isCerRequested = true;

    if (userCertificationEntity?.returnCode == Const.HTTP_SUCCESS) {
      _certificationInfos = YBDCommonUtil.getUsersSingleCer(
        userCertificationEntity!.record,
        widget.userId,
      );
    }

    if (mounted) {
      setState(() {});
    }
  }
  void _queryCerk1VHGoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return BlocBuilder<YBDMiniProfileBloc, YBDMiniProfileBlocState>(
      bloc: _miniProfileBloc,
      builder: (BuildContext context, YBDMiniProfileBlocState state) {
        _isFriend = state.isMyFriend;
        _nickName = state.userInfo?.nickname ?? 'null';
        final store = YBDCommonUtil.storeFromContext(context: context);

        dev.log('i have\n ${state.userInfo?.equipGoods?.cardFrame}');
        bool isMySelf = state.userInfo?.id == _myId;
        bool hasFrame = state.userInfo?.equipGoods?.cardFrame != null;
        String fullUrl = '';
        if (hasFrame) {
          String? frameUrl = isMySelf
              ? state.userInfo?.equipGoods?.cardFrame?.animation?.split(',').last
              : state.userInfo?.equipGoods?.cardFrame?.animation?.split(',').first;
          fullUrl = YBDImageUtil.getFullSize(context, frameUrl);
        }
        // fullUrl = 'https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/s1%E8%B5%84%E6%96%99@2x.webp';
        // fullUrl = 'https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/lv3_50-60.gif';
        // fullUrl = 'https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/31-40.webp';
        // fullUrl = 'https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/11-20.webp';
        // fullUrl = 'https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/11-20@2x.webp';
        // fullUrl = 'https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/2x_61-70 .gif';
        // fullUrl = 'https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/s2%E8%B5%84%E6%96%99%E6%A1%86@2x.webp';
        // fullUrl = 'https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/s1%E8%B5%84%E6%96%99%E6%A1%86@2x.webp';
        // hasFrame = true;
        logger.v('message.fullUrl:$fullUrl');

        return GestureDetector(
          onTap: () {
            logger.v('clicked mini profile dialog bg');
            _pop();
          },
          child: Material(
            color: Colors.black.withOpacity(0.1),
            child: Center(
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      GestureDetector(
                        onTap: () {
                          logger.v('clicked mini profile white bg');
                        },
                        child: Container(
                          width: ScreenUtil().setWidth(550),
                          height: ScreenUtil().setWidth(_getHeight(state)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(28))),
                            color: Colors.white,
                          ),
                          child: Column(
                            children: <Widget>[
                              // 举报按钮，关闭按钮
                              _topRow(state),
                              // SizedBox(height: 10.px),
                              // 昵称
                              _nameItem(state.userInfo?.nickname ?? ''),
                              // SizedBox(height: 1.px),
                              // ID, 性别，用户等级，房间等级，签约主播标签
                              _tagRow(state),
                              _badges(state),
                              ..._getCertified(),
                              // SizedBox(height: ScreenUtil().setWidth(hasFrame && !isMySelf ? 32 : 42)),
                              // 好友，关注，粉丝
                              // SizedBox(height: 10.px),
                              _friendsRow(state),
                              if (state.isOtherUser) ..._bottomPart(state, store),
                              // SizedBox(height: ScreenUtil().setWidth(16))
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 50.px),
                      GestureDetector(
                        onTap: () {
                          _pop();
                        },
                        child: Image.asset(
                          'assets/images/liveroom/icon_close.webp',
                          width: ScreenUtil().setWidth(50),
                          fit: BoxFit.cover,
                        ),
                      )
                    ],
                  ),
                  if (hasFrame) frameWidget(fullUrl, state),
                  // 头像
                  avatarWidget(state),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
  void myBuildk9hiWoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget avatarWidget(YBDMiniProfileBlocState state) {
    double dy = -65.px; // 顶层头像微调
    // if (state?.userInfo?.id == _myId) {
    //   dy = -44.px;
    // }
    // 头像
    return Positioned(
      top: 0,
      child: Transform.translate(
        offset: Offset(0, dy),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Stack(
              children: [
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(12), horizontal: ScreenUtil().setWidth(30)),
                  child: Container(
                    width: 130.px,
                    height: 130.px,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(70.px),
                      border: Border.all(width: 2.px, style: BorderStyle.solid, color: Colors.white),
                    ),
                    child: YBDRoundAvatar(
                      state.userInfo?.headimg,
                      scene: 'B',
                      sex: state.userInfo?.sex,
                      avatarWidth: 130,
                      labelWitdh: 40,
                      userId: widget.userId,
                      lableSrc: (state.userInfo?.vip ?? 0) == 0 ? '' : 'assets/images/vip_lv${state.userInfo?.vip ?? 0}.png',
                      closeCallback: widget.closeCallback,
                      levelFrame: state.userInfo?.headFrame,
                    ),
                  ),
                ),
                Positioned(
                    right: 0,
                    bottom: 0,
                    child: YBDVipIcon(
                      state.userInfo?.vipIcon,
                      size: 68,
                      padding: EdgeInsets.only(right: 6.px, bottom: 4.px),
                    ))
              ],
            ),
          ],
        ),
      ),
    );
  }
  void avatarWidgetMeDV7oyelive(YBDMiniProfileBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 边框
  Widget frameWidget(String fullUrl, YBDMiniProfileBlocState state) {
    // 高度：200/4.0 120/4.0
    // 宽度：180/4.0=45
    double dy = -31.5; // 根据图片的边距的四1/4.0倍来偏移
    double contentWidth = ScreenUtil().setWidth(550) + 44;
    double contentHeight = ScreenUtil().setWidth(_getHeight(state)) + 50.5;

    // 默认尺寸，为了适应定位头像
    Widget defaultWidget = Transform.translate(
      offset: Offset(0, dy),
      child: Container(
        width: contentWidth,
        height: contentHeight,
      ),
    );

    return IgnorePointer(
      //TODO 2.7.4 暂时隐藏卡片边框
      child: fullUrl.endsWith('svga') || true
          ? defaultWidget
          /* svga不能拉伸，默认不显示了，避免展示不匹配
          ? SizedBox(
        width: ScreenUtil().setWidth(668),
        child: YBDRoomSvgaTheme(
          fullUrl,
          showLoading: false,
          flexible: true,
          width: ScreenUtil().setWidth(668),
        ),
      )

       */
          : StreamBuilder<String>(
              stream: YBDDownloadCacheManager.instance.downloadStreamURL(fullUrl),
              builder: (BuildContext context, AsyncSnapshot<String> path) {
                if (path.data != null) {
                  logger.v('message.path:${path.data}');
                  FileImage provider = FileImage(File(path.data!));
                  return FutureBuilder<List<ImageInfo>?>(
                      future: fetchGif(provider),
                      builder: (BuildContext context, AsyncSnapshot<List<ImageInfo>?> imgInfos) {
                        if (imgInfos.data != null && (imgInfos.data?.length ?? 0) > 0) {
                          ui.Image img = imgInfos.data!.first.image;
                          double sliceWidth = 60.0; // 切边宽度
                          // 必须确保图片尺寸-切边尺寸<=视图尺寸
                          // 如果图片尺寸过大，可以通过scale适当放大视图尺寸来满足上面的要求
                          double widthScale = (img.width.toDouble() - sliceWidth) / contentWidth.toDouble();
                          double heightScale = (img.height.toDouble() - sliceWidth) / contentHeight.toDouble();
                          double scale = max(widthScale, heightScale).ceilToDouble();
                          // 1280, height:940
                          logger.v(
                              'message.img.width:${img.width}, height:${img.height}, contentWidth:$contentWidth, $contentHeight, $scale');
                          return Transform.translate(
                            offset: Offset(0, dy),
                            child: Container(
                                width: contentWidth,
                                height: contentHeight,
                                decoration: BoxDecoration(
                                    // color: Colors.red.withAlpha(100),
                                    image: DecorationImage(
                                        scale: scale,
                                        fit: BoxFit.fill,
                                        // centerSlice: Rect.fromLTWH(150, 70, 20, 20),
                                        centerSlice: Rect.fromLTWH(
                                            (img.width.toDouble() - sliceWidth) / scale / 2.0,
                                            (img.height.toDouble() - sliceWidth) / scale / 2.0,
                                            sliceWidth / scale,
                                            sliceWidth / scale),
                                        image: provider))),
                          );
                        }
                        return defaultWidget;
                      });
                }
                return defaultWidget;
                // return YBDNetworkImage(width: ScreenUtil().setWidth(668), imageUrl: fullUrl);
              }),
    );
  }
  void frameWidgetpbfZCoyelive(String fullUrl, YBDMiniProfileBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 内容高度
  double _getHeight(YBDMiniProfileBlocState state) {
    double height = 0.0;
    if (_certificationInfos != null) {
      height += 35; // 验证主播
    }
    if (state.userInfo?.id == _myId) {
      // 自己
      height += 350;
    } else if (!_showOPList()) {
      // 普通用户，非管理员
      height += 490;
    } else {
      // 管理员
      height += 610;
    }
    // 没有徽章，就减掉高度
    if (!_hasBadges(state)) {
      height -= 50;
    }
    if (showJumpToRoom()) {
      // pk
      height += 98;
    }
    logger
        .v('_getHeight badges:${_hasBadges(state)}, talent:${_certificationInfos != null},unManager:${!_showOPList()}');
    return height;
  }

  // 有无徽章
  bool _hasBadges(YBDMiniProfileBlocState state) {
    if (_certificationInfos == null || _certificationInfos?.hidBadge == 0) if (state.userInfo?.badges != null &&
        (state.userInfo?.badges?.length ?? 0) > 0) return true;
    return false;
  }
  void _hasBadgeszVcOLoyelive(YBDMiniProfileBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _badges(YBDMiniProfileBlocState state) {
    if (_hasBadges(state))
      return Transform.translate(
        offset: Offset(0, -40.px),
        child: Container(
            height: 60.px,
            // color: Colors.blue.withOpacity(0.3),
            child: Center(child: YBDWearBadgeItem(state.userInfo?.badges))),
      );
    return SizedBox(height: 10.px);
  }

  Widget _nameItem(String name) {
    return Transform.translate(
      offset: Offset(0, -70.px),
      child: Text.rich(
        TextSpan(
          style: TextStyle(
            color: Colors.black,
            fontSize: ScreenUtil().setSp(28),
            fontWeight: FontWeight.bold,
          ),
          children: [
            if (_certificationInfos != null)
              WidgetSpan(
                child: SizedBox(
                  width: ScreenUtil().setWidth(48),
                ),
              ),
            TextSpan(text: name, style: TextStyle(fontSize: 30.sp2)),
            if (_certificationInfos != null)
              WidgetSpan(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      width: ScreenUtil().setWidth(10),
                    ),
                    YBDNetworkImage(
                      imageUrl: _certificationInfos?.icon ?? '',
                      width: ScreenUtil().setWidth(38),
                    ),
                  ],
                ),
              ),
          ],
        ),
        maxLines: 1,
      ),
    );
  }
  void _nameItemp6R3joyelive(String name) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Widget> _bottomPart(YBDMiniProfileBlocState state, var store) {
    if (!widget.isEnemyGuest) {
      bool jumpToRoom = widget.isClickOnPkEnemy && store.state.bean.id != widget.roomId;

      return [
        SizedBox(height: 6.px),
        if (jumpToRoom) _jumpToRoom(),
        // 送礼，关注
        _bottomRow(state),
        // SizedBox(height: _showOPList() ? 20.px : 0),
        _showOPList() ? Divider(height: 1.px, color: Colors.black.withOpacity(0.2)) : SizedBox(),
        SizedBox(height: _showOPList() ? 27.px : 0),
        if (!jumpToRoom) _operationListWrap(state, widget.isClickOnPkEnemy),
        // SizedBox(height: ScreenUtil().setWidth(jumpToRoom ? 38 : 10)),
      ];
    }
    return [
      SizedBox(height: 6.px),
      // 对面主播的贵宾席只显示SayHi，关注
      _bottomRow(state),
      SizedBox(height: ScreenUtil().setWidth(38)),
    ];
  }

  bool showJumpToRoom() {
    return widget.isClickOnPkEnemy && YBDUserUtil.getUserIdSync != widget.roomId;
  }
  void showJumpToRoomjlul5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 是否显示操作列表
  bool _showOPList() {
    bool isRoomHolder = BlocProvider.of<YBDRoomBloc>(context).state.roomId == YBDUserUtil.getUserIdSync;
    bool isRoomManager =
        BlocProvider.of<YBDRoomBloc>(context).state.roomManagers?.any((e) => e?.id == YBDUserUtil.getUserIdSync) ?? false;
    print('22.12.25---isRoomHolder:$isRoomHolder---isRoomManager:$isRoomManager');
    if (isRoomManager && widget.roomId == widget.userId) return false;
    return isRoomHolder || isRoomManager;
  }

  /// 操作按钮列表
  Widget _operationListWrap(YBDMiniProfileBlocState state, bool isEnemy) {
    if (null == state.userInfo?.id) {
      // 用户信息为空时不显示操作按钮
      return Container();
    } else if (widget.gameRoomType) {
      return MultiBlocListener(
        listeners: [
          BlocListener<YBDGameMicBloc, YBDGameMicBlocState>(
            listener: (context, state) {
              setState(() {
                // _gameUsers = state.micUserList;
              });
            },
          ),
        ],
        child: _micWrapView(isEnemy),
      );
    } else {
      return MultiBlocListener(
        listeners: [
          BlocListener<YBDRoomBloc, YBDRoomState>(
            listener: (context, state) {
              setState(() {
                _roomManagers = state.roomManagers;
              });
            },
          ),
          BlocListener<YBDMicBloc, YBDMicBlocState>(
            listener: (context, state) {
              setState(() {
                _micBeanS = state.micBeanList;
              });
            },
          ),
        ],
        child: _micWrapView(isEnemy),
      );
    }
  }
  void _operationListWrapJJsVuoyelive(YBDMiniProfileBlocState state, bool isEnemy) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _micWrapView(bool isEnemy) {
    List<YBDOperationItem> opList = YBDMiniProfileUtil.operationList(
      _myId,
      widget.roomId!,
      widget.userId!,
      _isFriend,
      roomManagers: _roomManagers,
      micBeanS: _micBeanS,
      isEnemy: isEnemy,
      isEnemyMuted: widget.isEnemyMuted,
      gameRoomType: widget.gameRoomType,
      // gameUsers: _gameUsers,
    );

    if (null == opList) {
      return Container();
    }

    return Container(
      child: Wrap(
        spacing: ScreenUtil().setWidth(0),
        // runSpacing: ScreenUtil().setWidth(20),
        children: List.generate(
          opList.length,
          (index) {
            return Material(
              type: MaterialType.transparency,
              child: InkWell(
                onTap: () {
                  if (opList[index].enable) {
                    logger.v('clicked operation item : ${opList[index].type}');
                    _operationEventHandler(opList[index].type);
                  } else {
                    YBDToastUtil.toast(translate('not_able'));
                  }
                },
                child: Container(
                  width: ScreenUtil().setWidth(100),
                  height: ScreenUtil().setWidth(100),
                  // color: Colors.pink,
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        (opList[index].enable ? opList[index].img : opList[index].imgDisable) ?? opList[index].img,
                        width: ScreenUtil().setWidth(56),
                        // color: opList[index].enable ? null : Colors.grey,
                      ),
                      SizedBox(height: ScreenUtil().setWidth(10)),
                      Text(
                        opList[index].name,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xffA4A4A4),
                          fontSize: ScreenUtil().setSp(20),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
  void _micWrapView9by3Soyelive(bool isEnemy) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理 mini profile 操作列表事件
  void _operationEventHandler(OperationType type) {
    switch (type) {
      case OperationType.Chat:
      case OperationType.SayHi:
        YBDRoomUtil.jumpToInboxPage(context, widget.userId);
        break;
      case OperationType.Mute:
        widget.gameRoomType
            ? YBDGameRoomSocketApi.getInstance().block(
                target: widget.userId.toString(),
                muteType: 'mute',
              )
            : YBDDialogUtil.showMuteUserDialog(
                context,
                type: ForbiddenUserType.Mute,
                okCallback: (int period) async {
                  logger.v('mute user : ${widget.userId} for $period minutes');
                  if (await YBDRoomUtil.checkForbiddenCondition(widget.roomId, widget.userId)) {
                    YBDRoomUtil.muteUser(widget.roomId, widget.userId, period);
                  }
                },
              );
        break;
      case OperationType.Block:
        YBDDialogUtil.showMuteUserDialog(
          context,
          type: ForbiddenUserType.Block,
          okCallback: (int period) async {
            logger.v('block user : ${widget.userId}');
            if (await YBDRoomUtil.checkForbiddenCondition(widget.roomId, widget.userId)) {
              widget.gameRoomType
                  ? YBDToastUtil.toast('game mic block user : ${widget.userId}')
                  : YBDRoomUtil.blockUser(widget.roomId, widget.userId);
            }
          },
        );
        break;
      case OperationType.Boot:
        widget.gameRoomType
            ? gameBoot()
            : YBDDialogUtil.showMuteUserDialog(
                context,
                type: ForbiddenUserType.Boot,
                okCallback: (int period) async {
                  logger.v('boot user : ${widget.userId} for $period minutes');
                  if (await YBDRoomUtil.checkForbiddenCondition(widget.roomId, widget.userId)) {
                    YBDRoomUtil.bootUser(widget.roomId, widget.userId, period);
                  }
                },
              );
        break;
      case OperationType.Guardian:
        YBDRoomUtil.addGuardian(widget.roomId, widget.userId, () {
          logger.v('add guardian success : ${widget.userId}');
          context.read<YBDRoomBloc>().addRoomMangersList(widget.userId);
        });
        break;
      case OperationType.CancelGuardian:
        YBDRoomUtil.cancelGuardian(widget.roomId, widget.userId, () {
          logger.v('cancel guardian success : ${widget.userId}');
          context.read<YBDRoomBloc>().removeRoomMangersList(widget.userId);
        });
        break;
      case OperationType.InviteMic:
        // 邀麦
        _inviteMic();
        break;
      case OperationType.UnMuteMic:
        _muteMic(false);
        break;
      case OperationType.MuteMic:
        _muteMic(true);
        break;
      case OperationType.UnMuteEnemyMic:
        _muteEnemyMic(false);

        break;
      case OperationType.MuteEnemyMic:
        _muteEnemyMic(true);
        break;
    }

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: widget.gameRoomType ? YBDLocationName.GAME_ROOM_PAGE : YBDLocationName.ROOM_PAGE,
      itemName: '$type',
    ));
  }
  void _operationEventHandleriol9Voyelive(OperationType type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  gameBoot() {
    ///游戏中玩家不能被踢
    if ((Get.find<YBDGameRoomGetLogic>().state?.gamePlaying ?? false) &&
        Get.find<YBDGameRoomGetLogic>().isUserOnSeat('${widget.userId}')) {
      YBDToastUtil.toast(translate('gaming_kicked_out'));
      return;
    }
    YBDTPGradientDialog.show(
      context,
      title: 'tip'.i18n,
      width: 530,
      height: 450,
      textHeight: 160,
      type: DialogType.tpCommon,
      showClose: false,
      showGridBg: false,
      buttonNum: 2,
      color: ColorType.blue,
      textWidth: 470,
      contentMsg: sprintf(translate('game_room_kick_point'), [_nickName.overflow(5)]),
      onConfirm: () {
        logger.v('boot:${widget.userId}');
        YBDGameRoomSocketApi.getInstance().block(target: widget.userId.toString(), muteType: 'boot');
      },
    );
  }

  /// 邀请用户上麦
  void _inviteMic() {
    int? curMicIndexSize = YBDRoomUtil.getMicSize(
      roomTag: BlocProvider.of<YBDRoomBloc>(context).state.roomTag,
      roomInfo: BlocProvider.of<YBDRoomBloc>(context).state.roomInfo,
    );

    logger.v('micIds : ${context.read<YBDMicBloc>().state.micIds!.length}, $curMicIndexSize');

    if (context.read<YBDMicBloc>().state.micIds!.contains(widget.userId)) {
      logger.v('_inviteMic already on mic');
      YBDToastUtil.toast('already on mic!');
    } else if (context.read<YBDMicBloc>().state.micIds!.length == curMicIndexSize) {
      logger.v('_inviteMic All mics occupied');
      YBDToastUtil.toast('All mics occupied!');
    } else {
      for (int i = 0; i < curMicIndexSize!; i++) {
        if (context.read<YBDMicBloc>().state.micBeanList![i]!.userId! <= 0 &&
            !context.read<YBDMicBloc>().state.micBeanList![i]!.lock!) {
          logger.v('_inviteMic widget.userId: ${widget.userId}');
          context.read<YBDMicBloc>().requestMic(widget.roomId ?? -1, 2, widget.userId ?? -1, i);
          break;
        }
      }
    }

    _pop();
  }
  void _inviteMicfEQtNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 禁麦/取消禁麦
  _muteMic(bool mute) {
    YBDMicBean? mic;
    if (null != context.read<YBDMicBloc>().state.micBeanList) {
      mic = context.read<YBDMicBloc>().state.micBeanList!.firstWhereOrNull(
            (element) => element!.userId == widget.userId,
            /*orElse: () => null,*/
          );
    }

    if (null != mic) {
      context.read<YBDMicBloc>().muteLocalMic(mic.micIndex, mic.userId, -1, mute, 1);
    }
  }

  /// PK对对手实行禁麦/取消禁麦
  _muteEnemyMic(bool mute) {
    logger.v('muted the board');
    YBDPKRTMHelper.sendRTMMsgMuteB(
      widget.userId,
      type: !mute ? RTMType.RTMTypeResumeMuteB : RTMType.RTMTypePauseMuteB,
    );
    _pop();
  }

  /// 举报按钮，关闭按钮
  Widget _topRow(YBDMiniProfileBlocState state) {
    // 顶部 ui 行高
    double height = 76;
    return Transform.translate(
      offset: Offset(0, -75.px),
      child: Container(
        // height: ScreenUtil().setWidth(height),
        // width: ScreenUtil().setWidth(460),
        // color: Colors.pink,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Column(
              children: [
                state.isOtherUser
                    ? YBDDelayGestureDetector(
                        onTap: () {
                          logger.v('clicked report user btn');
                          YBDDialogUtil.showReportUserDialog(
                            context,
                            widget.userId,
                            state.userInfo?.nickname,
                          );
                        },
                        child: Container(
                          width: ScreenUtil().setWidth(84),
                          height: ScreenUtil().setWidth(height),
                          alignment: Alignment.center,
                          child: Image.asset(
                            'assets/images/liveroom/icon_report@2x.webp',
                            width: ScreenUtil().setWidth(40),
                          ),
                        ),
                      )
                    : Container(
                        width: ScreenUtil().setWidth(84),
                        height: ScreenUtil().setWidth(height),
                      ),
                // SizedBox(height: 20.px)
              ],
            ),

            Spacer(),
            Container(
              height: 154.px,
            ),
            /*
            // 头像 将头像移到最上层
            Stack(
              children: [
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(12), horizontal: ScreenUtil().setWidth(30)),
                  child: Container(
                    width: 130.px,
                    height: 130.px,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(70.px),
                      border: Border.all(width: 2.px, style: BorderStyle.solid, color: Colors.white),
                    ),
                    child: YBDRoundAvatar(
                      state.userInfo?.headimg,
                      scene: 'B',
                      sex: state.userInfo?.sex,
                      avatarWidth: 130,
                      labelWitdh: 40,
                      userId: widget.userId,
                      lableSrc: (state.userInfo?.vip ?? 0) == 0 ? '' : 'assets/images/vip_lv${state.userInfo.vip}.png',
                      closeCallback: widget.closeCallback,
                      levelFrame: state?.userInfo?.headFrame,
                    ),
                  ),
                ),
                Positioned(
                    right: 0,
                    bottom: 0,
                    child: YBDVipIcon(
                      state?.userInfo?.vipIcon,
                      size: 68,
                    ))
              ],
            ),
             */
            Spacer(),
            // todo @
            (state.isOtherUser && !widget.gameRoomType)
                ? GestureDetector(
                    onTap: () {
                      logger.v('clicked @ mini profile btn');
                      _pop();
                      YBDToastUtil.toast('game mic---AtUser:$_nickName');
                      YBDUserInfo? atUserInfo = context.read<YBDRoomBloc>().state.viewerList?.firstWhereOrNull(
                            (element) => element.id == widget.userId,
                          );
                      String? nickName = atUserInfo != null ? atUserInfo.nickname : '${widget.userId}';
                      context.read<YBDRoomBloc>().setAtUserNickName(nickName);
                    },
                    child: Container(
                      width: ScreenUtil().setWidth(84),
                      height: ScreenUtil().setWidth(height),
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/images/liveroom/icon_@@2x.webp',
                        width: ScreenUtil().setWidth(40),
                        // color: Color(0xffFE751A),
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                : Container(
                    width: ScreenUtil().setWidth(84),
                    height: ScreenUtil().setWidth(height),
                  ),
          ],
        ),
      ),
    );
  }
  void _topRowdrPlIoyelive(YBDMiniProfileBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// ID, 性别，用户等级，房间等级，签约主播标签
  Widget _tagRow(YBDMiniProfileBlocState state) {
    bool hasUid = state.userInfo?.uniqueNum != null;
    return Transform.translate(
      offset: Offset(0, -50.px),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          YBDMiniProfileId(hasUid ? state.userInfo?.uniqueNum : widget.userId.toString(), hasUid),
          // SizedBox(width: ScreenUtil().setWidth(10)),
          if (_certificationInfos == null || _certificationInfos?.hidUserLevel == 0)
            Padding(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(9)),
              child: YBDLevelTag(state.userInfo?.level),
            ),
          if (_certificationInfos == null || _certificationInfos?.hidTalentLevel == 0)
            YBDRoomLevelTag(state.userInfo?.roomlevel),
          if (_certificationInfos == null || _certificationInfos?.hidGender == 0) _genderLabel(state),
          // if (_certificationInfos == null || _certificationInfos.hidBadge == 0) YBDWearBadgeItem(state.userInfo?.badges),
          if (_certificationInfos == null || _certificationInfos?.hidTalent == 0)
            if (state.isOfficialTalent ?? false) ...[
              SizedBox(width: ScreenUtil().setWidth(6)),
              Image.asset(
                'assets/images/icon_talent.webp',
                width: ScreenUtil().setWidth(30),
              ),
            ]
        ],
      ),
    );
  }

  /// 性别标签
  Widget _genderLabel(YBDMiniProfileBlocState state) {
    SexType sex = SexType.Unknown;

    if (state.userInfo?.sex == 1) {
      sex = SexType.Female;
    } else if (state.userInfo?.sex == 2) {
      sex = SexType.Male;
    }

    return YBDMiniProfileSex(
      sex,
      YBDDateUtil.calculateAge(state.userInfo?.birthday),
    );
  }
  void _genderLabelp1URBoyelive(YBDMiniProfileBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 好友，关注，粉丝
  Widget _friendsRow(YBDMiniProfileBlocState state) {
    return Transform.translate(
      offset: Offset(0, -30.px),
      child: Wrap(
        spacing: ScreenUtil().setWidth(60),
        children: List.generate(3, (index) {
          double itemHeight = ScreenUtil().setWidth(70);
          if (state.isOtherUser) itemHeight = ScreenUtil().setWidth(80);
          if (index == 0) {
            return YBDRelationshipItem(
              'Friends',
              '${state.userInfo?.friends ?? 0}',
              () {
                logger.v('clicked friends btn');
                YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.follow + '/${state.userInfo?.id}/0');
              },
              height: itemHeight,
            );
          } else if (index == 1) {
            return YBDRelationshipItem(
              'Following',
              '${state.userInfo?.concern ?? 0}',
              () {
                logger.v('clicked following btn');
                YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.follow + '/${state.userInfo?.id}/1');
              },
              height: itemHeight,
            );
          } else {
            return YBDRelationshipItem(
              'Followers',
              '${state.userInfo?.fans ?? 0}',
              () {
                logger.v('clicked followers btn');
                YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.follow + '/${state.userInfo?.id}/2');
              },
              height: itemHeight,
            );
          }
        }),
      ),
    );
  }

  /// 私信，送礼，关注
  Widget _bottomRow(YBDMiniProfileBlocState state) {
    if (null == state.userInfo?.id) {
      // 用户信息为空时不显示送礼按钮和关注按钮
      return Container();
    } else {
      if (widget.gameRoomType) {
        // 游戏房只显示关注按钮
        return YBDMiniProfileFollow(widget.userId, widget.roomId);
      }

      return Transform.translate(
        offset: Offset(0, showJumpToRoom() ? 0 : -20.px),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(width: 74.px),
            // 当点击为敌人时，按钮要变成chat
            // chat
            YBDScaleAnimateButton(
              onTap: () {
                YBDRoomUtil.jumpToInboxPage(context, widget.userId);
              },
              child: _item('assets/images/liveroom/profile_chat@2x.webp', translate('chat')),
            ),
            // gift
            Spacer(),
            // SizedBox(width: 96.px),
            widget.isClickOnPkEnemy
                ? Container()
                : YBDScaleAnimateButton(
                    onTap: () {
                      logger.v('mini profile gift callback');
                      _pop();
                      if (widget.isClickOnPkEnemy) {
                        YBDRoomUtil.jumpToInboxPage(context, widget.userId);
                      } else {
                        YBDReceiverUser specUser = YBDReceiverUser();
                        specUser.userId = widget.userId;
                        specUser.img = state.userInfo?.headimg;
                        specUser.name = state.userInfo?.nickname;
                        specUser.position = 'User';
                        YBDRoomUtil.showGiftSheet(
                          specUsers: [specUser],
                        );
                      }
                    },
                    child: _item('assets/images/liveroom/profile_sent_gift@2x.webp', translate('send_gift')),
                  ),
            Spacer(),
            // SizedBox(width: 96.px),
            // follow
            YBDMiniProfileFollow(widget.userId, widget.roomId),
            SizedBox(width: 74.px),
          ],
        ),
      );
    }
  }
  void _bottomRowijTORoyelive(YBDMiniProfileBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // chat gift follow item
  Widget _item(String img, String text) {
    //
    return Container(
      width: 96.px,
      // height: 70.px,
      child: Column(
        children: [
          Image.asset(img, width: 70.px, height: 70.px),
          SizedBox(height: 5.px),
          Text(
            text,
            style: TextStyle(fontSize: 20.sp2, color: Colors.black.withOpacity(0.5)),
          ),
        ],
      ),
    );
  }

  Widget _jumpToRoom() {
    return GestureDetector(
      onTap: () {
        logger.v('clicked jump to room: ${widget.userId}');
        _pop();
        eventBus.fire(YBDChangeRoomBusEvent(widget.userId));
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.ROOM_PAGE,
          itemName: 'pk_jump_to_enemy',
        ));
      },
      child: Container(
        width: ScreenUtil().setWidth(314),
        height: ScreenUtil().setWidth(56),
        margin: EdgeInsets.only(bottom: 40.px),
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/liveroom/pk/pk_room_jump.webp'),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(left: ScreenUtil().setWidth(150)),
          child: Text(
            'On Live     >>',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(24),
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  void _pop() {
    if (widget.closeCallback != null) {
      widget.closeCallback!.call();
    } else {
      Navigator.pop(context);
    }
  }
}
