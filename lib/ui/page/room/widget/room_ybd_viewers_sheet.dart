import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/room/define/room_ybd_define.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_speaker_list.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../bloc/room_ybd_bloc.dart';
import '../bloc/room_ybd_bloc_state.dart';
import '../util/mini_ybd_profile_util.dart';
import 'room_ybd_viewer_item.dart';

/// 处理在线用户列表页面事件的回调
typedef RoomViewersSheetCallback = Function(YBDUserInfo);

/// 房间在线用户和管理员页面
class YBDRoomViewersSheet extends StatefulWidget {
  /// 选中的标签栏 0, 在线用户列表； 1，管理员列表
  final int index;

  /// 当前房间ID
  final int? roomId;

  YBDRoomViewersSheet(
    this.roomId, {
    this.index = 0,
  });

  @override
  State<StatefulWidget> createState() => _YBDRoomViewersSheetState();
}

class _YBDRoomViewersSheetState extends State<YBDRoomViewersSheet> with SingleTickerProviderStateMixin {
  TabController? _tabController;

  /// 管理员权限说明栏高度
  double _guardianPrivilegeHeight = 0;

  /// 管理员权限说明栏透明度
  double _opacity = 0.0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabController!.animateTo(widget.index);
  }
  void initStateXVq2loyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return BlocListener<YBDRoomBloc, YBDRoomState>(
      listener: (context, state) {
        if (state.showKeyboard ?? false) {
          if (mounted) {
            Navigator.pop(context);
          }
        }
      },
      child: BlocBuilder<YBDRoomBloc, YBDRoomState>(
        buildWhen: (prev, current) => current.type == RoomStateType.KeyboardUpdated,
        builder: (context, state) {
          return Container(
            height: ScreenUtil().screenHeight * 0.7,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(ScreenUtil().setWidth(32)),
                  topRight: Radius.circular(ScreenUtil().setWidth(32)),
                ),
              ),
              child: Column(
                children: [
                  Container(
                    width: ScreenUtil().screenWidth,
                    height: ScreenUtil().setWidth(96),
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          // 关闭按钮
                          top: 0,
                          right: 0,
                          child: _closeBtn(),
                        ),
                        // 标签栏
                        _tabBar(),
                      ],
                    ),
                  ),
                  // Container(
                  //   // 分割线
                  //   color: Color(0xffEAEAEA),
                  //   height: ScreenUtil().setWidth(1),
                  // ),
                  Expanded(
                    // tab 内容
                    child: TabBarView(
                      controller: _tabController,
                      children: [
                        viewersContent(),
                        speakersContent(),
                        guardianContent(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
  void buildf2ymVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 标签栏
  Widget _tabBar() {
    return Center(
      child: Container(
        padding: EdgeInsets.fromLTRB(
          ScreenUtil().setWidth(64),
          ScreenUtil().setWidth(16),
          ScreenUtil().setWidth(64),
          ScreenUtil().setWidth(0),
        ),
        child: TabBar(
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(
              width: ScreenUtil().setWidth(4),
              color: Color(0xff47CDCC),
            ),
            insets: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(110)),
          ),
          labelColor: Colors.black,
          unselectedLabelColor: Colors.black.withOpacity(0.5),
          controller: _tabController,
          labelStyle: TextStyle(fontSize: ScreenUtil().setSp(24)),
          unselectedLabelStyle: TextStyle(fontSize: ScreenUtil().setSp(24)),
          tabs: [
            // TODO: 翻译
            Tab(text: 'Viewers'),
            Tab(text: 'Speakers'),
            Tab(text: 'Guardians'),
          ],
        ),
      ),
    );
  }
  void _tabBarkTEs2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关闭按钮
  Widget _closeBtn() {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(ScreenUtil().setWidth(32)),
        ),
        onTap: () {
          logger.v('clicked room user bottom sheet close btn');
          Navigator.pop(context);
        },
        child: Container(
          width: ScreenUtil().setWidth(88),
          height: ScreenUtil().setWidth(88),
          padding: EdgeInsets.fromLTRB(
            ScreenUtil().setWidth(20),
            ScreenUtil().setWidth(8),
            ScreenUtil().setWidth(8),
            ScreenUtil().setWidth(20),
          ),
          child: Image.asset(
            'assets/images/icon_close.png',
            fit: BoxFit.cover,
            width: 20.px,
            color: Color(0xff363F4D),
          ),
        ),
      ),
    );
  }
  void _closeBtn1nUN5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 在线用户标签内容
  Widget viewersContent() {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
      buildWhen: (prev, current) =>
          current.type == RoomStateType.ViewerListUpdated || current.type == RoomStateType.UserCountUpdated,
      builder: (_, state) {
        if (state.viewerList == null || state.viewerList!.isEmpty) {
          return YBDPlaceHolderView(img: 'assets/images/empty/empty_my_profile.webp', text: 'No viewer');
        } else {
          return Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 16.px as double, bottom: 14.px as double),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text.rich(TextSpan(style: TextStyle(color: Color(0xff666666), fontSize: 24.sp2), children: [
                      TextSpan(text: "Room Visitors  "),
                      TextSpan(text: "(", style: TextStyle(fontSize: 22.sp2)),
                      TextSpan(
                          text: state.curOnLineUserCount.toString(),
                          style: TextStyle(color: Color(0xff49C9CE), fontSize: 22.sp2)),
                      TextSpan(text: "/${state.onLineUserCount})", style: TextStyle(fontSize: 22.sp2))
                    ])),
                    SizedBox(
                      width: 24.px,
                    )
                  ],
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: state.viewerList!.length,
                  itemBuilder: (_, index) {
                    return Container(
                      height: ScreenUtil().setWidth(130),
                      child: YBDRoomViewerItem(
                        state.viewerList![index],
                        widget.roomId,
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 20.px,
              ),
              Center(
                  child: Text(
                "— Show only viewers who are in the room at this time —",
                style: TextStyle(fontSize: 24.sp2, color: Color(0xff666666)),
              )),
              SizedBox(
                height: 20.px,
              )
            ],
          );
        }
      },
    );
  }
  void viewersContentYv5hUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 在线用户标签内容
  Widget speakersContent() {
    return YBDRoomSpeakerList(
      roomId: widget.roomId,
    );
  }

  /// 管理员标签内容
  Widget guardianContent() {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
      buildWhen: (prev, current) => current.type == RoomStateType.ManagerListUpdated,
      builder: (_, state) {
        int _count = YBDMiniProfileUtil.getMaxGuardianCount(context);
        if (state.roomManagers == null || state.roomManagers!.isEmpty) {
          // TODO: 翻译
          return Column(
            children: <Widget>[
              SizedBox(height: ScreenUtil().setWidth(20)),
              // 守护者列表无守护者时，显示守护者权限
              _guardianPrivilegesContainer(),
              SizedBox(height: ScreenUtil().setWidth(20)),
              Expanded(
                child: YBDPlaceHolderView(
                  img: 'assets/images/empty/empty_my_profile.webp',
                  text: 'No guardian',
                  textColor: Colors.black,
                ),
              )
            ],
          );
        } else {
          return Column(
            children: <Widget>[
              SizedBox(height: ScreenUtil().setWidth(20)),
              Stack(
                // 顶部标题
                children: <Widget>[
                  Center(
                    child: Container(
                      alignment: Alignment.center,
                      height: ScreenUtil().setWidth(88),
                      child: Text(
                        // TODO: 翻译
                        'Max $_count guardians allowed',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(28),
                          color: Color(0xff909399),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // 帮助按钮
                    right: 0,
                    child: _questionBtn(),
                  )
                ],
              ),
              // 点帮助按钮展开管理员权限说明
              _guardianPrivilegesContainer(),
              SizedBox(height: ScreenUtil().setWidth(20)),
              Expanded(
                child: ListView.builder(
                  // 管理员列表
                  itemCount: state.roomManagers!.length,
                  itemBuilder: (context, index) {
                    return Container(
                      height: ScreenUtil().setWidth(130),
                      child: YBDRoomViewerItem(
                        state.roomManagers![index],
                        widget.roomId,
                      ),
                    );
                  },
                ),
              ),
            ],
          );
        }
      },
    );
  }
  void guardianContentTR7ikoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 管理员权限说明
  Widget _guardianPrivilegesContainer() {
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      height: _guardianPrivilegeHeight,
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
      padding: EdgeInsets.only(top: ScreenUtil().setWidth(26)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(ScreenUtil().setWidth(34)),
        color: Color(0xffF7F7F7),
      ),
      child: AnimatedOpacity(
        duration: Duration(milliseconds: 500),
        opacity: _opacity,
        child: Column(
          children: <Widget>[
            Center(
              child: Text(
                translate('guardian_privilege'),
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  color: Color(0xff4B4B4C),
                ),
              ),
            ),
            SizedBox(height: ScreenUtil().setWidth(30)),

            // SizedBox(
            //   width: 500.px,
            //   child: Wrap(
            //     spacing: ScreenUtil().setWidth(40),
            //     runSpacing: ScreenUtil().setWidth(40),
            //     children: <Widget>[
            //       _guardianPrivilegesItem('assets/images/liveroom/icon_lock_mic@2x.webp', 'Lock/Unlock Mic'),
            //       // 禁麦权限
            //       _guardianPrivilegesItem('assets/images/liveroom/icon_mute_mic2@2x.webp', 'Mute/Unmute Mic'),
            //       // 邀请上麦
            //       _guardianPrivilegesItem('assets/images/liveroom/icon_take_mic@2x.webp', 'Invite Mic'),
            //       _guardianPrivilegesItem('assets/images/liveroom/icon_ban@2x.webp', 'Mute User'),
            //     ],
            //   ),
            // ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                // 锁麦权限
                // TODO: 翻译
                _guardianPrivilegesItem('assets/images/liveroom/icon_lock_mic@2x.webp', 'Lock/Unlock Mic'),
                // 禁麦权限
                _guardianPrivilegesItem('assets/images/liveroom/icon_mute_mic2@2x.webp', 'Mute/Unmute Mic'),
                // 邀请上麦
                _guardianPrivilegesItem('assets/images/liveroom/icon_take_mic@2x.webp', 'Invite Mic'),
              ],
            ),
            SizedBox(height: ScreenUtil().setWidth(10)),
            // 禁言权限
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                // SizedBox(width: ScreenUtil().setWidth(34)),
                Container(child: _guardianPrivilegesItem('assets/images/liveroom/icon_ban@2x.webp', 'Mute User')),
                Container(width: 170.px, height: 100.px),
                Container(width: 170.px, height: 100.px),
              ],
            ),
          ],
        ),
      ),
    );
  }
  void _guardianPrivilegesContainerKp6oaoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 管理员权限 item
  Widget _guardianPrivilegesItem(String img, String name) {
    return Container(
      width: 170.px,
      height: 100.px,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(56),
            // color: Colors.pink,
            child: Image.asset(
              img,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: ScreenUtil().setWidth(10)),
          Container(
            // color: Colors.blue,
            child: Text(
              name,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(20),
                color: Color(0xff4B4B4C),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void _guardianPrivilegesItemuo4Eboyelive(String img, String name) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 问号按钮
  Widget _questionBtn() {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        onTap: () {
          logger.v('clicked guardian question btn');
          setState(() {
            _guardianPrivilegeHeight = _guardianPrivilegeHeight == 0 ? ScreenUtil().setWidth(304) : 0;
            _opacity = _opacity == 0.0 ? 1.0 : 0.0;
          });
        },
        child: Container(
          width: ScreenUtil().setWidth(88),
          height: ScreenUtil().setWidth(88),
          padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
          child: Image.asset(
            'assets/images/liveroom/room_guardian_question.png',
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
