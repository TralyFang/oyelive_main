import 'dart:async';


import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_activity_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/room/room_ybd_api_helper.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../home/entity/banner_ybd_advertise_entity.dart';

class YBDRoomSlider extends StatefulWidget {
  @override
  YBDRoomSliderState createState() => new YBDRoomSliderState();
}

class YBDRoomSliderState extends BaseState<YBDRoomSlider> with SingleTickerProviderStateMixin {
  Store<YBDAppState>? store;
  int initPage = 999999;
  PageController? controller;
  List<String> imagePath = [
    // 'assets/images/topup/y_topup_icon.png',
    // 'assets/images/weekly_star_slider.png',
    // 'assets/images/slider_support.webp',
  ];

  List<Function> onTaps = [];
  List<YBDBannerAdvertiseRecord?>? activityList;

  Timer? changeRountine;
  Duration changeInterval = Duration(seconds: 5);
  @override
  Widget myBuild(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          width: ScreenUtil().setWidth(100),
          height: ScreenUtil().setWidth(100),
          child: imagePath.length > 0 && onTaps.length > 0 ? _pageView() : SizedBox(),
        ),
        //2021/8/19 11：45 删除掉两者之间的间距，瑶瑶决定 slider 图片自己做间距
        Row(
          children: List.generate(
              imagePath.length,
              (_) => Container(
                    width: ScreenUtil().setWidth(10),
                    height: ScreenUtil().setWidth(10),
                    margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(4)),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: (initPage % imagePath.length) == _ ? Colors.white : Colors.white.withOpacity(0.2)),
                  )),
        ),
      ],
    );
  }
  void myBuildYwMSAoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// [imagePath] 有数据时显示pageView
  Widget _pageView() {
    // 配置控制器
    _configPageController();
    return PageView.builder(
      controller: controller,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        int curIndex = index % imagePath.length;
        String curImage = imagePath[curIndex];
        return GestureDetector(
          onTap: onTaps[curIndex] as void Function()?,
          child: Padding(
            padding: EdgeInsets.all(ScreenUtil().setWidth(curImage.contains('support') ? 20 : 0)),
            child: curImage.contains('http')
                ? YBDNetworkImage(
                    imageUrl: curImage,
                    placeholder: (_, x) {
                      return YBDLoadingCircle();
                    })
                : new Image.asset(curImage),
          ),
        );
      },
    );
  }
  void _pageViewmO3jvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// pageView控制器
  void _configPageController() {
    if (null == controller) {
      controller = PageController(initialPage: initPage);
      controller!.addListener(() {
        initPage = controller!.page!.floor();
        setState(() {});
      });

      changeRountine = Timer.periodic(changeInterval, (_) {
        initPage++;
        controller?.nextPage(duration: Duration(milliseconds: 500), curve: Curves.ease);
      });
    }
  }
  void _configPageControllerqDZxdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 之前是从配置项获取一个活动的信息
  addConfigIcon() {
    if (store!.state.configs!.appEventRanking != null && store!.state.configs!.appEventRanking!.isNotEmpty) {
      List<String> eventInfoParts = store!.state.configs!.appEventRanking!.split('@');
      String eventName = eventInfoParts[0];
      String url = eventInfoParts[1];
      String headImg = eventInfoParts[2];
      imagePath.add(headImg);
      onTaps.add(() {
        logger.v('===slider bar web $eventName, $url');
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.ROOM_PAGE,
          itemName: YBDItemName.ROOM_PAGE_SLIDER,
          value: eventName,
        ));
        YBDNavigatorHelper.navigateToWeb(
          context,
          "?url=${Uri.encodeComponent(url)}&shareName=$eventName&title=$eventName",
        );
      });
      print(eventInfoParts);
      setState(() {});
    }
  }

  // 添加接口返回的数据
  addActivities() {
    if (activityList != null && activityList!.isNotEmpty) {
      print(' ${activityList!.length}');
      logger.v('activity list amount : ${activityList!.length}');
      for (YBDBannerAdvertiseRecord? activity in activityList!) {
        imagePath.add(YBDImageUtil.ad(context, activity!.img, 'A'));
        print('3.31------${activity.img}---${activity.adname}---${activity.adurl}');
        onTaps.add(() {
          logger.v('===slider bar web ${activity.adname}, ${activity.adurl}');
          YBDCommonTrack().commonTrack(YBDTAProps(location: 'slider', module: YBDTAModule.liveroom));
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.ROOM_PAGE,
            itemName: YBDItemName.ROOM_PAGE_SLIDER,
            value: activity.adname,
          ));
          // 进入活动埋点
          YBDActivityInTrack().activityIn(YBDTAProps(location: YBDTAEventLocation.slider, url: activity.adurl));
          YBDNavigatorHelper.navigateToWeb(
            context,
            "?url=${Uri.encodeComponent(activity.adurl!)}&shareName=${activity.adname}&title=${activity.adname}&entranceType=${WebEntranceType.Slider.index}",
          );
        });
      }
      setState(() {});
    } else {
      logger.v('activity list is null');
    }
  }

  /// 请求活动数据
  Future<List<YBDBannerAdvertiseRecord?>?> requestRoomSliderActivity() async {
    if (mounted) {
      YBDBannerAdvertiseEntity? advertiseEntity = await YBDRoomApiHelper.queryRoomSliderActivity(context);
      if (null != advertiseEntity && advertiseEntity.record != null) {
        return advertiseEntity.record;
      } else {
        logger.v('Room Slider Activity is null');
        return null;
      }
    }
  }

  @override
  void initState() {
    super.initState();
    // controller = new PageController(initialPage: initPage);

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      // 从接口获取数据
      activityList = await requestRoomSliderActivity();
      store = YBDCommonUtil.storeFromContext();

      // onTaps = [
      // // 充值活动跳转
      // () {
      //   YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.purchase_detail);
      // },
      // // 周星活动
      // () {
      //   logger.v('===slider bar weekly star');
      //   YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      //     YBDEventName.CLICK_EVENT,
      //     location: YBDLocationName.ROOM_PAGE,
      //     itemName: YBDItemName.ROOM_PAGE_SLIDER,
      //     value: 'Weekly Star',
      //   ));
      //   YBDNavigatorHelper.navigateToWeb(
      //     context,
      //     "?url=${Uri.encodeComponent(Const.ROOM_WEEKLY_STAR)}&shareName=${'Weekly Star'}&title=${'Weekly Star'}",
      //   );
      // },
      // // 客服
      // () {
      //   LivechatInc.start_chat(Const.LIVECHAT_KEY_LICENCE, "", store.state.bean?.nickname ?? 'Visitor',
      //       store.state.bean?.email ?? 'No Email');
      // }
      // ];
      // addConfigIcon();
      addActivities();
    });
    // controller.addListener(() {
    //   initPage = controller.page.floor();
    //   setState(() {});
    // });

    // changeRountine = Timer.periodic(changeInterval, (_) {
    //   initPage++;
    //   controller.nextPage(duration: Duration(milliseconds: 500), curve: Curves.ease);
    // });
  }
  void initState0cRkJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    changeRountine?.cancel();
    controller?.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDRoomSlider oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
