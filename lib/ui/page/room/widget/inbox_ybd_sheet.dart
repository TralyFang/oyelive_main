import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:sqlcool/sqlcool.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/db_ybd_config.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/inbox/db/model_ybd_conversation.dart';
import '../../inbox/item_ybd_conversation.dart';
import '../../../widget/loading_ybd_circle.dart';

class YBDInboxSheet extends StatefulWidget {
  @override
  YBDInboxSheetState createState() => new YBDInboxSheetState();
}

class YBDInboxSheetState extends BaseState<YBDInboxSheet> {
  SelectBloc? bloc;

  bool isEmptyConversation = false;

  List<YBDConversationModel> sortConversationList(List<YBDConversationModel> list) {
    List<YBDConversationModel> top = list.where((element) => element.isAtTop!).toList();
    List<YBDConversationModel> normal = list.where((element) => !element.isAtTop!).toList();
    return top
      ..addAll(normal)
      ..removeWhere((element) => (element.lastMessage?.isEmpty ?? true) || element.conversationType != "friend");
  }
  void sortConversationListnkV3woyelive(List<YBDConversationModel> list) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Container(
      height: ScreenUtil().screenHeight * 0.5,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(16)))),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(ScreenUtil().setWidth(16)), topRight: Radius.circular(ScreenUtil().setWidth(16))),
        child: Column(
          children: [
            Container(
              width: ScreenUtil().screenWidth,
              height: ScreenUtil().setWidth(116),
              child: Stack(
                children: <Widget>[
//                  Positioned(
//                    // 关闭按钮
//                    top: 0,
//                    bottom: 0,
//                    right: ScreenUtil().setWidth(30),
//                    child: Container(
//                      width: ScreenUtil().setWidth(60),
//                      height: ScreenUtil().setWidth(60),
//                      child: TextButton(
//                        onPressed: () {
//                          Navigator.pop(context);
//                        },
//                        child: Icon(Icons.cancel, color: Color(0xff626262)),
//                      ),
//                    ),
//                  ),
                  Center(
                    child: Text(
                      translate('Inbox'),
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(32), color: Color(0xff3A3A3A)),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: ScreenUtil().setWidth(1),
              color: Color(0xffEAEAEA),
            ),
            Expanded(
              child: StreamBuilder<List<Map<String, dynamic>>?>(
                stream: bloc?.items,
                builder: (BuildContext context, AsyncSnapshot<List<Map<String, dynamic>>?> snapshot) {
                  if (snapshot.hasData) {
                    List<YBDConversationModel> conversationList = sortConversationList(
                        List.generate(snapshot.data!.length, (x) => YBDConversationModel().fromDb(snapshot.data![x])));
                    if (conversationList.length == 0) {
                      return Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            // TODO: 换图片
                            Image.asset(
                              "assets/images/empty/empty_my_profile.webp",
                              width: ScreenUtil().setWidth(382),
                            ),
                            SizedBox(height: ScreenUtil().setWidth(30)),
                            Text(
                              translate('no_message'),
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      );
                    }
                    return ListView.separated(
                        padding: EdgeInsets.all(0),
                        itemBuilder: (_, index) => YBDConversationItem(
                              conversationList[index],
                              null,
                              isBlackText: true,
                            ),
                        separatorBuilder: (_, index) => Padding(
                              padding: EdgeInsets.only(
                                left: ScreenUtil().setWidth(112),
                              ),
                              child: Container(
                                height: ScreenUtil().setWidth(1),
                                color: Color(0xffEAEAEA),
                              ),
                            ),
                        itemCount: conversationList.length);
                  } else {
                    return Container(
                        width: ScreenUtil().setWidth(100), height: ScreenUtil().setWidth(100), child: YBDLoadingCircle());
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuild0G1VSoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    try {
      bloc = SelectBloc(
        database: messageDb!,
        table: YBDConversationModel.conversationTableName,
        orderBy: "update_time DESC",
        reactive: true,
      );
    } catch (e) {
      logger.v('init SelectBloc error : $e');
    }
  }
  void initStateJnmtkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDInboxSheet oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesrDQU9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
