import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';
import 'package:oyelive_main/ui/widget/bounce_ybd_animation.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../module/room/entity/gift_ybd_package_entity.dart';

class YBDGiftItem extends StatefulWidget {
  bool isSelected;
  YBDGiftPackageRecordGift? giftInfo;
  bool isMyGift;
  bool isStatus;
  bool blackNUmber;

  YBDGiftItem(this.isSelected, this.giftInfo, {this.isMyGift: false, this.isStatus: false, this.blackNUmber: false});

  @override
  YBDGiftItemState createState() => new YBDGiftItemState();
}

class YBDGiftItemState extends BaseState<YBDGiftItem> {
  getLeftDays() {
    return Duration(seconds: widget.giftInfo!.expireAfter!).inDays.toString();
  }

  @override
  Widget myBuild(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(16.px as double)),
        border: widget.isSelected
            ? Border.all(
                width: 1.px as double,
                color: Color(0xff49C9CE),
              )
            : null,
      ),
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Positioned.fill(
            child: Opacity(
              // 礼物受限制时设置透明度
              opacity: _isStintType() ? 0.5 : 1.0,
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  // 图片，礼物名称，价格
                  _giftContent(),
                  // 背包礼物的过期时间和数量
                  Positioned(
                    top: ScreenUtil().setWidth(10),
                    left: ScreenUtil().setWidth(12),
                    right: ScreenUtil().setWidth(12),
                    child: _expireDateAndCount(),
                  ),
                  // 礼物限制说明标签
                  Positioned(
                    top: ScreenUtil().setWidth(10),
                    left: ScreenUtil().setWidth(0),
                    child: _restrictionIcon(),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 60.px,
            // 礼物限制锁
            child: _lockMask(),
          ),
        ],
      ),
    );
  }
  void myBuildBXeSeoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 背包礼物的过期时间和数量
  Widget _expireDateAndCount() {
    if (!widget.isMyGift) {
      // 非背包礼物item不展示过期时间和数量
      return SizedBox();
    }

    return Container(
      child: Row(
        children: [
          _expirationRow(widget.giftInfo!.expireAfter == -1),
          Expanded(child: SizedBox()),
          Container(
            width: ScreenUtil().setWidth(48),
            height: ScreenUtil().setWidth(20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(100))),
                gradient: LinearGradient(
                  colors: [
                    Color(0xff47CDCC),
                    Color(0xff5E94E7),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                )),
            alignment: Alignment.center,
            child: Text(
              "x${widget.giftInfo!.number}",
              style: TextStyle(
                color: Colors.white,
                fontSize: ScreenUtil().setSp(14),
              ),
            ),
          )
        ],
      ),
    );
  }
  void _expireDateAndCount05Y00oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _expirationRow(bool hideDate) {
    Widget expireRow = SizedBox();
    var textColor = widget.isStatus ? Colors.black.withOpacity(0.7) : Color(0xffAFAFAF);

    if (!hideDate) {
      // 显示日期
      expireRow = Container(
        child: Row(
          children: [
            Container(
              width: 12.px,
              child: Image.asset(
                "assets/images/icon_clock.png",
                fit: BoxFit.cover,
                color: textColor,
              ),
            ),
            SizedBox(width: 4.px),
            Text(
              '${getLeftDays()} ${translate('days_left')}',
              style: TextStyle(
                color: textColor,
                fontSize: 16.sp2,
              ),
            )
          ],
        ),
      );
    }

    return expireRow;
  }
  void _expirationRow3OlnZoyelive(bool hideDate) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 图片，礼物名称，价格
  Column _giftContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: ScreenUtil().setWidth(16),
        ),
        Builder(builder: (context) {
          final child = YBDNetworkImage(
            imageUrl: YBDImageUtil.gift(context, widget.giftInfo!.img, "B"),
            width: ScreenUtil().setWidth(96),
            height: ScreenUtil().setWidth(96),
          );

          if (widget.isSelected) {
            return YBDBounceAnimation(
              child: child,
            );
          }

          return child;
        }),
        SizedBox(
          height: ScreenUtil().setWidth(12),
        ),
        Text(
          widget.giftInfo!.name!,
          style: TextStyle(
              fontSize: ScreenUtil().setSp(20),
              color: widget.isStatus ? Colors.black.withOpacity(0.7) : Color(0xffAFAFAF)),
        ),
        SizedBox(
          height: ScreenUtil().setWidth(5),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              'assets/images/tiny_bean.webp',
              width: ScreenUtil().setWidth(16),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(6),
            ),
            Text(
              widget.giftInfo!.price.toString(),
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(20),
                  color: widget.isStatus ? Colors.black : Colors.white,
                  fontWeight: FontWeight.w500),
            ),
          ],
        ),
      ],
    );
  }

  /// 受限制的礼物遮罩和加锁图标
  Widget _restrictionIcon() {
    return YBDLevelStintTag(
      stintType: widget.giftInfo!.conditionType,
      stintLv: widget.giftInfo!.conditionExtends,
      scale: 0.8,
    );
  }
  void _restrictionIconD27gXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 受限制的礼物
  bool _isStintType() {
    return YBDCommonUtil.stintBuy(
      type: widget.giftInfo!.conditionType,
      lv: widget.giftInfo!.conditionExtends,
    );
  }

  /// 受限制的礼物遮罩和加锁图标
  Widget _lockMask() {
    if (_isStintType()) {
      // 受限制的礼物item
      return IgnorePointer(
        child: Container(
          // color: widget.isStatus ? Colors.white.withOpacity(0.5) : Colors.black.withOpacity(0.4),
          child: Center(
            child: SizedBox(
              width: 25.px,
              child: Image.asset(
                widget.isStatus ? 'assets/images/icon_key_blue@2x.webp' : 'assets/images/icon_key@2x.webp',
                fit: BoxFit.cover,
                // color: Color(0),
              ),
            ),
          ),
        ),
      );
    }

    // 无限制的礼物item
    return SizedBox();
  }
  void _lockMask7HfcVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
