import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/widget/gift_ybd_category_row.dart';
import 'package:oyelive_main/module/room/entity/gift_ybd_package_entity.dart';
import 'package:oyelive_main/ui/page/room/bloc/gift_ybd_sheet_bloc.dart';

class YBDRoomGiftCategoryRow extends StatelessWidget {
  YBDRoomGiftCategoryRow({Key? key, this.giftPages}) : super(key: key);

  /// 分类列表，最后一个分类是背包
  /// 数组元素：其他分类礼物+背包礼物
  final List<YBDGiftPackageRecord?>? giftPages;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<YBDGiftSheetBloc, YBDGiftPackageState>(
      bloc: context.read<YBDGiftSheetBloc>(),
      builder: (_, YBDGiftPackageState state) {
        return YBDGiftCategoryRow(
            giftPages: giftPages,
            viewingTabIndex: state.viewingTabIndex,
            selectTap: (index) {
              context.read<YBDGiftSheetBloc>().add(YBDGiftPackageState(
                    SheetAction.ChangeTab,
                    viewingTabIndex: index,
                  ));
            });
      },
    );
  }
  void build7mcKkoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
