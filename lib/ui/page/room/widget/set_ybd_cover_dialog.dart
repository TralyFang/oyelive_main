import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';

/// 开播页面设置海报的提示框
class YBDSetCoverDialog extends StatelessWidget {
  final String? content;
  final String? confirmBtn;
  final String? laterBtn;
  final VoidCallback? onConfirm;
  final VoidCallback? onLater;

  YBDSetCoverDialog({
    this.content,
    this.confirmBtn,
    this.laterBtn,
    this.onConfirm,
    this.onLater,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the set cover dialog bg");
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  // 避免点白色背景隐藏弹框
                },
                child: Container(
                  width: ScreenUtil().setWidth(494),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xff47CDCC),
                      width: ScreenUtil().setWidth(3),
                    ),
                    borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(32)))),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setWidth(54)),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(60)),
                        // 对话框标题
                        child: Center(
                          child: Text(
                            content ?? '',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: ScreenUtil().setSp(28),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(40)),
                      _bottomRow(),
                      SizedBox(height: ScreenUtil().setWidth(50)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildBpPSPoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部的按钮
  Widget _bottomRow() {
    if ((laterBtn ?? '').isNotEmpty) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          YBDScaleAnimateButton(
            onTap: () {
              logger.v('clicked later btn');
              if (null != onLater) {
                onLater!();
              }
            },
            child: _bottomBtn(laterBtn!, 0),
          ),
          YBDScaleAnimateButton(
            onTap: () {
              logger.v('clicked confirm btn');
              if (null != onConfirm) {
                onConfirm!();
              }
            },
            child: _bottomBtn(confirmBtn!, 1),
          ),
        ],
      );
    } else {
      return YBDScaleAnimateButton(
        onTap: () {
          logger.v('clicked ok btn');
          onConfirm!();
        },
        child: _bottomBtn(confirmBtn!, 1),
      );
    }
  }
  void _bottomRowNncQaoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部按钮
  Widget _bottomBtn(String title, int index) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(ScreenUtil().setWidth(60)),
        ),
        gradient: LinearGradient(
          colors: index == 0 ? [Color(0xffCCCCCC), Color(0xffCCCCCC)] : [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      width: ScreenUtil().setWidth(200),
      height: ScreenUtil().setWidth(64),
      alignment: Alignment.center,
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(28),
          color: Colors.white,
        ),
      ),
    );
  }
  void _bottomBtnltPbtoyelive(String title, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
