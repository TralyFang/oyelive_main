
/*
 * @Author: William
 * @Date: 2022-12-23 16:48:03
 * @LastEditTime: 2023-01-05 21:24:35
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/room/widget/fans_sheet.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/room_socket/entity/fans_ybd_info.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/level_ybd_tag.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';

class YBDFansSheet extends StatefulWidget {
  final List<YBDFansInfo>? dailyFans;
  final List<YBDFansInfo>? weeklyFans;
  final List<YBDFansInfo>? monthlyFans;

  const YBDFansSheet({Key? key, this.dailyFans, this.weeklyFans, this.monthlyFans}) : super(key: key);

  @override
  State<YBDFansSheet> createState() => _YBDFansSheetState();
}

class _YBDFansSheetState extends State<YBDFansSheet> with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }
  void initStateQhyIZoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 1000.px,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(32.px), topRight: Radius.circular(32.px)),
      ),
      child: Column(
        children: [
          _topTabView(),
          Expanded(
            // tab 内容页面
            child: TabBarView(
              controller: _tabController,
              children: [
                fansContent(widget.dailyFans),
                fansContent(widget.weeklyFans),
                fansContent(widget.monthlyFans),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void buildsUYNRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// tab 选项卡
  Widget _topTabView() {
    return Container(
      height: 70.px,
      padding: EdgeInsets.fromLTRB(64.px, 16.px, 64.px, 0),
      child: TabBar(
        indicator: UnderlineTabIndicator(
          borderSide: BorderSide(width: 4.px, color: Color(0xff7DFAFF)),
          insets: EdgeInsets.symmetric(horizontal: 80.px),
        ),
        labelColor: Colors.black,
        unselectedLabelColor: Colors.black.withOpacity(0.5),
        labelStyle: TextStyle(fontWeight: FontWeight.w400, height: 1, fontSize: 24.sp),
        unselectedLabelStyle: TextStyle(fontWeight: FontWeight.w400, height: 1, fontSize: 24.sp),
        controller: _tabController,
        // isScrollable: true,
        tabs: [Tab(text: 'Daily'), Tab(text: 'Weekly'), Tab(text: 'Monthly')],
      ),
    );
  }
  void _topTabView0sFDroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget fansContent(List<YBDFansInfo>? fans) {
    if (fans == null || fans.isEmpty) {
      return YBDPlaceHolderView(
        img: 'assets/images/empty/empty_my_profile.webp',
        text: translate('no_data'),
        textColor: Colors.black,
      );
    } else {
      return Container(
        padding: EdgeInsets.only(bottom: 16.px),
        child: ListView.builder(
          padding: EdgeInsets.symmetric(horizontal: 30.px),
          itemBuilder: (_, index) => Material(color: Colors.transparent, child: _fansItem(index, fans[index])),
          itemCount: fans.length,
        ),
      );
    }
  }
  void fansContentkCpDFoyelive(List<YBDFansInfo>? fans) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _fansItem(int index, YBDFansInfo data) {
    if (index == 0) {
      return Container(
        // height: 300.px,
        constraints: BoxConstraints(maxHeight: 344.px),
        margin: EdgeInsets.only(bottom: 5.px),
        // color: Colors.blue,
        child: Column(
          children: [
            SizedBox(height: 40.px),
            // 头像
            Stack(alignment: Alignment.center, children: [
              Padding(
                padding: EdgeInsets.all(8.px),
                child: Container(
                  width: 170.px,
                  height: 170.px,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(85.px),
                      gradient: LinearGradient(colors: [
                        Color(0xffF7DC6C),
                        Color(0xffFFF5D3),
                        Color(0xffD9BA6B),
                        Color(0xffFFE9BB),
                        Color(0xffFFB015)
                      ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
                  child: Center(
                    child: YBDRoundAvatar(
                      YBDImageUtil.avatar(context, data.img ?? '', data.senderId, scene: 'C'),
                      avatarWidth: 160,
                      userId: data.senderId,
                    ),
                  ),
                ),
              ),
              Transform.translate(
                  offset: Offset(60.px, -80.px),
                  child: Image.asset(
                    'assets/images/liveroom/live_icon_fans_crown.png',
                    width: 78.px,
                  )),
              // VIP
              Positioned(
                bottom: 0,
                right: 0,
                child: YBDVipIcon(
                  data.vipIcon,
                  size: 80,
                  padding: EdgeInsets.all(0),
                ),
              ),
            ]),
            SizedBox(height: 12.px),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  data.nickname!,
                  style: TextStyle(fontSize: 24.sp, color: Colors.black),
                ),
                SizedBox(width: 12.px),
                YBDLevelTag(data.level),
                SizedBox(width: 12.px),
                //
                Container(
                  // 性别标签
                  height: 23.px,
                  width: 23.px,
                  child: Image.asset(
                    data.sex == 2
                        ? 'assets/images/liveroom/icon_male@2x.webp'
                        : 'assets/images/liveroom/icon_female@2x.webp',
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20.px,
            ),
            // 钻石
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/images/icon_gems.webp', width: 26.px, height: 26.px),
                SizedBox(width: 6.px),
                Text(YBDNumericUtil.compactNumberWithFixedDigits(data.score.toString(), rounding: false)!,
                    style: TextStyle(color: Colors.black, fontSize: 24.sp)),
              ],
            ),
          ],
        ),
      );
    }
    return Container(
      height: 130.px,
      // color: Colors.blue.withOpacity(0.5),
      child: Row(children: [
        Text(
          (index + 1).toString(),
          style: TextStyle(fontSize: 24.sp, color: Colors.black.withOpacity(0.7)),
        ),
        SizedBox(width: 30.px),
        YBDRoundAvatar(data.img, scene: 'B', avatarWidth: 80, userId: data.senderId),
        SizedBox(width: 20.px),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // SizedBox(height: 20.px),
            Row(
              children: [
                Container(
                  constraints: BoxConstraints(maxWidth: 300.px),
                  // color: Colors.blue.withOpacity(0.5),
                  child: Text(
                    data.nickname!,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 24.sp, color: Colors.black),
                  ),
                ),
                SizedBox(width: 10.px),
                Container(
                  // 性别标签
                  height: 23.px,
                  width: 23.px,
                  child: Image.asset(
                    data.sex == 2
                        ? 'assets/images/liveroom/icon_male@2x.webp'
                        : 'assets/images/liveroom/icon_female@2x.webp',
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
            SizedBox(height: data.vipIcon == null ? 12.px : 0.px),
            Row(
              children: [
                // VIP
                YBDVipIcon(
                  data.vipIcon,
                  padding: EdgeInsets.only(right: 2.px),
                ),
                YBDLevelTag(data.level),
              ],
            )
          ],
        ),
        Spacer(),
        Row(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/images/icon_gems.webp', width: 26.px, height: 26.px),
            SizedBox(width: 6.px),
            Text(YBDNumericUtil.compactNumberWithFixedDigits(data.score.toString(), rounding: false)!,
                style: TextStyle(color: Colors.black, fontSize: 24.sp)),
          ],
        ),
      ]),
    );
  }
  void _fansItemeGkReoyelive(int index, YBDFansInfo data) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
