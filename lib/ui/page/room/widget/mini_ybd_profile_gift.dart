import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';

/// mini profile 送礼按钮
class YBDMiniProfileGift extends StatelessWidget {
  /// 点击送礼按钮的回调
  final VoidCallback clickedCallback;

  bool isEnemyClick;
  YBDMiniProfileGift(this.clickedCallback, this.isEnemyClick);

  @override
  Widget build(BuildContext context) {
    return YBDScaleAnimateButton(
      onTap: () {
        logger.v('clicked mini profile send gift btn');
        if (null != clickedCallback) {
          clickedCallback();
        }
      },
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color(0xff47CDCC), Color(0xff5E94E7)], begin: Alignment.topCenter, end: Alignment.bottomCenter),
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              blurRadius: 1,
              offset: Offset(0, ScreenUtil().setWidth(2)),
            ),
          ],
        ),
        width: ScreenUtil().setWidth(220),
        height: ScreenUtil().setWidth(64),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              isEnemyClick ? 'assets/images/mini/mini_chat.webp' : 'assets/images/mini/mini_gift.png',
              width: ScreenUtil().setWidth(28),
              fit: BoxFit.cover,
              color: isEnemyClick ? Colors.white : null,
            ),
            SizedBox(width: ScreenUtil().setWidth(5)),
            Text(
              // TODO: 翻译
              isEnemyClick ? 'Say Hi' : 'Send YBDGift',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(28),
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
  void buildTUS0xoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
