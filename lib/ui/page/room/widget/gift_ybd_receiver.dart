import 'dart:async';


import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../widget/round_ybd_avatar.dart';
import '../../home/widget/bubble_ybd_widget.dart';
import '../bloc/gift_ybd_sheet_bloc.dart';

class YBDGiftReceiver extends StatefulWidget {
  YBDReceiverUser receiverUser;

  YBDGiftReceiver(this.receiverUser);

  @override
  YBDGiftReceiverState createState() => new YBDGiftReceiverState();
}

class YBDGiftReceiverState extends BaseState<YBDGiftReceiver> {
  GlobalKey _key = new GlobalKey();

  /// 昵称tips
  static OverlayEntry? _overlay;

  /// 删除昵称tips的定时器
  static Timer? _overlayTimer;

  /// 移除昵称tips
  void _removeOverlay() {
    if (null != _overlay) {
      _overlay!.remove();
      _overlay = null;
      return;
    }

  }
  void _removeOverlay5fjgXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 移除定时器
  void _removeTimer() {
    if (null != _overlayTimer) {
      _overlayTimer!.cancel();
      _overlayTimer = null;
      return;
    }

  }
  void _removeTimerKFFBJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 选中时显示tips
  /// [receiverX] 接收者头像的x坐标
  /// [receiverY] 接收者头像的y坐标
  /// [name] 接收者昵称
  void _showName(receiverX, receiverY, String? name) {
    _removeTimer();
    _removeOverlay();

    _overlayTimer = Timer(Duration(seconds: 2), () {
      _removeOverlay();
    });

    _overlay = OverlayEntry(builder: (_) {
      return Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(
              receiverX - (ScreenUtil().setWidth(68)),
              receiverY - ScreenUtil().setWidth(52),
            ),
            child: Material(
              borderRadius: BorderRadius.all(
                Radius.circular(ScreenUtil().setWidth(4)),
              ),
              color: Colors.transparent,
              child: YBDBubbleWidget(
                ScreenUtil().setWidth(120),
                ScreenUtil().setWidth(52),
                Color(0xFFFFFFFF),
                BubbleArrowDirection.bottom,
                innerPadding: 0.0,
                arrHeight: ScreenUtil().setWidth(8),
                length: ScreenUtil().setWidth(10),
                child: Text(
                  name ?? '',
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: ScreenUtil().setSp(20),
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    });

    Overlay.of(context)!.insert(_overlay!);
  }
  void _showNameQNYXMoyelive(receiverX, receiverY, String? name) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return BlocBuilder<YBDGiftSheetBloc, YBDGiftPackageState>(
        bloc: context.read<YBDGiftSheetBloc>(),
        builder: (_, YBDGiftPackageState state) {
          return GestureDetector(
            onTap: () {
              context
                  .read<YBDGiftSheetBloc>()
                  .add(YBDGiftPackageState(SheetAction.SelectReceiver, receiverUser: widget.receiverUser));
              RenderBox _cardBox = _key.currentContext!.findRenderObject() as RenderBox;
              final position = _cardBox.localToGlobal(Offset.zero);
              _showName(position.dx, position.dy, widget.receiverUser.name);
            },
            child: Container(
              width: ScreenUtil().setWidth(60),
              child: Stack(
                key: _key,
                alignment: Alignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                        width:
                            ScreenUtil().setWidth(state.receiverUser?.userId == widget.receiverUser.userId ? 60 : 58),
                        padding: EdgeInsets.all(
                            ScreenUtil().setWidth(state.receiverUser?.userId == widget.receiverUser.userId ? 2 : 1)),
                        decoration: BoxDecoration(
                            color: state.receiverUser?.userId == widget.receiverUser.userId
                                ? Color(0xff5E94E7)
                                : Color(0xffcccccc),
                            shape: BoxShape.circle),
                        alignment: Alignment.center,
                        child: YBDRoundAvatar(
                          widget.receiverUser.img,
                          avatarWidth: 56,
                          needNavigation: false,
                          userId: widget.receiverUser.userId,
                        )),
                  ),
                  Column(
                    children: <Widget>[
                      SizedBox(
                        height: ScreenUtil().setWidth(50),
                      ),
                      Container(
                        width: ScreenUtil().setWidth(40),
                        height: ScreenUtil().setWidth(16),
                        alignment: Alignment.center,
                        decoration: state.receiverUser?.userId == widget.receiverUser.userId
                            ? BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    Color(0xff47CDCC),
                                    Color(0xff5E94E7),
                                  ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(4))))
                            : BoxDecoration(
                                color: Color(0xffCDCDCD),
                                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(4)))),
                        child: Text(
                          // 显示麦位下标，房主显示Owner
                          widget.receiverUser.position ?? '',
                          style: TextStyle(fontSize: ScreenUtil().setSp(12), color: Colors.white),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }
  void myBuild8s0Dwoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    _removeTimer();
    _removeOverlay();
  }
}

class YBDReceiverUser {
  int? userId;
  String? img;
  String? name;
  String? position;

  YBDReceiverUser({this.userId, this.img, this.name, this.position});
}
