import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

/// 房间升级提示框
class YBDShowRoomLevelUpPage extends StatelessWidget {
  final int? roomLevel;

  /// 点确认按钮的回调
  final VoidCallback okCallback;

  final bool isRoomLevel;

  YBDShowRoomLevelUpPage(this.roomLevel, {required this.okCallback, this.isRoomLevel = false});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: ScreenUtil().setWidth(540),
        height: ScreenUtil().screenHeight,
        child: Column(children: <Widget>[
          Expanded(child: SizedBox(width: 1)),
          Center(
            child: Stack(children: [
              Container(
                width: ScreenUtil().setWidth(540),
                height: ScreenUtil().setWidth(416),
                child: Image.asset(
                  'assets/images/liveroom/room_levle_up.webp',
                  fit: BoxFit.contain,
                ),
              ),
              Container(
                width: ScreenUtil().setWidth(540),
                height: ScreenUtil().setWidth(416),
                alignment: Alignment.center,
                padding: EdgeInsets.only(
                  bottom: ScreenUtil().setWidth(30),
                  right: ScreenUtil().setWidth(15),
                ),
                child: Text(
                  '$roomLevel',
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(60),
                    color: Color(0xffC4954A),
                    fontWeight: FontWeight.w400,
                    decoration: TextDecoration.none,
                  ),
                ),
              ),
            ]),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(10),
          ),
          Text(
            'Leveled Up to ${isRoomLevel ? 'RLv.' : 'Lv.'}$roomLevel!',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(33),
              color: Color(0xffFFE5A7),
              fontWeight: FontWeight.w400,
              decoration: TextDecoration.none,
            ),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(80),
          ),
          GestureDetector(
            onTap: () async {
              // 关闭弹框
              okCallback();
            },
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(310),
              height: ScreenUtil().setWidth(72),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                gradient: LinearGradient(
                  colors: [
                    Color(0xFFD0B161),
                    Color(0xFFF2E082),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
              child: Text(
                translate('ok'),
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(32),
                  color: Colors.white,
                  fontWeight: FontWeight.w400,
                  decoration: TextDecoration.none,
                ),
              ),
            ),
          ),
          SizedBox(height: ScreenUtil().setWidth(200)),
          Expanded(child: SizedBox(width: 1)),
        ]));
  }
  void buildHHKt9oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
