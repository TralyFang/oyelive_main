import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/module/status/entity/agent_ybd_share_entity.dart';
import 'package:oyelive_main/module/status/status_ybd_api_helper.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/ui/page/room/widget/dialog_ybd_agent_award.dart';

class YBDRoomAgentShare extends StatefulWidget {
  YBDRoomInfo? room;

  YBDRoomAgentShare(this.room);

  @override
  YBDRoomAgentShareState createState() => new YBDRoomAgentShareState();
}

class YBDRoomAgentShareState extends BaseState<YBDRoomAgentShare> {
  YBDAgentShareEntity? agentShareEntity;
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild

    Store<YBDAppState> state = YBDCommonUtil.storeFromContext(context: context)!;
    bool isStreamer = state.state.bean!.id == widget.room?.id;
    print("isStreamer$isStreamer ${agentShareEntity?.data}");
    return agentShareEntity?.data != null && isStreamer
        ? GestureDetector(
            onTap: () {
              showDialog<Null>(
                  context: context, //BuildContext对象
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return YBDAgentAwardDialog(widget.room);
                  });
            },
            child: Image.asset(
              "assets/images/agent/agent_share.webp",
              width: ScreenUtil().setWidth(48),
            ),
          )
        : SizedBox();
  }
  void myBuildnqxUqoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getData() async {
    YBDAgentShareEntity? result = await YBDStatusApiHelper.getAgentShare(context);
    if (result?.code == Const.HTTP_SUCCESS_NEW && result?.data != null) {
      agentShareEntity = result;
      setState(() {});
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 2), () {
      if (mounted) getData();
    });
  }
  void initStatebCPpeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDRoomAgentShare oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetFpixboyelive(YBDRoomAgentShare oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
