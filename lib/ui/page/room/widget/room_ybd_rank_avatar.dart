import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import '../../../../common/room_socket/entity/fans_ybd_info.dart';
import '../../../../common/util/image_ybd_util.dart';

/// 房间顶部排名前三的头像
class YBDRoomRankAvatar extends StatelessWidget {
  /// 用户信息
  final YBDFansInfo userInfo;

  /// 名次
  final int rank;

  YBDRoomRankAvatar(this.userInfo, this.rank);

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.green,
      width: ScreenUtil().setWidth(60),
      height: ScreenUtil().setWidth(70),
      margin: EdgeInsets.only(left: 3.px),
      child: Stack(
        children: <Widget>[
          Positioned(
            top: ScreenUtil().setWidth(10),
            left: ScreenUtil().setWidth(6),
            child: Container(
              // 头像
              width: ScreenUtil().setWidth(50),
              height: ScreenUtil().setWidth(50),
              // decoration: BoxDecoration(
              //   color: _rankColor(),
              //   borderRadius: BorderRadius.all(Radius.circular(40)),
              // ),
              padding: EdgeInsets.all(ScreenUtil().setWidth(1)),
              child: ClipOval(
                // 用户头像
                child: YBDNetworkImage(
                  imageUrl: YBDImageUtil.avatar(
                        context,
                        userInfo.img,
                        userInfo.senderId,
                        scene: 'B',
                      ),
                  fit: BoxFit.cover,
                  placeholder: (context, url) {
                    return YBDResourcePathUtil.defaultMaleImage(male: false);
                  },
                  width: ScreenUtil().setWidth(50),
                  height: ScreenUtil().setWidth(50),
                ),
              ),
            ),
          ),
          Positioned(
            top: 3.px,
            right: 1.px,
            child: Container(
              // 帽子
              child: Image.asset(
                _rankCap(),
                width: ScreenUtil().setWidth(20),
                height: ScreenUtil().setWidth(20),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void buildKoa6Ooyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _rankColor() {
    if (rank == 1) {
      return Color(0xffFFE166);
    } else if (rank == 2) {
      return Color(0xffD7D7D7);
    } else {
      return Color(0xffE3AE7F);
    }
  }

  _rankIcon() {
    if (rank == 1) {
      return 'assets/images/liveroom/room_rank_first_icon.webp';
    } else if (rank == 2) {
      return 'assets/images/liveroom/room_rank_second_icon.webp';
    } else {
      return 'assets/images/liveroom/room_rank_third_icon.webp';
    }
  }

  _rankCap() {
    if (rank == 1) {
      return 'assets/images/liveroom/icon_crown_1@2x.webp';
    } else if (rank == 2) {
      return 'assets/images/liveroom/icon_crown_2@2x.webp';
    } else {
      return 'assets/images/liveroom/icon_crown_3@2x.webp';
    }
  }
}
