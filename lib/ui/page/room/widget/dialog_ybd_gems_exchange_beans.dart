import 'dart:async';


/*
 * @Author: William-Zhou
 * @Date: 2021-11-10 17:39:25
 * @LastEditTime: 2022-09-29 11:13:19
 * @LastEditors: William
 * @Description: 
 */
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/result_ybd_bean.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';
import 'package:redux/redux.dart';

class YBDGemsExchangeBeansDialog extends StatefulWidget {
  const YBDGemsExchangeBeansDialog({Key? key}) : super(key: key);

  @override
  _YBDGemsExchangeBeansDialogState createState() => _YBDGemsExchangeBeansDialogState();
}

class _YBDGemsExchangeBeansDialogState extends BaseState<YBDGemsExchangeBeansDialog> {
  TextEditingController _controller = TextEditingController();
  FocusNode _focusNode = FocusNode();
  List<String?>? _percents = [];
  List<bool> _alls = [];
  List<bool> _selects = [];
  int? _remainingGems = 0;
  String? _showRemainingGems = '';
  double _curSelectPercent = 0.5;
  String _curBeans = '';
  bool _canExchange = false;
  Map<String, dynamic>? _tpG2BConfig;
  int? _defaultSelect = 1;
  int? _selectChangeGems = 0;
  YBDUserInfo? _userInfo;

  @override
  void initState() {
    _tpG2BConfig = YBDCommonUtil.getTpG2BConfig();
    int? defaultIndex;
    int perLen;
    int allIndex;
    if (_tpG2BConfig!.isNotEmpty) {
      _percents = _tpG2BConfig!['percents'].split('|');
      perLen = _percents!.length;
      defaultIndex = _percents!.indexOf(_tpG2BConfig!['default']);
      _defaultSelect = defaultIndex;
      allIndex = _percents!.indexOf('1.0');
      _selects.length = _alls.length = perLen;
      _selects.fillRange(0, perLen, false);
      _alls.fillRange(0, perLen, false);
      if (allIndex != -1) _alls[allIndex] = true;
    }
    _userInfo = YBDUserUtil.getLoggedUser(context);
    _remainingGems = _userInfo!.gems;
    _showRemainingGems = YBDNumericUtil.format(_remainingGems);
    if (_tpG2BConfig!['default'] != '-1' && defaultIndex != -1) _selectByIndex(_defaultSelect!);
    _controller.addListener(() {
      if (_controller.text != null && _controller.text.isNotEmpty) {
        if (_controller.text.startsWith('0')) {
          _controller.text = '';
          setState(() {});
        }
        if (int.parse(_controller.text) >= _remainingGems!) {
          _focusNode.unfocus();
          _selects.last = true;
          _curSelectPercent = double.parse(_percents!.last!);
          _selectChangeGems = YBDNumericUtil.getGemsPercentage(_remainingGems, 1.0);
          _controller.text = _selectChangeGems.toString();
          _curBeans = YBDNumericUtil.gemsToBeans(_controller.text);
          _canExchange = true;
          setState(() {});
        }
      } else {
        logger.v('11.17-------_controller.text:null');
      }
    });
    super.initState();
  }
  void initStateaE950oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();
    super.dispose();
  }
  void disposeoEbrtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _focusNode.unfocus();
        if (_controller.text.isEmpty && _defaultSelect != -1) _selectByIndex(_defaultSelect!);
      },
      child: Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Center(
          child: Material(
              type: MaterialType.transparency,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
                  color: Colors.white,
                ),
                padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
                width: ScreenUtil().setWidth(600),
                height: ScreenUtil().setWidth(713),
                child: Column(
                  children: [
                    YBDTPGlobal.hSizedBox(1),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            'assets/images/dc/daily_check_close_btn.png',
                            width: ScreenUtil().setWidth(58),
                          ),
                        )
                      ],
                    ),
                    Transform.translate(
                      offset: Offset(0, -ScreenUtil().setWidth(30)),
                      child: Text(
                        translate('enjoy_teen_patti'),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: ScreenUtil().setSp(30),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    YBDTPGlobal.hSizedBox(13),
                    Row(
                      children: [
                        YBDTPGlobal.wSizedBox(33),
                        Text('${translate('gems')}:$_showRemainingGems', style: _fontSize24Style()),
                      ],
                    ),
                    YBDTPGlobal.hSizedBox(10),
                    _gemsInputBox(),
                    YBDTPGlobal.hSizedBox(10),
                    Container(
                        width: ScreenUtil().setWidth(500),
                        child: Text(translate('enter_gems_hint'), style: _fontSize22Style(color: 0xff22A5BF))),
                    YBDTPGlobal.hSizedBox(40),
                    _selectPercentGems(),
                    YBDTPGlobal.hSizedBox(40),
                    _beansInputBox(),
                    YBDTPGlobal.hSizedBox(62),
                    _exchangeBtn(),
                    YBDTPGlobal.hSizedBox(20),
                    Text(translate('enjoy_tp_hint'), style: _fontSize22Style()),
                  ],
                ),
              )),
        ),
      ),
    );
  }
  void myBuildsK3Fqoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 字号为24的样式
  TextStyle _fontSize24Style({int color = 0xff595959}) {
    return TextStyle(color: Color(color), fontSize: ScreenUtil().setSp(24));
  }

  /// 字号为22的样式
  TextStyle _fontSize22Style({int color = 0xff595959}) {
    return TextStyle(color: Color(color), fontSize: ScreenUtil().setSp(22));
  }

  /// 两个文本框的样式
  Widget _myTextBox({Widget? child}) {
    return Container(
        width: ScreenUtil().setWidth(520),
        height: ScreenUtil().setWidth(80),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
          color: Color(0xffEDEDED),
        ),
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(12)),
        child: child);
  }
  void _myTextBoxyQzM2oyelive({Widget? child}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// gems输入框
  Widget _gemsInputBox() {
    return _myTextBox(
        child: Row(
      children: [
        Image.asset(
          'assets/images/icon_gems.webp',
          width: ScreenUtil().setWidth(36),
        ),
        YBDTPGlobal.wSizedBox(17),
        Expanded(
          child: TextField(
            controller: _controller,
            style: _inputTextStyle(),
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            keyboardType: TextInputType.number,
            focusNode: _focusNode,
            decoration: InputDecoration(border: InputBorder.none),
            onTap: () {
              _canExchange = false;
              _unSelectAll();
            },
            onChanged: (value) {
              if (value.isEmpty) {
                _curBeans = '';
                _canExchange = false;
                setState(() {});
              } else if (int.parse(value) < _remainingGems!) {
                _curBeans = YBDNumericUtil.gemsToBeans(_controller.text);
                _canExchange = _curBeans.isNotEmpty;
                setState(() {});
              } else {
                logger.v('11.17-----value:$value');
              }
            },
          ),
        ),
      ],
    ));
  }
  void _gemsInputBoxx7LHNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 输入框字体
  TextStyle _inputTextStyle() {
    return TextStyle(
      color: Color(0xff595959),
      fontSize: ScreenUtil().setSp(28),
      fontWeight: FontWeight.w500,
    );
  }

  /// gems选择百分比
  Widget _selectPercentGems() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: List<Widget>.generate(
          _percents!.length,
          (index) => _selectBox(
                index: index,
                percent: double.parse(_percents![index]!),
                all: _alls[index],
                select: _selects[index],
              )),
    );
  }
  void _selectPercentGemsXcOHMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// gems单选选择框
  Widget _selectBox({int? index, double? percent, bool all = false, bool select = false}) {
    return YBDScaleAnimateButton(
      onTap: () {
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.ROOM_PAGE,
          itemName: YBDItemName.TP_G2B_PERCENT,
          value: percent.toString(),
        ));
        _selectByIndex(index!);
      },
      child: Container(
        width: ScreenUtil().setWidth(90),
        height: ScreenUtil().setWidth(50),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
          border: Border.all(
            color: select ? Colors.transparent : Color(0xffDDDDDD),
            width: ScreenUtil().setWidth(1),
          ),
          gradient: select ? YBDTPGlobal.blueGradient : null,
        ),
        alignment: Alignment.center,
        child: Text(
          all ? translate('all') : '${(percent! * 100).toInt()}%',
          style: _selectBoxTextStyle(select: select),
        ),
      ),
    );
  }
  void _selectBox3ncOuoyelive({int? index, double? percent, bool all = false, bool select = false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// gems单选字体样式
  TextStyle _selectBoxTextStyle({bool select = false}) {
    return TextStyle(
      color: select ? Color(0xffFFFFFF) : Color(0xff595959),
      fontSize: ScreenUtil().setSp(22),
      fontWeight: FontWeight.w500,
    );
  }

  /// 取消gems百分比选择
  void _unSelectAll() {
    _selects.fillRange(0, _percents!.length, false);
    _controller.text = '';
    _curBeans = '';
    setState(() {});
  }

  /// gems百分比单选
  void _selectByIndex(int index) {
    _focusNode.unfocus();
    _unSelectAll();
    _selects[index] = true;
    _curSelectPercent = double.parse(_percents![index]!);
    _selectChangeGems = YBDNumericUtil.getGemsPercentage(_remainingGems, _curSelectPercent);
    _controller.text = _selectChangeGems.toString();
    _curBeans = YBDNumericUtil.gemsToBeans(_controller.text);
    _canExchange = true;
    setState(() {});
  }
  void _selectByIndexX7ksAoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// beans显示框
  Widget _beansInputBox() {
    return _myTextBox(
      child: Row(
        children: [
          Image.asset(
            'assets/images/topup/y_top_up_beans@2x.webp',
            width: ScreenUtil().setWidth(36),
          ),
          YBDTPGlobal.wSizedBox(17),
          Text(_curBeans, style: _inputTextStyle()),
        ],
      ),
    );
  }

  /// 兑换按钮
  Widget _exchangeBtn() {
    return YBDScaleAnimateButton(
      onTap: _canExchange ? _exchange : null,
      child: Container(
          width: ScreenUtil().setWidth(400),
          height: ScreenUtil().setWidth(65),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(33))),
            gradient: _canExchange ? YBDTPGlobal.blueGradient : null,
            color: _canExchange ? null : Colors.grey,
          ),
          alignment: Alignment.center,
          child: Text(
            translate('exchange'),
            style: TextStyle(
              color: Color(0xffFFFFFF),
              fontSize: ScreenUtil().setSp(28),
            ),
          )),
    );
  }
  void _exchangeBtnHDvAOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<void> _exchange() async {
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: YBDLocationName.ROOM_PAGE,
      itemName: YBDItemName.TP_G2B_EXCHANGE,
    ));
    if (_controller.text.length > 0) {
      _focusNode.unfocus();
      logger.v('11.12-----_curInputGems:${_controller.text}--_curBeans:$_curBeans');
      showLockDialog();
      YBDResultBeanEntity? result =
          await ApiHelper.gemsExchangeBeans(context, int.parse(_controller.text), Const.CURRENCY_GEM);
      dismissLockDialog();
      if (result != null) {
        if (result.record != null && result.record['money'] != null && result.record['gems'] != null) {
          YBDToastUtil.toast('$_curBeans ${translate('tp_g2b_success')}');
          _userInfo!.money = result.record['money'];
          _userInfo!.gems = result.record['gems'];
          YBDSPUtil.save(Const.SP_USER_INFO, _userInfo);
          Store<YBDAppState> store = YBDCommonUtil.storeFromContext()!;
          store.dispatch(YBDUpdateUserAction(_userInfo));
          YBDModuleCenter.instance().updateTPMoney();
          Navigator.pop(context);
        } else {
          // 2.7.1 兑换失败关闭该弹窗
          YBDToastUtil.toast(result.returnMsg ?? translate('unknown_error'));
          Navigator.pop(context);
        }
      } else {
        YBDToastUtil.toast(translate('no_network'));
      }
    } else {
      YBDToastUtil.toast(translate('enter_gems_hint'));
      _canExchange = false;
      _curBeans = '';
      setState(() {});
    }
  }
  void _exchange45Cproyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
