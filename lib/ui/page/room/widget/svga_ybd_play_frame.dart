import 'dart:async';


import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import '../../../../common/util/log_ybd_util.dart';

///播放svga页面---Frame
class YBDSvgaPlayerView extends StatefulWidget {
  /// 动画类型  默认svga
  final int? type;

  /// 动画文件下载到本地的地址
  final String? url;

  final double height;
  final double width;

  final bool? repeat;

  /// 播放状态
  final callBack;

  YBDSvgaPlayerView({
    this.type,
    this.url,
    this.width = 100,
    this.height = 100,
    this.callBack,
    this.repeat,
  });

  @override
  _YBDSvgaPlayerPageState createState() => _YBDSvgaPlayerPageState();
}

class _YBDSvgaPlayerPageState extends State<YBDSvgaPlayerView> with SingleTickerProviderStateMixin {
  /// svga 动画控制器
  SVGAAnimationController? _animationController;

  @override
  void initState() {
    super.initState();
//    logger.v('YBDSvgaPlayerView svga widget.url: ${widget.url}');
    _animationController = SVGAAnimationController(vsync: this);
    _playSvga();
  }
  void initStatewNLIboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    if (_animationController != null) {
      _animationController!.stop();
      _animationController!.clear();
      _animationController = null;
    }
    super.dispose();
  }
  void dispose6sBi6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the combo dialog bg");
        /*  Navigator.pop(context);
        widget.callBack(false);*/
      },
      child: Container(
        color: Colors.transparent,
        width: widget.width,
        height: widget.height,
        child: _contentView(context),
      ),
    );
//    );
  }
  void buildPp8vXoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///SVGAImage view
  Widget _contentView(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      child: SVGAImage(
        _animationController!,
        fit: BoxFit.cover,
      ),
//      child: _isLoading ? YBDLoadingCircle() : SVGAImage(_animationController),
    );
  }
  void _contentViewMHjdLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDSvgaPlayerView oldWidget) {
    super.didUpdateWidget(oldWidget);
    // logger.v('oldWidget.url: ${oldWidget.url}, ${widget.url}');
    if (oldWidget.url != widget.url) {
      _playSvga();
    }

  }

  _playSvga() async {
//    widget.callBack(true);
    await _loadLoaclSvga(widget.url);
  }

  /// 加载本地 svga 文件
  _loadLoaclSvga(String? path) async {
    logger.v('YBDSvgaPlayerView svga path: $path');
    if (!mounted) return;
    try {
      File svgFile = File(path!);

      if (await svgFile.exists()) {
        Uint8List bytes = svgFile.readAsBytesSync();
        logger.v('YBDSvgaPlayerView bytes: ${bytes.length}');
        // 播放动画
        SVGAParser.shared.decodeFromBuffer(bytes).then((videoItem) {
          setState(() {});
          _animationController!.videoItem = videoItem;
          if (widget.repeat!) {
            _animationController!.repeat().whenComplete(() {
              logger.v('svga play Complete : $svgFile');
            });
          } else {
            _animationController!.forward().whenComplete(() {
              logger.v('svga play Complete : $svgFile');
            });
          }
        });
      } else {
        logger.v('svga file not exists : $svgFile');
      }
    } catch (e) {}
  }
}
