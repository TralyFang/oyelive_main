import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/room_socket/message/common/Global.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../widget/round_ybd_avatar.dart';

typedef GiftCallback = void Function(int? roomId);

class YBDGlobalGiftBanners extends StatefulWidget {
  final YBDGlobal _global;

  final GiftCallback successCallback;

  YBDGlobalGiftBanners(this._global, this.successCallback);

  @override
  YBDGlobalGiftBannersState createState() => new YBDGlobalGiftBannersState();
}

class YBDGlobalGiftBannersState extends BaseState<YBDGlobalGiftBanners> {
  // YBDGift gift;
  @override
  Widget myBuild(BuildContext context) {
    logger.v(
        'YBDGlobalGiftBanners fromUser: ${widget._global.content!.fromUser} senderImg:,${widget._global.content!.receiverImg},imgUrl:${widget._global.content!.imgUrl}');
    return GestureDetector(
      onTap: () {
        logger
            .d('YBDGlobalGiftBanners GlobalGift enter room :${widget._global.roomId} ..${widget._global.content!.roomId}');
        widget.successCallback(widget._global.content!.roomId);
        // YBDRoomHelper.enterRoom(context, widget._global.roomId, location: 'user_profile_page');
      },
      child: Container(
        color: Colors.transparent,
        width: ScreenUtil().setWidth(426),
        height: ScreenUtil().setWidth(88),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: ScreenUtil().setWidth(1),
            ),
            /*   YBDNetworkImage(
            imageUrl: YBDImageUtil.avatar(context, gift?.senderImg, gift?.fromUser, scene: 'B'),
            width: ScreenUtil().setWidth(70),
            height: ScreenUtil().setWidth(70),
          ),*/
            YBDRoundAvatar(
              // 用户头像
              widget._global.content?.senderImg,
              userId: widget._global.content?.fromUser,
              scene: "B",
              avatarWidth: ScreenUtil().setWidth(140),
              needNavigation: false,
            ),
            SizedBox(
              width: ScreenUtil().setWidth(2),
            ),
            Container(
              // 昵称
              alignment: Alignment.topLeft,
              width: ScreenUtil().setWidth(70),
              height: ScreenUtil().setWidth(60),
              child: Text(
                widget._global.content!.sender!,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: ScreenUtil().setSp(16), color: Colors.white),
              ),
            ),
            // SizedBox(
            //   width: ScreenUtil().setWidth(5),
            // ),
            YBDNetworkImage(
              imageUrl: YBDImageUtil.gift(context, widget._global.content?.imgUrl, 'B'),
              width: ScreenUtil().setWidth(70),
              height: ScreenUtil().setWidth(70),
            ),
            SizedBox(
              width: ScreenUtil().setWidth(2),
            ),
            Text(
              'X ${widget._global.content!.num}',
              style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white),
            ),
            SizedBox(
              width: ScreenUtil().setWidth(2),
            ),
            Container(
              // 昵称
              alignment: Alignment.bottomRight,
              width: ScreenUtil().setWidth(70),
              height: ScreenUtil().setWidth(60),
              child: Text(
                widget._global.content!.receiver!,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: ScreenUtil().setSp(16), color: Colors.white),
              ),
            ),
            SizedBox(
              width: ScreenUtil().setWidth(2),
            ),
            YBDRoundAvatar(
              // 用户头像
              widget._global.content?.receiverImg,
              userId: widget._global.content?.toUser,
              scene: "B",
              avatarWidth: ScreenUtil().setWidth(140),
              needNavigation: false,
            ),
            SizedBox(
              width: ScreenUtil().setWidth(5),
            ),
            /*  YBDNetworkImage(
            imageUrl: YBDImageUtil.avatar(context, gift?.receiverImg, gift?.toUser, scene: 'B'),
            width: ScreenUtil().setWidth(70),
            height: ScreenUtil().setWidth(70),
          ),*/
          ],
        ),
      ),
    );
  }
  void myBuilduC0UCoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatexw751oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesVBEDFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
