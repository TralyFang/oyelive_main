import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/pk_ybd_setting_entity.dart';
import 'package:oyelive_main/module/entity/result_ybd_bean.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

class YBDPkSettingDialog extends StatefulWidget {
  @override
  YBDPkSettingDialogState createState() => new YBDPkSettingDialogState();
}

class YBDPkSettingDialogState extends BaseState<YBDPkSettingDialog> {
  YBDPkSettingEntity? pkSettingEntity;
  bool noFriendInvite = false, noSystemInvite = false;
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Material(
        color: Colors.black.withOpacity(0.3),
        child: Center(
            child: Container(
          width: ScreenUtil().setWidth(500),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
            color: Colors.white,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: ScreenUtil().setWidth(10),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: ScreenUtil().setWidth(58),
                  ),
                  Spacer(),
                  Text(
                    "Set up",
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(28),
                        color: Colors.black.withOpacity(0.85),
                        fontWeight: FontWeight.w500),
                  ),
                  Spacer(),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Image.asset(
                          "assets/images/dc/daily_check_close_btn.png",
                          width: ScreenUtil().setWidth(48),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(10),
                  )
                ],
              ),
              SizedBox(
                height: ScreenUtil().setWidth(40),
              ),
              YBDDelayGestureDetector(
                onTap: () async {
                  if (pkSettingEntity != null) {
                    bool success =
                        await setSetting(!pkSettingEntity!.record!.rejectFriends!, pkSettingEntity!.record!.rejectSystem);
                    if (success) pkSettingEntity!.record!.rejectFriends = !pkSettingEntity!.record!.rejectFriends!;
                    setState(() {});
                  }
                },
                child: Container(
                  width: ScreenUtil().setWidth(400),
                  decoration: BoxDecoration(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        width: ScreenUtil().setWidth(24),
                        height: ScreenUtil().setWidth(24),
                        margin: EdgeInsets.only(bottom: ScreenUtil().setWidth(30), right: ScreenUtil().setWidth(14)),
                        decoration: BoxDecoration(
                            border: Border.all(color: Color(0xffC6C6C6), width: ScreenUtil().setWidth(1)),
                            shape: BoxShape.circle),
                        padding: EdgeInsets.all(ScreenUtil().setWidth(4)),
                        child: Container(
                          width: ScreenUtil().setWidth(24),
                          height: ScreenUtil().setWidth(24),
                          decoration: BoxDecoration(
                              color: pkSettingEntity != null && pkSettingEntity!.record!.rejectFriends!
                                  ? Color(0xff3EC1D3)
                                  : Colors.white,
                              shape: BoxShape.circle),
                        ),
                      ),
                      Text(
                        translate("no_friend_invite"),
                        style: TextStyle(color: Colors.black.withOpacity(0.85), fontSize: ScreenUtil().setSp(28)),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(14),
              ),
              YBDDelayGestureDetector(
                onTap: () async {
                  if (pkSettingEntity != null) {
                    bool success =
                        await setSetting(pkSettingEntity!.record!.rejectFriends, !pkSettingEntity!.record!.rejectSystem!);
                    if (success) pkSettingEntity!.record!.rejectSystem = !pkSettingEntity!.record!.rejectSystem!;
                    setState(() {});
                  }
                },
                child: Container(
                  width: ScreenUtil().setWidth(400),
                  decoration: BoxDecoration(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        width: ScreenUtil().setWidth(24),
                        height: ScreenUtil().setWidth(24),
                        margin: EdgeInsets.only(bottom: ScreenUtil().setWidth(30), right: ScreenUtil().setWidth(14)),
                        decoration: BoxDecoration(
                            border: Border.all(color: Color(0xffC6C6C6), width: ScreenUtil().setWidth(1)),
                            shape: BoxShape.circle),
                        padding: EdgeInsets.all(ScreenUtil().setWidth(4)),
                        child: Container(
                          width: ScreenUtil().setWidth(24),
                          height: ScreenUtil().setWidth(24),
                          decoration: BoxDecoration(
                              color: pkSettingEntity != null && pkSettingEntity!.record!.rejectSystem!
                                  ? Color(0xff3EC1D3)
                                  : Colors.white,
                              shape: BoxShape.circle),
                        ),
                      ),
                      Text(
                        translate("no_system_invite"),
                        style: TextStyle(color: Colors.black.withOpacity(0.85), fontSize: ScreenUtil().setSp(28)),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(44),
              ),
            ],
          ),
        )));
  }
  void myBuildVVHunoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getSetting() async {
    YBDPkSettingEntity? result = await ApiHelper.getPkSetting(context);
    if (result != null && result.returnCode == Const.HTTP_SUCCESS) {
      pkSettingEntity = result;
      setState(() {});
    }
  }

  Future<bool> setSetting(
    bool? friend,
    bool? system,
  ) async {
    YBDResultBeanEntity? result = await ApiHelper.setPkSetting(context, rejectFriends: friend, rejectSystem: system);
    if (result != null && result.returnCode == Const.HTTP_SUCCESS) {
      return true;
    }
    return false;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getSetting();
  }
  void initStatehA0Ykoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDPkSettingDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetLaqE7oyelive(YBDPkSettingDialog oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
