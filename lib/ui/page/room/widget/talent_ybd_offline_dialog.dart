import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';

/// 房间主播离线的弹框
class YBDTalentOfflineDialog extends StatefulWidget {
  final VoidCallback closeCallback;
  YBDTalentOfflineDialog({required this.closeCallback});

  @override
  State<StatefulWidget> createState() => _YBDTalentOfflineDialogState();
}

class _YBDTalentOfflineDialogState extends State<YBDTalentOfflineDialog> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked sleeping dialog bg");
        widget.closeCallback();
      },
      child: Material(
        // type: MaterialType.transparency,
        color: Colors.black.withOpacity(0.3),
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: ScreenUtil().setWidth(500),
                child: Column(
                  children: <Widget>[
                    // 图片
                    Container(
                      width: ScreenUtil().setWidth(420),
                      height: ScreenUtil().setHeight(493),
                      child: Image.asset('assets/images/liveroom/room_sleeping.webp', fit: BoxFit.cover),
                    ),
                    SizedBox(height: ScreenUtil().setWidth(40)),
                    Text(
                      'This room is offline, so you can’t grab',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: ScreenUtil().setSp(28),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setWidth(2)),
                    Text(
                      'a mic right now.',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: ScreenUtil().setSp(28),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setWidth(40)),
                    YBDScaleAnimateButton(
                      // OK 按钮
                      onTap: () {
                        logger.v('clicked ok btn');
                        widget.closeCallback();
                      },
                      child: _bottomBtn(),
                    ),
                    SizedBox(height: ScreenUtil().setWidth(50)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void build8PXsVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部按钮
  Widget _bottomBtn() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(60))),
        gradient: LinearGradient(
          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      width: ScreenUtil().setWidth(200),
      height: ScreenUtil().setWidth(64),
      alignment: Alignment.center,
      child: Text(
        // TODO: 翻译
        'OK',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(28),
          color: Colors.white,
        ),
      ),
    );
  }
  void _bottomBtnB1BfRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
