import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import '../../profile/user_profile/bloc/follow_ybd_user_bloc.dart';
import '../bloc/mini_ybd_profile_bloc.dart';
import '../bloc/room_ybd_bloc.dart';
import '../define/room_ybd_define.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';

/// mini profile 页面的关注按钮
class YBDMiniProfileFollow extends StatefulWidget {
  /// 被关注用户的用户 id
  final int? userId;

  /// 房间 id
  final int? roomId;

  YBDMiniProfileFollow(this.userId, this.roomId);

  @override
  _YBDMiniProfileFollowState createState() => _YBDMiniProfileFollowState();
}

class _YBDMiniProfileFollowState extends State<YBDMiniProfileFollow> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<YBDFollowUserBloc>(
          create: (context) {
            YBDFollowUserBloc bloc = YBDFollowUserBloc(context, '${widget.userId}');
            bloc.add(FollowUserEvent.Request);
            return bloc;
          },
        ),
      ],
      child: BlocBuilder<YBDFollowUserBloc, YBDFollowUserBlocState>(
        buildWhen: (prev, current) {
          if (prev.status != current.status) {
            context.read<YBDMiniProfileBloc>().add(MiniProfileEvent.Request);

            // if (widget.userId == widget.roomId) {
            //   // 刷新房间顶部的关注按钮
            // 用[YBDFollowUserBloc]刷新按钮状态
            //   context.read<YBDRoomBloc>().add(RoomBlocEvent.UpdateRoomInfo);
            // }
            return true;
          } else {
            return false;
          }
        },
        builder: (context, state) {
          // 关注按钮
          return _followBtn(context, state);
        },
      ),
    );
  }
  void buildHCVbXoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关注按钮
  Widget _followBtn(BuildContext context, YBDFollowUserBlocState state) {
    String btnTitle = '';
    String btnIcon = '';
    Color bgColor = Colors.white;

    switch (state.status) {
      case FollowStatus.Loading:
        btnTitle = translate('Loading');
        btnIcon = 'assets/images/liveroom/profile_follow@2x.webp';
        break;
      case FollowStatus.Following:
        btnTitle = translate('follow');
        btnIcon = 'assets/images/liveroom/profile_follow@2x.webp';
        bgColor = Color(0xffC8C8C8);
        break;
      case FollowStatus.UnFollowing:
        btnTitle = translate('following');
        btnIcon = 'assets/images/liveroom/profile_following@2x.webp';
        bgColor = Color(0xffC8C8C8);
        break;
      case FollowStatus.Followed:
        btnTitle = translate('following');
        btnIcon = 'assets/images/liveroom/profile_following@2x.webp';
        break;
      case FollowStatus.UnFollowed:
        btnTitle = translate('follow');
        btnIcon = 'assets/images/liveroom/profile_follow@2x.webp';
        break;
      case FollowStatus.Friend:
        btnTitle = translate('friends');
        btnIcon = 'assets/images/liveroom/profile_friends@2x.webp';
        break;
    }

    return YBDScaleAnimateButton(
        onTap: () {
          switch (state.status) {
            case FollowStatus.Loading:
            case FollowStatus.Following:
            case FollowStatus.UnFollowing:
              break;
            case FollowStatus.Followed:
              context.read<YBDFollowUserBloc>().add(FollowUserEvent.UnFollow);
              break;
            case FollowStatus.UnFollowed:
              context.read<YBDFollowUserBloc>().add(FollowUserEvent.Follow);
              break;
            case FollowStatus.Friend:
              context.read<YBDFollowUserBloc>().add(FollowUserEvent.UnFriend);
              break;
          }
        },
        child: Container(
          width: 96.px,
          child: Column(
            children: [
              Image.asset(btnIcon, width: 70.px, height: 70.px),
              SizedBox(height: 5.px),
              Text(
                btnTitle,
                style: TextStyle(fontSize: 20.sp2, color: Colors.black.withOpacity(0.5)),
              ),
            ],
          ),
        ));
  }
  void _followBtntcriKoyelive(BuildContext context, YBDFollowUserBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
