import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';

class YBDInsufficinetDialog extends StatefulWidget {
  @override
  YBDInsufficinetDialogState createState() => new YBDInsufficinetDialogState();
}

class YBDInsufficinetDialogState extends BaseState<YBDInsufficinetDialog> {
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Center(
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(width: ScreenUtil().setWidth(3), color: Color(0xff47CDCC)),
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32)))),
          padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
          width: ScreenUtil().setWidth(500),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset(
                      'assets/images/dc/daily_check_close_btn.png',
                      width: ScreenUtil().setWidth(55),
                    ),
                  )
                ],
              ),
              Text(
                translate('balance_insufficient'),
                style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(28)),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(12),
              ),
              Text(
                translate('top_up_now'),
                style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(28)),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(42),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.top_up);
                },
                child: Container(
                  width: ScreenUtil().setWidth(282),
                  height: ScreenUtil().setWidth(68),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter),
                      borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40)))),
                  child: Center(
                    child: Text(
                      translate('Go'),
                      style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(30),
              ),
              // ...(modify ? _warningWidget() : _modifyWidget())
            ],
          ),
        ),
      ),
    );
  }
  void myBuildyGNbyoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStateyybC8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDInsufficinetDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetPdSoVoyelive(YBDInsufficinetDialog oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
