
/*
 * @Author: William-Zhou
 * @Date: 2022-12-28 23:07:41
 * @LastEditTime: 2023-01-12 14:07:12
 * @LastEditors: William
 * @Description: 
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_activity_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/room_ybd_special_entrance_helper.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/home/widget/popular_ybd_task_float.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_icon_banner.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

class YBDSpecialEntrance extends StatefulWidget {
  const YBDSpecialEntrance({Key? key}) : super(key: key);

  @override
  State<YBDSpecialEntrance> createState() => _YBDSpecialEntranceState();
}

class _YBDSpecialEntranceState extends State<YBDSpecialEntrance> {
  int _currentPageIndex = 0;
  List<YBDSpecialEntranceEntity> data = [];

  @override
  void initState() {
    super.initState();
    // "specialEntrance":[{"route":"https://www.bilibili.com/","pic":"https://d3py0gff3tm44e.cloudfront.net/games/ic_Royal_Pattern.png","remainingTime":1673452800000},{"route":"https://www.baidu.com/","pic":"https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/tp_go_main.png","remainingTime":-1}]
    List<Map<String, dynamic>> specialEntrance =
        List<Map<String, dynamic>>.from(YBDCommonUtil.getRoomOperateInfo().specialEntrance!);
    List<YBDSpecialEntranceEntity> entity = specialEntrance.map((e) {
      YBDSpecialEntranceEntity s = YBDSpecialEntranceEntity();
      yBDSpecialEntranceEntityFromJson(s, e);
      // print('22.12.29---s:${s.toJson()}');
      return s;
    }).toList();
    // data = entity ?? [];
    List<YBDSpecialEntranceEntity> originData = entity;
    // 过期不展示
    for (YBDSpecialEntranceEntity a in originData) {
      int remainingTime = a.remainingTime ?? 0;
      if (remainingTime <= 0) {
        data.add(a);
        break;
      }
      // utc时间根据时区偏移来计算
      // DateTime.now().toUtc().millisecondsSinceEpoch 这个获取还是会转化为本地时间
      int utcMilliseconds = DateTime.now().millisecondsSinceEpoch - DateTime.now().timeZoneOffset.inMilliseconds;
      if (remainingTime > utcMilliseconds) data.add(a);
    }
  }
  void initStatext6FToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    if (data.length == 0) return Container();
    return Container(
        width: 120.px,
        // height: 165.px,
        // color: Colors.red.withOpacity(0.3),
        constraints: BoxConstraints(maxHeight: 125.px),
        margin: EdgeInsets.only(top: 20.px),
        // color: Colors.pink,
        child: Stack(alignment: Alignment.topCenter, children: [
          Container(),
          if (data.length > 0)
            Container(
              // color: Colors.red.withOpacity(0.3),
              child: PageView.builder(
                itemBuilder: (content, index) {
                  bool _visible = true;
                  return Visibility(
                    visible: _visible,
                    child: YBDDelayGestureDetector(
                      onTap: () {
                        if (data[index].route?.isNotEmpty ?? false) {
                          if (data[index].route?.startsWith('http') ?? false) {
                            YBDActivityInTrack()
                                .activityIn(YBDTAProps(location: YBDTAEventLocation.slider, url: data[index].route));
                          }
                          YBDNavigatorHelper.openUrl(context, data[index].route!, type: WebEntranceType.Slider);
                        }
                      },
                      child: Column(
                        children: [
                          // 图片
                          Container(
                            // color: Colors.blue,
                            child: YBDGameWidgetUtil.networkImage(
                              url: data[index].pic ?? '',
                              width: 100.px,
                              height: 78.px,
                              fit: BoxFit.contain,
                            ),
                          ),
                          // SizedBox(height: 7.px),
                          // 倒计时 有才显示
                          if ((data[index].remainingTime ?? -1) > 0)
                            Container(
                              // margin: EdgeInsets.only(bottom: 7.px),
                              child: YBDTaskFloatCountdown(
                                remainTime: data[index].remainingTime!,
                                isEndTime: true,
                                scale: 0.8,
                                finishCallback: () {
                                  _visible = false;
                                  data.removeAt(index);
                                  if (mounted) setState(() {});
                                },
                              ),
                            ),
                          /*
                          Container(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: List.generate(data.length, (i) {
                              return Container(
                                margin: EdgeInsets.symmetric(horizontal: 4.px),
                                width: 10.px,
                                height: 10.px,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: _currentPageIndex == i ? Colors.white : Colors.white.withOpacity(0.2)),
                              );
                            }).toList(),
                          ))

                           */
                        ],
                      ),
                    ),
                  );
                },
                onPageChanged: (index) {
                  if (mounted)
                    setState(() {
                      _currentPageIndex = index;
                    });
                },
                itemCount: data.length,
                scrollDirection: Axis.horizontal,
              ),
            ),
          if (data.length > 0)
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(data.length, (i) {
                  return Container(
                    margin: EdgeInsets.symmetric(horizontal: 4.px),
                    width: 10.px,
                    height: 10.px,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _currentPageIndex == i ? Colors.white : Colors.white.withOpacity(0.2)),
                  );
                }).toList(),
              )),
            )
        ]));
  }
  void buildG0mSDoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDSpecialEntranceEntity with JsonConvert<YBDSpecialEntranceEntity> {
  String? route;
  String? pic;
  int? remainingTime; // 结束时间戳
}
