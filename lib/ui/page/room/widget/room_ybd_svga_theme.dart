import 'dart:async';


import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/src/cache_managers/default_cache_manager.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:path/path.dart' as p;
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_theme_player.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';

/// svga文件后缀名
const FileSvga = '.svga';

/// 显示静态图和svga
/// 带缓存功能
/// [themeUrl]图片下载地址，用来下载图片和判断图片文件扩展名
/// [width]缩略图和图片的显示宽度，默认为屏幕宽度
/// [height]缩略图和图片的显示高度，默认为屏幕高度
class YBDRoomSvgaTheme extends StatefulWidget {
  final String? themeUrl;
  final double? width;
  final double? height;
  final bool showLoading;

  bool flexible;

  Widget? holder;

  YBDRoomSvgaTheme(
    this.themeUrl, {
    this.width,
    this.height,
    this.showLoading = true,
    this.flexible: false,
    this.holder,
    Key? key,
  }) : super(key: key);

  @override
  State<YBDRoomSvgaTheme> createState() => _YBDRoomSvgaThemeState();
}

class _YBDRoomSvgaThemeState extends State<YBDRoomSvgaTheme> {
  // String _localPath = '';

  @override
  void initState() {
    super.initState();
  }
  void initState7869Woyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    final imgWidth = widget.width ?? ScreenUtil().screenWidth;
    double? imgHeight;
    if (!widget.flexible) {
      imgHeight = widget.height ?? ScreenUtil().screenHeight;
    }
    return AnimatedSwitcher(
      child: _imgContent(imgWidth, imgHeight),
      duration: Duration(milliseconds: 500),
    );
  }
  void buildgRYMooyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _imgContent(double imgWidth, double? imgHeight) {
    return FutureBuilder(
        future: _loadImg(),
        builder: (_, data) {
          if (!data.hasData) {
            if (widget.showLoading) {
              if (widget.holder != null) return widget.holder!;
              return YBDLoadingCircle();
            } else {
              return SizedBox();
            }
          }
          // 显示加载后svga
          if (p.extension(widget.themeUrl!) == FileSvga) {
            return SizedBox(
              width: imgWidth,
              height: imgHeight,
              child: YBDRoomThemePlayer(
                file: File(data.data as String),
                boxFit: widget.flexible ? BoxFit.fitWidth : BoxFit.cover,
              ),
            );
          }
          return Container(
            width: imgWidth,
            height: imgHeight,
            child: Image.file(
              File(data.data as String),
              fit: BoxFit.cover,
            ),
          );
        });
  }
  void _imgContentJa2IWoyelive(double imgWidth, double? imgHeight) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 加载图片
  Future<String> _loadImg() async {
    // 从缓存获取文件
    File file = await DefaultCacheManager().getSingleFile(widget.themeUrl!);

    // 下载后获取文件路径
    String result = file.path;
    logger.v('loaded file: $result, url: ${widget.themeUrl}');

    return result;
  }
  void _loadImgp5ttHoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
