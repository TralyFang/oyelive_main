import 'dart:async';


import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../status/local_audio/circle_ybd_check_box.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import '../define/room_ybd_define.dart';

typedef ForbiddenUserCallback = Function(int);

/// 用户禁言对话框
class YBDMuteUserDialog extends StatefulWidget {
  /// 对话框类型
  final ForbiddenUserType? type;

  /// 点取消按钮的回调
  final VoidCallback? cancelCallback;

  /// 点确认按钮的回调
  final ForbiddenUserCallback? okCallback;

  YBDMuteUserDialog({
    this.cancelCallback,
    this.okCallback,
    this.type = ForbiddenUserType.Mute,
  });

  @override
  State<StatefulWidget> createState() => _YBDMuteUserDialogState();
}

class _YBDMuteUserDialogState extends State<YBDMuteUserDialog> {
  /// 列表数据
  List<YBDMuteUserItemData> _data = [];

  /// item 高度
  final double itemHeight = 80;

  @override
  void initState() {
    super.initState();

    if (widget.type == ForbiddenUserType.Boot) {
      // TODO: 翻译
      YBDMuteUserItemData fiveMinutes = YBDMuteUserItemData('1 hour', 60, true);
      _data.add(fiveMinutes);
      YBDMuteUserItemData oneHour = YBDMuteUserItemData('1 day', 60 * 24, false);
      _data.add(oneHour);
    } else if (widget.type == ForbiddenUserType.Mute) {
      YBDMuteUserItemData fiveMinutes = YBDMuteUserItemData('5 minutes', 5, true);
      _data.add(fiveMinutes);
      YBDMuteUserItemData oneHour = YBDMuteUserItemData('1 hour', 60, false);
      _data.add(oneHour);
      YBDMuteUserItemData oneDay = YBDMuteUserItemData('1 day', 60 * 24, false);
      _data.add(oneDay);
    } else {
      // bloc 列表为空
    }
  }
  void initStatekfkDooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the forbidden user dialog bg");
        // 隐藏禁止用户弹框
        Navigator.pop(context);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  // 避免点白色背景隐藏弹框
                },
                child: Container(
                  width: ScreenUtil().setWidth(494),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(32)))),
                      color: Colors.white),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setWidth(54)),
                      // 标题
                      Container(
                        child: Center(
                          child: Text(
                            // 对话框标题
                            _dialogTitle(),
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: ScreenUtil().setSp(28),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(20)),
                      _itemList(),
                      SizedBox(height: ScreenUtil().setWidth(20)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          YBDScaleAnimateButton(
                            onTap: () {
                              logger.v('clicked cancel btn');
                              if (null != widget.cancelCallback) {
                                widget.cancelCallback!();
                              }
                            },
                            child: _bottomBtn('Cancel', 0),
                          ),
                          YBDScaleAnimateButton(
                            onTap: () {
                              logger.v('clicked ok btn');
                              if (null != widget.okCallback) {
                                YBDMuteUserItemData? itemData = _data.firstWhereOrNull(
                                  (element) => element.isChecked,
                                );
                                widget.okCallback!(itemData?.period ?? 0);
                              }
                            },
                            child: _bottomBtn('Ok', 1),
                          ),
                        ],
                      ),
                      SizedBox(height: ScreenUtil().setWidth(50)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void build4AqyKoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 对话框标题
  String _dialogTitle() {
    // TODO: 翻译
    switch (widget.type) {
      case ForbiddenUserType.Boot:
        return 'Boot this user for how long?';
      case ForbiddenUserType.Block:
        return 'Are you sure to block?';
      case ForbiddenUserType.Mute:
        return 'Mute this user for how long?';
      default:
        return '';
    }
  }
  void _dialogTitle7y2dqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 选项列表
  Widget _itemList() {
    return Container(
      height: ScreenUtil().setWidth(_data.length * itemHeight),
      child: ListView.builder(
        itemCount: _data.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              logger.v('clicked mute item : $index');
              _data.forEach((element) {
                if (element.title == _data[index].title) {
                  element.isChecked = true;
                } else {
                  element.isChecked = false;
                }
              });
              setState(() {});
            },
            child: _muteUserItem(_data[index]),
          );
        },
      ),
    );
  }
  void _itemListuUr3Coyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item
  Widget _muteUserItem(YBDMuteUserItemData itemData) {
    return Container(
      height: ScreenUtil().setWidth(itemHeight),
      width: ScreenUtil().setWidth(400),
      color: Colors.white,
      child: Row(
        children: <Widget>[
          SizedBox(width: ScreenUtil().setWidth(70)),
          YBDCircleCheckBox(
            selected: itemData.isChecked,
            selectedColor: Color(0xff62C8F9),
            selectedBorderColor: Color(0xffC6C6C6),
            unselectedBorderColor: Color(0xffC6C6C6),
          ),
          SizedBox(width: ScreenUtil().setWidth(10)),
          Text(
            itemData.title,
            style: TextStyle(
              color: Colors.black,
              fontSize: ScreenUtil().setSp(28),
              fontWeight: FontWeight.normal,
            ),
          ),
        ],
      ),
    );
  }
  void _muteUserItemnwo3aoyelive(YBDMuteUserItemData itemData) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部按钮
  Widget _bottomBtn(String title, int index) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(ScreenUtil().setWidth(60)),
        ),
        gradient: LinearGradient(
          colors: index == 0 ? [Color(0xffCCCCCC), Color(0xffCCCCCC)] : [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      width: ScreenUtil().setWidth(200),
      height: ScreenUtil().setWidth(64),
      alignment: Alignment.center,
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(28),
          color: Colors.white,
        ),
      ),
    );
  }
  void _bottomBtnrUTZcoyelive(String title, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// item 数据模型
class YBDMuteUserItemData {
  /// 标题
  String title;

  /// 已选中
  bool isChecked;

  /// 禁言分钟数
  int period;

  YBDMuteUserItemData(this.title, this.period, this.isChecked);
}
