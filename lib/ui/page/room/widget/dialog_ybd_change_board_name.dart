import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/result_ybd_bean.dart';

//v2.1.0 borad name 警告修改需求

class YBDChangeBoardNameDialog extends StatefulWidget {
  @override
  YBDChangeBoardNameDialogState createState() => new YBDChangeBoardNameDialogState();
}

class YBDChangeBoardNameDialogState extends BaseState<YBDChangeBoardNameDialog> {
  bool modify = false;

  TextEditingController _textEditingController = TextEditingController();
  //警告的widget
  _warningWidget() {
    return [
      Padding(
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(38), vertical: ScreenUtil().setWidth(6)),
        child: Text(
          '${translate('revise_it_again')}\n\n${translate('now_board_name')}',
          style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(24)),
        ),
      ),
      SizedBox(
        height: ScreenUtil().setWidth(36),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              width: ScreenUtil().setWidth(202),
              height: ScreenUtil().setWidth(64),
              decoration: BoxDecoration(
                  color: Color(0xffCCCCCC), borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40)))),
              child: Center(
                child: Text(
                  translate('ok'),
                  style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                ),
              ),
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(40),
          ),
          GestureDetector(
            onTap: () {
              modify = true;
              setState(() {});
            },
            child: Container(
              width: ScreenUtil().setWidth(202),
              height: ScreenUtil().setWidth(64),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter),
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40)))),
              child: Center(
                child: Text(
                  translate('modify_now'),
                  style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                ),
              ),
            ),
          ),
        ],
      ),
      SizedBox(
        height: ScreenUtil().setWidth(38),
      ),
    ];
  }

  //修改widget
  _modifyWidget() {
    return [
      Text(
        '${translate('board_name')}: ',
        style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(24)),
      ),
      SizedBox(
        height: ScreenUtil().setWidth(20),
      ),
      SizedBox(
        width: ScreenUtil().setWidth(425),
        height: ScreenUtil().setWidth(38),
        child: TextField(
          style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.black),
          controller: _textEditingController,
          textAlign: TextAlign.center,
          maxLength: 30,
          cursorColor: Const.INPUT_CURSOR_COLOR,
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.all(0),
            hintText: translate('wel_mlr'),
            border: InputBorder.none,
            counterText: '',
            hintStyle: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.black.withOpacity(0.5)),
          ),
        ),
      ),
      SizedBox(
        height: ScreenUtil().setWidth(6),
      ),
      Container(
        height: ScreenUtil().setWidth(1),
        width: ScreenUtil().setWidth(348),
        color: Colors.black.withOpacity(0.1),
      ),
      SizedBox(
        height: ScreenUtil().setWidth(34),
      ),
      GestureDetector(
        onTap: _postNameNow,
        child: Container(
          width: ScreenUtil().setWidth(202),
          height: ScreenUtil().setWidth(64),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter),
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40)))),
          child: Center(
            child: Text(
              translate('ok'),
              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
            ),
          ),
        ),
      ),
      SizedBox(
        height: ScreenUtil().setWidth(28),
      ),
    ];
  }

  _postNameNow() async {
    //没填不做操作
    if (_textEditingController.text.isNotEmpty) {
      showLockDialog();
      YBDResultBeanEntity? resultBeanEntity = await ApiHelper.editTalent(context, title: _textEditingController.text);

      dismissLockDialog();

      if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
        popOut();
        YBDToastUtil.toast(translate('success'));
      } else {
        YBDToastUtil.toast(resultBeanEntity?.returnMsg);
      }
    } else {
      popOut();
    }
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Center(
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(width: ScreenUtil().setWidth(3), color: Color(0xff47CDCC)),
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32)))),
          padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
          width: ScreenUtil().setWidth(500),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset(
                      'assets/images/dc/daily_check_close_btn.png',
                      width: ScreenUtil().setWidth(48),
                    ),
                  )
                ],
              ),
              if (!modify) ..._warningWidget(),
              if (modify) ..._modifyWidget()
              // ...(modify ? _warningWidget() : _modifyWidget())
            ],
          ),
        ),
      ),
    );
  }
  void myBuildvMhHPoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStateAZsIqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDChangeBoardNameDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetullSBoyelive(YBDChangeBoardNameDialog oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
