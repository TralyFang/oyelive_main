import 'dart:async';


// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';

// import '../../../../base/base_state.dart';
// import '../../../../common/constant/const.dart';
// import '../../../../common/util/log_util.dart';
// import '../../../../module/api_helper.dart';
// import '../../home/entity/car_list_entity.dart';
// import '../../store/entity/downloadbean.dart';
// import '../../store/widget/svga_play.dart';
// import '../bloc/room_bloc.dart';
// import '../service/live_service.dart';

// /// 礼物动画&&进房座驾
// class RoomGiftAnimation extends StatefulWidget {
//   ///进房座驾
//   YBDCarListRecord carListRecord;

//   ///
//   List<String> roomAnimationPath = List(2);

//   YBDRoomBloc roomBloc;

//   RoomGiftAnimation({this.carListRecord, this.roomAnimationPath, this.roomBloc});

//   @override
//   _State createState() => _State();
// }

// class _State extends BaseState<RoomGiftAnimation> {
//   ///进房动画地址
//   List<String> enterRoomLocalPath = List(2);

//   ///进房动画地址集
//   Map<String, List<String>> enterRoomMap = new Map<String, List<String>>();

//   ///下载列表
//   List<YBDDownLoadBean> downLoadList = [];

//   ///座驾动画 --所有
//   List<YBDCarListRecord> carEnterRoomList = [];

//   ///进房用户
//   int curEnterRoomUser;

//   @override
//   void initState() {
//     super.initState();
//     _requestCarList();
//   }

//   /// 请求座驾列表数据(用于动画解析)
//   _requestCarList() async {
//     if ((YBDLiveService.instance.carList ?? []).isNotEmpty) {
//       carEnterRoomList = YBDLiveService.instance.carList;
//       return;
//     }

//     YBDCarListEntity carListEntity = await ApiHelper.carList(context, queryAll: true);
//     if (null != carListEntity && carListEntity.returnCode == Const.HTTP_SUCCESS) {
//       logger.v('room bg queryCAR SUCCESS');
//       carEnterRoomList = carListEntity.record;
//     } else {
//       logger.v('room bg queryCAR failed');
//     }
//   }

//   @override
//   Widget myBuild(BuildContext context) {
//     return _enterRoomAnimation(path: widget.roomAnimationPath[0], mp3file: widget.roomAnimationPath[1]);
//   }

//   ///进房座驾动画
//   Widget _enterRoomAnimation({String path, String mp3file}) {
//     print('_enterRoomAnimation path:$path mp3file:$mp3file');
//     return path == null
//         ? Container()
//         : YBDSvgaPlayerPage(url: path, mp3File: mp3file, callBack: playerStateCallBack, mud: 1);
//   }

//   /// 动画播放状态
//   Future playerStateCallBack(bool isPlaying) async {
//     print('playerStateCallBack isPlaying:$isPlaying');
//     widget.roomBloc.showEnterRoomAnimation(isPlaying);
//     if (!isPlaying) {
//       ///播放结束 把路劲清空
//       try {
//         enterRoomLocalPath[0] = null;
//         enterRoomLocalPath[1] = null;
//       } catch (e) {}
//     }
//   }

//   @override
//   void dispose() {
//     // TODO: implement dispose
//     super.dispose();
//   }
// }
