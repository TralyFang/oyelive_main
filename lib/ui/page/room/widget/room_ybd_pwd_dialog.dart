import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import '../bloc/room_ybd_bloc.dart';
import '../bloc/room_ybd_bloc_state.dart';
import '../define/room_ybd_define.dart';
import '../service/live_ybd_service.dart';
import '../util/tp_ybd_dialog_manager.dart';

/// 确认输入密码的回调
typedef RoomPwdDialogCallback = Function(String);

/// 输入房间密码的对话框
class YBDRoomPwdDialog extends StatefulWidget {
  final RoomPwdDialogCallback? enterPwdCallback;

  YBDRoomPwdDialog({this.enterPwdCallback});

  @override
  State<StatefulWidget> createState() => _YBDRoomPwdDialogState();
}

class _YBDRoomPwdDialogState extends State<YBDRoomPwdDialog> {
  FocusNode _focus = new FocusNode();
  bool _inputFocused = false;

  /// 显示密码
  bool _showPwd = false;

  TextEditingController? _textController;

  @override
  void initState() {
    super.initState();
    _textController = TextEditingController();

    _focus.addListener(() {
      setState(() {
        if (_focus.hasFocus) {
          logger.v('text input has focus');
          _inputFocused = true;
        } else {
          logger.v('text input has no focus');
          _inputFocused = false;
        }
      });
    });
  }
  void initStatefjoyGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    _textController!.dispose();
  }
  void dispose5g34Joyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    // 底部边距
    double bottomPadding = _inputFocused ? ScreenUtil().setWidth(500) : ScreenUtil().setWidth(0);

    // 容器宽度
    double width = ScreenUtil().setWidth(560);

    // 容器高度
    double outHeight = ScreenUtil().setWidth(610);

    // 头部背景高度
    double headHeight = ScreenUtil().setWidth(200);

    // 白色背景高度
    double innerHeight = outHeight - headHeight / 2;

    return GestureDetector(
      onTap: () {
        logger.v("clicked the room pwd dialog print(");
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          padding: EdgeInsets.only(bottom: bottomPadding),
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Container(
              width: width,
              height: outHeight,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    // 白色背景
                    top: headHeight / 2,
                    child: Container(
                      width: width,
                      height: innerHeight,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(26)))),
                          color: Colors.white),
                      child: Column(children: <Widget>[
                        SizedBox(height: ScreenUtil().setWidth(112)),
                        // 标题
                        _dialogTitle(),
                        SizedBox(height: ScreenUtil().setWidth(34)),
                        _textRow('This studio is password protected.'),
                        SizedBox(height: ScreenUtil().setWidth(4)),
                        _textRow('Type the password to enter.'),
                        SizedBox(height: ScreenUtil().setWidth(10)),
                        BlocListener<YBDRoomBloc, YBDRoomState>(
                          listener: (context, state) {
                            if (state.checkPwdStatus == CheckPwdStatus.Passed) {
                              if (YBDLiveService.instance.dialogManager.isShowing(ROOM_PWD_DIALOG_ID)) {
                                Navigator.pop(context);
                              }
                            }
                          },
                          child: BlocBuilder<YBDRoomBloc, YBDRoomState>(
                            buildWhen: (prev, current) => current.type == RoomStateType.PwdStatusUpdated,
                            builder: (blocContext, state) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  _inputRow(state.checkPwdStatus),
                                  state.checkPwdStatus == CheckPwdStatus.Retry ? _retryText() : Container(),
                                ],
                              );
                            },
                          ),
                        ),
                        // 输入框
                        SizedBox(height: ScreenUtil().setWidth(40)),
                        // 确认按钮
                        _enterBtn(),
                      ]),
                    ),
                  ),
                  // 头部背景
                  Positioned(
                    top: ScreenUtil().setWidth(0),
                    left: ScreenUtil().setWidth(1),
                    child: Container(
                      width: ScreenUtil().setWidth(520),
                      height: headHeight,
                      child: Image.asset(
                        'assets/images/lock_dialog_icon.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  // 关闭按钮
                  Positioned(
                    top: ScreenUtil().setWidth(110),
                    right: ScreenUtil().setWidth(1),
                    child: GestureDetector(
                      onTap: () {
                        logger.v('room_pwd_dialog clicked close btn');
                        logger.v('leave room page context dialog : $context');
                        BlocProvider.of<YBDRoomBloc>(context).exitRoom();
                      },
                      child: Container(
                        // 关闭按钮
                        width: ScreenUtil().setWidth(60),
                        height: ScreenUtil().setWidth(60),
                        child: Image.asset(
                          'assets/images/dialog_close_icon.png',
                          fit: BoxFit.cover,
                          color: Color(0xFF777676),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }
  void buildPE6Mpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 重新输入密码提示语
  Widget _retryText() {
    String text = translate('pwd_wrong_retry');
    return Container(
      padding: EdgeInsets.only(
        left: ScreenUtil().setWidth(66),
      ),
      child: Text(
        text,
        style: TextStyle(
          color: Colors.red,
          fontSize: ScreenUtil().setSp(20),
        ),
      ),
    );
  }
  void _retryTextmqsnPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 对话框标题
  Widget _dialogTitle() {
    return Center(
      // 标题
      child: Text(
        translate('password'),
        style: TextStyle(
          fontSize: ScreenUtil().setSp(36),
          fontWeight: FontWeight.w500,
          color: Colors.black,
        ),
      ),
    );
  }

  /// 对话框文字
  Widget _textRow(String text) {
    return Text(
      text,
      style: TextStyle(
        fontSize: ScreenUtil().setWidth(28),
        color: Color(0xff323232),
      ),
    );
  }
  void _textRowUghryoyelive(String text) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 输入框
  Widget _inputRow(CheckPwdStatus? pwdStatus) {
    return Stack(
      children: <Widget>[
        Row(
          children: <Widget>[
            SizedBox(width: ScreenUtil().setWidth(66)),
            Expanded(
              child: Container(
                height: ScreenUtil().setWidth(80),
                width: ScreenUtil().setWidth(80),
                child: TextField(
                  focusNode: _focus,
                  obscureText: !_showPwd,
                  maxLines: 1,
                  controller: _textController,
                  cursorColor: Color(0xffE422C7),
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: ScreenUtil().setSp(24),
                  ),
                  decoration: InputDecoration(
                    hintStyle: TextStyle(
                      color: Color(0xffADADAD),
                      fontSize: ScreenUtil().setSp(24),
                    ),
                    // TODO: 翻译
                    hintText: 'YBDEnter Password',
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            Container(
              // 眼睛按钮
              width: ScreenUtil().setWidth(100),
              child: TextButton(
                style: TextButton.styleFrom(
                  padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(34),
                  ),
                ),
                onPressed: () {
                  logger.v('clicked show pwd btn : $_showPwd');
                  setState(() {
                    _showPwd = !_showPwd;
                  });
                },
                child: Image.asset(
                  _showPwd ? "assets/images/show_pass.png" : "assets/images/hide_pass.png",
                  width: ScreenUtil().setWidth(38),
                ),
              ),
            ),
            SizedBox(width: ScreenUtil().setWidth(20)),
          ],
        ),
        Positioned(
          // 下划线
          bottom: ScreenUtil().setWidth(10),
          child: Container(
            height: ScreenUtil().setWidth(1),
            width: ScreenUtil().setWidth(452),
            color: pwdStatus == CheckPwdStatus.Retry ? Colors.red : Color(0xff979797),
            margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(56)),
          ),
        ),
      ],
    );
  }
  void _inputRowjoYmCoyelive(CheckPwdStatus? pwdStatus) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 确认按钮
  Widget _enterBtn() {
    return YBDScaleAnimateButton(
      onTap: () {
        logger.v('clicked enter pwd : ${_textController!.text}');
        context.read<YBDRoomBloc>().subscribeRoom(pwd: _textController!.text);
        // 输入错误的密码提交时, 输入框清除
        setState(() {
          _textController!.clear();
        });
      },
      child: Container(
        width: ScreenUtil().setWidth(300),
        height: ScreenUtil().setWidth(64),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
          borderRadius: BorderRadius.circular(30),
        ),
        alignment: Alignment.center,
        child: Text(
          translate('enter'),
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
  void _enterBtn1FkwBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
