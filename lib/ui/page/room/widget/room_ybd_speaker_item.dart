import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/page/room/define/room_ybd_define.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';
import 'package:oyelive_main/ui/widget/level_ybd_tag.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/round_ybd_avatar.dart';
import '../util/room_ybd_util.dart';

/// 房间speaker列表项
class YBDRoomSpeakerItem extends StatefulWidget {
  /// 声网uid
  final int? uid;

  /// 房间ID
  final int? roomId;

  /// The channel ID, which indicates which channel the speaker is in.
  // String channelId;

  YBDRoomSpeakerItem({this.uid, this.roomId});

  @override
  _YBDRoomSpeakerItemState createState() => _YBDRoomSpeakerItemState();
}

class _YBDRoomSpeakerItemState extends BaseState<YBDRoomSpeakerItem> {
  /// 用户信息
  YBDUserInfo? _userInfo;
  bool isTalent = false;

  @override
  void initState() {
    super.initState();

    // 查询speaker的用户信息
    _requestUserInfo();

    ISTalent();
  }
  void initState6cvIqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ISTalent() async {
    YBDUserInfo? uInfo = await YBDSPUtil.getUserInfo();
    isTalent = uInfo?.id == widget.roomId;
  }

  @override
  Widget myBuild(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: ScreenUtil().setWidth(20),
        horizontal: ScreenUtil().setWidth(30),
      ),
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              logger.v('clicked ${widget.uid}');
              YBDRoomUtil.showMiniProfileDialog(
                roomId: widget.roomId,
                userId: widget.uid,
              );
            },
            child: YBDRoundAvatar(
              _userInfo?.headimg,
              scene: "B",
              sex: _userInfo?.sex,
              avatarWidth: 80,
              userId: widget.uid,
              lableSrc: (_userInfo?.vip ?? 0) == 0 ? "" : "assets/images/vip_lv${_userInfo?.vip}.png",
              labelWitdh: 28,
              needNavigation: false,
              levelFrame: _userInfo?.headFrame,
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    // 昵称
                    constraints:
                        BoxConstraints(maxWidth: ScreenUtil().setWidth(370), maxHeight: ScreenUtil().setWidth(55)),
                    child: Text(
                      _userInfo?.nickname ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Color(0xff3A3A3A),
                        fontSize: ScreenUtil().setSp(28),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setWidth(_userInfo?.vipIcon != null ? 3 : 15)),
                  Row(
                    children: <Widget>[
                      YBDVipIcon(_userInfo?.vipIcon),
                      Container(
                        // 性别标签
                        height: ScreenUtil().setWidth(20),
                        child: Image.asset(
                          _userInfo?.sex == 2 ? "assets/images/tag_male.webp" : "assets/images/tag_female.webp",
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(20)),
                      YBDLevelTag(_userInfo?.level),
                    ],
                  )
                ],
              ),
            ),
          ),
          _channelId(),
        ],
      ),
    );
  }
  void myBuildHkNadoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 频道Id
  _channelId() {
    return Offstage(
      offstage: !isTalent,
      child: YBDScaleAnimateButton(
        onTap: () {
          YBDDialogUtil.showMuteUserDialog(
            context,
            type: ForbiddenUserType.Boot,
            okCallback: (int period) async {
              logger.v('boot user : ${widget.uid} for $period minutes');
              if (await YBDRoomUtil.checkForbiddenCondition(widget.roomId, widget.uid)) {
                YBDRoomUtil.bootUser(widget.roomId, widget.uid, period);
              }
            },
          );
          // YBDToastUtil.toast('call api to remove uid: ${widget.uid}');
          // if (!_isFollowing) {
          //   logger.v('clicked follow user btn : ${widget.userInfo.id}');
          //   _requestUnFollow('${widget.userInfo.id}');
          // }
        },
        child: Container(
          width: ScreenUtil().setWidth(145),
          height: ScreenUtil().setWidth(56),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(37)))),
            gradient: LinearGradient(
              colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: Center(
            child: Text(
              translate('add_black_list'),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(20),
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// 请求用户信息
  void _requestUserInfo() {
    ApiHelper.queryUserInfo(context, widget.uid).then((value) {
      if (null != value) {
        if (mounted) {
          setState(() {
            _userInfo = value;
          });
        }
      }
    });
  }
  void _requestUserInfoAN8Cboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
