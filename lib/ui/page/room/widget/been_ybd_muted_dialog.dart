import 'dart:async';


import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';

import '../../../../common/room_socket/message/common/internal.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import '../util/message_ybd_util.dart';

class YBDBeenMutedDialog extends StatefulWidget {
  final YBDInternal internal;
  Duration autoConfirmDuration;
  YBDBeenMutedDialog(this.internal, this.autoConfirmDuration);
  @override
  YBDBeenMutedDialogState createState() => new YBDBeenMutedDialogState();
}

class YBDBeenMutedDialogState extends BaseState<YBDBeenMutedDialog> {
  Timer? _autoConfirmTimer;
  final interval = Duration(seconds: 1);
  Duration countDown = Duration(seconds: 1);
  setAutoConfirm() {
    if (widget.autoConfirmDuration != null) {
      print("setAuto");
      countDown = widget.autoConfirmDuration;
      _autoConfirmTimer = Timer.periodic(interval, (timer) {
        if (countDown> Duration.zero) {
          countDown -= interval;
        } else {
          _autoConfirmTimer?.cancel();
          _autoConfirmTimer = null;

          Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
          onTap(store);
        }
        setState(() {});
      });
    }
  }

  onTap(store) {
    logger.v('get showDialogWithInternal22222');
    Navigator.pop(context);
    if (YBDMessageUtil.shouldLeaveRoomWithInternal(widget.internal, store.state.bean)) {
      // 在ludo游戏中，被踢出房间，需要等待游戏结束才能退出房间
      if (widget.internal.playingLudo!) {
        YBDLiveService.instance.bootedLiveRoomLimit = true;
        // YBDToastUtil.toast(translate('boot_ludo_progress_tips'));
        BlocProvider.of<YBDRoomBloc>(context).ludoplayingExitRoom();
      }else {
        BlocProvider.of<YBDRoomBloc>(context).exitRoom();
      }
    }
  }

  bool shouldLeave = false;

  @override
  Widget myBuild(BuildContext context) {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    if (YBDMessageUtil.shouldLeaveRoomWithInternal(widget.internal, store.state.bean!)) {
      BlocProvider.of<YBDRoomBloc>(context).disconnectRoom(widget.internal.roomId);
    }
    return GestureDetector(
      onTap: () {
        logger.v("clicked the been forbidden dialog print(");
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  // 避免点白色背景隐藏弹框
                },
                child: Container(
                  width: ScreenUtil().setWidth(494),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(32)))),
                      color: Colors.white),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setWidth(54)),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(60)),
                        // 对话框标题
                        child: Center(
                          child: Text(
                            YBDMessageUtil.forbiddenTextWithInternal(widget.internal, store.state.bean, type: 1),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: ScreenUtil().setSp(28),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(40)),
                      YBDScaleAnimateButton(
                        onTap: () {
                          onTap(store);
                        },
                        child: _bottomBtn(),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(50)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void myBuildov6ezoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部按钮
  Widget _bottomBtn() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
        gradient: LinearGradient(
          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      width: ScreenUtil().setWidth(280),
      height: ScreenUtil().setWidth(64),
      alignment: Alignment.center,
      child: Text(
        // TODO: 翻译
        'OK' + " ${shouldLeave ? "in ${countDown.inSeconds}s" : ''}",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(28),
          color: Colors.white,
        ),
      ),
    );
  }
  void _bottomBtngoAz3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    shouldLeave = YBDMessageUtil.shouldLeaveRoomWithInternal(widget.internal, store.state.bean!);
    if (shouldLeave) {
      setAutoConfirm();
    }
  }
  void initState1R39boyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    _autoConfirmTimer?.cancel();

    super.dispose();
  }

  @override
  void didUpdateWidget(YBDBeenMutedDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetetEz4oyelive(YBDBeenMutedDialog oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
