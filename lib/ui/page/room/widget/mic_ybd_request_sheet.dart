import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../bloc/room_ybd_bloc.dart';
import '../bloc/room_ybd_bloc_state.dart';
import '../mic/mic_ybd_bloc.dart';
import 'mic_ybd_request_item.dart';

/// 麦位请求列表
class YBDMicRequestSheet extends StatefulWidget {
  /// 房间ID
  final int roomId;

  YBDMicRequestSheet(this.roomId);

  @override
  State<StatefulWidget> createState() => _YBDMicRequestSheetState();
}

class _YBDMicRequestSheetState extends State<YBDMicRequestSheet> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<YBDRoomBloc, YBDRoomState>(
      listener: (context, state) {
        if (state.showKeyboard ?? false) {
          if (mounted) {
            Navigator.pop(context);
          }
        }
      },
      child: Container(
        height: ScreenUtil().screenHeight * 0.7,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(ScreenUtil().setWidth(16)),
              topRight: Radius.circular(ScreenUtil().setWidth(16)),
            ),
          ),
          child: Column(
            children: [
              // 标题栏
              _titleRow(),
              // 表头栏
              _columnTitleRow(),
              Expanded(
                // 用户列表
                child: _userList(),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildurJiPoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 标题栏
  Widget _titleRow() {
    return Container(
      width: ScreenUtil().screenWidth,
      height: ScreenUtil().setWidth(96),
      child: Stack(
        children: <Widget>[
          Center(
            // TODO: 翻译
            child: Text(
              'Mic Requests',
              style: TextStyle(
                fontSize: ScreenUtil().setSp(28),
                color: Colors.black,
              ),
            ),
          ),
          Positioned(
            // 关闭按钮
            top: 0,
            right: 0,
            child: _closeBtn(),
          ),
        ],
      ),
    );
  }
  void _titleRowu8CAgoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 申请麦位的用户列表
  Widget _userList() {
    return BlocBuilder<YBDMicBloc, YBDMicBlocState>(builder: (context, state) {
      if (state.micRequestlist == null || state.micRequestlist!.isEmpty) {
        return YBDPlaceHolderView(img: 'assets/images/empty/empty_my_profile.webp', text: 'No User');
      } else {
        return ListView.builder(
          itemCount: state.micRequestlist!.length,
          itemBuilder: (context, index) {
            return YBDMicRequestItem(index + 1, widget.roomId, state.micRequestlist![index]);
          },
        );
      }
    });
  }
  void _userListVbYryoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 表头栏
  Widget _columnTitleRow() {
    return Container(
      height: ScreenUtil().setWidth(78),
      color: Color(0xffEEEEEE),
      child: Row(
        children: <Widget>[
          SizedBox(width: ScreenUtil().setWidth(28)),
          Text(
            // TODO: 翻译
            'Mic',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(28),
              color: Colors.black,
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(25)),
          Text(
            // TODO: 翻译
            'User',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(28),
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
  void _columnTitleRowTmhJVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关闭按钮
  Widget _closeBtn() {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(ScreenUtil().setWidth(16)),
        ),
        onTap: () {
          logger.v('clicked mic request sheet close btn');
          Navigator.pop(context);
        },
        child: Container(
          width: ScreenUtil().setWidth(96),
          height: ScreenUtil().setWidth(96),
          child: Center(
            child: Image.asset(
              'assets/images/icon_close.png',
              width: ScreenUtil().setWidth(60),
              color: Color(0xff363F4D),
              // fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
  void _closeBtnFsDatoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
