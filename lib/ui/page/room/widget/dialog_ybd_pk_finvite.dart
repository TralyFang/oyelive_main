import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/room_socket/message/common/target_ybd_pk_message.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_data.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

class YBDPkFinviteDialog extends StatefulWidget {
  YBDTargetPkMessage message;

  YBDPkFinviteDialog(this.message);

  @override
  YBDPkFinviteDialogState createState() => new YBDPkFinviteDialogState();
}

class YBDPkFinviteDialogState extends BaseState<YBDPkFinviteDialog> {
  bool matched = false;
  bool noDisturb = false;

  bool expired = false;
  // OK按钮
  Widget _okBtn() {
    YBDLiveData data = YBDLiveService.instance.data;
    BuildContext ctx = data.roomContext!;
    var micBloc = ctx.read<YBDMicBloc>();
    return BlocBuilder<YBDMicBloc, YBDMicBlocState>(
      bloc: micBloc,
      builder: (context, state) {
        return YBDDelayGestureDetector(
          onTap: () async {
            if (expired) {
              YBDToastUtil.toast(translate(
                "invite_expired",
              ));
              Navigator.pop(context);
              return;
            }
            int? roomId = YBDLiveService.instance.data.roomId;
            logger.v('clicked ok btn');
            Navigator.pop(context);
            if (null != state.micBeanList) {
              // 1. 如果主播在麦位上且未静音
              if (state.micBeanList!.any((e) => e!.userId == roomId && !e.mute!)) onOkCallBack();

              // 2. 如果主播在麦位且静音提示Please unmute the mic to start!
              if (state.micBeanList!.any((e) => e!.userId == roomId && e.mute!)) {
                YBDToastUtil.toast(translate('pk_match_unmute_mic'));

                return;
              }

              // 3. 如果主播不在麦位上先让主播尝试上麦接受PK邀请
              if (!state.micBeanList!.any((e) => e!.userId == roomId)) {
                int id = YBDCommonUtil.storeFromContext()!.state.bean!.id ?? 0;
                if (YBDLiveService.instance.data != null && id != 0) {
                  YBDMicBlocState micState = micBloc.state;
                  List micList = micState.micBeanList!;
                  int curMicSize = YBDRoomUtil.getMicSize(
                    roomTag: data.roomCategory,
                    roomInfo: ctx.read<YBDRoomBloc>().state.roomInfo,
                  )!; // 当前房间类型最大麦位数
                  List lockedMicList = micList.where((e) => e.lock).toList();
                  logger.v('takeAMic---lockedMicList length:${lockedMicList.length}');

                  if (lockedMicList.length + micState.micIds!.length >= curMicSize) {
                    logger.v('takeAMic---No mic can use!');
                    YBDToastUtil.toast(translate('pk_invite_mic_full')); // 所有麦位都有人或被锁
                    return;
                  } else {
                    for (int i = 0; i < curMicSize; i++) {
                      // 遍历当前房间类型的所有麦位，找到第一个没有人且没有锁的麦位
                      if ((micList[i]?.userId ?? -1) <= 0 && !(micList[i]?.lock ?? false)) {
                        logger.v('takeAMic---roomId:${data.roomId},userId:$id,index:$i');
                        await micBloc.micOperation(data.roomId, 1, id, i); // 上麦
                        // 检查是否已经上麦
                        Future.delayed(Duration(milliseconds: 1000), () {
                          if (state.micBeanList!.any((e) => e!.userId == roomId)) {
                            logger.v('22.1.11---pk invite take mic success');
                            onOkCallBack();
                            return;
                          } else {
                            // 上麦时，声网处理失败
                            logger.v('22.1.11---pk invite take mic fail');
                            YBDToastUtil.toast(translate('pk_invite_net_error'));
                            return;
                          }
                        });
                        break;
                      }
                    }
                  }
                } else {
                  logger.v('takeAMic fail---YBDLiveService.instance.data is null! & id:$id');
                  return;
                }
              }
            }
          },
          child: Container(
            width: ScreenUtil().setWidth(200),
            height: ScreenUtil().setWidth(66),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(34))),
              gradient: LinearGradient(
                colors: [
                  Color(0xff47CDCC),
                  Color(0xff5E94E7),
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            child: Center(
              child: Text(
                "Accept",
                style: TextStyle(color: Colors.white.withOpacity(0.85), fontSize: ScreenUtil().setSp(28)),
              ),
            ),
          ),
        );
      },
    );
  }
  void _okBtn10CbOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  onOkCallBack() {
    YBDRoomSocketApi.getInstance().targetPkOperation(widget.message.content!.target, 2,
        rejectFriends: noDisturb, sourceUserId: widget.message.content!.source, onSuccess: (data) {
      if (data.code == Const.HTTP_SUCCESS) {
        matched = true;
        setState(() {});
        Future.delayed(Duration(seconds: 1), () {
          Navigator.pop(context);
        });
      } else {
        YBDToastUtil.toast(data.desc);
      }
    });
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    return Material(
      color: Colors.black.withOpacity(0.3),
      child: Center(
        child: Container(
          width: ScreenUtil().setWidth(500),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
            color: Colors.white,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: ScreenUtil().setWidth(10),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: ScreenUtil().setWidth(58),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      // matched = !matched;
                      // setState(() {});
                    },
                    child: Image.asset(
                      "assets/images/liveroom/pk/pk_invite_title.webp",
                      width: ScreenUtil().setWidth(320),
                    ),
                  ),
                  Spacer(),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Image.asset(
                          "assets/images/dc/daily_check_close_btn.png",
                          width: ScreenUtil().setWidth(48),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setWidth(60),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(10),
                  )
                ],
              ),
              Text(
                "${widget.message.content!.sourceNickName ?? "Someone"}\n${translate("invite_pk")}",
                style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.black.withOpacity(0.8)),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: ScreenUtil().setWidth(60),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xffFC8787), width: ScreenUtil().setWidth(2)),
                        shape: BoxShape.circle),
                    child: ClipOval(
                      child: YBDNetworkImage(
                        imageUrl: widget.message.content!.sourceHeadImg ?? "",
                        width: ScreenUtil().setWidth(132),
                        height: ScreenUtil().setWidth(132),
                        fit: BoxFit.cover,
                        placeholder: (context, url) {
                          return Image.asset('assets/images/liveroom/pk/pk_default_head.webp');
                        },
                        errorWidget: (context, error, url) => Image.asset('assets/images/liveroom/pk/pk_default_head.webp'),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(22),
                  ),
                  Image.asset(
                    "assets/images/invite_vs.png",
                    width: ScreenUtil().setWidth(84),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(22),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(132),
                    height: ScreenUtil().setWidth(132),
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xff87C3FF), width: ScreenUtil().setWidth(2)),
                        shape: BoxShape.circle),
                    child: Row(
                      children: [
                        AnimatedContainer(
                          width: matched ? 0 : ScreenUtil().setWidth(128),
                          duration: Duration(seconds: 1),
                          padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
                          decoration: BoxDecoration(
                            color: Color(0xffD8D8D8),
                            shape: BoxShape.circle,
                          ),
                          child: Image.asset("assets/images/blue_question_mark.png"),
                        ),
                        if (matched)
                          ClipOval(
                            child: YBDNetworkImage(
                              imageUrl: widget.message.content!.targetHeadImg ?? "",
                              width: ScreenUtil().setWidth(128),
                              height: ScreenUtil().setWidth(128),
                              fit: BoxFit.cover,
                              placeholder: (context, url) {
                                return Image.asset('assets/images/liveroom/pk/pk_default_head.webp');
                              },
                              errorWidget: (context, error, url) => Image.asset('assets/images/liveroom/pk/pk_default_head.webp'),
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: ScreenUtil().setWidth(60),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  GestureDetector(
                    onTap: () {
                      if (expired) {
                        YBDToastUtil.toast(translate(
                          "invite_expired",
                        ));
                        Navigator.pop(context);
                        return;
                      }
                      YBDRoomSocketApi.getInstance().targetPkOperation(widget.message.content!.target, 3,
                          rejectFriends: noDisturb, sourceUserId: widget.message.content!.source);
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: ScreenUtil().setWidth(200),
                      height: ScreenUtil().setWidth(66),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(34))),
                          color: Color(0xffcccccc)),
                      child: Center(
                        child: Text(
                          "Refuse",
                          style: TextStyle(color: Colors.white.withOpacity(0.85), fontSize: ScreenUtil().setSp(28)),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(40),
                  ),
                  _okBtn()
                ],
              ),
              SizedBox(
                height: ScreenUtil().setWidth(24),
              ),
              GestureDetector(
                onTap: () {
                  noDisturb = !noDisturb;
                  setState(() {});
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: ScreenUtil().setWidth(24),
                      height: ScreenUtil().setWidth(24),
                      margin: EdgeInsets.only(bottom: ScreenUtil().setWidth(24), right: ScreenUtil().setWidth(10)),
                      decoration: BoxDecoration(
                          border: Border.all(color: Color(0xffC6C6C6), width: ScreenUtil().setWidth(1)),
                          shape: BoxShape.circle),
                      padding: EdgeInsets.all(ScreenUtil().setWidth(4)),
                      child: Container(
                        width: ScreenUtil().setWidth(24),
                        height: ScreenUtil().setWidth(24),
                        decoration:
                            BoxDecoration(color: noDisturb ? Color(0xff3EC1D3) : Colors.white, shape: BoxShape.circle),
                      ),
                    ),
                    Text(
                      translate("no_disturb"),
                      style: TextStyle(color: Colors.black.withOpacity(0.85), fontSize: ScreenUtil().setSp(20)),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(24),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void myBuildkRBj0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 60), () {
      expired = true;
    });
  }
  void initStatege8dUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDPkFinviteDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetzjENJoyelive(YBDPkFinviteDialog oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
