import 'dart:async';


import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';

enum NormalDialogType {
  one,
  two,
}

/// 普通对话框
class YBDNormalDialog extends StatefulWidget {
  /// 对话框类型
  final NormalDialogType? type;

  /// 点取消按钮的回调
  final VoidCallback? cancelCallback;

  /// 点确认按钮的回调
  final VoidCallback? okCallback;

  ///显示的内容
  final String? content;

  ///显示的标题
  final String? title;

  ///
  final String? ok;
  final String? cancel;
  final bool showBorder;
  final double height;

  Duration? autoConfirmDuration;

  YBDNormalDialog({
    this.cancelCallback,
    this.okCallback,
    this.type = NormalDialogType.two,
    this.content,
    this.title,
    this.ok = 'OK',
    this.cancel = 'Cancel',
    this.autoConfirmDuration,
    this.showBorder = true,
    this.height = 0.0,
  });

  @override
  State<StatefulWidget> createState() => _YBDNormalDialogState();
}

class _YBDNormalDialogState extends State<YBDNormalDialog> {
  /// 每行高度
  final double itemHeight = 45;

  ///每行长度
  final double lineSize = 24;

  Timer? _autoConfirmTimer;
  final interval = Duration(seconds: 1);
  Duration countDown = Duration(seconds: 1);
  setAutoConfirm() {
    if (widget.autoConfirmDuration != null) {
      print("setAuto");
      countDown = widget.autoConfirmDuration!;
      _autoConfirmTimer = Timer.periodic(interval, (timer) {
        if (countDown> Duration.zero) {
          countDown -= interval;
        } else {
          _autoConfirmTimer?.cancel();
          _autoConfirmTimer = null;
          widget.okCallback?.call();
        }
        setState(() {});
      });
    }
  }

  @override
  void initState() {
    super.initState();
    setAutoConfirm();
    print('widget.type:${widget.type}');
  }
  void initStateCliLXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    print('widget.content.length: ${widget.content!.length}');
    return GestureDetector(
      onTap: () {
        //
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {},
                child: Container(
                  width: ScreenUtil().setWidth(500),
                  decoration: BoxDecoration(
                      border: widget.showBorder
                          ? Border.all(color: Color(0xff47CDCC), width: ScreenUtil().setWidth(3))
                          : null,
                      borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
                      color: Colors.white),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setWidth(10)),
                      // 标题

                      widget.title as bool? ?? null != null
                          ? Container(
                              child: Center(
                                child: Text(
                                  // 对话框标题
                                  widget.title!,
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(28),
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            )
                          : Container(),
                      widget.title as bool? ?? null != null ? SizedBox(height: ScreenUtil().setWidth(10)) : Container(),
                      // 内容
                      Container(
                        height: widget.content!.length > 50 ? ScreenUtil().setWidth(300) : ScreenUtil().setWidth(150),
                        alignment: Alignment.center,
                        child: SingleChildScrollView(
                          child: _content(),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(15)),
                      // 按钮
                      Container(
                        child: Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            widget.type == NormalDialogType.two
                                ? YBDScaleAnimateButton(
                                    onTap: () {
                                      logger.v('clicked cancel btn');
                                      if (null != widget.cancelCallback) {
                                        widget.cancelCallback!();
                                      }
                                    },
                                    child: _bottomBtn(widget.cancel, 0),
                                  )
                                : Container(),
                            widget.type == NormalDialogType.two
                                ? SizedBox(width: ScreenUtil().setWidth(15))
                                : Container(),
                            YBDScaleAnimateButton(
                              onTap: () {
                                logger.v('clicked ok btn');
                                if (null != widget.okCallback) {
                                  widget.okCallback!();
                                }
                              },
                              child: _bottomBtn(widget.ok, 1),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(40)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildF9tYNoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 内容
  Widget _content() {
    return widget.content != null
        ? Container(
            height: ScreenUtil().setWidth(
                widget.content!.length > lineSize ? (widget.content!.length / lineSize * itemHeight) : itemHeight),
            margin: EdgeInsets.fromLTRB(ScreenUtil().setWidth(30), ScreenUtil().setWidth(2), ScreenUtil().setWidth(30),
                ScreenUtil().setWidth(2)),
            alignment: Alignment.center,
            child: Center(
              child: Text(
                // 对话框标题
                widget.content!,
                textAlign: TextAlign.center,
                maxLines: int.parse(YBDCommonUtil.formatDoubleToint(widget.content!.length / lineSize)) > 0
                    ? (int.parse(YBDCommonUtil.formatDoubleToint(widget.content!.length / lineSize)) + 6)
                    : 3,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(28),
                  fontWeight: FontWeight.w500,
                  height: 1.5,
                  color: Color.fromRGBO(0, 0, 0, 0.85),
                ),
              ),
            ),
          )
        : Container();
  }
  void _contentrXRTooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部按钮
  Widget _bottomBtn(String? title, int index) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
        gradient: LinearGradient(
          colors: index == 0 ? [Color(0xffCCCCCC), Color(0xffCCCCCC)] : [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      width: ScreenUtil().setWidth(200),
      height: ScreenUtil().setWidth(64),
      alignment: Alignment.center,
      child: Text(
        (title ?? '') + " ${widget.autoConfirmDuration != null ? "in ${countDown.inSeconds}s" : ''}",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(28),
          color: Colors.white,
        ),
      ),
    );
  }
  void _bottomBtnWoc5Qoyelive(String? title, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _autoConfirmTimer?.cancel();
    super.dispose();
  }
}
