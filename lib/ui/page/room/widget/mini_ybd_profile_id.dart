import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// mini profile 用户 id
class YBDMiniProfileId extends StatelessWidget {
  /// 用户 id
  final String? userId;
  bool isUid;
  YBDMiniProfileId(this.userId, this.isUid);

  @override
  Widget build(BuildContext context) {
    bool hasUid = isUid;
    if (hasUid)
      return Stack(
        alignment: Alignment.centerLeft,
        children: [
          Container(
            height: ScreenUtil().setWidth(24),
            padding: EdgeInsets.only(
              right: ScreenUtil().setWidth(4),
            ),
            margin: EdgeInsets.only(left: ScreenUtil().setWidth(2)),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(1)),
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
              gradient: LinearGradient(
                colors: [
                  Color(0xffFBBD1A),
                  Color(0xffFCD323),
                ],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
            ),
            child: Row(
              children: [
                SizedBox(
                  width: ScreenUtil().setWidth(24),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(6),
                ),
                Text(
                  'ID: $userId',
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(16),
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
              ],
            ),
          ),
          Image.asset(
            "assets/images/u_crown.webp",
            width: ScreenUtil().setWidth(24),
            fit: BoxFit.fitWidth,
          ),
        ],
      );
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(10),
        vertical: ScreenUtil().setWidth(3),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(18))),
        gradient: LinearGradient(
          colors: [Color(0xff3792C0), Color(0xff78DFBD)],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
      child: Text(
        'ID: ${userId ?? ''}',
        style: TextStyle(
          fontSize: ScreenUtil().setSp(16),
          height: 1.2,
          color: Colors.white,
        ),
      ),
    );
  }
  void buildsx20soyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
