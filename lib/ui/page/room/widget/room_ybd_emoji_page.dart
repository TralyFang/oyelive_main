
/*
 * @Author: William
 * @Date: 2022-12-21 15:34:36
 * @LastEditTime: 2023-01-12 10:22:06
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/room/widget/room_emoji_page.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/func_ybd_restrictor.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/room/entity/emoji_ybd_entity.dart';
import 'package:oyelive_main/ui/page/room/widget/normal_ybd_dialog.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/colored_ybd_safe_area.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

class YBDEmojiPage extends StatefulWidget {
  const YBDEmojiPage({Key? key}) : super(key: key);

  @override
  State<YBDEmojiPage> createState() => _YBDEmojiPageState();
}

class _YBDEmojiPageState extends State<YBDEmojiPage> with SingleTickerProviderStateMixin {
  TabController? _tabController;
  YBDEmojiEntity? _allEmojis;
  YBDSelfEmoji? _selfEmojis;
  late int _tabLength;

  @override
  void initState() {
    super.initState();
    getEmoji().then((_) {
      _tabLength = _allEmojis?.record?.length ?? 1;
      _tabController = TabController(length: _tabLength, vsync: this);
    });
  }
  void initStateX4ZgVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<void> getEmoji() async {
    _allEmojis = await YBDSPUtil.getEmojiEntity();
    await ApiHelper.getSelfEmoji();
    _selfEmojis = await YBDSPUtil.getMyEmoji();
    if (mounted) setState(() {});
    logger.v('22.12.22---emojiIds:${_selfEmojis!.record?.emojiIds}--_allEmojis:${_allEmojis?.toJson()}');
  }
  void getEmojiTizSFoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return YBDColoredSafeArea(
      child: Container(
        height: 540.px,
        decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.84),
          borderRadius: BorderRadius.only(topLeft: Radius.circular(32.px), topRight: Radius.circular(32.px)),
        ),
        child: Column(
          children: [
            Expanded(
              // tab 内容页面
              child: _allEmojis?.record == null
                  ? Container(
                      height: 470.px,
                      decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.84),
                        borderRadius:
                            BorderRadius.only(topLeft: Radius.circular(32.px), topRight: Radius.circular(32.px)),
                      ),
                    )
                  : TabBarView(controller: _tabController, children: _getTabView()),
              // child: Container(color: Colors.pink),
            ),
            _rowDivider(),
            _topTabView(),
          ],
        ),
      ),
    );
  }
  void buildzCBzboyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // TEST DATA
  List<Widget> testTabs = [
    Tab(
      icon: Container(
        width: 120.px,
        height: 70.px,
        child: Center(child: Container(width: 52.px, height: 52.px, color: Colors.yellow)),
      ),
    ),
    Tab(
      icon: Container(
        width: 120.px,
        height: 70.px,
        child: Center(child: Container(width: 52.px, height: 52.px, color: Colors.transparent)),
      ),
    ),
  ];
  // List<Widget> testTabView = [_gridView(), _gridView1()];

  /// tab 选项卡
  Widget _topTabView() {
    if (_allEmojis?.record == null) return Container(height: 70.px, color: Colors.black);
    logger.v('22.12.26---_allEmojis.record:${_allEmojis!.record}');
    return Container(
      height: 70.px,
      // width: 240.px,
      // color: Colors.amber,
      constraints: BoxConstraints(minWidth: 300.px, maxWidth: 720.px),
      // padding: EdgeInsets.only(right: 360.px),
      alignment: Alignment.centerLeft,
      child: TabBar(
        indicator: BoxDecoration(color: Colors.white.withOpacity(0.2)),
        labelPadding: EdgeInsets.symmetric(horizontal: 0),
        controller: _tabController,
        isScrollable: true,
        onTap: (int index) {
          YBDCommonTrack().commonTrack(YBDTAProps(
            location: 'theme',
            module: YBDTAModule.emoji,
            name: _allEmojis!.record![index].name,
          ));
        },
        tabs: List.generate(
            _tabLength,
            (index) => Container(
                  width: 120.px,
                  height: 70.px,
                  child: Center(
                      child: Container(
                    width: 52.px,
                    height: 52.px,
                    child: YBDImage(
                      path: _allEmojis!.record![index].coverImg,
                      fit: BoxFit.cover,
                      placeholder: (a, b) => Container(color: Colors.transparent),
                      errorWidget: (_, a, b) => Container(color: Colors.transparent),
                    ),
                  )),
                )),
      ),
    );
  }
  void _topTabViewLceJOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 分割线
  Divider _rowDivider() {
    return Divider(
      height: 1.px,
      thickness: 1.px,
      color: Color(0xffE6E6E6).withOpacity(0.3),
    );
  }

  List<Widget> _getTabView() {
    logger.v('22.12.22---emojiIds:${_selfEmojis?.record?.emojiIds}');
    if (_allEmojis?.record == null) return [];
    List<int> myEmojiIds = _selfEmojis?.record?.emojiIds ?? [];
    List<Widget> tabViewList = List.generate(
      _tabLength,
      (index) => _gridView(
          _allEmojis!.record![index], !myEmojiIds.contains(YBDNumericUtil.stringToInt(_allEmojis!.record![index].id))),
    );

    return tabViewList;
  }
  void _getTabViewyhVAboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // grid
  Widget _gridView(YBDEmoji emoji, bool lock) {
    if (_allEmojis?.record == null) return Container();
    logger.v('22.12.22---emoji:${emoji.toJson()}');
    return Container(
        padding: EdgeInsets.only(top: 40.px),
        child: GridView.builder(
          shrinkWrap: true,
          itemCount: emoji.emojiDatas.length,
          padding: EdgeInsets.symmetric(horizontal: 5.px, vertical: 10.px),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
            mainAxisSpacing: 30.px,
            crossAxisSpacing: 16.px,
            childAspectRatio: 1.1,
          ),
          itemBuilder: (context, index) {
            return YBDDelayGestureDetector(
                delayDuration: Duration(milliseconds: 2800),
                onTap: () {
                  logger.v('22.12.22---click emoji id:${emoji.id}--emoji id:${emoji.emojiDatas[index].id}');
                  if (lock) {
                    Navigator.pop(context);
                    YBDDialogUtil.showNormalDialog(
                      context,
                      type: NormalDialogType.two,
                      showBorder: false,
                      height: 200.px,
                      okCallback: () {
                        Navigator.pop(Get.context!);
                        YBDNavigatorHelper.navigateTo(Get.context, YBDNavigatorHelper.vip + "/1");
                      },
                      content: YBDCommonUtil.getRoomOperateInfo().emojiRestrictionTips,
                      ok: 'Go to get',
                      cancel: translate('cancel'),
                    );
                  } else {
                    // todo send msg cd
                    YBDFuncRestrictor.getInstance()!.run(YBDRestrictTask(Task_Send_Emoji, () {
                      YBDCommonTrack().commonTrack(YBDTAProps(
                        location: 'emoji_send',
                        module: YBDTAModule.emoji,
                        name: emoji.emojiDatas[index].name,
                      ));
                      YBDRoomSocketApi.getInstance().sendEmojiMessage(
                        content: '\{\"id\":${emoji.id},\"emojiCode\":${emoji.emojiDatas[index].id}\}',
                      );
                    }));
                  }
                },
                child: _item(emoji.emojiDatas[index], lock));
          },
        ));
  }
  void _gridViewz6taLoyelive(YBDEmoji emoji, bool lock) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _item(YBDEmojiData data, bool lock) {
    return Container(
      // width: 180.px,
      // height: 150.px,
      // color: Colors.red,
      child: Stack(alignment: Alignment.center, children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: 110.px,
              height: 110.px,
              child: YBDImage(
                path: data.pic,
                fit: BoxFit.cover,
                errorWidget: (_, a, b) => Container(color: Colors.green),
              ),
            ),
            SizedBox(height: 14.px),
            Text(
              data.name ?? '',
              style: TextStyle(color: Colors.white.withOpacity(0.7), fontSize: 18.sp),
            )
          ],
        ),
        if (lock)
          Positioned(top: 10.px, left: 10.px, child: Image.asset('assets/images/icon_blue_lock.webp', width: 20.px))
      ]),
    );
  }
  void _itemHl6EToyelive(YBDEmojiData data, bool lock) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // grid
  Widget _gridView1() {
    return Container(
        child: GridView.builder(
      shrinkWrap: true,
      itemCount: 12,
      padding: EdgeInsets.symmetric(horizontal: 45.px, vertical: 40.px),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        mainAxisSpacing: 90.px,
        crossAxisSpacing: 60.px,
        // childAspectRatio: 0.7,
      ),
      itemBuilder: (context, index) {
        return Container(width: 90.px, height: 90.px, color: Colors.blue);
      },
    ));
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }
}
