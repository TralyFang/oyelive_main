import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../base/base_ybd_state.dart';

class YBDRoomIconBanner extends StatefulWidget {
  List<String?> gameIcons;

  Function(int index) callBack;

  YBDRoomIconBanner(this.gameIcons, this.callBack);

  @override
  _YBDRoomIconBannerState createState() => _YBDRoomIconBannerState();
}

class _YBDRoomIconBannerState extends BaseState<YBDRoomIconBanner> {
  int _currentPageIndex = 0;
  @override
  Widget myBuild(BuildContext context) {
    return Container(
        width: ScreenUtil().setWidth(100),
        height: ScreenUtil().setWidth(125),
        // color: Colors.pink,
        child: Stack(alignment: Alignment.topCenter, children: [
          Container(
            padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(1)),
            child: PageView.builder(
              itemBuilder: (content, index) {
                return GestureDetector(
                  onTap: () {
                    widget.callBack(index);
                  },
                  child: YBDNetworkImage(
                    // width: ScreenUtil().setWidth(105),
                    width: ScreenUtil().setWidth(100),
                    height: ScreenUtil().setWidth(100),
                    imageUrl: widget.gameIcons[index] ?? '',
                    fit: BoxFit.contain,
                  ),
                );
              },
              onPageChanged: (index) {
                setState(() {
                  _currentPageIndex = index;
                });
              },
              itemCount: widget.gameIcons.length,
              scrollDirection: Axis.horizontal,
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(widget.gameIcons.length, (i) {
                return Container(
                  margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(4)),
                  width: ScreenUtil().setWidth(10),
                  height: ScreenUtil().setWidth(10),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _currentPageIndex == i ? Colors.white : Colors.white.withOpacity(0.2)),
                );
              }).toList(),
            )),
          )
        ]));
  }
  void myBuilditwl8oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
