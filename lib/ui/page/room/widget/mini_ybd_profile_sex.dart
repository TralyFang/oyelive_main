import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

enum SexType {
  Male,
  Female,
  Unknown,
}

/// mini profile 性别标签
class YBDMiniProfileSex extends StatelessWidget {
  /// 用户性别
  final SexType sex;

  /// 用户年龄
  final String age;

  YBDMiniProfileSex(this.sex, this.age);

  @override
  Widget build(BuildContext context) {
    var female = [Color(0xffE37BBC), Color(0xffFF877D)];
    var male = [Color(0xff5E94E7), Color(0xff47CDCC)];

    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(10),
        vertical: ScreenUtil().setWidth(3),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(18))),
        gradient: LinearGradient(
          colors: sex == SexType.Female ? female : male,
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
      child: Row(
        children: <Widget>[
          Image.asset(
            sex == SexType.Female ? 'assets/images/female.png' : 'assets/images/male.png',
            height: ScreenUtil().setWidth(15),
          ),
          SizedBox(width: ScreenUtil().setWidth(2)),
          Text(
            age,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(16),
              height: 1.2,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
  void buildRk1NVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
