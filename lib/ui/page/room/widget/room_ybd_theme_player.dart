import 'dart:async';


import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';

class YBDRoomThemePlayer extends StatefulWidget {
  final String? resUrl;
  final String? assetsName;
  final File? file;
  final BoxFit? boxFit;

  YBDRoomThemePlayer({
    Key? key,
    this.resUrl,
    this.assetsName,
    this.file,
    this.boxFit,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _YBDRoomThemePlayerState();
  }
}

class _YBDRoomThemePlayerState extends State<YBDRoomThemePlayer> with SingleTickerProviderStateMixin {
  SVGAAnimationController? animationController;

  @override
  void initState() {
    super.initState();
    this.animationController = SVGAAnimationController(vsync: this);
    late Future decode;
    if (widget.resUrl != null) {
      decode = SVGAParser.shared.decodeFromURL(widget.resUrl!);
    } else if (widget.assetsName != null) {
      decode = SVGAParser.shared.decodeFromAssets(widget.assetsName!);
    } else if (widget.file != null) {
      decode = widget.file!.readAsBytes().then((Uint8List bytes) {
        return SVGAParser.shared.decodeFromBuffer(bytes);
      });
    }
    decode.then((videoItem) {
      if (mounted) {
        /*
        this.animationController
          ..videoItem = videoItem
          ..repeat();

         */
        animationController?.videoItem = videoItem;
        animationController?.reset();
      }
    });
  }
  void initStatey2iDqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return SVGAImage(
      this.animationController!,
      fit: widget.boxFit ?? BoxFit.cover,
    );
  }
  void buildZAtDOoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    this.animationController!.videoItem = null;
    this.animationController!.dispose();
    super.dispose();
  }
  void disposehPTzSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
