import 'dart:async';


import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import 'package:oyelive_main/common/ext/enum_ext/ui_ybd_ext.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/level_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/room/widget/gift_ybd_receiver_row.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_gift_category_row.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/local_data/room_ybd_gift_data.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/room/entity/gift_ybd_package_entity.dart';
import '../../../../module/room/entity/my_ybd_gift_entity.dart';
import '../../../../module/room/room_ybd_api_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/grid_ybd_pager.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../bloc/combo_ybd_bloc.dart';
import '../bloc/gift_ybd_sheet_bloc.dart';
import '../util/room_ybd_util.dart';
import 'gift_ybd_receiver.dart';
import 'item_ybd_gift.dart';

class YBDGiftSheet extends StatefulWidget {
  /// 房间信息
  final YBDRoomInfo? roomInfo;

  /// 主播信息
  final YBDUserInfo? talentInfo;

  final List<YBDReceiverUser>? specUsers;
  final Function? onSend;

  /// 显示背包
  final bool showPackage;

  YBDGiftSheet({
    this.roomInfo,
    this.talentInfo,
    this.specUsers,
    this.showPackage = false,
    this.onSend(YBDGiftPackageRecordGift info)?,
  });

  @override
  YBDGiftSheetState createState() => new YBDGiftSheetState();
}

class YBDGiftSheetState extends BaseState<YBDGiftSheet> with TickerProviderStateMixin {
  YBDGiftPackageEntity? _giftPackageEntity;
  YBDMyGiftEntity? _myGiftEntity;

  /// 背包礼物标签栏的索引号
  var _myGiftIndex = -1;

  List<String> titles = [];
  List<YBDGiftPackageRecord?> giftPages = [];

  /// 房间信息
  YBDRoomInfo? _roomInfo;

  /// 主播信息
  YBDUserInfo? _talentInfo;

  /// 引用bloc，退出页面时要用到
  late YBDRoomComboBloc _roomComboBloc;
  late YBDGiftSheetBloc _giftSheetBloc;

  List<int?> opts = [1, 10, 55, 99, 199, 555, 999];

  int selectingCount = 0;

  List<YBDReceiverUser> userList = [];

  late OverlayEntry _overlay;

  /// 星星动画控制器
  late SVGAAnimationController _animationController;

  Timer? _timer;

  // 可以送礼？
  bool _canSend = true;

  showOpt() {
    _overlay = OverlayEntry(builder: (_) {
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              _overlay.remove();
            },
            child: Container(
              color: Color(0x33000000),
            ),
          ),
          Positioned(
              bottom: ScreenUtil().setWidth(98),
              left: ScreenUtil().setWidth(300),
              child: Material(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(4))),
                child: GestureDetector(
                    onTap: () {
                      _overlay.remove();
                    },
                    child: Container(
                      width: ScreenUtil().setWidth(120),
                      height: ScreenUtil().setWidth(360),
                      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(14)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(4))),
                          gradient: LinearGradient(colors: [
                            Color(0xff47CDCC),
                            Color(0xff5E94E7),
                          ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
                      child: Column(
                        children: List.generate(
                            opts.length,
                            (index) => Expanded(
                                    child: TextButton(
                                  style: ButtonStyle(
                                    padding: MaterialStateProperty.all(EdgeInsets.zero),
                                    overlayColor: MaterialStateColor.resolveWith((states) => Color(0x4cffffff)),
                                  ),
                                  onPressed: () {
                                    selectingCount = index;
                                    context.read<YBDGiftSheetBloc>().add(
                                        YBDGiftPackageState(SheetAction.ChangeMultiple, multiple: opts[selectingCount]));
                                    setState(() {});
                                    Future.delayed(Duration(milliseconds: 500), () {
                                      _overlay.remove();
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(),
                                    alignment: Alignment.center,
                                    child: Text(
                                      opts[index].toString(),
                                      style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                                    ),
                                  ),
                                ))),
                      ),
                    )),
              )),
        ],
      );
    });

    Overlay.of(context)!.insert(_overlay);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _roomComboBloc = BlocProvider.of<YBDRoomComboBloc>(context);
    _giftSheetBloc = BlocProvider.of<YBDGiftSheetBloc>(context);
  }
  void didChangeDependenciesl9GbEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: IntlMAAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                    YBDEventName.CLICK_EVENT,
                    location: YBDLocationName.ROOM_PAGE,
                    itemName: YBDItemName.SHEET_WEEKLY_STAR,
                    value: 'Weekly Star',
                  ));
                  Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
                  String weeklyArUrl = store.state.configs?.weeklyArUrl ?? Const.ROOM_WEEKLY_STAR;
                  YBDNavigatorHelper.navigateToWeb(
                    context,
                    "?url=${Uri.encodeComponent(weeklyArUrl)}&shareName=${'Weekly Star'}&title=${'Weekly Star'}",
                  );
                },
                child: Stack(
                  children: [
                    Image.asset('assets/images/weekly_star_bg.webp', height: ScreenUtil().setWidth(88)),
                    Positioned(
                      top: -ScreenUtil().setWidth(9),
                      left: ScreenUtil().setWidth(9),
                      child: Container(
                          width: ScreenUtil().setWidth(100),
                          height: ScreenUtil().setWidth(100),
                          child: SVGAImage(_animationController)),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: ScreenUtil().setWidth(3)),
          Container(
            padding: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight),
            width: ScreenUtil().setWidth(720),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(8))),
              color: Color(0xb2000000),
            ),
            child: BlocBuilder<YBDGiftSheetBloc, YBDGiftPackageState>(
              bloc: context.read<YBDGiftSheetBloc>(),
              builder: (_, YBDGiftPackageState state) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: ScreenUtil().setWidth(24),
                    ),
                    // 显示选择要送礼的人row
                    YBDGiftReceiverRow(specUsers: widget.specUsers),
                    // 礼物栏
                    giftPages.length == 0
                        ? YBDLoadingCircle(height: 542)
                        : Column(
                            children: [
                              YBDRoomGiftCategoryRow(giftPages: giftPages),
                              SizedBox(
                                height: ScreenUtil().setWidth(20),
                              ),
                              (giftPages.length < state.viewingTabIndex! ||
                                      null == giftPages[state.viewingTabIndex!]!.gift)
                                  ? YBDLoadingCircle(height: 436)
                                  : YBDGridPager(
                                      itemWidgets: List.generate(
                                          giftPages[state.viewingTabIndex!]!.gift!.length,
                                          (index) => GestureDetector(
                                              onTap: () {
                                                context.read<YBDGiftSheetBloc>().add(YBDGiftPackageState(
                                                    SheetAction.SelectGift,
                                                    selectingGiftIndex:
                                                        state.selectingGiftIndex != index ? index : null,
                                                    giftInfo: state.selectingGiftIndex != index
                                                        ? giftPages[state.viewingTabIndex!]!.gift![index]
                                                        : null));
                                              },
                                              child: YBDGiftItem(
                                                state.selectingGiftIndex == index,
                                                giftPages[state.viewingTabIndex!]!.gift![index],
                                                isMyGift: giftPages[state.viewingTabIndex!]!.name == "Backpack",
                                              ))),
                                      blc: context.read<YBDGiftSheetBloc>(),
                                    ),
                              SizedBox(
                                height: ScreenUtil().setWidth(16),
                              ),
                            ],
                          ),
                    Container(
                      height: ScreenUtil().setWidth(0.5),
                      color: Color(0xff47CDCC),
                    ),
                    // 底部操作栏
                    Container(
                      height: ScreenUtil().setWidth(86),
                      child: Row(
                        children: [
                          Container(
                            width: ScreenUtil().setWidth(292),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'assets/images/tiny_bean.webp',
                                  width: ScreenUtil().setWidth(28),
                                ),
                                SizedBox(
                                  width: ScreenUtil().setWidth(24),
                                ),
                                Text(
                                  YBDNumericUtil.format(store!.state.bean!.money)!,
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(28),
                                      color: Color(0xffFFD87A),
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                          Container(
                              width: ScreenUtil().setWidth(0.5),
                              decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xff47CDCC),
                                  Color(0xff5E94E7),
                                ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                              )),
                          GestureDetector(
                            onTap: () {
                              showOpt();
                            },
                            child: Container(
                              width: ScreenUtil().setWidth(134),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(),
                              child: Text(
                                opts[selectingCount].toString(),
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(28), color: Colors.white, fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              showOpt();
                            },
                            child: Icon(
                              Icons.arrow_drop_down,
                              size: ScreenUtil().setWidth(50),
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            width: ScreenUtil().setWidth(50),
                          ),
                          Container(
                              width: ScreenUtil().setWidth(180),
                              height: ScreenUtil().setWidth(64),
                              child: TextButton(
                                  onPressed: () {
                                    if (!_canSend) {
                                      logger.v('stint buy!');
                                      return;
                                    }

                                    if (_giftSheetBloc.checkCanSend(store.state.bean!.money, context)) {
                                      _giftSheetBloc.send(_roomInfo?.id, store, context: context);
                                      try {
                                        // 从 miniProfile 弹出礼物弹框报错: Looking up a deactivated widget's ancestor is unsafe
                                        widget.onSend?.call(_giftSheetBloc.state.giftInfo);
                                      } catch (e) {
                                        logger.v('===send gift onSend error $e');
                                        YBDRoomUtil.combo = 1;
                                        _roomComboBloc.add(
                                          YBDComboInfo(
                                            comboStatus: ComboStatus.Counting,
                                            info: _giftSheetBloc.state.giftInfo,
                                          ),
                                        );
                                      }
                                      Navigator.pop(context);
                                    }
                                  },
                                  style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.zero)),
                                  child: Container(
                                    width: ScreenUtil().setWidth(180),
                                    height: ScreenUtil().setWidth(64),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(100))),
                                        gradient: LinearGradient(colors: [
                                          Color(0xff47CDCC),
                                          Color(0xff5E94E7),
                                        ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
                                    alignment: Alignment.center,
                                    child: Text(
                                      translate('send'),
                                      style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28)),
                                    ),
                                  )))
                        ],
                      ),
                    )
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
  void myBuild6EuyBoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getGifts() async {
    // 获取取房间礼物数据
    _giftPackageEntity = await YBDRoomGiftData.readRoomGift(context);

    if (null != _giftPackageEntity) {
      giftPages.addAll(_giftPackageEntity!.record!);
      giftPages.removeWhere((element) => element!.name == "All" || element.name == "other");
      setState(() {});
    }

    // 记录背包礼物标签栏的位置
    _myGiftIndex = giftPages.length;

    if (widget.showPackage) {
      context.read<YBDGiftSheetBloc>().add(
            YBDGiftPackageState(
              SheetAction.ChangeTab,
              viewingTabIndex: _myGiftIndex,
            ),
          );
    }

    // 背包礼物占位数据
    giftPages.add(YBDGiftPackageRecord()
      ..index = giftPages.length
      ..name = translate('backpack')
      ..gift = null);

    // 刷新背包礼物
    _queryBackpack();
  }

  /// 查询背包礼物数据
  Future<void> _queryBackpack() async {
    _myGiftEntity = await YBDRoomApiHelper.queryMyGift(context);

    if (_myGiftEntity!.returnCode == Const.HTTP_SUCCESS) {
      final package = YBDGiftPackageRecord()
        ..index = giftPages.length
        ..name = translate('backpack')
        ..gift = _myGiftEntity!.record!.length != 0 ? _myGiftEntity!.record![0]!.itemList : [];

      // 显示背包数据
      giftPages.replaceRange(_myGiftIndex, _myGiftIndex + 1, [package]);
      if (_myGiftEntity!.record != null && _myGiftEntity!.record!.isNotEmpty)
        YBDRoomUtil.roomGuideGiftInfo = _myGiftEntity!.record?.first?.itemList?.first;
      if (mounted) setState(() {});
    }
  }
  void _queryBackpackZj284oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 播放星星动画
  _playStarAnimation() async {
    _animationController = SVGAAnimationController(vsync: this);
    final videoItem = await SVGAParser.shared.decodeFromAssets('assets/images/weekly_star.svga');
    _animationController.videoItem = videoItem;
    // 弹出时先执行一次动画
    _animationController.forward();
    await Future.delayed(Duration(milliseconds: 1500), () async {
      _animationController.reset();
    });
    // 星星4s闪一次
    _timer = Timer.periodic(Duration(milliseconds: 5500), (_) async {
      _animationController.forward();
      await Future.delayed(Duration(milliseconds: 1500), () async {
        _animationController.reset();
      });
    });
  }

  ///获取等级信息
  _getUserLevelList() async {
    logger.v('listner talent _getUserLevelList');
    if (mounted) {
      YBDLevelInfoEntity? levelInfoEntity = await ApiHelper.queryLevel(context);
      if (levelInfoEntity?.returnCode == Const.HTTP_SUCCESS) {
        context.read<YBDGiftSheetBloc>().add(YBDGiftPackageState(SheetAction.Init, levelInfoEntity: levelInfoEntity));
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _roomInfo = widget.roomInfo;
    _talentInfo = widget.talentInfo;

    getGifts();
    _getUserLevelList();
    selectingCount = opts.indexOf(context.read<YBDGiftSheetBloc>().state.multiple);
    _playStarAnimation();
  }
  void initStateE39rsoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _animationController.dispose();
    if (_timer?.isActive ?? false) {
      _timer?.cancel();
    }
    super.dispose();
  }
}
