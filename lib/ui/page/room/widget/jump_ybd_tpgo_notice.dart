import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/go_ybd_tab_event.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../room_ybd_helper.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';

/// 确认离开房间的弹框
class YBDJumpTpgoNoticeDialog extends StatelessWidget {
  Function? onConfirm;

  YBDJumpTpgoNoticeDialog({this.onConfirm});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {},
                child: Container(
                  width: ScreenUtil().setWidth(500),
                  height: ScreenUtil().setWidth(300),
                  decoration: BoxDecoration(
                      // border: Border.all(
                      //   color: Color(0xff47CDCC),
                      //   width: ScreenUtil().setWidth(3),
                      // ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(ScreenUtil().setWidth(32)),
                      ),
                      color: Colors.white),
                  child: Column(
                    children: <Widget>[
                      Expanded(child: _content()),
                      _actionButtons(context),
                      SizedBox(height: ScreenUtil().setWidth(40)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildgO42Aoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消和确定按钮
  Widget _actionButtons(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        YBDScaleAnimateButton(
          onTap: () {
            logger.v('clicked cancel btn');
            Navigator.pop(context);
          },
          child: _bottomBtn(translate('cancel'), 0),
        ),
        SizedBox(width: ScreenUtil().setWidth(15)),
        YBDScaleAnimateButton(
          onTap: () {
            logger.v('clicked ok btn');
            if (onConfirm != null) {
              onConfirm!.call();
              return;
            }
            YBDNavigatorHelper.popUntilMain(context);
            eventBus.fire(YBDGoTabEvent(1, subIndex: 0));
            // YBDNavigatorHelper.navigateTo(context, url);
            // BlocProvider.of<YBDRoomBloc>(context).exitRoom();
          },
          child: _bottomBtn(translate('ok'), 1),
        ),
      ],
    );
  }
  void _actionButtonsJPxQ8oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 内容
  Widget _content() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(12)),
      child: Center(
        child: Text(
          // 对话框标题
          "You need to exit Live room if you want to jump to TP-GO game. Ready to jump?",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            fontWeight: FontWeight.w400,
            // height: 1.5,
            color: Color.fromRGBO(0, 0, 0, 0.85),
          ),
        ),
      ),
    );
  }
  void _contentHj35ooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部按钮
  Widget _bottomBtn(String title, int index) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
        gradient: LinearGradient(
          colors: index == 0 ? [Color(0xffCCCCCC), Color(0xffCCCCCC)] : [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      width: ScreenUtil().setWidth(200),
      height: ScreenUtil().setWidth(64),
      alignment: Alignment.center,
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(28),
          color: Colors.white,
        ),
      ),
    );
  }
  void _bottomBtnytdehoyelive(String title, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
