import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/widget/intl_ybd_positioned.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/room/entity/gift_ybd_package_entity.dart';
import '../bloc/combo_ybd_bloc.dart';

class YBDGiftFlyOut extends StatefulWidget {
  @override
  YBDGiftFlyOutState createState() => new YBDGiftFlyOutState();
}

class YBDGiftFlyOutState extends BaseState<YBDGiftFlyOut> with SingleTickerProviderStateMixin {
  Animation? xAnimation, yAnimation;
  late AnimationController animationController;
  bool counting = false;
  YBDGiftPackageRecordGift? gift;

  //渐变
  double getOpacity() {
    var result = (200 - (xAnimation?.value ?? 0.0)) / 200;
    if (result < 0) return 0.0;
    if (result > 1) return 1.0;
    return result;
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    if (gift == null || !counting) {
      return Container();
    }
    return YBDIntlPositioned(
      right: ScreenUtil().setWidth(70 + (xAnimation?.value ?? 0.0)),
      bottom: ScreenUtil().setWidth(160 + (yAnimation?.value ?? 0.0)),
      child: Opacity(
        opacity: getOpacity(),
        child: YBDNetworkImage(
          imageUrl: YBDImageUtil.gift(context, gift?.img ?? '', "A"),
          width: ScreenUtil().setWidth(50),
        ),
      ),
    );
  }
  void myBuildElZa6oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  runAnimation() {
    animationController.reset();
    animationController.forward();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 1200));
    //x轴为曲线
    xAnimation =
        Tween<double>(begin: 0, end: 200).chain(CurveTween(curve: Curves.easeInCubic)).animate(animationController);
    yAnimation = Tween<double>(begin: 0, end: 200).animate(animationController);
    animationController.forward();
    animationController.addListener(() {
      if (mounted) setState(() {});
    });
    animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        // dispose();
      }
    });
    context.read<YBDRoomComboBloc>().stream.listen((YBDComboInfo status) {
      logger.v("fktout${status.comboStatus.toString()}");
      if (status != null) if (status.comboStatus == ComboStatus.Counting) {
        counting = true;
        logger.v("ssdxxsxx${status.info!.img}");
        gift = status.info;
        runAnimation();
      } else {
        counting = false;
        if (mounted) setState(() {});
      }
    });
  }
  void initStateb5fWcoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    animationController.dispose();
    logger.v('dispose');
    super.dispose();
  }
  void disposePjLYxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDGiftFlyOut oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesMZ3VQoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
