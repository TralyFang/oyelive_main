import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../bloc/combo_ybd_bloc.dart';
import '../util/room_ybd_util.dart';

class YBDGiftButton extends StatefulWidget {
  @override
  YBDGiftButtonState createState() => new YBDGiftButtonState();
}

class YBDGiftButtonState extends BaseState<YBDGiftButton> with TickerProviderStateMixin {
  final Tween downTween = new Tween<double>(begin: 1, end: 0.8);
  final Tween upTween = new Tween<double>(begin: 1.1, end: 1);
  final Tween progressTween = new Tween<double>(begin: 1, end: 0);

  AnimationController? downController;
  AnimationController? upController;
  AnimationController? progressController;

  late Animation<double> downAnimate;
  late Animation<double> upAnimate;
  late Animation<double> progressAnimate;

  bool isDown = false;

  ComboStatus _comboStatus = ComboStatus.None;

  late SVGAAnimationController _animationController;

  //按下手指的动画
  forward() {
    isDown = true;

    downController!.forward();
    upController!.reset();
  }

  //抬起手指动画
  backward() {
    isDown = false;
    upController!.forward();
    downController!.reset();
    progressController!.reset();
    progressController!.forward();
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    if (_comboStatus == ComboStatus.None) return Container();

    return Transform.scale(
      scale: isDown ? downAnimate.value : upAnimate.value,
      origin: Offset.zero,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: ScreenUtil().setWidth(80),
            height: ScreenUtil().setWidth(80),
            child: CircularProgressIndicator(
              strokeWidth: ScreenUtil().setWidth(4),
              value: progressAnimate.value,
              valueColor:
                  AlwaysStoppedAnimation<Color>(progressAnimate.value > 0.25 ? Colors.white : Color(0xfff44784)),
            ),
          ),
          GestureDetector(
            onTapDown: (x) {
              logger.v("xdsdsd");

              forward();
            },
            onTapUp: (x) {
              // backward();
              YBDRoomUtil.sendCombo(context);
              context.read<YBDRoomComboBloc>().add(YBDComboInfo(comboStatus: ComboStatus.Counting));
            },
            child: Container(width: 80.px, height: 80.px, child: SVGAImage(_animationController)),
          ),
        ],
      ),
    );
  }
  void myBuildEIx9woyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    downController = new AnimationController(duration: const Duration(milliseconds: 500), vsync: this);
    downAnimate = downTween.animate(downController!) as Animation<double>;

    upController = new AnimationController(duration: const Duration(milliseconds: 500), vsync: this);
    upAnimate = upTween.animate(upController!) as Animation<double>;

    progressController = new AnimationController(duration: const Duration(milliseconds: 3000), vsync: this);
    progressAnimate = progressTween.animate(progressController!) as Animation<double>;

    downController!.addListener(() {
      logger.v(downController!.value);
      if (mounted) setState(() {});
    });

    upController!.addListener(() {
      logger.v(upController!.value);
      if (mounted) setState(() {});
    });

    progressController!.addListener(() {
      // logger.v(upController.value);
      if (mounted) setState(() {});
    });

    progressController!.addStatusListener((s) {
      if (s == AnimationStatus.completed) {
        context.read<YBDRoomComboBloc>().add(YBDComboInfo());
        _comboStatus = ComboStatus.None;
      } else {
        _comboStatus = ComboStatus.Counting;
      }
      if (mounted) setState(() {});
    });
    _playGiftAnimation();
  }
  void initStateFMHdPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  onAddSubscription() {
    addSub(context.read<YBDRoomComboBloc>().stream.listen((YBDComboInfo status) {
      logger.v("rrrrrxxxxx${status.toString()}");
      if (status != null) if (status.comboStatus == ComboStatus.Counting) {
        backward();
      } else {
        downController!.reset();
        upController!.reset();
      }
    }));
  }

  @override
  void dispose() {
    logger.v('dispose');
    downController?.dispose();
    upController?.dispose();
    progressController?.dispose();
    _animationController.dispose();
    super.dispose();
  }
  void disposefzSZqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 播放礼物动画
  void _playGiftAnimation() async {
    _animationController = SVGAAnimationController(vsync: this);
    final videoItem = await SVGAParser.shared.decodeFromAssets('assets/images/liveroom/live_gift_click.svga');
    _animationController.videoItem = videoItem;
    _animationController.repeat();
  }
  void _playGiftAnimationwcI1Goyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDGiftButton oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
