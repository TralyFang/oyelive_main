import 'dart:async';


import 'package:flutter/material.dart';
import '../../../common/constant/const.dart';
import '../../../common/http/http_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../module/api.dart';
import '../../../module/room/entity/gift_ybd_package_entity.dart';
import '../../../module/room/entity/my_ybd_gift_entity.dart';
import '../../../module/user/util/user_ybd_util.dart';
import '../home/entity/banner_ybd_advertise_entity.dart';
import 'room_ybd_api.dart';

class YBDRoomApiHelper {
  static Future<YBDGiftPackageEntity?> queryGiftPackage(BuildContext context) async {
    YBDGiftPackageEntity? giftPackageEntity =
        await YBDHttpUtil.getInstance().doGet<YBDGiftPackageEntity>(context, YBDRoomApi.QUERY_GIFT_PACKAGE);
    return giftPackageEntity;
  }

  static Future<YBDMyGiftEntity?> queryMyGift(BuildContext? context) async {
    YBDMyGiftEntity? giftPackageEntity = await YBDHttpUtil.getInstance().doGet<YBDMyGiftEntity>(context, YBDApi.QUERY_PERSONAL,
        params: {'queryType': 2, 'goodsType': 'GIFT', 'userId': await YBDUserUtil.userId()});
    return giftPackageEntity;
  }

  /// 房间活动Slider 通过API实现
  static Future<YBDBannerAdvertiseEntity?> queryRoomSliderActivity(BuildContext context, {int adType: 2}) async {
    YBDBannerAdvertiseEntity? advertiseEntity =
        await YBDHttpUtil.getInstance().doGet(context, YBDApi.HOME_BANNER_AD, params: {'adtype': adType});

    if (advertiseEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v("request slider activity success");
      return advertiseEntity;
    } else {
      logger.v("request slider activity failed");
      return null;
    }
  }
}
