
import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:synchronized/synchronized.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_operat_halper.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/room_socket/message/base/message.dart';
import '../../../../common/room_socket/message/common/internal.dart';
import '../../../../common/room_socket/message/common/mic_ybd_request_message.dart';
import '../../../../common/room_socket/message/common/text_ybd_message.dart';
import '../../../../common/room_socket/message/resp/response.dart';
import '../../../../common/room_socket/message/room_ybd_message_helper.dart';
import '../../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/download_ybd_manager.dart';
import '../../../../common/util/download_ybd_util.dart';
import '../../../../common/util/file_ybd_util.dart';
import '../../../../common/util/frame_ybd_animation_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../common/util/zip_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart' as user;
import '../../home/entity/car_ybd_list_entity.dart';
import '../../splash/splash_ybd_util.dart';
import '../../store/entity/downloadbean.dart';
import '../entity/agora_ybd_rtm_info.dart';
import '../entity/emoji_ybd_info.dart';
import '../entity/frame_ybd_Info.dart';
import '../entity/mic_ybd_bean.dart';
import '../entity/mic_ybd_item.dart';
import '../entity/mic_ybd_mute_response_message.dart';
import '../entity/mic_ybd_request_info.dart';
import '../entity/mic_ybd_user_publish.dart';
import '../entity/mini_ybd_game_info.dart';
import '../entity/mute_ybd_Info.dart';
import '../notice/notice_ybd_type.dart';
import '../pk/pk_ybd_rtm_helper.dart';
import '../service/join_ybd_channel_success_event.dart';
import '../service/live_ybd_agora_rtm_service.dart';
import '../service/live_ybd_define.dart';
import '../service/live_ybd_event.dart';
import '../service/live_ybd_service.dart';
import '../service/volume_ybd_event.dart';
import '../util/room_ybd_util.dart';

/// 事件类型
enum MicEvent {
  // 加载数据
  // Init,

  /// 加载刷新页面数据
  Refresh,
}

///麦位最多10个
const int MaxMicIndexSize = 10;

const int CurMicIndexSize = 8;

/// 声音大小达到20才显示效果
const int ShowVolumeTag = 20;

/// 申请上麦 超时阈值设置为5分钟 1000 * 60 * 5
const int MicRequestTagTime = 1000 * 60 * 5;

/// 麦位
class YBDMicBloc extends Bloc<MicEvent, YBDMicBlocState> {
  BuildContext? context;

  /// 房间ID
  int? _roomId;

  /// 当前登录用户信息
  user.YBDUserInfo? _userInfo;

  /// 当前登录用户是否为主播
  late bool _isTalent;

  /// 当前登录用户是否在麦位上
  bool gotMic = false;

  /// 记录当前登录用户麦位下标
  int _runTimeFlag = -1;

  /// 记录当前登录用户是否静音
  bool? _runTimemute = false;

  /// 已上麦的用户列表
  List<int?> _micIds = [];

  // List<int> micIdsTem = [];

  /// 三级以下用户申请上麦的队列
  List<YBDMicRequestInfo> _micRequestlist = [];

  /// 是否有上麦请求
  bool? _hasMicRequest;

//  List<YBDMicRequestInfo> micRequestlist = [];

  List<YBDMicBean?> _micBeans = List.filled(MaxMicIndexSize, null);

  /// 麦位数据
  List<YBDMicBean?> _micBeanList = List.filled(MaxMicIndexSize, null);

  /// 麦位游戏数据（根据下发的更改）
  List<YBDMiniGameInfo?> _micGames = List.filled(MaxMicIndexSize, null);

  /// 麦位emoji数据（根据下发的更改）
  List<YBDEmojiInfo?> _micEmoji = List.filled(MaxMicIndexSize, null);

  /// 麦位游戏数据（缓存）
//  List<YBDMiniGameInfo> _micGameList = List(maxMicIndexSize);

  List<YBDDownLoadBean> _downLoadList = [];

  // List<YBDMicUserPublish> micUserPublishList = [];

  // int unzipTimes = 0;
  // int unzipMaxTimes = 3;

  /// 麦位数据处理转换
  bool? _micOnline;
  bool? _lock;
  bool? _mute;
  int? _frameType;
  String? _frameUrl;
  String? _frameSvgLocalPath;
  bool? _game;
  int? _userId;
  int? _micIndex;
  String? _img;
  int? _volume;
  String? _nikName;
  String? _vipIcon;

  // int curMicIndexSize = 8;

  Timer? _micRequestTime;

  // double _volumeOpacity;

  /// 用户是否在播放麦位动画，默认没有播放
  bool _isPlayMicGame = false;

  /// 订阅麦位socket消息
  StreamSubscription<YBDMessage>? _micMsgSubscription;

  /// 订阅直播事件
  StreamSubscription<YBDLiveEvent>? _liveEventSubscription;

//  static YBDMicBloc _instance;
  YBDMicBloc(
    this.context, {
    int? roomIdx,
  }) : super(YBDMicBlocState()) {
    _roomId = roomIdx;
    logger.v('YBDMicBloc constructor roomId: $_roomId');
    _userInfo = YBDCommonUtil.storeFromContext()!.state.bean;

    // 进入房间后[isTalent]值保持不变：主播不会变成观众，观众不能变成主播
    _isTalent = YBDRoomUtil.isTalent(_roomId);

    // 接收socket消息
    streamlisten();

    YBDDownLoadManager.instance!.addCallBacks(
      YBDDownLoadManager.instance!.callBackMic,
      downloadCallback,
    );

    _listenLiveEvent();
  }

  /// 监听直播事件
  _listenLiveEvent() {
    logger.v('joined channel :initEngineLister');
    _liveEventSubscription = YBDLiveService.instance.onEvent.listen((YBDLiveEvent event) {
      if (event is YBDJoinChannelSuccessEvent) {
        logger.v('joined channel : ${event.channel}, ${event.uid}, ${event.elapsed}');
        YBDLiveService.instance.setClientRole(LiveRole.Audience);
      } else if (event is YBDVolumeEvent) {
        updateVolume(event.speakers);
      }
    });
  }

  /* static YBDMicBloc getInstance(BuildContext context, {int roomIdx}) {
    if (_instance == null) {
      _instance = new YBDMicBloc(context,roomIdx:roomIdx);
    }
    return _instance;
  }*/

  ///下载进度回调
  Function? downloadCallback(YBDDownloadTaskInfo event) {
    logger.v('downloadCallback callBackMic updateProgress');
    if (event == null) {
      logger.v('downloadCallback callBackMic task info arg is null');
      return null;
    }

    logger.v('downloadCallback----');

    for (int i = 0; i < _downLoadList.length; i++) {
      logger.v('downloadCallback i:$i taskId:${_downLoadList[i].taskId}');

      if (_downLoadList[i].taskId != null && _downLoadList[i].taskId!.compareTo(event.id!) == 0) {
        if (event.status == DownloadTaskStatus.complete) {
          logger.v('downloadCallback----complete taskID:${event.id}');
          if (_downLoadList[i].type == DownLoadType.svg_frame) {
            //          _unzipSvga(i);  边框 svga没有压缩  直接下载文件
            _setFrameSvgPath(i);
          } else if (_downLoadList[i].type == DownLoadType.svg_emoji) {
            _unzipSvga(i); //emoji svga有压缩
          }
        } else if (event.status == DownloadTaskStatus.failed) {
          logger.v('downloadCallback----failed taskID:${event.id}');
        } else if (event.status == DownloadTaskStatus.running) {
          double progress = YBDCommonUtil.formatDoubleNum(event.progress, 2);
          logger.v('downloadCallback taskID:${event.id}  progress :$progress  event.progress:$event.progress');
        } else {}
        break;
      }
    }

    return null;
  }

  /// 解压emoji动画文件
  _unzipSvga(int unzipIndex) async {
    if (unzipIndex >= 0 && _downLoadList != null && _downLoadList.length > unzipIndex) {
//      String path =
//          'Animation/${downLoadList[unzipIndex].foldername}/${downLoadList[unzipIndex].version}';
      String path = await YBDCommonUtil.getResourceDir(_downLoadList[unzipIndex].savePath);
      String pathZip = path + '/' + _downLoadList[unzipIndex].fileName!;
      logger.v('downloadCallback path:$path pathZip:$pathZip');
      bool unzip = await YBDZipUtil.asyncUnZip(path, pathZip);
      logger.v('downloadCallback unzip :$unzip taskId:${_downLoadList[unzipIndex].taskId} pathZip:$pathZip');

      ///解压完通知页面  状态修改
      if (unzip) {
        List path = await YBDFileUtil.getSvgPath(_downLoadList[unzipIndex]);
        _updateEmojiSvgaUrl(_downLoadList[unzipIndex].fileUrl, path[0]);
      }
    }
  }

  ///设置svg_frame 本地地址
  _setFrameSvgPath(int unzipIndex) async {
    if (unzipIndex >= 0 && _downLoadList != null && _downLoadList.length > unzipIndex) {
      List path = await YBDFileUtil.getSvgPath(_downLoadList[unzipIndex]);
      logger.v(
          '_setSvgPath path:${path[0]} savePath:${_downLoadList[unzipIndex].savePath} fileName:${_downLoadList[unzipIndex].fileName}');
      if (path == null) {
        return;
      }
      /* for (int i = 0; i < _micBeans.length; i++) {
        logger.v('_setSvgPath frameUrl :${_micBeans[i].frameUrl} fileUrl:${downLoadList[unzipIndex].fileUrl}');
        if (_micBeans[i].frameUrl == downLoadList[unzipIndex].fileUrl) {
          _micBeans[i].frameSvgLocalPath = path[0];
          break;
        }
      }
      add(MicEvent.Refresh);*/
      _updateFrameSvgaUrl(_downLoadList[unzipIndex].fileUrl, path[0]);
      _downLoadList.removeAt(unzipIndex);
    }
  }

  ///更新svg_frame 本地地址
  _updateFrameSvgaUrl(String? fileUrl, String? localUrl) {
    for (int i = 0; i < _micBeans.length; i++) {
      logger.v('_setSvgPath frameUrl :${_micBeans[i]!.frameUrl} fileUrl:${fileUrl}');
      if (_micBeans[i]!.frameUrl == fileUrl) {
        logger.v('_setSvgPath frameSvgLocalPath :${_micBeans[i]!.frameSvgLocalPath} localUrl:${localUrl}');
        if (_micBeans[i]!.frameSvgLocalPath == localUrl) {
          break;
        }
        _micBeans[i]!.frameSvgLocalPath = localUrl;
        break;
      }
    }
    logger.v("_updateFrameSvgaUrl MicEvent.Refresh");
    add(MicEvent.Refresh);
  }

  ///设置svg_emoji 本地地址
  _setEmojiSvgPath(int unzipIndex) async {
    if (unzipIndex >= 0 && _downLoadList != null && _downLoadList.length > unzipIndex) {
      List path = await YBDFileUtil.getSvgPath(_downLoadList[unzipIndex]);
      logger.v(
          '_setSvgPath path:${path[0]} savePath:${_downLoadList[unzipIndex].savePath} fileName:${_downLoadList[unzipIndex].fileName}');
      if (path == null) {
        return;
      }

      _updateFrameSvgaUrl(_downLoadList[unzipIndex].fileUrl, path[0]);
      _downLoadList.removeAt(unzipIndex);
    }
  }

  ///设置svg_emoji 本地地址
  _updateEmojiSvgaUrl(String? fileUrl, String? localUrl) {
    for (int i = 0; i < _micBeans.length; i++) {
      logger.v('_updateEmojiSvgaUrl emoji_Url :${_micBeans[i]?.emojiInfo?.emojiPath} fileUrl:${fileUrl}');
      if (_micBeans[i]?.emojiInfo?.emojiPath == fileUrl) {
        logger.v(
            '_updateEmojiSvgaUrl emojiSvgLocalPath :${_micBeans[i]?.emojiInfo?.emojiLocalPath ?? ''} localUrl:${localUrl}');
        if (_micBeans[i]?.emojiInfo?.emojiLocalPath == localUrl) {
          break;
        }
        _micBeans[i]?.emojiInfo?.emojiLocalPath = localUrl;
        break;
      }
    }
    logger.v("_updateEmojiSvgaUrl MicEvent.Refresh");
    add(MicEvent.Refresh);
  }

  _getUserInfo() {
    _userInfo = YBDCommonUtil.storeFromContext()!.state.bean;
    micOperation(_roomId, 2, _userInfo!.id, _runTimeFlag);
  }

  updateVolume(List<AudioVolumeInfo>? speakers) {
//    logger.v('updateVolume micIds:${micIds.toString()}');
//    logger.v('updateVolume speakers.length :${speakers.length}');
    bool hasVolume = false;
    bool volumeChange = false;
    for (int i = 0; i < _micBeans.length; i++) {
      if (_micBeans[i] == null) {
//        logger.v("updateVolume MicEvent.Refresh  _micBeans[i] == null i:$i");
        return;
      }
      hasVolume = false;
      for (int j = 0; j < speakers!.length; j++) {
        // logger.v("updateVolume uid $j: ${speakers[j].uid}, channelId: ${speakers[j].channelId}");
        if (speakers[j].uid == 0) {
          speakers[j].uid = _userInfo?.id ?? 0;
        }
        /*  if (!micIds.contains(speakers[j].uid) && speakers[j].uid != null && speakers[j].uid != 0) {
          muteRemote(speakers[j].uid, true);
//          logger.v("updateVolume MicEvent.Refresh  muteRemote[j] == null j:$j");
          break;
        }*/

        if (_micBeans[i]!.userId == speakers[j].uid && speakers[j].volume >= ShowVolumeTag) {
          if (_micBeans[i]!.volume != ShowVolumeTag) {
            _micBeans[i]!.volume = ShowVolumeTag;
//            _micBeans[i]?.volumeOpacity = 0.5;
//            logger.v('i:$i  _micBeans[i]?.volumeOpacity 0.5');
//            logger.v("updateVolume MicEvent.Refresh  showVolumeTag[j] == null i:$i");
            volumeChange = true;
          }
          hasVolume = true;
          break;
        }
      }
      if (!hasVolume) {
        if (_micBeans[i]!.volume != 0) {
          _micBeans[i]?.volume = 0;
          /* if (_micBeans[i]?.volumeOpacity ?? 0 > 0) {
//            logger.v('i:$i  _micBeans[i]?.volumeOpacity 0.5 -0.1');
            // ignore: null_aware_before_operator
            _micBeans[i]?.volumeOpacity = _micBeans[i]?.volumeOpacity - 0.1;
          }*/
          volumeChange = true;
        }
      }
      // logger.v('updateVolume speakers.userId:${_micBeans[i]?.userId} volume :${_micBeans[i]?.volume}');
//      logger.v('i:$i  _micBeans[i]?.volumeOpacity:${_micBeans[i]?.volumeOpacity}');
    }
//    logger.v("updateVolume MicEvent.Refresh  volumeChange:$volumeChange");
    if (volumeChange) {
      add(MicEvent.Refresh);
    }
//    add(MicEvent.Refresh);
  }

  /*setVolume(AudioVolumeInfo audioVolumeInfo) {
    for (int i = 0; i < _micBeans.length; i++) {
      if (_micBeans[i].micOnline && _micBeans[i].userId == audioVolumeInfo.uid) {
        _micBeans[i].volume = audioVolumeInfo.volume;
        muteRemote(audioVolumeInfo.uid, false);
        break;
      }
    }
  }*/

  muteRemote(int uid, bool mute) {
//    logger.v("muteRemote uid: $uid mute:$mute");
    YBDLiveService.instance.muteRemoteAudioStream(uid, mute);
  }

  @override
  Stream<YBDMicBlocState> mapEventToState(MicEvent event) async* {
    logger.v("mic refresh");
    switch (event) {
      case MicEvent.Refresh:
        _micBeanList = _micBeans;
        if (_micRequestlist.length > 0) {
          _hasMicRequest = true;
          requestTimeTask();
        } else {
          _hasMicRequest = false;
          if (_micRequestTime != null) {
            _micRequestTime!.cancel();
            _micRequestTime = null;
          }
        }
        yield YBDMicBlocState(
          gotMic: gotMic,
          runTimeFlag: _runTimeFlag,
          micIds: _micIds,
          isPlayMicGame: _isPlayMicGame,
          hasMicRequest: _hasMicRequest,
          micBeanList: _micBeanList,
          micRequestlist: _micRequestlist,
        );
        break;
      // case MicEvent.Init:
      //   userInfo = YBDCommonUtil.storeFromContext().state.bean;
      //   if (userInfo.id == roomId) {
      //     isTalent = true;
      //   } else {
      //     isTalent = false;
      //   }
      //   break;
    }
  }
  void mapEventToStatexcmv4oyelive(MicEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 拒绝用户上麦的申请
  void rejectMicApply(YBDMicRequestInfo micRequestInfo) {
    _micRequestlist.remove(micRequestInfo);
  }
  void rejectMicApplyUeD9Ooyelive(YBDMicRequestInfo micRequestInfo) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 刷新bloc数据
  void refreshMicBloc(int targetRoomId) {
    _roomId = targetRoomId;
    gotMic = false;
    _runTimeFlag = -1;
    _runTimemute = false;
    _micIds = [];
    _micRequestlist = [];
    _hasMicRequest = false;
    _micBeans = List.filled(MaxMicIndexSize, null);
    _micBeanList = List.filled(MaxMicIndexSize, null);
    _micGames = List.filled(MaxMicIndexSize, null);
    _micEmoji = List.filled(MaxMicIndexSize, null);
    _micOnline = false;
    _lock = false;
    _mute = false;
    _frameType = 0;
    _frameUrl = null;
    _frameSvgLocalPath = '';
    _game = false;
    _userId = 0;
    _micIndex = 0;
    _img = '';
    _volume = 0;
    _nikName = '';
    _micRequestTime?.cancel();
    _isPlayMicGame = false;
    add(MicEvent.Refresh);
  }
  void refreshMicBloca6gCZoyelive(int targetRoomId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  streamlisten() {
    logger.v('mic bloc streamlisten---------');
    _micMsgSubscription = YBDRoomMessageHelper.getInstance().messageOutStream.listen((message) {
      if (message is YBDMicUserPublish) {
        /// 1、 上下麦更新  2、静音与否更新  3、声网频道加入与否更新 声网用户角色更新  4、边框更新
        /// 声网回调  确认有声音的用户是否在线  如果没有在线 mRtcEngine.muteRemoteAudioStream(uid, true);
        ///麦位状态更新
        logger.v('====== testing... received YBDMicUserPublish: ${message.toJson()}, roomId:${message.roomId}');
        if (null != message && message.roomId == _roomId) {
          /* if(micUserPublishList?.length??0>0){
            micUserPublishList.add(message);
          }else{
            micUserPublishList.add(message);
            _updateMic(message.content);
            _updateAgoraStatus();
            add(MicEvent.Refresh);
          }*/
          Lock().synchronized(() {
            logger.v("YBDMicUserPublish MicEvent.Refresh  0001");
            _updateMic(message.content!);
            _updateAgoraStatus();
            logger.v("YBDMicUserPublish MicEvent.Refresh  0002");
            add(MicEvent.Refresh);
          });
        }
      } else if (message is YBDMicMuteResponseMessage) {
        ///被静音用户收到消息 调用声网接口实现静音
        logger.v('====== testing... received YBDMicMuteResponseMessage: ${message.toJson()}');
        if (null != message && message.roomId == _roomId) {
          if (message.type == 1 && message.direction == 1 && _userInfo != null && _userInfo!.id == message.toUser) {
            muteLocalMic(message.index, message.fromUser, message.originSequence, true, 2);
          } else if (message.type == 2 &&
              message.direction == 1 &&
              _userInfo != null &&
              _userInfo!.id == message.toUser) {
            muteLocalMic(message.index, message.fromUser, message.originSequence, false, 2);
          }
        }
      } else if (message is YBDMicRequestMessage) {
        logger.v("onMicRequestMessage  Type: ${message.type}  Index:${message.index}  isTalent:$_isTalent");
        if (message.type == 1 && _isTalent) {
          //  主播收到  3级以下用户 上麦请求 弹框选择 允许或拒绝
          if (_checkMicRequestInfos(message.fromUser)) {
            return;
          }
          user.YBDUserInfo userInfo = new user.YBDUserInfo();
          userInfo.nickname = message.sender;
          userInfo.id = message.fromUser;
          userInfo.level = message.senderLevel;
          userInfo.headimg = message.senderImg;
          userInfo.sex = message.senderSex;
          _micRequestlist.add(
            YBDMicRequestInfo()
              ..userInfo = userInfo
              ..micIndex = message.index
              ..time = YBDDateUtil.currentUtcTimestamp(),
          );
          logger.v("YBDMicRequestMessage MicEvent.Refresh");
          add(MicEvent.Refresh);
        } else if (message.type == 2) {
          logger.v('====== received talent mic invited event');
          // 普通用户  收到主播的邀请上麦请求  弹框选择 允许或拒绝
//          micInviteMessage(message.getToUser(), message.getIndex());
          logger.v(
              // 'received talent mic invited event YBDLiveService.instance.mainContext:${YBDLiveService.instance.mainContext}');
              'received talent mic invited event context:$context');
          if (context != null) {
            YBDDialogUtil.showMicInviteDialog(context, okCallback: () {
              _micRequestSuccess(message.toUser, message.index);
              logger.v('Accept Accept Accept');
            }, content: 'You are invited on mic!', ok: 'Accept', cancel: 'Reject');
          }
        } else if (message.type == 3) {
          // 普通用户  收到主播同意上麦的消息
          _micRequestSuccess(message.toUser, message.index);
        } else if (message.type == 50 || message.type == 51 || message.type == 52) {
          // 主播同意结果反馈
          _micRequestResult(message.type, message);
        }
      } else if (message is YBDTextMessage) {
        //MINI_GAMES  EMOJIES
        List miniGamesPattern = message.content!.split(Const.MINI_GAMES_SPLITTER);
        logger.v('====== testing... received YBDTextMessage: ${miniGamesPattern.toString()}');
        if (miniGamesPattern != null &&
            miniGamesPattern.isNotEmpty &&
            miniGamesPattern[0] == Const.MINI_GAMES_PATTERN) {
          /*  miniGameInfo.setId(Integer.parseInt(miniGamesPattern[1] + ""));
          miniGameInfo.setUserId(textMsg.getFromUser());
          miniGameInfo.setIndex(Integer.parseInt(miniGamesPattern[2] + ""));
          miniGameInfo.setStatus(Integer.parseInt(miniGamesPattern[3] + ""));*/
          logger.v('====== testing... received MINI_GAMES_PATTERN: id:${miniGamesPattern[1]}');
          if (_micIds.contains(message.fromUser)) {
            _showMiniGame(
              YBDMiniGameInfo(
                id: int.parse(miniGamesPattern[1]),
                userId: message.fromUser,
                index: int.parse(miniGamesPattern[2]),
                status: int.parse(miniGamesPattern[3]),
              ),
            );
          }
        } else if (miniGamesPattern != null &&
            miniGamesPattern.isNotEmpty &&
            miniGamesPattern[0] == Const.EMOJIES_PATTERN) {
          /*     EmojiListModel emojieInfo = new EmojiListModel();
          emojieInfo.setEmojiName(miniGamesPattern[1]);
          emojieInfo.setUserId(textMsg.getFromUser());
          emojieInfo.setMicIndex(Integer.parseInt(miniGamesPattern[4] + ""));
          emojieInfo.setVersion(Integer.parseInt(miniGamesPattern[3] + ""));
          emojieInfo.setFrameDuration(Integer.parseInt(miniGamesPattern[5] + ""));
          emojieInfo.setEmojiPath(miniGamesPattern[2]);
          miniGamesListner.emojieMessage(emojieInfo);*/
          logger.v('====== testing... received EMOJIES_PATTERN: time:${DateTime.now().millisecondsSinceEpoch}');

          _showMicEmoji(
            YBDEmojiInfo(
              emojiName: miniGamesPattern[1],
              userId: message.fromUser,
              micIndex: int.parse(miniGamesPattern[4]),
              version: int.parse(miniGamesPattern[3]),
              frameDuration: int.parse(miniGamesPattern[5]),
              emojiPath: YBDImageUtil.emojieZipFile(context, miniGamesPattern[2]),
//              emojiPath: miniGamesPattern[2],
            ),
          );

          download(
            YBDCarListRecordAnimationInfo()
              ..version = 1
              ..foldername = miniGamesPattern[1]
              ..name = miniGamesPattern[2]
              ..animationId = 0,
            YBDImageUtil.emojieZipFile(context, miniGamesPattern[2]),
            DownLoadType.svg_emoji,
          );
        }
      } else if (message is YBDInternal) {
        logger.v('received NoticeMessage TYPE_TALENT_OFFLINE type:${message.type} ');
        if (message.type == YBDNoticeType.TYPE_TALENT_OFFLINE) {
          ///离线房间消息
          _offline();
        } else {
          logger.v(
              'received NoticeMessage TYPE_TALENT_OFFLINE type:${YBDNoticeType.TYPE_TALENT_OFFLINE}  message.type${message.type}');
        }
      } else {
        logger.v('====== testing... received other: ${message.toString()}');
      }
    });

    YBDLiveAgoraRtmService.instance.onEvent.listen((event) {
      if (event is YBDAgoraRtmInfo) {
        logger.v('_listenRtmEvent YBDAgoraRtmInfo pk_id: ${event.pk_id}, pk_state:${event.pk_state} _gotMic:$gotMic');
        YBDPKRTMHelper.updateRtmState(event, gotMic);
      }
    });
  }

  ///离线房间 把麦位锁上
  _offline() {
    logger.v('received NoticeMessage TYPE_TALENT_OFFLINE _offline ${_micBeans.length}');
    for (int i = 0; i < _micBeans.length; i++) {
      _micBeans[i] = YBDMicBean(lock: true);
    }
    add(MicEvent.Refresh);
  }

  ///显示麦位游戏
  _showMiniGame(YBDMiniGameInfo miniGameInfo) {
    for (int i = 0; i < _micBeans.length; i++) {
      if (miniGameInfo.userId == _micBeans[i]!.userId) {
        _micBeans[i]!.game = true;
        _micBeans[i]!.emoji = false;

        ///本地用户麦位
        if (miniGameInfo.userId == _userInfo!.id) {
          _isPlayMicGame = true;
        }
        if (Const.DICE_GAME_ID == miniGameInfo.id) {
          miniGameInfo.images = YBDFrameAnimationUtil.getDiceGameImage(miniGameInfo);
        } else if (Const.FLIP_COIN_GAME_ID == miniGameInfo.id) {
          miniGameInfo.images = YBDFrameAnimationUtil.getFlipCoinGameImage(miniGameInfo);
        } else if (Const.FINGER_CROSS_GAME_ID == miniGameInfo.id) {
          miniGameInfo.images = YBDFrameAnimationUtil.getFingerGameImage(miniGameInfo);
        } else if (Const.FLIP_CIRCLE_GAME_ID == miniGameInfo.id) {
          miniGameInfo.images = YBDFrameAnimationUtil.getFlipCircleEGameImage(miniGameInfo);
        }
        _micGames[i] = miniGameInfo;
        _micBeans[i]!.miniGameInfo = _micGames[i];
      } else {
//        _micBeans[i].game = false;
      }
    }
    logger.v("_showMiniGame MicEvent.Refresh");
    add(MicEvent.Refresh);
  }

  ///隐藏麦位游戏
  hideMiniGame(YBDMiniGameInfo miniGameInfo) {
    logger.v('AnimationStatus.completed ${miniGameInfo.userId ?? 'null'}');
    for (int i = 0; i < _micGames.length; i++) {
      if (_micGames[i] != null && _micGames[i]!.userId == miniGameInfo.userId) {
        _micGames[i] = null;
        _micBeans[i]?.miniGameInfo = null;
        _micBeans[i]?.game = false;

        ///本地用户麦位
        if (miniGameInfo.userId == _userInfo!.id) {
          _isPlayMicGame = false;
        }
        break;
      }
    }
    logger.v("hideMiniGame MicEvent.Refresh");
    add(MicEvent.Refresh);
  }

  ///显示麦位emoji
  _showMicEmoji(YBDEmojiInfo micEmojiInfo) {
    logger.v("_showMicEmoji micEmojiInfo:${micEmojiInfo.toString()}");
    for (int i = 0; i < _micBeans.length; i++) {
      if (micEmojiInfo.userId == _micBeans[i]!.userId) {
        _micBeans[i]!.game = false;
        _micBeans[i]!.emoji = true;
        _micEmoji[i] = micEmojiInfo;
        _micBeans[i]!.emojiInfo = _micEmoji[i];
      } else {
//        _micBeans[i].game = false;
      }
    }
    logger.v("_showMicEmoji MicEvent.Refresh");
    add(MicEvent.Refresh);
  }

  ///隐藏麦位emoji
  hideMicEmoji(YBDEmojiInfo micEmojiInfo) {
    logger.v('MicEmoji.completed ${micEmojiInfo.userId ?? 'null'}');
    for (int i = 0; i < _micBeans.length; i++) {
      if (_micBeans[i] != null && _micBeans[i]!.userId == micEmojiInfo.userId) {
        _micBeans[i]?.emoji = false;
        _micEmoji[i] = null;
        _micBeans[i]?.emojiInfo = null;
        break;
      }
    }
    logger.v("hideMicEmoji MicEvent.Refresh");
    add(MicEvent.Refresh);
  }

  ///普通用户收到主播同意上麦消息  &&  收到邀麦
  _micRequestSuccess(int? id, int? index) {
    logger.v('_micRequestSuccess userInfo.id: ${_userInfo!.id} id:$id  index：$index');
    if (_userInfo!.id == id) {
      logger.v('_micRequestSuccess userInfo.id == id');
      if (_micBeans[index!]!.userId == 0 && !_micBeans[index]!.lock!) {
        // 请求上麦
        micOperation(_roomId, 1, id, index);
        logger.v('_micRequestSuccess micOperation 01');
      } else if (_micIds.contains(id)) {
        // 已在麦上
        logger.v('_micRequestSuccess You are already on mic');
      } else {
        logger.v('_micRequestSuccess micOperation 02');
        for (int i = 0; i < _micBeans.length; i++) {
          logger.v('_micRequestSuccess micOperation 02 _micBeans[i].userId:${_micBeans[i]!.userId}');
          if (_micBeans[i]!.userId! <= 0 && !_micBeans[i]!.lock!) {
            // 请求上麦
            micOperation(_roomId, 1, id, i);
            logger.v('_micRequestSuccess micOperation 02-----01');
            break;
          }
        }
      }
    }
  }

  ///麦位信息整理
  _updateMic(List<YBDMicItem?> micItems) {
    List<int?> micIdsTem = [];
    // micIdsTem.clear();
    ///判断是否存在一个人在两个麦位上
    // check if one person have multiple mics
    // for (int i = 0; i < micItems.length; i++) {
    //   for (int j = 0; j < i; j++) {
    //     if (null != micItems[j] &&
    //         null != micItems[i] &&
    //         null != micItems[i].userId &&
    //         micItems[i].userId == micItems[j].userId) {
    //       micItems[i] = new YBDMicItem();
    //       // drop user self if multi mics
    //       if (_userInfo != null && micItems[i].userId == _userInfo.id) {
    //         micOperation(_roomId, 2, micItems[i].userId, i);
    //       }
    //       break;
    //     }
    //   }
    // }
    _runTimeFlag = -1;

    logger.v("micItems.length :${micItems.length}");
    YBDMicOperateHelper.getInstance()!.reloadMuteInfo(micItems);

    for (int i = 0; i < micItems.length; i++) {
      if (micItems[i]?.userId != null) {
        micIdsTem.add(micItems[i]!.userId);
      }

      if (micItems[i]!.userId != null && micItems[i]!.userId! > 0) {
        muteRemote(micItems[i]!.userId!, false);
      }
      _lock = (micItems[i]!.status == 0); // 0 lock，1 open
      logger.v("_micBeans[i] i：$i  micItems[i].status:${micItems[i]!.status}");
      _micIndex = i;
      if (!_lock!) {
        ///未锁麦
        if (micItems[i]!.userId != null && micItems[i]!.userId! > 0 && micItems[i]!.nickname != null) {
          ///通过用户ID是和nickname否存在判断在线离线
          _micOnline = true;
          _userId = micItems[i]!.userId;
          _img = micItems[i]!.img;
          _nikName = micItems[i]!.nickname;
          _vipIcon = micItems[i]!.vipIcon;

          YBDMuteInfo? muteInfo;
          YBDFrameInfo? frameInfo;

          ///静音  边框判断
          if (micItems[i]!.attributes != null) {
            muteInfo = micItems[i]!.attributes!.muteInfo;
            frameInfo = micItems[i]!.attributes!.frame;

            ///静音状态：0 - 非静音  1- 静音
            if (muteInfo != null && muteInfo.status == '1') {
              ///1- 静音
              _mute = true;
            } else {
              _mute = false;
            }

            ///TODO 待测试
            muteRemote(micItems[i]!.userId!, _mute!);
            logger.v('_updateMic mute userId: ${micItems[i]!.userId}  mute:$_mute');

            ///边框状态：存在 有边框
            if (frameInfo != null && frameInfo.image != null) {
//              frameUrl = frameInfo.image;
              _frameType = YBDCommonUtil.getFileType(frameInfo.image);
              if (_frameType == 1) {
                _frameUrl = YBDImageUtil.frame(null, frameInfo.image, "B");
              } else if (_frameType == 2) {
                _frameUrl = YBDImageUtil.gif(null, frameInfo.image, YBDImageType.FRAME);
              } else if (_frameType == 3) {
                _frameUrl = YBDImageUtil.gif(null, frameInfo.image, YBDImageType.FRAME);
                /* // 使用的时候自己去下载了。
                download(
                  YBDCarListRecordAnimationInfo()
                    ..version = 1
                    ..foldername = frameInfo.name
                    ..name = frameInfo.name
                    ..animationId = 10,
                  _frameUrl,
                  DownLoadType.svg_frame,
                );

                 */
              }
            } else {
              _frameUrl = null;
              _frameType = 0;
            }
          } else {
            _mute = false;
            _frameUrl = null;
            _frameType = 0;
          }
        } else {
          ///未锁麦--用户不在线上
          _micOnline = false;
          _mute = false;
          _frameUrl = null;
          _frameType = 0;
          _micOnline = false;
          _userId = 0;
          _img = null;
        }
      } else {
        ///锁麦
        _mute = false;
        _frameUrl = null;
        _frameType = 0;
        _micOnline = false;
        _userId = 0;
        _img = null;
      }
      _micBeans[i] = YBDMicBean(
          micOnline: _micOnline,
          lock: _lock,
          mute: _mute,
          frameType: _frameType,
          frameUrl: _frameUrl,
          frameSvgLocalPath: _frameSvgLocalPath,
          game: _game,
          userId: _userId,
          micIndex: _micIndex,
          img: _img,
          volume: _volume,
          nikName: _nikName,
          miniGameInfo: _micGames[i],
          emojiInfo: _micEmoji[i],
          vipIcon: _vipIcon
//        volumeOpacity: 0,
          );
      logger.v("_micBeans[i] i：$i  YBDMicBean.toString:${_micBeans[i].toString()}");
      // logger.v("_micBeans[i] i：$i frameUrl:$frameUrl");
      _syncUserMic(micItems[i], i, _mute);
    }
    _micIds = micIdsTem;
    logger.v("micIdsTem[i]  micIdsTem.toString:${micIdsTem.toString()}");
//    logger.v("_micBeans[i]  YBDMicBean.toString:${_micBeans.toString()}");
    _dropUserMic();
  }

  /// Scenario: user don't have  Mic But Server have -----
  _syncUserMic(YBDMicItem? micItem, int index, bool? mute) {
    if (null != _userInfo && null != micItem && null != micItem.userId && micItem.userId == _userInfo!.id) {
      _runTimeFlag = index;
      _runTimemute = mute;
      if (!gotMic) {
        micOperation(_roomId, 1, _userInfo!.id, index);
      } else {}
      //break;
    }
  }

  ///用户下麦：服务器没有上麦数据   本地确在上麦   以服务器为准
  _dropUserMic() {
    _getUser();
  }

  _getUser() async {
    if (_userInfo == null) {
      _getUserInfo();
    }

    if (_runTimeFlag == -1) {
      if (gotMic) {
        micOperation(_roomId, 2, _userInfo!.id, _runTimeFlag);
      }
    }
  }

  ///Agora Engine On or Off ON basis Of Mic
  _updateAgoraStatus() {
    if (_userInfo == null) {
      ///TODO 登出
      return;
    }
    logger.v('_updateAgoraStatus userInfo.id:${_userInfo!.id}');
    for (int j = 0; j < _micIds.length; j++) {
      logger.v('_updateAgoraStatus micIds[j]:${_micIds[j]}  J:$j');
    }

    ///判断是否pk房，PK房=>观众主播 都静音，主播不静音
    if (YBDLiveAgoraRtmService.instance.agoraRtmInfoStart?.pk_state == 1 && _userInfo!.id != _roomId) {
      logger.v('pk room,all Audience muted');
      YBDLiveService.instance.muteLocalAudioStream(true);
      return;
    }

    ///TODO 判断是否加入了声网  没有加入的  加入
    if (!_micIds.contains(_userInfo!.id)) {
//      setClientRoleHost(false);  TODO 设置声网角色  false 接收声音 不发声音  true 接收  也可以发
      YBDLiveService.instance.setClientRole(LiveRole.Audience);
    } else {
//      setClientRoleHost(true);  TODO 设置声网角色  false 接收声音 不发声音  true 接收  也可以发
      logger.v('_updateAgoraStatus runTimemute:$_runTimemute');
      if (YBDMicOperateHelper.getInstance()!.status == '') {
        ///不为‘’ 表示被禁言了
        YBDLiveService.instance.setClientRole(LiveRole.Broadcaster, runTimemute: _runTimemute);
      }
    }
  }

  ///麦位操作 socket
  ///  type操作类型: 1.上麦  2.下麦  3.强制下麦  4.设置为房间master
  micOperation(final int? roomId, final int type, final int? targetId, final int? index) async {
    logger.i('===================type:$roomId type:$type targetId$targetId index:$index');
    bool ishavePermission = false;

    ///麦克风权限申请
    if (type == 1) {
      if (await Permission.microphone.isGranted) {
        ishavePermission = true;
      } else {
        ishavePermission = await YBDSplashUtil.requestMicrophonePermission();
      }
      logger.i('ishavePermission:$ishavePermission');
      if (!ishavePermission) {
        ///ios需要引导用户去设置页面打开麦克风权限，Android跟随系统 新版本属性删除了
        /*
        if (Platform.isIOS && !await Permission.microphone.isUndetermined) {

         */
        if (Platform.isIOS) {
          YBDDialogUtil.goSettingDialog(context!);
        }
        return;
      }
    }
    logger.i('ishavePermission ---:$ishavePermission');
    if (index! >= MaxMicIndexSize) {
      logger.i('micOperation index:$index invalid (0~9)');
      return;
    }

    // if(type == 1 && YBDLiveService.instance.data?.pkInfo?.pkState == PkState?.Matching){
    //
    // }

    YBDRoomSocketApi.getInstance().micOperation(roomId, targetId, index, type, onSuccess: (Response data) {
      logger.i('===================micOperation : ${data.result} type：$type  code: ${data.code} desc: ${data.desc}');
      if (data.code == Const.MAC_OPERATE_SUCCESS) {
        if (type == 1) {
//        setClientRoleHost(true);
          YBDLiveService.instance.setClientRole(LiveRole.Broadcaster);
          gotMic = true;
        } else if (type == 2) {
//        setClientRoleHost(false);
          YBDLiveService.instance.setClientRole(LiveRole.Audience);
          gotMic = false;
        } else if (type == 4) {
//        ToastHelper.showToast(context, getResources().getString(R.string.made_master_successfully));
        }
      } else {
        YBDToastUtil.toast(data.desc ?? '');
      }
    }, onTimeOut: () {
      //        ToastHelper.showToast(context, "TimeOut!");
    });
  }

  ///被静音者收到消息后  调用RtcEngine 实现 静音/解除静音
  muteLocalMic(int? _index, int? _userId, int? originSequence, bool muted, int direction) {
    if (_userInfo == null) {
      logger.v('muteLocalMic userInfo == null');
      return;
    }

    if ((direction == 2) || (_userId == _userInfo!.id)) {
      ///被静音 或者自己静音自己
      YBDLiveService.instance.muteLocalAudioStream(muted);
      logger.v('muteLocalMic direction:$direction ,_userId == userInfo.id');
    } else {
      logger.v('muteLocalMic direction:$direction ,_userId:$_userId ,userInfo.id:${_userInfo!.id}');
    }

    ///flutter 版本声网API没有返回码
    logger.v(
        'muteLocalMic userInfo != null _index:$_index muted $muted  _userId:$_userId originSequence:$originSequence');
    if (muted) {
      _micMuteOperation(1, _userId, _index!, direction, originSequence);
    } else {
      _micMuteOperation(2, _userId, _index!, direction, originSequence);
    }
  }

  ///静音/解除静音 操作
  _micMuteOperation(int type, int? targetId, int index, int direction, int? originSequence,
      {String? protectData, onSuccess(Response data)?, onTimeOut}) {
    if (index >= MaxMicIndexSize) {
      logger.v('micOperation index:$index invalid (0~9)');
      return;
    }

    YBDRoomSocketApi.getInstance().micMuteOperation(_roomId, type, targetId, index, direction, originSequence,
        onSuccess: (Response data) {
      logger.i('===================micMuteOperation :code${data.code} result：${data.result}  desc：${data.desc}');
      print('===================micMuteOperation :code${data.code} result：${data.result}  desc：${data.desc}');
      if (data.code == "000000") {
        //success code
        if (type == 1) {
          //静音
          YBDToastUtil.toast('Mute success!');
          /*ToastHelper.showToast(context, "Mute success!");
        updateProfileDialog(true);*/
        } else if (type == 2) {
          //解除静音
          YBDToastUtil.toast('UnMute success!');
          /*  ToastHelper.showToast(context, "UnMute success!");
        updateProfileDialog(false);*/
        }
      } else if (data.code == "900012") {
        logger.v("micMuteOperation receive fail: 900012");
        if (type == 1) {
          //静音
          YBDToastUtil.toast('Mute User failed!');
//        ToastHelper.showToast(context, "Mute User failed!");
        } else if (type == 2) {
          //解除静音
          YBDToastUtil.toast('UnMute User failed!');
//        ToastHelper.showToast(context, "UnMute User failed!");
        }
      } else if (data.code == "905015") {
        logger.v("micMuteOperation receive fail:905015");
        if (type == 1) {
          //静音
          YBDToastUtil.toast('Mute Manager failed!');
//        ToastHelper.showToast(context, "Mute Manager failed!");
        } else if (type == 2) {
          //解除静音
          YBDToastUtil.toast('UnMute Manager failed!');
//        ToastHelper.showToast(context, "UnMute Manager failed!");
        }
      } else {
        logger.v("micMuteOperation receive fail:");
        if (type == 1) {
          //静音
          YBDToastUtil.toast('Mute failed!');
//        ToastHelper.showToast(context, "Mute failed!");
        } else if (type == 2) {
          //解除静音
          YBDToastUtil.toast('UnMute failed!');
//        ToastHelper.showToast(context, "UnMute failed!");
        }
      }
    }, onTimeOut: () {
      logger.v("micMuteOperation receive onTimeOut:");
      //        ToastHelper.showToast(context, "TimeOut!");
    });
  }

  ///三级以下  请求上麦
  requestMic(final int roomId, final int type, final int targetId, final int index) {
    logger.v('requestMic roomId:$roomId type:$type targetId$targetId index:$index');
    if (index >= MaxMicIndexSize) {
      logger.v('requestMic index:$index invalid (0~9)');
      return;
    }

    YBDRoomSocketApi.getInstance().requestMic(roomId, type, targetId, index, onSuccess: (Response data) {
      logger.v('===================micOperation : ${data.result}');
    }, onTimeOut: () {
      logger.v('requestMic TimeOut');
      //        ToastHelper.showToast(context, "TimeOut!");
    });
  }

  _micRequestResult(int? code, YBDMicRequestMessage micRequestMessage) {
    logger.v("_micRequestResult fromUser:${micRequestMessage.fromUser}");
    for (int i = 0; i < _micRequestlist.length; i++) {
      logger.v("_micRequestResult micRequestInfos[i].userInfo.id:${_micRequestlist[i].userInfo!.id}");
      if (micRequestMessage.fromUser == _micRequestlist[i].userInfo!.id) {
        _micRequestlist.removeAt(i);
        logger.v("_micRequestResult removeAt:${micRequestMessage.fromUser}");
      }
    }
    if (code == 50) {
      // 主播同意请求成功
      logger.v('User is micRequest success');
    } else if (code == 51) {
      // 主播请求同意请求失败 ：用户不在房间内
      YBDToastUtil.toast('User is not online');
    } else if (code == 52) {
      // 主播请求同意请求失败 ：MIC位已经被占用
      YBDToastUtil.toast('Mic is occupied');
    }
    logger.v('_micRequestResult MicEvent.Refresh _micRequestlist:${_micRequestlist.length}');
    add(MicEvent.Refresh);
  }

  ///判断用户是否在请求队列
  _checkMicRequestInfos(int? userId) {
    bool hasMicRequest = false;
    for (int i = 0; i < _micRequestlist.length; i++) {
      if (_micRequestlist[i].userInfo!.id == userId) {
        hasMicRequest = true;
        break;
      }
    }
    return hasMicRequest;
  }

  ///申请上麦  计时器（超过5min不处理  作废）
  requestTimeTask() {
    logger.v("requestTimeTask micRequestInfos.length:${_micRequestlist.length}");
    if (_micRequestlist.length <= 0) {
      if (_micRequestTime != null) {
        _micRequestTime!.cancel();
        _micRequestTime = null;
      }
    } else {
      if (_micRequestTime == null) {
        _micRequestTime = Timer.periodic(
          Duration(seconds: 10),
          (_) {
            logger.v('requestTimeTask micRequestInfos.length:${_micRequestlist.length}');
            bool hasOverTime = false;
            for (int i = 0; i < _micRequestlist.length; i++) {
              if (YBDDateUtil.overTime(
                _micRequestlist[i].time!,
                MicRequestTagTime,
              )) {
                _micRequestlist.removeAt(i);
                hasOverTime = true;
              }
            }

            if (hasOverTime) {
              logger.v("requestTimeTask MicEvent.Refresh");
              add(MicEvent.Refresh);
            }
          },
        );
      }
    }
  }

  ///队列下载
  download(YBDCarListRecordAnimationInfo animationConfig, String? downLoadUrl, DownLoadType downLoadType) async {
    logger.v('download animationConfig:${animationConfig.toString()}');
    if (animationConfig != null) {
      bool isSave = await YBDFileUtil.isAnimationDownloaded(animationConfig, downLoadType: downLoadType);
      if (isSave) {
        ///本地要是已经下载好  就直接播放
        List path = await YBDFileUtil.getSvgPlayPath(animationConfig);
//        frameSvgLocalPath = path[0];
        logger.v('download Animation is exists path:$path  start play');
        if (downLoadType == DownLoadType.svg_frame) {
          _updateFrameSvgaUrl(downLoadUrl, path[0]);
        } else if (downLoadType == DownLoadType.svg_emoji) {
          _updateEmojiSvgaUrl(downLoadUrl, path[0]);
        }
//          eventBus.fire(YBDDownloadLoadEvent(path[0], 0, url_mp3: path[1]));
        return;
      } else {
        ///没下载的  去下载
        YBDDownLoadBean downLoadBean = YBDDownLoadBean(
          downLoadUrl,
          fileName: animationConfig.name,
          savePath:
              '${downLoadType == DownLoadType.svg_emoji ? Const.ANIMATION_EMOJI : Const.ANIMATION}/${animationConfig.foldername}/${animationConfig.version}',
          type: downLoadType,
        );
        String? taskId = await YBDDownLoadManager.instance!.addDownLoadfile(downLoadBean);
        if (taskId != null) {
          downLoadBean.taskId = taskId;
          _downLoadList.add(downLoadBean);
          logger.v(
              "animationConfig.name:${animationConfig.name} taskId:$taskId downLoadList.length:${_downLoadList.length}");
        }
      }
    }
  }

  @override
  // ignore: must_call_super
  Future<Function?> close() {
    logger.v('mic_bloc close');
    _micMsgSubscription?.cancel();
    _micRequestTime?.cancel();
    _liveEventSubscription?.cancel();

    if (_micIds.contains(_userInfo?.id)) {
      micOperation(_roomId, 2, _userInfo!.id, -1);
    }

    YBDDownLoadManager.instance!.removeCallBack(YBDDownLoadManager.instance!.callBackMic);
    return Future.value(null);
  }
}

/// Mic消息
class YBDMicBlocState {
  /// 当前登录用户是否在麦位上
  bool? gotMic;

  /// 记录当前登录用户麦位下标
  int? runTimeFlag;

  /// 已上麦的用户列表
  List<int?>? micIds;

  /// 是否有麦位请求
  bool? hasMicRequest;

  /// 用户是否在播放麦位动画，默认没有播放
  bool? isPlayMicGame;

  /// 麦位信息
  List<YBDMicBean?>? micBeanList;

  /// 三级以下用户申请上麦的请求列表
  List<YBDMicRequestInfo>? micRequestlist;

  YBDMicBlocState({
    this.gotMic,
    this.runTimeFlag,
    this.micIds,
    this.isPlayMicGame,
    this.hasMicRequest,
    this.micBeanList,
    this.micRequestlist,
  }) {
    if (null == gotMic) {
      gotMic = false;
    }

    if (null == runTimeFlag) {
      runTimeFlag = -1;
    }

    if (null == micIds) {
      micIds = [];
    }

    if (null == isPlayMicGame) {
      isPlayMicGame = false;
    }

    if (null == hasMicRequest) {
      hasMicRequest = false;
    }

    if (null == micBeanList) {
      micBeanList = List.filled(MaxMicIndexSize, null);
    }

    if (null == micRequestlist) {
      micRequestlist = [];
    }
  }
}
