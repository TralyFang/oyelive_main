import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/room/define/room_ybd_define.dart';
import 'package:oyelive_main/ui/page/room/mic/mic_ybd_bloc.dart';

import '../../../../base/base_ybd_state.dart';
import '../bloc/room_ybd_bloc.dart';
import '../bloc/room_ybd_bloc_state.dart';
import '../entity/mic_ybd_bean.dart';
import 'mic_ybd_view.dart';

/// 房间麦位
class YBDMicPage extends StatefulWidget {
  // final roomType;
  // final List<YBDMicBean> micBeans;
  final int? roomId;

  YBDMicPage({
    this.roomId,
    // this.roomType,
    // this.micBeans,
  });

  @override
  _YBDMicPageState createState() => _YBDMicPageState();
}

class _YBDMicPageState extends BaseState<YBDMicPage> {
  @override
  Widget myBuild(BuildContext context) {
    return BlocBuilder<YBDRoomBloc, YBDRoomState>(
      buildWhen: (prev, current) => current.type == RoomStateType.RoomTagUpdated,
      builder: (_, state) {
        return BlocBuilder<YBDRoomBloc, YBDRoomState>(builder: (_, state) {
          if (state.roomTag == '0') {
            return _categoryStandard();
          } else if (state.roomTag == '1') {
            return _categorySpecial();
          } else if (state.roomTag == '4') {
            return _categoryFriends();
          } else if (state.roomTag == '3') {
            return _categoryParty(0);
          } else if (state.roomTag == '2') {
            return _categoryHeart();
          } else if (state.roomTag == '5') {
            return _categoryExclusive();
          } else if (state.roomTag == '6') {
            return _categoryRoyal();
          } else {
            return _categoryNoDate();
          }
        });
      },
    );
  }
  void myBuildHXoNpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //房间类型--没有数据的时候
  Widget _categoryNoDate() {
    return Container(
      // padding: EdgeInsets.only(left: ScreenUtil().setWidth(10), right: ScreenUtil().setWidth(10)),
//      height: ScreenUtil().setWidth(420),
//         color: Colors.amberAccent,
      child: GridView.builder(
        padding: EdgeInsets.only(top: ScreenUtil().setWidth(25)),
        shrinkWrap: true,
        itemCount: 8,
        clipBehavior: Clip.none,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          //横轴元素个数
          crossAxisCount: 4,
          //纵轴间距
          mainAxisSpacing: ScreenUtil().setWidth(5),
          //横轴间距
          crossAxisSpacing: ScreenUtil().setWidth(5),
          //子组件宽高长度比例
          childAspectRatio: 1.15,
        ),
        itemBuilder: (context, index) {
          return YBDMicViewItem(
            index,
            // userId,
            widget.roomId,
            YBDMicBean(micIndex: -1),
          );
        },
      ),
    );
  }
  void _categoryNoDateiYVgAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //房间类型--Standard
  Widget _categoryStandard() {
    return Container(
        // padding: EdgeInsets.symmetric(horizontal: 10.px),

        child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(
      builder: (_, state) {
        return GridView.builder(
          padding: EdgeInsets.only(top: ScreenUtil().setWidth(10)),
          shrinkWrap: true,
          itemCount: 8,
          clipBehavior: Clip.none,
          physics: NeverScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            //横轴元素个数
            crossAxisCount: 4,
            //纵轴间距
            mainAxisSpacing: ScreenUtil().setWidth(5),
            //横轴间距
            crossAxisSpacing: ScreenUtil().setWidth(5),
            //子组件宽高长度比例
            childAspectRatio: 1.15,
          ),
          itemBuilder: (context, index) {
            return YBDMicViewItem(index, widget.roomId, state.micBeanList![index]);
          },
        );
      },
    ));
  }
  void _categoryStandardgIIiqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //房间类型--9和10个麦中  8个麦位
  Widget _micEight(int macIndexStart) {
    return Container(
        color: Colors.transparent,
        // padding: EdgeInsets.only(left: ScreenUtil().setWidth(10), right: ScreenUtil().setWidth(10)),
        child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(
          builder: (_, state) {
            return GridView.builder(
              padding: EdgeInsets.only(top: ScreenUtil().setWidth(0)),
              shrinkWrap: true,
              itemCount: 8,
              clipBehavior: Clip.none,
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                //横轴元素个数
                crossAxisCount: 4,
                //纵轴间距
                mainAxisSpacing: ScreenUtil().setWidth(5),
                //横轴间距
                crossAxisSpacing: ScreenUtil().setWidth(5),
                //子组件宽高长度比例
                childAspectRatio: 1.15,
              ),
              itemBuilder: (context, index) {
                return YBDMicViewItem(
                  index + macIndexStart,
                  // userId,
                  widget.roomId,
                  state.micBeanList![index + macIndexStart],
                );
              },
            );
          },
        ));
  }
  void _micEightGfX2doyelive(int macIndexStart) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //房间类型--Special
  Widget _categorySpecial() {
    return Column(
      children: [
        _twoMicView(0, 1),
        Row(
          children: [
            _twoMicView(2, 3),
            Expanded(child: SizedBox(width: 1)),
            _twoMicView(4, 5),
          ],
        ),
        _twoMicView(6, 7),
      ],
    );
  }

  //房间类型--Friends
  Widget _categoryFriends() {
    return Container(
      height: ScreenUtil().setWidth(515),
      child: Column(
        children: [
          _twoMicView(0, 1),
          Container(
//            color: Colors.amber,
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(80), right: ScreenUtil().setWidth(80)),
              child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(
                builder: (_, state) {
                  return GridView.builder(
                    padding: EdgeInsets.only(top: ScreenUtil().setWidth(0)),
                    shrinkWrap: true,
                    itemCount: 6,
                    clipBehavior: Clip.none,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      //横轴元素个数
                      crossAxisCount: 3,
                      //                  maxCrossAxisExtent: ScreenUtil().setWidth(200),

                      //纵轴间距
                      mainAxisSpacing: ScreenUtil().setWidth(0),
                      //横轴间距
                      crossAxisSpacing: ScreenUtil().setWidth(30),
                      //子组件宽高长度比例
                      childAspectRatio: 1.1,
                    ),
                    itemBuilder: (context, index) {
                      return YBDMicViewItem(
                        index + 2,
                        // userId,
                        widget.roomId,
                        state.micBeanList![index + 2],
                      );
                    },
                  );
                },
              )),
        ],
      ),
    );
  }
  void _categoryFriendsXSUuloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //房间类型--Party
  Widget _categoryParty(int index) {
    return Container(
      width: ScreenUtil().setWidth(230),
      height: ScreenUtil().setWidth(300),
      padding: EdgeInsets.only(top: 10.px),
      child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(
        builder: (_, state) {
          return YBDMicViewItem(
            index,
            // userId,
            widget.roomId,
            state.micBeanList![index],
            imageWidth: 150,
            micHeight: 210,
            micItemWidth: 240,
          );
        },
      ),
    );
  }
  void _categoryParty41utZoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //房间类型--Heart
  Widget _categoryHeart() {
    return Container(
      child: Column(
        children: [
          _threeMicView(1, 0, 2),
          Container(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(2), right: ScreenUtil().setWidth(2)),
              child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(
                builder: (_, state) {
                  return GridView.builder(
                    padding: EdgeInsets.only(top: ScreenUtil().setWidth(0)),
                    shrinkWrap: true,
                    itemCount: 5,
                    clipBehavior: Clip.none,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      //横轴元素个数
                      crossAxisCount: 5,
                      //纵轴间距
                      mainAxisSpacing: ScreenUtil().setWidth(5),
                      //横轴间距
                      crossAxisSpacing: ScreenUtil().setWidth(1),
                      //子组件宽高长度比例
                      childAspectRatio: 0.7,
                    ),
                    itemBuilder: (context, index) {
                      return YBDMicViewItem(
                        index + 3,
                        // userId,
                        widget.roomId,
                        state.micBeanList![index + 3],
                        crossAxisCount: 5,
                      );
                    },
                  );
                },
              )),
        ],
      ),
    );
  }
  void _categoryHeartgcjydoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //房间类型--Exclusive
  Widget _categoryExclusive() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: ScreenUtil().setWidth(165),
            height: ScreenUtil().setWidth(152),
            child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(
              builder: (_, state) {
                return YBDMicViewItem(
                  0,
                  // userId,
                  widget.roomId,
                  state.micBeanList![0],
                  //              specialHeight: ScreenUtil().setWidth(100),
                );
              },
            ),
          ),
          _micEight(1),
        ],
      ),
    );
  }
  void _categoryExclusiveVWlCnoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //房间类型--Royal
  Widget _categoryRoyal() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(width: ScreenUtil().setWidth(15)),
              Container(
                width: ScreenUtil().setWidth(150),
                height: ScreenUtil().setWidth(160),
                child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(builder: (_, state) {
                  return YBDMicViewItem(
                    0,
                    // userId,
                    widget.roomId,
                    state.micBeanList![0],
                    //                  specialHeight: ScreenUtil().setWidth(100),
                  );
                }),
              ),
              Expanded(child: SizedBox(width: 1)),
              Container(
                width: ScreenUtil().setWidth(150),
                height: ScreenUtil().setWidth(160),
                color: Colors.transparent,
                child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(builder: (_, state) {
                  return YBDMicViewItem(
                    1,
                    // userId,
                    widget.roomId,
                    state.micBeanList![1],
                    //                  specialHeight: ScreenUtil().setWidth(100),
                  );
                }),
              ),
              SizedBox(width: ScreenUtil().setWidth(15)),
            ],
          ),
          _micEight(2),
        ],
      ),
    );
  }
  void _categoryRoyalT18Jmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //两个麦位一起
  Widget _twoMicView(int indexOne, int indexTwo) {
    return BlocBuilder<YBDMicBloc, YBDMicBlocState>(builder: (_, state) {
      return Container(
        width: ScreenUtil().setWidth(280),
        height: ScreenUtil().setWidth(160),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: ScreenUtil().setWidth(140),
              // color: Colors.,
              child: YBDMicViewItem(
                indexOne,
                // userId,
                widget.roomId,
                state.micBeanList == null ? null : state.micBeanList![indexOne],
              ),
            ),
            Container(
              width: ScreenUtil().setWidth(140),
              child: YBDMicViewItem(
                indexTwo,
                // userId,
                widget.roomId,
                state.micBeanList == null ? null : state.micBeanList![indexTwo],
              ),
            ),
          ],
        ),
      );
    });
  }
  void _twoMicViewZKRdxoyelive(int indexOne, int indexTwo) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //三个麦位一起
  Widget _threeMicView(int indexOne, int indexTwo, int indexThree) {
    return Container(
//      width: ScreenUtil().setWidth(350),
      width: ScreenUtil().setWidth(800),
      height: ScreenUtil().setWidth(240),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: ScreenUtil().setWidth(160),
//            height: ScreenUtil().setWidth(165),
            padding: EdgeInsets.only(top: 40),
            child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(
              builder: (_, state) {
                return YBDMicViewItem(
                  indexOne,
                  // userId,
                  widget.roomId,
                  state.micBeanList![indexOne],
                  crossAxisCount: 5,
                );
              },
            ),
          ),
          Container(
            width: ScreenUtil().setWidth(200),
//            height: ScreenUtil().setWidth(165),
            //imageWidth = 100, this.micHeight = 140, this.micItemWidth
            child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(
              builder: (_, state) {
                return YBDMicViewItem(
                  indexTwo,
                  // userId,
                  widget.roomId,
                  state.micBeanList![indexTwo],
                  imageWidth: 150,
                  micHeight: 210,
                  micItemWidth: 240,
                  crossAxisCount: 5,
                );
              },
            ),
          ),
          Container(
            width: ScreenUtil().setWidth(160),
            padding: EdgeInsets.only(top: 40),
//            height: ScreenUtil().setWidth(165),
            child: BlocBuilder<YBDMicBloc, YBDMicBlocState>(
              builder: (_, state) {
                return YBDMicViewItem(
                  indexThree,
                  // userId,
                  widget.roomId,
                  state.micBeanList![indexThree],
                  crossAxisCount: 5,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
  void _threeMicViewDVOgFoyelive(int indexOne, int indexTwo, int indexThree) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
