import 'dart:async';


import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/entity/frame_ybd_Info.dart';
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_bean.dart';
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_item.dart';
import 'package:oyelive_main/ui/page/room/entity/mute_ybd_Info.dart';

/// mic_bloc 辅助类
class YBDMicBlocHelper {
  ///判断麦位上是否有用户
  static bool isMicOnline(YBDMicItem micItem) {
    if (micItem.userId != null && micItem.userId! > 0 && micItem.nickname != null) {
      return true;
    }
    return false;
  }

  ///获取静音对象
  static YBDMuteInfo? getMuteInfo(YBDMicItem micItem) {
    return micItem?.attributes?.muteInfo;
  }

  ///获取静音状态
  static bool getMuteState(YBDMuteInfo? muteInfo) {
    if (muteInfo != null && muteInfo.status == '1') {
      return true;
    }
    return false;
  }

  ///获取边框对象
  static YBDFrameInfo? getFrameInfo(YBDMicItem micItem) {
    return micItem?.attributes?.frame;
  }

  ///获取边框url
  static String? getFrameUrl(YBDFrameInfo? frameInfo, int frameType) {
    if (frameInfo == null) {
      return null;
    }
    String? _frameUrl;
    if (frameType == 1) {
      _frameUrl = YBDImageUtil.frame(null, frameInfo.image, "B");
    } else if (frameType == 2) {
      _frameUrl = YBDImageUtil.gif(null, frameInfo.image, YBDImageType.FRAME);
    } else if (frameType == 3) {
      _frameUrl = YBDImageUtil.gif(null, frameInfo.image, YBDImageType.FRAME);
    } else {
      YBDLogUtil.v('getFrameUrl frameType is invalid frameType : $frameType');
    }
    return _frameUrl;
  }

  ///获取当前麦位
  static YBDMicBean? getCurMicBean(MicState micState, {YBDMicItem? micItem, int? micIndex}) {
    YBDMicBean? micBean;
    switch (micState) {
      case MicState.LOCK:
        micBean = getLockMicBean();
        break;
      case MicState.UNLOCK_ONLINE:
        micBean = getOnlineMicBean(micItem!, micIndex);
        break;
      case MicState.UNLOCK_NO_ONLINE:
        micBean = getFreeMicBean();
        break;
      case MicState.UNKNOWN:
        break;
    }
    return micBean;
  }

  ///锁麦麦位
  static YBDMicBean getLockMicBean() {
    return YBDMicBean(lock: true);
  }

  ///未锁麦 有人
  static YBDMicBean getOnlineMicBean(YBDMicItem micItem, int? micIndex) {
    late YBDMicBean micBean;
    micBean = YBDMicBean();
    micBean.micOnline = true;
    micBean.userId = micItem.userId;
    micBean.img = micItem.img;
    micBean.nikName = micItem.nickname;
    micBean.mute = getMuteState(getMuteInfo(micItem));
    micBean.micIndex = micIndex;

    YBDFrameInfo? frameInfo = getFrameInfo(micItem);
    micBean.frameType = YBDCommonUtil.getFileType(frameInfo?.image);
    micBean.frameUrl = getFrameUrl(frameInfo, YBDCommonUtil.getFileType(frameInfo?.image));
    return YBDMicBean(lock: true);
  }

  ///未锁麦 没人
  static YBDMicBean getFreeMicBean() {
    return YBDMicBean();
  }

  ///用户下麦：服务器没有上麦数据   本地确在上麦   以服务器为准
  static void dropUserSelfMic() {}
}

/// 麦位状态
enum MicState {
  ///锁麦
  LOCK,

  ///未锁麦 麦位上有人
  UNLOCK_ONLINE,

  ///未锁麦 麦位上没人
  UNLOCK_NO_ONLINE,

  ///未知
  UNKNOWN,
}
