import 'dart:async';


import 'package:oyelive_main/common/analytics/thinking/ta_ybd_live_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_live_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/common/room_socket/message/common/emoji_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/room_ybd_message_helper.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/page/room/bloc/combo_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/entity/emoji_ybd_entity.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../widget/frame_ybd_animation_image.dart';
import '../../../widget/round_ybd_avatar.dart';
import '../bloc/pk_ybd_state_bloc.dart';
import '../bloc/room_ybd_bloc.dart';
import '../bloc/room_ybd_bloc_state.dart';
import '../define/room_ybd_define.dart';
import '../entity/mic_ybd_bean.dart';
import '../util/room_ybd_util.dart';
import '../widget/spread_ybd_widget.dart';
import '../widget/svga_ybd_play_frame.dart';
import 'mic_ybd_bloc.dart';
import 'mic_ybd_operat_view.dart';

/// 麦位 item
class YBDMicViewItem extends StatefulWidget {
  final YBDMicBean? micBean;
  final int micIndex;

  // String userId;
  final int? roomId;

//  double specialHeight;

  ///头像固定高
  final int imageWidth;

  ///头像固定宽

  final int micHeight;

  ///麦位Item固定高  包含边框 + 昵称
  final int micItemWidth;

  ///每行麦位数 5的时候特别处理
  final int crossAxisCount;

  YBDMicViewItem(
    this.micIndex,
    // this.userId,
    this.roomId,
    this.micBean, {
    this.imageWidth = 96,
    this.micHeight = 130,
    this.micItemWidth = 160,
    this.crossAxisCount = 4,
  });

  @override
  _YBDMicViewItemState createState() => _YBDMicViewItemState();
}

class _YBDMicViewItemState extends BaseState<YBDMicViewItem> with SingleTickerProviderStateMixin {
  final int _headTopWidth = 30;

  ///昵称文字大小
  final int _nikNameSize = 24;

  /// 0 普通用户 1 管理员  2主播
  int _role = 0;

  int? _userId;
  // emoji controller
  late SVGAAnimationController _animationController;
  @override
  void initState() {
    super.initState();
    _userId = YBDCommonUtil.storeFromContext()?.state.bean?.id;
    _animationController = SVGAAnimationController(vsync: this);
    addSub(YBDRoomMessageHelper.getInstance().messageOutStream.listen((YBDMessage event) {
      if (event is YBDEmojiMessage &&
          event.fromUser == widget.micBean?.userId &&
          _animationController.status == AnimationStatus.dismissed) {
        logger.v('22.12.29---mic view event:${event.content}');
        String emojiSvga = _getEmoji(event.content ?? '');
        if (emojiSvga.isNotEmpty) {
          Future decode = SVGAParser.shared.decodeFromURL(emojiSvga);
          decode.then((videoItem) async {
            if (mounted) {
              _animationController.videoItem = videoItem;
              await _animationController.forward().whenComplete(() {
                logger.v('new emoji svga play Complete : $emojiSvga');
                _animationController.reset();
                _animationController.videoItem = null;
              });
            }
          });
        } else {
          logger.v('22.12.30---emoji Svga is empty!');
        }
      }
    }));
  }
  void initState8dOlIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String _getEmoji(String content) {
    // "{id:1002 , emojiCode:1}"
    String path = '';
    // String testPath = 'https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/temp/kiss.svga';
    try {
      Map<String, int> map = Map<String, int>.from(json.decode(content));
      if (map != null && map.isNotEmpty) {
        logger.v('22.12.30---emojis:${YBDTPGlobal.emojis}');
        YBDEmojiEntity spEmojis = YBDEmojiEntity().fromJson(YBDTPGlobal.emojis as Map<String, dynamic>);
        // print('22.12.27---record:${spEmojis.record}');
        for (var e in spEmojis.record ?? []) {
          if (e.id == map['id'].toString()) {
            for (var a in e.emojiDatas) {
              if (a.id == map['emojiCode'].toString()) {
                logger.v('22.12.30---pic1:${a.svga}');
                path = a.svga ?? '';
                // if (a.svga == null || a.svga.isEmpty) {
                //   path = testPath;
                // } else {
                //   path = a.svga;
                // }
                break;
              }
            }
          }
        }
      }
    } catch (e) {}
    return path;
  }
  void _getEmojiSwioIoyelive(String content) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    if (widget.micBean == null || widget.micBean?.micIndex == null || widget.micBean?.micIndex == -1) {
      return _offLineItemView('assets/images/liveroom/icon_lock_mic_normal@2x.webp');
    }

    return _getMicWidget();
  }
  void myBuildVWA0Uoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _getMicWidget() {
    return GestureDetector(
      onTap: () {
        final YBDRoomState roomBlocState = BlocProvider.of<YBDRoomBloc>(context).state;

        if (_userId == widget.roomId) {
          _role = 2;
        } else if (roomBlocState.roomManagers != null) {
          for (int i = 0; i < (roomBlocState.roomManagers?.length ?? 0); i++) {
            if (_userId == roomBlocState.roomManagers![i]?.id && !YBDLiveService.instance.bootedLiveRoomLimit) {
              _role = 1;
              logger.v('roomManagers  role---:$_role');
              break;
            }
            _role = 0;
          }
        } else {
          _role = 0;
        }
//        logger.v('-----------------userId:${widget.userId} roomId:${widget.roomId.toString()}');
        logger.v('-----------------role:$_role');
//        logger.v('_getMicWidget micIndex: ${widget.micIndex} micBean:${widget.micBean.toString()}');
        logger.v('_getMicWidget micIndex role: $_role  gotMic:${BlocProvider.of<YBDMicBloc>(context).state.gotMic}');

        if (_role == 2 || _role == 1) {
          //主播 有权限打开
          _showMicOperation();
        } else if (!widget.micBean!.micOnline! && !widget.micBean!.lock! && !BlocProvider.of<YBDMicBloc>(context).gotMic) {
          //非主播 麦位离线且未锁麦
//          _showMicOperation();
          logger.v('_micRequestEnable: ${_micRequestEnable()}, _limitLevel:$_limitLevel');
          final userInfo = YBDCommonUtil.storeFromContext()!.state.bean;

          if ((_micRequestEnable() == 1) && userInfo!.roomlevel! < _limitLevel() && userInfo.level! < _limitLevel()) {
            //limitLevel级以下申请上麦&&上麦限制
            //final int roomId, final int type, final int targetId, final int index
            BlocProvider.of<YBDMicBloc>(context).requestMic(widget.roomId ?? -1, 1, widget.roomId ?? -1, widget.micBean!.micIndex!);
          } else {
            //三级以上直接上麦（包含三级）   或者 没有限制的时候
            // 上麦埋点
            YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_mic_take);
            BlocProvider.of<YBDMicBloc>(context).micOperation(widget.roomId, 1, _userId, widget.micBean!.micIndex);
          }
        } else if (widget.micBean!.micOnline! && _userId == widget.micBean!.userId) {
          //自己在麦位上
          _showMicOperation();
        } else if (widget.micBean!.micOnline! && _userId != widget.micBean!.userId) {
          //别人在麦位上
          YBDRoomUtil.showMiniProfileDialog(
            roomId: widget.roomId,
            userId: widget.micBean!.userId,
          );
        }
      },
      child: Stack(children: [
        (widget.micBean!.lock!
            ? _offLineItemView('assets/images/liveroom/icon_lock_mic_normal@2x.webp')
            : (widget.micBean!.micOnline!
                ? Positioned(
                    child: Stack(children: [
                      _getVolumeWidget(),
                      _getHeadWidget(),
                      _getFrameWidget(),
                      _getGameWidget(),
                      _getEmojiWidget(),
                      _getDefaultWidget(),
                      _getMuteWidget(),
                      _getVipIcon(),
                      _getNewEmojiWidget(),
                    ]),
                  )
                : Container(
                    child: _offLineItemView('assets/images/liveroom/icon_mic_normal@2x.webp'),
                  ))),
      ]),
    );
  }
  void _getMicWidget2vvSOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///锁麦和离线的时候widget
  Widget _offLineItemView(
    String picUrl, {
    bool locked = false,
  }) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Positioned(
          top: 0,
          child: Container(
            alignment: Alignment.center,
            height: ScreenUtil().setWidth(widget.micHeight),
            child: Image.asset(
              '$picUrl',
              fit: BoxFit.cover,
              width: ScreenUtil().setWidth(widget.imageWidth),
              height: ScreenUtil().setWidth(widget.imageWidth),
            ),
          ),
        ),
        Positioned(
          top: ScreenUtil().setWidth(widget.micHeight - 10),
          child: Container(
            width: ScreenUtil().setWidth(widget.micItemWidth),
            height: ScreenUtil().setWidth(_headTopWidth),
            alignment: Alignment.bottomCenter,
            child: BlocBuilder<YBDRoomBloc, YBDRoomState>(
              buildWhen: (previous, current) => current.type == RoomStateType.RoomTagUpdated,
              builder: (context, state) {
                return Text(
                  getMacIndex(widget.micBean),
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(_nikNameSize),
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  /// 调整麦位序号
  String _micNumberString(YBDMicBean micBean, String roomTag) {
    if (micBean == null || micBean.micIndex == null) {
      return '';
    }

    var micNumber = 0;
    var micNumberStr = '';

    // logger.v('_getMicNumber _roomBloc.roomTag${_roomBloc.roomTag} widget.micBean.micIndex${widget.micBean.micIndex}');
    if (roomTag == '1') {
      if (micBean.micIndex == 0) {
        micNumber = 3;
      } else if (micBean.micIndex == 1) {
        micNumber = 4;
      } else if (micBean.micIndex == 2) {
        micNumber = 1;
      } else if (micBean.micIndex == 3) {
        micNumber = 2;
      } else if (micBean.micIndex == 4) {
        micNumber = 7;
      } else if (micBean.micIndex == 5) {
        micNumber = 8;
      } else if (micBean.micIndex == 6) {
        micNumber = 5;
      } else if (micBean.micIndex == 7) {
        micNumber = 6;
      }
    } else if (roomTag == '2') {
      if (micBean.micIndex == 0) {
        micNumber = 2;
      } else if (micBean.micIndex == 1) {
        micNumber = 1;
      }
    } else if (roomTag == '5') {
      micNumber = micBean.micIndex! + 2;
      if (micNumber == 10) {
        micNumber = 1;
      }
    } else if (roomTag == '6') {
      micNumber = micBean.micIndex! + 3;
      if (micNumber == 11) {
        micNumber = 1;
      } else if (micNumber == 12) {
        micNumber = 2;
      }
    }

    micNumberStr = '$micNumber';

    if (roomTag == '3') {
      micNumberStr = 'Mic';
    }

    return micNumberStr;
    // logger.v(
    //     '_getMicNumber _roomBloc.roomTag${_roomBloc.roomTag}----micNumber$micNumber ----micNumbeString:$micNumbeString');
  }
  void _micNumberStringBNYYkoyelive(YBDMicBean micBean, String roomTag) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 锁麦的空麦名称
  String _lockedOffLineMicName(YBDMicBean micBean, String micNumberStr) {
    if (null == micBean) return '';
    return micNumberStr == '0' ? '${(micBean.micIndex! + 1)}' : micNumberStr;
  }

  /// 未锁麦的空麦名称
  String _offLineMicName(YBDMicBean micBean, String micNumberStr) {
    if (null == micBean) return '';
    return micBean.micIndex != -1 ? (micNumberStr == '0' ? '${micBean.micIndex! + 1}' : micNumberStr) : '';
  }

  /// 声音动效
  Widget _getVolumeWidget() {
    if (widget.micBean == null ||
        widget.micBean!.volume == null ||
        widget.micBean!.micOnline == null ||
        !widget.micBean!.micOnline!) {
      logger.v('_getVolume Widgetwidget.micBean == null');
      return Container();
    }
    logger
        .v('_getVolume Widgetwidget.micBean volume: ${widget.micBean!.volume}  micOnline: ${widget.micBean!.micOnline}');
    return (widget.micBean!.volume! >= ShowVolumeTag && widget.micBean!.micOnline!)
        ? Column(
            children: [
              Container(
                color: Colors.transparent,
                alignment: Alignment.center,
                height: ScreenUtil().setWidth(widget.micHeight),
                child: YBDSpreadWidget(
                  radius: ScreenUtil().setWidth(widget.micHeight - 30),
                  maxRadius: ScreenUtil().setWidth(widget.micHeight + 15),
                  spreadColor: Colors.white.withOpacity(0.3),
//                  volume: widget.micBean.volume,
//                  cycles: 6,
                ),
              ),
              /* Container(
                height: ScreenUtil().setWidth(headTopWidth),
                alignment: Alignment.center,
                child: Text(
                  "$nikName",
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(nikNameSize),
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),*/
            ],
          )
        : Container();
  }
  void _getVolumeWidgetPUam7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///游戏
  Widget _getGameWidget({String nikName = ""}) {
    if (widget.micBean == null || widget.micBean!.miniGameInfo == null) {
      return Container();
    }
    logger.v('_getVolume Widgetwidget.micBean game: ${widget.micBean!.game}  micOnline: ${widget.micBean!.micOnline}');
    return (widget.micBean!.micOnline! && widget.micBean!.miniGameInfo != null)
        ? Column(
            children: [
              Container(
                  color: Colors.transparent,
                  alignment: Alignment.center,
                  height: ScreenUtil().setWidth(widget.micHeight),
                  child: YBDFrameAnimationImage(
                    widget.micBean?.miniGameInfo?.images ?? <String>[],
                    interval: 120,
                    height: ScreenUtil().setWidth(widget.imageWidth),
                    width: ScreenUtil().setWidth(widget.imageWidth),
                    playCallback: () {
                      BlocProvider.of<YBDMicBloc>(context).hideMiniGame(widget.micBean!.miniGameInfo!);
                    },
                    hidedelayTime: 2000,
                  )),
              Container(
                height: ScreenUtil().setWidth(_headTopWidth),
                alignment: Alignment.center,
                child: Text(
                  "$nikName",
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(_nikNameSize), color: Colors.white, fontWeight: FontWeight.w400),
                ),
              ),
            ],
          )
        : Container();
  }
  void _getGameWidget2VHPFoyelive({String nikName = ""}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///静音图标
  Widget _getMuteWidget() {
    return widget.micBean!.mute!
        ? Column(
            children: [
              Stack(
                children: [
                  Container(
                    color: Colors.transparent,
                    // color: Colors.blue,
                    alignment: Alignment.center,
                    height: ScreenUtil().setWidth(widget.micHeight),
                    child: Container(
                      height: ScreenUtil().setWidth(widget.imageWidth),
                      width: ScreenUtil().setWidth(widget.imageWidth),
                      padding: EdgeInsets.only(right: 60.px, top: 60.px),
                      child: Center(
                        child: Image.asset(
                          'assets/images/liveroom/icon_mute_mic@2x.webp',
                          height: ScreenUtil().setWidth(30),
                          fit: BoxFit.contain,
                          // color: Colors.white.withOpacity(0.8),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                height: ScreenUtil().setWidth(_headTopWidth),
                alignment: Alignment.center,
                child: Text(
                  '',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(_nikNameSize), color: Colors.white, fontWeight: FontWeight.w400),
                ),
              ),
            ],
          )
        : Container();
  }
  void _getMuteWidgetK2KCMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///头像
  Widget _getHeadWidget() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Positioned(
          top: 0,
          child: Container(
            alignment: Alignment.center,
            height: ScreenUtil().setWidth(widget.micHeight),
            width: ScreenUtil().setWidth(widget.micHeight),
//            color: Colors.black54,
            child: YBDRoundAvatar(
              // 用户头像
              widget.micBean!.img,
              userId: widget.micBean!.userId,
              scene: "B",
              labelWitdh: ScreenUtil().setWidth(widget.imageWidth).toInt(),
              needNavigation: false,
              avatarWidth: widget.imageWidth - 2,
              sideBorder: false,
            ),
          ),
        ),
        Positioned(
          top: ScreenUtil().setWidth(widget.micHeight - 10),
          child: Container(
            ///每行5麦位的时候  昵称长度要减少  否则和旁边麦位的昵称靠得太近
            width: widget.crossAxisCount == 5
                ? ScreenUtil().setWidth(widget.micItemWidth - 20)
                : ScreenUtil().setWidth(widget.micItemWidth),
            height: ScreenUtil().setWidth(_headTopWidth),
            alignment: Alignment.bottomCenter,
            child: Text(
              "${widget.micBean?.nikName ?? ''}",
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(_nikNameSize),
                color: Colors.white,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ),
      ],
    );
  }
  void _getHeadWidgetkg4j2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///边框
  Widget _getFrameWidget() {
    logger.v(
        '_getFrameWidget frameType userid:${widget.micBean?.userId},${widget.micBean?.frameType}  frameUrl:${widget.micBean?.frameUrl}');
    return Column(
      children: [
        (((widget.micBean!.frameType == 1 || widget.micBean!.frameType == 2) && widget.micBean!.frameUrl != null)
            //png 或者 gif
            ? BlocBuilder<YBDRoomBloc, YBDRoomState>(
                buildWhen: (prev, current) => current.type == RoomStateType.RoomTagUpdated,
                builder: (_, state) {
                  return Container(
                    //                color: Colors.black26,
                    margin: EdgeInsets.only(top: ScreenUtil().setWidth((state.roomTag == '3') ? 6 : 0)),
                    alignment: Alignment.center,
                    height: ScreenUtil().setWidth((state.roomTag == '3') ? (widget.micHeight - 15) : widget.micHeight),
                    child: YBDNetworkImage(
                      //                width: ScreenUtil().setWidth(micWidth),
                      //                height: ScreenUtil().setWidth(micWidth),
                      imageUrl: widget.micBean?.frameUrl ?? '',
                      fit: BoxFit.fill,
                    ),
                  );
                },
              )
            : (widget.micBean!.frameType == 3 && widget.micBean!.frameUrl != null)
                //svga
                ? StreamBuilder<String>(
                    stream: YBDDownloadCacheManager.instance.downloadStreamURL(widget.micBean!.frameUrl!),
                    builder: (context, snapshot) {
                      if (snapshot.data != null)
                        return YBDSvgaPlayerView(
                          url: snapshot.data != null ? snapshot.data : widget.micBean!.frameSvgLocalPath,
                          width: ScreenUtil().setWidth(widget.micHeight + 5),
                          height: ScreenUtil().setWidth(widget.micHeight + 5),
                          repeat: true,
                        );
                      return Container();
                    })
                : Container()),
        Container(
          height: ScreenUtil().setWidth(_headTopWidth),
          alignment: Alignment.topCenter,
          child: Text(
            '',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(_nikNameSize),
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ],
    );
  }
  void _getFrameWidgetYNxcNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///YBDEmoji
  Widget _getEmojiWidget() {
    // logger.v(
    //     '_getEmojiWidget emoji:${widget.micBean.emoji}  emojiLocalPath:${widget.micBean?.emojiInfo?.emojiLocalPath ?? null}');
    return Column(children: [
      ((widget.micBean!.emoji && widget.micBean!.emojiInfo != null && widget.micBean!.emojiInfo!.emojiLocalPath != null)
          ? YBDSvgaPlayerView(
              url: widget.micBean!.emojiInfo!.emojiLocalPath,
              width: ScreenUtil().setWidth(widget.micHeight),
              height: ScreenUtil().setWidth(widget.micHeight),
              repeat: false,
            )
          : Container()),
      Container(
//                        color: Colors.white,
        height: ScreenUtil().setWidth(_headTopWidth),
        alignment: Alignment.topCenter,
        child: Text(
          '',
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(_nikNameSize),
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
    ]);
  }
  void _getEmojiWidgetPBuiEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _getVipIcon() {
    return Positioned.fill(
      // top: ScreenUtil().setWidth(widget.micHeight / 2),
      // right: ScreenUtil().setWidth(widget.imageWidth == 150 ? 18 : 12),
      // bottom: ScreenUtil().setWidth(42),
      child: Column(
        children: [
          SizedBox(
            height: ScreenUtil().setWidth(widget.imageWidth) * (widget.micHeight > 200 ? 0.7 : 0.65),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(widget.imageWidth) * 1.3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                YBDVipIcon(
                  widget.micBean?.vipIcon,
                  padding: EdgeInsets.only(),
                  size: widget.micHeight > 200 ? 70 : 56,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// New YBDEmoji
  Widget _getNewEmojiWidget() {
    return Column(children: [
      Transform.scale(
        scale: 1.2,
        child: Container(
          width: widget.micHeight.px,
          height: widget.micHeight.px,
          margin: EdgeInsets.only(bottom: 190.px),
          // color: Colors.pink,
          child: SVGAImage(_animationController, fit: BoxFit.cover),
        ),
      ),
      Container(
        height: ScreenUtil().setWidth(_headTopWidth),
        alignment: Alignment.topCenter,
        child: Text(
          '',
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(_nikNameSize),
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
    ]);
  }
  void _getNewEmojiWidgetTZXTWoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///
  Widget _getDefaultWidget() {
    return Container(
      height: ScreenUtil().setWidth(widget.micHeight),
      width: ScreenUtil().setWidth(widget.micHeight),
      alignment: Alignment.center,
      color: Colors.transparent,
      child: Text(""),
    );
  }

  // ignore: missing_return
  void callBackOperater(final int roomId, final int type, final int targetId, final int index) {
    logger.v('22.9.28--callBackOperater roomId :$roomId type :$type targetId :$targetId index :$index');
    if (type == 1) {
      YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_mic_take);
    } else if (type == 2) {
      YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.liveroom_mic_drop);
    }
    BlocProvider.of<YBDMicBloc>(context).micOperation(roomId, type, targetId, index);
  }

  // ignore: missing_return
  void callBackMute(int _index, int _userId, int originSequence, bool muted) {
    logger.v('callBackMute _index :$_index _userId:$_userId  originSequence:$originSequence muted:$muted');
    BlocProvider.of<YBDMicBloc>(context).muteLocalMic(_index, _userId, originSequence, muted, 1);
  }

  /// 弹出底部麦位操作选择框
  _showMicOperation() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(16))),
      ),
      builder: (_) {
        return MultiBlocProvider(
          providers: [
//            BlocProvider.value(value: BlocProvider.of<YBDRoomBloc>(context)),
            BlocProvider.value(value: BlocProvider.of<YBDMicBloc>(context)),
            BlocProvider.value(value: BlocProvider.of<YBDRoomBloc>(context)),
            BlocProvider.value(value: BlocProvider.of<YBDRoomComboBloc>(context)),
          ],
          child: YBDMicOperationPage(
            roomId: widget.roomId,
            userId: '$_userId',
            micBean: widget.micBean,
            micOperaterLister: callBackOperater,
            micMuteLister: callBackMute,
            pkStateBloc: context.read<YBDPkStateBloc>(),
          ),
        );
      },
    );
  }

  /// 配置项中的申请上麦的用户等级
  int _limitLevel() {
    // 3 为默认值
    var level = YBDCommonUtil.storeFromContext()!.state.configs?.micRequestUserLevel ?? '3';
    return YBDNumericUtil.stringToInt(level);
  }
  void _limitLevelr0HEfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 房间允许请求上麦
  /// 在[onTap]方法里可以直接访问state
  int? _micRequestEnable() {
    return BlocProvider.of<YBDRoomBloc>(context).state.roomInfo?.micRequestEnable;
  }

  String getMacIndex(YBDMicBean? micBean) {
    if (micBean == null || micBean.micIndex == null) {
      return "";
    }
    return (micBean.micIndex! + 1).toString();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
