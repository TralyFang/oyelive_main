import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/bloc/pk_ybd_state_bloc.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc_state.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../bloc/room_ybd_bloc.dart';
import '../entity/mic_ybd_bean.dart';
import '../util/room_ybd_util.dart';
import 'mic_ybd_bloc.dart';

class YBDMicOperationPage extends StatefulWidget {
  final int? roomId;
  final String userId;
  final YBDMicBean? micBean;
  final micOperaterLister;
  final micMuteLister;

  // final bool gotMic;
  YBDPkStateBloc? pkStateBloc;

  YBDMicOperationPage(
      {required this.roomId,
      required this.userId,
      required this.micBean,
      // this.gotMic,
      required this.micOperaterLister,
      required this.micMuteLister,
      this.pkStateBloc});

  @override
  _YBDMicOperationState createState() => _YBDMicOperationState();
}

class _YBDMicOperationState extends BaseState<YBDMicOperationPage> {
  int _operationType = 0; //0 隐藏  1 上麦  2下麦
  int _muteType = 0; //0 隐藏  1 静音  2 解除静音
  int _lockType = 0; //0 隐藏  1 锁麦  2 解除锁麦
  bool _dropMic = false; //false 隐藏  true 显示
  bool _showProfile = false; //false 隐藏  true 显示
  bool _showMicFrame = false; //false 隐藏  true 显示
  bool _talentFlag = false;

//  bool mute = false;
  int _role = 0; //0 普通用户   1 管理员  2主播

  // YBDMicBloc _micBloc;
  // YBDRoomBloc _roomBloc;

  @override
  void initState() {
    super.initState();
    // _roomBloc = context.read<YBDRoomBloc>();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _getDate();

      // 刷新界面
      if (mounted) setState(() {});
    });

    logger.v("YBDMicOperationPage initState");
  }
  void initStateAiTn2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _getDate() {
    if (widget.userId == null || widget.micBean == null) {
      logger.v('userId: ${widget.userId}, micBean: ${widget.micBean}');
      return;
    }

    if (widget.userId == widget.roomId.toString()) {
      _role = 2;
    } else {
      final YBDRoomState roomBlocState = BlocProvider.of<YBDRoomBloc>(context).state;
      if (roomBlocState.roomManagers != null && roomBlocState.roomManagers!.length > 0) {
        for (int i = 0; i < roomBlocState.roomManagers!.length; i++) {
          if (widget.userId == roomBlocState.roomManagers![i]?.id?.toString()) {
            _role = 1;
            logger.v('roomManagers  role---:$_role');
            break;
          }
          _role = 0;
        }
      } else {
        _role = 0;
      }
    }

    logger.v('roomManagers  role2---:$_role');
    _dropMic = false;
    _showProfile = false;
    _showMicFrame = false;
    _lockType = 0;
    _muteType = 0;
    _operationType = 0;

    if (_role == 2 || _role == 1) {
      //主播  || 管理员
      if (widget.micBean!.micOnline!) {
        if (widget.userId == widget.micBean!.userId.toString()) {
          ///用户本人在麦上  可以下麦、显示边框
          _operationType = 2;
          _showMicFrame = true;
        } else if (widget.userId != widget.micBean!.userId.toString()) {
          ///其它用户在麦上  可以强制下麦、查看profile
          if (_role == 2) {
            _dropMic = true; //只有主播可以强制下麦
          }
          _showProfile = true;
        }

        //静音
        if (widget.micBean!.mute!) {
          _muteType = 2;
        } else if (!widget.micBean!.mute!) {
          _muteType = 1;
        }
      } else {
        //锁麦
        if (widget.micBean!.lock!) {
          _lockType = 2;
        } else {
          _lockType = 1;
          if (!BlocProvider.of<YBDMicBloc>(context).state.gotMic!) {
            _operationType = 1;
          }
        }
      }
    } else if (_role == 0) {
      //普通用户
      _dropMic = false;
      if (widget.micBean!.micOnline!) {
        if (widget.userId == widget.micBean!.userId.toString()) {
          _operationType = 2;
          _showMicFrame = true;
          //静音
          if (widget.micBean!.mute!) {
            _muteType = 2;
          } else if (!widget.micBean!.mute!) {
            _muteType = 1;
          }
        } else {}
      } else {
        //上麦
        if (!widget.micBean!.lock!) {
          _operationType = 1;
        }
      }
    }
    logger.v(
      "YBDMicOperationPage initState dropMic:$_dropMic  showProfile:$_showProfile  lockType:$_lockType  muteType:$_muteType operationType:$_operationType",
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
  void disposeK0dbQoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    // _micBloc = context.bloc<YBDMicBloc>();
    int gap = 70;
    return Container(
      height: ScreenUtil().setWidth(200),
      padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
      decoration: BoxDecoration(
        //背景Colors.transparent 透明
        color: Colors.transparent,
        //设置四周圆角 角度
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(4.0),
          topRight: Radius.circular(4.0),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _muteItem(),
          _lockType != 0 && _muteType != 0
              ? SizedBox(
                  width: ScreenUtil().setWidth(gap),
                )
              : Container(),
          _lockMicItem(),
          _dropMic
              ? SizedBox(
                  width: ScreenUtil().setWidth(gap),
                )
              : Container(),
          _micDropItem(),
          _operationType != 0
              ? SizedBox(
                  width: ScreenUtil().setWidth(gap),
                )
              : Container(),
          _micItem(),
          _showProfile
              ? SizedBox(
                  width: ScreenUtil().setWidth(gap),
                )
              : Container(),
          _profileItem(),
          _showMicFrame
              ? SizedBox(
                  width: ScreenUtil().setWidth(gap),
                )
              : Container(),
          _micFrameItem(),
        ],
      ),
    );
  }
  void myBuild06f5Moyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///上下麦
  Widget _micItem() {
    return _operationType != 0
        ? _operationItem(
            _operationType == 2
                ? 'assets/images/liveroom/icon_drop_mic@2x.webp'
                : 'assets/images/liveroom/icon_take_mic@2x.webp',
            _operationType == 2 ? translate('drop_mic') : translate('take_mic'),
            0,
          )
        : Container();
  }
  void _micItemfNCq3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///强制下麦  主播权限
  Widget _micDropItem() {
    return _dropMic
        ? _operationItem(
            'assets/images/liveroom/icon_drop_mic@2x.webp',
            translate('drop_mic'),
            4,
          )
        : Container();
  }

  ///静音按钮
  Widget _muteItem() {
    return _muteType != 0
        ? _operationItem(
            _muteType == 2
                ? 'assets/images/liveroom/icon_mic@2x.webp'
                : 'assets/images/liveroom/icon_mute_mic2@2x.webp',
            _muteType == 1 ? translate('mute') : translate('unmute'),
            1,
          )
        : Container();
  }
  void _muteItemzc2hToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///索麦按钮
  Widget _lockMicItem() {
    return _lockType != 0
        ? _operationItem(
            _lockType == 2
                ? 'assets/images/liveroom/icon_unlock_mic@2x.webp'
                : 'assets/images/liveroom/icon_lock_mic@2x.webp',
            _lockType == 1 ? translate('lock_mic') : translate('unlock'),
            3,
          )
        : Container();
  }

  ///profile按钮
  Widget _profileItem() {
    return _showProfile
        ? _operationItem(
            'assets/images/liveroom/icon_profile@2x.webp',
            translate('profile'),
            2,
          )
        : Container();
  }
  void _profileItemOEn2aoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///micFrame按钮
  Widget _micFrameItem() {
    return _showMicFrame
        ? _operationItem(
            'assets/images/liveroom/icon_mic_frame@2x.webp',
            translate('mic_frames'),
            5,
          )
        : Container();
  }

  ///麦位操作  图标、描述、操作类型type:0 上下麦   1静音  2profile  3锁麦、解除锁麦  4、强制下麦  5、micFrame
  Widget _operationItem(String iconUrl, String description, int type) {
    return GestureDetector(
      onTap: () {
        logger.i('operationType: $_operationType, talentFlag: $_talentFlag, type: $type, userId: ${widget.userId}');

        if (widget.pkStateBloc!.current == PkState.Matching) {
          YBDToastUtil.toast(translate('not_able'));
          return;
        }

        if (type == 0) {
          if (_operationType == 2) {
            widget.micOperaterLister(
              widget.roomId,
              _operationType,
              widget.micBean!.userId,
              widget.micBean!.micIndex,
            );
          } else if (_operationType == 1) {
            widget.micOperaterLister(
              widget.roomId,
              _operationType,
              int.parse(widget.userId),
              widget.micBean!.micIndex,
            );
          }
          Navigator.pop(context);
        } else if (type == 1) {
          widget.micMuteLister(
            widget.micBean!.micIndex,
            widget.micBean!.userId,
            -1,
            !widget.micBean!.mute!,
          );
          Navigator.pop(context);
        } else if (type == 2) {
          //别人在麦位上
          Navigator.pop(context);
          YBDRoomUtil.showMiniProfileDialog(
            roomId: widget.roomId,
            userId: widget.micBean!.userId,
          );
        } else if (type == 3) {
          if (!widget.micBean!.lock!) {
            BlocProvider.of<YBDMicBloc>(context).micOperation(
              widget.roomId,
              5,
              int.parse(widget.userId),
              widget.micBean!.micIndex,
            );
          } else {
            BlocProvider.of<YBDMicBloc>(context).micOperation(
              widget.roomId,
              6,
              int.parse(widget.userId),
              widget.micBean!.micIndex,
            );
          }
          Navigator.pop(context);
        } else if (type == 4) {
          BlocProvider.of<YBDMicBloc>(context).micOperation(
            widget.roomId,
            3,
            widget.micBean!.userId,
            widget.micBean!.micIndex,
          );
          Navigator.pop(context);
        } else if (type == 5) {
          ///TODO playCent 麦位
          Navigator.pop(context);
          YBDRoomUtil.showPlayCenterSheet(
            widget.roomId,
            defaultTab: 3,
          );
        }
      },
      child: Material(
        child: Container(
          width: ScreenUtil().setWidth(150),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                iconUrl,
                width: ScreenUtil().setWidth(80),
                height: ScreenUtil().setWidth(80),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(16),
              ),
              Text(
                description,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  color: Colors.black.withOpacity(0.5),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void _operationItemr81G7oyelive(String iconUrl, String description, int type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
