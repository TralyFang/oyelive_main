import 'dart:async';


import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart' as user;
import 'package:oyelive_main/ui/page/room/entity/mic_ybd_item.dart';
import 'package:oyelive_main/ui/page/room/entity/room_ybd_operate_info.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';

class YBDMicOperateHelper {
  static YBDMicOperateHelper? _instance;

  Future _init() async {
    _instance = await YBDMicOperateHelper.getInstance();
  }
  void _initDjvgvoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  YBDMicOperateHelper._();
  static YBDMicOperateHelper? getInstance() {
    if (_instance == null) {
      var instance = YBDMicOperateHelper._();
      _instance = instance;
    }
    return _instance;
  }

  ///status ==1 表示自己禁麦了
  String? status = '';

  bool isBroad = false;

  int volume = 100;

  List<YBDMicItem?> micItem = [];

  ///记录自己麦位状态就好
  reloadMuteInfo(List<YBDMicItem?> micItems) {
    this.micItem = micItems;
    if (micItems.isEmpty) return;
    YBDRoomOperateInfo info = YBDCommonUtil.getRoomOperateInfo();
    logger.v('start optimize mute : ${info.muteChangeRole} ${micItems.isEmpty}');
    if (info.muteChangeRole != '1') return;

    ///默认不退房 这个配置项无法改变
    user.YBDUserInfo? userInfo = YBDCommonUtil.storeFromContext()!.state.bean;
    List<YBDMicItem?> list =
        micItems.where((element) => (element!.userId != null && element.userId == userInfo!.id)).toList();
    logger.v('start optimize mute 1: ${info.muteChangeRole} ${list}');
    this.isBroad = list.isNotEmpty;
    if (list.isEmpty) return;
    YBDMicItem? item = list.first;
    logger.v('start optimize mute 1: ${info.muteChangeRole} ${list} ${item?.attributes?.muteInfo?.status} '
        'status: ${this.status} '
        'meteuserid:${item?.attributes?.muteInfo?.muteUserId} '
        'micuserid:${item?.userId} '
        'currentuserid:${userInfo!.id.toString()}');
    if (item != null && item.attributes?.muteInfo != null) {
      ///与服务端状态一致 就什么都不操作
      if (this.status == item.attributes?.muteInfo?.status) return;

      ///state静音状态：0 - 非静音  1- 静音
      this.status = item.attributes?.muteInfo?.status;
      YBDLiveService.instance.getEngine()!.setClientRole(this.status == '1' ? ClientRole.Audience : ClientRole.Broadcaster);
      logger.v(
          'start optimize mute change role: ${this.status} ${item.attributes?.muteInfo?.status} ${this.status == '1' ? 'ClientRole.Audience' : 'ClientRole.Broadcaster'}');
      // YBDToastUtil.toast('${this.status == '1' ? 'ClientRole.Audience' : 'ClientRole.Broadcaster'}');
    } else {
      ///解除禁言后  直接返回null
      if (this.status == '1') {
        //如果本地记录状态是禁言，此时如果自己还在麦位上就切换成主播
        YBDLiveService.instance.getEngine()!
            .setClientRole(item!.userId == userInfo.id ? ClientRole.Broadcaster : ClientRole.Audience);
        logger.v(
            'start optimize mute change role 1: ${this.status} ${item.attributes?.muteInfo?.status} ${item.userId == userInfo.id ? 'ClientRole.Broadcaster' : 'ClientRole.Audience'}');
        // YBDToastUtil.toast('else:${item.userId == userInfo.id ? 'ClientRole.Broadcaster' : 'ClientRole.Audience'}');
      }

      ///如果是不包含 可能就是主动下麦或者不在房间
      this.status = '';
    }
  }

  ///本地音频调节是更新状态
  reloadMuteWithVolumn(int volume) {
    logger.v('start mute local volume: ${this.isBroad} ${this.volume}');
    if (!this.isBroad) return;
    YBDRoomOperateInfo info = YBDCommonUtil.getRoomOperateInfo();
    logger.v('start mute local volume : ${this.isBroad} ${this.volume}');
    if (info.muteChangeRole != '1') return;

    ///默认不退房 这个配置项无法改变
    // has mute by other
    if (this.status == '1') return;
    if (volume == 0) {
      //主播 禁掉本地音频
      this.volume = 0;
      YBDLiveService.instance.getEngine()!.setClientRole(ClientRole.Audience);
      logger.v('start mute local volume change audience: ${this.isBroad} ${this.volume}');
      // YBDToastUtil.toast('else:ClientRole.Audience');
      return;
    }
    if (this.volume == 0) {
      //本地音频变化时 更新  避免连续触发，只需要临界值触发
      this.volume = 100;
      YBDLiveService.instance.getEngine()!.setClientRole(ClientRole.Broadcaster);
      logger.v('start mute local volume change broadcaster: ${this.isBroad} ${this.volume}');
      // YBDToastUtil.toast('else:ClientRole.Broadcaster');

    }
  }

  /// cheak out mic legal
  checkMicLegal(ClientRole previous, ClientRole current) {
    // YBDToastUtil.toast('change role: ${current == ClientRole.Broadcaster ? "Broadcaster" :"Audience"}');
    if (current == ClientRole.Audience) return;
    logger.v('yourself has change role Broadcaster');
    user.YBDUserInfo? userInfo = YBDCommonUtil.storeFromContext()!.state.bean;
    List<YBDMicItem?> list =
        this.micItem.where((element) => (element!.userId != null && element.userId == userInfo!.id)).toList();
    logger.i('current mic list: $list');
    if (list.isNotEmpty) return;
    YBDLiveService.instance.getEngine()!.setClientRole(ClientRole.Audience);
    // YBDToastUtil.toast('change role spirit mic: Audience');
    logger.v('yourself has change role Audience');
  }

  cleanStatus() {
    this.status = '';
    this.volume = 100;
  }
}
