import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:sprintf/sprintf.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import '../../../../../common/util/common_ybd_util.dart';
import '../../../../../common/util/dialog_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/numeric_ybd_util.dart';
import '../../../../../common/util/sp_ybd_util.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../module/agency/agency_ybd_api_helper.dart';
import '../../../../../module/user/entity/agreements_ybd_entity.dart';
import '../../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../../redux/app_ybd_state.dart';

/// 开播页面工具类
class YBDStartLiveUtil {
  /// 检查开播用户是否能设置私密房间
  static bool checkPrivateRoomPermission(YBDUserInfo? talentInfo) {
    if (null == talentInfo) {
      return false;
    }

    if (null != talentInfo.vip && talentInfo.vip! > 0) {
      return true;
    } else {
      // TODO: 翻译
      // 提示 vip 才能使用私密房间的功能
      // 修改私密房创建提示toast文案
      YBDToastUtil.toast('Only COUNT or above can create a private room.');
      return false;
    }
  }

  /// 检查房间密码有效
  static bool checkPwd(String pwd) {
    if (null == pwd) {
      YBDToastUtil.toast("Password shouldn't be Empty!");
      return false;
    }

    // 私密房间检查密码
    if (pwd.isEmpty) {
      // TODO: 翻译
      YBDToastUtil.toast("Password shouldn't be Empty!");
      return false;
    } else if (pwd.trim().isEmpty) {
      // TODO: 翻译
      YBDToastUtil.toast("Password shouldn't be just Spaces!");
      return false;
    } else {
      return true;
    }
  }

  /// 检查房间标题
  static bool checkRoomTitle(String roomTitle) {
    if (roomTitle.isEmpty) {
      // 房间标题不能为空
      // TODO: 翻译
      YBDToastUtil.toast('Please set a title for room!');
      return false;
    } else {
      return true;
    }
  }

  /// 检查开播条件
  /// true 可以开播，false 不能开播
  /// [onLater] 稍后上传海报的回调函数
  /// [onNow] 稍后完善主播信息的回调函数
  static Future<bool> checkGoLiveCondition(
    BuildContext context, {
    VoidCallback? onLater,
    VoidCallback? onNow,
  }) async {
    // 当前用户信息
    YBDUserInfo userInfo = (await YBDSPUtil.getUserInfo()) ?? YBDUserInfo();

    // 查询签约主播状态
    YBDAgreementsEntity? agreementsEntity = await YBDAgencyApiHelper.queryTalentInfo(
      context,
      userInfo.id,
      'user',
      needErrorToast: false,
    );

    // 是否为签约主播
    bool isOfficial = YBDAgencyApiHelper.isOfficial(agreementsEntity);

    // 是否有主播海报
    bool haveCover = _checkTalentCover(
      context,
      userInfo,
      laterCallback: onLater,
      nowCallback: onNow,
    );

    // 用户等级是否能开播
    bool haveLevel = false;

    // 是否有主播信息
    bool haveInfo = false;

    if (isOfficial) {
      // 签约主播检查主播资料
      haveInfo = _checkTalentInfo(
        context,
        agreementsEntity,
        laterCallback: haveCover ? onLater : null,
        nowCallback: onNow,
      );
    } else {
      // 非签约主播检查用户等级
      haveLevel = _checkUserLevel(context, userInfo);
    }

    // 签约主播
    if (((isOfficial && haveInfo) || haveLevel) && haveCover) {
      return true;
    } else {
      return false;
    }
  }

  /// 检查是否为签约主播
  static bool _checkTalentInfo(
    BuildContext context,
    YBDAgreementsEntity? agreementsEntity, {
    VoidCallback? laterCallback,
    VoidCallback? nowCallback,
  }) {
    if (agreementsEntity?.data?.status == 'complete') {
      logger.v('talent info complete');
      return true;
    } else if (agreementsEntity?.data?.status == 'perfect') {
      logger.v('show complete talent info dialog');
      YBDDialogUtil.showCompleteNowDialog(
        context,
        onPressedLater: laterCallback,
        onPressedComlete: nowCallback,
      );
      return false;
    } else {
      logger.v('other talent status');
      return false;
    }
  }

  /// 检查用户等级是否能够开播
  /// true 用户等级能够开播, false 用户等级不够开播
  static bool _checkUserLevel(BuildContext context, YBDUserInfo userInfo) {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;

    // 用户等级
    int uLevelGoLive = YBDNumericUtil.stringToInt(store.state.configs?.ulevelGoLive ?? '3');

    // 主播等级
    int tLevelGoLive = YBDNumericUtil.stringToInt(store.state.configs?.tlevelGoLive ?? '3');

    if (userInfo.roomlevel! >= tLevelGoLive || userInfo.level! >= uLevelGoLive) {
      return true;
    } else {
      logger.v('room level or user level is too low');
      // 弹框提示：等级低，不能开播
      YBDDialogUtil.showEditInfoAlertDialog(
        context,
        sprintf(translate('go_live_level_limit'), [uLevelGoLive, tLevelGoLive]),
      );
      return false;
    }
  }

  /// 检查主播海报
  /// true 主播有海报, false 主播没有海报
  static bool _checkTalentCover(
    BuildContext context,
    YBDUserInfo userInfo, {
    VoidCallback? laterCallback,
    VoidCallback? nowCallback,
  }) {
    if ((null == userInfo.roomimg || userInfo.roomimg!.isEmpty)) {
      logger.v('room image is empty');
      Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;

      // 是否显示 Later 按钮，可点击跳过
      bool showLater = (store.state.configs?.goLiveCover ?? '1') == '0';
      YBDDialogUtil.hideLoading(context);
      YBDDialogUtil.hideLoading(context);
      YBDCommonTrack().alertExpose('upload_cover');
      // 弹框提示：房间海报为空
      YBDDialogUtil.showEditInfoAlertDialog(
        context,
        translate('upload_cover'),
        goSettings: true,
        showLater: showLater,
        onPressedLater: () {
          if (null != laterCallback) {
            laterCallback();
          }
        },
        onPressConfirm: () {
          if (null != nowCallback) {
            nowCallback();
          }
        },
      );
      return false;
    } else {
      return true;
    }
  }
}
