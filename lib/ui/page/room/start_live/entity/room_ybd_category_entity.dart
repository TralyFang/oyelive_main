import 'dart:async';


import '../../../../../generated/json/base/json_convert_content.dart';

class YBDRoomCategoryEntity with JsonConvert<YBDRoomCategoryEntity> {
  List<YBDRoomCategoryItem?>? items;
}

class YBDRoomCategoryItem with JsonConvert<YBDRoomCategoryItem> {
  bool? selected;
  String? title;
  String? img;
  int? index;
}
