import 'dart:async';


import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/set_ybd_ext.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/room_socket/message/common/start_ybd_live_msg.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/bloc/room_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_event.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/room_socket/message/common/enter.dart';
import '../../../../common/room_socket/message/common/internal.dart';
import '../../../../common/room_socket/message/common/super_ybd_fans_message.dart';
import '../../../../common/room_socket/message/common/user_ybd_publish.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';
import '../../../widget/my_ybd_app_bar.dart';
import '../../store/widget/round_ybd_checkbox.dart';
import '../notice/notice_ybd_type.dart';
import '../room_ybd_helper.dart';
import '../service/error_ybd_event.dart';
import '../service/join_ybd_channel_success_event.dart';
import '../service/live_ybd_service.dart';
import '../service/message_ybd_event.dart';
import '../widget/normal_ybd_dialog.dart';
import 'bloc/start_ybd_live_bloc.dart';
import 'entity/room_ybd_category_entity.dart';
import 'util/start_ybd_live_util.dart';
import 'widget/become_ybd_official_talent_btn.dart';
import 'widget/select_ybd_room_category.dart';
import 'widget/start_ybd_live_info.dart';
import 'package:collection/collection.dart';

class YBDStartLivePage extends StatefulWidget {
  ///首次进入主播页附带功能
  final int? funcType;

  const YBDStartLivePage({Key? key, this.funcType}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _YBDStartLivePageState();
}

class _YBDStartLivePageState extends BaseState<YBDStartLivePage> {
  /// 房间标题输入框控制器
  TextEditingController _roomTitleController = TextEditingController();

  /// 密码输入框控制器
  TextEditingController _pwdController = TextEditingController();

  bool? selected;

  /// 页面 bloc
  late YBDStartLiveBloc _bloc;

  /// 订阅直播事件
  StreamSubscription<YBDLiveEvent>? _liveEventSubscription;

  @override
  void initState() {
    super.initState();
    _listenLiveEvent();
  }
  void initStateJaLQEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _bloc.close();
    _liveEventSubscription?.cancel();
    _liveEventSubscription = null;
    super.dispose();
  }
  void dispose5R5zGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<YBDStartLiveBloc>(
          create: (context) {
            _bloc = YBDStartLiveBloc(context);
            _bloc.add(StartLiveEvent.Init);
            return _bloc;
          },
        ),
      ],
      child: Scaffold(
        appBar: YBDMyAppBar(
          title: Text(
            translate('create_studio'),
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(32),
            ),
          ),
          backgroundColor: YBDTPStyle.heliotrope,
          leading: Container(
            width: ScreenUtil().setWidth(88),
            height: ScreenUtil().setWidth(88),
            child: TextButton(
              onPressed: () async {
                logger.v('clicked back button');
                Navigator.pop(context);
                final userId = await YBDUserUtil.userIdInt();
                YBDLiveService.instance.leaveRoom(
                  roomId: userId,
                  // roomType: RoomPageType.Talent,
                );
              },
              child: Icon(Icons.arrow_back, color: Colors.white),
            ),
          ),
          elevation: 0.0,
        ),
        body: GestureDetector(
          onTap: () {
            logger.v('clicked bg to hide keyboard');
            FocusScope.of(context).requestFocus((FocusNode()));
          },
          child: Stack(
            children: <Widget>[
              Container(
                height: ScreenUtil().screenHeight,
                width: ScreenUtil().screenWidth,
                decoration: YBDTPStyle.gradientDecoration,
              ),
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    //  成为签约主播按钮
                    YBDBecomeOfficialTalentBtn(),
                    YBDStartLiveInfo(
                      roomTitleController: _roomTitleController,
                      pwdController: _pwdController,
                    ),
                    YBDSelectRoomCategory(),
                    SizedBox(height: ScreenUtil().setWidth(20)),
                    // 三级以下限制
                    _joinMicLimit(),
                    SizedBox(height: ScreenUtil().setWidth(20)),
                    // 开播按钮
                    _goLiveBtn(),
                    SizedBox(height: ScreenUtil().setWidth(60)),
                    _termsRow(),
                    SizedBox(height: ScreenUtil().setWidth(20)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void myBuildrbOiCoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 条款段落
  Widget _termsRow() {
    return YBDDelayGestureDetector(
      onTap: () {
        /// 点击条款按钮的响应事件
        logger.v("clicked Terms & Privacy Policy button");
        String title = Uri.encodeComponent(translate('terms'));
        YBDNavigatorHelper.navigateToWeb(
          context,
          "?url=${Uri.encodeComponent(YBDCommonUtil.getTermsURL)}&title=$title",
        );
      },
      child: Container(
        height: ScreenUtil().setWidth(70),
        width: double.infinity,
        decoration: BoxDecoration(color: Colors.transparent),
        child: Center(
          child: Text(
            // TODO: 翻译
            'By creating a room, you agree to Terms & Conditions ',
            style: TextStyle(
              color: Colors.white.withOpacity(0.7),
              fontSize: ScreenUtil().setSp(22),
            ),
          ),
        ),
      ),
    );
  }
  void _termsRowBd05Soyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _joinMicLimit() {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    String level = store.state.configs?.micRequestUserLevel ?? '3';

    if (level == '-1') {
      // -1  不显示 选择框
      return Container();
    }

    return BlocBuilder<YBDStartLiveBloc, YBDStartLiveBlocState>(
      builder: (context, state) {
        print("state.micRequestEnable: ${state.micRequestEnable}");
        return Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              YBDRoundCheckBox(
                  value: (state.micRequestEnable == 0),
                  onChanged: (check) {
                    setState(() {
                      if (check) {
                        _bloc.micEnable = 0;
                      } else {
                        _bloc.micEnable = 1;
                      }
                      _bloc.add(StartLiveEvent.YBDSelectRoomCategory);
                      print("state.micRequestEnable: ${state.micRequestEnable} check:$check");
//                      state.micRequestEnable = (state.micRequestEnable == 1) ? 0 : 1;
                    });
                  }),
              SizedBox(width: ScreenUtil().setWidth(1)),
              Container(
                child: Text(
                  translate('Allow below Level $level users to join as guest'),
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.7),
                    fontSize: ScreenUtil().setSp(24),
//                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(30)),
            ],
          ),
        );
      },
    );
  }
  void _joinMicLimitj4XTtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 开播按钮段落
  Widget _goLiveBtn() {
    return BlocBuilder<YBDStartLiveBloc, YBDStartLiveBlocState>(
      builder: (context, state) {
        return Container(
          width: ScreenUtil().setWidth(440),
          height: ScreenUtil().setWidth(72),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
            borderRadius: BorderRadius.all(Radius.circular(18)),
          ),
          child: Material(
            type: MaterialType.transparency,
            child: InkWell(
              borderRadius: BorderRadius.all(Radius.circular(18)),
              onTap: () async {
                logger.v('clicked go live btn');
                _clickGoLiveAction(state);
              },
              child: Center(
                child: Text(
                  translate('go_live_now'),
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(32),
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
  void _goLiveBtnEWabUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击开播按钮的响应方法
  _clickGoLiveAction(YBDStartLiveBlocState state) async {
    YBDDialogUtil.showLoading(context);
    // 当前用户信息
    Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    YBDRoomCategoryItem? selectedItem = state.categories?.firstWhereOrNull(
      (element) => element!.selected!,
      /*orElse: () => null,*/
    );

    // 检查房间标题
    if (!YBDStartLiveUtil.checkRoomTitle(_roomTitleController.text)) {
      logger.v('room title is null');
      YBDDialogUtil.hideLoading(context);
      return;
    }

    // 私密房间
    if (!state.isPublic!) {
      // 检查开播用户是否能设置私密房间
      if (!YBDStartLiveUtil.checkPrivateRoomPermission(store!.state.bean)) {
        logger.v('user can not set private room');
        YBDDialogUtil.hideLoading(context);
        return;
      }

      // 检查密码是是否为空
      if (!YBDStartLiveUtil.checkPwd(_pwdController.text)) {
        logger.v('room pwd is empty : ${_pwdController.text}');
        YBDDialogUtil.hideLoading(context);
        return;
      }
    }

    // 检查开播条件
    bool result = await YBDStartLiveUtil.checkGoLiveCondition(context, onLater: () {
      logger.v('on later');

      // 进入房间页面前显示加载框
      // 隐藏对话框后再隐藏加载框
      // YBDDialogUtil.hideLoading(context);

      // 开播
      _subscribeRoom(state);
    }, onNow: () {
      // 不开播，先完善开播条件
      logger.v('on now');

      // 隐藏对话框后再隐藏加载框
      YBDDialogUtil.hideLoading(context);
    });

    if (result) {
      logger.v('subscribe room after checked condition');
      // 进入房间页面前显示加载框
      // 符合开播条件隐藏加载框
      // YBDDialogUtil.hideLoading(context);

      // 开播埋点  todo 计时
      // YBDTATrack().timeEvent(YBDTATrackEvent.create_liveroom_click);

      // 开播
      _subscribeRoom(state);
    } else {
      // 不符合开播有其他弹框，加载框显示在其他弹框后面（执行 pop 会隐藏加载框上层的弹框）
      logger.v('start live condition not satisfied');
    }
  }

  /// 监听直播事件
  void _listenLiveEvent() {
    // 是否能够进房, 默认可以进房
    _liveEventSubscription = YBDLiveService.instance.onEvent.listen((event) {
      // logger.v('_listenLiveEvent: $event');
      if (event is YBDJoinChannelSuccessEvent) {
        enterRoom();
      } else if (event is YBDMessageEvent) {
        // 开播页面的消息用于房间刷新页面
        logger.v('_listenLiveEvent:YBDMessageEvent $event');
        if (event.message is YBDEnter || event.message is YBDSuperFansMessage || event.message is YBDUserPublish) {
          if (event.message is YBDEnter) {
            logger.v('enter _listenLiveEvent:YBDMessageEvent ${event}, ${StackTrace.current}');
          }
          YBDLiveService.instance.data.startPageMsgList.add(event.message);
        }

        if (event.message is YBDInternal) {
          final msg = event.message as YBDInternal;

          if (msg.type == YBDNoticeType.TYPE_LIMIT) {
            YBDDialogUtil.hideLoading(context);
            logger.w('create room talent limited desc : ${msg.content}');
            YBDDialogUtil.showNormalDialog(context, type: NormalDialogType.one, barrierDismissible: false, okCallback: () {
              Navigator.pop(YBDLiveService.instance.mainContext!);
            }, content: '${msg.content}', ok: 'OK', autoConfirmDuration: Duration(seconds: 5));
          }
        }

        // 开播回调处理， 非000000 全改成toast
        // agora engine creat success 不处理任何业务，放在server 回调中处理， engine init fail 直接忽略
        if (event.message is YBDStartLiveMsg) {
          logger.v('_listenLiveEvent:YBDStartLiveMsg $event');
          final msg = event.message as YBDStartLiveMsg;
          YBDLiveService.instance.enterRoomEnable = msg.code == Const.HTTP_SUCCESS;
          if (msg.code != Const.HTTP_SUCCESS) {
            YBDDialogUtil.hideLoading(context);
            YBDToastUtil.toast('${msg.desc}');
            return;
          }
          // enterRoom();
          YBDRoomCategoryItem selectedItem = _bloc.state.categories?.firstWhere((element) => element?.selected ?? false) ?? YBDRoomCategoryItem();
          YBDTATrack().trackEvent(YBDTATrackEvent.create_liveroom_click,
              prop: YBDTAProps(
                name: _roomTitleController.text,
                type: (_bloc.state.isPublic ?? true) ? 'public' : 'private',
                extParams: {
                  'room_category': selectedItem.title ?? 'Standard',
                  'status': (_bloc.state.micRequestEnable ?? 0) == 0,
                },
              ));
        }
        // YBDDialogUtil.hideLoading(context);
      } else if (event is YBDErrorEvent) {
        // 重启引擎
        // await YBDLiveService.instance.restartEngine();
        YBDLogUtil.e('create room sdk error : ${event.error}');
        // YBDToastUtil.toast(translate('unknown_error'));
        // logger.e(translate('unknown_error'));
      }
    });
  }
  void _listenLiveEventERAauoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 跳转房间
  void enterRoom() {
    YBDRoomCategoryItem selectedItem = _bloc.state.categories?.firstWhere((element) => element?.selected ?? false) ?? YBDRoomCategoryItem();
    logger.v('_listenLiveEvent: funcType: ${widget.funcType}'.subLog(step: 200, log: 'YBDJoinChannelSuccessEvent'));
    // 跳转到房间
    YBDRoomHelper.enterFromStartPage(this.context, tag: '${selectedItem.index}', funcType: widget.funcType);
  }

  /// 订阅房间
  void _subscribeRoom(YBDStartLiveBlocState state) async {
    // final userId = await YBDUserUtil.userIdInt();
    // 开播时记录 roomId，作为加入频道的判断条件
    YBDLiveService.instance.data.roomId = YBDCommonUtil.storeFromContext()!.state.bean!.id;

    YBDRoomCategoryItem? selectedItem = state.categories?.firstWhereOrNull(
      (element) => element!.selected!,
      /*orElse: () => null,*/
    );
    logger.v("start live page subscribeRoom".subLog(step: 1));
    YBDTATrack().timeEvent(YBDTATrackEvent.create_liveroom_click);
    await YBDLiveService.instance.createRoom(
      pwd: (state.isPublic ?? false) ? '' : _pwdController.text,
      title: _roomTitleController.text,
      category: '${selectedItem?.index ?? 0}',
      micEnable: state.micRequestEnable,
    );
  }
  void _subscribeRoomLwDlsoyelive(YBDStartLiveBlocState state)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// category  String to int
  int getCategory(String categoryText) {
    int category = 0;
    switch (categoryText) {
      case 'Standard':
        category = 0;
        break;
      case 'Special':
        category = 1;
        break;
      case 'Friends':
        category = 2;
        break;
      case 'Party':
        category = 3;
        break;
      case 'Heart':
        category = 4;
        break;
      case 'Exclusive':
        category = 5;
        break;
      case 'Royal':
        category = 6;
        break;
    }
    logger.v('getCategory categoryText:$categoryText  category:$category');
    return category;
  }
}
