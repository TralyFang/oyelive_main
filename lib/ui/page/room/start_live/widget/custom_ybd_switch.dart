import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 自定义的开关控件
/// 显示开关状态文字
class YBDCustomSwitch extends StatefulWidget {
  final bool? value;
  final ValueChanged<bool?>? onChanged;

  YBDCustomSwitch({
    Key? key,
    this.value = true,
    this.onChanged,
  });

  @override
  _YBDCustomSwitchState createState() => _YBDCustomSwitchState();
}

class _YBDCustomSwitchState extends State<YBDCustomSwitch> with SingleTickerProviderStateMixin {
  late Animation _circleAnimation;
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
      begin: widget.value! ? Alignment.centerRight : Alignment.centerLeft,
      end: widget.value! ? Alignment.centerLeft : Alignment.centerRight,
    ).animate(CurvedAnimation(parent: _animationController, curve: Curves.linear));
  }
  void initStateOsC27oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    // 开关背景颜色
    BoxDecoration bgDecoration = widget.value!
        ? BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white.withOpacity(0.3),
          )
        : BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            gradient: LinearGradient(
              colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          );

    // 开关圆形按钮颜色
    // 原型图按钮是白色 #682
    BoxDecoration circleDecoration = BoxDecoration(
      shape: BoxShape.circle,
      color: Colors.white,
    );

    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            widget.onChanged!(widget.value);
          },
          child: Container(
            height: ScreenUtil().setWidth(60),
            decoration: BoxDecoration(
              color: Colors.transparent,
            ),
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(14)),
            child: Container(
              width: ScreenUtil().setWidth(96),
              height: ScreenUtil().setWidth(32),
              decoration: bgDecoration,
              child: Padding(
                padding: EdgeInsets.all(ScreenUtil().setWidth(4)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    _circleAnimation.value == Alignment.centerRight
                        ? Padding(
                            padding: EdgeInsets.only(
                              left: ScreenUtil().setWidth(8),
                              // right: ScreenUtil().setWidth(8),
                            ),
                            child: Text(
                              // TODO: 翻译
                              'Public',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: ScreenUtil().setSp(16),
                              ),
                            ),
                          )
                        : Container(),
                    Align(
                      alignment: _circleAnimation.value,
                      child: Container(
                        width: ScreenUtil().setWidth(25),
                        height: ScreenUtil().setWidth(25),
                        decoration: circleDecoration,
                      ),
                    ),
                    _circleAnimation.value == Alignment.centerRight
                        ? Container()
                        : Padding(
                            padding: EdgeInsets.only(
                              left: ScreenUtil().setWidth(4),
                              right: ScreenUtil().setWidth(5),
                            ),
                            child: Text(
                              // TODO: 翻译
                              'Private',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: ScreenUtil().setSp(16),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
  void buildABfHloyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
