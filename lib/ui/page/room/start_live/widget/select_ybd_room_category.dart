import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

import '../../../../../common/util/log_ybd_util.dart';
import '../bloc/start_ybd_live_bloc.dart';
import '../entity/room_ybd_category_entity.dart';
import 'package:collection/collection.dart';

/// 开播页面选择房间类型
class YBDSelectRoomCategory extends StatefulWidget {
  @override
  _YBDSelectRoomCategory createState() => _YBDSelectRoomCategory();
}

class _YBDSelectRoomCategory extends State<YBDSelectRoomCategory> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<YBDStartLiveBloc, YBDStartLiveBlocState>(builder: (context, state) {
      YBDRoomCategoryItem? selectedItem = state.categories?.firstWhereOrNull((element) => element!.selected!, /*orElse: () => null*/);

      return Container(
        width: ScreenUtil().screenWidth,
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            color: Colors.white.withOpacity(0.1),
          ),
          child: Column(
            children: <Widget>[
              SizedBox(height: ScreenUtil().setWidth(40)),
              Text(
                // TODO: 翻译
                'Select Category',
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(28),
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(40)),
              Container(
                child: null != state.categories
                    ? Wrap(
                        runSpacing: ScreenUtil().setWidth(30),
                        spacing: ScreenUtil().setWidth(42),
                        children: List.generate(state.categories?.length ?? 0, (index) {
                          return GestureDetector(
                            onTap: () {
                              logger.v('selected category : ${state.categories![index]}');
                              context.read<YBDStartLiveBloc>().selectedRoomCategory(state.categories![index]);
                            },
                            child: _categoryItem(state.categories![index]!),
                          );
                        }),
                      )
                    : Container(),
              ),
              SizedBox(height: ScreenUtil().setWidth(30)),
              Container(
                // 房间麦位预览
                height: ScreenUtil().setWidth(320),
                child: YBDImage(
                  path: selectedItem?.img ?? '',
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(30)),
            ],
          ),
        ),
      );
    });
  }
  void buildV6hSVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _categoryItem(YBDRoomCategoryItem item) {
    if (item.selected!) {
      return Container(
        width: ScreenUtil().setWidth(120),
        height: ScreenUtil().setHeight(50),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
          borderRadius: BorderRadius.all(Radius.circular(4)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              blurRadius: 1,
              offset: Offset(0, ScreenUtil().setWidth(2)),
            )
          ],
        ),
        child: Center(
          child: Text(
            item.title!,
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(24),
            ),
          ),
        ),
      );
    } else {
      return Container(
        width: ScreenUtil().setWidth(120),
        height: ScreenUtil().setHeight(50),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(1)),
          borderRadius: BorderRadius.all(Radius.circular(4)),
        ),
        child: Center(
          child: Text(
            item.title!,
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(24),
            ),
          ),
        ),
      );
    }
  }
  void _categoryItem1r590oyelive(YBDRoomCategoryItem item) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
