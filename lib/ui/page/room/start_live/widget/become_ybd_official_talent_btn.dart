import 'dart:async';


import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux/redux.dart';

import '../../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../../common/util/common_ybd_util.dart';
import '../../../../../redux/app_ybd_state.dart';
import '../bloc/start_ybd_live_bloc.dart';

/// 成为签约主播的按钮
class YBDBecomeOfficialTalentBtn extends StatefulWidget {
  @override
  _YBDBecomeOfficialTalentBtnState createState() => _YBDBecomeOfficialTalentBtnState();
}

class _YBDBecomeOfficialTalentBtnState extends State<YBDBecomeOfficialTalentBtn> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<YBDStartLiveBloc, YBDStartLiveBlocState>(
      builder: (context, state) {
        if (state.showOfficialTalentBtn) {
          return Container(
            width: ScreenUtil().screenWidth,
            height: ScreenUtil().setWidth(100),
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    SizedBox(width: ScreenUtil().setWidth(30)),
                    Expanded(
                      child: Container(
                        height: ScreenUtil().setWidth(80),
                        decoration: BoxDecoration(
                          color: Color(0xffDDDDDD).withOpacity(0.1),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Material(
                          type: MaterialType.transparency,
                          child: InkWell(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            onTap: () {
                              final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
                              String applyAgencyUrl = store.state.configs?.talentSign ?? '';
                              // TODO: 测试代码 ios
                              // launch('http://www.oyetalk.live/event/OyeVoiceHost/index.html?portalType=201');
                              // http://192.168.50.64:8080/#/Info?index=1
                              // String url = Uri.encodeComponent('http://192.168.50.64:8080/#/earning');
                              // String url = Uri.encodeComponent('http://192.168.50.64:8080/#/Info');
                              // String url = Uri.encodeComponent('http://192.168.50.64:8080/#');
                              // String url = Uri.encodeComponent('http://192.168.50.64:8848/practice/index.html');
                              //String url = Uri.encodeComponent('http://192.168.50.64:8848/practice/c.html');
                              // String url = Uri.encodeComponent("http://www.baidu.com");
                              String url = Uri.encodeComponent(applyAgencyUrl);
                              // YBDNavigatorHelper.navigateTo(
                              //   context,
                              //   YBDNavigatorHelper.web_view_page + "/$url/${ad.adname}/${ad.img}",
                              // );
                              // TODO: 测试代码 ios
                              // YBDNavigatorHelper.navigateTo(
                              //   context,
                              //   YBDNavigatorHelper.flutter_web_view_page + "?url=$url&showNavBar=false",
                              // );
                              YBDNavigatorHelper.navigateToWeb(
                                context,
                                "?url=$url&showNavBar=false",
                              );
                            },
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: ScreenUtil().setWidth(40)),
                                Text(
                                  // TODO: 翻译
                                  'Become Official Talent',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: ScreenUtil().setSp(28),
                                  ),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(60)),
                                Container(
                                  width: ScreenUtil().setWidth(50),
                                  child: Transform.rotate(
                                    angle: math.pi,
                                    child: Image.asset(
                                      'assets/images/twinkling_star.webp',
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Expanded(child: Container()),
                                Container(
                                  width: ScreenUtil().setWidth(48),
                                  child: Image.asset(
                                    'assets/images/icon_more_grey.png',
                                    color: Colors.white,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(20)),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(30)),
                  ],
                ),
                Positioned(
                  top: ScreenUtil().setWidth(10),
                  left: ScreenUtil().setWidth(24),
                  child: Container(
                    width: ScreenUtil().setWidth(50),
                    child: Transform.rotate(
                      angle: math.pi * -0.4,
                      child: Image.asset(
                        'assets/images/twinkling_star.webp',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
  void build2xEs8oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
