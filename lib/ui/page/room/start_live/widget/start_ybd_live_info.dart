import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';

import '../../../../../common/util/image_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../../module/user/util/user_ybd_util.dart';
import '../bloc/start_ybd_live_bloc.dart';
import 'custom_ybd_switch.dart';

/// 填写开播信息的组件
class YBDStartLiveInfo extends StatefulWidget {
  /// 房间标题输入框控制器
  final TextEditingController? roomTitleController;

  /// 密码输入框控制器
  final TextEditingController? pwdController;

  YBDStartLiveInfo({this.roomTitleController, this.pwdController});

  @override
  _YBDStartLiveInfoState createState() => _YBDStartLiveInfoState();
}

class _YBDStartLiveInfoState extends State<YBDStartLiveInfo> {
  /// 当前用户的用户信息
  YBDUserInfo? _userInfo;

  @override
  void initState() {
    super.initState();
    YBDUserUtil.userInfo().then((value) {
      _userInfo = value;
      setState(() {});
    });
  }
  void initStatefjX1hoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    // 头像高度
    double avatarHeight = 154;

    // 添加标题栏高度
    double titleHeight = 58;

    /// 密码输入栏高度
    double pwdHeight = avatarHeight - titleHeight;

    return BlocBuilder<YBDStartLiveBloc, YBDStartLiveBlocState>(builder: (context, state) {
      print("YBDStartLiveInfo state.title:${state.title}");
      if (widget.roomTitleController?.text?.isEmpty ?? true) {
        widget.roomTitleController?.text = state.title ?? '';
      }
      return Container(
        width: ScreenUtil().screenWidth,
        padding: EdgeInsets.all(ScreenUtil().setWidth(30)),
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(24),
            vertical: ScreenUtil().setWidth(30),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
            color: Colors.white.withOpacity(0.1),
          ),
          child: Row(
            children: <Widget>[
              Container(
                // 主播海报
                width: ScreenUtil().setWidth(avatarHeight),
                height: ScreenUtil().setWidth(avatarHeight),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
                  child: YBDNetworkImage(
                    imageUrl: YBDImageUtil.cover(context, _userInfo?.roomimg, _userInfo?.id, scene: 'B'),
                    fit: BoxFit.cover,
                    placeholder: (context, url) => YBDResourcePathUtil.defaultMaleImage(male: _userInfo?.sex == 2),
                    width: ScreenUtil().setWidth(avatarHeight),
                    height: ScreenUtil().setWidth(avatarHeight),
                  ),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(20)),
              Expanded(
                child: Container(
                  height: ScreenUtil().setWidth(avatarHeight),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
                    color: Colors.white.withOpacity(0.1),
                  ),
                  child: Column(
                    children: <Widget>[
                      // 添加标题
                      _addTitleRow(titleHeight),
                      Container(
                        height: ScreenUtil().setWidth(pwdHeight),
                        child: Row(
                          children: <Widget>[
                            SizedBox(width: ScreenUtil().setWidth(20)),
                            Container(
                              // 公开或私密房间的开关
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  YBDCustomSwitch(
                                    value: state.isPublic,
                                    onChanged: (value) {
                                      context.read<YBDStartLiveBloc>().add(StartLiveEvent.UpdateRoomType);
                                    },
                                  ),
                                  SizedBox(height: ScreenUtil().setWidth(4)),
                                ],
                              ),
                            ),
                            SizedBox(width: ScreenUtil().setWidth(10)),
                            state.isPublic!
                                ?
                                // 隐藏密码输入框时的占位高度
                                Container(height: ScreenUtil().setWidth(pwdHeight))
                                :
                                // 密码输入栏
                                _inputPwdContainer(state, pwdHeight),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
  void buildMnN1Eoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 输入标题栏
  Widget _addTitleRow(double rowHeight) {
    return Container(
      height: ScreenUtil().setWidth(rowHeight),
      child: Row(
        children: <Widget>[
          SizedBox(width: ScreenUtil().setWidth(16)),
          Container(
            // 输入标题图标
            width: ScreenUtil().setWidth(26),
            alignment: Alignment.bottomCenter,
            child: Image.asset(
              'assets/images/liveroom/room_edit_icon.png',
              color: Colors.white,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(20)),
          Container(
            // 开播标题输入框
            width: ScreenUtil().setWidth(360),
            height: ScreenUtil().setWidth(rowHeight),
            child: TextField(
              // inputFormatters: [BlacklistingTextInputFormatter(RegExp("[ ]")), LengthLimitingTextInputFormatter(20)],
              inputFormatters: [FilteringTextInputFormatter.deny(RegExp("[ ]")), LengthLimitingTextInputFormatter(30)],
              cursorColor: Colors.white,
              controller: widget.roomTitleController,
              style: TextStyle(
                color: Colors.white,
                fontSize: ScreenUtil().setSp(24),
              ),
              decoration: InputDecoration(
                // TODO: 翻译
                hintText: 'Add a title to get more viewers',
                contentPadding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
                hintStyle: TextStyle(
                  color: Color(0xffE0E0E0),
                  fontSize: ScreenUtil().setSp(24),
                ),
                border: InputBorder.none,
              ),
            ),
          ),
        ],
      ),
    );
  }
  void _addTitleRowcU8p4oyelive(double rowHeight) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 密码输入栏
  Widget _inputPwdContainer(YBDStartLiveBlocState state, double rowHeight) {
    return Container(
      // 加密房间密码输入栏
      height: ScreenUtil().setWidth(rowHeight),
      width: ScreenUtil().setWidth(280),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          // 密码输入框和眼睛按钮
          _pwdInputRow(state),
          // 白色下划线
          Container(
            color: Colors.white.withOpacity(0.3),
            height: ScreenUtil().setWidth(1),
            width: ScreenUtil().setWidth(276),
          ),
          SizedBox(height: ScreenUtil().setWidth(18)),
        ],
      ),
    );
  }
  void _inputPwdContainerSqYL2oyelive(YBDStartLiveBlocState state, double rowHeight) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 密码输入框
  Widget _pwdInputRow(YBDStartLiveBlocState state) {
    double rowHeight = 60;
    return Row(
      children: <Widget>[
        Container(
          // 加密房间密码输入框
          height: ScreenUtil().setWidth(rowHeight),
          width: ScreenUtil().setWidth(200),
          alignment: Alignment.bottomCenter,
          child: TextField(
            obscureText: !state.showPwd!,
            controller: widget.pwdController,
            // #685 密码输入限制字数和空格
            inputFormatters: [FilteringTextInputFormatter.deny(RegExp("[ ]")), LengthLimitingTextInputFormatter(30)],
            cursorColor: Colors.white,
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(24),
            ),
            decoration: InputDecoration(
              // TODO: 翻译
              hintText: 'YBDEnter Password',
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(
                left: ScreenUtil().setWidth(5),
                bottom: ScreenUtil().setWidth(10),
              ),
              hintStyle: TextStyle(
                color: Color(0xffE0E0E0),
                fontSize: ScreenUtil().setSp(24),
              ),
            ),
          ),
        ),
        Expanded(child: Container()),
        Material(
          type: MaterialType.transparency,
          child: InkWell(
            onTap: () {
              logger.v('clicked show pwd btn : ${state.showPwd}');
              context.read<YBDStartLiveBloc>().add(StartLiveEvent.UpdatePwdStyle);
            },
            child: Container(
              // 眼睛按钮
              width: ScreenUtil().setWidth(rowHeight),
              height: ScreenUtil().setWidth(rowHeight),
              padding: EdgeInsets.all(ScreenUtil().setWidth(rowHeight - 38) / 2),
              child: Image.asset(
                state.showPwd! ? "assets/images/show_pass.png" : "assets/images/hide_pass.png",
                color: Colors.white,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ],
    );
  }
  void _pwdInputRowy2M0ooyelive(YBDStartLiveBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
