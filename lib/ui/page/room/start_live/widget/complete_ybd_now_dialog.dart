import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';

import '../../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../../common/util/common_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../redux/app_ybd_state.dart';
import '../../../../widget/button/delay_ybd_gesture_detector.dart';

/// 完善主播信息弹框
class YBDCompleteNowDialog extends StatelessWidget {
  /// Later 按钮的回调函数
  final VoidCallback? onLater;

  /// Later 按钮的回调函数
  final VoidCallback? onCompleteNow;

  YBDCompleteNowDialog({this.onLater, this.onCompleteNow});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the dialog bg");
        // 隐藏禁止用户弹框
        Navigator.pop(context);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  // 避免点白色背景隐藏弹框
                },
                child: Container(
                  width: ScreenUtil().setWidth(560),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(32)))),
                    color: Colors.white,
                    border: Border.all(
                      color: Color(0xff47CDCC),
                      width: ScreenUtil().setWidth(3),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setWidth(44)),
                      // 标题
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                        child: Center(
                          child: Text(
                            translate('complete_message'),
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(28),
                              fontWeight: FontWeight.normal,
                              color: Colors.black,
                              height: 1.5,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(28)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          _laterBtn(context),
                          _completeNowBtn(context),
                        ],
                      ),
                      SizedBox(height: ScreenUtil().setWidth(44)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildCvMAvoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Later 按钮
  Widget _laterBtn(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v('clicked later btn');
        Navigator.pop(context);

        if (null != onLater) {
          onLater!();
        }
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(ScreenUtil().setWidth(60)),
          ),
          color: Color(0xffCCCCCC),
        ),
        width: ScreenUtil().setWidth(200),
        height: ScreenUtil().setWidth(64),
        alignment: Alignment.center,
        child: Text(
          translate('later'),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
  void _laterBtnPJqsnoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// complete now 按钮
  Widget _completeNowBtn(BuildContext context) {
    return YBDDelayGestureDetector(
      onTap: () {
        logger.v('clicked complete now btn');
        Navigator.pop(context);

        if (null != onCompleteNow) {
          onCompleteNow!();
        }
        // TODO: 测试代码 ios
        // 跳转到完善主播信息的页面
        // YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.edit_profile);
        // YBDNavigatorHelper.navigateTo(context,
        //     YBDNavigatorHelper.web_view_page + "/${Uri.encodeComponent(_data[_change].activityH5)}//");
        final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
        String? applyAgencyUrl = store.state.configs?.talentSign;
        String url = Uri.encodeComponent('$applyAgencyUrl#/Info?index=1');
        YBDNavigatorHelper.navigateToWeb(
          context,
          "?url=$url&showNavBar=false",
        );
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(ScreenUtil().setWidth(60)),
          ),
          gradient: LinearGradient(
            colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        constraints: BoxConstraints(
          minWidth: ScreenUtil().setWidth(250),
        ),
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
        height: ScreenUtil().setWidth(64),
        alignment: Alignment.center,
        child: Text(
          translate('complete_now'),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
  void _completeNowBtnnJ6S1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
