import 'dart:async';


import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../generated/json/base/json_convert_content.dart';
import '../../../../../module/agency/agency_ybd_api_helper.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../../module/user/util/user_ybd_util.dart';
import '../entity/room_ybd_category_entity.dart';

/// 开播页面事件
enum StartLiveEvent {
  // 初始化页面数据
  Init,
  // 更新成为签约主播按钮的显示状态
  UpdateTalentBtn,
  // 更新房间密码显示类型
  UpdatePwdStyle,
  // 设置为公开房间或私密房间
  UpdateRoomType,
  // 选择房间类型
  YBDSelectRoomCategory,
}

/// 开播页面业务管理器
class YBDStartLiveBloc extends Bloc<StartLiveEvent, YBDStartLiveBlocState> {
  BuildContext context;

  /// 是否为签约主播, 签约主播不用显示成为签约主播按钮
  /// 默认不显示签约主播按钮
  bool _isOfficialTalent = true;

  /// 是否为公开房间
  bool _isPublic = true;

  /// 是否显示密码
  bool _showPwd = false;

  /// 房间分类数据
  List<YBDRoomCategoryItem?>? _categories;

  ///限制3级以下用户  上麦
  int? micRequestEnable;
  int? micEnable = 0;

  /// 房间信息 （用来获取 title category 等）
  late YBDRoomInfo _roomInfo;

  String? _title;

  String? _tag;

  YBDStartLiveBloc(this.context)
      : super(YBDStartLiveBlocState(
          showOfficialTalentBtn: false,
          isPublic: true,
          showPwd: false,
          categories: [],
        ));

  @override
  Stream<YBDStartLiveBlocState> mapEventToState(StartLiveEvent event) async* {
    switch (event) {
      case StartLiveEvent.Init:
        // 获取房间分类数据
        String jsonStr = await rootBundle.loadString("assets/data/category_data.json");
        Map? mapData = json.decode(jsonStr);
        YBDRoomCategoryEntity data = JsonConvert.fromJsonAsT<YBDRoomCategoryEntity>(mapData);
        _categories = data.items;

        // 获取是否为签约主播的状态
        _checkIfOfficialTalent();
        await _requestRoomInfo();
        _setData();
        yield _blocState();
        break;
      case StartLiveEvent.UpdateTalentBtn:
        yield _blocState();
        break;
      case StartLiveEvent.UpdateRoomType:
        _isPublic = !_isPublic;
        yield _blocState();
        break;
      case StartLiveEvent.UpdatePwdStyle:
        _showPwd = !_showPwd;
        yield _blocState();
        break;
      case StartLiveEvent.YBDSelectRoomCategory:
        yield _blocState();
        break;
    }
  }
  void mapEventToStatenn81ooyelive(StartLiveEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 配置 bloc state
  YBDStartLiveBlocState _blocState() {
    return YBDStartLiveBlocState(
      isPublic: _isPublic,
      showPwd: _showPwd,
      categories: _categories,
      showOfficialTalentBtn: !_isOfficialTalent,
      micRequestEnable: micEnable,
      tag: _tag,
      title: _title,
    );
  }

  /// 选中房间类型
  selectedRoomCategory(YBDRoomCategoryItem? item) {
    _categories!.forEach((element) {
      if (element!.index == item!.index) {
        element.selected = true;
      } else {
        element.selected = false;
      }
    });
    add(StartLiveEvent.YBDSelectRoomCategory);
  }

  /// 查询是否签约主播
  _checkIfOfficialTalent() async {
    int? userId = await YBDUserUtil.userIdInt();
    _isOfficialTalent = await YBDAgencyApiHelper.isOfficialTalent(context, userId);
    add(StartLiveEvent.UpdateTalentBtn);
  }

  /// 请求房间信息
  _requestRoomInfo() async {
    String? roomId = await YBDUserUtil.userId();
    YBDRoomInfo? result = await ApiHelper.queryOnlineInfo(context, int.parse(roomId ?? ''));
    if (null != result) {
      logger.v('room info : ${result.nickname}');
      _roomInfo = result;
      _tag = _roomInfo.tag;
      micEnable = _roomInfo.micRequestEnable;
      List<String>? oldTitle = _roomInfo.title?.split(',');

      if (oldTitle != null && oldTitle.length >= 2) {
        ///兼容老版本
        _tag = oldTitle[0];
        _title = oldTitle[1];
      } else if (oldTitle != null && oldTitle.length == 1) {
        _title = oldTitle[0];
        _tag = _roomInfo.tag;
      }
      print('tag:$_tag micEnable:$micEnable title:$_title');

      // TODO 拆分 title
    } else {
      logger.v('room info is null');
    }
  }

  _setData() {
    if (_tag == null) {
      logger.v('tag is null');
      return;
    }
    for (int i = 0; i < _categories!.length; i++) {
      if (_categories![i]!.index == int.parse(_tag!)) {
        print('_categories[i].index ${_categories![i]!.index} tag:${int.parse(_tag!)}');
        _categories![i]!.selected = true;
      } else {
        _categories![i]!.selected = false;
      }
    }
  }
}

class YBDCheckLiveResult {
  /// 0 表示可以开播, 非 0 表示不能开播
  String? code;

  /// [code] 不为 0 时有错误信息
  String? errorMsg;
}

/// 开播页面状态
class YBDStartLiveBlocState {
  /// 是否显示成为签约主播按钮
  bool showOfficialTalentBtn;

  /// 是否为公开房间
  bool? isPublic;

  /// 是否显示密码
  bool? showPwd;

  /// 房间分类数据
  List<YBDRoomCategoryItem?>? categories;

  String? tag;

  int? micRequestEnable = 0;

  String? title;

  YBDStartLiveBlocState({
    this.showOfficialTalentBtn = false,
    this.isPublic,
    this.showPwd,
    this.categories,
    this.micRequestEnable,
    this.title,
    this.tag,
  });
}
