import 'dart:async';


/*
 * @Author: William-Zhou
 * @Date: 2021-07-13 14:05:31
 * @LastEditTime: 2021-07-22 17:29:22
 * @LastEditors: William-Zhou
 * @Description: 
 */

enum RTMType {
  RTMTypeUnknow, //未知
  RTMTypePauseMuteL, //禁言所有麦位
  RTMTypeResumeMuteL, //恢复禁言
  RTMTypePauseMuteB, //禁言对手主播 转发流
  RTMTypeResumeMuteB, // 恢复对手主播 转发流
  RTMTypeStartPKMicNotice, // 开始匹配时给在麦位上的用户发送提醒
}

extension RTMTypeExt on RTMType? {
  int val() {
    int a = 0;
    switch (this) {
      case RTMType.RTMTypePauseMuteL:
        a = 1;
        break;
      case RTMType.RTMTypeResumeMuteL:
        a = 2;
        break;
      case RTMType.RTMTypePauseMuteB:
        a = 3;
        break;
      case RTMType.RTMTypeResumeMuteB:
        a = 4;
        break;
      case RTMType.RTMTypeStartPKMicNotice:
        a = 5;
        break;
      default:
        break;
    }
    return a;
  }
  void vallmmusoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  static RTMType type(int a) {
    RTMType type = RTMType.RTMTypeUnknow;
    switch (a) {
      case 1:
        type = RTMType.RTMTypePauseMuteL;
        break;
      case 2:
        type = RTMType.RTMTypeResumeMuteL;
        break;
      case 3:
        type = RTMType.RTMTypePauseMuteB;
        break;
      case 4:
        type = RTMType.RTMTypeResumeMuteB;
        break;
      case 5:
        type = RTMType.RTMTypeStartPKMicNotice;
        break;
      default:
        break;
    }
    return type;
  }
}
