import 'package:oyelive_main/common/room_socket/message/base/display_ybd_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/enter.dart';
import 'package:oyelive_main/common/room_socket/message/common/gift.dart';
import 'package:oyelive_main/common/room_socket/message/common/super_ybd_fans_message.dart';

import '../../../../common/room_socket/entity/fans_ybd_info.dart';
import '../../../../common/room_socket/message/common/Global.dart';
import '../../../../common/room_socket/message/common/internal.dart';
import '../../../../common/room_socket/message/common/room_ybd_theme.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../bloc/room_ybd_bloc_state.dart';
import '../teen_patti/entity/teen_ybd_patti_info.dart';

/// 最多允许密码输入错误次数
const MAX_RETRY_PWD = 3;

/// 默认房间分类  -1表示非主页进入  没有房间类型
const ROOM_DEFAULT_TAG = '-1';

/// 进入房间类型
enum EnterRoomType {
  /// 进入房间
  YBDEnter,

  /// 从开播页面进入房间
  StartLive,
}

/// 房间页面类型
// enum RoomPageType {
//   // 用户房间页面
//   User,
//   // 主播房间页面
//   Talent,
// }

/// 房间相关的自定义类型
/// 禁止用户的操作类型
enum ForbiddenUserType {
  /// 禁言
  Mute,

  /// 踢人
  Boot,

  /// 拉黑
  Block,
}

/// 大礼物动画播放状态
enum BigGiftStatus {
  /// 准备播放，还没有开始播放
  Ready,

  /// 播放中
  Playing,

  /// 停止播放
  Stoped,
}

/// 房间密码状态
enum CheckPwdStatus {
  /// 刚进房间，检查之前的状态
  InitStatus,

  /// 非主播进入加密房间需要输入密码
  NeedCheck,
  // NoNeedCheck,
  /// 已弹出密码输入框
  Checking,

  /// 密码校验成功
  Passed,

  /// 密码输入错误后重新输入密码
  Retry,

  /// 密码错误
  Failed,
}

/// 房间页面 bloc 事件
enum RoomEvent {
  /// 请求页面数据
  Init,

  /// 加载新房间信息
  LoadNewRoom,

  /// 请求房间信息
  // RequestRoomInfo,

  /// 显示键盘事件
  ShowKeyBoard,

  /// 隐藏键盘事件
  HideKeyBoard,

  /// atUser 事件
  AtUser,

  /// 更新房间信息
  // UpdateRoomInfo,

  /// 已显示完首充
  // DoneShowTopUp,

  /// 大礼物
  // GlobalGiftEvent,

  /// 已经显示大礼物
  // DoneShowGlobalGift,

  /// 已关闭房间
  // RoomBlocClosed,

  /// 更新主播信息
  UpdateTalentInfo,

  /// 更新在线用户数
  UpdateUserCount,

  /// 更新房间总榜数据
  UpdateTotalRank,

  /// 更新房间分数
  UpdateScore,

  /// 更新房间等级
  UpdateRoomLevel,

  /// 更新单个房间的信息
  UpdateRoomInfo,

  /// 更新房间类型
  UpdateRoomTag,

  /// 更新检查房间密码的状态
  UpdatePwdStatus,

  /// 更新房间用户列表
  UpdateViewerList,

  /// 更新房间管理员列表
  UpdateManagerList,

  /// 已显房间升级框
  // CompleteLevelUp,

  /// 更新房间升级弹框的显示状态
  UpdateLevelUpDialog,

  /// 更新房间充值弹框的显示状态
  // UpdateTopUpDialog,

  /// 更新新房间当前排行版
  UpdateFansRanking,
  /// 更新房间当前排行版
  UpdateCurrentRanking,

  /// 切房间
  ChangeRoom,

  /// 更新房间主题
  UpdateRoomTheme,

  /// 更新房间通知
  UpdateNotice,

  /// 更新房间动画下载状态
  UpdateDownload,

  /// 更新房间大礼物数据
  UpdateBigGift,

  /// 更新水果游戏数据
  UpdateGameData,
}

/// 房间状态更新类型，根据类型刷新界面
enum RoomStateType {
  /// 主播信息已更新
  TalentInfoUpdated,

  /// 在线用户数已更新
  UserCountUpdated,

  /// 房间总榜数据已更新
  TotalRankUpdated,

  /// 房间分数已更新
  ScoreUpdated,

  /// 房间等级已更新
  RoomLevelUpdated,

  /// 单个房间的信息已更新
  RoomInfoUpdated,

  /// 房间类型已更新
  RoomTagUpdated,

  /// 房间密码的状态已更新
  PwdStatusUpdated,

  /// 键盘弹出状态已更新
  /// 这个两事件会更新键盘状态[ShowKeyBoard][HideKeyBoard]
  KeyboardUpdated,

  /// 在线用户列表已更新
  ViewerListUpdated,

  /// 房间管理员列表已更新
  ManagerListUpdated,

  /// 房间升级弹框显示状态已更新
  LevelUpDialogUpdated,

  /// 房间充值弹框显示状态已更新
  // TopUpDialogUpdated,

  /// 房间新排行版已更新
  CurrentFansUpdated,
  /// 房间当前排行版已更新
  CurrentRankingUpdated,

  /// 切房间
  RoomChanged,

  /// 房间主题已更新
  RoomThemeUpdated,

  /// 收到新到房间通知
  NoticeUpdated,

  /// 房间下载动画的任务状态已更新
  DownloadUpdated,

  /// 房间大礼物数据已更新
  BigGiftUpdated,

  /// 水果游戏数据已更新
  GameDataUpdated,

  /// 默认不刷新界面
  Default,
}

typedef void OnRoomBlocState(YBDRoomState state);
typedef void OnCurrentRanking(List<YBDFansInfo?>? currentRanking);
typedef void OnRoomScore(int? roomScore);
typedef void OnTotalRanking(List<YBDFansInfo?>? superFans);
typedef void OnViewerList(List<YBDUserInfo?> viewerList);
typedef void OnViewerTotalCount(int? viewerTotalCount);
typedef void OnViewerOnlineCount(int? viewerOnlineCount);
typedef void OnFans(YBDFansContent content);
typedef void OnRoomManagers();
typedef void OnRoomTheme(YBDRoomTheme? roomTheme);
typedef void OnNoticeMessage(YBDDisplayMessage noticeMessage);
typedef void OnRoomExperience(int? roomExperience, int? roomLevel);
typedef void OnRoomAnimationPath(List<String?>? path, YBDEnter? enter, YBDGift? gift);
typedef void OnPwdRetry();
typedef void OnSubscribeError(String error);
typedef void OnSubscribeSuccess();
typedef void OnChannelToken(String? token);
typedef void OnUnSubscribe();
typedef void OnJoinChannelSuccess(String channel, int uid, int elapsed);
typedef void OnGlobalGift(YBDGlobal global);
typedef void OnTeenInfo(YBDTeenPattiInfo teenInfo);
typedef void OnBigGiftAnimation(BigGiftStatus status);
typedef void OnTalentCreateRoom(YBDInternal internal);

/// 房间错误

/// 房间操作成功
const String ROOM_SUCCESS = 'SUCCESS';

/// 获取channel key超时
const String GET_CHANNEL_KEY_TIMEOUT = 'GET_CHANNEL_KEY_TIMEOUT';

/// 获取rtm key超时
const String GET_RTM_KEY_TIMEOUT = 'GET_RTM_KEY_TIMEOUT';

/// 房间密码错误
const String ROOM_PWD_ERROR = 'ROOM_PWD_ERROR';

/// 主播限播
const String TALENT_LIMIT = 'TALENT_LIMIT';

/// 订阅房间错误
const String SUBSCRIBE_ROOM_ERROR = 'SUBSCRIBE_ROOM_ERROR';

// 游戏code
/// 水果游戏
const String GREEDY_CODE = '1005';

/// teenpatti游戏
const String TEENPATTI_CODE = '1004';

/// ludo游戏
const String LUDO_CODE = '1012';

/// Lucky1000游戏
const String LUCKY1000_CODE = '1014';

const String PokerColor_CODE = '1006';
const String FruitMachine_CODE = '1007';
const String BenzBMW_CODE = '1008';
const String Roshambo_CODE = '1009';
const String Baccarat_CODE = '1010';
const String ZhaJinHua_CODE = '1011';
const String Wheel_CODE = '1013';
const String EventGame_CODE = '1015';
const String BumperCar_CODE = '1016';
const String FlyCutter_CODE = '1017';
const String CricketQuiz_CODE = '1018';
const String TPGO_CODE = '1019';
const String GuessNumber_CODE = '1020';
const String FingerCross_CODE = '1021';
const String FlipCircle_CODE = '1022';
const String FlipCoin_CODE = '1023';
const String Dice_CODE = '1024';
