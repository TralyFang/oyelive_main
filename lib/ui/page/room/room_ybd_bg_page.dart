import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/page/room/widget/room_ybd_svga_theme.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/room_socket/message/common/room_ybd_theme.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/image_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';

/// 房间背景
class YBDRoomBgPage extends StatefulWidget {
  ///主题背景
  final YBDRoomTheme? roomTheme;

  YBDRoomBgPage(this.roomTheme);

  @override
  _YBDRoomBgPageState createState() => _YBDRoomBgPageState();
}

class _YBDRoomBgPageState extends BaseState<YBDRoomBgPage> {
  /// 主题背景下载URL
  String? netUrl;

  @override
  void initState() {
    super.initState();
  }
  void initStatemPDIQoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
  void didChangeDependenciesMbTP2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    logger.v('room_bg animation myBuild');
    return _roomAnimation();
  }
  void myBuildrIh4Hoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _roomAnimation() {
    return Stack(children: [
      _getRoomBgWidget(),
    ]);
  }

  ///主题背景
  Widget _getRoomBgWidget() {
    logger.v('_getRoomBgWidget bgUrl:${widget.roomTheme?.image ?? ''}');
    int type = YBDCommonUtil.getFileType(widget.roomTheme?.image ?? null);

    getNetUrl(type);
    logger.v('_getRoomBgWidget type:$type');

    if (type == 1 || type == 2) {
      return Container(
        color: Colors.transparent,
        height: ScreenUtil().screenHeight,
        width: ScreenUtil().screenWidth,
        child: YBDNetworkImage(
          imageUrl: netUrl!,
          fit: BoxFit.cover,
          placeholder: (context, url) => Image.asset(
            'assets/images/liveroom/live_beijing.jpg',
            fit: BoxFit.cover,
          ),
        ),
      );
    } else if (type == 3) {
      if (netUrl == null || netUrl!.isEmpty) {
        return Container();
      }

      return YBDRoomSvgaTheme(
        netUrl,
        showLoading: false,
        key: ValueKey(netUrl),
        height: ScreenUtil().screenHeight,
        width: ScreenUtil().screenWidth,
      );
    } else {
      return Container(
        height: ScreenUtil().screenHeight,
        width: ScreenUtil().screenWidth,
        child: Image.asset(
          'assets/images/liveroom/live_beijing.jpg',
          height: ScreenUtil().screenHeight,
          width: ScreenUtil().screenWidth,
          fit: BoxFit.cover,
        ),
      );
    }
  }
  void _getRoomBgWidgetPxqPZoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 下载路径组装--背景图
  void getNetUrl(int type) {
    if (type == 1) {
      netUrl = YBDImageUtil.theme(context, widget.roomTheme?.image, "B");
    } else if (type == 2) {
      netUrl = YBDImageUtil.gif(context, widget.roomTheme?.image, YBDImageType.THEME);
    } else if (type == 3) {
      netUrl = YBDImageUtil.gif(context, widget.roomTheme?.image, YBDImageType.THEME);
    }

    logger.v('getNetUrl netUrl:$netUrl widget.roomTheme?.image:${widget.roomTheme?.image}');
  }
  void getNetUrlsVvMyoyelive(int type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
}
