import 'dart:async';

import 'package:oyelive_main/common/room_socket/message/common/enter.dart';
import 'package:oyelive_main/common/room_socket/message/common/gift.dart';
import 'package:oyelive_main/common/room_socket/message/common/super_ybd_fans_message.dart';

import '../../../../common/room_socket/entity/fans_ybd_info.dart';
import '../../../../common/room_socket/message/common/Global.dart';
import '../../../../common/room_socket/message/common/internal.dart';
import '../../../../common/room_socket/message/common/room_ybd_theme.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../define/room_ybd_define.dart';
import '../play_center/entity/game_ybd_entity.dart';

// abstract class YBDRoomState {}

/// 房间页面状态
class YBDRoomState {
  /// 描述房间状态更新的内容
  RoomStateType? type;

  /// 房间 id
  int? roomId;

  /// 麦位不同，占用的高度不一样
  int? dynamicHeight;

  /// 单个房间的信息
  YBDRoomInfo? roomInfo;

  /// 主播信息
  YBDUserInfo? talentInfo;

  /// 房间管理员列表
  List<YBDUserInfo?>? roomManagers;

  /// 房间在线用户
  List<YBDUserInfo>? viewerList;

  /// 房间在线用户总数
  int? onLineUserCount;

  /// 当前在线用户总数
  int? curOnLineUserCount;

  /// 新粉丝榜
  YBDFansContent? fansContent;

  /// 当天粉丝榜
  List<YBDFansInfo>? dailyFans;

  /// 当周粉丝榜
  List<YBDFansInfo>? weeklyFans;

  /// 当月粉丝榜
  List<YBDFansInfo>? monthlyFans;

  /// 当场粉丝榜
  List<YBDFansInfo>? currentRanking;

  /// 总粉丝榜
  List<YBDFansInfo>? totalRanking;

  /// 房间分数
  int? roomScore;

  /// 检查房间密码状态
  CheckPwdStatus? checkPwdStatus;

  /// 是否显示键盘
  bool? showKeyboard;

  /// at user 昵称
  String? atUserNickName;

  YBDInternal? noticeMessage;

  /// 是否显示首充提示弹框
  bool? showTopUpDialog;

  /// 是否显示房间升级提示
  bool? showLevelUpDialog;

  /// 背景图片Url
  YBDRoomTheme? roomTheme;

  /// 当前播放礼物地址
  List<String?>? roomAnimationPath;

  ///进房座驾
  YBDEnter? enter;

  ///房间用户送的礼物
  YBDGift? gift;

  /// 大礼物
  YBDGlobal? global;

  /// 保存房间类型（用来修改麦位序号）
  String? roomTag;

  /// 游戏列表数据源
  List<YBDGameRecord?>? gameData;

  /// 横幅类型: 0 默认房间标题 1 大礼物
  int? bannerType;

  /// 房间顶部是否在显示标题或大礼物动画
  /// 房间标题和大礼物显示的位置相同，这里统一当成顶部横幅
  bool? showingBanner;

  /// 房间大礼物列表
  List<YBDGlobal>? bigGifts;

  /// 大礼物顶部边距
  int bigGiftTop;

  YBDRoomState({
    this.type,
    this.roomId,
    this.dynamicHeight,
    this.roomInfo,
    this.talentInfo,
    this.roomManagers,
    this.viewerList,
    this.onLineUserCount,
    this.fansContent,
    this.dailyFans,
    this.weeklyFans,
    this.monthlyFans,
    this.currentRanking,
    this.totalRanking,
    this.roomScore,
    this.curOnLineUserCount,
    this.checkPwdStatus,
    this.showKeyboard,
    this.atUserNickName,
    this.noticeMessage,
    this.showTopUpDialog,
    this.roomTheme,
    this.roomAnimationPath,
    this.enter,
    this.gift,
    this.showLevelUpDialog,
    this.global,
    this.roomTag,
    this.gameData,
    this.bannerType,
    this.showingBanner,
    this.bigGifts,
    this.bigGiftTop = 0,
  }) {
    if (null == roomAnimationPath) {
      roomAnimationPath = List.filled(3, '');
    }

    if (null == gameData) {
      gameData = [];
    }

    if (roomTag == null) {
      roomTag = '-1';
    }

    if (null == bannerType) {
      bannerType = 0;
    }

    if (null == checkPwdStatus) {
      checkPwdStatus = CheckPwdStatus.InitStatus;
    }

    if (null == showKeyboard) {
      showKeyboard = false;
    }

    if (null == dynamicHeight) {
      dynamicHeight = 0;
    }

    if (null == bannerType) {
      bannerType = 0;
    }

    if (null == bigGifts) {
      bigGifts = [];
    }

    if (null == showingBanner) {
      showingBanner = false;
    }

    if (null == bigGiftTop) {
      bigGiftTop = -200;
    }
  }

  static YBDRoomState copyState(
    YBDRoomState state, {
    RoomStateType type = RoomStateType.Default,
  }) {
    return YBDRoomState(
        type: type,
        roomId: state.roomId,
        dynamicHeight: state.dynamicHeight,
        roomInfo: state.roomInfo,
        talentInfo: state.talentInfo,
        roomManagers: state.roomManagers,
        viewerList: state.viewerList,
        onLineUserCount: state.onLineUserCount,
        fansContent: state.fansContent,
        dailyFans: state.dailyFans,
        weeklyFans: state.weeklyFans,
        monthlyFans: state.monthlyFans,
        currentRanking: state.currentRanking,
        totalRanking: state.totalRanking,
        roomScore: state.roomScore,
        checkPwdStatus: state.checkPwdStatus,
        showKeyboard: state.showKeyboard,
        atUserNickName: state.atUserNickName,
        noticeMessage: state.noticeMessage,
        showTopUpDialog: state.showTopUpDialog,
        roomTheme: state.roomTheme,
        roomAnimationPath: state.roomAnimationPath,
        enter: state.enter,
        gift: state.gift,
        showLevelUpDialog: state.showLevelUpDialog,
        global: state.global,
        roomTag: state.roomTag,
        gameData: state.gameData,
        bannerType: state.bannerType,
        showingBanner: state.showingBanner,
        bigGifts: state.bigGifts,
        bigGiftTop: state.bigGiftTop,
        curOnLineUserCount: state.curOnLineUserCount);
  }
}
