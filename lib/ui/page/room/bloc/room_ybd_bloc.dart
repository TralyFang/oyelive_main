import 'dart:async';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart' as g;
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_live_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/room_socket/entity/fans_ybd_info.dart';
import 'package:oyelive_main/common/room_socket/message/common/internal.dart';
import 'package:oyelive_main/common/room_socket/message/common/room_ybd_theme.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_util.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_define.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/room_socket/message/common/Global.dart';
import '../../../../common/room_socket/message/resp/response.dart';
import '../../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/user/entity/topup_ybd_summary_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../define/room_ybd_define.dart';
import '../mic/mic_ybd_bloc.dart';
import '../service/error_ybd_event.dart';
import '../service/join_ybd_channel_success_event.dart';
import '../service/live_ybd_event.dart';
import '../service/live_ybd_service.dart';
import '../service/message_ybd_event.dart';
import '../service/pwd_ybd_incorrect_event.dart';
import '../service/pwd_ybd_pass_event.dart';
import '../service/pwd_ybd_retry_event.dart';
import '../teen_patti/bus_event/event_ybd_fn.dart';
import '../teen_patti/entity/ybd_teen_info.dart';
import '../util/room_ybd_util.dart';
import '../widget/dialog_ybd_change_board_name.dart';
import '../widget/dialog_ybd_insufficient_warn.dart';
import 'pk_ybd_state_bloc.dart';
import 'room_ybd_bloc_state.dart';
import 'room_ybd_download_handler.dart';
import 'room_ybd_msg_handler.dart';

/// 房间业务逻辑管理器
class YBDRoomBloc extends Bloc<RoomEvent, YBDRoomState> {
  /// 创建bloc的context
  BuildContext? _context;

  /// 房间页面context用来访问[YBDMicBloc]
  BuildContext? _roomContext;

  final YBDPkStateBloc pkStateBloc;

  /// 当前房间id
  /// 第一次进房间和切房间时赋值
  int? _roomId;

  int? get roomId {
    return _roomId;
  }

  /// 保存bloc状态
  YBDRoomState? _state;

  /// 订阅直播事件
  StreamSubscription<YBDLiveEvent>? _liveEventSubscription;

  /// 处理房间消息
  late YBDRoomMsgHandler _msgHandler;

  /// 处理房间主题、座驾下载
  YBDRoomDownloadHandler? _downloadHandler;

  /// 播放大礼物的定时器
  /// 有多个大礼物时先保存大礼物数据再按一定的时间间隔播放
  Timer? _bigGiftTimer;

  /// 记录大礼物播放的时间间隔
  int _timerNum = 0;

  ///首次进入主播页附带功能
  int? funcType; // 1 跳转房间打开teenpatti 2 跳转房间打开水果游戏 3 跳转房间打开送礼框 4 跳转房间上麦

  ///webview游戏code
  int? gameCode;

  /// [context]创建bloc的context
  /// [roomId]房间Id
  /// [roomTag]房间类型，麦位排序时要用到
  YBDRoomBloc({
    required BuildContext? context,
    required int roomId,
    String? roomTag,
    required this.pkStateBloc,
    this.funcType,
    this.gameCode,
  }) : super(YBDRoomState()) {
    _context = context;
    _roomId = roomId;
    _state = YBDRoomState(roomId: roomId, roomTag: roomTag);

    // 处理房间消息的处理
    _configRoomMsgHandler();

    // 配置下载处理
    _configDownloadHandler();

    // 监听房间消息
    _listenLiveEvent();

    // 加载房间数据
    add(RoomEvent.Init);
  }

  @override
  Future<void> close() {
    logger.i('room bloc closed : $_roomId');
    _liveEventSubscription?.cancel();
    _bigGiftTimer?.cancel();
    return super.close();
  }
  void closexu0O2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Stream<YBDRoomState> mapEventToState(RoomEvent event) async* {
    logger.v(event);
    switch (event) {
      case RoomEvent.Init:
        _requestJoinRoomInfo(_roomId, currentState: _state);
        _requestGuardian();
        _requestTalentInfo();
        _topUpUser();
        // [YBDRoomPage]提交[645d8ff]中的注释了这个弹框
        // _showTopUpHint();
        _handleMessageLoss();
        _getGameData();
        break;
      case RoomEvent.LoadNewRoom:
        _requestSwitchRoomInfo(_roomId, currentState: _state);
        _requestGuardian();
        _requestTalentInfo();
        _topUpUser();
        _handleMessageLoss();
        _getGameData();
        break;
      // case RoomEvent.RequestRoomInfo:
      //   _requestRoomInfo();
      //   break;
      case RoomEvent.ShowKeyBoard:
        _state!.showKeyboard = true;
        yield YBDRoomState.copyState(_state!, type: RoomStateType.KeyboardUpdated);
        break;
      case RoomEvent.HideKeyBoard:
        _state!.showKeyboard = false;
        yield YBDRoomState.copyState(_state!, type: RoomStateType.KeyboardUpdated);
        break;
      case RoomEvent.AtUser:
        _state!.showKeyboard = true;
        yield YBDRoomState.copyState(_state!, type: RoomStateType.KeyboardUpdated);
        // 刷新 UI 后重置 nick name
        _state!.atUserNickName = null;
        break;
      // case RoomBlocEvent.DoneShowTopUp:
      //   _state.topUpHint = false;
      //   yield YBDRoomState.copyState(_state);
      //   break;
      // case RoomBlocEvent.CompleteLevelUp:
      //   _state.showLevelUp = false;
      //   yield YBDRoomState.copyState(_state);
      //   break;
      // case RoomBlocEvent.DoneShowGlobalGift:
      //   _state.global = null;
      //   yield YBDRoomState.copyState(_state);
      //   break;
      // case RoomBlocEvent.UpdateRoomInfo:
      //   yield YBDRoomState.copyState(_state);
      //   break;
      // case RoomBlocEvent.GlobalGiftEvent:
      //   logger.v('global gift: ${_state.global?.toJson()}');
      //   yield YBDRoomState.copyState(_state);
      //   break;
      case RoomEvent.UpdateTalentInfo:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.TalentInfoUpdated);
        break;
      case RoomEvent.UpdateUserCount:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.UserCountUpdated);
        break;
      case RoomEvent.UpdateTotalRank:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.TotalRankUpdated);
        break;
      case RoomEvent.UpdateScore:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.ScoreUpdated);
        break;
      case RoomEvent.UpdateRoomLevel:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.RoomLevelUpdated);
        break;
      case RoomEvent.UpdateRoomInfo:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.RoomInfoUpdated);
        break;
      case RoomEvent.UpdateRoomTag:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.RoomTagUpdated);
        break;
      case RoomEvent.UpdatePwdStatus:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.PwdStatusUpdated);
        break;
      case RoomEvent.UpdateViewerList:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.ViewerListUpdated);
        break;
      case RoomEvent.UpdateManagerList:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.ManagerListUpdated);
        break;
      case RoomEvent.UpdateLevelUpDialog:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.LevelUpDialogUpdated);
        break;
      // [YBDRoomPage][645d8ff]这次提交注释了该弹框
      // case RoomBlocEvent.UpdateTopUpDialog:
      //   yield YBDRoomState.copyState(_state, type: RoomStateType.TopUpDialogUpdated);
      //   break;
      case RoomEvent.UpdateFansRanking:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.CurrentFansUpdated);
        break;
      case RoomEvent.UpdateCurrentRanking:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.CurrentRankingUpdated);
        break;
      case RoomEvent.ChangeRoom:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.RoomChanged);
        break;
      case RoomEvent.UpdateRoomTheme:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.RoomThemeUpdated);
        break;
      case RoomEvent.UpdateNotice:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.NoticeUpdated);
        break;
      case RoomEvent.UpdateDownload:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.DownloadUpdated);
        break;
      case RoomEvent.UpdateBigGift:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.BigGiftUpdated);
        break;
      case RoomEvent.UpdateGameData:
        yield YBDRoomState.copyState(_state!, type: RoomStateType.GameDataUpdated);
        break;
    }
  }
  void mapEventToStatedxYjvoyelive(RoomEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 设置房间页面context
  void setRoomPageContext(BuildContext value) {
    _roomContext = value;
    YBDLiveService.instance.data.roomContext = value;
  }
  void setRoomPageContextV6qCIoyelive(BuildContext value) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 设置房间顶部横幅类型
  /// 横幅类型: 0 默认房间标题 1 大礼物
  void setBannerType(int value) {
    logger.v('BigGiftUpdated setBannerType: $value');
    _state!.bannerType = value;
  }

  /// 标记是否显示房间顶部横幅，房间标题或房间大礼物
  void setShowingBanner(bool value) {
    logger.v('BigGiftUpdated showing banner: $value');
    _state!.showingBanner = value;
  }
  void setShowingBannerwCWH9oyelive(bool value) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 切换房间
  /// [targetRoom]新房间
  /// [roomTag]房间类型
  void changeRoom({required int? targetRoom, String? roomTag}) {
    // 不符合切房间条件时直接返回
    if (!_shouldChangeRoom(_roomId, targetRoom)) return;
    logger.v('change room join room targetRoom:$targetRoom roomTag:$roomTag');
    YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.exit_liveroom);
    YBDTALiveRoomTrack.roomCategory = roomTag;
    // 离开socket和声网频道
    // YBDLiveService.instance.changeRoom(sourceRoomId: _roomId);
    YBDLiveService.instance.cleanOldRoom(roomId: _roomId);

    // 先切换成观众角色后再切房间
    YBDLiveService.instance.setClientRole(LiveRole.Audience);

    // 停止接收房间音频流，[YBDLiveService]里的[_switchChannel]会自动订阅音频流
    YBDLiveService.instance.muteAllRemoteAudioStreams(true);

    // 保存新的房间id
    _roomId = targetRoom;

    // 用新房间id
    _msgHandler.changeRoomId(targetRoom);
    _downloadHandler!.changeRoomId(targetRoom);

    // 进入房间页面时记录 roomId，作为加入频道的判断条件
    YBDLiveService.instance.data.roomId = targetRoom;

    // 标记已进入房间
    YBDLiveService.instance.data.isInRoom = true;

    // 刷新bloc状态
    _state = YBDRoomState(roomId: targetRoom, roomTag: roomTag);

    // 刷新房间界面
    add(RoomEvent.ChangeRoom);

    try {
      // 捕获获取bloc的错误
      // 刷新麦位数据
      BlocProvider.of<YBDMicBloc>(_roomContext!).refreshMicBloc(targetRoom ?? -1);

      // 刷新pk数据
      BlocProvider.of<YBDPkStateBloc>(_roomContext!).refreshPkBloc(targetRoom);
    } catch (e) {
      logger.e('BlocProvider _roomContext error: $_roomContext, $e');
    }

    // 进入新房间
    add(RoomEvent.LoadNewRoom);
  }
  void changeRoomZ6D8Noyelive({required int? targetRoom, String? roomTag}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 检查是否符合切房间的条件
  /// [sourceId]旧房间
  /// [targetId]新房间
  bool _shouldChangeRoom(int? sourceId, int? targetId) {
    // 两个房间的id相同时不切房间
    if (targetId == null || targetId <= 0 || targetId == sourceId) {
      return false;
    }

    return true;
  }

  /// 已显示房间升级弹框后不重复显示
  void showedLevelUpDialog() {
    // 标记为false复显示弹框
    _state!.showLevelUpDialog = false;
    add(RoomEvent.UpdateLevelUpDialog);
  }

  /// 播放结束 清空路径
  void showEnterRoomAnimation(bool playing) {
    _downloadHandler?.showEnterRoomAnimation(playing);
  }
  void showEnterRoomAnimation5KleDoyelive(bool playing) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 改变检查房间密码的状态
  void changeCheckPwdStatus(CheckPwdStatus status) {
    _state!.checkPwdStatus = status;
    add(RoomEvent.UpdatePwdStatus);
  }

  /// 保存大礼物数据清空数据
  // void addedBigGift() {
  //   _state.global = null;
  // }

  /// 设置 atUser
  void setAtUserNickName(String? nickName) {
    _state!.atUserNickName = nickName;
    add(RoomEvent.AtUser);
  }

  /// 添加房间管理员
  void addRoomMangersList(int? userId) {
    // 先检查管理员列表有没有该用户
    YBDUserInfo? targetUser = _state!.roomManagers!.firstWhereOrNull(
      (element) => element!.id == userId,
      // orElse: () => null,
    );

    // 管理员列表没有时从观众列表中获取该用户的 userInfo
    if (null == targetUser) {
      YBDUserInfo? userInfo = _state!.viewerList!.firstWhereOrNull(
        (element) => element.id == userId,
      );
      // 将该用户添加进管理员列表并刷新
      if (null != userInfo) {
        _state!.roomManagers!.add(userInfo);
        add(RoomEvent.UpdateManagerList);
      }
    }
  }
  void addRoomMangersListNkBOhoyelive(int? userId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 移除房间管理员
  void removeRoomMangersList(int? userId) {
    YBDUserInfo? targetUser = _state!.roomManagers!.firstWhereOrNull(
      (element) => element!.id == userId,
      // orElse: () => null,
    );

    if (null != targetUser) {
      _state!.roomManagers!.removeWhere((element) => element!.id == targetUser.id);
      add(RoomEvent.UpdateManagerList);
    }
  }

  /// 订阅房间
  void subscribeRoom({String? pwd}) {
    logger.v('subscribeRoom join room');
    YBDLiveService.instance.joinRoom(roomId: _roomId!, pwd: pwd);
  }

  /// ludo游戏中，被主播踢出房间
  void ludoplayingExitRoom() async {
    // ludo游戏中，被主播踢出房间，只需要断开声网，其他操作正常，相关权限问题由服务器限制
    logger.v('ludoplayingExitRoom : $roomId');
    YBDLiveService.instance.ludoplayingExitRoom(roomId);
  }

  /// 退出房间
  Future<void> exitRoom() async {
    YBDTALiveRoomTrack().taRoom(event: YBDTATrackEvent.exit_liveroom);
    // Exit the room id to avoid subscribing to the room information again when reconnecting
    YBDRoomSocketUtil.getInstance().outRoom();

    // 停止接收直播消息
    // _liveEventSubscription?.cancel();

    // 关闭[YBDRoomBloc]
    close();

    // 关闭[YBDMicBloc]
    BlocProvider.of<YBDMicBloc>(_roomContext!).close();

    // 释放下载处理器
    _downloadHandler = null;

    // 关闭[YBDMusicPlayerBloc]
    // BlocProvider.of<YBDMusicPlayerBloc>(roomContext).close();

    // 清理房间资源
    YBDRoomUtil.cleanRoom(_roomId);

    // 异常值还原
    YBDLiveService.instance.enterRoomEnable = true;
  }
  void exitRoomtFPTZoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 断开房间连接（通道和socket）刷新麦位
  Future<void> disconnectRoom(int? roomId) async {
    logger.v('get showDialogWithInternal disconnectRoom roomId:$roomId');
    BlocProvider.of<YBDMicBloc>(_roomContext!).close();
    BlocProvider.of<YBDMicBloc>(_roomContext!).refreshMicBloc(roomId ?? -1);
    // 离开socket和声网频道
    await YBDLiveService.instance.leaveRoomV2(roomId: _roomId);

    // 清理直播数据
    YBDLiveService.instance.cleanLive();
  }

  quitMatching() {
    YBDRoomSocketApi().endMatch(
      roomId: YBDLiveService.instance.data.roomId,
      onSuccess: (Response data) {},
    );
  }

  bool showPKInviteDialog = false;

  /// 配置房间消息处理器
  void _configRoomMsgHandler() {
    // bool _showPkStart = false;
    _msgHandler = YBDRoomMsgHandler(
      roomId: _roomId,
      context: _roomContext,
      pkStateBloc: pkStateBloc,
      onDailyFans: (v) {
        _state?.dailyFans = v?.removeNulls();
        add(RoomEvent.UpdateFansRanking);
      },
      onWeeklyFans: (v) {
        _state?.weeklyFans = v?.removeNulls();
        add(RoomEvent.UpdateFansRanking);
      },
      onMonthlyFans: (v) {
        _state?.monthlyFans = v?.removeNulls();
        add(RoomEvent.UpdateFansRanking);
      },
      onCurrentRanking: (value) {
        _state?.currentRanking = value?.removeNulls();
        add(RoomEvent.UpdateCurrentRanking);
      },
      onRoomScore: (value) {
        _state!.roomScore = value;
        add(RoomEvent.UpdateScore);
      },
      onChangeBoardName: (value) {
        showDialog<Null>(
            context: _context!, //BuildContext对象
            barrierDismissible: false,
            builder: (BuildContext context) {
              return YBDChangeBoardNameDialog();
            });
      },
      onInsufficientBalance: (value) {
        showDialog<Null>(
            context: _context!, //BuildContext对象
            barrierDismissible: false,
            builder: (BuildContext context) {
              return YBDInsufficinetDialog();
            });
      },
      onTotalRanking: (List<YBDFansInfo?>? value) {
        _state!.totalRanking = value?.removeNulls();
        add(RoomEvent.UpdateTotalRank);
      },
      onViewerList: (List<YBDUserInfo?> value) {
        _state!.viewerList = value.removeNulls();
        add(RoomEvent.UpdateViewerList);
      },
      onViewerTotalCount: (value) {
        logger.v('onViewerTotalCount: $value');
        _state!.onLineUserCount = value;
        add(RoomEvent.UpdateUserCount);
      },
      onViewerOnlineCount: (value) {
        logger.v('onViewerOnlineCount: $value');
        _state!.curOnLineUserCount = value;
        add(RoomEvent.UpdateUserCount);
      },
      onRoomManagers: () {
        // TODO: 测试代码 确认socket是否返回管理员列表数据？
        // 从接口刷新房间管理员列表
        _requestGuardian();
      },
      onRoomTheme: (YBDRoomTheme? value) {
        logger.v('onRoomTheme : ${value?.name} ${value?.image}');
        _state!.roomTheme = value;
        add(RoomEvent.UpdateRoomTheme);
      },
      onNoticeMessage: (value) {
        // 封号弹框需要测试
        _state?.noticeMessage = value as YBDInternal;
        add(RoomEvent.UpdateNotice);
      },
      onRoomExperience: (int? value, int? roomLevel) async {
        // if (_userInfo == null) {
        //   _userInfo = await YBDSPUtil.getUserInfo();
        // }
        logger.v('room level: $roomLevel');

        if (_state!.talentInfo?.roomlevel != null &&
            (_state!.talentInfo?.roomlevel ?? 0) > 0 &&
            (roomLevel ?? 0) > (_state!.talentInfo?.roomlevel ?? 0) &&
            _state!.talentInfo?.id == YBDCommonUtil.storeFromContext()!.state.bean!.id) {
          // _state.talentInfo?.roomlevel = roomLevel;
          _state!.showLevelUpDialog = true;
          add(RoomEvent.UpdateLevelUpDialog);
        }

        logger.v('showLevelUp: ${_state!.showLevelUpDialog}');
        // _state.talentInfo?.roomexper = value;
        _state!.talentInfo?.roomlevel = roomLevel;
        add(RoomEvent.UpdateRoomLevel);
      },
      onUnSubscribe: () {
        if (_state!.roomInfo?.protectMode == 0) {
          // 重新弹密码输入框
          _recheckRoomPwd();
        }
      },
      onGlobalGift: (YBDGlobal value) {
        logger.v('onBigGift: ${value.toJson()}');
        _state!.bigGifts!.add(YBDGlobal.copyWith(value));

        if (!state.showingBanner!) {
          _playBigGiftAnimation();
        }
      },
      onExitRoom: () {
        // 退出房间
        exitRoom();
      },
      onTeenPattiInfo: (value) {
        // logger.v("event: ${event.content}, ${event.result}");
        YBDTeenInfo.getInstance().pattiInfo = value;
        eventBus.fire(YBDReloadLoadingEvent());
      },
      onPKInvite: (source) async {
        //* 系统干预的匹配
        if (_state!.talentInfo?.id == YBDCommonUtil.storeFromContext()!.state.bean!.id &&
            source != _state!.roomId &&
            YBDLiveService.instance.data.pkInfo == null &&
            !showPKInviteDialog &&
            (g.Get.currentRoute.contains(YBDNavigatorHelper.flutter_room_list_page) ||
                g.Get.currentRoute.contains(YBDNavigatorHelper.flutter_room_page))) {
          showPKInviteDialog = true;
          Future.delayed(Duration(seconds: 20), () {
            showPKInviteDialog = false;
          });
          YBDDialogUtil.showPKInviteDialog(
            _roomContext,
            //* 接受pk邀请
            okCallback: () {
              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                YBDEventName.CLICK_EVENT,
                location: YBDLocationName.ROOM_PAGE,
                itemName: YBDItemName.PK_INVITE_ACCEPT,
              ));
              YBDRoomSocketApi().acceptInvite(source, roomId: YBDLiveService.instance.data.roomId,
                  onSuccess: (Response data) {
                if (data.code == Const.HTTP_SUCCESS) {
                  showPKInviteDialog = false;
                  pkStateBloc.add(PkState.PKing);
                } else if (data.code == Const.PK_MATCH_FAIL_OTHER) {
                  //* pk邀请已被其他人接收
                  YBDDialogUtil.showPKInviteDialog(
                    _roomContext,
                    okCallback: () {
                      showPKInviteDialog = false;
                      //* 开始匹配
                      YBDRoomSocketApi().startMatch(
                        roomId: YBDLiveService.instance.data.roomId,
                        onSuccess: (Response data) {
                          //开始匹配时如果 立马能匹配上 返回902004 如果当前没人 排队匹配的话返回000000
                          logger.v("start match with code: ${data.code}");
                          if (data.code == Const.PK_MATCH_SUCCESS || data.code == Const.HTTP_SUCCESS) {
                            pkStateBloc.add(PkState.Matching);
                          } else {
                            YBDToastUtil.toast(translate('failed'));
                          }
                        },
                      );
                    },
                    closeCallback: () {
                      showPKInviteDialog = false;
                      logger.v('clicked return btn');
                    },
                    bgImg: 'assets/images/liveroom/pk/pk_invited_bg.webp',
                    okImg: 'assets/images/liveroom/pk/pk_invited_start.webp',
                    closeImg: 'assets/images/liveroom/pk/pk_invited_return.webp',
                    haveCountDown: false,
                  );
                } else {
                  showPKInviteDialog = false;
                  YBDToastUtil.toast(translate('failed'));
                }
              }, onTimeOut: () {
                logger.v("accept Invite TimeOut");
              });
            },
            //* 拒绝pk邀请
            closeCallback: () {
              showPKInviteDialog = false;
              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                YBDEventName.CLICK_EVENT,
                location: YBDLocationName.ROOM_PAGE,
                itemName: YBDItemName.PK_INVITE_REJECT,
              ));
              YBDRoomSocketApi().rejectInvite(source, roomId: YBDLiveService.instance.data.roomId,
                  onSuccess: (Response data) {
                if (data.code == Const.HTTP_SUCCESS) {
                  pkStateBloc.add(PkState.None);
                } else {
                  YBDToastUtil.toast(translate('failed'));
                }
              }, onTimeOut: () {
                logger.v("reject Invite TimeOut");
              });
            },
            bgImg: 'assets/images/liveroom/pk/pk_invite_bg.webp',
            okImg: 'assets/images/liveroom/pk/pk_invite_accept.webp',
            closeImg: 'assets/images/liveroom/pk/pk_invite_refuse.webp',
            haveCountDown: true,
            countNum: 10,
          );
        }
      },
      onPKMatch: () {
        print('7.27------show PKStart Mic Notice Dialog');
        YBDDialogUtil.showPKStartMicNoticeDialog(_roomContext);
      },
      onTalentCreateRoom: (YBDInternal internal) {
        logger.d('YBDInternal internal type:${internal.type}  content:${internal.content} msgType:${internal.msgType}');
        _refreshRoomInfo(roomId, currentState: _state);
      },
    );
  }
  void _configRoomMsgHandlerfAIwEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 监听直播事件
  void _listenLiveEvent() {
    _liveEventSubscription = YBDLiveService.instance.onEvent.listen((event) {
      // logger.v('liveEventSubscription : $_roomId');
      if (event is YBDMessageEvent) {
        logger.v('====================event');
        _msgHandler.receivedMessage(event.message);
        _downloadHandler?.downloadWithMessage(event.message);
      } else if (event is YBDJoinChannelSuccessEvent) {
        logger.v('join channel success : ${event.channel}, ${event.uid}, ${event.elapsed}');
      } else if (event is YBDErrorEvent) {
        switch (event.type) {
          case LiveErrorType.SdkError:
            logger.e('sdk error : ${event.error}');
            break;
          case LiveErrorType.SubscribeRoomError:
            logger.e('subscribe room error : ${event.error}');
            // YBDToastUtil.toast(translate('unknown_error'));
            // YBDRoomHelper.closeRoom(context);
            break;
        }
      } else if (event is YBDPwdPassEvent) {
        _state!.checkPwdStatus = CheckPwdStatus.Passed;
        add(RoomEvent.UpdatePwdStatus);
      } else if (event is YBDPwdRetryEvent) {
        _state!.checkPwdStatus = CheckPwdStatus.Retry;
        add(RoomEvent.UpdatePwdStatus);
      } else if (event is YBDPwdIncorrectEvent) {
        logger.e(translate('retry_failed'));
        _state!.checkPwdStatus = CheckPwdStatus.Failed;
        YBDToastUtil.toast(translate('retry_failed'));
        // 退出房间
        exitRoom();
      }
    });
  }
  void _listenLiveEventsvLxcoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 处理房间下载任务
  void _configDownloadHandler() {
    _downloadHandler = YBDRoomDownloadHandler(
      roomId: _roomId,
      onRoomAnimationPath: (List<String?>? value, enter, gift) {
        logger.v('roomAnimationPath value download: $value, enter.carExtend: ${enter?.carExtend}');
        _state!.roomAnimationPath = value;
        _state!.enter = enter;
        _state!.gift = gift;
        add(RoomEvent.UpdateDownload);
      },
    );
    // _downloadHandler.configHandler();
  }

  /// 收到 unSubscribe 消息后重新验证密码
  Future<void> _recheckRoomPwd() async {
    logger.v('recheck room pwd');
    YBDRoomInfo? result = await ApiHelper.queryOnlineInfo(_context, _roomId);
    logger.v('room protect model : ${result?.protectMode}');
    _state!.roomInfo = result;
    add(RoomEvent.UpdateRoomInfo);

    if (_state!.roomInfo!.protectMode == 1) {
      logger.v('need check room pwd');
      if (!YBDRoomUtil.isTalent(_roomId)) {
        _state!.checkPwdStatus = CheckPwdStatus.NeedCheck;
        add(RoomEvent.UpdatePwdStatus);
      }
    }
  }

  /// 刷新房间信息
  Future<void> _refreshRoomInfo(
    int? roomId, {
    required YBDRoomState? currentState,
  }) async {
    YBDRoomInfo? result = await ApiHelper.queryOnlineInfo(_context, roomId);

    // 房间信息为空
    if (result == null) {
      logger.i('room info is null');
      return null;
    }

    // 更新房间状态
    _updateRoomState(result, currentState: currentState!);
  }

  /// 请求进房间信息
  Future<void> _requestJoinRoomInfo(
    int? roomId, {
    required YBDRoomState? currentState,
  }) async {
    YBDRoomInfo? result = await ApiHelper.queryOnlineInfo(_context, roomId);

    // 房间信息为空
    if (result == null) {
      logger.i('room info is null');
      return;
    }

    // 更新房间状态
    _updateRoomState(result, currentState: currentState!);

    // 获取房间信息后进房间
    _joinRoomWithValue(result);
  }

  /// 获取房间信息后切房间
  void _joinRoomWithValue(YBDRoomInfo value) {
    // 观众进加密房间
    if (value.isProtectRoom() && !YBDRoomUtil.isTalent(value.id)) {
      logger.i('protected room');
      return;
    }

    // 进房间
    YBDLiveService.instance.joinRoom(roomId: value.id!);
  }
  void _joinRoomWithValuevOwvuoyelive(YBDRoomInfo value) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求切房间信息
  Future<void> _requestSwitchRoomInfo(
    int? roomId, {
    required YBDRoomState? currentState,
  }) async {
    YBDRoomInfo? result = await ApiHelper.queryOnlineInfo(g.Get.context, roomId);

    // 房间信息为空
    if (result == null) {
      logger.i('room info is null');
      return;
    }

    // 更新房间状态
    _updateRoomState(result, currentState: currentState!);

    // 获取房间信息后切房间
    _switchRoomWithValue(result);
  }

  /// 获取房间信息后切房间
  void _switchRoomWithValue(YBDRoomInfo value) {
    // 观众进加密房间
    if (value.isProtectRoom() && !YBDRoomUtil.isTalent(value.id)) {
      logger.i('protected room');
      return;
    }

    // 切房间
    YBDLiveService.instance.switchRoom(roomId: value.id!);
  }

  /// 更新房间状态
  void _updateRoomState(
    YBDRoomInfo value, {
    required YBDRoomState currentState,
  }) {
    // 更新房间信息
    _updateRoomInfoWithValue(value);

    // 更新房间tag
    _updateRoomTagWithValue(value);

    // 更新房间密码状态
    _updatePwdStatusWithValue(value, currentStatus: currentState.checkPwdStatus);
  }

  /// 更新房间信息
  void _updateRoomInfoWithValue(YBDRoomInfo value) {
    _state!.roomInfo = value;
    add(RoomEvent.UpdateRoomInfo);
  }

  /// 更新房间tag
  void _updateRoomTagWithValue(YBDRoomInfo value) {
    String? tag = value.tag;
    if (tag == null) {
      // 兼容老版本
      List<String>? oldTitle = value.title?.split(',');
      if (oldTitle != null && oldTitle.length >= 2) {
        tag = oldTitle[0];
      }
    }

    _state!.roomTag = tag;

    // 三行麦位的时候需要调整 公聊区 高度
    if (tag == '1' || tag == '4' || tag == '5' || tag == '6') {
      _state!.dynamicHeight = 30;
    } else if (tag == '2') {
      _state!.dynamicHeight = 15;
    } else {
      _state!.dynamicHeight = 0;
    }

    add(RoomEvent.UpdateRoomTag);
  }
  void _updateRoomTagWithValueINaJ5oyelive(YBDRoomInfo value) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 更新房间密码状态
  void _updatePwdStatusWithValue(
    YBDRoomInfo value, {
    required CheckPwdStatus? currentStatus,
  }) {
    if (currentStatus != CheckPwdStatus.InitStatus) {
      logger.v('not CheckPwdStatus.InitStatus');
      return;
    }

    if (_state!.roomInfo!.protectMode == 1 && !YBDRoomUtil.isTalent(_roomId)) {
      logger.v('audience need check pwd');
      _state!.checkPwdStatus = CheckPwdStatus.NeedCheck;
    } else {
      logger.v('not protect room');
      _state!.checkPwdStatus = CheckPwdStatus.Passed;
    }

    add(RoomEvent.UpdatePwdStatus);
  }

  /// 查询房间管理员列表
  Future<void> _requestGuardian() async {
    List<YBDUserInfo?>? result = await ApiHelper.queryRoomManager(_context, _roomId);

    if (null != result) {
      // 按照用户等级降序排列
      result.sort((a, b) => b!.level!.compareTo(a!.level!));
      // 主播显示在第一条
      // result.removeWhere((e) => e?.id == _state.talentInfo?.id);
      // result.insert(0, _state.talentInfo);
      _state!.roomManagers = result;
      add(RoomEvent.UpdateManagerList);
    } else {
      logger.v('room guardian is null');
    }
  }

  /// 请求用户信息
  Future<void> _requestTalentInfo() async {
    YBDUserInfo? result = await ApiHelper.queryUserInfo(_context, _roomId);

    if (null != result) {
      // logger.v('talent info : ${result.roomexper}');
      _state!.talentInfo = result;
      add(RoomEvent.UpdateTalentInfo);
    } else {
      logger.v('talent info is null');
    }
  }
  void _requestTalentInfo9fPzNoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 检查是否充值用户
  Future<void> _topUpUser() async {
    YBDTopUpSummaryEntity? result = await ApiHelper.queryTopUpSummary(_context);
    if (result != null && result.record != null) {
      bool? isRecharge = result.record!.recharge;
      logger.v("_topUpUser isRecharge:$isRecharge ");
      if (isRecharge != null && isRecharge) {
        logger.v("_topUpUser isRecharge topUpHint:$isRecharge ");
        YBDSPUtil.save(Const.SP_KEY_RECHARGE, isRecharge);
      }
    }
  }

  /// 获取游戏数据
  Future<void> _getGameData() async {
    logger.v('get game data');
    YBDCommonUtil.getDefaultCountryCode(_context).then((value) {
      //获取国家码

      logger.v('getDefaultCountryCode countryCode: $value');
      ApiHelper.queryGame(_context, value).then((gameEntity) {
        //请求游戏数据
        if (gameEntity == null || gameEntity.returnCode != "000000") {
          // 刷新数据失败
          logger.v('queryGame failed');
        } else {
          logger.v('queryGame SUCCESS');

          _state!.gameData = gameEntity.record;
          add(RoomEvent.UpdateGameData);
          logger.v("gameData.length: ${_state!.gameData!.length}");
        }
      });
    });
  }

  /// 处理漏掉的消息
  void _handleMessageLoss() {
    // 处理开播页面的消息
    YBDLiveService.instance.data.startPageMsgList.forEach((element) {
      _msgHandler.receivedMessage(element);
      _downloadHandler?.downloadWithMessage(element);
    });
  }

  /// 播放房间大礼物动画
  void _playBigGiftAnimation() {
    logger.v('_playBigGiftAnimation showingBanner : ${_state!.showingBanner}');
    if (_state!.showingBanner!) {
      logger.v('BigGiftUpdated showingBanner');
      return;
    }

    logger.v("bigGiftTop: ${_state!.bigGiftTop}");
    setBannerType(1);

    _timerNum = 0;
    _state!.bigGiftTop = -200;

    _bigGiftTimer?.cancel();
    _bigGiftTimer = null;
    _bigGiftTimer = Timer.periodic(Duration(milliseconds: 100), (_) {
      if (_timerNum < 0) {
        logger.v('BigGiftUpdated _timerNum: $_timerNum');
        _bigGiftTimer!.cancel();
        return;
      }

      setShowingBanner(true);
      _timerNum++;

      if (_timerNum >= 20 && _timerNum <= 50) {
        // 悬停效果
      } else if (_timerNum < 20) {
        _state!.bigGiftTop += 10;
        // 向下滑动的效果
        add(RoomEvent.UpdateBigGift);
      } else if (_timerNum > 70) {
        _bigGiftTimer!.cancel();
        _state!.bigGiftTop = -200;
        _timerNum = -1;

        if (null != _state!.bigGifts && _state!.bigGifts!.isNotEmpty) {
          _state!.bigGifts!.removeAt(0);
        }
        setShowingBanner(false);
        logger.v("big gift count: ${_state!.bigGifts!.length}");

        if (_state!.bigGifts!.length > 0) {
          _playBigGiftAnimation();
        } else {
          // 隐藏大礼物
          add(RoomEvent.UpdateBigGift);
        }
      } else if (_timerNum > 50) {
        _state!.bigGiftTop -= 10;
        // 向上滑动的效果
        add(RoomEvent.UpdateBigGift);
      }

      logger.v("bigGiftTop: ${_state!.bigGiftTop}, _timerNum: $_timerNum");
    });
    // gift_x
  }
  void _playBigGiftAnimationMzl8Zoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
