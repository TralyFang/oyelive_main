import 'dart:async';

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/room_socket/message/base/message.dart';
import '../../../../common/room_socket/message/common/enter.dart';
import '../../../../common/room_socket/message/common/gift.dart';
import '../../../../common/util/download_ybd_manager.dart';
import '../../../../common/util/download_ybd_util.dart';
import '../../../../common/util/file_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/local_ybd_animation_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/zip_ybd_util.dart';
import '../../../../common/widget/local_ybd_animation_entity.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../home/entity/car_ybd_list_entity.dart';
import '../../home/entity/gift_ybd_list_entity.dart';
import '../../store/entity/downloadbean.dart';
import '../define/room_ybd_define.dart';
import '../service/live_ybd_service.dart';

/// 下载房间座驾和背景图的类
class YBDRoomDownloadHandler {
  int? roomId;
  final OnRoomAnimationPath? onRoomAnimationPath;

  /// 改变房间id，切房间后处理对应房间的消息
  void changeRoomId(int? value) {
    logger.v('change room : $roomId -> $value');
    roomId = value;
  }
  void changeRoomIdCoxpAoyelive(int? value) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  YBDRoomDownloadHandler({
    required this.roomId,
    this.onRoomAnimationPath,
  }) {
    YBDDownLoadManager.instance!.addCallBacks(
      YBDDownLoadManager.instance!.callBackRoomEntrances,
      _downloadCallback,
    );

    _carList = YBDLiveService.instance.carList;
    _giftList = YBDLiveService.instance.giftList;

    // 每次进房间刷新一次座驾列表和礼物列表
    _requestGiftList();
    _requestCarList();
  }

  /// 总座驾（包含送的）
  List<YBDCarListRecord?>? _carList = [];

  /// 礼物列表
  List<YBDGiftListRecordGift?>? _giftList = [];

  /// 进房动画地址集
  Map<String, List<String?>> _enterRoomMap = new Map<String, List<String>>();

  /// 进房动画模型地址集
  Map<String, YBDEnter> _enterRoomCarMap = new Map<String, YBDEnter>();

  /// 房间礼物动画模型地址集
  Map<String, YBDGift> _roomSendGiftMap = new Map<String, YBDGift>();

  /// 播放状态
  bool _roomAnimationPlay = false;

  List<YBDDownLoadBean> _downLoadList = [];

  /// 当前播放礼物  key
  String? _curAnimationKey;

  /// 动画定时器
  Timer? animationTimer;

  /// 待播放的动画
  List<Map<String, dynamic>> _animationAwaiting = [];
  /// 当前播放的动画信息
  String? lastPlayingExt;

  /// 根据消息类型下载对应的资源
  Future<void> downloadWithMessage(YBDMessage message) async {
    if (message.roomId != roomId) {
      // 过滤其他房间的消息
      return;
    }
    downloadFileWithMessage(message);
    return;

    if (message is YBDEnter) {
      logger.v('received YBDEnter Msg carId:${message.carId} carName:${message.carName} fromUser:${message.fromUser}');

      if (message.carId == null) {
        return;
      }
      if (_enterRoomCarMap.containsKey(message.fromUser.toString())) {
        _enterRoomCarMap.update(message.fromUser.toString(), (value) => message);
      } else {
        _enterRoomCarMap.putIfAbsent(message.fromUser.toString(), () => message);
      }

      /// 通过message.carId判断是否是本地动画（Assets）
      YBDLocalAnimationItems? localAnimationItems = await YBDLocalAnimationUtil.getLocalAnimationById(message.carId);
      if (localAnimationItems != null) {
        logger.d(
            'room enter animation id：${localAnimationItems.animationId} svga：${localAnimationItems.svga} mp3：${localAnimationItems.mp3}');
        showAssetsAnimation(message.fromUser, localAnimationItems);
        return;
      }

      checkRoomAnimation(
        YBDCarListRecord()
          ..id = message.carId
          ..name = message.carName
          ..fromUser = message.fromUser,
        null,
        downLoadType: DownLoadType.svg_entrances,
      );
    } else if (message is YBDGift) {
      logger.v('gift msg : ${message.toJsonContent()}');

      // 记录发送礼物的模型
      if (_roomSendGiftMap.containsKey(message.fromUser.toString())) {
        _roomSendGiftMap.update(message.fromUser.toString(), (value) => message);
      } else {
        _roomSendGiftMap.putIfAbsent(message.fromUser.toString(), () => message);
      }

      checkRoomAnimation(null, message, downLoadType: DownLoadType.gift);
    }
  }
  void downloadWithMessageLKwltoyelive(YBDMessage message)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  



  // 下载文件
  Future<void> downloadFileWithMessage(YBDMessage message) async {

    YBDCarListRecordAnimationInfo? carEntity; // 需要解析的座驾文件信息
    YBDGift? gift;
    YBDEnter? enter;
    switch (message.runtimeType) {
      case YBDGift:
        gift = message as YBDGift;
        try {
          if (gift.animationUrl != null) {
            var json = jsonDecode(gift.animationUrl!);
            carEntity = YBDCarListRecordAnimationInfo().fromJson(json);
          }
        }catch (e) {
          logger.e("${message.runtimeType} downloadFileWithMessage json error: $e");
        }
        break;
      case YBDEnter:
        enter = message as YBDEnter;
        try {
          if (enter.carAnimation != null) {
            var json = jsonDecode(enter.carAnimation!);
            carEntity = YBDCarListRecordAnimationInfo().fromJson(json);
          }
        }catch (e) {
          logger.e("${message.runtimeType} downloadFileWithMessage json error: $e");
        }
        break;
    }
    if (carEntity == null || carEntity.name == null) return; // 不存在

    // 文件下载地址
    var url = YBDImageUtil.animZipFile(YBDLiveService.instance.mainContext, carEntity.foldername).toString();
    // 文件保存地址
    String targetPath = "${await YBDCommonUtil.getResourceDir("Animation")}/${carEntity.foldername}/${carEntity.version}";
    logger.v("${message.runtimeType} downloadFileWithMessage car url: $url, targetPath: $targetPath");

    YBDDownloadCacheManager.instance.downloadURLToPath(url, targetPath, (path) {

      var svgaMp3s = path!.split(",");
      List<String?> localPath = List.filled(2, null);
      localPath[0] = svgaMp3s.first;
      if (svgaMp3s.length == 2) {
        localPath[1] = svgaMp3s.last;
      }
      var map = {
        'path': localPath,
        'enter': enter,
        'gift': gift,
        'url':url,
        'fromUser':message.fromUser.toString(),
      };
      checkAddPlaying(map);
    });

    /*
      * downloadEnterRoomAnimation downloadUrl gift:http://s3-us-west-2.amazonaws.com/thankyotest/anim/128fb26f-8afd-43f4-8b1a-ffae628114ff.zip,
      * YBDGift.json: {
      * code: 2001107,
      * name: Love,
      * num: 1,
      * imgUrl: 1569919507411.png,
      * animationUrl:
      *   {
            "animationId":2001107,
            "animationType":7,
            "foldername":"128fb26f-8afd-43f4-8b1a-ffae628114ff",
            "framDuration":null,
            "name":"128fb26f-8afd-43f4-8b1a-ffae628114ff.zip",
            "version":5
          },
      * animation: null,
      * price: 200,
      * combo: 1,
      * customized: null,
      * destination: /room/5000152,
      * fromUser: 2100181,
      * mode: m,
      * msgType: gift,
      * receiver: 152@@@ ⃟🇲𝐢𝐬𝐡𝐚𝐥༗⃝🇸𝐡𝐚𝐡,
      * receiverImg: 1657695885608.jpg,
      * receiverLevel: 17,
      * receiverSex: 0,
      * receiverVip: 0,
      * roomId: 5000152,
      * sender: Aks,
      * senderImg: 1655187313546.png,
      * senderLevel: 15,
      * senderSex: 1,
      * senderVip: 0,
      * time: 09:14,
      * toUser: 5000152
      * }
      * **/

    /*
    * YBDEnter.json: {
    * roomId: 5000152,
    * destination: /room/5000152,
    * costTime: [39, 39, 43],
    * msgType: enter,
    * fromUser: 2100181,
    * toUser: -1,
    * mode: m,
    * sender: Aks,
    * senderImg: 1655187313546.png,
    * senderLevel: 16,
    * senderVip: 0,
    * senderSex: 1,
    * receiver: null,
    * receiverImg: null,
    * receiverLevel: 0,
    * receiverVip: null,
    * receiverSex: null,
    * time: 09:20,
    * millisTime: 1657704026174,
    * senderCerInfos: null,
    * equipGoods: null,
    * userLevelEnterTip: https://oyetalk-status.s3.ap-south-1.amazonaws.com/icon/level/entry/1-29.png,,
    * carId: 3423567,
    * carName: name1,
    * carImg: 1649644793674.png,
    * carAnimation:
    * {
        "animationId":3423567,
        "animationType":8,
        "foldername":"43efbf72-4934-4f65-b8aa-dd3e3cb13167.mp4",
        "framDuration":8,
        "name":"43efbf72-4934-4f65-b8aa-dd3e3cb13167.mp4",
        "version":4
      },
    * carExtend: null}

    *
    *
    * **/

  }
  void downloadFileWithMessageMqYPfoyelive(YBDMessage message)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 进房动画 &&礼物动画
  /// 用缓存库下载礼物动画
  checkRoomAnimation(YBDCarListRecord? carEnterRoom, YBDGift? gift, {downLoadType = DownLoadType.svg_entrances}) async {
    late String downloadUrl;
    late YBDCarListRecordAnimationInfo carListRecordAnimationInfo;
    bool isSave = false;
    if (downLoadType == DownLoadType.svg_entrances) {
      if (_carList!.length <= 0) {
        logger.v('===_carList is empty');
        _requestCarList();
        return;
      }

      logger.v('enter msg get carListRecordAnimationInfo');
      YBDCarListRecord? item;
      for (int k = 0; k < _carList!.length; k++) {
        item = _carList![k];
        if (item!.animationInfo != null) {
          if (item.animationInfo!.animationId == carEnterRoom!.id) {
            logger.v('===download car context ${YBDLiveService.instance.mainContext}');
            downloadUrl =
                YBDImageUtil.animZipFile(YBDLiveService.instance.mainContext, item.animationInfo!.foldername).toString();
            logger.v('downloadEnterRoomAnimation downloadUrl svg_entrances:$downloadUrl');
            carListRecordAnimationInfo = YBDCarListRecordAnimationInfo()
              ..version = item.animationInfo!.version
              ..foldername = item.animationInfo!.foldername
              ..name = item.animationInfo!.name
              ..animationId = item.animationInfo!.animationId;

            isSave = await YBDFileUtil.isAnimationDownloaded(
              carListRecordAnimationInfo,
              downLoadType: DownLoadType.svg_entrances,
            );
            break;
          }
        }
      }
    } else if (downLoadType == DownLoadType.gift) {
      if (_giftList!.length <= 0) {
        logger.v('===_giftList is empty');
        _requestGiftList();
        return;
      }

      YBDGiftListRecordGift? item;
      for (int k = 0; k < _giftList!.length; k++) {
        item = _giftList![k];
        if (item!.animationInfo != null) {
          if (item.animationInfo!.animationId == gift!.code) {
            logger.v('context ${YBDLiveService.instance.mainContext}');
            downloadUrl =
                YBDImageUtil.animZipFile(YBDLiveService.instance.mainContext, item.animationInfo!.foldername).toString();
            logger.v('downloadEnterRoomAnimation downloadUrl gift:$downloadUrl');
            carListRecordAnimationInfo = YBDCarListRecordAnimationInfo()
              ..version = item.animationInfo!.version
              ..foldername = item.animationInfo!.foldername
              ..name = item.animationInfo!.name
              ..animationId = item.animationInfo!.animationId;
            logger.v('foldername: ${carListRecordAnimationInfo.foldername}');
            isSave = await YBDFileUtil.isAnimationDownloaded(carListRecordAnimationInfo,
                downLoadType: DownLoadType.svg_entrances);
            break;
          }
        }
      }
    }

    logger.v("enterRoomAnimationState isSave :$isSave  downLoadType:$downLoadType");
    if (isSave) {
      ///本地要是已经下载好  就直接播放
      List path = await YBDFileUtil.getSvgPlayPath(carListRecordAnimationInfo);
      logger.v("enterRoomAnimationState path ${path.toString()}");

      _animationFileSaved(
        path: path,
        carEnterRoom: carEnterRoom,
        gift: gift,
        downLoadType: downLoadType,
      );
    } else {
      logger.v('===download carListRecordAnimationInfo ${carListRecordAnimationInfo.name}, url: $downloadUrl');

      // 用缓存下载礼物动画
      File file = await DefaultCacheManager().getSingleFile(downloadUrl);
      logger.v('===download carListRecordAnimationInfo file: ${file.path}');

      // 获取svga的解压路径
      var svgaDir = await YBDFileUtil.animationDownloadedPath(carListRecordAnimationInfo);
      logger.v('giftSvgaDir: $svgaDir');

      // 解压zip文件
      bool result = YBDZipUtil.unzipSvgaAnimationZipFile(outDir: svgaDir, zip: file);
      logger.v('unzipGift result: $result');

      if (result) {
        // 解析动画的svga和mp3文件路径
        List svgaPath = await YBDFileUtil.getSvgPlayPath(carListRecordAnimationInfo);
        _animationFileSaved(
          path: svgaPath,
          carEnterRoom: carEnterRoom,
          gift: gift,
          downLoadType: downLoadType,
        );
      }

      ///没下载的  去下载
      // YBDDownLoadBean downLoadBean = new YBDDownLoadBean(
      //   downloadUrl,
      //   fileName: carListRecordAnimationInfo?.name,
      //   savePath:
      //       '${downLoadType == DownLoadType.svg_emoji ? Const.ANIMATION_EMOJI : Const.ANIMATION}/${carListRecordAnimationInfo.foldername}/${carListRecordAnimationInfo.version}',
      //   type: downLoadType,
      //   fromUser: downLoadType == DownLoadType.svg_entrances ? carEnterRoom.fromUser : gift.fromUser,
      // );

      // String taskId = await YBDDownLoadManager.instance.addDownLoadfile(downLoadBean);
      // Lock().synchronized(() async {
      //   logger.v("checkRoomAnimation  name:${carListRecordAnimationInfo.name} taskId:$taskId");
      //   if (taskId != null) {
      //     downLoadBean.taskId = taskId;
      //     _downLoadList.add(downLoadBean);
      //   }
      // });

    }
  }

  /// 本地已存在指定动画文件的处理逻辑
  /// [path]本地动画目录中的svga和mp3的文件路径
  /// [carEnterRoom]座驾消息
  /// [gift]礼物消息
  /// [downLoadType]动画类型
  void _animationFileSaved({
    required List path,
    YBDCarListRecord? carEnterRoom,
    YBDGift? gift,
    DownLoadType downLoadType = DownLoadType.svg_entrances,
  }) {
    if (downLoadType == DownLoadType.svg_room_bg) {
    } else if (downLoadType == DownLoadType.svg_entrances) {
      _carAnimationSaved(carEnterRoom, path);
      _enterRoomAnimationState(_roomAnimationPlay);
    } else if (downLoadType == DownLoadType.gift) {
      _giftAnimationSaved(gift, path);
      _enterRoomAnimationState(_roomAnimationPlay);
    }
  }

  /// 本地已存在该礼物的动画文件的处理逻辑
  /// [gift]礼物消息
  /// [path]本地动画目录中的svga和mp3的文件路径
  void _giftAnimationSaved(YBDGift? gift, List path) {
    List<String?> localPath = List.filled(2, null);
    localPath[0] = path[0];
    localPath[1] = path[1];

    if (localPath[0] == null) {
      return;
    }

    logger.v("enterRoomAnimationState localPath ${localPath.toString()}");

    if (_enterRoomMap.containsKey(gift!.fromUser.toString())) {
      _enterRoomMap.update(gift.fromUser.toString(), (value) => localPath);
    } else {
      _enterRoomMap.putIfAbsent(gift.fromUser.toString(), () => localPath);
    }
  }
  void _giftAnimationSavedUCr3zoyelive(YBDGift? gift, List path) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 本地已存在该座驾的动画文件的处理逻辑
  /// [carEnterRoom]座驾消息
  /// [path]本地动画目录中的svga和mp3的文件路径
  void _carAnimationSaved(YBDCarListRecord? carEnterRoom, List path) {
    List<String?> localPath = List.filled(2, null);
    localPath[0] = path[0];
    localPath[1] = path[1];

    if (localPath[0] == null) {
      return;
    }

    if (_enterRoomMap.containsKey(carEnterRoom!.fromUser.toString())) {
      _enterRoomMap.update(carEnterRoom.fromUser.toString(), (value) => localPath);
    } else {
      _enterRoomMap.putIfAbsent(carEnterRoom.fromUser.toString(), () => localPath);
    }
  }
  void _carAnimationSavedP0TiGoyelive(YBDCarListRecord? carEnterRoom, List path) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 进房动画&&礼物动画 播放状态
  void _enterRoomAnimationState(bool playing) {
    logger.v("enterRoomAnimationState playing:$playing");
    _roomAnimationPlay = playing;
    try {
      if (!playing && _enterRoomMap.length > 0) {
        _roomAnimationPlay = true;
        _enterRoomMap.forEach((key, value) {
          if (null != onRoomAnimationPath) {
            var enter = _enterRoomCarMap[key];
            var gift = _roomSendGiftMap[key];
            logger.v("enterRoomAnimationState enter.carExtend: ${enter?.carExtend}, "
                "enterMap: $_enterRoomCarMap,"
                "gift.customized: ${gift?.customized},"
                "giftMap: $_roomSendGiftMap");
            onRoomAnimationPath!(value, enter, gift);
          }

          _curAnimationKey = key;
//          enterRoomMap.remove(key);
//           print("enterRoomAnimationState key----1:$key value:${value[0]}");
          return;
        });
      } else {
        _animationTimer();
      }
    } catch (e) {
      logger.e("enterRoomAnimationState error:$e");
    }
  }
  void _enterRoomAnimationStatew4ZBioyelive(bool playing) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 检查动画定时器
  _animationTimer() {
    if (animationTimer == null) {
      animationTimer = Timer.periodic(Duration(milliseconds: 3000), (t) {
        _haveAnimation();
        /* print('执行');
            Navigator.pop(context);
            t.cancel();
            t.isActive;*/
      });
    } else if (!animationTimer!.isActive) {
      animationTimer!.cancel();
      animationTimer = null;
      animationTimer = Timer.periodic(Duration(milliseconds: 3000), (t) {
        _haveAnimation();
      });
    }
  }

  _haveAnimation() {
    // print("_haveAnimation enterRoomMap length:${enterRoomMap.length} roomAnimationplay:$roomAnimationPlay");
    if (_enterRoomMap.length == 0) {
      animationTimer!.cancel();
      return;
    }
    if (_roomAnimationPlay) {
      return;
    }
    try {
      _enterRoomMap.forEach((key, value) {
        if (null != onRoomAnimationPath) {
          var enter = _enterRoomCarMap[key];
          var gift = _roomSendGiftMap[key];
          logger.v("haveAnimation enter.carExtend: ${enter?.carExtend}, "
              "enterMap: $_enterRoomCarMap"
              "gift.customized: ${gift?.customized},"
              "giftMap: $_roomSendGiftMap");
          onRoomAnimationPath!(value, enter, gift);
        }

        _curAnimationKey = key;
//        enterRoomMap.remove(key);
//         print("_haveAnimation enterRoomMap key----2:$key value:${value[0]}");
        return;
      });
    } catch (e) {
      logger.e("_haveAnimation enterRoomMap  Error value:${e.toString()}");
    }
  }

  /// 请求座驾列表数据(用于动画解析)
  Future<void> _requestCarList() async {
    YBDCarListEntity? carListEntity = await ApiHelper.carList(YBDLiveService.instance.mainContext, queryAll: true);
    if (null != carListEntity && carListEntity.returnCode == Const.HTTP_SUCCESS) {
      logger.v('room bloc queryCAR SUCCESS');
      _carList = carListEntity.record;
    } else {
      logger.v('room bloc queryCAR failed');
    }
  }
  void _requestCarListsZcPaoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求礼物列表数据
  Future<void> _requestGiftList() async {
    YBDGiftListEntity? giftListEntity = await ApiHelper.giftList(YBDLiveService.instance.mainContext);

    if (null != giftListEntity && null != giftListEntity.record && giftListEntity.record!.isNotEmpty) {
      // 保存礼物列表数据
      _giftList = giftListEntity.record![0]!.gift;
    } else {
      logger.v('gift list is null');
    }
  }

  /// 下载进度回调
  _downloadCallback(YBDDownloadTaskInfo event) async {
//    print('downloadCallback callBackMic updateProgress');
    if (event == null) {
      logger.v('downloadCallback callBackMic task info arg is null');
      return;
    }

    logger.v('downloadCallback----');

    for (int i = 0; i < _downLoadList.length; i++) {
      logger.v('downloadCallback i:$i taskId:${_downLoadList[i].taskId}');

      if (_downLoadList[i].taskId != null && _downLoadList[i].taskId!.compareTo(event.id!) == 0) {
        if (event.status == DownloadTaskStatus.complete) {
          logger.v(
              'downloadCallback----complete taskID:${event.id} type:${_downLoadList[i].type} fileName:${_downLoadList[i].fileName}');
          if (_downLoadList[i].type == DownLoadType.svg_entrances) {
            bool isUnzip = await YBDZipUtil.unzipSvga(i, _downLoadList, _carList);
            if (isUnzip) {
              for (int j = 0; j < _carList!.length; j++) {
                if (_downLoadList[i].fileName == _carList![j]!.animationInfo!.name) {
                  List? path = await YBDFileUtil.getLocalUrl(_downLoadList[i], _carList!);
                  if (path == null) {
                    return;
                  }
                  List<String?> localPath = List.filled(2, '');
                  localPath[0] = path[0];
                  localPath[1] = path[1];
                  if (localPath[0] == null) {
                    return;
                  }
                  if (_enterRoomMap.containsKey(_downLoadList[i].fromUser.toString())) {
                    _enterRoomMap.update(_downLoadList[i].fromUser.toString(), (value) => localPath);
                  } else {
                    _enterRoomMap.putIfAbsent(_downLoadList[i].fromUser.toString(), () => localPath);
                  }
//                  showEnterRoomAnimation();
                  logger.v("enterRoomAnimationState roomAnimationplay---001:$_roomAnimationPlay");
                  _enterRoomAnimationState(_roomAnimationPlay);
                }
              }
              return;
            }
          } else if (_downLoadList[i].type == DownLoadType.gift) {
            if (_giftList!.length <= 0) {
              return;
            }
            bool isUnzip = await YBDZipUtil.unzipGiftAnimation(i, _downLoadList, _giftList);
            if (isUnzip) {
              for (int j = 0; j < _giftList!.length; j++) {
                if (_downLoadList[i].fileName == _giftList![j]!.animationInfo!.name) {
                  List? path = await YBDFileUtil.getGiftLocalUrl(_downLoadList[i], _giftList!);
                  if (path == null) {
                    return;
                  }
                  List<String?> localPath = List.filled(2, '');
                  localPath[0] = path[0];
                  localPath[1] = path[1];
                  if (localPath[0] == null) {
                    return;
                  }
                  if (_enterRoomMap.containsKey(_downLoadList[i].fromUser.toString())) {
                    _enterRoomMap.update(_downLoadList[i].fromUser.toString(), (value) => localPath);
                  } else {
                    _enterRoomMap.putIfAbsent(_downLoadList[i].fromUser.toString(), () => localPath);
                  }
//                  showEnterRoomAnimation();
                  logger.v("enterRoomAnimationState roomAnimationplay---002$_roomAnimationPlay");
                  _enterRoomAnimationState(_roomAnimationPlay);
                }
              }
              return;
            }
          }
          _downLoadList.remove(i);
        } else if (event.status == DownloadTaskStatus.failed) {
          logger.v('downloadCallback----failed taskID:${event.id}');
          _downLoadList.remove(i);
        }
        break;
      }
    }

    return;
  }

  // 播放完了，检查播放下一个
  checkNextPlaying(bool playing) async {
    logger.v("checkNextPlaying: $playing, _animationAwaiting: ${_animationAwaiting.length}");
    _roomAnimationPlay = playing;
    if (!_roomAnimationPlay) {
      // 先赋值为空，避免动画不起作用。再播放下个动画。
      lastPlayingExt = null;
      onRoomAnimationPath?.call([null, null], null, null);
      if (_animationAwaiting.length > 0) {
        await Future.delayed(Duration(milliseconds: 100));
        var map = _animationAwaiting.first;
        YBDGift? gift = map['gift'];
        YBDEnter? enter = map['enter'];
        List<String>? localPath = map['path'];
        String? url = map['url'];
        int? fromUser = map['fromUser'];
        if ((gift != null && gift.roomId != roomId) || (enter != null && enter.roomId != roomId)) {
          // 已经不在当前房间了，就丢弃，检查下一个
          _animationAwaiting.removeAt(0);
          checkNextPlaying(_roomAnimationPlay);
        }else {
          onRoomAnimationPath?.call(localPath, enter, gift);
          _animationAwaiting.removeAt(0);
          _roomAnimationPlay = true;
          lastPlayingExt = "$fromUser##$url";
        }
      }
    }
  }

  // 用户送礼了，先检查状态，再播放
  checkAddPlaying(Map<String, dynamic> map) {
    logger.v("checkAddPlaying: $_roomAnimationPlay, map: $map");
    YBDGift? gift = map['gift'];
    YBDEnter? enter = map['enter'];
    List<String>? localPath = map['path'];
    String? url = map['url'];
    String? fromUser = map['fromUser'];
    if (!_roomAnimationPlay) {
      onRoomAnimationPath!(localPath, enter, gift);
      _roomAnimationPlay = true;
      lastPlayingExt = "$fromUser##$url";
    }else {
      var lasts = lastPlayingExt?.split('##') ?? <String?>[null, null];
      String? preFromUser = lasts.first;
      String? preUrl = lasts.last;
      if (preFromUser == "$fromUser" && preUrl == url) {
        // 同一个人的combo就不追加了
      }else {
        _animationAwaiting.add(map);
      }
    }
  }

  /// 播放结束 路径清空
  showEnterRoomAnimation(bool playing) {

    logger.v("showEnterRoomAnimation complete playing: $playing, _animationAwaiting: ${_animationAwaiting.length}");
    // 播放完了需要检查是否还有下一个需要播放的动画
    checkNextPlaying(playing);
    return;

    logger.v(
        "showEnterRoomAnimation playing:$playing  !playing${!playing} enterRoomMap.length:${_enterRoomMap.length} curAnimationKey:$_curAnimationKey");
    if (!playing) {
      if (null != onRoomAnimationPath) {
        onRoomAnimationPath!([null, null], null, null);
      }

      if (_enterRoomMap.length > 0 && _curAnimationKey != null) {
        _enterRoomMap.remove(_curAnimationKey);
      }

      _roomAnimationPlay = false;
    }
  }

  ///加载本地assets动画
  showAssetsAnimation(int? fromUser, YBDLocalAnimationItems localAnimationItems) {
    List<String?> localPath = List.filled(3, null, growable: false);
    localPath[0] = localAnimationItems.svga;
    localPath[1] = localAnimationItems.mp3;
    localPath[2] = "true";

    if (localPath[0] == null) {
      return;
    }

    if (_enterRoomMap.containsKey(fromUser)) {
      _enterRoomMap.update(fromUser.toString(), (value) => localPath);
    } else {
      _enterRoomMap.putIfAbsent(fromUser.toString(), () => localPath);
    }
    _enterRoomAnimationState(_roomAnimationPlay);
  }
}
