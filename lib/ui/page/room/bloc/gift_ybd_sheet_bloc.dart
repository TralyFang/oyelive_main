import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/room_socket/message/resp/response.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/entity/level_ybd_info_entity.dart';
import 'package:oyelive_main/module/entity/payment_ybd_result_entity.dart';
import 'package:oyelive_main/module/room/entity/gift_ybd_package_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/room/widget/gift_ybd_receiver.dart';

import '../../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/room/entity/gift_ybd_package_entity.dart';
import '../../../../redux/user_ybd_redux.dart';
import '../widget/gift_ybd_receiver.dart';

class YBDGiftSheetBloc extends Bloc<YBDGiftPackageState, YBDGiftPackageState> {
  late YBDGiftPackageState state;
  YBDLevelInfoEntity? levelInfoEntity;

  YBDGiftSheetBloc(init) : super(init) {
    state = init;
  }

  @override
  Stream<YBDGiftPackageState> mapEventToState(YBDGiftPackageState event) async* {
    // TODO: implement mapEventToState

    switch (event.sheetAction) {
      case SheetAction.Init:
        // TODO: Handle this case.
        levelInfoEntity = event.levelInfoEntity;
        logger.v('YBDGiftSheetBloc levelInfoEntity ${levelInfoEntity?.record}');
        break;
      case SheetAction.ChangeTab:
        // TODO: Handle this case.
        state.viewingTabIndex = event.viewingTabIndex;
        state.selectingGiftIndex = null;
        state.giftInfo = null;
        break;
      case SheetAction.ChangePage:
        // TODO: Handle this case.
        state.viewingPageIndex = event.viewingPageIndex;
        break;
      case SheetAction.SelectGift:
        // TODO: Handle this case.
        state.selectingGiftIndex = event.selectingGiftIndex;
        state.giftInfo = event.giftInfo;
        break;
      case SheetAction.ChangeMultiple:
        // TODO: Handle this case.
        state.multiple = event.multiple;

        break;
      case SheetAction.SelectReceiver:
        // TODO: Handle this case.
        state.receiverUser = event.receiverUser;
        break;
    }
    state.sheetAction = event.sheetAction;

    yield new YBDGiftPackageState.clone(state);
  }
  void mapEventToStatevS7bFoyelive(YBDGiftPackageState event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  checkCanSend(int? money, BuildContext context) {
    if (state.receiverUser == null) {
      YBDToastUtil.toast(translate('select_receiver'));
      return false;
    }

    if (state.selectingGiftIndex == null) {
      YBDToastUtil.toast(translate('select_gift'));
      return false;
    }

    logger.v('state.giftInfo viewingTabIndex:${state.viewingTabIndex}');
    if (state.giftInfo?.personalId != null && (state.giftInfo?.number ?? 0) < state.multiple!) {
      YBDToastUtil.toast(translate('amount_insufficient'));

      // 余额不足的判断是不是放到服务端了
      // showDialog<Null>(
      //     context: context, //BuildContext对象
      //     barrierDismissible: false,
      //     builder: (BuildContext context) {
      //       return YBDInsufficinetDialog();
      //     });
      state.giftInfo = null;
      state.selectingGiftIndex = null;
      return false;
    }

    // 检查礼物限制条件
    var isRestriction = YBDCommonUtil.stintBuy(
      type: state.giftInfo!.conditionType,
      lv: state.giftInfo!.conditionExtends,
    );

    if (isRestriction) {
      YBDToastUtil.toast(translate('conditions_are_not_met'));
      return false;
    }

    // 默认返回值
    return true;

    // if (state.multiple * state.giftInfo.price > money) {
    //   // YBDToastUtil.toast(translate('amount_insufficient'));
    //   showDialog<Null>(
    //       context: context, //BuildContext对象
    //       barrierDismissible: false,
    //       builder: (BuildContext context) {
    //         return YBDInsufficinetDialog();
    //       });
    //   return false;
    // }
    //
    // return true;
  }

  var lastRoomId;
  var lastStore;

  send(roomId, store, {int combo: 1, BuildContext? context}) async {
    lastRoomId = roomId;
    lastStore = store;
    YBDLogUtil.v('send gift context context:$context');
    await YBDRoomSocketApi.getInstance().sendGift(
        roomId: roomId,
        to: state.receiverUser!.userId,
        code: state.giftInfo!.id,
        num: state.multiple,
        combo: combo,
        personalId: state.giftInfo?.personalId,
        onSuccess: (data) async {
          YBDLogUtil.v('send gift onSuccess----!');
          if (state.giftInfo!.expireAfter == null) {
            store.state.bean.money = store.state.bean.money - state.giftInfo!.price! * state.multiple!;
            store.dispatch(YBDUpdateUserAction(store.state.bean));
          } else {
            state.selectingGiftIndex = null;
            int number = state.giftInfo!.number ?? 0;
            number -= state.multiple!;
            state.giftInfo!.number = number;
          }
          checkUserLevel(context, data);
        },
        onTimeOut: () {
          YBDLogUtil.v('send gift time out!');
        });
  }

  sendCombo(combo, context) {
    YBDLogUtil.v('send gift time out! context:$context');
    if (checkCanSend(lastStore.state.bean.money, context)) send(lastRoomId, lastStore, combo: combo, context: context);
  }

  ///判断用户是否升级
  Future checkUserLevel(BuildContext? context, Response data) async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
    YBDPaymentResultEntity? paymentResultEntity = data.getResult<YBDPaymentResultEntity>();
    if (userInfo == null || paymentResultEntity == null) {
      YBDLogUtil.d(
          'checkUserLevel  userInfo or paymentResultEntity is null. userInfo:${userInfo} . paymentResultEntity:${paymentResultEntity}');
      return;
    }
    YBDLogUtil.v('userInfo.experience 3333:$context');
    if (paymentResultEntity.payResult?.userExp != null) {
      userInfo.experience = paymentResultEntity.payResult?.userExp;
      await YBDSPUtil.save(Const.SP_USER_INFO, userInfo);
    }
    YBDLogUtil.v(
        'checkUserLevel paymentResultEntity ->: money:${paymentResultEntity.payResult?.money}  userExp:${paymentResultEntity.payResult?.userExp}  pay:${paymentResultEntity.payResult?.pay}');
    YBDLogUtil.v('checkUserLevel userInfo userExp:${userInfo.experience}');
    if ((levelInfoEntity != null)) {
      if (userInfo == null) {
        return;
      }

      /// 2022-6-20 11:26 修改
      /// 原有bug: 升级判断有误, 之前获取大于本地level下一级 experience ,如果送个大礼物导致连升几级 会导致判断不准确
      /// 本来想改成升级后服务端推送通知，但是由于服务端有缓存数据，经夏龙和志鹏拍板还是放在本地计算等级来显示升级弹窗提醒，并承担更新不及时的问题(eg:经验值更新本地更新不及时)
      /// 优化后：获取经验值大于sendgift后返回的经验值，获取对应的level , level-1 > userInfo.level 提示升级弹窗
      /// type = 1用户，2主播
      for (int i = 0; i < (levelInfoEntity?.record?.length ?? 0); i++) {
        ///如果服务端 [record] 是乱序/倒叙的 下面不起作用 ,得先排序，排序工作放在服务端
        ///观众，通过sendgift返回的经验值,查询到对应等级大的那一级
        logger.v(
            'level experience l: ${levelInfoEntity?.record![i]!.level}, exp:${levelInfoEntity?.record![i]!.experience}, currentExp:${userInfo.experience}');
        if (levelInfoEntity?.record![i]!.experience >= userInfo.experience && levelInfoEntity?.record![i]!.type == 1) {
          ///正好升一级
          if (userInfo.level! < (levelInfoEntity?.record?[i]?.level ?? 0) - 1 ||
              (levelInfoEntity?.record![i]!.experience == userInfo.experience &&
                  userInfo.level == (levelInfoEntity?.record?[i]?.level ?? 0) - 1)) {
            int l = levelInfoEntity?.record![i]!.experience == userInfo.experience ? 0 : 1;
            YBDDialogUtil.showRoomLevelUpDialog(context, (levelInfoEntity?.record?[i]?.level ?? 0) - l, barrierDismissible: false);
            YBDUserUtil.updateUserLevel((levelInfoEntity?.record?[i]?.level ?? 0) - l);
          }
          break;
        } else if (i == (levelInfoEntity?.record?.length ?? 0) - 1 && (levelInfoEntity?.record?[i]?.level ?? 0) > (userInfo.level ?? 0)) {
          YBDDialogUtil.showRoomLevelUpDialog(context, (levelInfoEntity?.record?[i]?.level ?? 0) - 1, barrierDismissible: false);
          YBDUserUtil.updateUserLevel((levelInfoEntity?.record?[i]?.level ?? 0) - 1);
        }
        // if (levelInfoEntity?.record[i].level == userInfo.level + 1 && levelInfoEntity?.record[i].type == 1) {
        //   YBDLogUtil.v(
        //       'checkUserLevel userInfo.experience 002:${userInfo?.experience} level experience:${levelInfoEntity?.record[i].experience}');
        //   if (userInfo.experience >= levelInfoEntity?.record[i].experience) {
        //     YBDLogUtil.v('userInfo.experience 0003:$context');
        //     YBDDialogUtil.showRoomLevelUpDialog(context, userInfo.level + 1, barrierDismissible: false);
        //     YBDUserUtil.updateUserLevel(userInfo.level + 1);
        //     return;
        //   }
        //   break;
        // }
      }
    } else {
      YBDLogUtil.v('state?.levelInfoEntity is null ${levelInfoEntity}');
    }
  }
  void checkUserLevelWGxUMoyelive(BuildContext? context, Response data)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 可替换 【/// 2022-6-20 11:26 修改】部分修改
  updateDialog(BuildContext context, List<YBDLevelInfoRecord> records, YBDUserInfo userInfo)  {
    if (records == null || records.isEmpty) return;
    /**
     * record: [[0, 50],[1, 150], [2, 500], [3, 1000], [4, 3000], [5, 10000]]
     * */
    // 非主播升级，连续升级只显示最高级
    YBDLevelInfoRecord? userUpdate;
    for (int i = 0; i < records.length; i++) {
      var element = records[i];
      if (element.type == 1) { // 用户升级经验
        if (userInfo.experience! >= element.experienceInt()!) {
          userUpdate = element;
          logger.v("updateDialog for userUpdate.exp: ${element.experienceInt()}, level:${element.level},"
              "userInfo.exp: ${userInfo.experience}, level: ${userInfo.level}");
        }else { // 未达到的等级结束
          logger.v("updateDialog for break userUpdate.exp: ${element.experienceInt()}, level:${element.level},"
              "userInfo.exp: ${userInfo.experience}, level: ${userInfo.level}");
          break;
        }
      }
    }
    if (userUpdate != null && userUpdate.level != userInfo.level) {
      logger.v("updateDialog userUpdate.exp: ${userUpdate.experienceInt()}, level:${userUpdate.level},"
          "userInfo.exp: ${userInfo.experience}, level: ${userInfo.level}");
      YBDDialogUtil.showRoomLevelUpDialog(context, userUpdate.level, barrierDismissible: false);
      YBDUserUtil.updateUserLevel(userUpdate.level);
    }
  }
}

enum SheetAction { Init, ChangeTab, ChangePage, SelectGift, ChangeMultiple, SelectReceiver }

class YBDGiftPackageState {
  int? viewingTabIndex, viewingPageIndex, selectingGiftIndex, multiple;
  YBDGiftPackageRecordGift? giftInfo;
  SheetAction sheetAction;
  YBDReceiverUser? receiverUser;
  YBDLevelInfoEntity? levelInfoEntity;

  YBDGiftPackageState(this.sheetAction,
      {this.viewingTabIndex,
      this.viewingPageIndex,
      this.selectingGiftIndex,
      this.giftInfo,
      this.multiple,
      this.receiverUser,
      this.levelInfoEntity});

  YBDGiftPackageState.clone(YBDGiftPackageState state)
      : this(state.sheetAction,
            viewingTabIndex: state.viewingTabIndex,
            viewingPageIndex: state.viewingPageIndex,
            selectingGiftIndex: state.selectingGiftIndex,
            multiple: state.multiple,
            giftInfo: state.giftInfo,
            receiverUser: state.receiverUser,
            levelInfoEntity: state.levelInfoEntity);
}
