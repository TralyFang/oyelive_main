import 'dart:async';


import 'package:flutter_audio_query/flutter_audio_query.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../service/live_ybd_service.dart';

enum PlayerAction { InitList, Play, Pause, Next, Pre, SkipTo }

class YBDMusicPlayerBloc extends Bloc<PlayerAction, YBDMusicPlayerStatus> {
  YBDMusicPlayerBloc() : super(YBDMusicPlayerStatus(songList: [], isPlaying: false, playingIndx: -1));
  YBDMusicPlayerStatus _state = YBDMusicPlayerStatus(songList: [], isPlaying: false, playingIndx: -1);
  @override
  Stream<YBDMusicPlayerStatus> mapEventToState(PlayerAction event) async* {
    switch (event) {
      case PlayerAction.InitList:
        break;
      case PlayerAction.Play:
        if (_state.playingIndx == -1) {
          _state.playingIndx = 0;
          YBDLiveService.instance.startAudioMixing(_state.songList![state.playingIndx].filePath!, false, false, 1);
        } else {
          YBDLiveService.instance.resumeAudioMixing();
        }
        _state.isPlaying = true;
        break;
      case PlayerAction.Pause:
        _state.isPlaying = false;
        YBDLiveService.instance.pauseAudioMixing();

        break;
      case PlayerAction.Next:
        if (_state.songList!.length - 1 > _state.playingIndx) {
          _state.playingIndx++;
        } else {
          _state.playingIndx = 0;
        }
        YBDLiveService.instance.startAudioMixing(_state.songList![state.playingIndx].filePath!, false, false, 1);
        _state.isPlaying = true;
        break;
      case PlayerAction.Pre:
        // TODO: Handle this case.
        if (_state.playingIndx> 0) {
          _state.playingIndx--;
        } else {
          _state.playingIndx = _state.songList!.length - 1;
        }
        YBDLiveService.instance.startAudioMixing(_state.songList![state.playingIndx].filePath!, false, false, 1);
        _state.isPlaying = true;
        break;
      case PlayerAction.SkipTo:
        // TODO: Handle this case.
        YBDLiveService.instance.startAudioMixing(_state.songList![state.playingIndx].filePath!, false, false, 1);
        _state.isPlaying = true;
        break;
    }
    if (_state.playingIndx != -1) _state.selectedSong = _state.songList![_state.playingIndx];
    yield YBDMusicPlayerStatus.clone(_state);
  }
  void mapEventToStateMXkWboyelive(PlayerAction event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  addSongList(List<SongInfo> songList) {
    _state.songList = songList;
    add(PlayerAction.InitList);
  }

  playIndex(int index) {
    _state.playingIndx = index;
    add(PlayerAction.SkipTo);
  }
}

class YBDMusicPlayerStatus {
  /// 数据源
  List<SongInfo>? songList = [];

  /// 被选中的歌曲索引
  SongInfo? selectedSong;

  int playingIndx = -1;

  bool? isPlaying = false;

  YBDMusicPlayerStatus({this.songList, this.selectedSong, this.playingIndx = -1, this.isPlaying});

  YBDMusicPlayerStatus.clone(YBDMusicPlayerStatus state)
      : this(
            songList: state.songList,
            selectedSong: state.selectedSong,
            playingIndx: state.playingIndx,
            isPlaying: state.isPlaying);
}
