import 'dart:async';

/// 处理操作按钮的回调
typedef OperationCallback = Function(OperationType);

/// mini profile item 的操作类型
enum OperationType {
  SayHi,
  Chat,
  Block,
  Guardian,
  CancelGuardian,
  InviteMic,
  UnMuteMic,
  MuteMic,
  Mute,
  Boot,
  UnMuteEnemyMic,
  MuteEnemyMic,
}

/// mini profile item 的数据模型
class YBDOperationItem {
  /// 操作图标
  String img;

  /// 操作名称
  String name;

  /// 操作图标(置灰）
  String? imgDisable;

  /// 操作类型
  OperationType type;

  bool enable;

  YBDOperationItem(this.img, this.name, this.type, {this.enable: true, this.imgDisable});
}
