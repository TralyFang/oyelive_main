import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/agency/agency_ybd_api_helper.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import 'mini_ybd_operation_def.dart';

enum MiniProfileEvent {
  // 请求初始数据
  Request,
  // 刷新好友，关注，粉丝数据
  UpdateUserInfo,
  // 刷新是否为我的好友
  UpdateFriend,
  // 刷新是否为签约主播
  UpdateOfficialTalent,
  // 其他人的 profile 要显示操作按钮和送礼按钮
  UpdateOtherUser,
}

/// mini profile 业务逻辑管理器
class YBDMiniProfileBloc extends Bloc<MiniProfileEvent, YBDMiniProfileBlocState> {
  BuildContext? context;

  /// 用户 id
  int? userId;

  /// 主播 id
  int? roomId;

  /// 用户信息
  YBDUserInfo? _userInfo;

  /// 是否为签约主播
  bool _isOfficialTalent = false;

  /// 是否为好友
  bool? _isMyFriend;

  /// 是否为其他人的 profile
  bool _isOtherUser = false;

  /// 操作列表
  List<YBDOperationItem> _operationList = [];

  YBDMiniProfileBloc(this.context, this.userId, this.roomId) : super(YBDMiniProfileBlocState());

  @override
  Stream<YBDMiniProfileBlocState> mapEventToState(MiniProfileEvent event) async* {
    switch (event) {
      case MiniProfileEvent.Request:
        // 检查是否为其他人的 profile
        _checkIsOtherUserProfile();

        // 请求页面初始数据
        _requestUserInfo();
        _checkIfOfficialTalent();
        _requestIsFriend();
        break;
      case MiniProfileEvent.UpdateUserInfo:
      case MiniProfileEvent.UpdateFriend:
      case MiniProfileEvent.UpdateOtherUser:
      case MiniProfileEvent.UpdateOfficialTalent:
        yield (YBDMiniProfileBlocState(
          userInfo: _userInfo,
          isOfficialTalent: _isOfficialTalent,
          isMyFriend: _isMyFriend,
          isOtherUser: _isOtherUser,
        ));
        break;
    }
  }
  void mapEventToStatek2MJ6oyelive(MiniProfileEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求用户信息
  _requestUserInfo() async {
    YBDUserInfo? result = await ApiHelper.queryUserInfo(context, userId);

    if (result != null) {
      _userInfo = result;
      add(MiniProfileEvent.UpdateUserInfo);
    } else {
      logger.v('requested user info is null');
    }
  }

  /// 查询是否签约主播
  _checkIfOfficialTalent() async {
    // final userId = await YBDUserUtil.userIdInt();
    _isOfficialTalent = await YBDAgencyApiHelper.isOfficialTalent(context, userId);
    add(MiniProfileEvent.UpdateOfficialTalent);
  }

  /// 判断是为其他人的 profile
  _checkIsOtherUserProfile() async {
    YBDUserInfo? userInfo = await YBDUserUtil.userInfo();

    if (null != userInfo?.id && userInfo!.id == userId) {
      _isOtherUser = false;
    } else {
      _isOtherUser = true;
    }

    add(MiniProfileEvent.UpdateOtherUser);
  }

  /// 查询用户是否为好友关系
  _requestIsFriend() async {
    YBDUserInfo? userInfo = await YBDUserUtil.userInfo();
    _isMyFriend = await ApiHelper.queryFriendFromId(context, '${userInfo?.id}', '$userId');
    add(MiniProfileEvent.UpdateFriend);
  }
}

class YBDMiniProfileBlocState {
  /// 用户信息
  YBDUserInfo? userInfo;

  /// 是否为签约主播
  bool? isOfficialTalent;

  /// 是否为好友
  bool? isMyFriend;

  /// 是否为其他人的 profile
  bool isOtherUser = false;

  YBDMiniProfileBlocState({
    this.userInfo,
    this.isOfficialTalent,
    this.isMyFriend,
    this.isOtherUser = false,
  });
}
