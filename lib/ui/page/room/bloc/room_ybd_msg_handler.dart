import 'dart:async';


import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/room_socket/message/common/PkPublish.dart';
import 'package:oyelive_main/common/room_socket/message/common/ludo_ybd_status_message_entity.dart';
import 'package:oyelive_main/common/room_socket/message/common/pk_ybd_invite_pub.dart';
import 'package:oyelive_main/common/room_socket/message/common/sub_ybd_ack_message.dart';
import 'package:oyelive_main/common/room_socket/message/common/target_ybd_pk_message.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/ui/page/room/bloc/pk_ybd_state_bloc.dart';
import 'package:oyelive_main/ui/page/room/pk/pk_ybd_board.dart';
import 'package:oyelive_main/ui/page/room/pk/pk_ybd_result_dialog.dart';
import 'package:oyelive_main/ui/page/room/pk/pk_ybd_rtm_helper.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/service/pk_ybd_prohibit_service.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/teen_ybd_patti_info.dart';
import 'package:oyelive_main/ui/page/room/widget/dialog_ybd_pk_finvite.dart';

import '../../../../common/room_socket/message/base/message.dart';
import '../../../../common/room_socket/message/common/Global.dart';
import '../../../../common/room_socket/message/common/booted_ybd_message.dart';
import '../../../../common/room_socket/message/common/enter.dart';
import '../../../../common/room_socket/message/common/gift.dart';
import '../../../../common/room_socket/message/common/internal.dart';
import '../../../../common/room_socket/message/common/room_ybd_manager_display_message.dart';
import '../../../../common/room_socket/message/common/super_ybd_fans_message.dart';
import '../../../../common/room_socket/message/common/unsubscribe.dart';
import '../../../../common/room_socket/message/common/update_ybd_room_bg_publish.dart';
import '../../../../common/room_socket/message/common/user_ybd_info_publish.dart';
import '../../../../common/room_socket/message/common/user_ybd_publish.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../define/room_ybd_define.dart';
import '../notice/notice_ybd_type.dart';
import '../util/room_ybd_util.dart';

/// 房间消息处理器
class YBDRoomMsgHandler {
  int? roomId;
  final BuildContext? context;
  final OnCurrentRanking? onCurrentRanking;
  final OnRoomScore? onRoomScore;
  final Function? onChangeBoardName;
  final Function? onInsufficientBalance;
  final OnTotalRanking? onTotalRanking;
  final OnViewerList? onViewerList;
  final OnViewerTotalCount? onViewerTotalCount;
  final OnViewerOnlineCount? onViewerOnlineCount;
  // final OnFans onFans;
  final OnCurrentRanking? onDailyFans;
  final OnCurrentRanking? onWeeklyFans;
  final OnCurrentRanking? onMonthlyFans;
  final OnRoomManagers? onRoomManagers;
  final OnRoomTheme? onRoomTheme;
  final OnNoticeMessage? onNoticeMessage;
  final OnRoomExperience? onRoomExperience;
  final OnUnSubscribe? onUnSubscribe;
  final OnGlobalGift? onGlobalGift;
  final Function? onExitRoom;
  final OnTeenInfo? onTeenPattiInfo;
  final YBDPkStateBloc pkStateBloc;
  final Function? onPKInvite;
  final Function? onPKMatch;

  //主播创建房间 下发消息给留在房间里面的用户，更新麦位类型
  final OnTalentCreateRoom? onTalentCreateRoom;

  /// 记录sub消息和打开ludo消息的次数
  /// 修复进房间后多次自动打开ludo的bug
  var subCount = 0;
  var ludoCount = 0;

  YBDRoomMsgHandler({
    required this.roomId,
    required this.context,
    this.onCurrentRanking,
    this.onRoomScore,
    this.onTotalRanking,
    this.onViewerList,
    this.onViewerTotalCount,
    this.onRoomManagers,
    this.onRoomTheme,
    this.onNoticeMessage,
    this.onViewerOnlineCount,
    this.onRoomExperience,
    this.onUnSubscribe,
    this.onGlobalGift,
    this.onChangeBoardName,
    this.onInsufficientBalance,
    required this.pkStateBloc,
    this.onPKInvite,
    this.onExitRoom,
    this.onTeenPattiInfo,
    this.onPKMatch,
    this.onTalentCreateRoom,
    // this.onFans,
    this.onDailyFans,
    this.onWeeklyFans,
    this.onMonthlyFans,
  });

  /// 改变房间id，切房间后处理对应房间的消息
  void changeRoomId(int? value) {
    logger.v('change room : $roomId -> $value');
    roomId = value;
  }
  void changeRoomIdV7DJ8oyelive(int? value) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  bool showPKStartAnimation = false;

  void receivedMessage(YBDMessage message) {
    logger.v('received msg: $message}');
    // logger.l('received msg: ${message.toJsonContent()}');
    if (message.roomId != roomId) {
      logger.v('message roomId : ${message.roomId} != $roomId');
      return;
    }

    if (message is YBDSuperFansMessage) {
      _updateFansRankWithSuperFansMessage(message);
    } else if (message is YBDUserPublish) {
      _updateViewerWithPublishMsg(message);
    } else if (message is YBDRoomManagerDisplayMessage) {
      if (null != onRoomManagers) {
        onRoomManagers!();
      }

      if (message.operateType == 1) {
        // TODO: 测试代码 add someone as a room manager
      } else if (message.operateType == 2) {
        // TODO: 测试代码 remove room manager
      }
    } else if (message is YBDRoomBgPublish) {
      if (null != onRoomTheme) {
        onRoomTheme!(message.content!.theme);
      }
    } else if (message is YBDEnter) {
      logger.v('received YBDEnter carId:${message.carId} carName:${message.carName} fromUser:${message.fromUser}');
    } else if (message is YBDGift) {
      logger.v('received YBDGift name:${message.name} fromUser:${message.fromUser}');
    } else if (message is YBDGlobal) {
      logger.v(
          'received YBDGlobal YBDGift name:${message.content!.name} fromUser:${message.fromUser} roomId:${message.roomId} tag:${message.roomId}');
      if (null != onGlobalGift) {
        onGlobalGift!(message);
      }
    } else if (message is YBDInternal) {
      if (message.type == YBDNoticeType.TYPE_TITLE || message.type == YBDNoticeType.TYPE_LIMIT) {
        ///封号 限播
        if (null != onNoticeMessage) {
          onNoticeMessage!(message);
        }
      } else if (message.type == YBDNoticeType.TYPE_WARN) {
        ///警告
        YBDToastUtil.toast('${message.content ?? ""}');
      } else if (message.type == YBDNoticeType.TYPE_CHANGE_BOARD_NAME) {
        if (null != onChangeBoardName) {
          onChangeBoardName!(message);
        }
      } else if (message.type == YBDNoticeType.TYPE_ERROR) {
        if (message.record == "901001") {
          if (null != onInsufficientBalance) {
            onInsufficientBalance!(message);
          }
        } else {
          YBDToastUtil.toast(message.content);
        }
      } else if (message.type == YBDNoticeType.TYPE_TEEN_PATTI_VIP) {
        YBDRoomUtil.showTeenPattiVipView(context, false);
      } else if (message.type == YBDNoticeType.TYPE_TALENT_CREATE_ROOM) {
        onTalentCreateRoom!(message);
      }
    } else if (message is YBDUserInfoPublish) {
      YBDUserInfoPublish _userInfoPublish = message;
      String jsonString11 = jsonEncode(_userInfoPublish);
      logger.v("_userInfoPublish----jsonString11: $jsonString11");

      var jsonString = jsonEncode(_userInfoPublish.content);
      Map<String, dynamic> map = jsonDecode(jsonString);

      if (map['talent'] != null) {
        logger.v('map roomexper: ${map['talent']['roomexper']}');
        // if (null != onRoomScore) {
        //   onRoomScore(map['talent']['roomexper']);
        // }

        if (null != onRoomExperience) {
          onRoomExperience!(map['talent']['roomexper'], map['talent']['roomlevel']);
        }
      }
    } else if (message is YBDBootedMessage) {
      YBDToastUtil.toast('${message.desc}');

      if (null != onExitRoom) {
        onExitRoom!();
      }
    } else if (message is YBDUnsubscribe) {
      logger.v('received YBDUnsubscribe : ${message.toJson()}');
      if (null != onUnSubscribe) {
        onUnSubscribe!();
      }
    } else if (message is YBDTeenPattiInfo) {
      if (null != onTeenPattiInfo) {
        onTeenPattiInfo!(message);
      }
    } else if (message is YBDPkPublish) {
      logger.w('received YBDPkPublish : ${message.toJson()}');
      changePkStatus(message);

      ///pk心跳包
      YBDPKRTMHelper.updateServicePkState(message);
    } else if (message is YBDPKInvitePub) {
      print('7.19---message---${message.toJson()}');
      //* 系统干预的匹配
      if (message.type == 13 && null != onPKMatch) {
        //* 开始匹配通知在麦上的用户
        onPKMatch!();
        return;
      }
      if (null != onPKInvite) {
        onPKInvite!(message.content!.source);
      }
    } else if (message is YBDTargetPkMessage) {
      logger.v('tagggget---${message.toJson()}');
      showDialog<Null>(
          context: Get.context!, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return YBDPkFinviteDialog(message);
          });
    } else if (message is YBDSubAckMessage) {
      // 收到订阅成功的消息
      subCount++;
    } else if (message is YBDLudoStatusMessageEntity) {
      ludoCount++;

      // 第一次进房间
      if (subCount == 1) {
        // 打开ludo弹窗
        _handleLudoStatusMessageEntity(message);
      } else {
        // 进房间后sub重连
        if (ludoCount > subCount) {
          // 非重连触发的ludo消息需要打开ludo弹窗
          // 打开ludo弹窗
          _handleLudoStatusMessageEntity(message);
          ludoCount--;
        }
      }
    } else {
      logger.v("else message : $message");
    }
  }

  /// 处理ludo消息
  void _handleLudoStatusMessageEntity(YBDLudoStatusMessageEntity message) {
    // 游戏状态，打开游戏，内部已经处理重复打开
    var gaming = message.content?.ludo?.gaming ?? false;
    var roomId = message.content?.ludo?.roomId;
    if (gaming && roomId != null) {
      YBDModuleCenter.instance().enterRoom(roomId: roomId, type: YBDModuleCenter.enterRoomType(6));
    }
  }
  void _handleLudoStatusMessageEntity2vgUjoyelive(YBDLudoStatusMessageEntity message) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 更新在线用户列表和在线总人数
  void _updateViewerWithPublishMsg(YBDUserPublish msg) {
    log('YBDUserPublish : ${msg.toJson()}');
    if (null != msg && null != msg.content && null != msg.content!.users) {
      // 获取主播信息
      final talentInfo = msg.content!.users!.firstWhereOrNull(
        (element) => element?.id == roomId,
        /*orElse: () => null,*/
      );

      List<YBDUserInfo?> list = [];
      list.addAll(msg.content!.users!);

      // 移除主播
      list.removeWhere((element) => element?.id == roomId);

      // 把主播排到第一位
      if (null != talentInfo) {
        list.insert(0, talentInfo);
      }

      if (null != onViewerList) {
        onViewerList!(list);
      }

      if (null != onViewerTotalCount) {
        onViewerTotalCount!(msg.content!.totalCount);
      }

      if (null != onViewerOnlineCount) {
        onViewerOnlineCount!(msg.content!.onlineCount);
      }
    } else {
      logger.v('YBDUserPublish msg no viewer list data');
    }
  }
  void _updateViewerWithPublishMsgar0Tooyelive(YBDUserPublish msg) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 更新粉丝榜数据
  void _updateFansRankWithSuperFansMessage(YBDSuperFansMessage message) {
    // if (message.content != null && onFans != null) {
    //   onFans(message.content);
    // }
    if (message.content?.dailyFans != null) {
      // if (null != onCurrentRanking) {
      //   onCurrentRanking(message.content.fans);
      // }
      if (onDailyFans != null) onDailyFans!(message.content!.dailyFans);

      if (null != onRoomScore) {
        // 计算房间分数
        List<int?> scoreList = message.content!.dailyFans?.map((e) => e?.score).toList() ?? [];
        logger.v('room msg score list: $scoreList');
        int? roomScore = 0;
        if (scoreList.isNotEmpty) {
          roomScore = scoreList.reduce((value, element) => (value ?? 0) + (element ?? 0));
        }
        logger.v('room score : $roomScore');
        onRoomScore!(roomScore);
      }
    }
    if (message.content?.weeklyFans != null) {
      if (onWeeklyFans != null) onWeeklyFans!(message.content!.weeklyFans);
    }
    if (message.content?.monthlyFans != null) {
      if (onMonthlyFans != null) onMonthlyFans!(message.content!.monthlyFans);
    }

    if (message.content?.superFans != null && message.content!.superFans!.isNotEmpty) {
      if (null != onTotalRanking) {
        onTotalRanking!(message.content!.superFans);
      }
    } else {
      logger.v('total fans rank data is empty');
    }
  }
  void _updateFansRankWithSuperFansMessagebFhztoyelive(YBDSuperFansMessage message) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  changePkStatus(YBDPkPublish data) {
    logger.v("pk=======******SERVER  PK STATE  :${data.content!.status}");
    switch (data.content!.status) {
      case SERVER_PK_SATUS_IN_PK:

        ///设定pkId,如果已经存在已结束pkID，过滤,pkInfo为null说明这个pk还没开始需要改变为开始
        if (YBDLiveService.instance.data.pkInfo == null || data.content!.pkId != YBDLiveService.instance.data.pkIdEnded) {
          YBDLiveService.instance.data.pkInfo = data.content;
          pkStateBloc.add(PkState.PKing);
        }
        break;
      case SERVER_PK_SATUS_ENDED:
      case SERVER_PK_SATUS_ERROR:
      case SERVER_PK_STATUS_SURRENDED:
        pkStateBloc.add(PkState.End);
        if (YBDLiveService.instance.data.pkInfo?.pkId != null) {
          YBDLiveService.instance.data.pkIdEnded = YBDLiveService.instance.data.pkInfo?.pkId;
        }
        YBDLiveService.instance.data.pkInfo = null;
        YBDPkProhibitService.stopChannelMediaRelay();
        Future.delayed(Duration(milliseconds: 300), () {
          pkStateBloc.add(PkState.None);
          return Future.delayed(Duration(milliseconds: 300));
        }).then((value) {
          YBDPKAShow.showOpt(
            Get.context,
            data.content!.sourceGifts,
            data.content!.targetGifts,
            data.content!.sourceTopGifts,
            pkStateBloc: pkStateBloc,
            data: data,
          );
        });
        break;
      case SERVER_PK_SATUS_NONE:
        break;
      default:
        //记录结束的pkid 用于过滤延迟消息
        if (YBDLiveService.instance.data.pkInfo?.pkId != null) {
          YBDLiveService.instance.data.pkIdEnded = YBDLiveService.instance.data.pkInfo?.pkId;
        }
        YBDLiveService.instance.data.pkInfo = null;
        pkStateBloc.add(PkState.None);
        break;
    }
  }
}
