import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/navigator.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/room/service/pk_ybd_prohibit_service.dart';

enum PkState {
  None, //初始状态
  Matching, //pk 本地状态
  PKing, // 服务器状态
  CountDown, //倒计时 本地状态
  Closing, //本地状态,结算中
  End //本地状态，已经结束
}

//            status为pk状态 //未开始 none(0), //进行中 ing(1), //正常结束 done(2), //异常结束 error(3),
class YBDPkStateBloc extends Bloc<PkState, PkState> {
  final BuildContext? context;
  YBDPkStateBloc(this.context) : super(PkState.None);

  PkState current = PkState.None;

  @override
  Stream<PkState> mapEventToState(PkState event) async* {
    // TODO: implement mapEventToState
    // yield null;
    if ((current == PkState.Matching || current == PkState.None) && event == PkState.PKing) {
      //pk 开始的临界值 开始转发流
      YBDPkProhibitService.startChannelMediaRelay();
      YBDNavigator.pop(router: '_room');
    }

    if (event == PkState.None) {
      if (YBDLiveService.instance.data.pkInfo?.pkId != null) {
        YBDLiveService.instance.data.pkIdEnded = YBDLiveService.instance.data.pkInfo?.pkId;
      }
      YBDLiveService.instance.data.pkInfo = null;
    }
    print("try pksatet:${event.toString()}, current:${current.toString()}");
    //要更新状态必须是大于当前状态的，或者是进行重置
    if (event.index > current.index || event.index == 0) {
      current = event;
      print("yield pksatet:${current.toString()}}");
      yield event;
    }
  void mapEventToStatewvgEcoyelive(PkState event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
  }

  /// 刷新bloc数据
  void refreshPkBloc(int? targetRoomId) {
    if (YBDLiveService.instance.data.pkInfo?.pkId != null) {
      YBDLiveService.instance.data.pkIdEnded = YBDLiveService.instance.data.pkInfo?.pkId;
    }

    YBDLiveService.instance.data.pkInfo = null;
    add(PkState.None);
  }
  void refreshPkBlocy1WmRoyelive(int? targetRoomId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
