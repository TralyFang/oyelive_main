import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/room_socket/entity/fans_ybd_info.dart';
import '../../widget/scaffold/nav_ybd_back_button.dart';
import '../follow/widget/place_ybd_holder_view.dart';
import 'widget/room_ybd_fans_item.dart';
import 'package:redux/redux.dart';

class YBDRoomFansPage extends StatefulWidget {
  final int? roomId;
  final List<YBDFansInfo>? currentRanking;
  final List<YBDFansInfo>? totalRanking;

  YBDRoomFansPage(this.roomId, this.currentRanking, this.totalRanking);

  @override
  State<StatefulWidget> createState() => _YBDRoomFansPageState();
}

class _YBDRoomFansPageState extends BaseState<YBDRoomFansPage> with SingleTickerProviderStateMixin {
  TabController? _tabController;
  bool isStreamerViewing = true;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 2, vsync: this);
  }
  void initState82Q0aoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    // final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context);
    // isStreamerViewing = widget.roomId == store.state.bean.id;
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
        width: double.infinity,
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: [
            Container(
              width: ScreenUtil().screenWidth,
              height: ScreenUtil().setWidth(116),
              child: Stack(
                children: <Widget>[
                  Positioned(
                    // 导航栏返回按钮
                    top: ScreenUtil().setWidth(15),
                    left: 0,
                    child: YBDNavBackButton(),
                  ),
                  Center(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(80)),
                      child: TabBar(
                        indicator: UnderlineTabIndicator(
                          borderSide: BorderSide(
                            width: ScreenUtil().setWidth(4),
                            color: Color(0xff47CCCB),
                          ),
                          insets: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(110)),
                        ),
                        labelColor: Colors.white,
                        unselectedLabelColor: Colors.white,
                        controller: _tabController,
                        labelStyle: TextStyle(fontWeight: FontWeight.w400, height: 1, fontSize: ScreenUtil().setSp(28)),
                        unselectedLabelStyle:
                            TextStyle(fontWeight: FontWeight.w400, height: 1, fontSize: ScreenUtil().setSp(24)),
                        tabs: [
                          Tab(text: 'Current Ranking'),
                          Tab(text: 'Total Ranking'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  fansContent(widget.currentRanking),
                  fansContent(widget.totalRanking),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildhwYdAoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget fansContent(List<YBDFansInfo>? fans) {
    if (fans == null || fans.isEmpty) {
      return YBDPlaceHolderView(
        img: 'assets/images/empty/empty_my_profile.webp',
        text: translate('no_data'),
      );
    } else {
      return Container(
        padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(16)),
        child: ListView.builder(
          padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
          itemBuilder: (_, index) => Material(
            color: Colors.transparent,
            child: fans.length == 1
                ? YBDRoomFansItem(index, fans[index], ScreenUtil().setWidth(1170), isStreamerViewing)
                : YBDRoomFansItem(index, fans[index], ScreenUtil().setWidth(484), isStreamerViewing),
          ),
          // separatorBuilder: (_, index) => Container(
          //   margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
          //   height: ScreenUtil().setWidth(1),
          //   color: Const.COLOR_BORDER,
          // ),
          itemCount: fans.length,
        ),
      );
    }
  }
  void fansContentUUYz2oyelive(List<YBDFansInfo>? fans) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
