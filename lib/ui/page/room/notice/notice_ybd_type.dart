import 'dart:async';


class YBDNoticeType {
  //房主创建房间 下发消息
  static int TYPE_TALENT_CREATE_ROOM = 22;
//封号  限制  警告
  static int TYPE_TALENT_OFFLINE = 8;
  static int TYPE_ERROR = 12;

  static int TYPE_TITLE = 16;
  static int TYPE_LIMIT = 17;
  static int TYPE_WARN = 18;
  static int TYPE_START_LIVE_ERROR = 19;
//
  static int TYPE_CHANGE_BOARD_NAME = 20;

  static int TYPE_TEEN_PATTI_VIP = 21;
}
