import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/entity/query_ybd_room_blacklist_entity.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

class YBDRoomBlacklistItem extends StatefulWidget {
  final YBDRoomBlacklist blacklist;
  final Function callback;

  YBDRoomBlacklistItem(this.blacklist, this.callback);

  @override
  _YBDRoomBlacklistItemState createState() => new _YBDRoomBlacklistItemState();
}

class _YBDRoomBlacklistItemState extends BaseState<YBDRoomBlacklistItem> {
  @override
  Widget myBuild(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
      margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
      child: Row(
        children: [
          YBDRoundAvatar(
            // 头像
            widget.blacklist.user?.headimg,
            // 默认为 1： 女
            sex: widget.blacklist.user?.sex ?? 1,
            lableSrc:
                (widget.blacklist.user?.vip ?? 0) == 0 ? "" : "assets/images/vip_lv${widget.blacklist.user?.vip}.png",
            userId: widget.blacklist.user?.id,
            scene: "B",
            labelWitdh: 22,
            labelPadding: EdgeInsets.only(right: ScreenUtil().setWidth(5), bottom: ScreenUtil().setWidth(5)),
            avatarWidth: 96,
          ),
          SizedBox(width: ScreenUtil().setWidth(10)),
          Container(
            width: ScreenUtil().setWidth(400),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center, //就是与当前控件方向一致的轴
              crossAxisAlignment: CrossAxisAlignment.start, //就是与当前控件方向垂直的轴
              children: [
                Container(
                  child: Text(widget.blacklist.user?.nickname ?? '',
                      style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis, //内容超出控件  省略号
                      softWrap: true),
                ),
                SizedBox(height: ScreenUtil().setWidth(8)),
                Row(
                  children: [
                    Image.asset(
                        widget.blacklist.user?.sex == 2 ? "assets/images/tag_male.webp" : "assets/images/tag_female.webp",
                        fit: BoxFit.fill,
                        width: ScreenUtil().setWidth(60),
                        height: ScreenUtil().setWidth(24)),
                    // 用户级别标签
                    widget.blacklist.user?.level != null ? YBDLevelTag(widget.blacklist.user?.level) : Container(),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(10),
          ),
          GestureDetector(
              onTap: () async {
                logger.v("Unblock userId: ${widget.blacklist.user?.id}, record id: ${widget.blacklist.id}");

                showLockDialog();
                bool success = await ApiHelper.deleteRoomBlacklist(context, widget.blacklist.id);
                dismissLockDialog();
                if (success) {
                  YBDToastUtil.toast('Unblock successful');
                  widget.callback();
                } else {
                  YBDToastUtil.toast('Unblock failed');
                }
                // return success;
              },
              child: Container(
                height: ScreenUtil().setWidth(56),
                width: ScreenUtil().setWidth(125),
                decoration: BoxDecoration(
                    color: Color(0xffffff).withOpacity(0.29),
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30)))),
                alignment: Alignment.center,
                child:
                    Text(translate('Unblock'), style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white)),
              )),
        ],
      ),
    );
  }
  void myBuildoPfFwoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
