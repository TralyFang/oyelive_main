
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cupertino_date_picker_fork/flutter_cupertino_date_picker_fork.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/util/input_ybd_restrict.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/constant/const.dart';
import '../../../common/http/http_ybd_util.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/remote_ybd_config_service.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../module/api.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/user/entity/login_ybd_resp_entity.dart';
import '../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../module/user/util/user_ybd_util.dart';
import '../../../redux/app_ybd_state.dart';
import '../../../redux/user_ybd_redux.dart';
import '../../widget/intl_input/countries.dart';
import '../../widget/loading_ybd_circle.dart';
import '../../widget/select_ybd_country.dart';
import 'handler/login_ybd_handler.dart';

class YBDSetProfilePage extends StatefulWidget {
  String tokenId;
  String token;

  /// 社交账号登录编辑个人信息；手机号和验证码登录不编辑信息
  bool? isEdit;

  YBDSetProfilePage(this.token, this.tokenId, {this.isEdit});

  @override
  YBDSetProfilePageState createState() => YBDSetProfilePageState();
}

class YBDSetProfilePageState extends BaseState<YBDSetProfilePage> {
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _nickNameController = TextEditingController();
  final FocusNode _nicknameFocusNode = FocusNode();
  final TextEditingController _genderController = TextEditingController();
  final TextEditingController _birthdayController = TextEditingController();
  final TextEditingController _countryController = TextEditingController();

  bool isObsecure = true;
  bool isPwdLegally = true;
  String? countryCode = 'PK';
  String? countryFlag = "🇵🇰";
  DateTime? selectedTime;
  var locationKey = GlobalKey();
  OverlayEntry? popupOverlayEntry;
  var inputDecoration = BoxDecoration(
    border: Border(bottom: BorderSide(width: ScreenUtil().setWidth(1), color: Colors.white.withOpacity(0.2))),
  );

  @override
  void initState() {
    super.initState();
    logger.v('token : ${widget.token}, token id : ${widget.tokenId}');

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE_SET_PROFILE));

    YBDSPUtil.getUserInfo().then((userInfo) async {
      _nickNameController.text = userInfo?.nickname ?? '';
      _genderController.text = userInfo?.sex == 2
          ? translate('male')
          : userInfo?.sex == 1
              ? translate('female')
              : ''; //0.unknown, 1.female, 2.male
      _birthdayController.text = userInfo?.birthday != null
          ? DateFormat('yyyy-MM-dd').format(DateTime.fromMillisecondsSinceEpoch(userInfo!.birthday!))
          : '';
      _countryController.text = 'Pakistan';
      if ((userInfo?.country ?? '').isNotEmpty) {
        countryCode = userInfo!.country;
        _countryController.text = YBDCommonUtil.getCountryByCode(userInfo.country)!;

        // YBDCommonUtil.getCountryByCode(userInfo.country).then((value) {
        //   setState(() {
        //     _countryController.text = value;
        //   });
        // });
      } else {
        // 已登录用户，country为空，设置默认值
        countryCode = await YBDCommonUtil.getDefaultCountryCode(context);

        _countryController.text = YBDCommonUtil.getCountryByCode(countryCode)!;
      }
      try {
        List<Map<String, String>> result = YBDCountries.list().where((element) => countryCode == element['code']).toList();

        if (null == result || result.isEmpty) {
          result = [YBDCountries.emptyCounty];
        }

        countryFlag = result[0]['flag'];
      } catch (e) {
        print(e);
      }
      if (mounted) setState(() {});
    });

    _nicknameFocusNode.addListener(() async {
      if (widget.isEdit! && !_nicknameFocusNode.hasFocus && _nickNameController.text.trim().isNotEmpty) {
        YBDUserInfo? _userInfo = await YBDSPUtil.getUserInfo();
        if (_nickNameController.text.trim() != _userInfo?.nickname) {
          // 修改昵称
          Map<String, dynamic> params = {'nickname': _nickNameController.text.trim()};
          ApiHelper.editUserInfo(params).then((bool value) async {
            if (value) {
              _userInfo = await YBDSPUtil.getUserInfo();
              _userInfo!.nickname = _nickNameController.text.trim();
              updateUserInfo(_userInfo);
            }
          });
        }
      }
    });
  }
  void initStatevsyZPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  updateBirthday(String _birthday) async {
    YBDUserInfo? _userInfo = await YBDSPUtil.getUserInfo();
    if (_birthday != DateFormat('yyyy-MM-dd').format(DateTime.fromMillisecondsSinceEpoch(_userInfo?.birthday ?? 0))) {
      // 修改生日
      Map<String, dynamic> params = {'birthday': _birthday};
      ApiHelper.editUserInfo(params).then((bool value) async {
        if (value) {
          _userInfo = await YBDSPUtil.getUserInfo();
          _userInfo?.birthday = DateTime.parse(_birthday).millisecondsSinceEpoch;
          updateUserInfo(_userInfo);
        }
      });
    }
  }

  /// 弹出选择国家对话框
  showCountryDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return YBDSelectCountry((Map<String, String> selected) {
            logger.v('selected country : $selected');
//        saveToNetwork(EditType.Location, selected['code']);
            countryFlag = selected['flag'];
            countryCode = selected['code'];
            _countryController.text = selected['name']!;
            YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
              YBDEventName.CLICK_EVENT,
              location: YBDEventName.OPEN_PAGE_SET_PROFILE,
              itemName: YBDItemName.NEW_USER_SET_COUNTRY_CODE,
            ));
            setState(() {});
          });
        });
  }

  updateGender(int _gender) async {
    YBDUserInfo? _userInfo = await YBDSPUtil.getUserInfo();
    if (_gender != _userInfo?.sex) {
      // 修改性别
      Map<String, dynamic> params = {'sex': _gender};
      ApiHelper.editUserInfo(params).then((bool value) async {
        if (value) {
          _userInfo = await YBDSPUtil.getUserInfo();
          _userInfo!.sex = _gender;
          updateUserInfo(_userInfo);
        }
      });
    }
  }

  /// 更新SP、redux 用户信息
  updateUserInfo(YBDUserInfo? _info) {
    // 更新 SP
    YBDSPUtil.save(Const.SP_USER_INFO, _info);

    // 更新 redux
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    store.dispatch(YBDUpdateUserAction(_info));
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _nickNameController.dispose();
    _genderController.dispose();
    _birthdayController.dispose();
    _countryController.dispose();
    super.dispose();
  }
  void disposeZKlmKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color(0xff6C6CDF), Color(0xff6C6CDF), Color(0xff3D0B8D)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter),
        ),
        child: Column(
          children: [
            SizedBox(
              height: ScreenUtil().setWidth(100),
            ),
            Text(
              translate('title_welcome'),
              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28)),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(100),
            ),
            setInputLine(translate('enter_nickname'),
                controller: _nickNameController,
                focusNode: _nicknameFocusNode,
                imagePath: "assets/images/set_nickname.png"),
            SizedBox(
              height: ScreenUtil().setWidth(30),
            ),
            setInputLine(translate('gender'),
                controller: _genderController,
                isReadOnly: true,
                key: locationKey,
                imagePath: "assets/images/set_gender.png",
                rightWidget: Container(
                  width: ScreenUtil().setWidth(100),
                  child: TextButton(
                      style: TextButton.styleFrom(padding: EdgeInsets.zero),
                      // padding: EdgeInsets.zero,
                      onPressed: () {
                        showPopupView();
                      },
                      child: Icon(
                        Icons.arrow_drop_down,
                        color: Colors.white,
                      )),
                )),
            SizedBox(
              height: ScreenUtil().setWidth(30),
            ),
            if (!widget.isEdit!) ...[
              setInputLine(
                translate('password'),
                controller: _passwordController,
                isObsecure: isObsecure,
                imagePath: 'assets/images/set_pwd.png',
                rightWidget: Container(
                  width: ScreenUtil().setWidth(100),
                  child: TextButton(
                    style: TextButton.styleFrom(padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(34))),
                    // padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(34)),
                    onPressed: () {
                      setState(() {
                        isObsecure = !isObsecure;
                      });
                    },
                    child: Image.asset(
                      isObsecure ? "assets/images/show_pass.png" : "assets/images/hide_pass.png",
                      width: ScreenUtil().setWidth(38),
                    ),
                  ),
                ),
              ),
              isPwdLegally
                  ? SizedBox(
                      height: ScreenUtil().setWidth(30),
                    )
                  : Container(
                      margin: EdgeInsets.only(top: ScreenUtil().setWidth(10), bottom: ScreenUtil().setWidth(14)),
                      width: ScreenUtil().setWidth(556),
                      child: Text(
                        translate('pwd_format_warn'),
                        style: TextStyle(fontSize: ScreenUtil().setSp(20), color: Color(0xffFF8B8B)),
                      ),
                    ),
            ],
            setInputLine(
              translate('birthday'),
              isReadOnly: true,
              controller: _birthdayController,
              imagePath: "assets/images/set_bd.png",
              rightWidget: Container(
                width: ScreenUtil().setWidth(100),
                child: TextButton(
                  style: TextButton.styleFrom(padding: EdgeInsets.zero),
                  // padding: EdgeInsets.zero,
                  onPressed: () {
                    DatePicker.showDatePicker(
                      context,
                      initialDateTime: selectedTime,
                      maxDateTime: DateTime.now(),
                      onConfirm: (DateTime s, k) {
                        selectedTime = s;
                        _birthdayController.text = DateFormat('yyyy-MM-dd').format(s);
                        if (widget.isEdit!) updateBirthday(_birthdayController.text);
                      },
                      pickerTheme: DateTimePickerTheme(
                        confirmTextStyle: TextStyle(
                          color: Colors.blueAccent,
                          fontSize: ScreenUtil().setSp(32),
                        ),
                      ),
                    );
                  },
                  child: Icon(
                    Icons.calendar_today,
                    color: Colors.white,
                    size: ScreenUtil().setWidth(30),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(30),
            ),
            setInputLine(translate('select_country'),
                controller: _countryController,
                isReadOnly: true,
                countryFlag: countryFlag,
                rightWidget: Container(
                  width: ScreenUtil().setWidth(100),
                  child: TextButton(
                      style: TextButton.styleFrom(padding: EdgeInsets.zero),
                      // padding: EdgeInsets.zero,
                      onPressed: () {
                        showCountryDialog();
                      },
                      child: Icon(
                        Icons.arrow_drop_down,
                        color: Colors.white,
                      )),
                )),
            SizedBox(
              height: ScreenUtil().setWidth(100),
            ),
            GestureDetector(
              onTap: setProfileNow,
              child: Container(
                width: ScreenUtil().setWidth(560),
                height: ScreenUtil().setWidth(86),
                child: Text(
                  translate("next"),
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: ScreenUtil().setSp(34), color: Color(0xff5E50C7)),
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(50))),
//                  image: DecorationImage(
//                    image: AssetImage(
//                      "assets/images/btn_background.png",
//                    ),
//                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildJwQ3Aoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点下一步按钮的响应事件
  setProfileNow() {
    if (widget.isEdit!) {
      // 跳转推荐页面
      YBDNavigatorHelper.navigateTo(
        context,
        YBDNavigatorHelper.recommended,
        forceFluro: true,
        clearStack: true,
      );
      return;
    }

    if (_passwordController.text.length == 0) {
      YBDToastUtil.toast(translate("enter_password"));
      return;
    }
    isPwdLegally = YBDCommonUtil.checkPwdLegal(_passwordController.text);
    setState(() {});
    if (!isPwdLegally) {
      return;
    }
    if (_nickNameController.text.length == 0) {
      YBDToastUtil.toast(translate("enter_nickname"));
      return;
    }
    if (_genderController.text.length == 0) {
      YBDToastUtil.toast(translate("select_gender"));
      return;
    }
    if (_birthdayController.text.length == 0) {
      YBDToastUtil.toast(translate("select_birthday"));
      return;
    }
    if (_countryController.text.length == 0) {
      YBDToastUtil.toast(translate("select_country"));
      return;
    }

    if (!widget.isEdit!) {
      // 保存用户信息并注册
      requestProcessToken();
    }
  }

  /// 请求修改用户地理位置的接口
  // requestModifyLocation(String codeStr) {
  //   YBDHttpUtil.getInstance()
  //       .doGet(context, YBDApi.ADD_LOCATION, params: {"selectCountry": codeStr, "country": "", "city": ""});
  //
  //   // 修改后保存到本地
  //   YBDSPUtil.save(Const.SP_SELECT_COUNTRY_CODE, codeStr);
  // }

  /// 请求手机号注册新用户时编辑信息的接口
  requestProcessToken() async {
    // 网络请求时的加载框
    showLockDialog();
    String platformImei;
    try {
      platformImei = ''; //await ImeiPlugin.getImei(shouldShowRequestPermissionRationale: true);
    } on PlatformException {
      platformImei = 'Failed to get platform version.';
    }

    YBDLoginResp? data = await YBDHttpUtil.getInstance().doPost<YBDLoginResp>(
      context,
      YBDApi.PROCESS_TOKEN,
      {
        "token": widget.token,
        "tokenId": widget.tokenId,
        "imei": platformImei,
        "nickname": _nickNameController.text.trim(),
        "password": _passwordController.text.trim(),
        "gender": _genderController.text == translate('male') ? 2 : 1, //0.unknown, 1.female, 2.male
        "birthday": selectedTime!.millisecondsSinceEpoch,
        "country": countryCode
      },
    );

    // 隐藏网络请求的加载框
    dismissLockDialog();

    if (null != data && data.returnCode == Const.HTTP_SUCCESS) {
      try {
        Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
        store.dispatch(YBDUpdateUserAction(data.record!.userInfo));
        YBDSPUtil.save(Const.SP_USER_INFO, data.record!.userInfo);
      } catch (e) {
        logger.v('update user store error : $e');
      }

      /// 通过手机号+验证码登录，认定为新用户注册
//      if (data.record.newUser) {
      YBDAnalyticsUtil.logAFEvent(YBDEventName.AF_COMPLETE_REGISTRATION, {'af_registration_method': 'phone_number'});
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.COMPLETE_REGISTRATION, method: 'phone_number'));
//      }

      // 网络请求加载框
      showDialog(context: context, builder: (BuildContext context) => YBDLoadingCircle());
//      await onLoginSuccess(context, data.record.newUser, processToken: true);
      await onLoginSuccess(context, true, processToken: true, userId: data?.record?.userInfo?.id);

      // 隐藏网络请求加载框
      Navigator.pop(context);

      if (!widget.isEdit!) {
        // 保存地理位置
        ApiHelper.updateLocation(context, countryCode).then((value) {
          if (value?.returnCode == Const.HTTP_SUCCESS) {
            // 更新成功，更新本地用户信息国家
            YBDUserUtil.updateCountry(context, countryCode);
          }
        });
      }

      logger.v('begin new_user_recommended');
      // 跳转推荐页面 || 首页
      YBDRemoteConfigService? r = await YBDRemoteConfigService.getInstance();
      String? goRecommended;

      try {
        goRecommended = r?.getConfig()?.getString("new_user_recommended");
      } catch (e) {
        logger.v("after new_user_recommended goRecommended error: $e");
      }

      logger.v("after new_user_recommended : $goRecommended");

      if ((goRecommended ?? '').isEmpty || goRecommended == 'true') {
        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.recommended, forceFluro: true, clearStack: true);
      } else {
        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.index_page, forceFluro: true, clearStack: true);
      }
    } else {
      logger.v('request process token failed');
      YBDToastUtil.toast(data?.returnMsg);
    }

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.LOGIN_EVENT,
        method: 'phone_number', location: 'phone_login_page', returnExtra: "phone_number: ${data?.returnMsg ?? ''}"));
  }

  setInputLine(String hintText,
      {FocusNode? focusNode,
      TextEditingController? controller,
      bool isObsecure: false,
      bool isReadOnly: false,
      Widget? rightWidget,
      String? imagePath,
      String? countryFlag,
      key}) {
    return Container(
      decoration: inputDecoration,
      height: ScreenUtil().setWidth(80),
      width: ScreenUtil().setWidth(580),
      key: key,
//      padding: EdgeInsets.only(left: ScreenUtil().setWidth(50)),
      child: new Row(
        children: [
          if (imagePath != null)
            Image.asset(
              imagePath,
              width: ScreenUtil().setWidth(62),
              height: ScreenUtil().setWidth(62),
            ),
          if (countryFlag != null)
            SizedBox(
              width: ScreenUtil().setWidth(62),
              height: ScreenUtil().setWidth(62),
              child: Center(
                child: Text(
                  countryFlag,
                  style: TextStyle(fontSize: ScreenUtil().setSp(30)),
                ),
              ),
            ),
          SizedBox(
            width: ScreenUtil().setWidth(14),
          ),
          Expanded(
            child: TextField(
              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28), fontWeight: FontWeight.w500),
              obscureText: isObsecure,
              readOnly: isReadOnly,
              inputFormatters: PWD_RESTRICT,
              decoration: InputDecoration(
                  isDense: true,
                  hintStyle: TextStyle(
                    color: Color(0xffDCDCDC),
                  ),
                  hintText: hintText,
                  border: InputBorder.none),
              controller: controller,
              focusNode: focusNode,
              onChanged: imagePath == 'assets/images/set_pwd.png'
                  ? (x) {
                      // if (x?.contains("") ?? false) {
                      //   controller.text = x.replaceAll(" ", "");
                      // }
                      isPwdLegally = YBDCommonUtil.checkPwdLegal(x);
                      setState(() {});
                    }
                  : null,
            ),
          ),
          rightWidget ?? Container()
        ],
      ),
    );
  }

  void clear() {
    if (popupOverlayEntry != null) {
      popupOverlayEntry!.remove();
      popupOverlayEntry = null;
    }
  }
  void clear26euOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  showPopupView() {
    clear();
    RenderBox renderObject = locationKey.currentContext!.findRenderObject() as RenderBox;
    var size = renderObject.size;
    var offset = renderObject.localToGlobal(Offset.zero);
    popupOverlayEntry = OverlayEntry(
      builder: (context) {
        return Positioned(
          left: ScreenUtil().setWidth(92),
//          right: ScreenUtil().setWidth(62),
          top: offset.dy + size.height + ScreenUtil().setWidth(14),
          width: ScreenUtil().setWidth(536),
          child: Material(
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
            child: Container(
              width: ScreenUtil().setWidth(536),
              height: ScreenUtil().setWidth(140),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8)))),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(50)),
                        decoration: BoxDecoration(),
                        child: Text(
                          translate('male'),
                          style: TextStyle(
                              color: Color(0xff333333), fontSize: ScreenUtil().setSp(24), fontWeight: FontWeight.w400),
                        ),
                      ),
                      onTap: () {
                        clear();
                        _genderController.text = translate('male');
                        if (widget.isEdit!) updateGender(2);
                      },
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        decoration: BoxDecoration(),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(50)),
                        child: Text(
                          translate('female'),
                          style: TextStyle(
                              color: Color(0xff333333), fontSize: ScreenUtil().setSp(24), fontWeight: FontWeight.w400),
                        ),
                      ),
                      onTap: () {
                        clear();
                        _genderController.text = translate('female');
                        if (widget.isEdit!) updateGender(1);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
    Overlay.of(context)!.insert(popupOverlayEntry!);
  }
}
