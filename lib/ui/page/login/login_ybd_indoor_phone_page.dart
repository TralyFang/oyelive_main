import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/util/log_ybd_util.dart';
import 'widget/input_ybd_content.dart';
import 'widget/phone_ybd_number_input.dart';
import '../../widget/scaffold/nav_ybd_back_button.dart';

class YBDLoginIndoorPhonePage extends StatefulWidget {
  @override
  YBDLoginIndoorPhonePageState createState() => YBDLoginIndoorPhonePageState();
}

class YBDLoginIndoorPhonePageState extends BaseState<YBDLoginIndoorPhonePage> {
  @override
  Widget myBuild(BuildContext context) {
    TextStyle tabTextStyle = TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w500,
      height: 1.6,
      fontSize: ScreenUtil().setSp(34),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(translate("phone_number"), style: tabTextStyle),
        backgroundColor: Color(0xff6C6CDF),
        leading: YBDNavBackButton(color: Colors.white),
        elevation: 0,
        centerTitle: true,
      ),
      resizeToAvoidBottomInset: false,
      body: Container(
        padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [Color(0xff6C6CDF), Color(0xff6C6CDF), Color(0xff3D0B8D)],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        )),
        child: Column(
          children: <Widget>[
            Expanded(
              child: GestureDetector(
                onTap: () {
                  logger.v('clicked hide keyboard');
                  FocusScope.of(context).unfocus();
                },
                child: YBDInputContent(YBDPhoneNumberInput()),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuilduvhmKoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
  }
  void initStatepSaQCoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
}
