import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/ui/page/login/util/login_ybd_page_util.dart';
import 'package:oyelive_main/ui/widget/risk_ybd_warn_dialog.dart';
import 'package:redux/redux.dart';
import 'package:sqlcool/sqlcool.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/db_ybd_config.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/database_ybd_until.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../common/util/unique_ybd_id_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/inbox/inbox_ybd_api_helper.dart';
import '../../../../module/user/entity/login_ybd_resp_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../../redux/user_ybd_redux.dart';

enum LoginType {
  facebook,
  twitter,
  google,
  apple,
  phone,

// 1: fb   2:twitter   3:google 6: phone
}

enum LoginErrorType {
  progress,
  cancel,
  error,
}

/// 处理第三方登录的帮助类
class YBDLoginHandler {
  BuildContext context;

  /// 登录类型
  LoginType type;

  BaseState baseState;
  String location;

  Function(String? token)? _callback;
  bool _isLinkAuth = false; // 是绑定授权操作，提示语要区别于登录授权，默认登录授权的提示语

  /// 构造方法
  YBDLoginHandler(this.type, this.context, this.baseState, this.location) {
    _callback = null;
  }

  authTokenCallback(Function(String? token) callback) {
    _callback = callback;
    _isLinkAuth = true;
    return this;
  }

  /// 开始登录
  startLogin() {
    logger.i('start login type : $type');
  }

  // 授权错误提示 esDefalut 默认的提示文案, logMsg埋点信息
  _errorToast(LoginErrorType et,
      {String? esDefalut = '', String? logMsg = ''}) {
    String? errorMsg = esDefalut;
    if (_isLinkAuth) {
      // 绑定获取token失败的提示问题
      switch (et) {
        case LoginErrorType.progress:
          errorMsg = 'facebook_link_in_progress';
          break;
        case LoginErrorType.cancel:
          errorMsg = 'link_cancel';
          break;
        case LoginErrorType.error:
          errorMsg = 'link_failed';
          break;
      }
    } else {
      // 默认登录获取token失败的提示问题
      switch (et) {
        case LoginErrorType.progress:
          errorMsg = 'facebook_login_in_progress';
          break;
        case LoginErrorType.cancel:
          errorMsg = 'login_cancel';
          break;
        case LoginErrorType.error:
          errorMsg = 'login_failed';
          break;
      }
    }
    YBDToastUtil.toast(esDefalut!.isNotEmpty ? esDefalut : translate(errorMsg));
    if (logMsg!.isNotEmpty) {
      loginEvent(logMsg);
    } else {
      loginEvent(esDefalut.isNotEmpty ? esDefalut : translate(errorMsg));
    }
  }

  static snsLink(
    BuildContext context,
    String? token,
    String type, {
    bool showLoading = false,
  }) async {
    YBDLoginResp? loginResp = await ApiHelper.snsLink(
      context,
      token,
      type,
      showLoading: showLoading,
    );
    logger.v(
        "snsLink type: $type, token:$token, response : ${loginResp?.toJson()}");
    if (loginResp != null && loginResp.returnCode == Const.HTTP_SUCCESS) {
      logger.i("snsLink success");
      YBDToastUtil.toast(translate('success'));
      await ApiHelper.checkLogin(context, showLoading: showLoading);
    } else {
      logger.e("snsLink failed");
      YBDToastUtil.toast(loginResp?.returnMsg ?? translate('unknown_error'));
    }
  }

  static snsUnlink(BuildContext context, String type) async {
    YBDLoginResp? loginResp = await ApiHelper.snsUnlink(
      context,
      type,
    );
    logger.v("snsUnlink type: $type response : ${loginResp?.toJson()}");
    if (loginResp != null && loginResp.returnCode == Const.HTTP_SUCCESS) {
      logger.i("snsUnlink success");
      YBDToastUtil.toast(translate('success'));
      await ApiHelper.checkLogin(context);
      YBDNavigatorHelper.popPage(context);
    } else {
      //失败
      logger.e("snsUnlink failed");
      YBDToastUtil.toast(loginResp?.returnMsg ?? translate('unknown_error'));
    }
  }

  /// 请求社交账号登录接口
  Future<void> _snsLogin(String? token, String snsType,
      {String? firstName, String? lastName}) async {
    if (_callback != null) {
      _callback!(token);
      return;
    }

    baseState.showLockDialog(info: translate('login_ing'));
    TA.timeEvent(YBDTATrackEvent.generic_loading_time); // 注册时长埋点
    YBDLoginResp? loginResp = await ApiHelper.snsLogin(
      context,
      token,
      snsType,
      firstName: firstName,
      lastName: lastName,
    );
    bool isNewUser = loginResp?.record?.newUser ?? false;
    if (loginResp != null && loginResp.returnCode == Const.HTTP_SUCCESS) {
      logger.i("login sns success");
      await YBDSPUtil.save(Const.SP_USER_INFO, loginResp.record!.userInfo);
      Store<YBDAppState> store =
          YBDCommonUtil.storeFromContext(context: context)!;
      store.dispatch(YBDUpdateUserAction(loginResp.record!.userInfo));
      await onLoginSuccess(context, loginResp.record!.newUser!,
          userId: loginResp.record!.userInfo?.id);
      String loginType = type.toString().substring(10);
      if (isNewUser) YBDCommonTrack().regTrack(regType: loginType);
      YBDCommonTrack().loginTrack(firstLogin: isNewUser, loginWay: loginType);
    } else if ((loginResp?.returnCode == Const.BAN_ACCOUNT ||
            loginResp?.returnCode == Const.BAN_DEVICE) &&
        (loginResp?.returnMsg?.isNotEmpty ?? false)) {
      YBDLoginPageUtil.banAccountDialog(context, loginResp?.returnMsg ?? '');
    } else {
      //失败
      logger.e("login sns failed");
      YBDToastUtil.toast(loginResp?.returnMsg ?? translate('unknown_error'));
      // 隐藏 loading 框
      YBDNavigatorHelper.popPage(context);
    }
    // baseState.dismissLockDialog();
    loginEvent(loginResp?.returnMsg, newUser: isNewUser);
  }

  void _snsLoginWs5vQoyelive(String? token, String snsType,
      {String? firstName, String? lastName}) {
    int needCount = 0;
    print('input result:$needCount');
  }

  loginEvent(String? returnExtra, {bool? newUser}) {
    if (null != newUser && newUser) {
      YBDAnalyticsUtil.logAFEvent(YBDEventName.AF_COMPLETE_REGISTRATION,
          {'af_registration_method': type.toString()});
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.COMPLETE_REGISTRATION,
          method: type.toString()));
    }
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.LOGIN_EVENT,
        method: type.toString(),
        location: location,
        returnExtra: '$type : $returnExtra'));
  }
}

/// 用户名登录，社交账号登录，验证码注册登录 都会调用该方法
onLoginSuccess(
  BuildContext? context,
  bool newUser, {
  bool processToken = false,
  int? userId,
  bool needChangePwd: false,
  Function? onFixPwd,
}) async {
  await YBDDataBaseUtil.getInstance().setUserId();
  YBDSPUtil.save(Const.SP_MESSAGE_QUEUE, "0");

  // 保持新用户标记进SP，埋点分析，e.g. 新用户进房
  if (newUser && userId != null) {
    String? spNewUsers = await YBDSPUtil.get(Const.SP_NEW_USER);
    Map<String, dynamic> map = jsonDecode(spNewUsers ?? '{}');
    map.addAll({"$userId": DateFormat('yyyy-MM-dd').format(DateTime.now())});
    YBDSPUtil.save(Const.SP_NEW_USER, jsonEncode(map));
    YBDSPUtil.save(Const.TP_NEW_USER, '1');
  }

  //init messageDB
  YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
  YBDInboxApiHelper.imLogin(context, needErrorToast: false);
  if (userInfo != null) {
    if (messageDb != null) {
      print("login page set messagedb null");
      messageDb!.database?.close();
      messageDb = null;
    }
    messageDb = Db();

    await initDb(db: messageDb!, userId: userInfo.id.toString());
    // 设置账号 ID
    TA.login(accountId: '${userInfo.id}');
  }

  if (newUser && !processToken) {
    // 社交账号新注册用户跳转推荐页面
    logger.i('jump to HOME page NEW_USER 001');

    // 在前面ACTIVATION类型的时候  检查是否被封设备  这里不再检查
    getDeviceUniqueIDs(context, UniqueIDType.NEW_USER);
    bool? showRecommendPage =
        await YBDSPUtil.getBool(Const.SP_SHOW_RECOMMEND_PAGE);
    // 跳转推荐页面
    YBDNavigatorHelper.navigateTo(
      context,
      (showRecommendPage ?? true)
          ? YBDNavigatorHelper.recommended
          : YBDNavigatorHelper.index_page,
      forceFluro: true,
      clearStack: true,
    );
  } else {
    if (processToken) {
      // 在前面ACTIVATION类型的时候  检查是否被封设备  这里不再检查
      getDeviceUniqueIDs(context, UniqueIDType.NEW_USER);
      logger.i('jump to HOME page NEW_USER  002');
    } else {
      logger.i('jump to HOME page NORMAL_LOGIN');
      // 在前面ACTIVATION类型的时候  检查是否被封设备  这里不再检查
      getDeviceUniqueIDs(context, UniqueIDType.NORMAL_LOGIN);
    }

    if (!processToken) {
      if (needChangePwd) {
        // 目前已弃用
        showDialog<Null>(
            context: context!, //BuildContext对象
            barrierDismissible: false,
            builder: (BuildContext context) {
              return YBDRiskWarnDialog();
            });
        onFixPwd?.call();
      } else {
        YBDSPUtil.remove(Const.SP_NEED_FIX_PWD);
        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.index_page,
            clearStack: true);
      }
    }
  }
}
