import 'dart:async';
import 'dart:io';

import 'package:dart_ping/dart_ping.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_util.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/remote_ybd_config_service.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_commom_util.dart';
import 'package:oyelive_main/ui/page/login/widget/phone_login_btn.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:social_share/social_share.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/constant/const.dart';
import '../../../common/handler/back_ybd_action_handler.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/reload_ybd_sp_event.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/notification_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../common/web_socket/socket_ybd_util.dart';
import '../../../main.dart';
import '../status/local_audio/scale_ybd_animate_button.dart';
import 'util/login_ybd_page_util.dart';
import 'widget/login_ybd_page_bg.dart';
import 'widget/login_ybd_terms.dart';
import 'widget/login_ybd_voice_text.dart';
import 'widget/logo_ybd_app_name.dart';

class YBDLoginPage extends StatefulWidget {
  final String? reLogin;

  YBDLoginPage({this.reLogin});

  @override
  State<StatefulWidget> createState() => _YBDLoginPageState();
}

class _YBDLoginPageState extends BaseState<YBDLoginPage>
    with SingleTickerProviderStateMixin {
  /// 是否需要显示手机登录图标
  bool _shouldShowSMS = false;

  /// 配置项
  String? _sms;

  /// 双击退出 app 的事件处理器
  YBDBackActionHandler _backActionHandler = YBDBackActionHandler();

  Map<dynamic, dynamic>? appsForShare = {};
  bool _showNewBg = false;

  @override
  void initState() {
    super.initState();

    YBDRtcHelper.getInstance().dispose(islogout: true);

    YBDSocketUtil.getInstance()!.logOutSocket();

    YBDLiveService.instance.leaveRoomInvalid();

    // firebase 登录页面统计
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE_LOGIN));

    // 输出设备信息到日志文件
    YBDLoginPageUtil.printDeviceInfo();

    YBDNotificationUtil.unsubscribeAll(context);

    // 监听 app 切换到后台的事件
    WidgetsBinding.instance?.addObserver(this);

    // 根据配置项和IP判断是否显示手机短信登录图标
    YBDSPUtil.get(Const.SP_IP_COUNTRY_CODE).then((value) async {
      _sms = await YBDCommonUtil.getSMSLogin(context);
      logger.v('get ip country code through SP: $value');
      setState(() {
        if (_sms != null && _sms!.isNotEmpty && _sms!.contains(value ?? '')) {
          _shouldShowSMS = true;
        } else {
          _shouldShowSMS = false;
        }
      });
    });

    // 日志弹框
    YBDLogUtil.popLogViewOnShake(context);
    eventBus.on<YBDReloadSpEvent>().listen((YBDReloadSpEvent event) async {
      _sms = await YBDCommonUtil.getSMSLogin(context);
      switch (event.key) {
        case Const.SP_IP_COUNTRY_CODE:
          logger
              .v('get ip country code through eventBus: ${event.value ?? ''}');
          if ((event.value ?? '').isNotEmpty) {
            setState(() {
              if (_sms != null &&
                  _sms!.isNotEmpty &&
                  _sms!.contains(event.value!)) {
                _shouldShowSMS = true;
              } else {
                _shouldShowSMS = false;
              }
            });
          }
          break;
      }
    });

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      setConfig();
      logger.v('login_page addPostFrameCallback: ${widget.reLogin}');
      if (widget.reLogin != null && widget.reLogin!.isNotEmpty) {
        YBDDialogUtil.showReloginDialog(context);
      }
    });

    // [ping登录地址和oyetalk地址]
    // facebook---m.facebook.com||google---accounts.google.com||twitter---twitter.com||oyetalk---oyetalk.live
    List<String> pings = YBDGameCommomUtil.getGameConfig().pings.split('||');
    logger.v('22.10.27--pings:$pings');
    pings.forEach((e) {
      List<String> line = e.split('---');
      logger.v('22.10.27--line:$line');
      String platform = line.first;
      String url = line.last;
      Ping ping = Ping(url, count: 1);
      ping.stream.listen((PingData event) {
        logger.v('22.10.27--${event.summary?.received ?? 0}');
        bool pingSuccess = (event.summary?.received ?? 0) > 0;
        if (platform == 'facebook') YBDTAUtil.pingFb = pingSuccess;
        if (platform == 'google') YBDTAUtil.pingGoogle = pingSuccess;
        if (platform == 'twitter') YBDTAUtil.pingTwitter = pingSuccess;
        if (platform == 'oyetalk') YBDTAUtil.pingOyetalk = pingSuccess;
      });
    });
    // 是否安装

    SocialShare.checkInstalledAppsForShare().then((value) {
      logger.i('22.11.10---apps for share : $value');
      appsForShare = value;
    });

    YBDSPUtil.remove(Const.SP_MY_EMOJI);
  }

  void initState2KFdtoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  // 设置AB Test配置项
  Future<void> setConfig() async {
    bool showNew = (await YBDSPUtil.getBool(Const.SP_LOGIN_NEW)) ?? false;
    bool showRecommendPage =
        (await YBDSPUtil.getBool(Const.SP_SHOW_RECOMMEND_PAGE)) ?? false;
    logger.v(
        '22.11.18---SP---new_login_page2022:$showNew---showRecommendPage:$showRecommendPage');
    if ((showNew == null && showRecommendPage == null) || Const.TEST_ENV) {
      // YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.FIRST_INSTALL));
      YBDRemoteConfigService? r = await YBDRemoteConfigService.getInstance();
      bool newLogin = r?.getConfig()?.getBool('new_login_page2022') ?? false;
      bool newRecommend =
          r?.getConfig()?.getBool('show_recommend2022') ?? false;
      logger.v(
          '22.11.18---RemoteConfig---new_login_page2022:$newLogin---show_recommend2022:$newRecommend');
      YBDSPUtil.save(Const.SP_LOGIN_NEW, newLogin);
      YBDSPUtil.save(Const.SP_SHOW_RECOMMEND_PAGE, newRecommend);
      _showNewBg = newLogin;
    }
  }

  void setConfigzhNtwoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void dispose() {
    // 移除监听 app 切换到后台的事件
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  void disposePXRmPoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: _backActionHandler.onWillPop,
        child: Container(
          color: Colors.black,
          height: ScreenUtil().screenHeight,
          width: ScreenUtil().screenWidth,
          child: Stack(
            children: [
              /// 背景图
              YBDLoginPageBgWidget(showNewBg: _showNewBg),
              Column(
                children: <Widget>[
                  SizedBox(
                      height: ScreenUtil().statusBarHeight +
                          ScreenUtil().setWidth(30)),
                  Row(
                    children: <Widget>[
                      SizedBox(width: ScreenUtil().setWidth(30)),
                      // logo 图标
                      if (!_showNewBg) YBDLogoAppName(),
                      Expanded(child: Container()),
                      // 客服图标
                      _supportButton(),
                      SizedBox(width: ScreenUtil().setWidth(30)),
                    ],
                  ),
                  if (_showNewBg) SizedBox(height: 90.px),
                  if (_showNewBg) YBDLogoAppName(showNewBg: true),
                  Expanded(child: Container()),
                  // voice 文字
                  if (!_showNewBg) YBDLoginVoiceText(),
                  if (!_showNewBg) SizedBox(height: ScreenUtil().setWidth(110)),
                  // 登录按钮
                  Container(
                    child: Column(
                      children: <Widget>[
                        YBDPhoneLoginButton(_onPhoneLogin),
                        SizedBox(height: ScreenUtil().setWidth(180)),
                      ],
                    ),
                  ),
                  // 底部协议按钮
                  YBDLoginTerms(showNewBg: _showNewBg),
                  SizedBox(
                      height: ScreenUtil()
                          .setWidth(ScreenUtil().bottomBarHeight + 10)),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void myBuildAChvSoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 客服图标
  Widget _supportButton() {
    return Container(
      child: YBDScaleAnimateButton(
        onTap: _onTapChat,
        child: Image.asset('assets/images/login/icon_support_login.png',
            width: ScreenUtil().setWidth(80),
            height: ScreenUtil().setWidth(80),
            fit: BoxFit.cover),
      ),
    );
  }

  /// 点击聊天按钮的响应事件
  _onTapChat() {
    logger.v("clicked chat button");
    YBDCommonTrack().loginClick('support');
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: 'login_page',
      itemName: 'support',
    ));
    YBDCommonUtil.showSupport(context);
  }

  void _onPhoneLogin() {
    Platform.isIOS
        ? YBDNavigatorHelper.navigateTo(
            context,
            YBDNavigatorHelper.login_indoor + "/false",
            forceFluro: true,
          )
        : YBDNavigatorHelper.navigateTo(
            context,
            YBDNavigatorHelper.login_indoor + "/$_shouldShowSMS",
            forceFluro: true,
          );
  }
}
