import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/sp_ybd_util.dart';

typedef OnChosenCallback = Function(String account, String password);
typedef OnRemoveCallback = Function(String account);

class YBDUsernameField extends StatefulWidget {
  TextEditingController _usernameController;
  OnChosenCallback _onChosen;
  final OnRemoveCallback? onRemove;
  FocusNode _focusNode;

  YBDUsernameField(this._usernameController, this._focusNode, this._onChosen, {this.onRemove});

  @override
  _YBDUsernameFieldState createState() => new _YBDUsernameFieldState();
}

class _YBDUsernameFieldState extends BaseState<YBDUsernameField> {
  final LayerLink _layerLink = LayerLink();
  OverlayEntry? _overlayEntry; // 记住多个账号密码
  OverlayEntry? _overlayEntryMask;

  bool saved = false;
  var _value = ValueNotifier<String?>('');
  List<List<String>> _credentials = [];

  @override
  void initState() {
    super.initState();

    YBDSPUtil.get(Const.SP_SAVE_PASSWORD).then((value) => _value.value = value);
    YBDSPUtil.getSavedCredentials().then((value) {
      _credentials = value;
      if (value != null && value.isNotEmpty) saved = true;
      setState(() {});
    });
  }
  void initStateVa3ONoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Row(
      children: <Widget>[
        Image.asset('assets/images/icon_login_user.png', width: ScreenUtil().setWidth(62)),
        SizedBox(width: ScreenUtil().setWidth(20)),
        Expanded(
          child: CompositedTransformTarget(
            link: this._layerLink,
            child: TextField(
              controller: widget._usernameController,
              focusNode: widget._focusNode,
              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(30), fontWeight: FontWeight.w500),
              decoration: InputDecoration(
                  hintStyle: TextStyle(
                    color: Color(0xffDCDCDC),
                    fontSize: ScreenUtil().setSp(28),
                    fontWeight: FontWeight.w400,
                  ),
                  hintText: translate('username_id'),
                  border: InputBorder.none),
            ),
          ),
        ),
        saved
            ? Container(
                width: ScreenUtil().setWidth(80),
                child: TextButton(
                  style: TextButton.styleFrom(padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20))),
                    // padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
                    onPressed: () {
                      // 显示记住的账号列表
                      this._overlayEntry = _createOverlayEntry();
                      this._overlayEntryMask = OverlayEntry(builder: (context) {
                        return Stack(children: <Widget>[
                          GestureDetector(
                              onTap: () {
                                this._overlayEntryMask?.remove();
                                this._overlayEntry?.remove();
                              },
                              child: Container(color: Colors.black.withOpacity(0.1)))
                        ]);
                      });
                      Overlay.of(context)!.insert(this._overlayEntryMask!);
                      Overlay.of(context)!.insert(this._overlayEntry!);
                    },
                    child: Icon(Icons.arrow_drop_down, color: Colors.white)),
              )
            : Container()
      ],
    );
  }
  void myBuildoHqnQoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  OverlayEntry _createOverlayEntry() {
    RenderBox renderBox = context.findRenderObject() as RenderBox;
    var size = renderBox.size;

    return OverlayEntry(builder: (context) {
      return Positioned(
          width: ScreenUtil().setWidth(462),
          child: CompositedTransformFollower(
              link: this._layerLink,
              showWhenUnlinked: false,
              offset: Offset(0.0, size.height + 5.0),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
                child: Material(
                    elevation: 4.0,
                    child: ValueListenableBuilder<String?>(
                        valueListenable: _value,
                        builder: (context, value, child) {
                          return credentialsItem();
                        })),
              )));
    });
  }

  Widget credentialsItem() {
    return ListView.builder(
        padding: EdgeInsets.zero,
        shrinkWrap: true,
        reverse: true,
        itemCount: _credentials.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () async {
              widget._onChosen.call(_credentials[index][0], _credentials[index][1]);

              /// 选择保存的账号密码后，隐藏下拉列表
              this._overlayEntryMask?.remove();
              this._overlayEntry?.remove();
            },
            child: Container(
              decoration: BoxDecoration(),
              height: ScreenUtil().setWidth(70),
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(24), right: ScreenUtil().setWidth(10)),
              child: Row(
                children: [
                  Text(_credentials[index][0],
                      style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff333333))),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.all(ScreenUtil().setWidth(14)),
                    height: ScreenUtil().setWidth(54),
                    width: ScreenUtil().setWidth(54),
                    child: TextButton(
                      style: TextButton.styleFrom(padding: EdgeInsets.zero),
                      // padding: EdgeInsets.zero,
                      onPressed: () async {
                        await YBDSPUtil.removeSavedCredentials(_credentials[index][0]);

                        /// 回调
                        widget.onRemove?.call(_credentials[index][0]);

                        _credentials.removeAt(index);

                        /// 当保存的账号密码全部被清空，隐藏下拉列表
                        if (_credentials.isEmpty) {
                          this._overlayEntryMask?.remove();
                          this._overlayEntry?.remove();
                        }

                        YBDSPUtil.get(Const.SP_SAVE_PASSWORD).then((value) => _value.value = value);
                        setState(() {});
                      },
                      child: Image.asset('assets/images/icon_close_grey.png',
                          width: ScreenUtil().setWidth(48), height: ScreenUtil().setWidth(48)),
                    ),
                  )
                ],
              ),
//              child: ListTile(
//                  contentPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
//                  dense: true,
//                  title: Text(_credentials[index][0],
//                      style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff333333))),
//                  trailing: Container(
//                    padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
//                    height: ScreenUtil().setWidth(54),
//                    width: ScreenUtil().setWidth(54),
//                    child: TextButton(
//                      padding: EdgeInsets.zero,
//                      onPressed: () async {
//                        await YBDSPUtil.removeSavedCredentials(_credentials[index][0]);
//
//                        /// 回调
//                        widget.onRemove?.call(_credentials[index][0]);
//
//                        _credentials.removeAt(index);
//
//                        /// 当保存的账号密码全部被清空，隐藏下拉列表
//                        if (_credentials.isEmpty) {
//                          this._overlayEntryMask?.remove();
//                          this._overlayEntry?.remove();
//                        }
//
//                        YBDSPUtil.get(Const.SP_SAVE_PASSWORD).then((value) => _value.value = value);
//                        setState(() {});
//                      },
//                      child: Image.asset('assets/images/icon_close_grey.png',
//                          width: ScreenUtil().setWidth(48), height: ScreenUtil().setWidth(48)),
//                    ),
//                  ),
//                  onTap: () {
//                    /// 回调
//                    widget._onChosen?.call(_credentials[index][0], _credentials[index][1]);
//
//                    /// 选择保存的账号密码后，隐藏下拉列表
//                    this._overlayEntryMask?.remove();
//                    this._overlayEntry?.remove();
//                  }),
            ),
          );
        });
  }
  void credentialsItemzlNzOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
