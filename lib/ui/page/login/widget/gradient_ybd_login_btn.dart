import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';

/// 注册页面的登录按钮
class YBDGradientLoginButton extends StatelessWidget {
  /// 按钮标题
  final String? title;

  /// 点击按钮的回调
  final VoidCallback? onTap;

  YBDGradientLoginButton({this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(96),
      width: ScreenUtil().setWidth(640),
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xffD677E5).withOpacity(0.8), Color(0xff5078FF).withOpacity(0.8)],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          ),
          borderRadius: BorderRadius.all(Radius.circular(30))),
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(30)),
          onTap: () {
            logger.v('clicked new facebook login button');

            if (null != onTap) {
              onTap!();
            }
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                title!,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: ScreenUtil().setSp(36),
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  void build76pO3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
