import 'dart:async';

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';

/// 登录页面底部条款
class YBDLoginTerms extends StatelessWidget {
  final bool showNewBg;
  YBDLoginTerms({this.showNewBg = false});

  @override
  Widget build(BuildContext context) {
    double height = ScreenUtil().setWidth(60);
    return Container(
      height: height,
      width: ScreenUtil().screenWidth,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            '${translate('agree_to')} ',
            style: TextStyle(
                fontSize: ScreenUtil().setSp(24),
                color: Colors.white.withOpacity(showNewBg ? 0.6 : 1.0),
                fontWeight: showNewBg ? FontWeight.w300 : FontWeight.w500),
          ),
          YBDDelayGestureDetector(
            onTap: () {
              _onTapTerms(context);
            },
            child: Container(
              height: height,
              alignment: Alignment.center,
              child: Text(
                translate('terms'),
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  decoration: TextDecoration.underline,
                  color: Colors.white.withOpacity(showNewBg ? 0.6 : 1.0),
                  fontWeight: showNewBg ? FontWeight.w300 : FontWeight.w500,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void buildoLeoFoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 点击条款按钮的响应事件
void _onTapTerms(BuildContext context) {
  logger.v("clicked Terms & Privacy Policy button");
  YBDCommonTrack().loginClick('policy');
  YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT, location: 'login_page', itemName: 'terms'));
  YBDNavigatorHelper.navigateToWeb(
    context,
    "?url=${Uri.encodeComponent(YBDCommonUtil.getTermsURL)}&title=Agreement",
  );
}
