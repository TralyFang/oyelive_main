import 'dart:async';

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/image_ybd_code_entity.dart';
import 'package:oyelive_main/ui/page/login/util/login_ybd_page_util.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/user/entity/mobile_ybd_check_resp_entity.dart';
import '../bloc/phonenumber_ybd_input_bloc.dart';

class YBDOperation {
  static String get SignUp => "signUp";
  static String get ResetPwd => "resetPwd";
  static String get LinkPhone => "perfectPhone";
  static String get ChangePwd => "changePassword";
}

class YBDGraphicCodeDialog extends StatefulWidget {
  YBDPhoneNumberInputBloc? inputBloc;
  String? phone;

  String operation;
  Function? onSuccess;
  YBDGraphicCodeDialog(this.phone, this.operation, {this.onSuccess(tokenId)?});

  @override
  YBDGraphicCodeDialogState createState() => new YBDGraphicCodeDialogState();
}

class YBDGraphicCodeDialogState extends BaseState<YBDGraphicCodeDialog> with SingleTickerProviderStateMixin {
  String? code;

  late AnimationController _animationController;
  late Animation _animation;

  YBDImageCodeEntity? imageCodeEntity;
  bool init = true;

  TextEditingController _textEditingController = TextEditingController();

  bool incorrectAuthCode = false;

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    // Widget centerWidget;
    // if (init) {
    //   centerWidget = Center(
    //     child: YBDLoadingCircle(),
    //   );
    // }
    // if(imageCodeEntity==null){
    //   centerWidget =Center(child: GestureDetector(
    //     child: RotationTransition(
    //       //设置动画的旋转中心
    //       alignment: Alignment.center,
    //       //动画控制器
    //       turns: _animation,
    //
    //       child: Icon(
    //         Icons.refresh,
    //         color: Colors.white,
    //         size: ScreenUtil().setWidth(50),
    //       ),
    //     ),
    //   ),)
    // }
    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: true,
      body: Center(
        child: Container(
          width: ScreenUtil().setWidth(600),
          height: ScreenUtil().setWidth(470),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32)))),
          child: Column(
            children: [
              _closeBtn(),
              SizedBox(
                height: ScreenUtil().setWidth(20),
              ),
              Text(
                "Please enter the graphic verification code",
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(28),
                  color: Colors.black,
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(44),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: ScreenUtil().setWidth(350),
                    height: ScreenUtil().setWidth(80),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
                        border: Border.all(color: Colors.grey, width: ScreenUtil().setWidth(1))),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
                      child: Image.memory(
                        Base64Decoder().convert(imageCodeEntity?.record?.imageCode ?? ""),
                        fit: BoxFit.fill,
                        gaplessPlayback: true,
                        width: ScreenUtil().setWidth(350),
                        height: ScreenUtil().setWidth(80),
                        errorBuilder: (_, s, d) {
                          return Container(
                            width: ScreenUtil().setWidth(350),
                            height: ScreenUtil().setWidth(80),
                            color: Colors.grey,
                          );
                        },
                        // loadingBuilder: (_, s, d) {
                        //   return Container(
                        //     width: ScreenUtil().setWidth(350),
                        //     height: ScreenUtil().setWidth(80),
                        //     color: Colors.grey,
                        //   );
                        // },
                        // color: Colors.grey,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(20),
                  ),
                  GestureDetector(
                    onTap: () {
                      _animationController.reset();
                      _animationController.forward();
                      getImageCode();
                    },
                    child: Container(
                      width: ScreenUtil().setWidth(170),
                      height: ScreenUtil().setWidth(80),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
                        gradient: LinearGradient(
                          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        ),
                      ),
                      child: RotationTransition(
                        //设置动画的旋转中心
                        alignment: Alignment.center,
                        //动画控制器
                        turns: _animation as Animation<double>,

                        child: Icon(
                          Icons.refresh,
                          color: Colors.white,
                          size: ScreenUtil().setWidth(50),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: ScreenUtil().setWidth(54),
              ),
              Container(
                width: ScreenUtil().setWidth(474),
                // height: ScreenUtil().setWidth(200),

                // alignment: Alignment.center,
                child: TextField(
                    controller: _textEditingController,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ScreenUtil().setWidth(60),
                        fontWeight: FontWeight.w400,
                        letterSpacing: ScreenUtil().setWidth(84)),
                    maxLength: 4,
                    // textAlign: TextAlign.center,
                    inputFormatters: [LengthLimitingTextInputFormatter(6), FilteringTextInputFormatter.digitsOnly],
                    keyboardType: TextInputType.number,
                    // focusNode: _authFocusNode,
                    showCursor: false,
                    onChanged: (code) {
                      this.code = code;

                      if ((code.length) == 4) {
                        incorrectAuthCode = false;
                        submit();
                      }
                      setState(() {});
                    },
                    decoration: InputDecoration(
                      isDense: true,
                      counterText: '',
                      contentPadding: EdgeInsets.all(0),
                      // hintStyle: TextStyle(
                      //   color: Color(0xff9A9A9A),
                      //   fontSize: ScreenUtil().setSp(24),
                      //   fontWeight: FontWeight.w400,
                      // ),
                      // hintText: translate('edit_phone_verification_hint'),
                      border: InputBorder.none,
                    ),
                    cursorColor: Color(0xff7DFAFF)),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: List.generate(7, (index) {
                  if (index % 2 == 0) {
                    int actualIndex = index ~/ 2;
                    bool currentFilled = (code?.length ?? 0) > actualIndex;
                    return Container(
                      width: ScreenUtil().setWidth(40),
                      height: ScreenUtil().setWidth(5),
                      decoration: BoxDecoration(
                          color: Color(currentFilled ? 0xff5D95E6 : 0xffD8D8D8),
                          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(2)))),
                    );
                  } else {
                    return SizedBox(
                      width: ScreenUtil().setWidth(78),
                    );
                  }
                }),
              ),
              if (incorrectAuthCode && _textEditingController.text.length == 4)
                Expanded(
                  child: Center(
                    child: Text(
                      "Incorrect verification code,please re-enter",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        color: Color(0xffD03333).withOpacity(0.85),
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
  void myBuildXxfHGoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关闭按钮
  Widget _closeBtn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        GestureDetector(
          onTap: () {
            logger.v('clicked close btn');
            Navigator.pop(context);
          },
          child: Container(
            width: ScreenUtil().setWidth(68),
            padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
            // height: ScreenUtil().setWidth(58),
            child: Image.asset(
              'assets/images/dc/daily_check_close_btn.png',
              color: Color(0xff626262),
              fit: BoxFit.cover,
            ),
          ),
        ),
        // SizedBox(width: ScreenUtil().setWidth(10)),
      ],
    );
  }
  void _closeBtnUEvLqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _animationController = AnimationController(duration: Duration(milliseconds: 800), vsync: this);
    _animation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_animationController)
      ..addListener(() {
        setState(() {});
      });
    getImageCode();
  }
  void initStateqlLiAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getImageCode() async {
    YBDImageCodeEntity? imageCodeEntity = await ApiHelper.getImageCode(context, widget.phone, widget.operation);
    if (imageCodeEntity != null && imageCodeEntity.returnCode == Const.HTTP_SUCCESS) {
      this.imageCodeEntity = imageCodeEntity;
      setState(() {});
    } else {
      YBDToastUtil.toast(imageCodeEntity?.returnMsg ?? translate("failed"));
    }
  }

  submit() async {
    YBDCommonTrack().loginClick('phone_check');
    showLockDialog();
    YBDMobileCheckResp? mobileCheckResp =
        await ApiHelper.sendSMSCode(context, widget.phone, _textEditingController.text, widget.operation);
    dismissLockDialog();

    if (mobileCheckResp != null && mobileCheckResp.returnCode == Const.HTTP_SUCCESS) {
      Navigator.pop(context);
      YBDLoginPageUtil.smsPostExceptionRecord("tokenId=${mobileCheckResp.record?.tokenId}"
          "&operation=${widget.operation}"
          "&phone=${widget.phone}");

      widget.onSuccess?.call(mobileCheckResp.record?.tokenId);
      setState(() {});
    } else {
      incorrectAuthCode = true;
      YBDToastUtil.toast(mobileCheckResp?.returnMsg ?? translate("failed"));
      setState(() {});
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  void disposeEjpRdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDGraphicCodeDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
