import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';

/// 登录页顶部 logo 部分
class YBDLogoAppName extends StatelessWidget {
  final bool showNewBg;

  const YBDLogoAppName({Key? key, this.showNewBg = false}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (showNewBg)
      return Container(
          child: Column(children: [
        Image.asset('assets/images/setting/icon_logo_2022.webp', width: 280.px, height: 200.px, fit: BoxFit.cover),
      ]));
    return Container(
      child: Row(
        children: <Widget>[
          Image.asset(
            // logo 图标
            'assets/images/logo.webp',
            width: ScreenUtil().setWidth(72),
            height: ScreenUtil().setWidth(72),
            fit: BoxFit.cover,
          ),
          SizedBox(width: ScreenUtil().setWidth(20)),
          Image.asset(
            // logo 图标
            'assets/images/logo_title.png',
            height: ScreenUtil().setWidth(36),
            fit: BoxFit.cover,
          ),
        ],
      ),
    );
  }
  void buildV6KBVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
