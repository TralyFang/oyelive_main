import 'dart:async';

import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 登录页面第二张背景图上的星星
class YBDWallSecondStar extends StatefulWidget {
  @override
  _YBDWallSecondStarState createState() => _YBDWallSecondStarState();
}

class _YBDWallSecondStarState extends State<YBDWallSecondStar> with SingleTickerProviderStateMixin {
  late Animation<double> _doubleAnim;
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
    )..forward();

    _doubleAnim = Tween<double>(begin: 0.5, end: 1.5).animate(_animationController)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _animationController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          _animationController.forward();
        }
      });
  }
  void initStatec6NnGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
  void disposeVcvAxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth,
      height: ScreenUtil().screenHeight,
      child: Column(
        // 星星从上到下排列
        children: <Widget>[
          SizedBox(height: ScreenUtil().setSp(20)),
          Container(
            // 第一个
            height: ScreenUtil().setWidth(100),
            child: Row(
              children: <Widget>[
                SizedBox(width: ScreenUtil().setSp(400)),
                Container(
                  width: ScreenUtil().setWidth(200),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Opacity(
                        opacity: 0.6,
                        child: Transform.rotate(
                          angle: math.pi * -0.2,
                          child: Image.asset(
                            'assets/images/twinkling_star.webp',
                            width: ScreenUtil().setWidth(40 * _doubleAnim.value),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: ScreenUtil().setSp(20)),
          Container(
            height: ScreenUtil().setWidth(100),
            child: Row(
              // 第二个
              children: <Widget>[
                Expanded(child: Container()),
                Transform.rotate(
                  angle: math.pi * 0.2,
                  child: Image.asset(
                    'assets/images/twinkling_star.webp',
                    width: ScreenUtil().setWidth(60 * _doubleAnim.value),
                    fit: BoxFit.cover,
                  ),
                ),
                Expanded(child: Container()),
              ],
            ),
          ),
          Container(
            // 第三个
            height: ScreenUtil().setWidth(100),
            child: Row(
              children: <Widget>[
                SizedBox(width: ScreenUtil().setSp(500)),
                Container(
                  width: ScreenUtil().setWidth(200),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Transform.rotate(
                        angle: math.pi * -0.2,
                        child: Image.asset(
                          'assets/images/twinkling_star.webp',
                          width: ScreenUtil().setWidth(80 * _doubleAnim.value),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            // 第四个
            height: ScreenUtil().setWidth(100),
            child: Row(
              children: <Widget>[
                SizedBox(width: ScreenUtil().setSp(600)),
                Container(
                  width: ScreenUtil().setWidth(120),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Opacity(
                        opacity: 0.5,
                        child: Transform.rotate(
                          angle: math.pi * -0.2,
                          child: Image.asset(
                            'assets/images/twinkling_star.webp',
                            width: ScreenUtil().setWidth(60 * _doubleAnim.value),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: ScreenUtil().setSp(30)),
          Container(
            // 第五个
            height: ScreenUtil().setWidth(100),
            child: Row(
              children: <Widget>[
                Container(
                  width: ScreenUtil().setWidth(140),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Transform.rotate(
                        angle: math.pi * -0.2,
                        child: Image.asset(
                          'assets/images/twinkling_star.webp',
                          width: ScreenUtil().setWidth(90 * _doubleAnim.value),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: ScreenUtil().setSp(140)),
          Container(
            // 第六个
            height: ScreenUtil().setWidth(100),
            child: Row(
              children: <Widget>[
                SizedBox(width: ScreenUtil().setSp(340)),
                Container(
                  width: ScreenUtil().setWidth(200),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Opacity(
                        opacity: 0.5,
                        child: Transform.rotate(
                          angle: math.pi * -0.2,
                          child: Image.asset(
                            'assets/images/twinkling_star.webp',
                            width: ScreenUtil().setWidth(40 * _doubleAnim.value),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            // 第七个
            height: ScreenUtil().setWidth(100),
            child: Row(
              children: <Widget>[
                SizedBox(width: ScreenUtil().setSp(600)),
                Container(
                  width: ScreenUtil().setWidth(120),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Transform.rotate(
                        angle: math.pi * -0.2,
                        child: Image.asset(
                          'assets/images/twinkling_star.webp',
                          width: ScreenUtil().setWidth(60 * _doubleAnim.value),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void buildsR82aoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
