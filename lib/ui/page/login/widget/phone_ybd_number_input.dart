
import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/ui/page/login/util/login_ybd_page_util.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_ink_well.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/connectivity_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../common/util/valid_ybd_phone_util.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';
import '../../../widget/intl_input/countries.dart';
import '../../../widget/intl_input/intl_ybd_phone_field.dart';
import '../bloc/phonenumber_ybd_input_bloc.dart';
import '../handler/login_ybd_handler.dart';
import 'dialog_ybd_graphic_code.dart';

/// 手机号登录输入框和登录按钮
class YBDPhoneNumberInput extends StatefulWidget {
  @override
  YBDPhoneNumberInputState createState() => new YBDPhoneNumberInputState();
}

final countDownSecs = 60;

class YBDPhoneNumberInputState extends BaseState<YBDPhoneNumberInput> with SingleTickerProviderStateMixin {
  /// 记录密码为明文或密文的状态
  bool _isObscure = true;
  bool agreeTerms = true;

  /// 包含国家码的电话号码
  String? _phoneNumberComplete;

  String? _initialCountryCode;

  bool _isLoading = false;

  /// 数据框控制器，用来获取输入内容
  TextEditingController _phoneNumerController = TextEditingController();
  TextEditingController _authCodeController = TextEditingController();
  TextEditingController _pwdController = TextEditingController();

  /// 焦点
  FocusNode _phoneFocusNode = new FocusNode();
  FocusNode _authFocusNode = new FocusNode();
  FocusNode _pwdFocusNode = new FocusNode();

  Timer? _authCodeTimer;
  int _seconds = countDownSecs;

  /// 下划线样式
  BoxDecoration _colorDecoration = BoxDecoration(
      gradient: LinearGradient(
    colors: [Color(0xffD677E5).withOpacity(0.8), Color(0xff5078FF).withOpacity(0.8)],
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
  ));
  BoxDecoration _grayDecoration = BoxDecoration(color: Colors.white.withOpacity(0.4));

  /// 输入框字体
  TextStyle inputTextStyle() {
    return TextStyle(
      color: Colors.white,
      fontSize: ScreenUtil().setSp(30),
      fontWeight: FontWeight.w500,
    );
  }

  /// 输入框占位字符字体
  TextStyle inputTextHintStyle() {
    return TextStyle(
      color: Color(0xffDCDCDC),
      fontSize: ScreenUtil().setSp(28),
      fontWeight: FontWeight.w400,
    );
  }

  sendAuthCodeDecoration(bool isAvailable) {
    return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
        gradient: isAvailable
            ? LinearGradient(colors: [
                Color(0xff47CDCC),
                Color(0xff5E94E7),
              ], begin: Alignment.topCenter, end: Alignment.bottomCenter)
            : null,
        color: isAvailable ? null : Color(0xffCECECE).withOpacity(0.3));
  }

  authCodeBtn() {
    bool isAvailable = _seconds == countDownSecs;
    return GestureDetector(
      onTap: () {
        if (isAvailable) {
          if (context.read<YBDPhoneNumberInputBloc>().needGraphicCode)
            showDialog<Null>(
                context: context, //BuildContext对象
                barrierDismissible: false,
                builder: (BuildContext _) {
                  return YBDGraphicCodeDialog(
                    _phoneNumberComplete,
                    YBDOperation.SignUp,
                    onSuccess: (tokenId) {
                      startTimer();
                      context.read<YBDPhoneNumberInputBloc>().setTokenId(tokenId);
                    },
                  );
                });
          else
            sendAuthCode(context.read<YBDPhoneNumberInputBloc>());
        }
      },
      child: Container(
        width: ScreenUtil().setWidth(140),
        height: ScreenUtil().setWidth(60),
        alignment: Alignment.center,
        decoration: sendAuthCodeDecoration(isAvailable),
        child: Text(
          isAvailable ? translate('send') : '${_seconds}s',
          style: TextStyle(fontSize: ScreenUtil().setSp(22), color: Colors.white),
        ),
      ),
    );
  }

  /// 验证码输入框
  inputAuthCode() {
    return Row(
      children: [
//        Image.asset(
//          'assets/images/icon_password.png',
//          width: ScreenUtil().setWidth(60),
//        ),
        SizedBox(
          width: ScreenUtil().setWidth(20),
        ),
        Expanded(
          child: TextField(
            controller: _authCodeController,
            style: inputTextStyle(),
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            keyboardType: TextInputType.number,
            focusNode: _authFocusNode,
            decoration: InputDecoration(
              hintStyle: inputTextHintStyle(),
              hintText: translate('verification_code'),
              border: InputBorder.none,
            ),
          ),
        ),
        authCodeBtn(),
        SizedBox(
          width: ScreenUtil().setWidth(14),
        ),
      ],
    );
  }

  /// 密码输入框
  inputPwd() {
    return Container(
      width: ScreenUtil().setWidth(580),
      child: Row(
        children: [
          // 左侧密码图标
          Image.asset(
            'assets/images/icon_password.png',
            width: ScreenUtil().setWidth(62),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(20),
          ),
          // 密码输入部分
          Expanded(
            child: TextField(
              style: inputTextStyle(),
              obscureText: _isObscure,
              focusNode: _pwdFocusNode,
              inputFormatters: [FilteringTextInputFormatter.deny(' ')],
              decoration: InputDecoration(
                hintStyle: inputTextHintStyle(),
                hintText: translate('password'),
                border: InputBorder.none,
              ),
              controller: _pwdController,
            ),
          ),
          // 右边的小眼睛
          Container(
            width: ScreenUtil().setWidth(80),
            child: TextButton(
              style: ButtonStyle(
                padding: MaterialStateProperty.all(
                  EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
                ),
              ),
              onPressed: () {
                YBDLogUtil.v('pressed show pwd button');
                setState(() {
                  _isObscure = !_isObscure;
                });
              },
              child: Image.asset(
                _isObscure ? "assets/images/show_pass.png" : "assets/images/hide_pass.png",
                width: ScreenUtil().setWidth(40),
                color: Color(0xffDCDCDC),
              ),
            ),
          )
        ],
      ),
    );
  }

  startTimer() {
    _authCodeTimer?.cancel();
    _authCodeTimer = Timer.periodic(Duration(seconds: 1), (_) {
      if (_seconds > 0) {
        _seconds--;
      } else {
        _seconds = countDownSecs;
        _.cancel();
      }
      setState(() {});
    });
  }

  resetTimer() {
    _authCodeTimer?.cancel();
    _seconds = 60;
    setState(() {});
  }

  resetPhoneInput() {
    _phoneNumerController.clear();
    _phoneNumberComplete = null;
  }

  /// 手机号输入框
  /// readOnly: 发送验证码成功后显示为只读状态
  inputPhoneNumber(bool readOnly) {
    return YBDIntlPhoneField(
        style: inputTextStyle(),
        decoration: InputDecoration(
            hintText: translate('phone_number'),
            hintStyle: inputTextHintStyle(),
            border: InputBorder.none,
            floatingLabelBehavior: FloatingLabelBehavior.never),
        initialCountryCode: _initialCountryCode,
        autoValidate: false,
        readOnly: readOnly,
        focusNode: _phoneFocusNode,
        maxLength: 20,
        keyboardType: TextInputType.phone,
        controller: _phoneNumerController,
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
        onChanged: (phone) {
          _initialCountryCode = phone.countryCode;
          _phoneNumberComplete = phone.completeNumber.trim();
          YBDLogUtil.v('input phone number : $_phoneNumberComplete');
        },
        onCountryChange: (selectedCountryName, selectedCountryCode, phone) {
          _initialCountryCode = selectedCountryCode;
          _phoneNumberComplete = phone.completeNumber.trim();
          YBDLogUtil.v('on change country code : $_phoneNumberComplete');
        },
        onTap: () {
          YBDCommonTrack().loginClick('phone_text');
        });
  }

  Future<bool> sendAuthCode(YBDPhoneNumberInputBloc bloc) async {
    YBDLogUtil.v('phone is valid');
    //兼容电话号码后面带一个空格的情况
    var dd = _phoneNumberComplete![_phoneNumberComplete!.length - 1];
    YBDLogUtil.v('input phone number : $_phoneNumberComplete dd $dd');
    try {
      //如果带空格则转化int异常，catch里面把最后一个空格去掉
      int dd1 = int.parse(dd);
      YBDLogUtil.v('input phone number : dd1 $dd1');
    } catch (e) {
      _phoneNumberComplete = _phoneNumberComplete!.substring(0, _phoneNumberComplete!.length - 1);
    }
    YBDLogUtil.v('input phone number : _phoneNumberComplete $_phoneNumberComplete');
    bool isAuthCode = await bloc.mobileLogin(
      context,
      _phoneNumberComplete!,
      showLoading: true,
    );
    if (isAuthCode) startTimer();
    return isAuthCode;
  }
  void sendAuthCodeVncGToyelive(YBDPhoneNumberInputBloc bloc)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击下一步或登录按钮
  onTapNext() async {
    FocusScope.of(context).requestFocus(FocusNode());

    if (YBDConnectivityUtil.getInstance()!.rs == ConnectivityResult.none) {
      YBDToastUtil.toast('You are facing the internet issue, please check.');
      return;
    }

    YBDLogUtil.v('clicked next step button');

    if (Platform.isIOS) {
      // iOS 不做勾选框
    } else {
      if (!agreeTerms) {
        YBDToastUtil.toast("Please agree to our Terms & Privacy Policy first!");
        return;
      }
    }

    YBDPhoneNumberInputBloc bloc = context.read<YBDPhoneNumberInputBloc>();
    switch (bloc.state) {
      case InputStep.CodeNotSend:
      case InputStep.GetImageCode:
        YBDLogUtil.v('code not send');
        YBDCommonTrack().loginClick('phone_next');
        if (YBDValidPhoneUtil.validatorPhone(_phoneNumberComplete)) {
          bool isAuthLogin = await sendAuthCode(bloc);
          if (isAuthLogin) {
            _authFocusNode.requestFocus();
          } else {
            _pwdFocusNode.requestFocus();
          }
        } else {
          YBDLogUtil.v('phone is not valid');
          YBDToastUtil.toast(translate("invalid_phone"));
        }
        break;
      case InputStep.AuthCodeLogin:
        YBDLogUtil.v('auth code login');
        YBDCommonTrack().loginClick('more_opt_phone_login');
        if (_authCodeController.text.isEmpty) {
          YBDLogUtil.v('auth code is empty');
          YBDToastUtil.toast(translate('enter_code'));
          return;
        }

        bool verifySuccess = await bloc.loginByAuthCode(
          context,
          _authCodeController.text.trim(),
          showLoading: true,
        );

        if (verifySuccess) {
          logger.i('auth code verify success');
          YBDSPUtil.save(Const.SP_LOGIN_PHONE, _phoneNumerController.text);
          YBDSPUtil.save(Const.SP_LOGIN_PHONE_CODE, _initialCountryCode);
          YBDDialogUtil.showLoading(context, barrierDismissible: false);
          // 调注册接口
          await YBDLoginPageUtil.requestProcessToken(
            context: context,
            token: _authCodeController.text,
            tokenId: '${bloc.tokenId}',
          );
          YBDDialogUtil.hideLoading(context);
        } else {
          logger.e('auth code verify failed');
        }
        break;
      case InputStep.PwdLogin:
        YBDLogUtil.v('password login');
        YBDCommonTrack().loginClick('more_opt_phone_login');
        if (_pwdController.text.isEmpty) {
          YBDToastUtil.toast(translate("enter_password"));
          return;
        }

        if (YBDValidPhoneUtil.validatorPhone(_phoneNumberComplete)) {
          YBDLogUtil.v('phone is valid');
          int code = await bloc.loginByPassWord(
            context,
            _phoneNumberComplete,
            _pwdController.text,
            showLoading: true,
          );

          if (code > 0) {
            YBDSPUtil.save(Const.SP_LOGIN_PHONE, _phoneNumerController.text);
            YBDSPUtil.save(Const.SP_LOGIN_PHONE_CODE, _initialCountryCode);
            await onLoginSuccess(context, false, needChangePwd: code == 2, onFixPwd: () {
              _pwdController.clear();
            });
            YBDCommonTrack().loginTrack(loginWay: "phone");
          }
        } else {
          YBDLogUtil.v('phone is not valid');
          YBDToastUtil.toast(translate("invalid_phone"));
        }
        break;
    }
  }

  @override
  void initState() {
    super.initState();

    YBDCommonUtil.getDefaultCountryCode(context).then((value) {
      if (mounted) {
        setState(() {
          _initialCountryCode = value;
        });
      }
      return YBDSPUtil.get(Const.SP_LOGIN_PHONE_CODE);
    }).then((value) {
      if (value != null) {
        _initialCountryCode = value;
        setState(() {});
      }
      return YBDSPUtil.get(Const.SP_LOGIN_PHONE);
    }).then((value) {
      if (value != null) {
        List<Map<String, String>> result =
            YBDCountries.list().where((element) => _initialCountryCode == element['code']).toList();

        if (null == result || result.isEmpty) {
          result = [YBDCountries.emptyCounty];
        }

        _phoneNumerController.text = value;
        _phoneNumberComplete = result[0]['dial_code']! + value;
        setState(() {});
      }
    });
    addSub(context.read<YBDPhoneNumberInputBloc>().listen((state) {
      if (state == InputStep.GetImageCode) {
        showDialog<Null>(
            context: context, //BuildContext对象
            barrierDismissible: false,
            builder: (BuildContext _) {
              return YBDGraphicCodeDialog(
                _phoneNumberComplete,
                YBDOperation.SignUp,
                onSuccess: (tokenId) {
                  startTimer();
                  context.read<YBDPhoneNumberInputBloc>()
                    ..setTokenId(tokenId)
                    ..add(InputStep.AuthCodeLogin);
                },
              );
            });
      }
    }));
  }
  void initState5XdAioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    if (_authCodeController.text.isEmpty) {
      YBDLoginPageUtil.smsPostExceptionToAnalytics();
    }
    YBDLoginPageUtil.smsPostExceptionClean();

    _authCodeController.dispose();
    _pwdController.dispose();
    _phoneFocusNode.dispose();
    _authFocusNode.dispose();
    _pwdFocusNode.dispose();
    _authCodeTimer?.cancel();
    super.dispose();
  }
  void disposeN6TiCoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Column(
            children: [
              _phoneNumberNotice(),
              BlocBuilder<YBDPhoneNumberInputBloc, InputStep>(builder: (_, state) {
                // 手机号输入框
                return Column(
                  children: <Widget>[
                    Container(
                      height: ScreenUtil().setWidth(100),
                      width: ScreenUtil().setWidth(580),
                      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
                      // 如果已发送验证码不可编辑
                      child: Row(
                        children: [
                          Expanded(child: inputPhoneNumber(!isNextStep(state))),
                          if (state == InputStep.AuthCodeLogin || state == InputStep.PwdLogin)
                            Container(
                              width: ScreenUtil().setWidth(100),
                              child: TextButton(
                                onPressed: () {
                                  resetPhoneInput();
                                  resetTimer();

                                  context.read<YBDPhoneNumberInputBloc>().add(InputStep.CodeNotSend);
                                },
                                style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.zero)),
                                child: Icon(
                                  Icons.cancel,
                                  size: ScreenUtil().setWidth(40),
                                  color: Colors.white,
                                ),
                              ),
                            )
                        ],
                      ),
                    ),
                    // 下划线
                    Container(
                      height: ScreenUtil().setWidth(1),
                      width: ScreenUtil().setWidth(580),
                      decoration: _phoneFocusNode.hasFocus ? _colorDecoration : _grayDecoration,
                    ),
                  ],
                );
              }),
              SizedBox(height: ScreenUtil().setWidth(40)),
              BlocBuilder<YBDPhoneNumberInputBloc, InputStep>(builder: (_, state) {
                // 验证码输入框或密码输入框
                switch (state) {
                  case InputStep.CodeNotSend:
                    YBDLogUtil.v('code not send');
                    return Container();
                  case InputStep.AuthCodeLogin:
                    YBDLogUtil.v('auth code login');
                    return Column(
                      children: <Widget>[
                        Container(
                          // 验证码输入框
                          height: ScreenUtil().setWidth(92),
                          width: ScreenUtil().setWidth(580),
                          padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
                          child: inputAuthCode(),
                        ),
                        // 下划线
                        Container(
                          height: ScreenUtil().setWidth(1),
                          width: ScreenUtil().setWidth(580),
                          decoration: _authFocusNode.hasFocus ? _colorDecoration : _grayDecoration,
                        ),
                      ],
                    );
                  case InputStep.PwdLogin:
                    YBDLogUtil.v('password login');
                    return Column(
                      children: <Widget>[
                        // 密码输入框
                        Container(
                            height: ScreenUtil().setWidth(92),
                            width: ScreenUtil().setWidth(580),
                            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
                            child: inputPwd()),
                        // 下划线
                        Container(
                          height: ScreenUtil().setWidth(1),
                          width: ScreenUtil().setWidth(580),
                          decoration: _pwdFocusNode.hasFocus ? _colorDecoration : _grayDecoration,
                        ),
                        SizedBox(
                          height: ScreenUtil().setWidth(30),
                        ),
                        // 忘记密码按钮
                        Container(
                          width: ScreenUtil().setWidth(580),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              GestureDetector(
                                child: Text(
                                  translate('forgot_password'),
                                  style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                                ),
                                onTap: () {
                                  YBDLogUtil.v('clicked forgot password button');
                                  YBDNavigatorHelper.navigateTo(
                                      context,
                                      YBDNavigatorHelper.forgot_password +
                                          "/${_phoneNumerController.text}/$_phoneNumberComplete/$_initialCountryCode");
                                },
                              )
                            ],
                          ),
                        )
                      ],
                    );
                  default:
                    YBDLogUtil.v('default case');
                    return Container();
                }
              }),
              _termRow(),
            ],
          ),
        ),
        // 登录按钮
        BlocBuilder<YBDPhoneNumberInputBloc, InputStep>(builder: (_, state) {
          return Column(
            children: <Widget>[
              SizedBox(height: ScreenUtil().setWidth(isNextStep(state) ? 20 : 50)),
              // YBDGradientLoginButton(
              //   title: translate(state == InputStep.CodeNotSend ? "next" : "login"),
              //   onTap: onTapNext,
              // )
              // v13.0使用新的白色登录按钮
              Container(
                height: ScreenUtil().setWidth(96),
                width: ScreenUtil().setWidth(640),
                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(30))),
                child: Material(
                  type: MaterialType.transparency,
                  child: YBDDelayInkWell(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    onTap: onTapNext,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          translate(isNextStep(state) ? "next" : "login"),
                          style: TextStyle(
                            color: Color(0xff5E50C7),
                            fontSize: ScreenUtil().setSp(36),
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          );
        }),
      ],
    );
  }
  void myBuildCPTGVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 输入手机号的提示语
  Widget _phoneNumberNotice() {
    if (Platform.isIOS) {
      return SizedBox(height: ScreenUtil().setWidth(112));
    } else {
      return Column(
        children: [
          SizedBox(height: ScreenUtil().setWidth(60)),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(60)),
            child: Text(
              "YBDEnter the phone number you want to sign up/in with. We won't share your phone number with anyone.",
              style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
            ),
          ),
          SizedBox(height: ScreenUtil().setWidth(50)),
        ],
      );
    }
  }
  void _phoneNumberNotice79pWDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 协议复选框一栏
  Widget _termRow() {
    if (Platform.isIOS) {
      return Container();
    } else {
      return Column(
        children: [
          SizedBox(height: ScreenUtil().setWidth(40)),
          Container(
            width: ScreenUtil().setWidth(562),
            child: Row(
              children: [
                Container(
                  height: ScreenUtil().setWidth(36),
                  width: ScreenUtil().setWidth(36),
                  decoration: BoxDecoration(color: Colors.white),
                  child: Theme(
                    data: Theme.of(context).copyWith(unselectedWidgetColor: Colors.white),
                    child: Checkbox(
                        activeColor: Colors.white,
                        hoverColor: Colors.white,
                        checkColor: Colors.black,
                        value: agreeTerms,
                        onChanged: (check) {
                          if (check!) YBDCommonTrack().loginClick('phone_agree');
                          setState(() {
                            agreeTerms = !agreeTerms;
                          });
                        }),
                  ),
                ),
                SizedBox(width: ScreenUtil().setWidth(20)),
                Text('Agree to our ', style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white)),
                YBDDelayGestureDetector(
                  onTap: () {
                    _onTapTerms(context);
                  },
                  child: Text(
                    translate('terms'),
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(24), decoration: TextDecoration.underline, color: Colors.white),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: ScreenUtil().setWidth(10)),
        ],
      );
    }
  }
  void _termRowbqVfdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击条款按钮的响应事件
  void _onTapTerms(BuildContext context) {
    YBDLogUtil.v("clicked Terms & Privacy Policy button");
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT, location: 'login_page', itemName: 'terms'));
    YBDNavigatorHelper.navigateToWeb(
      context,
      "?url=${Uri.encodeComponent(YBDCommonUtil.getTermsURL)}&title=Agreement",
    );
  }
}
