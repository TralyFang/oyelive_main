import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

class YBDPhoneLoginButton extends StatelessWidget {
  /// 点击登录按钮的回调
  final VoidCallback? onTap;

  YBDPhoneLoginButton(this.onTap);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(96),
      width: ScreenUtil().setWidth(640),
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xffD677E5).withOpacity(0.8),
              Color(0xff5078FF).withOpacity(0.8)
            ],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          ),
          borderRadius: BorderRadius.all(Radius.circular(30))),
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(30)),
          onTap: () {
            logger.v('clicked phone login button');

            if (null != onTap) {
              onTap!();
            }
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff3C3C3C).withOpacity(0.5),
                      blurRadius: ScreenUtil().setWidth(4),
                      offset: Offset(0, ScreenUtil().setWidth(2)),
                    ),
                  ],
                ),
                child: Image.asset(
                  'assets/images/phone.png',
                  width: ScreenUtil().setWidth(25),
                  // height: ScreenUtil().setWidth(30),
                  fit: BoxFit.fitWidth,
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(30)),
              Text(
                'Phone',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: ScreenUtil().setSp(36),
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
