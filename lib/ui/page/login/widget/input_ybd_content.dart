import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:social_share/social_share.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../handler/login_ybd_handler.dart';
import 'user_ybd_name_input.dart';

class YBDInputContent extends StatefulWidget {
  Widget input;

  YBDInputContent(this.input);

  @override
  YBDInputContentState createState() => new YBDInputContentState();
}

class YBDInputContentState extends BaseState<YBDInputContent>
    with
        AutomaticKeepAliveClientMixin,
        SingleTickerProviderStateMixin,
        WidgetsBindingObserver {
  @override
  Widget myBuild(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(''),
      ),
    );
  }

  void myBuildrvI4eoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  // logo动画控制器
  late AnimationController _logoController;
  late Animation _logoAnimation;

  Map<dynamic, dynamic>? appsForShare = {};
  @override
  void initState() {
    super.initState();
    _logoController = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this)
      ..addListener(() {
        setState(() {});
      });
    _logoAnimation = Tween(begin: 1.0, end: 0.0)
        .chain(CurveTween(curve: Curves.easeInCubic))
        .animate(_logoController);

    // 添加监听
    WidgetsBinding.instance?.addObserver(this);

    // Query
    SocialShare.checkInstalledAppsForShare().then((value) {
      logger.i('22.11.10---apps for share : $value');
      appsForShare = value;
    });
  }

  void initStatekrtPboyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void dispose() {
    super.dispose();
    _logoController.dispose();
    WidgetsBinding.instance?.removeObserver(this);
  }

  void disposebRbihoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  // 尺寸改变时回调
  @override
  void didChangeMetrics() {
    super.didChangeMetrics();

//    WidgetsBinding.instance?.addPostFrameCallback((_) {
//      setState(() {
//        // 获取底部遮挡区域的高度(键盘的高度)
//        if (MediaQuery.of(context).viewInsets.bottom == 0) {
//          _logoController.reverse();
//        } else {
//          _logoController.forward();
//        }
//      });
//    });
  }

  void didChangeMetricstSRLroyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 第三方登录按钮
  Widget getButton(String imagePath, Function onTap) {
    return Container(
      width: ScreenUtil().setWidth(100),
      child: TextButton(
        onPressed: onTap as void Function()?,
        style: ButtonStyle(
          padding: MaterialStateProperty.all(EdgeInsets.zero),
        ),
        child: Image.asset(
          imagePath,
          width: ScreenUtil().setWidth(75),
          height: ScreenUtil().setWidth(75),
        ),
      ),
    );
  }

  void getButtonnRBqCoyelive(String imagePath, Function onTap) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// logo部分
  logoWidget() {
    return KeyboardVisibilityBuilder(
      builder: (c, isV) {
        if (isV) {
          _logoController.forward();
        } else {
          _logoController.reverse();
        }
        return ScaleTransition(
            // 设置动画的缩放中心
            alignment: Alignment.center,
            // 动画控制器
            scale: _logoAnimation as Animation<double>,
            child: Container(
              height: ScreenUtil().setWidth(200) * _logoAnimation.value,
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setWidth(70)),
                  // logo
                  GestureDetector(
                    onTap: () {
                      logger.v('clicked logo button');
                      FocusScope.of(context).requestFocus((FocusNode()));
                    },
                    child: Image.asset(
                      'assets/images/logo.webp',
                      width: ScreenUtil().setWidth(113),
                    ),
                  ),
                  // SizedBox(height: ScreenUtil().setWidth(25)),
                  // //文字部分
                  // Image.asset(
                  //   'assets/images/logo_title.png',
                  //   height: ScreenUtil().setWidth(59),
                  //   fit: BoxFit.cover,
                  // ),
                ],
              ),
            ));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    String location = widget.input is YBDUserNameInput
        ? 'account_login_page'
        : 'phone_login_page';

    // 第三方登录按钮的间隔
    final optionSpace = 60;

    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          // SizedBox(height: ScreenUtil().setWidth(200) * (1 - _logoController.value)),
          // 监听键盘失效，暂时去掉logo
          SizedBox(height: ScreenUtil().setWidth(30)),
          logoWidget(),
          SizedBox(height: ScreenUtil().setWidth(25)),
          // 输入框和登录按钮部分
//          Expanded(child: ),
          widget.input,
          Spacer(),
          SizedBox(width: ScreenUtil().screenWidth)
        ],
      ),
    );
  }

  void build6Npvwoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  bool get wantKeepAlive => true;

  /// 点击苹果登录按钮
  void _onTapAppleLogin(String location) {
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: location,
      itemName: 'apple',
    ));
    YBDLoginHandler(LoginType.apple, context, this, location).startLogin();
  }
}
