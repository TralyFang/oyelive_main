import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/widget/scroll_ybd_loop_auto.dart';

import 'wall_ybd_stars_first.dart';
import 'wall_ybd_stars_second.dart';

/// 登录页面背景图切换动画
class YBDLoginPageBgWidget extends StatefulWidget {
  final bool showNewBg;
  YBDLoginPageBgWidget({this.showNewBg = false});

  @override
  _YBDLoginPageBgWidgetState createState() => _YBDLoginPageBgWidgetState();
}

class _YBDLoginPageBgWidgetState extends State<YBDLoginPageBgWidget> with SingleTickerProviderStateMixin {
  /// 动态切换的一组背景图
  List<String> _bgImages = [
    'assets/images/login/login_bg_1.webp',
    'assets/images/login/login_bg_2.webp',
    'assets/images/login/login_bg_1.webp',
    'assets/images/login/login_bg_2.webp',
  ];

  /// 动画控制器
  late AnimationController _controller;

  /// 动画渐变值
  late Animation<double> _opacity;

  /// 当前背景图索引
  int _index = 0;

  @override
  void initState() {
    super.initState();

    // 切换背景图的动画时长
    _controller = AnimationController(duration: Duration(milliseconds: 500), vsync: this);

    // 背景图切换动画
    _opacity = Tween<double>(begin: 0.0, end: 1.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          Future.delayed(Duration(seconds: 10), () {
            try {
              if (mounted) _controller.reverse();
            } catch (e) {
              print(e);
            }
          });
        } else if (status == AnimationStatus.dismissed) {
          _index = _index < 3 ? ++_index : 0;
          _controller.forward();
        }
      });

    _controller.forward();
  }
  void initStateF2eZRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  void dispose8bEhxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    if (widget.showNewBg)
      return Stack(children: [
        _scrollPic(),
        Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xff39057D).withOpacity(0.14), Color(0xff04003B).withOpacity(0.5)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter)),
        )
      ]);
    if (null == _bgImages || _bgImages.isEmpty) {
      // 默认显示白色背景
      return Container(
        width: ScreenUtil().screenWidth,
        height: ScreenUtil().screenHeight,
        color: Colors.white,
      );
    }

    return Stack(
      children: <Widget>[
        Opacity(
          opacity: _opacity.value,
          child: Image.asset(
            _bgImages[_index],
            width: ScreenUtil().screenWidth,
            height: ScreenUtil().screenHeight,
            fit: BoxFit.cover,
          ),
        ),
        _starAnimation(),
      ],
    );
  }
  void buildv4AeNoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _scrollPic() {
    return YBDScrollLoopAutoScroll(
      scrollDirection: Axis.vertical,
      enableScrollInput: false,
      duration: Duration(seconds: 60),
      child: _longPic(),
    );
  }

  Widget _longPic() {
    return Image.asset(
      'assets/images/login/login_bg_pic.jpg',
      width: ScreenUtil().screenWidth,
      height: 3429.px,
      fit: BoxFit.fitWidth,
    );
  }
  void _longPicE01Gvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 星星动画
  Widget _starAnimation() {
    return _bgImages[_index] == 'assets/images/login/login_bg_1.webp'
        ? Opacity(
            opacity: _opacity.value,
            child: YBDWallFirstStar(),
          )
        : Opacity(
            opacity: _opacity.value,
            child: YBDWallSecondStar(),
          );
  }
}
