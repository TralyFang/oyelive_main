import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
// import 'package:imei_plugin/imei_plugin.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/login/util/login_ybd_page_util.dart';
import 'package:oyelive_main/ui/page/login/widget/ban_ybd_account_dialog.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/http/http_ybd_util.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/connectivity_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api.dart';
import '../../../../module/user/entity/login_ybd_resp_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../../redux/user_ybd_redux.dart';
import '../handler/login_ybd_handler.dart';
import 'user_ybd_name_field.dart';

class YBDUserNameInput extends StatefulWidget {
  @override
  YBDUserNameInputState createState() => new YBDUserNameInputState();
}

/// 用户名密码登录的输入框和登录按钮
class YBDUserNameInputState extends BaseState<YBDUserNameInput> {
  TextEditingController _usrnameController = TextEditingController();
  TextEditingController _pwdController = TextEditingController();
  FocusNode _usernameFocusNode = new FocusNode();
  FocusNode _pwdFocusNode = new FocusNode();

  /// 下划线样式
  BoxDecoration _colorDecoration = BoxDecoration(
      gradient: LinearGradient(
    colors: [Color(0xffD677E5).withOpacity(0.8), Color(0xff5078FF).withOpacity(0.8)],
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
  ));
  BoxDecoration _grayDecoration = BoxDecoration(color: Colors.white.withOpacity(0.4));

  bool savePWD = false;
  bool isObsecure = true;

  inputPwd() {
    return Container(
      width: ScreenUtil().setWidth(580),
      child: Row(
        children: [
          // 左侧密码图标
          Image.asset('assets/images/icon_password.png', width: ScreenUtil().setWidth(62)),
          SizedBox(width: ScreenUtil().setWidth(20)),
          // 密码输入部分
          Expanded(
            child: TextField(
              style: TextStyle(
                color: Colors.white,
                fontSize: ScreenUtil().setSp(30),
                fontWeight: FontWeight.w500,
              ),
              obscureText: isObsecure,
              focusNode: _pwdFocusNode,
              inputFormatters: [FilteringTextInputFormatter.deny(' ')],
              decoration: InputDecoration(
                hintStyle: TextStyle(
                  color: Color(0xffDCDCDC),
                  fontSize: ScreenUtil().setSp(28),
                  fontWeight: FontWeight.w400,
                ),
                hintText: translate('password'),
                border: InputBorder.none,
              ),
              controller: _pwdController,
            ),
          ),
          Container(
            width: ScreenUtil().setWidth(80),
            child: TextButton(
              style: TextButton.styleFrom(padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20))),
                // padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
                onPressed: () {
                  setState(() {
                    isObsecure = !isObsecure;
                  });
                },
                child: Image.asset(
                  isObsecure ? "assets/images/show_pass.png" : "assets/images/hide_pass.png",
                  width: ScreenUtil().setWidth(40),
                  color: Color(0xffDCDCDC),
                )),
          ),
        ],
      ),
    );
  }

  loginByUsrnamePassword() async {
    YBDCommonTrack().loginClick('more_opt_account_login');
    if (YBDConnectivityUtil.getInstance()!.rs == ConnectivityResult.none) {
      YBDToastUtil.toast('You are facing the internet issue, please check.');
      return;
    }
    //check form
    if (_usrnameController.text.length == 0) {
      YBDToastUtil.toast(translate('enter_username'));
      return;
    }
    if (_pwdController.text.length == 0) {
      YBDToastUtil.toast(translate('enter_password'));
      return;
    }

    showLockDialog(barrierDismissible: true);
    String platformImei = '';
    // TODO: 修复v2.6.4提审包闪退的问题，先删掉获取imei的插件
    // try {
    //   platformImei = await ImeiPlugin.getImei(shouldShowRequestPermissionRationale: true);
    // } on PlatformException {
    //   platformImei = 'Failed to get platform version.';
    // }

    FocusScope.of(context).requestFocus(FocusNode());
    YBDLoginResp? data = await YBDHttpUtil.getInstance().doPost<YBDLoginResp>(
      context,
      YBDApi.USERNAME_PASSWORD_LOGIN,
      {"account": _usrnameController.text.trim(), "password": _pwdController.text, "imei": platformImei},
    );
    logger.v('login data = ${data}');
    if (data == null || (data.returnCode != Const.HTTP_SUCCESS && data.returnCode != Const.PWD_NEED_FIX)) {
      setState(() {
        savePWD = false;
      });

      YBDSPUtil.removeSavedCredentials(_usrnameController.text.trim());
      dismissLockDialog();
      if ((data!.returnCode == Const.BAN_ACCOUNT || data.returnCode == Const.BAN_DEVICE) && (data.returnMsg?.isNotEmpty ?? false)) {
        YBDLoginPageUtil.banAccountDialog(context, data.returnMsg ?? '');
      } else if (data.returnCode != Const.PWD_NEED_FIX) YBDToastUtil.toast(data.returnMsg);
    } else {
      // 保存用户信息到redux  SP
      Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
      store.dispatch(YBDUpdateUserAction(data.record!.userInfo));
      await YBDSPUtil.save(Const.SP_USER_INFO, data.record!.userInfo);
      dismissLockDialog();
      if (savePWD) {
        await YBDSPUtil.saveCredentials(_usrnameController.text.trim(), _pwdController.text);
      } else {
        await YBDSPUtil.removeSavedCredentials(_usrnameController.text.trim());
      }
      await onLoginSuccess(context, false, needChangePwd: data.returnCode == Const.PWD_NEED_FIX, onFixPwd: () {
        _pwdController.clear();
      });
      YBDCommonTrack().loginTrack(loginWay: "userid");
    }
  }

  @override
  Widget myBuild(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: ScreenUtil().setWidth(71),
        ),
        // 用户名输入框
        Container(
          height: ScreenUtil().setWidth(92),
          width: ScreenUtil().setWidth(580),
          child: YBDUsernameField(_usrnameController, _usernameFocusNode, (account, password) {
            _usrnameController.text = account;
            _pwdController.text = password;
            savePWD = true;
            setState(() {});
          }, onRemove: (account) {
            /// 被移除的账号密码为当前填入的，则清空
            if (_usrnameController.text.trim() == account) {
              _usrnameController.text = '';
              _pwdController.text = '';
              savePWD = false;
              setState(() {});
            }
          }),
        ),
        // 下划线
        Container(
          height: ScreenUtil().setWidth(1),
          width: ScreenUtil().setWidth(580),
          decoration: _usernameFocusNode.hasFocus ? _colorDecoration : _grayDecoration,
        ),
        SizedBox(
          height: ScreenUtil().setWidth(50),
        ),
        // 密码输入框
        Container(
          height: ScreenUtil().setWidth(92),
          width: ScreenUtil().setWidth(580),
          child: inputPwd(),
        ),
        // 下划线
        Container(
          height: ScreenUtil().setWidth(1),
          width: ScreenUtil().setWidth(580),
          decoration: _pwdFocusNode.hasFocus ? _colorDecoration : _grayDecoration,
        ),
        SizedBox(
          height: ScreenUtil().setWidth(20),
        ),
        // 记住密码
        Container(
          width: ScreenUtil().setWidth(562),
          child: Row(
            children: [
              Container(
                height: ScreenUtil().setWidth(30),
                width: ScreenUtil().setWidth(30),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Colors.white,
                  ),
                  child: Checkbox(
                      activeColor: Colors.white,
                      hoverColor: Colors.white,
                      checkColor: Colors.black,
                      value: savePWD,
                      onChanged: (check) {
                        setState(() {
                          savePWD = !savePWD;
                        });
                      }),
                ),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(20),
              ),
              TextButton(
                style: TextButton.styleFrom(padding: EdgeInsets.all(0)),
                // padding: EdgeInsets.all(0),
                onPressed: () {
                  setState(() {
                    savePWD = !savePWD;
                  });
                },
                child: Text(
                  translate('remember_me'),
                  style: TextStyle(
                    color: Color(0xFFFBEB66),
                    fontSize: ScreenUtil().setSp(26),
                    fontWeight: FontWeight.w400,
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(height: ScreenUtil().setWidth(30)),
        // 登录按钮
        // YBDGradientLoginButton(title: translate('login'), onTap: loginByUsrnamePassword)
        // v13.0使用新的白色登录按钮
        RepaintBoundary(
          child: Container(
            height: ScreenUtil().setWidth(96),
            width: ScreenUtil().setWidth(640),
            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Material(
              type: MaterialType.transparency,
              child: InkWell(
                borderRadius: BorderRadius.all(Radius.circular(30)),
                onTap: loginByUsrnamePassword,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      translate('login'),
                      style: TextStyle(
                        color: Color(0xff5E50C7),
                        fontSize: ScreenUtil().setSp(36),
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
  void myBuildtqebyoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    YBDSPUtil.getSavedCredentials().then((value) {
      if (value != null && value.isNotEmpty) {
        savePWD = true;
        List<String> pwdText = value.elementAt(value.length - 1);
        _usrnameController.text = pwdText[0];
        _pwdController.text = pwdText[1];

        setState(() {});
      }
    });
  }
  void initStateNLCbIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    _usernameFocusNode.dispose();
    _pwdFocusNode.dispose();
    _usrnameController.dispose();
    _pwdController.dispose();
  }
  void disposeGFCbvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
