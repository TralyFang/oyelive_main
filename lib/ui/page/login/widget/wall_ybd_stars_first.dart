import 'dart:async';

import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 登录页面第一张背景图上的星星
class YBDWallFirstStar extends StatefulWidget {
  @override
  _YBDWallFirstStarState createState() => _YBDWallFirstStarState();
}

class _YBDWallFirstStarState extends State<YBDWallFirstStar> with SingleTickerProviderStateMixin {
  late Animation<double> _doubleAnim;
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
    )..forward();

    _doubleAnim = Tween<double>(begin: 0.5, end: 1.5).animate(_animationController)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _animationController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          _animationController.forward();
        }
      });
  }
  void initStatee3odpoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
  void disposegT6aaoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth,
      height: ScreenUtil().screenHeight,
      child: Column(
        // 星星从上到下排列
        children: <Widget>[
          SizedBox(height: ScreenUtil().setSp(260)),
          Container(
            height: ScreenUtil().setWidth(100),
            child: Row(
              // 第一个
              children: <Widget>[
                Expanded(child: Container()),
                Transform.rotate(
                  angle: math.pi * -0.2,
                  child: Image.asset(
                    'assets/images/twinkling_star.webp',
                    width: ScreenUtil().setWidth(50 * _doubleAnim.value),
                    fit: BoxFit.cover,
                  ),
                ),
                Expanded(child: Container()),
              ],
            ),
          ),
          SizedBox(height: ScreenUtil().setSp(50)),
          Container(
            // 第二个
            height: ScreenUtil().setWidth(100),
            child: Row(
              children: <Widget>[
                Container(
                  width: ScreenUtil().setWidth(200),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Transform.rotate(
                          angle: math.pi * -0.2,
                          child: Image.asset(
                            'assets/images/twinkling_star.webp',
                            width: ScreenUtil().setWidth(80 * _doubleAnim.value),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: ScreenUtil().setSp(200)),
          Container(
            // 第三个
            height: ScreenUtil().setWidth(100),
            child: Row(children: <Widget>[
              SizedBox(width: ScreenUtil().setWidth(210)),
              Container(
                width: ScreenUtil().setWidth(200),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Transform.rotate(
                        angle: math.pi * -0.3,
                        child: Image.asset(
                          'assets/images/twinkling_star.webp',
                          width: ScreenUtil().setWidth(50 * _doubleAnim.value),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          ),
          SizedBox(height: ScreenUtil().setSp(100)),
          Container(
            // 第四个
            height: ScreenUtil().setWidth(100),
            child: Row(children: <Widget>[
              SizedBox(width: ScreenUtil().setWidth(0)),
              Container(
                width: ScreenUtil().setWidth(100),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Transform.rotate(
                        angle: math.pi * -0.3,
                        child: Image.asset(
                          'assets/images/twinkling_star.webp',
                          width: ScreenUtil().setWidth(60 * _doubleAnim.value),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
  void builduzoRnoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
