/*
 * @Author: William
 * @Date: 2022-11-21 14:14:04
 * @LastEditTime: 2022-12-07 16:34:07
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/login/widget/ban_account_dialog.dart
 */
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/status/local_audio/blue_ybd_gradient_button.dart';

class YBDBanAccountDialog extends StatelessWidget {
  final String title;
  final String description;
  final String endTime;
  final String reason;
  YBDBanAccountDialog({Key? key, this.title = '', this.description = '', this.endTime = '', this.reason = ''})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 500.px,
        height: 580.px,
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(32.px)),
        child: Column(children: [
          SizedBox(height: 40.px),
          _text(title, bold: true),
          SizedBox(height: 40.px),
          Container(
            width: 440.px,
            height: 296.px,
            // color: Colors.pink.withOpacity(0.3),
            child: SingleChildScrollView(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(width: 440.px, child: _text(description)),
                SizedBox(height: 10.px),
                Row(children: [_text('End time:', bold: true)]),
                Row(children: [_text(endTime, bigFont: false)]),
                SizedBox(height: 10.px),
                Row(children: [_text('Reason:', bold: true)]),
                Container(width: 440.px, child: _text(reason, bigFont: false)),
              ],
            )),
          ),
          SizedBox(height: 50.px),
          GestureDetector(
              onTap: () {
                logger.v('clicked the ban account dialog close');
                Navigator.pop(context);
              },
              child: YBDBlueGradientButton(title: 'OK', height: 65, width: 300, vDirection: true, reverse: true)),
          // SizedBox(height: 40.px),
        ]),
      ),
    );
  }
  void buildWw5w7oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _text(String text, {bool bold = false, bool bigFont = true}) {
    return Text(text,
        style: TextStyle(
          color: Colors.black.withOpacity(0.85),
          fontSize: bigFont ? 28.sp : 24.sp,
          fontWeight: bold ? FontWeight.bold : FontWeight.normal,
        ));
  }
  void _textN9wGtoyelive(String text, {bool bold = false, bool bigFont = true}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
