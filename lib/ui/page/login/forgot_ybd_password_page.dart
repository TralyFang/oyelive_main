import 'dart:async';

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/util/input_ybd_restrict.dart';
import 'package:oyelive_main/ui/page/login/util/login_ybd_page_util.dart';
import 'package:oyelive_main/ui/page/login/widget/dialog_ybd_graphic_code.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/constant/const.dart';
import '../../../common/http/http_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../common/util/valid_ybd_phone_util.dart';
import '../../../module/api.dart';
import '../../../module/entity/result_ybd_bean.dart';
import '../../../module/user/entity/mobile_ybd_check_resp_entity.dart';
import '../../widget/intl_input/intl_ybd_phone_field.dart';

class YBDForgotPasswordPage extends StatefulWidget {
  String? initCountry, phoneNumberComplete, phoneNumber;

  YBDForgotPasswordPage({this.initCountry, this.phoneNumberComplete, this.phoneNumber});

  @override
  YBDForgotPasswordPageState createState() => YBDForgotPasswordPageState();
}

class YBDForgotPasswordPageState extends BaseState<YBDForgotPasswordPage> {
  String? phoneNumberComplete;
  late bool isPhoneValid;

  bool isCodeSent = false;

  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _authCodeController = TextEditingController();
  TextEditingController _pwdController = TextEditingController();

  FocusNode authFocus = new FocusNode();
  FocusNode pwdFocus = new FocusNode();

  String? _selectCountryCode;

  var inputDecoration =
      BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))));

  getTitle() {
    return Container(
      height: ScreenUtil().setWidth(120),
      child: Row(
        children: [
          SizedBox(
            width: ScreenUtil().setWidth(24),
          ),
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
          ),
          Expanded(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    translate('forgot_password'),
                    style: TextStyle(fontSize: ScreenUtil().setSp(32), color: Colors.white),
                  ),
                  Text(
                    translate('enter_code'),
                    style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Color(0x99ffffff)),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(24),
          ),
          Icon(
            Icons.arrow_back_ios,
            color: Colors.transparent,
          ),
        ],
      ),
    );
  }

  inputPhoneNumber() {
    return Container(
      height: ScreenUtil().setWidth(92),
      width: ScreenUtil().setWidth(562),
      decoration: inputDecoration,
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
      child: YBDIntlPhoneField(
        decoration: InputDecoration(
          hintText: translate('phone_number'),
          hintStyle: TextStyle(fontSize: ScreenUtil().setSp(26), color: Color(0xff7D8080), fontWeight: FontWeight.w500),
          border: InputBorder.none,
        ),
        initialCountryCode: _selectCountryCode ?? 'PK',
        validator: (va) {
          logger.v(va);
          return va;
        },
        readOnly: false,
        keyboardType: TextInputType.number,
        controller: _phoneNumberController,
        onChanged: (phone) {
          phoneNumberComplete = phone.completeNumber.trim();

          isPhoneValid = YBDValidPhoneUtil.validatorPhone(phone.number);
          logger.v(phone.completeNumber);
        },
        onCountryChange: (selectedCountryName, selectedCountryCode, phone) {
          phoneNumberComplete = phone.completeNumber.trim();
          logger.v('on change country code : $phoneNumberComplete');
          _selectCountryCode = selectedCountryCode;
          isPhoneValid = YBDValidPhoneUtil.validatorPhone(phone.number);
        },
      ),
    );
  }

  //倒计时60s
  int secs = 60;
  Timer? _codeTimer;

  inputAuthCode() {
    return Container(
      height: ScreenUtil().setWidth(92),
      width: ScreenUtil().setWidth(562),
      decoration: inputDecoration,
      padding: EdgeInsets.only(left: ScreenUtil().setWidth(50), right: ScreenUtil().setWidth(20)),
      child: Row(
        children: [
          Expanded(
              child: TextField(
            controller: _authCodeController,
            focusNode: authFocus,
            style: TextStyle(color: Color(0xff333333), fontSize: ScreenUtil().setSp(28), fontWeight: FontWeight.w500),
            decoration: InputDecoration(
                hintStyle: TextStyle(
                  color: Color(0xff7D8080),
                ),
                hintText: translate('verification_code'),
                border: InputBorder.none),
          )),
          GestureDetector(
            onTap: onTapSend,
            child: Container(
              height: ScreenUtil().setWidth(60),
              width: ScreenUtil().setWidth(120),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(80))),
                  gradient: LinearGradient(
                      colors: [Color(0xff5237cc), Color(0xffc745f6)],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter)),
              alignment: Alignment.center,
              child: Text(
                translate(
                  secs == 60 ? 'send' : '${secs}s',
                ),
                style: TextStyle(fontSize: ScreenUtil().setSp(22), color: secs == 60 ? Colors.white : Colors.grey),
              ),
            ),
          )
        ],
      ),
    );
  }

  bool isObsecure = true;

  inputPwd() {
    return Container(
      height: ScreenUtil().setWidth(92),
      width: ScreenUtil().setWidth(562),
      decoration: inputDecoration,
      padding: EdgeInsets.only(left: ScreenUtil().setWidth(50)),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              style: TextStyle(color: Color(0xff333333), fontSize: ScreenUtil().setSp(28), fontWeight: FontWeight.w500),
              obscureText: isObsecure,
              focusNode: pwdFocus,
              inputFormatters: PWD_RESTRICT,
              decoration: InputDecoration(
                  hintStyle: const TextStyle(
                    color: Color(0xff7D8080),
                  ),
                  hintText: translate('password'),
                  border: InputBorder.none),
              controller: _pwdController,
            ),
          ),
          Container(
            width: ScreenUtil().setWidth(100),
            child: TextButton(
              style: TextButton.styleFrom(padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(34))),
                // padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(34)),
                onPressed: () {
                  setState(() {
                    isObsecure = !isObsecure;
                  });
                },
                child: Image.asset(
                  isObsecure ? "assets/images/show_pass.png" : "assets/images/hide_pass.png",
                  color: Color(0xff505050),
                  width: ScreenUtil().setWidth(38),
                )),
          )
        ],
      ),
    );
  }

  int? tokenId;

  Future onTapSend() async {
    if (secs == 60) {
      if (isPhoneValid) {
        showDialog<Null>(
            context: context, //BuildContext对象
            barrierDismissible: false,
            builder: (BuildContext _) {
              return YBDGraphicCodeDialog(
                phoneNumberComplete,
                YBDOperation.ResetPwd,
                onSuccess: (tokenId) async {
                  this.tokenId = tokenId;
                  _codeTimer = Timer.periodic(Duration(seconds: 1), (timer) {
                    if (secs != 0) {
                      --secs;
                    } else {
                      //复原倒计时
                      timer.cancel();
                      secs = 60;
                    }
                    setState(() {});
                  });

                  setState(() {
                    isCodeSent = true;
                  });
                },
              );
            });
      }
    }
  }
  void onTapSendEX9wjoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  onTapConfirm() async {
    if (!isPhoneValid) {
      YBDToastUtil.toast(translate('invalid_phone'));
      return;
    }
    if (_authCodeController.text.isEmpty) {
      YBDToastUtil.toast(translate('enter_code'));
      return;
    }
    if (!isCodeSent && tokenId != null) {
      //verification code not send

      YBDToastUtil.toast(translate('get_code'));
    } else {
      if (_pwdController.text.isEmpty) {
        YBDToastUtil.toast(translate('enter_new_password'));
        return;
      }

      showLockDialog();

      YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(
        context,
        YBDApi.RESET_PWD,
        params: {'tokenId': tokenId, 'token': _authCodeController.text.trim(), 'password': _pwdController.text},
      );
      dismissLockDialog();
      YBDToastUtil.toast(resultBean?.returnMsg);
      if (resultBean?.returnCode == Const.HTTP_SUCCESS) {
        Navigator.pop(context);
      }
    }
  }

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color(0xff5C4AB0), Color(0xff130855)], begin: Alignment.topCenter, end: Alignment.bottomCenter),
        ),
        child: Column(
          children: [
            getTitle(),
            SizedBox(
              height: ScreenUtil().setWidth(100),
            ),
            Image.asset(
              'assets/images/logo.webp',
              width: ScreenUtil().setWidth(210),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(40),
            ),
            inputPhoneNumber(),
            SizedBox(
              height: ScreenUtil().setWidth(20),
            ),
            inputAuthCode(),
            SizedBox(
              height: ScreenUtil().setWidth(20),
            ),
            isCodeSent ? inputPwd() : Container(),
            isCodeSent
                ? SizedBox(
                    height: ScreenUtil().setWidth(20),
                  )
                : Container(),
            GestureDetector(
              onTap: onTapConfirm,
              child: Container(
                width: ScreenUtil().setWidth(230),
                height: ScreenUtil().setWidth(80),
                alignment: Alignment.center,
                decoration: const BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage(
                    'assets/images/btn_background.webp',
                  ),
                )),
                child: Text(
                  translate('ok'),
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: ScreenUtil().setSp(25), color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildAjQgRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    _selectCountryCode = widget.initCountry;
    phoneNumberComplete = widget.phoneNumberComplete;
    _phoneNumberController.text = widget.phoneNumber!;
    isPhoneValid = YBDValidPhoneUtil.validatorPhone(widget.phoneNumber);
  }
  void initState8Al6Koyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    if (_authCodeController.text.isEmpty) {
      // 没有输入验证码就退出了，暂判定为没有收到验证码，上报异常
      YBDLoginPageUtil.smsPostExceptionToAnalytics();
    }
    YBDLoginPageUtil.smsPostExceptionClean();

    super.dispose();
    if (_codeTimer != null) {
      _codeTimer!.cancel();
    }
  }
  void disposeLBcTUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDForgotPasswordPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
