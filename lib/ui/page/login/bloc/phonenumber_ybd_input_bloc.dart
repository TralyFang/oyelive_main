import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/login/util/login_ybd_page_util.dart';
import 'package:oyelive_main/ui/page/login/widget/ban_ybd_account_dialog.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/http/http_ybd_util.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../common/util/unique_ybd_id_util.dart';
import '../../../../module/api.dart';
import '../../../../module/user/entity/login_ybd_resp_entity.dart';
import '../../../../module/user/entity/mobile_ybd_check_resp_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../../redux/user_ybd_redux.dart';

enum InputStep {
  CodeNotSend,
  GetImageCode,
  AuthCodeLogin,
  PwdLogin,
}

/// 是否显示next按钮
bool isNextStep(InputStep step) {
  // 显示next按钮
  return step == InputStep.CodeNotSend || step == InputStep.GetImageCode;
}

class YBDPhoneNumberInputBloc extends Bloc<InputStep, InputStep> {
  YBDPhoneNumberInputBloc() : super(InputStep.CodeNotSend);

  @override
  Stream<InputStep> mapEventToState(InputStep event) async* {
    yield event;
  }
  void mapEventToStatevMPOPoyelive(InputStep event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  int? tokenId;

  setTokenId(tokenId) {
    this.tokenId = tokenId;
  }

  bool _needGraphicCode = false;

  bool get needGraphicCode => _needGraphicCode;

  set needGraphicCode(bool value) {
    _needGraphicCode = value;
  }

  Future<bool> mobileLogin(
    BuildContext context,
    String phone, {
    bool? showLoading,
  }) async {
    logger.v(Uri.encodeComponent(phone));
    YBDMobileCheckResp? data = await YBDHttpUtil.getInstance().doGet<YBDMobileCheckResp>(
      context,
      YBDApi.VALID_PHONE,
      params: {
        "phone": phone,
        "deviceId": (await getUniqueID(
          context,
          UniqueIDType.NORMAL_LOGIN,
        ))
      },
      showLoading: showLoading,
    );

    if (data != null) {
      if (data.returnCode == "902068") {
        add(InputStep.PwdLogin);
        return false;
      } else if (data.returnCode == "902057") {
        tokenId = data.record?.tokenId;
        if (tokenId == null) {
          YBDToastUtil.toast(data.returnMsg ?? translate('login_failed'));
          return false;
        }
        logger.v("tokenId : $tokenId");
        _needGraphicCode = false;
        add(InputStep.AuthCodeLogin);
        return true;
      } else if (data.returnCode == "00211") {
        // tokenId = data?.record?.tokenId;
        // if (tokenId == null) {
        //   YBDToastUtil.toast(data?.returnMsg ?? translate('login_failed'));
        //   return false;
        // }
        Navigator.pop(context);
        logger.v("tokenId : $tokenId");
        _needGraphicCode = true;
        add(InputStep.CodeNotSend);
        add(InputStep.GetImageCode);
        return false;
      } else {
        YBDToastUtil.toast(data.returnMsg);
        return false;
      }
    } else {
      logger.v('check mobile return null');
    }
    return false;
  }

  Future<bool> loginByAuthCode(
    BuildContext context,
    String token, {
    bool? showLoading,
  }) async {
    YBDLoginResp? result = await YBDHttpUtil.getInstance().doPost<YBDLoginResp>(
      context,
      YBDApi.CHECK_AUTH_CODE,
      {'tokenId': tokenId, 'token': token, 'preAuth': true},
      showLoading: showLoading,
    );

    if (result != null && result.returnCode != Const.HTTP_SUCCESS) {
      logger.v('login by auth code failed');

      if (null != result.returnCode && result.returnCode == '902067') {
        // 无效验证码
        YBDToastUtil.toast(result.returnMsg);
      } else {
        YBDToastUtil.toast(translate('failed'));
      }
      return false;
    } else {
      logger.v('login by auth code success');
      return true;
    }
  }

  //-1 失败 1成功 2需要修改
  Future<int> loginByPassWord(
    context,
    String? phone,
    String password, {
    bool? showLoading,
  }) async {
    YBDLoginResp? data = await YBDHttpUtil.getInstance().doGet<YBDLoginResp>(
      context,
      YBDApi.VALID_PHONE,
      params: {"phone": phone, "password": password},
      showLoading: showLoading,
    );
    if (data == null || (data.returnCode != Const.HTTP_SUCCESS && data.returnCode != Const.PWD_NEED_FIX)) {
      if ((data?.returnCode == Const.BAN_ACCOUNT || data?.returnCode == Const.BAN_DEVICE) && (data?.returnMsg?.isNotEmpty ?? false)) {
        YBDLoginPageUtil.banAccountDialog(context, data?.returnMsg ?? '');
      } else if (data != null) YBDToastUtil.toast(data.returnMsg);

      return -1;
    } else {
      if (data.returnCode != Const.PWD_NEED_FIX) YBDToastUtil.toast(data.returnMsg);
      Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;

      store.dispatch(YBDUpdateUserAction(data.record!.userInfo));
      await YBDSPUtil.save(Const.SP_USER_INFO, data.record!.userInfo);
      return data.returnCode == Const.PWD_NEED_FIX ? 2 : 1;
    }
  }
}
