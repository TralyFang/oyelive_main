import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:imei_plugin/imei_plugin.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api.dart';
import 'package:oyelive_main/module/user/entity/login_ybd_resp_entity.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/login/handler/login_ybd_handler.dart';
import 'package:oyelive_main/ui/page/login/widget/ban_ybd_account_dialog.dart';

import '../../../../common/util/log_ybd_util.dart';

/// 登录页面工具类
class YBDLoginPageUtil {
  /// 输出设备信息到日志文件
  static printDeviceInfo() async {
    if (Platform.isAndroid) {
      var androidInfo = await DeviceInfoPlugin().androidInfo;
      var release = androidInfo.version.release;
      var sdkInt = androidInfo.version.sdkInt;
      var manufacturer = androidInfo.manufacturer;
      var model = androidInfo.model;
      logger.i('Android $release (SDK $sdkInt), $manufacturer $model');
    }

    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      var systemName = iosInfo.systemName;
      var version = iosInfo.systemVersion;
      var name = iosInfo.name;
      var model = iosInfo.model;
      logger.i('$systemName $version, $name $model');
    }
  }

  /// 请求手机号注册新用户时编辑信息的接口
  static requestProcessToken({BuildContext? context, String? token, String? tokenId}) async {
    // 网络请求时的加载框
    String platformImei = '';
    // TODO: 修复v2.6.4提审包闪退的问题，先删掉获取imei的插件
    // try {
    //   platformImei = await ImeiPlugin.getImei(shouldShowRequestPermissionRationale: true);
    // } on PlatformException {
    //   platformImei = 'Failed to get platform version.';
    // }

    String ipCountry = await YBDCommonUtil.getIpCountryCode(Get.context);
    logger.v('requestProcessToken ip country: $ipCountry');
    TA.timeEvent(YBDTATrackEvent.generic_loading_time); // 注册时长埋点
    YBDLoginResp? data = await YBDHttpUtil.getInstance().doPost<YBDLoginResp>(
      context,
      YBDApi.PROCESS_TOKEN,
      {
        'token': token,
        'tokenId': tokenId,
        'imei': platformImei,
        'country': ipCountry,
        // 'distinctId': TA.distinctId,
        'distinctId': await TA.ta?.getDistinctId(),
      },
    );

    if (null != data && data.returnCode == Const.HTTP_SUCCESS) {
      try {
        Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
        store.dispatch(YBDUpdateUserAction(data.record!.userInfo));
        YBDSPUtil.save(Const.SP_USER_INFO, data.record!.userInfo);
      } catch (e) {
        logger.e('update user store error : $e');
      }

      // 通过手机号+验证码登录，认定为新用户注册
      YBDAnalyticsUtil.logAFEvent(YBDEventName.AF_COMPLETE_REGISTRATION, {'af_registration_method': 'phone_number'});
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.COMPLETE_REGISTRATION, method: 'phone_number'));
      YBDCommonTrack().regTrack(regType: 'phone');
      // 网络请求加载框
      await onLoginSuccess(context, true, processToken: true, userId: data.record?.userInfo?.id);
      YBDCommonTrack().loginTrack(firstLogin: true, loginWay: 'phone');
      logger.i('begin new_user_recommended');
      bool? showRecommendPage = await YBDSPUtil.getBool(Const.SP_SHOW_RECOMMEND_PAGE);
      // 跳转推荐页面
      YBDNavigatorHelper.navigateTo(
        context,
        (showRecommendPage ?? true) ? YBDNavigatorHelper.recommended : YBDNavigatorHelper.index_page,
        forceFluro: true,
        clearStack: true,
      );
    } else {
      logger.e('request process token failed');
      YBDToastUtil.toast(data?.returnMsg);
    }

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.LOGIN_EVENT,
      method: 'phone_number',
      location: 'phone_login_page',
      returnExtra: 'phone_number: ' + (data?.returnMsg ?? ''),
    ));
  }

  /// 短信图形验证码tokenId，异常上报
  static String? smsPostExceptionTokenId;
  static smsPostExceptionToAnalytics() {
    logger.i('smsPostExceptionToAnalytics: $smsPostExceptionTokenId');
    if (smsPostExceptionTokenId != null) {
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.SMS_POST_EXCEPTION,
        itemName: 'item:' + YBDEventName.SMS_POST_EXCEPTION,
        returnExtra: maxParamValue(smsPostExceptionTokenId),
      ));
      YBDTATrack().track(YBDEventName.SMS_POST_EXCEPTION, properties: YBDTAProps(id: smsPostExceptionTokenId).toJson());
      smsPostExceptionClean();
    }
  }

  /// 清空
  static smsPostExceptionClean() {
    smsPostExceptionTokenId = null;
  }

  /// 记录
  static smsPostExceptionRecord(String tokenId) {
    smsPostExceptionTokenId = tokenId;
  }

  // 封号弹窗
  static void banAccountDialog(BuildContext context, String msg) {
    Map map, extend;
    tryCatch(() {
      map = jsonDecode(msg);
      extend = jsonDecode(map['extend']);
      logger.v('22.11.25---msg:$msg---map:$map---extend:${map['extend']}');
      showDialog(
        context: context,
        builder: (BuildContext builder) {
          return YBDBanAccountDialog(
            title: map['title'] ?? '',
            description: map['content'] ?? '',
            endTime: extend['endTime'] ?? '',
            reason: extend['reason'] ?? '',
          );
        },
      );
    });
  }
}
