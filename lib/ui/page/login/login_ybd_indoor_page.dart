import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../widget/scaffold/nav_ybd_back_button.dart';
import 'widget/input_ybd_content.dart';
import 'widget/phone_ybd_number_input.dart';
import 'widget/user_ybd_name_input.dart';

class YBDLoginIndoorPage extends StatefulWidget {
  //是否显示手机短信登录
  final bool showSMS;

  YBDLoginIndoorPage(this.showSMS);

  @override
  YBDLoginIndoorPageState createState() => YBDLoginIndoorPageState();
}

class YBDLoginIndoorPageState extends BaseState<YBDLoginIndoorPage> {
  @override
  Widget myBuild(BuildContext context) {
    TextStyle tabTextStyle = TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w500,
      height: 1,
      fontSize: ScreenUtil().setSp(28),
    );

    return !widget.showSMS
        ? Scaffold(
//            resizeToAvoidBottomInset: true,
            resizeToAvoidBottomInset: false,
            body: Container(
//              padding: MediaQuery.of(context).viewInsets,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xff6C6CDF),
                    Color(0xff6C6CDF),
                    Color(0xff3D0B8D)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: DefaultTabController(
                length: 2,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: ScreenUtil().statusBarHeight,
                    ),
                    Container(
                      // 导航栏部分
                      height: ScreenUtil().setWidth(100),
                      width: ScreenUtil().screenWidth,
                      child: Row(children: [
                        Stack(
                          children: <Widget>[
                            Positioned(
                              left: 0,
                              child: Container(
                                width: ScreenUtil().setWidth(100),
                                height: ScreenUtil().setWidth(100),
                                child: TextButton(
                                  onPressed: () {
                                    logger.v('clicked back button');
                                    Navigator.pop(context);
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                height: ScreenUtil().setWidth(100),
                                width: ScreenUtil().screenWidth,
                                padding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(100),
                                    right: ScreenUtil().setWidth(10)),
                                child: TabBar(
                                  indicator: UnderlineTabIndicator(
                                    borderSide: BorderSide(
                                      width: ScreenUtil().setWidth(4),
                                      color: Color(0xff63FFDF),
                                    ),
                                    insets: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(120),
                                      vertical: ScreenUtil().setWidth(6),
                                    ),
                                  ),
                                  onTap: (int index) {
                                    logger.v('clicked tab index : $index');
                                    FocusScope.of(context)
                                        .requestFocus((FocusNode()));
                                  },
                                  tabs: [
                                    Tab(
                                      child: Text(translate("phone_number"),
                                          style: tabTextStyle),
                                    ),
                                    Tab(
                                      child: Text(translate("username"),
                                          style: tabTextStyle),
                                    ),
                                  ],
                                  labelColor: Colors.white,
                                  unselectedLabelColor:
                                      Colors.white.withOpacity(0.1),
                                  labelStyle: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    height: 1.5,
                                    fontSize: ScreenUtil().setSp(32),
                                  ),
                                  unselectedLabelStyle: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    height: 1.5,
                                    fontSize: ScreenUtil().setSp(28),
                                  ),
                                  // labelPadding: EdgeInsets.only(top: 60),
                                ),
                              ),
                            )
                          ],
                        ),
                      ]),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          logger.v('clicked hide keyboard');
                          FocusScope.of(context).unfocus();
                        },
                        child: TabBarView(
                          children: <Widget>[
                            YBDInputContent(YBDPhoneNumberInput()),
                            YBDInputContent(YBDUserNameInput()),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        : Scaffold(
            appBar: AppBar(
              title: Text(translate("username"),
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    height: 1.6,
                    fontSize: ScreenUtil().setSp(34),
                  )),
              backgroundColor: Color(0xff6C6CDF),
              leading: YBDNavBackButton(color: Colors.white),
              elevation: 0,
              centerTitle: true,
            ),
            resizeToAvoidBottomInset: false,
            body: Container(
              padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xff6C6CDF),
                    Color(0xff6C6CDF),
                    Color(0xff3D0B8D)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: GestureDetector(
                onTap: () {
                  logger.v('clicked hide keyboard');
                  FocusScope.of(context).unfocus();
                },
                child: YBDInputContent(YBDUserNameInput()),
              ),
            ),
          );
  }

  void myBuildbjAWEoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void initState() {
    super.initState();
  }

  void initStaterjbhroyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void dispose() {
    super.dispose();
  }
}
