import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/entity/zego_ybd_code_entity.dart';

import 'sudmpg_ybd_helper.dart';

class YBDSudMpgPage extends StatefulWidget {
  YBDSudMpgPage({Key? key, this.title}) : super(key: key);
  final String? title;
  final MethodChannel channel = const MethodChannel('zego_sudmpg_plugin/gameView');

  @override
  _YBDSudMpgPageState createState() => _YBDSudMpgPageState();
}

class _YBDSudMpgPageState extends State<YBDSudMpgPage> {
  YBDZegoCodeEntity? _zegoCodeEntity;
  Widget getPlatformTextView() {

    logger.v("getPlatformTextViewgetPlatformTextView");

    if (_zegoCodeEntity?.code == null || _zegoCodeEntity?.data?.code == null) {
      getZegoCode();
      return Container();
    }
    if (defaultTargetPlatform == TargetPlatform.android) {
      ///https://www.jianshu.com/p/126def13d5fa
      return AndroidView(
          onPlatformViewCreated: (id) async {
            // MethodChannel _channel = const MethodChannel('ace_method_text_view');
            // ignore: missing_return
            widget.channel
              ..invokeMethod(YBDSudMgpHelper.METHOD_INIT, _zegoCodeEntity?.data?.code)
              ..setMethodCallHandler((call) {
                if (call.method == 'method_click') {
                  print('Method Text FlutterToast!');
                } else if (call.method == YBDSudMgpHelper.METHOD_ONPLAYERSTATER) {
                  print('Method Text FlutterToast!1111111: ${call.arguments}');
                }
                return Future.value(true);
              });
          },
          viewType: "zego_sudmpg_plugin/gameView",
          creationParams: <String, dynamic>{"text": "Android Text View"},
          creationParamsCodec: const StandardMessageCodec());
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      return UiKitView(
          viewType: "zego_sudmpg_plugin/gameView",
          creationParams: <String, dynamic>{"text": "iOS Label"},
          creationParamsCodec: const StandardMessageCodec());
    } else {
      return Text("Not supported");
    }
  }
  void getPlatformTextViewqMWACoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      child: getPlatformTextView(),
    );
  }
  void buildu2nwEoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getZegoCode() async {
    _zegoCodeEntity = await ApiHelper.getZegoCode(context);
    if (_zegoCodeEntity?.code == Const.HTTP_SUCCESS_NEW && _zegoCodeEntity?.data?.code != null) {
      setState(() {});
    } else {
      logger.v('getZegoCode error code->${_zegoCodeEntity?.code} data.code->${_zegoCodeEntity?.data?.code}');
    }
  }
}
