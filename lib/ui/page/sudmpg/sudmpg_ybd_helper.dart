import 'dart:async';

import 'package:flutter/services.dart';

class YBDSudMgpHelper {
  /****************Native层->flutter层******************/
  /***玩家状态**/
  static const String PLAER_INIT = 'init'; //初始化
  static const String PLAER_ADD_GAME = 'addGame'; //加入游戏
  static const String PLAER_QUIT_GAME = 'quitGame'; //退出游戏
  static const String PLAER_READY = 'ready'; //准备
  static const String PLAER_UN_READY = 'unReady'; //取消准备
  static const String PLAER_PLAYING = 'playing'; //游戏中
  static const String PLAER_PLAY_END = 'PlayEnd'; //游戏结束
  static const String PLAER_NETWORK_ERROR = 'NetworkError'; //网络错误
  static const String PLAER_OTHER = 'other'; //其他
  static const String METHOD_ONPLAYERSTATER = 'onPlayerState'; //方法名

  /***游戏状态***/
  static const String GAME_CREATE = 'create'; //游戏创建（队长创建）
  static const String GAME_READY = 'ready'; //游戏准备
  static const String GAME_CANCEL = 'cancel'; //游戏取消
  static const String GAME_PLAYING = 'playing'; //游戏中
  static const String GAME_PLAY_END = 'playEnd'; //游戏结束
  static const String GAME_OTHER = 'other'; //其它
  static const String METHOD_ONGAMESTATE = 'onGameState'; //方法名

  /**公屏消息***/
  static const String PUBLISH_MESSAGE = 'publishMessage';
  static const String METHOD_ONPUBLISHMESSAGE = 'onPublicMessage'; //方法名

  /**角色**/
  static const String PLAER_ROLE_LEADER = 'leader'; //队长（默认房主）
  static const String PLAER_ROLE_PLAYER = 'player'; //玩家
  static const String PLAER_ROLE_AUDIENCE = 'audience'; //观众
  static const String METHOD_ONPLAYERROLER = 'onPlayerRole'; //方法名

  /****************flutter层->Native层******************/
  /***玩家进入游戏**/
  static const String INIT = 'init'; //初始化 传输必要的初始化数据（例如token、userID、userName）
  static const String METHOD_INIT = 'onInit'; //方法名

  /***房间状态（根据状态控制游戏正常进行、暂停、销毁、结束）**/
  static const String ROOM_STATE_RESUMED = 'resumed'; //加入游戏
  static const String ROOM_STATE_PAUSED = 'paused'; //暂停（只是该玩家暂停）
  static const String ROOM_STATE_STOPPED = 'stopped'; //销毁游戏（例如回到桌面，如果回来重新加载游戏）
  static const String METHOD_ROOM_STATE = 'onRoomState'; //方法名

  /***用户金币**/
  static const String GOLD = 'gold'; //用户金币（玩家加入游戏的时候检验gold）
  static const String METHOD_GOLD = 'onGold'; //方法名

  ///初始化 传输必要的初始化数据（例如Code、userID、userName）
  static Future<void> onInit(MethodChannel _channel, List<dynamic> programs) async {
    if (_channel != null) {
      _channel.invokeMethod(METHOD_INIT, programs ?? '');
    }
  }

  ///房间状态（根据状态控制游戏正常进行、暂停、销毁、结束）
  static Future<void> onRoomState(MethodChannel _channel, String roomState) async {
    if (_channel != null) {
      _channel.invokeMethod(METHOD_ROOM_STATE, roomState ?? '');
    }
  }

  ///用户金币
  static Future<void> onGold(MethodChannel _channel, String gold) async {
    if (_channel != null) {
      _channel.invokeMethod(METHOD_GOLD, gold ?? '');
    }
  }
}
