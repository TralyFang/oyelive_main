import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/util/certification_ybd_util.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/common/util/connectivity_ybd_util.dart';
import 'package:oyelive_main/common/util/referrer_ybd_util.dart';
import 'package:oyelive_main/common/util/uniqueid_ybd_helper.dart';
import 'package:oyelive_main/module/entity/query_ybd_configs_resp_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_file.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/constant/const.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/dialog_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import 'splash_ybd_page_bloc.dart';
import 'splash_ybd_util.dart';

class YBDSplashPage extends StatefulWidget {
  @override
  YBDSplashPageState createState() => new YBDSplashPageState();
}

class YBDSplashPageState extends BaseState<YBDSplashPage> with SingleTickerProviderStateMixin {
  static const TAG = "YBDSplashPage";

  @override
  void initState() {
    super.initState();
    logger.v('YBDSplashPage initState');

    // 日志弹框
    YBDLogUtil.popLogViewOnShake(context);

    // 埋点
    YBDSplashUtil.analyzeOpenSplashPage();

    // 配置数据库
    YBDSplashUtil.configDatabase();

    // 检查手机网络
    YBDSplashUtil.checkNetWork();

    // 申请隐私权限
    // YBDSplashUtil.requestPermission();

    // 查询IP所在国家
    YBDSPUtil.remove(Const.SP_IP_COUNTRY_CODE);
    YBDCommonUtil.getIpCountryCode(context);

    YBDConnectivityUtil.getInstance()!.initConnectivity();
    YBDConnectivityUtil.getInstance()!.addListen();

    YBDUniqueIdHelper.getInstance()!.getUniqId();
    YBDCertificationUtil.getInstance()!.init();

    referrer();
  }
  void initStateJNUBGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<YBDSplashPageBloc>(create: (context) {
          logger.v('create splash page bloc');
          YBDSplashPageBloc bloc = YBDSplashPageBloc(context);
          bloc.add(SplashEvent.CheckBlockedDevice);
          return bloc;
        }),
      ],
      child: BlocListener<YBDSplashPageBloc, YBDSplashBlocState>(
        listener: (context, state) async {
          if (state.shouldShowDialog) {
            if (null != state.deviceUniqueID && state.deviceUniqueID!.blocked!) {
              logger.v('device is blocked');
              YBDDialogUtil.showDeviceBlock(context, state.deviceUniqueID!.content);
            } else {
              context.read<YBDSplashPageBloc>().add(SplashEvent.GetAppConfig);
            }
          }

          if (null == state.configsEntity) {
            logger.v('config entity is null');
            if (state.requestedConfigs) {
              context.read<YBDSplashPageBloc>().add(SplashEvent.RetryGetAppConfig);
            }
          } else {
            logger.v('config success');
            _configAppSuccess();
            logger.v('state.configsEntity skinConfig: ${state.configsEntity!.record!.skinConfig}');
            _setSkinConfig(state.configsEntity!);
          }
        },
        child: Scaffold(
            body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/images/logo.webp',
                  width: ScreenUtil().setWidth(140), height: ScreenUtil().setWidth(140), fit: BoxFit.fill),
              SizedBox(height: ScreenUtil().setWidth(22)),
              Image.asset('assets/images/splash_name.png', width: ScreenUtil().setWidth(270), fit: BoxFit.fitWidth),
              SizedBox(height: ScreenUtil().setWidth(200))
            ],
          ),
        )),
      ),
    );
  }
  void myBuild7lEZDoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 设置皮肤配置项
  void _setSkinConfig(YBDQueryConfigsRespEntity configsEntity) {
    // logger.v('skin config: ${ConfigUtil.skinConfig().toJson()}');
    YBDActivityFile.instance!.setConfigEntity(ConfigUtil.skinConfig(configsEntity)!);
    YBDActivityFile.instance!.setCanShowSkin();
  }
  void _setSkinConfig4RdDwoyelive(YBDQueryConfigsRespEntity configsEntity) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取配置信息成功后跳转页面
  _configAppSuccess() async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
    if (null != userInfo && null == await YBDSPUtil.get(Const.SP_NEED_FIX_PWD)) {
      logger.v('has user info, im login');
      // YBDInboxApiHelper.imLogin(context, needErrorToast: false, timeout: 10 * 1000);
      logger.v('complete im login');
      YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.index_page, replace: true, forceFluro: true);
      TA.login(accountId: userInfo.id.toString());
      logger.v('jumped to index page');
      // getDeviceUniqueIDs(context, UniqueIDType.NORMAL_LOGIN);
    } else {
      logger.v('jump to login page');
      YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.login, clearStack: true, forceFluro: true);
    }
  }
}
