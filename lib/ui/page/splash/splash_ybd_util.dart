import 'dart:async';


import 'package:connectivity/connectivity.dart';
import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';

import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/constant/const.dart';
import '../../../common/util/config_ybd_util.dart';
import '../../../common/util/database_ybd_until.dart';
import '../../../common/util/dialog_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/entity/query_ybd_configs_resp_entity.dart';
import '../../../redux/app_ybd_state.dart';
import '../../../redux/configs_ybd_redux.dart';
import '../../../redux/locale_ybd_redux.dart';
import '../status/config/post_ybd_config.dart';

/// 启动页面工具类
class YBDSplashUtil {
  /// 埋点: 打开启动页
  static analyzeOpenSplashPage() {
    YBDSPUtil.getUserId().then((value) {
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE_SPLASH, loggedIn: value != null));
    });
  }

  /// 配置数据库
  static configDatabase() async {
    logger.v('init data base');
    await YBDDataBaseUtil.getInstance().initDataBase();
  }

  /// 检查手机网络，手机无网 toast 提示
  static checkNetWork() async {
    var connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.none) {
      YBDToastUtil.toast(translate("no_network"));
    } else {
      logger.v("network is available");
    }
  }

  /// 检查权限
  static requestPermission() async {
    try {
      [
        Permission.location,
        Permission.storage,
      ].request().then((value) {
        logger.v('request permission done : $value');
        YBDCommonTrack().permissionTrack(value);
      });
    } catch (e) {
      logger.v('request permission error $e');
    }
  }

  /// 检查存储权限
  static Future<bool> requestStoragePermission() async {
    try {
      dynamic value = await [
        Permission.storage,
      ].request();
      logger.v('request permission done : $value');
      if (await Permission.storage.isGranted) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      logger.v('request permission error $e');
      return false;
    }
  }

  /// 检查麦克风权限
  static Future<bool> requestMicrophonePermission() async {
    try {
      dynamic sds = await [
        Permission.microphone,
      ].request();
      logger.d('request requestMicrophonePermission sds :$sds');
      logger.d(
          'request requestMicrophonePermission Permission.microphone.isGranted :${await Permission.microphone.isGranted}');
      if (await Permission.microphone.isGranted) {
        return true;
      } else {
        return false;
      }

      [
        Permission.microphone,
      ].request().then((value) async {
        logger.d('request requestMicrophonePermission done : $value');
        if (value.containsKey("Permission.microphone")) {
          logger.d('request requestMicrophonePermission done containsKey("Permission.microphone")');
        }
        return true;
      });
    } catch (e) {
      logger.d('request requestMicrophonePermission error $e');
      return false;
    }
    logger.d('request requestMicrophonePermission done :--------');
    // return false;
  }

  /// 从网络同步最新的配置信息到本地文件并更新 store
  static void refreshAppConfig(BuildContext? context, Store<YBDAppState>? store) {
    // 刷新本地配置信息
    ApiHelper.queryConfigs(context).then((queryConfigsResp) {
      if (queryConfigsResp?.returnCode == Const.HTTP_SUCCESS) {
        // 从 response 获取配置项
        YBDConfigInfo configInfo = YBDSplashUtil.addUniqueIdsFromStore(queryConfigsResp!, store)!;

        // 刷新 store 里的配置信息
        refreshStoreWithConfig(store!, configInfo);

        // 把配置信息保存到本地 json 文件
        ConfigUtil.writeConfig(queryConfigsResp);

        //设置Slog配置
        YBDSLogConfig.setConfig(queryConfigsResp.record?.slogConfig ?? '');
      } else {
        logger.v('async request app configure info failed');
      }
    });
  }

  /// 从接口获取配置项后添加 store 里的靓号配置
  static YBDConfigInfo? addUniqueIdsFromStore(YBDQueryConfigsRespEntity resp, Store<YBDAppState>? store) {
    YBDConfigInfo? configInfo = resp.record;

    if (store != null) {
      configInfo!.uniqueIds = store.state.configs?.uniqueIds;
    }

    return configInfo;
  }

  /// 刷新 store 里面的配置项
  static void refreshStoreWithConfig(Store<YBDAppState> store, YBDConfigInfo configInfo) {
    assert(null != store, 'store is null');
    assert(null != configInfo, 'config info is null');

    // 观察刷新 store 异常
    try {
      logger.v('refresh config with store : $store, config info : ${configInfo.appAndroid}');
      store.dispatch(YBDUpdateConfigsAction(configInfo));
    } catch (e) {
      logger.v('refresh config error : $e');
    }
  }

  /// 设置 app 语言
  /// true: 已切换成 Hindi 语；false：没有切换
  static Future<bool> configAppLang(BuildContext context) async {
    String? spLocale = await YBDSPUtil.get(Const.SP_LOCALE);
    String? deviceLocale;

    try {
      deviceLocale = (await Devicelocale.currentLocale).split('_')[0];
    } catch (e) {
      YBDLogUtil.e('get current local error ');
    }

    if (spLocale == null && deviceLocale == Const.EXT_LANG) {
      // 新用户直接设置为 hindi 语
      logger.v('locale debug --- locale is null and device locale is hi');
      _changeAppLanguage(context, Const.EXT_LANG);

      // 新用户切换语言后，不需要提示
      YBDSPUtil.save(Const.SP_HINDI_TIP, true);
      return true;
    } else if (spLocale != Const.EXT_LANG && deviceLocale == Const.EXT_LANG) {
      // 提醒用户当前已经支持 Hindi，是否切换
      logger.v('locale debug --- locale is not hi and device locale is hi');
      bool? hindiTip = await YBDSPUtil.getBool(Const.SP_HINDI_TIP);
      logger.v('locale debug --- hindiTip : $hindiTip');

      if (hindiTip == null || hindiTip == false) {
        YBDSPUtil.save(Const.SP_HINDI_TIP, true);

        // 弹框让用户选择是否切换到预设置语言
        bool? changeLang = await YBDDialogUtil.showChangeToHindiDialog(context);

        if (null == changeLang) {
          return false;
        } else {
          _changeAppLanguage(context, Const.EXT_LANG);
          return true;
        }
      } else {
        return false;
      }
    } else {
      logger.v('locale debug --- else');
      return false;
    }
  }

  /// 更新 store 里的配置信息
  /// 用 [refreshStoreWithConfig] 方法
  // static updateStoreWithConfig(BuildContext context, YBDQueryConfigsRespEntity queryConfigsResp) {
  //   if (null != queryConfigsResp) {
  //     // 保存到 redux
  //     final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context);
  //     YBDConfigInfo configInfo = queryConfigsResp.record ?? YBDConfigInfo.initial();
  //     configInfo.uniqueIds = store.state.configs?.uniqueIds;
  //     store.dispatch(YBDUpdateConfigsAction(configInfo));
  //   }
  // }

  /// 切换 app 语言
  static _changeAppLanguage(BuildContext context, String lang) {
    Store<YBDAppState> store = StoreProvider.of<YBDAppState>(context);
    store.dispatch(YBDUpdateLocaleAction(localeFromString(Const.EXT_LANG)));
    YBDSPUtil.save(Const.SP_LOCALE, Const.EXT_LANG);
  }
}
