import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import '../../../common/constant/const.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/config_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/unique_ybd_id_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/entity/device_ybd_unique_id.dart';
import '../../../module/entity/query_ybd_configs_resp_entity.dart';
import 'splash_ybd_util.dart';

enum SplashEvent {
  // 初始化 app
  InitAppData,
  // 检查封设备
  CheckBlockedDevice,
  // 获取配置信息
  GetAppConfig,
  // 重试请求配置信息
  RetryGetAppConfig,
}

/// 启动页 bloc
class YBDSplashPageBloc extends Bloc<SplashEvent, YBDSplashBlocState> {
  BuildContext context;

  /// 封设备信息
  YBDDeviceUniqueID? _deviceUniqueID;

  /// app 配置信息
  YBDQueryConfigsRespEntity? _configsEntity;

  /// 尝试重新获取配置信息的次数
  int _retryTimes = 0;

  YBDSplashPageBloc(this.context) : super(YBDSplashBlocState());

  @override
  Stream<YBDSplashBlocState> mapEventToState(SplashEvent event) async* {
    switch (event) {
      case SplashEvent.CheckBlockedDevice:
        // await _getDeviceUniqueID();
        await YBDSplashUtil.requestPermission();
        yield YBDSplashBlocState(
          deviceUniqueID: _deviceUniqueID,
          configsEntity: _configsEntity,
          shouldShowDialog: true,
        );
        break;
      case SplashEvent.GetAppConfig:
        ApiHelper.carList(context, queryAll: true);
        ApiHelper.giftList(context);
        await _getAppConfigure();
        yield YBDSplashBlocState(
          deviceUniqueID: _deviceUniqueID,
          configsEntity: _configsEntity,
          requestedConfigs: true,
        );
        break;
      case SplashEvent.RetryGetAppConfig:
        // 间隔一段时间后尝试重新获取配置信息
        await Future.delayed(Duration(seconds: ++_retryTimes), () {});
        logger.v('retry get app configure after : $_retryTimes seconds');
        await _getAppConfigure();
        yield YBDSplashBlocState(
          deviceUniqueID: _deviceUniqueID,
          configsEntity: _configsEntity,
          requestedConfigs: true,
        );
        break;
    }
  }
  void mapEventToStatew1wMioyelive(SplashEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 激活的时候检查设备是否被封
  _getDeviceUniqueID() async {
    logger.v('_getDeviceUniqueID');
    // 10 秒超时
    YBDDeviceUniqueID? deviceUniqueID = await getDeviceUniqueIDs(context, UniqueIDType.ACTIVATION, timeout: 10 * 1000);
    _deviceUniqueID = deviceUniqueID;
  }

  /// 获取 app 配置信息
  _getAppConfigure() async {
    // 已启动过 app 的用户从本地文件获取配置信息
    YBDQueryConfigsRespEntity? localConfigEntity = await ConfigUtil.readConfig();
    // 第一次安装的用户从资源目录里获取
    if (null == localConfigEntity) {
      logger.v('local config file is null');
      TA.trackFirstEvent();
      localConfigEntity = await ConfigUtil.readConfigFromAssets();
    }

    // 获取 store
    final store = YBDCommonUtil.storeFromContext(context: context);

    if (null == localConfigEntity) {
      logger.v("request app configure info");
      // 获取配置项的讨论超时时间，理论上不会执行这一行代码
      YBDQueryConfigsRespEntity? configEntity = await ApiHelper.queryConfigs(context, timeout: 10 * 1000);
      logger.v("request app configure info is completed");

      if (configEntity?.returnCode == Const.HTTP_SUCCESS) {
        // 本地保存配置信息
        ConfigUtil.writeConfig(configEntity!);
        _configsEntity = configEntity;

        // 刷新 store 里的配置信息
        YBDSplashUtil.refreshStoreWithConfig(store!, _configsEntity!.record!);

        await YBDSplashUtil.configAppLang(context);
      } else {
        logger.v('sync request app configure info failed');
      }
    } else {
      _configsEntity = localConfigEntity;

      // 刷新 store 里的配置信息
      YBDSplashUtil.refreshStoreWithConfig(store!, _configsEntity!.record!);

      await YBDSplashUtil.configAppLang(context);

      // 从接口获取最新的配置信息然后更新 store
      YBDSplashUtil.refreshAppConfig(context, store);
    }
  }
}

class YBDSplashBlocState {
  /// 封设备信息
  YBDDeviceUniqueID? deviceUniqueID;

  /// app 配置信息
  YBDQueryConfigsRespEntity? configsEntity;

  /// 显示封设备弹框
  bool shouldShowDialog;

  /// 是否请求过配置项
  bool requestedConfigs;

  YBDSplashBlocState({
    this.requestedConfigs = false,
    this.deviceUniqueID,
    this.configsEntity,
    this.shouldShowDialog = false,
  });
}
