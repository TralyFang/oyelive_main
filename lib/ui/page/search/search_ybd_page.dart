import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/share_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/room/room_ybd_helper.dart';
import 'package:oyelive_main/ui/page/search/bloc/search_ybd_page_bloc.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/room_ybd_list_item.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';
import 'package:oyelive_main/ui/widget/sharing_ybd_widget.dart';
import 'package:oyelive_main/ui/widget/user_ybd_list_item.dart';

class YBDSearchPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => YBDSearchPageState();
}

class YBDSearchPageState extends BaseState<YBDSearchPage> {
  TextEditingController _searchController = TextEditingController();

  _search({String? searchKey}) async {
    logger.v('start searching ...');
    String key = searchKey ?? _searchController.text.trim();

    if (key.isEmpty) {
      logger.v('search key is empty');
      YBDToastUtil.toast(translate('enter_search_key'), gravity: ToastGravity.CENTER);
      return;
    }

    /// 点击历史记录时，搜索框显示搜索词
    _searchController.text = searchKey ?? _searchController.text;

    /// 搜索
    BlocProvider.of<YBDSearchPageBloc>(context).searchUser(context, key);

    /// 收起键盘
    FocusScope.of(context).requestFocus(FocusNode());

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.SEARCH,
      location: YBDLocationName.SEARCH_PAGE,
      searchTerm: searchKey,
    ));
  }

  @override
  Widget myBuild(BuildContext context) {
    return BlocBuilder<YBDSearchPageBloc, YBDSearchPageBlocState>(
      builder: (BuildContext context, YBDSearchPageBlocState blocState) {
        return Scaffold(
          backgroundColor: YBDTPStyle.heliotrope,
          appBar: AppBar(
            leading: YBDNavBackButton(),
            title: ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: ScreenUtil().setWidth(74),
              ),
              child: TextField(
                controller: _searchController,
                textInputAction: TextInputAction.search,
                style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                decoration: InputDecoration(
                  hintText: translate('search_user_hint'),
                  hintStyle: TextStyle(color: Colors.white.withOpacity(0.5)),
                  contentPadding: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(40), borderSide: BorderSide.none),
                  fillColor: Colors.white.withOpacity(0.2),
                  filled: true,
                  suffixIcon: CloseButton(
                      color: Colors.white.withOpacity(0.5),
                      onPressed: () {
                        logger.v('clicked clear button');
                        // 清空输入搜索词，回到初始页面
                        _searchController.clear();
                        BlocProvider.of<YBDSearchPageBloc>(context).resetSearch();
                      }),
                ),
                onSubmitted: (t) {
                  logger.v('submit search text : $t');
                  _search();
                },
              ),
            ),
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            actions: <Widget>[
              IconButton(
                padding:
                    EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10), vertical: ScreenUtil().setWidth(10)),
                icon: Image.asset('assets/images/icon_search.png', width: ScreenUtil().setWidth(40)),
                onPressed: () {
                  logger.v('clicked search button');
                  _search();
                },
              )
            ],
          ),
          body: Container(
            width: double.infinity,
            decoration: YBDTPStyle.gradientDecoration,
            child: blocState.searchState == SearchState.init // 初始页面，显示搜索记录、you may like 、 invite
                ? Column(
                    children: <Widget>[
                      blocState.searchHistory!.isNotEmpty
                          ? Padding(
                              padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(25)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    translate('search_history'),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: ScreenUtil().setSp(24),
                                    ),
                                  ),
                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      borderRadius: BorderRadius.circular(ScreenUtil().setSp(30)),
                                      splashColor: Colors.white10,
                                      highlightColor: Colors.white12,
                                      onTap: () async {
                                        await BlocProvider.of<YBDSearchPageBloc>(context).clearSearchHistory();
                                      },
                                      child: Container(
                                        height: ScreenUtil().setWidth(60),
                                        width: ScreenUtil().setWidth(60),
                                        child: Icon(
                                          Icons.delete,
                                          color: Colors.white,
                                          size: ScreenUtil().setWidth(40),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Container(),
                      Wrap(
                        spacing: ScreenUtil().setWidth(20), // 左右两个Widget的间距
                        runSpacing: ScreenUtil().setWidth(0), // 上下两个Widget的间距
                        children: blocState.searchHistory!
                            .map((item) => YBDSearchItem(
                                  text: item.searchKey,
                                  onPressed: (searchKey) {
                                    logger.v('clicked search key : $searchKey');
                                    _search(searchKey: searchKey);
                                  },
                                ))
                            .toList(),
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                              top: ScreenUtil().setWidth(50),
                              left: ScreenUtil().setWidth(25),
                            ),
                            child: Text(
                              translate('you_may_like'),
                              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(32)),
                            ),
                          )
                        ],
                      ),
                      Expanded(
                        child: blocState.loadedRooms!
                            ? ListView.separated(
                                keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                                itemBuilder: (_, index) => Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                          splashColor: Colors.white10,
                                          highlightColor: Colors.white12,
                                          onTap: () async {
                                            logger.v('item click ... ${blocState.youMayLike![index]!.nickname}');
                                            YBDRoomHelper.enterRoom(context, blocState.youMayLike![index]!.id,
                                                location: 'search_you_may_like');
                                          },
                                          child: YBDRoomListItem(
                                            blocState.youMayLike![index],
                                            Colors.white,
                                            nameColor: Colors.white.withOpacity(0.6),
                                          )),
                                    ),
                                separatorBuilder: (_, index) => Container(
                                      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                                      height: ScreenUtil().setWidth(1),
                                      color: Const.COLOR_BORDER,
                                    ),
                                itemCount: blocState.youMayLike == null ? 0 : blocState.youMayLike!.length)
                            : YBDLoadingCircle(),
                      ),
                      Container(
                        height: ScreenUtil().setWidth(96),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                splashColor: Colors.white10,
                                highlightColor: Colors.white12,
                                onTap: () async {
                                  showModalBottomSheet(
                                      context: context,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(ScreenUtil().setWidth(16)),
                                            topRight: Radius.circular(ScreenUtil().setWidth(16))),
                                      ),
                                      builder: (BuildContext context) {
                                        return YBDSharingWidget(
                                          title: translate('invite_friends'),
                                          // 不显示分享给好友的图标
                                          shareInside: false,
                                        );
                                      });
                                },
                                child: Container(
                                  height: ScreenUtil().setWidth(96),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Image.asset(
                                        'assets/images/icon_invite.png',
                                        width: ScreenUtil().setWidth(36),
                                        height: ScreenUtil().setWidth(36),
                                        fit: BoxFit.contain,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(9)),
                                      ),
                                      Text(
                                        translate('invite'),
                                        style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(30)),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )),
                            Container(
                              color: Colors.white.withOpacity(0.3),
                              width: ScreenUtil().setWidth(1),
                              height: ScreenUtil().setWidth(50),
                            ),
                            Expanded(
                                child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                splashColor: Colors.white10,
                                highlightColor: Colors.white12,
                                onTap: () async {
                                  await YBDShareUtil.shareSms(context);
                                },
                                child: Container(
                                  height: ScreenUtil().setWidth(96),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Image.asset(
                                        'assets/images/icon_contact.png',
                                        width: ScreenUtil().setWidth(36),
                                        height: ScreenUtil().setWidth(36),
                                        fit: BoxFit.contain,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(9)),
                                      ),
                                      Text(
                                        translate('contact'),
                                        style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(30)),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )),
                          ],
                        ),
                      )
                    ],
                  )
                : (blocState.searchState == SearchState.searching // 搜索中
                    ? YBDLoadingCircle()
                    : (blocState.searchUsers == null || blocState.searchUsers!.isEmpty)
                        ? Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Image.asset(
                                  "assets/images/empty/empty_my_profile.webp",
                                  width: ScreenUtil().setWidth(382),
                                ),
                                SizedBox(
                                  height: ScreenUtil().setWidth(30),
                                ),
                                Text(
                                  translate('no_result'),
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          )
                        : ListView.separated(
                            itemBuilder: (_, index) => Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                      splashColor: Colors.white10,
                                      highlightColor: Colors.white12,
                                      onTap: () async {
                                        logger.v('search item click ... ${blocState.searchUsers![index]!.nickname}');

                                        YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
                                        if (null != userInfo &&
                                            '${userInfo.id}' == '${blocState.searchUsers![index]!.id}') {
                                          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.myprofile);
                                        } else {
                                          YBDNavigatorHelper.navigateTo(context,
                                              YBDNavigatorHelper.user_profile + "/${blocState.searchUsers![index]!.id}");
                                        }
                                      },
                                      child: YBDUserListItem(blocState.searchUsers![index])),
                                ),
                            separatorBuilder: (_, index) => Container(
                                  margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                                  height: ScreenUtil().setWidth(1),
                                  color: Const.COLOR_BORDER,
                                ),
                            itemCount: blocState.searchUsers == null ? 0 : blocState.searchUsers!.length)),
          ),
        );
      },
    );
  }
  void myBuildoll4Qoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();

    YBDSearchPageBloc bloc = BlocProvider.of<YBDSearchPageBloc>(context);
    bloc.loadSearchHistory(); // 加载搜索记录
    bloc.loadRooms(context); // 加载 You may like 房间列表
  }
  void initState4WFsQoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    _searchController.dispose();
    super.dispose();
  }
}

class YBDSearchItem extends StatelessWidget {
  YBDSearchItem({
    Key? key,
    required this.text,
    this.onPressed,
  }) : super(key: key);

  final String? text;
  Function? onPressed;

  @override
  Widget build(BuildContext context) {
    // RaisedButton -
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadiusDirectional.circular(ScreenUtil().setSp(50))),
          elevation: 0.0,
          padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
          primary: Colors.white.withOpacity(0.2)),
      /*
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadiusDirectional.circular(ScreenUtil().setSp(50)),
      ),
      elevation: 0.0,
       */
      child: Text(
        this.text!,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(24),
          color: Colors.white,
        ),
      ),
      /*
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
      color: Colors.white.withOpacity(0.2),
       */
      onPressed: () {
        onPressed?.call(this.text);
      },
    );
  }
  void buildFFebtoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
