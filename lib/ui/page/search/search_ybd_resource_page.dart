import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/constant/const.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../module/status/entity/dialogue_ybd_list_entity.dart';
import '../../../module/status/entity/lyrics_ybd_list_entity.dart';
import '../../../module/status/entity/music_ybd_list_entity.dart';
import '../../../module/status/entity/random_ybd_dialogue_entity.dart';
import '../../../module/status/entity/random_ybd_lyric_entity.dart';
import '../../../module/status/status_ybd_api_helper.dart';
import '../guide/guide_ybd_pages.dart';
import 'item_ybd_script.dart';
import 'item_ybd_song.dart';
import 'search_ybd_title.dart';
import '../status/post_audio/handler/audio_ybd_handler.dart';
import '../../widget/my_ybd_refresh_indicator.dart';

enum ResourceType { Lyrics, Song, Dialogue }

/// 搜索歌词，对白，背景音乐的页面
class YBDSearchResourcePage extends StatefulWidget {
  int resourceTypeIndex;

  YBDSearchResourcePage(this.resourceTypeIndex);

  @override
  YBDSearchResourcePageState createState() => new YBDSearchResourcePageState();
}

class YBDSearchResourcePageState extends BaseState<YBDSearchResourcePage> with WidgetsBindingObserver {
  ResourceType? _resourceType;

  int page = 1;
  int pageSize = 10;

  String? currentSearchKey;

  AudioPlayer _audioPlayer = AudioPlayer(playerId: "background_demo");

  //播放音乐标记
  int playingIndex = -1;

  List<YBDRandomDialogueDataDialogue?>? _dialogueList;
  List<YBDRandomLyricDataLyric?>? _lyricsList;
  List<YBDMusicListDataRow?>? _musicList;

  RefreshController _smartRefresher = new RefreshController();

  /// 列表滚动监听器
  final ScrollController _listScrollController = ScrollController();

  /// 1，下拉加载下一页；2，开始搜索会调用这个方法
  searchNow(String? searchKey) async {
    switch (_resourceType) {
      case ResourceType.Song:
        YBDMusicListEntity? musicListEntity = await YBDStatusApiHelper.getMusicList(
          context,
          page: page,
          pageSize: pageSize,
          searchKey: searchKey,
        );

        // 加载结束
        if (_smartRefresher.isLoading) {
          logger.v('set footer state to idle');
          _smartRefresher.loadComplete();
        }

        if (musicListEntity?.code == Const.HTTP_SUCCESS_NEW) {
          logger.v('search song success');
          if (page == 1) {
            _musicList = musicListEntity?.data?.rows;
          } else {
            _musicList?.addAll(musicListEntity?.data?.rows ?? []);
          }

          // 没有更多数据
          if ((musicListEntity?.data?.rows?.length ?? 0) < pageSize) {
            logger.v('no more song : ${musicListEntity?.data?.rows?.length}');
            _smartRefresher.loadNoData();
          } else {
            logger.v('loaded songs amount : ${_musicList?.length}');
          }
        } else {
          logger.v('search song failed');
          YBDToastUtil.toast("Failed!");
        }
        break;
      case ResourceType.Lyrics:
        YBDLyricsListEntity? lyricsListEntity = await YBDStatusApiHelper.getLyricsList(
          context,
          page: page,
          pageSize: pageSize,
          searchKey: searchKey,
        );

        // 加载结束
        if (_smartRefresher.isLoading) {
          logger.v('set footer state to idle');
          _smartRefresher.loadComplete();
        }

        if (lyricsListEntity?.code == Const.HTTP_SUCCESS_NEW) {
          logger.v('search lyrics success');
          if (page == 1) {
            _lyricsList = lyricsListEntity?.data?.rows;
          } else {
            _lyricsList?.addAll(lyricsListEntity?.data?.rows ?? []);
          }

          // 没有更多数据
          if ((lyricsListEntity?.data?.rows?.length ?? 0) < pageSize) {
            logger.v('no more lyrics : ${lyricsListEntity?.data?.rows?.length}');
            _smartRefresher.loadNoData();
          } else {
            logger.v('loaded lyrics amount : ${_lyricsList?.length}');
          }
        } else {
          logger.v('search lyrics failed');
          YBDToastUtil.toast(lyricsListEntity?.message);
        }
        break;
      case ResourceType.Dialogue:
        YBDDialogueListEntity? dialogueListEntity = await YBDStatusApiHelper.getDialogueList(
          context,
          page: page,
          pageSize: pageSize,
          searchKey: searchKey,
        );

        // 加载结束
        if (_smartRefresher.isLoading) {
          logger.v('set footer state to idle');
          _smartRefresher.loadComplete();
        }

        if (dialogueListEntity?.code == Const.HTTP_SUCCESS_NEW) {
          logger.v('search dialogue success');
          if (page == 1) {
            _dialogueList = dialogueListEntity?.data?.rows;
          } else {
            _dialogueList?.addAll(dialogueListEntity?.data?.rows ?? []);
          }

          // 没有更多数据
          if ((dialogueListEntity?.data?.rows?.length ?? 0) < pageSize) {
            logger.v('no more dialogue : ${dialogueListEntity?.data?.rows?.length}');
            _smartRefresher.loadNoData();
          } else {
            logger.v('loaded dialogue amount : ${_dialogueList?.length}');
          }
        } else {
          logger.v('search dialogue failed');
          YBDToastUtil.toast(dialogueListEntity?.message);
        }
        break;
    }

    setState(() {});
  }

  /// 下拉加载更多
  nextPage(String? searchKey) {
    page++;
    logger.v('load next page : $page, search key : $searchKey');
    searchNow(searchKey);
  }

  /// 1，默认搜索；2，按搜索按钮会调用这个方法
  initSearch(String? searchKey) async {
    page = 1;
    _dialogueList = null;
    _lyricsList = null;
    _musicList = null;
    setState(() {});
    showLockDialog();

    // 重新搜索时重置列表 footer 显示状态
    _smartRefresher.resetNoData();
    await searchNow(searchKey);
    dismissLockDialog();
  }

  getList() {
    late IndexedWidgetBuilder itemBuilder;
    int? itemCount;
    switch (_resourceType) {
      case ResourceType.Song:
        if (_musicList == null) {
          return Container();
        }
        if (_musicList!.length == 0) {
          return Center(
            child: Text(
              translate('no_result'),
              style: TextStyle(color: Colors.white),
            ),
          );
        }
        itemBuilder = (_, index) => YBDSongItem(
              _musicList![index],
              playingIndex == index,
              onCLickPlay: () {
                playingIndex = index;
                setState(() {});
              },
              audioPlayer: _audioPlayer,
              onClickUse: (String filePath) async {
                logger.v('on click use music');
                await _audioPlayer.stop();

                YBDAudioHandler.getInstance().useBackgroundMusic = true;
                YBDAudioHandler.getInstance().selectedMusic = _musicList![index];
                YBDAudioHandler.getInstance().backgroundMusicFilePath = filePath;
                showLockDialog(info: "Remixing");
                await YBDAudioHandler.getInstance().editAudioAttribute();
                dismissLockDialog();
              },
            );
        itemCount = _musicList!.length;

        break;
      case ResourceType.Lyrics:
        if (_lyricsList == null) {
          return Container();
        }
        if (_lyricsList!.length == 0) {
          return Center(
            child: Text(
              translate('no_result'),
              style: TextStyle(color: Colors.white),
            ),
          );
        }

        itemBuilder = (_, index) => GestureDetector(
              onTap: () {
                YBDAudioHandler.getInstance().lyric = _lyricsList![index];
                Navigator.pop(context);
              },
              child: YBDScriptItem(
                "\"${_lyricsList![index]!.name}\"    ${translate('by')} ${_lyricsList![index]!.author}",
                _lyricsList![index]!.text,
                isMark: _lyricsList![index]!.mark,
                webSite: _lyricsList![index]!.activityH5,
              ),
            );
        itemCount = _lyricsList!.length;

        break;
      case ResourceType.Dialogue:
        if (_dialogueList == null) {
          return Container();
        }
        if (_dialogueList!.length == 0) {
          return Center(
            child: Text(
              translate('no_result'),
              style: TextStyle(color: Colors.white),
            ),
          );
        }

        itemBuilder = (_, index) => GestureDetector(
              onTap: () {
                logger.v('clicked search resource list item');
                YBDAudioHandler.getInstance().dialogue = _dialogueList![index];
                Navigator.pop(context);
              },
              child: YBDScriptItem(
                "\"${_dialogueList![index]!.name}\"    ${translate('by')} ${_dialogueList![index]!.author}",
                _dialogueList![index]!.text,
              ),
            );
        itemCount = _dialogueList!.length;

        break;
    }

    return SmartRefresher(
      enablePullDown: false,
      enablePullUp: true,
      controller: _smartRefresher,
      header: YBDMyRefreshIndicator.myHeader,
      footer: YBDMyRefreshIndicator.myFooter,
      onLoading: () {
        logger.v('pull to load more data');
        nextPage(currentSearchKey);
      },
      child: ListView.builder(
        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(13)),
        controller: _listScrollController,
        itemBuilder: itemBuilder,
        itemCount: itemCount,
      ),
    );
  }

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          logger.v('click to hide keyboard');
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
          decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  "assets/images/bg_post_audio.webp",
                )),
          ),
          child: Column(
            children: [
              YBDSearchTitle(
                _resourceType,
                onClickUpload: () {
                  logger.v('clicked upload button');
                  logger.v('upload resource type : $_resourceType');
                  if (_resourceType != ResourceType.Song) {
                    YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.upload_script);
                  } else {
                    YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.upload_music);
                  }
                },
                onSubmit: (sk) {
                  currentSearchKey = sk;
                  initSearch(sk);
                },
              ),
              Expanded(child: getList()),
            ],
          ),
        ),
      ),
    );
  }
  void myBuildgJgDaoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 滑动列表隐藏键盘
  _scrollListener() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  @override
  void initState() {
    super.initState();
    _listScrollController.addListener(_scrollListener);

    _resourceType = ResourceType.values[widget.resourceTypeIndex];
    WidgetsBinding.instance?.addObserver(this);
    _audioPlayer.onPlayerCompletion.listen((event) {
      playingIndex = -1;
      setState(() {});
    });

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      // 默认搜
      initSearch(null);
      itemGuideUpload(context);
    });
  }
  void initStatezYnftoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    _listScrollController.dispose();
    _smartRefresher.dispose();
    if (_audioPlayer != null) {
      _audioPlayer.stop();
      _audioPlayer.dispose();
    }
    super.dispose();
  }
  void disposeDxrPToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDSearchResourcePage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      setState(() {});
    }
  }
  void didChangeAppLifecycleStatePxbzNoyelive(AppLifecycleState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
