import 'dart:async';

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/util/log_ybd_util.dart';
import 'search_ybd_resource_page.dart';

class YBDSearchTitle extends StatefulWidget {
  Function? onSubmit;
  Function? onClickUpload;
  ResourceType? resourceType;

  YBDSearchTitle(this.resourceType, {this.onSubmit(String text)?, this.onClickUpload});

  @override
  YBDSearchTitleState createState() => new YBDSearchTitleState();
}

class YBDSearchTitleState extends BaseState<YBDSearchTitle> {
  TextEditingController _textEditingController = new TextEditingController();

  @override
  Widget myBuild(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(93),
      child: Row(
        children: [
          Container(
            width: ScreenUtil().setWidth(88),
            child: TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              style: TextButton.styleFrom(padding: EdgeInsets.all(ScreenUtil().setWidth(20))),
              // padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
            ),
          ),
          Expanded(
            child: Container(),
          ),
          Container(
            width: ScreenUtil().setWidth(520),
            height: ScreenUtil().setWidth(70),
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
            decoration: BoxDecoration(
                color: Color(0x32ffffff), borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(38)))),
            child: Row(
              children: [
                Icon(
                  Icons.search,
                  size: ScreenUtil().setWidth(44),
                  color: Colors.white,
                ),
                Expanded(
                  child: TextField(
                    style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                    textInputAction: TextInputAction.search,
                    controller: _textEditingController,
                    onSubmitted: (text) {
                      widget.onSubmit?.call(text);
                    },
                    decoration: InputDecoration(
                        suffixIcon: GestureDetector(
                          onTap: () {
                            _textEditingController.clear();
                            widget.onSubmit?.call(null);
                          },
                          child: Icon(
                            Icons.clear,
                            color: Colors.white,
                          ),
                        ),
                        contentPadding: EdgeInsets.only(top: ScreenUtil().setWidth(8)),
                        border: InputBorder.none,
                        hintText: getHint(),
                        counterText: '',
                        hintStyle: TextStyle(color: Color(0x88ffffff))),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: Container(),
          ),
          getUploadIcon(),
        ],
      ),
    );
  }
  void myBuildMkQAEoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget getUploadIcon() {
    if (Platform.isIOS && widget.resourceType == ResourceType.Song) {
      logger.v('iOS not implement upload local music feature');
      return SizedBox(width: ScreenUtil().setWidth(88));
    } else {
      return Container(
        width: ScreenUtil().setWidth(88),
        child: TextButton(
          onPressed: () {
            widget.onClickUpload?.call();
          },
          child: Image.asset('assets/images/icon_upload.png'),
          style: TextButton.styleFrom(padding: EdgeInsets.all(ScreenUtil().setWidth(20))),
          // padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
        ),
      );
    }
  }
  void getUploadIconlqjJ8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 搜索框占位字符
  String getHint() {
    switch (widget.resourceType) {
      case ResourceType.Song:
        return translate('song_hint');
        break;
      case ResourceType.Lyrics:
        return translate('lyrics_hint');
        break;
      case ResourceType.Dialogue:
        return translate('dialogue_hint');
        break;
      default:
        return '';
    }
  }
  void getHintnTEMFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDSearchTitle oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
  void didChangeDependencies9X6Ndoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
