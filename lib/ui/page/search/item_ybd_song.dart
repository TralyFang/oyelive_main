
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/constant/const.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../module/status/entity/music_ybd_list_entity.dart';
import '../../widget/dialog_ybd_download.dart';
import 'package:intl/intl.dart';

enum SongStatus {
  NotDownload,
  Playing,
  Pause,
}

class YBDSongItem extends StatefulWidget {
  YBDMusicListDataRow? data;
  bool isPlaying;
  Function? onCLickPlay, onClickUse;
  AudioPlayer? audioPlayer;

  YBDSongItem(this.data, this.isPlaying, {this.audioPlayer, this.onCLickPlay, this.onClickUse(String musicPath)?});

  @override
  YBDSongItemState createState() => new YBDSongItemState();
}

class YBDSongItemState extends BaseState<YBDSongItem> with SingleTickerProviderStateMixin {
  bool isPlaying = false;

  late AnimationController controller;

  SongStatus _songStatus = SongStatus.NotDownload;

  late String filePath;

  Future<bool> checkIfLocalExit() async {
    filePath = await YBDCommonUtil.getResourceDir(Const.RESOURCE_BACKGROUND_MUSIC) +
        Platform.pathSeparator +
        (widget.data?.id ?? '') +
        '.' +
        (widget.data?.resource?.split('.').last ?? '');
    final songs = File(filePath);

    bool isExist = await songs.exists();
    print(songs.path + isExist.toString());
    if (isExist && widget.audioPlayer!.state != PlayerState.PLAYING) _songStatus = SongStatus.Pause;
    return isExist;
  }
  void checkIfLocalExitoAvlNoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getCenterIcon() {
    print(_songStatus.toString());
    switch (_songStatus) {
      case SongStatus.NotDownload:
        return Icon(
          Icons.file_download,
          color: Colors.white,
          size: ScreenUtil().setWidth(28),
        );
        break;
      case SongStatus.Playing:
        return Image.asset(
          "assets/images/icon_dics_pause.png",
          width: ScreenUtil().setWidth(14),
        );
        break;
      case SongStatus.Pause:
        return Image.asset(
          "assets/images/icon_dics_play.png",
          width: ScreenUtil().setWidth(28),
        );
        break;
    }
  }

  getDuration(int secs) {
    return DateFormat("mm:ss").format(DateTime(0, 0, 0, 0, 0, secs));
  }

  @override
  Widget myBuild(BuildContext context) {
    if (_songStatus != SongStatus.NotDownload) {
      if (widget.audioPlayer!.state == PlayerState.PLAYING) {
        if (widget.isPlaying) {
          controller.forward();
          _songStatus = SongStatus.Playing;
        } else {
          controller.stop();
          _songStatus = SongStatus.Pause;
        }
      } else {
        controller.stop();
        _songStatus = SongStatus.Pause;
      }
    }

    return GestureDetector(
      onTap: () async {
        switch (_songStatus) {
          case SongStatus.NotDownload:
            String saveDir = await YBDCommonUtil.getResourceDir(Const.RESOURCE_BACKGROUND_MUSIC);
            showDialog<Null>(
                context: context, //BuildContext对象
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return YBDDownloadDialog(
                    widget.data?.resource,
                    savePath: saveDir,
                    fileName: (widget.data?.id ?? '') + '.' + (widget.data?.resource?.split('.').last ?? ''),
                    onDownloadComplete: (path) async {
                      ///after download play
                      _songStatus = SongStatus.Playing;
                      isPlaying = true;
                      widget.onCLickPlay?.call();
                      controller.forward();
                      widget.audioPlayer!.play(filePath, isLocal: true);

                      setState(() {});
                    },
                  );
                });
            break;
          case SongStatus.Playing:
            _songStatus = SongStatus.Pause;
            controller.stop();
            print('papapapapapapap');
            if (widget.audioPlayer!.state == PlayerState.PLAYING) {
              widget.audioPlayer!.pause();
            }

            break;
          case SongStatus.Pause:
            if (widget.audioPlayer!.state == PlayerState.PAUSED) {
              widget.audioPlayer!.resume();
            }
            widget.audioPlayer!.play(filePath);
            widget.onCLickPlay?.call();
            controller.forward();
            _songStatus = SongStatus.Playing;
            break;
        }
        setState(() {});
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(25), vertical: ScreenUtil().setWidth(13)),
        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(34), horizontal: ScreenUtil().setWidth(25)),
        decoration: BoxDecoration(
          color: Color(0x32ffffff),
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
        ),
        child: Row(
          children: [
            Container(
              width: ScreenUtil().setWidth(96),
              height: ScreenUtil().setWidth(96),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  RotationTransition(turns: controller, child: Image.asset('assets/images/icon_dics.webp')),
                  Container(
                    width: ScreenUtil().setWidth(48),
                    decoration: BoxDecoration(color: Color(0xff4EBCD4), shape: BoxShape.circle),
                  ),
                  FutureBuilder(
                      future: checkIfLocalExit(),
                      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                        if (snapshot.hasData) {
                          return getCenterIcon();
                        } else {
                          return Container();
                        }
                      })
                ],
              ),
            ),
            SizedBox(
              width: ScreenUtil().setWidth(40),
            ),
            Container(
              height: ScreenUtil().setWidth(96),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.data?.name ?? '',
                    style:
                        TextStyle(fontSize: ScreenUtil().setSp(30), color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  Spacer(),
                  Text(
                    'From the Internet    ${getDuration(widget.data?.duration ?? 0)}',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(24), color: Color(0xccffffff), fontWeight: FontWeight.w300),
                  ),
                ],
              ),
            ),
            Spacer(),
            widget.isPlaying
                ? GestureDetector(
                    onTap: () {
                      widget.onClickUse?.call(filePath);
                      _songStatus = SongStatus.Pause;
                      controller.stop();
                      setState(() {});
                    },
                    child: Container(
                      width: ScreenUtil().setWidth(140),
                      height: ScreenUtil().setWidth(56),
                      decoration: BoxDecoration(
                        color: Color(0xff5694FA),
                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        translate('Use'),
                        style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
  void myBuildFRegfoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(seconds: 4), vsync: this);
    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.repeat();
      }
    });

    widget.audioPlayer!.onPlayerCompletion.listen((event) {
      if (controller.status != AnimationStatus.completed) {
        controller.stop();
      }
      setState(() {});

      print("bo wanl  e");
    });
  }
  void initStatetLSWmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    controller.dispose();
    widget.audioPlayer?.stop();
//    widget.audioPlayer?.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDSongItem oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetFzdNOoyelive(YBDSongItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
