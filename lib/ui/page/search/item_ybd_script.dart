import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';

class YBDScriptItem extends StatelessWidget {
  String? title, content;
  bool? isMark;
  String? webSite;
  YBDScriptItem(this.title, this.content, {this.isMark: false, this.webSite});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(25), vertical: ScreenUtil().setWidth(13)),
          padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(34), horizontal: ScreenUtil().setWidth(12)),
          width: ScreenUtil().setWidth(720),
          decoration: BoxDecoration(
            color: Color(0x32ffffff),
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title!,
                style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xccffffff)),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(30),
              ),
              Padding(
                padding: EdgeInsets.only(left: ScreenUtil().setWidth(12)),
                child: Text(
                  content!,
                  style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        isMark!
            ? Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    YBDNavigatorHelper.navigateToWeb(
                      context,
                      "?url=${Uri.encodeComponent(webSite!)}",
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: ScreenUtil().setWidth(10), right: ScreenUtil().setWidth(18)),
                    child: Image.asset(
                      "assets/images/event_lyric.webp",
                      width: ScreenUtil().setWidth(80),
                    ),
                    decoration: BoxDecoration(),
                  ),
                ),
              )
            : Container()
      ],
    );
  }
  void buildwHeMwoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
