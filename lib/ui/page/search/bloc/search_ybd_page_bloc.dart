import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/query_ybd_label_rooms_resp_entity.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/db/serarch_ybd_history_model_v2.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

class YBDSearchPageBloc extends Bloc<int, YBDSearchPageBlocState> {
  late YBDSearchPageBlocState state;

  YBDSearchPageBloc() : super(YBDSearchPageBlocState.initial()) {
    state = YBDSearchPageBlocState.initial();
  }

  loadRooms(BuildContext context) async {
    YBDQueryLabelRoomsRespEntity? resp = await ApiHelper.queryPopularRooms(context, 0, 10);
    state.youMayLike = resp?.record?.rank;
    state.loadedRooms = true;
    add(0);
  }

  loadSearchHistory() async {
    logger.v('===begin===');
    var userId = await YBDUserUtil.userIdInt();
    List<dynamic> history = await YBDSearchHistoryModelV2()
        .sqlSelect(where: "user_id=\'$userId\'", orderBy: "time DESC")
        .catchError((dynamic e) {
      logger.e('id=$userId error: $e');
    });
    state.searchHistory = history;
    add(0);
    logger.v('===end===');
  }

  clearSearchHistory() async {
    logger.v('===begin===');
    // 删除必须给条件
    await YBDSearchHistoryModelV2().sqlDelete(where: '1=1').catchError((dynamic e) {
      logger.e('clear history error: $e');
    });

    state.searchHistory = [];
    add(0);
    logger.v('===end===');
  }

  searchUser(BuildContext context, String keyword) async {
    state.searchState = SearchState.searching;
    add(0);

    /// 如果用户搜索Lucky ID，直接搜索用户ID
    String? luckyUserID = await YBDCommonUtil.getUserIDByLuckyID(context, keyword);

    List<YBDUserInfo?>? result = await ApiHelper.searchUser(
      context,
      luckyUserID != null && luckyUserID.isNotEmpty ? luckyUserID : keyword,
    );

    state.searchUsers = result;
    state.searchState = SearchState.searched;
    add(0);

    /// 保存搜索记录到数据库
    await YBDSearchHistoryModelV2(
      searchKey: keyword,
      time: DateTime.now().millisecondsSinceEpoch,
      userId: '${await YBDUserUtil.userIdInt()}',
    ).insert();

    // YBDDataBaseUtil.instance.insertSearchHistory(keyword);
    await loadSearchHistory();
  }

  resetSearch() {
    state.searchState = SearchState.init;
    add(0);
  }

  @override
  Stream<YBDSearchPageBlocState> mapEventToState(int event) async* {
    yield YBDSearchPageBlocState.clone(state);
  }
  void mapEventToStateGtv56oyelive(int event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

enum SearchState { init, searching, searched }

class YBDSearchPageBlocState {
  List<dynamic>? searchHistory;

  /// you may like 房间列表
  bool? loadedRooms;
  List<YBDRoomInfo?>? youMayLike;

  /// 搜索用户结果列表
  SearchState? searchState; // init - 初始页面 ;  searching  -  搜索中 ；   searched - 搜索结束
  List<YBDUserInfo?>? searchUsers;

  YBDSearchPageBlocState({this.searchHistory, this.loadedRooms, this.youMayLike, this.searchState, this.searchUsers});

  factory YBDSearchPageBlocState.initial() => YBDSearchPageBlocState(
        searchHistory: [],
        youMayLike: [],
        loadedRooms: false,
        searchState: SearchState.init,
        searchUsers: [],
      );

  static YBDSearchPageBlocState clone(YBDSearchPageBlocState value) {
    return YBDSearchPageBlocState(
      searchHistory: value.searchHistory,
      loadedRooms: value.loadedRooms,
      youMayLike: value.youMayLike,
      searchState: value.searchState,
      searchUsers: value.searchUsers,
    );
  }
}
