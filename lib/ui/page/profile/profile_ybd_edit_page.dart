import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:oyelive_main/ui/page/profile/widgets/system_ybd_avatar_dialog.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_cupertino_date_picker_fork/flutter_cupertino_date_picker_fork.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:image_pickers/CropConfig.dart';
import 'package:image_pickers/Media.dart';
import 'package:image_pickers/image_pickers.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/upload_ybd_s3_util.dart';
import 'package:oyelive_main/common/widget/gradient_ybd_mask.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/base_ybd_resp_entity.dart';
import 'package:oyelive_main/ui/widget/profile_ybd_bg_swiper.dart';
import 'package:oyelive_main/ui/widget/profile_ybd_upload_bg.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/constant/const.dart';
import '../../../common/http/http_ybd_util.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/config_ybd_util.dart';
import '../../../common/util/date_ybd_util.dart';
import '../../../common/util/file_ybd_util.dart';
import '../../../common/util/image_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/numeric_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../module/agency/agency_ybd_api_helper.dart';
import '../../../module/api.dart';
import '../../../module/entity/result_ybd_bean.dart';
import '../../../module/user/util/user_ybd_util.dart';
import '../../../redux/app_ybd_state.dart';
import '../../widget/scaffold/nav_ybd_back_button.dart';
import '../../widget/profile_ybd_edit_row.dart';
import '../home/widget/language_ybd_list_dialog.dart';
import 'my_profile/util/profile_ybd_edit_util.dart';

/// profile 编辑页面
class YBDProfileEditPage extends StatefulWidget {
  @override
  YBDProfileEditPageState createState() => YBDProfileEditPageState();
}

class YBDProfileEditPageState extends BaseState<YBDProfileEditPage> {
  /// 选择的生日
  DateTime selectedTime = DateTime.now();

  /// 压缩文件尺寸
  static const COMPRESS_WIDTH = 720;

  /// 上传中dialogue 提示语Controller
  ValueNotifier<String> _infoController = ValueNotifier<String>('');

  /// 是否为签约主播 （上传头像、海报 限制）
  bool? isOfficialTalent = YBDTPGlobal.isOfficialTalent;

  /// 是否显示上传Cover （是否签约主播，  用户 || 主播等级）
  bool? showCover = false;
  final YBDTextCtrl tc = Get.put(YBDTextCtrl());
  // 替换的S3key
  String _replaceKey = '';

  /// 可以选择图片
  bool enabled = true;
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _showCover();
    });

    YBDFileUtil.clearTempDir();
  }
  void initStateToZXtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    double headerHeight = ScreenUtil().setWidth(720); // 头部海报的高度
    double addHeight = ScreenUtil().setWidth(40); // padding
    double deHeight = ScreenUtil().setWidth(120); // padding
    return Scaffold(
      body: StoreBuilder<YBDAppState>(builder: (context, store) {
        try {
          if (store.state.bean?.birthday != null) {
            selectedTime = DateTime.fromMillisecondsSinceEpoch(store.state.bean!.birthday!);
            logger.v(
                'user birthday: ${store.state.bean!.birthday}, selectedTime : ${YBDDateUtil.formatStringWithDate(selectedTime)}');
          }
        } catch (e) {
          YBDLogUtil.e('parse user birthday error');
        }

        return GestureDetector(
          onTap: () {
            // 触摸收起键盘
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Stack(children: <Widget>[
            Positioned(
                top: 0,
                child: Container(
                    height: ScreenUtil().screenHeight,
                    width: ScreenUtil().screenWidth,
                    color: Colors.white,
                    child: SingleChildScrollView(
                        child: Column(children: <Widget>[
                      Container(
                        height: headerHeight + addHeight,
                        width: ScreenUtil().screenWidth,
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              // 头部海报
                              top: 0,
                              // child: Image.asset('assets/images/bg_profile.jpg',
                              //     height: headerHeight, width: ScreenUtil().screenWidth),
                              child: Container(
                                height: headerHeight,
                                width: ScreenUtil().screenWidth,
                                child: YBDBGSwiper(data: store.state.bean!.backgrounds?.bgs),
                              ),
                            ),
                            // Gradient Mask
                            Positioned(top: 0, child: YBDGradientMask(height: 181, black: 0.6)),
                            Positioned(
                                top: headerHeight - 181.px,
                                child: YBDGradientMask(height: 181, black: 0.4, up2down: false)),
                            Positioned(
                                left: 18.px,
                                top: headerHeight - deHeight,
                                child: YBDProfileUploadBgRow(
                                  data: store.state.bean!.backgrounds?.bgs ?? [],
                                  uploadCallback: () {
                                    updateImage(EditType.Background);
                                  },
                                  replaceCallback: (oldKey) async {
                                    updateImage(EditType.ReplaceBg, oldKey: oldKey);
                                  },
                                )),
                          ],
                        ),
                      ),
                      // 头像
                      YBDProfileEditRow(translate('head_portrait'), EditType.Avatar, store.state.bean,
                          clickedCallback: () async {
                        logger.v('on click avatar camera...');
                        // 用户等级 >= 3 (ulevel_privilege_avatar) 或者 签约主播  可以上传本地图片做为头像
                        // 否则，只能从系统库设置头像
                        if (YBDUserUtil.getLoggedUser(context)!.level! >=
                            YBDNumericUtil.stringToInt(await ConfigUtil.ulevelPrivilegeAvatar(context) ?? '3')) {
                          updateImage(EditType.Avatar);
                        } else {
                          if (isOfficialTalent == null) {
                            await _requestOfficialTalent();
                          }

                          if (isOfficialTalent!) {
                            updateImage(EditType.Avatar);
                          } else {
                            YBDSystemAvatarDialog.show();
                          }
                        }
                      }),
                      // ID
                      YBDProfileEditRow(translate('id'), EditType.ID, store.state.bean, clickedCallback: () {
                        logger.v('id callback');
                        Clipboard.setData(ClipboardData(text: '${store.state.bean!.id}'));
                        YBDToastUtil.toast(translate('copied_to_clipboard'));
                      }),
                      // 昵称
                      YBDProfileEditRow(
                        translate('nick_name'),
                        EditType.NickName,
                        store.state.bean,
                        clickedCallback: () {
                          logger.v('nick name callback');
                          profileEditShowInputDialog(EditType.NickName, store.state.bean!.nickname);
                        },
                        showInfoBtn: true,
                        infoBtnClick: () {
                          profileEditClickNameWarn('15', '5', '15');
                        },
                      ),
                      YBDTPGlobal.isOfficialTalent! ? SizedBox() : Obx(() => profileEditNameWarn('${tc.nameWarn}')),
                      // 语言
                      YBDProfileEditRow(translate('language_text'), EditType.Language, store.state.bean,
                          clickedCallback: () {
                        logger.v('language callback');
                        showLanguageDialog();
                      }),
                      // 位置
                      YBDProfileEditRow(
                        translate('location'),
                        EditType.Location,
                        store.state.bean,
                        clickedCallback: () {
                          logger.v('location callback');
                          profileEditShowCountryDialog();
                        },
                        showInfoBtn: true,
                        infoBtnClick: () {
                          logger.v('22.1.5----clickLocationWarn');
                          profileEditClickLocationWarn(store.state.configs?.user_modify_country_day);
                        },
                      ),
                      // _locationWarn(store.state?.configs?.user_modify_country_day),
                      //* 手机号
                      YBDProfileEditRow(translate('phone_number'), EditType.Phone, store.state.bean, clickedCallback: () {
                        logger.v('phone callback');
                        if (store.state.bean!.cellphone == null || store.state.bean!.cellphone!.isEmpty) {
                          profileEditShowPhoneEditDialog();
                        }
                      }),
                      // 邮箱
                      YBDProfileEditRow(translate('email'), EditType.Email, store.state.bean, clickedCallback: () {
                        logger.v('email callback');
                        profileEditShowInputDialog(EditType.Email, store.state.bean!.email);
                      }),
                      // 性别
                      YBDProfileEditRow(translate('gender'), EditType.Gender, store.state.bean,
                          clickedSexCallback: (int sex) {
                        logger.v('clicked sex : $sex');
                        YBDDialogUtil.showLoading(context);
                        profileEditSaveToNetwork(EditType.Gender, '$sex', saveSuccess: () {
                          YBDDialogUtil.hideLoading(context);
                        });
                      }),
                      // 生日
                      YBDProfileEditRow(translate('birthday'), EditType.Birthday, store.state.bean, clickedCallback: () {
                        showBirthSelectView();
                      }),
                      // 语录
                      YBDProfileEditRow(translate('bio'), EditType.Bio, store.state.bean, clickedCallback: () {
                        profileEditShowInputDialog(EditType.Bio, store.state.bean!.comment, inputMaxLength: 40);
                      }),
                      showCover!
                          ? YBDProfileEditRow(translate('change_cover_picture'), EditType.Cover, store.state.bean,
                              clickedCallback: () {
                              updateImage(EditType.Cover);
                            })
                          : Container(),
                      SizedBox(height: ScreenUtil().setWidth(88)),
                    ])))),
            // 固定在顶部的导航栏
            Positioned(top: 0, child: topNavBar()),
          ]),
        );
      }),
    );
  }
  void myBuildYKWwnoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 弹出选择语言对话框
  showLanguageDialog() async {
    String locale = (await YBDSPUtil.get(Const.SP_LOCALE)) ?? Const.DEFAULT_LANG;

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return YBDLanguageListDialog(locale);
        });
  }

  /// 弹出日期选择器
  showBirthSelectView() {
    DatePicker.showDatePicker(context,
        initialDateTime: selectedTime,
        maxDateTime: DateTime.now(),
        pickerTheme: DateTimePickerTheme(
            cancelTextStyle: TextStyle(color: Color(0xffADAFB2), fontSize: ScreenUtil().setSp(24)),
            confirmTextStyle: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(28))),
        onConfirm: (DateTime s, k) {
      logger.v('selected date : ${s.millisecondsSinceEpoch}');
      selectedTime = s;
      profileEditSaveToNetwork(EditType.Birthday, '${s.millisecondsSinceEpoch}');
    });
  }

  // 导航栏
  Container topNavBar() {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
      height: ScreenUtil().setWidth(94),
      width: ScreenUtil().screenWidth,
      child: Stack(children: [
        Positioned(left: 0, child: YBDNavBackButton()),
        Center(
            child: Text(
          translate('edit_profile'),
          style: TextStyle(color: Colors.white, fontSize: YBDTPStyle.spNav),
        ))
      ]),
    );
  }

  updateImage(EditType type, {String? oldKey}) async {
    Future.delayed(Duration(seconds: 2), () => enabled = true);
    if (enabled) {
      enabled = false;
      List<Media> medias = await ImagePickers.pickerPaths(showCamera: true, cropConfig: CropConfig(enableCrop: true));

      if (null != medias && medias.isNotEmpty) {
        File _file = File(medias[0].path!);
        bool fileExist = await _file.exists();
        if (fileExist) {
          logger.v("selected image: ${_file.path}");

          // compress image
          String name = _file.path.toLowerCase();
          if (name.endsWith(".jpg") ||
              name.endsWith(".jpeg") ||
              name.endsWith(".png") ||
              (name.endsWith(".webp") && Platform.isAndroid)) {
            var decodedImage = await decodeImageFromList(_file.readAsBytesSync());
            int minWidth, minHeight;
            if (decodedImage.width <= COMPRESS_WIDTH) {
              minWidth = decodedImage.width;
              minHeight = decodedImage.height;
            } else {
              minWidth = COMPRESS_WIDTH;
              minHeight = COMPRESS_WIDTH * decodedImage.height ~/ decodedImage.width;
            }
            String targetPath = (await YBDCommonUtil.getResourceDir(Const.TEMP)) +
                Platform.pathSeparator +
                _file.path.substring(_file.path.lastIndexOf('/') + 1);
            File? compressedFile = await FlutterImageCompress.compressAndGetFile(_file.path, targetPath,
                quality: 80,
                minWidth: minWidth,
                minHeight: minHeight,
                format: name.endsWith(".jpg") || name.endsWith(".jpeg")
                    ? CompressFormat.jpeg
                    : name.endsWith(".png")
                        ? CompressFormat.png
                        : CompressFormat.webp);
            fileExist = await compressedFile?.exists() ?? false;
            if (fileExist) {
              _file = compressedFile!;
            }
          } else {
            YBDToastUtil.toast(translate('picture_type_not_support'));
            return;
          }

          showLockDialog(info: '${translate('uploading')}... ', infoController: _infoController);
          if (type == EditType.Background) {
            _uploadBg(_file);
          } else if (type == EditType.ReplaceBg) {
            _replaceBg(_file, oldKey);
          } else {
            _uploadFile(_file, type);
          }
        } else {
          logger.v('picked image not exists : ${_file.path}');
        }
      } else {
        logger.v('pick file failed : media is empty');
      }
    } else {
      logger.v('updateImage is not enable');
    }
  }

  // S3上传背景
  _uploadBg(File file) {
    YBDUploadS3Util.uploadFile(file).then((s3Name) async {
      if (s3Name != null) {
        logger.v('22.5.16---s3Name:$s3Name');
        logger.v('22.5.17---s3path:${s3Name.getS3Path()}');
        YBDBaseRespEntity? res = await ApiHelper.uploadBg(context, s3Name);
        if (res?.returnCode == Const.HTTP_SUCCESS) {
          ApiHelper.checkLogin(context);
        }
        YBDToastUtil.toast(res?.returnMsg);
      } else {
        logger.v('22.5.16---upload s3Name error !');
        YBDToastUtil.toast(translate('failed'));
      }
      dismissLockDialog();
    });
  }

  // S3替换背景
  _replaceBg(File file, String? oldKey) {
    YBDUploadS3Util.uploadFile(file).then((s3Name) async {
      if (s3Name != null) {
        logger.v('22.5.16---s3Name:$s3Name');
        _replaceKey = s3Name;
        YBDBaseRespEntity? res = await ApiHelper.replaceBg(context, oldKey, s3Name);
        if (res?.returnCode == Const.HTTP_SUCCESS) {
          ApiHelper.checkLogin(context);
        }
        YBDToastUtil.toast(res?.returnMsg);
      } else {
        logger.v('22.5.16---upload s3Name error !');
        YBDToastUtil.toast(translate('failed'));
      }
      dismissLockDialog();
    });
  }

  _uploadFile(File _file, EditType type) async {
    // uploadfile api
    // 上传次数限制接口
    YBDResultBeanEntity? result = await YBDHttpUtil.getInstance().upload(context, YBDApi.UPLOAD_FILE, _file.path,
        onSendProgress: (int sent, int total) {
      print("uploading file: $sent / $total : ${100 * sent / total}%");
      _infoController.value = '${translate('uploading')}... ${YBDNumericUtil.doubleToInt(100 * sent / total)}%';
    });
    logger.v('upload file result: ${result?.returnCode} : ${result?.record}');
    _infoController.value = '';
    if (result?.returnCode == Const.HTTP_SUCCESS) {
      dismissLockDialog();
      profileEditSaveToNetwork(type, result!.record);
    } else {
      // 上传失败，提示
      dismissLockDialog();
      YBDToastUtil.toast(result?.returnMsg ?? translate('failed'));
    }
  }

  /// 是否显示上传Cover    用户 || 主播等级
  _showCover() async {
    if (YBDUserUtil.getLoggedUser(context)!.level! >=
            YBDNumericUtil.stringToInt(await ConfigUtil.ulevelGoLive(context) ?? '3') ||
        YBDUserUtil.getLoggedUser(context)!.roomlevel! >=
            YBDNumericUtil.stringToInt(await ConfigUtil.tlevelGoLive(context) ?? '3')) {
      if (mounted)
        setState(() {
          showCover = true;
        });
    } else {
      if (isOfficialTalent == null) {
        await _requestOfficialTalent();
      }

      if (mounted)
        setState(() {
          showCover = isOfficialTalent;
        });
    }
  }

  /// 请求是否签约主播
  _requestOfficialTalent() async {
    await YBDAgencyApiHelper.isOfficialTalent(context, YBDUserUtil.getLoggedUserID(context)).then((value) {
      isOfficialTalent = value;
    });
  }
}
