import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/vip_ybd_info_entity.dart';
import '../../../../module/entity/vip_ybd_valid_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../buy/buy_ybd_sheet.dart';
import '../../buy/buy_ybd_vip_step.dart';
import '../../../widget/colored_ybd_safe_area.dart';
import '../../../widget/loading_ybd_circle.dart';

class YBDVipPage extends StatefulWidget {
  @override
  YBDVipPageState createState() => new YBDVipPageState();
}

class YBDVipPageState extends BaseState<YBDVipPage> with SingleTickerProviderStateMixin {
  TabController? _tabController;
  YBDVipInfoEntity? _vipInfoEntity;
  YBDVipValidInfoEntity? _vipValidInfoEntity;

  var tagImage = [
    "assets/images/prince.png",
    "assets/images/pirate.png",
  ];
  var privilegesImage = [
    "assets/images/profile/privileges_silver.png",
    "assets/images/profile/privileges_gold.png",
  ];
  var type = [
    translate('prince'),
    translate('pirate'),
  ];

  var notVip = [
    translate('not_vip'),
    translate('not_svip'),
  ];

  var privilegesListImages = [
    "assets/images/vip_name.webp",
    "assets/images/entrance_effect.png",
    "assets/images/high_speed_upgrade.webp",
    "assets/images/vip_badge.png",
    "assets/images/front_row.webp",
    "assets/images/private_room.webp",
    "assets/images/bullet_message.png",
    "assets/images/highlighted_name.webp",
    "assets/images/edit_avatar.webp",
    "assets/images/edit_name.webp",
  ];
  var privilegesListNames = [
    translate("vip_name_card"),
    translate("entrance_effect"),
    translate("high_speed_upgrade"),
    translate("vip_badge"),
    translate("front_row"),
    translate("private_room"),
    translate("bullet_message"),
    translate("hightlighted_name"),
    '${translate("modify")}\n${translate("avatar")}',
    '${translate("modify")}\n${translate("nick_name")}',
  ];

  var iconImages = [
    "assets/images/ic_vip_prince.png",
    "assets/images/ic_vip_pirate.png",
  ];
  getPrivilege(int index) {
    print(Platform.operatingSystem);
    return Container(
      width: ScreenUtil().setWidth(668),
      decoration: BoxDecoration(
          color: Color(0x1bffffff), borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
      child: Column(
        children: [
          SizedBox(
            height: ScreenUtil().setWidth(43),
          ),
          Image.asset(
            privilegesImage[index],
            width: ScreenUtil().setWidth(483),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(20),
          ),
          GridView.count(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            crossAxisCount: 3,
            children: List.generate(9 + index, (tagIndex) {
              if (index == 0 && tagIndex > 6) tagIndex += 1;
              String imagePath = privilegesListImages[tagIndex];
              if (tagIndex == 6)
                imagePath =
                    index == 0 ? "assets/images/bullet_message_silver.webp" : "assets/images/bullet_message_gold.webp";

              return Container(
                decoration: tagIndex > 5
                    ? BoxDecoration()
                    : BoxDecoration(
                        border: Border(bottom: BorderSide(color: Color(0x09D8D8D8), width: ScreenUtil().setWidth(1)))),
                child: Column(
                  children: [
                    SizedBox(
                      height: ScreenUtil().setWidth(20),
                    ),
                    Image.asset(
                      imagePath,
                      width: ScreenUtil().setWidth(112),
                    ),
                    SizedBox(
                      height: ScreenUtil().setWidth(20),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(140),
                      child: Text(
                        privilegesListNames[tagIndex],
                        style: TextStyle(fontSize: ScreenUtil().setSp(22), color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
        ],
      ),
    );
  }

  getBody(int index) {
    String? validDate;
    List<YBDVipValidInfoRecord?> info = _vipValidInfoEntity!.record!.where((element) => element!.type == "VIP").toList();
    if (info.length != 0) {
      List<YBDVipValidInfoRecordItemList?> vip =
          info[0]!.itemList!.where((element) => element!.id == _vipInfoEntity!.record![index]!.id).toList();
      if (vip.length != 0) {
        validDate = translate("valid_date") +
            DateFormat(" yyyy/MM/dd HH:mm").format(
                DateTime.fromMillisecondsSinceEpoch(DateTime.now().millisecondsSinceEpoch + vip[0]!.expireAfter! * 1000));
      }
    }
    return Column(
      children: <Widget>[
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  alignment: Alignment.topCenter,
                  children: <Widget>[
                    Image.asset(
                      tagImage[index],
                      width: ScreenUtil().setWidth(720),
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: ScreenUtil().setWidth(262),
                        ),
                        Text(
                          type[index],
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(38), fontWeight: FontWeight.w600, color: Colors.white),
                        ),
                        SizedBox(
                          height: ScreenUtil().setWidth(5),
                        ),
                        Text(
                          validDate ?? notVip[index],
                          style: TextStyle(fontSize: ScreenUtil().setSp(20), color: Color(0xffE0E0E0)),
                        ),
                        SizedBox(
                          height: ScreenUtil().setWidth(33),
                        ),
                      ],
                    ),
                  ],
                ),
                getPrivilege(index),
                SizedBox(
                  height: ScreenUtil().setWidth(20),
                ),
              ],
            ),
          ),
        ),
        Container(
          height: ScreenUtil().setWidth(96),
          child: Row(
            children: [
              SizedBox(
                width: ScreenUtil().setWidth(380),
                child: Center(
                  child: Text(
                    translate('only') +
                        " ${YBDCommonUtil.formatNum(_vipInfoEntity!.record![index]!.price)} " +
                        translate('unit'),
                    style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                  ),
                ),
              ),
              Expanded(
                  child: GestureDetector(
                onTap: () async {
                  var data = json.decode(await YBDSPUtil.get(Const.SP_DISCOUNT_VIP));
                  data = data['priceInfo'];
                  print("datall${data}");
                  showModalBottomSheet(
                      context: context,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.only(topLeft: Radius.circular(8.0), topRight: Radius.circular(8.0)),
                          side: BorderSide(
                            style: BorderStyle.none,
                          )),
                      builder: (context) => YBDBuySheet(
                            YBDBuyVipStep(_vipInfoEntity!.record![index]!.id, data["${_vipInfoEntity!.record![index]!.id}"],
                                _vipInfoEntity!.record![index]!.price, type[index], iconImages[index]),
                            onSuccessBuy: () {
                              logger.v('buy vip success');
                              YBDUserUtil.updateVip(1);
                              getVipValidInfo();
                            },
                          ));
                },
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                    Color(0xff5E94E7),
                    Color(0xff47CDCC),
                  ])),
                  child: Text(
                    translate('buy'),
                    style:
                        TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(32), fontWeight: FontWeight.w600),
                  ),
                ),
              ))
            ],
          ),
        )
      ],
    );
  }

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          translate('purchase_vip'),
          style: TextStyle(color: Colors.white, fontSize: YBDTPStyle.spNav),
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color(0xffA33CCA),
        leading: Container(
          width: ScreenUtil().setWidth(88),
          child: TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            style: TextButton.styleFrom(padding: EdgeInsets.all(ScreenUtil().setWidth(20))),
            // padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
          ),
        ),
      ),
      body: Container(
        color: Color(0xff32249D),
        child: YBDColoredSafeArea(
          color: Color(0xff32249D),
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                Color(0xffA33CCA),
                Color(0xff32249D),
              ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
            ),
            child: _vipInfoEntity == null || _vipValidInfoEntity == null
                ? YBDLoadingCircle()
                : Column(
                    children: [
                      Container(
                        height: ScreenUtil().setWidth(66),
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(100)),
                        child: TabBar(
                          indicator: UnderlineTabIndicator(
                            borderSide: BorderSide(
                              width: ScreenUtil().setWidth(4),
                              color: Color(0xff47CCCB),
                            ),
                            insets: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(86)),
                          ),
                          labelColor: Colors.white,
                          unselectedLabelColor: Color(0x7fffffff),
                          indicatorPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                          controller: _tabController,
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              height: 1,
                              fontSize: ScreenUtil().setSp(32),
                              color: Colors.white),
                          unselectedLabelStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              height: 1,
                              fontSize: ScreenUtil().setSp(28),
                              color: Color(0x7fffffff)),
                          tabs: [
                            Tab(text: translate("prince")),
                            Tab(text: translate("pirate")),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setWidth(40),
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: [getBody(0), getBody(1)],
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
  void myBuildiDvKToyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getVipInfo() async {
    YBDVipInfoEntity? result = await ApiHelper.queryVip(context);
    if (result != null && result.returnCode == Const.HTTP_SUCCESS) {
      _vipInfoEntity = result;
      setState(() {});
    } else {
      if (result != null) YBDToastUtil.toast(result.returnMsg);
    }
  }

  getVipValidInfo() async {
    YBDVipValidInfoEntity? result = await ApiHelper.queryVipExpireDate(context);
    if (result != null && result.returnCode == Const.HTTP_SUCCESS) {
      _vipValidInfoEntity = result;
      setState(() {});
    } else {
      if (result != null) YBDToastUtil.toast(result.returnMsg);
    }
  }

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 2, vsync: this);

    getVipInfo();

    getVipValidInfo();

    ApiHelper.queryVipDisCount(context);
  }
  void initStateEiFNKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDVipPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidget0Ickloyelive(YBDVipPage oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}

class YBDVipBuyInfo {}
