import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/profile/controllers/system_ybd_avatar_ctr.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

class YBDSystemAvatarDialog extends StatelessWidget {
  /// 显示系统头像库
  static void show() {
    YBDSystemAvatarController ctr = Get.put(YBDSystemAvatarController());
    ctr.initConfigs();

    showModalBottomSheet(
      context: Get.context!,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.px as double),
          topRight: Radius.circular(16.px as double),
        ),
      ),
      builder: (BuildContext context) {
        return YBDSystemAvatarDialog();
      },
    ).then(
      (_) => Get.delete<YBDSystemAvatarController>(),
    );
  }

  const YBDSystemAvatarDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16.px as double),
            topRight: Radius.circular(16.px as double),
          ),
        ),
        height: 320.px,
        child: Column(
          children: <Widget>[
            Container(
              height: 123.px,
              padding: EdgeInsets.only(bottom: 19.px as double),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    translate('edit_avatar'),
                    style: TextStyle(
                      fontSize: 30.sp,
                      color: Color(0xff434343),
                    ),
                  ),
                ],),
            ),
            /*
            SizedBox(height: 41.px),
            Text(
              translate('edit_avatar'),
              style: TextStyle(
                fontSize: 30.sp,
                color: Color(0xff434343),
              ),
            ),
            SizedBox(height: 60.px),
             */
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _systemAvatars(),
                _lockAvatar(),
              ],
            ),
          ],
        ),
      ),
    );
  }
  void buildmEwKOoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _systemAvatars() {
    return GetBuilder<YBDSystemAvatarController>(
      builder: (YBDSystemAvatarController ctr) {
        if (ctr.avatarConfigs.isEmpty) return SizedBox();
        return Row(
          children: List<Widget>.generate(
            ctr.avatarConfigs.length,
            (int index) => _avatarItem(index),
          ),
        );
      },
    );
  }
  void _systemAvatarsbClXdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _lockAvatar() {
    return GestureDetector(
      onTap: () async {
        String level = await Get.find<YBDSystemAvatarController>().uLevelPrivilegeAvatar();
        YBDToastUtil.toast('Can only be supported after User Level $level');
      },
      child: Container(
        width: 160.px,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              clipBehavior: Clip.none,
              children: <Widget>[
                YBDImage(
                  path: 'assets/images/profile/avatar_lock.webp',
                  width: 120,
                  height: 120,
                  fit: BoxFit.cover,
                ),
                Positioned(
                  top: -4.px,
                  right: -2.px,
                  child: YBDImage(
                    path: 'assets/images/lock_icon.webp',
                    width: 36,
                    height: 36,
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.px as double),
              child: Text(
                translate('gallery'),
                style: TextStyle(
                  fontSize: 20.sp,
                  color: Color(0xff434343),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void _lockAvatarWWFEboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _avatarItem(int index) {
    return GetBuilder<YBDSystemAvatarController>(
      builder: (YBDSystemAvatarController ctr) {
        return YBDDelayGestureDetector(
          onTap: () async {
            // 关闭弹框
            Navigator.pop(Get.context!);

            // 三级以下用户换默认头像
            ctr.updateAvatar(index);
          },
          child: Container(
            width: 160.px,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                YBDNetworkImage(
                  imageUrl: ctr.avatarUrlWithIndex(index),
                  width: 120.px,
                  height: 120.px,
                  fit: BoxFit.cover,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.px as double),
                  child: Text(
                    ctr.avatarNameWithIndex(index),
                    style: TextStyle(
                      fontSize: 20.sp,
                      color: Color(0xff434343),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
  void _avatarItemsO6uCoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
