import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/aristocracy_ybd_const.dart';

class YBDVipIcon extends StatelessWidget {
  EdgeInsetsGeometry? padding;
  String? iconUrl;
  int size;
  YBDVipIcon(this.iconUrl, {this.padding, this.size: 48});

  @override
  Widget build(BuildContext context) {
    if (iconUrl == null || iconUrl!.isEmpty) return SizedBox();
    return Padding(
        padding: padding == null ? EdgeInsets.only(right: 6.px) : padding!,
        // child: Image.asset(
        //   image_prefix + "icon_vip1.png",
        //   width: 44.px,
        // ),
        child: CachedNetworkImage(
          imageUrl: iconUrl!,
          width: size.px,
        ));
  }
  void buildhfYzFoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
