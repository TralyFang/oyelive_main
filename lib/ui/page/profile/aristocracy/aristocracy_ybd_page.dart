
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/dialog_ybd_info.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/dialog_ybd_rules.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/entity/aristocracy_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/item_ybd_privilege.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/sheet_ybd_order.dart';
import 'package:oyelive_main/ui/widget/gradient_ybd_text.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'dart:math' as math;

import '../../../../common/style/tp_ybd_style.dart';
import '../../../../module/entity/vip_ybd_info_entity.dart';
import 'aristocracy_ybd_const.dart';
import 'title_ybd_with_mark.dart';

const notSpecificIndex = -1;

class YBDAristocracyPage extends StatefulWidget {
  int index;

  YBDAristocracyPage(this.index);

  @override
  YBDAristocracyPageState createState() => new YBDAristocracyPageState();
}

class YBDAristocracyPageState extends State<YBDAristocracyPage> {
  int _currentIndex = 0;
  List<YBDAristocracyInfoRecordVipData>? vipData;
  Widget _header() {
    return Container(
      height: 93.px,
      child: Row(
        children: [
          SizedBox(
            width: 88.px,
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              padding: EdgeInsets.symmetric(horizontal: 20.px, vertical: 26.px),
            ),
          ),
          Spacer(),
          Text(
            "Aristocracy",
            style: TextStyle(fontSize: YBDTPStyle.spNav),
          ),
          Spacer(),
          SizedBox(
            width: 88.px,
            child: GestureDetector(
              onTap: () {
                YBDCommonTrack().commonTrack(YBDTAProps(location: 'rule', module: YBDTAModule.vip));
                YBDRulesDialog.show();
              },
              child: Container(
                  width: 40.px,
                  height: 40.px,
                  margin: EdgeInsets.all(20.px),
                  decoration:
                      BoxDecoration(shape: BoxShape.circle, border: Border.all(width: 2.px, color: Colors.white)),
                  child: Center(
                      child: Icon(
                    Icons.question_mark,
                    color: Colors.white,
                    size: 30.px,
                  ))),
            ),
          )
        ],
      ),
    );
  }
  void _headerlc11soyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List gradientColors = [
    <Color>[Color(0xffC85523), Color(0xffA23100)],
    <Color>[Color(0xff15BD6D), Color(0xff237850)],
    <Color>[Color(0xff5069FF), Color(0xff4051CA)],
  ];

  List<Color> unlockColors = <Color>[Color(0xffA23100), Color(0xff0E673C), Color(0xff3143C7)];

  int _getInitPage() {
    if (maxIndex != notSpecificIndex) {
      return maxIndex;
    }
    return (widget.index < vipData!.length || widget.index != null) ? widget.index : 0;
  }
  void _getInitPagerN81Moyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  int getFixPosition(int maxLength, int position) {
    if (position > maxLength - 1) {
      return maxLength - 1;
    }
    return position;
  }
  void getFixPositionr1axRoyelive(int maxLength, int position) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _banner() {
    return SizedBox(
        height: 320.px,
        child: Center(
          child: PageView.builder(
              itemCount: vipData!.length + 1,
              padEnds: true,
              controller: PageController(
                viewportFraction: 0.9,
                initialPage: _getInitPage(),
              ),
              onPageChanged: (i) {
                _currentIndex = i;
                setState(() {});
              },
              itemBuilder: (c, position) {
                return Container(
                  margin: EdgeInsets.symmetric(horizontal: 5.px),
                  height: 300.px,
                  width: 452.px,
                  child: Stack(
                    children: [
                      Image.asset(image_prefix + "vip${position + 1}.png"),
                      Center(
                        child: Row(
                          children: [
                            SizedBox(
                              width: 42.px,
                            ),
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                YBDGradientText(
                                  text: position == vipData!.length ? "Coming soon" : vipData![position].vipName,
                                  fontSize: 40.sp,
                                  fontWeight: FontWeight.w600,
                                  textColors: gradientColors[getFixPosition(gradientColors.length, position)],
                                ),
                                SizedBox(
                                  height: 6.px,
                                ),
                                if (position != vipData!.length &&
                                    !vipData![position].vipValidContent.toLowerCase().contains("valid"))
                                  Padding(
                                    padding: EdgeInsets.only(top: 8.px),
                                    child: new Row(
                                      children: [
                                        // SizedBox(
                                        //   width: 20.px,
                                        // ),
                                        Icon(
                                          Icons.lock,
                                          color: unlockColors[getFixPosition(unlockColors.length, position)]
                                              .withOpacity(0.5),
                                          size: 22.px,
                                        ),
                                        SizedBox(
                                          width: 6.px,
                                        ),
                                        Text(
                                          "To be unlocked",
                                          style: TextStyle(
                                              fontSize: 22.sp,
                                              color: unlockColors[getFixPosition(unlockColors.length, position)]
                                                  .withOpacity(0.8)),
                                        )
                                      ],
                                    ),
                                  ),
                                if (position != vipData!.length &&
                                    vipData![position].vipValidContent.toLowerCase().contains("valid"))
                                  Padding(
                                    padding: EdgeInsets.only(left: 4.px),
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          vipData![position].vipValidContent,
                                          style: TextStyle(
                                              fontSize: 22.sp,
                                              color: unlockColors[getFixPosition(unlockColors.length, position)]
                                                  .withOpacity(0.8)),
                                        ),
                                        SizedBox(
                                          height: 8.px,
                                        ),
                                        Text(
                                          vipData![position].checkInBoundsContent ?? "",
                                          style: TextStyle(
                                              fontSize: 18.sp,
                                              color: unlockColors[getFixPosition(unlockColors.length, position)]
                                                  .withOpacity(0.8)),
                                        )
                                      ],
                                    ),
                                  ),
                                if (position == vipData!.length)
                                  Padding(
                                    padding: EdgeInsets.only(left: 4.px),
                                    child: Text(
                                      "Stay Tuned",
                                      style: TextStyle(
                                          fontSize: 22.sp,
                                          color: unlockColors[getFixPosition(unlockColors.length, position)]
                                              .withOpacity(0.8)),
                                    ),
                                  )
                              ],
                            ),
                            Spacer(),
                            if (position != vipData!.length)
                              CachedNetworkImage(
                                imageUrl: vipData![position].vipIcon!,
                                height: 244.px,
                              ),
                            SizedBox(
                              width: 60.px,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }),
        ));
  }
  void _bannertd73loyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _priceArea() {
    return Container(
      height: 120.px,
      color: Color(0xff1B0F50),
      child: new Row(
        children: [
          SizedBox(
            width: 40.px,
          ),
          Expanded(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  vipData![_currentIndex].vipPrice,
                  style: TextStyle(fontSize: 30.sp, color: Color(0xffE4CB88)),
                ),
                if (vipData![_currentIndex].buyPrice!.length > 0 &&
                    vipData![_currentIndex].buyPrice!.first.introduction != null)
                  Text(
                    vipData![_currentIndex].buyPrice!.first.introduction!,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 22.sp, color: Color(0xffE4CB88).withOpacity(0.6)),
                  ),
              ],
            ),
          ),
          SizedBox(
            width: 10.px,
          ),
          GestureDetector(
            onTap: () {
              YBDCommonTrack().commonTrack(YBDTAProps(
                location: 'buy',
                module: YBDTAModule.vip,
                name: vipData![_currentIndex].vipName,
              ));
              YBDOrderSheet.show(vipData![_currentIndex].buyPrice, vipData![_currentIndex].vipIcon,
                  vipData![_currentIndex].vipName, vipData![_currentIndex].vipCode, onSuccess: () {
                YBDInfoDialog.show(vipInfo: vipData![_currentIndex]);
                queryVip(context, setMax: false);
              });
            },
            child: Container(
              width: 170.px,
              height: 64.px,
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                    Color(0xffF2CB5C),
                    Color(0xffFCDF8B),
                  ]),
                  borderRadius: BorderRadius.all(Radius.circular(32.px))),
              child: Center(
                child: Text(
                  "Buy",
                  style: TextStyle(fontSize: 28.sp, color: Color(0xff563F00), fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 40.px,
          ),
        ],
      ),
    );
  }
  void _priceArea4xo39oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  int maxIndex = notSpecificIndex;

  /// 查询Vip
  Future<YBDAristocracyInfoEntity?> queryVip(BuildContext context, {bool setMax = true}) async {
    YBDAristocracyInfoEntity? result = await YBDHttpUtil.getInstance().doGet<YBDAristocracyInfoEntity>(
      context,
      YBDApi.QUERY_VIP,
      params: {'vipStatus': 1},
    );

    if (result?.returnCode == Const.HTTP_SUCCESS) {
      vipData = result?.record?.vipData;

      if (setMax)
        vipData!.asMap().forEach((index, value) {
          if (widget.index == notSpecificIndex && value.vipValidContent.contains("until")) {
            maxIndex = index;
            _currentIndex = maxIndex;

            print("set max to $index");
          }
        });

      if (_currentIndex == notSpecificIndex) {
        _currentIndex = 0;
      }
      setState(() {});
    } else {
      YBDToastUtil.toast(result?.returnMsg);
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        //background
        Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
            Color(0xff270036),
            Color(0xff08023A),
          ])),
        ),
        Image.asset(image_prefix + "atbg.png"),
        //
        Column(
          children: [
            SizedBox(
              height: ScreenUtil.getStatusBarH(context),
            ),
            _header(),
            if (vipData == null) ...[
              Expanded(
                  child: Center(
                child: YBDLoadingCircle(),
              ))
            ],
            if (vipData != null) ...[
              _banner(),
              SizedBox(
                height: 60.px,
              ),
              YBDTitleWithMark("Exclusive Privileges"),
              SizedBox(
                height: 20.px,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    image_prefix + "thinerline.png",
                    width: 300.px,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 14.px),
                    child: Text(
                      _currentIndex == vipData!.length
                          ? "(${vipData![_currentIndex - 1].privileges.length - 1}/${vipData![_currentIndex - 1].privileges.length - 1})"
                          : "(${vipData![_currentIndex].privileges.where((element) => element.havePrivilege).toList().length}/${vipData![_currentIndex].privileges.length - 1})",
                      style: TextStyle(fontSize: 20.sp, color: Color(0xffFBE0A0).withOpacity(0.6)),
                    ),
                  ),
                  Transform(
                    alignment: Alignment.center,
                    transform: Matrix4.rotationY(math.pi),
                    child: Image.asset(
                      image_prefix + "thinerline.png",
                      width: 300.px,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 60.px,
              ),
              Expanded(
                child: GridView.builder(
                    padding: EdgeInsets.all(0),
                    itemCount: vipData![_currentIndex == vipData!.length ? (vipData!.length - 1) : _currentIndex]
                        .privileges
                        .length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisSpacing: 22.px,
                      crossAxisSpacing: 20.px,
                      childAspectRatio: 160 / 122,
                    ),
                    itemBuilder: (c, index) {
                      return GestureDetector(
                          onTap: () {
                            if (vipData![_currentIndex == vipData!.length ? (vipData!.length - 1) : _currentIndex]
                                .privileges[index]
                                .havePrivilege)
                              YBDInfoDialog.show(
                                  datas: vipData![_currentIndex == vipData!.length ? (vipData!.length - 1) : _currentIndex]
                                      .privileges
                                      .where((element) => element.havePrivilege)
                                      .toList(),
                                  index: index);
                          },
                          child: YBDPrivilegeItem(
                              vipData![_currentIndex == vipData!.length ? (vipData!.length - 1) : _currentIndex]
                                  .privileges[index]));
                    }),
              ),
              if (_currentIndex != vipData!.length) _priceArea()
            ]
          ],
        ),
      ],
    );
  }
  void build8uu78oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentIndex = widget.index;

    queryVip(context);
    print("pass index== ${widget.index}");
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDAristocracyPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependencies4EcWtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
