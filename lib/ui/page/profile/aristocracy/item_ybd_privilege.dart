import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/aristocracy_ybd_const.dart';

import 'entity/aristocracy_ybd_info_entity.dart';

class YBDPrivilegeItem extends StatelessWidget {
  YBDAristocracyInfoRecordVipDataPrivileges data;
  YBDPrivilegeItem(this.data);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(),
      child: Column(
        children: [
          CachedNetworkImage(
            imageUrl: data.privilegeIcon ?? '',
            width: 80.px,
          ),
          SizedBox(
            height: 20.px,
          ),
          SizedBox(
            width: 160.px,
            child: Center(
              child: Text(
                data.privilegeName,
                style: TextStyle(fontSize: 20.sp, color: Color(data.havePrivilege ? 0xffFBE0A0 : 0xffC8C8C8)),
              ),
            ),
          )
        ],
      ),
    );
  }
  void buildXqRv6oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
