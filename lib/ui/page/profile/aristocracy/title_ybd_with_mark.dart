import 'package:flutter/material.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'dart:math' as math;

import 'package:oyelive_main/ui/page/profile/aristocracy/aristocracy_ybd_const.dart';

class YBDTitleWithMark extends StatelessWidget {
  String text;

  YBDTitleWithMark(this.text);

  @override
  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Transform(
          alignment: Alignment.center,
          transform: Matrix4.rotationY(math.pi),
          child: Image.asset(
            image_prefix + "mark.webp",
            width: 28.px,
          ),
        ),
        SizedBox(
          width: 20.px,
        ),
        Text(
          text,
          style: new TextStyle(
            color: Color(0xffFBE0A0),
            fontSize: 28.sp,
          ),
        ),
        SizedBox(
          width: 20.px,
        ),
        Image.asset(
          image_prefix + "mark.webp",
          width: 28.px,
        )
      ],
    );
  }
  void buildpjMu5oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
