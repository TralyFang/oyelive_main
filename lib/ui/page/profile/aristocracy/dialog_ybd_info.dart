
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/aristocracy_ybd_const.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/title_ybd_with_mark.dart';

import 'entity/aristocracy_ybd_info_entity.dart';

class YBDInfoDialog extends StatelessWidget {
  List<YBDAristocracyInfoRecordVipDataPrivileges>? datas;
  YBDAristocracyInfoRecordVipData? vipInfo;
  int? index;
  YBDInfoDialog(this.datas, this.vipInfo, this.index);

  static show({List<YBDAristocracyInfoRecordVipDataPrivileges>? datas, YBDAristocracyInfoRecordVipData? vipInfo, int? index}) {
    showDialog<Null>(
        context: Get.context!, //BuildContext对象
        barrierDismissible: false,
        builder: (BuildContext context) {
          return YBDInfoDialog(datas, vipInfo, index);
        });
  }

  Widget _cardItem(String title, String iconUrl, String? info) {
    return Stack(
      children: [
        Image.asset(
          image_prefix + "ari_infobg.png",
          width: 550.px,
          height: 600.px,
        ),
        SizedBox(
          height: 600.px,
          width: 550.px,
          child: Column(
            children: [
              SizedBox(
                height: 42.px,
              ),
              YBDTitleWithMark(title),
              SizedBox(
                height: 30.px,
              ),
              CachedNetworkImage(
                imageUrl: iconUrl,
                height: 260.px,
              ),
              SizedBox(
                height: 24.px,
              ),
              Expanded(
                  child: Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 48.px,
                  ),
                  child: Text(
                    info ?? "",
                    style: TextStyle(fontSize: 24.sp, color: Color(0xff130438)),
                  ),
                ),
              ))
            ],
          ),
        )
      ],
    );
  }
  void _cardItem8Rjkpoyelive(String title, String iconUrl, String? info) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _centerWidget() {
    bool isCongras = vipInfo != null;
    if (isCongras) return _cardItem("Congratulations", vipInfo!.vipIcon!, vipInfo!.buySuccessNotice);

    return SizedBox(
      height: 600.px,
      // width: double.infinity,
      child: PageView.builder(
        controller: PageController(initialPage: index!),
        itemBuilder: (c, index) {
          return Center(
              child:
                  _cardItem(datas![index].privilegeName, datas![index].privilegeDetailIcon, datas![index].privilegeInfo));
        },
        itemCount: datas!.length,
      ),
    );
  }
  void _centerWidgetJdq11oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 98.px,
          ),
          _centerWidget(),
          SizedBox(
            height: 50.px,
          ),
          GestureDetector(
            onTap: () {
              YBDNavigatorHelper.popPage(context);
            },
            child: Container(
                width: 48.px,
                height: 48.px,
                decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(width: 2.px, color: Colors.white)),
                child: Center(
                  child: Text(
                    String.fromCharCode(Icons.clear_rounded.codePoint),
                    style: TextStyle(
                      inherit: false,
                      color: Colors.white,
                      fontSize: 38.px,
                      fontWeight: FontWeight.w500,
                      fontFamily: Icons.clear.fontFamily,
                      package: Icons.clear.fontPackage,
                    ),
                  ),
                )),
          )
        ],
      ),
    );
  }
  void buildQ9KNgoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
