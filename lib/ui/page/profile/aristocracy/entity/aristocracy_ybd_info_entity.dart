
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDAristocracyInfoEntity with JsonConvert<YBDAristocracyInfoEntity> {
  String? returnCode;
  String? returnMsg;
  late YBDAristocracyInfoRecord record;
}

class YBDAristocracyInfoRecord with JsonConvert<YBDAristocracyInfoRecord> {
  String? title;
  String? buyUrl;
  List<YBDAristocracyInfoRecordVipData>? vipData;
}

class YBDAristocracyInfoRecordVipData with JsonConvert<YBDAristocracyInfoRecordVipData> {
  String? vipName;
  int? vipCode;
  late String vipPrice;
  String? vipIcon;
  String? vipMiniIcon;
  String? ruleIcon;
  dynamic buySuccessNotice;
  String? index;
  late String vipValidContent;
  String? checkInBoundsContent;
  String? introduction;

  late List<YBDAristocracyInfoRecordVipDataPrivileges> privileges;
  List<YBDAristocracyInfoRecordVipDataBuyPrice>? buyPrice;
}

class YBDAristocracyInfoRecordVipDataPrivileges with JsonConvert<YBDAristocracyInfoRecordVipDataPrivileges> {
  late String privilegeName;
  String? privilegeIcon;
  String? privilegeInfo;
  late String privilegeDetailIcon;
  String? index;
  late bool havePrivilege;
}

class YBDAristocracyInfoRecordVipDataBuyPrice with JsonConvert<YBDAristocracyInfoRecordVipDataBuyPrice> {
  int? productPriceId;
  String? periodType;
  int? period;
  int? price;
  int? originalPrice;
  dynamic index;
  String? introduction;
}
