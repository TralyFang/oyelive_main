import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/title_ybd_with_mark.dart';

import '../../../../common/navigator/navigator_ybd_helper.dart';

class YBDRulesDialog extends StatelessWidget {
  static show() {
    showDialog<Null>(
        context: Get.context!, //BuildContext对象
        barrierDismissible: false,
        builder: (BuildContext context) {
          return YBDRulesDialog();
        });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 98.px,
          ),
          Container(
            width: 550.px,
            height: 600.px,
            padding: EdgeInsets.all(8.px),
            decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(24.px)), color: Color(0xff502E7C)),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 30.px),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(22.px)),
                  border: Border.all(width: 1.px, color: Color(0xfffcde8b))),
              child: new Column(
                children: [
                  SizedBox(
                    height: 30.px,
                  ),
                  YBDTitleWithMark("Rules"),
                  SizedBox(
                    height: 20.px,
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Text(
                        "1. The VIP system was upgraded to a new aristocracy system in V2.7.4. Users who bought Prince and Pirate before that will be automatically upgraded to VISCOUNT and COUNT.\n\n"
                        "2. Only users who purchased COUNT and above on the current page can get the right to check in and receive beans. Aristocracy purchased before V2.7.4 or given by the system does not have this benefit.\n\n"
                        "3. If you have already purchased Aristocracy benefits and have received Aristocracy benefits through other non-purchase methods, you will be given priority to enjoy the purchased benefits, and the non-purchase benefits will be used after the expiration of the purchased benefits.\n\n",
                        style: TextStyle(fontSize: 24.sp, height: 1.4),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.px,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 50.px,
          ),
          GestureDetector(
            onTap: () {
              YBDNavigatorHelper.popPage(context);
            },
            child: Container(
                width: 48.px,
                height: 48.px,
                decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(width: 2.px, color: Colors.white)),
                child: Center(
                    child: Icon(
                  Icons.close,
                  color: Colors.white,
                  size: 30.px,
                ))),
          )
        ],
      ),
    );
  }
  void buildaHYdgoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
