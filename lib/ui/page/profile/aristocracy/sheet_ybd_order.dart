
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/buy_ybd_vip_result_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/aristocracy_ybd_const.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/dialog_ybd_common.dart';

import 'entity/aristocracy_ybd_info_entity.dart';

class YBDOrderSheet extends StatefulWidget {
  List<YBDAristocracyInfoRecordVipDataBuyPrice>? data;
  String? icon, name;
  int? code;
  Function? onSuccess;
  YBDOrderSheet(this.data, this.icon, this.name, this.code, this.onSuccess);

  static show(List<YBDAristocracyInfoRecordVipDataBuyPrice>? data, String? icon, String? name, int? code,
      {Function? onSuccess}) {
    showModalBottomSheet(
        context: Get.context!,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(32.px),
            topRight: Radius.circular(32.px),
          ),
          side: BorderSide(style: BorderStyle.none),
        ),
        builder: (c) {
          return YBDOrderSheet(data, icon, name, code, onSuccess);
        });
  }

  @override
  YBDOrderSheetState createState() => new YBDOrderSheetState();
}

class YBDOrderSheetState extends BaseState<YBDOrderSheet> {
  int selectingIndex = -1;

  buyNow() async {
    YBDCommonTrack().commonTrack(YBDTAProps(
      location: 'buy_now',
      module: YBDTAModule.vip,
      num: widget.data![selectingIndex].period,
    ));
    if (selectingIndex == -1) return YBDToastUtil.toast('Please select an option!');
    showLockDialog(info: 'Buying');
    YBDBuyVipResultEntity? resultEntity =
        await ApiHelper.buyVip(context, widget.code, widget.data![selectingIndex].productPriceId.toString());
    dismissLockDialog();
    if (resultEntity?.returnCode == Const.Insiffcient_Code) {
      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return YBDCommonDialog(
              'Infficient balance.\nGo and recharge now!',
              onConfirm: () {
                YBDCommonTrack().commonTrack(YBDTAProps(location: 'recharge_ok', module: YBDTAModule.vip));
                YBDNavigatorHelper.openTopUpPage(context);
              },
              onCancel: () {
                YBDCommonTrack().commonTrack(YBDTAProps(location: 'recharge_cancel', module: YBDTAModule.vip));
              },
            );
          });
    } else if (resultEntity?.returnCode != Const.HTTP_SUCCESS) {
      YBDToastUtil.toast(resultEntity?.returnMsg);
      return false;
    } else {
      YBDUserInfo? info = await YBDUserUtil.userInfo();
      info?.money = resultEntity?.record?.money;
      await YBDSPUtil.save(Const.SP_USER_INFO, info);
      final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
      store.dispatch(YBDUpdateUserAction(info));
      ApiHelper.checkLogin(context);
      YBDNavigatorHelper.popPage(context);
      widget.onSuccess?.call();
      return true;
    }
  }

  Widget _getItemWidget(int i) {
    return GestureDetector(
        onTap: () {
          YBDCommonTrack().commonTrack(YBDTAProps(
            location: 'buy_mouth_choose',
            module: YBDTAModule.vip,
            num: widget.data![i].period,
          ));
          selectingIndex = i;
          setState(() {});
        },
        child: Container(
            alignment: Alignment.center,
            width: ScreenUtil().setWidth(180),
            height: ScreenUtil().setWidth(56),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8.px)),
                color: i == selectingIndex ? Color(0xffFCF0CE) : Colors.white,
                border: Border.all(width: 1.px, color: Color(0xffDA9127))),
            child: Text('${widget.data![i].period} ${widget.data![i].periodType}',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(24), color: Color(0xff333333), fontWeight: FontWeight.w400))));
  }
  void _getItemWidgetfY3WSoyelive(int i) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    bool hasDiscout =
        selectingIndex != -1 && widget.data![selectingIndex].price != widget.data![selectingIndex].originalPrice;
    return Container(
      height: 580.px,
      // padding: EdgeInsets.all(20.px),
      decoration: new BoxDecoration(
        //背景Colors.transparent 透明
        color: Colors.transparent,
        //设置四周圆角 角度
        borderRadius: BorderRadius.only(topLeft: Radius.circular(32.px), topRight: Radius.circular(32.px)),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(),
              ),
              GestureDetector(
                onTap: () {
                  print('close------');
                  Navigator.pop(context);
                },
                child: Container(
                  decoration: BoxDecoration(),
                  padding: EdgeInsets.all(22.px),
                  child: Text(
                    String.fromCharCode(Icons.clear.codePoint),
                    style: TextStyle(
                      inherit: false,
                      color: Color(0xff363F4D),
                      fontSize: 36.px,
                      fontWeight: FontWeight.w900,
                      fontFamily: Icons.clear.fontFamily,
                      package: Icons.clear.fontPackage,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 12.px,
          ),
          Row(
            children: [
              SizedBox(
                width: 42.px,
              ),
              CachedNetworkImage(
                imageUrl: widget.icon!,
                width: 200.px,
              ),
              SizedBox(
                width: ScreenUtil().setWidth(40),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: ScreenUtil().setWidth(15),
                  ),
                  Container(
                      child: Row(
                    children: [
                      Image.asset(
                        'assets/images/topup/y_top_up_beans@2x.webp',
                        width: ScreenUtil().setWidth(30),
                        // height: ScreenUtil().setWidth(30),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(15),
                      ),
                      Text(
                        selectingIndex != -1 ? YBDCommonUtil.formatNum(widget.data![selectingIndex].price) : '',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(36), color: Color(0xffEBB01E), fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(10),
                      ),
                      Container(
                        child: hasDiscout
                            ? Row(
                                children: [
                                  Image.asset(
                                    'assets/images/beans_grey.webp',
                                    width: ScreenUtil().setWidth(18),
                                    // height: ScreenUtil().setWidth(30),
                                  ),
                                  SizedBox(
                                    width: ScreenUtil().setWidth(5),
                                  ),
                                  Text(
                                    YBDCommonUtil.formatNum(widget.data![selectingIndex].originalPrice),
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(22),
                                      color: Color(0xff717171),
                                      fontWeight: FontWeight.w400,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ),
                                ],
                              )
                            : Container(),
                      ),
                    ],
                  )),
                  SizedBox(
                    height: ScreenUtil().setWidth(12),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(300),
                    child: Text(
                      widget.name!,
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.black, fontWeight: FontWeight.w400),
                    ),
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(18),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(300),
                    child: Text(
                      selectingIndex != -1 ? (widget.data![selectingIndex].introduction!.removeBlankSpace()) : '',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(20), color: Color(0xff717171), fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: ScreenUtil().setWidth(44),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.all(0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: widget.data!.length == 0
                  ? []
                  : List.generate(widget.data!.length * 2 + 1, (index) {
                      if (index % 2 == 0)
                        return SizedBox(
                          width: 40.px,
                        );
                      else
                        return _getItemWidget((index - 1) ~/ 2);
                    }),
            ),
          ),
          Expanded(
            child: Center(
              child: GestureDetector(
                onTap: () async {
                  buyNow();
                },
                child: Container(
                  alignment: Alignment.center,
                  width: ScreenUtil().setWidth(440),
                  height: ScreenUtil().setWidth(72),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                        Color(0xffF2CB5C),
                        Color(0xffFCDF8B),
                      ]),
                      borderRadius: BorderRadius.all(Radius.circular(36.px))),
                  child: Text(translate('buy_now'),
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(32), color: Color(0xff563F00), fontWeight: FontWeight.w400)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void myBuilduO86hoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.data!.length > 0) selectingIndex = 0;
  }
  void initState6ehFUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDOrderSheet oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciespHaXIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
