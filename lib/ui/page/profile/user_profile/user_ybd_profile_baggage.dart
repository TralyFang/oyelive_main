import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../baggage/baggage_ybd_page.dart';

/// 其他用户 profile 背包
class YBDUserProfileBaggage extends StatefulWidget {
  final YBDUserInfo userInfo;

  YBDUserProfileBaggage(this.userInfo);

  @override
  YBDUserProfileBaggageState createState() => new YBDUserProfileBaggageState();
}

class YBDUserProfileBaggageState extends State<YBDUserProfileBaggage> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v('clicked baggage');
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.USER_PROFILE_BAGGAGE,
          itemName: '',
        ));
        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.baggage + "/${widget.userInfo.id}");
      },
      child: Container(
        height: ScreenUtil().setWidth(80),
        width: ScreenUtil().setWidth(670),
        // padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(20)),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(50))),
        ),
        child: Stack(
          children: <Widget>[
            Container(
              height: ScreenUtil().setWidth(80),
              width: ScreenUtil().setWidth(670),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(50))),
                color: Color(0xff7E06B5).withOpacity(0.2),
              ),
            ),
            Row(
              children: [
                SizedBox(
                  width: ScreenUtil().setWidth(40),
                ),
                Image.asset(
                  'assets/images/icon_baggage_other.webp',
                  width: ScreenUtil().setWidth(30),
                  color: Colors.white,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(20),
                ),
                Text(
                  translate('baggage'),
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(24),
                    color: YBDActivitySkinRoot().curAct().userProfileNameColor(context),
                  ),
                ),
                Expanded(
                  child: Container(),
                ),
                Image.asset(
                  'assets/images/icon_more_white.webp',
                  width: ScreenUtil().setWidth(48),
                  color: Colors.white,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(26),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
  void buildjFmOnoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
