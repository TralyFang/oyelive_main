import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../../module/user/util/user_ybd_util.dart';

/// 事件类型
enum FollowUserEvent {
  // 查询当前的追踪状态
  Request,
  // 关注
  Follow,
  // 取关
  UnFollow,
  // 取消好友
  UnFriend,
}

/// 追踪按钮的状态
enum FollowStatus {
  // 正在查询 Follow 状态
  Loading,
  // 正在请求关注
  Following,
  // 已经关注
  Followed,
  // 正在取消关注
  UnFollowing,
  // 已经取消关注
  UnFollowed,
  // 好友关系
  Friend,
}

/// 关注按钮类型
enum FollowType {
  // 关注后隐藏按钮
  Hide,
  // 关注后改变关注状态
  Change,
}

/// 用来管理 follow 按钮的状态
class YBDFollowUserBloc extends Bloc<FollowUserEvent, YBDFollowUserBlocState> {
  BuildContext context;

  /// 当前查看的用户页面对应的用户 id
  String userId;

  /// 关注按钮类型
  FollowType type;

  YBDFollowUserBloc(
    this.context,
    this.userId, {
    this.type = FollowType.Change,
  }) : super(YBDFollowUserBlocState(FollowStatus.Loading));

  @override
  Stream<YBDFollowUserBlocState> mapEventToState(FollowUserEvent event) async* {
    switch (event) {
      case FollowUserEvent.Request:
        if (type == FollowType.Hide) {
          // 查询我是否已关注该用户
          bool isFollowed = await _requestConcern();
          if (isFollowed) {
            yield YBDFollowUserBlocState(FollowStatus.Followed);
          } else {
            yield YBDFollowUserBlocState(FollowStatus.UnFollowed);
          }
        } else {
          // 查询是否为我的好友
          bool isFriend = await _requestIsFriend();

          if (isFriend) {
            yield YBDFollowUserBlocState(FollowStatus.Friend);
          } else {
            // 查询我是否已关注该用户
            bool isFollowed = await _requestConcern();

            if (isFollowed) {
              yield YBDFollowUserBlocState(FollowStatus.Followed);
            } else {
              yield YBDFollowUserBlocState(FollowStatus.UnFollowed);
            }
          }
        }
        break;
      case FollowUserEvent.Follow:
        {
          // 界面显示正在关注的状态
          yield YBDFollowUserBlocState(FollowStatus.Following);
          // 关注用户
          bool followSuccess = await _requestFollow();

          if (followSuccess) {
            if (type == FollowType.Change) {
              // 关注成功检查是否为好友
              bool isFriend = await _requestIsFriend();

              if (isFriend) {
                yield YBDFollowUserBlocState(FollowStatus.Friend);
              } else {
                yield YBDFollowUserBlocState(FollowStatus.Followed);
              }
            } else {
              yield YBDFollowUserBlocState(FollowStatus.Followed);
            }
          } else {
            // 关注失败仍然是未关注状态
            yield YBDFollowUserBlocState(FollowStatus.UnFollowed);
          }
          break;
        }
      case FollowUserEvent.UnFollow:
        {
          // 界面显示正在取消关注的状态
          yield YBDFollowUserBlocState(FollowStatus.UnFollowing);
          // 取关该用户
          bool result = await _requestUnFollow();

          if (result) {
            // 关注失败界面显示未关注状态
            yield YBDFollowUserBlocState(FollowStatus.UnFollowed);
          } else {
            // 取关失败界面仍然显示关注状态
            yield YBDFollowUserBlocState(FollowStatus.Followed);
          }
          break;
        }
      case FollowUserEvent.UnFriend:
        {
          // 界面显示正在取消关注的状态
          yield YBDFollowUserBlocState(FollowStatus.UnFollowing);
          // 取关该用户
          bool result = await _requestUnFollow();

          if (result) {
            // 关注失败界面显示未关注状态
            yield YBDFollowUserBlocState(FollowStatus.UnFollowed);
          } else {
            // 取关失败界面仍然显示好友状态
            yield YBDFollowUserBlocState(FollowStatus.Friend);
          }
          break;
        }
    }
  }
  void mapEventToState25QDLoyelive(FollowUserEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询用户是否已关注
  /// true: 已关注该好友，false：未关注该好友
  Future<bool> _requestConcern() async {
    String? myId = await YBDUserUtil.userId();
    bool result = await ApiHelper.queryConcern(context, myId, userId);
    logger.v('user : $userId is followed : $result');
    return result;
  }
  void _requestConcernNiykaoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询用户是否为好友关系
  Future<bool> _requestIsFriend() async {
    String? myId = await YBDUserUtil.userId();
    bool result = await ApiHelper.queryFriendFromId(context, myId, userId);
    return result;
  }
  void _requestIsFriendMgHQ6oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求取消关注接口
  /// true: 取关成功；false：取关失败
  Future<bool> _requestUnFollow() async {
    bool result = await ApiHelper.unFollowUser(context, userId);
    logger.v('un follow user : $userId result : $result');
    return result;
  }

  /// 请求关注接口
  /// true: 关注成功；false：关注失败
  Future<bool> _requestFollow() async {
    bool result = await ApiHelper.followUser(context, userId);
    logger.v('follow user : $userId result : $result');
    return result;
  }
  void _requestFollowHfZ8Noyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// Follow 按钮的状态
class YBDFollowUserBlocState {
  /// 关注状态
  FollowStatus status;

  YBDFollowUserBlocState(this.status);
}
