import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/common/widget/gradient_ybd_mask.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/widget/profile_ybd_bg_swiper.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../common/util/image_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../room/room_ybd_helper.dart';
import 'user_ybd_profile_info.dart';

/// 其他人的 profile 的头部海报
class YBDUserProfileHeader extends StatefulWidget {
  final YBDUserInfo userInfo;

  /// 是否为签约主播
  final bool isOfficialTalent;
  YBDUserProfileHeader(this.userInfo, this.isOfficialTalent);

  @override
  YBDUserProfileHeaderState createState() => new YBDUserProfileHeaderState();
}

class YBDUserProfileHeaderState extends State<YBDUserProfileHeader> {
  /// 好友，追踪信息和粉丝信息容器
  Container _fansInfoContainer(String itemName, String amount) {
    logger.v('item name : $itemName, amount : $amount');
    return Container(
      width: ScreenUtil().setWidth(110),
      // padding: EdgeInsets.only(top: ScreenUtil().setWidth(50), left: ScreenUtil().setWidth(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            // 数字
            width: ScreenUtil().setWidth(110),
            child: Text(
              YBDNumericUtil.compactNumberWithFixedDigits(amount) ?? '0',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(28),
                fontWeight: FontWeight.bold,
                color: YBDActivitySkinRoot().curAct().userProfileFontColor(),
              ),
            ),
          ),
          SizedBox(height: ScreenUtil().setWidth(8)),
          SizedBox(
            // item 标题
            width: ScreenUtil().setWidth(110),
            child: Text(
              itemName,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(24),
                color: YBDActivitySkinRoot().curAct().userProfileIDColor(0.79),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerRight,
      children: <Widget>[
        Container(
          height: ScreenUtil().setWidth(720),
          width: ScreenUtil().screenWidth,
          child: YBDBGSwiper(
            data: widget.userInfo.backgrounds?.bgs,
            haveDot: true,
            myProfile: false,
          ),
        ),
        // Positioned(top: 0, child: YBDGradientMask(height: 181, black: 0.1)),
        // Positioned(bottom: 0, child: YBDGradientMask(height: 181, black: 0.8, up2down: false)),
        Positioned.fill(
          child: IgnorePointer(
            child: Opacity(
              opacity: 0.7,
              child: Container(
                // height: 700.px + ScreenUtil.statusBarHeight,
                width: ScreenUtil().screenWidth,
                // color: Colors.black.withOpacity(0.4),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Color(0xff0C0019).withOpacity(0), Color(0xff0C0019)],
                        begin: Alignment.topCenter,
                        stops: [0.3, 1],
                        end: Alignment.bottomCenter)),
              ),
            ),
          ),
        ),
        Positioned(
          top: Platform.isIOS
              ? ScreenUtil().setWidth(120) + ScreenUtil().setWidth(ScreenUtil().statusBarHeight)
              : ScreenUtil().setWidth(140),
          child: GestureDetector(
            onTap: () {
              YBDRoomHelper.enterRoom(context, widget.userInfo.id, location: 'user_profile_page');
            },
            child: Container(
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.3),
                borderRadius: BorderRadius.horizontal(left: Radius.circular(ScreenUtil().setWidth(35))),
              ),
              width: ScreenUtil().setWidth(170),
              height: ScreenUtil().setWidth(70),
              padding: EdgeInsets.only(
                  left: ScreenUtil().setWidth(10),
                  right: ScreenUtil().setWidth(16),
                  top: ScreenUtil().setWidth(6),
                  bottom: ScreenUtil().setWidth(6)),
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(60),
                    height: ScreenUtil().setWidth(60),
                    decoration: BoxDecoration(shape: BoxShape.rectangle),
                    child: ClipOval(
                      child: YBDNetworkImage(
                        imageUrl:
                            YBDImageUtil.cover(context, widget.userInfo.roomimg, widget.userInfo.id, scene: 'A'),
                        fit: BoxFit.cover,
                        placeholder: (context, url) =>
                            YBDResourcePathUtil.defaultMaleImage(male: widget.userInfo.sex == 2),
                        errorWidget: (context, url, err) =>
                            YBDResourcePathUtil.defaultMaleImage(male: widget.userInfo.sex == 2),
                        width: ScreenUtil().setWidth(60),
                        height: ScreenUtil().setWidth(60),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(12)),
                    child: Text(translate('Room'),
                        style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28))),
                  ),
                  // Image.asset(
                  //   'assets/images/icon_mic_room.png',
                  //   height: ScreenUtil().setWidth(26),
                  //   width: ScreenUtil().setWidth(19),
                  // )
                ],
              ),
            ),
          ),
        ),
        Positioned(bottom: 200.px, left: 0, child: YBDUserProfileInfo(widget.userInfo, widget.isOfficialTalent)),
        Positioned(
            top: Platform.isIOS
                ? ScreenUtil().setWidth(220) + ScreenUtil().setWidth(ScreenUtil().statusBarHeight)
                : 230.px, //230.px,
            right: 0,
            child: Container(
              height: 40.px,
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.3),
                borderRadius: BorderRadius.horizontal(left: Radius.circular(ScreenUtil().setWidth(35))),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    width: 16.px,
                  ),
                  YBDImage(
                    path: YBDActivitySkinRoot().curAct().userProfileLocation(),
                    width: 18,
                    height: 22,
                    color: YBDActivitySkinRoot().curAct().userProfileFontColor(),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(10)),
                  Container(
                    constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(120)),
                    child: Text(
                      YBDCommonUtil.getCountryByCode(widget.userInfo.country) ?? '',
                      maxLines: 1,
                      style: TextStyle(
                          color: YBDActivitySkinRoot().curAct().userProfileFontColor(), fontSize: ScreenUtil().setSp(20)),
                    ),
                  ),
                ],
              ),
            )),
        Positioned(
          bottom: 140.px,
          left: 40.px,
          child: // 关注数，粉丝数，好友数
              Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  // 好友数
                  onTap: () {
                    logger.v('clicked friends button');
                    YBDNavigatorHelper.navigateTo(
                      context,
                      YBDNavigatorHelper.follow + "/${widget.userInfo.id}/0",
                    );
                  },
                  child: _fansInfoContainer(
                    translate('friends'),
                    widget.userInfo.friends?.toString() ?? '0',
                  ),
                ),
                SizedBox(width: ScreenUtil().setWidth(80)),
                GestureDetector(
                  // 追踪数
                  onTap: () {
                    logger.v('clicked Following button');
                    YBDNavigatorHelper.navigateTo(
                      context,
                      YBDNavigatorHelper.follow + "/${widget.userInfo.id}/1",
                    );
                  },
                  child: _fansInfoContainer(
                    translate("following"),
                    widget.userInfo.concern.toString(),
                  ),
                ),
                SizedBox(width: ScreenUtil().setWidth(80)),
                GestureDetector(
                  // 粉丝数
                  onTap: () {
                    logger.v('clicked Follower button');
                    YBDNavigatorHelper.navigateTo(
                      context,
                      YBDNavigatorHelper.follow + "/${widget.userInfo.id}/2",
                    );
                  },
                  child: _fansInfoContainer(
                    translate("followers"),
                    widget.userInfo.fans.toString(),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 86.px,
          left: 50.px,
          child: Container(
            width: ScreenUtil().setWidth(500),
            child: Text(
              widget.userInfo.comment ?? translate('default_comment'),
              style: TextStyle(
                fontSize: ScreenUtil().setSp(24),
                color: YBDActivitySkinRoot().curAct().userProfileIDColor(1),
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        )
      ],
    );
  }
  void buildmDfhooyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
