import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 其他人的 profile 的导航栏和状态栏
class YBDUserProfileNav extends StatefulWidget {
  /// 返回按钮回调
  final VoidCallback backActionCallback;

  /// 举报按钮回调
  final VoidCallback moreActionCallback;

  YBDUserProfileNav(this.backActionCallback, this.moreActionCallback);

  @override
  YBDUserProfileNavState createState() => new YBDUserProfileNavState();
}

class YBDUserProfileNavState extends State<YBDUserProfileNav> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth,
      height: ScreenUtil().setWidth(120),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: [
              Material(
                // 返回按钮
                color: Colors.transparent,
                child: InkWell(
                  onTap: widget.backActionCallback,
                  child: Container(
                    decoration: BoxDecoration(),
                    width: ScreenUtil().setWidth(120),
                    height: ScreenUtil().setWidth(120),
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              // 导航栏中间空白区
              Expanded(child: SizedBox(width: 1)),
              Material(
                // 更多按钮
                color: Colors.transparent,
                child: InkWell(
                  onTap: widget.moreActionCallback,
                  child: Container(
                    width: ScreenUtil().setWidth(120),
                    height: ScreenUtil().setWidth(120),
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.more_vert,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
  void buildJBvSOoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
