import 'dart:async';

import 'dart:developer';

import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:spritewidget/spritewidget.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import '../../activty/activity_ybd_skin.dart';

import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/agency/agency_ybd_api_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/profile_ybd_tag_info.dart';
import '../../../widget/round_ybd_avatar.dart';
import '../../home/widget/top_ybd_talent_item.dart';

/// 其他人 profile 页面
class YBDUserProfileInfo extends StatefulWidget {
  final YBDUserInfo userInfo;

  /// 是否为签约主播
  final bool? isOfficialTalent;

  YBDUserProfileInfo(this.userInfo, this.isOfficialTalent);

  @override
  _YBDUserProfileInfoState createState() => _YBDUserProfileInfoState();
}

class _YBDUserProfileInfoState extends State<YBDUserProfileInfo> {
  bool isAgent = false;

  YBDUserCertificationRecordCertificationInfos? _certificationInfos;
  bool isCerRequested = false;
  checkIsAgent() async {
    if (mounted) {
      isAgent = await YBDAgencyApiHelper.searchAgentByUserId(context, widget.userInfo.id.toString());
      setState(() {});
    }
  }

  _queryCer() async {
    if (mounted) {
      YBDUserCertificationEntity? userCertificationEntity =
          await ApiHelper.queryCertification(context, [widget.userInfo.id]);
      isCerRequested = true;
      if (userCertificationEntity?.returnCode == Const.HTTP_SUCCESS) {
        _certificationInfos = YBDCommonUtil.getUsersSingleCer(userCertificationEntity!.record, widget.userInfo.id);
      }
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      checkIsAgent();
      _queryCer();
    });
  }
  void initState4Edp7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _getIdArea() {
    bool hasUid = widget.userInfo.uniqueNum != null;
    if (hasUid)
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: ScreenUtil().setWidth(40),
          ),
          Stack(
            alignment: Alignment.centerLeft,
            children: [
              Container(
                height: ScreenUtil().setWidth(24),
                padding: EdgeInsets.only(
                  right: ScreenUtil().setWidth(4),
                ),
                margin: EdgeInsets.only(left: ScreenUtil().setWidth(2)),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(1)),
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
                  gradient: LinearGradient(
                    colors: [
                      Color(0xffFBBD1A),
                      Color(0xffFCD323),
                    ],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                ),
                child: Row(
                  children: [
                    SizedBox(
                      width: ScreenUtil().setWidth(24),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(6),
                    ),
                    Text(
                      'ID: ${widget.userInfo.uniqueNum}',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(6),
                    ),
                    Container(
                      width: ScreenUtil().setWidth(23),
                      padding: EdgeInsets.all(2),
                      child: InkWell(
                        onTap: () {
                          logger.v('copied user id : ${widget.userInfo.id}');
                          Clipboard.setData(ClipboardData(text: '${widget.userInfo.uniqueNum}'));
                          YBDToastUtil.toast(translate('copied_to_clipboard'));
                        },
                        child: YBDImage(
                          path: YBDActivitySkinRoot().curAct().userProfileCopy(),
                          fit: BoxFit.fitWidth,
//              scale: 3,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Image.asset(
                "assets/images/u_crown.webp",
                width: ScreenUtil().setWidth(24),
                fit: BoxFit.fitWidth,
              ),
            ],
          ),
        ],
      );
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          width: ScreenUtil().setWidth(40),
        ),
        Text(
          'ID: ${widget.userInfo.id}',
          style: TextStyle(
            fontSize: ScreenUtil().setSp(24),
            color: YBDActivitySkinRoot().curAct().userProfileIDColor(0.79),
          ),
        ),
        Container(
          width: ScreenUtil().setWidth(26),
          padding: EdgeInsets.all(2),
          child: InkWell(
            onTap: () {
              logger.v('copied user id : ${widget.userInfo.id}');
              Clipboard.setData(ClipboardData(text: '${widget.userInfo.id}'));
              YBDToastUtil.toast(translate('copied_to_clipboard'));
            },
            child: YBDImage(
              path: YBDActivitySkinRoot().curAct().userProfileCopy(),
              fit: BoxFit.fitWidth,
//              scale: 3,
            ),
          ),
        ),
        SizedBox(
          width: ScreenUtil().setWidth(10),
        ),
      ],
    );
  }

  _getAgentIcon() {
    return Image.asset(
      "assets/images/agent/icon_agent.webp",
      width: ScreenUtil().setWidth(240),
    );
  }

  List<Widget> _getCertified() {
    if (_certificationInfos != null)
      return [
        Row(
          children: [
            SizedBox(
              width: ScreenUtil().setWidth(40),
            ),
            Text(
              _certificationInfos!.title!,
              style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff89FCF7)),
            ),
          ],
        ),
        SizedBox(
          height: ScreenUtil().setWidth(8),
        ),
      ];

    return [];
  }
  void _getCertifiedfb4YYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    // 文字宽度
    double textWidth = ScreenUtil().screenWidth - ScreenUtil().setWidth(303);

    log("yes${widget.userInfo.toJson()}");
    return GestureDetector(
      onTap: () {
        logger.v('clicked user info rect');
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(40))),
        ),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(height: ScreenUtil().setWidth(60)),
                Stack(children: [
                  Container(
                    child: Row(
                      // 头像栏
                      children: [
                        SizedBox(width: ScreenUtil().setWidth(30)),
                        Column(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                String aPath = YBDImageUtil.avatar(context, widget.userInfo.headimg, widget.userInfo.id);
                                if (aPath != '')
                                  YBDNavigatorHelper.navigateTo(
                                      context, YBDCommonUtil.getPhotoViewUrl([aPath], 0, download: false));
                              },
                              child: YBDRoundAvatar(
                                // 头像
                                widget.userInfo.headimg ?? '',
                                avatarWidth: 150,
                                sex: widget.userInfo.sex ?? 0,
                                lableSrc: (widget.userInfo.vip ?? 0) == 0
                                    ? ""
                                    : "assets/images/vip_lv${widget.userInfo.vip}.png",
                                labelWitdh: 0,
                                userId: widget.userInfo.id,
                                needNavigation: false,
                                scene: "C",
                                showVip: false,
                                labelPadding: EdgeInsets.only(right: ScreenUtil().setWidth(0)),
                                levelFrame: widget.userInfo.headFrame,
                                sideBorder: true,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          width: ScreenUtil().setWidth(520),
                          // padding: EdgeInsets.only(left: ScreenUtil().setWidth(7)),
                          // 名字ID标签等信息

                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              // 名字
                              Container(
                                width: ScreenUtil().setWidth(400),
                                child: Row(mainAxisSize: MainAxisSize.min, children: [
                                  SizedBox(
                                    width: ScreenUtil().setWidth(40),
                                  ),
                                  Container(
                                    child: Text(
                                      widget.userInfo.nickname ?? '',
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(32),
                                        color: YBDActivitySkinRoot().curAct().userProfileNameColor(context),
                                        fontWeight: FontWeight.bold,
                                      ),
                                      overflow: TextOverflow.fade,
                                      maxLines: 1,
                                    ),
                                    constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(340), minWidth: 0),
                                  ),
                                  if (_certificationInfos != null) ...[
                                    SizedBox(
                                      width: ScreenUtil().setWidth(6),
                                    ),
                                    YBDNetworkImage(
                                      imageUrl: _certificationInfos!.icon!,
                                      width: ScreenUtil().setWidth(34),
                                    )
                                  ]
                                ]),
                              ),

                              // 靓号
                              Container(
                                width: textWidth,
                                child: _uniqueIdItem(),
                              ),
                              SizedBox(
                                height: ScreenUtil().setWidth(isAgent ? 8 : 16),
                              ),
                              // ID和复制按钮
                              _getIdArea(),
                              SizedBox(
                                height: ScreenUtil().setWidth(isAgent ? 8 : 16),
                              ),
                              // 等级标签栏
                              YBDProfileTagInfo(
                                  widget.userInfo, widget.isOfficialTalent, isCerRequested, _certificationInfos),
                              ..._getCertified(),
                              isAgent
                                  ? Padding(
                                      padding: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(40),
                                        top: ScreenUtil().setWidth(4),
                                      ),
                                      child: Row(
                                        children: [_getAgentIcon()],
                                      ),
                                    )
                                  : SizedBox(
                                      height: ScreenUtil().setWidth(0),
                                    ),
                              // 个人语录
                              if (isAgent)
                                SizedBox(
                                  height: ScreenUtil().setWidth(6),
                                ),
                              YBDVipIcon(
                                widget.userInfo.vipIcon ?? '',
                                padding:
                                    EdgeInsets.only(top: ScreenUtil().setWidth(0), left: ScreenUtil().setWidth(40)),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 20,
                    left: ScreenUtil().setWidth(20),
                    child: YBDTopTalentItem(
                      widget.userInfo.extendVo == null ? 0 : widget.userInfo.extendVo!.activityRanking,
                      width: ScreenUtil().setWidth(190),
                      height: ScreenUtil().setWidth(76),
                      font: 20,
                    ),
                  ),
                ]),
                // SizedBox(height: ScreenUtil().setWidth(20)),
                SizedBox(height: ScreenUtil().setWidth(30)),
              ],
            ),
          ],
        ),
      ),
    );
  }
  void buildicKiVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 靓号
  Widget _uniqueIdItem() {
    String? luckyID = YBDCommonUtil.getUserLuckyID(context, widget.userInfo.id);

    return luckyID != null && luckyID.isNotEmpty
        ? Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Center(
                    child: Image(
                        image: AssetImage("assets/images/lucky_id_bg.webp"),
                        fit: BoxFit.fill,
                        width: ScreenUtil().setWidth(120),
                        height: ScreenUtil().setWidth(36)),
                  ),
                  Center(
                    child: Container(
                      padding: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
                      width: ScreenUtil().setWidth(120),
                      height: ScreenUtil().setWidth(36),
                      child: Center(
                        child: Text(
                          luckyID,
                          textAlign: TextAlign.right,
                          style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(16)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          )
        : Container();
  }
  void _uniqueIdItemPOr17oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
