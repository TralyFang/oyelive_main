import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import 'bloc/follow_ybd_user_bloc.dart';

/// 其他人的 profile 底部两个按钮
class YBDUserProfileBottom extends StatefulWidget {
  YBDUserProfileBottom();

  @override
  YBDUserProfileBottomState createState() => new YBDUserProfileBottomState();
}

class YBDUserProfileBottomState extends State<YBDUserProfileBottom> {
  @override
  void initState() {
    super.initState();
    context.read<YBDFollowUserBloc>().add(FollowUserEvent.Request);
  }
  void initStategK6aSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<YBDFollowUserBloc, YBDFollowUserBlocState>(builder: (context, state) {
      return Padding(
        padding: EdgeInsets.all(ScreenUtil().setWidth(22)),
//        decoration: BoxDecoration(color: Colors.transparent),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // SizedBox(width: ScreenUtil().setWidth(30)),
            YBDScaleAnimateButton(
              // 聊天按钮
              onTap: () {
                logger.v('clicked say hello button');
                YBDNavigatorHelper.navigateTo(
                  context,
                  YBDNavigatorHelper.inbox_private_message + "/${context.read<YBDFollowUserBloc>().userId}/}",
                );

                YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
                    location: "user_profile_page",
                    itemID: '${context.read<YBDFollowUserBloc>().userId}',
                    itemName: 'private_message'));
              },
              child: _gradientRadiusButton(state.status),
            ),
            SizedBox(width: ScreenUtil().setWidth(20)),
            YBDScaleAnimateButton(
              // 关注、取关、好友按钮，同一个组件显示的三种状态
              onTap: () {
                switch (state.status) {
                  case FollowStatus.Loading:
                  case FollowStatus.Following:
                  case FollowStatus.UnFollowing:
                    break;
                  case FollowStatus.Followed:
                    context.read<YBDFollowUserBloc>().add(FollowUserEvent.UnFollow);
                    break;
                  case FollowStatus.UnFollowed:
                    context.read<YBDFollowUserBloc>().add(FollowUserEvent.Follow);
                    break;
                  case FollowStatus.Friend:
                    context.read<YBDFollowUserBloc>().add(FollowUserEvent.UnFriend);
                    break;
                }
              },
              child: whiteRadiusButton(state.status),
            ),
            // SizedBox(width: ScreenUtil().setWidth(30)),
          ],
        ),
      );
    });
  }

  /// 白色圆角按钮
  Container whiteRadiusButton(FollowStatus status) {
    String btnTitle = '';
    String btnIcon = '';
    Color bgColor = Colors.white;

    switch (status) {
      case FollowStatus.Loading:
        btnTitle = translate('Loading');
        break;
      case FollowStatus.Following:
        btnTitle = translate('following');
        bgColor = Color(0xffC8C8C8);
        break;
      case FollowStatus.UnFollowing:
        btnTitle = translate('unfollowing');
        bgColor = Color(0xffC8C8C8);
        break;
      case FollowStatus.Followed:
        btnTitle = translate('following');
        btnIcon = 'assets/images/liveroom/icon_following@2x.webp';
        break;
      case FollowStatus.UnFollowed:
        btnTitle = translate('follow');
        btnIcon = 'assets/images/liveroom/icon_follow@2x.webp';
        break;
      case FollowStatus.Friend:
        btnTitle = translate('friends');
        btnIcon = 'assets/images/liveroom/icon_friends@2x.webp';
        break;
    }

    return Container(
      padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
      decoration: BoxDecoration(
          border: Border.all(color: Color(0xffcccccc), width: YBDActivitySkinRoot().curAct().borderW() as double),
          color: bgColor,
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(37))),
          boxShadow: [
            BoxShadow(
                color: Color(0xff000000).withOpacity(0.5),
                offset: Offset(0, ScreenUtil().setWidth(2)),
                blurRadius: ScreenUtil().setWidth(4))
          ]),
      width: ScreenUtil().setWidth(310),
      height: ScreenUtil().setWidth(74),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          btnIcon.isNotEmpty
              ? Image.asset(
                  btnIcon,
                  width: ScreenUtil().setWidth(40),
                  height: ScreenUtil().setWidth(40),
                )
              : Container(),
          SizedBox(width: ScreenUtil().setWidth(10)),
          Text(
            btnTitle,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: ScreenUtil().setSp(28),
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  /// 彩色按钮
  Container _gradientRadiusButton(FollowStatus status) {
    String btnTitle = '';
    String btnIcon = '';

    // if (status == FollowStatus.Friend) {
    //
    // } else {
    //   btnTitle = translate('say_hello');
    //   btnIcon = 'assets/images/say_hello_icon.png';
    // }
    btnTitle = translate('chat');
    btnIcon = 'assets/images/liveroom/icon_chat 2@2x.webp';
    return Container(
      padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
      decoration: BoxDecoration(
          border: Border.all(color: Color(0xffcccccc), width: YBDActivitySkinRoot().curAct().borderW() as double),
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(37)))),
          boxShadow: [
            BoxShadow(
                color: Color(0xff000000).withOpacity(0.5),
                offset: Offset(0, ScreenUtil().setWidth(2)),
                blurRadius: ScreenUtil().setWidth(4))
          ]),
      width: ScreenUtil().setWidth(310),
      height: ScreenUtil().setWidth(74),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            btnIcon,
            width: ScreenUtil().setWidth(40),
            height: ScreenUtil().setWidth(40),
          ),
          SizedBox(width: ScreenUtil().setWidth(10)),
          Text(
            btnTitle,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: ScreenUtil().setSp(28),
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
