import 'dart:async';

import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/status/entity/block_ybd_user_entity.dart';

import '../../../../../module/status/status_ybd_api_helper.dart';

class YBDBlockController extends GetxController {
  int? state;

  int? toId;

  load(int toId) async {
    this.toId = toId;
    logger.v('load block status: $toId');
    YBDBlockUserEntity? entity = await YBDStatusApiHelper.blockUserSearch(Get.context, 0, toId);
    state = entity?.data?.state;
  }

  reLoad() async {
    YBDBlockUserEntity? ent = await YBDStatusApiHelper.blockUserSearch(Get.context, state == 1 ? 2 : 1, toId);
    YBDToastUtil.toast(ent?.code == Const.HTTP_SUCCESS_NEW ? translate('operation_success') : translate('operation_fail'));
    if (ent?.code == Const.HTTP_SUCCESS_NEW) {
      state = state == 1 ? 2 : 1;
    }
  }
}
