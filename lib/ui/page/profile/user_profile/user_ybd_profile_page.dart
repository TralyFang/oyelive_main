import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/profile/user_profile/getx/block_ybd_controller.dart';
import 'package:oyelive_main/ui/widget/profile_ybd_operator_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/agency/agency_ybd_api_helper.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/inbox/message_ybd_helper.dart';
import '../../../../module/status/entity/status_ybd_entity.dart';
import '../../../../module/status/entity/status_ybd_list_entity.dart';
import '../../../../module/status/status_ybd_api_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/colored_ybd_safe_area.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';
import '../../../widget/profile_ybd_report_dialog.dart';
import '../../../widget/status_ybd_loading_circle.dart';
import '../../status/handler/status_ybd_delete_event.dart';
import 'bloc/follow_ybd_user_bloc.dart';
import 'user_ybd_profile_baggage.dart';
import 'user_ybd_profile_bottom.dart';
import 'user_ybd_profile_header.dart';
import 'user_ybd_profile_nav.dart';

/// 其他人的 profile 页面
class YBDUserProfilePage extends StatefulWidget {
  final int userId;

  YBDUserProfilePage(this.userId);

  @override
  YBDUserProfilePageState createState() => YBDUserProfilePageState();
}

class YBDUserProfilePageState extends BaseState<YBDUserProfilePage> {
  RefreshController _refreshController = new RefreshController();

  /// 是否为签约主播
  bool? isOfficialTalent;
  YBDUserInfo _userInfo = YBDUserInfo();

  int currentPage = 1;
  bool hasNext = true;

  final pageSize = 10;

  /// 先请求接口再渲染页面
  bool requested = false;

  List<YBDStatusInfo?>? rowsData;

  late YBDBlockController blockController;

  @override
  void initState() {
    super.initState();

    // 页面显示初始值
    _userInfo.id = widget.userId;
    _userInfo.nickname = '-';
    _userInfo.concern = 0;
    _userInfo.fans = 0;
    _userInfo.vip = 0;
    _userInfo.sex = 0;
    _userInfo.comment = translate('default_comment');

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      // 请求动态列表数据
      _refreshAction();

      // 请求用户信息
      _requestUserInfo();

      // 请求房间标签
      _requestOfficialTalent();

      YBDMessageHelper.checkSingleContact(context, widget.userId);

      blockController = Get.put(YBDBlockController());
      blockController.load(widget.userId);
    });

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE,
        location: "user_profile_page", itemID: '${widget.userId}'));
  }

  void initStateGYGdCoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  Widget myBuild(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    Decoration decoration = BoxDecoration(
      gradient: LinearGradient(
        colors: YBDActivitySkinRoot().curAct().userProfileColors(),
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
    return MultiBlocProvider(
      providers: [
        BlocProvider<YBDFollowUserBloc>(create: (context) {
          logger.v('create popular bloc');
          YBDFollowUserBloc bloc =
              YBDFollowUserBloc(context, '${widget.userId}');
          return bloc;
        }),
      ],
      child: YBDColoredSafeArea(
        top: Platform.isIOS ? false : true,
        bottom: Platform.isIOS ? false : true,
        child: Scaffold(
          body: Container(
            decoration: decoration,
            child: Stack(
//              alignment: Alignment.center,
              children: <Widget>[
                Positioned.fill(
                  bottom: 0,
                  child: Container(
                    // 动态列表刷新容器
//                    padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(118)),
                    child: SmartRefresher(
                      controller: _refreshController,
                      header: YBDActivitySkinRoot()
                          .curAct()
                          .activityRefreshHeader(),
                      // 列表尾部是否显示 no more data 提示文字
                      footer: YBDMyRefreshIndicator.myFooterWithDataStatus(
                        _shouldShowNoMoreDataText(),
                      ),
                      enablePullDown: true,
                      enablePullUp: true,

                      onRefresh: () {
                        logger.v("user profile page pull down refresh");
                        _refreshAction();
                      },
                      onLoading: () {
                        logger.v("user profile page pull up load more");
                        _nextPage();
                      },
                      // 动态列表
                      child: _statusListView(),
                    ),
                  ),
                ),
                // Positioned(
                //   // 导航栏
                //   top: 0,
                //   child: _appStatusAndNavBar(),
                // ),
                Positioned(
                  // 底部 follow 按钮
                  bottom: ScreenUtil().setWidth(0),
                  left: 0,
                  right: 0,
                  child: YBDUserProfileBottom(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void myBuildFM0oSoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 举报按钮的响应事件content + "|" + reportId + "|" + nickname
  _clickedReportAction() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return YBDProfileOperatorDialog(
            callback: (title) {
              Navigator.pop(context);
              if (title == report) {
                showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (context) {
                      return YBDProfileReportDialog(
                        (String reportTitle) async {
                          logger.v(
                              'report dialog ok button callback : $reportTitle');
                          // 隐藏举报弹框
                          Navigator.pop(context);

                          String? nickName = '';
                          if (null != _userInfo.nickname) {
                            nickName = _userInfo.nickname;
                          }
                          bool result = false;

                          if (reportTitle == 'Block') {
                            await YBDSPUtil.blockUser('${widget.userId}');
                            eventBus.fire(YBDStatusDeleteEvent('',
                                userId: '${widget.userId}'));

                            // 退出用户信息页面
                            Navigator.pop(context);
                          } else {
                            result = await ApiHelper.complain(
                              context,
                              'user',
                              widget.userId,
                              '$reportTitle|${widget.userId}|$nickName',
                            );

                            if (result) {
                              YBDToastUtil.toast(translate('under_review'));
                            } else {
                              YBDToastUtil.toast(translate('system_busy'));
                            }
                          }
                        },
                        () {
                          logger.v('report dialog cancel button callback');
                          // 隐藏举报弹框
                          Navigator.pop(context);
                        },
                        showBlockBtn: false,
                      );
                    });
              }
            },
          );
        });
  }

  /// 请求动态列表
  Future<YBDStatusListEntity?> _requestStatusList(int page) async {
    YBDStatusListEntity? result =
        await YBDStatusApiHelper.queryStatusWithUserId(context, widget.userId,
            page: page, pageSize: pageSize, status: 1); //状态(0:待审，1发布，2拒绝)
    if (page == 1) _refreshController.resetNoData();

    if (null != result) {
      if (result.code != Const.HTTP_SUCCESS_NEW) {
        if (result.code != Const.HTTP_SESSION_TIMEOUT_NEW)
          YBDToastUtil.toast(result.message ?? '');
        return null;
      } else {
        hasNext = !(result.data!.xList!.rows!.length < pageSize);

        return result;
      }
    } else {
      logger.v('result is empty');
      return null;
    }
  }

  /// 请求用户信息
  _requestUserInfo() {
    ApiHelper.queryUserInfo(context, widget.userId).then((value) {
      if (null != value) {
        if (mounted) {
          setState(() {
            _userInfo = value;
          });
        }
      }
    });
  }

  /// 请求是否签约主播
  _requestOfficialTalent() {
    YBDAgencyApiHelper.isOfficialTalent(context, widget.userId).then((value) {
      if (value != null) {
        if (mounted) {
          setState(() {
            isOfficialTalent = value;
          });
        }
      }
    });
  }

  /// 下拉刷新
  Future<YBDStatusListEntity?> _refreshAction() async {
    YBDStatusListEntity? result = await _requestStatusList(1);

    if (_refreshController.isRefresh) {
      _refreshController.refreshCompleted();
    }

    if (null != result && result.code == Const.HTTP_SUCCESS_NEW) {
      _refreshController.resetNoData();
      if (mounted) {
        setState(() {
          rowsData = result.data!.xList!.rows;
          currentPage = 1;
          requested = true;
          logger.v('refresh page success set requested : $requested');
        });
      }
      return result;
    } else {
      return null;
    }
  }

  /// 上拉加载更多动态
  _nextPage() async {
    if (hasNext) {
      YBDStatusListEntity? result = await _requestStatusList(++currentPage);

      if (null != result && result.code == Const.HTTP_SUCCESS_NEW) {
        logger.v('next page loading success');
        _refreshController.loadComplete();
        rowsData!.addAll(result.data!.xList!.rows!);
        if (mounted) {
          setState(() {
            requested = true;
            logger.v('next page success set requested : $requested');
          });
        }
      } else {
        logger.v('next page loading failed');
        _refreshController.loadFailed();
        if (mounted) {
          setState(() {
            requested = true;
            logger.v('next page failed set requested : $requested');
          });
        }
      }
    } else {
      // 没有更多
      if (_refreshController.isLoading) {
        logger.v('next page is loading');
        _refreshController.loadNoData();
      } else {
        logger.v('next page is not loading');
      }
    }
  }

  /// 状态栏和导航栏
  Widget _appStatusAndNavBar() {
    if (Platform.isIOS) {
      ///iphone 非刘海 的 被遮住了
      return Positioned(
        top: ScreenUtil().statusBarHeight,
        child: YBDUserProfileNav(() {
          logger.v('back button callback');
          YBDNavigatorHelper.popPage(context);
        }, () {
          logger.v('report button callback');
          _clickedReportAction();
        }),
      );
    }
    return YBDUserProfileNav(() {
      logger.v('back button callback');
      YBDNavigatorHelper.popPage(context);
    }, () {
      logger.v('report button callback');
      _clickedReportAction();
    });
  }

  void _appStatusAndNavBarJ7kOEoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 头部海报部分
  Widget _headerImageContainer() {
    return YBDUserProfileHeader(_userInfo, isOfficialTalent ?? false);
  }

  /// 用户信息展示部分
  Widget _userInfoContainer() {
    return SizedBox();
    // return YBDUserProfileInfo(_userInfo, isOfficialTalent);
  }

  void _userInfoContainerliMPvoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 背包栏
  Widget _baggageContainer() {
    return Padding(
      padding: EdgeInsets.only(
          top: ScreenUtil().setWidth(30), bottom: ScreenUtil().setWidth(30)),
      child: YBDUserProfileBaggage(_userInfo),
    );
  }

  /// 动态为空显示占位图
  Widget _statusListView() {
    if (null == rowsData || rowsData!.isEmpty) {
      // 没有动态数据时动态列表区域展示的 UI
      logger.v('status list is empty show placeholder');
      return Container(
        height: ScreenUtil().setWidth(1600),
        child: Stack(
          children: <Widget>[
            _headerImageContainer(),
            _appStatusAndNavBar(),
            // 用户信息部分
            Positioned.fill(
              top: ScreenUtil().setWidth(680),
              child: Container(
                height: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                      top: Radius.circular(ScreenUtil().setWidth(40))),
                  gradient: LinearGradient(
                    colors: YBDActivitySkinRoot().curAct().userProfileColors(),
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                ),
                child: Column(
                  children: <Widget>[
                    _userInfoContainer(),
                    Expanded(
                      child: Column(
                        children: [
                          _baggageContainer(),
                          // 初始状态显示加载框，否则显示缺省图
                          requested
                              ? _statusEmptyPlaceholder()
                              : YBDStatusLoadingCircle(30, () {
                                  logger.v(
                                      'status loading circle timeout callback');

                                  if (mounted) {
                                    setState(() {
                                      requested = true;
                                      logger.v(
                                          'timeout set requested : $requested');
                                    });
                                  }
                                }),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    }

    // 给列表添加表头
    int itemCount = 1;
    if (null != rowsData && rowsData!.isNotEmpty) {
      itemCount += rowsData!.length;
    }
    logger.v('list item count : $itemCount');

    return SingleChildScrollView(
      child: Stack(
        children: <Widget>[
          _headerImageContainer(),
          _appStatusAndNavBar(),
          // 用户信息部分
          Container(
            margin: EdgeInsets.only(top: ScreenUtil().setWidth(680)),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                  top: Radius.circular(ScreenUtil().setWidth(40))),
              gradient: LinearGradient(
                colors: YBDActivitySkinRoot().curAct().userProfileColors(),
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            child: Column(
              children: <Widget>[
                _userInfoContainer(),
                _baggageContainer(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _listViewItemaQjGBoyelive(index) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 动态空列表
  Container _statusEmptyPlaceholder() {
    return Container(
      padding: EdgeInsets.all(30),
      child: Column(
        children: [
          Image.asset(
            "assets/images/user_profile_empty_status.png",
            width: ScreenUtil().setWidth(244),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(30),
          ),
          Text(
            translate('no_post'),
            style: TextStyle(
                color: YBDActivitySkinRoot().curAct().userProfileFontColor()),
          ),
        ],
      ),
    );
  }

  /// 列表尾部是否显示 No more data
  bool _shouldShowNoMoreDataText() {
    // 没有请求过网络不显示
    if (!requested) {
      logger.v('not show no more data : not requested network');
      logger.v('should requested : $requested');
      return false;
    } else if (null == rowsData || rowsData!.isEmpty) {
      // 请求过网络，数据为空不显示
      logger.v('not show no more data : no data');
      return false;
    } else {
      // 请求过网络，数据不为空显示
      logger.v('show no more data');
      return true;
    }
  }

  void _shouldShowNoMoreDataTextcZva3oyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    Get.delete<YBDBlockController>();
  }
}
