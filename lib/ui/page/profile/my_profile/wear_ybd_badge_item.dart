import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';

import 'badge_ybd_page.dart';

class YBDWearBadgeItem extends StatelessWidget {
  List<YBDBadgeInfo?>? badges;
  bool needReload;

  YBDWearBadgeItem(this.badges, {this.needReload = false});

  @override
  Widget build(BuildContext context) {
    //三目太多也不好阅读
    if (badges == null || badges?.length == 0)
      return Container(
        width: 0,
      );
    if (badges?.first?.iconPng != null)
      return YBDNetworkImage(
        imageUrl: badges?.first?.iconPng ?? '',
        height: ScreenUtil().setWidth(35),
        fit: BoxFit.fitHeight,
        errorWidget: (context, url, error) => Container(),
      );
    return (badges?.first?.icon?.endsWith('.svga') ?? false)
        ? YBDBadgeAnimationView(
            ScreenUtil().setWidth(30),
            url: badges?.first?.icon,
            needReload: needReload,
          )
        : YBDNetworkImage(
            imageUrl: badges?.first?.icon ?? '',
            height: ScreenUtil().setWidth((badges?.first?.icon ?? '').length == 0 ? 0 : 35),
            fit: BoxFit.fitHeight,
            errorWidget: (context, url, error) => Container(),
          );
  }
  void buildBwmrloyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
