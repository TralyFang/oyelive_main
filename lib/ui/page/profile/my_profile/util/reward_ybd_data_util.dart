import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/aristocracy_ybd_const.dart';

import '../../../../../common/util/image_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../home/entity/car_ybd_list_entity.dart';
import '../../../home/entity/daily_ybd_check_entity.dart';
import '../../../home/entity/gift_ybd_list_entity.dart';

/// 处理每日签到奖励数据的工具类
class YBDRewardDataUtil {
  /// 返回签到 item 的数据
  /// [dailyCheckTask] 对应连续签到任务 item 的一个签到任务
  static YBDRewardData rewardData(
    BuildContext context, {
    required YBDDailyCheckRecordExtendRewardListDailyReward dailyCheckTask,
    required List<YBDGiftListRecordGift?>? giftList,
    required List<YBDCarListRecord?>? carList,
  }) {
    String? name;
    String? img;

    if (dailyCheckTask.item == 'vip') {
      if (dailyCheckTask.itemId == 1) {
        name = '${dailyCheckTask.name} x${dailyCheckTask.days}d';
        // img = 'assets/images/vip_lv1.png';
        img = image_prefix + "icon_vip1.png";
      } else {
        name = '${dailyCheckTask.name} x${dailyCheckTask.days}d';
        // img = 'assets/images/vip_lv2.png';
        img = image_prefix + "icon_vip2.png";
      }
    } else if (dailyCheckTask.item == 'gift') {
      name = dailyCheckTask.item;
      logger.v('dailyCheckTask itemId:${dailyCheckTask.itemId}');
      // if ((dailyCheckTask?.icon ?? '').isNotEmpty) {
      name = '${dailyCheckTask.name} x${dailyCheckTask.number}';
      img = YBDImageUtil.gift(context, dailyCheckTask.icon, 'B');
      // } else { //icon、name 已经从服务端获取 else代码 可以去掉
      //   if (null != giftList && giftList.isNotEmpty) {
      //     YBDGiftListRecordGift gift = giftList.firstWhere((element) {
      //       if (element.id == dailyCheckTask.itemId) {
      //         return true;
      //       } else {
      //         return false;
      //       }
      //     }, orElse: () => null);
      //
      //     if (null != gift) {
      //       name = '${gift.name} x${dailyCheckTask.number}';
      //       img = YBDImageUtil.gift(context, gift.img, 'B');
      //       logger.v('gift img url: $img, orgimg: ${gift.img}');
      //     } else {
      //       logger.v('gift is null');
      //     }
      //   } else {
      //     logger.v('gift list is empty');
      //   }
      // }
    } else if (dailyCheckTask.item == 'entrances') {
      name = dailyCheckTask.item;
      // if ((dailyCheckTask?.icon ?? '').isNotEmpty) {
      name = '${dailyCheckTask.name} x${dailyCheckTask.days}d';
      img = YBDImageUtil.car(context, dailyCheckTask.icon, 'B');
      //icon、name v2.6.0已经从服务端获取 else代码 可以去掉
      // } else {
      //   if (null != carList && carList.isNotEmpty) {
      //     YBDCarListRecord car = carList.firstWhere((element) {
      //       if (element.id == dailyCheckTask.itemId) {
      //         return true;
      //       } else {
      //         return false;
      //       }
      //     }, orElse: () => null);
      //
      //     if (null != car) {
      //       name = '${car.name} x${dailyCheckTask.days}d';
      //       img = YBDImageUtil.car(context, car.img, 'B');
      //     } else {
      //       logger.v('car is null');
      //     }
      //   } else {
      //     logger.v('car list is empty');
      //   }
      // }
    } else {
      name = dailyCheckTask.name ?? '';
      img = 'assets/images/topup/y_top_up_beans@2x.webp';
    }

    // 是否为本地图片
    bool isLocalImg = true;

    if (dailyCheckTask.icon?.startsWith("http") ?? false) {
      img = dailyCheckTask.icon;
    }

    if (null != img && img.startsWith('http')) {
      isLocalImg = false;
    }

    // 是否完成签到
    bool claimed = dailyCheckTask.receive ?? false;

    return YBDRewardData(
      name: name,
      img: img,
      isLocalImg: isLocalImg,
      isClaimed: claimed,
    );
  }

  /// 当天签到的数据
  static YBDRewardData? currentDayRewardData(
    BuildContext context, {
    required List<YBDDailyCheckRecordExtendRewardListDailyReward?>? dailyCheckTasks,
    required List<YBDGiftListRecordGift?>? giftList,
    required List<YBDCarListRecord?>? carList,
  }) {
    if (null != dailyCheckTasks) {
      // 第一个没完成签到的任务
      YBDDailyCheckRecordExtendRewardListDailyReward? task;
      int index = 0;

      for (int i = 0; i < dailyCheckTasks.length; i++) {
        if ((dailyCheckTasks[i]!.receive ?? false) == false) {
          task = dailyCheckTasks[i];
          index = i;
          break;
        }
      }

      // 获取任务数据
      final rewardData = YBDRewardDataUtil.rewardData(
        context,
        dailyCheckTask: task!,
        giftList: giftList,
        carList: carList,
      );
      rewardData.index = index;
      return rewardData;
    } else {
      return null;
    }
  }
}

/// 签到奖励数据
class YBDRewardData {
  /// 奖励名称
  String? name;

  /// 奖励图片
  String? img;

  /// 是否为本地图片
  bool isLocalImg;

  /// 是否完成签到
  bool isClaimed;

  /// 连续签到第几天
  int index;

  YBDRewardData({
    required this.name,
    required this.img,
    required this.isLocalImg,
    required this.isClaimed,
    this.index = 0,
  });
}
