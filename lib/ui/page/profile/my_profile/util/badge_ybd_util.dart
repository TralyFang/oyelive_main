import 'dart:async';

import 'dart:convert';

import 'package:svgaplayer_flutter/proto/svga.pb.dart';
import '../../../../../common/util/file_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../room/teen_patti/entity/teen_ybd_patti_info.dart';
import '../../../room/teen_patti/enum/teen_ybd_state.dart';

class YBDBadgeUtil{

  static YBDBadgeUtil? _instance;

  YBDBadgeUtil._();

  Map map = {};

  setUrl(String? url,MovieEntity? videoItem){
    map[url] = videoItem;
    // YBDFileUtil.writeJsonToFile(json.encode(videoItem), "Video_JSON");
  }

  MovieEntity? getVideoItem(String? url){
    return map[url];
  }

  Future _init() async{
    _instance = await YBDBadgeUtil.getInstance();
  }

  static YBDBadgeUtil? getInstance(){
    if (_instance == null){
      var instance = YBDBadgeUtil._();
      // await instance._init();
      _instance = instance;
    }
    return _instance;
  }
}

class YBDBagdeChangeEvent{}