import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:svgaplayer_flutter/parser.dart';
import 'package:svgaplayer_flutter/player.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../main.dart';
import '../../../../../module/inbox/entity/message_ybd_list_entity.dart';
import '../../../../widget/sharing_ybd_widget.dart';

class YBDTopUpBadgesAlertUtil {
  static String systemlayout = "text_user_badge_get";

  static List<YBDBadgeObj> objList = [];

  static bool _showGoogleAlert = false;

  static String? image = '';

  static changeGoogleAlertState(bool isShow) {
    _showGoogleAlert = isShow;
    if (!isShow) {
      alertShow();
    }
  }

  static alertShow() {
    if (YBDTopUpBadgesAlertUtil.objList.length == 0) return;
    YBDBadgeObj cobj = YBDTopUpBadgesAlertUtil.objList.first;
    showBadgeView(cobj);
    YBDTopUpBadgesAlertUtil.objList.remove(cobj);
  }

  static receiveMessage(YBDMessageListDataPullMessage message) {
    if (message.content != null && ((message.content!.layout ?? '') == YBDTopUpBadgesAlertUtil.systemlayout)) {
      // print("message queueid : ${message.queueId}, ${YBDTopUpBadgesAlertUtil.queueId}");
      // if(message.queueId == YBDTopUpBadgesAlertUtil.queueId)return;
      //queueId 无效
      // queueId = message.queueId;
      logger.v("receive Badge msg :${message.content!.layout}");
      if (message.content!.attributes is Map) {
        YBDBadgeObj obj = YBDBadgeObj.getObj(message.content!.attributes);
        if (obj.image == YBDTopUpBadgesAlertUtil.image) return;
        YBDTopUpBadgesAlertUtil.image = obj.image;
        Future.delayed(Duration(seconds: 3)).then((_) {
          YBDTopUpBadgesAlertUtil.image = '';
        });
        if (obj.channel != null && obj.channel!.toLowerCase() == 'google' && _showGoogleAlert) {
          YBDTopUpBadgesAlertUtil.objList.add(obj);
          return;
        }
        ;
        showBadgeView(obj);
      }
    }
  }

  static test() {
    YBDBadgeObj obj = YBDBadgeObj.getObj({
      "image": "https://oyetalk-test.s3.cn-northwest-1.amazonaws.com.cn/icon/badge/badge_4.svga",
      "title": "hhhh",
      "content": "aaaa",
      "channel": "google"
    });
    if (obj.channel != null && obj.channel == 'google' && _showGoogleAlert) {
      YBDTopUpBadgesAlertUtil.objList.add(obj);
      return;
    }
    ;
    if (obj.image == YBDTopUpBadgesAlertUtil.image) return;
    YBDTopUpBadgesAlertUtil.image = obj.image;
    showBadgeView(obj);
  }

  static showBadgeView(YBDBadgeObj obj) {
    logger.v('badge alert begin with image path: ${obj.image}');
    eventBus.fire(YBDBadgeReload());

    // if (GetPlatform.isAndroid) {
    //   print("showMedalDialog ------medalUrl:" + obj.image + ",medalText:" + obj.context);
    // FlutterBoost.singleton
    //     .open(getNativePageUrl(NativePageName.BADGE_DIALOG), urlParams: {'image': obj.image, 'context': obj.title});
    //   return;
    // }

    showGeneralDialog(
        context: navigatorKey.currentState!.overlay!.context,
        barrierDismissible: false,
        barrierLabel: '',
        barrierColor: Colors.black.withOpacity(0.85),
        transitionDuration: Duration(milliseconds: 200),
        pageBuilder: (context, animation, sectionAnimation) {
          return Container(
            child: Center(child: YBDTopUpBadgeAlertView(obj)),
          );
        });
  }
}

class YBDTopUpBadgeAlertView extends StatefulWidget {
  YBDBadgeObj obj;
  VoidCallback? popBack;

  YBDTopUpBadgeAlertView(this.obj, {this.popBack});

  @override
  _YBDTopUpBadgeAlertViewState createState() => _YBDTopUpBadgeAlertViewState();
}

class _YBDTopUpBadgeAlertViewState extends State<YBDTopUpBadgeAlertView> with TickerProviderStateMixin {
  /// 动画控制器
  late SVGAAnimationController _animationController;

  /// 播放 456 勋章 动画
  playBadgeAnimation() async {
    if (!widget.obj.image!.endsWith('.svga')) return;
    _animationController = SVGAAnimationController(vsync: this);
    final videoItem = await SVGAParser.shared.decodeFromURL(widget.obj.image!);
    _animationController.videoItem = videoItem;
    _animationController.repeat();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }
  void disposexcpY7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    playBadgeAnimation();
  }
  void initStateK9HChoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(622),
      height: ScreenUtil().setHeight(541 + 27 + 67),
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(ScreenUtil().setWidth(32)), color: Colors.transparent),
      child: Stack(
        children: [
          Positioned(
            child: Image.asset('assets/images/profile/badge_show_bottom.webp'),
            bottom: ScreenUtil().setHeight(27 + 67 + 37 + (MediaQuery.of(context).padding.top == 20 ? 0 : 20)),
            left: 0,
            right: 0,
          ),
          itemColumn(context),
          Positioned(
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
                // eventBus.fire(YBDBadgeReload());
                YBDTopUpBadgesAlertUtil.alertShow();
                YBDTopUpBadgesAlertUtil.image = '';
              },
              child: Container(
                padding: EdgeInsets.only(
                    left: ScreenUtil().setWidth(10),
                    right: ScreenUtil().setWidth(10),
                    top: ScreenUtil().setWidth(10),
                    bottom: ScreenUtil().setWidth(10)),
                width: ScreenUtil().setWidth(52),
                height: ScreenUtil().setWidth(52),
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white, borderRadius: BorderRadius.circular(ScreenUtil().setWidth(32) / 2.0)),
                    child: Image.asset('assets/images/icon_close.png')),
              ),
            ),
            top: ScreenUtil().setWidth(37),
            right: ScreenUtil().setWidth(77),
          )
        ],
      ),
    );
  }
  void buildGQVfboyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget itemColumn(BuildContext context) {
    logger.v('badge alert image path: ${widget.obj.image}');
    return Container(
      alignment: Alignment.center,
      // padding: EdgeInsets.only(
      //     left: ScreenUtil().setWidth(73), right: ScreenUtil().setWidth(72)),
      child: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).padding.top == 20 ? 0 : ScreenUtil().setHeight(59),
          ),
          Container(
              width: ScreenUtil().setWidth(363),
              height: ScreenUtil().setWidth(363),
              child: widget.obj.image!.endsWith('.svga')
                  ? SVGAImage(_animationController)
                  : YBDNetworkImage(
                      imageUrl: widget.obj.image!,
                      width: ScreenUtil().setWidth(363),
                      height: ScreenUtil().setWidth(363),
                      fit: BoxFit.cover,
                    )),
          SizedBox(
            height: ScreenUtil().setHeight(32),
          ),
          Container(
              child: Text(
            '${translate('congratulations')}\n ${translate('you_have_got_the')} ${widget.obj.title} ${translate('badge')}',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(28),
                color: Colors.white,
                decoration: TextDecoration.none,
                fontWeight: FontWeight.normal),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          )),
          SizedBox(
            height: ScreenUtil().setHeight(57),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(ScreenUtil().setHeight(67) / 2.0),
            child: GestureDetector(
              onTap: () {
                // Navigator.pop(context);
                showModalBottomSheet(
                    context: context, //navigatorKey.currentState.overlay.context,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(ScreenUtil().setWidth(16)),
                          topRight: Radius.circular(ScreenUtil().setWidth(16))),
                    ),
                    builder: (BuildContext context) {
                      return YBDSharingWidget(title: widget.obj.title, badgeUrl: widget.obj.image);
                    });
              },
              child: Container(
                  alignment: Alignment.center,
                  width: ScreenUtil().setWidth(380),
                  height: ScreenUtil().setHeight(67),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xFF5E94E7),
                        Color(0xFF47CDCC),
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  child: Text(
                    translate('share'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(28),
                        color: Color(0xD9FFFFFF),
                        decoration: TextDecoration.none,
                        fontWeight: FontWeight.normal),
                  )),
            ),
          ),
        ],
      ),
    );
  }
  void itemColumn9P6w3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDBadgeObj {
  String? image;
  String? context;
  String? title;
  String? channel;

  static YBDBadgeObj getObj(Map map) {
    YBDBadgeObj obj = YBDBadgeObj();
    obj.image = map['image'] ?? '';
    obj.context = map['context'] ?? '';
    obj.title = map['title'] ?? '';
    obj.channel = map['channel'] ?? '';
    return obj;
  }
}

class YBDBadgeReload {}
