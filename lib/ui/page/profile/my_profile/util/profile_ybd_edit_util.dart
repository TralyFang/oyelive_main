import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:sprintf/sprintf.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/event/safe_ybd_account_bus_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/file_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/login/bloc/phonenumber_ybd_input_bloc.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';
import 'package:oyelive_main/ui/widget/edit_ybd_phone_dialog.dart';
import 'package:oyelive_main/ui/widget/profile_ybd_edit_row.dart';
import 'package:oyelive_main/ui/widget/profile_ybd_input_dialog.dart';
import 'package:oyelive_main/ui/widget/select_ybd_country.dart';

/// 弹出完善手机号对话框
Future<String?> profileEditShowPhoneEditDialog() async {
  logger.v('show edit phone dialog ');
  return await showDialog<String>(
    context: Get.context!,
    barrierDismissible: true,
    builder: (context) {
      return MultiBlocProvider(
        providers: [
          BlocProvider<YBDPhoneNumberInputBloc>(
            create: (_) => YBDPhoneNumberInputBloc(),
          ),
        ],
        child: YBDEditPhoneDialog(),
      );
    },
  );
}

/// 弹出输入对话框
profileEditShowInputDialog(EditType type, String? value, {int? inputMaxLength}) {
  logger.v('show input dialog type : $type, value : $value');
  showDialog(
    context: Get.context!,
    barrierDismissible: true,
    builder: (context) {
      return YBDProfileInputDialog(
        type,
        (String inputValue) {
          logger.v('input dialog callback : $inputValue');
          profileEditSaveToNetwork(type, inputValue);
        },
        oldValue: value,
        inputMaxLength: inputMaxLength,
      );
    },
  );
}

/// 保存输入数据
profileEditSaveToNetwork(
  EditType type,
  String? inputValue, {
  VoidCallback? saveSuccess,
}) {
  Store<YBDAppState>? store = YBDCommonUtil.storeFromContext();
  final YBDTextCtrl tc = Get.put(YBDTextCtrl());
  switch (type) {
    case EditType.Bio:
      Map<String, dynamic> params = {'comment': inputValue};
      ApiHelper.editUserInfo(params).then((bool value) {
        logger.v('edit bio result : $value');
        if (value) {
          profileEditUpdateUserInfo(EditType.Bio, inputValue);
        }
      });
      break;
    case EditType.NickName:
      Map<String, dynamic> params = {'nickname': inputValue};
      ApiHelper.editUserInfo(params).then((bool value) {
        logger.v('edit nick name result : $value');
        if (value) {
          eventBus.fire(YBDSafeAccountBusEvent());
          profileEditUpdateUserInfo(EditType.NickName, inputValue);
        }
      });
      Future.delayed(Duration(milliseconds: 1000), () {
        ApiHelper.queryRemainingTimes(Get.context).then((respEntity) {
          logger.v('22.1.12--------returnMsg:${respEntity?.returnMsg ?? ''}');
          tc.setNameWarn(respEntity?.returnMsg ?? '');
          logger.v('22.1.12---tcnamewarn:${tc.nameWarn}');
        });
      });
      break;
    case EditType.Email:
      Map<String, dynamic> params = {'email': inputValue};
      ApiHelper.editUserInfo(params).then((bool value) {
        logger.v('edit email result : $value');
        if (value) {
          profileEditUpdateUserInfo(EditType.Email, inputValue);
        }
      });
      break;
    case EditType.Gender:
      Map<String, dynamic> params = {'sex': int.parse(inputValue!)};
      ApiHelper.editUserInfo(params).then((bool value) {
        logger.v('edit sex result : $value');
        if (value) {
          eventBus.fire(YBDSafeAccountBusEvent());
          profileEditUpdateUserInfo(EditType.Gender, inputValue);
          saveSuccess?.call();
        }
      });
      break;
    case EditType.Birthday:
      String birthdayStr = DateFormat('yyyy-MM-dd').format(DateTime.fromMillisecondsSinceEpoch(int.parse(inputValue!)));
      Map<String, dynamic> params = {'birthday': birthdayStr};
      ApiHelper.editUserInfo(params).then((bool value) {
        logger.v('edit birthday result : $value');
        if (value) {
          profileEditUpdateUserInfo(EditType.Birthday, inputValue);
        }
      });
      break;
    case EditType.Location:
      // 添加地理位置
      ApiHelper.updateLocation(Get.context, inputValue, modifyByUser: 1).then((value) {
        if (value?.returnCode == Const.HTTP_SUCCESS) {
          // 更新成功，更新本地用户信息国家
          YBDUserUtil.updateCountry(Get.context, inputValue);
          profileEditUpdateUserInfo(EditType.Location, inputValue);
          eventBus.fire(YBDSafeAccountBusEvent());
        } else if (value?.returnCode == '902070') {
          logger.v(store!.state.configs?.user_modify_country_day);
          YBDToastUtil.toast(
              'Can only modify your location once every ${store.state.configs?.user_modify_country_day ?? 15} days.');
        } else {
          YBDToastUtil.toast(value?.returnMsg);
        }
      });

      break;
    case EditType.Avatar: // 修改头像
      ApiHelper.changeAvatar(Get.context, inputValue).then((bool value) {
        logger.v('edit user avatar result : $value');
        if (value) ApiHelper.checkLogin(Get.context);
        YBDFileUtil.clearTempDir();
      });
      Future.delayed(Duration(milliseconds: 1000), () {
        ApiHelper.queryRemainingTimes(Get.context).then((respEntity) {
          logger.v('22.1.12--------returnMsg:${respEntity?.returnMsg ?? ''}');
          tc.setNameWarn(respEntity?.returnMsg ?? '');
          logger.v('22.1.12---tcnamewarn:${tc.nameWarn}');
        });
      });
      break;
    case EditType.Cover:
      Map<String, dynamic> params = {'roomimg': inputValue};
      ApiHelper.editUserInfo(params).then((bool value) {
        logger.v('edit user cover result : $value');
        if (value) {
          ApiHelper.checkLogin(Get.context);
        }
      });
      break;
  }
}

/// 更新 SP 和 redux
profileEditUpdateUserInfo(EditType type, String? value) async {
  YBDUserInfo? _userInfo = await YBDSPUtil.getUserInfo();

  switch (type) {
    case EditType.NickName:
      _userInfo!.nickname = value;
      break;
    case EditType.Email:
      _userInfo!.email = value;
      break;
    case EditType.Bio:
      _userInfo!.comment = value;
      break;
    case EditType.Gender:
      _userInfo!.sex = int.parse(value!);
      break;
    case EditType.Birthday:
      _userInfo!.birthday = int.parse(value!);
      break;
    case EditType.Location:
      _userInfo!.country = value;
      break;
  }

  // 更新 SP
  YBDSPUtil.save(Const.SP_USER_INFO, _userInfo);

  // 更新 redux
  Store<YBDAppState> store = YBDCommonUtil.storeFromContext()!;
  store.dispatch(YBDUpdateUserAction(_userInfo));
}

void profileEditClickNameWarn(String ordinaryDays, String vipTimes, String vipDays) {
  showDialog(
      context: Get.context!,
      builder: (BuildContext context) {
        return Material(
          type: MaterialType.transparency,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(500),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
                        color: Colors.white),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: ScreenUtil().setWidth(50)),
                        Container(
                          width: ScreenUtil().setWidth(400),
                          child: Text(sprintf(translate('modify_name_warn1'), [ordinaryDays]),
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff333333))),
                        ),
                        SizedBox(height: ScreenUtil().setWidth(30)),
                        Container(
                          width: ScreenUtil().setWidth(450),
                          child: Text(sprintf(translate('modify_name_warn2'), [vipDays, vipTimes]),
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff333333))),
                        ),
                        SizedBox(height: ScreenUtil().setWidth(40)),
                        profileEditOkBtn(),
                        SizedBox(height: ScreenUtil().setWidth(40)),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      });
}

Widget profileEditOkBtn() {
  return YBDScaleAnimateButton(
    onTap: () {
      logger.v('clicked ok btn');
      Navigator.pop(Get.context!);
    },
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(33)),
        gradient: LinearGradient(
          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      width: ScreenUtil().setWidth(300),
      height: ScreenUtil().setWidth(65),
      alignment: Alignment.center,
      child: Text(
        translate('ok'),
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white),
      ),
    ),
  );
}

/// 弹出选择国家对话框
profileEditShowCountryDialog() {
  showDialog(
    context: Get.context!,
    builder: (context) {
      return YBDSelectCountry((Map<String, String> selected) {
        logger.v('selected country : $selected');
        profileEditSaveToNetwork(EditType.Location, selected['code']);
      });
    },
  );
}

void profileEditClickLocationWarn(String? day) {
  showDialog(
      context: Get.context!,
      builder: (BuildContext context) {
        return Material(
          type: MaterialType.transparency,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(500),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
                        color: Colors.white),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: ScreenUtil().setWidth(50)),
                        Container(
                          width: ScreenUtil().setWidth(400),
                          child: Text(sprintf(translate('modify_location_warn'), [day ?? 15]),
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff333333))),
                        ),
                        SizedBox(height: ScreenUtil().setWidth(40)),
                        profileEditOkBtn(),
                        SizedBox(height: ScreenUtil().setWidth(40)),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      });
}

Widget profileEditNameWarn(String warn) {
  return Container(
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(40), vertical: ScreenUtil().setWidth(10)),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))), color: Color(0xffFEF9EF)),
      height: ScreenUtil().setWidth(90),
      child: Stack(
        children: [
          Row(children: <Widget>[
            SizedBox(width: ScreenUtil().setWidth(56)),
            Container(
              height: ScreenUtil().setWidth(60),
              width: ScreenUtil().setWidth(540),
              child: Center(
                  child: Text(warn, style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xffD89A0B)))),
            )
          ]),
          Positioned(
              left: ScreenUtil().setWidth(17),
              top: ScreenUtil().setWidth(4),
              child: Image.asset(
                "assets/images/icon_warn_yellow.png",
                width: ScreenUtil().setWidth(24),
                fit: BoxFit.fitWidth,
              )),
        ],
      ));
}
