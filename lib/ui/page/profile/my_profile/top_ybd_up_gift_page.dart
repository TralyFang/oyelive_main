import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import 'entity/top_ybd_up_gift_entity.dart';
import 'widget/top_ybd_up_gift_item.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_app_bar.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';

/// 首充礼物列表页
class YBDTopUpGiftPage extends StatefulWidget {
  /// 从充值页面进入该页面
  /// 兼容路由用 true 或 false 字符串
  final String fromTopUp;

  /// 页面路径
  String location = '';

  YBDTopUpGiftPage({this.fromTopUp = 'true', this.location = ''});

  @override
  _YBDTopUpGiftPageState createState() => _YBDTopUpGiftPageState();
}

class _YBDTopUpGiftPageState extends BaseState<YBDTopUpGiftPage> {
  /// combo 列表数组
  List<YBDTopUpGiftEntityRecord?>? _data;

  /// 是否为初始状态
  bool _isInitState = true;

  /// 礼物列表里是否包含 Lucky package
  /// TODO: 接口数据要返回两个数组才能显示两组不同的礼物 item
  bool _haveLuckyPackage = false;

  @override
  void initState() {
    super.initState();
    logger.v('topup gift location topup gift page init : ${widget.location}');
    logger.v('initState : ${widget.fromTopUp}');

    // 获取页面数据
    _onRefresh();
  }
  void initStatejHztboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void dispose0OMBHoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      appBar: YBDMyAppBar(
        title: Text(
          translate('top_up_gifts'),
          style: TextStyle(
            color: Colors.white,
            fontSize: ScreenUtil().setSp(32),
          ),
        ),
        backgroundColor: YBDTPStyle.heliotrope,
        leading: YBDNavBackButton(color: Colors.white),
        elevation: 0.0,
      ),
      body: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: <Widget>[
            Container(
              height: ScreenUtil().setWidth(220),
              child: Image.asset('assets/images/topup/y_top_up_gitf_banner.webp'),
            ),
            _contentView(),
          ],
        ),
      ),
    );
  }
  void myBuild0g63Eoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 页面内容
  Widget _contentView() {
    if (_isInitState) {
      return Expanded(child: YBDLoadingCircle());
    } else {
      if (null == _data || _data!.isEmpty) {
        logger.v('top up gift list is empty');
        return Expanded(child: YBDPlaceHolderView(text: translate('no_data')));
      } else {
        return _topUpGiftListView();
      }
    }
  }
  void _contentViewOoO8goyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 首充礼物列表
  Widget _topUpGiftListView() {
    return Expanded(
      child: Container(
        child: ListView.builder(
          itemCount: _data!.length, // 数据的数量
          itemBuilder: (context, index) {
            // 是否显示 lucky package 分割线
            bool showLuckyLine = false;

            // 是否显示 top up reward 分割线
            bool showRewardLine = false;

            // 有 Lucky package 时显示分割线，否则不显示分割线
            if (_haveLuckyPackage) {
              if (index == 0) {
                // 第一条显示 lucky package 分割线
                showLuckyLine = true;
              } else if (index == 1) {
                // 第二条显示 Top up reward 分割线
                showRewardLine = true;
              }
            }

            print('showLuckyLine : $showLuckyLine, $showRewardLine');
            return GestureDetector(
              onTap: () {
                logger.v('clicked combo item');
              },
              child: YBDTopUpGiftItem(
                _data![index],
                showLuckyLine: showLuckyLine,
                showRewardLine: showRewardLine,
                callbackWinNow: () {
                  logger.v('clicked win now button : ${widget.fromTopUp}');
                  if (widget.fromTopUp.contains('true')) {
                    logger.v('pop to topup page');
                    // 从充值页面进入该页面点击 win now 按钮返回到上一级页面
                    Navigator.pop(context);
                  } else {
                    logger.v('navigate to topup page');
                    // 跳转到充值页面
                    YBDNavigatorHelper.openTopUpPage(context, location: widget.location);
                  }

                  YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                    YBDEventName.CLICK_EVENT,
                    location: 'topup_gift_page',
                    itemName: 'win_now',
                    adUnitName: widget.location,
                  ));
                },
              ),
            );
          },
        ),
      ),
    );
  }
  void _topUpGiftListViewWCpoDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 列表刷新响应方法
  _onRefresh() async {
    ApiHelper.firstTopUpGift(context).then((topUpGiftEntity) {
      if (topUpGiftEntity == null) {
        logger.v('top up gift entity is null');
      } else {
        _data = topUpGiftEntity.record;
        logger.v('get top up gift entity data success');

        // 检查接口数据是否有 Lucky package
        _data!.forEach((element) {
          // 1: Lucky package
          if (element!.luckPackage == 1) {
            logger.v('haveLuckyPackage true');
            _haveLuckyPackage = true;
          }
        });

        setState(() {});
      }
      _cancelInitState();
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }
}
