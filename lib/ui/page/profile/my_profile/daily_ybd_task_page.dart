import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/abstract/route/route_ybd_service.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_app_bar.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../../home/entity/daily_ybd_check_entity.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import 'bloc/daily_ybd_task_bloc.dart';
import 'go_ybd_tab_event.dart';
import 'widget/daily_ybd_task_reward_item.dart';
import 'widget/my_ybd_task_item.dart';

/// 每日任务页面
class YBDDailyTaskPage extends StatefulWidget {
  @override
  _YBDDailyTaskPageState createState() => _YBDDailyTaskPageState();
}

class _YBDDailyTaskPageState extends BaseState<YBDDailyTaskPage> {
  @override
  Widget myBuild(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<YBDDailyTaskBloc>(
          create: (context) {
            logger.v('create daily task bloc');
            YBDDailyTaskBloc bloc = YBDDailyTaskBloc(context);
            // 获取任务列表
            bloc.add(DailyTaskEvent.LoadTask);
            // 获取礼物列表和座驾列表
            bloc.add(DailyTaskEvent.LoadRewardInfo);
            return bloc;
          },
        ),
      ],
      child: Scaffold(
        appBar: YBDMyAppBar(
          title: Text(
            translate('check_in_tasks'),
            style: TextStyle(
              color: Colors.white,
              fontSize: YBDTPStyle.spNav,
            ),
          ),
          backgroundColor: YBDTPStyle.heliotrope,
          leading: YBDNavBackButton(color: Colors.white),
          elevation: 0.0,
        ),
        body: Container(
          width: ScreenUtil().screenWidth,
          height: ScreenUtil().screenHeight,
          decoration: YBDTPStyle.gradientDecoration,
          child: SingleChildScrollView(
            child: BlocBuilder<YBDDailyTaskBloc, YBDDailyTaskBlocState>(builder: (context, state) {
              if (state.isInit) {
                // 没有数据时隐藏奖励列表
                return Container(
                  height: ScreenUtil().screenHeight,
                  child: YBDLoadingCircle(),
                );
              } else if (null != state.myTasks && state.myTasks!.isEmpty) {
                return YBDPlaceHolderView(text: translate('no_task'));
              } else {
                logger.v('===daily check state.rewards: ${state.rewards}');
                return Column(
                  children: <Widget>[
                    // 连续签到天数
                    null != state.rewards && state.rewards!.isNotEmpty ? _consecutiveDays(state, context) : Container(),
                    // 每日签到奖励列表
                    null != state.rewards && state.rewards!.isNotEmpty ? _dailyTaskRewards(state) : Container(),
                    SizedBox(height: ScreenUtil().setWidth(30)),
                    if (state.dailyTasks != null) ...[
                      Container(
                        // 每日列表
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
                        child: _dailyTask(state),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(22)),
                    ],
                    if (state.myTasks != null)
                      Container(
                        // 任务列表
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
                        child: _myTaskContainer(context, state),
                      ),
                    SizedBox(height: ScreenUtil().setWidth(30)),
                  ],
                );
              }
            }),
          ),
        ),
      ),
    );
  }
  void myBuildtzDqOoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 每日签到已完成天数
  Widget _consecutiveDays(YBDDailyTaskBlocState state, BuildContext context) {
    // 已经签到的天数
    int checkedInDays = 0;
    state.rewards!.forEach((element) {
      // 判断签到奖励列表 receive 字段是否为 true
      if (null != element!.receive && element.receive) {
        // receive 为 true 表示已签到
        checkedInDays += 1;
      }
    });

    logger.v('===daily check : $checkedInDays');

    return Container(
      child: Stack(
        children: <Widget>[
          Image.asset(
            'assets/images/dc/daily_task_bg.png',
            height: ScreenUtil().setWidth(320),
          ),
          Column(
            children: <Widget>[
              Container(
                width: ScreenUtil().screenWidth,
                padding: EdgeInsets.only(top: ScreenUtil().setWidth(100)),
                alignment: Alignment.center,
                child: Text(
                  checkedInDays > 1 ? '$checkedInDays Days' : '$checkedInDays Day',
                  style: TextStyle(
                    fontFamily: 'Abril Fatface',
                    fontSize: ScreenUtil().setSp(72),
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(70)),
              Text(
                // TODO: 翻译
                checkedInDays > 1
                    ? 'You have signed in for $checkedInDays consecutive days'
                    : 'You have signed in for $checkedInDays consecutive day',
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  color: Colors.white.withOpacity(0.8),
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(36)),
              state.checkedIn ? _checkedInLabel() : _checkInButton(context, state),
              SizedBox(height: ScreenUtil().setWidth(50)),
            ],
          ),
        ],
      ),
    );
  }
  void _consecutiveDayspizPdoyelive(YBDDailyTaskBlocState state, BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 可点击的签到按钮
  Widget _checkInButton(BuildContext context, YBDDailyTaskBlocState state) {
    return Row(
      children: [
        Spacer(),
        YBDScaleAnimateButton(
          // Check in 按钮
          onTap: () {
            logger.v('clicked check in btn');
            if (!state.checkedIn) {
              // 完成签到任务
              context.read<YBDDailyTaskBloc>().add(DailyTaskEvent.CheckIn);
            } else {
              logger.v('already checked in');
            }
          },
          child: StoreBuilder<YBDAppState>(builder: (context, store) {
            bool hasExtra =
                store.state.bean?.vipDailyCheckRewardBeans != null && store.state.bean?.vipDailyCheckRewardBeans != 0;
            return Container(
              width: hasExtra ? null : ScreenUtil().setWidth(310),
              height: ScreenUtil().setWidth(72),
              padding: hasExtra ? EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)) : null,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40))),
                gradient: LinearGradient(
                  colors: state.checkedIn
                      ? [Colors.white.withOpacity(0.5), Colors.white.withOpacity(0.5)]
                      : [Color(0xff5E95E7), Color(0xff48CBCD)],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                ),
              ),
              // alignment: Alignment.center,
              child: Center(
                child: Container(
                  child: Text.rich(
                    TextSpan(
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(30),
                          color: Colors.white,
                        ),
                        children: [
                          TextSpan(text: translate('check_in')),
                          if (hasExtra) ...[
                            TextSpan(text: " to receive "),
                            WidgetSpan(
                                child: Padding(
                              padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(4)),
                              child: Image.asset(
                                "assets/images/topup/y_top_up_beans@2x.webp",
                                width: ScreenUtil().setWidth(24),
                              ),
                            )),
                            TextSpan(
                                text: " " + (store.state.bean?.vipDailyCheckRewardBeans.toString() ?? ''),
                                style: TextStyle(color: Color(0xffFFE951))),
                          ]
                        ]),
                    softWrap: true,
                  ),
                ),
              ),
            );
          }),
        ),
        Spacer(),
      ],
    );
  }
  void _checkInButton8K6hyoyelive(BuildContext context, YBDDailyTaskBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 已签到标签
  Widget _checkedInLabel() {
    return Container(
      width: ScreenUtil().setWidth(310),
      height: ScreenUtil().setWidth(72),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40))),
        color: Colors.white.withOpacity(0.5),
      ),
      child: Center(
        child: Text(
          translate('check_in'),
          style: TextStyle(
            fontSize: ScreenUtil().setSp(30),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
  void _checkedInLabel0F17coyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 每日签到奖励列表
  Widget _dailyTaskRewards(YBDDailyTaskBlocState state) {
    logger.v('===daily check _dailyTaskRewards : ${state.rewards}');
    return Container(
      alignment: Alignment.center,
      height: ScreenUtil().setWidth(430),
      width: ScreenUtil().screenWidth,
      child: Wrap(
        // 每日签到任务奖励列表
        spacing: ScreenUtil().setWidth(10),
        runSpacing: ScreenUtil().setWidth(10),
        children: List.generate(state.rewards!.length, (index) {
          return YBDDailyTaskRewardItem(
            itemData: state.rewards![index],
            dayName: 'Day ${index + 1}',
            giftList: state.gifts,
            carList: state.cars,
            itemWidth: ScreenUtil().setWidth(163),
            itemHeight: ScreenUtil().setWidth(200),
          );
        }),
      ),
    );
  }
  void _dailyTaskRewardskYFYtoyelive(YBDDailyTaskBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<YBDDailyCheckRecord?>? myTasks;

  /// 我的任务列表
  Widget _myTaskContainer(BuildContext context, YBDDailyTaskBlocState state) {
    // 一次性任务
    myTasks = state.myTasks?.where((element) => element!.name != Const.DAILY_CHECKED_IN)?.toList();
    myTasks = myTasks?..removeWhere((element) => element!.receive != null && element.receive!);

    if (null == myTasks || myTasks!.isEmpty) {
      // 没有任务时隐藏列表
      return Container();
    }

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(ScreenUtil().setWidth(16)),
        ),
        color: Colors.white.withOpacity(0.2),
      ),
      child: Container(
        // 我的任务列表
        child: ListView.separated(
          shrinkWrap: true,
          padding: EdgeInsets.all(0),
          physics: NeverScrollableScrollPhysics(),
          itemCount: myTasks!.length,
          separatorBuilder: (context, index) {
            return Container(
              color: Colors.white.withOpacity(0.1),
              height: ScreenUtil().setWidth(1),
              margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(46)),
            );
          },
          itemBuilder: (context, index) {
            return Container(
              padding: EdgeInsets.fromLTRB(
                ScreenUtil().setWidth(20),
                ScreenUtil().setWidth(0),
                ScreenUtil().setWidth(20),
                ScreenUtil().setWidth(0),
              ),
              child: YBDMyTaskItem(context, myTasks![index], callback: (MyTaskStatus status) {
                /// 任务数据
                YBDDailyCheckRecord? taskData = myTasks![index];
                switch (status) {
                  case MyTaskStatus.ToDo:
                  case MyTaskStatus.Overdue:
                    {
                      if (taskData!.name!.contains('nickname')) {
                        // 跳转到设置页面
                        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.edit_profile).then((value) {
                          // 返回到当前页面时刷新任务列表
                          context.read<YBDDailyTaskBloc>().add(DailyTaskEvent.LoadTask);
                        });
                      } else if (taskData.name!.contains('Follow')) {
                        // 退出当前页面
                        Navigator.pop(context);
                      } else if (taskData.name!.contains('Recharge')) {
                        // 跳转到充值
                        YBDNavigatorHelper.openTopUpPage(context).then((value) {
                          // 返回到当前页面时刷新任务列表
                          logger.v("DailyTaskEvent.LoadTask");
                          context.read<YBDDailyTaskBloc>().add(DailyTaskEvent.LoadTask);
                        });
                      } else if (taskData.name!.contains('YBDEnter mobile phone') || taskData.name!.contains('mail')) {
                        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.edit_profile);
                      }
                    }
                    break;
                  case MyTaskStatus.Claim:
                    // 获取奖励
                    _claimReward(context, taskData!.id, taskData.name, index);
                    break;
                  default:
                    logger.v('do nothing : $status');
                }
              }),
            );
          },
        ),
      ),
    );
  }
  void _myTaskContainerzQwSpoyelive(BuildContext context, YBDDailyTaskBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<YBDDailyCheckRecord?>? dailyTask;
  _dailyTask(YBDDailyTaskBlocState state) {
    dailyTask = state.dailyTasks;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(ScreenUtil().setWidth(16)),
        ),
        color: Colors.white.withOpacity(0.2),
      ),
      child: Container(
        // 我的任务列表
        child: Column(
          children: <Widget>[
            SizedBox(height: ScreenUtil().setWidth(40)),
            Container(
              child: Image.asset(
                'assets/images/my_task_title.webp',
                height: ScreenUtil().setWidth(36),
                fit: BoxFit.fitHeight,
              ),
            ),
            SizedBox(height: ScreenUtil().setWidth(30)),
            ListView.separated(
              padding: EdgeInsets.all(0),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: dailyTask!.length,
              separatorBuilder: (context, index) {
                return Container(
                  color: Colors.white.withOpacity(0.1),
                  height: ScreenUtil().setWidth(1),
                  margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(46)),
                );
              },
              itemBuilder: (context, index) {
                return Container(
                  padding: EdgeInsets.fromLTRB(
                    ScreenUtil().setWidth(20),
                    ScreenUtil().setWidth(0),
                    ScreenUtil().setWidth(20),
                    ScreenUtil().setWidth(0),
                  ),
                  child: YBDMyTaskItem(context, dailyTask![index], callback: (MyTaskStatus status) {
                    /// 任务数据
                    YBDDailyCheckRecord? taskData = dailyTask![index];
                    logger.v('task data name : ${dailyTask![index]!.name}, status: $status, ${dailyTask![index]!.route}');
                    switch (status) {
                      case MyTaskStatus.ToDo:
                      case MyTaskStatus.Overdue:
                        if (dailyTask![index]!.name!.contains('Play Royal Pattern')) {
                          YBDTeenInfo.getInstance().enterType = TPEnterType.TPTask;
                          YBDModuleCenter.instance().enterRoom(type: EnterRoomType.OpenTp);
                          return;
                        }
                        if (dailyTask![index]!.name!.contains('Lucky')) {
                          YBDTeenInfo.getInstance().enterType = TPEnterType.TPTask;
                          YBDModuleCenter.instance().enterRoom(type: EnterRoomType.OpenLucky1000);
                          return;
                        }
                        if (dailyTask![index]!.route!.contains(RouteConst.gameRoute)) {
                          // String path = "flutter://game?type=fly&postion=home&index=3";
                          YBDNavigatorHelper.navigateTo(context, dailyTask![index]!.route);
                          return;
                        }
                        if (dailyTask![index]!.route!.contains(YBDNavigatorHelper.index_page) ||
                            dailyTask![index]!.route!.contains("flutter://main")) {
                          YBDNavigatorHelper.popPage(context);
                          eventBus.fire(YBDGoTabEvent(1));
                          return;
                        }

                        YBDNavigatorHelper.navigateTo(context, dailyTask![index]!.route);
                        break;
                      case MyTaskStatus.Claim:
                        // 获取奖励
                        _claimDailyReward(context, taskData!.id, index);
                        break;
                      default:
                        logger.v('do nothing : $status');
                    }
                  }),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  /// 获取任务的奖励
  _claimReward(BuildContext context, int? taskId, String? taskName, int index) async {
    bool ok = await ApiHelper.claimReward(context, taskId, taskName: taskName);

    // 刷新任务状态
    if (ok) {
      myTasks![index]!.receive = true;
      setState(() {});
      // context.read<YBDDailyTaskBloc>().add(DailyTaskEvent.LoadTask);
    } else {
      YBDToastUtil.toast('YBDOperation failed!');
    }
  }

  /// 获取任务的奖励
  _claimDailyReward(BuildContext context, int? taskId, int index) async {
    bool ok = await ApiHelper.claimDailyReward(context, taskId);
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.COMPLETE_TASK,
      location: YBDLocationName.TASK_PAGE,
      itemName: YBDItemName.CLAIM_DAILY_TASK + '$taskId',
    ));
    // 刷新任务状态
    if (ok) {
      dailyTask![index]!.receive = true;
      setState(() {});

      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.COMPLETE_TASK,
        location: YBDLocationName.TASK_PAGE,
        itemName: YBDItemName.CLAIM_DAILY_TASK_SUCCESS + '$taskId',
      ));
      // context.read<YBDDailyTaskBloc>().add(DailyTaskEvent.LoadTask);
    } else {
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.COMPLETE_TASK,
        location: YBDLocationName.TASK_PAGE,
        itemName: YBDItemName.CLAIM_DAILY_TASK_ERROR + '$taskId',
      ));

      YBDToastUtil.toast('YBDOperation failed!');
      context.read<YBDDailyTaskBloc>().add(DailyTaskEvent.LoadTask);
    }
  }
}
