import 'dart:async';

class YBDGoTabEvent {
  int? index;

  /// 子标
  int? subIndex;

  String? postion;

  ///游戏类型
  String? type;

  /// 匹配 0匹配 -1不匹配
  int? match;

  YBDGoTabEvent(this.index, {this.postion, this.subIndex, this.type, this.match});
}
