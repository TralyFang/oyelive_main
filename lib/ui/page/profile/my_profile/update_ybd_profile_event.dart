import 'dart:async';

enum UpdateProfileEventType {
  // 更新用户信息
  UpdateUserInfo,

  // 更新动态列表
  UpdateStatus,
  Unknown,
}

/// 更新个人信息的事件
class YBDUpdateProfileEvent {
  // 从哪个页面触发的更新事件
  UpdateProfileEventType type;
  YBDUpdateProfileEvent({this.type = UpdateProfileEventType.Unknown});
}
