import 'dart:async';

/*
 * @Author: William-Zhou
 * @Date: 2021-11-16 16:42:04
 * @LastEditTime: 2022-12-01 17:39:16
 * @LastEditors: William
 * @Description: 
 */
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/dialog_ybd_wsa_support_helper.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';
import 'package:url_launcher/url_launcher.dart';

class YBDWSASupportDialog extends StatefulWidget {
  const YBDWSASupportDialog({Key? key}) : super(key: key);

  @override
  State<YBDWSASupportDialog> createState() => _YBDWSASupportDialogState();
}

class _YBDWSASupportDialogState extends State<YBDWSASupportDialog> {
  // bool _roman = true;
  // bool _english = false;
  List<YBDWSAEntity>? data;
  String? curName = 'roman_english';

  @override
  void initState() {
    super.initState();
    logger.v('22.11.22---lv:${YBDUserUtil.getUserInfoSync.level}');

    List<Map<String, dynamic>> wsaConfig = List<Map<String, dynamic>>.from(YBDCommonUtil.getRoomOperateInfo().wsaConfig as List<dynamic>);
    List<YBDWSAEntity> entity = wsaConfig.map(
      (e) {
        YBDWSAEntity a = YBDWSAEntity();
        yBDWSAEntityFromJson(a, e);
        logger.v('22.11.22---a:${a.toJson()}');
        return a;
      },
    ).toList();
    data = entity;
  }
  void initState8GVE2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Material(
            type: MaterialType.transparency,
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(ScreenUtil().setWidth(32)),
                  color: Colors.white,
                ),
                // padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
                width: 520.px,
                height: 430.px,
                // constraints: BoxConstraints(minHeight: 420.px, maxHeight: 520.px),
                child: Column(children: [
                  YBDTPGlobal.hSizedBox(40),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/wsa.webp',
                        width: ScreenUtil().setWidth(42),
                      ),
                      YBDTPGlobal.wSizedBox(10),
                      Text(translate('support'),
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: ScreenUtil().setSp(28),
                            fontWeight: FontWeight.w500,
                          ))
                    ],
                  ),
                  YBDTPGlobal.hSizedBox(30),
                  Expanded(child: SingleChildScrollView(child: Column(children: getItems()))),
                  YBDTPGlobal.hSizedBox(30),
                  // 按钮
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      YBDScaleAnimateButton(
                        onTap: () {
                          logger.v('clicked cancel btn');
                          Navigator.pop(context);
                        },
                        child: _bottomBtn('Cancel', 0),
                      ),
                      YBDScaleAnimateButton(
                        onTap: () async {
                          logger.v('clicked ok btn');
                          Navigator.pop(context);
                          logger.v('22.12.1---url:${_getUrl()}');
                          await launch(_getUrl());
                        },
                        child: _bottomBtn('Ok', 1),
                      ),
                    ],
                  ),
                  YBDTPGlobal.hSizedBox(40)
                ]))));
  }
  void buildCjDBToyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Widget> getItems() {
    if (_isOldType()) {
      return [
        _item('roman_english', 15),
        _item('english', 15),
      ];
    }
    if (data != null) {
      return data!.map((YBDWSAEntity e) => _item(e.name, e.level)).toList();
    }
    return [];
  }
  void getItemsPCwqqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  bool _isOldType() {
    int level = YBDUserUtil.getUserInfoSync.level ?? 0;
    return level > 14 && level < 40;
  }

  Widget _item(String? name, int? level) {
    if ((YBDUserUtil.getUserInfoSync.level ?? 0) < (level ?? 0)) return Container();
    return GestureDetector(
      onTap: () {
        setState(() {
          curName = name;
        });
      },
      child: Container(
        color: Colors.transparent,
        width: ScreenUtil().setWidth(500),
        height: ScreenUtil().setWidth(80),
        margin: EdgeInsets.only(top: 10.px),
        child: Row(
          children: [
            YBDTPGlobal.wSizedBox(51),
            _selectIcon(curName == name),
            YBDTPGlobal.wSizedBox(7),
            Text(translate(name ?? ''),
                style: TextStyle(
                  color: Colors.black,
                  fontSize: ScreenUtil().setSp(28),
                ))
          ],
        ),
      ),
    );
  }
  void _itemyRI6Coyelive(String? name, int? level) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _selectIcon(bool check) {
    return check
        ? Image.asset(
            'assets/images/icon_check.webp',
            width: ScreenUtil().setWidth(27),
          )
        : Container(
            width: ScreenUtil().setWidth(27),
            height: ScreenUtil().setWidth(27),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(27)),
              border: Border.all(
                width: ScreenUtil().setWidth(1),
                style: BorderStyle.solid,
                color: Color(0xff979797),
              ),
            ));
  }
  void _selectIconWKhGEoyelive(bool check) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部按钮
  Widget _bottomBtn(String title, int index) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(ScreenUtil().setWidth(60)),
        ),
        gradient: LinearGradient(
          colors: index == 0 ? [Color(0xffCCCCCC), Color(0xffCCCCCC)] : [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      width: ScreenUtil().setWidth(200),
      height: ScreenUtil().setWidth(64),
      alignment: Alignment.center,
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(28),
          color: Colors.white,
        ),
      ),
    );
  }
  void _bottomBtnRj6OPoyelive(String title, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String _getUrl() {
    if (_isOldType()) {
      if (curName == 'roman_english') {
        return Platform.isAndroid
            ? 'https://wa.me/${YBDCommonUtil.getRoomOperateInfo().wsaRoman}'
            : 'https://api.whatsapp.com/send?phone=${YBDCommonUtil.getRoomOperateInfo().wsaRoman}';
      } else {
        return Platform.isAndroid
            ? 'https://wa.me/${YBDCommonUtil.getRoomOperateInfo().wsaEnglish}'
            : 'https://api.whatsapp.com/send?phone=${YBDCommonUtil.getRoomOperateInfo().wsaEnglish}';
      }
    }
    int number = data?.firstWhere((e) => e.name == curName).number ?? 0;
    logger.v('22.11.22---number:$number');
    return Platform.isAndroid ? 'https://wa.me/$number' : 'https://api.whatsapp.com/send?phone=$number';
  }
  void _getUrlg4z98oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDWSAEntity with JsonConvert<YBDWSAEntity> {
  String? name;
  int? level;
  int? number;
}
