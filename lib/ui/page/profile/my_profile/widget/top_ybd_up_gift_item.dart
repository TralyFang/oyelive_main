import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../entity/top_ybd_up_gift_entity.dart';
import '../../../status/local_audio/scale_ybd_animate_button.dart';

/// 首充礼物列表 item
class YBDTopUpGiftItem extends StatelessWidget {
  /// 点击 win now 按钮的回调
  final VoidCallback? callbackWinNow;

  /// item 数据
  final YBDTopUpGiftEntityRecord? itemData;

  /// 显示 Lucky package 分割线
  final bool showLuckyLine;

  /// 显示 Top up reward 分割线
  final bool showRewardLine;

  YBDTopUpGiftItem(
    this.itemData, {
    this.callbackWinNow,
    this.showLuckyLine = false,
    this.showRewardLine = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(
        ScreenUtil().setWidth(20),
        ScreenUtil().setWidth(0),
        ScreenUtil().setWidth(20),
        ScreenUtil().setWidth(20),
      ),
      child: Column(
        children: <Widget>[
          showLuckyLine ? _luckyPackageLine() : SizedBox(),
          showRewardLine ? _topUpRewardLine() : SizedBox(),
          itemData!.luckPackage == 1 ? _luckyPackageItem() : _topUpRewardItem(),
        ],
      ),
    );
  }
  void buildDZDluoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Lucky package item
  Widget _luckyPackageItem() {
    return Container(
      // height: ScreenUtil().setWidth(345),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/lucky_package.webp'),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setWidth(94)),
          // 标题栏
          _giftTitle(),
          SizedBox(height: ScreenUtil().setWidth(20)),
          // 礼物栏
          _giftListView(),
          SizedBox(height: ScreenUtil().setWidth(30)),
        ],
      ),
    );
  }
  void _luckyPackageItemDwhVSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Top up reward item
  Widget _topUpRewardItem() {
    return Container(
      // height: ScreenUtil().setWidth(280),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.2),
        borderRadius: BorderRadius.all(
          Radius.circular(ScreenUtil().setWidth(8)),
        ),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setWidth(20)),
          // 标题栏
          _giftTitle(),
          SizedBox(height: ScreenUtil().setWidth(20)),
          // 礼物栏
          _giftListView(),
          SizedBox(height: ScreenUtil().setWidth(20)),
        ],
      ),
    );
  }
  void _topUpRewardItemMkPnloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Lucky package 分割线
  Widget _luckyPackageLine() {
    return Column(
      children: <Widget>[
        SizedBox(height: ScreenUtil().setWidth(60)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _line(),
            SizedBox(width: ScreenUtil().setWidth(34)),
            Text(
              translate('lucky_package'),
              style: TextStyle(
                fontSize: ScreenUtil().setSp(24),
                color: Colors.white,
              ),
            ),
            SizedBox(width: ScreenUtil().setWidth(34)),
            _line(),
          ],
        ),
        SizedBox(height: ScreenUtil().setWidth(36)),
      ],
    );
  }
  void _luckyPackageLinehSvj0oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Top up reward 分割线
  Widget _topUpRewardLine() {
    return Column(
      children: <Widget>[
        SizedBox(height: ScreenUtil().setWidth(20)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _line(),
            SizedBox(width: ScreenUtil().setWidth(34)),
            Text(
              translate('top_up_reward'),
              style: TextStyle(
                fontSize: ScreenUtil().setSp(24),
                color: Colors.white,
              ),
            ),
            SizedBox(width: ScreenUtil().setWidth(34)),
            _line(),
          ],
        ),
        SizedBox(height: ScreenUtil().setWidth(36)),
      ],
    );
  }
  void _topUpRewardLineyu1K4oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 横线
  Widget _line() {
    return Container(
      height: ScreenUtil().setWidth(1),
      width: ScreenUtil().setWidth(97),
      color: Colors.white,
    );
  }

  /// 礼物标题栏
  /// 奖励说明、 win now 按钮
  Widget _giftTitle() {
    return Row(
      children: <Widget>[
        SizedBox(width: ScreenUtil().setWidth(30)),
        Text(
          '${translate('top_up')} ',
          style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
        ),
        Text(
          itemData!.spendCoins ?? '',
          style: TextStyle(color: Color(0xffFFE731), fontSize: ScreenUtil().setSp(24)),
        ),
        Text(
          ' ${translate('win_rewards')} ',
          style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
        ),
        Text(
          itemData!.winCoins ?? '',
          style: TextStyle(color: Color(0xffFFE731), fontSize: ScreenUtil().setSp(24)),
        ),
        Expanded(child: SizedBox(width: 1)),
        // 1: Received 标签和 0: win now 按钮
        // TODO: 状态放到常量文件
        itemData!.status == 1 ? _receivedLabel() : _winNowButton(),
        SizedBox(width: ScreenUtil().setWidth(20)),
      ],
    );
  }
  void _giftTitleUc3inoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 礼物横向列表
  Widget _giftListView() {
    return Container(
      height: ScreenUtil().setWidth(180),
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
          itemBuilder: (_, index) {
            return Container(
              width: ScreenUtil().setWidth(120),
              child: Column(children: <Widget>[
                Expanded(child: SizedBox(height: 1)),
                Container(
                  width: ScreenUtil().setWidth(88),
                  height: ScreenUtil().setWidth(88),
                  child: _giftImage(itemData!.rewards![index]!),
                ),
                Expanded(child: SizedBox(height: 1)),
                Text(
                  itemData!.rewards![index]!.desc ?? '',
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(22),
                    color: Colors.white,
                  ),
                ),
              ]),
            );
          },
          separatorBuilder: (_, index) {
            return SizedBox(width: ScreenUtil().setWidth(26));
          },
          itemCount: itemData!.rewards!.length),
    );
  }
  void _giftListViewwMZnroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _giftImage(YBDTopUpGiftEntityRecordReward reward) {
    if (null != reward.image && reward.image!.isNotEmpty) {
      return YBDNetworkImage(
        imageUrl: reward.image ?? '',
        fit: BoxFit.contain,
      );
    } else if (reward.type == 16) {
      return Container(
        padding: EdgeInsets.all(ScreenUtil().setWidth(11)),
        child: Image.asset(
          'assets/images/ic_vip_prince.png',
          fit: BoxFit.cover,
        ),
      );
    } else if (reward.type == 17) {
      return Container(
        padding: EdgeInsets.all(ScreenUtil().setWidth(11)),
        child: Image.asset(
          'assets/images/ic_vip_pirate.png',
          fit: BoxFit.cover,
        ),
      );
    } else {
      return Container();
    }
  }
  void _giftImageSIYutoyelive(YBDTopUpGiftEntityRecordReward reward) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 已接受礼物的标签
  Widget _receivedLabel() {
    return Container(
      width: ScreenUtil().setWidth(140),
      height: ScreenUtil().setWidth(56),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
        color: Colors.white.withOpacity(0.3),
      ),
      child: Center(
        child: Text(
          translate('received'),
          style: TextStyle(
            fontSize: ScreenUtil().setSp(24),
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  /// 获取礼物的按钮
  Widget _winNowButton() {
    return YBDScaleAnimateButton(
      onTap: () {
        logger.v('clicked win now btn');

        if (null != callbackWinNow) {
          callbackWinNow!();
        } else {
          logger.v('callback is null');
        }
      },
      child: Container(
        width: ScreenUtil().setWidth(140),
        height: ScreenUtil().setWidth(56),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
          gradient: LinearGradient(
            colors: [Color(0xff47CDCC), Color(0xff3E76CC)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Center(
          child: Text(
            translate('win_now'),
            style: TextStyle(
              fontSize: ScreenUtil().setSp(24),
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
  void _winNowButton2unQSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
