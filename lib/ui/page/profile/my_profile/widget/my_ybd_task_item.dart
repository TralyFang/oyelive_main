import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../home/entity/daily_ybd_check_entity.dart';
import '../../../status/local_audio/scale_ybd_animate_button.dart';

enum MyTaskStatus {
  Done,
  ToDo,
  Overdue,
  Claim,
  Claimed,
  Unknown,
}

/// 点击任务状态按钮的回调
typedef MyTaskCallback = Function(MyTaskStatus);

/// 我的任务列表 item
class YBDMyTaskItem extends StatelessWidget {
  final BuildContext context;

  /// 点击任务状态按钮的回调
  final MyTaskCallback? callback;

  /// 任务数据
  final YBDDailyCheckRecord? taskData;

  YBDMyTaskItem(this.context, this.taskData, {this.callback});

  @override
  Widget build(BuildContext context) {
    MyTaskStatus status;

    // TODO: 我的任务列表已经过滤了签到任务，这个判断可去掉
    if (null != taskData!.receive && taskData!.receive!) {
      status = MyTaskStatus.Claimed;
    } else {
      // TODO: 状态码提到常量文件里
      if (taskData!.status == 1) {
        status = MyTaskStatus.Claim;
      } else if (taskData!.status == 2) {
        status = MyTaskStatus.Overdue;
      } else {
        status = MyTaskStatus.ToDo;
      }
    }

    return Container(
      height: ScreenUtil().setWidth(130),
      child: Row(
        children: [
          Container(
            width: ScreenUtil().setWidth(120),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                // 2.4.8修改为任务奖励图标使用网络图片
                YBDNetworkImage(
                  imageUrl: taskData!.currencyImage ?? '',
                  width: ScreenUtil().setWidth(36),
                  fit: BoxFit.cover,
                  placeholder: (context, url) => Image.asset("assets/images/topup/y_top_up_beans@2x.webp"),
                  errorWidget: (context, error, url) => Image.asset("assets/images/topup/y_top_up_beans@2x.webp"),
                ),
                SizedBox(height: ScreenUtil().setWidth(4)),
                Container(
                  // 奖励名称
                  child: Text(
                    taskData!.reward ?? '',
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white.withOpacity(0.7),
                      fontSize: ScreenUtil().setSp(18),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(20)),
          Container(
            constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(350)),
            child: Text(
              // 任务名称
              taskData!.name ?? '',
              style: TextStyle(
                color: Colors.white,
                fontSize: ScreenUtil().setWidth(24),
              ),
            ),
          ),
          Expanded(child: SizedBox(width: 1)),
          //连续登陆的 隐藏掉
          (taskData!.name ?? '').contains('continuously') ? Container() : _taskStatusButton(status),
          SizedBox(width: ScreenUtil().setWidth(20)),
        ],
      ),
    );
  }
  void build56lMboyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 任务状态的按钮
  Widget _taskStatusButton(MyTaskStatus status) {
    String statusTitle;
    LinearGradient backGround = LinearGradient(colors: [
      Color(0xFF47CDCC),
      Color(0xFF3E76CC),
    ], begin: Alignment.topCenter, end: Alignment.bottomCenter);
    switch (status) {
      case MyTaskStatus.Claim:
        statusTitle = translate('claim');
        backGround = LinearGradient(colors: [
          Color(0xFFD877C2),
          Color(0xFFFF9CE1),
        ], begin: Alignment.topCenter, end: Alignment.bottomCenter);
        break;
      case MyTaskStatus.Claimed:
        statusTitle = translate('claimed');
        return Container(
          width: ScreenUtil().setWidth(125),
          alignment: Alignment.center,
          child: Icon(
            Icons.check,
            color: Colors.white,
            size: ScreenUtil().setWidth(30),
          ),
        );
        break;
      case MyTaskStatus.Done:
        statusTitle = translate('done');
        break;
      case MyTaskStatus.Overdue:
        statusTitle = translate('overdue');
        break;
      case MyTaskStatus.ToDo:
        statusTitle = translate('to_do');
        break;
      default:
        statusTitle = '';
    }

    return YBDScaleAnimateButton(
      onTap: () {
        logger.v('clicked win now btn');
        if (null != callback) {
          callback!(status);
        } else {
          logger.v('callback is null');
        }
      },
      child: Container(
        width: ScreenUtil().setWidth(125),
        height: ScreenUtil().setWidth(54),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))), gradient: backGround),
        child: Center(
          child: Text(
            statusTitle,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(22),
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
  void _taskStatusButtonCKzBNoyelive(MyTaskStatus status) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
