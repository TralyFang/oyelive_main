import 'dart:async';

import 'package:flutter/material.dart';

/// 底部裁剪
class YBDBottomClipper extends CustomClipper<Path> {
  late double roundnessFactor;
  YBDBottomClipper(double roundnessFactor) {
    this.roundnessFactor = roundnessFactor;
  }

  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, 0);
    path.lineTo(0, size.height);

    var first1Point = Offset(0, size.height - roundnessFactor);
    var end1Point = Offset(roundnessFactor, size.height - roundnessFactor);
    // 绘制贝塞尔曲线
    path.quadraticBezierTo(first1Point.dx, first1Point.dy, end1Point.dx, end1Point.dy);

    path.lineTo(size.width - roundnessFactor, size.height - roundnessFactor);
    var first2Point = Offset(size.width, size.height - roundnessFactor);
    var end2Point = Offset(size.width, size.height);
    path.quadraticBezierTo(first2Point.dx, first2Point.dy, end2Point.dx, end2Point.dy);
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
  void shouldReclipjEWkloyelive(covariant CustomClipper<Path> oldClipper) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
