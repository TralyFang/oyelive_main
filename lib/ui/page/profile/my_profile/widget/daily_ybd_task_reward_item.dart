import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_image.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import '../../../home/entity/car_ybd_list_entity.dart';
import '../../../home/entity/daily_ybd_check_entity.dart';
import '../../../home/entity/gift_ybd_list_entity.dart';
import '../util/reward_ybd_data_util.dart';

/// 每日任务奖励 item
class YBDDailyTaskRewardItem extends StatelessWidget {
  /// 第几天签到
  final String? dayName;

  /// 奖励 item 的数据
  final YBDDailyCheckRecordExtendRewardListDailyReward? itemData;

  /// 礼物列表
  final List<YBDGiftListRecordGift?>? giftList;

  /// 座驾列表
  final List<YBDCarListRecord?>? carList;

  /// item 宽度
  final double? itemWidth;

  /// item 高度
  final double? itemHeight;

  /// 签到完成是否修改背景色
  final bool changeBg;

  /// 背景色不透明度
  final double bgOpacity;

  YBDDailyTaskRewardItem({
    this.itemData,
    this.dayName,
    this.giftList,
    this.carList,
    this.itemWidth,
    this.itemHeight,
    this.changeBg = true,
    this.bgOpacity = 0.4,
  });

  @override
  Widget build(BuildContext context) {
    final rewardData = YBDRewardDataUtil.rewardData(
      context,
      dailyCheckTask: itemData!,
      giftList: giftList,
      carList: carList,
    );

    logger.v("reward item img: ${rewardData.img}, name: ${rewardData.name}, day: $dayName");

    return Container(
      padding: EdgeInsets.fromLTRB(
        ScreenUtil().setWidth(10),
        ScreenUtil().setWidth(10),
        ScreenUtil().setWidth(10),
        0,
      ),
      width: itemWidth,
      height: itemHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: rewardData.isClaimed && changeBg ? Colors.white.withOpacity(0.1) : Colors.white.withOpacity(bgOpacity),
      ),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                // 星期
                width: ScreenUtil().setWidth(54),
                height: ScreenUtil().setWidth(24),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(9)),
                  color: Colors.white,
                ),
                child: Center(
                  child: Text(
                    dayName!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(14),
                      color: Color(0xff863CCE),
                    ),
                  ),
                ),
              ),
              Expanded(child: SizedBox(width: 1)),
              rewardData.isClaimed
                  ? Image.asset(
                      'assets/images/checked_in_icon.png',
                      width: _checkedInIconWidth(),
                    )
                  : Container(),
              SizedBox(width: ScreenUtil().setWidth(0)),
            ],
          ),
          Expanded(child: SizedBox(height: 1)),
          Container(
            // 每日任务奖励图片
            width: itemWidth,
            height: ScreenUtil().setWidth(68),
            child: rewardData.isLocalImg
                ? YBDImage(
                    path: rewardData.img ?? '',
                    fit: BoxFit.contain,
                  )
                : YBDNetworkImage(
                    imageUrl: rewardData.img ?? '',
                    fit: BoxFit.contain,
                  ),
          ),
          Expanded(child: SizedBox(height: 1)),
          Container(
            // 奖励名称和数量
            width: ScreenUtil().setWidth(160),
            child: Center(
              child: Text(
                rewardData.name!,
                style: TextStyle(
                  fontSize: _nameTextFont(rewardData.name!),
                  color: Colors.white,
                ),
                maxLines: 2,
              ),
            ),
          ),
          SizedBox(height: 4),
        ],
      ),
    );
  }
  void build8TNPCoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 文字太长缩小字体
  double _nameTextFont(String name) {
    if (name.length >= 10) {
      if (itemWidth! <= ScreenUtil().setWidth(122)) {
        return ScreenUtil().setSp(14);
      } else {
        return ScreenUtil().setSp(16);
      }
    } else {
      return ScreenUtil().setSp(22);
    }
  }

  /// 根据 item 宽度设置签到图标大小
  double _checkedInIconWidth() {
    if (itemWidth! <= ScreenUtil().setWidth(122)) {
      return ScreenUtil().setWidth(24);
    } else {
      return ScreenUtil().setWidth(30);
    }
  }
}
