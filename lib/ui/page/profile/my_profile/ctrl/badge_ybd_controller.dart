import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../../../common/constant/const.dart';
import '../../../../../common/http/http_ybd_util.dart';
import '../../../../../common/util/dialog_ybd_util.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../module/api.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../../module/entity/query_ybd_badge_entity.dart';
import '../util/badge_ybd_util.dart';

import '../../../../../main.dart';

class YBDBadgesController extends GetxController{

  var data = [].obs;

  var index = 0;

  int selectIndex = -1.obs;

  YBDQueryBadgeRecordBadges? getRecord(){
    return (data.value as YBDQueryBadgeEntity).record!.badges![index];
  }
    @override
  void onInit(){
    // TODO: implement onInit
    super.onInit();
  }

  dealWith() async{
    YBDDialogUtil.showLoading(Get.context);
    YBDQueryBadgeEntity? entity = await getBadgeList();
    YBDDialogUtil.hideLoading(Get.context);

    if (entity?.record?.userBadge != null){
      entity?.record?.badges?.forEach((element) {
        element!.isRecive = (entity.record?.userBadge?.map((e) => e!.name == element.badgeCode).toList() ?? []).contains(true);
      });
    }
    data.value = entity?.record?.badges ?? [];
  }

  static Future<YBDQueryBadgeEntity?> getBadgeList() async {
    return YBDHttpUtil.getInstance().doGet<YBDQueryBadgeEntity>(
      Get.context,
      YBDApi.QUERY_BADGE_LIST,
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  //佩戴勋章 selectIndex == index 说明是取消佩戴
  void wearBadge(String? name, int index) async {
    YBDQueryBadgeEntity? entity = await YBDHttpUtil.getInstance().doPost<YBDQueryBadgeEntity>(Get.context, YBDApi.USER_UPDATEINFO,
        {'type':'badge','code': selectIndex == index ? '' : name});
      if(entity?.returnCode == Const.HTTP_SUCCESS){
        await ApiHelper.checkLogin(Get.context);
        selectIndex = selectIndex == index ? -1 : index;
        update();
        eventBus.fire(YBDBagdeChangeEvent());
      }
  }
  void wearBadge5fRK1oyelive(String? name, int index)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}