import 'dart:async';


class YBDBadgeColor{
    String colour = '';
    String grey = '';

    static YBDBadgeColor get(Map map){
      YBDBadgeColor color = YBDBadgeColor();
      color.colour = map["colour"] ?? "";
      color.grey = map["grey"] ?? "";
      return color;
    }
}