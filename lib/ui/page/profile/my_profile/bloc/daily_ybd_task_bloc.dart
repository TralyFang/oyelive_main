import 'dart:async';

import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';

import '../../../../../common/constant/const.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../generated/json/base/json_convert_content.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../home/entity/car_ybd_list_entity.dart';
import '../../../home/entity/daily_ybd_check_entity.dart';
import '../../../home/entity/daily_ybd_task_entity.dart';
import '../../../home/entity/gift_ybd_list_entity.dart';
import '../../../room/service/live_ybd_service.dart';

enum DailyTaskEvent {
  // 请求每日任务列表，礼物，座驾 数据
  LoadTask,
  // 获取签到奖励列表的礼物和座驾数据
  LoadRewardInfo,
  // 获取任务列表数据后刷新列表
  RefreshTaskList,
  // 获取每日任务列表数据后刷新列表
  RefreshDailyTaskList,
  // 刷新每日签到奖励列表
  RefreshRewardList,
  // 完成签到任务
  CheckIn,
}

/// 每日任务页面业务逻辑管理器
class YBDDailyTaskBloc extends Bloc<DailyTaskEvent, YBDDailyTaskBlocState> {
  BuildContext context;

  /// 任务列表
  List<YBDDailyCheckRecord?>? _dailyTaskList;

  /// 每日任务奖励列表
  List<YBDDailyCheckRecordExtendRewardListDailyReward?>? _rewardList;

  /// 每日签到任务
  YBDDailyCheckRecord? _dailyCheckRecord;

  /// 礼物列表
  List<YBDGiftListRecordGift?>? _giftList;

  List<YBDDailyCheckRecord?>? _routineTaskList;

  /// 座驾列表
  List<YBDCarListRecord?>? _carList;

  YBDDailyTaskBloc(this.context) : super(YBDDailyTaskBlocState(isInit: true));

  @override
  Stream<YBDDailyTaskBlocState> mapEventToState(DailyTaskEvent event) async* {
    switch (event) {
      case DailyTaskEvent.LoadTask:
        // 先请求任务列表再请求礼物和座驾
        _requestDailyTask();
        _requestMyTask();
        break;
      case DailyTaskEvent.LoadRewardInfo:
        _requestGiftList();
        _requestCarList();
        break;
      case DailyTaskEvent.RefreshTaskList:
        yield new YBDDailyTaskBlocState(myTasks: _dailyTaskList, dailyTasks: _routineTaskList);
        break;
      case DailyTaskEvent.RefreshRewardList:
        yield YBDDailyTaskBlocState(
          myTasks: _dailyTaskList,
          checkedIn: _dailyCheckRecord?.receive ?? false,
          dailyTasks: _routineTaskList,
          rewards: _rewardList,
          gifts: _giftList,
          cars: _carList,
        );
        break;
      case DailyTaskEvent.CheckIn:
        // 签到
        _requestCheckIn();
        break;
      case DailyTaskEvent.RefreshDailyTaskList:
        // TODO: Handle this case.
        print("sexpa");
        yield new YBDDailyTaskBlocState(myTasks: _dailyTaskList, dailyTasks: _routineTaskList);
        break;
    }
  }
  void mapEventToStateEwpG8oyelive(DailyTaskEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求完成每日签到任务的接口
  _requestCheckIn() async {
    int whichDay = 1;

    YBDDailyCheckRecordExtend extendObj =
        await Future.value(JsonConvert.fromJsonAsT<YBDDailyCheckRecordExtend>(json.decode(_dailyCheckRecord!.extend!)));
    extendObj.rewardList!.forEach((element) {
      if (element!.dailyReward![0]!.receive ?? false) {
        whichDay++;
      }
    });
    // 数数埋点
    alog.v('22.8.1---day:$whichDay');
    YBDTATrack().trackEvent(YBDTATrackEvent.sign_complete, prop: YBDTAProps(num: whichDay));
    await ApiHelper.claimReward(context, _dailyCheckRecord?.id, taskName: 'Check-In Day $whichDay');

    // 刷新任务状态
    add(DailyTaskEvent.LoadTask);
  }

  /// 请求任务列表
  _requestMyTask() async {
    YBDDailyTaskEntity? dailyTaskEntity = await ApiHelper.queryTask(context);

    if (null != dailyTaskEntity && null != dailyTaskEntity.record) {
      _dailyTaskList = dailyTaskEntity.record;

      // 刷新一次性任务列表
      add(DailyTaskEvent.RefreshTaskList);

      // 过滤出每日签到任务并刷新每日签到任务奖励列表
      _filterDailyTask(dailyTaskEntity);
    } else {
      logger.v('daily task rewardList is null');
    }
  }

  /// 请求每日任务列表
  _requestDailyTask() async {
    YBDMyDailyTaskEntity? dailyTaskEntity = await ApiHelper.queryDailyTask(context);

    if (null != dailyTaskEntity && null != dailyTaskEntity.record) {
      _routineTaskList = dailyTaskEntity.record;

      // 刷新一次性任务列表
      add(DailyTaskEvent.RefreshRewardList);

      // 过滤出每日签到任务并刷新每日签到任务奖励列表
      // _filterDailyTask(dailyTaskEntity);
    } else {
      logger.v('daily task rewardList is null');
    }
  }

  /// 每日签到任务
  _filterDailyTask(YBDDailyTaskEntity dailyTaskEntity) async {
    YBDDailyCheckRecord? dailyCheckRecord = dailyTaskEntity.record!.firstWhereOrNull((record) {
      logger.v('record name : ${record!.name}');
      if (record.name == Const.DAILY_CHECKED_IN) {
        return true;
      } else {
        return false;
      }
    }, /*orElse: () => null*/);

    if (null == dailyCheckRecord) {
      logger.v('daily check record is null');
    } else {
      // 保存每日签到任务数据
      _dailyCheckRecord = dailyCheckRecord;

      // 获取每日签到任务奖励列表
      YBDDailyCheckRecordExtend extendObj =
          await Future.value(JsonConvert.fromJsonAsT<YBDDailyCheckRecordExtend>(json.decode(dailyCheckRecord.extend!)));
      logger.v('===daily check rewardList : ${extendObj.rewardList}');
      if (null != extendObj.rewardList && extendObj.rewardList!.isNotEmpty) {
        _rewardList = extendObj.rewardList!.map((e) {
          return e!.dailyReward![0];
        }).toList();

        // 根据每日签到任务奖励数据刷新奖励列表
        add(DailyTaskEvent.RefreshRewardList);
        logger.v('daily task rewards amount : ${extendObj.rewardList!.length}');
      } else {
        logger.v('daily task rewardList is empty');
      }
    }
  }

  /// 请求礼物列表数据
  _requestGiftList() async {
    if ((YBDLiveService.instance.giftList ?? []).isNotEmpty) {
      _giftList = YBDLiveService.instance.giftList;
      add(DailyTaskEvent.RefreshRewardList);
      return;
    }

    YBDGiftListEntity? giftListEntity = await ApiHelper.giftList(context);

    if (null != giftListEntity && null != giftListEntity.record && giftListEntity.record!.isNotEmpty) {
      // 保存礼物列表数据
      _giftList = giftListEntity.record![0]!.gift;

      // 根据礼物数据刷新签到奖励列表
      add(DailyTaskEvent.RefreshRewardList);
    } else {
      logger.v('gift list is null');
    }
  }

  /// 请求座驾列表数据
  _requestCarList() async {
    if ((YBDLiveService.instance.carList ?? []).isNotEmpty) {
      _carList = YBDLiveService.instance.carList;
      add(DailyTaskEvent.RefreshRewardList);
      return;
    }

    YBDCarListEntity? giftListEntity = await ApiHelper.carList(context, queryAll: true);

    if (null != giftListEntity && null != giftListEntity.record) {
      _carList = giftListEntity.record;

      // 根据座驾数据刷新签到奖励列表
      add(DailyTaskEvent.RefreshRewardList);
    } else {
      logger.v('car list is null');
    }
  }
}

/// 每日任务页面业务状态
class YBDDailyTaskBlocState {
  /// 初始状态
  bool isInit;

  /// 我的任务
  List<YBDDailyCheckRecord?>? myTasks;

  //每日任务
  List<YBDDailyCheckRecord?>? dailyTasks;

  /// 每日任务奖励
  List<YBDDailyCheckRecordExtendRewardListDailyReward?>? rewards;

  /// 是否完成签到
  bool checkedIn;

  /// 礼物列表
  List<YBDGiftListRecordGift?>? gifts;

  /// 座驾列表
  List<YBDCarListRecord?>? cars;

  YBDDailyTaskBlocState(
      {this.isInit: false, this.myTasks, this.checkedIn: false, this.rewards, this.gifts, this.cars, this.dailyTasks});
}
