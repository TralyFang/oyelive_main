import 'dart:async';

import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:svgaplayer_flutter/parser.dart';
import 'package:svgaplayer_flutter/player.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/http/http_ybd_util.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/api.dart';
import '../../../../module/entity/query_ybd_badge_entity.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';
import 'ctrl/badge_ybd_controller.dart';
import 'util/badge_ybd_util.dart';
import 'util/top_ybd_up_badges_alert_util.dart';

class YBDBadgePage extends StatefulWidget {
  @override
  _YBDBadgePageState createState() => _YBDBadgePageState();
}

class _YBDBadgePageState extends BaseState<YBDBadgePage> with TickerProviderStateMixin {
  YBDBadgesController ct = Get.put(YBDBadgesController());

  List<YBDQueryBadgeRecordBadges?>? badges = [];

  //动画控制器
  late AnimationController controller;
  StreamSubscription? sSubscription;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      // ct.dealWith();
      dealWithData();
    });

    sSubscription = eventBus.on<YBDBadgeReload>().listen((event) {
      if (!mounted) return;
      dealWithData();
    });

    controller = AnimationController(duration: const Duration(seconds: 2), vsync: this);
    //动画开始、结束、向前移动或向后移动时会调用StatusListener
    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        print("status is completed");
        //反向执行
      } else if (status == AnimationStatus.dismissed) {
        print("status is dismissed");
      } else if (status == AnimationStatus.forward) {
        print("status is forward");
      } else if (status == AnimationStatus.reverse) {
        print("status is reverse");
      }
    });
  }
  void initStatehdbW0oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  dealWithData() async {
    YBDDialogUtil.showLoading(context);
    YBDQueryBadgeEntity? entity = await getBadgeList();
    YBDDialogUtil.hideLoading(context);
    List<String>? wearList = entity?.record?.wearBadge == null ? [] : entity?.record?.wearBadge?.split('&');
    if (entity?.record?.userBadge != null) {
      int index = -1;
      entity?.record?.badges!.forEach((element) {
        index++;
        element!.isRecive =
            (entity.record!.userBadge!.map((e) => e!.name == element.badgeCode).toList()).contains(true);
        bool iswear = (wearList?.map((e) => e == element.badgeCode).toList() ?? []).contains(true);
        if (iswear) {
          ct.selectIndex = index;
        }
      });
    }
    badges = entity?.record?.badges;
    setState(() {});
  }

  Future<YBDQueryBadgeEntity?> getBadgeList() async {
    return YBDHttpUtil.getInstance().doGet<YBDQueryBadgeEntity>(
      Get.context,
      YBDApi.QUERY_BADGE_LIST,
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  @override
  Widget myBuild(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: GetBuilder<YBDBadgesController>(
        init: YBDBadgesController(),
        builder: (ctrl) => Scaffold(
          body: Container(
            decoration: YBDTPStyle.gradientDecoration,
            child: Column(
              children: [
                SafeArea(
                  bottom: false,
                  child: Container(
                    width: ScreenUtil().screenWidth,
                    height: ScreenUtil().setWidth(116),
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                            // 导航栏返回按钮
                            top: ScreenUtil().setWidth(24),
                            left: 0,
                            child: YBDNavBackButton()),
                        Center(
                          child:
                              Text(translate("Badges"), style: TextStyle(fontSize: YBDTPStyle.spNav, color: Colors.white)),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                      padding: EdgeInsets.only(left: ScreenUtil().setWidth(25), right: ScreenUtil().setWidth(25)),
                      child: GridView.builder(
                          padding: EdgeInsets.all(0),
                          // physics: NeverScrollableScrollPhysics(),
                          itemCount: badges!.length,
                          //ctrl.data.value.length,
                          shrinkWrap: true,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              mainAxisSpacing: ScreenUtil().setWidth(10),
                              crossAxisSpacing: ScreenUtil().setWidth(10),
                              childAspectRatio: 1),
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () {
                                // Get.find<YBDBadgesController>().index = index;
                                showBadgeView(context, index, badges![index]);
                              },
                              child: YBDBadgePageItem(index, badges![index]),
                            );
                          })),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  void myBuildKsm5soyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<bool> _onBackPressed() {
    Navigator.pop(context);
    return Future<bool>.value(true);
  }

  ///显示徽章弹窗
  showBadgeView(BuildContext context, int index, YBDQueryBadgeRecordBadges? badges) {
    controller.forward();
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: '',
      barrierColor: Colors.black.withOpacity(0.79),
      transitionDuration: Duration(milliseconds: 500),
      pageBuilder: (context, animation, sectionAnimation) {
        return FadeTransition(
          opacity: controller,
          child: Container(
            child: Center(
              child: YBDBadgeAlertItem(index, badges),
            ),
          ),
        );
      },
      // transitionBuilder: (ctx, animation, _, child) {
      //   return FractionalTranslation(
      //     translation: Offset(0, 1 - animation.value),
      //     child: child,
      //   );
      // }
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    sSubscription?.cancel();
    Get.delete<YBDBadgesController>();
    super.dispose();
  }
  void disposeFZB0royelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDBadgePageItem extends StatelessWidget {
  final int index;
  YBDQueryBadgeRecordBadges? record;

  YBDBadgePageItem(this.index, this.record);

  @override
  Widget build(BuildContext context) {
    YBDBadgesController ct = Get.find<YBDBadgesController>();
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)), color: Colors.white.withOpacity(0.09)),
      child: Stack(alignment: Alignment.center, children: [
        Column(
          children: [
            SizedBox(
              height: ScreenUtil().setHeight(19),
            ),
            (record!.isRecive! && record!.iconObj.grey.endsWith('.svga'))
                ? YBDBadgeAnimationView(
                    ScreenUtil().setWidth(252),
                    // index: index,
                    record: record,
                  )
                : YBDNetworkImage(
                    imageUrl: !record!.isRecive! ? record!.iconObj.colour : record!.iconObj.grey,
                    width: ScreenUtil().setWidth(252),
                    height: ScreenUtil().setWidth(252),
                    fit: BoxFit.cover,
                  ),
            SizedBox(
              height: ScreenUtil().setHeight(6),
            ),
            Container(
                height: ScreenUtil().setHeight(30),
                child: Text(
                  record!.title ?? "",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                )),
          ],
        ),
        Positioned(
          top: 0,
          right: 0,
          child: Offstage(
            offstage: !record!.isRecive!,
            child: Container(
              padding: EdgeInsets.all(5),
              child: Obx(
                () {
                  return Image.asset(
                    'assets/images/agree${ct.selectIndex.obs.value == index ? '' : "_no"}_chose.webp',
                    width: ScreenUtil().setWidth(60),
                  );
                },
              ),
            ),
          ),
        ),
      ]),
    );
  }
  void buildnbrEXoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDBadgeAnimationView extends StatefulWidget {
  // final int index;
  YBDQueryBadgeRecordBadges? record;
  final double width;
  final double? height;
  final BoxFit fit;
  String? url;

  bool needReload;

  YBDBadgeAnimationView(this.width,
      {this.record, this.url, this.needReload = false, this.height, this.fit = BoxFit.contain});

  @override
  _YBDBadgeAnimationViewState createState() => _YBDBadgeAnimationViewState();
}

class _YBDBadgeAnimationViewState extends State<YBDBadgeAnimationView> with TickerProviderStateMixin {
  /// 动画控制器
  late SVGAAnimationController _animationController;

  /// 播放 456 勋章 动画
  playBadgeAnimation() async {
    if (widget.url == null) {
      // YBDQueryBadgeRecordBadges record = Get.find<YBDBadgesController>().data.value[widget.index] as YBDQueryBadgeRecordBadges;
      widget.url = widget.record!.iconObj.grey;
    }

    _animationController = SVGAAnimationController(vsync: this);
    var videoItem;
    if (YBDBadgeUtil.getInstance()!.getVideoItem(widget.url) == null) {
      ///需要直接缓存buffer？
      // videoItem = widget.url.startsWith('assets')
//     ? await SVGAParser.shared.decodeFromAssets(widget.url)
//     : await SVGAParser.shared.decodeFromURL(widget.url);
//
      if (widget.url!.startsWith('assets')) {
        videoItem = await SVGAParser.shared.decodeFromAssets(widget.url!);
      }else if (File(widget.url!).existsSync()) {
        Uint8List bytes = await File(widget.url!).readAsBytes();
        videoItem = await SVGAParser.shared.decodeFromBuffer(bytes);
      }else if (widget.url!.startsWith('http')) {
        Uint8List bytes = await YBDDownloadCacheManager.instance.downloadUint8ListURL(widget.url!);
        videoItem = await SVGAParser.shared.decodeFromBuffer(bytes);
      }
      YBDBadgeUtil.getInstance()!.setUrl(widget.url, videoItem);
    } else {
      videoItem = YBDBadgeUtil.getInstance()!.getVideoItem(widget.url);
    }

    _animationController.videoItem = videoItem;
    _animationController.repeat();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    playBadgeAnimation();
    eventBus.on<YBDBagdeChangeEvent>().listen((event) {
      if (!mounted) return;
      widget.needReload = true;
      // playBadgeAnimation();
    });
  }
  void initStateudrbkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    if (widget.needReload) {
      widget.needReload = false;
      playBadgeAnimation();
    }
    return Container(
      width: widget.width,
      height: widget.height ?? widget.width,
      child: SVGAImage(
        _animationController,
        fit: widget.fit,
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }
  void dispose2oTshoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDBadgeAlertItem extends StatelessWidget {
  final int index;
  final YBDQueryBadgeRecordBadges? record;

  YBDBadgeAlertItem(this.index, this.record);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        width: double.infinity,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(ScreenUtil().setWidth(16)), color: Colors.transparent),
        child: itemColumn(),
      ),
    );
  }

  Widget itemColumn() {
    YBDBadgesController ct = Get.find<YBDBadgesController>();
    int beans = record!.speed == null ? 0 : (record!.speed!.beans ?? 0);
    int max = record!.speed == null ? 0 : (record!.speed!.max ?? 0);
    double pix = (record!.speed == null) ? 0 : (beans / max);
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          record!.iconObj.grey.endsWith('.svga')
              ? YBDBadgeAnimationView(
                  ScreenUtil().setWidth(500),
                  // index: index,
                  record: record,
                )
              : YBDNetworkImage(
                  imageUrl: record!.iconObj.grey,
                  width: ScreenUtil().setWidth(500),
                  height: ScreenUtil().setWidth(500),
                  fit: BoxFit.cover,
                ),
          SizedBox(
            height: ScreenUtil().setHeight(31),
          ),
          Container(
              width: ScreenUtil().setWidth(500),
              child: Text(
                record!.context ?? "",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(28),
                    color: Colors.white,
                    decoration: TextDecoration.none,
                    fontWeight: FontWeight.normal),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              )),
          SizedBox(
            height: ScreenUtil().setHeight(55),
          ),
          ClipRRect(
              borderRadius: BorderRadius.circular(ScreenUtil().setHeight(19) / 2.0),
              child: Offstage(
                offstage: (record!.speed == null || pix == 1), //第一个勋章没有进度的 ， 进度条满了 都隐藏
                child: Container(
                  width: ScreenUtil().setWidth(450),
                  height: ScreenUtil().setHeight(18),
                  color: Color(0xffD9D9D9),
                  child: Stack(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(ScreenUtil().setHeight(18) / 2.0),
                        child: Container(
                          color: Color(pix == 1 ? 0xFFFFDB61 : 0xFF0DC3B8),
                          width: ScreenUtil().setWidth(500) * pix,
                        ),
                      )
                    ],
                  ),
                ),
              )
              // LinearProgressIndicator(
              //   backgroundColor: Color(0xffD9D9D9),
              //   valueColor: AlwaysStoppedAnimation<Color>(Color(0xFF0DC3B8)),
              //   value: 0.7,
              //   minHeight: ScreenUtil().setHeight(19),
              // ),
              ),
          Offstage(
            offstage: (record!.speed == null || pix == 1),
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(top: ScreenUtil().setWidth(24)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    beans < 1000 ? "$beans" : NumberFormat('0,000').format(beans).toString(),
                    style: TextStyle(
                        color: Color(pix == 1 ? 0xFFFFDB61 : 0xFF0DC3B8),
                        fontWeight: FontWeight.normal,
                        fontSize: ScreenUtil().setSp(20),
                        decoration: TextDecoration.none),
                  ),
                  SizedBox(
                    width: ScreenUtil().setHeight(10),
                  ),
                  Text(
                    "/ ${max < 1000 ? "$max" : NumberFormat('0,000').format(max).toString()}",
                    style: TextStyle(
                        color: Colors.white.withOpacity(0.5),
                        fontWeight: FontWeight.normal,
                        fontSize: ScreenUtil().setSp(20),
                        decoration: TextDecoration.none),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(80),
          ),
          Offstage(
            offstage: !record!.isRecive!,
            child: GestureDetector(
              onTap: () {
                ct.wearBadge(record!.badgeCode, index);
                Navigator.pop(Get.context!);
              },
              child: Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(440),
                height: ScreenUtil().setWidth(72),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(ScreenUtil().setWidth(36)),
                  gradient: LinearGradient(
                      colors: ct.selectIndex == index
                          ? [Colors.white, Colors.white]
                          : [
                              Color(0xff5E94E7),
                              Color(0xff47CDCD),
                            ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter),
                ),
                child: Text(
                  ct.selectIndex == index ? "Remove" : "Apply",
                  style: TextStyle(
                      color: ct.selectIndex == index ? Colors.black : Colors.white,
                      decoration: TextDecoration.none,
                      fontSize: ScreenUtil().setSp(28),
                      fontWeight: FontWeight.normal),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
  void itemColumnORzI8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
