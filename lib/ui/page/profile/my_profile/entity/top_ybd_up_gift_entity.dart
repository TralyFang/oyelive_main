import 'dart:async';

import '../../../../../generated/json/base/json_convert_content.dart';

class YBDTopUpGiftEntity with JsonConvert<YBDTopUpGiftEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDTopUpGiftEntityRecord?>? record;
  String? recordSum;
}

class YBDTopUpGiftEntityRecord with JsonConvert<YBDTopUpGiftEntityRecord> {
  String? title;
  int? status;
  String? spendCoins;
  String? winCoins;

  /// 1: Lucky package
  int? luckPackage;
  List<YBDTopUpGiftEntityRecordReward?>? rewards;
}

class YBDTopUpGiftEntityRecordReward with JsonConvert<YBDTopUpGiftEntityRecordReward> {
  String? image;
  int? type;
  String? desc;
}
