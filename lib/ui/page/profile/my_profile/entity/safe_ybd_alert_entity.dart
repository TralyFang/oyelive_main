import 'dart:async';

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDSafeAlertEntity with JsonConvert<YBDSafeAlertEntity> {
  String? returnCode;
  String? returnMsg;
  YBDSafeAlertRecord? record;
}

class YBDSafeAlertRecord with JsonConvert<YBDSafeAlertRecord> {
  YBDSafeAlertRecordUserInfo? userInfo;
  int? total;
  int? level;
  String? safety;
  String? topTitle;
  String? bottomTitle;
  bool? window;
}

class YBDSafeAlertRecordUserInfo with JsonConvert<YBDSafeAlertRecordUserInfo> {
  String? country;
  bool? password;
  String? phone;
  int? sex;
  String? nickname;
  int? userId;
}
