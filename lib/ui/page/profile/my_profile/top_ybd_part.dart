import 'dart:async';

import 'dart:io';

import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_ScreenUtil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/widget/red_ybd_dot_alert.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/activty/constants/act_ybd_consts.dart';
import 'package:oyelive_main/ui/widget/profile_ybd_bg_swiper.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/agency/agency_ybd_api_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/room_ybd_level_tag.dart';
import '../../../widget/round_ybd_avatar.dart';
import '../../activty/activity_ybd_skin.dart';
import '../../home/widget/top_ybd_talent_item.dart';
import 'wear_ybd_badge_item.dart';

class YBDTopPart extends StatefulWidget {
  YBDUserInfo? userInfo;
  Function? showTalenTime;

  /// 是否为签约主播
  bool? isOfficialTalent;

  YBDTopPart(this.userInfo, {this.showTalenTime()?, this.isOfficialTalent});

  @override
  _YBDTopPartState createState() => new _YBDTopPartState();
}

class _YBDTopPartState extends BaseState<YBDTopPart> {
  String selectCountry = '';

  YBDUserCertificationRecordCertificationInfos? _certificationInfos;

  /// 根据生日时间戳计算年龄
  _calculateAge(int? timestamp) {
    if (null == timestamp) {
      return '';
    }

    DateTime birthDate = DateTime.fromMillisecondsSinceEpoch(timestamp);
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age.toString();
  }

  bool isCerRequested = false;

  /// 用户等级标签栏
  _getRow() {
    List<Widget> rows = [];

    var female = [Color(0xffE37BBC), Color(0xffFF877D)];
    var male = [Color(0xff5E94E7), Color(0xff47CDCC)];
    var id = [
      Color(0xff3792C0),
      Color(0xff78DFBD),
    ];

    var constellation = [
      Color(0xffFFD972),
      Color(0xffFF6161),
    ];

    if (isAgent) rows.add(_getIdArea());

    // rows.add(_getTag(id, "ID:${widget.userInfo.id}"));

    if (_certificationInfos == null || _certificationInfos!.hidUserLevel == 0)
      rows.add(YBDLevelTag(
        widget.userInfo!.level,
      ));
    if (_certificationInfos == null || _certificationInfos!.hidTalentLevel == 0)
      rows.add(YBDRoomLevelTag(widget.userInfo!.roomlevel));
//    rows.add(getTag(level, "Lv${widget.userInfo.level}", image: 'assets/images/icon_diamond.png'));
//    rows.add(getTag(roomLevel, "RLv${widget.userInfo.roomlevel}", image: 'assets/images/icon_mic.png'));
    if (_certificationInfos == null || _certificationInfos!.hidGender == 0)
      rows.add(_getTag(widget.userInfo!.sex == 2 ? male : female, _calculateAge(widget.userInfo!.birthday),
          image: widget.userInfo!.sex == 2 ? 'assets/images/male.png' : 'assets/images/female.png'));
    //2.2 需求去掉了 bug1994
    // rows.add(_getTag(
    //     constellation,
    //     widget.userInfo?.birthday == null
    //         ? ""
    //         : YBDCommonUtil.getZodicaSign(DateTime.fromMillisecondsSinceEpoch(widget.userInfo.birthday))));
    // if (_certificationInfos == null || _certificationInfos.hidVip == 0)
    //   if ((widget.userInfo?.vip ?? 0) != 0)
    //   rows.add(Padding(
    //     padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
    //     child: Image.asset(
    //       "assets/images/vip_lv${widget.userInfo.vip}.png",
    //       width: ScreenUtil().setWidth(25),
    //     ),
    //   ));
    if (_certificationInfos == null || _certificationInfos?.hidBadge == 0)
      rows.add(Padding(
        padding: EdgeInsets.only(left: ScreenUtil().setWidth(8)),
        child: YBDWearBadgeItem(
          widget.userInfo?.badges,
          needReload: true,
        ),
      ));
    if (_certificationInfos == null || _certificationInfos?.hidTalent == 0) if (widget.isOfficialTalent == true) {
      rows.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(8)),
        child: Image.asset(
          'assets/images/icon_talent.webp',
          width: ScreenUtil().setWidth(30),
        ),
      ));
      widget.showTalenTime?.call();
    }

    if (!isCerRequested || rows.isEmpty) {
      return SizedBox();
    }
    return Padding(
      padding: EdgeInsets.only(top: ScreenUtil().setWidth(14)),
      child: Row(
        // 用户等级标签栏
        mainAxisAlignment: MainAxisAlignment.center,
        children: rows,
      ),
    );
  }

  _getTag(List<Color> colors, String text, {String? image}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10), vertical: ScreenUtil().setWidth(3)),
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(4)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(18))),
        gradient: LinearGradient(colors: colors, begin: Alignment.centerLeft, end: Alignment.centerRight),
      ),
      child: Row(
        children: [
          image != null
              ? Image.asset(
                  image,
                  height: ScreenUtil().setWidth(15),
                )
              : Container(),
          image != null
              ? SizedBox(
                  width: ScreenUtil().setWidth(2),
                )
              : Container(),
          Text(
            text,
            style: TextStyle(fontSize: ScreenUtil().setSp(16), height: 1.2, color: Colors.white),
          )
        ],
      ),
    );
  }

  bool isAgent = false;

  checkIsAgent() async {
    isAgent = await YBDAgencyApiHelper.searchAgentByUserId(context, await YBDUserUtil.userId());
    setState(() {});
  }

  _getIdArea() {
    if (widget.userInfo!.uniqueNum != null)
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Stack(
            alignment: Alignment.centerLeft,
            children: [
              Container(
                height: ScreenUtil().setWidth(24),
                padding: EdgeInsets.only(
                  right: ScreenUtil().setWidth(4),
                ),
                margin: EdgeInsets.only(left: ScreenUtil().setWidth(2)),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(1)),
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
                  gradient: LinearGradient(
                    colors: [
                      Color(0xffFBBD1A),
                      Color(0xffFCD323),
                    ],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                ),
                child: Row(
                  children: [
                    SizedBox(
                      width: ScreenUtil().setWidth(24),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(6),
                    ),
                    Text(
                      'ID: ${widget.userInfo!.uniqueNum}',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(6),
                    ),
                    Container(
                      width: ScreenUtil().setWidth(23),
                      padding: EdgeInsets.all(2),
                      child: InkWell(
                        onTap: () {
                          logger.v('copied user id : ${widget.userInfo!.id}');
                          Clipboard.setData(ClipboardData(text: '${widget.userInfo!.uniqueNum}'));
                          YBDToastUtil.toast(translate('copied_to_clipboard'));
                        },
                        child: YBDImage(
                          path: YBDActivitySkinRoot().curAct().userProfileCopy(),
                          fit: BoxFit.fitWidth,
//              scale: 3,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Image.asset(
                'assets/images/u_crown.webp',
                width: ScreenUtil().setWidth(24),
                fit: BoxFit.fitWidth,
              ),
            ],
          ),
          SizedBox(
            width: ScreenUtil().setWidth(10),
          ),
          Text(
            '(ID: ${widget.userInfo!.id})',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(20),
              color: Colors.white.withOpacity(0.7),
            ),
          ),
        ],
      );
    return Container(
      height: ScreenUtil().setWidth(40),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: ScreenUtil().setWidth(20),
          ),
          Text(
            'ID: ${widget.userInfo!.id}',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(24),
              color: Color(0x99ffffff),
            ),
          ),
          Container(
            width: ScreenUtil().setWidth(26),
            padding: EdgeInsets.all(2),
            child: InkWell(
              onTap: () {
                logger.v('copied user id : ${widget.userInfo!.id}');
                Clipboard.setData(ClipboardData(text: '${widget.userInfo!.id}'));
                YBDToastUtil.toast(translate('copied_to_clipboard'));
              },
              child: Image.asset(
                'assets/images/icon_copy.webp',
                fit: BoxFit.fitWidth,
//              scale: 3,
              ),
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(10),
          ),
        ],
      ),
    );
  }

  _getAgentIcon() {
    return Image.asset(
      'assets/images/agent/icon_agent.webp',
      width: ScreenUtil().setWidth(288),
    );
  }

  _getBigButton(String image, int imageWidth, String text, onTap) {
    return Expanded(
        child: GestureDetector(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              image,
              width: ScreenUtil().setWidth(imageWidth), //46
            ),
            SizedBox(
              width: ScreenUtil().setWidth(14),
            ),
            Text(
              text,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(26),
                fontWeight: FontWeight.normal,
                color: YBDActivitySkinRoot().curAct().activityTextColor(),
              ),
            )
          ],
        ),
      ),
    ));
  }

  List<Widget> _getCertified() {
    if (_certificationInfos != null)
      return [
        SizedBox(
          height: ScreenUtil().setWidth(14),
        ),
        Text(
          _certificationInfos!.title!,
          style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff89FCF7), fontWeight: FontWeight.bold),
        )
      ];

    return [];
  }
  void _getCertifiedqZhexoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    String? luckyID = YBDCommonUtil.getUserLuckyID(context, widget.userInfo?.id);
    bool lessLayout = widget.userInfo?.vipIcon == null && !isAgent;
    return Stack(
      children: <Widget>[
        // 轮播背景图
        Container(
          height: 700.px + ScreenUtil().statusBarHeight,
          width: ScreenUtil().screenWidth,
          child: YBDBGSwiper(data: widget.userInfo!.backgrounds?.bgs),
        ),
        IgnorePointer(
          child: Opacity(
            opacity: 0.7,
            child: Container(
              height: 700.px + ScreenUtil().statusBarHeight,
              width: ScreenUtil().screenWidth,
              // color: Colors.black.withOpacity(0.4),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Color(0xff0C0019).withOpacity(0), Color(0xff0C0019)],
                      begin: Alignment.topCenter,
                      stops: [0.3, 1],
                      end: Alignment.bottomCenter)),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
          height: ScreenUtil().setWidth(700) + ScreenUtil().statusBarHeight,
          child: Column(
            children: [
              Container(
                  height: ScreenUtil().setWidth(94),
                  child: Row(children: [
                    Container(
                      width: ScreenUtil().setWidth(120),
                      height: ScreenUtil().setWidth(120),
                      child: Stack(
                        children: [
                          TextButton(
                            style: ButtonStyle(
                              padding: MaterialStateProperty.all(EdgeInsets.zero),
                            ),
                            child: Icon(
                              Icons.edit,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              setState(() {
                                YBDSPUtil.save(Const.SP_EDIT, true);
                              });
                              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.edit_profile);
                            },
                          ),
                          Positioned(top: 20.px, right: 40.px, child: YBDRedDotAlert(spKey: Const.SP_EDIT))
                        ],
                      ),
                    ),
                    Expanded(child: Container()),
                    Container(
                        width: ScreenUtil().setWidth(120),
                        height: ScreenUtil().setWidth(120),
                        child: TextButton(
                            style: ButtonStyle(
                              padding: MaterialStateProperty.all(EdgeInsets.zero),
                            ),
                            child: Icon(
                              Icons.settings,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              // if (Platform.isAndroid && !Const.FULL_FLUTTER_ENV)
                              //   FlutterBoost.singleton.open(getNativePageUrl(NativePageName.SETTING));
                              // else
                              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.setting);
                            }))
                  ])),
              Stack(
                alignment: Alignment.centerRight,
                children: [
                  Row(
                    children: [
                      Expanded(child: Container()),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              // 跳转修改信息页面
                              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.edit_profile);
                            },
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(20), vertical: ScreenUtil().setWidth(10)),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Image.asset(
                                    'assets/images/location.png',
                                    width: ScreenUtil().setWidth(18),
                                    height: ScreenUtil().setWidth(22),
                                    color: Colors.white,
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(10)),
                                  ConstrainedBox(
                                    constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(150)),
                                    child: Text(
                                      YBDCommonUtil.getCountryByCode(widget.userInfo!.country)!,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.3),
                                  borderRadius:
                                      BorderRadius.horizontal(left: Radius.circular(ScreenUtil().setWidth(32)))),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  Stack(alignment: Alignment.bottomCenter, children: [
                    Center(
                      child: GestureDetector(
                        onTap: () {
                          String aPath = YBDImageUtil.avatar(context, widget.userInfo!.headimg, widget.userInfo!.id);
                          if (aPath != '')
                            YBDNavigatorHelper.navigateTo(
                                context, YBDCommonUtil.getPhotoViewUrl([aPath], 0, download: false));
                        },
                        child: YBDRoundAvatar(
                          widget.userInfo?.headimg,
                          sex: widget.userInfo?.sex,
                          avatarWidth: 150,
                          userId: widget.userInfo?.id,
                          needNavigation: false,
                          labelWitdh: 50,
                          lableSrc:
                              (widget.userInfo?.vip ?? 0) == 0 ? '' : 'assets/images/vip_lv${widget.userInfo!.vip}.png',
                          scene: 'C',
                          showVip: false,
                          labelPadding: EdgeInsets.only(right: ScreenUtil().setWidth(18)),
                          levelFrame: widget.userInfo!.headFrame,
                        ),
                      ),
                    ),
                    //由于 YBDRoundAvatar 使用地方太多了 就不加到里面
                    // YBDTopTalentItem(
                    //   widget.userInfo.extendVo == null ? 0 : widget.userInfo.extendVo.activityRanking,
                    //   width: ScreenUtil().setWidth(190),
                    //   height: ScreenUtil().setWidth(86),
                    //   font: 19,
                    // ),
                  ]),
                ],
              ),
              Container(
                height: ScreenUtil().setWidth(lessLayout ? 88 : 68),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        if (_certificationInfos != null)
                          SizedBox(
                            width: ScreenUtil().setWidth(40),
                          ),
                        Text(
                          widget.userInfo?.nickname ?? '',
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(32),
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            height: 1.9,
                          ),
                        ),
                        if (_certificationInfos != null) ...[
                          SizedBox(
                            width: ScreenUtil().setWidth(6),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: ScreenUtil().setWidth(10)),
                            child: YBDNetworkImage(
                              imageUrl: _certificationInfos!.icon!,
                              width: ScreenUtil().setWidth(34),
                            ),
                          )
                        ]
                      ],
                    ),
                    luckyID != null && luckyID.isNotEmpty
                        ? Container(
                            width: ScreenUtil().setWidth(120),
                            height: ScreenUtil().setWidth(36),
                            padding: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/images/lucky_id_bg.webp'), fit: BoxFit.fill)),
                            alignment: Alignment.center,
                            child: Text(
                              luckyID,
                              style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(16)),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
              // ID和复制按钮
              isAgent ? _getAgentIcon() : _getIdArea(),
              // SizedBox(height: ScreenUtil().setWidth(16)),
              _getRow(),
              ..._getCertified(),
              SizedBox(height: ScreenUtil().setWidth(6)),

              YBDVipIcon(
                widget.userInfo?.vipIcon ?? '',
                size: 48,
                padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(2)),
              ),
              SizedBox(height: ScreenUtil().setWidth(lessLayout ? 28 : 8)),

              Container(
                padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                child: Text(
                  // 个人语录
                  // TODO: 翻译
                  widget.userInfo!.comment ?? 'Hey, I am an OyeTalk Member.',
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(24),
                    color: Color(0x99ffffff),
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(10)),
              // 关注数，粉丝数，好友数
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      // 好友数
                      onTap: () {
                        logger.v('clicked friends item');
                        YBDNavigatorHelper.navigateTo(
                          context,
                          YBDNavigatorHelper.follow + '/${widget.userInfo!.id}/0',
                        );
                      },
                      child: Container(
                        width: ScreenUtil().setWidth(150),
                        height: ScreenUtil().setWidth(110),
                        decoration: BoxDecoration(shape: BoxShape.rectangle),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              // 好友数量
                              widget.userInfo!.friends?.toString() ?? '0',
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(36),
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                height: 1.2,
                              ),
                            ),
                            SizedBox(
                              height: ScreenUtil().setWidth(6),
                            ),
                            Text(
                              // 好友文字
                              translate('friends'),
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(24),
                                color: Color(0x99ffffff),
                                height: 1.2,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(80)),
                    GestureDetector(
                      // 追踪数
                      onTap: () {
                        logger.v('clicked following item');
                        YBDNavigatorHelper.navigateTo(
                          context,
                          YBDNavigatorHelper.follow + '/${widget.userInfo!.id}/1',
                        );
                      },
                      child: Container(
                        width: ScreenUtil().setWidth(150),
                        height: ScreenUtil().setWidth(110),
                        decoration: BoxDecoration(shape: BoxShape.rectangle),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              // 数量
                              widget.userInfo!.concern.toString(),
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(36),
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                height: 1.2,
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setWidth(6)),
                            Text(
                              // 文字
                              translate('following'),
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(24),
                                color: Color(0x99ffffff),
                                height: 1.2,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(80)),
                    GestureDetector(
                      // 粉丝数
                      onTap: () {
                        logger.v('clicked follower item');
                        YBDNavigatorHelper.navigateTo(
                          context,
                          YBDNavigatorHelper.follow + '/${widget.userInfo!.id}/2',
                        );
                      },
                      child: Container(
                        width: ScreenUtil().setWidth(150),
                        height: ScreenUtil().setWidth(110),
                        decoration: BoxDecoration(shape: BoxShape.rectangle),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              // 数量
                              widget.userInfo!.fans.toString(),
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(36),
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                height: 1.2,
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setWidth(6)),
                            Text(
                              // 文字
                              translate('followers'),
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(24),
                                color: Color(0x99ffffff),
                                height: 1.2,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(80)),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: ScreenUtil().setWidth(640) + ScreenUtil().statusBarHeight),
          height: ScreenUtil().setWidth(106),
          decoration: BoxDecoration(
              color: YBDActivitySkinRoot().curAct().myprofileRowColor(),
              borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(40)))),
          child: Stack(
            children: <Widget>[
              Container(
                height: ScreenUtil().setWidth(106),
                padding: EdgeInsets.only(top: 0),
                child: Row(
                  children: [
                    _getBigButton('assets/images/topup/y_top_up_beans@2x.webp', 38,
                        widget.userInfo?.money == null ? '0' : (YBDNumericUtil.format(widget.userInfo?.money) ?? ''), () {
                      YBDNavigatorHelper.openTopUpPage(context);
                    }),
                    // Container(
                    //   width: ScreenUtil().setWidth(1),
                    //   height: ScreenUtil().setWidth(70),
                    //   color: Colors.white.withOpacity(0.1),
                    // ),
                    _getBigButton(
                        'assets/images/icon_gold.webp',
                        33,
                        widget.userInfo!.golds == null
                            ? '0'
                            : YBDNumericUtil.compactNumberWithFixedDigits('${widget.userInfo!.golds}',
                                fixedDigits: 2, removeZero: false, rounding: false, goldType: true)!, () async {
                      logger.v('clicked gold btn');
                      final store = YBDCommonUtil.storeFromContext();
                      final url = store?.state?.configs?.beans2GoldsUrl ?? 'http://121.37.214.86/talent/#/exchangeGold';
                      YBDNavigatorHelper.navigateToWeb(context, '?url=${Uri.encodeComponent(url)}&showNavBar=false');
                    }),
                    _getBigButton('assets/images/icon_gems.webp', 36,
                        widget.userInfo!.gems == null ? '0' : YBDNumericUtil.format(widget.userInfo!.gems)!, () async {
                      logger.v('clicked gem btn');
                      final store = YBDCommonUtil.storeFromContext(context: context)!;
                      final url = store.state.configs!.gemsBeans!;
                      YBDNavigatorHelper.navigateToWeb(
                        context,
                        '?url=${Uri.encodeComponent(url)}&showNavBar=false',
                      );
                    }),
                  ],
                ),
              ),
            ],
          ),
        ),
        Positioned(
          top: ScreenUtil().setWidth(190) + ScreenUtil().statusBarHeight,
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            child: YBDTopTalentItem(
              widget.userInfo!.extendVo == null ? 0 : widget.userInfo!.extendVo!.activityRanking,
              width: ScreenUtil().setWidth(190),
              height: ScreenUtil().setWidth(86),
              font: 22,
            ),
          ),
        ),
      ],
    );
  }
  void myBuildcYtLHoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _queryCer() async {
    YBDUserCertificationEntity? userCertificationEntity = await ApiHelper.queryCertification(context, [widget.userInfo!.id]);
    isCerRequested = true;
    if (userCertificationEntity?.returnCode == Const.HTTP_SUCCESS) {
      _certificationInfos = YBDCommonUtil.getUsersSingleCer(userCertificationEntity!.record, widget.userInfo!.id);
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    YBDTPGlobal.s3Host = widget.userInfo!.backgrounds?.host ?? '';
    checkIsAgent();
    _queryCer();
  }
  void initStatez2xhfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
