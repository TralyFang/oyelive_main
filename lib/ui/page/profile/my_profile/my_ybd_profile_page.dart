import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/module/entity/base_ybd_resp_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/profile/account_safe/account_ybd_safe_controller.dart';
import 'package:oyelive_main/ui/page/profile/account_safe/safe_ybd_alert.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/database_ybd_until.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/agency/agency_ybd_api_helper.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/status/entity/db_ybd_status_entity.dart';
import '../../../../module/status/entity/status_ybd_entity.dart';
import '../../../../module/status/entity/status_ybd_list_entity.dart';
import '../../../../module/status/status_ybd_api_helper.dart';
import '../../../../module/status/status_ybd_def.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../status/handler/status_ybd_delete_event.dart';
import 'center_ybd_part.dart';
import 'top_ybd_part.dart';
import 'update_ybd_profile_event.dart';

class YBDMyProfilePage extends StatefulWidget {
  @override
  YBDMyProfilePageState createState() => new YBDMyProfilePageState();
}

class YBDMyProfilePageState extends BaseState<YBDMyProfilePage>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  int currentPage = 1;
  bool hasNext = true;
  final pageSize = 10;

  bool canLoadMore = false;

  List<YBDStatusInfo?>? rowsData;
  RefreshController _refreshController = new RefreshController();

  /// 是否签约主播
  bool isOfficialTalent = false;

  /// 加载动态超时标志位
  bool statusTimeOut = false;

  final ct = Get.put(YBDAccountSafeController());
  final tc = Get.put(YBDTextCtrl());

  bool? showTalentTime = false;

  @override
  Widget myBuild(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return Scaffold(
      body: StoreBuilder<YBDAppState>(
        builder: (context, store) {
          var iconList = [
            YBDTabsInProfile(translate('aristocracy'),
                YBDActivitySkinRoot().curAct().myProfileVip(), onTap: () {
              _track('VIP');
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.vip);
            }),
            YBDTabsInProfile(translate('top_up'),
                YBDActivitySkinRoot().curAct().myProfileTopUp(), onTap: () {
              YBDNavigatorHelper.openTopUpPage(context);
            }),
            YBDTabsInProfile(translate('invite_friends'),
                YBDActivitySkinRoot().curAct().myProfileInvite(), onTap: () {
              // if (Platform.isAndroid) FlutterBoost.singleton.open(getNativePageUrl(NativePageName.INVITE_FRIENDS));
              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                YBDEventName.OPEN_PAGE,
                location: YBDLocationName.MY_PROFILE_PAGE,
                itemName: YBDLocationName.INVITE_PAGE,
              ));
              _track('invite');
              YBDNavigatorHelper.navigateTo(
                  context, YBDNavigatorHelper.my_invites);
            }),
            YBDTabsInProfile(translate('daily_tasks'),
                YBDActivitySkinRoot().curAct().myProfileDailyTask(), onTap: () {
              _track('task');
              YBDNavigatorHelper.navigateTo(
                  context, YBDNavigatorHelper.daily_task);
            }),
            YBDTabsInProfile(translate('store'),
                YBDActivitySkinRoot().curAct().myProfileStore(), onTap: () {
              _track('store');
              String? index = store.state.configs!.storeInitIndex;
              YBDNavigatorHelper.navigateTo(
                  context, YBDNavigatorHelper.store + '/$index');
            }),
            YBDTabsInProfile(translate('baggage'),
                YBDActivitySkinRoot().curAct().myProfileBaggage(), onTap: () {
              logger.v('clicked baggage btn');
              _track('baggage');
              // 用户 id 传空时显示自己的背包
              YBDNavigatorHelper.navigateTo(
                  context, YBDNavigatorHelper.baggage + "/");
            }),
            //* 2.4 把徽章和等级移动到此位置
            YBDTabsInProfile(translate('badges'),
                YBDActivitySkinRoot().curAct().myProfileBadges(), onTap: () {
              logger.v('clicked badges btn');
              _track('badge');
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.badges);
            }),
            // 等级
            YBDTabsInProfile(translate('level'),
                YBDActivitySkinRoot().curAct().myProfileLevel(), onTap: () {
              logger.v('clicked level btn');
              _track('level');
              YBDSPUtil.save(Const.SP_LEVEL, true);
              final store = YBDCommonUtil.storeFromContext(context: context)!;
              logger
                  .v("level page url : ${store.state.configs?.level_page_url}");
              String url = store.state.configs?.level_page_url ?? '';
              YBDNavigatorHelper.navigateToWeb(
                context,
                "?url=${Uri.encodeComponent(url)}&showNavBar=false",
              );
            }),

            ///
            YBDTabsInProfile(translate('guardian'),
                YBDActivitySkinRoot().curAct().myProfileGuardian(), onTap: () {
              // if (Platform.isAndroid) FlutterBoost.singleton.open(getNativePageUrl(NativePageName.GUARDIAN));
              _track('guardian');
              YBDNavigatorHelper.navigateTo(
                  context, YBDNavigatorHelper.guardian);
            }),
            YBDTabsInProfile(translate('support'),
                YBDActivitySkinRoot().curAct().myProfileSupport(), onTap: () {
              _track('support');
              YBDCommonUtil.showSupport(context);
            }),
          ];
          if (showTalentTime!) {
            final store = YBDCommonUtil.storeFromContext(context: context);
            iconList.add(YBDTabsInProfile(translate('talent'),
                YBDActivitySkinRoot().curAct().myProfileTalent(),
                onTap: () async {
              _track('talent');
              // String talentEarning = await ConfigUtil.talentApplyUrl(context);
              final url = store!.state.configs!.talentPayroll!;
              YBDNavigatorHelper.navigateToWeb(
                context,
                "?url=${Uri.encodeComponent(url)}&showNavBar=false",
              );
            }));
          }
          //* 签约主播测试号5000401
          iconList.add(YBDTabsInProfile(translate('pk_record'),
              YBDActivitySkinRoot().curAct().myProfilePkRecord(), onTap: () {
            _track('PK_record');
            YBDNavigatorHelper.navigateTo(
                context, YBDNavigatorHelper.pk_record);
          }));
          return Stack(
            alignment: Alignment.center,
            children: [
              Container(
                decoration: YBDActivitySkinRoot()
                    .curAct()
                    .myprofileIconListBgDecoration(),
                child: SmartRefresher(
                  controller: _refreshController,
                  header:
                      YBDActivitySkinRoot().curAct().activityRefreshHeader(),
                  footer:
                      YBDActivitySkinRoot().curAct().activityRefreshFooter(),
                  enablePullDown: true,
                  enablePullUp: canLoadMore,
                  onRefresh: () {
                    initData();
                    ApiHelper.checkLogin(context);
                    asyncRequestOfficialTalent();
                    YBDTPGlobal.s3Host =
                        store.state.bean!.backgrounds?.host ?? '';
                  },
                  onLoading: () {
                    nextPage();
                  },
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        YBDTopPart(store.state.bean,
                            isOfficialTalent: isOfficialTalent),
                        SizedBox(height: ScreenUtil().setWidth(20)),
                        YBDCenterPart(iconList),
                        SizedBox(height: ScreenUtil().setWidth(50)),
                        Row(
                          children: [
                            Container(
                              width: ScreenUtil().setWidth(160),
                              height: ScreenUtil().setWidth(64),
                              alignment: Alignment.center,
                              child: Text(
                                translate('status'),
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(28),
                                  color: YBDActivitySkinRoot()
                                      .curAct()
                                      .activityTextColor(),
                                ),
                              ),
                              decoration: BoxDecoration(
                                  color: YBDActivitySkinRoot()
                                      .curAct()
                                      .myprofileSlogBgColor(),
                                  borderRadius: BorderRadius.horizontal(
                                    right: Radius.circular(
                                        ScreenUtil().setWidth(32)),
                                  )),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              _safeAlert(),
            ],
          );
        },
      ),
    );
  }

  void myBuilds5du6oyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  Future<YBDStatusListEntity?> getStatusList(
    int page,
  ) async {
    if (page == 1) _refreshController.resetNoData();

    YBDStatusListEntity? result = await YBDStatusApiHelper.queryStatusList(
        context, StatusListType.MyProfile,
        page: page, pageSize: pageSize, status: null); //状态(0:待审，1发布，2拒绝)

    if (result != null && result.code != Const.HTTP_SUCCESS_NEW) {
      logger.v('===load my status error : ${result.message ?? ''}');
      // if (result.code != Const.HTTP_SESSION_TIMEOUT_NEW) YBDToastUtil.toast(result.message ?? '');
    } else {
      hasNext = result == null
          ? false
          : !(result.data!.xList!.rows!.length < pageSize);

      return result;
    }
  }

  Future<YBDStatusListEntity?> initData() async {
    //_refreshController.resetNoData();
    currentPage = 1;
    YBDStatusListEntity? result = await getStatusList(currentPage);
    if (_refreshController.isRefresh) {
      _refreshController.refreshCompleted();
    }
    if (result != null && result.code == Const.HTTP_SUCCESS_NEW) {
      _refreshController.resetNoData();
      currentPage = 1;

      rowsData = result.data!.xList!.rows;
      if (rowsData!.length != 0) {
        canLoadMore = true;
      }
      if (mounted) {
        setState(() {});
      }

      return result;
    } else {
      return null;
    }
  }

  Future<YBDStatusListEntity?> nextPage() async {
    if (hasNext) {
      _refreshController.resetNoData();
      YBDStatusListEntity? result = await getStatusList(++currentPage);
      if (_refreshController.isLoading) {
        _refreshController.loadComplete();
      }
      if (result?.code == Const.HTTP_SUCCESS_NEW) {
        rowsData!.addAll(result?.data?.xList?.rows ?? []);
        setState(() {});
        return result;
      } else {
        return null;
      }
    } else {
      ///没有更多
      ///
      ///
      if (_refreshController.isLoading) {
        _refreshController.loadNoData();
      }

      logger.v("set no more");
    }
  }

  loadHistoryStatus() async {
    YBDDBStatusEntity? dbStatusEntity =
        await YBDDataBaseUtil.getInstance().queryStatus(StatusScene.MyProfile);
    if (dbStatusEntity != null) {
      try {
        YBDStatusListEntity? data = await Future.value(YBDStatusListEntity()
            .fromJson(json.decode(dbStatusEntity.YBDStatusContent ?? '')));
        rowsData = data?.data?.xList?.rows;
        setState(() {});
      } catch (e) {
        print(e);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    // 要判断是否签约主播
    asyncRequestOfficialTalent();

    WidgetsBinding.instance?.addObserver(this);
    loadHistoryStatus();
    initData();

    eventBus.on<YBDStatusDeleteEvent>().listen((YBDStatusDeleteEvent event) {
      var list = rowsData!;

      for (int index = 0; index < list.length; index++) {
        if (list[index]!.id == event.statusId) {
          list.removeAt(index);
          setState(() {});
        }
      }
    });

    eventBus
        .on<YBDUpdateProfileEvent>()
        .listen((YBDUpdateProfileEvent event) async {
      logger.v('received update profile event from : ${event.type}');
      // 刷新动态列表
      initData();
    });

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      ApiHelper.checkLogin(context);
    });
  }

  void initStatesABjDoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    Get.delete<YBDAccountSafeController>();
    super.dispose();
  }

  void disposeEmqrdoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  void didChangeAppLifecycleState0TOL8oyelive(AppLifecycleState state) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 异步查询房间标签
  /// 在 init() 方法里或刷新列表时调用，得到结果后赋值给实例变量
  asyncRequestOfficialTalent() {
    YBDSPUtil.getUserInfo().then((userInfo) async {
      isOfficialTalent =
          await YBDAgencyApiHelper.isOfficialTalent(context, userInfo!.id);
      logger.v('11.17------isOfficialTalent:$isOfficialTalent');
      YBDTPGlobal.isOfficialTalent = isOfficialTalent;
      if (mounted) {
        setState(() {
          showTalentTime = isOfficialTalent;
        });
      }
      YBDBaseRespEntity? respEntity =
          await ApiHelper.queryRemainingTimes(context);
      YBDTPGlobal.nameWarn = respEntity?.returnMsg ?? '';
      tc.setNameWarn(respEntity?.returnMsg ?? '');
      logger.v('22.1.10---getnameWarn:${respEntity?.returnMsg}');
    });
  }

  /// 安全弹框
  Widget _safeAlert() {
    return Obx(
      () {
        bool showAlert =
            (ct.record.value?.window ?? false) && ct.firstOpen.value;
        logger.v(
            'show safe alert: $showAlert, ${ct.firstOpen.value}, ${ct.record.value?.window}');

        // TODO: 测试代码
        // if (false) {
        if (showAlert) {
          // 保存安全弹窗显示时间
          YBDUserUtil.saveSafeAlertTime(Const.SP_USER_OPEN_SAFE_ALERT_TIME);

          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.OPEN_PAGE,
            location: YBDLocationName.MY_PROFILE_PAGE,
            itemName: YBDItemName.SAFE_ALERT_SHOW,
          ));

          return YBDSafeAlert(
            onHideAlert: () {
              // 标记隐藏安全弹框
              ct.openedAlert();
              setState(() {});
            },
          );
        } else {
          return SizedBox();
        }
      },
    );
  }

  /// 点击埋点
  void _track(String name) {
    YBDCommonTrack()
        .commonTrack(YBDTAProps(location: name, module: YBDTAModule.my));
  }
}

void _safeAlertANA6ioyelive() {
  int needCount = 0;
  print('input result:$needCount');
}
