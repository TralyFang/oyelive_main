import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/widget/red_ybd_dot_alert.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import '../../activty/activity_ybd_skin.dart';

import '../../../../base/base_ybd_state.dart';

class YBDCenterPart extends StatefulWidget {
  List<YBDTabsInProfile> rows;

  YBDCenterPart(this.rows);

  @override
  YBDCenterPartState createState() => new YBDCenterPartState();
}

class YBDCenterPartState extends BaseState<YBDCenterPart> {
  var invisible = BorderSide(color: Colors.transparent, width: ScreenUtil().setWidth(0));

  getRight(int index) {
    if ((index + 1) % 3 == 0) {
      return invisible;
    } else {
      return BorderSide(
        color: YBDActivitySkinRoot().curAct().activityTextColor().withOpacity(0.1),
        width: ScreenUtil().setWidth(1),
      );
    }
  }

  getBot(int index) {
    int allRow = (widget.rows.length ~/ 3) + (widget.rows.length % 3 == 0 ? 0 : 1);
    int currentRow = ((index + 1) ~/ 3) + ((index + 1) % 3 == 0 ? 0 : 1);
    if (allRow == currentRow) {
      return invisible;
    } else {
      return BorderSide(
        color: YBDActivitySkinRoot().curAct().activityTextColor().withOpacity(0.1),
        width: ScreenUtil().setWidth(1),
      );
    }
  }

  @override
  Widget myBuild(BuildContext context) {
    return GridView.count(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(32)),
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      crossAxisCount: 3,
      children: List.generate(
          widget.rows.length,
          (index) => GridTile(
                  child: GestureDetector(
                onTap: () async {
                  if (index == 0) {
                    String prefix = await YBDSPUtil.get(Const.SP_ARISTOCRACY);
                    if (prefix == null) prefix = "";
                    if (!prefix.contains(YBDUserUtil.getUserIdSync.toString()))
                      await YBDSPUtil.save(Const.SP_ARISTOCRACY, prefix + YBDUserUtil.getUserIdSync.toString());
                    setState(() {});
                  }
                  widget.rows[index].onTap?.call();
                },
                child: Container(
                  decoration: BoxDecoration(border: Border(right: getRight(index), bottom: getBot(index))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // 2.4.9 替换为自定义图片组件
                      Stack(children: [
                        YBDImage(path: widget.rows[index].imagePath, width: 96, height: 96),
                        // 2.6.0 添加new标签后面版本需要去除
                        Positioned(
                            top: 0,
                            right: 0,
                            child: index == 7 ? YBDRedDotAlert(spKey: Const.SP_LEVEL, dot: false) : Container()),
                        Positioned(
                            top: 0,
                            right: 0,
                            child: index == 0
                                ? YBDRedDotAlert(
                                    spKey: Const.SP_ARISTOCRACY,
                                    dot: false,
                                    showAsId: true,
                                  )
                                : Container())
                      ]),
                      SizedBox(height: ScreenUtil().setWidth(14)),
                      Text(
                        widget.rows[index].title,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(24),
                          color: YBDActivitySkinRoot().curAct().activityTextColor(),
                        ),
                      ),
                    ],
                  ),
                ),
              ))),
      childAspectRatio: 1,
    );
  }
  void myBuildFoZmKoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
  }
  void initStatef3z5joyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDCenterPart oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgett1xMJoyelive(YBDCenterPart oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}

class YBDTabsInProfile {
  String title, imagePath;
  Function? onTap;

  YBDTabsInProfile(this.title, this.imagePath, {this.onTap});
}
