import 'dart:async';

/*
 * @Date: 2021-07-07 11:15:01
 * @LastEditors: William-Zhou
 * @LastEditTime: 2021-10-26 14:07:38
 * @FilePath: \oyetalk_flutter\lib\ui\page\profile\my_profile\pk_record_page.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/date_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/pk_ybd_record_entity.dart';
import 'package:oyelive_main/ui/page/inbox/empty_ybd_view.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/my_ybd_app_bar.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';

class YBDPKRecordPage extends StatefulWidget {
  const YBDPKRecordPage({Key? key}) : super(key: key);

  @override
  _YBDPKRecordPageState createState() => _YBDPKRecordPageState();
}

class _YBDPKRecordPageState extends State<YBDPKRecordPage> {
  RefreshController _refreshController = RefreshController();
  final int pageSize = 10;
  int page = 1;
  // {pkTime: 918172800000, rivalId: 111, result: 'WIN', gift: 100, rivalGift:50}
  List<YBDPKRecord?>? dataList;
  @override
  void initState() {
    super.initState();
    getPKRecords();
  }
  void initStateiQr2Voyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void dispose9yT3Xoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: YBDMyAppBar(
        title: Text(
          translate('pk_record'),
          style: TextStyle(
            color: Colors.white,
            fontSize: YBDTPStyle.spNav,
          ),
        ),
        backgroundColor: YBDTPStyle.heliotrope,
        leading: YBDNavBackButton(color: Colors.white),
        elevation: 0.0,
      ),
      body: Container(
        width: double.infinity,
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: [
            _tableHead(),
            Expanded(child: _pageContent()),
          ],
        ),
      ),
    );
  }
  void buildgOgtRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 表头
  Widget _tableHead() {
    return Container(
      height: ScreenUtil().setWidth(90),
      color: Color(0xffFFFFFF).withOpacity(0.1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(translate('pk_record_date'), style: tableHeadStyle),
          Text(translate('pk_record_rival_id'), style: tableHeadStyle),
          Text(translate('pk_record_result'), style: tableHeadStyle),
          Text(translate('pk_record_gift'), style: tableHeadStyle),
          Text(translate('pk_record_rival_gift'), style: tableHeadStyle),
        ],
      ),
    );
  }
  void _tableHeadGxQo9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  TextStyle tableHeadStyle = TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xffFFFFFF));
  TextStyle tableBodyStyle = TextStyle(fontSize: ScreenUtil().setSp(22), color: Color(0xffFFFFFF));
  Widget _tableBodyText(String text, {int width = 120}) {
    return Container(
      width: ScreenUtil().setWidth(width),
      child: Text(text, style: tableBodyStyle, textAlign: TextAlign.center),
    );
  }
  void _tableBodyTextLFRP2oyelive(String text, {int width = 120}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 页面内容
  Widget _pageContent() {
    if (dataList == null) return YBDLoadingCircle();
    return dataList!.length == 0
        ? YBDEmptyView()
        : SmartRefresher(
            controller: _refreshController,
            header: YBDMyRefreshIndicator.myHeader,
            footer: YBDMyRefreshIndicator.myFooter,
            enablePullDown: true,
            enablePullUp: false,
            onRefresh: getPKRecords,
            onLoading: nextPage,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  ListView.separated(
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: dataList!.length,
                      separatorBuilder: (_, index) => Container(
                            height: ScreenUtil().setWidth(1),
                            color: Colors.white.withOpacity(0.1),
                            // margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                          ),
                      itemBuilder: (_, index) => Material(
                            // color: Colors.black,
                            color: Colors.transparent,
                            child: InkWell(
                              splashColor: Colors.white.withOpacity(0.1),
                              onTap: () {
                                logger.v('clicked pk record');
                              },
                              child: Container(
                                height: ScreenUtil().setWidth(90),
                                // padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                                child: Row(
                                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    SizedBox(width: ScreenUtil().setWidth(10)),
                                    _tableBodyText(
                                        '${YBDDateUtil.dateWithFormat(dataList![index]!.createTime, yearFormat: 'MM-dd-yyyy HH:mm:ss')}',
                                        width: 130),
                                    SizedBox(width: ScreenUtil().setWidth(10)),
                                    _tableBodyText('${dataList![index]!.competitor}'),
                                    SizedBox(width: ScreenUtil().setWidth(30)),
                                    _tableBodyText('${getResult(dataList![index]!.result)}'),
                                    SizedBox(width: ScreenUtil().setWidth(15)),
                                    _tableBodyText('${YBDNumericUtil.format(dataList![index]!.gifts)}'),
                                    SizedBox(width: ScreenUtil().setWidth(35)),
                                    _tableBodyText('${YBDNumericUtil.format(dataList![index]!.competitorGifts)}', width: 90),
                                  ],
                                ),
                              ),
                            ),
                          )),
                  Container(
                    height: ScreenUtil().setWidth(1),
                    color: Colors.white.withOpacity(0.1),
                  ),
                ],
              ),
            ),
          );
  }
  void _pageContentVqxFdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getPKRecords({int? setPage}) async {
    if (setPage == null) {
      page = 1;
    }
    YBDPKRecordEntity? beanDetailEntity = await ApiHelper.getPKRecords(context, pageSize: pageSize, page: page);
    if (_refreshController.isLoading) _refreshController.loadComplete();
    if (_refreshController.isRefresh) _refreshController.refreshCompleted();
    if (beanDetailEntity != null && beanDetailEntity.returnCode == Const.HTTP_SUCCESS) {
      if (page == 1) {
        dataList = beanDetailEntity.record;
      } else {
        dataList!.addAll(beanDetailEntity.record!);
      }
      if (beanDetailEntity.record!.length < pageSize) {
        _refreshController.loadNoData();
      } else {
        _refreshController.resetNoData();
      }
      setState(() {
        // 时间倒序排列
        dataList!.sort((a, b) => b!.createTime!.compareTo(a!.createTime!));
      });
    }
  }

  nextPage() {
    getPKRecords(setPage: ++page);
  }

  getResult(int? result) {
    switch (result) {
      case 0:
        return 'Draw';
        break;
      case 1:
        return 'Win';
        break;
      case 2:
        return 'Lose';
        break;
      default:
        return 'Draw';
        break;
    }
  }
}
