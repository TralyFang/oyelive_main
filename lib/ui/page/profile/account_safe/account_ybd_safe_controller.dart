import 'dart:async';

import 'package:get/get.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/entity/safe_ybd_alert_entity.dart';

class YBDAccountSafeController extends GetxController {
  Rx<YBDSafeAlertRecord?> record = YBDSafeAlertRecord().obs;
  var firstOpen = false.obs;

  @override
  void onInit() {
    super.onInit();
    logger.i('YBDAccountSafeController close');
    _checkFirstOpen();
    requestSafeAlert();
  }
  void onInitTMm64oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void onClose() {
    logger.i('YBDAccountSafeController close');
    super.onClose();
  }
  void onCloseNs16Hoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 标记已显示过弹框
  void openedAlert() {
    firstOpen.value = false;
  }

  /// 检查是否当天第一次打开安全弹框
  Future<void> _checkFirstOpen() async {
    firstOpen.value = await YBDUserUtil.todayFirstOpenSafeAlert();
    logger.i('_checkFirstOpen: ${firstOpen.value}');
  }
  void _checkFirstOpenmGwvboyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求用户安全信息数据
  Future<void> requestSafeAlert() async {
    final YBDSafeAlertEntity? entity = await ApiHelper.queryUserSafetyLevel(Get.context);
    record.value = entity?.record;
    logger.i('queryUserSafetyLevel show window: ${record.value?.window}');
  }
  void requestSafeAlertZ3wa6oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
