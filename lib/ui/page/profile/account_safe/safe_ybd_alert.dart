import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/profile/account_safe/account_ybd_safe_controller.dart';

class YBDSafeAlert extends StatelessWidget {
  // final YBDSafeAlertEntity safeAlertEntity;
  final VoidCallback? onHideAlert;

  const YBDSafeAlert({
    // this.safeAlertEntity,
    this.onHideAlert,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final ct = Get.find<YBDAccountSafeController>();

    return GestureDetector(
      onTap: () {
        logger.i('clicked safe alert bg');
        onHideAlert?.call();
      },
      child: Material(
        color: Colors.black.withOpacity(0.4),
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // 个人中心页面不包含底部标签栏，这里让弹框向下偏移
              SizedBox(height: Const.BOTTOM_BAR_HEIGHT / 2),
              GestureDetector(
                onTap: () {
                  // 弹框内忽略点击事件
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      ScreenUtil().setWidth(32),
                    ),
                    color: Colors.white,
                  ),
                  width: ScreenUtil().setWidth(500),
                  // height: ScreenUtil().setWidth(576),
                  child: Column(
                    children: [
                      SizedBox(height: ScreenUtil().setWidth(45)),
                      GetBuilder<YBDAccountSafeController>(builder: (ctr) {
                        return Container(
                          width: ScreenUtil().setWidth(157),
                          child: Image.asset(
                            _safeAlertImg(ctr.record.value?.safety),
                            fit: BoxFit.cover,
                          ),
                        );
                      }),
                      SizedBox(height: ScreenUtil().setWidth(30)),
                      GetBuilder<YBDAccountSafeController>(
                        builder: (ctr) {
                          String safeInfo = '(${ctr.record.value?.level}/${ctr.record.value?.total})';
                          return Text(
                            '${translate('safe_alert_title')}$safeInfo',
                            style: TextStyle(
                              color: Colors.black.withOpacity(0.85),
                              fontSize: ScreenUtil().setSp(28),
                              fontWeight: FontWeight.w500,
                            ),
                          );
                        },
                      ),
                      SizedBox(height: ScreenUtil().setWidth(15)),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(50),
                        ),
                        child: Text(
                          translate('safe_alert_content'),
                          style: TextStyle(
                            color: Color(0xff333333),
                            fontSize: ScreenUtil().setSp(24),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(45)),
                      GestureDetector(
                        onTap: () {
                          logger.v('clicked safe alert improve btn');
                          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                            YBDEventName.CLICK_EVENT,
                            itemName: YBDItemName.SAFE_ALERT_IMPROVE,
                          ));

                          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.account_safe);
                          onHideAlert?.call();
                        },
                        child: Container(
                          width: ScreenUtil().setWidth(300),
                          height: ScreenUtil().setWidth(65),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                            ),
                            borderRadius: BorderRadius.circular(
                              ScreenUtil().setWidth(33),
                            ),
                          ),
                          child: Text(
                            translate('safe_alert_improve'),
                            style: TextStyle(
                              color: Colors.white.withOpacity(0.85),
                              fontSize: ScreenUtil().setSp(28),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(45)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void builduT12yoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String _safeAlertImg(String? safety) {
    logger.v('safe alert safety: $safety');

    if (safety == 'Low') {
      return 'assets/images/profile/safe_low_icon.webp';
    }

    if (safety == 'Medium') {
      return 'assets/images/profile/safe_medium_icon.webp';
    }

    if (safety == 'High') {
      return 'assets/images/profile/safe_high_icon.webp';
    }

    return 'assets/images/profile/safe_low_icon.webp';
  }
  void _safeAlertImggtvCDoyelive(String? safety) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
