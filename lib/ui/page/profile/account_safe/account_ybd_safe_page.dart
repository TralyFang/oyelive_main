import 'dart:async';

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/event/safe_ybd_account_bus_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/profile/account_safe/account_ybd_safe_controller.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/util/profile_ybd_edit_util.dart';
import 'package:oyelive_main/ui/widget/my_ybd_app_bar.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';
import 'package:oyelive_main/ui/widget/profile_ybd_edit_row.dart';

class YBDAccountSafePage extends StatefulWidget {
  const YBDAccountSafePage({
    Key? key,
  }) : super(key: key);

  @override
  _YBDAccountSafePageState createState() => _YBDAccountSafePageState();
}

class _YBDAccountSafePageState extends State<YBDAccountSafePage> {
  final YBDTextCtrl tc = Get.put(YBDTextCtrl());
  final YBDAccountSafeController safeCtr = Get.find<YBDAccountSafeController>();
  late StreamSubscription<YBDSafeAccountBusEvent> _safeAccountSubscription;

  @override
  void initState() {
    super.initState();

    _safeAccountSubscription = eventBus.on<YBDSafeAccountBusEvent>().listen((YBDSafeAccountBusEvent event) async {
      logger.i('received safe account bus event');
      safeCtr.requestSafeAlert();
    });

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.OPEN_PAGE,
      location: YBDLocationName.ACCOUNT_SAFE_PAGE,
    ));
  }
  void initStatedGoNMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _safeAccountSubscription.cancel();
    super.dispose();
  }
  void dispose7nsgKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: YBDMyAppBar(
        backgroundColor: Colors.white,
        leading: YBDNavBackButton(color: Colors.black),
        elevation: 0.0,
        title: Text(
          translate('account_safe'),
          style: TextStyle(
            color: Colors.black,
            fontSize: ScreenUtil().setSp(32),
          ),
        ),
      ),
      body: Container(
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              _safeTopBg(),
              _topText(),
              SizedBox(height: ScreenUtil().setWidth(25)),
              _phoneRow(),
              _pwdRow(),
              _greyLine(),
              _nickNameRow(),
              YBDTPGlobal.isOfficialTalent! ? SizedBox() : Obx(() => profileEditNameWarn('${tc.nameWarn}')),
              _sexRow(),
              _locationRow(),
              _bottomText(),
            ],
          ),
        ),
      ),
    );
  }
  void buildClvtioyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 顶部背景图
  Widget _safeTopBg() {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: double.infinity,
            child: Image.asset(
              _safeBgImg(),
              fit: BoxFit.fitWidth,
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: ScreenUtil().setWidth(157),
                child: Image.asset(
                  _safeIcon(),
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(14)),
              Obx(() {
                return Text(
                  _safeAlertTitle(),
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: ScreenUtil().setSp(24),
                  ),
                );
              })
            ],
          ),
        ],
      ),
    );
  }
  void _safeTopBg1OZMhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 顶部描述内容
  Widget _topText() {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(90),
      ),
      child: Text(
        translate('safe_alert_content'),
        style: TextStyle(
          color: Colors.black,
          fontSize: ScreenUtil().setSp(24),
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
  void _topTextsrpiwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部描述内容
  Widget _bottomText() {
    return Padding(
      padding: EdgeInsets.all(
        ScreenUtil().setWidth(30),
      ),
      child: Text(
        translate('safe_setting_detail'),
        style: TextStyle(
          color: Color(0xff868686),
          fontSize: ScreenUtil().setSp(24),
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  /// 手机号
  Widget _phoneRow() {
    return YBDProfileEditRow(
      translate('phone_number'),
      EditType.Phone,
      YBDCommonUtil.storeFromContext()!.state.bean,
      clickedCallback: () {
        logger.v('phone callback');
        if (YBDCommonUtil.storeFromContext()!.state.bean!.cellphone == null ||
            YBDCommonUtil.storeFromContext()!.state.bean!.cellphone!.isEmpty) {
          profileEditShowPhoneEditDialog();
        }
      },
    );
    // return Container(
    //   // color: Colors.green,
    //   height: ScreenUtil().setWidth(96),
    //   padding: EdgeInsets.symmetric(
    //     horizontal: ScreenUtil().setWidth(33),
    //   ),
    //   child: Row(
    //     children: [
    //       Text(
    //         'Phone',
    //         style: TextStyle(
    //           color: Color(0xff333333),
    //           fontSize: ScreenUtil().setSp(28),
    //         ),
    //       ),
    //       Expanded(child: SizedBox()),
    //       Text(
    //         'YBDEnter Phone Number',
    //         style: TextStyle(
    //           color: Color(0xff868686),
    //           fontSize: ScreenUtil().setSp(28),
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
  void _phoneRowLUuCAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 密码
  Widget _pwdRow() {
    return GestureDetector(
      onTap: () {
        logger.v('clicked change pwd row');
        // 跳转到修改密码界面
        YBDNavigatorHelper.navigateTo(
          context,
          YBDNavigatorHelper.fix_password + "/false" + "/${YBDLocationName.ACCOUNT_SAFE_PAGE}",
        );
      },
      child: Container(
        decoration: BoxDecoration(),
        // color: Colors.green,
        height: ScreenUtil().setWidth(96),
        padding: EdgeInsets.fromLTRB(
          ScreenUtil().setWidth(33),
          0,
          ScreenUtil().setWidth(20),
          0,
        ),
        child: Row(
          children: [
            Text(
              translate('password'),
              style: TextStyle(
                color: Color(0xff333333),
                fontSize: ScreenUtil().setSp(28),
              ),
            ),
            Expanded(child: SizedBox()),
            Obx(() {
              bool hasPwd = safeCtr.record.value?.userInfo?.password ?? false;
              return Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setWidth(hasPwd ? 10 : 0),
                ),
                child: Text(
                  hasPwd ? '******' : translate('no_setting'),
                  style: TextStyle(
                    color: Color(0xff868686),
                    fontSize: ScreenUtil().setSp(28),
                  ),
                ),
              );
            }),
            Container(
              width: ScreenUtil().setWidth(48),
              child: Image.asset(
                "assets/images/icon_more_grey.png",
                fit: BoxFit.cover,
              ),
            )
          ],
        ),
      ),
    );
  }
  void _pwdRowurVAcoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 昵称
  Widget _nickNameRow() {
    return YBDProfileEditRow(
      translate('nick_name'),
      EditType.NickName,
      YBDCommonUtil.storeFromContext()!.state.bean,
      clickedCallback: () {
        logger.v('nick name callback');
        profileEditShowInputDialog(
          EditType.NickName,
          YBDCommonUtil.storeFromContext()!.state.bean!.nickname,
        );
      },
      showInfoBtn: true,
      infoBtnClick: () {
        profileEditClickNameWarn('15', '5', '15');
      },
    );
  }

  /// 性别
  Widget _sexRow() {
    return YBDProfileEditRow(
      translate('gender'),
      EditType.Gender,
      YBDCommonUtil.storeFromContext()!.state.bean,
      clickedSexCallback: (int sex) {
        logger.v('clicked sex : $sex');
        YBDDialogUtil.showLoading(context);
        profileEditSaveToNetwork(EditType.Gender, '$sex', saveSuccess: () {
          YBDDialogUtil.hideLoading(context);
        });
      },
    );
  }
  void _sexRowtGIcaoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 位置
  Widget _locationRow() {
    return YBDProfileEditRow(
      translate('location'),
      EditType.Location,
      YBDCommonUtil.storeFromContext()!.state.bean,
      clickedCallback: () {
        logger.v('location callback');
        profileEditShowCountryDialog();
      },
      showInfoBtn: true,
      infoBtnClick: () {
        logger.v('22.1.5----clickLocationWarn');
        profileEditClickLocationWarn(
          YBDCommonUtil.storeFromContext()!.state.configs?.user_modify_country_day,
        );
      },
    );
  }

  Widget _greyLine() {
    return Divider(
      height: ScreenUtil().setWidth(1),
      thickness: ScreenUtil().setWidth(1),
      color: Color(0xffEAEAEA),
    );
  }

  /// 已填写的安全项数量
  String _safeAlertTitle() {
    final ct = Get.find<YBDAccountSafeController>();
    return '${ct.record.value?.level}/${ct.record.value?.total}';
  }
  void _safeAlertTitlebNsaNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 安全盾牌图标
  String _safeIcon() {
    final ct = Get.find<YBDAccountSafeController>();
    String? safety = ct.record.value?.safety;
    logger.v('safe alert safety: $safety');

    if (safety == 'Low') {
      return 'assets/images/profile/safe_low_icon.webp';
    }

    if (safety == 'Medium') {
      return 'assets/images/profile/safe_medium_icon.webp';
    }

    if (safety == 'High') {
      return 'assets/images/profile/safe_high_icon.webp';
    }

    return 'assets/images/profile/safe_low_icon.webp';
  }

  /// 顶部背景图
  String _safeBgImg() {
    final ct = Get.find<YBDAccountSafeController>();
    String? safety = ct.record.value?.safety;
    logger.v('safe alert safety: $safety');

    if (safety == 'Low') {
      return 'assets/images/profile/safe_low_bg.png';
    }

    if (safety == 'Medium') {
      return 'assets/images/profile/safe_medium_bg.png';
    }

    if (safety == 'High') {
      return 'assets/images/profile/safe_high_bg.png';
    }

    return 'assets/images/profile/safe_low_bg.png';
  }
  void _safeBgImgSlHBXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
