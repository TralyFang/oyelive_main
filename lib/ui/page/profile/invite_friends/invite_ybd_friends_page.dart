import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/invite_ybd_info_entity.dart';
import 'invite_ybd_rules.dart';
import 'my_ybd_invites.dart';

class YBDInviteFriendsPage extends StatefulWidget {
  @override
  YBDInviteFriendsPageState createState() => new YBDInviteFriendsPageState();
}

class YBDInviteFriendsPageState extends BaseState<YBDInviteFriendsPage> with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Scaffold(
      backgroundColor: YBDTPStyle.heliotrope,
      appBar: AppBar(
        centerTitle: true,
        leading: GestureDetector(
          child: Icon(Icons.arrow_back, color: Colors.white),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        title: Container(
          // height: ScreenUtil().setWidth(80),
          width: ScreenUtil().setWidth(440),
          child: TabBar(
            controller: _tabController,
            tabs: [
              Tab(
                text: translate('invite_rules'),
              ),
              Tab(
                text: translate('my_invites'),
              ),
            ],
            labelStyle: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white),
            unselectedLabelStyle: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white.withOpacity(0.5)),
            labelColor: Colors.white,
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(90)),
            indicatorColor: Color(0xff7DFAFF),
            labelPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      floatingActionButton: FutureBuilder(
          initialData: null,
          future: getInviteInfo(),
          builder: (c, data) {
            if (data.hasData && data.data != null)
              return GestureDetector(
                onTap: () {
                  logger.v('web share operation: ${data.data}');
                  YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
                      location: YBDLocationName.INVITE_PAGE,
                      itemName: _tabController!.index == 0 ? 'Invite Rules' : 'My Invites'));
                  YBDDialogUtil.showLinkWidget(context, data.data as String, translate('Invite Friends'), 16, shareInside: false);
                },
                child: Container(
                  width: ScreenUtil().setWidth(572),
                  height: ScreenUtil().setWidth(72),
                  margin: EdgeInsets.only(bottom: ScreenUtil().setWidth(40)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/icon_invite_black.webp',
                        width: ScreenUtil().setWidth(30),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(20),
                      ),
                      Text(
                        translate('invite_more_friends'),
                        style: TextStyle(fontSize: ScreenUtil().setSp(30), color: Colors.black),
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40))), color: Colors.white),
                ),
              );
            return Container();
          }),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: TabBarView(
          controller: _tabController,
          children: <Widget>[YBDInviteRules(), YBDMyInvites()],
        ),
      ),
    );
  }
  void myBuildJi3qooyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<dynamic> getInviteInfo() async {
    YBDInviteInfoEntity? infoEntity = await ApiHelper.getInviteInfo(context);
    if (infoEntity?.record?.isNotEmpty ?? false) {
      return infoEntity!.record;
    } else {
      return null;
    }
  }
  void getInviteInfocJY0Royelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }
  void initStatetLaUtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDInviteFriendsPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetgaCRQoyelive(YBDInviteFriendsPage oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
