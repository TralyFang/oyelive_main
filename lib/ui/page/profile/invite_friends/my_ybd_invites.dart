import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/my_ybd_invites_entity.dart';
import '../../inbox/empty_ybd_view.dart';
import '../../inbox/system_msg/widget/opacity_ybd_bg_net_img.dart';
import '../../status/follow_ybd_button.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';

class YBDMyInvites extends StatefulWidget {
  @override
  YBDMyInvitesState createState() => new YBDMyInvitesState();
}

class YBDMyInvitesState extends State<YBDMyInvites> with AutomaticKeepAliveClientMixin {
  RefreshController _refreshController = RefreshController();
  final int pageSize = 10;
  int page = 1;
  List<YBDMyInvitesRecord?>? dataList;
  getMyInvites({int? setPage}) async {
    if (setPage == null) {
      page = 1;
    }
    YBDMyInvitesEntity? myInvitesEntity = await ApiHelper.getMyInvites(context, pageSize: pageSize, page: page);
    if (_refreshController.isLoading) _refreshController.loadComplete();
    if (_refreshController.isRefresh) _refreshController.refreshCompleted();
    if (myInvitesEntity != null && myInvitesEntity.returnCode == Const.HTTP_SUCCESS) {
      if (page == 1) {
        dataList = myInvitesEntity.record;
      } else {
        dataList!.addAll(myInvitesEntity.record!);
      }
      if (myInvitesEntity.record!.length < pageSize) {
        _refreshController.loadNoData();
      } else {
        _refreshController.resetNoData();
      }
      setState(() {});
    }
  }

  nextPage() {
    getMyInvites(setPage: ++page);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement myBuild
    super.build(context);
    if (dataList == null) return Center(child: YBDLoadingCircle());
    return dataList!.length == 0
        ? YBDEmptyView()
        : SmartRefresher(
            controller: _refreshController,
            header: YBDMyRefreshIndicator.myHeader,
            footer: YBDMyRefreshIndicator.myFooter,
            enablePullDown: true,
            enablePullUp: true,
            onRefresh: getMyInvites,
            onLoading: nextPage,
            child: ListView.separated(
                itemCount: dataList!.length,
                separatorBuilder: (_, index) => Container(
                      height: ScreenUtil().setWidth(1),
                      color: Colors.white.withOpacity(0.1),
                      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                    ),
                itemBuilder: (_, index) => Container(
                      height: ScreenUtil().setWidth(152),
                      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              YBDNavigatorHelper.navigateTo(
                                  context, YBDNavigatorHelper.user_profile + "/${dataList![index]!.userId}");
                            },
                            child: YBDOpacityBgNetImg(
                              // 动态图片和占位图
                              iconUrl: dataList![index]!.userHead ?? '',
                              height: ScreenUtil().setWidth(90),
                              width: ScreenUtil().setWidth(90),
                              radius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40))),
                              defaultWidget: YBDResourcePathUtil.defaultMaleImage(male: false),
                            ),
                          ),
                          SizedBox(
                            width: ScreenUtil().setWidth(36),
                          ),
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 400.px,
                                child: Text(
                                  dataList![index]!.userName ?? '',
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(fontSize: ScreenUtil().setSp(30), color: Colors.white),
                                ),
                              ),
                              SizedBox(
                                height: ScreenUtil().setWidth(10),
                              ),
                              Text(
                                'You have got ${YBDNumericUtil.format(dataList![index]!.parentRewards)} beans rewards',
                                style:
                                    TextStyle(fontSize: ScreenUtil().setSp(20), color: Colors.white.withOpacity(0.7)),
                              ),
                            ],
                          ),
                          Spacer(),
                          YBDFollowButton(
                            dataList![index]?.concern ?? false,
                            dataList![index]?.userId?.toString(),
                            "locationForAnalytics",
                            onChange: (x) {
                              dataList![index]?.concern = x;
                              setState(() {});
                            },
                          )
                        ],
                      ),
                    )),
          );
  }
  void buildZqOfmoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMyInvites();
  }
  void initState6rB2toyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDMyInvites oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidget6ZzJjoyelive(YBDMyInvites oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
