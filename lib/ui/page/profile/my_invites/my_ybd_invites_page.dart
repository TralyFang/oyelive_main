import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../base/page_ybd_bloc_provider.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/share_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/colored_ybd_safe_area.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_invite_item.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';
import '../../../widget/sharing_ybd_widget.dart';

import 'my_ybd_invites_page_bloc.dart';

class YBDMyInvitesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => YBDMyInvitesPageState();
}

class YBDMyInvitesPageState extends BaseState<YBDMyInvitesPage> {
  RefreshController _refreshController = RefreshController();

  @override
  Widget myBuild(BuildContext context) {
    YBDMyInvitesPageBloc bloc = YBDPageBlocProvider.of<YBDMyInvitesPageBloc>(context);
    return StreamBuilder(
      stream: bloc.stream,
      initialData: YBDMyInvitesPageBlocState.initial(),
      builder: (BuildContext context, AsyncSnapshot<YBDMyInvitesPageBlocState?> snapshot) {
        return Scaffold(
          backgroundColor: YBDTPStyle.gentianBlue,
          appBar: AppBar(
            backgroundColor: YBDTPStyle.heliotrope,
            leading: GestureDetector(
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              translate('my_invites'),
              style: TextStyle(color: Colors.white),
            ),
            elevation: 0.0,
          ),
          body: YBDColoredSafeArea(
            child: Container(
              width: double.infinity,
              decoration: YBDTPStyle.gradientDecoration,
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: bloc.state!.loadState == LoadState.init
                          ? YBDLoadingCircle()
                          : (bloc.state!.invitees == null || bloc.state!.invitees!.isEmpty)
                              ? Center(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Image.asset(
                                        "assets/images/empty/empty_my_profile.webp",
                                        width: ScreenUtil().setWidth(382),
                                      ),
                                      SizedBox(
                                        height: ScreenUtil().setWidth(30),
                                      ),
                                      Text(
                                        translate('no_invitee'),
                                        style: TextStyle(color: Colors.white),
                                      )
                                    ],
                                  ),
                                )
                              : SmartRefresher(
                                  controller: _refreshController,
                                  header: YBDMyRefreshIndicator.myHeader,
                                  footer: YBDMyRefreshIndicator.myFooter,
                                  enablePullDown: true,
                                  enablePullUp: true,
                                  onRefresh: () async {
                                    await bloc.loadInvitees(context, refresh: true);
                                    if (_refreshController.isRefresh) {
                                      _refreshController.refreshCompleted(resetFooterState: true);
                                    }
                                  },
                                  onLoading: () async {
                                    await bloc.loadInvitees(context, loadMore: true);
                                    if (_refreshController.isLoading) {
                                      _refreshController.loadComplete();
                                    }
                                    if (bloc.state!.noMore!) {
                                      _refreshController.loadNoData();
                                    }
                                  },
                                  child: ListView.separated(
                                      itemBuilder: (_, index) => Material(
                                            color: Colors.transparent,
                                            child: InkWell(
                                                splashColor: Colors.white10,
                                                highlightColor: Colors.white12,
                                                onTap: () async {
                                                  debugPrint(
                                                      'search item click ... ${bloc.state!.invitees![index]!.inviteeNickname}');

                                                  YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
                                                  if (null != userInfo &&
                                                      '${userInfo.id}' == '${bloc.state!.invitees![index]!.invitee}') {
                                                    YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.myprofile);
                                                  } else {
                                                    YBDNavigatorHelper.navigateTo(
                                                        context,
                                                        YBDNavigatorHelper.user_profile +
                                                            "/${bloc.state!.invitees![index]!.invitee}");
                                                  }
                                                },
                                                child: YBDMyInviteItem(bloc.state!.invitees![index])),
                                          ),
                                      separatorBuilder: (_, index) => Container(
                                            margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                                            height: ScreenUtil().setWidth(1),
                                            color: Const.COLOR_BORDER,
                                          ),
                                      itemCount: bloc.state!.invitees == null ? 0 : bloc.state!.invitees!.length),
                                )),
                  Container(
                    height: ScreenUtil().setWidth(96),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.white10,
                            highlightColor: Colors.white12,
                            onTap: () async {
                              showModalBottomSheet(
                                  context: context,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(ScreenUtil().setWidth(16)),
                                        topRight: Radius.circular(ScreenUtil().setWidth(16))),
                                  ),
                                  builder: (BuildContext context) {
                                    return YBDSharingWidget(
                                      title: 'Invite Friends',
                                    );
                                  });
                            },
                            child: Container(
                              height: ScreenUtil().setWidth(96),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    'assets/images/icon_invite.png',
                                    width: ScreenUtil().setWidth(36),
                                    height: ScreenUtil().setWidth(36),
                                    fit: BoxFit.contain,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(9)),
                                  ),
                                  Text(
                                    translate('invite'),
                                    style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(30)),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )),
                        Container(
                          color: Colors.white.withOpacity(0.3),
                          width: ScreenUtil().setWidth(1),
                          height: ScreenUtil().setWidth(50),
                        ),
                        Expanded(
                            child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.white10,
                            highlightColor: Colors.white12,
                            onTap: () async {
                              await YBDShareUtil.shareSms(context);
                            },
                            child: Container(
                              height: ScreenUtil().setWidth(96),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    'assets/images/icon_contact.png',
                                    width: ScreenUtil().setWidth(36),
                                    height: ScreenUtil().setWidth(36),
                                    fit: BoxFit.contain,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(9)),
                                  ),
                                  Text(
                                    translate('contact'),
                                    style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(30)),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
  void myBuildHpvsMoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();

    YBDMyInvitesPageBloc bloc = YBDPageBlocProvider.of<YBDMyInvitesPageBloc>(context);
    bloc.loadInvitees(context);
  }
  void initStatevSY84oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
