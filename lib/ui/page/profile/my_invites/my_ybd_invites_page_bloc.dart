import 'dart:async';

import 'package:flutter/cupertino.dart';
import '../../../../base/base_ybd_bloc.dart';
import '../../../../common/constant/const.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/entity/query_ybd_my_invites_resp_entity.dart';

class YBDMyInvitesPageBloc extends BaseBloc<YBDMyInvitesPageBlocState?> {
  final int pageSize = 15;
  YBDMyInvitesPageBlocState? state;

  YBDMyInvitesPageBloc() {
    state = YBDMyInvitesPageBlocState.initial();
  }

  loadInvitees(BuildContext context, {bool refresh = false, bool loadMore = false}) async {
    if (refresh) {
      state!.page = 1;
      state!.invitees = [];
      state!.noMore = false;
    } else if (loadMore) {
      state!.page++;
    }

    // 查询邀请用户列表
    YBDQueryMyInvitesRespEntity? result = await ApiHelper.queryMyInvites(context, state!.page, pageSize);
    if (result == null || result.returnCode != Const.HTTP_SUCCESS) {
      state!.invitees = null;
    } else {
      if (result.record == null || result.record!.length < pageSize) {
        state!.noMore = true;
      }
      state!.invitees!.addAll(result.record!);
    }

    state!.loadState = LoadState.loaded;
    sink.add(state);
  }
}

enum LoadState { init, loaded }

class YBDMyInvitesPageBlocState {
  List<YBDInviteeInfo?>? invitees;
  LoadState? loadState;
  int page = 1; // 查询页码
  bool? noMore = false; // 页面无数据

  YBDMyInvitesPageBlocState({this.invitees, this.loadState, this.page = 1, this.noMore});

  factory YBDMyInvitesPageBlocState.initial() => YBDMyInvitesPageBlocState(
        invitees: [],
        loadState: LoadState.init,
        page: 1,
        noMore: false,
      );
}
