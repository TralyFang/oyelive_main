import 'dart:async';

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

class YBDQueryRoomListRespEntity with JsonConvert<YBDQueryRoomListRespEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDRoomInfo?>? record;
  String? recordSum;
}
