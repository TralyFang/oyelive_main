import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/result_ybd_bean.dart';
import '../../../../module/user/entity/query_ybd_user_resp_entity.dart';
import 'guardian_ybd_instruction_page.dart';
import 'query_ybd_room_list_resp_entity.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';
import '../../../widget/room_ybd_level_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

class YBDGuardianPage extends StatefulWidget {
  @override
  _YBDGuardianPageState createState() => new _YBDGuardianPageState();
}

class _YBDGuardianPageState extends BaseState<YBDGuardianPage> with SingleTickerProviderStateMixin {
  TabController? _tabController;
  YBDQueryUserRespEntity? _queryUserRespEntity;
  YBDQueryRoomListRespEntity? _queryRoomRespEntity;

  item(avatar, sex, vip, userId, name, level, talentLevel, {bool isRemove: false}) {
    return Container(
      padding: EdgeInsets.only(
          top: ScreenUtil().setWidth(
        10,
      )),
      height: ScreenUtil().setWidth(
        166,
      ),
      child: Row(
        children: [
          SizedBox(
            width: ScreenUtil().setWidth(
              25,
            ),
          ),
          YBDRoundAvatar(
            avatar,
            sex: sex,
            lableSrc: (vip ?? 0) == 0 ? "" : "assets/images/vip_lv${vip}.png",
            userId: userId,
            scene: "B",
            labelWitdh: 22,
            needNavigation: true,
            labelPadding: EdgeInsets.all(0),
            avatarWidth: 90,
          ),
          SizedBox(
            width: ScreenUtil().setWidth(
              30,
            ),
          ),
          Container(
            width: ScreenUtil().setWidth(
              360,
            ),
            height: ScreenUtil().setWidth(
              86,
            ),
            child: Stack(
              children: [
                Align(
                  child: Text(
                    name ?? '',
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(
                        28,
                      ),
                      color: Theme.of(context).primaryColor,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                  ),
                  alignment: Alignment.topLeft,
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    children: <Widget>[
                      Image.asset(
                        sex == 2 ? "assets/images/tag_male.webp" : "assets/images/tag_female.webp",
                        fit: BoxFit.fill,
                        width: ScreenUtil().setWidth(
                          60,
                        ),
                        height: ScreenUtil().setWidth(
                          24,
                        ),
                      ),
                      level != null
                          ? YBDLevelTag(
                              level,
                            )
                          : Container(),
                      talentLevel != null
                          ? YBDRoomLevelTag(
                              talentLevel,
                            )
                          : Container(),
                    ],
                  ),
                )

                //0未知，1女2男
              ],
            ),
          ),
          Expanded(child: Container()),
          isRemove
              ? GestureDetector(
                  onTap: () {
                    removeGuardian(userId);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: ScreenUtil().setWidth(56),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(60))),
                        color: Color(0xff5694FA)),
                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                    child: Text(
                      translate('remove'),
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              : Container(),
          SizedBox(
            width: ScreenUtil().setWidth(
              25,
            ),
          ),
        ],
      ),
    );
  }

  emptyView() {
    return Center(
      child: Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/empty/no_item.png',
            width: ScreenUtil().setWidth(360),
            height: ScreenUtil().setWidth(232),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(10),
          ),
          Text(
            translate(translate('no_data')),
            textAlign: TextAlign.start,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(24),
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      )),
    );
  }

  getMyGuardians() {
    if (_queryUserRespEntity == null) {
      return YBDLoadingCircle();
    }
    if (_queryUserRespEntity!.record!.length == 0) {
      return emptyView();
    }
    return ListView.builder(
        itemCount: _queryUserRespEntity!.record!.length,
        itemBuilder: (_, index) => item(
            _queryUserRespEntity!.record![index]!.headimg,
            _queryUserRespEntity!.record![index]!.sex,
            _queryUserRespEntity!.record![index]!.vip,
            _queryUserRespEntity!.record![index]!.id,
            _queryUserRespEntity!.record![index]!.nickname,
            _queryUserRespEntity!.record![index]!.level,
            _queryUserRespEntity!.record![index]!.roomlevel,
            isRemove: true));
  }

  getMyManageRoom() {
    if (_queryRoomRespEntity == null) {
      return YBDLoadingCircle();
    }
    if (_queryRoomRespEntity!.record == null || _queryRoomRespEntity!.record!.length == 0) {
      return emptyView();
    }
    return ListView.builder(
        itemCount: _queryRoomRespEntity!.record!.length,
        itemBuilder: (_, index) => item(
            _queryRoomRespEntity!.record![index]!.headimg,
            _queryRoomRespEntity!.record![index]!.sex,
            _queryRoomRespEntity!.record![index]!.vip,
            _queryRoomRespEntity!.record![index]!.id,
            _queryRoomRespEntity!.record![index]!.nickname,
            _queryRoomRespEntity!.record![index]!.level,
            _queryRoomRespEntity!.record![index]!.roomlevel));
  }

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
              'Guardian',
              style: TextStyle(
                color: Colors.white,
                fontSize: YBDTPStyle.spNav,
              ),
            ),
            centerTitle: true,
            elevation: 0,
            actions: <Widget>[
              IconButton(
                icon:
                    Image.asset('assets/images/question_mark.png', fit: BoxFit.cover, width: ScreenUtil().setWidth(48)),
                tooltip: 'Question',
                onPressed: () {
                  Navigator.push(context, CupertinoPageRoute(builder: (context) {
                    return YBDGuardianInstructionPage();
                  }));
                },
              )
            ],
            backgroundColor: Color(0xffA33CCA),
            leading: YBDNavBackButton(color: Colors.white)),
        body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xffA33CCA), Color(0xff32249D)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter)),
          child: Column(
            children: [
              Container(
                height: ScreenUtil().setWidth(96),
                padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(40)),
                child: TabBar(
                  indicator: UnderlineTabIndicator(
                      borderSide: BorderSide(width: ScreenUtil().setWidth(4), color: Color(0xff47CCCB)),
                      insets: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(86))),
                  labelColor: Colors.white,
                  unselectedLabelColor: Color(0x7fffffff),
                  controller: _tabController,
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.w400, height: 1, fontSize: ScreenUtil().setSp(32), color: Colors.white),
                  unselectedLabelStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      height: 1,
                      fontSize: ScreenUtil().setSp(28),
                      color: Color(0x7fffffff)),
                  tabs: [Tab(text: 'I am Guardian of'), Tab(text: 'My Guardians')],
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(40),
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [getMyManageRoom(), getMyGuardians()],
                ),
              ),
            ],
          ),
        ));
  }
  void myBuildexBa8oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getMyGuardianData() async {
    YBDQueryUserRespEntity? queryUserRespEntity = await ApiHelper.queryMyGuardians(context);
    if (queryUserRespEntity != null && queryUserRespEntity.returnCode == Const.HTTP_SUCCESS) {
      _queryUserRespEntity = queryUserRespEntity;
      setState(() {});
    }
  }

  getMyManageRoomData() async {
    YBDQueryRoomListRespEntity? queryRoomRespEntity = await ApiHelper.queryGuardedRooms(context);
    if (queryRoomRespEntity != null && queryRoomRespEntity.returnCode == Const.HTTP_SUCCESS) {
      _queryRoomRespEntity = queryRoomRespEntity;
      setState(() {});
    }
  }

  removeGuardian(id) async {
    showLockDialog();
    YBDResultBeanEntity? resultBeanEntity = await ApiHelper.deleteMyGuardian(context, id);
    dismissLockDialog();
    if (resultBeanEntity != null && resultBeanEntity.returnCode == Const.HTTP_SUCCESS) {
      getMyGuardianData();
    } else {
      YBDToastUtil.toast(resultBeanEntity!.returnMsg);
    }
  }

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 2, vsync: this);
    getMyGuardianData();
    getMyManageRoomData();
  }
  void initState7JSaEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
