import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:sprintf/sprintf.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../../base/base_ybd_state.dart';
import '../../room/util/mini_ybd_profile_util.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';

class YBDGuardianInstructionPage extends StatefulWidget {
  @override
  _YBDGuardianInstructionPageState createState() => new _YBDGuardianInstructionPageState();
}

class _YBDGuardianInstructionPageState extends BaseState<YBDGuardianInstructionPage> {
  @override
  Widget myBuild(BuildContext context) {
    int _count = YBDMiniProfileUtil.getMaxGuardianCount(context);
    return Scaffold(
      appBar: AppBar(
          title: Text(
            translate('guardian_privilege'),
            style: TextStyle(
              color: Colors.white,
              fontSize: YBDTPStyle.spNav,
            ),
          ),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Color(0xffA33CCA),
          leading: YBDNavBackButton(color: Colors.white)),
      body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xffA33CCA), Color(0xff32249D)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter)),
          child: Column(
            children: [
              SizedBox(height: ScreenUtil().setWidth(65)),
              Container(
                width: ScreenUtil().setWidth(670),
                height: ScreenUtil().setWidth(846),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
                  color: Colors.white10,
                ),
                child: Column(children: <Widget>[
                  SizedBox(height: ScreenUtil().setWidth(70)),
                  Center(child: Text(sprintf(translate('max_guardians'), [15]))),
                  SizedBox(height: ScreenUtil().setWidth(50)),
                  _itemContent(translate('lock_unlock_mic'), translate('lock_unlock_mic_tip'),
                      'assets/images/icon_lock_mic.png'),
                  _greyLine(),
                  _itemContent(translate('mute_unmute_mic'), translate('mute_unmute_mic_tip'),
                      'assets/images/icon_mute_mic.png'),
                  _greyLine(),
                  _itemContent(translate('invite_mic'), translate('invite_mic_tip'), 'assets/images/liveroom/ico_mic_take.png'),
                  _greyLine(),
                  _itemContent(translate('mute_user'), translate('mute_user_tip'), 'assets/images/icon_mute_user.png'),
                ]),
              ),
            ],
          )),
    );
  }
  void myBuildkJ5uYoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _greyLine() {
    return Container(
        height: ScreenUtil().setWidth(1),
        width: ScreenUtil().setWidth(620),
        color: Color(0xffEAEAEA).withOpacity(0.14));
  }
  void _greyLineT7lzMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _itemContent(String title, String description, String icon) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(30), horizontal: ScreenUtil().setWidth(40)),
      child: Row(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(75),
            height: ScreenUtil().setWidth(75),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(50))),
              color: Colors.white,
            ),
            child:
                Center(child: Image.asset(icon, width: ScreenUtil().setWidth(40), height: ScreenUtil().setWidth(40))),
          ),
          SizedBox(width: ScreenUtil().setWidth(38)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(title, style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white)),
              SizedBox(height: ScreenUtil().setWidth(16)),
              ConstrainedBox(
                constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(462)),
                child: Text(description,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        color: Colors.white.withOpacity(0.7),
                        fontWeight: FontWeight.w300)),
              ),
            ],
          )
        ],
      ),
    );
  }
  void _itemContentumDKxoyelive(String title, String description, String icon) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
