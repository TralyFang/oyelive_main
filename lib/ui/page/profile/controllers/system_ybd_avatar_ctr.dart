import 'dart:async';

import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/base_ybd_resp_entity.dart';

class YBDSystemAvatarController extends GetxController {
  // 测试代码
  bool _mock = false;
  String? _mockConfig;
  String? _mockAvatarPath;
  YBDApiHelperX? _mockApiHelper;
  YBDConfigUtilX? _mockConfigUtil;

  /// 测试代码
  void setMock(
    String systemConfig,
    String avatarPath,
    YBDApiHelperX apiHelper,
    YBDConfigUtilX configUtil,
  ) {
    _mock = true;
    _mockConfig = systemConfig;
    _mockAvatarPath = avatarPath;
    _mockApiHelper = apiHelper;
    _mockConfigUtil = configUtil;
  }

  /// 从头像配置列表中取单个头像的配置数据
  /// 第一个配置元素为头像名称，第二个为头像链接
  /// 配置举例："Female Avatar|https://d2zc7tapf7a4we.cloudfront.net/icon/avatar/female_avatar.webp"
  final int kAvatarNameIndex = 0;
  final int kAvatarUrlIndex = 1;

  /// 最多显示3个默认头像
  final int kMaxAvatarCount = 3;

  /// 修改昵称和头像剩余次数的提示语
  String remainingNotice = '';

  /// 系统头像列表
  List<String> avatarConfigs = <String>[];

  /// 加载默认头像配置
  void initConfigs() {
    avatarConfigs = _systemConfig()!.split(',');

    // 限制默认头像个数
    if (avatarConfigs.length > kMaxAvatarCount) {
      avatarConfigs.removeRange(kMaxAvatarCount, avatarConfigs.length);
    }
  }
  void initConfigsXZ1UJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 读配置项
  String? _systemConfig() {
    if (_mock) return _mockConfig;
    return YBDCommonUtil.storeFromContext()!.state.configs?.systemAvatar ?? '';
  }

  /// 系统头像库地址
  String? _systemAvatarPath() {
    if (_mock) return _mockAvatarPath;
    return YBDImageUtil.getAvatarPath(Get.context);
  }

  YBDApiHelperX? get apiHelper {
    if (_mock) return _mockApiHelper;
    return ApiHelper;
  }

  YBDConfigUtilX? get configUtil {
    if (_mock) return _mockConfigUtil;
    return ConfigUtil;
  }

  /// 上传头像的等级配置项
  Future<String> uLevelPrivilegeAvatar() async {
    return (await configUtil!.ulevelPrivilegeAvatar(Get.context)) ?? '${Const.DEFAULT_U_LEVEL_PRIVILEGE}';
  }
  void uLevelPrivilegeAvatarasD3Poyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 头像全路径
  String avatarFullPath(String imgUrl) {
    if (imgUrl.startsWith('http')) return imgUrl;

    return _systemAvatarPath()! + imgUrl;
  }
  void avatarFullPathmpuNtoyelive(String imgUrl) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 单个avatar的配置
  List<String> avatarConfigWithIndex(int index) {
    List<String> avatarConfig = avatarConfigs[index].split('|');
    return avatarConfig;
  }

  /// 从配置取头像链接
  String avatarUrlWithIndex(int index) {
    List<String> config = avatarConfigWithIndex(index);
    return avatarFullPath(config[kAvatarUrlIndex]);
  }
  void avatarUrlWithIndex83Tcloyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 从配置取头像名称
  String avatarNameWithIndex(int index) {
    List<String> config = avatarConfigWithIndex(index);
    return config[kAvatarNameIndex];
  }

  /// 修改用户默认头像
  Future<void> updateAvatar(int index) async {
    bool success = await apiHelper!.editUserInfo(
      {'headimg': avatarUrlWithIndex(index)},
      showLoading: true,
    );

    if (success) {
      await apiHelper!.checkLogin(Get.context);
    }

    // 刷新修改昵称和头像剩余次数的提示语
    await queryRemainingTimes();
  }
  void updateAvatar93jSQoyelive(int index)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 调接口查询修改昵称和头像剩余次数
  /// [milliseconds] 延迟请求接口
  Future<void> queryRemainingTimes() async {
    await Future<void>.delayed(Duration(milliseconds: 1000));
    YBDBaseRespEntity? respEntity = await apiHelper!.queryRemainingTimes(Get.context);

    if (respEntity?.returnMsg != null) {
      Get.find<YBDTextCtrl>().setNameWarn(respEntity!.returnMsg!);
    }
  }
}
