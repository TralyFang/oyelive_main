import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/user/entity/account_ybd_delete_resp_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/login/handler/login_ybd_handler.dart';
import 'package:oyelive_main/ui/page/profile/setting/setting_ybd_item.dart';
import 'package:oyelive_main/ui/widget/dialog/confirm_ybd_dialog.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/my_ybd_app_bar.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';

class YBDAccountPage extends StatefulWidget {
  @override
  YBDAccountPageState createState() => new YBDAccountPageState();
}

class YBDAccountPageState extends BaseState<YBDAccountPage> {
  @override
  void initState() {
    super.initState();
    // The page does three-party data statistics.
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent("account_page"));
  }

  void initState2u7O8oyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void dispose() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    super.dispose();
  }

  void disposeYUl2Loyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  Widget myBuild(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return Scaffold(
      appBar: YBDMyAppBar(
        backgroundColor: Colors.white,
        leading: YBDNavBackButton(color: Colors.black),
        elevation: 0.0,
        title: Text(
          translate('account'),
          style: TextStyle(
            color: Colors.black,
            fontSize: YBDTPStyle.spNav,
          ),
        ),
      ),
      body: StoreBuilder<YBDAppState>(builder: (context, store) {
        // 获取绑定的手机号
        var cellPhone = store.state.bean!.snsLinkInfo(LoginType.phone)?.snsId;

        return Container(
          color: Colors.white,
          child: SingleChildScrollView(
            child: Column(
              children: [
                // YBDSettingItem.boldTitleWidget(translate('link_account')),
                GestureDetector(
                  onDoubleTap: () {
                    /// TODO: 测试begin======
                    if (Const.TEST_ENV) {
                      YBDTPRoomGetLogic.openQuickEnter =
                          !YBDTPRoomGetLogic.openQuickEnter;
                      YBDToastUtil.toast(
                          'TPOpenQuickEnter: ${YBDTPRoomGetLogic.openQuickEnter}');
                    }

                    /// TODO: 测试end======
                  },
                  child:
                      YBDSettingItem.boldTitleWidget(translate('link_account')),
                ),
                SizedBox(height: ScreenUtil().setWidth(10)),
                YBDSettingItem.settingItemContent(translate('phone'), '',
                    onTap: () {
                  if (cellPhone?.isEmpty ?? true) {
                    YBDSettingItem.showPhoneEditDialog(this.context);
                  }
                },
                    leading: 'assets/images/setting/settings_phone@2x.png',
                    tail: YBDSettingItem.cellphoneStar(cellPhone ?? ''),
                    extraPadding: 0,
                    hasMore: cellPhone?.isEmpty ?? true),
                YBDSettingItem.accountSafeTips(cellPhone?.isNotEmpty ?? false),
                // _thirdAuthWidget(LoginType.facebook, translate('facebook'), store.state.bean),
                // _thirdAuthWidget(LoginType.twitter, translate('twitter'), store.state.bean),
                // _thirdAuthWidget(LoginType.google, translate('google'), store.state.bean),
                // _thirdAuthWidget(LoginType.apple, translate('apple'), store.state.bean),
                YBDSettingItem.greyLine,
                GestureDetector(
                  onDoubleTap: () {
                    /// TODO: 测试begin======
                    if (Const.TEST_ENV && YBDTPRoomGetLogic.openQuickEnter) {
                      YBDTPRoomGetLogic.openTestData =
                          !YBDTPRoomGetLogic.openTestData;
                      YBDToastUtil.toast(
                          'TPOpenTestData: ${YBDTPRoomGetLogic.openTestData}');
                    }

                    /// TODO: 测试end======
                  },
                  child: YBDSettingItem.boldTitleWidget(translate('security')),
                ),
                YBDSettingItem.settingItemContent(translate('change_pwd'), '',
                    onTap: () {
                  YBDNavigatorHelper.navigateTo(
                    context,
                    YBDNavigatorHelper.fix_password +
                        "/false" +
                        "/${YBDLocationName.SETTING_PAGE}",
                  );
                }, leading: 'assets/images/purple_lock.webp', extraPadding: 10),
                _deleteAccountWidget(),
                YBDSettingItem.greyLine,
              ],
            ),
          ),
        );
      }),
    );
  }

  void myBuildiTfhYoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 安卓平台隐藏苹果登录界面
  bool _hideAppleItem() {
    if (Platform.isIOS) {
      return false;
    } else {
      return true;
    }
  }

  Widget _thirdAuthWidget(LoginType type, String title, YBDUserInfo? user) {
    if (type == LoginType.apple && _hideAppleItem()) {
      return SizedBox();
    }

    return Column(children: [
      YBDSettingItem.paddingLine,
      YBDSettingItem.settingItemContent(title, '', onTap: () {
        if (user!.snsLinkInfo(type) != null) {
          // Already bound, go to unbind the interface
          YBDNavigatorHelper.navigateTo(
              context,
              YBDNavigatorHelper.account_unlink +
                  '?title=$title&type=${YBDSettingItem.loginTypeToInt(type)}');
          return;
        }

        /// 测试代码twitter---start
        // var token = "{secret: SeVMjyJ8Pl4KoCx1eWF3IGMWy0x4tJBVHqqBFRZlnCmI9, token: 4896941772-MZEpXix6X6OMDW9JIMBG8y1oTYiGQHxr7XFWVIf}";

        // google token : Bearer ya29.a0ARrdaM8fGcd94UOMkbUeIeYTSKvlWf7wvJnH8VcEadUboRYvmVpWr5gOj8py7ctISTgge7YxHQyGweRPBX9L9wYDR5i4SpKbeilm2ugMwN_S-yJfCRlyUyMP5SIwnzoOIdEZpfrV_BHpu9Lg-Hndbw6d8bDd
        // var token = "ya29.a0ARrdaM8fGcd94UOMkbUeIeYTSKvlWf7wvJnH8VcEadUboRYvmVpWr5gOj8py7ctISTgge7YxHQyGweRPBX9L9wYDR5i4SpKbeilm2ugMwN_S-yJfCRlyUyMP5SIwnzoOIdEZpfrV_BHpu9Lg-Hndbw6d8bDd";

        //fb login sns type : 1 token :
        // var token = "EAAdTcBZCs5fsBAJ7LRq97g8B1sq5qP9ZBeHD4rxutQBOzZCnzdPis8lXokeJha0uZC9fz8ZBDMcxspMfvlu2MINWhcbqoJPOINUIQt4GH709UcyLUvmp5PnabkPdKzhZAwLG6I7u9Qij3KWZAHPHobZAQ12gyZADSHW1kXY6l8oFgIhe01kZCVuUe4Pi536NdIwROaun466h5OVwq6kwRltiLt0XRRsvaD472t8YtPEZCIotQZDZD"

        // YBDLoginHandler.snsLink(context, token, '${YBDSettingItem.loginTypeToInt(type)}');
        /// 测试代码---end

        YBDLoginHandler(type, context, this, 'account_page')
            .authTokenCallback((token) {
          logger.v("$title auth token : $token");
          YBDLoginHandler.snsLink(
            context,
            token,
            '${YBDSettingItem.loginTypeToInt(type)}',
            showLoading: true,
          );
        }).startLogin();
      },
          leading: YBDSettingItem.loginTypeToImageAssets(type),
          tail: user!.snsLinkInfo(type)?.nickname ?? '',
          extraPadding: 10),
    ]);
  }

  void _thirdAuthWidgetVNkwboyelive(
      LoginType type, String title, YBDUserInfo? user) {
    int needCount = 0;
    print('input result:$needCount');
  }

  Widget _deleteAccountWidget() {
    // 由后台控制是否开启这个接口
    if (YBDCommonUtil.getRoomOperateInfo().isOpenAccountDelete == 0) {
      return Container();
    }
    return Column(children: [
      YBDSettingItem.greyLine,
      YBDSettingItem.settingItemContent(translate('Delete Account'), '',
          onTap: () {
        _deleteAccountDialog();
      },
          leading: 'assets/images/setting/settings_delete@2x.webp',
          extraPadding: 10),
    ]);
  }

  void _deleteAccountWidgetKjsXWoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  // delete account dialog
  _deleteAccountDialog() {
    YBDCommonTrack().commonTrack(
        YBDTAProps(location: 'delete_account', module: YBDTAModule.settings));
    YBDConfirmDialog.show(this.context,
        title: translate('account_delete'),
        sureTitle: translate('continue'),
        barrierDismissible: true,
        content: translate('account_delete_tips_one'),
        onSureCallback: () async {
      // The server needs to be informed
      _accountDelete(this.context);
      // 审核期间就toast提示：you have applied,please waiting some hours or contact Customer Service
      // 如果不是：则dialog说明
    });
  }

  _accountDelete(BuildContext context) async {
    showLockDialog();
    YBDAccountDeleteRespEntity? resp = await ApiHelper.accountDelete(
      context,
    );
    dismissLockDialog();
    logger.v("accountDelete response : ${resp?.toJson()}");
    if (resp != null && resp.returnCode == Const.HTTP_SUCCESS) {
      logger.i("accountDelete success");
      // pending/approved/rejected
      if (resp.record?.status == null) {
        // 首次申请弹窗
        _deleteAccountDialogCheck();
      } else if (resp.record?.message != null) {
        // pending: you have applied,please waiting some hours or contact Customer Service
        YBDToastUtil.toast(resp.record?.message);
      }
    } else {
      logger.e("accountDelete failed");
      YBDToastUtil.toast(resp?.returnMsg ?? translate('unknown_error'));
    }
  }

  // Server validation status
  _deleteAccountDialogCheck() {
    YBDConfirmDialog.show(this.context,
        title: translate('waiting'),
        cancelTitle: '',
        sureTitle: translate('ok'),
        barrierDismissible: true,
        content: translate('account_delete_tips_two'),
        onSureCallback: () async {});
  }
}
