import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../widget/my_ybd_app_bar.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';

class YBDLiteVersionPage extends StatefulWidget {
  @override
  _YBDLiteVersionPageState createState() => new _YBDLiteVersionPageState();
}

class _YBDLiteVersionPageState extends BaseState<YBDLiteVersionPage> {
  bool liteVersion = false;

  @override
  void initState() {
    super.initState();

    YBDSPUtil.getBool(Const.SP_LITE_VERSION).then((value) {
      setState(() {
        liteVersion = value == true;
      });
    });
  }
  void initStateDE2jroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      appBar: YBDMyAppBar(
        backgroundColor: Colors.white,
        leading: YBDNavBackButton(color: Colors.black),
        elevation: 0.0,
        title: Text(translate('lite_version'), style: TextStyle(color: Colors.black)),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Row(
              children: [
                SizedBox(width: ScreenUtil().setWidth(50)),
                Text('Enable/Disable Lite Version',
                    style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Color(0xff3A3A3A), height: 1.2)),
                Expanded(child: Container()),
                Switch(
                    value: liteVersion,
                    activeColor: Color(0xff8D0EB1),
                    onChanged: (bool value) {
                      YBDCommonTrack().commonTrack(YBDTAProps(
                        location: value ? 'lite_open' : 'lite_close',
                        module: YBDTAModule.settings,
                      ));
                      YBDSPUtil.save(Const.SP_LITE_VERSION, value);
                      setState(() {
                        liteVersion = value == true;
                      });
                    }),
                SizedBox(width: ScreenUtil().setWidth(16)),
              ],
            ),
            Container(height: ScreenUtil().setWidth(16), color: Color(0xffF0F0F0)),
            Padding(
              padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(36), horizontal: ScreenUtil().setWidth(40)),
              child: Text(
                  'You can turn on Lite Version if your mobile is getting slow due to low memory and storage. '
                  'Here is the list of all the animations that will not be supported in the Light Version.',
                  style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff3A3A3A))),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(40)),
              child: Text(
                  '1. Bullet YBDMessage Animation \n\n'
                  '2. Animated Emojis \n\n'
                  '3. VIP Animation \n\n'
                  '4. Gifts Animation \n\n'
                  '5. Entrance Effects \n\n'
                  '6. YBDGlobal YBDMessage Bullet \n\n'
                  '7. Mic decoration props \n\n'
                  '8. Backgrounds/themes \n\n'
                  '9. Mini Profile Animation',
                  style: TextStyle(fontSize: ScreenUtil().setSp(26), color: Colors.black)),
            )
          ],
        ),
      ),
    );
  }
  void myBuildC4oswoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
