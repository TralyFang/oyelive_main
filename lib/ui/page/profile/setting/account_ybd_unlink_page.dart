import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/ui/page/login/handler/login_ybd_handler.dart';
import 'package:oyelive_main/ui/page/profile/setting/setting_ybd_item.dart';
import 'package:oyelive_main/ui/page/profile/setting/setting_ybd_page.dart';
import 'package:oyelive_main/ui/page/room/widget/leave_ybd_confirm_dialog.dart';
import 'package:oyelive_main/ui/widget/dialog/confirm_ybd_dialog.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/my_ybd_app_bar.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';

class YBDAccountUnlinkPage extends StatefulWidget {
  String title;
  String loginType;

  /// 跟[LoginType]是对应关系的

  YBDAccountUnlinkPage(this.title, this.loginType);

  @override
  YBDAccountUnlinkPageState createState() => new YBDAccountUnlinkPageState();
}

class YBDAccountUnlinkPageState extends BaseState<YBDAccountUnlinkPage> {
  @override
  void initState() {
    super.initState();
  }
  void initStatelpwIWoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    super.dispose();
  }
  void disposeKc1pCoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    // 获取对应的类型
    var type = YBDSettingItem.loginTypeWithInt(int.tryParse(widget.loginType) ?? 1);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: YBDMyAppBar(
        backgroundColor: Colors.white,
        leading: YBDNavBackButton(color: Colors.black),
        elevation: 0.0,
        title: Text(widget.title, style: TextStyle(color: Colors.black)),
      ),
      body: StoreBuilder<YBDAppState>(builder: (context, store) {
        return Container(
          color: Colors.white,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Center(
                    child: Column(children: [
                  SizedBox(height: ScreenUtil().setWidth(63)),
                  Image.asset(
                    YBDSettingItem.loginTypeToImageAssets(type, big: true),
                    width: ScreenUtil().setWidth(160),
                    height: ScreenUtil().setWidth(160),
                  ),
                  SizedBox(height: ScreenUtil().setWidth(20)),
                  Text("${store.state.bean!.snsLinkInfo(type)?.nickname ?? ""}",
                      style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Color(0xff000000), height: 1.2)),
                  SizedBox(height: ScreenUtil().setWidth(35))
                ])),
                _thirdAuthWidget(type, translate('unlink')),
              ],
            ),
          ),
        );
      }),
    );
  }
  void myBuildCs2tLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 显示手机输入弹框
  bool _showPhoneDialog() {
    var userInfo = YBDCommonUtil.storeFromContext()!.state.bean!;

    // 没有绑定手机时显示输入弹框
    return !userInfo.isBindPhone();
  }

  Widget _thirdAuthWidget(LoginType type, String title) {
    return Column(children: [
      YBDSettingItem.settingItemContent(title, '', onTap: () async {
        // 点击解绑绑定, 先判断是否绑定手机号
        if (_showPhoneDialog()) {
          YBDConfirmDialog.show(this.context, title: translate('sns_unlink_third_phone_tips'), sureTitle: translate('ok'),
              onSureCallback: () {
            YBDSettingItem.showPhoneEditDialog(this.context);
          });
          return;
        }

        YBDConfirmDialog.show(this.context,
            title: translate('sns_unlink_third_tips', args: {'title': this.widget.title}),
            sureTitle: translate('sure'), onSureCallback: () async {
          showLockDialog();
          await YBDLoginHandler.snsUnlink(context, '${YBDSettingItem.loginTypeToInt(type)}');
          dismissLockDialog();
        });
      }, leading: 'assets/images/setting/settings_unlink@2x.png', extraPadding: 10),
      YBDSettingItem.paddingLine
    ]);
  }
  void _thirdAuthWidgetCEl7zoyelive(LoginType type, String title) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
