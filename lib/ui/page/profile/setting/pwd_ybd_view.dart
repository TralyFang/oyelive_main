import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/input_ybd_restrict.dart';

typedef CheckPwd(bool legal);

class YBDPwdView extends StatefulWidget {
  final String hint;
  final TextEditingController controller;
  final CheckPwd? checkPwd;

  const YBDPwdView({
    required this.controller,
    this.hint = '',
    this.checkPwd,
    Key? key,
  }) : super(key: key);

  @override
  _YBDPwdViewState createState() => _YBDPwdViewState();
}

class _YBDPwdViewState extends State<YBDPwdView> {
  bool isObscure = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(80),
      width: ScreenUtil().setWidth(580),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(
            ScreenUtil().setWidth(30),
          ),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: TextField(
                style: TextStyle(
                  color: Colors.black,
                  fontSize: ScreenUtil().setSp(28),
                ),
                inputFormatters: PWD_RESTRICT,
                obscureText: isObscure,
                decoration: InputDecoration(
                    hintStyle: TextStyle(
                      color: Colors.black.withOpacity(0.5),
                    ),
                    hintText: widget.hint,
                    border: InputBorder.none),
                controller: widget.controller,
                onChanged: (value) {
                  if (widget.checkPwd != null) {
                    widget.checkPwd!(YBDCommonUtil.checkPwdLegal(value));
                  }
                }),
          ),
          Container(
            width: ScreenUtil().setWidth(80),
            child: TextButton(
              style: TextButton.styleFrom(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(20),
                ),
              ),
              onPressed: () {
                setState(() {
                  isObscure = !isObscure;
                });
              },
              child: Image.asset(
                isObscure ? "assets/images/show_pass.png" : "assets/images/hide_pass.png",
                color: Color(0xff505050),
                width: ScreenUtil().setWidth(46),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void buildqNqd1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
