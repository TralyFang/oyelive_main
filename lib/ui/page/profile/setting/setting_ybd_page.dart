import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/ui/page/profile/setting/setting_ybd_item.dart';
import 'package:package_info/package_info.dart';
import 'package:redux/redux.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/config_ybd_util.dart';
import '../../../../common/util/file_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/notification_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/version_update/check_ybd_update_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/query_ybd_configs_resp_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../../redux/followed_ybd_ids_redux.dart';
import '../../../../redux/user_ybd_redux.dart';
import '../../../widget/my_ybd_app_bar.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';
import '../../home/widget/language_ybd_list_dialog.dart';
import '../../splash/splash_ybd_util.dart';

class YBDSettingPage extends StatefulWidget {
  @override
  YBDSettingPageState createState() => new YBDSettingPageState();
}

class YBDSettingPageState extends BaseState<YBDSettingPage> {
  String cacheSize = ' MB';
  int cacheFileSize = 0;

  bool liteVersion = false;
  bool appNotification = false;
  bool vipTalents = false;
  bool subscriptions = false;

  String versionName = "";
  String versionCode = "";

  @override
  void initState() {
    super.initState();

    getCacheSize();

    YBDSPUtil.getBool(Const.SP_LITE_VERSION).then((value) {
      setState(() {
        liteVersion = value == true;
      });
    });

    YBDSPUtil.getBool(Const.SP_NOTIFICATION_SYSTEM).then((value) {
      setState(() {
        appNotification = value != false;
      });
    });
    YBDSPUtil.getBool(Const.SP_NOTIFICATION_VIP_TALENT).then((value) {
      setState(() {
        vipTalents = value != false;
      });
    });
    YBDSPUtil.getBool(Const.SP_NOTIFICATION_FOLLOWED_TALENT).then((value) {
      setState(() {
        subscriptions = value != false;
      });
    });

    PackageInfo.fromPlatform().then((value) {
      setState(() {
        versionName = value.version;
        versionCode = value.buildNumber;
      });
    });
  }

  void initStateqRmFuoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void dispose() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    super.dispose();
  }

  void disposeX7ONeoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  Widget myBuild(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return Scaffold(
      appBar: YBDMyAppBar(
        backgroundColor: Colors.white,
        leading: YBDNavBackButton(color: Colors.black),
        elevation: 0.0,
        title: Text(
          translate('settings'),
          style: TextStyle(
            color: Colors.black,
            fontSize: YBDTPStyle.spNav,
          ),
        ),
      ),
      body: Container(
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              _itemContent(translate('rate_us'), translate('rate_us_desc'),
                  onTap: () {
                if (Platform.isIOS) {
                  launch(Const.APPLE_STORE);
                } else {
                  launch(Const.GOOGLE_STORE);
                }
              }, leading: 'assets/images/rate.png'),
              paddingLine,
              _itemContent(translate('like_fb'), translate('like_fb_desc'),
                  onTap: () async {
                try {
                  bool launched = await launch(Const.FACEBOOK_APP_IOS,
                      forceSafariVC: false);

                  if (!launched) {
                    await launch(Const.FACEBOOK_WEB, forceSafariVC: false);
                  }
                } catch (e) {
                  await launch(Const.FACEBOOK_WEB, forceSafariVC: false);
                }
              }, leading: 'assets/images/like_fb.png'),
              paddingLine,
              _itemContent(
                  translate('lite_version'), translate('lite_version_tip'),
                  onTap: () {
                    //  go to lite version instruction page
                    YBDNavigatorHelper.navigateTo(
                            context, YBDNavigatorHelper.lite_version)
                        .then((value) {
                      YBDSPUtil.getBool(Const.SP_LITE_VERSION).then((value) {
                        if (mounted)
                          setState(() {
                            liteVersion = value == true;
                          });
                      });
                    });
                    YBDCommonTrack().commonTrack(YBDTAProps(
                        location: 'lite_click', module: YBDTAModule.settings));
                  },
                  switchValue: liteVersion,
                  onSwitchTap: (value) {
                    YBDCommonTrack().commonTrack(YBDTAProps(
                      location: value ? 'lite_open' : 'lite_close',
                      module: YBDTAModule.settings,
                    ));
                    YBDSPUtil.save(Const.SP_LITE_VERSION, value);
                    setState(() {
                      liteVersion = value == true;
                    });
                  },
                  leading: 'assets/images/icon_lite.webp'),
              paddingLine,
              _itemContent(translate('language_text'), '', onTap: () {
                languageTap(context);
              }, leading: 'assets/images/langa@2x.webp', extraPadding: 10),
              greyLine,
              _itemContent(translate('account'), '', onTap: () {
                YBDNavigatorHelper.navigateTo(
                    context, YBDNavigatorHelper.account);
              },
                  leading: 'assets/images/setting/setting_account@2x.png',
                  extraPadding: 10),
              greyLine,
              _itemContent(translate('change_pwd'), '', onTap: () {
                YBDNavigatorHelper.navigateTo(
                  context,
                  YBDNavigatorHelper.fix_password +
                      "/false" +
                      "/${YBDLocationName.SETTING_PAGE}",
                );
              }, leading: 'assets/images/purple_lock.webp', extraPadding: 10),
              greyLine,
              SizedBox(height: ScreenUtil().setWidth(34)),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: ScreenUtil().setWidth(30)),
                  Text(translate('notifications'),
                      style: TextStyle(
                          color: Color(0xff8A0FE2),
                          fontSize: ScreenUtil().setSp(28),
                          fontWeight: FontWeight.bold)),
                ],
              ),
              _itemContent(translate('app_notifications'),
                  translate('app_notifications_tip'),
                  onTap: () {
                    bool _t =
                        appNotification == null ? false : !appNotification;
                    YBDSPUtil.save(Const.SP_NOTIFICATION_SYSTEM, _t);
                    setState(() {
                      appNotification = _t;
                    });

                    if (_t)
                      YBDNotificationUtil.subscribeGlobal(context);
                    else
                      YBDNotificationUtil.unsubscribeGlobal(context);
                  },
                  switchValue: appNotification,
                  onSwitchTap: (value) {
                    YBDSPUtil.save(Const.SP_NOTIFICATION_SYSTEM, value);
                    setState(() {
                      appNotification = value != false;
                    });

                    if (value)
                      YBDNotificationUtil.subscribeGlobal(context);
                    else
                      YBDNotificationUtil.unsubscribeGlobal(context);
                  }),
              paddingLine,
              _itemContent(
                  translate('vip_talents'), translate('vip_talents_tip'),
                  onTap: () {
                    bool _t = vipTalents == null ? false : !vipTalents;
                    YBDSPUtil.save(Const.SP_NOTIFICATION_VIP_TALENT, _t);
                    setState(() {
                      vipTalents = _t;
                    });

                    if (_t)
                      YBDNotificationUtil.subscribeVipTalent(context);
                    else
                      YBDNotificationUtil.unsubscribeVipTalent(context);
                  },
                  switchValue: vipTalents,
                  onSwitchTap: (value) {
                    YBDSPUtil.save(Const.SP_NOTIFICATION_VIP_TALENT, value);
                    setState(() {
                      vipTalents = value != false;
                    });

                    if (value)
                      YBDNotificationUtil.subscribeVipTalent(context);
                    else
                      YBDNotificationUtil.unsubscribeVipTalent(context);
                  }),
              paddingLine,
              _itemContent(
                  translate('subscriptions'), translate('subscriptions_tip'),
                  onTap: () {
                    bool _t = subscriptions == null ? false : !subscriptions;
                    YBDSPUtil.save(Const.SP_NOTIFICATION_FOLLOWED_TALENT, _t);
                    setState(() {
                      subscriptions = _t;
                    });

                    if (_t)
                      YBDNotificationUtil.subscribeFollowingTalent(context);
                    else
                      YBDNotificationUtil.unsubscribeFollowingTalent(context);
                  },
                  switchValue: subscriptions,
                  onSwitchTap: (value) {
                    YBDSPUtil.save(
                        Const.SP_NOTIFICATION_FOLLOWED_TALENT, value);
                    setState(() {
                      subscriptions = value != false;
                    });

                    if (value)
                      YBDNotificationUtil.subscribeFollowingTalent(context);
                    else
                      YBDNotificationUtil.unsubscribeFollowingTalent(context);
                  }),
              greyLine,
              SizedBox(height: ScreenUtil().setWidth(34)),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: ScreenUtil().setWidth(30)),
                  Text(translate('blacklist'),
                      style: TextStyle(
                          color: Color(0xff8A0FE2),
                          fontSize: ScreenUtil().setSp(28),
                          fontWeight: FontWeight.bold)),
                ],
              ),
              setGeneralLine(translate('room_blocklist'), onTap: () {
                logger.v('clicked room black list row');
                YBDNavigatorHelper.navigateTo(
                    context, YBDNavigatorHelper.room_blacklist);
              }),
              paddingLine,
              setGeneralLine(translate('inbox_blocklist'), onTap: () {
                YBDNavigatorHelper.navigateTo(
                    context, YBDNavigatorHelper.inbox_blacklist);
              }),
              greyLine,
              SizedBox(height: ScreenUtil().setWidth(34)),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: ScreenUtil().setWidth(30)),
                  Text(translate('general_settings'),
                      style: TextStyle(
                          color: Color(0xff8A0FE2),
                          fontSize: ScreenUtil().setSp(28),
                          fontWeight: FontWeight.bold)),
                ],
              ),
              setGeneralLine(translate('check_updates'), onTap: () {
                _checkUpdate();
              }, rightText: '$versionName ($versionCode)'),
              paddingLine,
              setGeneralLine(translate('clear_cache'),
                  onTap: clearCache, rightText: cacheSize),
              paddingLine,
              // if (Platform.isIOS) ...[
              //   paddingLine,
              //   setGeneralLine(translate('edit_profile'), onTap: () {
              //     YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.edit_profile);
              //   })
              // ],
              paddingLine,
              setGeneralLine(translate('about'), onTap: () {
                YBDNavigatorHelper.navigateTo(
                    context, YBDNavigatorHelper.about);
              }),
              // paddingLine,
              // setGeneralLine('Upload log', () {
              //   uploadLog();
              //   //  查看日志的页面现在不需要了，可以删除
              //   // YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.read_log);
              // }),
              paddingLine,
              setGeneralLine(translate('sign_out'), onTap: () {
                showConfirmDialog(context);
              }),
              SizedBox(height: ScreenUtil().setWidth(50))
            ],
          ),
        ),
      ),
    );
  }

  void myBuild3Wixaoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 上传日志栏
  // Widget _uploadLogItem() {
  // 不显示上传日志的 ui，改成在about页面连点5次弹分享日志的弹框
  // if (Const.SHOW_DEBUG) {
  //   return setGeneralLine(
  //     translate('upload_log'),
  //     onTap: () async {
  //       YBDDialogUtil.showLoading(context);
  //       final result = await YBDLogUtil.uploadLogToS3();
  //       YBDDialogUtil.hideLoading(context);
  //       // Clipboard.setData(ClipboardData(text: '$result'));
  //       // YBDToastUtil.toast(translate('copied_to_clipboard'));
  //     },
  //   );
  // } else {
  // return SizedBox();
  // }
  // }

  languageTap(BuildContext context) async {
    String locale =
        (await YBDSPUtil.get(Const.SP_LOCALE)) ?? Const.DEFAULT_LANG;

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return YBDLanguageListDialog(locale);
        });
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
        location: YBDLocationName.HOME_PAGE, itemName: 'language'));
  }

  _itemContent(String title, String desc,
      {String leading = '',
      Function? onTap,
      bool switchValue = false,
      Function? onSwitchTap,
      int extraPadding: 0}) {
    return YBDSettingItem.settingItemContent(title, desc,
        leading: leading,
        onTap: onTap,
        switchValue: switchValue,
        onSwitchTap: onSwitchTap,
        extraPadding: extraPadding);
  }

  Widget setGeneralLine(String title, {Function? onTap, String? rightText}) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onTap as void Function()?,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(28)),
          decoration: BoxDecoration(shape: BoxShape.rectangle),
          child: Row(
            children: [
              SizedBox(width: ScreenUtil().setWidth(50)),
              Text(title,
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xff717171),
                      height: 1.2)),
              Expanded(child: Container()),
              Text(rightText ?? "",
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xff717171),
                      height: 1.2)),
              SizedBox(width: ScreenUtil().setWidth(30)),
            ],
          ),
        ),
      ),
    );
  }

  void setGeneralLineKQtEioyelive(String title,
      {Function? onTap, String? rightText}) {
    int needCount = 0;
    print('input result:$needCount');
  }

  var paddingLine = Container(
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
      height: ScreenUtil().setWidth(1),
      color: Color(0xffF0F0F0));
  var greyLine =
      Container(height: ScreenUtil().setWidth(1), color: Color(0xffF0F0F0));

  getCacheSize() async {
    // 清除OyeTalk、com.oyetalk.tv 目录下文件 ?
    int fileSize = 0;

    // temp 目录
    String tempPath = await YBDCommonUtil.getResourceDir(Const.TEMP);
    Directory directory = Directory(tempPath);
    fileSize += await YBDFileUtil.getTotalSizeOfFilesInDir(directory);

    // cache 目录
    directory = Directory(await YBDFileUtil.getCacheManagerPath());
    fileSize += await YBDFileUtil.getTotalSizeOfFilesInDir(directory);

    /* // ios
    directory = Directory(await DefaultCacheManager().store.storeKey);
    if (directory.existsSync()) {
      fileStat = directory.statSync();
      fileSize += fileStat.size;
    }*/

    // cacheSize = '${(fileSize / 1024.0).toStringAsFixed(2)} MB';
    //
    // cacheFileSize = fileSize;
    cacheSize = '${(fileSize / 1024.0 / 1024.0).toStringAsFixed(2)} MB';
    logger.v("cacheSize : $cacheSize, cache path : $tempPath");
    setState(() {});
  }

  clearCache() async {
    try {
      await YBDFileUtil.clearCacheManagerPath();
      await YBDFileUtil.clearTempDir();

      /*
      YBDFileUtil.clearTempDir();
       */
      if (cacheFileSize > 0) {
        // 清理了缓存，上报
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.WEB_INTERCEPT_EVENT,
            location: YBDLocationName.SETTING_PAGE,
            returnExtra: '${cacheFileSize}bytes'));
      }

      logger.v('cleared cache');
    } catch (e) {
      logger.v(e);
    }

    getCacheSize();
  }

  /// 查询最新配置
  _refreshConfig() async {
    showLockDialog();
    YBDQueryConfigsRespEntity? _resp = await ApiHelper.queryConfigs(context);
    dismissLockDialog();

    if (_resp?.returnCode == Const.HTTP_SUCCESS && _resp?.record != null) {
      // 刷新 store
      final store = YBDCommonUtil.storeFromContext(context: context)!;

      // 从 response 获取配置项
      YBDConfigInfo configInfo =
          YBDSplashUtil.addUniqueIdsFromStore(_resp!, store)!;

      // 刷新 store 里的配置信息
      YBDSplashUtil.refreshStoreWithConfig(store, configInfo);

      // 把配置信息保存到本地 json 文件
      ConfigUtil.writeConfig(_resp);
    }
  }

  /// 检查更新
  _checkUpdate() async {
    await _refreshConfig();
    YBDCheckUpdateUtil.checkUpdate(context, showLatestVersionMessage: true);
  }

  showConfirmDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text(
        translate('cancel'),
        style: TextStyle(color: Color(0xff5E94E7)),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text(
        translate('ok'),
        style: TextStyle(color: Color(0xff5E94E7)),
      ),
      onPressed: () async {
        Navigator.pop(context);
        // 清空 redux/SP 用户信息
        Store<YBDAppState> store =
            YBDCommonUtil.storeFromContext(context: context)!;
        store.dispatch(YBDUpdateUserAction(YBDUserInfo()));
        store.dispatch(YBDUpdateFollowedIdsAction(Set()));
        await YBDSPUtil.remove(Const.SP_USER_INFO);
        await YBDSPUtil.remove(Const.SP_COOKIE_APP);
        await YBDSPUtil.remove(Const.SP_JSESSION_ID);
        // 清除账号 ID
        alog.v('22.7.26---user log out');
        TA.logout();
        // 跳转登录页
        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.login,
            clearStack: true, forceFluro: true);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text(translate('logout_confirm')),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
