import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/login/bloc/phonenumber_ybd_input_bloc.dart';
import 'package:oyelive_main/ui/page/login/handler/login_ybd_handler.dart';
import 'package:oyelive_main/ui/widget/edit_ybd_phone_dialog.dart';

class YBDSettingItem {
  // 粗体标题，自带上空间
  static boldTitleWidget(String title) {
    return Column(children: [
      SizedBox(height: ScreenUtil().setWidth(34)),
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(width: ScreenUtil().setWidth(30)),
          Text(title,
              style: TextStyle(
                  color: Color(0xff8A0FE2),
                  fontSize: ScreenUtil().setSp(28),
                  fontWeight: FontWeight.bold)),
        ],
      ),
    ]);
  }

  // 灰色的线
  static var greyLine =
      Container(height: ScreenUtil().setWidth(1), color: Color(0xffF0F0F0));
  // 缩进线
  static var paddingLine = Container(
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
      height: ScreenUtil().setWidth(1),
      color: Color(0xffF0F0F0));

  // item row
  static Widget settingItemContent(String title, String desc,
      {String leading = '',
      Function? onTap,
      bool switchValue = false,
      Function? onSwitchTap,
      String tail = '',
      int extraPadding: 0,
      bool hasMore = true}) {
    return Material(
      key: Key(title),
      color: Colors.transparent,
      child: InkWell(
        onTap: onTap as void Function()?,
        child: Container(
          padding: EdgeInsets.symmetric(
              vertical: ScreenUtil().setWidth(24 + extraPadding)),
          decoration: BoxDecoration(shape: BoxShape.rectangle),
          child: Row(
            children: [
              SizedBox(width: ScreenUtil().setWidth(30)),
              if (leading != '') ...[
                Image.asset(
                  leading,
                  width: ScreenUtil().setWidth(43),
                  height: ScreenUtil().setWidth(40),
                ),
              ],
              SizedBox(width: ScreenUtil().setWidth(20)),
              Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(title,
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(28),
                            color: Color(0xff3A3A3A),
                            height: 1.2)),
                    SizedBox(height: ScreenUtil().setWidth(5)),
                    Offstage(
                      offstage: desc.isEmpty,
                      child: ConstrainedBox(
                          constraints: BoxConstraints(
                              maxWidth: ScreenUtil().setWidth(490)),
                          child: Text(desc,
                              style: TextStyle(
                                  fontSize: ScreenUtil().setSp(24),
                                  color: Color(0xff909399),
                                  height: 1.2,
                                  fontWeight: FontWeight.w300))),
                    ),
                  ]),
              Expanded(child: Container()),
              tail.isNotEmpty
                  ? Text(tail,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(28),
                          color: Color(0xff868686),
                          height: 1.2))
                  : Container(),
              onSwitchTap != null
                  ? Switch(
                      value: switchValue,
                      activeColor: Color(0xff8D0EB1),
                      onChanged: (bool value) {
                        onSwitchTap.call(value);
                      })
                  : hasMore
                      ? Image.asset("assets/images/icon_more_grey.png",
                          width: ScreenUtil().setWidth(48))
                      : Container(),
              SizedBox(width: ScreenUtil().setWidth(16)),
            ],
          ),
        ),
      ),
    );
  }

  static Widget accountSafeTips(bool binding) {
    // 黄色： 待绑定的警告状态
    var bgColor = '#FEF9EF';
    var icon = "assets/images/setting/settings_warring@2x.png";
    var title = translate("link_phone_poor");
    var titleColor = '#D89A0B';

    if (binding) {
      // 绿色: 绑定的安全状态
      bgColor = '#E3F4EF';
      icon = "assets/images/setting/settings_perfect@2x.webp";
      title = translate("link_phone_strong");
      titleColor = '#0E8F50';
    }
    return Container(
      margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(39),
          right: ScreenUtil().setWidth(39),
          bottom: ScreenUtil().setWidth(30)),
      padding: EdgeInsets.symmetric(
          vertical: ScreenUtil().setWidth(10),
          horizontal: ScreenUtil().setWidth(10)),
      decoration: BoxDecoration(
          color: YBDHexColor(bgColor),
          borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
          shape: BoxShape.rectangle),
      child: Row(
        children: [
          Image.asset(icon, width: ScreenUtil().setWidth(24)),
          SizedBox(width: ScreenUtil().setWidth(13)),
          Container(
            width: ScreenUtil().setWidth(570),
            child: Text(title,
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(24),
                    color: YBDHexColor(titleColor),
                    height: 1.2)),
          ),
        ],
      ),
    );
  }

  /// 弹出完善手机号对话框
  static showPhoneEditDialog(BuildContext context) {
    logger.v('show edit phone dialog ');
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return MultiBlocProvider(
          providers: [
            BlocProvider<YBDPhoneNumberInputBloc>(
              create: (_) => YBDPhoneNumberInputBloc(),
            ),
          ],
          child: YBDEditPhoneDialog(),
        );
      },
    );
  }

  static int loginTypeToInt(LoginType type) {
    var typeInt = 0;
    switch (type) {
      case LoginType.facebook:
        typeInt = 1;
        break;
      case LoginType.twitter:
        typeInt = 2;
        break;
      case LoginType.google:
        typeInt = 3;
        break;
      case LoginType.phone:
        typeInt = 6;
        break;
      case LoginType.apple:
        typeInt = 7;
        break;
      default:
        break;
    }
    return typeInt;
  }

  static LoginType loginTypeWithInt(int type) {
    var typeInt = LoginType.facebook;
    if (type == 1) {
      return LoginType.facebook;
    } else if (type == 2) {
      return LoginType.twitter;
    } else if (type == 3) {
      return LoginType.google;
    } else if (type == 6) {
      return LoginType.phone;
    } else if (type == 7) {
      return LoginType.apple;
    }
    return typeInt;
  }

  static String loginTypeToImageAssets(LoginType type, {bool big = false}) {
    var typeInt = 'assets/images/setting/';
    switch (type) {
      case LoginType.facebook:
        typeInt += 'settings_fb';
        break;
      case LoginType.twitter:
        typeInt += 'settings_twitter';
        break;
      case LoginType.google:
        typeInt += 'settings_google';
        break;
      case LoginType.apple:
        return 'assets/images/login/number_apple_icon.webp';
      default:
        break;
    }
    typeInt += '${big ? '_big' : ''}@2x.webp';
    return typeInt;
  }

  static String cellphoneStar(String phone) {
    if (phone.length > 10) {
      return phone.replaceRange(6, 10, "****");
    }
    return phone;
  }
}
