import 'dart:async';

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/user/entity/mobile_ybd_check_resp_entity.dart';
import 'package:oyelive_main/ui/page/login/widget/dialog_ybd_graphic_code.dart';

typedef void OnTokenId(int? value);

class YBDVerificationCodeView extends StatefulWidget {
  const YBDVerificationCodeView({
    required this.phone,
    required this.authCodeController,
    this.onTokenId,
    Key? key,
  }) : super(key: key);

  final String? phone;
  final OnTokenId? onTokenId;
  final TextEditingController authCodeController;

  @override
  _YBDVerificationCodeViewState createState() => _YBDVerificationCodeViewState();
}

class _YBDVerificationCodeViewState extends State<YBDVerificationCodeView> {
  bool init = true;
  Timer? _codeTimer;

  /// 倒计时60s
  int secs = 60;

  @override
  void dispose() {
    _codeTimer?.cancel();
    _codeTimer = null;
    super.dispose();
  }
  void disposeatIq2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(80),
      width: ScreenUtil().setWidth(580),
      padding: EdgeInsets.only(right: ScreenUtil().setWidth(20)),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: widget.authCodeController,
              style: TextStyle(
                color: Color(0xff333333),
                fontSize: ScreenUtil().setSp(28),
              ),
              decoration: InputDecoration(
                  hintStyle: TextStyle(
                    color: Colors.black.withOpacity(0.5),
                  ),
                  hintText: translate('verification_code'),
                  border: InputBorder.none),
            ),
          ),
          GestureDetector(
            onTap: onTapSend,
            child: Container(
              height: ScreenUtil().setWidth(60),
              width: ScreenUtil().setWidth(init ? 196 : 140),
              decoration: BoxDecoration(
                  color: secs == 60 ? null : Color(0xffC9C9C9),
                  borderRadius: BorderRadius.all(
                    Radius.circular(ScreenUtil().setWidth(8)),
                  ),
                  gradient: secs == 60
                      ? LinearGradient(
                          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter)
                      : null),
              alignment: Alignment.center,
              child: Text(
                translate(
                  secs == 60 ? translate(init ? 'verification_code' : 'edit_phone_resend') : '${secs}s',
                ),
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(22),
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void buildkSCYWoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future onTapSend() async {
    if (secs == 60) {
      YBDDialogUtil.showLoading(context);
      YBDMobileCheckResp? mobileSignInEntity = await ApiHelper.getSMSCode(
        context,
        phone: widget.phone,
        operation: YBDOperation.ChangePwd,
      );
      if (mobileSignInEntity != null && mobileSignInEntity.returnCode == Const.HTTP_SUCCESS) {
        init = false;
        secs--;
        widget.onTokenId?.call(mobileSignInEntity.record!.tokenId);

        _codeTimer = Timer.periodic(Duration(seconds: 1), (timer) {
          if (secs != 0) {
            --secs;
          } else {
            // 复原倒计时
            timer.cancel();
            secs = 60;
          }
          setState(() {});
        });
      } else {
        //
      }

      YBDDialogUtil.hideLoading(context);
      YBDToastUtil.toast(mobileSignInEntity?.returnMsg);
    }
  }
  void onTapSendavjN1oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
