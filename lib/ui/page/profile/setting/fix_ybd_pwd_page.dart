import 'dart:async';

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/event/safe_ybd_account_bus_event.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/result_ybd_bean.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/redux/followed_ybd_ids_redux.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/profile/account_safe/account_ybd_safe_controller.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/entity/safe_ybd_alert_entity.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/util/profile_ybd_edit_util.dart';
import 'package:oyelive_main/ui/page/profile/setting/phone_ybd_view.dart';
import 'package:oyelive_main/ui/page/profile/setting/pwd_ybd_view.dart';
import 'package:oyelive_main/ui/page/profile/setting/verification_ybd_code_view.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/my_ybd_app_bar.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';

class YBDFixPwdPage extends StatefulWidget {
  final bool navigateToLogin;

  final String? location;

  YBDFixPwdPage(
    this.navigateToLogin, {
    this.location,
  });

  @override
  _YBDFixPwdPageState createState() => new _YBDFixPwdPageState();
}

class _YBDFixPwdPageState extends BaseState<YBDFixPwdPage> {
  String? phoneNumberComplete;
  TextEditingController _authCodeController = TextEditingController();
  TextEditingController _oldPwdController = TextEditingController();
  TextEditingController _newPwdController = TextEditingController();
  TextEditingController _confirmPwdController = TextEditingController();

  bool isPwdLegally = true;
  bool? hasPhoneNumber;
  bool? hasPwd;
  int? tokenId;

  @override
  void initState() {
    super.initState();
    _getUserInfo();
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.OPEN_PAGE,
      location: YBDLocationName.FIX_PWD_PAGE,
      itemName: widget.location,
    ));
  }
  void initStateFFDBtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposezbaRhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询用户的安全信息
  Future<void> _getUserInfo() async {
    logger.v('before getUserPhone');
    YBDSafeAlertEntity? resultBeanEntity = await ApiHelper.queryUserSafetyLevel(context);
    logger.v('after getUserPhone');

    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('after getUserPhone success');
      // 根据接口返回的用户信息判断是否有手机号和密码
      phoneNumberComplete = resultBeanEntity!.record?.userInfo?.phone ?? '';
      hasPhoneNumber = phoneNumberComplete!.isNotEmpty;
      hasPwd = resultBeanEntity.record?.userInfo?.password ?? false;
    } else {
      logger.v('after getUserPhone empty');
      // 接口报错默认没手机号, 没有密码
      hasPhoneNumber = false;
      hasPwd = false;
    }

    if (mounted) {
      setState(() {});
    }
  }
  void _getUserInfoB2cd6oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget greyLine() {
    return Container(
      width: ScreenUtil().setWidth(580),
      height: ScreenUtil().setWidth(1),
      color: Colors.black.withOpacity(0.2),
    );
  }

  void onTapConfirm(bool? hasPhone) async {
    if (!hasPwd!) {
      // 设置新密码的提示语
      if (_newPwdController.text != _confirmPwdController.text) {
        YBDToastUtil.toast(translate('confirm_pwd_not_correct'));
        return;
      }
      changePwd();
      return;
    }

    // 改密码流程
    if (hasPhone!) {
      if (_authCodeController.text.isEmpty) {
        YBDToastUtil.toast(translate('enter_code'));
        return;
      }

      if (!checkPwdLegally()) {
        return;
      }
    } else {
      if (_oldPwdController.text.isEmpty) {
        YBDToastUtil.toast(translate('old_pwd_empty'));
        return;
      }
      if (!checkPwdLegally()) {
        return;
      }
    }

    if (_newPwdController.text != _confirmPwdController.text) {
      YBDToastUtil.toast(translate('confirm_pwd_not_correct'));
      return;
    }
    changePwd();
  }
  void onTapConfirmeN3O7oyelive(bool? hasPhone)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  bool checkPwdLegally() {
    isPwdLegally = YBDCommonUtil.checkPwdLegal(_newPwdController.text);
    setState(() {});
    return isPwdLegally;
  }

  void changePwd() async {
    showLockDialog();

    // 设置新密码的参数
    Map<String, dynamic> params = {
      'newPwd': _newPwdController.text,
    };

    if (hasPwd!) {
      // 修改密码的参数
      params['oldPwd'] = _oldPwdController.text.trim();
      params['tokenId'] = tokenId;
      params['token'] = _authCodeController.text.trim();
    }

    YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
      context,
      YBDApi.CHANGE_PWD,
      params,
    );
    dismissLockDialog();
    if (resultBean?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast("Success");
      YBDSPUtil.remove(Const.SP_NEED_FIX_PWD);
      _refreshSafeAccountPwd(updatedPwd: true);
      if (widget.navigateToLogin) {
        Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
        store.dispatch(YBDUpdateUserAction(YBDUserInfo()));
        store.dispatch(YBDUpdateFollowedIdsAction(Set()));
        await YBDSPUtil.remove(Const.SP_USER_INFO);
        await YBDSPUtil.remove(Const.SP_COOKIE_APP);
        await YBDSPUtil.remove(Const.SP_JSESSION_ID);
        Navigator.pop(context);
        // YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.index_page, clearStack: true);
      } else {
        Navigator.pop(context);
      }
    } else {
      YBDToastUtil.toast(resultBean?.returnMsg ?? translate('failed'));
    }
  }
  void changePwdV0Y3Soyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 设置密码后更新安全账号数据
  void _refreshSafeAccountPwd({bool? updatedPwd}) {
    logger.i('update account safe data pwd: $updatedPwd');
    eventBus.fire(YBDSafeAccountBusEvent());
  }

  /// 设置手机后更新安全账号数据
  void _refreshSafeAccountPhone({String? phone}) {
    phoneNumberComplete = phone;
    logger.i('update account safe data phone: $phone');
    final ctr = Get.find<YBDAccountSafeController>();
    if (ctr != null) {
      ctr.record.value?.userInfo?.phone = phone;
      logger.i('account safe data pwd: ${ctr.record.value?.userInfo?.phone}');
    }
  }
  void _refreshSafeAccountPhoneA9tFUoyelive({String? phone}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: YBDMyAppBar(
        backgroundColor: Colors.white,
        leading: YBDNavBackButton(color: Colors.black),
        elevation: 0.0,
        title: Text(
          (hasPwd ?? false) ? translate('change_pwd') : translate('setting_pwd'),
          style: TextStyle(
            color: Colors.black,
            fontSize: YBDTPStyle.spNav,
          ),
        ),
      ),
      body: GestureDetector(
        onTap: () {
          logger.v('clicked bg');
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          decoration: BoxDecoration(),
          width: double.infinity,
          child: hasPhoneNumber != null && hasPwd != null
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: ScreenUtil().setWidth(40),
                    ),
                    // 有手机号时修改密码显示手机号和验证码输入框
                    if (hasPhoneNumber! && hasPwd!) ...[
                      YBDPhoneView(phone: phoneNumberComplete),
                      greyLine(),
                      SizedBox(
                        height: ScreenUtil().setWidth(30),
                      ),
                      YBDVerificationCodeView(
                        phone: phoneNumberComplete,
                        authCodeController: _authCodeController,
                        onTokenId: (value) {
                          tokenId = value;
                        },
                      ),
                      greyLine(),
                      SizedBox(
                        height: ScreenUtil().setWidth(30),
                      ),
                    ],
                    // 没有手机号有旧密码显示旧密码输入框
                    if (!hasPhoneNumber! && hasPwd!) ...[
                      YBDPwdView(
                        controller: _oldPwdController,
                        hint: translate('old_password'),
                      ),
                      greyLine(),
                      SizedBox(height: ScreenUtil().setWidth(30)),
                    ],
                    YBDPwdView(
                      controller: _newPwdController,
                      hint: translate('new_password'),
                      checkPwd: (legal) {
                        setState(() {
                          isPwdLegally = legal;
                        });
                      },
                    ),
                    greyLine(),
                    isPwdLegally
                        ? SizedBox(
                            height: ScreenUtil().setWidth(30),
                          )
                        : Container(
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setWidth(10),
                              bottom: ScreenUtil().setWidth(14),
                            ),
                            width: ScreenUtil().setWidth(580),
                            child: Text(
                              translate('pwd_format_warn'),
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(20),
                                color: Color(0xffD52F2F),
                              ),
                            ),
                          ),
                    YBDPwdView(
                      controller: _confirmPwdController,
                      hint: translate('confirm_password'),
                    ),
                    greyLine(),
                    SizedBox(height: ScreenUtil().setWidth(100)),
                    GestureDetector(
                      onTap: () {
                        onTapConfirm(hasPhoneNumber);
                      },
                      child: Container(
                        width: ScreenUtil().setWidth(560),
                        height: ScreenUtil().setWidth(86),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(ScreenUtil().setWidth(48)),
                          ),
                          gradient: LinearGradient(
                              colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter),
                        ),
                        child: Center(
                          child: Text(
                            translate('ok'),
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(34),
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setWidth(40)),
                    _forgetPwdText(),
                  ],
                )
              : Center(
                  child: YBDLoadingCircle(),
                ),
        ),
      ),
    );
  }
  void myBuild2yaH1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 忘记密码按钮
  /// 点击弹出绑定手机号的弹框,绑定成功后显示手机号修改密码的界面
  Widget _forgetPwdText() {
    if (null != hasPhoneNumber && null != hasPwd && !hasPhoneNumber! && hasPwd!) {
      // 有旧密码并且没有手机号时显示忘记密码按钮
      return GestureDetector(
        onTap: () {
          logger.v('clicked forget pwd btn');
          profileEditShowPhoneEditDialog().then((value) {
            logger.v('profileEditShowPhoneEditDialog result: $value');

            if (value != null && value.isNotEmpty) {
              // 绑定手机号成功后刷新页面
              logger.i('bind phone number success: $value');
              hasPhoneNumber = true;
              _refreshSafeAccountPhone(phone: value);
              if (mounted) {
                setState(() {});
              }
            }
          });

          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.FIX_PWD_PAGE,
            itemName: YBDItemName.FORGET_OLD_PWD,
          ));
        },
        child: Container(
          child: Text(
            translate('forget_old_pwd'),
            style: TextStyle(
              color: Color(0xff868686),
              fontSize: ScreenUtil().setSp(24),
            ),
          ),
        ),
      );
    }

    return SizedBox();
  }
  void _forgetPwdTextpxsxpoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
