import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

class YBDPhoneView extends StatelessWidget {
  final String? phone;
  const YBDPhoneView({
    required this.phone,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(80),
      width: ScreenUtil().setWidth(580),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(
            ScreenUtil().setWidth(30),
          ),
        ),
      ),
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          Text(
            "${translate('phone_number')}: ",
            style: TextStyle(
              fontSize: ScreenUtil().setSp(28),
              color: Colors.black.withOpacity(0.5),
            ),
          ),
          Text(
            phone ?? '',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(28),
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
  void buildwce8Coyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
