import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:package_info/package_info.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../widget/my_ybd_app_bar.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';

class YBDAboutPage extends StatefulWidget {
  @override
  YBDAboutPageState createState() => new YBDAboutPageState();
}

class YBDAboutPageState extends BaseState<YBDAboutPage> {
  String versionName = "";
  String versionCode = "";
  String portalType = "";

  /// 2 秒内点击 5 次弹出共享日志的弹框
  final _clickDuration = 3;
  final _clickAmount = 5;
  var _count = 0;

  init() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    versionName = packageInfo.version;
    versionCode = packageInfo.buildNumber;

    portalType = (await YBDSPUtil.get(Const.SP_PORTAL_TYPE)) ?? '-';
    setState(() {});
  }

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      appBar: YBDMyAppBar(
        backgroundColor: Colors.white,
        leading: YBDNavBackButton(color: Colors.black),
        elevation: 0.0,
      ),
      body: Container(
        width: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            GestureDetector(
              onTap: () async {
                // 连击显示日志弹框
                if (++_count > _clickAmount) {
                  _count = 0;
                  YBDDialogUtil.showLoading(context, barrierDismissible: false);
                  await YBDLogUtil.shareLog();
                  YBDDialogUtil.hideLoading(context);
                }

                // 连击3次收集皮肤日志
                if (_count > 2) {
                  // 收集皮肤日志
                  YBDCommonUtil.storeSkinError('skin log');
                  YBDToastUtil.toast('Version: $versionName');
                }

                // 清空连击次数
                Future.delayed(Duration(seconds: _clickDuration), () {
                  _count = 0;
                });
              },
              child: Image.asset("assets/images/logo.webp", width: ScreenUtil().setWidth(300)),
            ),
            Expanded(child: Container()),
            Text("Version: $versionName ($versionCode)", style: TextStyle(color: Colors.black)),
            SizedBox(height: ScreenUtil().setWidth(10)),
            Text("$portalType", style: TextStyle(color: Colors.black)),
            SizedBox(height: ScreenUtil().setWidth(10)),
            Text("Copyright © 2018-2022", style: TextStyle(color: Colors.black)),
            SizedBox(height: ScreenUtil().setWidth(10)),
            Text("OyeTalk Dev.", style: TextStyle(color: Colors.black)),
            SizedBox(height: ScreenUtil().setWidth(10)),
            Text("All Rights Reserved.", style: TextStyle(color: Colors.black)),
            SizedBox(height: ScreenUtil().setWidth(100)),
          ],
        ),
      ),
    );
  }
  void myBuildkhD3koyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    init();
  }
  void initStatew3xZzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDAboutPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetu11oeoyelive(YBDAboutPage oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
