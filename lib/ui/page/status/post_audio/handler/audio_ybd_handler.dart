import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter_ffmpeg/flutter_ffmpeg.dart';
import 'package:flutter_ffmpeg/media_information.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:random_string/random_string.dart';

import '../../../../../common/constant/const.dart';
import '../../../../../common/util/common_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/numeric_ybd_util.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../module/status/entity/music_ybd_list_entity.dart';
import '../../../../../module/status/entity/random_ybd_background_entity.dart';
import '../../../../../module/status/entity/random_ybd_dialogue_entity.dart';
import '../../../../../module/status/entity/random_ybd_lyric_entity.dart';

// import '../post_ybd_audio_page.dart';
enum AudioStatusType { Sing, Mood, Dialogue }

class YBDAudioHandler {
  static final playerId = "oyetalk_mixer";

  AudioPlayer audioPlayer = AudioPlayer(playerId: playerId);

  static YBDAudioHandler? _instance;

  static YBDAudioHandler getInstance() {
    if (_instance == null) {
      _instance = YBDAudioHandler();
    }

    return _instance!;
  }

  destroy() {
    logger.v("clear");
    _instance = null;
  }

  //选择的网络音乐
  YBDMusicListDataRow? selectedMusic;

// 音频动态类型
  AudioStatusType audioStatusType = AudioStatusType.Mood;

  double? backgroundMusicVolume = 0.5;
  double? recordedMusicVolume = 1.0;

  //是否使用背景音乐
  bool useBackgroundMusic = false;

//原录音文件地址
  String? recordedFilePath;

  //使用的背景音乐文件地址
  String? backgroundMusicFilePath;

//录音时长
  Duration? audioDuration = Duration();

  //最后混音的文件
  String? finalAudioPath;

  /// text content
  /// 选择Mood后填写的文本内容
  String moodTextContent = '';

  //选择dialogue的对话实体
  YBDRandomDialogueDataDialogue? dialogue;

  //选择Sing的歌词实体
  YBDRandomLyricDataLyric? lyric;

  //选择的背景实体
  YBDRandomBackgroundDataBgimage? backgroundImage;

  noiseReduce() async {
    FlutterFFmpeg _flutterFFmpeg = new FlutterFFmpeg();
    String newFileName = recordedFilePath!.replaceAll(".m4a", "_nr.m4a");
    await _flutterFFmpeg.execute(
        '-i $recordedFilePath -af "highpass=f=200, lowpass=f=3000" $newFileName -y');
    recordedFilePath = newFileName;
    print("set noise cancel file as recorded file $newFileName");
  }

  /// 制作录音与背景音乐的混音
  Future<String?> mixRecordAndBgMusic() async {
    if (recordedFilePath == null) {
      YBDLogUtil.e("record not found $recordedFilePath");
      return null;
    }

    ///    storage/emulated/0/ybd_oyetalk/record/11.m4a
    try {
      String mixedPath =
          await YBDCommonUtil.getResourceDir(Const.RESOURCE_BACKGROUND_MUSIC);
      logger.v('mixed path : $mixedPath');
      String saveName = recordedFilePath!
          .split('/')
          .last
          .replaceAll('.', "_${randomAlphaNumeric(5)}_mixed.");
      logger.v('save name : $saveName');

      FlutterFFmpeg _flutterFFmpeg = new FlutterFFmpeg();
      FlutterFFprobe _pr = FlutterFFprobe();

      String ffcomand;
      await noiseReduce();
      if (!useBackgroundMusic ||
          (backgroundMusicFilePath == null && useBackgroundMusic)) {
        logger.v('not use background music');
        ffcomand =
            '-i $recordedFilePath -filter:a "volume=$recordedMusicVolume" $mixedPath/$saveName -y';
      } else {
        int backgroundMusicDuration = 0;
        MediaInformation bgMusicInfo =
            await _pr.getMediaInformation(backgroundMusicFilePath!);
        backgroundMusicDuration = YBDNumericUtil.dynamicToInt(
            bgMusicInfo.getMediaProperties()!['duration']);
        logger.v(
            '===audio status backgroundMusicDuration : $backgroundMusicDuration');

        int recordDuration = 0;
        MediaInformation recordedMusicInfo =
            await _pr.getMediaInformation(recordedFilePath!);
        recordDuration = YBDNumericUtil.dynamicToInt(
            recordedMusicInfo.getMediaProperties()!['duration']);
        logger.v('===audio status recordDuration : $recordDuration');

        if (backgroundMusicDuration < recordDuration) {
          logger.v('background music duration less than record duration');
          ffcomand =
              '-i $recordedFilePath -filter_complex \"amovie=$backgroundMusicFilePath:loop=999,volume=$backgroundMusicVolume[s];[0][s]amix=duration=shortest,volume=$recordedMusicVolume\" $mixedPath/$saveName -y ';
        } else {
          logger.v('background music duration greater than record duration');
          logger.v('recorded file path : $recordedFilePath');
          logger.v('background music file path : $backgroundMusicFilePath');
          ffcomand =
              "-i $recordedFilePath -i $backgroundMusicFilePath -filter_complex "
              "\"[0:a]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=$recordedMusicVolume[a0];"
              " [1:a]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=$backgroundMusicVolume[a1]; [a0][a1]amerge=inputs=2[aout]\" -map \"[aout]\" -ac 2 $mixedPath/$saveName -y -shortest";
        }
      }

      logger.v("ffmpeg command: $ffcomand");
      await _flutterFFmpeg.execute(ffcomand);
      logger.v("ffmpeg mix complete file path : $mixedPath/$saveName");
      return '$mixedPath/$saveName';
    } catch (e) {
      logger.e('===audio status mix audio error : $e');
      return null;
    }
  }

  editAudioAttribute() async {
    logger.v('remix music');
    String? filePath = await mixRecordAndBgMusic();

    if (filePath != null) {
      finalAudioPath = filePath;
      logger.v('remixed music path : $finalAudioPath');
      if (audioPlayer.state == PlayerState.PLAYING) {
        await audioPlayer.stop();
      }

      audioPlayer.play(finalAudioPath!);
    } else {
      YBDToastUtil.toast(translate('missing_file'));
    }
  }
}
