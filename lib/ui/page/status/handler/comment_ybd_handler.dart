import 'dart:async';

import 'package:flutter_translate/flutter_translate.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/status/entity/comment_ybd_list_entity.dart';
import '../../../../module/status/status_ybd_api_helper.dart';
import 'status_ybd_change_event.dart';

import '../../../../main.dart';

class YBDCommentHandler {
  List<YBDCommantListDataRows?>? _commentRows;

  List<YBDCommantListDataRows?>? get CommentRows => _commentRows;

  set CommentRows(List<YBDCommantListDataRows?>? value) {
    _commentRows = value;
  }

  int currentPage = 1;
  int totalPage = 0;
  final pageSize = 10;

  int? allCount, countWithReply;

  Future<List<YBDCommantListDataRows?>?> initData(context, String? statusId) async {
    currentPage = 1;

    YBDCommentListEntity? commentListEntity = await YBDStatusApiHelper.queryCommentList(context, statusId, page: 1);
    if (commentListEntity != null) {
      if (commentListEntity.code == Const.HTTP_SUCCESS_NEW) {
        totalPage = commentListEntity.data!.total! ~/ pageSize + (commentListEntity.data!.total! % pageSize == 0 ? 0 : 1);
        _commentRows = commentListEntity.data!.rows;
        countWithReply = commentListEntity.data!.commentTotalCount;
        /////改变comment数量

        eventBus.fire(YBDStatusChangeEvent(statusId, commentCount: countWithReply));
        return _commentRows;
      } else {
        YBDToastUtil.toast(commentListEntity.message);
      }
    }
    return null;
  }

  Future nextPage(context, statusId) async {
    logger.v("currentPage$currentPage totalPage$totalPage");
    if (currentPage < totalPage) {
      YBDCommentListEntity? commentListEntity =
          await YBDStatusApiHelper.queryCommentList(context, statusId, page: ++currentPage);
      if (commentListEntity != null) {
        if (commentListEntity.code == Const.HTTP_SUCCESS_NEW) {
          totalPage = commentListEntity.data!.total! ~/ pageSize + (commentListEntity.data!.total! % pageSize == 0 ? 0 : 1);
          _commentRows!.addAll(commentListEntity.data!.rows!);
          return _commentRows;
        } else {
          YBDToastUtil.toast(commentListEntity.message);
        }
      }
      return null;
    } else {
      logger.v('no more data');
      return "false";
    }
  }
  void nextPageEu0KWoyelive(context, statusId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
