import 'dart:async';

class YBDStatusDeleteEvent {
  String? statusId;

  /// 用户 id
  String? userId;

  YBDStatusDeleteEvent(this.statusId, {this.userId});
}
