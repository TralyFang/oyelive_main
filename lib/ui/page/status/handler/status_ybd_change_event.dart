import 'dart:async';

class YBDStatusChangeEvent {
  bool? isLiked, isFollowed;
  String? statusid;
  int? shareCount, likeCount, commentCount, giftCount;

  YBDStatusChangeEvent(this.statusid,
      {this.isLiked, this.isFollowed, this.shareCount, this.likeCount, this.commentCount, this.giftCount});
}
