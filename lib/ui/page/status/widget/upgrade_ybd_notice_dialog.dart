import 'dart:async';

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../local_audio/scale_ybd_animate_button.dart';

/// 发动态时提醒升级app的弹框
class YBDUpgradeNoticeDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {},
                child: Container(
                  width: ScreenUtil().setWidth(500),
                  // height: ScreenUtil().setWidth(300),
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Color(0xff47CDCC),
                        width: ScreenUtil().setWidth(3),
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(ScreenUtil().setWidth(32)),
                      ),
                      color: Colors.white),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setWidth(10)),
                      _content(),
                      SizedBox(height: ScreenUtil().setWidth(10)),
                      _actionButtons(context),
                      SizedBox(height: ScreenUtil().setWidth(50)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void builddMEZxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消和确定按钮
  Widget _actionButtons(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        YBDScaleAnimateButton(
          onTap: () {
            logger.v('clicked cancel btn');
            Navigator.pop(context);
          },
          child: _bottomBtn('Later', 0),
        ),
        SizedBox(width: ScreenUtil().setWidth(15)),
        YBDScaleAnimateButton(
          onTap: () {
            logger.v('clicked ok btn');
            Platform.isIOS ? launch(Const.APPLE_STORE) : launch(Const.GOOGLE_STORE);
          },
          child: _bottomBtn('Upgrade Now', 1),
        ),
      ],
    );
  }
  void _actionButtonsEOIDpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 内容
  Widget _content() {
    return Container(
      margin: EdgeInsets.all(ScreenUtil().setWidth(30)),
      child: Center(
        child: Text(
          // 对话框标题
          'Oops! App version out of date. Please upgrade to the latest version.',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            fontWeight: FontWeight.w500,
            height: 1.5,
            color: Color.fromRGBO(0, 0, 0, 0.85),
          ),
        ),
      ),
    );
  }
  void _contentPoNNPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部按钮
  Widget _bottomBtn(String title, int index) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
        gradient: LinearGradient(
          colors: index == 0 ? [Color(0xffCCCCCC), Color(0xffCCCCCC)] : [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      width: ScreenUtil().setWidth(200),
      height: ScreenUtil().setWidth(64),
      alignment: Alignment.center,
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(28),
          color: Colors.white,
        ),
      ),
    );
  }
  void _bottomBtnIpyZAoyelive(String title, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
