import 'dart:async';

import '../../../../generated/json/base/json_convert_content.dart';

class YBDSLogConfigFromServer with JsonConvert<YBDSLogConfigFromServer> {
  int? max_picture;
  int? max_record_duration;
  int? min_record_duration;
  int? bg_changing_interval;
  int? max_record_clip;
  int? max_text_length;
  int? max_text_line;
  int? min_play_visibility_percentage;
}
