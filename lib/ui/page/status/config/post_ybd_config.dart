import 'dart:async';

import 'dart:convert';

import 'slog_ybd_config_from_server.dart';

class YBDSLogConfig {
  static int max_picture = 5;
  static int max_record_duration = 90;
  static int min_record_duration = 5;
  static int bg_changing_interval = 3;
  static int max_record_clip = 10;
  static int max_text_length = 144;
  static int max_text_line = 6;
  static int min_play_visibility_percentage = 100;
  static final download_music_name_header = "oy_download_music_";

  static setConfig(String config) {
    try {
      YBDSLogConfigFromServer? server = YBDSLogConfigFromServer().fromJson(json.decode(config));
      max_picture = server.max_picture ?? 5;
      max_record_duration = server.max_record_duration ?? 90;
      min_record_duration = server.min_record_duration ?? 5;
      bg_changing_interval = server.bg_changing_interval ?? 3;
      max_record_clip = server.max_record_clip ?? 10;
      max_text_length = server.max_text_length ?? 144;
      max_text_line = server.max_text_line ?? 6;
      min_play_visibility_percentage = server.min_play_visibility_percentage ?? 100;
    } catch (e) {
      print(e);
    }
  }
}
