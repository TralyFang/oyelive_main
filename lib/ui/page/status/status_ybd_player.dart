import 'dart:async';

import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';

import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/resource_ybd_path_util.dart';
import '../../../module/status/entity/status_ybd_entity.dart';

/// 动态播放器单例
class YBDStatusPlayer {
  /// 默认动态 id
  static const String DEFAULT_STATUS_ID = '-1';

  /// 播放器实例
  static YBDStatusPlayer? _instance;

  static YBDStatusPlayer? get instance => _getInstance();

  /// 构造函数返回实例
  factory YBDStatusPlayer() => _getInstance()!;

  static YBDStatusPlayer? _getInstance() {
    if (_instance == null) {
      _instance = YBDStatusPlayer._();
    }
    return _instance;
  }

  /// 私有构造函数
  YBDStatusPlayer._() {
    _audioPlayer = AudioPlayer(playerId: "status_player");
    _audioPlayer.setReleaseMode(ReleaseMode.LOOP);
    _audioPlayer.onAudioPositionChanged.listen((Duration duration) {
      // 解决调 [stop] 方法仍然播放的问题
      if (duration.inSeconds > 0 && _currentPlayingId == DEFAULT_STATUS_ID) {
        // true 表示已经调用了 [stop] 方法来停止播放动态
        // 这里直接停止播放器
        _audioPlayer.stop();
      }

      if (null != _onDuration) {
        _onDuration!(duration, _currentPlayingId);
      } else {
        logger.v('on duration call back is null');
      }
    });

    _controller = StreamController<YBDStatusPlayerEvent>.broadcast();
  }

  /// 规避混合包进房间[VisibilityDetector]检测动态列表可见的问题
  var scrolling = true;

  /// 播放进度回调
  void Function(Duration, String?)? _onDuration;

  set onDuration(void Function(Duration, String?) value) {
    _onDuration = null;
    _onDuration = value;
  }

  /// 事件广播
  /// 调用 onEvent.listen() 接收广播
  // ignore: close_sinks
  late StreamController<YBDStatusPlayerEvent> _controller;

  /// 监听消息：onEvent.listen((event) {}
  /// 广播消息：_controller.sink.add(YBDStatusPlayerEvent());
  Stream<YBDStatusPlayerEvent> get onEvent => _controller.stream;

  /// 正在播放的动态 id
  String? _currentPlayingId = DEFAULT_STATUS_ID;

  /// 音频播放器
  late AudioPlayer _audioPlayer;

  /// 停止播放
  void stop() {
    logger.v('stop play status : $_currentPlayingId');
    // 广播停止播放的动态 id
    _controller.sink.add(YBDStatusPlayerEvent(type: EventType.Stop, statusId: _currentPlayingId));
    _audioPlayer.stop();
    _currentPlayingId = DEFAULT_STATUS_ID;
  }
  void stop6z03doyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 停止指定的动态
  void stopWithStatusId(String? statusId) {
    if (statusId != _currentPlayingId) {
      // id 不一致则取消停止操作
      return;
    }

    // 停止播放
    stop();
  }
  void stopWithStatusIdCNVWDoyelive(String? statusId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 暂停播放
  void pause() {
    _controller.sink.add(YBDStatusPlayerEvent(type: EventType.Pause, statusId: _currentPlayingId));
    _audioPlayer.pause();
    _currentPlayingId = DEFAULT_STATUS_ID;
  }
  void pauseZb4HIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 开始播放
  Future<void> play(BuildContext context, YBDStatusInfo statusInfo) async {
    _currentPlayingId = statusInfo.id;
    logger.v(
        '===play=== _currentPlayingId : $_currentPlayingId , ${await YBDResourcePathUtil.getFullPath(context, statusInfo.content!.audio?.resource)}');
    _controller.sink.add(YBDStatusPlayerEvent(type: EventType.Play, statusId: _currentPlayingId));
    _audioPlayer.play(await YBDResourcePathUtil.getFullPath(context, statusInfo.content!.audio?.resource));
  }
  void playzVKWKoyelive(BuildContext context, YBDStatusInfo statusInfo)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 检查动态是否正在播放
  bool isPlaying(String? statusId) {
    return statusId == _currentPlayingId;
  }
}

enum EventType {
  Play,
  Pause,
  Stop,
}

/// 动态播放器事件
class YBDStatusPlayerEvent {
  /// 播放进度
  Duration? duration;

  /// 当前播放的动态 id
  String? statusId;

  EventType? type;

  YBDStatusPlayerEvent({this.type, this.duration, this.statusId});
}
