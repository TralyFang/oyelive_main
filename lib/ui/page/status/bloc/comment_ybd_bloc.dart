import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../module/status/entity/comment_ybd_list_entity.dart';
import '../../../../module/status/entity/comment_ybd_submit_resp_entity.dart';
import '../../../../module/status/status_ybd_api_helper.dart';

enum CommentEvent { Reply, Comment, ReplySuccess, DeleteReply }

class YBDCommentBloc extends Bloc<CommentEvent, YBDCommentState> {
  YBDCommentBloc(YBDCommentState initialState) : super(initialState);
  late YBDCommentState _commentState;
  TextEditingController? _textEditingController;
  FocusNode? _focusNode;
  @override
  Stream<YBDCommentState> mapEventToState(CommentEvent event) async* {
    // TODO: implement mapEventToState

    switch (event) {
      case CommentEvent.Reply:
        // TODO: Handle this case.
        break;
      case CommentEvent.Comment:
        // TODO: Handle this case.
        _commentState = YBDCommentState();
        break;
      case CommentEvent.ReplySuccess:
        // TODO: Handle this case.
        break;
      case CommentEvent.DeleteReply:
        // TODO: Handle this case.
        yield YBDCommentState()..deleteCount = 1;
        break;
    }
    yield YBDCommentState.clone(_commentState);
  }
  void mapEventToStateCRBBYoyelive(CommentEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  reply(YBDCommentState commentState) {
    _commentState = commentState;
    _focusNode?.requestFocus();
    add(CommentEvent.Reply);
  }

  replySubmit(YBDReplyData? replyData) async {
    _commentState.replyData = replyData;
    add(CommentEvent.ReplySuccess);
  }

  setControllerNFocusNode(TextEditingController textEditingController, FocusNode focusNode) {
    _focusNode = focusNode;
    _textEditingController = textEditingController;
  }

  deleteReply(id, count) {
    _commentState = YBDCommentState()
      ..deleteCount = count
      ..deleteId = id;
    add(CommentEvent.DeleteReply);
  }
}

class YBDCommentState {
  String? commentId;
  String? replyToUserId;
  String? replyName;
  YBDReplyData? replyData;
  int? deleteCount;
  String? deleteId;

  YBDCommentState({this.commentId, this.replyName, this.replyToUserId, this.replyData, this.deleteCount, this.deleteId});

  YBDCommentState.clone(YBDCommentState state)
      : this(
            commentId: state.commentId,
            replyName: state.replyName,
            replyToUserId: state.replyToUserId,
            replyData: state.replyData,
            deleteCount: state.deleteCount,
            deleteId: state.deleteId);
}
