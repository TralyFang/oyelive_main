import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/util/database_ybd_until.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/status/entity/db_ybd_status_entity.dart';
import '../../../../module/status/entity/status_ybd_entity.dart';
import '../../../../module/status/entity/status_ybd_list_entity.dart';
import '../../../../module/status/status_ybd_api_helper.dart';
import '../../../../module/status/status_ybd_def.dart';

enum LoadType {
  LoadHistory,
  Init,

  /// 请求接口成功后刷新状态
  LoadSuccessData,

  /// 请求接口报错后刷新状态
  LoadErrorData,
  NextPage,
  ReLoad,
  Delete,
  Follow,
  FeaturedTop, // Feature 滚动到顶部
  FeaturedSelected, // 选中 Feature 事件
  FollowingTop, // Following 滚动到顶部
  FollowingSelected, // 选中 Following 事件
}

/// 位移大于[STATUS_SCROLLED_OFFSET]标记为滚动状态
const STATUS_SCROLLED_OFFSET = 20;

class YBDStatusContentBloc extends Bloc<LoadType, YBDStatusListWithS> {
  // TODO: 测试代码 动态改全屏后不再使用该刷新控制器
  // RefreshController _refreshController;

  // set refreshController(RefreshController value) {
  //   _refreshController = value;
  // }

  /// 列表滚动控制器
  ScrollController? scrollController;

  /// 规避混合包进房间[VisibilityDetector]检测动态列表可见的问题
  var _currentOffset = 0.0;

  StatusListType dataType;
  BuildContext context;

  YBDStatusContentBloc(this.context, this.dataType)
      : super(YBDStatusListWithS(
          DataStatus.NoData,
          null,
        ));

  int currentPage = 1;
  bool hasNext = true;
  final pageSize = 10;
  List<YBDStatusInfo?>? rowsData;

  /// 正在加载下一页
  bool _isLoadingNext = false;

  @override
  Stream<YBDStatusListWithS> mapEventToState(LoadType event) async* {
    YBDStatusListEntity? result;
    switch (event) {
      case LoadType.LoadHistory:
        YBDDBStatusEntity? dbStatusEntity;
        try {
          dbStatusEntity = await YBDDataBaseUtil.getInstance().queryStatus(StatusScene.values[dataType.index]);
        } catch (e) {
          logger.v(e);
        }

        if (dbStatusEntity != null) {
          try {
            YBDStatusListEntity? data = await Future.value(YBDStatusListEntity().fromJson(json.decode(dbStatusEntity.YBDStatusContent ?? '')));
            rowsData = data?.data?.xList?.rows;

            yield YBDStatusListWithS(DataStatus.Loaded, rowsData);
          } catch (e) {
            print(e);
            add(LoadType.ReLoad);
          }
        } else {
          add(LoadType.ReLoad);
        }
        break;
      case LoadType.ReLoad:
      case LoadType.Init:
        if (event == LoadType.ReLoad) {
          yield YBDStatusListWithS(DataStatus.NoData, null);
        }

        //初始化
        rowsData = null;

        result = await initData(context);

        if (result != null) {
//          rowsData = [];
//          if (result.data.tops != null) {
//            //loop to add status from tops
//            result.data.tops.forEach((element) {
//              rowsData.add(element.status);
//            });
//          }
//          rowsData.addAll(addDataNRemoveReplicated(result.data.xList.rows));
          rowsData = await addTopsByPosition(result.data!.tops, result.data!.xList!.rows);

          if (Platform.isIOS) {
            await removeBlockedUserStatusData();
          }

          yield YBDStatusListWithS(DataStatus.Loaded, rowsData);
        } else {
          logger.v('refresh data empty');
          yield YBDStatusListWithS(DataStatus.Error, null);
        }

        break;
      case LoadType.NextPage:
        if (_isLoadingNext) {
          logger.v('is loading next page');
          return;
        }
        _isLoadingNext = true;
        result = await nextPage(context);
        logger.v('nextPage result length : ${result?.data?.xList?.rows?.length}');
        _isLoadingNext = false;
        if (result != null) {
          rowsData!.addAll(addDataNRemoveReplicated(result.data!.xList!.rows)!);

          if (Platform.isIOS) {
            await removeBlockedUserStatusData();
          }
        }
        yield YBDStatusListWithS(DataStatus.Loaded, rowsData);
        break;
      case LoadType.Delete:
        if (Platform.isIOS) {
          await removeBlockedUserStatusData();
        }
        yield YBDStatusListWithS(DataStatus.Loaded, rowsData);
        break;
      case LoadType.Follow:
        if (Platform.isIOS) {
          await removeBlockedUserStatusData();
        }
        yield YBDStatusListWithS(DataStatus.Loaded, rowsData);
        break;
      case LoadType.FeaturedTop:
        yield YBDStatusListWithS(DataStatus.Loaded, rowsData, featureScrollToP: true);
        break;
      case LoadType.FeaturedSelected:
        logger.v('mapevent LoadType.FeatureSelected');
        yield YBDStatusListWithS(DataStatus.Loaded, rowsData, featuredClicked: true);
        break;
      case LoadType.FollowingTop:
        yield YBDStatusListWithS(DataStatus.Loaded, rowsData, followingScrollToP: true);
        break;
      case LoadType.FollowingSelected:
        logger.v('mapevent LoadType.FollowingSelected');
        yield YBDStatusListWithS(DataStatus.Loaded, rowsData, followingClicked: true);
        break;
      case LoadType.LoadSuccessData:
        yield YBDStatusListWithS(DataStatus.Loaded, rowsData);
        break;
      case LoadType.LoadErrorData:
        yield YBDStatusListWithS(DataStatus.Error, null);
        break;
    }
  }
  void mapEventToStateND6yvoyelive(LoadType event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 移除被调被屏蔽的用户动态
  removeBlockedUserStatusData() async {
    List<String>? blockedUsers = await YBDSPUtil.getBlockedUserList();

    if (null != blockedUsers && blockedUsers.isNotEmpty) {
      rowsData!.retainWhere((element) => !blockedUsers.contains(element!.userId));
    }
  }

  Future<YBDStatusListEntity?> getStatusList(
    context,
    int page,
  ) async {
    // if (page == 1) {
    //   _refreshController?.resetNoData();
    // }
    YBDStatusListEntity? result = await YBDStatusApiHelper.queryStatusList(context, dataType,
        page: page, pageSize: pageSize, status: 1); //状态(0:待审，1发布，2拒绝)

    if (result == null) return null;
    if (result.code != Const.HTTP_SUCCESS_NEW) {
      logger.v('get status list failed result code : ${result.code}');
      if (result.code != Const.HTTP_SESSION_TIMEOUT_NEW) YBDToastUtil.toast(result.message ?? 'Load status failed!');
      return null;
    } else {
      hasNext = !(result.data!.xList!.rows!.length < pageSize);

      return result;
    }
  }

  Future<YBDStatusListEntity?> initData(context) async {
    logger.v('initData');
    //_refreshController?.resetNoData();
    currentPage = 1;
    YBDStatusListEntity? result = await getStatusList(context, currentPage);
    // if (_refreshController?.isRefresh) {
    //   _refreshController?.refreshCompleted();
    // }
    if (result != null && result.code == Const.HTTP_SUCCESS_NEW) {
      currentPage = 1;
      return result;
    } else {
      return null;
    }
  }

  /// 刷新列表
  Future<void> refreshData(BuildContext? context) async {
    logger.v('refreshData');
    rowsData = null;
    _isLoadingNext = false;
    YBDStatusListEntity? result = await initData(context);

    if (result != null) {
      rowsData = await addTopsByPosition(result.data!.tops, result.data!.xList!.rows);

      if (Platform.isIOS) {
        await removeBlockedUserStatusData();
      }

      add(LoadType.LoadSuccessData);
      // yield YBDStatusListWithS(DataStatus.Loaded, rowsData);
    } else {
      logger.v('refresh data empty');
      add(LoadType.LoadErrorData);
      // yield YBDStatusListWithS(DataStatus.Error, null);
    }
  }
  void refreshDataQ8KfSoyelive(BuildContext? context)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<YBDStatusListEntity?> nextPage(context) async {
    if (hasNext) {
      // _refreshController?.resetNoData();
      logger.v('currentPage : $currentPage');
      YBDStatusListEntity? result = await getStatusList(context, ++currentPage);
      // if (_refreshController?.isLoading) {
      //   _refreshController?.loadComplete();
      // }
      if (result?.code == Const.HTTP_SUCCESS_NEW) {
        return result;
      } else {
        return null;
      }
    } else {
      // 没有更多
      // if (_refreshController?.isLoading) {
      //   _refreshController?.loadNoData();
      // }
      return null;
    }
  }

  deleteAt(int index) {
    rowsData!.removeAt(index);
    add(LoadType.Delete);
  }

  followRefresh(String? userId) {
    for (int index = 0; index < rowsData!.length; index++) {
      if (rowsData![index]!.userId == userId) {
        rowsData![index]!.follow = true;

        logger.v("fixIndex$index");
      }
    }
    add(LoadType.Follow);
  }

  List<YBDStatusInfo?>? addDataNRemoveReplicated(List<YBDStatusInfo?>? data) {
    if (rowsData == null || rowsData!.length == 0) {
      return data;
    }
    List<String?> currentExistId = [];
    rowsData!.forEach((element) {
      currentExistId.add(element!.id);
    });
    final result = data!.where((element) => !currentExistId.contains(element!.id)).toList();
    logger.v('add data length : ${result.length}');
    return result;
  }

  Future<List<YBDStatusInfo?>?> addTopsByPosition(List<YBDtopContent?>? tops, List<YBDStatusInfo?>? dataResource) async {
    if (tops == null || tops.length == 0) {
      return dataResource;
    }

    if (dataResource == null || dataResource.length == 0) {
      List<YBDStatusInfo?> result = [];

      tops.sort((a, b) => a!.no!.compareTo(b!.no!));
      tops.forEach((element) {
        result.add(element!.status);
      });
    }

    List<String?> currentExistId = [];
    tops.forEach((element) {
      if (element!.status != null) currentExistId.add(element.status!.id);
    });

    List<YBDStatusInfo?> afterRemoveReplicated =
        await Future.value(dataResource!.where((element) => !currentExistId.contains(element!.id)).toList());

    tops.forEach((element) {
      if (element!.no! <= afterRemoveReplicated.length)
        afterRemoveReplicated.insert(element.no! - 1, element.status);
      else
        afterRemoveReplicated.add(element.status);
    });

    return afterRemoveReplicated;
  }

  /// 滚动到列表顶部
  void scrollToTop(LoadType type) {
    logger.v('scrollToTop: $type');
    add(type);
  }
  void scrollToTopoxd45oyelive(LoadType type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 监听滚动事件
  // TODO: 测试代码 动态改成全屏翻页后不用判断列表的滚动状态
  // void addScrollListener() {
  //   scrollController.addListener(() {
  //     // 位移大于[STATUS_SCROLLED_OFFSET]标记为滚动状态
  //     if ((scrollController.offset - _currentOffset).abs() > STATUS_SCROLLED_OFFSET) {
  //       YBDStatusPlayer.instance.scrolling = true;
  //       _currentOffset = scrollController.offset;
  //     }
  //   });
  // }
}

enum DataStatus { NoData, Loaded, Empty, Error }

class YBDStatusListWithS {
  DataStatus dataStatus;
  List<YBDStatusInfo?>? listData;

  /// Feature 列表是否滚动到列表顶部
  bool featureScrollToP;

  /// Following 列表是否滚动到列表顶部
  bool followingScrollToP;

  /// 点击了 Featured 标签
  bool featuredClicked;

  /// 点击了 Following 标签
  bool followingClicked;

  YBDStatusListWithS(
    this.dataStatus,
    this.listData, {
    this.featureScrollToP = false,
    this.followingScrollToP = false,
    this.featuredClicked = false,
    this.followingClicked = false,
  });
}
