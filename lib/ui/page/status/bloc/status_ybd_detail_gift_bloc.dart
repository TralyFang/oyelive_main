import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/int_ybd_ext.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/room/entity/gift_ybd_package_entity.dart';
import '../../../../module/status/entity/status_ybd_entity.dart';
import '../../../../module/status/status_ybd_api_helper.dart';
import '../../../../redux/user_ybd_redux.dart';
import '../../room/widget/dialog_ybd_insufficient_warn.dart';

enum GiftEvent { Init, Send, SendSuccess, Refresh }
enum StatusSheetAction { Init, ChangeTab, ChangePage, SelectGift, ChangeMultiple, SelectReceiver }

class YBDGiftBloc extends Bloc<YBDGiftState, YBDGiftState> {
  late YBDGiftState state;
  YBDGiftBloc(YBDGiftState initialState) : super(initialState) {
    state = initialState;
  }

  @override
  Stream<YBDGiftState> mapEventToState(YBDGiftState event) async* {
    switch (event.sheetAction) {
      case StatusSheetAction.Init:
        // TODO: Handle this case.
        break;
      case StatusSheetAction.ChangeTab:
        // TODO: Handle this case.
        state.viewingTabIndex = event.viewingTabIndex;
        state.selectingGiftIndex = null;
        state.giftInfo = null;
        break;
      case StatusSheetAction.ChangePage:
        // TODO: Handle this case.
        state.viewingPageIndex = event.viewingPageIndex;
        break;
      case StatusSheetAction.SelectGift:
        // TODO: Handle this case.
        state.selectingGiftIndex = event.selectingGiftIndex;
        state.giftInfo = event.giftInfo;
        break;
      case StatusSheetAction.ChangeMultiple:
        // TODO: Handle this case.
        state.multiple = event.multiple;
        break;
      case StatusSheetAction.SelectReceiver:
        // TODO: Handle this case.
        state.statusInfo = event.statusInfo;
        break;
      // case StatusSheetAction.Send:
      //   state = YBDGiftState(event.sheetAction);
      //   break;
    }
    state.sheetAction = event.sheetAction;

    yield new YBDGiftState.clone(state);
  }
  void mapEventToStateGRqDDoyelive(YBDGiftState event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  checkCanSend(int? money, BuildContext context) {
    if (state.statusInfo!.userId == null) {
      YBDToastUtil.toast(translate('select_receiver'));
      return false;
    }

    if (state.selectingGiftIndex == null) {
      YBDToastUtil.toast(translate('select_gift'));
      return false;
    }

    logger.v('state.giftInfo viewingTabIndex:${state.viewingTabIndex}');
    if (state.multiple! * state.giftInfo!.price! > money!) {
      // YBDToastUtil.toast(translate('amount_insufficient'));
      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return YBDInsufficinetDialog();
          });
      return false;
    }

    // 检查礼物限制条件
    var isRestriction = YBDCommonUtil.stintBuy(
      type: state.giftInfo!.conditionType,
      lv: state.giftInfo!.conditionExtends,
    );

    if (isRestriction) {
      YBDToastUtil.toast(translate('conditions_are_not_met'));
      return false;
    }

    return true;
  }

  send(BuildContext context, store, {String? presentTime}) async {
    print(
        '4.22-------------slogId:${state.statusInfo!.id}--slogUserId:${state.statusInfo!.userId}--giftId:${state.giftInfo!.id}--giftNumber:${state.multiple}--personalId:${state.giftInfo?.personalId}');

    bool isSuccess = await YBDStatusApiHelper.sendGift(
      context,
      slogId: int.parse(state.statusInfo!.id!),
      slogUserId: int.parse(state.statusInfo!.userId!),
      giftId: state.giftInfo!.id,
      giftNumber: state.multiple,
      personalId: state.giftInfo?.personalId,
    );
    if (isSuccess) {
      if (state.giftInfo!.expireAfter == null) {
        store.state.bean.money = store.state.bean.money - state.giftInfo!.price! * state.multiple!;
        store.dispatch(YBDUpdateUserAction(store.state.bean));
      } else {
        int number = state.giftInfo!.number ?? 0;
        number -= state.multiple!;
        state.giftInfo!.number = number;
      }
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.RESPONSE_EVENT,
          method: 'send_gift', location: 'status_list_page', returnExtra: 'success'));
      // YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      //   YBDEventName.SLOG_SEND_GIFT,
      //   // location: widget.locationForAnalytics,
      //   itemName: YBDItemName.SEND_GIFT_SUCCESS,
      // ));
    }
  }
}

class YBDGiftState {
  YBDGiftPackageRecordGift? giftInfo;
  // YBDReceiverUser receiverUser;
  YBDStatusInfo? statusInfo;
  int? multiple, selectingGiftIndex, viewingTabIndex, viewingPageIndex;
  StatusSheetAction sheetAction;
  YBDGiftState(
    this.sheetAction, {
    this.viewingTabIndex,
    this.viewingPageIndex,
    this.selectingGiftIndex,
    this.giftInfo,
    this.multiple,
    this.statusInfo,
  });
  YBDGiftState.clone(YBDGiftState state)
      : this(
          state.sheetAction,
          viewingTabIndex: state.viewingTabIndex,
          viewingPageIndex: state.viewingPageIndex,
          selectingGiftIndex: state.selectingGiftIndex,
          multiple: state.multiple,
          giftInfo: state.giftInfo,
          statusInfo: state.statusInfo,
        );
}
