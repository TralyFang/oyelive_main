import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import 'bloc/status_ybd_content_bloc.dart';

class YBDFollowButton extends StatefulWidget {
  bool? isFollowed;
  bool isStatusPage;
  bool isOnTap;
  String? userId;
  String locationForAnalytics;
  Key? key;
  Function? onChange;

  YBDFollowButton(this.isFollowed, this.userId, this.locationForAnalytics,
      {Key? key, this.isStatusPage = false, this.isOnTap = true, this.onChange(bool x)?})
      : super(key: key);

  @override
  YBDFollowButtonState createState() => new YBDFollowButtonState();
}

class YBDFollowButtonState extends BaseState<YBDFollowButton> {
  bool? isFollowed;

  @override
  Widget myBuild(BuildContext context) {
    isFollowed = widget.isFollowed;
    // if (isFollowed == null) {
    //   Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context);
    //   isFollowed = (store.state.followedIds ?? []).contains(int.parse(widget.userId));
    // }
    return Container(
      key: PageStorageKey(widget.key),
      child: isFollowed!
          ? Container()
          : GestureDetector(
              onTap: () {
                if (!widget.isOnTap) {
                  return;
                }
                YBDAnalyticsUtil.logEvent(
                    YBDAnalyticsEvent(YBDEventName.CLICK_EVENT, location: widget.locationForAnalytics, itemName: "follow"));
                YBDCommonTrack().commonTrack(YBDTAProps(
                  location: YBDTAClickName.guide_follow,
                  module: YBDTAModule.guide,
                ));
                followNow();
              },
              child: Padding(
                padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
                child: Image.asset(
                  "assets/images/n_follow.png",
                  width: ScreenUtil().setWidth(40),
                ),
              ),
              // child: Container(
              //   height: ScreenUtil().setWidth(56),
              //   width: ScreenUtil().setWidth(125),
              //   decoration: BoxDecoration(
              //       gradient: LinearGradient(
              //         colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
              //         begin: Alignment.topCenter,
              //         end: Alignment.bottomCenter,
              //       ),
              //       borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30)))),
              //   alignment: Alignment.center,
              //   child: Text(
              //     translate('follow'),
              //     style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
              //   ),
              // ),
            ),
    );
  }
  void myBuildFKrMEoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  followNow() async {
    setState(() {
      widget.isFollowed = true;
      if (widget.onChange != null) {
        widget.onChange!.call(true);
      }
    });

    bool result = await ApiHelper.followUser(context, widget.userId);
    if (!result) {
      widget.isFollowed = false;
      if (widget.onChange != null) {
        widget.onChange!.call(false);
      }
      setState(() {});
      return;
    } else {
      widget.isFollowed = true;
      setState(() {});
    }

    if (widget.isStatusPage) {
      context.read<YBDStatusContentBloc>().followRefresh(widget.userId);
    }
  }

  @override
  void initState() {
    super.initState();
    isFollowed = widget.isFollowed;
  }
  void initStatebSwcboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDFollowButton oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetL7I8Goyelive(YBDFollowButton oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
