import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/room/entity/gift_ybd_package_entity.dart';

class YBDGiftSheetBloc extends Bloc<YBDGiftPackageState, YBDGiftPackageState> {
  late YBDGiftPackageState state;

  YBDGiftSheetBloc(init) : super(init) {
    state = init;
  }

  @override
  Stream<YBDGiftPackageState> mapEventToState(YBDGiftPackageState event) async* {
    // TODO: implement mapEventToState

    switch (event.sheetAction) {
      case SheetAction.Init:
        // TODO: Handle this case.
        break;
      case SheetAction.ChangeTab:
        // TODO: Handle this case.
        state!.viewingTabIndex = event.viewingTabIndex;
        state!.selectingGiftIndex = null;
        state!.giftInfo = null;
        break;
      case SheetAction.ChangePage:
        // TODO: Handle this case.
        state!.viewingPageIndex = event.viewingPageIndex;
        break;
      case SheetAction.SelectGift:
        // TODO: Handle this case.
        state!.selectingGiftIndex = event.selectingGiftIndex;
        state!.giftInfo = event.giftInfo;
        break;
      case SheetAction.ChangeMultiple:
        // TODO: Handle this case.
        state!.multiple = event.multiple;

        break;
      case SheetAction.SelectReceiver:
        // TODO: Handle this case.
        break;
    }
    state!.sheetAction = event.sheetAction;

    yield new YBDGiftPackageState.clone(state!);
  }
  void mapEventToStateTNILNoyelive(YBDGiftPackageState event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  checkCanSend(int money) {
    if (state!.selectingGiftIndex == null) {
      YBDToastUtil.toast(translate('select_gift'));
      return false;
    }
    logger.v('state.giftInfo viewingTabIndex:${state!.viewingTabIndex}');
    if (state?.viewingTabIndex == 3) {
      logger.v('Backpack gift ----not need gold coins');
      return true;
    }
    if (state?.giftInfo?.personalId != null && (state?.giftInfo?.number ?? 0) < (state!.multiple ?? 0)) {
      YBDToastUtil.toast(translate('amount_insufficient'));
      state!.giftInfo = null;
      state!.selectingGiftIndex = null;
      return false;
    } else if (state!.multiple! * state!.giftInfo!.price! > money) {
      YBDToastUtil.toast(translate('amount_insufficient'));
      return false;
    }

    return true;
  }

  var lastRoomId;
  var lastStore;
}

enum SheetAction { Init, ChangeTab, ChangePage, SelectGift, ChangeMultiple, SelectReceiver }

class YBDGiftPackageState {
  int? viewingTabIndex, viewingPageIndex, selectingGiftIndex, multiple;
  YBDGiftPackageRecordGift? giftInfo;
  SheetAction sheetAction;

  YBDGiftPackageState(
    this.sheetAction, {
    this.viewingTabIndex,
    this.viewingPageIndex,
    this.selectingGiftIndex,
    this.giftInfo,
    this.multiple,
  });

  YBDGiftPackageState.clone(YBDGiftPackageState state)
      : this(
          state.sheetAction,
          viewingTabIndex: state.viewingTabIndex,
          viewingPageIndex: state.viewingPageIndex,
          selectingGiftIndex: state.selectingGiftIndex,
          multiple: state.multiple,
          giftInfo: state.giftInfo,
        );
}
