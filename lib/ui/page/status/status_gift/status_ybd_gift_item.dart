import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../module/room/entity/gift_ybd_package_entity.dart';

class YBDStatusGiftItem extends StatefulWidget {
  bool isSelected;
  YBDGiftPackageRecordGift giftInfo;
  bool isMyGift;
  bool isStatus;
  YBDStatusGiftItem(this.isSelected, this.giftInfo, {this.isMyGift: false, this.isStatus: false});

  @override
  YBDStatusGiftItemState createState() => new YBDStatusGiftItemState();
}

class YBDStatusGiftItemState extends BaseState<YBDStatusGiftItem> {
  getLeftDays() {
    return Duration(seconds: widget.giftInfo.expireAfter!).inDays.toString();
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Container(
      decoration: BoxDecoration(
          border: widget.isSelected ? Border.all(width: ScreenUtil().setWidth(1), color: Color(0xff49C9CE)) : null),
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: ScreenUtil().setWidth(16),
              ),
              YBDNetworkImage(
                imageUrl: YBDImageUtil.gift(context, widget.giftInfo.img, "B"),
                width: ScreenUtil().setWidth(96),
                height: ScreenUtil().setWidth(96),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(12),
              ),
              Text(
                widget.giftInfo.name!,
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(20),
                    color: widget.isStatus ? Colors.black.withOpacity(0.7) : Color(0xffAFAFAF)),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(5),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset(
                    'assets/images/tiny_bean.webp',
                    width: ScreenUtil().setWidth(16),
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(6),
                  ),
                  Text(
                    widget.giftInfo.price.toString(),
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(20),
                        color: widget.isStatus ? Colors.black : Colors.white,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ],
          ),
          widget.isMyGift
              ? Padding(
                  padding: EdgeInsets.only(top: ScreenUtil().setWidth(8)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/icon_clock.png",
                        width: ScreenUtil().setWidth(12),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(4),
                      ),
                      Container(
                        width: ScreenUtil().setWidth(86),
                        child: Text(
                          '${getLeftDays()}${translate('days_left')}',
                          style: TextStyle(color: Color(0xffAFAFAF), fontSize: ScreenUtil().setSp(12)),
                        ),
                      ),
                      Container(
                        width: ScreenUtil().setWidth(48),
                        height: ScreenUtil().setWidth(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(100))),
                            gradient: LinearGradient(colors: [
                              Color(0xff47CDCC),
                              Color(0xff5E94E7),
                            ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
                        alignment: Alignment.center,
                        child: Text(
                          "x${widget.giftInfo.number}",
                          style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(14)),
                        ),
                      )
                    ],
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
  void myBuildJNBP2oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatejawSroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDStatusGiftItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetLLVk1oyelive(YBDStatusGiftItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
