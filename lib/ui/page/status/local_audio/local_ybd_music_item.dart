import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'circle_ybd_check_box.dart';

/// 本地音乐列表 item
class YBDLocalMusicListItem extends StatelessWidget {
  final bool selected;
  final String? singer;
  final String? name;
  YBDLocalMusicListItem(this.name, this.singer, {this.selected = false});

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      SizedBox(width: ScreenUtil().setWidth(30)),
      Expanded(
        child: Container(
          padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  this.name!,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: ScreenUtil().setSp(28),
                  ),
                ),
                SizedBox(height: ScreenUtil().setWidth(10)),
                Text(
                  this.singer!,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 0.7),
                    fontSize: ScreenUtil().setSp(28),
                  ),
                ),
              ]),
        ),
      ),
      YBDCircleCheckBox(selected: selected),
      SizedBox(width: ScreenUtil().setWidth(30)),
    ]);
  }
  void buildhK2qGoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
