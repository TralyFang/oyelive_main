import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

/// 本地音乐列表空状态
class YBDEmptyLocalAudio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth,
      height: ScreenUtil().setWidth(260),
      child: Column(
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/icon_no_music.webp'),
            fit: BoxFit.cover,
            width: ScreenUtil().setWidth(354),
            height: ScreenUtil().setWidth(184),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(20),
          ),
          Text(
            translate('no_music'),
            style: TextStyle(color: Color.fromRGBO(255, 255, 255, 0.85), fontSize: ScreenUtil().setSp(28)),
          ),
        ],
      ),
    );
  }
  void buildOzmMNoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
