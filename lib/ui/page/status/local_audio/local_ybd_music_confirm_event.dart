import 'dart:async';

import 'package:flutter_audio_query/flutter_audio_query.dart';

/// 确认选择本地音乐的事件
class YBDLocalMusicConfirmEvent {
  SongInfo musicEntity;
  YBDLocalMusicConfirmEvent(this.musicEntity);
}
