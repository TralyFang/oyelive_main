import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_inbox_track.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';

/// 顶部导航栏
class YBDTopNavigateBar extends StatelessWidget {
  final String? title;
  YBDTopNavigateBar({this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
      height: ScreenUtil().setWidth(94),
      width: ScreenUtil().screenWidth,
      child: Row(children: [
        Container(
          width: ScreenUtil().setWidth(120),
          height: ScreenUtil().setWidth(80),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                logger.v('clicked back button');
                YBDInboxTrack().backToInbox();
                YBDNavigatorHelper.backAction(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
          ),
        ),
        SizedBox(
          width: ScreenUtil().screenWidth - ScreenUtil().setWidth(200),
          child: Center(
            child: Text(
              title!,
              style: TextStyle(
                color: Colors.white,
                fontSize: YBDTPStyle.spNav,
              ),
            ),
          ),
        ),
      ]),
    );
  }
  void buildW0sDPoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
