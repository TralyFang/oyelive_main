import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 圆形勾选框
class YBDCircleCheckBox extends StatelessWidget {
  /// 是否选中
  final bool selected;

  /// 选中时的颜色
  final Color selectedColor;

  /// 未选中时的颜色
  final Color unselectedColor;

  /// 选中时的边框颜色
  final Color selectedBorderColor;

  /// 未选中时的边框颜色
  final Color unselectedBorderColor;

  YBDCircleCheckBox({
    this.selected = false,
    this.selectedColor = Colors.white,
    this.unselectedColor = Colors.transparent,
    this.selectedBorderColor = Colors.white,
    this.unselectedBorderColor = Colors.white,
  });
  @override
  Widget build(BuildContext context) {
    Color borderColor;
    Color centerColor;

    if (selected) {
      centerColor = selectedColor;
      borderColor = selectedBorderColor;
    } else {
      centerColor = unselectedColor;
      borderColor = unselectedBorderColor;
    }

    return Container(
      width: ScreenUtil().setWidth(34),
      height: ScreenUtil().setWidth(34),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(17))),
        border: Border.all(color: borderColor, width: ScreenUtil().setWidth(1)),
      ),
      child: Container(
        margin: EdgeInsets.all(4),
        decoration: BoxDecoration(
          color: centerColor,
          shape: BoxShape.circle,
        ),
      ),
    );
  }
  void buildAUXa3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
