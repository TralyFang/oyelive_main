import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 蓝色渐变按钮
class YBDBlueGradientButton extends StatelessWidget {
  final String? title;
  final int? height;
  final int? width;
  // 是否上下渐变
  final bool vDirection;
  // 是否颜色反转
  final bool reverse;

  YBDBlueGradientButton({this.title, this.height, this.width, this.vDirection = false, this.reverse = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            width: ScreenUtil().setWidth(height == null ? 441 : width!),
            height: ScreenUtil().setWidth(width == null ? 74 : height!),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: reverse ? [Color(0xff47CDCC), Color(0xff5E94E7)] : [Color(0xff5E94E7), Color(0xff47CDCC)],
                  begin: vDirection ? Alignment.topCenter : Alignment.centerLeft,
                  end: vDirection ? Alignment.bottomCenter : Alignment.centerRight),
              borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(37)))),
            ),
            child: Text(
              // TODO: 翻译
              title!,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: ScreenUtil().setSp(28),
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
  void buildiIfgvoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
