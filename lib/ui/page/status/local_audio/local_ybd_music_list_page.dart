import 'dart:async';

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_query/flutter_audio_query.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../main.dart';
import '../../room/music_player/local_ybd_music_info.dart';
import 'blue_ybd_gradient_button.dart';
import 'empty_ybd_local_audio.dart';
import 'local_ybd_music_confirm_event.dart';
import 'local_ybd_music_item.dart';
import 'scale_ybd_animate_button.dart';
import 'top_ybd_navigate_bar.dart';

/// 本地音乐列表页
class YBDLocalMusicListPage extends StatefulWidget {
  YBDLocalMusicListPage();

  @override
  YBDLocalMusicListPageState createState() => YBDLocalMusicListPageState();
}

class YBDLocalMusicListPageState extends BaseState<YBDLocalMusicListPage> {
  /// 数据源
  List<dynamic> _dataSource = [];

  List<SongInfo> songList = [];

  /// 被选中的歌曲索引
  int selectedIndex = -1;

  /// create a FlutterAudioQuery instance.
  final FlutterAudioQuery audioQuery = FlutterAudioQuery();

  /// 添加滚动条
  ScrollController _scrollController = ScrollController();

  /// 标记初始状态
  bool _isInitState = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      await _scanLocalMusic();
    });
  }
  void initStateJgPVkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<void> _scanLocalMusic() async {
    showLockDialog();

    /// getting all albums available on device storage
    List<AlbumInfo> albumList = await audioQuery.getAlbums();

    /// getting all songs available on device storage
    List<SongInfo> songs = await audioQuery.getSongs();
    songs.retainWhere((element) => element.displayName!.contains('.mp3'));
    songList = songs;
    // 歌曲文件添加日期
    Set<String> dates = songs.map((element) {
      final stat = FileStat.statSync(element.filePath!);
      return YBDDateUtil.formatStringWithDate(stat.modified);
    }).toSet();

    // 按最近修改日期排序
    List<String> sortDates = dates.toList();
    sortDates.sort((a, b) => b.compareTo(a));

    // 歌曲文件按修改日期分组
    List<dynamic> items = [];
    for (int i = 0; i < sortDates.length; i++) {
      List<SongInfo> sameDateSongs = songs.where((element) {
        final stat = FileStat.statSync(element.filePath!);
        String dateStr = YBDDateUtil.formatStringWithDate(stat.modified);
        return dateStr == sortDates[i];
      }).toList();

      items.add(sortDates[i]);
      items.addAll(sameDateSongs);
    }

    // 把文件时间添加到列表数据源中
    _dataSource.clear();
    _dataSource.addAll(items);

    _isInitState = false;
    setState(() {});
    dismissLockDialog();
  }
  void _scanLocalMusicj0C7hoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: Stack(children: <Widget>[
          // 音乐或添加音乐按钮
          !_isInitState && isLocalMusicEmpty() ? emptyMusicList() : musicListView(),
          Positioned(
            // 底部扫描本地音乐按钮
            bottom: ScreenUtil().setWidth(120),
            child: YBDScaleAnimateButton(
              child: YBDBlueGradientButton(
                  title: isSelectedLocalMusic() ? translate('ok') : translate('scan_local_music')),
              onTap: () async {
                if (isSelectedLocalMusic()) {
                  logger.v('confirm selected music');
                  eventBus.fire(YBDLocalMusicConfirmEvent(_dataSource[selectedIndex]));
                  eventBus.fire(YBDLocalMusicInfo(songList, _dataSource[selectedIndex]));

                  YBDNavigatorHelper.backAction(context);
                } else {
                  logger.v('scan local music');
                  await _scanLocalMusic();
                }
              },
            ),
          ),
        ]),
      ),
    );
  }
  void myBuildTRXFMoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 音乐列表
  Widget musicListView() {
    return Container(
      child: Column(children: <Widget>[
        YBDTopNavigateBar(title: 'Local music'),
        Container(
          width: ScreenUtil().screenWidth,
          height: ScreenUtil().screenHeight - ScreenUtil().setWidth(400),
          child: Scrollbar(
            isAlwaysShown: true,
            controller: _scrollController,
            child: ListView.separated(
              padding: EdgeInsets.symmetric(vertical: 0),
              controller: _scrollController,
              itemBuilder: (_, index) {
                var item = _dataSource[index];

                if (item is SongInfo) {
                  // 歌曲信息 item
                  return Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        logger.v('clicked music item index : $index');
                        setState(() {
                          selectedIndex = index;
                        });
                      },
                      child: Container(
                        width: ScreenUtil().screenWidth,
                        height: ScreenUtil().setWidth(134),
                        child: YBDLocalMusicListItem(item.displayName, item.artist, selected: index == selectedIndex),
                      ),
                    ),
                  );
                } else {
                  // 日期 item
                  String today = DateFormat("yyyy-MM-dd").format(DateTime.now());
                  return Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(30),
                      vertical: ScreenUtil().setWidth(30),
                    ),
                    child: Text(
                      item == today ? 'Today' : item,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: ScreenUtil().setSp(34),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  );
                }
              },
              separatorBuilder: (_, index) {
                return Container(
                  height: ScreenUtil().setWidth(1),
                  color: Const.COLOR_BORDER,
                );
              },
              itemCount: _dataSource.length,
            ),
          ),
        ),
      ]),
    );
  }
  void musicListViewXu7P5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 音乐列表缺省图
  Widget emptyMusicList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        // 导航栏
        YBDTopNavigateBar(title: translate('no_local_music')),
        Expanded(child: SizedBox(height: 20)),
        YBDEmptyLocalAudio(),
        Expanded(child: SizedBox(height: 20)),
      ],
    );
  }
  void emptyMusicListh8NJ7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是否有本地音乐
  bool isLocalMusicEmpty() {
    logger.v('local music data source is empty : ${_dataSource.isEmpty}');
    return _dataSource.isEmpty;
  }

  /// 是否已经选中了本地音乐
  bool isSelectedLocalMusic() {
    if (selectedIndex != -1) {
      logger.v('selected local music : ${_dataSource[selectedIndex].artist}');
      return true;
    } else {
      return false;
    }
  }
}
