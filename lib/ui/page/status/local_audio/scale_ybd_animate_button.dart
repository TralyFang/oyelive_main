import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

/// 点击带缩小动画的按钮
class YBDScaleAnimateButton extends StatefulWidget {
  final Widget? child;
  final VoidCallback? onTap;
  // 扩展点击事件
  final EdgeInsets? extraHitTestArea;
  YBDScaleAnimateButton({this.child, this.onTap, this.extraHitTestArea});

  @override
  YBDScaleAnimateButtonState createState() => new YBDScaleAnimateButtonState();
}

class YBDScaleAnimateButtonState extends State<YBDScaleAnimateButton> with SingleTickerProviderStateMixin {
  double? _scale;
  late AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 200,
      ),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
    super.initState();
  }
  void initStateKrAc9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  void disposeHPxHSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    _scale = 1 - _controller.value;

    return YBDDelayGestureDetector(
      onTapDown: _onTapDown,
      onTapCancel: _onTapCancel,
      onTapUp: _onTapUp,
      onTap: widget.onTap,
      extraHitTestArea: widget.extraHitTestArea,
      child: Transform.scale(
        scale: _scale,
        child: widget.child,
      ),
    );
  }
  void buildjfax2oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _onTapDown(TapDownDetails details) {
    _controller.forward();
  }

  void _onTapUp(TapUpDetails details) {
    _controller.reverse();
  }

  void _onTapCancel() {
    _controller.reverse();
  }
}
