import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_query/flutter_audio_query.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../common/util/log_ybd_util.dart';
import 'scale_ybd_animate_button.dart';

/// 要上传的音乐 item
class YBDUploadMusicItem extends StatefulWidget {
  final SongInfo? musicItem;
  final VoidCallback changeCallback;

  YBDUploadMusicItem(this.musicItem, this.changeCallback);

  @override
  YBDUploadMusicItemState createState() => new YBDUploadMusicItemState();
}

class YBDUploadMusicItemState extends State<YBDUploadMusicItem> with SingleTickerProviderStateMixin {
  final AudioPlayer audioPlayer = AudioPlayer();
  bool isPlaying = false;
  late AnimationController controller;

  @override
  void initState() {
    super.initState();

    // 控制碟片旋转速度
    controller = AnimationController(duration: const Duration(seconds: 5), vsync: this);

    // 可以多次旋转
    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        logger.v('rotation completed, repeat...');
        controller.repeat();
      }
    });

    audioPlayer.onPlayerCompletion.listen((event) {
      isPlaying = false;
      controller.stop();
      setState(() {});
    });
  }
  void initState0KyRpoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    audioPlayer.pause();
    audioPlayer.dispose();
    controller.dispose();
    super.dispose();
  }
  void dispose1mQ3Zoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(161),
      width: ScreenUtil().screenWidth,
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(25))),
      ),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            height: ScreenUtil().setWidth(161),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(12))),
              color: Color.fromRGBO(255, 255, 255, 0.1),
            ),
          ),
          Row(
            children: [
              SizedBox(width: ScreenUtil().setWidth(30)),
              Container(
                width: ScreenUtil().setWidth(96),
                height: ScreenUtil().setWidth(96),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      logger.v('play local music');
                      playLocal();
                    },
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        RotationTransition(turns: controller, child: Image.asset('assets/images/icon_dics.webp')),
                        Container(
                          width: ScreenUtil().setWidth(48),
                          decoration: BoxDecoration(color: Color(0xff4EBCD4), shape: BoxShape.circle),
                        ),
                        isPlaying
                            ?
                            // 正在播放显示暂停按钮
                            Icon(Icons.pause, color: Colors.white)
                            :
                            // 没有播放时显示播放按钮
                            Icon(Icons.play_arrow, color: Colors.white),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(40),
              ),
              Container(
                height: ScreenUtil().setWidth(96),
                width: ScreenUtil().screenWidth - ScreenUtil().setWidth(440),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.musicItem!.displayName!,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontSize: ScreenUtil().setSp(30), color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    Spacer(),
                    Text(
                      "Singer : ${widget.musicItem!.artist}    ${milliSecondsFormat(widget.musicItem!.duration!)}",
                      maxLines: 2,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(24), color: Color(0xccffffff), fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
              ),
              Spacer(),
              YBDScaleAnimateButton(
                onTap: () {
                  widget.changeCallback();
                },
                child: Container(
                  width: ScreenUtil().setWidth(140),
                  height: ScreenUtil().setWidth(56),
                  decoration: BoxDecoration(
                    color: Color(0xff5694FA),
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    translate('change'),
                    style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                  ),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(30)),
            ],
          ),
        ],
      ),
    );
  }
  void buildtyylxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 微秒格式化显示
  String milliSecondsFormat(String milliSec) {
    logger.v('mill seconds : $milliSec');
    return _printDuration(Duration(milliseconds: int.parse(milliSec)));
  }

  String _printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    logger.v('time : ${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds');
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }
  void _printDurationLlvlzoyelive(Duration duration) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 播放本地音乐
  playLocal() async {
    logger.v('play local music');
    if (audioPlayer.state == PlayerState.PLAYING) {
      logger.v('stop play');
      isPlaying = false;
      await audioPlayer.pause();
    } else {
      logger.v('start play');
      isPlaying = true;
      await audioPlayer.play(widget.musicItem!.filePath!, isLocal: true);
    }

    if (isPlaying) {
      logger.v('rotation forward');
      controller.forward();
    } else {
      logger.v('rotation stop');
      controller.stop();
    }

    setState(() {});
  }
}
