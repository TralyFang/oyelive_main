import 'dart:async';

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_audio_query/flutter_audio_query.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../common/util/upload_ybd_s3_util.dart';
import '../../../../main.dart';
import '../../../../module/status/status_ybd_api_helper.dart';
import 'blue_ybd_gradient_button.dart';
import 'local_ybd_music_confirm_event.dart';
import 'scale_ybd_animate_button.dart';
import 'top_ybd_navigate_bar.dart';
import 'upload_ybd_music_item.dart';

/// 上传本地音乐页面
class YBDUploadMusicPage extends StatefulWidget {
  @override
  YBDUploadMusicPageState createState() => new YBDUploadMusicPageState();
}

class YBDUploadMusicPageState extends BaseState<YBDUploadMusicPage> {
  TextEditingController _songNameController = new TextEditingController();
  TextEditingController? _singerController;
  SongInfo? selectedMusic;

  @override
  void initState() {
    super.initState();
    _songNameController = TextEditingController();
    _singerController = TextEditingController();
    eventBus.on<YBDLocalMusicConfirmEvent>().listen((YBDLocalMusicConfirmEvent event) {
      logger.v('received music confirm event');

      if (event.musicEntity.displayName!.isNotEmpty) {
        _songNameController.text = event.musicEntity.title!;
      }

      if (event.musicEntity.artist!.isNotEmpty) {
        _singerController!.text = event.musicEntity.artist!;
      }

      setState(() {
        selectedMusic = event.musicEntity;
      });
    });
  }
  void initStateZfAp1oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _songNameController.dispose();
    _singerController!.dispose();
    super.dispose();
  }
  void disposehu25aoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Material(
      child: Stack(
        children: <Widget>[
          Container(
            decoration: YBDTPStyle.gradientDecoration,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // 导航栏
                topNavBar(),
                // TODO: 翻译
                inputRow('Song Name :', 'Please enter the song name', _songNameController),
                SizedBox(height: ScreenUtil().setWidth(20)),
                // TODO: 翻译
                inputRow('Singer :', "Please enter the singer's name", _singerController),
                SizedBox(height: ScreenUtil().setWidth(20)),
                SizedBox(height: ScreenUtil().setWidth((null != selectedMusic) ? 0 : 197)),
                (null != selectedMusic)
                    ?
                    // 选中准备上传的音乐 item
                    YBDUploadMusicItem(selectedMusic, () {
                        logger.v('change music to upload');
                        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.scan_local_music);
                      })
                    :
                    // 添加音乐的圆形按钮
                    YBDScaleAnimateButton(
                        child: selectMusicButtonRow(),
                        onTap: () {
                          logger.v('clicked select music to upload');
                          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.scan_local_music);
                        },
                      ),
                SizedBox(height: ScreenUtil().setWidth((null != selectedMusic) ? 0 : 40)),
                // 提示文字
                (null != selectedMusic) ? Container() : selectMusicText(),
              ],
            ),
          ),
          Positioned(
            bottom: ScreenUtil().setWidth(120),
            child: YBDScaleAnimateButton(
              // 提交按钮
              child: YBDBlueGradientButton(title: 'Submit'),
              onTap: () {
                logger.v('clicked submit button');

                if (null == selectedMusic) {
                  YBDToastUtil.toast('Please select music to upload');
                } else {
                  uploadSelectedMusic();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
  void myBuild1Ycuvoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 上传选择的本地音乐
  uploadSelectedMusic() async {
    showLockDialog();
    String? s3Path = await YBDUploadS3Util.uploadFile(File(selectedMusic!.filePath!));

    if (null != s3Path && s3Path.isNotEmpty) {
      logger.v('upload local music to S3 success : $s3Path');
      YBDStatusApiHelper.uploadBgMusic(
        context,
        _songNameController.text,
        _singerController!.text,
        s3Path,
        int.parse(selectedMusic!.duration!) / 1000,
      ).then((result) {
        if (result?.code == Const.HTTP_SUCCESS_NEW) {
          logger.v("upload local music success");
          // TODO: 翻译
          YBDToastUtil.toast('Success');

          if (mounted) {
            logger.v("pop out upload music page");
            // 退出当前页面
            YBDNavigatorHelper.backAction(context);
          } else {
            logger.v("upload music page not mounted");
          }
        } else {
          logger.v("upload music failed");
          YBDToastUtil.toast(translate('failed'));
        }
        // 隐藏加载框
        dismissLockDialog();
      });
    } else {
      // 隐藏加载框
      dismissLockDialog();
      YBDToastUtil.toast(translate('failed'));
      logger.v('upload local music to S3 failed');
    }
  }

  /// 提示文字
  selectMusicText() {
    return Container(
      child: Text(
        translate('select_music'),
        style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setWidth(28)),
      ),
    );
  }

  /// 扫描本地音乐按钮栏
  selectMusicButtonRow() {
    return Stack(
      children: <Widget>[
        Container(
          width: ScreenUtil().setWidth(190),
          height: ScreenUtil().setWidth(190),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(95))),
            color: Color.fromRGBO(255, 255, 255, 0.1),
          ),
          child: Icon(
            Icons.add,
            color: Colors.white,
            size: ScreenUtil().setWidth(67),
          ),
        )
      ],
    );
  }

  /// 输入栏
  inputRow(String title, String placeholder, TextEditingController? textController) {
    return Container(
      height: ScreenUtil().setWidth(96),
      width: ScreenUtil().screenWidth,
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(25))),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            height: ScreenUtil().setWidth(96),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(12))),
              color: Color.fromRGBO(255, 255, 255, 0.1),
            ),
          ),
          Row(
            children: [
              SizedBox(
                width: ScreenUtil().setWidth(40),
              ),
              Text(
                '$title',
                style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(16),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: ScreenUtil().setWidth(2)),
                  width: ScreenUtil().setWidth(380),
                  height: ScreenUtil().setWidth(96),
                  child: TextField(
                    maxLines: 1,
                    controller: textController,
                    cursorColor: Color(0xffE422C7),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: ScreenUtil().setSp(28),
                      fontWeight: FontWeight.normal,
                    ),
                    decoration: InputDecoration(
                      hintStyle: TextStyle(
                        color: Color(0x63ffffff),
                        fontWeight: FontWeight.normal,
                      ),
                      hintText: placeholder,
                      border: InputBorder.none,
                    ),
                    onChanged: (text) {
                      logger.v('input text : $text');
                    },
                  ),
                ),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(30),
              ),
            ],
          ),
        ],
      ),
    );
  }

  /// 导航栏
  Widget topNavBar() {
    // TODO: 翻译
    return YBDTopNavigateBar(title: 'Upload Music');
  }
  void topNavBarFQZC9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
