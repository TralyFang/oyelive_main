import 'dart:async';


/*
 * @Author: William-Zhou
 * @Date: 2021-12-15 09:36:25
 * @LastEditTime: 2021-12-17 11:33:33
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_christmas.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_new_year.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_file.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_skin.dart';
import 'package:oyelive_main/ui/page/activty/constants/act_ybd_consts.dart';
import 'package:oyelive_main/ui/page/activty/entity/activity_ybd_skin_entity.dart';

import 'entity/activity_ybd_skin_entity.dart';

class YBDActivitySkinRoot {
  /// 活动配置信息
  static YBDActivitySkinEntity? activityEntity;

  /// 是否显示皮肤
  bool showSkin() {
    // print('showSkin==== : ${YBDActivitySkin.activityEntity?.toJson()}');
    // return true;
    return YBDActivityFile.instance!.canShowSkin;
  }
  void showSkinoiqWPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 当前皮肤模板
  YBDActivitySkinRoot curAct() {
    switch (YBDActivityFile.CURRENT_TEMPLATE) {
      case 'eid_al_adha_2021':
        return YBDActivitySkin();
        break;
      case 'Christmas_2021':
        return YBDActivityChristmas();
        break;
      case 'new_year_2022':
        return YBDActivityNewYear();
        break;
      default:
        return YBDDefaultSkin();
        break;
    }
  }

  getDecorationImage(String path) {
    if (path.contains(ActImagePrefix)) {
      return FileImage(File(path));
    }
    return AssetImage(path);
  }

  /// 个人中心vip图标地址
  String myProfileVip() {
    return 'assets/images/buy_vip.png';
  }
  void myProfileVipyOgNooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 个人中心Top-up图标地址
  String myProfileTopUp() {
    return 'assets/images/topup/y_top_up.png';
  }

  /// 个人中心Invite Friends图标地址
  String myProfileInvite() {
    return 'assets/images/invite.png';
  }
  void myProfileInvitepcVd5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 个人中心Daily Tasks图标地址
  String myProfileDailyTask() {
    return 'assets/images/dc/daily_task.png';
  }

  /// 个人中心Store图标地址
  String myProfileStore() {
    return 'assets/images/store.png';
  }

  /// 个人中心Baggage图标地址
  String myProfileBaggage() {
    return 'assets/images/profile/baggage.png';
  }
  void myProfileBaggageIT5m3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 个人中心Badges图标地址
  String myProfileBadges() {
    return 'assets/images/profile/badges.webp';
  }

  /// 个人中心Level图标地址
  String myProfileLevel() {
    return 'assets/images/level.png';
  }

  /// 个人中心Guardian图标地址
  String myProfileGuardian() {
    return 'assets/images/guardian.png';
  }

  /// 个人中心Support图标地址
  String myProfileSupport() {
    return 'assets/images/icon_support.png';
  }
  void myProfileSupportfXg4Uoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 个人中心Talent图标地址
  String myProfileTalent() {
    return 'assets/images/talent.png';
  }

  /// 个人中心pk record图标地址
  String myProfilePkRecord() {
    return 'assets/images/icon_pk.png';
  }

  ///首页board图标 combo,Team,Leaderboard
  List<String> homeBoard() {
    return [
      'assets/images/default/popular_default.webp',
      'assets/images/default/popular_default.webp',
      'assets/images/default/popular_default.webp',
    ];
  }
  void homeBoardFgqnHoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 个人中心的顶部背景图
  String myprofileTopBg() {
    return 'assets/images/bg_profile.webp';
  }

  /// 个人中心九宫额背景颜色
  BoxDecoration myprofileIconListBgDecoration() {
    return YBDTPStyle.gradientDecoration;
  }

  /// 个人中心豆子，勋章，等级的背景颜色
  Color myprofileRowColor() {
    return Color(0xff7332B7);
  }

  /// 个人中心slog列表标签的背景颜色
  Color myprofileSlogBgColor() {
    return Color(0xff4A00A2);
  }

  /// 房间列表人数文字的颜色
  Color roomListItemTextColor() {
    return Colors.white.withOpacity(0.6);
  }

  /// 底部tab动态的开播图标
  String tabStartLive() {
    return 'assets/images/home_add.webp';
  }
  void tabStartLiveQv2UUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// popular banner的背景图
  Widget popularBannerBg() {
    return SizedBox();
  }

  /// popular 导航栏颜色
  Color popularNavTitleColor() {
    return Colors.white;
  }

  /// 活动分割线颜色
  Color activitySeparatorColor() {
    return Colors.white.withOpacity(0.1);
  }

  /// 活动文字颜色 使用skinTextColor字段
  Color activityTextColor() {
    return Colors.white;
  }

  /// 活动文字颜色
  Color exploreRecordBgColor() {
    return Colors.white.withOpacity(0.2);
  }

  /// 背景颜色 使用skinHomeBgColor字段
  BoxDecoration activityBgDecoration() {
    return BoxDecoration(
      gradient: LinearGradient(
        colors: YBDTPStyle.colorList,
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
  }

  /// other profile
  String userProfileBg() {
    return 'assets/images/bg_profile.webp';
  }
  void userProfileBgNFaq8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// other profile
  String userProfileCopy() {
    return 'assets/images/icon_copy.webp';
  }

  /// other profile
  String userProfileLocation() {
    return 'assets/images/location.png';
  }

  /// tab
  List<String> getTabDefaultIcon() {
    return [
      "assets/images/home_default.webp",
      "assets/images/inbox/inbox_default.webp",
      "assets/images/user_default.webp",
    ];
  }

  /// tab
  List<String> getTabIcon() {
    return [
      "assets/images/switch/switch_home.gif",
      "assets/images/switch/switch_inbox.gif",
      "assets/images/switch/switch_user.gif"
    ];
  }

  /// 下拉刷新 header
  ClassicHeader activityRefreshHeader() {
    return ClassicHeader(
      textStyle: TextStyle(
        color: Colors.white.withOpacity(0.6),
      ),
    );
  }

  /// 上拉刷新 footer
  ClassicFooter activityRefreshFooter() {
    return ClassicFooter(
      textStyle: TextStyle(
        color: Colors.white.withOpacity(0.6),
      ),
    );
  }

  List<Color> userProfileColors() {
    return YBDTPStyle.colorList;
  }
  void userProfileColorsLMltooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///other profile color
  Color userProfileFontColor() {
    return Colors.white;
  }

  ///other profile color
  Color userProfileNameColor(BuildContext context) {
    return Theme.of(context).primaryColor;
  }

  ///other profile color
  Color userProfileIDColor(double a) {
    return Color(0x99ffffff);
  }

  ///other profile color
  Color indexBgColor() {
    return Colors.transparent;
  }

  ///other profile 炫浮按钮
  double? borderW() {
    return 0;
  }

  String profileBg(bool isMe) {
    return isMe ? myprofileTopBg() : userProfileBg();
  }
}

class YBDDefaultSkin extends YBDActivitySkinRoot {}
