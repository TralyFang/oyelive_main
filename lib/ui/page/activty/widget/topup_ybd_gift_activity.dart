import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';

import '../../../../common/navigator/navigator_ybd_helper.dart';

/// 充值送礼活动弹框
class YBDTopUpGiftActivity extends StatelessWidget {
  /// 活动地址
  final String gotoAddress;

  final BuildContext context;

  YBDTopUpGiftActivity(this.context, this.gotoAddress);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked topup gift activity dialog bg");
        // 点空白处隐藏弹框
        Navigator.pop(context);
      },
      child: Material(
        color: Colors.black.withOpacity(0.2),
        child: Container(
          width: ScreenUtil().screenWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: ScreenUtil().setWidth(100)),
              _contentView(context),
            ],
          ),
        ),
      ),
    );
  }
  void buildtCSh2oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 对话框内容
  Widget _contentView(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // 闭免点击图片时关闭弹框
      },
      child: Container(
        child: Column(
          children: <Widget>[
            // 图片
            _bgImage(),
            // go 按钮
            _goBtn(),
          ],
        ),
      ),
    );
  }
  void _contentViewAjlvioyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 背景图
  Widget _bgImage() {
    return Container(
      width: ScreenUtil().setWidth(537),
      child: Image.asset(
        'assets/images/topup/y_topup_gift_bg.png',
        fit: BoxFit.cover,
      ),
    );
  }
  void _bgImageNm7Gfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 跳转按钮
  Widget _goBtn() {
    return YBDDelayGestureDetector(
      onTap: () {
        logger.v('clicked activity dialog go btn');

        Navigator.pop(context);
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.HOME_PAGE,
          itemName: YBDItemName.TOPUP_GIFT_ACTIVITY,
        ));

        logger.v('topup gift location open address : $gotoAddress');
        // 跳转到网页和 app 页面
        YBDNavigatorHelper.openUrl(context, gotoAddress, title: 'Lucky package');
      },
      child: Container(
        width: ScreenUtil().setWidth(258),
        child: Image.asset('assets/images/topup/y_topup_gift_go.webp', fit: BoxFit.cover),
      ),
    );
  }
  void _goBtnOKXfBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
