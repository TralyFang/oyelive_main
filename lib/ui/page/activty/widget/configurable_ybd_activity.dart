import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_activity_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/short_ybd_route.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../entity/configurable_ybd_activity_entity.dart';

/// 管理后台配置的活动弹框
class YBDConfigurableDialog extends StatefulWidget {
  /// 活动列表
  final List<YBDConfigurableActivityRecord?>? recordList;

  const YBDConfigurableDialog({
    Key? key,
    required this.recordList,
  }) : super(key: key);

  @override
  _YBDConfigurableDialogState createState() => _YBDConfigurableDialogState();
}

class _YBDConfigurableDialogState extends State<YBDConfigurableDialog> {
  final _imgWidth = ScreenUtil().setWidth(500);
  final _imgHeight = ScreenUtil().setWidth(760);

  @override
  void initState() {
    super.initState();
  }
  void initStatedmETloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposeDGiFnoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the background activity dialog bg");
        // Navigator.pop(context);
      },
      child: Material(
        color: Colors.black.withOpacity(0.2),
        child: Container(
          width: ScreenUtil().screenWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                // color: Colors.green,
                // 活动图片滑动组件
                height: _imgHeight,
                width: _imgWidth,
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(
                      // 活动图片滑动组件
                      height: _imgHeight,
                      width: _imgWidth,
                      child: _bgImageSwiper(),
                    ),
                    Positioned(
                      // 关闭按钮
                      top: ScreenUtil().setWidth(50),
                      right: ScreenUtil().setWidth(10),
                      child: _closeBtn(context),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildYJMnGoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 背景图
  Widget _bgImageSwiper() {
    if (null == widget.recordList || widget.recordList!.isEmpty) {
      return SizedBox();
    }

    if (widget.recordList!.length == 1) {
      // 只有一张图片时屏蔽滑动效果
      return _bgImg(widget.recordList!.first!);
    }

    return Swiper(
        // 滑动预览
        scrollDirection: Axis.horizontal,
        itemCount: widget.recordList!.length,
        autoplay: false,
        outer: false,
        index: 0,
        pagination: SwiperPagination(
          alignment: Alignment.bottomCenter,
          builder: DotSwiperPaginationBuilder(
            // 当前图片圆点
            size: ScreenUtil().setWidth(10),
            activeSize: ScreenUtil().setWidth(10),
            color: Colors.white,
            activeColor: YBDTPStyle.heliotrope,
          ),
        ),
        itemBuilder: (BuildContext context, int index) {
          final record = widget.recordList![index]!;

          // 单张活动背景图片
          return _bgImg(record);
        },
        onIndexChanged: (int index) {
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.HOME_POPULAR_PAGE,
            itemName: YBDItemName.EVENT_POPUP_SWIPE,
          ));
        });
  }
  void _bgImageSwiper1i67goyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 单张活动背景图片
  Widget _bgImg(YBDConfigurableActivityRecord record) {
    // 显示原图，后台上传时限制大小
    String imgUrl = YBDImageUtil.configActivityBg(context, record.img, null);
    logger.v('YBDConfigurableActivityRecord: $imgUrl');
    if (null == record.img || record.img!.isEmpty) {
      return SizedBox();
    }

    return GestureDetector(
      onTap: () {
        logger.v("clicked the background activity bg img");

        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.HOME_PAGE,
          itemName: record.title,
        ));
        // 进入活动埋点
        YBDCommonTrack().commonTrack(YBDTAProps(location: 'events', module: YBDTAModule.home));
        YBDActivityInTrack().activityIn(YBDTAProps(location: YBDTAEventLocation.popular_alert_view, url: record.link));
        if (record.link!.startsWith('http')) {
          YBDNavigatorHelper.navigateToWeb(
            context,
            "?url=${Uri.encodeComponent(record.link!)}&title=${record.title ?? ''}&entranceType=${WebEntranceType.AlertView.index}",
          );
        } else {
          YBDNavigatorHelper.navigateToWeb(context, record.link!);
        }
      },
      child: Container(
        height: _imgHeight,
        width: _imgWidth,
        child: YBDNetworkImage(
          imageUrl: imgUrl,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
  void _bgImg52B2doyelive(YBDConfigurableActivityRecord record) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关闭按钮
  Widget _closeBtn(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v('clicked close btn');
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.HOME_POPULAR_PAGE,
          itemName: YBDItemName.EVENT_POPUP_CLOSE,
        ));
        // 埋点
        YBDObsUtil.instance().pop(currentRoute: YBDShortRoute.home, previousRoute: YBDShortRoute.activity_pop);
        Navigator.pop(context);
      },
      child: Image.asset(
        'assets/images/dc/daily_check_close_btn.png',
        width: ScreenUtil().setWidth(60),
        color: Colors.white,
        fit: BoxFit.cover,
      ),
    );
  }
  void _closeBtnsl9Ntoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
