import 'dart:async';


import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/date_ybd_util.dart';
import 'package:oyelive_main/common/util/download_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/zip_ybd_util.dart';
import 'package:oyelive_main/ui/page/activty/entity/activity_ybd_skin_entity.dart';

/// zip中只包含图片文件，不包含文件夹
class YBDActivityFile {
  /// 当前版本支持的皮肤模版
  static String CURRENT_TEMPLATE = 'Christmas_2021';

  /// 获取皮肤根路径报错时用的默认路径
  static String NULL_PATH = 'null';
  static YBDActivityFile? get instance => _getInstance();
  static YBDActivityFile? _instance;

  static YBDActivityFile? _getInstance() {
    if (_instance == null) {
      _instance = YBDActivityFile._();
    }
    return _instance;
  }

  /// 私有构造函数
  YBDActivityFile._() {
    //
  }

  /// 皮肤根目录
  String? _baseDir;

  /// 活动配置信息
  YBDActivitySkinEntity? _configEntity = YBDActivitySkinEntity();

  /// zip下载任务id
  String? _taskId = '';

  /// 解压中
  bool _unzipping = false;

  /// 订阅zip下载事件
  late StreamSubscription<YBDDownloadTaskInfo> _downloadSubscription;

  /// 是否显示皮肤
  bool canShowSkin = false;

  /// 设置是否可以显示皮肤
  /// 获取配置项后在[YBDIndexPageState]调用
  void setCanShowSkin() {
    canShowSkin = isInPeriod() && folderExist() && supportTemplate();
    logger.i('can show skin result: $canShowSkin');

    if (canShowSkin) {
      // 添加显示皮肤的埋点
      YBDAnalyticsUtil.logEvent(
        YBDAnalyticsEvent(
          YBDEventName.OPEN_PAGE,
          itemName: _configEntity!.folderName,
          location: YBDLocationName.SKIN_PAGE,
        ),
      );
    }
  }
  void setCanShowSkin4JfUSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 设置皮肤根路径
  /// app启动时调用
  Future<void> setBasePath() async {
    try {
      _baseDir = (await getApplicationDocumentsDirectory()).path;
    } catch (e) {
      logger.e('get skin base dir error: $e');
      YBDCommonUtil.storeSkinError('get skin base dir error: $e');
    }

    _baseDir ??= NULL_PATH;

    logger.v('set skin base dir: $_baseDir');
    if (_baseDir!.isEmpty || _baseDir == NULL_PATH) {
      logger.v('skin base dir is empty: $_baseDir');
      YBDCommonUtil.storeSkinError('skin base dir is empty: $_baseDir');
    }
  }
  void setBasePathPe0NToyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 设置皮肤配置项
  /// 获取到配置项后调用
  void setConfigEntity(YBDActivitySkinEntity source) {
    if (null == source) {
      logger.w('skin config is null');
      YBDCommonUtil.storeSkinError('skin config is null');
    }

    logger.v('set config entity: ${source.toJson()}');
    _configEntity = YBDActivitySkinEntity.copyWith(source);
  }
  void setConfigEntityP15nhoyelive(YBDActivitySkinEntity source) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 从皮肤文件夹获取图片
  /// [fileName] 图片名称
  /// 返回图片文件
  File getFile(String fileName) {
    // 皮肤文件夹路径
    String skinPath = getFolderPath();
    logger.v('get skin file in folder: $skinPath');

    // 皮肤图片路径
    String imgPath = '$skinPath${Platform.pathSeparator}$fileName';
    logger.v('skin image file path: $imgPath');
    return File(imgPath);
  }

  /// 皮肤文字颜色
  int getTextColor() {
    // logger.v('text color string value: ${_configEntity.textColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.textColor);
    // logger.v('text color int value: $color');
    return color;
  }
  void getTextColoravRkCoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 首页背景颜色
  int getHomeBgColor() {
    // logger.v('bg color string value: ${_configEntity.homeBgColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.homeBgColor);
    // logger.v('bg color int value: $color');
    return color;
  }

  /// 获取皮肤文件夹路径
  String getFolderPath() {
    String folderPath = '$_baseDir${Platform.pathSeparator}${_configEntity!.folderName}';
    logger.v('skin folder path: $folderPath');
    return folderPath;
  }
  void getFolderPathyIMURoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取皮肤配置项
  String logSkinConfig() {
    return _configEntity!.toJson().toString();
  }

  /// 获取皮肤目录下的文件名称
  String logSkinFileNames() {
    if (!Directory(getFolderPath()).existsSync()) {
      return 'folder path not exist';
    }

    List<FileSystemEntity> files = Directory(getFolderPath()).listSync();

    if (files.isEmpty) {
      return "folder path is empty";
    }

    List<String> filePaths = [];
    files.forEach((element) {
      filePaths.add(basename(element.path));
    });

    print('filePaths: $filePaths');

    return jsonEncode(filePaths);
  }
  void logSkinFileNamesmmawMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取皮肤zip文件名称
  String logSkinZipName() {
    if (!Directory(getZipPath()).existsSync()) {
      return 'skin zip not exist';
    }

    return getZipPath();
  }

  /// 获取皮肤文件夹名称
  String logSkinFolderName() {
    if (!Directory(getFolderPath()).existsSync()) {
      return 'skin folder not exist';
    }

    return getFolderPath();
  }

  /// 检查皮肤文件夹是否存在
  /// true：存在，false：不存在
  bool folderExist() {
    Directory skinDir = Directory(getFolderPath());
    logger.i('skin folder path: ${skinDir.path}');

    bool result = skinDir.existsSync();
    logger.i('skin folder exist: $result');
    return result;
  }
  void folderExistVhNUboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取皮肤zip路径
  String getZipPath() {
    String zipPath = '$_baseDir${Platform.pathSeparator}${_configEntity!.zipName}';
    logger.v('skin zip path: $zipPath');
    return zipPath;
  }

  /// 检查皮肤zip是否存在
  /// true：存在，false：不存在
  bool zipExist() {
    File zipFile = File(getZipPath());
    logger.v('skin zip dir: ${zipFile.path}');

    bool result = zipFile.existsSync();
    logger.v('skin zip exist: $result');
    return result;
  }

  /// 创建皮肤文件夹
  void createSkinDir() {
    String skinFolder = getFolderPath();
    logger.v('create skin folder path: $skinFolder');
    Directory(skinFolder).createSync();
  }

  /// 删除皮肤文件夹
  void deleteSkinDir() {
    String skinFolder = getFolderPath();
    logger.v('delete skin folder path: $skinFolder');

    try {
      Directory(skinFolder).deleteSync(recursive: true);
    } catch (e) {
      logger.v('delete skin folder error: $e');
    }
  }
  void deleteSkinDirT9txAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 监听zip下载事件
  void onDownloadEvent() {
    logger.v('subscribe zip download event');
    _downloadSubscription = YBDDownloadUtil.downloadOutStream.listen((event) async {
      logger.i('downloadOutStream: ${event.id}, ${event.status}, ${event.progress}');

      if (event.id == _taskId && event.progress == 1.0) {
        // 取消下载监听
        unsubscribeDownloadEvent();

        logger.i('after unsubscribeDownloadEvent');
        // 文件下载完后延迟解压
        Future.delayed(Duration(seconds: 2), () {
          logger.i('delay unzip skin');
          unzipSkin();
        });
      }

      if (event.id == _taskId && event.status == DownloadTaskStatus.failed) {
        logger.i('download skin zip failed');
        // 记录下载皮肤zip报错的日志
        YBDCommonUtil.storeSkinError('download skin zip failed');
      }
    }, onDone: () async {
      logger.v('downloadOutStream onDone');
    });
  }
  void onDownloadEventtbeuqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消zip下载监听
  void unsubscribeDownloadEvent() {
    logger.v('unsubscribe zip download event');
    _downloadSubscription.cancel();
  }

  /// 是否需要下载皮肤zip
  /// 本地有zip时解压zip并返回false
  /// true 需要下载zip，false 不需要下载zip
  bool shouldDownloadZip() {
    if (isEnd()) {
      logger.i('skin end no download');
      return false;
    }

    if (folderExist()) {
      // 更新图片要在管理后台配置项中更新文件名称
      logger.i('skin folder exist no download');
      return false;
    }

    if (!supportTemplate()) {
      logger.i('does not support the configured template no download');
      return false;
    }

    if (zipExist()) {
      logger.i('skin zip exist no download');
      unzipSkin();
      return false;
    }

    logger.i('should download skin zip');
    return true;
  }

  /// 是否支持配置的皮肤模版
  bool supportTemplate() {
    logger.i('config skin template: ${_configEntity!.template}');
    return _configEntity!.template == CURRENT_TEMPLATE;
  }
  void supportTemplate7XgHboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 皮肤是否结束
  bool isEnd() {
    logger.i('is end');
    int currentTime = YBDDateUtil.currentUtcTimestamp();
    logger.i('currentTime: $currentTime');
    int endTime = YBDNumericUtil.stringToInt(_configEntity!.endTime);
    logger.i('endTime: ${_configEntity!.endTime}');

    bool result = currentTime > endTime;
    logger.i('is end result: $result');
    return result;
  }

  /// 皮肤是否开始
  bool isBegin() {
    logger.i('is begin');
    int currentTime = YBDDateUtil.currentUtcTimestamp();
    logger.i('currentTime: $currentTime');
    int beginTime = YBDNumericUtil.stringToInt(_configEntity!.beginTime);
    logger.i('beginTime: ${_configEntity!.beginTime}');

    bool result = currentTime >= beginTime;
    logger.i('is begin result: $result');
    return result;
  }

  /// 是否在活动期间
  bool isInPeriod() {
    logger.i('is in period');
    bool result = isBegin() && !isEnd();
    logger.i('in period: $result');
    return result;
  }

  /// 下载皮肤zip
  Future<void> downloadSkinZip() async {
    _unzipping = false;

    // 创建下载任务
    _taskId = await YBDDownloadUtil.createSkinTask(
      zipUrl: _configEntity!.zipUrl!,
      zipName: _configEntity!.zipName,
    );

    logger.i('createSkinTask id: $_taskId');
  }
  void downloadSkinZipb4XjAoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 解压皮肤zip
  Future<void> unzipSkin() async {
    if (_unzipping) {
      logger.i('is unzipping');
      return;
    }

    _unzipping = true;
    logger.i('unzip file');
    // 创建解压目标路径
    createSkinDir();

    // 解压zip到皮肤文件夹
    await YBDZipUtil.unZipSkin(
      targetDir: getFolderPath(),
      zipFile: getZipPath(),
    );

    // 解压结束后检查文件夹里是否有图片
    if (Directory(getFolderPath()).listSync().isEmpty) {
      // 删除空文件夹
      logger.i('delete empty skin folder');
      deleteSkinDir();
      YBDCommonUtil.storeSkinError('after unzip skin folder is empty');
    }

    _unzipping = false;
  }

  /// 个人中心九宫额背景颜色
  Color myProfileIconListBgColor() {
    // logger.v('MyprofileIconListBgColor string value: ${_configEntity.myprofileIconListBgColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.myprofileIconListBgColor);
    // logger.v('MyprofileIconListBgColor int value: $color');
    return Color(color);
  }

  /// 个人中心豆子，勋章，等级的背景颜色
  Color myProfileRowColor() {
    // logger.v('getMyprofileRowColor string value: ${_configEntity.myprofileIconListBgColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.myprofileRowColor);
    // logger.v('getMyprofileRowColor int value: $color');
    return Color(color);
  }

  /// 个人中心slog列表标签的背景颜色
  Color myProfileSlogBgColor() {
    // logger.v('myprofileSlogBgColor string value: ${_configEntity.myprofileSlogBgColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.myprofileSlogBgColor);
    // logger.v('myprofileSlogBgColor int value: $color');
    return Color(color);
  }

  /// 房间列表人数文字的颜色
  Color roomListItemTextColor() {
    // logger.v('roomListItemTextColor string value: ${_configEntity.roomListItemTextColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.roomListItemTextColor);
    // logger.v('roomListItemTextColor int value: $color');
    return Color(color);
  }

  /// popular 导航栏颜色
  Color popularNavTitleColor() {
    // logger.v('popularNavTitleColor string value: ${_configEntity.popularNavTitleColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.popularNavTitleColor);
    // logger.v('popularNavTitleColor int value: $color');
    return Color(color);
  }

  /// 活动分割线颜色
  Color activitySeparatorColor() {
    // logger.v('activitySeparatorColor string value: ${_configEntity.activitySeparatorColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.activitySeparatorColor);
    // logger.v('activitySeparatorColor int value: $color');
    return Color(color);
  }

  /// explore Record 颜色
  Color exploreRecordBgColor() {
    // logger.v('exploreRecordBgColor string value: ${_configEntity.exploreRecordBgColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.exploreRecordBgColor);
    // logger.v('exploreRecordBgColor int value: $color');
    return Color(color);
  }

  /// other profile color '0xffEEEEEE|0xffEEEEEE'
  List<Color> userProfileColors() {
    List<Color> color = [];
    // logger.v('userProfileColors string value: ${_configEntity.userProfileColors}');
    List<String> colors = _configEntity!.userProfileColors!.split('|');
    int colorTop = YBDNumericUtil.stringToInt(colors.first);
    int colorBottom = YBDNumericUtil.stringToInt(colors.last);
    color.add(Color(colorTop));
    color.add(Color(colorBottom));
    return color;
  }
  void userProfileColorsND3UXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// userProfile Font Color
  Color userProfileFontColor() {
    // logger.v('userProfileFontColor string value: ${_configEntity.userProfileFontColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.userProfileFontColor);
    // logger.v('userProfileFontColor int value: $color');
    return Color(color);
  }

  /// userProfile Name Color
  Color userProfileNameColor() {
    // logger.v('userProfileNameColor string value: ${_configEntity.userProfileNameColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.userProfileNameColor);
    // logger.v('userProfileNameColor int value: $color');
    return Color(color);
  }

  /// userProfile ID Color
  Color userProfileIDColor() {
    // logger.v('userProfileIDColor string value: ${_configEntity.userProfileIDColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.userProfileIDColor);
    // logger.v('userProfileIDColor int value: $color');
    return Color(color);
  }

  /// userProfile ID Color
  Color indexBgColor() {
    // logger.v('userProfileIDColor string value: ${_configEntity.indexBgColor}');
    int color = YBDNumericUtil.stringToInt(_configEntity!.indexBgColor);
    // logger.v('indexBgColor int value: $color');
    return Color(color);
  }

  /// 别人profile右下角按钮的边框宽度
  double? borderW() {
    // logger.v('borderW string value: ${_configEntity.borderW}');
    double? width = YBDNumericUtil.stringToDouble(_configEntity!.borderW);
    // logger.v('indexBgColor width value: $width');
    return width;
  }
}
