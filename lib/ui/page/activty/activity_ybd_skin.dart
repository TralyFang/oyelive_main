import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_file.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'constants/act_ybd_consts.dart';

/// 宰牲节模板
class YBDActivitySkin extends YBDActivitySkinRoot {
  /// 个人中心vip图标地址
  String myProfileVip() {
    if (showSkin() && File('${ActImagePrefix}act_buy_vip.png').existsSync()) {
      return '${ActImagePrefix}act_buy_vip.png';
    }

    return 'assets/images/buy_vip.png';
  }

  void myProfileVipT3GUNoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 个人中心Top-up图标地址
  String myProfileTopUp() {
    if (showSkin() && File('${ActImagePrefix}act_top_up.png').existsSync()) {
      return '${ActImagePrefix}act_top_up.png';
    }

    return 'assets/images/topup/y_top_up.png';
  }

  void myProfileTopUp8syvqoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 个人中心Invite Friends图标地址
  String myProfileInvite() {
    if (showSkin() && File('${ActImagePrefix}act_invite.png').existsSync()) {
      return '${ActImagePrefix}act_invite.png';
    }

    return 'assets/images/invite.png';
  }

  void myProfileInvitea4Pkeoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 个人中心Daily Tasks图标地址
  String myProfileDailyTask() {
    if (showSkin() &&
        File('${ActImagePrefix}act_daily_task.png').existsSync()) {
      return '${ActImagePrefix}act_daily_task.png';
    }
    return 'assets/images/dc/daily_task.png';
  }

  void myProfileDailyTaskc8hDtoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 个人中心Store图标地址
  String myProfileStore() {
    if (showSkin() && File('${ActImagePrefix}act_store.png').existsSync()) {
      return '${ActImagePrefix}act_store.png';
    }

    return 'assets/images/store.png';
  }

  /// 个人中心Baggage图标地址
  String myProfileBaggage() {
    if (showSkin() && File('${ActImagePrefix}act_baggage.png').existsSync()) {
      return '${ActImagePrefix}act_baggage.png';
    }

    return 'assets/images/profile/baggage.png';
  }

  void myProfileBaggageFY1tMoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 个人中心Badges图标地址
  String myProfileBadges() {
    return 'assets/images/profile/badges.webp';
  }

  /// 个人中心Level图标地址
  String myProfileLevel() {
    return 'assets/images/level.png';
  }

  /// 个人中心Guardian图标地址
  String myProfileGuardian() {
    if (showSkin() && File('${ActImagePrefix}act_guardian.png').existsSync()) {
      return '${ActImagePrefix}act_guardian.png';
    }

    return 'assets/images/guardian.png';
  }

  /// 个人中心Support图标地址
  String myProfileSupport() {
    if (showSkin() &&
        File('${ActImagePrefix}act_icon_support.png').existsSync()) {
      return '${ActImagePrefix}act_icon_support.png';
    }

    return 'assets/images/icon_support.png';
  }

  void myProfileSupportzF8heoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 个人中心Talent图标地址
  String myProfileTalent() {
    if (showSkin() && File('${ActImagePrefix}act_talent.png').existsSync()) {
      return '${ActImagePrefix}act_talent.png';
    }

    return 'assets/images/talent.png';
  }

  /// 个人中心的顶部背景图
  String myprofileTopBg() {
    if (showSkin() &&
        File('${ActImagePrefix}act_bg_profile_me.png').existsSync()) {
      return '${ActImagePrefix}act_bg_profile_me.png';
    }

    return 'assets/images/bg_profile.webp';
  }

  /// 个人中心九宫额背景颜色
  BoxDecoration myprofileIconListBgDecoration() {
    if (showSkin()) {
      return BoxDecoration(
          color: YBDActivityFile.instance!.myProfileIconListBgColor());
    }

    return YBDTPStyle.gradientDecoration;
  }

  /// 个人中心豆子，勋章，等级的背景颜色
  Color myprofileRowColor() {
    if (showSkin()) {
      return YBDActivityFile.instance!.myProfileRowColor();
    }

    return Color(0xff7332B7);
  }

  /// 个人中心slog列表标签的背景颜色
  Color myprofileSlogBgColor() {
    if (showSkin()) {
      return YBDActivityFile.instance!.myProfileSlogBgColor();
    }

    return Color(0xff4A00A2);
  }

  /// 房间列表人数文字的颜色
  Color roomListItemTextColor() {
    if (showSkin()) {
      return YBDActivityFile.instance!.roomListItemTextColor();
    }

    return Colors.white.withOpacity(0.6);
  }

  /// 底部tab动态的开播图标
  String tabStartLive() {
    if (showSkin() && File('${ActImagePrefix}act_home_add.png').existsSync()) {
      return '${ActImagePrefix}act_home_add.png';
    }

    return 'assets/images/home_add.webp';
  }

  void tabStartLivexc1yEoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// popular banner的背景图
  Widget popularBannerBg() {
    if (showSkin() &&
        File('${ActImagePrefix}act_popular_banner_bg.jpg').existsSync()) {
      return Container(
        width: double.infinity,
        height: ScreenUtil().setWidth(444),
        child: YBDImage(
          path: '${ActImagePrefix}act_popular_banner_bg.jpg',
          fit: BoxFit.fill,
        ),
      );
    }

    return SizedBox();
  }

  /// popular 导航栏颜色
  Color popularNavTitleColor() {
    if (showSkin()) {
      // 宰牲节皮肤显示白色
      return YBDActivityFile.instance!.popularNavTitleColor();
    }

    return Colors.white;
  }

  /// 活动分割线颜色
  Color activitySeparatorColor() {
    if (showSkin()) {
      return YBDActivityFile.instance!
          .activitySeparatorColor()
          .withOpacity(0.1);
    }

    return Colors.white.withOpacity(0.1);
  }

  /// 活动文字颜色 使用skinTextColor字段
  Color activityTextColor() {
    if (showSkin()) {
      return Color(YBDActivityFile.instance!.getTextColor());
    }

    return Colors.white;
  }

  /// 活动文字颜色
  Color exploreRecordBgColor() {
    if (showSkin()) {
      return YBDActivityFile.instance!.exploreRecordBgColor();
    }

    return Colors.white.withOpacity(0.2);
  }

  /// 背景颜色 使用skinHomeBgColor字段
  BoxDecoration activityBgDecoration() {
    if (showSkin()) {
      return BoxDecoration(
          color: Color(YBDActivityFile.instance!.getHomeBgColor()));
    }

    return YBDTPStyle.gradientDecoration;
  }

  String profileBg(bool isMe) {
    return isMe ? myprofileTopBg() : userProfileBg();
  }

  void profileBgdO08ioyelive(bool isMe) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// other profile
  String userProfileBg() {
    return (showSkin() &&
            File('${ActImagePrefix}act_user_profile_bg.png').existsSync())
        ? '${ActImagePrefix}act_user_profile_bg.png'
        : 'assets/images/bg_profile.webp';
  }

  /// other profile
  String userProfileCopy() {
    return (showSkin() && File('${ActImagePrefix}act_copy.png').existsSync())
        ? '${ActImagePrefix}act_copy.png'
        : 'assets/images/icon_copy.webp';
  }

  /// other profile
  String userProfileLocation() {
    return (showSkin() &&
            File('${ActImagePrefix}act_location.png').existsSync())
        ? '${ActImagePrefix}act_location.png'
        : 'assets/images/location.png';
  }

  /// tab
  List<String> getTabDefaultIcon() {
    if (showSkin()) {
      bool home = File('${ActImagePrefix}act_home_default.png').existsSync();
      bool inbox = File('${ActImagePrefix}act_inbox_default.png').existsSync();
      bool user = File('${ActImagePrefix}act_user_default.png').existsSync();
      List<String> defaultIcons = [];
      defaultIcons.add(home
          ? "${ActImagePrefix}act_home_default.png"
          : "assets/images/home_default.webp");
      defaultIcons.add(inbox
          ? "${ActImagePrefix}act_inbox_default.png"
          : "assets/images/inbox/inbox_default.webp");
      defaultIcons.add(user
          ? "${ActImagePrefix}act_user_default.png"
          : "assets/images/user_default.webp");
      return defaultIcons;
    }
    return [
      "assets/images/home_default.webp",
      "assets/images/inbox/inbox_default.webp",
      "assets/images/user_default.webp",
    ];
  }

  void getTabDefaultIconKyKjeoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// tab
  List<String> getTabIcon() {
    if (showSkin()) {
      bool home = File('${ActImagePrefix}act_home.gif').existsSync();
      bool inbox = File('${ActImagePrefix}act_inbox.gif').existsSync();
      bool user = File('${ActImagePrefix}act_profile.gif').existsSync();
      List<String> tabIcons = [];
      tabIcons.add(home
          ? "${ActImagePrefix}act_home.gif"
          : "assets/images/switch/switch_home.gif");
      tabIcons.add(inbox
          ? "${ActImagePrefix}act_inbox.gif"
          : "assets/images/switch/switch_inbox.gif");
      tabIcons.add(user
          ? "${ActImagePrefix}act_profile.gif"
          : "assets/images/switch/switch_user.gif");
      return tabIcons;
    }
    return [
      "assets/images/switch/switch_home.gif",
      "assets/images/switch/switch_inbox.gif",
      "assets/images/switch/switch_user.gif"
    ];
  }

  void getTabIconzRq8Xoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 下拉刷新 header
  ClassicHeader activityRefreshHeader() {
    if (showSkin()) {
      return ClassicHeader(
        textStyle: TextStyle(
          color: activityTextColor().withOpacity(0.6),
        ),
      );
    }

    return ClassicHeader(
      textStyle: TextStyle(
        color: Colors.white.withOpacity(0.6),
      ),
    );
  }

  /// 上拉刷新 footer
  ClassicFooter activityRefreshFooter() {
    if (showSkin()) {
      return ClassicFooter(
        textStyle: TextStyle(
          color: activityTextColor().withOpacity(0.6),
        ),
      );
    }

    return ClassicFooter(
      textStyle: TextStyle(
        color: Colors.white.withOpacity(0.6),
      ),
    );
  }

  List<Color> userProfileColors() {
    return showSkin()
        ? YBDActivityFile.instance!.userProfileColors()
        : YBDTPStyle.colorList;
  }

  ///other profile color
  Color userProfileFontColor() {
    return showSkin()
        ? YBDActivityFile.instance!.userProfileFontColor()
        : Colors.white;
  }

  ///other profile color
  Color userProfileNameColor(BuildContext context) {
    return showSkin()
        ? YBDActivityFile.instance!.userProfileNameColor()
        : Theme.of(context).primaryColor;
  }

  ///other profile color
  Color userProfileIDColor(double a) {
    return showSkin()
        ? YBDActivityFile.instance!.userProfileIDColor().withOpacity(a)
        : Color(0x99ffffff);
  }

  ///other profile color
  Color indexBgColor() {
    return showSkin()
        ? YBDActivityFile.instance!.indexBgColor()
        : Colors.transparent;
  }

  ///other profile 炫浮按钮
  double? borderW() {
    return showSkin() ? YBDActivityFile.instance!.borderW() : 0;
  }
}
