import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/constant/short_ybd_route.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';

import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/constant/const.dart';
import '../../../common/service/task_service/task_ybd_service.dart';
import '../../../common/util/config_ybd_util.dart';
import '../../../common/util/date_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/numeric_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/inbox/message_ybd_helper.dart';
import '../../../module/user/util/user_ybd_util.dart';
import 'entity/configurable_ybd_activity_entity.dart';
import 'widget/configurable_ybd_activity.dart';
import 'widget/topup_ybd_gift_activity.dart';

/// 首页活动的弹框处理类
/// 根据配置项按先后顺序弹出活动弹框
class YBDActivityHandler {
  BuildContext context;

  YBDActivityHandler(this.context);

  /// 显示活动弹框
  void showActivity() {
    // TODO: 测试代码
    // showDialog(
    //   barrierDismissible: true,
    //   context: context,
    //   builder: (BuildContext builder) {
    //     return YBDTopUpGiftActivity(context, 'flutter://top_up_gift_location/false/topup_gift_activity');
    //   },
    // );
    // showDialog(
    //   barrierDismissible: false,
    //   context: context,
    //   builder: (BuildContext builder) {
    //     return YBDTopUpGiftActivity(context, 'https://www.baidu.com');
    //   },
    // );
    //
    // return;

    // 后台可配置的弹框
    _showConfigurableActivity();

    // 充值送礼活动弹框
    _showTopUpGift();
  }
  void showActivityikJTeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 充值送礼活动弹框
  Future<void> showTopUpGiftIgnoreTask() async {
    List<String>? topUpGiftConfigs = await _getTopUpGiftActivity(context);

    if (false == await _checkTopUpGiftCondition(topUpGiftConfigs)) {
      YBDTaskService.instance!.end(TASK_TOPUP_GIFT_ACTIVITY);
      logger.v('showTopUpGiftIgnoreTask task condition false');
      return;
    }

    logger.v('ignore dialog task to show topup gift activity');
    // 显示弹框的任务
    _showTopupGiftDialog(topUpGiftConfigs![2]);
  }
  void showTopUpGiftIgnoreTaskaZYlsoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 判断现在是否处于输入的时间段中
  bool _isInPeriod(String start, String end) {
    logger.v('activity period start : $start, end : $end');
    return DateTime.now().isAfter(DateTime.fromMillisecondsSinceEpoch(int.parse(start))) &&
        DateTime.now().isBefore(DateTime.fromMillisecondsSinceEpoch(int.parse(end)));
  }
  void _isInPeriod1Ixpooyelive(String start, String end) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 后台可配置的活动弹框
  Future<void> _showConfigurableActivity() async {
    bool firstLogin = await YBDUserUtil.isTodayFirstOpenApp();
    bool isNewUser = await YBDUserUtil.isNewUser();

    logger.v('===firstLogin : $firstLogin, isNewUser : $isNewUser');

    /// 获取弹框数据
    final YBDConfigurableActivityEntity? configActivityEntity = await ApiHelper.queryConfigActivityList(
      context,
      isNewUser: isNewUser,
      firstLogin: firstLogin,
    );

    // 配置活动数据为 null
    if (configActivityEntity == null) {
      logger.v('query configurable failed');
      YBDTaskService.instance!.end(TASK_CONFIG_ACTIVITY);
      return;
    }

    // 没有配置活动数据
    if (configActivityEntity.record!.isEmpty) {
      logger.v('query configurable is empty');
      YBDTaskService.instance!.end(TASK_CONFIG_ACTIVITY);
      return;
    }

    logger.v('query configurable success');

    // 当天已弹框不用重复弹
    // v2.0 给后台做逻辑处理，客户端只根据返回的数据显示
    // if (await _checkConfigurableActivitySp()) {
    //   logger.v('already showed config activity dialog');
    //   YBDTaskService.instance.end(TASK_CONFIG_ACTIVITY);
    //   return;
    // }

    // 显示弹框任务
    _configurableActivityDialogTask(configActivityEntity);
  }
  void _showConfigurableActivityod0iLoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 可配置活动弹框的任务
  /// [configActivityEntity] 配置的活动列表数据
  void _configurableActivityDialogTask(YBDConfigurableActivityEntity configActivityEntity) {
    YBDTaskService.instance!.onEvent.listen((int taskId) {
      if (taskId == TASK_CONFIG_ACTIVITY) {
        // v2.0 给后台做逻辑处理，客户端只根据返回的数据显示
        // YBDSPUtil.save(Const.SP_SHOW_CONFIG_ACTIVITY_DIALOG, YBDDateUtil.currentUtcTimestamp());
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.ACTIVITY_DIALOG_IMPRESSION));
        // 埋点
        YBDObsUtil.instance().push(currentRoute: YBDShortRoute.activity_pop, previousRoute: YBDShortRoute.home);
        logger.v('show config activity dialog context : $context');
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext builder) {
            return YBDConfigurableDialog(
              recordList: configActivityEntity.record,
            );
          },
        ).then((value) {
          logger.v('config activity dialog end task');
          // 已完成活动弹框任务
          YBDTaskService.instance!.end(TASK_CONFIG_ACTIVITY);
        });
      }
    });

    YBDTaskService.instance!.waiting(TASK_CONFIG_ACTIVITY);
  }
  void _configurableActivityDialogTaskapKDIoyelive(YBDConfigurableActivityEntity configActivityEntity) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 充值送礼活动弹框
  Future<void> _showTopUpGift() async {
    List<String>? topUpGiftConfigs = await _getTopUpGiftActivity(context);

    if (false == await _checkTopUpGiftCondition(topUpGiftConfigs)) {
      YBDTaskService.instance!.end(TASK_TOPUP_GIFT_ACTIVITY);
      logger.v('_showTopUpGift task condition false');
      return;
    }

    // 显示弹框的任务
    _topupGiftDialogTask(topUpGiftConfigs![2]);
  }

  /// 检查充值送礼活动的弹出条件
  /// true: 符合弹框的条件；false: 不符合弹框条件
  Future<bool> _checkTopUpGiftCondition(List<String>? topUpGiftConfigs) async {
    List<String>? topUpGiftConfigs = await _getTopUpGiftActivity(context);

    // 配置项错误不显示弹框
    if (!_checkConfig(topUpGiftConfigs)) {
      logger.v('invalid topup gift config : $topUpGiftConfigs');
      YBDTaskService.instance!.end(TASK_TOPUP_GIFT_ACTIVITY);
      return false;
    }

    // 活动已结束
    if (!_isInPeriod(topUpGiftConfigs![0], topUpGiftConfigs[1])) {
      logger.v('topup gift activity not in period');
      YBDTaskService.instance!.end(TASK_TOPUP_GIFT_ACTIVITY);
      return false;
    }

    // 显示活动弹框太频繁则不显示
    if (await _checkFrequency(int.parse(topUpGiftConfigs[3]), Const.SP_SHOW_TOPUP_GIFT_ACTIVITY_DIALOG)) {
      logger.v('show topup gift activity dialog too frequency');
      YBDTaskService.instance!.end(TASK_TOPUP_GIFT_ACTIVITY);
      return false;
    }

    // 检查是否为充值送礼活动人群
    if (!await _checkTopUpGiftUser()) {
      logger.v('not topup gift users');
      YBDTaskService.instance!.end(TASK_TOPUP_GIFT_ACTIVITY);
      return false;
    }

    return true;
  }
  void _checkTopUpGiftConditiongbq5Goyelive(List<String>? topUpGiftConfigs)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 首页充值送礼活动弹框
  /// [ignoreTask] 在 slog 页面停留一分钟后会用 true 来调用这个方法
  Future<void> _topupGiftDialogTask(String url) async {
    YBDTaskService.instance!.onEvent.listen((int taskId) async {
      if (taskId == TASK_TOPUP_GIFT_ACTIVITY) {
        logger.v('topupGift TASK_TOPUP_GIFT_ACTIVITY');

        // 需求：签到弹框消失一分钟后再弹框
        Future.delayed(Duration(seconds: 60), () async {
          logger.v('Future.delayed(Duration(seconds: 60');
          List<String>? topUpGiftConfigs = await _getTopUpGiftActivity(context);

          if (false == await _checkTopUpGiftCondition(topUpGiftConfigs)) {
            logger.v('_checkTopUpGiftCondition task delay 60');
            YBDTaskService.instance!.end(TASK_TOPUP_GIFT_ACTIVITY);
            return;
          }

          // 显示弹框
          _showTopupGiftDialog(url);
        });
      }
    });

    YBDTaskService.instance!.waiting(TASK_TOPUP_GIFT_ACTIVITY);
  }
  void _topupGiftDialogTaskmkeb3oyelive(String url)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 显示充值送礼活动弹框
  Future<void> _showTopupGiftDialog(String url) async {
    logger.v('_showTopupGiftDialog');
    final String? userId = await YBDUserUtil.userId();

    // 延迟弹出来的弹框，要检查登录状态
    if (null == userId || userId.isEmpty) {
      logger.v('_showTopupGiftDialog not login');
      // 已完任务
      YBDTaskService.instance!.end(TASK_TOPUP_GIFT_ACTIVITY);
      return;
    }

    // 记录已显示弹框，原生代码会根据这个 sp 值判断是否显示弹框
    YBDSPUtil.save(Const.SP_KEY_RECHARGE, true);

    // 记录显示弹框的时间，判断是否弹得太频繁
    await YBDSPUtil.saveCurrent(Const.SP_SHOW_TOPUP_GIFT_ACTIVITY_DIALOG, YBDDateUtil.currentUtcTimestamp());

    // 弹出弹框的埋点
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.ACTIVITY_DIALOG_IMPRESSION));
    // if (Const.FULL_FLUTTER_ENV) {
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext builder) {
        return YBDTopUpGiftActivity(context, url);
      },
    ).then((value) {
      // 已完任务
      YBDTaskService.instance!.end(TASK_TOPUP_GIFT_ACTIVITY);
    });
    // } else {
    //   // 弹出全局的充值送礼活动弹框
    //   FlutterBoost.singleton.open(getNativePageUrl(NativePageName.TOPUP_GIFT_DIALOG));
    //   // 混合包直接标记为已完任务
    //   YBDTaskService.instance.end(TASK_TOPUP_GIFT_ACTIVITY);
    // }

    // 发送 event notice 消息
    _sendLocalTopUpGiftEventNotice(url);
  }
  void _showTopupGiftDialogoFn5Aoyelive(String url)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 插入 event notice 消息
  void _sendLocalTopUpGiftEventNotice(String url) async {
    List<String>? topUpGiftConfigs = await _getTopUpGiftActivity(context);
    String? img;

    if (null != topUpGiftConfigs && topUpGiftConfigs.length > 4) {
      img = topUpGiftConfigs[4];
    }

    YBDMessageHelper.insertEventNoticeMessage(
      url: url,
      title: 'Lucky package',
      text: 'Congratulations, you have got the lucky package，come and enjoy it now~',
      resource: img ?? 'assets/images/topup/y_topup_gift_bg.png',
    );
  }

  /// 查询充值送礼活动配置项
  /// 格式 ："1618905030299|1619769600000|http://voice-cluster-314173693.cn-northwest-1.elb.amazonaws.com.cn:8090/event/fightForLimelight/|0"
  Future<List<String>?> _getTopUpGiftActivity(BuildContext context) async {
    String? topUpGiftActivity = await ConfigUtil.topUpGiftActivity(context);
    List<String>? configs = topUpGiftActivity?.split('|');
    return configs;
  }

  /// 检查充值送礼活动的人群（未充值的用户）
  /// true 是充值送礼活动的人群，false 不是充值送礼活动人群
  Future<bool> _checkTopUpGiftUser() async {
    if (false == await YBDUserUtil.isToppedUpUser(context)) {
      logger.v('_checkTopUpGiftUser true');
      // 未充值用户要为送礼活动的人群
      return true;
    }

    logger.v('_checkTopUpGiftUser false');
    // 充值用户不是充值送礼活动人群
    return false;
  }
  void _checkTopUpGiftUserBTf6zoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 检查活动配置项是否正确
  /// 配置项正确返回 true 配置项错误返回 false
  bool _checkConfig(List<String>? configs) {
    // 配置项为空
    if (null == configs) {
      return false;
    }

    if (configs.length < 4) {
      return false;
    }

    return true;
  }

  /// 检查当前用户的活动弹框显示频率
  /// [spKey] 保存上次弹框的 sp
  /// [config] 显示频率配置项
  /// return : 显示频繁返回 true，显示不频繁要返回 false
  Future<bool> _checkFrequency(int config, String spKey) async {
    try {
      // 当前用户上次显示活动的日期
      final lastShowDate = await YBDSPUtil.getCurrent(spKey);
      logger.v('last date : $lastShowDate, frequency config : $config');
      if (null == lastShowDate) {
        logger.v('last show date is null');
        return false;
      }

      // 距离上次显示弹框的间隔大于配置间隔
      if (YBDDateUtil.currentUtcTimestamp() - lastShowDate >= config * Const.FREQUENCY_UNIT_HOUR) {
        logger.v('last show date is old');
        return false;
      }
    } catch (e) {
      // 定位用户日志中报错的
      // type '_Smi' is not a subtype of type 'String'
      // YBDActivityHandler._checkFrequency (package:ybd_oyetalk/ui/page/activty/activity_handler.dart:600)
      logger.e('_checkFrequency error: $e');
    }

    return true;
  }
  void _checkFrequencyr7MYBoyelive(int config, String spKey)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
