import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDConfigurableActivityEntity with JsonConvert<YBDConfigurableActivityEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDConfigurableActivityRecord?>? record;
}

class YBDConfigurableActivityRecord with JsonConvert<YBDConfigurableActivityRecord> {
  int? id;
  int? type;
  int? triggerEvent;
  int? triggerUser;
  String? img;
  String? link;
  String? title;
  int? startTime;
  int? endTime;
  int? rank;
  int? status;
  int? createTime;
  int? deleted;
  int? version;
  dynamic projectCode;
  dynamic tenantCode;
}
