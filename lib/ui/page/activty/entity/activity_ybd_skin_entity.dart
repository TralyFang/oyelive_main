import 'dart:async';


import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDActivitySkinEntity with JsonConvert<YBDActivitySkinEntity> {
  /// 皮肤文件夹名称
  String? folderName;

  /// 皮肤zip名称
  String? zipName;

  /// 皮肤zip下载链接
  String? zipUrl;

  /// 皮肤开始时间
  String? beginTime;

  /// 皮肤结束时间
  String? endTime;

  /// 皮肤模版
  String? template;

  /// 皮肤字体颜色
  String? textColor;

  /// 首页背景颜色
  String? homeBgColor;

  /// 个人中心九宫额背景颜色
  String? myprofileIconListBgColor;

  /// 个人中心豆子，勋章，等级的背景颜色
  String? myprofileRowColor;

  /// 个人中心slog列表标签的背景颜色
  String? myprofileSlogBgColor;

  /// 房间列表人数文字的颜色
  String? roomListItemTextColor;

  /// popular 导航栏颜色
  String? popularNavTitleColor;

  /// popular活动分割线颜色
  String? activitySeparatorColor;

  /// explore Record 颜色
  String? exploreRecordBgColor;

  /// other profile color
  String? userProfileColors;

  /// userProfile Font Color
  String? userProfileFontColor;

  /// userProfile Name Color
  String? userProfileNameColor;

  /// userProfile ID Color
  String? userProfileIDColor;

  /// inbox Bg Color和底部tab栏Color
  String? indexBgColor;

  /// 别人profile右下角按钮的边框宽度
  String? borderW;

  // 模拟皮肤配置
  // static demoData() {
  //   var entity = YBDActivitySkinEntity();
  //   entity.template = 'Christmas_2021';
  //   entity.zipUrl =
  //       'https://firebasestorage.googleapis.com/v0/b/iapserverdemo.appspot.com/o/skin%2Fskin.zip?alt=media&token=523b7340-3957-484d-bdbd-5ceba26e947f';
  //   entity.zipName = 'EidUlAzha_v1.zip';
  //   entity.beginTime = '1640361600000'; // 2021-12-25 00:00:00
  //   entity.endTime = '1640966400000'; // 2022-01-01 00:00:00
  //   entity.textColor = '0xff000000';
  //   entity.homeBgColor = '0xffFCFCFC';
  //   entity.folderName = 'EidUlAzha_v1';
  //   entity.myprofileIconListBgColor = '0xffFCFCFC';
  //   entity.myprofileRowColor = '0xFFFCFCFC';
  //   entity.myprofileSlogBgColor = '0xFFE9E9E9';
  //   entity.roomListItemTextColor = '0xff000000';
  //   entity.popularNavTitleColor = '0xFFFFFFFF';
  //   entity.activitySeparatorColor = '0xFF000000';
  //   entity.exploreRecordBgColor = '0xffEEEEEE';
  //   entity.userProfileColors = '0xffEEEEEE|0xffEEEEEE';
  //   entity.userProfileFontColor = '0xFF000000';
  //   entity.userProfileNameColor = '0xFF000000';
  //   entity.userProfileIDColor = '0xFF000000';
  //   entity.indexBgColor = '0xFFFFFFFF';
  //   entity.borderW = "1";
  //   return entity;
  // }

  static YBDActivitySkinEntity? copyWith(YBDActivitySkinEntity entity) {
    YBDActivitySkinEntity? result = YBDActivitySkinEntity();
    try {
      result = JsonConvert.fromJsonAsT<YBDActivitySkinEntity>(entity.toJson());
    } catch (e) {
      logger.e('copy YBDActivitySkinEntity error: $e');
    }

    return result;
  }
}
