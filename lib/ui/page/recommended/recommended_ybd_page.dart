import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/constant/const.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/entity/query_ybd_followed_resp_entity.dart';
import '../../../module/entity/query_ybd_star_resp_entity.dart';
import '../../../module/entity/room_ybd_info_entity.dart';
import '../../widget/loading_ybd_circle.dart';
import '../../widget/recommended_ybd_item.dart';
import 'follow_ybd_all_btn.dart';

class YBDRecommendedPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _YBDRecommendedPageState();
}

class _YBDRecommendedPageState extends BaseState<YBDRecommendedPage> {
  bool loaded = false;
  List<int>? _followedIDs = [];
  List<YBDRoomInfo?> _rooms = [];

  @override
  void initState() {
    super.initState();

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE_RECOMMENDED));
    queryFollowedIDs();
  }
  void initStatedcc5Voyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
        width: double.infinity,
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: <Widget>[
            Image.asset(
              "assets/images/recommended_header.webp",
              width: ScreenUtil().screenWidth,
              height: ScreenUtil().setWidth(262),
            ),
            Row(
              children: <Widget>[
                SizedBox(width: ScreenUtil().setWidth(24)),
                Expanded(
                  child: Text(
                    translate('famous_stars'),
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(36),
                      color: Colors.white,
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    YBDAnalyticsUtil.logEvent(
                      YBDAnalyticsEvent(
                        YBDEventName.CLICK_EVENT,
                        location: 'recommended_page',
                        itemName: 'recommended_page_next',
                      ),
                    );
                    YBDCommonTrack().commonTrack(YBDTAProps(
                      location: YBDTAClickName.guide_skip,
                      module: YBDTAModule.guide,
                    ));
                    YBDNavigatorHelper.navigateTo(
                      context,
                      YBDNavigatorHelper.index_page,
                      replace: true,
                      clearStack: true,
                    );
                  },
                  child: Text(
                    translate('next'),
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(36),
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            Text(
              translate('recommended_hint'),
              style: TextStyle(
                fontSize: ScreenUtil().setSp(24),
                color: Colors.white70,
              ),
            ),
            Divider(thickness: ScreenUtil().setWidth(1)),
            Expanded(
              child: !loaded
                  ? YBDLoadingCircle()
                  : _rooms == null || _rooms.isEmpty
                      ? Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Image.asset(
                                "assets/images/empty/empty_my_profile.webp",
                                width: ScreenUtil().setWidth(382),
                              ),
                              SizedBox(
                                height: ScreenUtil().setWidth(30),
                              ),
                              Text(
                                translate('no_data'),
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                        )
                      : Stack(
                          alignment: Alignment.center,
                          children: [
                            ListView.separated(
                              padding: EdgeInsets.all(0),
                              itemBuilder: (_, index) => Material(
                                color: Colors.transparent,
                                child: InkWell(
                                    splashColor: Colors.white10,
                                    highlightColor: Colors.white12,
                                    onTap: () async {},
                                    child: YBDRecommendedItem(
                                      _rooms[index],
                                      followed: _followedIDs == null || _followedIDs!.isEmpty
                                          ? false
                                          : _followedIDs!.contains(_rooms[index]!.id),
                                      key: Key(_rooms[index]!.id.toString()),
                                      onChange: (id) {
                                        _followedIDs!.add(id);
                                      },
                                    )),
                              ),
                              separatorBuilder: (_, index) => Container(
                                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                                height: ScreenUtil().setWidth(1),
                                color: Const.COLOR_BORDER,
                              ),
                              itemCount: _rooms.length,
                            ),
                            Positioned(
                              bottom: 0,
                              child: YBDFollowAllBtn(roomIdList: _rooms),
                            ),
                          ],
                        ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildU0TPpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  queryFollowedIDs() {
    YBDSPUtil.getUserInfo().then((userInfo) async {
      /// 查询用户已经关注的用户列表
      YBDQueryFollowedRespEntity? queryFollowedRespEntity =
          await ApiHelper.queryFollowed(context, -1, 0, 0, userInfo?.id, 1);
      _followedIDs = queryFollowedRespEntity?.record?.followed;

      YBDQueryStarRespEntity? queryStarRespEntity = await ApiHelper.queryStar(context, 0, 20, 3);
      _rooms = queryStarRespEntity?.record?.rank ?? [];
      loaded = true;

      if (mounted) {
        setState(() {});
      }
    });
  }
}
