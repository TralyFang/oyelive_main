import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

class YBDFollowAllBtn extends StatelessWidget {
  final List<YBDRoomInfo?>? roomIdList;

  YBDFollowAllBtn({
    this.roomIdList,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        alignment: Alignment.center,
        children: [
          _blackBg(),
          _whiteBtn(context),
        ],
      ),
    );
  }
  void build0WbzSoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _blackBg() {
    return IgnorePointer(
      child: Container(
        width: ScreenUtil().screenWidth,
        height: ScreenUtil().setWidth(274),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.black.withOpacity(0.0),
              Colors.black.withOpacity(0.7),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
      ),
    );
  }
  void _blackBgXIXsyoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _whiteBtn(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v('clicked follow all btn');
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(ScreenUtil().setWidth(60)),
          color: Colors.white,
        ),
        width: ScreenUtil().setWidth(600),
        child: TextButton(
          onPressed: () {
            logger.v('clicked follow all btn');
            YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
              YBDEventName.CLICK_EVENT,
              itemName: YBDItemName.RECOMMENDED_FOLLOW_ALL,
            ));
            YBDCommonTrack().commonTrack(YBDTAProps(
              location: YBDTAClickName.guide_follow_all,
              module: YBDTAModule.guide,
            ));
            // 请求关注接口
            _requestFollowTalentList();

            // 点击按钮跳到首页
            YBDNavigatorHelper.navigateTo(
              context,
              YBDNavigatorHelper.index_page,
              replace: true,
              clearStack: true,
            );
          },
          child: Text(
            translate('follow_all_to_chat'),
            style: TextStyle(
              color: Colors.black,
              fontSize: ScreenUtil().setSp(28),
            ),
          ),
        ),
      ),
    );
  }
  void _whiteBtn96L4goyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _requestFollowTalentList() {
    List<int?> talentIdList = roomIdList!.map((e) => e!.id).toList();
    logger.v('talentIdList: $talentIdList');
    ApiHelper.followTalentList(Get.context, talentIdList);
  }
}
