import 'dart:async';

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:uuid/uuid.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../generated/json/base/json_convert_content.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/inbox/db/model_ybd_message.dart';
import '../../../../module/inbox/entity/message_ybd_list_entity.dart';
import '../../../../module/inbox/inbox_ybd_api_helper.dart';
import '../../../../module/inbox/message_ybd_helper.dart';
import '../../../../module/status/entity/status_ybd_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../home/entity/banner_ybd_advertise_entity.dart';
import '../entity/friend_ybd_share_resp_entity.dart';

enum FriendShareType {
  // 分享房间
  ShareRoom,
  // 分享活动
  ShareAd,
  // 分享动态
  ShareStatus,
  // 分享勋章
  ShareBadge,
  // 分享游戏房间
  ShareGameRoom,
  // 分享tpgo房间
  ShareTpgoRoom,
  // 未知类型
  Unknown,
}

/// 好友分享工具类
class YBDFriendShareUtil {
  /// 最多可选择的好友数
  static final int defaultMaxAllowedFriend = 20;

  /// 分享动态的 content
  static Future<Map<String, dynamic>> statusMsgContent(
    BuildContext? context,
    YBDStatusInfo? status,
    YBDUserInfo? userInfo,
  ) async {
    Map<String, dynamic> contentJson = {};
    contentJson['layout'] = Const.SHARE_STATUS;
    contentJson['text'] = '';
    contentJson['click'] = {'url': 'flutter://status/detail/${status?.id ?? ''}', 'action': 'redirect'};
    contentJson['title'] = '${status?.nickName} just shared a moment!';
    contentJson['comment'] = '"SLog share"';
    contentJson['attributes'] = status?.toJson();

    // resource
    // TODO: attributes 包含 resource 数据，这里的 resource 字段可移除
//    if (null != status.content.images) {
//      if (status.content.images.isNotEmpty) {
//        // 取第一张图片作为 resource
//        contentJson['resource'] = status.content.images[0];
//      } else {
//        logger.v('no image resource');
//      }
//    } else if (null != status.content.audio) {
//      if (null != status.content.audio.bgImage) {
//        // 音频动态取背景图片作为 resource
//        contentJson['resource'] = status.content.audio.bgImage;
//      } else {
//        logger.v('no audio resource');
//      }
//    }

    // icon
    // TODO: attributes 包含 icon 数据，这里的 icon 字段可移除
//    String headImg = await YBDUserUtil.headImage(context) ?? '';
//    if (YBDStringUtil.beginWith(headImg, 'http')) {
//      contentJson['icon'] = '$headImg';
//    } else {
//      contentJson['icon'] = YBDImageUtil.avatar(context, headImg, userInfo.id, scene: 'B') ?? '';
//    }

    return contentJson;
  }

  /// flutter 分享活动的 content
  // TODO: 修改为调 adMsgContent() 方法
  static Future<Map<String, dynamic>> eventsMsgContent(
    BuildContext context,
    YBDBannerAdvertiseRecord advertise,
    YBDUserInfo userInfo,
  ) async {
    Map<String, dynamic> contentJson = {};
    contentJson['layout'] = Const.SHARE_EVENT;
    contentJson['text'] = '';
    contentJson['click'] = {
      'url': '${advertise.adurl}',
      'action': 'redirect',
    };
    contentJson['title'] = advertise.adname ?? '';
    contentJson['comment'] = '"Event share"';

    // 私信显示取[title]作为私信标题
    advertise.title = advertise.adname ?? '';

    // 生成全路径图片地址
    advertise.img = YBDImageUtil.ad(context, advertise.img, 'A');
    contentJson['attributes'] = advertise.toJson();
    return contentJson;
  }

  /// 原生分享活动的 content
  static Future<Map<String, dynamic>> adMsgContent(
    BuildContext? context,
    dynamic advertise,
    YBDUserInfo? userInfo,
  ) async {
    Map<String, dynamic> contentJson = {};
    contentJson['layout'] = Const.SHARE_EVENT_CENTER;
    contentJson['text'] = '';
    contentJson['click'] = {
      'url': '${advertise['adUrl']}',
      'action': 'redirect',
    };
    contentJson['title'] = advertise['adName'] ?? '';
    contentJson['adDetail'] = advertise['adDetail'] ?? '';
    contentJson['comment'] = '"Event share"';

    // 私信显示取[title]作为私信标题
    advertise['title'] = advertise['adName'] ?? '';

    String imgUrl;
    if (null != advertise['adImg']) {
      imgUrl = YBDImageUtil.ad(context, advertise['adImg'], 'A');
    } else {
      logger.v('ad is null show default ad img');
      imgUrl = YBDImageUtil.getDefaultImg(context);
    }

    // 生成全路径图片地址
    advertise['adImg'] = imgUrl;
    contentJson['attributes'] = advertise;
    return contentJson;
  }

  /// 游戏分享房间的 content
  static Future<Map<String, dynamic>> gameRoomMsgContent(BuildContext? context, Map<String, dynamic> data,
      {bool isTpgo: false}) async {
    /// 游戏分享map里面必须带的以下参数
    var title = data["title"];
    var roomId = data["roomId"];
    var content = data["content"];
    var roomImg = data["roomImg"];

    Map<String, dynamic> contentJson = {};
    contentJson['layout'] = isTpgo ? Const.SHAERE_TPGO_ROOM : Const.SHAERE_GAME_ROOM;
    // flutter://game_room_page
    contentJson['click'] = {
      'url': 'native://game_room_page?roomId=${roomId ?? ''}',
      'action': 'redirect',
    };
    contentJson['title'] = title;
    contentJson['text'] = '';
    contentJson['comment'] = '"Game room share"';
    Map<String, dynamic> attr = {'roomimg': roomImg, 'content': content, 'roomId': roomId};
    contentJson['attributes'] = attr;

    logger.v("message==contentJson====$contentJson");
    return contentJson;
  }

  /// 分享房间的 content
  static Future<Map<String, dynamic>> roomMsgContent(
    BuildContext? context,
    // Map<String, dynamic> 类型
    dynamic roomInfo,
    YBDUserInfo? userInfo,
  ) async {
    if (roomInfo is YBDRoomInfo) {
      roomInfo = roomInfo.toJson();
    }
    Map<String, dynamic> contentJson = {};
    contentJson['layout'] = Const.SHARE_ROOM;
    contentJson['text'] = '';
    contentJson['click'] = {
      'url': 'native://room?roomId=${roomInfo['id'] ?? ''}',
      'action': 'redirect',
    };
    contentJson['title'] = '${roomInfo['nickname'] ?? ''} ${translate('room_share_info')}';
    contentJson['comment'] = '"Room share"';

    // 生成全路径图片地址
    roomInfo['roomimg'] = YBDImageUtil.cover(
      context,
      roomInfo['roomimg'],
      roomInfo['id'],
      scene: 'C',
    );

    contentJson['attributes'] = roomInfo;
    return contentJson;
  }

  /// 分享badges
  static Future<Map<String, dynamic>> badgesContent(
    BuildContext? context,
    dynamic data,
    YBDUserInfo? userInfo,
  ) async {
    Map<String, dynamic> contentJson = {};
    contentJson['text'] = '';
    contentJson['layout'] = Const.Share_Badges;
    contentJson['comment'] = '"Badges share"';
    contentJson['attributes'] = json.encode(data);

    return contentJson;
  }

  /// 好友分享消息实体
  static YBDMessageListDataPullMessage? friendShareMessage(
    String? requestId,
    int? senderId,
    int? receiveId,
    Map<String, dynamic>? contentJson,
// TODO: content 包含 attributes 数据，这里 attrJson 可移除
//    Map<String, dynamic> attrJson,
  ) {
    Map<String, dynamic> shareMsg = {};
    shareMsg['contentType'] = 'application/cell';
    shareMsg['content'] = contentJson;
    shareMsg['sender'] = {'id': senderId};
    shareMsg['receivers'] = [
      {'id': receiveId}
    ];
    shareMsg['queueId'] = '';
    shareMsg['sendTime'] = YBDDateUtil.currentUtcTimestamp();
    shareMsg['category'] = 'friend';

    // TODO: content 包含 attributes 数据，这里可移除
//    shareMsg['attributes'] = attrJson;
    shareMsg['requestId'] = requestId;
    return JsonConvert.fromJsonAsT<YBDMessageListDataPullMessage>(shareMsg);
  }

  /// 根据消息实体和消息状态生成消息模型
  static YBDMessageModel messageModelFromEntity(YBDMessageListDataPullMessage msgEntity, MessageStatus status) {
    return YBDMessageModel(
        messageId: msgEntity.messageId,
        messageStatus: status,
        jsonContent: jsonEncode(msgEntity.toJson()),
        category: msgEntity.category,
        contentType: msgEntity.contentType,
        senderID: msgEntity.sender?.id.toString(),
        receiverID: msgEntity.receivers![0]!.id.toString(),
        sendTime: msgEntity.sendTime,
        text: msgEntity.content!.text,
        comment: msgEntity.content!.comment,
        requestId: msgEntity.requestId,
        isRead: true);
  }

  /// 好友分享接口的参数
  static Map<String, dynamic> statusParam(
    List<Map<String, dynamic>> toIds,
    Map<String, dynamic>? content,
  ) {
    Map<String, dynamic> result = {};
    result['contentType'] = 'application/cell';
    result['toIds'] = toIds;
    result['content'] = content;
    return result;
  }

  /// 最多可选择的好友数
  static int maxAllowedFriend(List<YBDRoomInfo?>? _friends) {
    if (null != _friends && _friends.length < defaultMaxAllowedFriend) {
      // 小于允许选择的最大好友数时显示好友数
      return _friends.length;
    } else {
      return defaultMaxAllowedFriend;
    }
  }

  /// 分享数据给好友
  static Future<bool> shareToFriends(
      BuildContext? context, FriendShareType type, dynamic data, List<YBDRoomInfo?> selectedFriends,
      {bool needLoading: true}) async {
    if (null == selectedFriends || selectedFriends.isEmpty) {
      logger.v('selected friends is empty');
      YBDToastUtil.toast('Please select friends');
      return false;
    } else {
      if (needLoading) YBDDialogUtil.showLoading(context);

      // 发送者为当前登录的用户
      YBDUserInfo? userInfo = await YBDUserUtil.userInfo();

      // YBDMessageModel 的 content 字段对应的数据
      Map<String, dynamic>? contentJson;

      switch (type) {
        case FriendShareType.ShareRoom:
          // 分享房间
          contentJson = await YBDFriendShareUtil.roomMsgContent(context, data, userInfo);
          break;
        case FriendShareType.ShareGameRoom:
          // 分享房间
          contentJson = await YBDFriendShareUtil.gameRoomMsgContent(context, data);
          break;
        case FriendShareType.ShareTpgoRoom:
          // 分享tpgo房间
          contentJson = await YBDFriendShareUtil.gameRoomMsgContent(context, data, isTpgo: true);
          break;
        case FriendShareType.ShareAd:
          // 分享活动
          contentJson = await YBDFriendShareUtil.adMsgContent(context, data, userInfo);
          break;
        case FriendShareType.ShareStatus:
          // 分享动态
          contentJson = await YBDFriendShareUtil.statusMsgContent(context, data, userInfo);
          break;
        case FriendShareType.ShareBadge:
          contentJson = await YBDFriendShareUtil.badgesContent(context, data, userInfo);
          break;
      }

      // 消息接收者
      List<Map<String, dynamic>> toIds = selectedFriends.map((e) {
        return {'userId': e!.id, 'requestId': Uuid().v4()};
      }).toList();

      // 插入数据库的消息实体
      List<YBDMessageListDataPullMessage?> shareMsgList = toIds.map((e) {
        return YBDFriendShareUtil.friendShareMessage(
          e['requestId'],
          userInfo!.id,
          e['userId'],
          contentJson,
        );
      }).toList();

      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.SHARE,
        method: 'friend',
        itemName: YBDItemName.BEFORE_SHARE,
      ));

      // 调好友分享接口
      bool result = await _requestShareStatus(context, toIds, shareMsgList, contentJson);

      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.SHARE,
        method: 'friend',
        itemName: YBDItemName.SHARE_RESULT,
        value: '$result',
      ));

      if (needLoading) YBDDialogUtil.hideLoading(context);

      // 返回分享成功或失败的结果
      return result;
    }
  }

  /// 请求好友分享接口
  static Future<bool> _requestShareStatus(
    BuildContext? context,
    List<Map<String, dynamic>> toIds,
    List<YBDMessageListDataPullMessage?> shareMsgList,
    Map<String, dynamic>? contentJson,
  ) async {
    // 调接口
    Map<String, dynamic> param = YBDFriendShareUtil.statusParam(toIds, contentJson);
    YBDFriendShareRespEntity? respEntity = await YBDInboxApiHelper.shareToFriend(context, param);

    if (respEntity?.code == Const.HTTP_SUCCESS_NEW) {
      logger.v('request friend share success');
      await _shareStatusSuccess(context, respEntity!, shareMsgList);
      return true;
    } else {
      logger.v('request friend share failed');
      return false;
    }
  }

  /// 请求好友分享接口成功后将消息插入到本地数据库
  static _shareStatusSuccess(
    BuildContext? context,
    YBDFriendShareRespEntity respEntity,
    List<YBDMessageListDataPullMessage?> shareMsgList,
  ) async {
    if (null != respEntity.data!.respList) {
      // 给消息实体添加 messageId 然后插入数据库
      respEntity.data!.respList!.forEach((resp) async {
        try {
          YBDMessageModel messageModel;

          // 找不到消息抛异常
          YBDMessageListDataPullMessage msg = shareMsgList.firstWhere((shareMsg) => shareMsg!.requestId == resp!.requestId)!;
          msg.messageId = resp!.messageId;

          if (resp.code == Const.HTTP_SUCCESS_NEW) {
            messageModel = YBDFriendShareUtil.messageModelFromEntity(msg, MessageStatus.sent);
          } else if (resp.code == Const.HTTP_MESSAGE_BLOCKED) {
            messageModel = YBDFriendShareUtil.messageModelFromEntity(msg, MessageStatus.reject);
          } else if (resp.code == Const.HTTP_MESSAGE_BLOCKING) {
            messageModel = YBDFriendShareUtil.messageModelFromEntity(msg, MessageStatus.blocking);
          } else {
            messageModel = YBDFriendShareUtil.messageModelFromEntity(msg, MessageStatus.send_failed);
          }

          // 插入数据库
          await messageModel.sqlInsert();

          // 更新会话列表
          await YBDMessageHelper.updateConversation(messageModel);

          // 主动拉消息
          YBDMessageHelper.pullMessage(context);
        } catch (e) {
          logger.v('search msg failed');
        }
      });
    }
  }
}
