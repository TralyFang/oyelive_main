import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/page/share/widget/game_ybd_friend_list_dialog.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/icon_ybd_gender.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../status/local_audio/circle_ybd_check_box.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

/// 参数表示是否选中的状态
typedef FriendListItemCallback = Function(bool);

/// 好友列表项
class YBDFriendDialogListItem extends StatefulWidget {
  /// 好友信息
  final YBDRoomInfo? userInfo;

  /// 点击列表项的回调
  final FriendListItemCallback callback;

  /// 是否小于最大可选数
  final bool belowMax;

  /// item 的背景颜色
  final Color bgColor;

  /// 昵称颜色
  final Color nickNameColor;

  /// 是否选中
  final List<YBDRoomInfo?>? selectedFriends;

  final GameShareType? type;

  YBDFriendDialogListItem(
    this.userInfo,
    this.belowMax,
    this.callback, {
    this.selectedFriends,
    this.bgColor = Colors.white,
    this.nickNameColor = Colors.black,
    this.type,
  });

  @override
  _YBDFriendDialogListItemState createState() => new _YBDFriendDialogListItemState();
}

class _YBDFriendDialogListItemState extends State<YBDFriendDialogListItem> {
  /// 是否被选中
  bool _isSelected = false;

  @override
  Widget build(BuildContext context) {
    if (null != widget.selectedFriends && widget.selectedFriends!.map((e) => e?.id).contains(widget.userInfo?.id)) {
      _isSelected = true;
    }

    if (widget.type != null && widget.type == GameShareType.ludo)
      return GestureDetector(
        onTap: () {
          logger.v('clicked friend list item');

          // 小于最大可选数时可点击
          // 达到最大可选数且当前为选中状态时可点击
          if (widget.belowMax || (!widget.belowMax && _isSelected)) {
            _isSelected = !_isSelected;

            // 更新选中好友数组
            if (null != widget.callback) {
              widget.callback(_isSelected);
            }

            // 刷新选中状态
            setState(() {});
          } else {
            YBDToastUtil.toast('Can not select more');
            logger.v('item is not enable');
          }
        },
        child: Container(
            // width: 500.px,
            height: 90.px,
            margin: EdgeInsets.only(bottom: 15.px as double),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(16.px as double)),
              color: Color(0xff0C68AC).withOpacity(0.5),
            ),
            child: Row(
              children: [
                SizedBox(width: 40.px),
                // 打勾
                _selectedCheckBox(_isSelected),
                SizedBox(width: 20.px),
                // 用户头像
                YBDRoundAvatar(
                  widget.userInfo!.headimg,
                  sex: widget.userInfo!.sex,
                  lableSrc: (widget.userInfo?.vip ?? 0) == 0 ? '' : 'assets/images/vip_lv${widget.userInfo!.vip}.png',
                  userId: widget.userInfo!.id,
                  scene: 'B',
                  labelWitdh: 22,
                  labelPadding: EdgeInsets.only(right: 5.px as double, bottom: 5.px as double),
                  avatarWidth: 72,
                ),
                // 昵称
                SizedBox(width: 20.px),
                SizedBox(
                  width: 260.px,
                  child: Text(
                    // 昵称
                    widget.userInfo?.nickname?.removeBlankSpace() ?? '',
                    style: TextStyle(
                      fontSize: 28.sp2,
                      color: widget.nickNameColor,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                  ),
                ),
              ],
            )),
      );

    return GestureDetector(
      onTap: () {
        logger.v('clicked friend list item');

        // 小于最大可选数时可点击
        // 达到最大可选数且当前为选中状态时可点击
        if (widget.belowMax || (!widget.belowMax && _isSelected)) {
          _isSelected = !_isSelected;

          // 更新选中好友数组
          if (null != widget.callback) {
            widget.callback(_isSelected);
          }

          // 刷新选中状态
          setState(() {});
        } else {
          // TODO: 翻译
          YBDToastUtil.toast('Can not select more');
          logger.v('item is not enable');
        }
      },
      child: Container(
        color: widget.bgColor,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
              child: Column(
                children: [
                  Container(
                    height: ScreenUtil().setHeight(106),
                    child: Row(
                      children: [
                        YBDRoundAvatar(
                          // 用户头像
                          widget.userInfo!.headimg,
                          sex: widget.userInfo!.sex,
                          lableSrc:
                              (widget.userInfo?.vip ?? 0) == 0 ? '' : 'assets/images/vip_lv${widget.userInfo!.vip}.png',
                          userId: widget.userInfo!.id,
                          scene: 'B',
                          labelWitdh: 22,
                          labelPadding:
                              EdgeInsets.only(right: ScreenUtil().setWidth(5), bottom: ScreenUtil().setWidth(5)),
                          avatarWidth: 80,
                        ),
                        SizedBox(width: ScreenUtil().setWidth(10)),
                        Container(
                          width: ScreenUtil().setWidth(360),
                          padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  SizedBox(width: ScreenUtil().setWidth(10)),
                                  Container(
                                      child: Text(
                                        // 昵称
                                        widget.userInfo?.nickname?.removeBlankSpace() ?? '',
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(24),
                                          color: widget.nickNameColor,
                                        ),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: true,
                                      ),
                                      constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(260))),
                                  YBDGenderIcon(widget.userInfo?.sex)
                                ],
                              ),
                              if (widget.userInfo?.vipIcon == null) SizedBox(height: ScreenUtil().setWidth(10)),
                              Row(
                                children: <Widget>[
                                  SizedBox(width: ScreenUtil().setWidth(10)),
                                  YBDVipIcon(widget.userInfo?.vipIcon),
                                  // 性别标签 0未知，1女2男

                                  // 用户级别标签
                                  widget.userInfo!.level != null ? YBDLevelTag(widget.userInfo!.level) : Container(),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(25)),
                        Expanded(child: SizedBox(width: 1)),
                        // 圆形选框
                        YBDCircleCheckBox(
                          selected: _isSelected,
                          selectedColor: Color(0xff62C8F9),
                          selectedBorderColor: Color(0xffC6C6C6),
                          unselectedBorderColor: Color(0xff000000).withOpacity(0.5),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setWidth(10)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void buildc08lkoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _selectedCheckBox(bool selected) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 30.px,
          height: 30.px,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: selected ? Color(0xffFFC841) : Colors.grey,
              border: Border.all(color: Colors.white, width: 1.px as double)),
        ),
        selected
            ? Image.asset(
                YBDGameResource.assetPadding('tpg_icon_correct_green', need2x: false, isWebp: true),
                width: 25.px,
                color: Color(0xff0665A5),
              )
            : Container()
      ],
    );
  }
  void _selectedCheckBoxZyd3coyelive(bool selected) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
