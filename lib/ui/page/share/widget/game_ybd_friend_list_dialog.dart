
/*
 * @Author: William
 * @Date: 2022-10-26 14:16:41
 * @LastEditTime: 2022-11-14 21:53:37
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/share/widget/game_friend_list_dialog.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/follow/entity/friend_ybd_list_entity.dart';
import 'package:oyelive_main/ui/page/follow/util/firend_ybd_util.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/text_ybd_with_stroke.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/share/widget/friend_ybd_dialog_list_item.dart';
import 'package:oyelive_main/ui/page/share/widget/friend_ybd_list_dialog.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/inner_shadow/flutter_ybd_inset_box_shadow.dart' as i;

enum GameShareType { ludo }

class YBDGameFriendListDialog extends StatefulWidget {
  /// 点 share 按钮的回调
  final ShareCallbackFunc? shareCallback;
  final GameShareType? type;

  const YBDGameFriendListDialog({Key? key, this.shareCallback, this.type}) : super(key: key);

  @override
  State<YBDGameFriendListDialog> createState() => _YBDGameFriendListDialogState();
}

class _YBDGameFriendListDialogState extends State<YBDGameFriendListDialog> {
  /// 好友列表数据
  List<YBDRoomInfo?>? _friends;

  /// 已选中的好友
  List<YBDRoomInfo?> _selectedFriends = [];

  /// 是否正在加载好友数据
  bool _isInitState = true;

  /// 最多可选择的好友数
  final int _maxFriendAmount = 20;

  @override
  void initState() {
    super.initState();
    _requestFriendsData();
  }
  void initStatejew9foyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求好友数据
  void _requestFriendsData() async {
    ApiHelper.friendList(context).then((YBDFriendListEntity? friendListEntity) {
      // 刷新数据失败
      if (friendListEntity == null || friendListEntity.returnCode != Const.HTTP_SUCCESS) {
        _cancelInitState();
      } else {
        // 好友排序
        _friends = YBDFriendUtil.sortFriends(friendListEntity.record);
        // 是当前页面则刷新界面
        _cancelInitState();
      }
    });
  }
  void _requestFriendsDataxxVjToyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消初始状态并刷新页面
  void _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }
  void _cancelInitStategJF7eoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 最多可选择的好友数
  int _allowSelectedFriendsAmount() {
    if (null != _friends && _friends!.length < _maxFriendAmount) {
      // 小于允许选择的最大好友数时显示好友数
      return _friends!.length;
    } else {
      return _maxFriendAmount;
    }
  }
  void _allowSelectedFriendsAmount3PBO3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Color> getGradient() {
    if (widget.type == GameShareType.ludo) return [Color(0xff51D9FF), Color(0xff1C58DB)];
    return [Color(0xff51D9FF), Color(0xff1C58DB)];
  }

  Color getTitleColor() {
    if (widget.type == GameShareType.ludo) return Color(0xff2F91FF);
    return Color(0xff2F91FF);
  }

  Color getBottomColor() {
    if (widget.type == GameShareType.ludo) return Color(0xff1879FF);
    return Color(0xff1879FF);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(650),
            height: ScreenUtil().setWidth(815),
            decoration: BoxDecoration(
              border: Border.all(width: 3.px as double, style: BorderStyle.solid, color: Colors.yellow),
              borderRadius: BorderRadius.all(Radius.circular(33.px as double)),
              gradient: LinearGradient(colors: getGradient(), begin: Alignment.topCenter, end: Alignment.bottomCenter),
            ),
            child: Column(
              children: <Widget>[
                // 标题
                Container(
                    height: 92.px,
                    decoration: BoxDecoration(
                      color: getTitleColor(),
                      borderRadius: BorderRadius.vertical(top: Radius.circular(32.px as double)),
                    ),
                    child: Center(
                      child: YBDGameWidgetUtil.textBalooRegular('Share to friends (${_friends?.length ?? 0})',
                          fontSize: 32.sp2),
                    )),

                // 好友列表
                _friendListContent(),

                Container(
                  width: 580.px,
                  padding: EdgeInsets.only(top: 25.px as double),
                  child: Row(
                    children: [
                      SizedBox(width: 20.px),
                      Text(
                        '${_selectedFriends.length}/${_allowSelectedFriendsAmount()}',
                        style: TextStyle(fontSize: 28.sp2, color: Colors.white),
                      ),
                      Spacer(),
                      YBDScaleAnimateButton(
                        onTap: () {
                          logger.v('clicked share friends button');

                          if (_selectedFriends.isEmpty) {
                            YBDToastUtil.toast('Please select friends');
                          } else {
                            logger.v('selected friends amount : ${_selectedFriends.length}');
                            // 关闭好友列表对话框
                            Navigator.pop(context);

                            if (null != widget.shareCallback) {
                              widget.shareCallback!(_selectedFriends);
                            } else {
                              logger.v('share button call back is null');
                            }
                          }
                        },
                        child: Container(
                          height: 70.px,
                          width: 280.px,
                          decoration: i.BoxDecoration(
                              gradient: LinearGradient(colors: [
                                Color(0xffFFCE5B),
                                Color(0xffFFBC04),
                              ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                              borderRadius: BorderRadius.all(Radius.circular(40.px as double)),
                              boxShadow: [
                                i.BoxShadow(
                                    color: Colors.white.withOpacity(0.5),
                                    offset: Offset(0, 2.px as double),
                                    blurRadius: 3.px as double,
                                    inset: true),
                                i.BoxShadow(
                                    color: Color(0xff7E5A03), offset: Offset(0, -2.px as double), blurRadius: 3.px as double, inset: true),
                              ]),
                          child: Center(
                            child: YBDTextWithStroke(
                              text: translate('share'),
                              fontSize: 32.sp2 as double,
                              fontFamily: 'baloo',
                              strokeColor: Color(0xff786000),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 53.px),
          YBDGameWidgetUtil.assetImage(
            'icon_close_blue',
            width: 58.px,
            height: 58.px,
            onTap: () {
              logger.v('clicked close friend list dialog button');
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
  void buildLT4cVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 好友列表区域显示的内容
  Widget _friendListContent() {
    if (_isInitState) {
      return Container(
        height: ScreenUtil().setWidth(554),
        child: YBDLoadingCircle(),
      );
    } else if (null == _friends || _friends!.isEmpty) {
      return Container(
        height: ScreenUtil().setWidth(554),
        child: YBDPlaceHolderView(text: translate('no_friend'), textColor: Colors.black),
      );
    } else {
      return Container(
        // 好友列表
        width: 600.px,
        height: 600.px,
        child: ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 20.px as double),
            itemBuilder: (BuildContext context, int index) {
              bool isEnable = true;
              if (_selectedFriends.length >= _allowSelectedFriendsAmount()) {
                isEnable = false;
              }

              return YBDFriendDialogListItem(
                _friends![index],
                isEnable,
                (bool selected) {
                  if (selected) {
                    if (_selectedFriends.length <= _allowSelectedFriendsAmount()) {
                      // 添加到已选好友数组
                      if (!_selectedFriends.contains(_friends![index])) {
                        _selectedFriends.add(_friends![index]);
                      }
                    } else {
                      YBDToastUtil.toast('Greater than max amount');
                      logger.v('selected friends greater than max amount');
                    }
                  } else {
                    // 从已选好友数组移除该好友
                    if (_selectedFriends.contains(_friends![index])) {
                      _selectedFriends.remove(_friends![index]);
                    }
                  }

                  // 刷新选中好友数
                  setState(() {});
                },
                selectedFriends: _selectedFriends,
                nickNameColor: Colors.white,
                type: GameShareType.ludo,
              );
            },
            itemCount: _friends!.length),
      );
    }
  }
  void _friendListContentGSjqYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
