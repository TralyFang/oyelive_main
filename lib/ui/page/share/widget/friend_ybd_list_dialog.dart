import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../follow/util/firend_ybd_util.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import 'friend_ybd_dialog_list_item.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import '../../../widget/loading_ybd_circle.dart';

/// 点 share 按钮的回调函数类型
/// [friends] 好友 id 列表
typedef ShareCallbackFunc = Function(List<YBDRoomInfo?> friends);

/// 好友分享列表对话框
class YBDFriendListDialog extends StatefulWidget {
  /// 点 share 按钮的回调
  final ShareCallbackFunc shareCallback;

  YBDFriendListDialog(this.shareCallback);

  @override
  YBDFriendListDialogState createState() => YBDFriendListDialogState();
}

class YBDFriendListDialogState extends State<YBDFriendListDialog> {
  /// 好友列表数据
  List<YBDRoomInfo?>? _friends;

  /// 已选中的好友
  List<YBDRoomInfo?> _selectedFriends = [];

  /// 是否正在加载好友数据
  bool _isInitState = true;

  /// 最多可选择的好友数
  final int _maxFriendAmount = 20;

  @override
  void initState() {
    super.initState();
    _requestFriendsData();
  }
  void initStatehpx6joyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Material(
      // 弹出对话框后背景色变暗，这里调成白色的背景
      color: Colors.white.withOpacity(0.1),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: ScreenUtil().setWidth(600),
              height: ScreenUtil().setWidth(872),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(16)))),
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  // 标题
                  Container(
                    height: ScreenUtil().setWidth(92),
                    child: Stack(
                      children: <Widget>[
                        Center(
                          child: Text(
                            // TODO: 翻译
                            'Share to friends (${_friends?.length ?? 0})',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: ScreenUtil().setSp(28),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                        Positioned(
                          // 关闭按钮
                          top: ScreenUtil().setWidth(0),
                          right: ScreenUtil().setWidth(0),
                          child: Container(
                            width: ScreenUtil().setWidth(92),
                            height: ScreenUtil().setWidth(92),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(16)))),
                                onTap: () {
                                  logger.v('clicked close friend list dialog button');
                                  Navigator.pop(context);
                                },
                                child: Icon(Icons.close_rounded),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  // rowDivider(),
                  // 好友列表
                  _friendListContent(),
                  rowDivider(),
                  Container(
                    // 已选好友和可选好友的比例
                    height: ScreenUtil().setWidth(64),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          '${_selectedFriends.length}/${_allowSelectedFriendsAmount()}',
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(24),
                            color: Color(0xff9B9B9B),
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(18)),
                      ],
                    ),
                  ),
                  Container(
                    // 分享按钮
                    height: ScreenUtil().setWidth(150),
                    child: Center(
                      child: YBDScaleAnimateButton(
                        onTap: () {
                          logger.v('clicked share friends button');

                          if (_selectedFriends.isEmpty) {
                            YBDToastUtil.toast('Please select friends');
                          } else {
                            logger.v('selected friends amount : ${_selectedFriends.length}');
                            // 关闭好友列表对话框
                            Navigator.pop(context);

                            if (null != widget.shareCallback) {
                              widget.shareCallback(_selectedFriends);
                            } else {
                              logger.v('share button call back is null');
                            }
                          }
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color(0xff47CDCC),
                                Color(0xff5E94E7),
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(36)))),
                            // boxShadow: [
                            //   BoxShadow(
                            //     color: Color(0xff3C3C3C).withOpacity(0.6),
                            //     blurRadius: 1,
                            //     offset: Offset(0, ScreenUtil().setWidth(2)),
                            //   )
                            // ],
                          ),
                          width: ScreenUtil().setWidth(440),
                          height: ScreenUtil().setWidth(72),
                          child: Center(
                            child: Text(
                              translate('share'),
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(28),
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void buildQRGQroyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 好友列表区域显示的内容
  Widget _friendListContent() {
    if (_isInitState) {
      return Container(
        height: ScreenUtil().setWidth(554),
        child: YBDLoadingCircle(),
      );
    } else if (null == _friends || _friends!.isEmpty) {
      return Container(
        height: ScreenUtil().setWidth(554),
        child: YBDPlaceHolderView(text: translate('no_friend'), textColor: Colors.black),
      );
    } else {
      return Container(
        // 好友列表
        height: ScreenUtil().setWidth(554),
        child: ListView.builder(
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(0)),
            itemBuilder: (context, index) {
              bool isEnable = true;
              if (_selectedFriends.length >= _allowSelectedFriendsAmount()) {
                isEnable = false;
              }

              return YBDFriendDialogListItem(
                _friends![index],
                isEnable,
                (selected) {
                  if (selected) {
                    if (_selectedFriends.length <= _allowSelectedFriendsAmount()) {
                      // 添加到已选好友数组
                      if (!_selectedFriends.contains(_friends![index])) {
                        _selectedFriends.add(_friends![index]);
                      }
                    } else {
                      // TODO: 翻译
                      YBDToastUtil.toast('Greater than max amount');
                      logger.v('selected friends greater than max amount');
                    }
                  } else {
                    // 从已选好友数组移除该好友
                    if (_selectedFriends.contains(_friends![index])) {
                      _selectedFriends.remove(_friends![index]);
                    }
                  }

                  // 刷新选中好友数
                  setState(() {});
                },
                selectedFriends: _selectedFriends,
              );
            },
            itemCount: _friends!.length),
      );
    }
  }
  void _friendListContent2pvxkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 最多可选择的好友数
  int _allowSelectedFriendsAmount() {
    if (null != _friends && _friends!.length < _maxFriendAmount) {
      // 小于允许选择的最大好友数时显示好友数
      return _friends!.length;
    } else {
      return _maxFriendAmount;
    }
  }
  void _allowSelectedFriendsAmountXN36ioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // TODO: 跳转到个人详情页解除好友关系后要更新好友列表
  /// 请求好友数据
  _requestFriendsData() async {
    ApiHelper.friendList(context).then((friendListEntity) {
      // 刷新数据失败
      if (friendListEntity == null || friendListEntity.returnCode != Const.HTTP_SUCCESS) {
        _cancelInitState();
      } else {
        // 好友排序
        _friends = YBDFriendUtil.sortFriends(friendListEntity.record);
        // 是当前页面则刷新界面
        _cancelInitState();
      }
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }

  /// 分割线
  Divider rowDivider() {
    return Divider(
      height: ScreenUtil().setWidth(1),
      thickness: ScreenUtil().setWidth(1),
      color: Color(0xffDEDCDC),
    );
  }
}
