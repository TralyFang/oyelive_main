import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 好友分享的导航栏
// TODO: 可以提取出共用的导航栏控件
class YBDFriendShareNavBar extends StatelessWidget {
  /// 导航栏标题
  final String title;

  /// 子组件不能为 null
  final Widget body;

  /// 右上角按钮 不能为 null，没有 action 传空数组 []
  final List<Widget> actions;

  YBDFriendShareNavBar(this.title, this.actions, this.body);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color(0xffA33CCA),
        leading: Container(
          width: ScreenUtil().setWidth(88),
          child: TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            style: TextButton.styleFrom(padding: EdgeInsets.all(ScreenUtil().setWidth(20))),
            // padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
          ),
        ),
        actions: actions,
      ),
      body: body,
    );
  }
  void build7WrQkoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
