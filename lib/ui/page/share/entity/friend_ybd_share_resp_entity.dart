import 'dart:async';

import '../../../../generated/json/base/json_convert_content.dart';

class YBDFriendShareRespEntity with JsonConvert<YBDFriendShareRespEntity> {
  String? code;
  YBDFriendShareRespData? data;
}

class YBDFriendShareRespData with JsonConvert<YBDFriendShareRespData> {
  List<YBDFriendShareRespDataRespList?>? respList;
}

class YBDFriendShareRespDataRespList with JsonConvert<YBDFriendShareRespDataRespList> {
  String? messageId;
  String? code;
  String? msg;
  String? requestId;
  bool? success;
}
