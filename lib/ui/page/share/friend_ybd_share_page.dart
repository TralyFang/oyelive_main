import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/constant/const.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/entity/room_ybd_info_entity.dart';
import '../follow/util/firend_ybd_util.dart';
import '../follow/widget/place_ybd_holder_view.dart';
import 'util/friend_ybd_share_util.dart';
import 'widget/friend_ybd_dialog_list_item.dart';
import 'widget/friend_ybd_share_nav_bar.dart';
import '../status/local_audio/scale_ybd_animate_button.dart';
import '../../widget/loading_ybd_circle.dart';

/// 好友分享页面
class YBDFriendSharePage extends StatefulWidget {
  /// 房间 id
  final int? roomId;

  /// 主播昵称
  final String? nickName;

  /// 房间海报
  final String? roomImg;

  /// 活动链接
  final String? adUrl;

  /// 活动名称 或者勋章title type区分
  final String? adName;

  /// 活动图片 或者勋章图片 type区分
  final String? adImg;

  /// 分享类型
  final FriendShareType type;

  YBDFriendSharePage(
    this.type, {
    this.roomId,
    this.nickName,
    this.roomImg,
    this.adUrl,
    this.adName,
    this.adImg,
  });

  @override
  _YBDFriendSharePageState createState() => _YBDFriendSharePageState();
}

class _YBDFriendSharePageState extends BaseState<YBDFriendSharePage> {
  /// 好友列表数据
  List<YBDRoomInfo?>? _friends;

  /// 已选中的好友
  List<YBDRoomInfo?> _selectedFriends = [];

  /// 是否正在加载好友数据
  bool _isInitState = true;

  @override
  void initState() {
    super.initState();

    // 查询我的好友列表
    _requestFriendsData();
  }
  void initState88BWKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return YBDFriendShareNavBar(
      // TODO: 翻译
      'Share to friends',
      [_navShareButton()],
      Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color(0xffA33CCA), Color(0xff32249D)],
          ),
        ),
        child: _pageContent(),
      ),
    );
  }
  void myBuildC8HIAoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 导航栏右上角的分享按钮
  Widget _navShareButton() {
    return YBDScaleAnimateButton(
      onTap: () async {
        logger.v('clicked share button');
        // TODO: 添加响应事件
        if (_selectedFriends.isEmpty) {
          YBDToastUtil.toast('Please select friends');
        } else {
          logger.v('selected friends amount : ${_selectedFriends.length}');
          // 分享给好友
          bool result = await YBDFriendShareUtil.shareToFriends(
            context,
            widget.type,
            // TODO: 添加广告数据字段  添加勋章支持
            widget.type != FriendShareType.ShareBadge
                ? {
                    'id': widget.roomId,
                    'nickname': widget.nickName,
                    'roomimg': widget.roomImg,
                    'adUrl': widget.adUrl,
                    'adName': widget.adName,
                    'adImg': widget.adImg,
                  }
                : {
                    'image': widget.adImg,
                    'title': widget.adName,
                  },
            _selectedFriends,
          );

          if (result) {
            // 分享成功退出好友分享列表页面
            YBDToastUtil.toast(translate("sharing_succeeded"));

            if (Navigator.canPop(context)) Navigator.pop(context);
          } else {
            // 分享失败停留在好友分享列表页面
            logger.v('share failed');
          }
        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(20),
          vertical: ScreenUtil().setWidth(25),
        ),
        child: Container(
          width: ScreenUtil().setWidth(125),
          decoration: BoxDecoration(
            color: Color(0xff5694FA),
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
          ),
          child: Center(
            child: Text(
              // TODO: 翻译
              'Share',
              style: TextStyle(
                fontSize: ScreenUtil().setSp(28),
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
  void _navShareButton94V9Noyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 页面内容
  Widget _pageContent() {
    if (_isInitState) {
      // 初始状态显示加载框
      return Container(
        child: YBDLoadingCircle(),
      );
    } else if (null == _friends || _friends!.isEmpty) {
      // 没有好友显示缺省图
      return Container(
        width: ScreenUtil().screenWidth,
        height: ScreenUtil().screenHeight,
        child: YBDPlaceHolderView(text: translate('no_friend')),
      );
    } else {
      // 好友列表
      return Container(
        child: Column(
          children: <Widget>[
            _friendListHead(),
            _friendListContent(),
          ],
        ),
      );
    }
  }
  void _pageContentxZK98oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 好友总数和选中好友比例
  Widget _friendListHead() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(30),
        vertical: ScreenUtil().setWidth(30),
      ),
      child: Row(
        children: <Widget>[
          Text(
            // 好友总数
            'You have ${_friends?.length ?? 0} friends',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(24),
              color: Colors.white,
            ),
          ),
          Expanded(child: SizedBox(width: 1)),
          Text(
            // 选中好友数与最大可选好友数的比例
            '${_selectedFriends.length}/${YBDFriendShareUtil.maxAllowedFriend(_friends)}',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(24),
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
  void _friendListHeadB0iWKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 好友列表区域显示的内容
  Widget _friendListContent() {
    return Container(
      // 好友列表
//      height: ScreenUtil().setWidth(554),
      child: Expanded(
        child: ListView.builder(
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(20)),
            itemBuilder: (context, index) {
              bool isEnable = true;
              if (_selectedFriends.length >= YBDFriendShareUtil.maxAllowedFriend(_friends)) {
                isEnable = false;
              }

              return YBDFriendDialogListItem(
                _friends![index],
                isEnable,
                (selected) {
                  if (selected) {
                    if (_selectedFriends.length <= YBDFriendShareUtil.maxAllowedFriend(_friends)) {
                      // 添加到已选好友数组
                      if (!_selectedFriends.contains(_friends![index])) {
                        _selectedFriends.add(_friends![index]);
                      }
                    } else {
                      // TODO: 翻译
                      YBDToastUtil.toast('Greater than max amount');
                      logger.v('selected friends greater than max amount');
                    }
                  } else {
                    // 从已选好友数组移除该好友
                    if (_selectedFriends.contains(_friends![index])) {
                      _selectedFriends.remove(_friends![index]);
                    }
                  }

                  // 刷新选中好友数
                  setState(() {});
                },
                bgColor: Colors.transparent,
                nickNameColor: Colors.white,
              );
            },
            itemCount: _friends!.length),
      ),
    );
  }
  void _friendListContentN8TULoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // TODO: 跳转到个人详情页解除好友关系后要更新好友列表
  /// 请求好友数据
  _requestFriendsData() async {
    ApiHelper.friendList(context).then((friendListEntity) {
      // 刷新数据失败
      if (friendListEntity == null || friendListEntity.returnCode != Const.HTTP_SUCCESS) {
        _cancelInitState();
      } else {
        // 好友排序
        _friends = YBDFriendUtil.sortFriends(friendListEntity.record);

        // 是当前页面则刷新界面
        _cancelInitState();
      }
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }
}
