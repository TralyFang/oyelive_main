import 'dart:async';


import 'package:flutter/material.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

class YBDTextWithStroke extends StatelessWidget {
  String? text;
  String? fontFamily;
  double fontSize;
  double? strokeWidth;
  Color textColor;
  Color strokeColor;

  YBDTextWithStroke(
      {this.text,
      this.fontFamily,
      this.fontSize: 12,
      this.strokeWidth,
      this.textColor: Colors.white,
      this.strokeColor: Colors.black});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Text(
          text!,
          style: TextStyle(
            fontSize: fontSize,
            fontFamily: fontFamily,
            foreground: Paint()
              ..style = PaintingStyle.stroke
              ..strokeWidth = strokeWidth ?? 2.px.toDouble()
              ..color = strokeColor,
          ),
        ),
        Text(text!, style: TextStyle(fontFamily: fontFamily, fontSize: fontSize, color: textColor)),
      ],
    );
  }
  void buildrbYZQoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
