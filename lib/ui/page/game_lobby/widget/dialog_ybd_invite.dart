import 'dart:async';


// import 'package:flutter/material.dart';
import 'dart:io';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_lobby/enter_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/btn_ybd_invite.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/item_ybd_friend.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/search_ybd_top.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:flutter/material.dart' hide BoxDecoration, BoxShadow;
import 'package:oyelive_main/ui/widget/inner_shadow/flutter_ybd_inset_box_shadow.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';

enum ShowType { FriendList, Searching, SearchedResults }

class YBDInviteDialog extends StatefulWidget {
  @override
  YBDInviteDialogState createState() => new YBDInviteDialogState();
  YBDTpRoomDataUser? gameData;

  String? roomId;
  YBDInviteDialog(this.gameData, this.roomId);

  static show(YBDTpRoomDataUser? gameData, String? roomId) {
    showDialog<Null>(
        context: Get.context!, //BuildContext对象
        barrierDismissible: false,
        builder: (BuildContext context) {
          return YBDInviteDialog(gameData, roomId);
        });
  }
}

class YBDInviteDialogState extends BaseState<YBDInviteDialog> {
  ShowType showType = ShowType.FriendList;

  final pageSize = 50;
  final noLimit = 999;

  int page = 0;
  List<YBDRoomInfo?>? _friends;
  List<YBDRoomInfo?>? _searchResult;

  List<YBDFriendStatus> friendStatusList = [];
  List<YBDFriendStatus> resultStatusList = [];

  _getFriendList({int page: 0, bool isSearch: false, String? keyWord}) {
    if (isSearch) YBDDialogUtil.showLoading(Get.context);
    ApiHelper.friendList(context,
            page: isSearch ? 0 : page, pageSize: isSearch ? noLimit : pageSize, keyWord: keyWord, enablePage: true)
        .then((friendListEntity) {
      // 刷新数据失败
      if (isSearch) YBDDialogUtil.hideLoading(Get.context);
      _cancelInitState();
      if (friendListEntity == null || friendListEntity.returnCode != Const.HTTP_SUCCESS) {
        YBDToastUtil.toast("Error");
      } else {
        //搜索
        if (isSearch) {
          if (friendListEntity.record!.isEmpty) {
            YBDToastUtil.toast("No results!");
            return;
          }
          showType = ShowType.SearchedResults;
          _searchResult = friendListEntity.record;
          resultStatusList = List.generate(
              friendListEntity.record!.length,
              (index) => YBDFriendStatus()
                ..btnStatus = (friendListEntity.record![index]!.isOnline ?? true) ? BtnStatus.Init : BtnStatus.Offline
                ..isSeleted = false);
          setState(() {});
          return;
        }

        //好友列表
        if (_refreshController.isLoading) _refreshController.loadComplete();

        if (friendListEntity.record!.length < pageSize) _refreshController.loadNoData();

        if (_friends == null) {
          _friends = friendListEntity.record;
          friendStatusList = List.generate(
              friendListEntity.record!.length,
              (index) => YBDFriendStatus()
                ..btnStatus = (friendListEntity.record![index]!.isOnline ?? true) ? BtnStatus.Init : BtnStatus.Offline
                ..isSeleted = false);
        } else {
          _friends!.addAll(friendListEntity.record!);
          friendStatusList.addAll(List.generate(
              friendListEntity.record!.length,
              (index) => YBDFriendStatus()
                ..btnStatus = (friendListEntity.record![index]!.isOnline ?? true) ? BtnStatus.Init : BtnStatus.Offline
                ..isSeleted = false));
        }
        setState(() {});
      }
    });
  }

  _nextPage() {
    _getFriendList(page: ++page);
  }

  _title() {
    //
    return Container(
      height: 77.px,
      decoration:
          BoxDecoration(color: Color(0xff0C7D4F), borderRadius: BorderRadius.vertical(top: Radius.circular(28.px.toDouble()))),
      child: Row(
        children: [
          SizedBox(
            width: 86.px.toDouble(),
          ),
          Spacer(),
          Text(
            "Invite Friends To Table",
            style: TextStyle(fontSize: 32.sp, color: Colors.white, fontFamily: "baloo"),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              showType = ShowType.Searching;
              setState(() {});
            },
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.px.toDouble(), vertical: 14.px.toDouble()),
              child: Image.asset(
                YBDGameResource.assetPadding(
                  'tpg_icon_search',
                  need2x: false,
                  isWebp: true,
                ),
                width: 52.px.toDouble(),
                // height: ,
              ),
            ),
          ),
          SizedBox(
            width: 10.px,
          )
        ],
      ),
    );
  }

  _selectedCheckBox() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 34.px.toDouble(),
          height: 34.px.toDouble(),
          decoration: BoxDecoration(
              shape: BoxShape.circle, color: Color(0xffFFC841), border: Border.all(color: Colors.white, width: 1.px as double)),
        ),
        Image.asset(
          YBDGameResource.assetPadding("tpg_icon_correct_green", need2x: false, isWebp: true),
          width: 22.px.toDouble(),
        )
      ],
    );
  }

  bool selectAll = false;
  _checkBox(bool status) {
    if (status)
      return _selectedCheckBox();
    else
      return Container(
        width: 34.px.toDouble(),
        height: 34.px.toDouble(),
        decoration: BoxDecoration(
            shape: BoxShape.circle, color: Color(0xffadadad), border: Border.all(color: Colors.white, width: 1.px as double)),
      );
  }

  /// 是否正在加载好友数据
  bool _isInitState = true;

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }

  _selectAllChange() {
    friendStatusList.forEach((element) {
      element.isSeleted = selectAll && (element.btnStatus == BtnStatus.Init);
    });
  }

  RefreshController _refreshController = RefreshController();

  void _checkSelectAll() {
    selectAll = selectedFriendIndex.isNotEmpty &&
        friendStatusList.where((element) => !element.isSeleted && element.btnStatus == BtnStatus.Init).isEmpty;
  }
  void _checkSelectAll1wmjboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget friendListContent() {
    if (_friends!.isEmpty) return emptyView();
    return SmartRefresher(
      controller: _refreshController,
      header: YBDMyRefreshIndicator.myHeader,
      enablePullDown: false,
      enablePullUp: true,
      onLoading: () {
        _nextPage();
      },
      child: ListView.separated(
        itemBuilder: (_, i) => GestureDetector(
            onTap: () {
              if (friendStatusList[i].btnStatus == BtnStatus.Init) {
                friendStatusList[i].isSeleted = !friendStatusList[i].isSeleted;
                _checkSelectedFriends();
                setState(() {});
              }
            },
            child: YBDFriendItem(_friends![i], friendStatusList[i], widget.gameData, widget.roomId)),
        separatorBuilder: (_, i) => SizedBox(
          height: 15.px,
        ),
        itemCount: _friends!.length,
      ),
    );
  }
  void friendListContentRn9l3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget searchResultContent() {
    if (_searchResult!.isEmpty) return emptyView();
    return ListView.separated(
      itemBuilder: (_, i) => YBDFriendItem(_searchResult![i], resultStatusList[i], widget.gameData, widget.roomId),
      separatorBuilder: (_, i) => SizedBox(
        height: 15.px,
      ),
      itemCount: _searchResult!.length,
    );
  }
  void searchResultContentAeDIfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget emptyView() {
    return Center(
      child: Text("No data"),
    );
  }

  BtnStatus? selectAllBtnStatus;
  _checkSelectAllBtnStatus() {
    bool isAnySelected = friendStatusList.where((element) => element.isSeleted).isNotEmpty;
    selectAllBtnStatus = isAnySelected ? BtnStatus.Init : BtnStatus.Invited;
  }

  List<int> selectedFriendIndex = [];

  void _checkSelectedFriends() {
    List<int> result = [];
    friendStatusList.asMap().forEach((index, value) {
      if (value.isSeleted) {
        result.add(index);
      }
    });
    selectedFriendIndex = result;
  }
  void _checkSelectedFriendsYYVtLoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<YBDRoomInfo?> _getSelectedFriends() {
    List<YBDRoomInfo?> result = [];
    selectedFriendIndex.forEach((element) {
      result.add(_friends![element]);
    });
    return result;
  }

  List<YBDFriendStatus> _getSelectedFriendStatus({BtnStatus? changeStatus, bool? isSlected}) {
    List<YBDFriendStatus> result = [];
    selectedFriendIndex.forEach((element) {
      YBDFriendStatus data = friendStatusList[element];
      if (changeStatus != null) {
        data.btnStatus = changeStatus;
      }
      if (isSlected != null) {
        data.isSeleted = isSlected;
      }
      result.add(data);
    });
    return result;
  }
  void _getSelectedFriendStatusQx6rhoyelive({BtnStatus? changeStatus, bool? isSlected}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild

    if (_isInitState) {
      // 初始状态显示加载框
      return Container(
        child: YBDLoadingCircle(),
      );
    }
    _checkSelectAll();

    if (selectAllBtnStatus != BtnStatus.Processing) {
      _checkSelectAllBtnStatus();
    }

    print("ssds${selectAllBtnStatus.toString()}");
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 108.px,
          ),
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(32.px as double)),
            child: Container(
              width: 628.px,
              height: 780.px,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 2.px as double,
                  color: Color(0xffFCCE63),
                ),
                borderRadius: BorderRadius.all(Radius.circular(32.px as double)),
              ),
              child: Column(
                children: [
                  if (showType == ShowType.FriendList) _title(),
                  if (showType != ShowType.FriendList)
                    YBDSearchTop(
                      onCancel: () {
                        _searchResult?.clear();
                        showType = ShowType.FriendList;
                        setState(() {});
                      },
                      onSubmitted: (tx) {
                        YBDCommonTrack().tpgo('search_text');
                        _getFriendList(isSearch: true, keyWord: tx);
                      },
                      onClear: () {
                        _searchResult?.clear();
                        showType = ShowType.Searching;

                        setState(() {});
                      },
                    ),
                  Expanded(
                    child: ClipRRect(
                      borderRadius: BorderRadius.vertical(bottom: Radius.circular(28.px.toDouble())),
                      child: Stack(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                              Color(0xff00b367),
                              Color(0xff106542),
                            ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
                          ),
                          // Image.asset(
                          //   YBDGameResource.assetPadding(
                          //     'tpg_iv_bg',
                          //     need2x: false,
                          //     isWebp: true,
                          //   ),
                          //   // width: 32.px,
                          // ),
                          Padding(
                            padding: EdgeInsets.only(top: 24.px as double, bottom: 115.px as double),
                            child: showType == ShowType.SearchedResults ? searchResultContent() : friendListContent(),
                          ),
                          if (showType != ShowType.SearchedResults)
                            Positioned(
                                bottom: 0,
                                left: 0,
                                right: 0,
                                child: Container(
                                  height: 115.px,
                                  decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
                                  child: Row(
                                    children: [
                                      SizedBox(width: Platform.isAndroid ? 20.px.toDouble() : 24.px.toDouble()),
                                      GestureDetector(
                                        onTap: () {
                                          selectAll = !selectAll;
                                          if (selectAll) YBDCommonTrack().tpgo('select_all');
                                          _selectAllChange();
                                          _checkSelectedFriends();
                                          setState(() {});
                                        },
                                        child: Padding(
                                          padding: EdgeInsets.all(20),
                                          child: Transform.scale(scale: 1.3, child: _checkBox(selectAll)),
                                        ),
                                      ),
                                      Text.rich(
                                          TextSpan(style: TextStyle(color: Colors.white, fontSize: 28.sp), children: [
                                        TextSpan(text: "Select All "),
                                        TextSpan(
                                            text: "(${selectedFriendIndex.length})",
                                            style: TextStyle(color: Colors.white.withOpacity(0.6))),
                                      ])),
                                      Spacer(),
                                      GestureDetector(
                                        onTap: () {
                                          if (selectAllBtnStatus == BtnStatus.Init) {
                                            YBDCommonTrack().tpgo('select_all_invite');
                                            setState(() {
                                              selectAllBtnStatus = BtnStatus.Processing;

                                              _getSelectedFriendStatus(changeStatus: BtnStatus.Processing);
                                            });
                                            Future.delayed(Duration(seconds: 1), () {
                                              YBDEnterHelper()
                                                  .shareTofriends(_getSelectedFriends(), widget.gameData!.nickname,
                                                      widget.roomId, widget.gameData!.avatarUrl(),
                                                      needLoading: false)
                                                  .then(
                                                (isSuccess) {
                                                  _getSelectedFriendStatus(
                                                      changeStatus: BtnStatus.Invited, isSlected: false);
                                                  _checkSelectAllBtnStatus();
                                                  selectedFriendIndex.clear();
                                                  setState(() {});
                                                },
                                              );
                                            });
                                          }
                                        },
                                        child: YBDInviteBtn(selectAllBtnStatus),
                                      ),
                                      SizedBox(
                                        width: 42.px,
                                      )
                                    ],
                                  ),
                                )),
                          if (showType == ShowType.Searching)
                            Positioned.fill(
                                child: GestureDetector(
                              child: Container(
                                color: Colors.black.withOpacity(0.5),
                              ),
                            ))
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 50.px,
          ),
          GestureDetector(
            onTap: () {
              YBDNavigatorHelper.popPage(context);
            },
            child: Image.asset(
              YBDGameResource.assetPadding("tpg_icon_close_green", isWebp: true, need2x: false),
              width: 58.px,
            ),
          )
        ],
      ),
    );
  }
  void myBuildisnAWoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getFriendList(page: page);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDInviteDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}

class YBDFriendStatus {
  BtnStatus? btnStatus;
  late bool isSeleted;
}
