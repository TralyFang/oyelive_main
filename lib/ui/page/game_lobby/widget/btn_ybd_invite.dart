import 'dart:async';


import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:flutter/material.dart' hide BoxDecoration, BoxShadow;
import 'package:oyelive_main/ui/widget/inner_shadow/flutter_ybd_inset_box_shadow.dart';

import 'text_ybd_with_stroke.dart';

enum BtnStatus { Init, Processing, Invited, Offline }

class YBDInviteBtn extends StatelessWidget {
  BtnStatus? status;

  YBDInviteBtn(this.status);

  @override
  Widget build(BuildContext context) {
    String? text;
    late List<Color> colors;
    List<BoxShadow>? shadows;
    Color? color;
    switch (status) {
      case BtnStatus.Init:
        // TODO: Handle this case.
        // background = YBDGameResource.assetPadding("tpg_btn_bg_yellow", isWebp: true, need2x: false);
        colors = [
          Color(0xffFFCE5B),
          Color(0xffFFBC04),
        ];
        text = "Invite";
        shadows = [
          BoxShadow(color: Colors.white.withOpacity(0.5), offset: Offset(0, 2.px as double), blurRadius: 3.px as double, inset: true),
          BoxShadow(color: Color(0xff7E5A03), offset: Offset(0, -2.px as double), blurRadius: 3.px as double, inset: true),
        ];
        break;
      case BtnStatus.Processing:
        // TODO: Handle this case.
        // background = YBDGameResource.assetPadding("tpg_btn_bg_white", isWebp: true, need2x: false);
        colors = [
          Color(0xffFFCE5B),
          Color(0xffFFBC04),
        ];
        text = "...";
        shadows = [
          BoxShadow(color: Colors.white.withOpacity(0.5), offset: Offset(0, 2.px as double), blurRadius: 3.px as double, inset: true),
          BoxShadow(color: Color(0xff7E5A03), offset: Offset(0, -2.px as double), blurRadius: 3.px as double, inset: true),
        ];
        break;
      case BtnStatus.Invited:
        // TODO: Handle this case.
        // background = YBDGameResource.assetPadding("tpg_btn_bg_white", isWebp: true, need2x: false);
        colors = [
          // Color(0xffE7E7E7),
          // Color(0xffADADAD),
        ];
        text = "Invite";
        shadows = [
          BoxShadow(color: Colors.white.withOpacity(0.5), offset: Offset(0, 2.px as double), blurRadius: 3.px as double, inset: true),
          BoxShadow(color: Color(0xff737373), offset: Offset(0, -2.px as double), blurRadius: 3.px as double, inset: true),
        ];
        color = Color(0xffe7e7e7).withOpacity(0.7);
        break;
      case BtnStatus.Offline:
        // TODO: Handle this case.
        // background = YBDGameResource.assetPadding("tpg_btn_bg_white", isWebp: true, need2x: false);
        text = "Offline";
        colors = [
          // Color(0xffE7E7E7),
          // Color(0xffADADAD),
        ];
        shadows = [
          BoxShadow(color: Colors.white.withOpacity(0.5), offset: Offset(0, 2.px as double), blurRadius: 3.px as double, inset: true),
          BoxShadow(color: Color(0xff737373), offset: Offset(0, -2.px as double), blurRadius: 3.px as double, inset: true),
        ];
        color = Color(0xffe7e7e7).withOpacity(0.7);
        break;
    }

    return Container(
      width: 154.px,
      height: 61.px,
      decoration: BoxDecoration(
          gradient: colors.isEmpty
              ? null
              : LinearGradient(colors: colors, begin: Alignment.topCenter, end: Alignment.bottomCenter),
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(40.px as double)),
          boxShadow: shadows),
      child: Center(
        // child: Text(
        //   text,
        //   style: TextStyle(fontSize: 30.sp, color: Colors.white),
        // ),
        child: status == BtnStatus.Processing
            ? Container(
                width: 32.px,
                height: 32.px,
                child: CircularProgressIndicator(
                  strokeWidth: 2.px as double,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              )
            : YBDTextWithStroke(
                text: text,
                fontSize: 30.sp as double,
                fontFamily: 'baloo',
                strokeWidth: 2.px.toDouble(),
                strokeColor: (status?.index ?? 0) < 2 ? Color(0xff786000) : Color(0xff4b4b4b),
              ),
      ),
    );
  }
  void buildXgYCwoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
