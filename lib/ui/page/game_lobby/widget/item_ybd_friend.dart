import 'dart:async';


import 'package:flutter/material.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_lobby/enter_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';

import 'btn_ybd_invite.dart';
import 'dialog_ybd_invite.dart';

class YBDFriendItem extends StatefulWidget {
  YBDRoomInfo? data;
  YBDFriendStatus status;
  YBDTpRoomDataUser? gameData;
  String? roomId;
  YBDFriendItem(this.data, this.status, this.gameData, this.roomId);

  @override
  YBDFriendItemState createState() => new YBDFriendItemState();
}

class YBDFriendItemState extends BaseState<YBDFriendItem> {
  _selectedCheckBox() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 29.px,
          height: 29.px,
          decoration: BoxDecoration(
              shape: BoxShape.circle, color: Color(0xffFFC841), border: Border.all(color: Colors.white, width: 1.px as double)),
        ),
        Image.asset(
          YBDGameResource.assetPadding("tpg_icon_correct_green", need2x: false, isWebp: true),
          width: 19.px,
        )
      ],
    );
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Align(
      alignment: Alignment.center,
      child: Container(
        height: 86.px,
        width: 576.px,
        decoration: BoxDecoration(
            color: Color(0xff015A32).withOpacity(0.3), borderRadius: BorderRadius.all(Radius.circular(15.px as double))),
        child: Row(
          children: [
            SizedBox(
              width: 18.px,
            ),
            if (widget.status.isSeleted)
              Padding(
                padding: EdgeInsets.all(20.px as double),
                child: _selectedCheckBox(),
              ),
            Container(
              width: 68.px.toDouble(),
              height: 68.px.toDouble(),
              decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(width: 1.px.toDouble(), color: Colors.white)),
              // padding: EdgeInsets.all(1.px),
              child: ClipOval(
                  child: Image.network(
                YBDImageUtil.avatar(context, widget.data?.headimg, widget.data?.id, scene: "A"),
                width: 68.px.toDouble(),
                height: 68.px.toDouble(),
                fit: BoxFit.cover,
                errorBuilder: (
                  BuildContext context,
                  Object error,
                  StackTrace? stackTrace,
                ) {
                  return Image.asset(
                    YBDGameResource.assetPadding(
                      "invite_defaul_head",
                      need2x: false,
                    ),
                    width: 68.px.toDouble(),
                  );
                },
              )),
            ),
            SizedBox(
              width: 18.px,
            ),

            Expanded(
              child: Text(
                widget.data!.nickname!,
                style: TextStyle(
                  fontSize: 28.sp,
                  color: Colors.white,
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
            // Spacer(
            //
            // ),
            GestureDetector(
                onTap: () {
                  if (widget.status.btnStatus == BtnStatus.Init) {
                    YBDCommonTrack().tpgo('invite');
                    widget.status.btnStatus = BtnStatus.Processing;
                    setState(() {});
                    YBDEnterHelper().shareTofriends(
                        [widget.data], widget.gameData!.nickname, widget.roomId, widget.gameData!.avatarUrl(),
                        needLoading: false).then(
                      (isSuccess) {
                        widget.status.btnStatus = BtnStatus.Invited;
                        widget.status.isSeleted = false;
                        setState(() {});
                      },
                    );
                  }
                },
                child: YBDInviteBtn(widget.status.btnStatus)),
            SizedBox(
              width: 18.px,
            )
          ],
        ),
      ),
    );
  }
  void myBuildukks5oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStateZIAJsoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDFriendItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetj29hIoyelive(YBDFriendItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
