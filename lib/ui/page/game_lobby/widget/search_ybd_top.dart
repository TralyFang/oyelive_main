import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';

class YBDSearchTop extends StatefulWidget {
  Function? onCancel, onSubmitted, onClear;

  YBDSearchTop({this.onCancel, this.onSubmitted(String tx)?, this.onClear});
  @override
  YBDSearchTopState createState() => new YBDSearchTopState();
}

class YBDSearchTopState extends State<YBDSearchTop> {
  TextEditingController textEditingController = TextEditingController();

  FocusNode focus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 77.px,
      decoration:
          BoxDecoration(color: Color(0xff00b067), borderRadius: BorderRadius.vertical(top: Radius.circular(32.px as double))),
      child: Row(
        children: [
          SizedBox(
            width: 24.px,
          ),
          Container(
            height: 60.px,
            width: 466.px,
            decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(30.px as double)), color: Color(0xff096D44)),
            child: Row(
              children: [
                SizedBox(
                  width: 14.px,
                ),
                Image.asset(
                  YBDGameResource.assetPadding(
                    'tpg_icon_search',
                    need2x: false,
                    isWebp: true,
                  ),
                  width: 36.px.toDouble(),
                  // height: ,
                ),
                SizedBox(
                  width: 14.px,
                ),
                Expanded(
                  child: TextField(
                    focusNode: focus,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        isDense: true,
                        contentPadding: EdgeInsets.all(0),
                        hintText: "Search by user ID/name",
                        hintStyle: TextStyle(fontSize: 27.sp, color: Colors.white.withOpacity(0.8)),
                        counterText: ''),
                    maxLength: 20,
                    textInputAction: TextInputAction.search,
                    onSubmitted: (tx) {
                      if (tx.isNotEmpty) {
                        widget.onSubmitted?.call(tx);
                        // textEditingController.text = tx;

                        Future.delayed(Duration(seconds: 2), () {});
                      } else
                        YBDToastUtil.toast("Please input something!");
                    },
                    controller: textEditingController,
                    cursorColor: Colors.white.withOpacity(0.8),
                    style: TextStyle(fontSize: 27.sp, color: Colors.white.withOpacity(0.8)),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    textEditingController.clear();
                    widget.onClear?.call();
                  },
                  child: Padding(
                    padding: EdgeInsets.only(left: 34.px.toDouble(), right: 14.px.toDouble(), top: 16.px.toDouble(), bottom: 16.px.toDouble()),
                    child: Image.asset(
                      YBDGameResource.assetPadding(
                        'tpg_icon_clear',
                        need2x: false,
                        isWebp: true,
                      ),
                      width: 32.px,
                      // height: ,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                widget.onCancel?.call();
              },
              child: Container(
                child: Center(
                  child: Text(
                    "Cancel",
                    style: TextStyle(fontSize: 27.sp, color: Colors.white),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
  void build9CVrUoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    focus.requestFocus();
  }
  void initState8pdEyoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDSearchTop oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetGFDLQoyelive(YBDSearchTop oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
