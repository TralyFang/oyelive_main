import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_category_entity.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/game_ybd_hall_list.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/widget/game_ybd_hall_start.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';

import '../game_room/widget/game_ybd_resource_util.dart';

class YBDGameRoomListPage extends StatefulWidget {
  String title, subCategory;

  YBDGameRoomListPage(this.title, this.subCategory);

  @override
  YBDRoomListPageState createState() => new YBDRoomListPageState();
}

class YBDRoomListPageState extends BaseState<YBDGameRoomListPage> {
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            // image: DecorationImage(
            //     image:
            //         AssetImage(YBDGameResource.assetPadding('game_list_bg', need2x: false, isWebp: false, suffix: "jpg")),
            //     fit: BoxFit.cover)
            gradient: LinearGradient(colors: [
          Color(0xff7B42CE),
          Color(0xff6713C1),
        ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
        child: Column(
          children: [
            Container(
              height: ScreenUtil().statusBarHeight,
              // decoration: BoxDecoration(
              //   gradient: LinearGradient(colors: [
              //     Color(0xff4D0BB9),
              //     Color(0xff8D3AFA),
              //   ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
              // ),
              child: Image.asset(
                YBDGameResource.assetPadding(
                  'picture_lamp',
                  need2x: false,
                  isWebp: true,
                ),
                fit: BoxFit.fitWidth,
              ),
            ),
            Container(
              height: 84.px,
              child: Row(
                children: [
                  Container(
                    width: ScreenUtil().setWidth(120),
                    height: ScreenUtil().setWidth(80),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          YBDNavigatorHelper.backAction(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Spacer(),
                  Text(
                    widget.title,
                    style: TextStyle(fontSize: 30.sp2, color: Colors.white, fontWeight: FontWeight.w500),
                  ),
                  Spacer(),
                  SizedBox(
                    width: 120.px,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 26.px,
            ),
            YBDGameHallStart(
              canJoinRoom: true,
              isTpgo: widget.subCategory == GameConst.GAME_SUB_TPGO,
            ),
            SizedBox(
              height: 10.px,
            ),
            Expanded(
                child: YBDGameHallList(
              subCategory: widget.subCategory,
            ))
          ],
        ),
      ),
    );
  }
  void myBuildJasZJoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

//加载的页数
  int _page = 1;
  List<YBDGameCategoryData> _gamesData = [];

  YBDGameCategoryData? _selectedCategory;

  /// 游戏分类列表
  Future<YBDGameCategoryData?> _gameCategorys() async {
    List<YBDGameCategoryData?>? gameCategory = _gamesData;
    if (_page == 1 && gameCategory.isEmpty) {
      // 刷新了就跟着刷新
      gameCategory = await ApiHelper.gameCategorySearch(context);
    }

    return null;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    YBDRtcHelper.getInstance().setGameSubType(widget.subCategory);
  }
  void initState8nchpoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDGameRoomListPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetVcW06oyelive(YBDGameRoomListPage oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    precacheImage(
        AssetImage(YBDGameResource.assetPadding('game_list_bg', need2x: false, isWebp: false, suffix: "jpg")), context);
  }
  void didChangeDependenciesCzQ3Noyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
