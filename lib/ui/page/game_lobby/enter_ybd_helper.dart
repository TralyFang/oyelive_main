import 'dart:async';


import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_lobby/entity/enter_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/quick_ybd_join_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/widget/dialog_ybd_tpg_match.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_gradient_dialog.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/util/tp_ybd_cache.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/go_ybd_tab_event.dart';
import 'package:oyelive_main/ui/page/share/util/friend_ybd_share_util.dart';
import 'package:oyelive_main/ui/widget/dialog/confirm_ybd_dialog.dart';

import '../../../main.dart';

class YBDEnterHelper {
  static String needSeat = "1";
  static const String noNeed = "0";
  static const String need = "1";

  //匹配不带人机
  quickJoin(String subType) async {
    YBDCommonTrack().tpgo('quickly_join');
    YBDDialogUtil.showLoading(Get.context);
    YBDQuickJoinEntity? entity = await ApiHelper.quickJoin(subCategory: subType);

    if (entity?.code == GameConst.game_success) {
      connectSocketNjoin(subType, entity?.data?.roomId, entity?.data?.matchType, onSuccess: () {
        YBDDialogUtil.hideLoading(Get.context);
      });
    } else if (entity?.code == GameConst.game_create_room_tips) {
      YBDDialogUtil.hideLoading(Get.context);
      YBDCommonTrack().alertExpose('TP_no_room');
      YBDTPGradientDialog.show(
        Get.context!,
        title: 'Tips',
        height: 426,
        type: DialogType.tpCommon,
        showClose: false,
        buttonNum: 2,
        textHeight: 150,
        color: ColorType.purple,
        contentMsg: "There are currently no eligible playrooms available, can you create a new playroom?",
        onConfirm: () {
          YBDCommonTrack().tpgo('quickly_join_create_confirm');
          createRoom(subType, GameConst.matchTypePerson);
        },
        closeCallback: (_) {
          YBDCommonTrack().tpgo('quickly_join_create_cancel');
        },
      );
    } else if (entity?.code == GameConst.game_insufficen) {
      YBDDialogUtil.hideLoading(Get.context);
      YBDCommonTrack().alertExpose('TP_in_balance_insufficient');
      showInsuff(type: 'quickJoin');
    } else {
      YBDDialogUtil.hideLoading(Get.context);
      YBDToastUtil.toast(entity?.message);
    }
  }

  //匹配带人机
  quickGame(String subType, {String? modeName}) async {
    YBDTpgMatchDialog.show();
    YBDQuickJoinEntity? entity = await ApiHelper.quickGame(subCategory: subType, modeName: modeName);
    if (entity?.code == GameConst.game_success) {
      connectSocketNjoin(subType, entity?.data?.roomId, entity?.data?.matchType,
          enterType: EnterGameRoomType.QuickGame, quickGame: true, onSuccess: () {
        YBDTpgMatchDialog.dismiss();
        // 匹配成功时长埋点
        YBDTATrack().trackEvent(YBDTATimeTrackType.match_success_time,
            prop: YBDTAProps(id: entity?.data?.roomId.toString(), name: 'TPGO'));
      });
    } else if (entity?.code == GameConst.game_insufficen) {
      YBDTpgMatchDialog.dismiss();
      YBDCommonTrack().alertExpose('TP_match_balance_insufficient');
      showInsuff(type: 'match');
    } else {
      YBDTpgMatchDialog.dismiss();
      YBDToastUtil.toast(entity?.message);
    }
  }

  showInsuff({bool create: false, String? type}) {
    YBDTPGradientDialog.show(Get.context!,
        title: 'Tips',
        height: 380,
        type: DialogType.tpCommon,
        showClose: false,
        buttonNum: 2,
        textHeight: 108,
        color: ColorType.purple,
        contentMsg:
            "Insufficient balance, failed to ${create ? "create the room" : "join the room"}. Do you want to go to the recharge?",
        onConfirm: () {
      trackWithType(type, true);
      // YBDNavigatorHelper.popPage(Get.context);
      YBDNavigatorHelper.openTopUpPage(Get.context);
    }, closeCallback: (_) {
      trackWithType(type, false);
      // YBDNavigatorHelper.popPage(Get.context);
    });
  }

  void trackWithType(String? type, bool confirm) {
    if (type == 'match') {
      YBDCommonTrack().gameLobby(confirm ? 'match_topup_confirm' : 'match_topup_cancel');
    }
    if (type == 'create') {
      YBDCommonTrack().tpgo(confirm ? 'create_topup_confirm' : 'create_topup_cancel');
    }
    if (type == 'join') {
      YBDCommonTrack().tpgo(confirm ? 'join_topup_confirm' : 'join_topup_cancel');
    }
  }
  void trackWithTypeJudw0oyelive(String? type, bool confirm) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  connectSocketNjoin(String subType, String? roomId, String? matchType,
      {Function? onSuccess,
      bool create = false,
      EnterGameRoomType enterType: EnterGameRoomType.QuickJoin,
      bool popFirst: false,
      bool quickGame = false}) async {
    // 检测资源是否下载完成
    await YBDTPCache.downloadCacheFuture();

    YBDRtcHelper.getInstance().setGameSubType(subType);
    YBDRtcHelper.getInstance().roomId = roomId;
    YBDRtcHelper.getInstance().matchType = matchType;
    YBDGameRoomSocketApi.getInstance().enterRoom(
        type: enterType,
        onSuccess: (data) {
          print("roooom$roomId   \njoin${data.toJson()} ");
          onSuccess?.call();

          if (data.code == GameConst.game_insufficen) {
            YBDCommonTrack().alertExpose(create
                ? 'TP_create_balance_insufficient'
                : quickGame
                    ? 'TP_match_balance_insufficient'
                    : 'TP_in_balance_insufficient');
            showInsuff(
                create: create,
                type: create
                    ? 'create'
                    : quickGame
                        ? 'match'
                        : 'join');
            return;
          } else if (data.code == GameConst.game_success) {
            if (create)
              YBDTATrack().trackEvent('TP_go_create',
                  prop: YBDTAProps(
                    mode: YBDRtcHelper.getInstance().modeName,
                    currency: YBDRtcHelper.getInstance().currency,
                  ));
            String socketUrl = Uri.encodeComponent(data.body!.wsUrl!);
            if (popFirst) {
              YBDNavigatorHelper.popUntilMain(Get.context!);
              eventBus.fire(YBDGoTabEvent(1, subIndex: 0));
            }
            YBDNavigatorHelper.navigateTo(
                Get.context,
                YBDNavigatorHelper.game_tp_room_page +
                    "?roomId=$roomId"
                        "&socketUrl=$socketUrl");
          } else {
            YBDToastUtil.toast(data.codeMsg);
          }
        },
        onTimeOut: () {
          YBDToastUtil.toast("Time out!");
        });
  }

  //matchType 1人机2不是人机
  // tpgo 开房间
  createRoom(String subType, String matchType) {
    YBDCommonTrack().tpgo('create_room');
    YBDTPGradientDialog.show(Get.context!, width: 650, height: 650, title: 'Create', type: DialogType.create,
        onConfirm: () {
      YBDDialogUtil.showLoading(Get.context);
      connectSocketNjoin(subType, YBDUserUtil.getUserIdSync.toString(), matchType, onSuccess: () {
        YBDDialogUtil.hideLoading(Get.context);
      }, create: true);
    });
  }

  Future<bool> shareTofriends(List<YBDRoomInfo?> selectedFriends, String? nickName, String? roomId, String? roomImg,
      {bool needLoading: true}) async {
    var data = {
      "title": nickName,
      "content": "Come and play ${GameConst.GAME_NAME_TPGO} game together!",
      "roomId": roomId,
      "roomImg": roomImg,
    };
    print("showdww$data");
    return YBDFriendShareUtil.shareToFriends(Get.context, FriendShareType.ShareTpgoRoom, data, selectedFriends,
        needLoading: needLoading);
  }
}
