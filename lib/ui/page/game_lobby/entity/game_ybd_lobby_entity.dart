import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDGameLobbyEntity with JsonConvert<YBDGameLobbyEntity> {
  String? code;
  List<YBDGameLobbyData?>? data;
  bool? success;
}

class YBDGameLobbyData with JsonConvert<YBDGameLobbyData> {
  String? title;
  String? tabIcon;
  String? tabIconSelected;
  String? titleIcon;
  List<YBDGameLobbyDataItem?>? item;
  int? tabIndex;
}

class YBDGameLobbyDataItem with JsonConvert<YBDGameLobbyDataItem> {
  String? img;
  int? routeType;
  String? param;
  dynamic status;
  int? index;
  YBDGameRoomConfig? gameRoomOpenConfig;
  String? modeName; // 模式名
  String? currency; // 币种
  String? boot; // 底注
  String? minTicket; // 最低金额
}

class YBDGameRoomConfig with JsonConvert<YBDGameRoomConfig> {
  // {
  // "disableImgUrl": "https://oyetalk-status.s3.ap-south-1.amazonaws.com/game/icon/room/category2/lb_tpgo.png",
  // "openStatus": 1,
  // "startTime": "03:00",
  // "endTime": "05:00",
  // "alertMsg": "The current game is open from 03:00-05:00 UTC "
  // },

  String? disableImgUrl;
  int? openStatus;
  String? alertMsg;
}
