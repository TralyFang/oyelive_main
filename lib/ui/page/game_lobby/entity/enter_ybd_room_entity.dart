import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/base/json_field.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

class YBDEnterRoomEntity extends YBDGameBase with JsonConvert<YBDEnterRoomEntity> {
  YBDEnterRoomBody? body;
  String? cmd;
  String? code;
  String? codeMsg;
  YBDEnterRoomHeader? header;
  YBDEnterRoomRoute? route;
  String? ver;
}

class YBDEnterRoomBody with JsonConvert<YBDEnterRoomBody> {
  String? wsUrl;
  String? entryFrom;
  String? quickMsgConfig;
}

class YBDEnterRoomHeader with JsonConvert<YBDEnterRoomHeader> {
  @JSONField(name: "connection-id")
  String? connectionId;
}

class YBDEnterRoomRoute with JsonConvert<YBDEnterRoomRoute> {
  String? from;
  String? mode;
  String? room;
  List<String>? to;
}
