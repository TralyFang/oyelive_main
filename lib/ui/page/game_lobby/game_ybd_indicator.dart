import 'dart:async';


import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

import '../game_room/widget/game_ybd_resource_util.dart';
import 'entity/game_ybd_lobby_entity.dart';

class YBDGameIndicator extends StatefulWidget {
  TabController? controller;
  PageController pageController;
  List<YBDGameLobbyData?>? data;
  double scale;

  Key? key;
  YBDGameIndicator(this.controller, this.pageController, this.data, this.scale, {this.key});

  @override
  YBDGameIndicatorState createState() => new YBDGameIndicatorState();
}

class YBDGameIndicatorState extends BaseState<YBDGameIndicator> {
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Column(
      // key: widget.key,
      children: [
        Container(
          height: 86.px * widget.scale,
          // width: 400.px*widget.scale,
          constraints: BoxConstraints(maxWidth: 372.px * widget.scale),
          // padding: EdgeInsets.only(top: 10.px*widget.scale),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(100.px * widget.scale)),
              color: Color(0xff6907D9),
              boxShadow: [
                BoxShadow(blurRadius: 5.px * widget.scale, color: Colors.white),
                BoxShadow(
                    blurRadius: 14.px * widget.scale, spreadRadius: -14.px * widget.scale, color: Color(0xff460093))
              ]),
          child: Theme(
            data: ThemeData().copyWith(splashColor: Colors.transparent),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(49.px * widget.scale)),
              child: TabBar(
                key: widget.key,

                controller: widget.controller,
                // indicatorSize: TabBarIndicatorSize() 124.px*widget.scale,
                // indicatorColor: widget.indicatorColor,
                // labelColor: widget.labelColor,

                isScrollable: true,
                labelPadding: EdgeInsets.all(0),
                onTap: (index) {
                  widget.pageController.jumpToPage(
                    index,
                  );
                  YBDCommonTrack().gameLobby(widget.data![index]!.title);
                  widget.pageController.animateToPage(index, duration: Duration(milliseconds: 300), curve: Curves.ease);
                },
                indicator: BoxDecoration(),
                labelColor: Colors.white,
                unselectedLabelColor: Colors.white.withOpacity(0.5),
                labelStyle: TextStyle(color: Colors.white, fontSize: 18.sp * widget.scale, fontFamily: "baloo"),
                unselectedLabelStyle: TextStyle(
                    color: Colors.white.withOpacity(0.5), fontSize: 18.sp * widget.scale, fontFamily: "baloo"),

                // tabs: [
                //   Tab(
                //     icon: iconWithPadding(0),
                //     text: "LUxxDO",
                //     iconMargin: EdgeInsets.all(0),
                //   ),
                //   Tab(
                //     icon: iconWithPadding(1),
                //     text: "LUDO",
                //     iconMargin: EdgeInsets.all(0),
                //   ),
                //   Tab(
                //     icon: iconWithPadding(2),
                //     text: "LUDO",
                //     iconMargin: EdgeInsets.all(0),
                //   ),
                //   Tab(
                //     icon: iconWithPadding(3),
                //     text: "LUDO",
                //     iconMargin: EdgeInsets.all(0),
                //   ),
                // ],

                tabs: List.generate(
                  widget.controller?.length ?? 0,
                  (index) => Tab(
                    icon: iconWithPadding(index, widget.data?[index], onlyOne: (widget.controller?.length ?? 0) == 1),
                    text: widget.data?[index]?.title,
                    iconMargin: EdgeInsets.only(top: 9.px * widget.scale),
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 20.px * widget.scale,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: List.generate(
              widget.controller!.length,
              (index) => Container(
                    margin: EdgeInsets.symmetric(horizontal: 5.px * widget.scale),
                    width: 13.px * widget.scale,
                    height: 13.px * widget.scale,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: widget.controller!.index == index ? Colors.white : Colors.white.withOpacity(0.5)),
                  )),
        )
      ],
    );
  }
  void myBuildEXnE3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  iconWithPadding(cIndex, YBDGameLobbyData? tab, {bool onlyOne: false}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: onlyOne ? 30.px.toDouble() : (40.px * widget.scale)),
      child: CachedNetworkImage(
        imageUrl: widget.controller!.index == cIndex
            ? tab!.tabIcon!.replaceAll("https", "http")
            : tab!.tabIconSelected!.replaceAll("https", "http"),
        width: 43.px * widget.scale,
        placeholder: (x, c) {
          return SizedBox(
            width: 43.px * widget.scale,
            height: 43.px * widget.scale,
          );
        },
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.controller!.addListener(() {
      setState(() {});
    });
  }
  void initStateCYWrSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDGameIndicator oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetLvScPoyelive(YBDGameIndicator oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
