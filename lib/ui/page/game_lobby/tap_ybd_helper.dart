import 'dart:async';


import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_game_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/match_ybd_commands.dart';
import 'package:oyelive_main/ui/page/game_lobby/enter_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_lobby/entity/function_ybd_parase_entity.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/widget/dialog_ybd_tpg_match.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/tp_ybd_cache.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/widget/dialog_ybd_lock.dart';
import 'package:collection/collection.dart';

import 'entity/game_ybd_lobby_entity.dart';

class YBDTapHelper {
  YBDGameLobbyDataItem? data;

  onTap(YBDGameLobbyDataItem? data, YBDGameRoomConfig config) async {
    bool enable = (config == null) ? true : (config.openStatus == 1);
    print("shoxwww${config.toJson()}");
    if (!enable) {
      YBDToastUtil.toast(config.alertMsg);
      return;
    }
    try {
      switch (data!.routeType) {
        case 1:
          //跳路由

          YBDNavigatorHelper.openUrl(Get.context, data.param!);
          break;

        case 2:
          //走方法

          YBDFunctionParaseEntity param = YBDFunctionParaseEntity().fromJson(json.decode(data.param!));
          switch (param.functionName) {
            case 'matchGame':
              String gameName = param.functionParam?.firstOrNull ?? '';
              if (gameName == 'tp') YBDGameRoomTrack().match();
              matchGame(gameName);
              break;
            case 'quickJoin':
              quickJoin(param.functionParam?.firstOrNull ?? '', int.parse(param.functionParam?.valueOpt[1] ?? ''));
              break;
            case 'quickGame':
              if (param.functionParam?.contains('ludo') ?? false) YBDCommonTrack().gameLobby('LUDO_quick_game');
              quickGame(param.functionParam?.firstOrNull ?? '', int.parse(param.functionParam?.valueOpt[1] ?? ''));
              break;
            case 'goGameList':
              if (param.functionParam?.valueOpt[1] == 'ludo') YBDCommonTrack().gameLobby('LUDO_open_room');
              if (param.functionParam?.valueOpt[1] == 'bumper_car') YBDCommonTrack().gameLobby('More_bumper_blaster');
              if (param.functionParam?.valueOpt[1] == 'fly_cutter') YBDCommonTrack().gameLobby('More_knife_challenge');
              if (param.functionParam?.valueOpt[1] == 'tpgo') YBDCommonTrack().gameLobby('TPGO_open');
              await precacheImage(
                  AssetImage(YBDGameResource.assetPadding('game_list_bg', need2x: false, isWebp: false, suffix: "jpg")),
                  Get.context!);
              YBDNavigatorHelper.navigateTo(
                  Get.context, YBDNavigatorHelper.game_room_list + '/${param.functionParam?.valueOpt[0]}/${param.functionParam?.valueOpt[1]}');
              break;
            case 'tpgoBot':
              YBDCommonTrack().commonTrack(YBDTAProps(location: 'match', module: YBDTAModule.TPGO, name: data.modeName));
              YBDGameRoomTrack().match();
              YBDRtcHelper.getInstance().modeName = data.modeName ?? '';
              YBDEnterHelper().quickGame(GameConst.GAME_SUB_TPGO, modeName: data.modeName);
              break;
            default:
              YBDToastUtil.toast('No function');
              break;
          }
          YBDNavigatorHelper.openUrl(Get.context, data.param!);
          break;
      }
    } catch (e) {}
  }

  late BuildContext lockContext;

  matchGame(
    String matchType,
  ) {
    showDialog<Null>(
        context: Get.context!, //BuildContext对象
        barrierDismissible: true,
        builder: (BuildContext context) {
          lockContext = context;
          return YBDLockDialog(() {
            YBDNavigatorHelper.popPage(lockContext);
          });
        });

    void match() {
      YBDMatchCommands.Match(matchType: matchType).then((value) {
        if (value == null) {
          YBDToastUtil.toast('No connection,please try again later!');
          YBDNavigatorHelper.popPage(Get.context!);

          return;
        }
        print('xd11${value.toJson()}');

        YBDNavigatorHelper.popPage(lockContext);

        if (value.code == Const.HTTP_SUCCESS) {
          print('xd');
          YBDMatchCommands.curMatchId = value.matchId;

          showDialog<Null>(
              context: Get.context!, //BuildContext对象
              barrierDismissible: false,
              builder: (BuildContext context) {
                return YBDTpgMatchDialog();
              });
        } else {
          YBDToastUtil.toast(value.message);
        }
      });
    }
    void matchLkWrUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

    // 检测资源是否下载完成
    YBDTPCache.downloadCache(callback: () {
      match();
    });
  }

  quickJoin(String subCategory, int typeIndex) {
    YBDRtcHelper.getInstance().setGameSubType(subCategory);

    YBDGameSocketApiUtil.quickEnterRoom(type: EnterGameRoomType.values[typeIndex]);
  }

  void quickGame(String subCategory, int typeIndex) {
    YBDRtcHelper.getInstance().setGameSubType(subCategory);

    YBDGameSocketApiUtil.quickEnterRoom(type: EnterGameRoomType.values[typeIndex]);
  }
  void quickGameaB16loyelive(String subCategory, int typeIndex) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
