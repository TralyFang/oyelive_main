import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oye_tool_package/widget/draggable_float_widget.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/event/refresh_ybd_status_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigation_ybd_service.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_event.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/home/bloc/advertise_ybd_bloc.dart';
import 'package:oyelive_main/ui/page/home/widget/popular_ybd_banner.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/go_ybd_tab_event.dart';
// import 'package:oyelive_main/ui/page/room/widget/tp_ybd_page_view.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';
import 'package:collection/collection.dart';
import 'entity/game_ybd_lobby_entity.dart';
import 'game_ybd_indicator.dart';
import 'game_ybd_item.dart';

class YBDGameLobbyPage extends StatefulWidget {
  @override
  YBDGameLobbyPageState createState() => new YBDGameLobbyPageState();
}

class YBDGameLobbyPageState extends BaseState<YBDGameLobbyPage> with TickerProviderStateMixin {
  var containerKey = GlobalKey();
  var floatController = DraggableFloatController();
  late YBDAdvertiseBloc bloc;
  dynamic _pageBlocs() {
    return [
      BlocProvider<YBDAdvertiseBloc>(create: (context) {
        return bloc;
      }),
    ];
  }

  double? currentPageValue = 0.0;
  PageController controller = PageController();
  TabController? tabController;

  YBDGameLobbyEntity? data;
  final double designScale = 9 / 19.5;
  late double actualScale;
  Key buildKey = UniqueKey();

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild

    final scale = designScale / actualScale;
    print("actualScale is $actualScale scale is $scale");
    return Scaffold(
        key: containerKey,
        backgroundColor: Colors.transparent,
        body: MultiBlocProvider(
            providers: _pageBlocs(),
            child: Stack(
              children: [
                Column(
                  children: [
                    StoreBuilder<YBDAppState>(builder: (context, store) {
                      YBDUserInfo userInfo = store.state.bean!;
                      return Container(
                        height: ScreenUtil().statusBarHeight + 114.px,
                        // decoration: BoxDecoration(
                        //   gradient: LinearGradient(colors: [
                        //     Color(0xff4D0BB9),
                        //     Color(0xff8D3AFA),
                        //   ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                        // ),
                        child: Stack(
                          children: [
                            Image.asset(
                              YBDGameResource.assetPadding(
                                'picture_lamp',
                                need2x: false,
                                isWebp: true,
                              ),
                              fit: BoxFit.fitWidth,
                            ),
                            Positioned(
                              bottom: 10.px,
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 26.px,
                                  ),
                                  YBDRoundAvatar(
                                    userInfo.headimg,
                                    sex: userInfo.sex,
                                    avatarWidth: 96,
                                    userId: userInfo.id,
                                    needNavigation: false,
                                    // labelWitdh: 50,
                                    // lableSrc:
                                    // (userInfo?.vip ?? 0) == 0 ? "" : "assets/images/vip_lv${userInfo.vip}.png",
                                    scene: "C",
                                    showVip: false,
                                    labelPadding: EdgeInsets.only(right: ScreenUtil().setWidth(18)),
                                    // levelFrame: userInfo.headFrame,
                                    onTap: () {
                                      eventBus.fire(YBDGoTabEvent(
                                        3,
                                      ));
                                    },
                                  ),
                                  SizedBox(
                                    width: 32.px,
                                  ),
                                  assetsBlock(YBDGameResource.assetPadding('icon_beans', need2x: false, isWebp: false),
                                      userInfo.money, () {
                                    YBDNavigatorHelper.openTopUpPage(context);
                                  }),
                                  SizedBox(
                                    width: 32.px,
                                  ),
                                  assetsBlock(YBDGameResource.assetPadding('game_icon_gold', need2x: true, isWebp: false),
                                      userInfo.golds, () {
                                    YBDCommonTrack().gameLobby('gold_exchange');
                                    final url = store.state.configs?.beans2GoldsUrl ??
                                        'http://121.37.214.86/talent/#/exchangeGold';
                                    YBDNavigatorHelper.navigateToWeb(
                                        context, "?url=${Uri.encodeComponent(url)}&showNavBar=false");
                                  }),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
                    SizedBox(
                      height: 25.px,
                    ),
                    YBDPopularBanner(),

                    // YBDGameIndicator(tabController, controller)
                  ],
                ),
                if (data != null && data!.data != null)
                  Positioned(
                    right: 0,
                    left: 0,
                    top: ScreenUtil().statusBarHeight + 350.px,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 720.px * scale,
                          child: PageView.builder(
                              key: buildKey,
                              itemCount: data!.data!.length,
                              onPageChanged: (index) {
                                if (tabController!.index != index) {
                                  print("sxx$index");

                                  tabController!.animateTo(index, duration: Duration(milliseconds: 300));
                                }
                                YBDCommonTrack().gameLobby('slide');
                                print("sxx$index");
                                tabController!.animateTo(index, duration: Duration(milliseconds: 300));
                              },
                              controller: controller,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, position) {
                                double angle = 0;
                                double middle =
                                    (currentPageValue! > position ? (2 * position + 1) : (2 * position - 1)) / 2;
                                double percentage = 1 - ((currentPageValue! - middle) * 2).abs();
                                print(
                                    "c$currentPageValue my position$position,middle$middle angle${currentPageValue! > position ? percentage : -percentage}");

                                return Transform(
                                  // transform: Matrix4.skewY(position - currentPageValue),
                                  child: YBDGameItem(data!.data![position], scale),
                                  alignment: FractionalOffset.center,
                                  transform: Matrix4.identity()
                                    ..setEntry(3, 2, 0.008)
                                    ..rotateY((currentPageValue! > position ? percentage : -percentage) * 0.06),
                                );
                              }),
                        ),
                        YBDGameIndicator(
                          tabController,
                          controller,
                          data!.data,
                          scale,
                          key: buildKey,
                        )
                      ],
                    ),
                  ),
                draggableFloatWidget(),
              ],
            )));
  }
  void myBuildnPJlwoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget assetsBlock(String image, num? counts, Function onTap) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.all(6.px),
          width: 168.px,
          height: 38.px,
          decoration: BoxDecoration(
              color: Color(0xff4711A6),
              borderRadius: BorderRadius.all(Radius.circular(40.px)),
              boxShadow: [BoxShadow(color: Color(0xff24035E), spreadRadius: -20.px, blurRadius: 10.px)]),
        ),
        Positioned.fill(
          child: Align(
              alignment: Alignment.center,
              child: Text(
                YBDNumericUtil.compactNumberWithFixedDigits(counts.toString(),
                    fixedDigits: 2, rounding: false, removeZero: false, goldType: true)!,
                style: TextStyle(color: Colors.white, fontSize: 26.sp2, fontFamily: "baloo"),
              )),
        ),
        Positioned(
            left: 0,
            top: 0,
            bottom: 0,
            child: Image.asset(
              image,
              width: 40.px,
            )),
        Positioned(
            right: 0,
            top: 0,
            bottom: 0,
            child: GestureDetector(
              onTap: onTap as void Function()?,
              child: Image.asset(
                YBDGameResource.assetPadding('icon_top up', need2x: false, isWebp: false),
                width: 40.px,
              ),
            ))
      ],
    );
  }
  void assetsBlock8EMY3oyelive(String image, num? counts, Function onTap) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 可拖拽浮窗
  DraggableFloatWidget draggableFloatWidget() {
    return DraggableFloatWidget(
      top: ScreenUtil().statusBarHeight + 1000.px,
      right: 20.px,
      child: YBDGameEvent(
        YBDLocationName.GAME_HALL_PAGE,
        didChangeWidgetCallback: () {
          floatController.updateWidget();
        },
      ),
      controller: floatController,
      containerKey: containerKey,
    );
  }

  getData() async {
    YBDGameLobbyEntity? entity = await ApiHelper.gameCategorySearchV2(context);
    if (entity != null) {
      data = entity;
      tryCatch(() {
        // 保存游戏模式信息
        if (entity.data != null) {
          List<YBDGameLobbyDataItem?> items = entity.data
                  ?.firstWhere(
                    (e) => e!.title == GameConst.GAME_NAME_TPGO,
                    orElse: () => null,
                  )
                  ?.item ?? [];
          // items.removeWhere((e) => e.modeName == null); // 过滤数据
          if (items.isNotEmpty) YBDTPGlobal.tpgoModels = items.map((e) => e!.toJson()).toList();
          logger.v('22.11.23---getData - tpgoModels:${YBDTPGlobal.tpgoModels}');
        }
      });
      buildKey = UniqueKey();
      if (tabController == null)
        tabController = TabController(length: data?.data?.length ?? 0, vsync: this);
      else {
        tabController = TabController(length: data?.data?.length ?? 0, vsync: this);
        controller.jumpTo(0);
      }
      setState(() {});
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //banner
    bloc = YBDAdvertiseBloc(context);
    bloc.type = 8;
    bloc.add(AdvertiseEvent.Refresh);
    //
    getData();
    controller.addListener(() {
      // setState method to
      // rebuild the widget
      setState(() {
        currentPageValue = controller.page;
        print("my page$currentPageValue");
      });
    });
    addSub(eventBus.on<YBDGoTabEvent>().listen((YBDGoTabEvent event) async {
      if (event.index == 1 && event.subIndex != null) {
        controller.jumpToPage(
          event.subIndex!,
        );
      }
    }));

    addSub(eventBus.on<YBDRefreshStatusEvent>().listen((YBDRefreshStatusEvent event) async {
      getData();
      bloc.add(AdvertiseEvent.Refresh);
    }));
    actualScale = ScreenUtil().screenWidth / ScreenUtil().screenHeight;
  }
  void initStatevR4Fvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDGameLobbyPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesR0QTkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
