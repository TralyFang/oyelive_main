import 'dart:async';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/match_ybd_commands.dart';
import 'package:oyelive_main/ui/page/game_lobby/tap_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/widget/dialog_ybd_tpg_match.dart';
import 'package:oyelive_main/ui/widget/dialog_ybd_lock.dart';

import '../game_room/widget/game_ybd_resource_util.dart';
import 'entity/game_ybd_lobby_entity.dart';

// class YBDGameItem extends StatelessWidget with AutomaticKeepAliveClientMixin {
//   YBDGameLobbyData data;
//
//   YBDGameItem(this.data);
//
//
// }
class YBDGameItem extends StatefulWidget {
  YBDGameLobbyData? data;

  double scale;
  YBDGameItem(this.data, this.scale);
  @override
  YBDGameItemState createState() => new YBDGameItemState();
}

class YBDGameItemState extends State<YBDGameItem> with AutomaticKeepAliveClientMixin {
  // @override
  // Widget myBuild(BuildContext context) {
  //   // TODO: implement myBuild
  //   return new Scaffold(
  //     appBar: new AppBar(
  //       title: new Text(''),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Align(
      alignment: Alignment.center,
      child: Container(
        width: 672.px.toDouble(),
        height: 672.px * widget.scale,
        // margin: EdgeInsets.all(24.px),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(YBDGameResource.assetPadding('lobby_board', need2x: false, isWebp: true)),
                fit: BoxFit.fill)),
        child: Column(
          children: [
            SizedBox(
              height: 58.px * widget.scale,
            ),
            // Image.asset(
            //   YBDGameResource.assetPadding(
            //     'lb_ludo',
            //     need2x: false,
            //     isWebp: true,
            //   ),
            //   width: 292.px*widget.scale,
            //   fit: BoxFit.fitWidth,
            // ),
            YBDImageUtil().showImageOrSvga(widget.data!.titleIcon!,
                width: 230.px * widget.scale, height: 58.px * widget.scale, fill: true),
            Spacer(),
            inWhichLayout(widget.data?.item?.length ?? 0),
            Spacer(),
          ],
        ),
      ),
    );
  }
  void buildL4b2Hoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget inWhichLayout(num count) {
    switch (count) {
      case 1:
        return oneLayout();
        break;
      case 2:
        return twoLayout();
        break;
      case 3:
        return threeLayout();
        break;
      case 4:
        return fourLayout();
        break;
      case 5:
        return fiveLayout();
        break;
      case 6:
        return sixLayout();
        break;
      default:
        return SizedBox();
        break;
    }
  }
  void inWhichLayoutGF0g3oyelive(num count) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  bool isEnable(int index) {
    return (widget.data!.item![index]!.gameRoomOpenConfig?.openStatus ?? 1) == 1;
  }

  getDisableImagUrl(int index) {
    return widget.data!.item![index]!.gameRoomOpenConfig?.disableImgUrl;
  }

  Widget oneLayout() {
    return InkWell(
      onTap: () {
        onTap(0);
      },
      child: Container(
        width: 337.px * widget.scale,
        height: 296.px * widget.scale,
        // margin: EdgeInsets.only(top: 130.px * widget.scale),
        child: YBDImageUtil().showImageOrSvga(isEnable(0) ? widget.data!.item!.first!.img! : getDisableImagUrl(0),
            width: 337.px * widget.scale, height: 296.px * widget.scale, showHolder: true),
      ),
    );
  }
  void oneLayoutFZ0UDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget twoLayout() {
    return Padding(
      // padding: EdgeInsets.only(top: 150.px * widget.scale),
      padding: EdgeInsets.all(0),

      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          InkWell(
            onTap: () {
              onTap(0);
            },
            child: Container(
              width: 276.px * widget.scale,
              height: 247.px * widget.scale,
              child: YBDImageUtil().showImageOrSvga(isEnable(0) ? widget.data!.item!.first!.img! : getDisableImagUrl(0),
                  width: 276.px * widget.scale, height: 247.px * widget.scale, showHolder: true),
            ),
          ),
          SizedBox(
            width: 24.px * widget.scale,
          ),
          InkWell(
            onTap: () {
              onTap(1);
            },
            child: Container(
              width: 276.px * widget.scale,
              height: 247.px * widget.scale,
              child: YBDImageUtil().showImageOrSvga(isEnable(1) ? widget.data!.item!.last!.img! : getDisableImagUrl(1),
                  width: 276.px * widget.scale, height: 247.px * widget.scale, showHolder: true),
            ),
          ),
        ],
      ),
    );
  }
  void twoLayoutz2jJGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget threeLayout() {
    return Padding(
      // padding: EdgeInsets.only(top: 60.px * widget.scale),
      padding: EdgeInsets.all(0),

      child: Column(
        children: [
          InkWell(
            onTap: () {
              onTap(0);
            },
            child: Container(
              width: 576.px * widget.scale,
              height: 204.px * widget.scale,
              child: YBDImageUtil().showImageOrSvga(isEnable(0) ? widget.data!.item!.first!.img! : getDisableImagUrl(0),
                  width: 576.px * widget.scale, height: 204.px * widget.scale, showHolder: true),
            ),
          ),
          SizedBox(
            height: 22.px * widget.scale,
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              InkWell(
                onTap: () {
                  onTap(1);
                },
                child: Container(
                  width: 276.px * widget.scale,
                  height: 204.px * widget.scale,
                  child: YBDImageUtil().showImageOrSvga(isEnable(1) ? widget.data!.item![1]!.img! : getDisableImagUrl(1),
                      width: 276.px * widget.scale, height: 204.px * widget.scale, showHolder: true),
                ),
              ),
              SizedBox(
                width: 24.px * widget.scale,
              ),
              InkWell(
                onTap: () {
                  onTap(2);
                },
                child: Container(
                  width: 276.px * widget.scale,
                  height: 204.px * widget.scale,
                  child: YBDImageUtil().showImageOrSvga(isEnable(2) ? widget.data!.item![2]!.img! : getDisableImagUrl(2),
                      width: 276.px * widget.scale, height: 204.px * widget.scale, showHolder: true),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
  void threeLayout8t7pboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget fourLayout() {
    return Container(
      width: 576.px * widget.scale,
      // height: 432.px*widget.scale,
      // padding: EdgeInsets.only(top: 60.px * widget.scale),
      child: GridView.builder(
        padding: EdgeInsets.all(0),
        shrinkWrap: true,
        itemCount: 4,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 24.px * widget.scale,
          crossAxisSpacing: 24.px * widget.scale,
          childAspectRatio: 276 / 204,
        ),
        itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            onTap(index);
          },
          child: Container(
            width: 276.px * widget.scale,
            height: 204.px * widget.scale,
            child: YBDImageUtil().showImageOrSvga(isEnable(index) ? widget.data!.item![index]!.img! : getDisableImagUrl(index),
                width: 276.px * widget.scale, height: 204.px * widget.scale, showHolder: true),
          ),
        ),
      ),
    );
  }
  void fourLayout3He3aoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget fiveLayout() {
    return Padding(
      // padding: EdgeInsets.only(top: 60.px * widget.scale),
      padding: EdgeInsets.all(0),

      child: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              InkWell(
                onTap: () {
                  onTap(0);
                },
                child: Container(
                  width: 276.px * widget.scale,
                  height: 228.px * widget.scale,
                  child: YBDImageUtil().showImageOrSvga(isEnable(0) ? widget.data!.item![0]!.img! : getDisableImagUrl(0),
                      width: 276.px * widget.scale, height: 228.px * widget.scale, showHolder: true),
                ),
              ),
              SizedBox(
                width: 24.px * widget.scale,
              ),
              InkWell(
                onTap: () {
                  onTap(1);
                },
                child: Container(
                  width: 276.px * widget.scale,
                  height: 228.px * widget.scale,
                  child: YBDImageUtil().showImageOrSvga(isEnable(1) ? widget.data!.item![1]!.img! : getDisableImagUrl(1),
                      width: 276.px * widget.scale, height: 228.px * widget.scale, showHolder: true),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 24.px * widget.scale,
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              InkWell(
                onTap: () {
                  onTap(2);
                },
                child: Container(
                  width: 177.px * widget.scale,
                  height: 180.px * widget.scale,
                  child: YBDImageUtil().showImageOrSvga(isEnable(2) ? widget.data!.item![2]!.img! : getDisableImagUrl(2),
                      width: 177.px * widget.scale, height: 180.px * widget.scale, showHolder: true),
                ),
              ),
              SizedBox(
                width: 23.px * widget.scale,
              ),
              InkWell(
                onTap: () {
                  onTap(3);
                },
                child: Container(
                  width: 177.px * widget.scale,
                  height: 180.px * widget.scale,
                  child: YBDImageUtil().showImageOrSvga(isEnable(3) ? widget.data!.item![3]!.img! : getDisableImagUrl(3),
                      width: 177.px * widget.scale, height: 180.px * widget.scale, showHolder: true),
                ),
              ),
              SizedBox(
                width: 23.px * widget.scale,
              ),
              InkWell(
                onTap: () {
                  onTap(4);
                },
                child: Container(
                  width: 177.px * widget.scale,
                  height: 180.px * widget.scale,
                  child: YBDImageUtil().showImageOrSvga(isEnable(4) ? widget.data!.item![4]!.img! : getDisableImagUrl(4),
                      width: 177.px * widget.scale, height: 180.px * widget.scale, showHolder: true),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
  void fiveLayout8zjAQoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget sixLayout() {
    return Container(
      width: 576.px * widget.scale,
      // height: 432.px*widget.scale,
      // padding: EdgeInsets.only(top: 60.px * widget.scale),
      child: GridView.builder(
        padding: EdgeInsets.all(0),
        shrinkWrap: true,
        itemCount: 6,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 24.px * widget.scale,
          crossAxisSpacing: 24.px * widget.scale,
          childAspectRatio: 177 / 204,
        ),
        itemBuilder: (context, index) => InkWell(
          onTap: () {
            onTap(index);
          },
          child: Container(
            width: 177.px * widget.scale,
            height: 204.px * widget.scale,
            child: YBDImageUtil().showImageOrSvga(isEnable(index) ? widget.data!.item![index]!.img! : getDisableImagUrl(index),
                width: 177.px * widget.scale, height: 204.px * widget.scale, showHolder: true),
          ),
        ),
      ),
    );
  }
  void sixLayoutkVkBcoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  onTap(int index) {
    logger.v('object========$index');
    YBDTapHelper().onTap(widget.data!.item![index], widget.data!.item![index]!.gameRoomOpenConfig!);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDGameItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
