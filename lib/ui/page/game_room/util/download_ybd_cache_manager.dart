import 'dart:async';

import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/download_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/zip_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/web_ybd_cache_manager.dart';

class YBDDownloadCacheManager {
  static YBDDownloadCacheManager? _helper;
  YBDDownloadCacheManager._();
  static YBDDownloadCacheManager _getInstance() {
    if (_helper == null) {
      YBDDownloadCacheManager instance = YBDDownloadCacheManager._();
      _helper = instance;
      instance.init();
    }
    return _helper!;
  }
  factory YBDDownloadCacheManager() {
    _helper ??= YBDDownloadCacheManager._();
    return _helper!;
  }
  static YBDDownloadCacheManager get instance => _getInstance();
  /// 文件目录名称
  static final String _savePathDirName = 'download_task_cache';

  /// 记录下载信息，以taskId作为key
  static final Map<String?, _YBDInstanceInfo> _single = {};
  /// 记录下载状态，避免下载的过程中再次调用下载浪费资源,以保存地址作为key
  static final Map<String, _YBDInstanceInfo> _statusSingle = {};

  // static StreamController _downloadStreamController = StreamController<_YBDInstanceInfo>.broadcast();
  // static StreamSink<_YBDInstanceInfo> get _add_download => _downloadStreamController.sink;
  // static Stream<_YBDInstanceInfo> get _downloadOutStream => _downloadStreamController.stream;

  init() {

    /// 下载任务状态监听
    YBDDownloadUtil.downloadOutStream.listen((YBDDownloadTaskInfo event) {

      String? taskId = event.id;
      _YBDInstanceInfo? info = _single[taskId];
      if (info != null) {
        if (event.status == DownloadTaskStatus.complete) {
          logger.v('download cache path complete localPath: ${info.localPath}');
          /// 下载完成获取地址
          List<String?>? svgaMp3s = _zipDownloadToUnzip(info.localPath);
          if (svgaMp3s != null) {
            // 压缩包已经下载解压好了， 返回"svga,mp3" 本地路径
            String svgaMp3sPath = svgaMp3s.join(',');
            info.completeCallback?.call(svgaMp3sPath);
          }else {
            info.completeCallback?.call(info.localPath);
          }
          _statusSingle.remove(info.localPath);
          _single.remove(taskId);
          logger.v('download cache path complete _single: ${_single.length}, _statusSingle: ${_statusSingle.length}');
          // _add_download.add(info);
        }else if (event.status == DownloadTaskStatus.failed) {
          logger.v('download cache path failed retry ${info.retry}, localPath: ${info.localPath}');
          /// 重试任务
          if (info.retry < 3) {
            info.retry = info.retry + 1;
            YBDDownloadUtil.retry(event.id!);
          }else {
            _statusSingle.remove(info.localPath);
            _single.remove(taskId);
          }
        }
      }

    });

  }

  /// 下载文件到指定目录下 url 下载地址， fileDir：仅做文件目录判空
  downloadURLToPath(String url, String locationPath, void Function(String? path) callback, {
    bool isFileDir = false,
    void Function({String? error, int? ms, String? url})? failCallback, // 下载失败的回调
    void Function({String? url, int? ms})? durationCallback, // 下载的时间ms
  }) async {
    logger.v('download cache path url: $url, locationPath: $locationPath');
    if (url.toLowerCase().endsWith('.zip') &&
        locationPath != null &&
        !locationPath.toLowerCase().endsWith('.zip')) {
      // 如果下载的文件是zip包，而目标路径不是zip，那就追加zip
      locationPath += '.zip';
    }
    if (url.toLowerCase().endsWith('.mp4')
        && locationPath != null
        && !locationPath.toLowerCase().endsWith('.mp4')) {
      // 如果下载的文件是mp4，而目标路径不是mp4后缀，那就添加mp4后缀
      locationPath += '.mp4';
    }
    if (locationPath != null) {
      String? path = unzipTargetPath(locationPath, isFileDir: isFileDir);
      if (path != null) {
        callback.call(path);
        return; // 已经存在了
      }
    }
    logger.v('download cache path download starting');
    DateTime startTime = DateTime.now();
    File file = await getSingleFile(url).catchError((dynamic error) {
      int duration = DateTime.now().millisecondsSinceEpoch - startTime.millisecondsSinceEpoch;
      logger.e('download cache path download fail error:${error.toString()}');
      callback.call('null');
      failCallback?.call(error:error.toString(), ms:duration, url: url);
    });
    if (file == null) return; // 下载失败了, 上面catchError捕抓了异常，无需回调
    logger.v('download cache path download completed file.path: ${file.path}, exists: ${File(file.path).existsSync()}');
    int duration = DateTime.now().millisecondsSinceEpoch - startTime.millisecondsSinceEpoch;
    durationCallback?.call(url: url, ms: duration);
    if (!File(file.path).existsSync()) return; // 避免其他地方操作被移除了。
    if (locationPath != null) {
      // 下载文件到指定目录
      File(locationPath)
        ..createSync(recursive: true)
        ..writeAsBytesSync(file.readAsBytesSync());

      // 删除插件默认目录下的下载文件，结合上一句就是移动文件
      file.deleteSync();

      callback.call(unzipTargetPath(locationPath, isFileDir: isFileDir));
    }else {
      callback.call(unzipTargetPath(file.path, isFileDir: isFileDir));
    }
  }

  String? unzipTargetPath(String path, {bool isFileDir = false}) {
    logger.v('download cache path unzipTargetPath: $path');
    if (isFileDir) {
      return _unzipFileDirCreated(path, deleteZip: true);
    }
    List<String?>? svgaMp3s = _zipDownloadToUnzip(path, deleteZip: true);
    if (svgaMp3s != null) {
      // 压缩包已经下载解压好了， 返回"svga,mp3" 本地路径
      String svgaMp3sPath = svgaMp3s.join(',');
      logger.v('download cache path svgaMp3sPath: $svgaMp3sPath');
      return svgaMp3sPath;
    }else if (File(path).existsSync()) {
      return path;
    }
    return null;
  }


  /// 下载目标文件，并回调下载地址，如果本地有了，就直接回调
  downloadURL(String url, void Function(String? path) callback) async {

    defaultCacheManagerFile(url, callback);
    return;

    /// 文件名根据url经过md5加密，反正重名
    String _fileName = StringExt.md5String(url) + url.split('/').last;
    String savePathDirName = _savePathDirName;
    String _downloadPath = await YBDCommonUtil.getResourceDir(savePathDirName);
    String downloadLocalPath = _downloadPath + '/' + _fileName;

    List<String?>? svgaMp3s = _zipDownloadToUnzip(downloadLocalPath);
    if (svgaMp3s != null) {
      // 压缩包已经下载解压好了， 返回"svga,mp3" 本地路径
      String svgaMp3sPath = svgaMp3s.join(',');
      callback.call(svgaMp3sPath);
      logger.v('download cache path is exist $svgaMp3sPath, url: $url');
      return;
    }

    logger.v('download cache path _single: ${_single.length}, _statusSingle: ${_statusSingle.length}');

    /// 需要下载的文件已经存在了，就直接回调了
    if (File(downloadLocalPath).existsSync()) {
      callback.call(downloadLocalPath);
      logger.v('download cache path is exist $downloadLocalPath, url: $url');
      return;
    }
    /// 正在下载中就等着就好了
    _YBDInstanceInfo? statusInfo = _statusSingle[downloadLocalPath];
    if (statusInfo != null && statusInfo.isDownloading) {
      logger.v('download cache path is downloading $downloadLocalPath, url: $url');
      return;
    }
    logger.v('download cache path $downloadLocalPath, url: $url');
    String? taskId = await YBDDownloadUtil.createTask(url, fileName: _fileName, savePath: savePathDirName);
    logger.v('download cache path taskId: $taskId');
    _YBDInstanceInfo _instance = _YBDInstanceInfo()
      ..taskId = taskId
      ..retry = 0
      ..isDownloading = true
      ..localPath = downloadLocalPath
      ..completeCallback = callback;

    _single.putIfAbsent(taskId, ()
    => _instance);
    _statusSingle.putIfAbsent(downloadLocalPath, ()
    => _instance);

  }

  /// 统一管理下载入口
  Future<File> getSingleFile(String url) async {
    return DefaultCacheManager().getSingleFile(url);
  }
  void getSingleFileOSEceoyelive(String url)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 下载Uint8List类型文件
  Future<Uint8List> downloadUint8ListURL(String url) async {
    File file = await getSingleFile(url);
    return file.readAsBytesSync();
  }
  void downloadUint8ListURLxGtyuoyelive(String url)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 以流的形式下载
  Stream<String> downloadStreamURL(String url) async* {
    String path = await defaultCacheManagerDownload(url);
    yield path;
  }
  void downloadStreamURL5UZ05oyelive(String url)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 更换另一种下载方式，为了处理google手机文件写入失败的问题
  defaultCacheManagerFile(String url, void Function(String path) callback) async {
    String path = await defaultCacheManagerDownload(url);
    callback.call(path);
  }

  /// 回调下载完成的本地路径，zip：[svga, mp3], 否则：path
  Future<String> defaultCacheManagerDownload(String url) async {
    logger.v('download cache path url: $url');
    File file = await getSingleFile(url);
    logger.v('download cache path file.path: ${file.path}');
    List<String?>? svgaMp3s = _zipDownloadToUnzip(file.path);
    if (svgaMp3s != null) {
      // 压缩包已经下载解压好了， 返回"svga,mp3" 本地路径
      String svgaMp3sPath = svgaMp3s.join(',');
      logger.v('download cache path svgaMp3sPath: $svgaMp3sPath');
      return svgaMp3sPath;
    }else {
      return file.path;
    }
  }
  void defaultCacheManagerDownloadVdTzNoyelive(String url)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是否是zip文件下载
  bool _isZipFileDownload(String? zipPath) {
    /// 文件后缀名，是不是zip
    String zipPathSuffix = zipPath?.split('\.').last.toLowerCase() ?? '';
    if (zipPathSuffix != 'zip') {
      return false;
    }
    logger.v('download cache path is file zip');
    return true;
  }

  /// 压缩包的文件解压，返回[svga, mp3]的路径数组；不存在，不是zip，解压失败都返回null
  List<String?>? _zipDownloadToUnzip(String? zipPath, {bool deleteZip = false}) {

    /// 文件后缀名，是不是zip
    if (!_isZipFileDownload(zipPath)){
      return null;
    }
    /// 给压缩包.zip创建一个同级同名文件夹
    String zipFileDirPath = zipPath!.substring(0, zipPath.lastIndexOf('\.'));
    logger.v('download cache path unzip dir: $zipFileDirPath');

    List<String?>? svgaMp3Paths = _zipFileDirSvgaAndMp3Path(zipFileDirPath);
    if (svgaMp3Paths != null) {
      // 不为空， 则说明已经存在了
      return svgaMp3Paths;
    }

    /// 解压
    bool zipResult = YBDZipUtil.unzipSvgaAnimationZipFile(
        outDir: zipFileDirPath,
        zip: File(zipPath));
    if (!zipResult) {
      logger.v('download cache path unzip fail!');
      return null;
    }

    /// 查找解压文件目录下的[svga, mp3]
    svgaMp3Paths = _zipFileDirSvgaAndMp3Path(zipFileDirPath);

    /// 移除压缩包
    /// 改成defaultCacheManagerFile 用于判断是否已经下载过了，别删zip
    if (deleteZip) File(zipPath).deleteSync();

    return svgaMp3Paths;
  }

  /// 获取压缩目录下的svga+mp3路径或mp4
  List<String?>? _zipFileDirSvgaAndMp3Path(String zipFileDirPath) {

    List<String?> svgaMp3Paths = new List.filled(2, null);
    try {
      Directory zipFileDir = Directory(zipFileDirPath);
      // 先判断文件夹是否存在
      if (zipFileDir.existsSync()){
        List<FileSystemEntity> files = zipFileDir.listSync();
        for (FileSystemEntity f in files) {
          bool isSync = FileSystemEntity.isFileSync(f.path);
          logger.v('download cache path unzip f.path ${f.path}  bool:$isSync');
          if (isSync) {
            if (f.path.toLowerCase().endsWith('.svga')) {
              svgaMp3Paths[0] = f.path;
            } else if (f.path.toLowerCase().endsWith('.mp3')) {
              svgaMp3Paths[1] = f.path;
            }else if (f.path.toLowerCase().endsWith('.mp4')) {
              svgaMp3Paths[0] = f.path;
            }
          }
        }
      }
    } catch (e) {
      logger.v('download cache path not found: $e');
    }
    if (svgaMp3Paths.first?.isNotEmpty ?? false) {
      // 不为空， 则说明已经存在了
      logger.v('download cache path unzip did extist!');
      return svgaMp3Paths;
    }
    return null;
  }

  // 解压，且创建同名目录
  String? _unzipFileDirCreated(String zipPath, {bool deleteZip = false}) {
    /// 文件后缀名，是不是zip
    if (!_isZipFileDownload(zipPath)){
      return null;
    }
    /// 给压缩包.zip创建一个同级同名文件夹
    String zipFileDirPath = zipPath.substring(0, zipPath.lastIndexOf('\.'));
    String zipFileDir = zipPath.substring(0, zipPath.lastIndexOf('\/'));
    logger.v('download cache path unzip dir: $zipFileDirPath, $zipFileDir');

    if (_zipFileDirIsNotEmpty(zipFileDirPath)) {
      return zipFileDir;
    }

    /// 解压
    /**
     * zipPath: /storage/emulated/0/Android/data/com.oyetalk.tv/files/OyeTalk/TPResource/tp_room.zip
     * zipFileDirPath: /storage/emulated/0/Android/data/com.oyetalk.tv/files/OyeTalk/TPResource/tp_room
     * zipFileDir: /storage/emulated/0/Android/data/com.oyetalk.tv/files/OyeTalk/TPResource
     * */
    bool zipResult = YBDZipUtil.unzipSvgaAnimationZipFile(
        outDir: zipFileDir,
        zip: File(zipPath));
    if (!zipResult) {
      logger.v('download cache path unzip fail!');
      return null;
    }

    /// 移除压缩包
    if (deleteZip) File(zipPath).deleteSync();

    if (_zipFileDirIsNotEmpty(zipFileDirPath)) {
      return zipFileDir;
    }
    return null;
  }

  // 目录是否存在文件， 不为空
  bool _zipFileDirIsNotEmpty(String zipFileDirPath) {
    try {
      Directory zipFileDir = Directory(zipFileDirPath);
      // 先判断文件夹是否存在
      if (zipFileDir.existsSync()){
        List<FileSystemEntity> files = zipFileDir.listSync();
        return files != null && files.length > 1;
      }
    } catch (e) {
      logger.v('download cache path zip is not empty not found: $e');
    }
    return false;
  }
  void _zipFileDirIsNotEmptyIQamWoyelive(String zipFileDirPath) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 目录是否非空
  bool fileDirIsNotEmpty(String zipFileDirPath) {
    return _zipFileDirIsNotEmpty(zipFileDirPath);
  }
}

class _YBDInstanceInfo {
  late bool isDownloading; // 是否正在加载中
  String? taskId; // 下载任务id
  String? localPath; // 下载的本地地址
  void Function(String? path)? completeCallback; // 下载完成的回调
  late int retry; // 重试次数，最多3次
}