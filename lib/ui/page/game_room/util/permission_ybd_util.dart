import 'dart:async';



import 'dart:io';

import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/splash/splash_ybd_util.dart';

class YBDPermissionUtil{
  Function? onSuccess;

  static microphone(onSuccess()) async{
    bool ishavePermission = false;
    if (await Permission.microphone.isGranted) {
      ishavePermission = true;
    } else {
      ishavePermission = await YBDSplashUtil.requestMicrophonePermission();
    }
    logger.i('ishavePermission:$ishavePermission');
    if (!ishavePermission) {
      ///ios需要引导用户去设置页面打开麦克风权限，Android跟随系统
      if (Platform.isIOS && !await Permission.microphone.isGranted) {
        YBDDialogUtil.goSettingDialog(Get.context!);
      }
      return;
    }
    onSuccess.call();
  }
}