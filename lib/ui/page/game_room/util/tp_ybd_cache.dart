import 'dart:async';

import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/page/room/entity/room_ybd_operate_info.dart';

class YBDTPCache {
  static String? _cachePath;

  // 需要预先加载好
  static Future<String?> preCachePathInit() async {
    _cachePath = await YBDCommonUtil.getResourceDir('TPResource');
    return _cachePath;
  }

  static void downloadCache({bool update = true, void Function()? callback}) {
    // 更新下配置信息
    singlePathProfix(update: update);

    YBDDownloadCacheManager.instance.downloadURLToPath(
      zipURL(),
      cacheFileDirPath(),
      (String? path) {
        logger.v('YBDTPCache downloadCache end: $path');
        callback?.call();
      },
      isFileDir: true,
      failCallback: ({String? error, int? ms, String? url}) {
        // 下载失败
        YBDTATrack().track(YBDEventName.REPORT_EXCEPTION,
            properties: YBDTAProps(
                position: 'tpgo_download_fail',
                reason: maxParamValue(error),
                extParams:<String, dynamic>{
                  'ms':ms,
                  'url':url,
                  'item_name': 'fail'
                }).toJson());
      },
      durationCallback: ({String? url, int? ms}){
        YBDTATrack().track(YBDEventName.REPORT_EXCEPTION,
            properties: YBDTAProps(
                position: 'tpgo_download_success',
                extParams:<String, dynamic>{
                  'ms':ms,
                  'url':url,
                  'item_name': 'success'
                }).toJson());
      },
    );
  }

  // 同上面的方法的 Future形式 需要 await 实现
  static Future<String> downloadCacheFuture({bool update = true}) {
    Completer<String> completer = Completer<String>();

    downloadCache(update: update, callback: () {
      completer.complete('');
    });
    return completer.future;
  }

  static String cacheFileDirPath() {
    String name = singlePathProfix()!.split('\/').last;
    return _cachePath! + '/${singlePathProfix()!.generate_md5()}/$name';
  }

  static String? _singlePathProfix;

  static String? singlePathProfix({bool update = false}) {
    if (_singlePathProfix == null || update) {
      _singlePathProfix = YBDCommonUtil.getRoomOperateInfo().tpgoResourceProfix;
    }
    return _singlePathProfix;
  }

  // 与zip同名同目录
  static const String defaultProfix = 'https://d2zc7tapf7a4we.cloudfront.net/game/tpGo/tp_room';

  static String zipURL() {
    return singlePathProfix()! + '.zip';
  }
}
