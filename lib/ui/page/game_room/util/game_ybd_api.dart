import 'dart:async';

import 'package:get/get.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/quick_ybd_join_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_gradient_dialog.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/widget/dialog/confirm_ybd_dialog.dart';

class YBDGameApi {
  Function? onSuccess;

  ///快速加入
  static enterRoom({EnterGameRoomType? type, onSuccess()?}) async {
    logger.v('enter room with type: $type');
    if (type == EnterGameRoomType.Join || type == EnterGameRoomType.Watch) return onSuccess?.call();

    ///快速加入、创建房间请求接口
    YBDRtcHelper.getInstance().roomId = YBDRtcHelper.getInstance().info!.id.toString();
    if (type == EnterGameRoomType.QuickJoin) {
      quickJoin(type);
      return;
    }
    if (type == EnterGameRoomType.QuickGame) {
      quickGame(type);
      return;
    }
    YBDGameSocketApiUtil.joinGameRoom(type: type, roomId: YBDRtcHelper.getInstance().roomId!);
  }

  static joinRoom(String roomId, EnterGameRoomType? type, {int? waitingTime}) {
    YBDGameSocketApiUtil.joinGameRoom(type: type, roomId: roomId, waitingTime: waitingTime);
  }

  static quickJoin(EnterGameRoomType? type) async {
    YBDQuickJoinEntity? entity = await ApiHelper.quickJoin();
    if (entity?.code == GameConst.game_success) {
      // 传matchType到header
      YBDRtcHelper.getInstance().matchType = entity?.data?.matchType ?? GameConst.matchTypePerson;
      joinRoom(entity?.data?.roomId ?? '', type);
      return;
    }
    logger.v('22.11.2---ludo quick join no room');
    YBDDialogUtil.hideLoading(Get.context);
    YBDTPGradientDialog.show(
      Get.context!,
      title: 'tip'.i18n,
      width: 530,
      height: 390,
      type: DialogType.tpCommon,
      showClose: false,
      showGridBg: false,
      buttonNum: 2,
      color: ColorType.blue,
      textWidth: 480,
      textHeight: 110,
      contentMsg: entity?.message ?? '',
      onConfirm: () {
        ///直接创建房间
        logger.v('create game room');
        joinRoom(YBDRtcHelper.getInstance().info!.id.toString(), EnterGameRoomType.QuickCreate);
      },
      closeCallback: (_) {
        YBDDialogUtil.hideLoading(Get.context);
      },
    );
  }

  static void quickGame(EnterGameRoomType? type) async {
    YBDQuickJoinEntity? entity = await ApiHelper.quickGame();
    if (entity != null) {
      if (entity.code == GameConst.game_success) {
        YBDRtcHelper.getInstance().matchType = entity.data?.matchType ?? GameConst.matchTypePerson;
        joinRoom(entity.data?.roomId ?? '', type, waitingTime: entity.data?.waitingTime);
      } else {
        YBDDialogUtil.hideLoading(Get.context);
        YBDToastUtil.toast(entity.message);
      }
    }
  }
}
