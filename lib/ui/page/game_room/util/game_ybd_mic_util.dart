import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_mic_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_bloc.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_volume_event.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_event_handler.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/room/bloc/mini_ybd_profile_bloc.dart';
import 'package:oyelive_main/ui/page/room/widget/mini_ybd_profile_dialog.dart';
// import 'package:zego_express_engine/zego_express_engine.dart';
import 'package:oyelive_main/ui/page/game_room/ext/list_ybd_ext.dart';

class YBDGameMicUtil {
  /*
  ///stream update
  static updateStream(
      String room, ZegoUpdateType type, List<ZegoStream> streamList, Map<String, dynamic> extendedData) {
    assert(YBDRtcHelper.getInstance().roomId != room);
    if (YBDRtcHelper.getInstance().roomId != room) return;

    ///这里回调 是不是一定比服务端通知快
    logger.v('zego update stream: $streamList');
    tryCatch(() {
      List<String> streamIdList = [];
      streamList.map((e) {
        streamIdList.add(e.streamID);
        return e.streamID;
      }).toList();
      type == ZegoUpdateType.Add
          ? YBDRtcHelper.getInstance().streamIdList.addAll(streamIdList)
          : YBDRtcHelper.getInstance().streamIdList.removeWhere((element) => streamList?.contains(element) ?? false);
      YBDRtcHelper.getInstance().streamIdList.duplicate();
      if (type == ZegoUpdateType.Delete) {
        YBDRtcHelper.getInstance().getRtcApi.takeMic(micOperateType: RtcMicOperateType.down, streamIds: streamIdList);
      }
    });
  }

   */

  /// 开始推拉流
  static zegoStreamStateChange() {
    tryCatch(() {
      List<String> grabIds = YBDRtcHelper.getInstance().micList!.grab();
      List<String> muteIds = YBDRtcHelper.getInstance().micList!.mute();
      logger.i('ybd mic list: $grabIds $muteIds');

      ///grab 状态直接上麦
      YBDRtcHelper.getInstance().getRtcApi.takeMic(micOperateType: RtcMicOperateType.up, streamIds: grabIds);

      ///mute 状态 停止推流 按照下麦处理
      YBDRtcHelper.getInstance().getRtcApi.takeMic(micOperateType: RtcMicOperateType.down, streamIds: muteIds);
    });
  }

  /// 水波纹回调
  static zegoSoundLevelUpdate(List<YBDRtcVolumeInfo> infoList) {
    // logger.v('info list ----');
    YBDGameVolumeEvent event = new YBDGameVolumeEvent();
    event.speakers = infoList;
    YBDGameRoomSocketApi.getInstance().publishMessage(event);
  }

  /// 展示miniprofile
  static showMiniProfile({int? roomId, int? userId}) {
    showDialog(
      context: Get.context!,
      builder: (BuildContext builder) {
        return MultiBlocProvider(
          providers: [
            BlocProvider<YBDGameMicBloc>(create: (_) => YBDGameMicBloc(YBDGameMicBlocState(), Get.context)),
            BlocProvider<YBDMiniProfileBloc>(create: (_) => YBDMiniProfileBloc(Get.context, userId, roomId)),
          ],
          child: YBDMiniProfileDialog(userId, roomId, gameRoomType: true),
        );
      },
    );
  }
}
