import 'dart:async';

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart' as g;
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_game_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/room_socket/message/common/subscribe.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/socket_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_lobby/entity/enter_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base_send_resp.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_resp_result_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_socket_route.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';

import '../../../../common/room_socket/message/base/message.dart';
import '../entity/game_ybd_base.dart';

class YBDGameRoomSocketApi {
  static YBDGameRoomSocketApi? _instance;

  double packId = 0;

  YBDGameRoomSocketApi._();
  static YBDGameRoomSocketApi getInstance() {
    if (_instance == null) {
      _instance = YBDGameRoomSocketApi._();
    }
    return _instance!;
  }

  StreamController _messageStreamController = StreamController<YBDGameBase>.broadcast();

  StreamSink<YBDGameBase?> get _inAdd_message => _messageStreamController.sink as StreamSink<YBDGameBase?>;

  Stream<YBDGameBase> get messageOutStream => _messageStreamController.stream as Stream<YBDGameBase>;
  void publishMessage(YBDGameBase? message) {
    _inAdd_message.add(message);
  }
  void publishMessagemzTLpoyelive(YBDGameBase? message) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///点击进入房间
  enterRoom({required EnterGameRoomType? type, onSuccess(YBDEnterRoomEntity data)?, onTimeOut}) async {
    logger.v('enter game room : ${YBDRtcHelper.getInstance().roomId}');
    YBDGameRoomSocketApi.getInstance().sendGameMsg<YBDEnterRoomEntity>(
        type: SendMsgType.EnterRoom, body: await type.body, onSuccess: onSuccess, onTimeOut: onTimeOut);
  }

  ///取消订阅
  ///note: UI 界面没有退出房间不能取消订阅
  exitRoom({bool isConfirm = false}) {
    logger.i("exit roomid ${YBDRtcHelper.getInstance().roomId}");
    YBDGameRoomTrack().gameRoomTrack(event: YBDTATrackEvent.gameroom_exit);
    YBDDialogUtil.showLoading(Get.context);
    if (YBDRtcHelper.getInstance().roomId != YBDRtcHelper.getInstance().info!.id.toString()) {
      YBDGameRoomSocketApi.getInstance().sendGameMsg(
          type: SendMsgType.ExitRoom,
          body: isConfirm ? {'userConfirm': true} : {},
          onSuccess: (dynamic a) {
            YBDDialogUtil.hideLoading(Get.context);
          },
          onTimeOut: () {
            logger.v('exit room page time out 68');
            YBDDialogUtil.hideLoading(Get.context);
            YBDGameSocketApiUtil.pop();
          });
      return;
    }
    roomStatusUpdate(isConfirm: isConfirm);
  }

  ///房间状态更新 仅限房主操作
  roomStatusUpdate({GameRoomStatus status = GameRoomStatus.offline, bool? isConfirm}) {
    if (YBDRtcHelper.getInstance().roomId != YBDRtcHelper.getInstance().info!.id.toString()) return;
    logger.i("room status update $status");
    YBDGameRoomSocketApi.getInstance().sendGameMsg(
        type: SendMsgType.RoomStatus, body: status.body(YBDRtcHelper.getInstance().selectedGame?.subCategory));
    Future.delayed(Duration(milliseconds: 200), () {
      YBDGameRoomSocketApi.getInstance().sendGameMsg(
          type: SendMsgType.ExitRoom,
          onSuccess: (dynamic a) {
            YBDDialogUtil.hideLoading(Get.context);
          },
          onTimeOut: () {
            logger.v('board exit room page time out');
            YBDDialogUtil.hideLoading(Get.context);
            YBDGameSocketApiUtil.pop();
          });
    });
  }

  ///房主解散房间 点击离线弹窗 服务端需要主动调用exit
  exit() {
    if (YBDRtcHelper.getInstance().roomId == GameConst.defaultValue) return;
    YBDGameRoomSocketApi.getInstance().sendGameMsg(
        type: SendMsgType.ExitRoom,
        onSuccess: (dynamic a) {},
        onTimeOut: () {
          YBDGameSocketApiUtil.pop();
          logger.v('exit room page time out');
        });
  }

  ///麦位操作
  /// [SendMsgType] : 麦位操作枚举 [LockedMic] 、[GrabMic]、[DropMic]、[MuteMic]、[MuteSelfMic]
  /// index：麦位编号
  micOperation(int? targetId, int? index, SendMsgType type, {onSuccess(YBDGameBase data)?, onTimeOut}) {
    YBDGameRoomSocketApi.getInstance().sendGameMsg(
        type: type, body: type.body(targetId.toString(), index), onSuccess: onSuccess, onTimeOut: onTimeOut);
  }

  ///发送公聊消息
  sendPublicMessage({int? to, String? content}) {
    to = (to == null) ? -1 : to;
    YBDGameRoomSocketApi.getInstance().sendGameMsg(
        type: SendMsgType.ChatMsg,
        route: YBDGameSocketRoute()..mode = modeU,
        body: {"type": "public", "content": content});
  }

  ///发送快捷消息
  sendQuickMsg(String code, {onSuccess(YBDEnterRoomEntity data)?, onTimeOut}) async {
    YBDGameRoomSocketApi.getInstance().sendGameMsg<YBDEnterRoomEntity>(
        type: SendMsgType.SendQuickMsg,
        to: -1,
        route: YBDGameSocketRoute()..mode = modeM,
        body: {"type": "1", "data": code},
        onSuccess: onSuccess,
        onTimeOut: onTimeOut);
  }

  /// miniprofile禁言和踢人
  /// 房主A 踢用户 block 1.0 操作成功提示
  /// Leave 1.0 提示用户
  /// Exit 1.0 把被踢用户清理出房间
  block({String? target, String? muteType}) {
    YBDDialogUtil.showLoading(g.Get.context);
    YBDGameRoomSocketApi.getInstance().sendGameMsg(
        type: SendMsgType.Block,
        body: {"target": target, "duration": 30, "timeUnit": "second", "type": muteType},
        onTimeOut: () {
          YBDToastUtil.toast(translate('operation_fail'));
        });
  }

  /// onSuccess(E data)中的E泛型依赖JsonConvert解析
  sendGameMsg<E>(
      {YBDGameSocketRoute? route,
      int? to,
      Map<String, dynamic>? body,
      SendMsgType? type,
      onSuccess(E data)?,
      onTimeOut}) async {
    YBDSocketUtil.getInstance()!.sendSocketMsg(YBDGameBaseSendResp<YBDSubscribe, E>(
        toUser: to,
        body: body,
        msgType: type,
        route: route ?? YBDGameSocketRoute(),
        onSuccess: onSuccess,
        onTimeOut: onTimeOut));
  }
}

/****************************************************************************************************/
/***************************************    游戏相关   ***********************************************/
/****************************************************************************************************/
/// 游戏相关
extension GameSocketApiExt on YBDGameRoomSocketApi? {
  ///上/下座 index索引，下座无需index
  void gameSeatOperate(bool isTakeSeat, {int? index, void Function()? onSuccess}) {
    Map<String, dynamic> body = {};

    if (index != null) {
      body["index"] = index;
    }

    YBDGameRoomSocketApi.getInstance().sendGameMsg<YBDGameRespResultEntity>(
        type: isTakeSeat ? SendMsgType.TakeSeat : SendMsgType.LeaveSeat,
        body: body,
        onSuccess: (data) {
          if (data.code?.toString() == Const.HTTP_SUCCESS_NEW) {
            onSuccess?.call();
          }
        });
  }
  void gameSeatOperatexCJg3oyelive(bool isTakeSeat, {int? index, void Function()? onSuccess}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///准备 取消准备
  gameReadyOperater(bool isReady) {
    YBDGameRoomSocketApi.getInstance().sendGameMsg(type: isReady ? SendMsgType.Ready : SendMsgType.CancelReady);
  }

  /// 修改游戏配置
  void gameUpdateConfig(Map<String, dynamic> body) {
    YBDGameRoomSocketApi.getInstance().sendGameMsg(
      type: SendMsgType.UpdateGameConfig,
      body: body,
    );
  }
  void gameUpdateConfigtrx2noyelive(Map<String, dynamic> body) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 剔出玩家
  void gameKickOutPlayer(String kickUserId) {
    YBDGameRoomSocketApi.getInstance().sendGameMsg(
      type: SendMsgType.KickOutPlayer,
      route: YBDGameSocketRoute(to: [kickUserId]),
    );
  }

  ///开始游戏, 回传battleId
  gameStart({bool? addRobot, void Function(int)? onSuccess}) {
    var body = {
      "addRobot": addRobot,
    };
    YBDGameRoomSocketApi.getInstance().sendGameMsg<YBDGameRespResultEntity>(
        type: SendMsgType.Start,
        body: body,
        onSuccess: (data) {
          if (data.code?.toString() == Const.HTTP_SUCCESS_NEW) {
            int? battleId = data.getBodyIntValue("battleId");
            if (battleId != null) {
              onSuccess?.call(battleId);
            }
          }
        });
  }

  gameReport({String? battleId, String? code}) {
    var body = {
      "battleId": battleId,
      "status": 0, // 0 游戏开始失败， 1 游戏开局成功
      "code": code, // 游戏开始失败的错误编码
    };
    YBDGameRoomSocketApi.getInstance().sendGameMsg(
      type: SendMsgType.GameReport,
      body: body,
    );
  }

  ///送礼, 只有成功才会触发回调
  gifting({int? giftId, int? number, int? toUserId, void Function()? onSuccess}) {
    var body = {"giftId": giftId, "number": number};
    YBDGameRoomSocketApi.getInstance().sendGameMsg<YBDGameRespResultEntity>(
        type: SendMsgType.Gifting,
        route: YBDGameSocketRoute()..to = ["$toUserId"],
        body: body,
        onSuccess: (data) {
          if (data.code?.toString() == Const.HTTP_SUCCESS_NEW) onSuccess?.call();
        });
  }
}
