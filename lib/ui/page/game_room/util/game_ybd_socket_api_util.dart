import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/set_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/match_ybd_loading.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_gradient_dialog.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/room/widget/talent_ybd_offline_dialog.dart';
import 'package:oyelive_main/ui/widget/dialog/confirm_ybd_dialog.dart';

class YBDGameSocketApiUtil {
  /// QuickJoin, //快速加入
  /// QuickCreate, //快速创建房间
  /// Join
  static quickEnterRoom({required EnterGameRoomType type, String? roomId}) async {
    ///创建引擎
    YBDRtcHelper.getInstance().init();

    YBDDialogUtil.showLoading(Get.context);
    await YBDGameApi.enterRoom(
        type: type,
        // EnterGameRoomType.Join和Watch才会有onSuccess回调
        onSuccess: () async {
          if (roomId == null) {
            ///从任务进来的时候 这里是watch类型 不带roomId
            YBDGameApi.quickJoin(type);
            return;
          }
          YBDGameSocketApiUtil.joinGameRoom(type: type, roomId: roomId);
        });
  }

  static enterViaRoute({
    EnterGameRoomType type: EnterGameRoomType.Watch,
    String? subCategory,
    String? roomId,
  }) {
    /// 已经在游戏房间了，就不要做跳转了
    if (YBDObsUtil.instance().routeList.subContain(YBDNavigatorHelper.game_room_page)) return;
    YBDRtcHelper.getInstance().setGameSubType(subCategory);
    YBDGameSocketApiUtil.quickEnterRoom(type: type, roomId: roomId);
  }

  static joinGameRoom({required EnterGameRoomType? type, required String roomId, int? waitingTime}) async {
    if (roomId.isEmpty) {
      logger.v('join game room abnormal $roomId');
      return;
    }
    YBDRtcHelper.getInstance().roomId = roomId;
    YBDRtcHelper.getInstance().enterRoomType = type;
    if (waitingTime != null) {
      YBDDialogUtil.hideLoading(Get.context);
      logger.v('show fake loading ... time $waitingTime');
      // 添加假loading框
      showDialog<Null>(
          context: Get.context!, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return YBDMatchLoading(time: waitingTime);
          });
      return;
    }
    await YBDGameRoomSocketApi.getInstance().enterRoom(
        type: type,
        onSuccess: (gameBase) {
          YBDDialogUtil.hideLoading(Get.context);
        },
        onTimeOut: () {
          logger.v('enter room time out');
          YBDDialogUtil.hideLoading(Get.context);
        });
  }

  static showTalentOffline() {
    // 游戏房不处于顶层，也不要弹出离线弹窗，否则点击离线弹窗会退出所有界面，造成假死状态。
    logger.v('talent is offline: ${YBDRtcHelper.getInstance().roomId}, ${YBDObsUtil.instance().routeSets}');
    if (YBDRtcHelper.getInstance().roomId == GameConst.defaultValue ||
        !YBDObsUtil.instance().routeList.subContain(YBDNavigatorHelper.game_room_page) ||
        YBDObsUtil.instance().routeSets.last != YBDNavigatorHelper.game_room_page) return;
    if (YBDRtcHelper.getInstance().showOffline) return;
    // 主播自己不显示离线弹框
    if (YBDRtcHelper.getInstance().roomId == '${YBDCommonUtil.storeFromContext()!.state.bean!.id}') return;
    YBDRtcHelper.getInstance().showOffline = true;

    tryCatch(() {
      showDialog(
          context: Get.context!,
          builder: (context) {
            return YBDTalentOfflineDialog(closeCallback: () {
              logger.v('talent is offline exit: tap: ${YBDRtcHelper.getInstance().roomId}');
              YBDRtcHelper.getInstance().showOffline = false;
              Navigator.maybePop(context);
            });
          }).then((value) {
        if (YBDRtcHelper.getInstance().showOffline) {
          Navigator.maybePop(Get.context!);
        }
        YBDRtcHelper.getInstance().showOffline = false;

        ///服务端要求所有用户主动发送exit room 不过滤房主
        logger.v('talent is offline exit: ${YBDRtcHelper.getInstance().roomId}');
        YBDGameRoomSocketApi.getInstance().exit();

        Future.delayed(Duration(milliseconds: 300), () {
          pop();
        });
      });
    });
  }

  static gamingToast(String msg) {
    YBDTPGradientDialog.show(
      Get.context!,
      title: 'tip'.i18n,
      width: 530,
      height: 400,
      type: DialogType.tpCommon,
      showClose: false,
      showGridBg: false,
      buttonNum: 2,
      color: ColorType.blue,
      textWidth: 510,
      contentMsg: msg,
      onConfirm: () {
        ///强制退出
        logger.v('exit room by :$msg');
        YBDGameRoomSocketApi.getInstance().exitRoom(isConfirm: true);
      },
    );
  }

  static pop() {
    logger.v('game room page pop');
    var isLudo = false;
    tryCatch(() {
      var logic = Get.find<YBDGameRoomGetLogic>();
      if (logic.state!.gameSubCategory == "ludo") {
        /// 如果是ludo游戏，需要延迟100ms，先隐藏webview，
        /// 再执行退出操作，避免退出闪烁问题
        isLudo = true;
      }
    });
    if (isLudo) {
      Future.delayed(Duration(milliseconds: 100), () {
        logger.v('ludo pop');
        _popRoom();
      });
    } else {
      _popRoom();
    }
  }

  static _popRoom() {
    logger.v('game room pop');
    YBDRtcHelper.getInstance().getRtcApi.leaveChannel();
    YBDRtcHelper.getInstance().dispose(islogout: true);
    while (Get.currentRoute == YBDNavigatorHelper.game_room_page) {
      if (!Navigator.canPop(Get.context!)) break;
      Navigator.pop(Get.context!);
    }
  }
}
