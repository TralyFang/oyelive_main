import 'dart:async';


import 'dart:convert';

import 'package:get/get.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_config.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';

class YBDGameCommomUtil{
  static YBDGameRoomConfig getGameConfig() {
    final store = YBDCommonUtil.storeFromContext(context: Get.context);
    Map? result;
    tryCatch((){
      // logger.v('store state configs: ${store.state.configs.gameRoomConfig}');
      result = jsonDecode(store?.state?.configs?.gameRoomConfig ?? '');
    });
    // logger.v('store state configs: $result');
    YBDGameRoomConfig info = YBDGameRoomConfig.get(result);
    return info;
  }
}