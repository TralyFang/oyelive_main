import 'dart:async';

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/receive/received_ybd_message_type.dart';
import 'package:oyelive_main/common/web_socket/receive/received_ybd_socket_message.dart';
import 'package:oyelive_main/common/web_socket/socket_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/chat_ybd_publish.dart';
import 'package:oyelive_main/ui/page/game_room/entity/enter_ybd_room_body_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gifting_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_mic_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_exit_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/room_ybd_status_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_event_handle/zego_ybd_event_handle.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_mic_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/chat_ybd_logger.dart';

void subAck({required YBDReceivedSocketMessage receivedSocketMessage}) {
  print('suback msg data: ${receivedSocketMessage.data}');
  logger.i('suback msg data: ${receivedSocketMessage.data}');
  logger.i('suback msg code: ${receivedSocketMessage.headers.keys.contains(GameConst.code)}');
  YBDSocketUtil.getInstance()!.matchRespMessage(receivedSocketMessage);
  String topic = receivedSocketMessage.headers[GameConst.topic]!;
  if (!topic.startsWith('/room')) return;
  List<String> topicList = topic.split('/');

  ///房间信息不存在，或者服务端返回的非当前房间信息时不处理该信息
  logger.i('suback msg topic abnormal: roomId: ${YBDRtcHelper.getInstance().roomId},topic: ${topicList.last}');
  if (YBDRtcHelper.getInstance().roomId == GameConst.defaultValue || YBDRtcHelper.getInstance().roomId != topicList.last) {
    //ToDo 测试用
    logger.i('suback msg topic abnormal: $topicList, ${topicList.last}, ${topicList.last.runtimeType}');
    // if (Const.TEST_ENV) YBDToastUtil.toast('topic message abnormal');
    return;
  }

  if (!toastErrorCode(receivedSocketMessage)) return;
  logger.i('suback msg cmd: ${receivedSocketMessage.headers.keys.contains(GameConst.cmd)}');
  if (!receivedSocketMessage.headers.keys.contains(GameConst.cmd)) return;

  cmd(receivedSocketMessage: receivedSocketMessage);
}

toastErrorCode(YBDReceivedSocketMessage receivedSocketMessage) {
  ///如过头部不带code 就直接pass 不管
  if (!receivedSocketMessage.headers.keys.contains(GameConst.code)) return true;
  String? code = receivedSocketMessage.headers[GameConst.code];
  if (code == GameConst.game_insufficen) {
    return true;
  }
  logger.i(
      'suback msg codemsg: ${receivedSocketMessage.headers.keys.contains(GameConst.codeMsg)}, code:$code, codeMsg:${receivedSocketMessage.headers[GameConst.codeMsg]}');
  if (code != GameConst.game_success && receivedSocketMessage.headers.keys.contains(GameConst.codeMsg)) {
    YBDDialogUtil.hideLoading(Get.context);
    String? codeMsg = receivedSocketMessage.headers[GameConst.codeMsg];
    errorCode(code, codeMsg, receivedSocketMessage.headers[GameConst.cmd]);
    return false;
  }
  return true;
}

void cmd({required YBDReceivedSocketMessage receivedSocketMessage}) {
  String? cmd = receivedSocketMessage.headers[GameConst.cmd];
  switch (cmd) {
    case GameConst.enterRoomCmd:
      tryCatch(() {
        logger.i('receive suback msg enter room');
        YBDEnterRoomBodyEntityEntity? enter =
            YBDEnterRoomBodyEntityEntity().fromJson(json.decode(receivedSocketMessage.data));
        YBDRtcHelper.getInstance()
            .setRtcType(enter.body?.provider?.type == GameConst.zego ? RtcType.RtcTypeZego : RtcType.RtcTypeAgora);

        ///里面回调无效
/*
        if (enter?.body?.provider?.type == GameConst.zego)
          YBDRtcHelper.getInstance().getRtcApi.setEventHandler(YBDZegoEventHandle());

 */
        logger.v('set zego handle success');
        YBDRtcHelper.getInstance()
            .getRtcApi
            .joinChannel(token: enter.body?.provider?.token, channelName: YBDRtcHelper.getInstance().getRoomId());
      });
      break;
    case GameConst.EntryNotifyCmd:
      break;
    case GameConst.RoomInfoNotify:
      // 房间基础信息更新
      YBDGameRoomInfoEntity? entity = YBDGameRoomInfoEntity().fromJson(json.decode(receivedSocketMessage.data));
      YBDGameRoomSocketApi.getInstance().publishMessage(entity);
      break;
    case GameConst.sendQuickMsgCmd:
      // 房间消息
      YBDChatPublish? entity = YBDChatPublish().fromJson(json.decode(receivedSocketMessage.data));
      if (entity.body?.type == GameConst.PublicChat) YBDGameRoomSocketApi.getInstance().publishMessage(entity);
      break;
    case GameConst.EntryMicCmd:
      logger.v('3.23---gameEntryMicCmd data:${receivedSocketMessage.data}');
      logger.v('3.23---gameEntryMicCmd data:${YBDGameMicInfoEntity().fromJson(json.decode(receivedSocketMessage.data))}');
      YBDGameMicInfoEntity micInfoEntity = YBDGameMicInfoEntity().fromJson(json.decode(receivedSocketMessage.data));
      YBDGameRoomSocketApi.getInstance().publishMessage(micInfoEntity);
      YBDRtcHelper.getInstance().micList = micInfoEntity.body?.mic;
      YBDGameMicUtil.zegoStreamStateChange();
      break;
    case GameConst.GrabMicCmd:
    case GameConst.DropMicCmd:
    case GameConst.LockMicCmd:
    case GameConst.UnlockMicCmd:
    case GameConst.MuteMicCmd:
    case GameConst.UnMuteMicCmd:
      logger.v('mic operator cmd: $cmd success');
      break;
    case GameConst.GiftingNotifyCmd:
      // 送礼通知
      YBDGameGiftingNotifyEntity? entity = YBDGameGiftingNotifyEntity().fromJson(json.decode(receivedSocketMessage.data));
      YBDGameRoomSocketApi.getInstance().publishMessage(entity);
      break;
    case GameConst.GiftingCmd:
      // 送礼结果
      break;
    case GameConst.GameStartResultCmd:
      // 游戏开始通知
      break;
    case GameConst.GameSettlementCmd:
      // 游戏结算
      break;
    case GameConst.KickOutPlayerCmd:
      // 游戏踢出玩家
      break;
    case GameConst.UpdateGameConfigCmd:
      // 游戏配置修改
      break;
    case GameConst.ReadyGameCmd:
    case GameConst.CancelReadyGameCmd:
      // 游戏准备/取消准备
      break;
    case GameConst.TakeSeatCmd:
    case GameConst.LeaveSeatCmd:
      // 游戏上座/下座
      break;
    case GameConst.RoomStatusNotify:
      // 游戏状态 通知(默认配置等信息)
      YBDRoomStatusNotifyEntity? entity = YBDRoomStatusNotifyEntity().fromJson(json.decode(receivedSocketMessage.data));
      YBDGameRoomSocketApi.getInstance().publishMessage(entity);
      break;
    case GameConst.ChatNotifyCmd:
      // YBDGameRoomSocketApi.getInstance().publishMessage();
      YBDChatLogger.getInstance().add(YBDChatPublish().fromJson(json.decode(receivedSocketMessage.data)));
      break;
    case GameConst.chatCmd:
      // 消息
      break;
    case GameConst.RoomStatusNotifyCmd:
      break;
    case GameConst.exitRoomCmd:
      YBDGameRoomExitEntity? entity = YBDGameRoomExitEntity().fromJson(json.decode(receivedSocketMessage.data));
      YBDGameRoomSocketApi.getInstance().publishMessage(entity);
      YBDGameSocketApiUtil.pop();
      break;
    case GameConst.LeaveCmd:
      // 房主解散房间
      logger.v('room owner exit game room');
      YBDGameSocketApiUtil.showTalentOffline();
      break;
    case GameConst.BlockCmd:
      logger.v('block success');
      YBDToastUtil.toast(translate('operation_success'));
      break;
    default:
      break;
  }
}

errorCode(String? code, String? codeMsg, String? cmd) {
  switch (cmd) {
    case GameConst.RoomStatusCmd: //忽略掉 状态修改的报错
      logger.i('room status change fail');
      break;
    case GameConst.enterRoomCmd:

      ///Todo 重新订阅异常时  退出房间
      ///Todo 下期根据code 区分业务场景
      YBDToastUtil.toast(codeMsg ?? translate('failed'));
      if (Get.currentRoute == YBDNavigatorHelper.game_room_page) {
        YBDGameSocketApiUtil.pop();
      }
      break;
    default:
      {
        switch (code) {
          case GameConst.game_exitFail:
            logger.e('game room cmd error code: $code, codeMsg:$codeMsg, cmd:$cmd');
            YBDGameSocketApiUtil.gamingToast(codeMsg ?? '');
            break;
          default:
            YBDToastUtil.toast(codeMsg ?? translate('failed'));
            break;
        }
      }
      break;
  }
}
