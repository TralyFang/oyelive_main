import 'dart:async';

import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';

class YBDGameHeaderOperatorCenter{
    static exitRoom(){
        tryCatch((){
          YBDGameRoomSocketApi.getInstance().exitRoom();
        });
    }
}