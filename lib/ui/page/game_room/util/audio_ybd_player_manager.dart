import 'dart:async';

import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

class YBDAudioPlayerManager {

  static YBDAudioPlayerManager? _helper;
  YBDAudioPlayerManager._();
  static YBDAudioPlayerManager? _getInstance() {
    if (_helper == null) {
      var instance = YBDAudioPlayerManager._();
      _helper = instance;
    }
    return _helper;
  }
  /// 向外暴露的单例类方法
  static YBDAudioPlayerManager? get instance => _getInstance();

  /// 音频播放器
  AudioPlayer? _audioPlayer;
  ///
  AudioCache? _assetsPlayer;

  initPlayer({PlayerMode mode = PlayerMode.MEDIA_PLAYER, String? playerId}) {
    if (_audioPlayer != null && _audioPlayer?.mode == mode) {
      return;
    }
    _audioPlayer = AudioPlayer(mode: mode, playerId: playerId);
    // AudioPlayer.logEnabled = true;
    _audioPlayer!.onPlayerCompletion.listen((event) async {
    });
    _audioPlayer!.onPlayerStateChanged.listen((event) {
      logger.v('YBDAudioPlayerManager player state : $event');
    });
    _audioPlayer!.onPlayerError.listen((event) {
      logger.v('YBDAudioPlayerManager player state : $event');
    });
  }

  /// 播放 mp3 playerId：相同的playerId可以共用同一个播放器
  playAudio(String? filePath, {double volume = 1.0, PlayerMode mode = PlayerMode.MEDIA_PLAYER, String? playerId}) async {
    if (filePath == null) {
      logger.v('YBDAudioPlayerManager play mp3 filePath is null');
      return;
    }

    // 看看是不是asset文件
    if (filePath.startsWith("assets/")) {
      if (_assetsPlayer == null) {
        initPlayer(mode: mode, playerId: playerId);
        _assetsPlayer = AudioCache(fixedPlayer: _audioPlayer);
      }

      try {
        /// _assetsPlayer自带了assets，需要移除多余的assets/
        var repath = filePath.replaceFirst(RegExp(r'assets/'), "");
        final result = await _assetsPlayer!.play(repath, volume: volume, mode: mode);
        logger.v('YBDAudioPlayerManager assets play _playAudio result: $result');
      } catch (e) {
        // 捕获异常：FileSystemException: Cannot create file, path = '/data/user/0/com.oyetalk.tv/cache/' (OS Error: Is a directory, errno = 21)
        logger.v('YBDAudioPlayerManager assets mp3 error: $e');
      }
      return;
    }

    // 本地磁盘文件
    File audioFile = File(filePath);
    if (!audioFile.existsSync()) {
      logger.v('YBDAudioPlayerManager no audio file : $filePath');
    }

    // 初始化播放器
    initPlayer(mode: mode, playerId: playerId);

    try {
      final result = (await _audioPlayer?.play(audioFile.path, isLocal: true, volume: volume))!;
      logger.v('YBDAudioPlayerManager file play _playAudio result: $result');
    } catch (e) {
      // 捕获异常：FileSystemException: Cannot create file, path = '/data/user/0/com.oyetalk.tv/cache/' (OS Error: Is a directory, errno = 21)
      logger.v('YBDAudioPlayerManager file mp3 error: $e');
    }

  }

  // 不用的时候要移除掉
  dispose() {
    _assetsPlayer?.fixedPlayer?.release();
    _audioPlayer?.release();
    _audioPlayer = null;
    _assetsPlayer = null;
  }
}