import 'dart:async';

import 'dart:async';
import 'dart:io';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_cache_manager/src/storage/cache_object.dart';
import 'package:path_provider/path_provider.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

class YBDWebCacheManager {
  static YBDWebCacheManager? _instance;
  factory YBDWebCacheManager() {
    _instance ??= YBDWebCacheManager._();
    return _instance!;
  }
  YBDWebCacheManager._();

  /**
   * web拦截下载的文件
   * http://121.37.214.86/event/luckyPatternJune2k22/img/btns/rewardsBtn.png
   * 去掉域名，使用path创建本地文件
   * 还需要做一个过期资源删除
   * 或者放在DefaultCacheManager目录下，由DefaultCacheManager管理
   *
   * */
  /// web缓存管理下载入口 可以设置存储本地目标地址locationPath
  Future<File> getWebDownloadFile(String url, {String? locationPath}) async {
    var targetPath = await locationTargetPath(url, locationPath: locationPath);
    logger.v("download cache path webCache url: $url, path: $targetPath");
    if (File(targetPath).existsSync()) {
      return _getFile(url, targetPath);
    }
    return _putFile(url, targetPath);
  }
  void getWebDownloadFileLCYtEoyelive(String url, {String? locationPath})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 根据url生成本地缓存地址 temp/libCachedImageData/host/path
  Future<String> locationTargetPath(String url, {String? locationPath}) async {
    if (locationPath != null) return locationPath;
    Uri uri = Uri.parse(url);
    var targetPath = "${await getCacheManagerPath()}" +
        "${Platform.pathSeparator}${uri.host.generate_md5()}" +
        uri.path;
    return targetPath;
  }
  void locationTargetPathyh5tkoyelive(String url, {String? locationPath})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 缓存地址的默认路径
  Future<String> getCacheManagerPath() async {
    Directory directory = await getTemporaryDirectory();
    var targetPath = directory.path +
        "${Platform.pathSeparator}${DefaultCacheManager.key}";
    return targetPath;
  }
  void getCacheManagerPathOm0WYoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<bool> fileExists(String url, {String? locationPath}) async {
    return File(await locationTargetPath(url, locationPath:locationPath)).existsSync();
  }

  Future<File> _downloadFile(String url, String targetPath) async {
    File loadFile = await DefaultCacheManager().getSingleFile(url);

    // 下载文件到指定目录
    File(targetPath)
      ..createSync(recursive: true)
      ..writeAsBytesSync(loadFile.readAsBytesSync());

    // 删除插件默认目录下的下载文件，结合上一句就是移动文件
    loadFile.deleteSync();
    logger.v("download cache path webCache _downloadFile: $targetPath");

    return File(targetPath);
  }
  void _downloadFileiPXf3oyelive(String url, String targetPath)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 获取文件
  Future<File> _getFile(String url, String targetPath) async {
    var key = targetPath;
    final FileInfo? cacheFile = await DefaultCacheManager().getFileFromCache(key);
    if (cacheFile != null) {
      if (cacheFile.validTill.isBefore(DateTime.now())) {
        // 文件已经过期了，重新下载
        logger.v("download cache path webCache validTill: ${cacheFile.validTill}");
        unawaited(_putFile(url, targetPath));
      }
      logger.v("download cache path webCache _getFile: ${cacheFile.file.path}");
      return cacheFile.file;
    }
    return _putFile(url, targetPath);
  }
  void _getFile8aIXsoyelive(String url, String targetPath)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<File> _putFile(String url, String targetPath) async {
    try {
      await _downloadFile(url, targetPath);
    } catch (e) {
      // 下载地址异常
      logger.e("download cache path webCache _downloadFile error: ${e.toString()}");
    }

    var key = targetPath;
    var cacheObject = await DefaultCacheManager().store.retrieveCacheData(key);
    cacheObject ??= CacheObject(url, key: key, relativePath: targetPath, validTill: DateTime.now().add(Duration(days: 30)));

    cacheObject = cacheObject.copyWith(
      // 文件有效期30天
      validTill: DateTime.now().add(Duration(days: 30)),
    );
    unawaited(DefaultCacheManager().store.putFile(cacheObject));

    logger.v("download cache path webCache _putFile: ${cacheObject.key}");

    return File(targetPath);
  }
  void _putFileS7Tljoyelive(String url, String targetPath)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
