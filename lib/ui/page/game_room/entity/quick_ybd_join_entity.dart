import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDQuickJoinEntity with JsonConvert<YBDQuickJoinEntity> {
  String? code;
  String? message;
  YBDQuickJoinData? data;
}

class YBDQuickJoinData with JsonConvert<YBDQuickJoinData> {
  String? roomId;
  String? matchType; // 1-人机
  int? waitingTime; // ludo quick game loading随机时间
}
