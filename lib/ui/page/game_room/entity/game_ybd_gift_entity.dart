import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDGameGiftEntity with JsonConvert<YBDGameGiftEntity> {
  String? code;
  List<YBDGameGiftData?>? data;
  bool? success;
}

class YBDGameGiftData with JsonConvert<YBDGameGiftData> {
  int? id;
  String? name;
  int? status;
  int? label;
  int? index;
  int? price;
  String? currency;
  String? currencyImage;
  int? count;
  int? earn;
  String? img;
  int? display;
  dynamic attribute;
  YBDGameGiftDataAnimation? animation;
  String? animationFile;
  int? animationDisplay;
}

class YBDGameGiftDataAnimation with JsonConvert<YBDGameGiftDataAnimation> {
  String? type;
}
