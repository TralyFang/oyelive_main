import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDGameUsersAttribute with JsonConvert<YBDGameUsersAttribute> {
  String? gender;
  String? nickName;
  String? avatar;
  int? level;
  int? roomLevel;
  String? id;
}