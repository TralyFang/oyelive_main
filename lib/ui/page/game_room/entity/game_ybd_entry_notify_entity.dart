import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/game_room/entity/enter_ybd_room_body_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_user_attribute.dart';

class YBDGameEntryNotifyEntity with JsonConvert<YBDGameEntryNotifyEntity> {
	YBDGameEntryNotifyBody? body;
	String? cmd;
	YBDGameEntryNotifyResources? resources;
	YBDEnterRoomBodyEntityRoute? route;
	bool? success;
}

class YBDGameEntryNotifyBody with JsonConvert<YBDGameEntryNotifyBody> {
	String? userId;
}

class YBDGameEntryNotifyResources with JsonConvert<YBDGameEntryNotifyResources> {
	YBDGameEntryNotifyResourcesUser? user;
}

class YBDGameEntryNotifyResourcesUser with JsonConvert<YBDGameEntryNotifyResourcesUser> {
	YBDGameUsersAttribute? attribute;
	String? clientType;
	int? connectionId;
	String? projectCode;
	String? roomId;
	String? sessionId;
	String? tenantCode;
	String? userId;
}
