import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/widget/rich_ybd_text_decoder.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

class YBDChatPublish extends YBDGameBase with JsonConvert<YBDChatPublish> {
  YBDChatPublishBody? body;
  YBDResources? resources;
}

class YBDChatPublishBody with JsonConvert<YBDChatPublishBody> {
  String? type;
  String? sender;
  YBDPlainText? content;
}
