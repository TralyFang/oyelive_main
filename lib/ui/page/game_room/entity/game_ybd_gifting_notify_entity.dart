import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/game_room/entity/enter_ybd_room_body_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

class YBDGameGiftingNotifyEntity extends YBDGameBase with JsonConvert<YBDGameGiftingNotifyEntity> {
	String? cmd;
	String? ver;
	YBDEnterRoomBodyEntityRoute? route;
	int? code;
	String? codeMsg;
	YBDGameGiftingNotifyBody? body;
	YBDEnterRoomBodyEntityResources? resource;
}

class YBDGameGiftingNotifyBody with JsonConvert<YBDGameGiftingNotifyBody> {
	String? name;
	String? imageUrl;
	String? animationUrl;
	int? giftId;
	int? number;
	int? receiver;
	int? sender;
	/// 已经下载好的svga本地路径
	String? animationLocalPath;
	/// 已经下载好的mp3本地路径
	String? animationMp3LocalPath;
}

class YBDGamePlayerPosition with JsonConvert<YBDGamePlayerPosition> {
	num? centerX;
	num? centerY;
	int? userId;
}
