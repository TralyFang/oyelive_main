import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

class YBDGameRoomExitEntity extends YBDGameBase with JsonConvert<YBDGameRoomExitEntity> {
  String? cmd;
  String? code;
  String? codeMsg;
  YBDGameRoomExitRoute? route;
}

class YBDGameRoomExitRoute with JsonConvert<YBDGameRoomExitRoute> {
  String? from;
  String? mode;
  String? room;
  List<String>? to;
}
