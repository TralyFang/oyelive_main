import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDGameCategoryEntity with JsonConvert<YBDGameCategoryEntity> {
	String? code;
	List<YBDGameCategoryData?>? data;
	bool? success;
	String? message;
}

class YBDGameCategoryData with JsonConvert<YBDGameCategoryData> {
	String? projectCode;
	String? tenantCode;
	/// 分类 game、voice
	String? category;
	/// 游戏类别 ludo
	String? subCategory;
	/// 状态，1-enable, 0-disable
	int? status;
	/// 支持的最低版本
	int? appMinVersion;
	String? selectedImage;
	String? image;
	/// 点击跳转url
	String? redirectUrl;
	/// 扩展属性
	dynamic property;
}
