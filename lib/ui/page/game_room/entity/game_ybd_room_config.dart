import 'dart:async';


import '../util/rtc_ybd_log.dart';

class YBDGameRoomConfig {
  ///zego 码率
  late int bitrate;

  ///zego room 最大人数
  late int maxMemberCount;

  ///悬浮球图标
  String? suspendIcon;

  ///跳转链接
  late String suspendLink;

  ///活动页title
  String? sudpendTitle;

  ///归因名称白名单 eg: 2100163/2100153
  String? userIds;

  ///是否显示TPGO房间里面的任务图标
  late bool showTPGOTaskIcon;

  ///TPGO发牌速度-- eg: 200/350  左右4个位置：200ms 下面的位置：350ms
  late String cardSpeed;

  /// 需要ping的地址
  late String pings;

  YBDGameRoomConfig();

  static YBDGameRoomConfig get(Map? result) {
    logger.v('game_room_operate: ${result}');
    return result == null ? YBDGameRoomConfig.def() : YBDGameRoomConfig.from(result);
  }

  YBDGameRoomConfig.from(Map result) {
    this.bitrate = int.tryParse(result['bitrate'] ?? '32') ?? 32;
    this.maxMemberCount = int.tryParse(result['maxMemberCount'] ?? '100') ?? 100;
    this.suspendIcon = result['suspendIcon'] ?? 'https://d2zc7tapf7a4we.cloudfront.net/game/icon/room/task/task1.webp';
    this.suspendLink = result['suspendLink'] ?? '';
    this.sudpendTitle = result['sudpendTitle'] ?? '';
    this.userIds = result['userIds'] ?? '';
    this.showTPGOTaskIcon = result['showTPGOTaskIcon'] ?? false;
    this.cardSpeed = result['cardSpeed'] ?? '200/350';
    this.pings = result['pings'] ??
        'facebook---m.facebook.com||google---accounts.google.com||twitter---twitter.com||oyetalk---oyetalk.live';
  }

  static YBDGameRoomConfig def() {
    return YBDGameRoomConfig()
      ..bitrate = 32
      ..maxMemberCount = 100
      ..suspendIcon = 'https://d2zc7tapf7a4we.cloudfront.net/game/icon/room/task/task1.webp'
      ..suspendLink = ''
      ..sudpendTitle = ''
      ..userIds = ''
      ..showTPGOTaskIcon = false
      ..cardSpeed = '200/350'
      ..pings =
          'facebook---m.facebook.com||google---accounts.google.com||twitter---twitter.com||oyetalk---oyetalk.live';
  }
}
