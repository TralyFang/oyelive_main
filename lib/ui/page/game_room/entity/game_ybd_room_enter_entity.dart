
import 'package:intl/intl.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';

class YBDGameRoomEnterEntity with JsonConvert<YBDGameRoomEnterEntity> {
  String? code;
  YBDGameRoomEnterData? data;
  bool? success;
  String? message;

  YBDGameRoomEnterEntity fromJson(Map<String, dynamic> json) {
    // runtimeType
    return super.fromJson(json);
  }
  // T asT<T extends Object>(dynamic value) {
  // 	try {
  // 		String type = T.toString();
  // 		if (type == "DateTime") {
  // 			return DateFormat("dd.MM.yyyy").parse(value) as T;
  // 		}else{
  // 			return super.asT<T>(value);
  // 		}
  // 	} catch (e, stackTrace) {
  // 		print('asT<$T> $e $stackTrace');
  // 		return null;
  // 	}
  // }
}

class YBDGameRoomEnterData with JsonConvert<YBDGameRoomEnterData> {
  List<YBDGameRoomEnterDataRows>? rows;
  int? total;
}

class YBDGameRoomEnterDataRows with JsonConvert<YBDGameRoomEnterDataRows> {
  dynamic id;
  String? projectCode;
  String? roomId;
  String? tenantCode;
  String? category;
  String? subCategory;
  String? status;
  String? subStatus;
  YBDGameRoomEnterDataRowsProperty? property;
  List<YBDGameRoomEnterDataRowsUsers>? users;
  int? userCount;

  /// 游戏可加入状态，否则只能观看
  gameJoinStatus() {
    if (subStatus == GameConst.GameGrouping) {
      // 组队中
      return true;
    } else if (subStatus == GameConst.GameGaming) {
      // 游戏中
      return false;
    }
    return false;
  }
}

class YBDGameRoomEnterDataRowsProperty with JsonConvert<YBDGameRoomEnterDataRowsProperty> {
  YBDGameRoomEnterDataRowsPropertyPubAttr? pubAttr;
  YBDGameRoomEnterDataRowsPropertyGameAttr? gameAttr;
}

class YBDGameRoomEnterDataRowsPropertyPubAttr with JsonConvert<YBDGameRoomEnterDataRowsPropertyPubAttr> {
  String? poster;
  String? title;
}

class YBDGameRoomEnterDataRowsPropertyGameAttr with JsonConvert<YBDGameRoomEnterDataRowsPropertyGameAttr> {
  /// 模式
  String? model;

  /// 金额
  int? amount;

  /// 币种
  String? currency;

  // tpgo 匹配模式
  String? modeName;
}

class YBDGameRoomEnterDataRowsUsers with JsonConvert<YBDGameRoomEnterDataRowsUsers> {
  String? userId;
  YBDGameRoomEnterDataRowsUsersAttribute? attribute;
}

class YBDGameRoomEnterDataRowsUsersAttribute with JsonConvert<YBDGameRoomEnterDataRowsUsersAttribute> {
  String? gender;
  String? nickName;
  String? avatar;
  int? level;
  int? roomLevel;
}
