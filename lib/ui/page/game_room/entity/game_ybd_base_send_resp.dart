import 'dart:async';


import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_socket_route.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';

class YBDBaseMsg {
  String? cmd;
  String? mode;
  String? type;
  String? roomId;
  YBDGameSocketRoute? route;
  Map<String, dynamic>? body;

  YBDBaseMsg({this.cmd, this.mode, this.route, this.body,this.roomId});

  Map<String, dynamic> toJsonContent() {
    return {
      "cmd": this.cmd,
      "ver": GameConst.bodyVer,
      "mode": this.mode ?? modeU,
      "route": this.route!.toMap(),
      "body": this.body
    };
  }

  YBDBaseMsg fromMap(Map json) {
    type = json['type'];
    mode = json['mode'];
    return this;
  }
}

class YBDGameBaseSendResp<T, E> extends YBDBaseMsg {
  YBDGameSocketRoute? route = YBDGameSocketRoute();

  //成功匹配消息的回调
  Function? onSuccess;

  //超时的回调
  Function? onTimeOut;

  //不用填自动会生成
  DateTime? sendTime;

  //不用填，自动生成
  int? packetId;

  Map<String, dynamic>? body;

  SendMsgType? msgType;

  YBDGameBaseSendResp(
      {var fromUser,
      var toUser,
      String? mode,
      this.body,
      this.route,
      this.msgType,
      this.onSuccess(E data)?,
      this.onTimeOut})
      : super(route: route, mode: mode, cmd: msgType.value, roomId: YBDRtcHelper.getInstance().roomId);

  callSuccess(json) {
    return onSuccess?.call(JsonConvert.fromJsonAsT<E>(json));
  }

  @override
  Map<String, dynamic> toJsonContent() {
    // TODO: implement toJson
    Map<String, dynamic> json = super.toJsonContent();
    logger.v("send message super: $json");

    try {
      if (body != null) {
        json.addAll(body!);
      }
      json.removeWhere((key, value) => value == null);
      logger.v("game base send message:$json");
      return json;
    } catch (e) {
      logger.e(e);
      json.removeWhere((key, value) => value == null);
      return json;
    }
  }
}
