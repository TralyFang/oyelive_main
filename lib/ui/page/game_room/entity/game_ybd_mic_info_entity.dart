import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

class YBDGameMicInfoEntity extends YBDGameBase with JsonConvert<YBDGameMicInfoEntity> {
  YBDGameMicInfoBody? body;
  String? cmd;
  String? code;
  String? codeMsg;

  // String ver;
  YBDGameMicInfoResources? resources;
  YBDGameMicInfoRoute? route;
  bool? success;
}

class YBDGameMicInfoRoute with JsonConvert<YBDGameMicInfoRoute> {
  String? from;
  String? mode;
  String? room;
  List<String>? to;
}

class YBDGameMicInfoBody with JsonConvert<YBDGameMicInfoBody> {
  List<YBDGameMicInfoBodyMic?>? mic;
}

class YBDGameMicInfoBodyMic with JsonConvert<YBDGameMicInfoBodyMic> {
  int? index;
  String? roomId;
  String? status;
  String? userId;
  int? version;
  String? avatar;

  bool get micOccupied {
    if (status != null && status!.isNotEmpty) {
      return status == GameConst.Grabbed ||
          status == GameConst.Muted ||
          status == GameConst.MuteSelf ||
          status == GameConst.MuteDouble;
    }
    return false;
  }
}

class YBDGameMicInfoResources with JsonConvert<YBDGameMicInfoResources> {
  dynamic user;
  YBDGameMicInfoResourcesUserAttribute? userAttribute;

  Map<String, dynamic> get userMap {
    try {
      return Map<String, dynamic>.from(user);
    } on Exception catch (e) {
      print('3.24----error:$e');
      return {};
    }
  }
}

// class GameMicInfoResourcesUser with JsonConvert<GameMicInfoResourcesUser> {
//   int createTime;
//   int freshness;
//   int id;
//   String projectCode;
//   GameMicInfoResourcesUserProperty property;
//   String tenantCode;
//   String userId;
// }

// class GameMicInfoResourcesUserProperty with JsonConvert<GameMicInfoResourcesUserProperty> {
//   String gender;
//   int level;
//   String nickName;
//   int roomLevel;
//   String id;
//   String avatar;
// }

class YBDGameMicInfoResourcesUserAttribute with JsonConvert<YBDGameMicInfoResourcesUserAttribute> {
  String? userId;
  String? name;
  String? identity;
  YBDGameMicInfoResourcesUserAttributeValue? value;
  int? expireAt;
  int? version;
}

class YBDGameMicInfoResourcesUserAttributeValue with JsonConvert<YBDGameMicInfoResourcesUserAttributeValue> {}
