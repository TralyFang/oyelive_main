import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/base/json_field.dart';

class YBDZegoCodeEntity with JsonConvert<YBDZegoCodeEntity> {
  String? code;
  YBDZegoCodeData? data;
  bool? success;
}

class YBDZegoCodeData with JsonConvert<YBDZegoCodeData> {
  String? code;
  int? expireDate;
}


class YBDZegoMGAPPKeyEntity with JsonConvert<YBDZegoMGAPPKeyEntity> {
  String? code;
  YBDZegoMGAPPKeyData? data;
  bool? success;
}

class YBDZegoMGAPPKeyData with JsonConvert<YBDZegoMGAPPKeyData> {
  @JSONField(name: "app_key")
  String? appKey;
  @JSONField(name: "app_id")
  String? appId;
}