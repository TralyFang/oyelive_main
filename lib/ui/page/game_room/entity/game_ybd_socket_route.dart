import 'dart:async';


import 'package:oyelive_main/common/room_socket/message/base/message.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';

class YBDGameSocketRoute {
  String? room;
  String? from;
  String? mode;
  List<String>? copyTo, to;

  YBDGameSocketRoute({this.room, this.from, this.to});

  toMap() {
    Map m = {
      'room': this.room ?? YBDRtcHelper.getInstance().roomId,
      'from': this.from ?? (YBDRtcHelper.getInstance().info?.id ?? -1).toString(),
      'mode': this.mode ?? modeU,
      'to': this.to
    };
    return m;//.removeWhere((key, value) => value == null);
  }
}
