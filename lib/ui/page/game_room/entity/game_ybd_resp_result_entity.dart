import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/game_room/entity/enter_ybd_room_body_entity.dart';

class YBDGameRespResultEntity with JsonConvert<YBDGameRespResultEntity> {
	String? cmd;
	String? ver;
	YBDEnterRoomBodyEntityRoute? route;
	int? code;
	String? codeMsg;
	var body;

	/// 获取body中的Int类型值，可能为空
	int? getBodyIntValue(String key) {
		if (body != null && (body is Map) && body[key] != null) {
			var value = body[key] is String
					? int.tryParse(body[key])
					: body[key].toInt();
			return value;
		}
		return null;
	}
}
