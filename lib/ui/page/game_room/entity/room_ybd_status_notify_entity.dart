import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';

class YBDRoomStatusNotifyEntity extends YBDGameBase with JsonConvert<YBDRoomStatusNotifyEntity> {
  String? cmd;
  String? ver;
  YBDRoomStatusNotifyRoute? route;
  YBDRoomStatusNotifyBody? body;
  YBDRoomStatusNotifyResources? resources;
}

class YBDRoomStatusNotifyRoute with JsonConvert<YBDRoomStatusNotifyRoute> {
  String? room;
  String? from;
  String? mode;
}

class YBDRoomStatusNotifyBody with JsonConvert<YBDRoomStatusNotifyBody> {
  String? category;
  YBDRoomStatusNotifyBodyGame? game;
  String? subCategory;
  String? roomId;
  String? status;
  String? subStatus;

  /// 游戏状态
  bool gamePlaying() {
    if (subStatus == GameConst.GameGrouping) {
      // 组队中
      return false;
    } else if (subStatus == GameConst.GameGaming || subStatus == GameConst.RobotMatch) {
      // 游戏中
      return true;
    }
    return false;
  }
  void gamePlayingfsw7doyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDRoomStatusNotifyBodyGame with JsonConvert<YBDRoomStatusNotifyBodyGame> {
  int? amount;
  String? currency;
  String? currencyImage; // 20220523服务器已经移除，没用
  int? minimum;
  int? maximum;
  String? model;
  String? needWinPawnDesc;
  String? status;
  int? battleId;
  int? gameId;
  List<int>? ticketLevel; // 已过期，使用optionalMode
  String? thirdPartyId;
  List<YBDRoomStatusNotifyBodyGameMembers?>? members;
  List<YBDRoomStatusNotifyBodyGameSettlementList?>? settlementList;
  YBDRoomStatusNotifyBodyGameAttribute? attribute;
  List<YBDRoomStatusGameMode?>? optionalMode;
  YBDRoomStatusNotifyBodyGameImage? image;
  String? matchType; // 1是机器人 2是真人
}

class YBDRoomStatusNotifyBodyGameImage with JsonConvert<YBDRoomStatusNotifyBodyGameImage> {
  String? logo;
  YBDRoomStatusNotifyBodyGameImageRule? rule;
}

class YBDRoomStatusNotifyBodyGameImageRule with JsonConvert<YBDRoomStatusNotifyBodyGameImageRule> {
  String? body;
  String? title;
}

class YBDRoomStatusNotifyBodyGameAttribute with JsonConvert<YBDRoomStatusNotifyBodyGameAttribute> {
  int? minWinPawn;
  String? url;
}

class YBDRoomStatusNotifyBodyGameMembers with JsonConvert<YBDRoomStatusNotifyBodyGameMembers> {
  int? index;
  String? role;
  String? status;
  String? subStatus;
  String? userId;
}

class YBDRoomStatusNotifyBodyGameSettlementList with JsonConvert<YBDRoomStatusNotifyBodyGameSettlementList> {
  String? gameCurrency;
  int? index;
  String? result;
  String? userId;
  int? winNumber;
}

class YBDRoomStatusNotifyResources with JsonConvert<YBDRoomStatusNotifyResources> {
  dynamic user;
  dynamic userAttribute;

  String? userAvatarUrl(String? userid) {
    try {
      final userMap = Map<String, dynamic>.from(user);
      if (userMap.containsKey('$userid')) {
        Map<String, dynamic> userInfoMap = userMap['$userid'];
        return userInfoMap['property']['avatar'];
      }

      return '';
    } catch (e) {
      logger.e('parse YBDRoomStatusNotifyResources user avatar error: $e');
      return '';
    }
  }

  String? userNickName(String? userid) {
    try {
      final userMap = Map<String, dynamic>.from(user);
      if (userMap.containsKey('$userid')) {
        Map<String, dynamic> userInfoMap = userMap['$userid'];
        return userInfoMap['property']['nickName'];
      }

      return '';
    } catch (e) {
      logger.e('parse YBDRoomStatusNotifyResources user nickName error: $e');
      return '';
    }
  }
}

class YBDRoomStatusGameMode with JsonConvert<YBDRoomStatusGameMode> {
  String? mode;
  List<YBDRoomStatusGameModeCurrency?>? gameCurrency;

  /// 获取门票档位
  List<int> gameTicketLevel(String currency) {
    try {
      return gameCurrency!.firstWhere((element) => element!.currency == currency)?.ticketLevel ?? [];
    } catch (e) {
      YBDGameLog.v("model setting error: $e");
      return [];
    }
  }
  void gameTicketLevelRMBpkoyelive(String currency) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDRoomStatusGameModeCurrency with JsonConvert<YBDRoomStatusGameModeCurrency> {
  List<int>? ticketLevel;
  String? currency;
  List<int>? robotSupportLevel; // 支持机器人的筹码等位
}
