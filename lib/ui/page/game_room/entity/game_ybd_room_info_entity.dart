import 'dart:async';


/*
 * @Author: William-Zhou
 * @Date: 2022-03-26 17:35:52
 * @LastEditTime: 2022-03-26 17:41:13
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

class YBDGameRoomInfoEntity extends YBDGameBase with JsonConvert<YBDGameRoomInfoEntity> {
  YBDGameRoomInfoBody? body;
  String? cmd;
  YBDGameRoomInfoRoute? route;
  String? ver;
}

class YBDGameRoomInfoBody with JsonConvert<YBDGameRoomInfoBody> {
  int? userCount;
  YBDGameRoomInfoBodyProperty? property;
  String? roomId;
}

class YBDGameRoomInfoRoute with JsonConvert<YBDGameRoomInfoRoute> {
  String? from;
  String? mode;
  String? room;
}

class YBDGameRoomInfoBodyProperty with JsonConvert<YBDGameRoomInfoBodyProperty> {
  String? backgroundImg;
  int? roomLevel;
  String? name;
  String? id;
  String? title;
  String? poster;
  YBDGameRoomInfoBodyPropertyTheme? theme;
}
class YBDGameRoomInfoBodyPropertyTheme with JsonConvert<YBDGameRoomInfoBodyPropertyTheme> {
  String? resource;
  String? type;
  String? name;
  dynamic attribute;
}
