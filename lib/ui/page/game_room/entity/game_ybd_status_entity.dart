import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/base/json_field.dart';
import 'package:oyelive_main/ui/page/game_room/entity/enter_ybd_room_body_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_entry_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:collection/collection.dart';

class YBDGameStatusEntity with JsonConvert<YBDGameStatusEntity> {
  String? cmd;
  String? ver;
  String? code;
  String? codeMsg;
  YBDEnterRoomBodyEntityRoute? route;
  YBDGameStatusBody? body;
  YBDGameStatusResources? resources;
}

class YBDGameStatusBody with JsonConvert<YBDGameStatusBody> {
  String? category;
  YBDGameStatusBodyGroup? group;
  int? groupId;
  List<YBDGameStatusBodyMembers?>? members;
  String? roomId;
  String? status;
  String? subCategory;
  String? subStatus;

  /// 当前用户是否队长
  bool currentUserIsCaptain() {
    var userId = YBDRtcHelper.getInstance().info?.id;
    var firstEle = this.members?.firstWhereOrNull(
          (element) => (element!.role == "captain" && element.userId == "$userId"),
          // orElse: () => null,
        );

    return firstEle != null;
  }
  void currentUserIsCaptaingNQTyoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDGameStatusBodyGroup with JsonConvert<YBDGameStatusBodyGroup> {
  int? amount;
  String? currency;
  int? groupId;
  int? minimum;
  int? maximum;

  /// "classic",
  String? model;
  String? status;

  String currencyAsset() { return '';}
  void currencyAssetxnPzfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDGameStatusBodyMembers with JsonConvert<YBDGameStatusBodyMembers> {
  int? groupId;
  int? id;
  int? index;

  /// 角色：captain，player
  String? role;
  String? status;
  String? subStatus;
  String? userId;
}

class YBDGameStatusResources with JsonConvert<YBDGameStatusResources> {
  YBDGameEntryNotifyResourcesUser? user;
}
