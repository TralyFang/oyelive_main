import 'dart:async';


import 'package:get/get.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

class YBDTpRoomEntity extends YBDGameBase with JsonConvert<YBDTpRoomEntity> {
  String? command;
  YBDTpRoomData? data;
  int? code; // 0
  String? message; //":"success",
}

class YBDTpRoomData with JsonConvert<YBDTpRoomData> {
  YBDTpRoomDataBattle? battle;
  List<YBDTpRoomDataUser?>? users;
  List<YBDTpRoomDataAnimation?>? animation;
  int? code;
  String? message;
  String? roomId;
  List<YBDTpRoomStandUpUser?>? standUpUser;
  int? leaderId; // 房主ID
}
class YBDTpRoomStandUpUser with JsonConvert<YBDTpRoomStandUpUser> {
  int? userId;
  int? beforePosition; // 站起前的坐标
}
// {"userId":"","beforePosition":""}

class YBDTpRoomDataAnimation with JsonConvert<YBDTpRoomDataAnimation> {
  int? beans;
  int? operator; // 操作人
  int? timeout; // 100 ms
  int? type; // 1-投注成功动画，2-弃牌动画, 3-倒计时，4-定庄发牌
  int? sync; // 是否同步 默认0，不同步 1同步
}

/// 对局信息
class YBDTpRoomDataBattle with JsonConvert<YBDTpRoomDataBattle> {
  int? id; //对局id
  int? status; //1-准备中，2-游戏中，4-结算结束
  int? subStatus; //0-等待指令中，1-比牌中, 2-发牌中
  int? startTime; //开始时间
  int? endTime; //结束时间
  int? currentOperator; //当前操作用户
  String? roomId;
  int? term; //第几轮
  int? maxTerm; //第几轮
  int? boot; //底注
  int? chaalValue; //总押注金额
  int? minChaalValue; //最少投注额
  String? currency; //币种
  int? timeout; //用户不操作的超时时间 ms
  int? version; // 版本号
  int? optTimesRemaining; //操作剩余时间 ms
  int? optExpireAt; //操作过期的时间戳ms
  List<YBDTpRoomDataSettle?>? settle; //":[ //结算信息，只有最后开牌才有值
  List<int>? winners; //[1001],//赢家id,在开牌，比牌场景，该字段有值
  int? finishBeforeAction; //对局结束的触发动作1-投注2-弃牌3-比牌4-开牌
  List<int>? players; // 在座位上的玩家集合
}

class YBDTpRoomDataSettle with JsonConvert<YBDTpRoomDataSettle> {
  int? userId; //":1001,//用户id
  int? revenue; //":1140,//赢了多少钱
  int? index; //":1//排名，目前都是1
}

/// 玩家信息
class YBDTpRoomDataUser with JsonConvert<YBDTpRoomDataUser> {
  int? battleId; //对局id
  int? id; //用户id
  int? term; //第几轮
  int? roomId;
  int? position; //座位位置
  String? avatar; //头像
  int? role; //角色 0-观看者，1-玩家 2-庄家
  int? gender; //性别1:女, 2:男
  String? nickname;
  int? chaalValue; // 投注额
  List<YBDTpRoomDataCard?>? cards;
  int? cardState; //0-blind 1-seen,2-packed，3-比牌中，4-开牌
  YBDTpRoomDataUserOpt? optPrivilege;
  int? balance; // 余额更新
  int? hasBet; // 用户是否已经投注0-否 1-是
  bool? isStandUp = false; // 站起的状态，客户端追加标识
  int? hasBetAfterSeen; // 用户看牌之后是否已经投注0-否 1-是
  int? userTag; // 1房主 2机器人
  String? iframe; // 头像框

  // 开牌状态
  String cardStateTips() {
    if (role == 0) return '';
    if (cardState == 0) {
      return 'Blind';
    } else if (cardState == 1) {
      if (hasBetAfterSeen == 1) return 'Chaal';
      return 'Seen';
    } else if (cardState == 2) {
      return 'Packed';
    } else if (cardState == 3) {
      return 'S.Show';
    } else if (cardState == 4) {
      return '';
    }
    return '';
  }
  void cardStateTipsnxDCPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 下注状态提示
  String bidStateTips() {
    if (cardState == 0) {
      return 'Blind';
    } else if (cardState == 1 || cardState == 3) {
      return 'Chaal';
    } else if (cardState == 2) {
      return 'Packed';
    }
    return '';
  }
  void bidStateTipsuW77Coyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 是庄家
  bool isBanker() {
    return role == 2;
  }

  // 是男性
  bool isMale() {
    return gender != 1;
  }
  void isMale7ZGSBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String? avatarUrl() {
    if (avatar != null && id != null) {
      String url = YBDImageUtil.avatar(
        Get.context,
        avatar,
        id,
        scene: 'C',
      );
      logger.v('avatarUrl:$url, isMale:${isMale()}');
      return url;
    }
    return avatar;
  }
}

/// 牌型
class YBDTpRoomDataCard with JsonConvert<YBDTpRoomDataCard> {
  int? type; //1-方块，2-梅花，3-红桃，4-黑桃
  int? number; // 1,2,3,4,5,6,7,8,9,10,11,12,13
  YBDTpRoomDataCard({this.number, this.type});

  @override
  bool operator ==(Object other) {
    return other is YBDTpRoomDataCard &&
        type == other.type &&
        number == other.number;
  }
}

/// 玩家游戏状态
class YBDTpRoomDataUserOpt with JsonConvert<YBDTpRoomDataUserOpt> {
  bool? canChaal; //是否可以跟注
  bool? canDoubleBet; //是否可以加倍押注
  bool? canComparedCard; //是否可以和上家比牌
  bool? canShowCard; //是否可以开牌，
  bool? canPackCard; //是否可以弃牌
  bool? canSeeCard; //是否可以看牌
}
