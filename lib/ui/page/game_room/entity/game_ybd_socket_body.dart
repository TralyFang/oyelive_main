import 'dart:async';



import 'package:json_annotation/json_annotation.dart';

// @JsonSerializable()
abstract class GameSocketBody{
    List<String>? userIds;
    String? userId;
    ///socket 需要的body map
    Map getMap(){
        return {'userIds': this.userIds, 'userId':this.userId};
    }

}