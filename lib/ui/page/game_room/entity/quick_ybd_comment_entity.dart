import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDQuickCommentEntity with JsonConvert<YBDQuickCommentEntity> {
  String? code;
  List<YBDQuickCommentData?>? data;
  bool? success;
}

class YBDQuickCommentData with JsonConvert<YBDQuickCommentData> {
  int? type;
  String? data;
}
