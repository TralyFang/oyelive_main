import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDGameConfigDisplayEntity with JsonConvert<YBDGameConfigDisplayEntity> {
  String? code;
  YBDGameConfigDisplayData? data;
  String? success;
}

class YBDGameConfigDisplayData with JsonConvert<YBDGameConfigDisplayData> {
  String? logo;
  YBDGameConfigDisplayDataRule? rule;
  YBDGameConfigDisplayDataSetUp? setUp;
}

class YBDGameConfigDisplayDataRule with JsonConvert<YBDGameConfigDisplayDataRule> {
  String? body;
  String? title;
}

class YBDGameConfigDisplayDataSetUp with JsonConvert<YBDGameConfigDisplayDataSetUp> {
  String? addTicketImg; // 增加
  String? bodyBgc; // 中间部分颜色
  String? buttonBgC;
  String? buttonInnerC;
  String? selectedImg; // 选中图片
  String? titleBgC; // 标题栏颜色
  String? selectedC; // 选中的颜色
  String? reduceTicketImg; // 减少
}
