import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/base/json_field.dart';

class YBDEnterRoomBodyEntityEntity with JsonConvert<YBDEnterRoomBodyEntityEntity> {
	YBDEnterRoomBodyEntityBody? body;
	String? cmd;
	String? code;
	String? codeMsg;
	YBDEnterRoomBodyEntityHeader? header;
	YBDEnterRoomBodyEntityResources? resources;
	YBDEnterRoomBodyEntityRoute? route;
	bool? success;
	String? ver;
}

class YBDEnterRoomBodyEntityBody with JsonConvert<YBDEnterRoomBodyEntityBody> {
	YBDEnterRoomBodyEntityBodyProvider? provider;
}

class YBDEnterRoomBodyEntityBodyProvider with JsonConvert<YBDEnterRoomBodyEntityBodyProvider> {
	String? type;
	String? token;
}

class YBDEnterRoomBodyEntityHeader with JsonConvert<YBDEnterRoomBodyEntityHeader> {
	@JSONField(name: "connection-id")
	String? connectionId;
}

class YBDEnterRoomBodyEntityResources with JsonConvert<YBDEnterRoomBodyEntityResources> {
	var user;
	var userAttribute;
}

class YBDEnterRoomBodyEntityRoute with JsonConvert<YBDEnterRoomBodyEntityRoute> {
	String? from;
	String? mode;
	String? room;
	List<String>? to;
}
