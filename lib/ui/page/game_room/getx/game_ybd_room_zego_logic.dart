import 'dart:async';


import 'dart:convert';

import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_game_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/method_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';

/// zego_sud游戏逻辑处理
extension GameRoomZegoLogic on YBDGameRoomGetLogic {
  /// 初始化即构小游戏需要准备的数据
  initZegoHandle() {
    // 获取即构游戏code
    getZegoCode(needUpdate: true);

    // 即构小游戏加载完成
    YBDGameUtils.getInstance()!.notifyAppGameStarted((isStarted) {
      print('22.8.3--即构小游戏加载完成');
      // YBDGameRoomTrack().gameRoomTrack(event: YBDTATrackEvent.game_load_finish);
      state!.zegoMGLoading = false;
      update(["state.gamePlaying"]);

      /// 加载完成游戏，需要同步下状态到即构
      YBDGameLog.i("notifyAppGameStarted");
      zegoPlayerJoinStatusSyncPre();
      // 同步声音状态
      YBDGameUtils.getInstance()!.notifyAppCommonSelfSound(state!.isOpenZegoMGSound);
    });
  }

  /// 是否所有即构小游戏的玩家都准备了
  bool isZegoMGAllReady() {
    if (!isZegoMiniGame()) {
      return true;
    }
    // 先判断是否所有的人都准备了
    var joinUsers = YBDGameUtils.getInstance()!.zegoJoinUsers;
    var readyUsers = YBDGameUtils.getInstance()!.zegoReadyUsers;
    if (joinUsers.length != readyUsers.length) {
      var gameMemebers = state!.roomStatusNotify.body?.game?.members;
      var seatUsers = gameMemebers?.map((e) => e!.userId).toList();
      logger.e("_isZegoMGAllReady Error joinUsers: $joinUsers, readyUsers: $readyUsers, users: $seatUsers");
      if (seatUsers == null) {
        YBDToastUtil.toast("zego mini game seatUsers is null");
        return false;
      }
      var seatCount = 0;
      seatUsers.forEach((element) {
        if (joinUsers.contains(element)) seatCount += 1;
      });
      if (seatCount != seatUsers.length || seatUsers.length != joinUsers.length) {
        YBDToastUtil.toast("zego mini game ready abnormal, please try again later.");
        zegoPlayerJoinStatusSyncPre();
        return false;
      }
    }
    return true;
  }
  void isZegoMGAllReadyStrudoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是即构小游戏
  bool isZegoMiniGame() {
    if (GameKindTypeValue.typeWithValue(state!.gameSubCategory) == GameKindType.ludo) {
      return false;
    }
    return true;
  }
  void isZegoMiniGameFJe2voyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 即构小游戏加入异常数据准备
  zegoPlayerJoinStatusSyncPre() {
    var gameMemebers = state!.roomStatusNotify.body?.game?.members;
    if (gameMemebers == null || gameMemebers.length == 0 || state!.gamePlaying) {
      logger.v("_zegoPlayerJoinStatusSyncPre return gameMemebers: $gameMemebers,"
          " gamePlaying: ${state!.gamePlaying}");
      return;
    }
    try {
      // 座位上的用户
      List<String?> seatUsers = gameMemebers.map((e) => e!.userId).toList();
      // 当前用户是否需要准备
      String? currentUserReady = gameMemebers
          .firstWhereOrNull((e) => e!.userId == "${state!.currentUserId}" && e.subStatus == GameConst.MemberReady,
              /*orElse: () => null*/)
          ?.userId;
      // 队长id
      String? captainUser =
          gameMemebers.firstWhereOrNull((element) => element!.role == GameConst.CaptainRole, /*orElse: () => null*/)?.userId;
      logger.v("_zegoPlayerJoinStatusSyncPre "
          "seatUsers: $seatUsers, "
          "currentUserReady: $currentUserReady, "
          "captainUser: $captainUser");

      _zegoPlayerJoinStatusSync(
          YBDGameUtils.getInstance()!.zegoJoinUsers, seatUsers, captainUser, currentUserReady != null);
    } catch (e) {
      logger.v("_zegoPlayerJoinStatusSyncPre error: $e");
    }
  }

  /// 处理即构小游戏加入异常用户，以服务为准
  _zegoPlayerJoinStatusSync(List<String> zegoUsers, List<String?> users, String? captainId, bool needReady) {
    if (zegoUsers == null || users == null || !isZegoMiniGame()) {
      return;
    }
    logger.v("_zegoPlayerJoinStatusSync zegoUsers: $zegoUsers, users: $users");
    // 先设置队长角色，再处理异常用户
    if (captainId == "${state!.currentUserId}") {
      YBDGameUtils.getInstance()!.notifyAppCommonSelfCaptain(captainId);
    }

    /// 服务器未上坐用户，主动上坐
    users.forEach((element) {
      if (!zegoUsers.contains(element) && element == "${state!.currentUserId}") {
        logger.v("_zegoPlayerJoinStatusSync selfIn userId: $element, needReady: $needReady");
        YBDGameUtils.getInstance()!.notifyAppCommonSelfIn(true, 0);
      }
    });

    if (needReady) {
      // 当前用户需要准备了
      Future.delayed(Duration(milliseconds: 100), () {
        YBDGameUtils.getInstance()!.notifyAppCommonSelfReady(true);
      });
    }

    /// 即构异常用户，踢出座位(队长才有的权利)
    if (captainId == "${state!.currentUserId}") {
      Future.delayed(Duration(milliseconds: 100), () {
        zegoUsers.forEach((element) {
          if (!users.contains(element)) {
            logger.v("_zegoPlayerJoinStatusSync Kick userId: $element");
            YBDGameUtils.getInstance()!.notifyAppCommonSelfKick(element);
          }
        });
      });
    }
  }

  /// 即构小游戏事件处理
  void gameZegoEventHandler(
    GameJoinStatus? event, {
    int index = GameConst.DefaultSeatIndex,
    int? targetId = 0,
    int? battleId,
  }) {
    if (!isZegoMiniGame()) {
      return;
    }

    /// 需要写一个工具维护服务器状态，跟即构小游戏状态同步的问题
    YBDGameLog.v("_gameZegoEventHandler event: $event");
    switch (event) {
      case GameJoinStatus.quit:
        // 离开座位
        YBDGameUtils.getInstance()!.notifyAppCommonSelfIn(false, 0);
        break;
      case GameJoinStatus.join:
        // 上座, 如果是第一个加入的为队长，主动设置为准备状态（由插件里面自己去设置）
        YBDGameUtils.getInstance()!.notifyAppCommonSelfIn(true, index);
        break;
      case GameJoinStatus.ready:
        // 准备游戏
        YBDGameUtils.getInstance()!.notifyAppCommonSelfReady(true);
        break;
      case GameJoinStatus.cancel:
        // 取消准备游戏
        YBDGameUtils.getInstance()!.notifyAppCommonSelfReady(false);
        break;
      case GameJoinStatus.kickOut:
        // 踢人
        YBDGameUtils.getInstance()!.notifyAppCommonSelfKick('$targetId');
        break;
      case GameJoinStatus.waiting:
        // 等待开始游戏
        YBDToastUtil.toast("waiting");
        break;
      case GameJoinStatus.start:
        // 开始游戏 透传
        var extras = {
          'battleId': battleId,
        };
        final String jsonString = jsonEncode(extras);
        YBDGameLog.v("notifyAppCommonSelfPlaying extras: $jsonString");
        // 没有拿到batleId需要等待报文回调
        if (battleId == null) {
          return;
        }
        YBDGameUtils.getInstance()!.notifyAppGameStartException((code) {
          print('22.8.3--游戏开始异常');
          YBDGameRoomTrack().gameRoomTrack(event: YBDTATrackEvent.game_load_fail);
          getGameApi().gameReport(battleId: "$battleId", code: code);
        });
        YBDGameUtils.getInstance()!.notifyAppCommonSelfPlaying(true, jsonString);
        break;
      default:
        logger.v('GameJoinStatus event: $event');
        break;
    }
  }

  /// 获取机构小游戏Code
  Future<String> getZegoCode({bool needUpdate: false}) async {
    var resp = await ApiHelper.getZegoCode(Get.context);
    if (resp?.code == Const.HTTP_SUCCESS_NEW && resp?.data?.code != null) {
      state!.zegoCodeData = resp!.data;
      if (needUpdate) update();
    }
    return resp?.data?.code ?? "";
  }
  void getZegoCodegDEecoyelive({bool needUpdate: false})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
