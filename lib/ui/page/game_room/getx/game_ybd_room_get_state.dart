import 'dart:async';


import 'package:get/get.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_config_display_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gift_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gifting_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_status_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/room_ybd_status_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/zego_ybd_code_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';

/// 游戏房间相关逻辑状态
class YBDGameRoomGetState {
  /// 房间ID
  int? roomId;

  /// 当前用户是主播
  bool? isTalent;

  /// 当前登录用户id
  int? currentUserId;

  /// 游戏状态报文
  // YBDGameStatusEntity gameStatusEntity;

  /// 主播信息
  YBDUserInfo? talentInfo;

  /// 是否已经关注主播
  var isFollowed = false.obs;

  /// 房间信息
  YBDGameRoomInfoEntity? roomInfo;
  /// 外观配置信息
  YBDGameConfigDisplayData? configDisplay;

  /// 游戏状态
  var roomStatusNotify = YBDRoomStatusNotifyEntity();

  /// 游戏是否已经初始化了，也就是拿到数据了
  bool gameInit = false;
  /// 游戏开始了
  bool gamePlaying = false;
  /// 当前用户是否是队长
  var isCaptain = false;
  /// 机器人陪玩
  var isRobot = false;
  /// 对应筹码是否支持开启机器人
  var isOpenRobot = false;
  /// 游戏类型
  var gameSubCategory = "";
  /// ludo游戏的退出房间需要先隐藏webview，避免闪烁
  var exitRoom = false;

  /// ludo游戏地址
  String ludoUrl = "";
  bool ludoUrlDidChange = false;

  /// ludo游戏对局Id
  int? battleId;
  /// ludo游戏对局Id+ 配置信息的JsonString
  String? gameJSConfig;
  YBDGameWebLudoConfig? webLudoConfig;

  /// ludo游戏玩家位置信息
  List<YBDGamePlayerPosition?>? playerPositions;
  /// ludo游戏玩家位置信息
  var playerPositionChange = false.obs;
  var playerPositionChangeStr = "".obs;

  /// 礼物列表
  List<YBDGameGiftData?>? giftList = <YBDGameGiftData>[];

  /// zego小游戏code
  YBDZegoCodeData? zegoCodeData = YBDZegoCodeData();
  /// 即构小游戏正在加载中
  bool zegoMGLoading = true;
  /// 是否是即构小游戏
  bool isZegoMG = false;
  /// 是否开启即构小游戏声音
  var isOpenZegoMGSound = true;

  YBDGameRoomGetState() {
    YBDGameLog.v("$runtimeType init");

    /// 初始化 这里不做数据初始化，只做默认值初始化
    /// 数据初始化在 YBDGameRoomGetLogic 类中初始化
    isTalent = false;
  }
}

/// 结算榜单Item数据
class YBDGameSettlement {
  bool win;
  String? result;
  String? userAvatar;
  String? nickName;
  String? currency;
  int? winNumber;
  String index; // 玩家名次

  YBDGameSettlement({
    this.win = false,
    this.userAvatar = '',
    this.nickName = '',
    this.currency = '',
    this.winNumber = 0,
    this.index = '',
  });

  @override
  String toString() {
    return 'YBDGameSettlement{win: $win, userAvatar: $userAvatar, nickName: $nickName, currency: $currency, winNumber: $winNumber, index: $index}';
  }
  void toStringUrzWkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 游戏配置
class YBDGameConfig {
  int? amount;
  GameCurrencyType currency;
  GameModelType mode;
  String modeDesc;
  List<int>? ticketLevel;

  YBDGameConfig({
    this.amount = 1000,
    this.currency = GameCurrencyType.golds,
    this.mode = GameModelType.classic,
    this.modeDesc = "",
    this.ticketLevel = const [1000, 2000, 5000],
  });

  static YBDGameConfig fromRoomStatusNotify(YBDRoomStatusNotifyEntity entity) {
    var config = YBDGameConfig();

    if (entity.body?.game?.model == GameModelType.classic.value) {
      config.mode = GameModelType.classic;
    } else if (entity.body?.game?.model == GameModelType.fast.value) {
      config.mode = GameModelType.fast;
    }
    config.modeDesc = entity.body?.game?.needWinPawnDesc ?? "";
    if (entity.body?.game?.currency == GameCurrencyType.golds.value) {
      config.currency = GameCurrencyType.golds;
    } else if (entity.body?.game?.currency == GameCurrencyType.beans.value) {
      config.currency = GameCurrencyType.beans;
    }

    if (entity.body?.game?.amount != null) {
      config.amount = entity.body!.game!.amount;
    }

    if (entity.body?.game?.ticketLevel != null) {
      config.ticketLevel = entity.body!.game!.ticketLevel;
    }

    return config;
  }

  @override
  String toString() {
    return 'YBDGameConfig{amount: $amount, currency: $currency, mode: $mode, currency: $currency}';
  }
  void toString9xHJHoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 座位头像数据
/// 队长
/// 准备状态
/// 用户头像
class YBDGameSeat {
  bool isCaptain;
  bool ready;
  bool onSeat;
  String? url;
  String? userId;
  int index;

  YBDGameSeat({
    this.isCaptain = false,
    this.ready = false,
    this.url, // YBDGameLudoUserAvatar判断用户头像为null时显示空座位，默认显示空座位
    this.userId = '-1',
    this.onSeat = false,
    this.index = 0,
  });

  @override
  String toString() {
    return 'GameUser{isCaptain: $isCaptain, ready: $ready, url: $url, index: $index}';
  }
  void toStringCPVaSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDGameWebLudoConfig with JsonConvert<YBDGameWebLudoConfig> {
  bool? gamePlaying;
  int? battleId;
  String? battleMode;
  String? chargeCurrency;

  YBDGameWebLudoConfig({
    this.gamePlaying,
    this.battleId,
    this.battleMode,
    this.chargeCurrency,
  });

  /// 是否发生变动了
  bool isChangeToWebLudoConfig(YBDGameWebLudoConfig? oldConfig) {
    if (oldConfig == null) {
      return true;
    }
    if (gamePlaying != oldConfig.gamePlaying ||
    battleId != oldConfig.battleId ||
    battleMode != oldConfig.battleMode ||
    this.chargeCurrency != oldConfig.chargeCurrency
    ) {
      return true;
    }
    return false;
  }
  void isChangeToWebLudoConfigfM3ofoyelive(YBDGameWebLudoConfig? oldConfig) {

     int needCount = 0;
     print('input result:$needCount');
  }
  


}