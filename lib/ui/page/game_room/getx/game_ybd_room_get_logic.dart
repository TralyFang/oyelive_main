import 'dart:async';


import 'dart:async';
import 'dart:convert';

import 'package:get/get.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_game_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_config_display_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gifting_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_exit_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/room_ybd_status_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_zego_logic.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

/// 游戏房间相关逻辑
class YBDGameRoomGetLogic extends GetxController {

  static bool dropZegoMiniGame = true; // v2.7.2 下架游戏相关代码, 已经移除插件了

  YBDGameRoomGetState? state = YBDGameRoomGetState();

  /// 监听消息回调
  StreamSubscription? _gameRoomSubscription;

  /// 座位列表
  var seatList = <YBDGameSeat>[];

  void updateSeatList(List<YBDGameSeat> value) {
    seatList = value;
  }
  void updateSeatListCNpMgoyelive(List<YBDGameSeat> value) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 游戏配置
  var gameConfig = YBDGameConfig();

  /// 游戏结算
  var settlementList = <YBDGameSettlement>[];

  /// 更新是否加入陪玩
  void updateIsRobot() {
    state!.isRobot = !state!.isRobot;
    update();
  }
  void updateIsRobotQFkoKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  YBDGameRoomGetLogic() {
    // 初始化
    // 当前房间ID
    state!.roomId = int.tryParse(YBDRtcHelper.getInstance().roomId ?? "") ?? 0;
    YBDGameLog.v("$runtimeType init roomId: ${state!.roomId}");

    // 当前用户ID
    var currentUserId = YBDRtcHelper.getInstance().info?.id ?? -1;
    state!.isTalent = (currentUserId == state!.roomId);
    state!.currentUserId = currentUserId;
    state!.gameSubCategory = YBDRtcHelper.getInstance().selectedGame?.subCategory ?? "";

    // 获取主播信息
    _requestTalentInfo();

    // 获取配置信息
    _requestGameConfigDisplay();

    // 监听游戏房socket消息
    _listenGameRoomSocket();

    // 设置空座位
    updateSeatList(seatListFromRoomStatusNotify(state!.roomStatusNotify));

    // 查询用户是否已关注
    _requestConcern();

    // 请求游戏房礼物列表
    requestGiftList();

    // 即构小游戏初始化
    if (!dropZegoMiniGame)
      initZegoHandle();
  }

  /// 主播重试次数
  int _talentRetryCount = 0;

  /// 主播信息
  Future<void> _requestTalentInfo() async {
    YBDUserInfo? result = await ApiHelper.queryUserInfo(Get.context, state!.roomId);
    if (result == null && _talentRetryCount < 3) {
      _talentRetryCount += 1;
      _requestTalentInfo();
      return;
    }
    _talentRetryCount = 0;
    state!.talentInfo = result;
    update();
  }
  void _requestTalentInfowImD1oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 重试次数
  int _gameConfigRetryCount = 0;

  /// 游戏是否开始
  bool gameStarted = false;

  /// 游戏房类型
  String matchType = GameConst.matchTypePerson; // 1是机器人 2是真人

  /// 获取游戏配置信息
  _requestGameConfigDisplay() async {
    YBDGameLog.i("_requestGameConfigDisplay request");
    YBDGameConfigDisplayData? result = await ApiHelper.gameRoomConfigDisplay(Get.context, state!.gameSubCategory);
    YBDGameLog.i("_requestGameConfigDisplay result: $result");
    if (result == null && _gameConfigRetryCount < 3) {
      _gameConfigRetryCount += 1;
      _requestTalentInfo();
      return;
    }
    _gameConfigRetryCount = 0;
    state!.configDisplay = result;
    update();
  }

  /// 监听游戏房socket消息
  void _listenGameRoomSocket() {
    _gameRoomSubscription = YBDGameRoomSocketApi.getInstance().messageOutStream.listen((msg) {
      switch (msg.runtimeType) {
        case YBDGameRoomExitEntity:
          state!.exitRoom = true;
          YBDGameLog.i("YBDGameRoomExitEntity did exitRoom: ${state!.roomId}");
          eventBus.fire(YBDGameRoomGetEvent()..eventName = YBDGameRoomGetEvent.updateWebView);
          break;
        case YBDGameRoomInfoEntity: // 房间信息
          {
            state!.roomInfo = msg as YBDGameRoomInfoEntity;
            logger.v("roomInfo json: ----init, ${state!.roomInfo}");
            update();
          }
          break;
        case YBDRoomStatusNotifyEntity: // 游戏状态通知
          {
            var roomStatusNotify = msg as YBDRoomStatusNotifyEntity;

            ///  不是当前房间的报文就不做处理了
            if (roomStatusNotify.body!.roomId != "${state!.roomId}") {
              return;
            }

            state!.roomStatusNotify = roomStatusNotify;
            var subCategory = roomStatusNotify.body?.subCategory ?? "ludo";
            // 游戏类型
            if (state!.gameSubCategory != subCategory) {
              state!.gameSubCategory = subCategory;
              // 游戏类型修改，需要更新配置信息
              _requestGameConfigDisplay();
            }

            var battleId = roomStatusNotify.body?.game?.battleId;
            var gamePlaying = roomStatusNotify.body?.gamePlaying() ?? false;
            if (gamePlaying != gameStarted) {
              if (gamePlaying) {
                Future.delayed(Duration(seconds: 3), () {
                  YBDGameRoomTrack().gameRoomTrack(event: YBDTATrackEvent.game_start);
                });

                // 埋点
                gameStarted = true;
              } else {
                // 埋点
                gameStarted = false;
              }
            }
            // 需要拼接roomId, battleId
            var ludoUrl = (roomStatusNotify.body?.game?.attribute?.url ?? "");
            if (Const.TEST_ENV && (state!.currentUserId == 2000001 || state!.currentUserId == 2000002)) {
              /// 金龙的本地地址
              ludoUrl = "http://game.locl.com:3000/index.html";
            }
            ludoUrl = ludoUrl.httpAppending("roomId=${state!.roomId}");
            ludoUrl = ludoUrl.httpAppending("battleId=$battleId");
            state!.ludoUrlDidChange = (ludoUrl != state!.ludoUrl);
            if (ludoUrl != state!.ludoUrl && ludoUrl.contains("http")) {
              YBDGameLog.i("YBDRoomStatusNotifyEntity ludoUrl is change!");
              state!.ludoUrl = ludoUrl;
              eventBus.fire(YBDGameRoomGetEvent()..eventName = YBDGameRoomGetEvent.updateWebView);
            }
            state!.battleId = battleId;
            state!.gamePlaying = gamePlaying;
            sendGameConfigToWebView();
            YBDGameLog.v("YBDRoomStatusNotifyEntity ludoUrl: ${state!.ludoUrl}, "
                "battleId: ${state!.battleId}, playing: ${state!.gamePlaying}, "
                "roomId: ${roomStatusNotify.body?.roomId}, subCategory: $subCategory");

            // 更新座位列表数据
            updateSeatList(seatListFromRoomStatusNotify(roomStatusNotify));

            // 即构小游戏加入异常数据准备
            zegoPlayerJoinStatusSyncPre();

            // 更新游戏配置的数据
            gameConfig = YBDGameConfig.fromRoomStatusNotify(roomStatusNotify);

            // 是否支持机器人
            state!.isOpenRobot = _suportOpenRobot();

            /// 更新结算榜单数据
            var settleList = settlementListFromRoomStatusNotify(roomStatusNotify);
            if (settleList.length > 0) {
              logger.i('22.10.10--matchType:${roomStatusNotify.body?.game?.matchType}');
              matchType = roomStatusNotify.body?.game?.matchType ?? GameConst.matchTypePerson;
              settlementList = settleList;
              eventBus.fire(YBDGameRoomGetEvent()..eventName = YBDGameRoomGetEvent.openSettleDialog);
            }

            /// 当前用户是否队长
            state!.isCaptain = currentUserSeat().isCaptain;

            state!.gameInit = true;
            state!.isZegoMG = isZegoMiniGame();

            /// 刷新数据
            update();
          }
          break;
        case YBDGameGiftingNotifyEntity: // 收到礼物动画
          {
            var entity = msg as YBDGameGiftingNotifyEntity;
            var animationUrl = entity.body!.animationUrl;
            if (animationUrl?.isEmpty ?? true) {
              return;
            }

            /// 提前加载图片
            YBDGameWidgetUtil.networkImage(url: entity.body!.imageUrl);
            YBDDownloadCacheManager.instance.downloadURL(animationUrl!, (path) {
              logger.v("$runtimeType download path: $path");
              var svgaMp3s = path!.split(",");
              entity.body!.animationLocalPath = svgaMp3s.first;
              if (svgaMp3s.length == 2) {
                entity.body!.animationMp3LocalPath = svgaMp3s.last;
              }
              eventBus.fire(YBDGameRoomGiftEvent()..entity = entity.body);
            });
          }
          break;
        default:
          break;
      }
    });
  }
  void _listenGameRoomSocketljUMeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求游戏房礼物列表
  Future<void> requestGiftList() async {
    final entity = await ApiHelper.gameRoomGiftList(Get.context);
    state!.giftList = entity?.data;

    /// 直接下载礼物列表,图片+动图
    entity?.data?.forEach((element) {
      YBDGameWidgetUtil.networkImage(url: element!.img);
      if (element.animationFile?.contains("http") ?? false) {
        YBDDownloadCacheManager.instance.downloadURL(element.animationFile!, (path) {});
      }
    });
  }
  void requestGiftList8uurkoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询用户是否已关注
  /// true: 已关注该好友，false：未关注该好友
  _requestConcern() async {
    if (state!.isTalent!) {
      // 是主播自己
      state!.isFollowed.value = true;
      return;
    }
    int? myId = state!.currentUserId;
    state!.isFollowed.value = await ApiHelper.queryConcern(Get.context, "$myId", "${state!.roomId}");
  }

  /// 点击关注主播， 回调统一在syncInvokeFollowUser处理了
  followTalent() {
    ApiHelper.followUser(Get.context, "${state!.roomId}");
  }

  /// 同步其他地方调用关注/取消的状态到房间
  static syncInvokeFollowUser(String? followUserId, bool isFollowed) {
    try {
      var logic = Get.find<YBDGameRoomGetLogic>();
      if (logic.state!.roomId.toString() == followUserId) {
        logger.v("game room logic talent follow status sync");
        logic.state!.isFollowed.value = isFollowed;
      }
    } catch (e) {
      logger.v("game room logic not found");
    }
  }

  @override
  void onClose() {
    YBDGameLog.v("$runtimeType onClose");
    _gameRoomSubscription?.cancel();
    state = null;
    super.onClose();
  }
  void onCloseMgX2yoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 传递游戏信息给web update:忽略判断，直接传递
  sendGameConfigToWebView({bool update = false}) {
    if (isZegoMiniGame()) return;
    var webJson = {
      'gamePlaying': state!.gamePlaying, // 是否开始游戏
      'battleId': state!.battleId,
      'battleMode': state!.roomStatusNotify.body?.game?.model,
      'chargeCurrency': state!.roomStatusNotify.body?.game?.currency
    };
    var config = YBDGameWebLudoConfig().fromJson(webJson);
    var isChange = config.isChangeToWebLudoConfig(state!.webLudoConfig);
    YBDGameLog.i("sendGameConfigToWebView ischange: $isChange, update:$update, json: $webJson");
    if (isChange || update) {
      state!.webLudoConfig = config;
      final String jsonString = jsonEncode(webJson);
      state!.gameJSConfig = jsonString;
      eventBus.fire(YBDGameRoomJSChannelEvent()
        ..gameJSConfig = jsonString
        ..eventName = YBDGameRoomJSChannelEvent.deliverBattleId);
    }
  }

  /// 修改机器人陪玩状态
  void robotPlay(bool value) {
    state!.isRobot = value;
  }
  void robotPlayxXxxHoyelive(bool value) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 该门票是否支持开启机器人
  bool _suportOpenRobot() {
    var gameEntity = state!.roomStatusNotify.body!.game!;
    try {
      var gameModel = gameEntity.optionalMode?.firstWhere((element) => (element!.mode == gameEntity.model));
      var gameCurrency = gameModel?.gameCurrency?.firstWhere((element) => (element!.currency == gameEntity.currency));
      return gameCurrency?.robotSupportLevel?.contains(gameEntity.amount) ?? false;
    } catch (e) {
      YBDGameLog.v("_suportOpenRobot error: $e");
    }
    return false;
  }

  /// 座位列表
  /// 从resource中解析出用户头像
  List<YBDGameSeat> seatListFromRoomStatusNotify(YBDRoomStatusNotifyEntity entity) {
    int maxNumber = entity.body?.game?.maximum ?? GameConst.DefaultMaximum;
    // 生成[maxNumber]个初始座位
    var users = <YBDGameSeat>[];

    // 填充[maxNumber]个座位
    for (int i = 1; i <= maxNumber; i++) {
      // 座位index从1开始
      var gameUser = YBDGameSeat(index: i);
      final indexMember = _memberWithIndex(i, entity);

      if (indexMember != null) {
        // 填充members中index相等的数据
        gameUser.isCaptain = indexMember.role == GameConst.CaptainRole;
        gameUser.ready = indexMember.subStatus == GameConst.MemberReady;
        gameUser.userId = indexMember.userId;
        gameUser.onSeat = true;
        gameUser.url = entity.resources!.userAvatarUrl(indexMember.userId);
      }

      users.add(gameUser);
    }

    // 解析接口中的玩家数据修改对应的座位
    return users;
  }
  void seatListFromRoomStatusNotifygPGrXoyelive(YBDRoomStatusNotifyEntity entity) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 从座位列表中取[index]对应的座位数据
  YBDRoomStatusNotifyBodyGameMembers? _memberWithIndex(int index, YBDRoomStatusNotifyEntity entity) {
    return entity.body?.game?.members?.firstWhereOrNull(
      (element) => element!.index == index,
      // orElse: () => null,
    );
  }
}

/// 结算榜单列表数据
List<YBDGameSettlement> settlementListFromRoomStatusNotify(YBDRoomStatusNotifyEntity entity) {
  var settlementList = <YBDGameSettlement>[];

  if (entity.body?.game?.settlementList?.isNotEmpty ?? false) {
    for (var settlementEntity in entity.body!.game!.settlementList!) {
      var gameSettlement = YBDGameSettlement();
      gameSettlement.win = settlementEntity!.result == 'win';
      gameSettlement.result = settlementEntity.result;
      gameSettlement.winNumber = settlementEntity.winNumber;
      gameSettlement.index = '${settlementEntity.index}';
      gameSettlement.currency = settlementEntity.gameCurrency;

      if (null != entity.resources) {
        gameSettlement.userAvatar = entity.resources!.userAvatarUrl(settlementEntity.userId);
        gameSettlement.nickName = entity.resources!.userNickName(settlementEntity.userId);
      }

      settlementList.add(gameSettlement);
    }
  }

  return settlementList;
}

/// 游戏逻辑处理
extension GameRoomGameLogic on YBDGameRoomGetLogic {
  YBDGameRoomSocketApi? _gameApi() => YBDGameRoomSocketApi.getInstance();
  YBDGameRoomSocketApi? getGameApi() => _gameApi();

  /// [index]等于-1服务端随机分配座位
  void gameEventHandler(GameJoinStatus? event, {int index = GameConst.DefaultSeatIndex, int? targetId = 0}) {
    switch (event) {
      case GameJoinStatus.quit:
        // 离开座位
        _gameApi().gameSeatOperate(false, onSuccess: () {
          gameZegoEventHandler(event);
        });
        break;
      case GameJoinStatus.join:
        // 上座
        _gameApi().gameSeatOperate(true, index: index, onSuccess: () {
          gameZegoEventHandler(event);
        });
        break;
      case GameJoinStatus.ready:
        // 准备游戏
        _gameApi().gameReadyOperater(true);
        break;
      case GameJoinStatus.cancel:
        // 取消准备游戏
        _gameApi().gameReadyOperater(false);
        break;
      case GameJoinStatus.kickOut:
        // 踢人
        _gameApi().gameKickOutPlayer('$targetId');
        break;
      case GameJoinStatus.waiting:
        // 等待开始游戏
        YBDToastUtil.toast("waiting");
        break;
      case GameJoinStatus.start:
        // 开始游戏
        if (!isZegoMGAllReady()) {
          return;
        }
        TA.timeEvent(YBDTATrackEvent.generic_loading_time);
        _gameApi().gameStart(
            addRobot: (state!.isRobot && state!.isOpenRobot),
            onSuccess: (battleId) {
              gameZegoEventHandler(event, battleId: battleId);
            });
        break;
    }

    /// zego开始游戏需要单独调用，因为需要透传battleId；加入游戏
    if (event == GameJoinStatus.start || event == GameJoinStatus.join || event == GameJoinStatus.quit) {
      // 需要根据后台的状态才能同步到小游戏
    } else {
      gameZegoEventHandler(event, index: index, targetId: targetId);
    }
  }
  void gameEventHandlerGrT8moyelive(GameJoinStatus? event, {int index = GameConst.DefaultSeatIndex, int? targetId = 0}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 队长踢出玩家
  void kickOutPlayer(String targetId) {
    gameEventHandler(GameJoinStatus.kickOut, targetId: int.tryParse(targetId));
  }

  /// 修改游戏配置
  void updateGameConfig(
    int? amount,
    GameCurrencyType? currency,
    GameModelType? mode,
  ) {
    _gameApi().gameUpdateConfig({
      'amount': amount,
      'currency': currency.value,
      'mode': mode.value,
    });
  }

  /// 队长座位
  YBDGameSeat captainSeat() {
    return seatList.firstWhere(
      (element) => element.isCaptain,
      orElse: () => YBDGameSeat(),
    );
  }

  /// 当前用户的座位
  YBDGameSeat currentUserSeat() {
    var userId = YBDCommonUtil.storeFromContext()!.state.bean!.id;
    var seat = seatList.firstWhere(
      (element) => element.userId == '$userId',
      orElse: () => YBDGameSeat(),
    );

    return seat;
  }

  /// 用户是否在座位上
  bool isUserOnSeat(String userId) {
    var seat = seatList.firstWhere(
      (element) => element.userId == '$userId',
      orElse: () => YBDGameSeat(),
    );
    return seat.onSeat;
  }
  void isUserOnSeatYvAPkoyelive(String userId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 根据用户id获取用户的座位信息
  YBDUserInfo userSeatWithId(String? userId) {
    var userInfo = YBDUserInfo();

    // 填充用户id和用户头像
    userInfo.id = YBDNumericUtil.stringToInt(userId);
    userInfo.nickname = state!.roomStatusNotify.resources!.userNickName(userId);
    userInfo.headimg = state!.roomStatusNotify.resources!.userAvatarUrl(userId);
    return userInfo;
  }

  /// 准备状态的玩家人数等于最大玩家人数
  bool isAllPlayerReady() {
    // 在座位的人全部准备了，且满足了最小开始游戏人数
    int maximum = state!.roomStatusNotify.body?.game?.minimum ?? GameConst.DefaultMinimum;
    var readyList = seatList.where((element) => element.ready).toList();
    var onSeatList = seatList.where((element) => element.onSeat).toList();
    if ((readyList.length == onSeatList.length) && state!.isRobot && state!.isOpenRobot) {
      return true; // 是加入陪玩
    }
    return (readyList.length == onSeatList.length) && onSeatList.length >= maximum;
  }

  /// 已经满座了，就不显示陪玩按钮了
  bool isSeatFull() {
    int maximum = state!.roomStatusNotify.body?.game?.maximum ?? GameConst.DefaultMaximum;
    var onSeatList = seatList.where((element) => element.onSeat).toList();
    return maximum == onSeatList.length;
  }
  void isSeatFullswQ3coyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
