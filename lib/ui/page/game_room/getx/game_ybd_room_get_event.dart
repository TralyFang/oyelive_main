import 'dart:async';



import 'package:flutter/material.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gifting_notify_entity.dart';

/// 游戏房间相关事件
class YBDGameRoomGetEvent {

  String? eventName;
  /// 结算弹窗
  static const String openSettleDialog = "openSettleDialog";
  /// 刷新webView
  static const String updateWebView = "updateWebView";
}

/// 游戏房间礼物事件
class YBDGameRoomGiftEvent {
  /// 发送礼物成功之后需要做个动画
  YBDGameGiftingNotifyBody? entity;
  /// 需要设置对应的目标key
  static GlobalKey? giftBeginKey;
  static GlobalKey? giftEndKey;

  /// 记录四个游戏玩家的坐标
  static late List<GlobalKey> endAnimationKeys;
}

/// 游戏房间通道信息
class YBDGameRoomJSChannelEvent {
  int? userId;
  int? roomId;

  /// ludo游戏对局Id
  String? gameJSConfig;

  String? eventName; // openGiftPop
  /// ludo游戏声音
  bool? isMute;
  /// 对应[eventName]的事件
  /// 打开礼物弹窗 userId roomId
  static const String openGiftDialog = "openGiftDialog";
  /// 主动透传battleId给web+ gameConfig
  static const String deliverBattleId = "deliverBattleId";
  /// 打开ludo游戏声音
  static const String muteGameVoice = 'muteGameVoice';
}
