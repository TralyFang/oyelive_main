import 'dart:async';


import 'package:flutter/material.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_top_sheet.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_chat_sheet.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

import '../../../../../common/analytics/analytics_ybd_util.dart';

/// 游戏房间底部
class YBDGameRoomFooter extends StatefulWidget {
  const YBDGameRoomFooter({Key? key}) : super(key: key);

  @override
  _YBDGameRoomFooterState createState() => _YBDGameRoomFooterState();
}

class _YBDGameRoomFooterState extends State<YBDGameRoomFooter> {
  final GlobalKey beginKey = GlobalKey();

  bool mute = false;

  @override
  Widget build(BuildContext context) {
    YBDGameRoomGiftEvent.giftBeginKey = beginKey;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 23.dp720 as double),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // 聊天图标
          Container(
            key: YBDGameRoomGiftEvent.giftBeginKey,
            child: YBDGameWidgetUtil.assetImage("game_chat_icon", width: 62.dp720, height: 61.dp720, onTap: () {
              // showModalBottomSheet(
              //     context: context, //
              //     isScrollControlled: true,
              //     barrierColor: Colors.transparent,
              //     backgroundColor: Colors.transparent, // BuildContext对象
              //     builder: (BuildContext context) {
              //       return YBDGameChatSheet(true);
              //     });
              YBDGameChatSheet.show(
                context,
                true,
              );
              YBDGameLog.v("tap game_chat_icon");

              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                YBDEventName.CLICK_EVENT,
                location: YBDLocationName.GAME_ROOM_PAGE,
                itemName: "click chat",
              ));
            }),
          ),
          // 更多图标
          YBDGameWidgetUtil.assetImage("game_more_icon", width: 62.dp720, height: 61.dp720, onTap: () {
            YBDGameLog.v("tap game_more_icon");
            YBDGameTopSheet.showBottom(context, this.mute, () {
              this.mute = !this.mute;
            });
          }),
        ],
      ),
    );
  }
  void buildA3efQoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
