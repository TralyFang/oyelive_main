import 'dart:async';


import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/method_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_bloc.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_top_sheet.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/widget/mini_ybd_profile_dialog.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';

/// 游戏房间头部
class YBDGameRoomHeader extends StatefulWidget {
  const YBDGameRoomHeader({Key? key}) : super(key: key);

  @override
  _YBDGameRoomHeaderState createState() => _YBDGameRoomHeaderState();
}

class _YBDGameRoomHeaderState extends State<YBDGameRoomHeader> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      var talentInfo = logic.state!.talentInfo;
      assert(() {
        print('talentInfo?.avatarURL(): ${talentInfo?.avatarURL()}');
        return true;
      }());
      return Container(
        padding: EdgeInsets.only(
          left: 23.dp720 as double,
          top: 28.dp720 as double,
          right: 18.dp720 as double,
        ),
        child: Row(
          children: [
            YBDGameWidgetUtil.avatar(
                url: talentInfo?.avatarURL(),
                addSize: false,
                width: 67.dp720,
                radius: 33.5.dp720,
                onTap: () {
                  /// 查看主播信息
                  YBDGameLog.v("tap talent show miniprofile");
                  YBDMiniProfileDialog.showGame(
                    context,
                    roomId: logic.state!.roomId,
                    userId: logic.state!.roomId,
                  );
                }),
            SizedBox(width: 19.dp720),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      YBDGameWidgetUtil.textSingleRegular(
                        talentInfo?.nickname,
                        color: Colors.white,
                        fontSize: 23.sp720,
                      ),
                      SizedBox(width: 10.dp720),
                      Obx(() {
                        // 交互还需要调整，暂时用原来的
                        // return AnimatedContainer(
                        //   duration: Duration(milliseconds: 500),
                        //   width: logic.state.isFollowed.value ? 0 : 38.dp720,
                        //   height: logic.state.isFollowed.value ? 0 : 38.dp720,
                        //   child: YBDGameWidgetUtil.assetImage(
                        //       "game_follow_icon",
                        //       // width: logic.state.isFollowed.value ? 0 : 38.dp720,
                        //       // height: logic.state.isFollowed.value ? 0 : 38.dp720,
                        //       onTap: () {
                        //         YBDGameLog.v("followTalent AnimatedContainer");
                        //         logic.followTalent();
                        //       }
                        //   ),
                        // );

                        if (!logic.state!.isFollowed.value)
                          return YBDGameWidgetUtil.assetImage(
                            "game_follow_icon",
                            width: 38.dp720,
                            height: 38.dp720,
                            onTap: () {
                              logic.followTalent();
                            },
                          );
                        return Container();
                      }),
                    ],
                  ),
                  SizedBox(height: 2.dp720),
                  YBDGameWidgetUtil.textSingleRegular("ID:${talentInfo?.id ?? ""}",
                      color: Colors.white.withOpacity(0.7), fontSize: 19.sp720),
                ],
              ),
            ),
            Row(
              children: [
                if (false) // TODO: 先隐藏
                  YBDGameWidgetUtil.assetImage("game_square_icon", width: 46.dp720, height: 46.dp720, onTap: () {
                    YBDGameLog.v("tap game_square_icon");
                  }),
                SizedBox(height: 5.dp720),
                YBDGameWidgetUtil.assetImage(
                  "game_points_icon",
                  width: 46.dp720,
                  height: 46.dp720,
                  minTapSize: 70.dp720,
                  onTap: () {
                    YBDGameLog.v("tap game_points_icon");
                    YBDGameTopSheet.showQuit(context, logic.state!.isTalent);
                  },
                ),
                SizedBox(width: 12.dp720),
              ],
            )
          ],
        ),
      );
    });
  }
  void buildC3HtUoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
