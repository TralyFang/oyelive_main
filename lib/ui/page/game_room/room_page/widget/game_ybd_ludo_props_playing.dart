import 'dart:async';


import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:synchronized/synchronized.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gifting_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_animate.dart';
import 'package:oyelive_main/ui/page/game_room/util/audio_ybd_player_manager.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oye_tool_package/widget/should_rebuild.dart';
import 'package:oyelive_main/ui/page/room/widget/svga_ybd_ctrl_player.dart';

class YBDGameLudoPropsPlaying extends StatefulWidget {
  const YBDGameLudoPropsPlaying({Key? key}) : super(key: key);

  @override
  _YBDGameLudoPropsPlayingState createState() => _YBDGameLudoPropsPlayingState();
}

class _YBDGameLudoPropsPlayingState extends BaseState<YBDGameLudoPropsPlaying> {
  List<YBDGameLudoPropsPlayItem> _playingItems = []; // 播放的动画
  static final _lock = Lock();
  int _itemsCount = 0;

  @override
  void initState() {
    super.initState();

    /// 游戏层交互动画
    ///监听触发小道具送礼动画播放
    addSub(eventBus.on<YBDGameRoomGiftEvent>().listen((event) {
      if (event.entity?.animationLocalPath?.isEmpty ?? true) {
        YBDGameLog.v("YBDGameRoomGiftEvent play animation local path not exists");
        return;
      }
      YBDGameRoomGetLogic? logic;
      try {
        logic = Get.find<YBDGameRoomGetLogic>();
      } catch (e) {
        YBDGameLog.v("YBDGameRoomGiftEvent YBDGameRoomGetLogic not found!");
      }

      // 接受动画的对象
      var userId = event.entity!.receiver;
      var index = logic?.state?.playerPositions
          ?.indexWhere((element) => element!.userId == userId);
      if (index == null ||
          index < 0 ||
          index >= YBDGameRoomGiftEvent.endAnimationKeys.length) {
        YBDGameLog.v(
            "YBDGameRoomGiftEvent play animation index not found index: $index");
        return;
      }
      YBDGameRoomGiftEvent.giftEndKey = YBDGameRoomGiftEvent.endAnimationKeys[index];
      // 送礼动画
      var size = Size(100.dp720 as double, 100.dp720 as double);
      Widget icon = YBDGameWidgetUtil.networkImage(
        url: event.entity?.imageUrl,
        width: size.width,
        height: size.height,
      );
      YBDGameLog.v("YBDGameRoomGiftEvent play animation");

      var animationItem = YBDGameLudoPropsPlayItem(
        beginKey: YBDGameRoomGiftEvent.giftBeginKey,
        endKey: YBDGameRoomGiftEvent.giftEndKey,
        animateEntity: event.entity,
        animationWidget: icon,
        widgetSize: size,
        completionCallback: (item) {
          removeItem(item);
        },
      );
      appendItem(animationItem);
    }));
  }
  void initStateDjgXyoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 需要的动画添加到数组中
  appendItem(YBDGameLudoPropsPlayItem item) {
    _lock.synchronized(() {
      _playingItems.add(item);
      YBDGameLog.v("YBDGameRoomGiftEvent play animation add item:${item.hashCode}, "
          "entity: ${item.animateEntity!.animationUrl}, "
          "_items:${_playingItems.length}");
      setState(() {});
    });
  }

  /// 已经完成的动画从数组中移除
  removeItem(YBDGameLudoPropsPlayItem item) {
    _lock.synchronized(() {
      _itemsCount += 1; // 需要所有动画都结束再移除，避免动画失效
      if (_playingItems.length == _itemsCount) {
        _playingItems.clear();
        _itemsCount = 0;
        setState(() {});
      }
      YBDGameLog.v(
          "YBDGameRoomGiftEvent play animation remove item:${item.hashCode}, "
          "entity: ${item.animateEntity!.animationUrl}, "
          "_items:${_playingItems.length}, "
          "count:$_itemsCount");
    });
  }

  @override
  Widget myBuild(BuildContext context) {
    return Stack(
      children: [
        ..._playingItems,
      ],
    );
  }
  void myBuildIlzrDoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _playingItems.clear();
    _itemsCount = 0;
    logger.v("YBDGameLudoPropsPlaying dispose: $_playingItems");
    super.dispose();
  }
  void disposeYSdH5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDGameLudoPropsPlayItem extends StatefulWidget {
  bool tranformBig = true; // 逐渐变大，否则变小
  GlobalKey? beginKey; // 开始动画坐标
  GlobalKey? endKey; // 结束动画坐标
  Widget? animationWidget; // 动画对象
  YBDGameGiftingNotifyBody? animateEntity; // 动画实体类
  Size? widgetSize = Size.zero; // 动画对象大小
  Function(YBDGameLudoPropsPlayItem)? completionCallback; // 所有动画播放完成的回调

  YBDGameLudoPropsPlayItem({
    Key? key,
    this.beginKey,
    this.endKey,
    this.animateEntity,
    this.animationWidget,
    this.widgetSize,
    this.completionCallback,
  }) : super(key: key);

  @override
  _YBDGameLudoPropsPlayItemState createState() => _YBDGameLudoPropsPlayItemState();
}

class _YBDGameLudoPropsPlayItemState extends BaseState<YBDGameLudoPropsPlayItem> {
  Offset? _endfOfset;

  bool _trackAnimationPlaying = true; // 轨迹动画播放中
  bool _endAnimationPlaying = false; // 结束动画播放中

  /// 轨迹动画
  Widget trackAnimation() {
    return ShouldRebuild(
      shouldRebuild: (dynamic oldWidget, dynamic newWidget) =>
          oldWidget.trackAnimationPlaying = !oldWidget.trackAnimationPlaying,
      child: YBDTrackAnimation(
        tranformBig: widget.tranformBig,
        beginKey: widget.beginKey,
        endKey: widget.endKey,
        animationWidget: widget.animationWidget,
        animateEntity: widget.animateEntity,
        widgetSize: widget.widgetSize,
        trackAnimationPlaying: _trackAnimationPlaying,
        completionFirstCallback: (endOffset) {
          _endfOfset = endOffset;
          _endAnimationPlaying = true;
          setState(() {});
        },
        completionCallback: () {
          _trackAnimationPlaying = false;
          setState(() {});
        },
      ),
    );
  }
  void trackAnimationbWuc9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 结束动画
  Widget endAnimation() {
    if (!_endAnimationPlaying) return Container();

    return YBDEndAnimation(
      endOffset: _endfOfset,
      animateEntity: widget.animateEntity,
      completionCallback: (child) {
        widget.completionCallback?.call(widget);
      },
    );
  }

  @override
  void dispose() {
    logger.v("YBDGameLudoPropsPlayItem dispose");
    YBDAudioPlayerManager.instance!.dispose();
    super.dispose();
  }
  void disposev6cPdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Stack(
      children: [
        trackAnimation(),
        endAnimation(),
      ],
    );
  }
}

class YBDTrackAnimation extends StatefulWidget {
  bool? tranformBig = true; // 逐渐变大，否则变小
  GlobalKey? beginKey; // 开始动画坐标
  GlobalKey? endKey; // 结束动画坐标
  Widget? animationWidget; // 动画对象
  YBDGameGiftingNotifyBody? animateEntity; // 动画实体类
  Size? widgetSize = Size.zero; // 动画对象大小
  Function(Offset endfOfset)? completionFirstCallback; // 第一个动画完成
  Function()? completionCallback; // 所有动画完成
  bool? trackAnimationPlaying = true;

  YBDTrackAnimation(
      {this.tranformBig,
      this.beginKey,
      this.endKey,
      this.animationWidget,
      this.animateEntity,
      this.widgetSize,
      this.trackAnimationPlaying,
      this.completionFirstCallback,
      this.completionCallback});

  @override
  _YBDTrackAnimationState createState() => _YBDTrackAnimationState();
}

class _YBDTrackAnimationState extends State<YBDTrackAnimation> {
  @override
  void initState() {
    super.initState();
  }
  void initState2eV5Hoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return trackAnimation();
  }

  /// 轨迹动画
  Widget trackAnimation() {
    if (!widget.trackAnimationPlaying!) return Container();

    logger.v("YBDGamePropsAnimateWidget, context: $context");

    // 这里需要转换下widget的坐标。
    RenderObject beginRenderBox =
        widget.beginKey!.currentContext!.findRenderObject()!;
    var beginV3 = beginRenderBox
        .getTransformTo(context.findRenderObject())
        .getTranslation();
    var beginSize = beginRenderBox.paintBounds.size;
    Offset beginfOfset = Offset(
        beginV3[0] + beginSize.width / 2, beginV3[1] + beginSize.height / 2);

    RenderObject endRenderBox = widget.endKey!.currentContext!.findRenderObject()!;
    var endV3 = endRenderBox
        .getTransformTo(context.findRenderObject())
        .getTranslation();
    var endSize = endRenderBox.paintBounds.size;
    Offset endfOfset =
        Offset(endV3[0] + endSize.width / 2, endV3[1] + endSize.height / 2);

    logger.v("YBDGamePropsAnimateWidget, context: $context, "
        "begin: $beginfOfset, beginSize: $beginSize, "
        "end: $endfOfset, endSize: $endSize");

    var isCallback = false; // 只能回调一次
    var count = widget.animateEntity!.number ?? 1;
    var animationIndex = 1;
    void Function(AnimationStatus) callback = (status) {
      if (status == AnimationStatus.completed ||
          status == AnimationStatus.dismissed) {
        if (animationIndex == count) {
          widget.trackAnimationPlaying = false;
          widget.completionCallback?.call();
          setState(() {});
          logger.v("YBDGamePropsAnimateWidget trackAnimation end");
        }
        animationIndex += 1;
        if (isCallback) return;
        isCallback = true;

        /// 轨迹动画已经播放完毕， 需要播放结果动画了
        logger.v("YBDGamePropsAnimateWidget trackAnimation first end");
        widget.completionFirstCallback?.call(endfOfset);
      }
    };
    var animations = List.filled(count, null);
    var index = 0;
    var animationWidgets = animations.map((e) {
      var animationWidget = YBDGamePropsAnimateWidget(
        beginfOfset, // 开始坐标
        endfOfset, // 结束坐标
        widget.animationWidget!, // 动画widget
        (status) {
          logger.v(
              "YBDGamePropsAnimateWidget index: $index, callback: $callback, element: $e");
          callback.call(status);
        }, // 回调
        tranformBig: widget.tranformBig, // 逐渐变大
        widgetSize: widget.widgetSize,
        delayed: index * 100,
      );
      index += 1;
      return animationWidget;
    }).toList();
    return Stack(
      children: animationWidgets,
    );
  }
  void trackAnimationOcrBroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDEndAnimation extends StatefulWidget {
  Offset? endOffset;
  YBDGameGiftingNotifyBody? animateEntity; // 动画实体类
  Function(Widget widget)? completionCallback;

  YBDEndAnimation({this.endOffset, this.animateEntity, this.completionCallback});

  @override
  _YBDEndAnimationState createState() => _YBDEndAnimationState();
}

class _YBDEndAnimationState extends State<YBDEndAnimation> {
  Offset? _endfOfset;

  @override
  Widget build(BuildContext context) {
    return endAnimation();
  }

  /// 结束动画
  Widget endAnimation() {
    _endfOfset = widget.endOffset;

    logger.v("YBDGamePropsAnimateWidget, endOffset: $_endfOfset, "
        "animation local path: ${widget.animateEntity?.animationLocalPath}");
    if (widget.animateEntity?.animationLocalPath?.isEmpty ?? true) {
      return Container();
    }

    /// 展示结束之后的动画
    var left = _endfOfset!.dx - 75.dp720;
    if (_endfOfset!.dx > SizeNum.screentWidth / 2.0) {
      /// 仅仅调整屏幕右边的
      left = _endfOfset!.dx - 65.dp720;
    }
    return Positioned(
      left: left,
      top: _endfOfset!.dy - 75.dp720,
      child: Container(
        width: 150.dp720,
        height: 150.dp720,
        // color: Colors.red.withOpacity(0.3),
        child: YBDSVGACtrlPlayImage(
          // assetsName: "assets/animation/dev_game_egg.svga",
          file: File(widget.animateEntity!.animationLocalPath!),
          startPlay: true,
          playStateListener: (status) {
            if (status == AnimationStatus.completed ||
                status == AnimationStatus.dismissed) {
              logger.v("YBDGamePropsAnimateWidget endAnimation end");
              // _endAnimationPlaying = false;
              // setState(() {});
              widget.completionCallback?.call(widget);
            } else if (status == AnimationStatus.forward) {
              // 开始播放动画了，同时播放音频
              logger.v(
                  "YBDGamePropsAnimateWidget, play audio: ${widget.animateEntity!.animationMp3LocalPath}");
              YBDAudioPlayerManager.instance!
                  .playAudio(widget.animateEntity!.animationMp3LocalPath);
            }
          },
        ),
      ),
    );
  }
  void endAnimation5LF2Poyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
