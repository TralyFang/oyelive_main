import 'dart:async';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gifting_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_ludo_props_playing.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';

class YBDGameRoomLudoPlaying extends StatefulWidget {
  final bool showRefresh;
  YBDGameRoomLudoPlaying(this.showRefresh);

  @override
  _YBDGameRoomLudoPlayingState createState() => _YBDGameRoomLudoPlayingState();
}

class _YBDGameRoomLudoPlayingState extends BaseState<YBDGameRoomLudoPlaying> {
  final GlobalKey endAnimationKey0 = GlobalKey();
  final GlobalKey endAnimationKey1 = GlobalKey();
  final GlobalKey endAnimationKey2 = GlobalKey();
  final GlobalKey endAnimationKey3 = GlobalKey();

  final GlobalKey webviewKey = GlobalKey();

  WebViewController? _webViewController;

  @override
  void initState() {
    super.initState();

    YBDGameLog.v("gamePlayingView initstate");

    /// 需要定位的动画坐标key
    YBDGameRoomGiftEvent.endAnimationKeys = [
      endAnimationKey0,
      endAnimationKey1,
      endAnimationKey2,
      endAnimationKey3,
    ];

    /// 监听js channel 事件 由客户端处理了
    // addSub(eventBus.on<YBDGameRoomJSChannelEvent>().listen((event) {
    //   switch (event.eventName) {
    //     case YBDGameRoomJSChannelEvent.openGiftDialog:
    //       {
    //         if (event.userId != null) {
    //           /// 礼物弹窗
    //           YBDGamePropsPop.show(context, userId: "${event.userId}");
    //         }
    //       }
    //       break;
    //   }
    // }));

    addSub(eventBus.on<YBDGameRoomGetEvent>().listen((YBDGameRoomGetEvent event) {
      switch (event.eventName) {
        case YBDGameRoomGetEvent.updateWebView:
          {
            if (mounted) {
              if (_webViewController != null) {
                YBDGameRoomGetLogic logic = Get.find<YBDGameRoomGetLogic>();
                if (logic.state!.exitRoom) {
                  setState(() {}); // 触发隐藏webview
                }
                YBDGameLog.v(
                    "updateWebView to reloadWebViewURL exitRoom： ${logic.state!.exitRoom}");
                // 不需要重载webview，避免webview重载一闪
                // YBDTPWebViewState.reloadWebViewURL(
                //     _webViewController,
                //     Get.find<YBDGameRoomGetLogic>().state.ludoUrl);
              } else {
                YBDGameLog.v("updateWebView to setState");
                setState(() {});
              }
            }
          }
          break;
      }
    }));
  }
  void initState8gclPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    YBDGameLog.v("game_room_ludo_playing dispose");
    _webViewController = null;
    super.dispose();
  }
  void disposeyEuseoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    /// ludo
    YBDGameLog.v("gamePlayingView build");
    return Stack(
      children: [
        /// web用户定位层级
        gamePlayerPosition(),

        /// 游戏层级
        gamePlayingView(),

        /// 礼物层级
        YBDGameLudoPropsPlaying(),

        /// 点击玩家头像层
        webViewPlayerAvatars(),
      ],
    );
  }
  void myBuildBNeDkoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget gamePlayingView() {
    // return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
    YBDGameRoomGetLogic logic = Get.find<YBDGameRoomGetLogic>();
    bool playing = logic.state!.gamePlaying;
    String ludoUrl = logic.state!.ludoUrl;
    bool exitRoom = logic.state!.exitRoom;

    /// 不展示ludo游戏
    if (YBDCommonUtil.getRoomOperateInfo().gameRoomShowLudo == 0) {
      YBDGameLog.v("gamePlayingView not show ludoUrl: $ludoUrl, playing:$playing");
      return Container();
    }

    YBDGameLog.i(
        "gamePlayingView ludoUrl: $ludoUrl, playing:$playing, exitRoom: $exitRoom");

    // 退出房间移除webView
    if (exitRoom) {
      _webViewController = null;
      return Container();
    }

    /// 游戏界面
    if (!ludoUrl.contains("http")) {
      return YBDLoadingCircle(); // 这里可以换成ludo一样的加载叶
    }
    // 直接全屏展示了，由web控制显隐

    return YBDTPWebView(
      ludoUrl,
      key: webviewKey,
      showNavBar: false,
      opaque: false,
      enableHybrid: true,
      gameType: WebGameType.Ludo,
      showStartLoading: false,
      finishedCallback: () {
        logic.sendGameConfigToWebView(update: true);
      },
      createdCallback: (WebViewController webViewController) {
        _webViewController = webViewController;
      },
    );
    // });
  }
  void gamePlayingViewCt43coyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// web游戏玩家坐标
  Widget gamePlayerPosition() {
    return Obx(() {
      YBDGameRoomGetLogic logic = Get.find<YBDGameRoomGetLogic>();
      YBDGameLog.v(
          "gamePlayerPosition change: ${logic.state!.playerPositionChange.value}");
      if (!logic.state!.playerPositionChange.value) {
        return Container();
      }
      List<YBDGamePlayerPosition?>? positions = logic.state!.playerPositions;
      if ((positions?.length ?? 0) != 4) {
        YBDGameLog.v("gamePlayerPosition length: ${positions?.length}");
        return Container();
      }

      /// 对角坐标即可
      num left = positions![0]?.centerX ?? -1;
      num bottom = positions[0]?.centerY ?? -1;
      num top = positions[2]?.centerY ?? -1;
      if (top == -1) {
        top = positions[1]?.centerY ?? -1;
      }
      num height = bottom - top;
      if (left == -1 || top == -1 || bottom == -1 || height < 0) {
        YBDGameLog.v("gamePlayerPosition value not found");
        return Container();
      }
      YBDGameLog.v("gamePlayerPosition value "
          "screentWidth: ${SizeNum.screentWidth} "
          "screentheight: ${SizeNum.screentHeight} "
          "left: $left, bottom: $bottom, top:$top, height:$height");
      // type 'int' is not a subtype of type 'double'
      return Container(
        width: SizeNum.screentWidth,
        height: height.toDouble(),
        padding: EdgeInsets.symmetric(horizontal: left.toDouble()),
        margin: EdgeInsets.only(top: top + (SizeNum.statusBarHeight as double)),
        child: Stack(
          children: [
            // 0号玩家 左下角
            Positioned(
              key: endAnimationKey0,
              left: 0,
              bottom: 0,
              child: redContainer(),
            ),
            // 1号玩家 左上角
            Positioned(
              key: endAnimationKey1,
              left: 0,
              top: 0,
              child: redContainer(),
            ),
            // 2号玩家 右上角
            Positioned(
              key: endAnimationKey2,
              right: 0,
              top: 0,
              child: redContainer(),
            ),
            // 3号玩家 右下角
            Positioned(
              key: endAnimationKey3,
              right: 0,
              bottom: 0,
              child: redContainer(),
            )
          ],
        ),
      );
    });
  }
  void gamePlayerPositionVPTXioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  redContainer() {
    return Container();
  }

  /// 可点击的webView玩家头像层
  webViewPlayerAvatars() {
    return Obx(() {
      YBDGameRoomGetLogic logic = Get.find<YBDGameRoomGetLogic>();
      YBDGameLog.v(
          "webViewPlayerAvatars change: ${logic.state!.playerPositionChange.value}");
      if (!logic.state!.playerPositionChange.value ||
          logic.state!.playerPositionChangeStr.value.isEmpty) {
        // return Container();
      }
      List<YBDGamePlayerPosition?>? positions = logic.state!.playerPositions;
      if ((positions?.length ?? 0) != 4) {
        YBDGameLog.v("webViewPlayerAvatars length: ${positions?.length}");
        return Container();
      }
      Iterable<Widget> positionWidgets = positions?.map((YBDGamePlayerPosition? e) {
        if (e!.userId == null || e.centerY == null || e.centerX == null)
          return Container();
        return Positioned(
          left: e.centerX!.toDouble(),
          top: e.centerY!.toDouble() + SizeNum.statusBarHeight,
          child: YBDGameWidgetUtil.assetImage(
            "",
            minTapSize: 80.dp720,
            onTap: () {
              /// 礼物弹窗
              if (logic.state!.gamePlaying) {
                YBDGamePropsPop.show(context, userId: "${e.userId}");
              }
            },
            child: Container(
              width: 1,
              height: 1,
            ),
          ),
        );
      }) ?? [Container()];
      return Stack(
        children: [...positionWidgets],
      );
    });
  }
}
