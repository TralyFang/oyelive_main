import 'dart:async';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_zego_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/method_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/platform_ybd_game_view.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/game_ybd_room_page.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_room_background.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/media_ybd_cache_widget.dart';
import 'package:oye_tool_package/widget/should_rebuild.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';

class YBDGameRoomZegoPlaying extends StatefulWidget {
  final bool showRefresh;
  YBDGameRoomZegoPlaying(this.showRefresh);

  static bool zegoDataDidChange() {
    try {
      var logic = Get.find<YBDGameRoomGetLogic>();
      var code = logic.state!.zegoCodeData;
      var mgId = logic.state!.roomStatusNotify.body?.game?.thirdPartyId ?? "";
      var mgIdInt = int.tryParse(mgId);
      if (code == null || (code.code?.isEmpty ?? true) || mgIdInt == null) {
        return false;
      }
      return true;
    } catch (e) {
      logger.v("zegoDataChange error: $e");
    }
    return false;
  }

  @override
  _YBDGameRoomZegoPlayingState createState() => _YBDGameRoomZegoPlayingState();
}

class _YBDGameRoomZegoPlayingState extends State<YBDGameRoomZegoPlaying> {
  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      loadZegoMiniGame(),
      /// 加入背景，在匹配过程中遮住小游戏背景
      GetBuilder<YBDGameRoomGetLogic>(
          id: "state.gamePlaying",
          builder: (logic) {
            var gamePlaying = logic.state!.gamePlaying;
            logger.v("loadZegoMiniGame pre gamePlaying: $gamePlaying");
            if (logic.state!.zegoMGLoading) {
              gamePlaying = true; // 游戏还在加载中，不能有匹配界面
            }
            logger.v("loadZegoMiniGame gamePlaying: $gamePlaying, ${logic.state!.gamePlaying}");
            return Offstage(
              offstage: gamePlaying,
              child: YBDGameRoomBackground(),
            );
          }),
    ]);
  }
  void buildRUKgpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  loadZegoMiniGame() {
    logger.v("loadZegoMiniGame build");

    YBDGameRoomGetLogic? logic;
    try {
      logic = Get.find<YBDGameRoomGetLogic>();
    } catch (e) {
      logger.v("loadZegoMiniGame error: $e");
    }
    if (logic == null) return Container();

    /// 没有游戏初始化的appcode就不创建游戏了
    var code = logic.state!.zegoCodeData;
    var mgId = logic.state!.roomStatusNotify.body?.game?.thirdPartyId ?? "";
    var mgIdInt = int.tryParse(mgId);
    if (code == null || (code.code?.isEmpty ?? true) || mgIdInt == null || mgId.length < 10) {
      return YBDLoadingCircle();
    }
    var playing = logic.state!.gamePlaying;
    YBDGameUtils.getInstance()!.notifyAppUpdateCodeCallback(
        () => logic!.getZegoCode(needUpdate: false));

    playing = true;
    logger
        .v("loadZegoMiniGame build YBDPlatformGameView code: $code, mgId: $mgId");
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: playing ? SizeNum.screentWidth : 600.dp720,
          height: playing ? SizeNum.screentHeight : 550.dp720,
          margin: playing ? EdgeInsets.zero : EdgeInsets.only(top: 600.dp720 as double),
          child: ShouldRebuild(
            shouldRebuild: ((dynamic oldWidget, dynamic newWidget) => false),
            child: YBDPlatformGameView(params: {
              "roomId": "${logic.state!.roomId}",
              "userId": "${logic.state!.currentUserId}",
              "appCode": "${code.code}", // appcode
              "expireDate": "${code.expireDate}",
              "mgId": mgIdInt, // 游戏id
              "appKey": YBDGameUtils.getInstance()!.mgAppKeyData?.appKey ?? GameConst.ZEGO_MG_APP_KEY, // appkey
              "appId": YBDGameUtils.getInstance()!.mgAppKeyData?.appId ?? GameConst.ZEGO_MG_APP_ID, // appId
              "view_game_top": YBDGameRoomPageState.zegoMiniGameViewTop(),
              "view_game_bottom": YBDGameRoomPageState.zegoMiniGameViewBottom(),
              /** 默认语言  游戏语言 现支持，简体：zh-CN 繁体：zh-TW 英语：en-US 马来语：ms-MY */
              "language":"en-US",
              "isTestEnv": Const.TEST_ENV,
              // "view_game_bottom":SizeNum
            }),
          ),
        ),
      ],
    );
  }
  @override
  void dispose() {
    logger.v("YBDGameRoomZegoPlaying dispose");
    super.dispose();
  }
  void disposei5i2voyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
