
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/text_ybd_with_stroke.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gift_entity.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';
import 'package:oyelive_main/ui/page/room/widget/mini_ybd_profile_dialog.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/inner_shadow/flutter_ybd_inset_box_shadow.dart' as i;

typedef GameVoidCallback = void Function(BuildContext context);

/// 礼物弹窗
class YBDGamePropsPop extends StatefulWidget {
  static bool _isShowPop = false;
  static show(BuildContext context, {String? userId}) {
    YBDGameLog.v("YBDGamePropsPop show: $_isShowPop");
    if (_isShowPop) return;
    _isShowPop = true;
    YBDEasyPopup.show(
      context,
      YBDGamePropsPop(
        userId: userId,
        gameVoidCallback: (popContext) {
          YBDEasyPopup.pop(popContext);
          _isShowPop = false;
        },
      ),
      position: EasyPopupPosition.center,
      outsideTouchCancelable: true,
      duration: Duration(milliseconds: 100),
      dismissCallback: () {
        _isShowPop = false;
      },
    );
  }

  final GameVoidCallback? gameVoidCallback;
  final String? userId;

  YBDGamePropsPop({Key? key, this.gameVoidCallback, this.userId = ''}) : super(key: key);

  @override
  _YBDGamePropsPopState createState() => _YBDGamePropsPopState();
}

class _YBDGamePropsPopState extends State<YBDGamePropsPop> {
  int _selectedIndex = 0; // 默认选中第一个

  @override
  void initState() {
    super.initState();
    if (Get.find<YBDGameRoomGetLogic>().state!.giftList?.isEmpty ?? true) {
      // 礼物列表为空从服务器拉数据
      Get.find<YBDGameRoomGetLogic>().requestGiftList();
    }
  }
  void initStateX2zuOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    /// 避免被挤退了，没有走回调，这里需要重置下
    YBDGamePropsPop._isShowPop = false;
    super.dispose();
  }
  void disposeq3g22oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          width: 614.dp720,
          height: 774.dp720,
          decoration: BoxDecoration(
            border: Border.all(width: 3.dp750 as double, style: BorderStyle.solid, color: Color(0xffFFD94E)),
            borderRadius: BorderRadius.all(Radius.circular(33.dp750 as double)),
            gradient: LinearGradient(
              colors: [Color(0xff32C7F1), Color(0xff1C58DB)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
            // image: DecorationImage(image: AssetImage(YBDGameResource.assetPadding("game_props_bg"))),
          ),
          child: Stack(
            children: [
              Column(
                children: [
                  // 用户信息
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 38.dp720 as double, horizontal: 35.dp720 as double),
                    child: YBDGamePropUserInfo(
                      userInfo: Get.find<YBDGameRoomGetLogic>().userSeatWithId(widget.userId),
                    ),
                  ),
                  // 道具
                  Container(
                    height: (138 * 3 + 36).dp720,
                    child: GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
                      var giftList = logic.state!.giftList;

                      if (giftList?.isEmpty ?? true) {
                        return YBDLoadingCircle();
                      }

                      return GridView.builder(
                        shrinkWrap: true,
                        itemCount: giftList!.length,
                        padding: EdgeInsets.symmetric(horizontal: 20.dp720 as double, vertical: 0),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 4,
                          mainAxisSpacing: 18.dp720 as double,
                          crossAxisSpacing: 19.dp720 as double,
                          childAspectRatio: 129 / 138.0, // 宽高比
                        ),
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                this._selectedIndex = index;
                              });
                            },
                            child: YBDGamePropsItem(
                              isSelected: index == _selectedIndex,
                              giftData: giftList[index],
                            ),
                          );
                        },
                      );
                    }),
                  ),
                  Expanded(child: SizedBox()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      YBDDelayGestureDetector(
                        onTap: () {
                          logger.v('clicked send gift btn');
                          if (Get.find<YBDGameRoomGetLogic>().state?.giftList?.isEmpty ?? true) {
                            logger.w('gift list is empty');
                            return;
                          }

                          final gift = Get.find<YBDGameRoomGetLogic>().state!.giftList![_selectedIndex]!;

                          // 发送礼物
                          YBDGameRoomSocketApi.getInstance().gifting(
                            giftId: gift.id,
                            number: 1, //gift.count, 志强说的，x10的礼物也是数量为1
                            toUserId: YBDNumericUtil.stringToInt(widget.userId),
                          );
                          widget.gameVoidCallback?.call(context);
                        },
                        child: Container(
                          height: 80.dp750,
                          width: 330.dp750,
                          decoration: i.BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [Color(0xffFFCE5B), Color(0xffFFBC04)],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter),
                              borderRadius: BorderRadius.all(Radius.circular(40.dp750 as double)),
                              boxShadow: [
                                i.BoxShadow(
                                    color: Colors.white.withOpacity(0.5),
                                    offset: Offset(0, 1.px as double),
                                    blurRadius: 2.px as double,
                                    inset: true),
                                i.BoxShadow(
                                    color: Color(0xff7E5A03), offset: Offset(0, -1.px as double), blurRadius: 2.px as double, inset: true),
                              ]),
                          child: Center(
                            child: YBDTextWithStroke(
                              text: 'Sent',
                              fontSize: 32.sp750 as double,
                              strokeColor: Color(0xff786000),
                              fontFamily: 'baloo',
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 48.dp720)
                ],
              ),
            ],
          ),
        ),
        SizedBox(height: 53.dp720),
        YBDGameWidgetUtil.assetImage(
          'icon_close_blue',
          width: 58.dp720,
          height: 58.dp720,
          onTap: () {
            YBDGameLog.v("game props pop tap close");
            widget.gameVoidCallback?.call(context);
          },
        ),
      ],
    );
  }
  void buildDMCmxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 用户信息
class YBDGamePropUserInfo extends StatelessWidget {
  const YBDGamePropUserInfo({Key? key, this.userInfo}) : super(key: key);

  /// 用户信息
  final YBDUserInfo? userInfo;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        YBDGameWidgetUtil.avatar(
          url: "${userInfo!.headimg}",
          addSize: true,
          width: 69.dp720,
          radius: 34.5.dp720,
          sideBorder: true,
        ),
        SizedBox(width: 20.dp720),
        Flexible(
          fit: FlexFit.loose,
          flex: 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              YBDGameWidgetUtil.textSingleRegular("${userInfo!.nickname}", color: Colors.white, fontSize: 27.sp720),
              SizedBox(height: 1.dp720),
              YBDGameWidgetUtil.textSingleRegular("ID:${userInfo!.id}",
                  color: Colors.white.withOpacity(0.7), fontSize: 23.sp720),
            ],
          ),
        ),
        SizedBox(width: 20.dp720),
        // miniprofile
        YBDDelayGestureDetector(
          onTap: () {
            tryCatch(() {
              int? roomId = Get.find<YBDGameRoomGetLogic>().state!.roomId;
              // 展示mini profile
              YBDMiniProfileDialog.showGame(context, roomId: roomId, userId: userInfo!.id);
            });
          },
          child: Container(
            height: 56.dp750,
            width: 120.dp750,
            decoration: i.BoxDecoration(
                gradient: LinearGradient(
                    colors: [Color(0xff409CFC), Color(0xff308AFA)],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter),
                borderRadius: BorderRadius.all(Radius.circular(42.dp750 as double)),
                boxShadow: [
                  i.BoxShadow(
                      color: Colors.white.withOpacity(0.5), offset: Offset(0, 1.px as double), blurRadius: 2.px as double, inset: true),
                  i.BoxShadow(color: Color(0xff0E51BA), offset: Offset(0, -1.px as double), blurRadius: 2.px as double, inset: true),
                ]),
            child: Center(
              child: YBDTextWithStroke(
                text: "profile".i18n,
                fontSize: 28.sp750 as double,
                strokeColor: Color(0xff0A59A2),
                fontFamily: 'baloo',
              ),
            ),
          ),
        ),
      ],
    );
  }
  void buildj0uL9oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 单个礼物
class YBDGamePropsItem extends StatelessWidget {
  final bool? isSelected;
  final YBDGameGiftData? giftData;

  const YBDGamePropsItem({Key? key, this.isSelected, this.giftData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 129.dp720,
      height: 138.dp720,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            YBDGameResource.assetPadding(this.isSelected! ? "game_props_sel" : "game_ props_nor"),
          ),
        ),
      ),
      child: Stack(
        children: [
          Column(
            children: [
              // 图标
              Container(
                width: 125.dp720,
                height: 91.dp720,
                child: Container(
                  width: 70.dp720,
                  height: 70.dp720,
                  alignment: Alignment.center,
                  child: YBDGameWidgetUtil.networkImage(
                      url: YBDImageUtil.gift(context, giftData!.img, 'B'),
                      height: 70.dp720,
                      width: 70.dp720,
                      fit: BoxFit.scaleDown,
                      placeholder: CupertinoActivityIndicator()),
                ),
              ),
              // 金币
              Container(
                height: 47.dp720,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    YBDGameWidgetUtil.networkImage(
                      url: giftData!.currencyImage ?? '',
                      width: 19.dp720,
                      height: 20.dp720,
                    ),
                    SizedBox(width: 6.dp720),
                    YBDGameWidgetUtil.textSingleRegular(
                      '${giftData!.price}',
                      color: Colors.white,
                      fontSize: 23.sp720,
                    )
                  ],
                ),
              )
            ],
          ),
          if ((giftData?.count ?? 0) > 1)
            Positioned(
              top: 3.dp720,
              right: 5.dp720,
              child: Container(
                height: 21.dp720,
                decoration: BoxDecoration(
                  color: Color(0xff0062fd),
                  borderRadius: BorderRadius.circular(9.5.dp720 as double),
                ),
                padding: EdgeInsets.symmetric(horizontal: 5.dp720 as double),
                child: YBDGameWidgetUtil.textAlegreyaBlack(
                  "x${giftData!.count}",
                  color: Colors.white,
                  fontSize: 19.sp720,
                ),
              ),
            ),
        ],
      ),
    );
  }
  void buildvSCoBoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
