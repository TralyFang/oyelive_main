import 'dart:async';


import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gifting_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/room/widget/svga_ybd_ctrl_player.dart';

/// 动画展示流程
/// 动画目标-->初始位置从小--->终点变大--->播放终点动画
class YBDGamePropsAnimateWidget extends StatefulWidget{
  final Offset startOffset;
  final Offset endOffset;
  final Widget animateWidget;
  final Size? widgetSize;
  final Function? animateCallback;
  final Function(int)? animateDurationCallback;// ms
  final int duration; // ms
  final int? delayed; // 延迟ms播放
  final bool? tranformBig;


  YBDGamePropsAnimateWidget(
      this.startOffset,
      this.endOffset,
      this.animateWidget,
      this.animateCallback,
      {
        int duration = -1,
        this.delayed,
        this.tranformBig = false,
        this.widgetSize = Size.zero,
        this.animateDurationCallback,
      })
      : this.duration = duration,
        assert(animateWidget != null);


  @override
  State<StatefulWidget> createState() {
    return YBDGamePropsAnimateWidgetState();
  }

  static void beansBetAnimation(
      BuildContext context, // 所在的容器
      {
        bool tranformBig=true, // 逐渐变大，否则变小
        required GlobalKey beginKey,  // 开始动画坐标
        required GlobalKey endKey,  // 结束动画坐标
        Widget? animationWidget,  // 动画对象
        YBDGameGiftingNotifyBody? animateEntity, // 动画实体类
        Function()? completedCallback, // 结束动画的回调
        Size widgetSize = Size.zero, // 动画对象大小
      }) {

    logger.v("$YBDGamePropsAnimateWidget, context: $context");

    // 这里需要转换下widget的坐标。
    RenderObject beginRenderBox = beginKey.currentContext!.findRenderObject()!;
    var beginV3 = beginRenderBox.getTransformTo(context.findRenderObject()).getTranslation();
    var beginSize = beginRenderBox.paintBounds.size;
    Offset beginfOfset =  Offset(beginV3[0]+beginSize.width/2, beginV3[1]+beginSize.height/2);


    RenderObject endRenderBox = endKey.currentContext!.findRenderObject()!;
    var endV3 = endRenderBox.getTransformTo(context.findRenderObject()).getTranslation();
    var endSize = endRenderBox.paintBounds.size;
    Offset endfOfset =  Offset(endV3[0]+endSize.width/2, endV3[1]+endSize.height/2);

    logger.v("$YBDGamePropsAnimateWidget, context: $context, "
        "begin: $beginfOfset, beginSize: $beginSize, "
        "end: $endfOfset, endSize: $endSize");

    Function? callback;
    Function? svgaCallback;
    bool playingSvga = false;
    OverlayEntry entry = OverlayEntry(
        builder: (ctx) {

          if (playingSvga) {
            logger.v("$YBDGamePropsAnimateWidget, endOffset: $endfOfset, animation local path: ${animateEntity?.animationLocalPath}");
            if (animateEntity?.animationLocalPath?.isEmpty ?? true) {
              /// 动画播放完了
              svgaCallback?.call();
              return Container();
            }
            /// 展示结束之后的动画
            return Positioned(
              left: endfOfset.dx-75.dp720,
              top: endfOfset.dy-75.dp720,
              child: Container(
                width: 150.dp720,
                height: 150.dp720,
                child: YBDSVGACtrlPlayImage(
                  // assetsName: "assets/animation/dev_game_egg.svga",
                  file: File(animateEntity!.animationLocalPath!),
                  startPlay: true,
                  playStateListener: (status) {
                    if (status == AnimationStatus.completed
                        || status == AnimationStatus.dismissed) {
                      /// 动画播放完了
                      svgaCallback?.call();
                    }
                  },
                ),
              ),
            );
          }

          return YBDGamePropsAnimateWidget(
            beginfOfset, // 开始坐标
            endfOfset, // 结束坐标
            animationWidget!, // 动画widget
            callback, // 回调
            tranformBig: tranformBig, // 逐渐变大
            widgetSize: widgetSize,
          );
        });
    callback = (status) {
      if (status == AnimationStatus.completed
      || status == AnimationStatus.dismissed) {
        playingSvga = true;
        entry.markNeedsBuild();
        logger.v("$YBDGamePropsAnimateWidget, animation end entry: $entry");
      }
    };
    svgaCallback = (){
      logger.v("$YBDGamePropsAnimateWidget, svga play end entry: $entry");
      completedCallback?.call();
      entry.remove();
    };
    // 需要移动的widget key
    Overlay.of(context)!.insert(entry);
  }

}

class YBDGamePropsAnimateWidgetState extends State<YBDGamePropsAnimateWidget> with
    SingleTickerProviderStateMixin {

  YBDGamePropsAnimateWidgetState();

  AnimationController? _controller;
  late Animation<Offset> _slideAnimation;
  late Animation<double> _scaleAnimation;
  late Animation<double> _fadeAnimation;

  @override
  void initState() {

    super.initState();

    var startPoint = Point(widget.startOffset.dx, widget.startOffset.dy);
    var endPoint = Point(widget.endOffset.dx, widget.endOffset.dy);
    var distance = startPoint.distanceTo(endPoint);
    //最近距离 134.14975983952948(400ms) 最远距离 678.6799896498279
    var ms = (distance * 1.4).toInt();
    if (ms < 300) ms = 300;
    widget.animateDurationCallback?.call(ms);
    logger.v("YBDGamePropsAnimateWidgetState distance: $distance, ms: $ms");


    _controller = AnimationController(vsync: this,duration: Duration(milliseconds:
    widget.duration <=0 ? ms : widget.duration));


    // 缩放+渐变+位移
    _scaleAnimation = Tween(begin: widget.tranformBig! ? 0.1 : 1.0,  end: widget.tranformBig! ? 1.0 : 0.1).animate(_controller!);
    _fadeAnimation = Tween(begin: widget.tranformBig! ? 0.1 : 1.0, end: widget.tranformBig! ? 1.0 : 0.1)
        .animate(
        CurvedAnimation(
            parent: _controller!,
            curve: Interval(
                0.0,
                1.0,
                curve: Curves.easeOut)
        ));
    _slideAnimation = Tween(begin: widget.startOffset, end: widget.endOffset).animate(_controller!);

    WidgetsBinding.instance?.addPostFrameCallback((_){
      if(widget.startOffset == null || widget.endOffset == null){
        widget.animateCallback?.call(AnimationStatus.completed);
        return;
      }
      if (widget.delayed != null && widget.delayed! > 0) {
        Future.delayed(Duration(milliseconds: widget.delayed!),(){
          if (mounted)
            _controller?.forward();
        });
      }else {
        _controller!.forward();
      }
    });

    _controller!.addStatusListener((status) {
      widget.animateCallback?.call(status);
      if(status == AnimationStatus.completed
          || status == AnimationStatus.dismissed){
        clearAnimationController();
      }
    });

  }
  void initStatebFtRPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 清理动画控制器
  void clearAnimationController() {
    if (_controller != null) {
      _controller!.stop();
      _controller!.dispose();
      _controller = null;
    }
  }
  void clearAnimationControllerycG7ooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    clearAnimationController();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {

    return AnimatedBuilder(
      animation: _slideAnimation,
      builder: (context, child) {
        return Positioned(
          left: _slideAnimation.value.dx-(widget.widgetSize!.width*_scaleAnimation.value)/2,
          top: _slideAnimation.value.dy-(widget.widgetSize!.height*_scaleAnimation.value)/2,
          child: Opacity(
            opacity: _fadeAnimation.value,
            child: Transform.scale(
                scale: _scaleAnimation.value,
                child: widget.animateWidget
            )
        ),
        );
      },
    );
  }
  void buildT7fMpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

}