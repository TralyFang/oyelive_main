import 'dart:async';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_room_ludo_playing.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_room_zego_playing.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oye_tool_package/widget/should_rebuild.dart';

/// 正在玩游戏: YBDGameRoomLudoPlaying or YBDGameRoomZegoPlaying
class YBDGameRoomScreenPlaying extends StatefulWidget {
  const YBDGameRoomScreenPlaying({Key? key}) : super(key: key);

  @override
  _YBDGameRoomScreenPlayingState createState() => _YBDGameRoomScreenPlayingState();
}

class _YBDGameRoomScreenPlayingState extends BaseState<YBDGameRoomScreenPlaying> {
  @override
  void initState() {
    super.initState();
    YBDGameLog.v("gamePlayingView gameroomscreenplaying initstate");
  }
  void initStateV75Itoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      YBDGameLog.v("gamePlayingView gameroomscreenplaying build");
      var gameType = GameKindTypeValue.typeWithValue(logic.state!.gameSubCategory);
      if (gameType == GameKindType.bumper ||
          gameType == GameKindType.knife ||
          gameType == GameKindType.zegoMG) {
        return ShouldRebuild<YBDGameRoomZegoPlaying>(
          // shouldRebuild: (oldWidget, newWidget) =>
          //     oldWidget.showRefresh != newWidget.showRefresh,
          child: YBDGameRoomZegoPlaying(YBDGameRoomZegoPlaying.zegoDataDidChange()),
        );
      }

      /// ludo 不要去刷新，避免webview异常问题
      return ShouldRebuild<YBDGameRoomLudoPlaying>(
        shouldRebuild: (oldWidget, newWidget) =>
            oldWidget.showRefresh != newWidget.showRefresh,
        child: YBDGameRoomLudoPlaying(false),
      );
    });
  }
  void myBuildyZjy1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
