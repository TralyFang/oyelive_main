import 'dart:async';


import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/method_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_operator_center/game_ybd_header_operator_center.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';
import 'package:oyelive_main/ui/page/share/widget/game_ybd_friend_list_dialog.dart';
import 'package:oyelive_main/ui/widget/sharing_ybd_widget.dart';

import '../../../../../common/analytics/analytics_ybd_util.dart';

typedef GameChildrenCallback = List<Widget> Function(BuildContext context);

/// 游戏房间上面弹出操作
class YBDGameTopSheet extends StatefulWidget {
  /// 退出房间 是否是主播
  static showQuit(BuildContext context, bool? isTalent) {
    /// 退出房间
    YBDEasyPopup.show(context, YBDGameTopSheet(childrenCallback: (popContext) {
      return [
        YBDGameTopSheet.gameButtonText(
            isTalent! ? "end_live".i18n : "quit".i18n, isTalent ? "game_end_icon" : "game_exit_icon", onTap: () {
          YBDEasyPopup.pop(popContext);
          YBDGameHeaderOperatorCenter.exitRoom();
        })
      ];
    }), darkEnable: false, contentTouchCancelable: true);
  }

  static showBottom(BuildContext context, bool isMute, Function callBack) {
    /// 分享禁麦
    YBDEasyPopup.show(
        context,
        YBDGameTopSheet(
            isFromTop: false,
            childrenCallback: (context) {
              return [
                YBDGameTopSheet.gameButtonText(
                    isMute ? "game_unmute".i18n : 'game_mute'.i18n, // Unmute
                    isMute ? "game_numute_icon" : "game_mute_icon", // game_numute_icon
                    onTap: () {
                  callBack.call();
                  YBDRtcHelper.getInstance().getRtcApi.muteAllRemoteAudioStreams(!isMute);
                  eventBus.fire(YBDGameRoomJSChannelEvent()
                    ..isMute = isMute
                    ..eventName = YBDGameRoomJSChannelEvent.muteGameVoice);
                  // 关闭即构小游戏音频
                  try {
                    var logic = Get.find<YBDGameRoomGetLogic>();
                    if (logic.state!.isZegoMG) {
                      // 这里的声音竟然要取反
                      YBDGameUtils.getInstance()!.notifyAppCommonSelfSound(isMute);
                      logic.state!.isOpenZegoMGSound = isMute;
                      logic.update(["zegoMG.sound"]);
                    }
                  } catch (e) {
                    YBDGameLog.v("share get find YBDGameRoomGetLogic fail");
                  }
                  YBDEasyPopup.pop(context);

                  YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                    YBDEventName.CLICK_EVENT,
                    location: YBDLocationName.GAME_ROOM_PAGE,
                    itemName: "mute all",
                  ));
                }),
                SizedBox(width: 38.dp720),
                YBDGameTopSheet.gameButtonText("share".i18n, "game_forward_icon", onTap: () {
                  YBDEasyPopup.pop(context);

                  try {
                    var logic = Get.find<YBDGameRoomGetLogic>();

                    /// 分享相关类
                    showModalBottomSheet(
                        context: context,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.vertical(top: Radius.circular(16.dp720 as double)),
                        ),
                        builder: (BuildContext context) {
                          /// 游戏房分享
                          return YBDSharingWidget(
                            onlyFriends: true,
                            shareInside: false,
                            gameShareData: {
                              "title": "${logic.state!.talentInfo?.nickname}",
                              "content": "Come and play ${logic.state!.gameSubCategory} game together!",
                              "roomId": "${logic.state!.roomId}",
                              "roomImg": "${logic.state!.talentInfo?.avatarURL()}",
                            },
                            type: GameShareType.ludo,
                          );
                        });
                  } catch (e) {
                    YBDGameLog.v("share get find YBDGameRoomGetLogic fail");
                  }

                  YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                    YBDEventName.CLICK_EVENT,
                    location: YBDLocationName.GAME_ROOM_PAGE,
                    itemName: "share",
                  ));
                })
              ];
            }),
        position: EasyPopupPosition.bottom,
        darkEnable: false,
        contentTouchCancelable: true);
  }

  /// 按钮：图标+标题
  static Widget gameButtonText(String text, String imgName, {GestureTapCallback? onTap}) {
    return GestureDetector(
      // behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Column(
        children: [
          YBDGameWidgetUtil.assetImage(imgName, width: 77.dp720, height: 77.dp720),
          SizedBox(height: 5.dp720),
          YBDGameWidgetUtil.textSingleMedium(text, color: Colors.white, fontSize: 21.sp720)
        ],
      ),
    );
  }

  GameChildrenCallback? childrenCallback;
  bool isFromTop = true; // 是否从顶部弹出，否则从底部弹出

  YBDGameTopSheet({Key? key, this.childrenCallback, this.isFromTop = true}) : super(key: key);

  @override
  _YBDGameTopSheetState createState() => _YBDGameTopSheetState();
}

class _YBDGameTopSheetState extends State<YBDGameTopSheet> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 422.dp720,
      width: SizeNum.screentWidth,
      decoration: YBDGameWidgetUtil.showdowGradient(isFromTop: widget.isFromTop),
      padding: contentPadding(),
      child: fromTopRow(
          child: Row(mainAxisAlignment: MainAxisAlignment.end, children: widget.childrenCallback!(context)),
          isFromTop: widget.isFromTop),
    );
  }
  void buildvjAWVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  fromTopRow({Widget? child, bool isFromTop = true}) {
    if (!isFromTop) {
      return Stack(children: [Positioned(bottom: 0, right: 0, child: child!)]);
    }
    return child;
  }

  /// 内容缩进
  EdgeInsets contentPadding() {
    return widget.isFromTop
        ? EdgeInsets.only(top: 80.dpTopBar as double, right: 120.dp750 as double)
        : EdgeInsets.only(bottom: SizeNum.bottomBarHeight.getMaxValue(130.dp720) as double, right: 120.dp750 as double);
  }
}
