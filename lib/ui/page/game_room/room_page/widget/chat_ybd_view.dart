import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/entity/chat_ybd_view_event.dart';

import '../../../../../base/base_ybd_state.dart';
import '../../entity/chat_ybd_publish.dart';
import '../../widget/chat_ybd_logger.dart';
import '../../widget/game_ybd_chat_sheet.dart';

bool showChatView = true;

class YBDChatView extends StatefulWidget {
  // bool showView;
  @override
  YBDChatViewState createState() => YBDChatViewState();
}

class YBDChatViewState extends BaseState<YBDChatView> {
  List<YBDChatPublish> chatList = [];

  ScrollController _scrollController = ScrollController();

  @override
  Widget myBuild(BuildContext context) {
    if (showChatView)
      return Container(
        child: StreamBuilder<List<YBDChatPublish?>>(
            stream: YBDChatLogger.getInstance().gameChatHistoryOutStream,
            initialData: YBDChatLogger.getInstance().chatList,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Row(
                  children: [
                    SizedBox(
                      width: ScreenUtil().setWidth(24),
                    ),
                    Container(
                      width: ScreenUtil().setWidth(580),
                      child: SingleChildScrollView(
                        controller: _scrollController,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: List.generate(
                              snapshot.data!.length,
                              (index) => GestureDetector(
                                    onTap: () {
                                      // showModalBottomSheet(
                                      //     context: context, //
                                      //     isScrollControlled: true,
                                      //     barrierColor: Colors.transparent,
                                      //     backgroundColor: Colors.transparent, // BuildContext对象
                                      //     builder: (BuildContext context) {
                                      //       return YBDGameChatSheet(false);
                                      //     });
                                      YBDGameChatSheet.show(
                                        context,
                                        false,
                                      );
                                    },
                                    child: YBDChatItemSplit.getItem(snapshot.data![index]!, false,
                                        isLatest: index == (snapshot.data!.length - 1)),
                                  )),
                        ),
                      ),
                    ),
                  ],
                );
              }
              return SizedBox();
            }),
      );
    return SizedBox();
  }
  void myBuildy0QZDoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    addSub(YBDChatLogger.getInstance().gameChatHistoryOutStream.listen((event) {
      Future.delayed(Duration(seconds: 1), () {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 200),
          curve: Curves.fastOutSlowIn,
        );
      });
    }));

    addSub(eventBus.on<YBDChatViewEvent>().listen((event) {
      setState(() {});
      Future.delayed(Duration(seconds: 1), () {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 200),
          curve: Curves.fastOutSlowIn,
        );
      });
    }));
  }
  void initStatekzi9Foyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDChatView oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidget9wAEboyelive(YBDChatView oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
