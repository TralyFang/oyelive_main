import 'dart:async';


import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/media_ybd_cache_widget.dart';

/// 房间背景
class YBDGameRoomBackground extends StatelessWidget {
  const YBDGameRoomBackground({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      var url = logic.state!.roomInfo?.body?.property?.theme?.resource ?? "";
      // url = "https://img1.doubanio.com/view/group_topic/l/public/p494934158.webp";
      // url = "https://c-ssl.duitang.com/uploads/item/201703/25/20170325131518_eAHBa.gif";
      // url = "http://s3-us-west-2.amazonaws.com/thankyotest/image/13/6ffb957c-c3a1-425b-b089-41568aafb15a.svga";
      // url = "http://s3-us-west-2.amazonaws.com/thankyotest/image/13/b00829ef-4d60-471e-974d-9b2189542686.svga";
      // url = "http://s3-us-west-2.amazonaws.com/thankyotest/image/13/d8a2e901-79ea-42e3-b953-25524f8247df.svga";
      logger.v("YBDGameRoomBackground url: $url");
      // url = "https://oyetalk-status.s3.ap-south-1.amazonaws.com/game/icon/room/theme/default.jpg";
      if (!logic.state!.isZegoMG) { // 不是即构小游戏
        if (url.isNotEmpty)
          YBDRtcHelper.getInstance().ludoBgURL = url;
        if (url.isEmpty)
          url = YBDRtcHelper.getInstance().ludoBgURL ?? "";
      }
      return Stack(
        children: [
          Container(decoration: YBDGameWidgetUtil.linearGradientBackground()),
          YBDMediaCacheWidthWidget(
            url: url,
            width: SizeNum.screentWidth,
            height: SizeNum.screentHeight,
            fit: BoxFit.cover,
            placeholder: Container(decoration: YBDGameWidgetUtil.linearGradientBackground()),
          ),
        ],
      );
    });
  }
  void buildCqyq7oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
