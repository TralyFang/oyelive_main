import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wakelock/wakelock.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/game_ybd_match_page.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/game_ybd_settle_pop.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_page.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/chat_ybd_view.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_room_background.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_room_footer.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_room_header.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_room_playing.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_operator_center/game_ybd_header_operator_center.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/chat_ybd_logger.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_event.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/widget/intl_ybd_positioned.dart';

/// 游戏房间
class YBDGameRoomPage extends StatefulWidget {
  const YBDGameRoomPage({Key? key}) : super(key: key);

  @override
  YBDGameRoomPageState createState() => YBDGameRoomPageState();
}

class YBDGameRoomPageState extends BaseState<YBDGameRoomPage> {
  YBDGameRoomGetLogic logic = Get.put(YBDGameRoomGetLogic());

  @override
  Widget myBuild(BuildContext context) {
    YBDGameLog.v("gamePlayingView gameroompage build");

    return WillPopScope(
      onWillPop: () {
        YBDGameHeaderOperatorCenter.exitRoom();
        return Future<bool>.value(false);
      },
      child: Scaffold(
        body: GestureDetector(
          onTap: () {
            logger.v("game room page tap");
          },
          child: Stack(
            children: [
              /// 背景图
              YBDGameRoomBackground(),

              /// 全屏游戏
              YBDGameRoomScreenPlaying(),
              Container(
                /// 内容缩进
                padding: EdgeInsets.only(top: SizeNum.statusBarHeight as double, bottom: 65.dpBottomElse(20) as double),
                child: Stack(
                  children: [
                    Column(
                      children: [
                        /// 头部主播信息
                        YBDGameRoomHeader(),

                        /// 麦位信息
                        YBDGameRoomMicPage(),

                        /// 游戏匹配模块
                        Expanded(
                          child: YBDGameMatchPage(),
                        ),
                        Container(height: 80.dp720, child: YBDChatView()),

                        /// 底部操作模块
                        YBDGameRoomFooter()
                      ],
                    ),

                    /// 悬浮任务
                    YBDIntlPositioned(right: 11.dp720, bottom: 80.dp720, child: YBDGameEvent(YBDLocationName.GAME_ROOM_PAGE)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void myBuild1l4GMoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 即构小游戏可操作缩进,上面试图的缩进修改了，注意要同步修改这里的👆
  static double zegoMiniGameViewTop() {
    return SizeNum.statusBarHeight + (205.dp720 as double);
  }

  static double zegoMiniGameViewBottom() {
    return 65.dpBottomElse(20) + (141.dp720 as double);
  }

  @override
  void initState() {
    YBDGameLog.v("gamePlayingView gameroompage initstate");

    YBDChatLogger.getInstance().clear();
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      logger.v('initState : $context');
      // 监听app生命周期事件
      WidgetsBinding.instance?.addObserver(this);

      // 保持屏幕常亮
      Wakelock.enable();
    });
    super.initState();

    /// 监听房间事件
    addSub(eventBus.on<YBDGameRoomGetEvent>().listen((event) {
      try {
        var logic = Get.find<YBDGameRoomGetLogic>();

        switch (event.eventName) {
          case YBDGameRoomGetEvent.openSettleDialog:
            {
              YBDGameSettlePop.show(
                context,
                logic.settlementList,
                logic.currentUserSeat().onSeat,
                logic.matchType == GameConst.matchTypeBot,
              );
            }
            break;
        }
      } catch (e) {
        logger.v("YBDGameRoomGetLogic error: $e");
      }
    }));
  }
  void initStateLWEG3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        YBDGameRoomSocketApi.getInstance().enterRoom(type: EnterGameRoomType.Watch);
        logger.v('room page didChangeAppLifecycleState resumend subscribe roomId');
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
    }
  }
  void didChangeAppLifecycleStateThFxEoyelive(AppLifecycleState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // YBDGameSocketApiUtil.pop();
    YBDGameLog.v("YBDGameRoomPageState dispose");
    Wakelock.disable();
    Get.delete<YBDGameRoomGetLogic>();
    super.dispose();
  }
}
