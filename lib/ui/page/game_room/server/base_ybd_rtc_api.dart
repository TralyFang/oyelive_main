import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_event_handler.dart';

enum RtcMicOperateType {
  /// 上麦
  up,
  /// 下麦
  down,
  ///自己操作完
  selfUp,
  selfDown,
}

mixin RtcMicOperate{

  /// take or down mic
  /// agora: change role
  /// zego: publish or play stream
  /// [streamId]: 如果为空 则表示操作自己
  /// [ismix]: 是否混流
  /// [micOperateType]: 自己上下麦成功时 使用selfUp /selfDown  此时streamIds 为空
  void takeMic({RtcMicOperateType? micOperateType, List<String>? streamIds});

  /// 禁麦
  /// 主播或者房主 禁麦其他主播
  /// [muted]: 禁麦/解除禁麦
  /// [muteUserId]: 被禁麦/解除禁麦的麦上用户id
  /// [mutedByAboardcast]: 是否被房主管理员禁麦
  void muteMic({required bool muted, required int muteUserId, bool mutedByAboardcast = true});

  /// 禁言（单独不外放某个人音频，不影响其他人接收）
  void muteRemoteAudioStream(int uid, bool muted);

  /// 全部禁言
  void muteAllRemoteAudioStreams(bool muted);
}

mixin RtcChannelOperate{
  /// join channel
  /// agora: join channel
  /// zego: login room
  void joinChannel({required String? token, required String? channelName});

  /// 切换频道
  void switchChannel({required String token,required String channelName}){}

  /// leave channel
  /// agora: leave channel
  /// zego: logout room
  void leaveChannel();
}

mixin RtcEventHandlerMixin {
  void setEventHandler(YBDBaseRtcEventHandler? handler);
}

/// 由[YBDAgoraRtcApi] [YBDZegoRtcApi] 实现对应的功能
abstract class BaseRtcApi with RtcMicOperate, RtcChannelOperate, RtcEventHandlerMixin {

}