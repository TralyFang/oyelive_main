import 'dart:async';


enum RtcConnectionState {
  /// 连接中
  connecting,
  /// 已连接
  connected,
  /// 连接失败, 断开连接 Disconnected
  failed,
}

enum RtcAudioState {
  /// 录音中, 如果是远程状态表示starting，decoding
  recording,
  /// 停止录音
  stopped,
  /// 录音失败
  failed,
}

class YBDRtcVolumeInfo {
  /// 音量[0, 255]
  int volume;
  /// 用户id，本地用户id为0
  int? uid;

  YBDRtcVolumeInfo(this.volume,this.uid);
}

typedef RtcEmptyCallback = void Function();
typedef RtcConnectionStateCallback = void Function(
    RtcConnectionState state);
typedef RtcErrorCallback = void Function(int errorCode);
typedef RtcUidWithChannelCallback = void Function(
    String channel, int uid);
typedef RtcClientRoleCallback = void Function(
    String oldRole, String newRole);
typedef RtcUidCallback = void Function(int uid);
typedef RtcAudioVolumeCallback = void Function(
    List<YBDRtcVolumeInfo> speakers);
typedef RtcAudioStateCallback = void Function(
    RtcAudioState state);
typedef RtcEnabledCallback = void Function(bool enabled);
typedef RtcUidWithMutedCallback = void Function(int uid, bool muted);



class YBDBaseRtcEventHandler {

  /// ✅ 网络连接状态：(connecting, connected, failed)
  RtcConnectionStateCallback? connectionStateChanged;
  /// ✅ 错误信息：or api调用异常：
  RtcErrorCallback? error;
  // /// 加入频道成功：(channel, uid)
  // RtcUidWithChannelCallback joinChannelSuccess;
  // /// 角色变更：(previous, current)
  // RtcClientRoleCallback clientRoleChanged;
  // /// 离开频道：仅表示当前用户离开频道的回调
  // RtcEmptyCallback leaveChannel;
  /// ✅ 用户加入：(uid)
  RtcUidCallback? userJoined;
  /// ✅ 用户离线：(uid)
  RtcUidCallback? userOffline;
  /// ✅ 声浪回调：(speakers: volume, uid) uid: 本地发声uid为0
  RtcAudioVolumeCallback? audioVolumeIndication;
  // /// 本地音频状态改变：(starting, stopped, failed)
  // RtcAudioStateCallback localAudioStateChanged;
  // /// 远程音频状态改变：(starting, stopped, failed)
  // RtcAudioStateCallback remoteAudioStateChanged;
  // /// 麦克风激活回调：(enabled)
  // RtcEnabledCallback microphoneEnabled;
  // /// 用户禁麦回调 (uid, meted)
  // RtcUidWithMutedCallback userMuteAudio;

  /// 通道媒体转发状态改变: [channelMediaRelayStateChanged]
}