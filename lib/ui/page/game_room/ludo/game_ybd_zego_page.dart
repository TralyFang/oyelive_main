import 'dart:async';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/game_ybd_kind_page.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/game_ybd_help_pop.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/game_ybd_ludo_setting_pop.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/method_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/widget/ExtraHittest/gesture_ybd_detector.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';

/// 即构小游戏匹配界面公共UI
///
/// 帮助中心+声音设置
class YBDGameZegoQuestionAndSound extends StatefulWidget {
  const YBDGameZegoQuestionAndSound({Key? key}) : super(key: key);

  @override
  _YBDGameZegoQuestionAndSoundState createState() =>
      _YBDGameZegoQuestionAndSoundState();
}

class _YBDGameZegoQuestionAndSoundState extends State<YBDGameZegoQuestionAndSound> {

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,
      right: 0,
      child: Padding(
        padding: EdgeInsets.only(right: 30.dp720 as double, top: 10.dpH1559 as double),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            YBDGameWidgetUtil.assetImage("game_question",
                isWebp: true,
                width: 47.dp720, height: 47.dp720, onTap: () {
                  YBDGameLog.v("game question tap");
                  try {
                    var logic = Get.find<YBDGameRoomGetLogic>();
                    var preRule = logic.state!.roomStatusNotify.body?.game?.image
                        ?.rule;
                    var rule = logic.state!.configDisplay?.rule;

                    if (rule != null) {
                      YBDGameHelpPop.show(
                          context,
                          rule.title ?? preRule?.title,
                          rule.body ?? preRule?.body);
                    }
                  } catch (e) {
                    YBDGameLog.v(
                        "game question tap find YBDGameRoomGetLogic error: $e");
                  }
                }),
            SizedBox(width: 11.dp720),
            GetBuilder<YBDGameRoomGetLogic>(
                id: "zegoMG.sound",
                builder: (logic) {
              return YBDGameWidgetUtil.assetImage(
                  logic.state!.isOpenZegoMGSound
                      ? "game_music_on"
                      : "game_music_off", // game_music_off 需要设置静音的图标
                  isWebp: true,
                  width: 47.dp720,
                  height: 47.dp720, onTap: () {
                YBDGameLog.v("game music tap");
                logic.state!.isOpenZegoMGSound = !logic.state!.isOpenZegoMGSound;
                YBDGameUtils.getInstance()!
                    .notifyAppCommonSelfSound(logic.state!.isOpenZegoMGSound);
                logic.update(["zegoMG.sound"]);
              });
            }),
          ],
        ),
      ),
    );
  }
  void buildJA5Gfoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDGameZegoMatchUI {

  static Widget loadingToMatch(Widget matchWiew) {
    return GetBuilder<YBDGameRoomGetLogic>(
        id: "state.gamePlaying",
        builder: (logic) {
          var gamePlaying = logic.state!.gamePlaying || !logic.state!.gameInit;
          if (logic.state!.zegoMGLoading) {
            gamePlaying = true; // 游戏还在加载中，不能有匹配界面
          }
          return Offstage(
            offstage: gamePlaying,
            child: matchWiew,
          );
        });
  }

  static Widget gameChooseBetSetting(BuildContext context,
      GameKindType? gameType) {
    return Container( // 保持间距
      height: 50.dp720,
      // color: Colors.red.withOpacity(0.3),
      // padding: EdgeInsets.symmetric(vertical: 5.dp720, horizontal: 10.dp720),
      margin: EdgeInsets.only(bottom: 66.dpH1559 as double, top: 42.dpH1559 as double),
      child: YBDGameZegoMatchUI.gameChooseBet(context, gameType),
    );
  }

  /// 游戏下注额
  static Widget gameChooseBet(BuildContext context, GameKindType? gameType) {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      var gameConfig = logic.gameConfig;

      return YBDGestureDetectorHitTestWithoutSizeLimit(
        // debugHitTestAreaColor: Colors.white.withOpacity(0.7),
        extraHitTestArea: EdgeInsets.all(10.dp720.toDouble()),
        onTap: () {
          gameSettingPop(context, gameType);
        },
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            YBDGameWidgetUtil.textAlegreyaBlack(
              "choose_bet".i18n + ":",
              color: Colors.white,
              fontSize: 36.sp720,
            ),
            SizedBox(width: 10.dp720),
            YBDGameWidgetUtil.assetImage(
              YBDGameResource.currencyIconType(gameConfig.currency),
              width: 33.dp720,
              height: 33.dp720,
            ),
            SizedBox(width: 4.dp720),
            YBDGameWidgetUtil.textAlegreyaBlack(
              "${gameConfig.amount}".currencyIntUnit(),
              color: Colors.white, //Color(0xffffe862),
              fontSize: 38.sp720,
            ),
            Padding(
              padding: EdgeInsets.only(left: 16.dp720 as double),
              child: YBDGameWidgetUtil.assetImage(
                "game_set_white",
                width: 27.dp720,
                height: 27.dp720,
              ),
            ),
          ],
        ),
      );
    });
  }

  static gameSettingPop(BuildContext context, GameKindType? gameType) {
    try {
      var logic = Get.find<YBDGameRoomGetLogic>();
      YBDGameLog.v('clicked setting btn captain: ${logic
          .currentUserSeat()
          .isCaptain}');
      if (logic
          .currentUserSeat()
          .isCaptain) {
        /// 只限队长可设置
        YBDGameLudoSettingPop.show(
            context,
            gameType,
            logic.state?.roomStatusNotify.body?.game?.optionalMode?.removeNulls() ?? []);
      } else {
        // only_captain_choose_mode
        YBDToastUtil.toast("only_captain_choose_mode".i18n);
      }
    } catch (e) {
      YBDGameLog.v('clicked setting btn captain: error: $e');
    }
  }
}
