
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/text_ybd_with_stroke.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_operator_center/game_ybd_header_operator_center.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/dialog/confirm_ybd_dialog.dart';
import 'package:oyelive_main/ui/widget/inner_shadow/flutter_ybd_inset_box_shadow.dart' as i;
import '../../../../../common/analytics/analytics_ybd_util.dart';

/// 游戏结束的结算列表
class YBDGameSettlePop extends StatefulWidget {
  static bool _isShowPop = false;
  static show(BuildContext context, List<YBDGameSettlement> settlementList, bool isPlayer, bool isBotGame) {
    List<YBDGameSettlement> cpList = List.from(settlementList);
    if (cpList.length <= 0) {
      return;
    }
    YBDGameLog.v('YBDGameSettlePop show: $_isShowPop');
    if (_isShowPop) return;
    _isShowPop = true;

    // 修复点击再来一局，直接退出游戏房的bug
    SmartDialog.show(
        alignment: Alignment.center,
        tag: 'YBDGameSettlePop',
        clickMaskDismiss: false,
        builder: (BuildContext context) => YBDGameSettlePop(
          isPlayer: isPlayer,
          settlementList: cpList,
          isBotGame: isBotGame,
          gameVoidCallback: (popContext) {
            _isShowPop = false;
            logger.v('YBDGameSettlePop gameVoidCallback pop');
            SmartDialog.dismiss(tag: 'YBDGameSettlePop');
            logger.v('22.10.11--isBotGame:$isBotGame');
            YBDRtcHelper.getInstance().matchType = '';
          },
        ),
        onDismiss: () {
          logger.v('YBDGameSettlePop onDismiss pop');
          _isShowPop = false;
        });
    return;
  }

  final GameVoidCallback? gameVoidCallback;
  final List<YBDGameSettlement>? settlementList;
  final bool isPlayer; // 是否是玩家
  final bool isBotGame; // 是否是机器人局

  YBDGameSettlePop({
    Key? key,
    this.gameVoidCallback,
    this.settlementList,
    this.isPlayer = false,
    this.isBotGame = false,
  }) : super(key: key);

  @override
  _YBDGameSettlePopState createState() => _YBDGameSettlePopState();
}

class _YBDGameSettlePopState extends State<YBDGameSettlePop> {
  int _countDown = 8;
  Timer? _timer;

  @override
  void initState() {
    super.initState();

    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      // 5s 自动关闭
      if (!mounted) {
        return;
      }

      _countDown -= 1;
      setState(() {});

      if (_countDown < 1) {
        widget.gameVoidCallback?.call(context);
        botGameClose();
      }
    });
  }
  void initStateeed9Qoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _timer?.cancel();
    YBDGameSettlePop._isShowPop = false;
    super.dispose();
  }
  void disposeDcgQdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void botGameClose() {
    print('22.10.11--isBotGame:${widget.isBotGame}--isPlayer:${widget.isPlayer}');
    if (widget.isBotGame) {
      YBDGameHeaderOperatorCenter.exitRoom();
      if (!widget.isPlayer) {
        Future.delayed(Duration(seconds: 2), () {
          YBDToastUtil.toast('The game is over');
        });
      }
    }
  }
  void botGameClosejdlbEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(width: 755.dp720, height: 140.dp720),
            Container(
              width: 650.dp720,
              height: 700.dp720,
              decoration: BoxDecoration(
                border: Border.all(width: 3.dp750 as double, style: BorderStyle.solid, color: Colors.yellow),
                borderRadius: BorderRadius.all(Radius.circular(33.dp750 as double)),
                gradient: LinearGradient(
                  colors: [Color(0xff51D9FF), Color(0xff1C58DB)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
              child: Column(
                children: [
                  SizedBox(height: 120.dp720),
                  // 列表
                  Container(
                    height: (95 * 4 + 33).dp720,
                    child: ListView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      itemBuilder: (context, index) {
                        return YBDGameSettleItem(settlementData: widget.settlementList![index]);
                      },
                      itemCount: widget.settlementList!.length,
                    ),
                  ),
                  SizedBox(height: 45.dp720),
                  // 底部按钮 关闭 再一次
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      YBDDelayGestureDetector(
                        onTap: () {
                          widget.gameVoidCallback?.call(context);
                          botGameClose();
                        },
                        child: Container(
                            height: 72.dp750,
                            width: 275.dp750,
                            decoration: i.BoxDecoration(
                                color: Color(0xffe7e7e7).withOpacity(0.7),
                                borderRadius: BorderRadius.all(Radius.circular(32.dp750 as double)),
                                boxShadow: [
                                  i.BoxShadow(
                                      color: Colors.white.withOpacity(0.5),
                                      offset: Offset(0, 1.px as double),
                                      blurRadius: 2.px as double,
                                      inset: true),
                                  i.BoxShadow(
                                      color: Color(0xff818181),
                                      offset: Offset(0, -1.px as double),
                                      blurRadius: 2.px as double,
                                      inset: true),
                                ]),
                            child: Center(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  YBDTextWithStroke(
                                    text: 'close'.i18n,
                                    fontSize: 32.sp750 as double,
                                    fontFamily: 'baloo',
                                    strokeColor: Color(0xff4b4b4b),
                                  ),
                                  YBDTextWithStroke(
                                    text: '($_countDown)',
                                    fontSize: 32.sp750 as double,
                                    fontFamily: 'baloo',
                                    strokeColor: Color(0xff4b4b4b),
                                  ),
                                ],
                              ),
                            )),
                      ),
                      if (widget.isPlayer)
                        YBDDelayGestureDetector(
                          onTap: () {
                            logger.v('clicked again btn');
                            widget.gameVoidCallback?.call(context);
                            // v2.7.1 真人房正常进入准备状态   机器人房退出重新匹配
                            if (widget.isBotGame) {
                              YBDGameHeaderOperatorCenter.exitRoom();
                              YBDRtcHelper.getInstance().setGameSubType('ludo');
                              Future.delayed(Duration(seconds: 2), () {
                                YBDGameSocketApiUtil.quickEnterRoom(type: EnterGameRoomType.QuickGame);
                              });
                            } else {
                              // 再来一次进入准备状态
                              Get.find<YBDGameRoomGetLogic>().gameEventHandler(GameJoinStatus.ready);
                              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                                YBDEventName.CLICK_EVENT,
                                location: YBDLocationName.GAME_ROOM_PAGE,
                                itemName: 'play again',
                              ));
                            }
                          },
                          child: Container(
                            height: 72.dp750,
                            width: 275.dp750,
                            margin: EdgeInsets.only(left: 26.dp720 as double),
                            decoration: i.BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xffFFCE5B),
                                  Color(0xffFFBC04),
                                ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                                borderRadius: BorderRadius.all(Radius.circular(40.dp750 as double)),
                                boxShadow: [
                                  i.BoxShadow(
                                      color: Colors.white.withOpacity(0.5),
                                      offset: Offset(0, 1.px as double),
                                      blurRadius: 2.px as double,
                                      inset: true),
                                  i.BoxShadow(
                                      color: Color(0xff7E5A03),
                                      offset: Offset(0, -1.px as double),
                                      blurRadius: 2.px as double,
                                      inset: true),
                                ]),
                            child: Center(
                              child: YBDTextWithStroke(
                                text: 'again'.i18n,
                                fontSize: 32.sp750 as double,
                                fontFamily: 'baloo',
                                strokeColor: Color(0xff786000),
                              ),
                            ),
                          ),
                        )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        Positioned(
            top: 30.dp720,
            child: YBDGameWidgetUtil.assetImage('ludo_settle_banner', width: 755.dp720, height: 190.dp720, isWebp: true)),
      ],
    );
  }
  void buildgtpgaoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDGameSettleItem extends StatelessWidget {
  final YBDGameSettlement? settlementData;

  const YBDGameSettleItem({Key? key, this.settlementData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 95.dp720,
      margin: EdgeInsets.only(
        left: 60.dp720 as double,
        bottom: 11.dp720 as double,
        right: 57.dp720 as double,
      ),
      padding: EdgeInsets.only(right: 36.dp720 as double),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.19),
        borderRadius: BorderRadius.circular(15.dp720 as double),
      ),
      child: Row(
        children: [
          Container(
            width: 84.dp720,
            padding: EdgeInsets.only(right: 30.dp720 as double),
            alignment: Alignment.centerRight,
            child: YBDGameWidgetUtil.textBalooRegular(
              settlementData!.index,
              fontSize: 42.sp720,
            ),
          ),
          YBDGameWidgetUtil.avatar(
            url: settlementData!.userAvatar,
            addSize: true,
            width: 66.dp720,
            radius: 33.dp720,
            sideBorder: true,
          ),
          SizedBox(width: 25.dp720),
          Expanded(
            child: YBDGameWidgetUtil.textSingleRegular(
              settlementData!.nickName,
              fontSize: 28.sp720,
            ),
          ),
          Row(
            children: [
              YBDGameWidgetUtil.assetImage(
                YBDGameResource.currencyIconAsset(settlementData!.currency),
                width: 37.dp720,
                height: 37.dp720,
              ),
              SizedBox(width: 8.dp720),
              settlementData!.win
                  ? YBDGameWidgetUtil.textBalooRegular(
                      '${settlementData!.winNumber}'.currencyIntUnit(point: 2),
                      color: Color(0xffffe862),
                      fontSize: 36.sp720,
                    )
                  : YBDGameWidgetUtil.textBalooRegular(settlementData!.result ?? 'lost'.i18n, fontSize: 36.sp720)
            ],
          )
        ],
      ),
    );
  }
  void buildyGHhFoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
