import 'dart:async';


import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/method_ybd_helper.dart';

class YBDPlatformGameView extends StatefulWidget {
  /**
   * var params = {
      // "roomId": "",
      // "userId":"",
      // "appCode":"", // appcode
      // "mgId":"", // 游戏id
      // "appKey": "", // appkey
      // "appId":"", // appId
      // "view_game_top" // 游戏内容上边距离
      // "view_game_bottom"// 游戏内容下边距离
      };
   * */
  /// 必须包含以上字段
  Map<String, dynamic>? params;

  YBDPlatformGameView({Key? key, this.params}) : super(key: key);

  @override
  _YBDPlatformGameViewState createState() => _YBDPlatformGameViewState();
}

class _YBDPlatformGameViewState extends State<YBDPlatformGameView> {

  final String viewType = 'zego_sudmpg_plugin/gameView';

  @override
  Widget build(BuildContext context) {
    return platformViewLink();
  }
  void buildOFmqjoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget platformView() {
    logger.v(
        "platformView Platform.operatingSystem: ${Platform.operatingSystem}");
    if (Platform.isAndroid) {
      return AndroidView(
        viewType: viewType,
        creationParams: widget.params,
        onPlatformViewCreated: onPlatformViewCreated,
        creationParamsCodec: const StandardMessageCodec(),
        // hitTestBehavior: PlatformViewHitTestBehavior.transparent,
      );
    } else if (Platform.isIOS) {
      return UiKitView(
        viewType: viewType,
        creationParams: widget.params,
        onPlatformViewCreated: onPlatformViewCreated,
        creationParamsCodec: const StandardMessageCodec(),
        // hitTestBehavior: PlatformViewHitTestBehavior.opaque,
      );
    }
    return Container();
  }
  void platformViewuMTaNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  


  Widget platformViewLink() {
    /// 混合集成模式
    logger.v(
        "platformViewLink Platform.operatingSystem: ${Platform.operatingSystem}");
    if (Platform.isAndroid) {
      return PlatformViewLink(
        viewType: viewType,
        surfaceFactory:
            (BuildContext context, PlatformViewController controller) {
          return AndroidViewSurface(
            controller: controller as AndroidViewController,
            gestureRecognizers: const <Factory<OneSequenceGestureRecognizer>>{},
            hitTestBehavior: PlatformViewHitTestBehavior.opaque,
          );
        },
        onCreatePlatformView: (PlatformViewCreationParams params) {
          return PlatformViewsService.initSurfaceAndroidView(
            id: params.id,
            viewType: viewType,
            layoutDirection: TextDirection.ltr,
            creationParams: widget.params,
            creationParamsCodec: const StandardMessageCodec(),
            onFocus: () {
              params.onFocusChanged(true);
            },
          )
            ..addOnPlatformViewCreatedListener(params.onPlatformViewCreated)
            ..addOnPlatformViewCreatedListener((id) {
              onPlatformViewCreated(params.id);
            })
            ..create();
        },
      );
    }else if (Platform.isIOS) {
      return UiKitView(
        viewType: viewType,
        creationParams: widget.params,
        onPlatformViewCreated: onPlatformViewCreated,
        creationParamsCodec: const StandardMessageCodec(),
        // hitTestBehavior: PlatformViewHitTestBehavior.opaque,
      );
    }
    return Container();
  }
  void platformViewLinkNGBY9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  onPlatformViewCreated(int id) {
    logger.v("platformView onPlatformViewCreated id: $id");
    YBDGameUtils.getInstance()!.initChannelHandler();
  }

  @override
  void dispose() {
    YBDGameUtils.getInstance()!.disposeChannel();
    super.dispose();
  }
  void disposeDmKR4oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
