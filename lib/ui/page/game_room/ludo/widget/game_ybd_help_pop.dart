import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';

/// 即构游戏帮助中心弹窗
class YBDGameHelpPop extends StatefulWidget {

  static show(BuildContext context, String? titleImgUrl, String? bodyImgUrl) {
    YBDEasyPopup.show(
        context,
        YBDGameHelpPop(
          titleImgUrl: titleImgUrl,
          bodyImgUrl: bodyImgUrl,
          gameVoidCallback: (popContext) {
            YBDEasyPopup.pop(popContext);
          },
        ),
        position: EasyPopupPosition.center,
        duration: Duration(milliseconds: 50),
        outsideTouchCancelable: true);
  }

  GameVoidCallback? gameVoidCallback;
  String? titleImgUrl;
  String? bodyImgUrl;

  YBDGameHelpPop({this.gameVoidCallback, this.titleImgUrl, this.bodyImgUrl});



  @override
  _YBDGameHelpPopState createState() => _YBDGameHelpPopState();
}

class _YBDGameHelpPopState extends State<YBDGameHelpPop> {


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(16.dp750 as double)),
      width: 540.dp750,
      height: 564.dp750,
      padding: EdgeInsets.all(5.dp720 as double),
      child: Column(
        children: [
          _setupTitle(),
          Expanded(
              child: Container(
                padding: EdgeInsets.only(top: 20.dp750 as double, left: 15.dp720 as double, right: 15.dp720 as double, bottom: 15.dp720 as double),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16.dp750 as double),
                  child: SingleChildScrollView(
                    child: YBDGameWidgetUtil.networkImage(
                        url: widget.bodyImgUrl,
                        placeholder: CupertinoActivityIndicator()),
                  ),
                ),
              )
          ),
        ],
      ),
    );
  }
  void buildMvJ10oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _setupTitle() {
    return Container(
      height: 70.dp750,
      child: Stack(
        children: [
          YBDGameWidgetUtil.networkImage(
              url: widget.titleImgUrl,
              placeholder: CupertinoActivityIndicator()),
          Positioned(
            right: 29.dp720,
              top: 25.dp720,
              child: YBDGameWidgetUtil.assetImage(
                  "game_close_white",
                  width: 20.dp750,
                  height: 20.dp750,
                  minTapSize: 70.dp750, onTap: () {
                YBDGameLog.v("YBDGameSoundSettingPop tap close");
                widget.gameVoidCallback?.call(context);
              })),
          // 以下这种方式点击事件有一定的滞后型
          // Align(
          //   alignment: Alignment.centerRight,
          //   child: Padding(
          //     padding: EdgeInsets.only(right: 29.dp750),
          //     child: YBDGameWidgetUtil.assetImage(
          //         "game_close_white",
          //         width: 20.dp750,
          //         height: 20.dp750,
          //         minTapSize: 70.dp750, onTap: () {
          //       YBDGameLog.v("YBDGameSoundSettingPop tap close");
          //       widget.gameVoidCallback?.call(context);
          //     }),
          //   ),
          // )
        ],
      ),
    );
  }
}
