
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/text_ybd_with_stroke.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_config_display_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/room_ybd_status_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/inner_ybd_shadow.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';
import 'package:oyelive_main/ui/widget/inner_shadow/flutter_ybd_inset_box_shadow.dart' as i;

class YBDGameLudoSettingPop extends StatefulWidget {
  static show(BuildContext context, GameKindType? gameType, List<YBDRoomStatusGameMode> gameModes) {
    YBDEasyPopup.show(
        context,
        YBDGameLudoSettingPop(
          gameType: gameType,
          gameModes: gameModes,
          gameVoidCallback: (popContext) {
            YBDEasyPopup.pop(popContext);
          },
        ),
        position: EasyPopupPosition.center,
        duration: Duration(milliseconds: 50),
        outsideTouchCancelable: true);
  }

  GameVoidCallback? gameVoidCallback;
  final GameKindType? gameType;
  final List<YBDRoomStatusGameMode>? gameModes;

  YBDGameLudoSettingPop({
    Key? key,
    this.gameType,
    this.gameModes,
    this.gameVoidCallback,
  }) : super(key: key);

  @override
  _YBDGameLudoSettingPopState createState() => _YBDGameLudoSettingPopState();
}

class _YBDGameLudoSettingPopState extends State<YBDGameLudoSettingPop> {
  GameModelType? _model; // 当前模式
  GameCurrencyType? _currency; // 当前币种
  int? _amount; // 当前门票
  List<int>? _ticketLevel; // 门票档位
  late List<YBDRoomStatusGameModeCurrency?> _gameCurrencys; // 模式对应下的币种
  YBDGameConfigDisplayData? _configDisplay; // /// 外观配置信息
  @override
  void initState() {
    super.initState();

    final gameConfig = Get.find<YBDGameRoomGetLogic>().gameConfig;
    _configDisplay = Get.find<YBDGameRoomGetLogic>().state!.configDisplay;
    _model = gameConfig.mode;
    _currency = gameConfig.currency;
    _amount = gameConfig.amount;
    _ticketLevel = gameConfig.ticketLevel ?? [];
    updateData();
  }
  void initStatekxWtvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 更新数据
  updateData() {
    try {
      var gameModel = widget.gameModes!.firstWhere((element) => (element.mode == _model.value));
      _gameCurrencys = gameModel.gameCurrency ?? [];
      _ticketLevel = gameModel.gameTicketLevel(_currency.value);
      if (_ticketLevel!.length == 0) {
        // 不存在币种, 那就默认选中第一个
        var currencyModel = _gameCurrencys.first!;
        _currency = GameCurrencyTypeExt.type(currencyModel.currency!);
        _ticketLevel = currencyModel.ticketLevel;
      }
      setState(() {});
    } catch (e) {
      YBDGameLog.v("model setting error: $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return ludoSettingContainer();
  }
  void buildTaZRkoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// ludo游戏配置
  Widget ludoSettingContainer() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          width: 650.dp720,
          height: 590.dp720,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(28.dp720 as double)),
            border: Border.all(width: 2.dp720 as double, style: BorderStyle.solid, color: Color(0xffFFD94E)),
            gradient: LinearGradient(
                colors: [Color(0xff51D9FF), Color(0xff1C58DB)],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter),
          ),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // 顶部标题
                  Container(
                      width: 650.dp720,
                      height: 80.dp720,
                      decoration: BoxDecoration(
                          color: YBDHexColor(_configDisplay?.setUp?.titleBgC ?? "#3F8CE2"),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(28.dp720 as double), topRight: Radius.circular(28.dp720 as double))),
                      child: Center(
                        child: YBDGameWidgetUtil.textBalooRegular("set_up".i18n, fontSize: 38.sp720),
                      )),
                  SizedBox(height: 50.dp720),
                  // 中间内容区域
                  Container(
                    width: 604.dp720,
                    height: 290.dp720,
                    decoration: BoxDecoration(
                      color: YBDHexColor(_configDisplay?.setUp?.bodyBgc ?? "#0080FF").withOpacity(0.2),
                      borderRadius: BorderRadius.circular(24.dp720 as double),
                    ),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              titleRightWidget("model".i18n + " :"),
                              SizedBox(width: 26.dp720),
                              ...modelsRow(),
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              titleRightWidget("currency".i18n + " :"),
                              SizedBox(width: 26.dp720),
                              ...currencyTypesRow(),
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              titleRightWidget("choose_bet".i18n + " :"),
                              SizedBox(width: 22.dp720),
                              YBDGameCoinValuesChange(
                                configDisplay: _configDisplay,
                                currencyType: _currency,
                                currentValue: _amount,
                                values: _ticketLevel,
                                valueDidChangeCallback: (value) {
                                  YBDGameLog.v("ludo bet value $value");
                                  _amount = value;
                                },
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 50.dp720),
                  // create 按钮
                  _newCreateBtn(),
                ],
              ),
              // Positioned(
              //   right: 29.dp720,
              //   top: 24.dp720,
              //   child: YBDGameWidgetUtil.assetImage(
              //     "game_close2_white",
              //     width: 23.dp720,
              //     height: 23.dp720,
              //     minTapSize: 60.dp720,
              //     onTap: () {
              //       YBDGameLog.v("game ludo set tap close");
              //       widget.gameVoidCallback?.call(context);
              //     },
              //   ),
              // )
            ],
          ),
        ),
        SizedBox(height: 53.dp720),
        YBDGameWidgetUtil.assetImage(
          'icon_close_blue',
          width: 58.dp720,
          height: 58.dp720,
          onTap: () {
            YBDGameLog.v("game ludo set tap close");
            widget.gameVoidCallback?.call(context);
          },
        ),
      ],
    );
  }
  void ludoSettingContainerRQL4Loyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _newCreateBtn() {
    return GestureDetector(
      onTap: () {
        YBDGameLog.v("game ludo set tap ok");
        Get.find<YBDGameRoomGetLogic>().updateGameConfig(_amount, _currency, _model);
        widget.gameVoidCallback?.call(context);
      },
      child: Container(
        height: 80.dp720,
        width: 330.dp720,
        decoration: i.BoxDecoration(
            gradient: LinearGradient(colors: [
              Color(0xffFFCE5B),
              Color(0xffFFBC04),
            ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
            borderRadius: BorderRadius.all(Radius.circular(40.dp720 as double)),
            boxShadow: [
              i.BoxShadow(color: Colors.white.withOpacity(0.5), offset: Offset(0, 1.px as double), blurRadius: 2.px as double, inset: true),
              i.BoxShadow(color: Color(0xff7E5A03), offset: Offset(0, -1.px as double), blurRadius: 2.px as double, inset: true),
            ]),
        child: Center(
          child: YBDTextWithStroke(
            text: 'create'.i18n,
            fontSize: 32.sp750 as double,
            fontFamily: 'baloo',
            strokeColor: Color(0xff786000),
          ),
        ),
      ),
    );
  }
  void _newCreateBtns1nt6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget shadowInnerCreate() {
    return Container(
      width: 250.dp720,
      height: 77.dp720,
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(16.dp720 as double)),
              boxShadow: [
                // 外阴影
                BoxShadow(
                  color: Colors.black.withOpacity(0.5),
                  blurRadius: 4.dp720 as double,
                  offset: Offset(0, 2.dp720 as double),
                ),
              ],
            ),
          ),
          YBDInnerShadow(
            shadows: [
              // 内阴影
              BoxShadow(
                color: YBDHexColor(_configDisplay?.setUp?.buttonInnerC ?? "#035E73").withOpacity(0.5),
                blurRadius: 10.dp720 as double,
                offset: Offset(0, 0),
              )
            ],
            child: YBDGameWidgetUtil.buttonTextBg("create".i18n,
                width: 250.dp720,
                height: 77.dp720 as double,
                color: Colors.white,
                fontSize: 40.sp720,
                bgName: "game_join_bg",
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(16.dp720 as double)),
                  color: YBDHexColor(_configDisplay?.setUp?.buttonBgC ?? "#70ADFF"),
                  boxShadow: [
                    // 外阴影会有切割效果，所以底部叠加了一层外阴影
                    BoxShadow(
                      color: Colors.black.withOpacity(0.0),
                      blurRadius: 4.dp720 as double,
                      offset: Offset(0, 2.dp720 as double),
                    ),
                  ],
                ), onTap: () {
              YBDGameLog.v("game ludo set tap ok");
              Get.find<YBDGameRoomGetLogic>().updateGameConfig(_amount, _currency, _model);
              widget.gameVoidCallback?.call(context);
            }),
          )
        ],
      ),
    );
  }
  void shadowInnerCreatesGuKioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 模式
  List<Widget> modelsRow() {
    List<Widget> modeWidgets = this.widget.gameModes!.map((element) {
      return YBDGameChooseText(
        configDisplay: _configDisplay,
        isSelected: this._model.value == element.mode,
        text: element.mode!.toFirstCapitalize(),
        onTap: () {
          this._model = GameModelTypeExt.type(element.mode!);
          updateData();
        },
      );
    }).toList();

    /// 超过两个只显示前两个
    if (modeWidgets.length > 2) {
      return modeWidgets.getRange(0, 2).toList();
    }
    return modeWidgets;
  }
  void modelsRowN0buToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 币种
  List<Widget> currencyTypesRow() {
    var currencyWidgets = _gameCurrencys.map((element) {
      return YBDGameChooseText(
        configDisplay: _configDisplay,
        isSelected: this._currency.value == element!.currency,
        text: element.currency!.toFirstCapitalize(), //"golds".i18n,
        onTap: () {
          this._currency = GameCurrencyTypeExt.type(element.currency!);
          updateData();
        },
      );
    }).toList();

    /// 超过两个只显示前两个
    if (currencyWidgets.length > 2) {
      return currencyWidgets.getRange(0, 2).toList();
    }
    return currencyWidgets;
  }
  void currencyTypesRowU1Knqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 有对齐的标题
  Widget titleRightWidget(String text) {
    return Container(
      width: 190.dp720,
      padding: EdgeInsets.only(left: 8.dp720 as double),
      child: Align(
        alignment: Alignment.centerRight,
        child: YBDGameWidgetUtil.textBalooRegular(text, color: YBDHexColor("#FFFFFF"), fontSize: 32.sp720),
      ),
    );
  }
}

/// 标题选项
class YBDGameChooseText extends StatelessWidget {
  final bool? isSelected;
  final String? text;
  final GestureTapCallback? onTap;
  final YBDGameConfigDisplayData? configDisplay; // 外观配置信息

  const YBDGameChooseText({
    Key? key,
    this.isSelected,
    this.text,
    this.onTap,
    this.configDisplay,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onTap,
      child: Container(
        width: 165.dp720,
        // color: Colors.red.withOpacity(0.2),
        child: Row(
          children: [
            // SizedBox(width: 49.dp720),
            YBDGameWidgetUtil.assetImage(
              this.isSelected! ? configDisplay?.setUp?.selectedImg ?? "game_choose_sel" : "game_choose_nor",
              width: 30.dp750,
              height: 30.dp750,
              placeholder: CupertinoActivityIndicator(),
            ),
            SizedBox(width: 9.dp750),
            YBDGameWidgetUtil.textBalooRegular(
              this.text,
              color: this.isSelected! ? YBDHexColor(configDisplay?.setUp?.selectedC ?? "#0846AC") : YBDHexColor("#FFFFFF"),
              fontSize: 36.sp720,
            ),
          ],
        ),
      ),
    );
  }
  void buildSYOUIoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 多个档位之前切换
class YBDGameCoinValuesChange extends StatefulWidget {
  final int? currentValue; // 当前值
  final List<int?>? values; // 可切换的值
  final GameCurrencyType? currencyType; // 币种类型
  final YBDGameConfigDisplayData? configDisplay; // 外观配置信息

  /// 值已经发生改变了
  void Function(int? value) valueDidChangeCallback;

  YBDGameCoinValuesChange({
    Key? key,
    required this.currentValue,
    required this.currencyType,
    required this.values,
    required this.valueDidChangeCallback,
    this.configDisplay,
  }) : super(key: key);

  @override
  _YBDGameCoinValuesChangeState createState() => _YBDGameCoinValuesChangeState();
}

class _YBDGameCoinValuesChangeState extends State<YBDGameCoinValuesChange> {
  int? _currentValue = 0; // 当前值
  int _index = 0; // 默认索引

  @override
  void initState() {
    super.initState();

    /// 初始化值
    _index = widget.values!.indexOf(widget.currentValue);
    if (_index >= 0 && _index < widget.values!.length) {
      _currentValue = widget.currentValue ?? widget.values![_index];
      widget.valueDidChangeCallback.call(_currentValue);
    } else {
      // 没有金币等位
      _currentValue = widget.currentValue;
    }
  }
  void initStatej3UIDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  updateValue() {
    int maxIndex = widget.values!.length - 1;
    if (_index > maxIndex) _index = maxIndex;
    if (_index < 0) _index = 0;
    _currentValue = widget.values![_index];
    widget.valueDidChangeCallback(_currentValue);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          /// 减
          YBDGameWidgetUtil.assetImage(
              _currentValue! <= widget.values!.first!
                  ? "game_reduce_grey"
                  : widget.configDisplay?.setUp?.reduceTicketImg ?? "game_reduce_blue",
              width: 43.dp720,
              height: 42.dp720,
              placeholder: CupertinoActivityIndicator(), onTap: () {
            YBDGameLog.v("game coin tap reduce");
            _index -= 1;
            updateValue();
          }),
          SizedBox(width: 8.dp720),

          /// 金币显示
          Container(
            // width: 167.dp720,
            height: 51.dp720,
            constraints: BoxConstraints(maxWidth: 250.dp720 as double, minWidth: 167.dp720 as double),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.dp720 as double),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                YBDGameWidgetUtil.assetImage(YBDGameResource.currencyIconType(widget.currencyType),
                    width: 26.dp720, height: 25.dp720),
                SizedBox(width: 6.dp720),
                ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 200.dp720 as double),
                  child: YBDGameWidgetUtil.textBalooRegular(
                    "$_currentValue".currencyIntUnit(),
                    fontSize: 35.sp720,
                    color: YBDHexColor("#0846AC"),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 8.dp720),

          /// 加
          Container(
            // color: Colors.red,
            child: YBDGameWidgetUtil.assetImage(
              _currentValue! >= widget.values!.last!
                  ? "game_add_grey"
                  : widget.configDisplay?.setUp?.addTicketImg ?? "game_add_blue",
              width: 43.dp720,
              height: 42.dp720,
              placeholder: CupertinoActivityIndicator(),
              onTap: () {
                YBDGameLog.v("game coin tap add");
                _index += 1;
                updateValue();
              },
            ),
          ),
        ],
      ),
    );
  }
  void buildGSdDcoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 币种追加
class YBDGameCoinAddWidget extends StatefulWidget {
  final int? currentValue;
  final int maxValue;
  final int minValue;
  final int addValue; // 增量
  //TODO: 添加币种

  /// 值已经发生改变了
  void Function(int? value)? valueDidChangeCallback;

  YBDGameCoinAddWidget({
    Key? key,
    this.currentValue,
    required this.maxValue,
    required this.minValue,
    required this.addValue,
    this.valueDidChangeCallback,
  }) : super(key: key);

  @override
  _YBDGameCoinAddWidgetState createState() => _YBDGameCoinAddWidgetState();
}

class _YBDGameCoinAddWidgetState extends State<YBDGameCoinAddWidget> {
  late int _currentValue; // 当前值

  @override
  void initState() {
    super.initState();

    /// 初始化值
    _currentValue = widget.currentValue ?? widget.minValue;
    widget.valueDidChangeCallback?.call(_currentValue);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          YBDGameWidgetUtil.assetImage(_currentValue == widget.minValue ? "game_reduce_grey" : "game_reduce_blue",
              width: 43.dp720, height: 42.dp720, onTap: () {
            YBDGameLog.v("game coin tap reduce");
            _currentValue -= widget.addValue;

            if (_currentValue< widget.minValue) {
              _currentValue = widget.minValue;
            }

            widget.valueDidChangeCallback!(_currentValue);
            setState(() {});
          }),
          SizedBox(width: 8.dp720),
          Container(
            width: 167.dp720,
            height: 51.dp720,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.dp720 as double),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                YBDGameWidgetUtil.assetImage("game_icon_gold", width: 26.dp720, height: 25.dp720),
                SizedBox(width: 6.dp720),
                YBDGameWidgetUtil.textAlegreyaBlack(
                  "$_currentValue",
                  fontSize: 35.sp720,
                  color: Color(0xff0846ac),
                ),
              ],
            ),
          ),
          SizedBox(width: 8.dp720),
          Container(
            // color: Colors.red,
            child: YBDGameWidgetUtil.assetImage(
              _currentValue == widget.maxValue ? "game_add_grey" : "game_add_blue",
              width: 43.dp720,
              height: 42.dp720,
              onTap: () {
                YBDGameLog.v("game coin tap add");
                _currentValue += widget.addValue;

                if (_currentValue> widget.maxValue) {
                  _currentValue = widget.maxValue;
                }

                widget.valueDidChangeCallback!(_currentValue);
                setState(() {});
              },
            ),
          ),
        ],
      ),
    );
  }
  void build7l1cxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
