import 'dart:async';


import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/zego_ybd_code_entity.dart';

/// code过期需要更新回调
typedef GameUpdateCodeCallback = Future<String> Function();

/// 游戏加载完成
typedef GameStartedCallback = void Function(bool isStarted);

/// 游戏开始异常
typedef GameStartExceptionCallback = void Function(String code);

class YBDGameUtils {
  /****************Native层->flutter层******************/
  static const String MG_EXPIRE_APP_CODE = "mg_expire_app_code"; // 原生appCode过期回调，需要通知flutter获取最新code，去更新游戏code
  static const String MG_JOIN_USERID = "mg_join_userId"; // 加入游戏的用户
  static const String MG_READY_USERID = "mg_ready_userId"; // 准备游戏的用户
  static const String MG_STATE_STARTED = "mg_state_started"; // 游戏已经加载完成
  static const String MG_STATE_START_EXCEPTION = "mg_state_start_exception"; // 游戏开始异常需要上报服务器

  /****************flutter层->Native层******************/
  static const String MG_SELF_IN = "mg_self_in"; // 加入游戏
  static const String MG_SELF_READY = "mg_self_ready"; // 准备游戏
  static const String MG_SELF_PLAYING = "mg_self_playing"; // 开始游戏
  static const String MG_SELF_KICK = "mg_self_kick"; // 踢人
  static const String MG_SELF_CAPTAIN = "mg_self_captain"; // 设置队长
  static const String MG_SELF_END = "mg_self_end"; // 结束游戏

  static const String MG_DISPOSE = "mg_dispose"; // 退出游戏了

  static const String MG_SELF_SOUND = "mg_self_sound"; // 打开游戏声音
  static const String MG_SELF_MUSIC = "mg_self_music"; // 打开游戏背景声音
  static const String MG_SELF_VIBRATE = "mg_self_vibrate"; // 打开游戏震动声音
  static const String MG_STATE_PLAYING = "mg_state_playing"; // 游戏是否开始

  GameUpdateCodeCallback? _updateCodeCallback;
  GameStartedCallback? _gameStartedCallback;
  GameStartExceptionCallback? _gameStartExceptionCallback;

  MethodChannel? _methodChannel;
  static const String _channelName = "zego_sudmpg_plugin/gameView";

  List<String> zegoJoinUsers = [];
  List<String> zegoReadyUsers = []; // 这个维护不是那么准的
  bool isOpenSound = false; // 是否开启游戏声音
  bool isOpenMusic = false; // 是否开启背景音乐
  bool isOpenVibrate = false; // 是否开启震动

  // 记录下appkey
  YBDZegoMGAPPKeyData? mgAppKeyData;

  static YBDGameUtils? _helper;
  _init() {
    _helper = YBDGameUtils.getInstance();
  }

  YBDGameUtils._();
  static YBDGameUtils? getInstance() {
    if (_helper == null) {
      var instance = YBDGameUtils._();
      _helper = instance;
    }
    return _helper;
  }

  /// 初始化通道处理器
  void initChannelHandler() {
    logger.v("$_channelName initChannelHandler: $_methodChannel");
    if (_methodChannel != null) {
      return;
    }
    var channelName = _channelName;
    _methodChannel = MethodChannel(channelName);
    _methodChannel!.setMethodCallHandler((call) {
      switch (call.method) {
        case YBDGameUtils.MG_EXPIRE_APP_CODE: // code码过期
          {
            return _updateCodeCallback?.call() ?? Future.value("success");
          }
          break;
        case YBDGameUtils.MG_JOIN_USERID: // 有人加入游戏或退出游戏
          {
            var params = call.arguments;
            // _InternalLinkedHashMap<Object?, Object?>
            // if (params is Map) { // 不要对类型进行判断，会导致判断失败
            String? userId = params["userId"];
            bool? isIn = params["isIn"];
            if (userId != null) {
              if (isIn!) {
                if (!zegoJoinUsers.contains(userId)) zegoJoinUsers.add(userId);
              } else {
                zegoJoinUsers.removeWhere((element) => element == userId);
                zegoReadyUsers.removeWhere((element) => element == userId);
              }
            }
            // }
          }
          break;
        case YBDGameUtils.MG_READY_USERID: // 有人准备游戏
          {
            var params = call.arguments;
            // _InternalLinkedHashMap<Object?, Object?>
            // if (params is Map) { // 不要对类型进行判断，会导致判断失败
            String? userId = params["userId"];
            bool? isReady = params["isReady"];
            if (userId != null) {
              if (isReady!) {
                if (!zegoReadyUsers.contains(userId)) zegoReadyUsers.add(userId);
                if (!zegoJoinUsers.contains(userId)) zegoJoinUsers.add(userId);
              } else {
                zegoReadyUsers.removeWhere((element) => element == userId);
              }
            }
            // }
          }
          break;
        case YBDGameUtils.MG_STATE_STARTED: // 游戏加载完成
          {
            print('22.8.3--游戏加载完成');
            _gameStartedCallback?.call(true);
          }
          break;
        case YBDGameUtils.MG_STATE_START_EXCEPTION: // 游戏开始异常
          {
            var jsonString = call.arguments;
            var params = jsonDecode("$jsonString");
            var code = params["resultCode"]; // 是个int类型
            _gameStartExceptionCallback?.call("$code");
          }
          break;
      }
      logger.v("$_channelName: call.method: ${call.method}, "
          "call.arguments: ${call.arguments}, "
          "zegoJoinUsers: $zegoJoinUsers, "
          "zegoReadyUsers: $zegoReadyUsers");
      return Future.value("success");
    });
    logger.v("$_channelName initChannelHandler");
  }
  void initChannelHandlers51cdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 销毁通道
  void disposeChannel() {
    logger.v("$_channelName disposeChannel");
    _methodChannel = null;
    isOpenVibrate = false;
    isOpenSound = false;
    isOpenMusic = false;
    cleanData();
  }
  void disposeChannelWZHh2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 游戏结束了或下一局清空数据
  void cleanData() {
    zegoJoinUsers.clear();
    zegoReadyUsers.clear();
  }

  bool _checkMethodChannel() {
    if (_methodChannel != null) {
      return true;
    }
    print("$_channelName channel is not init");
    return false;
  }
  void _checkMethodChannelJnbwioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 需要监听code过期回调去更新code
  notifyAppUpdateCodeCallback(GameUpdateCodeCallback callback) {
    _updateCodeCallback = callback;
  }

  /// 通知app游戏已经加载完成
  notifyAppGameStarted(GameStartedCallback callback) {
    _gameStartedCallback = callback;
  }

  /// 通知app游戏开始异常了
  notifyAppGameStartException(GameStartExceptionCallback callback) {
    _gameStartExceptionCallback = callback;
  }

  /// 加入游戏
  notifyAppCommonSelfIn(bool isIn, int seatIndex) {
    invokeMethod(YBDGameUtils.MG_SELF_IN, {"isIn": isIn, "seatIndex": seatIndex});
  }

  /// 准备游戏
  notifyAppCommonSelfReady(bool isReady) {
    invokeMethod(YBDGameUtils.MG_SELF_READY, {"isReady": isReady});
  }

  /// 开始游戏
  notifyAppCommonSelfPlaying(bool isPlaying, String extras) {
    print('22.8.3--开始游戏埋点');
    invokeMethod(YBDGameUtils.MG_SELF_PLAYING, {
      "isPlaying": isPlaying,
      "extras": extras,
    });
  }

  /// 设置队长
  notifyAppCommonSelfCaptain(String? userId) {
    invokeMethod(YBDGameUtils.MG_SELF_CAPTAIN, {"captainUserId": userId});
  }

  /// 踢出某人
  notifyAppCommonSelfKick(String userId) {
    invokeMethod(YBDGameUtils.MG_SELF_KICK, {"kickUserId": userId});
  }

  /// 结束游戏
  notifyAppCommonSelfEnd() {
    invokeMethod(YBDGameUtils.MG_SELF_END);
    cleanData();
  }

  /// 游戏是否开始
  Future<bool?> getAppCommonStatePlaying() {
    return invokeMethod(YBDGameUtils.MG_STATE_PLAYING) ?? Future.value(false);
  }

  /// 打开声音
  notifyAppCommonSelfSound(bool isOpen) {
    invokeMethod(YBDGameUtils.MG_SELF_SOUND, {"isOpen": isOpen});
    isOpenSound = isOpen;
    _notifyAppCommonSelfMusic(isOpen);
    _notifyAppCommonSelfVibrate(isOpen);
  }

  /// 打开背景声音
  _notifyAppCommonSelfMusic(bool isOpen) {
    invokeMethod(YBDGameUtils.MG_SELF_MUSIC, {"isOpen": isOpen});
    isOpenMusic = isOpen;
  }

  /// 打开震动声音
  _notifyAppCommonSelfVibrate(bool isOpen) {
    invokeMethod(YBDGameUtils.MG_SELF_VIBRATE, {"isOpen": isOpen});
    isOpenVibrate = isOpen;
  }

  /// 销毁
  notifyAppDispose() {
    cleanData();
    invokeMethod(YBDGameUtils.MG_DISPOSE);
  }

  Future<T?>? invokeMethod<T>(String method, [dynamic arguments]) {
    logger.v("$_channelName: $_methodChannel, invokeMethod: $method, arguments: $arguments");
    if (!_checkMethodChannel()) return null;
    return _methodChannel!.invokeMethod(method, arguments);
  }
}
