import 'dart:async';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/game_ybd_zego_page.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/game_ybd_ludo_setting_pop.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

class YBDGameLudoSettingItem extends StatelessWidget {
  final GameKindType? gameType;

  YBDGameLudoSettingItem({Key? key, this.gameType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      return GestureDetector(
        onTap: () {
          YBDGameZegoMatchUI.gameSettingPop(context, gameType);
        },
        child: Container(
          width: 529.dp720,
          height: 155.dp720,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(YBDGameResource.assetPadding("game_ludo_bg")),
            ),
          ),
          child: Column(
            children: [
              SizedBox(height: 20.dp720),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  YBDGameWidgetUtil.textBalooStrokRegular(
                    "Model",
                    color: Colors.white,
                    fontSize: 31.sp720,
                  ),
                  // 设置图标
                  _settingBtn(),
                ],
              ),
              SizedBox(height: 20.dp720),
              /// 底部配置
              _bottomConfig(),
            ],
          ),
        ),
      );
    });
  }
  void buildz6yCPoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部配置
  Widget _bottomConfig() {
    if (gameType == GameKindType.bumper ||
        gameType == GameKindType.knife ||
        gameType == GameKindType.zegoMG) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // 游戏下注额
          _chooseBet(),
        ],
      );
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        // ludo 游戏模式
        _ludoModel(),
        // 游戏下注额
        _chooseBet(),
      ],
    );
  }
  void _bottomConfighF8GYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// ludo 游戏模式
  Widget _ludoModel() {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      var gameConfig = logic.gameConfig;

      if (gameType == GameKindType.ludo) {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            YBDGameWidgetUtil.textBalooStrokRegular(
              "${gameConfig.mode.value.toFirstCapitalize()} ",
              color: Color(0xffffe862),
              fontSize: 25.sp720,
            ),
            YBDGameWidgetUtil.textBalooStrokRegular(
              gameConfig.modeDesc.toFirstCapitalize(),
              color: Color(0xffffe862),
              fontSize: 23.sp720,
            ),
          ],
        );
      }

      return SizedBox();
    });
  }
  void _ludoModel0XF6Ooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 游戏下注额
  Widget _chooseBet() {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      var gameConfig = logic.gameConfig;

      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          YBDGameWidgetUtil.textBalooStrokRegular(
            "choose_bet".i18n + ":",
            color: Colors.white,
            fontSize: 25.sp720,
          ),
          SizedBox(width: 10.dp720),
          YBDGameWidgetUtil.assetImage(
            YBDGameResource.currencyIconType(gameConfig.currency),
            width: 23.dp720,
            height: 23.dp720,
          ),
          SizedBox(width: 2.dp720),
          YBDGameWidgetUtil.textBalooStrokRegular(
            "${gameConfig.amount}".currencyIntUnit(),
            color: Color(0xffffe862),
            fontSize: 28.sp720,
          ),
        ],
      );
    });
  }
  void _chooseBetWHUgdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 修改游戏配置的图标，改成一直显示了
  Widget _settingBtn() {
    return Padding(
      padding: EdgeInsets.only(left: 10.dp720 as double),
      child: YBDGameWidgetUtil.assetImage(
        "game_set",
        width: 28.dp720,
        height: 28.dp720,
      ),
    );
  }
}
