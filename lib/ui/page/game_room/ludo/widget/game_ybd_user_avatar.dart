import 'dart:async';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/room/widget/mini_ybd_profile_dialog.dart';

/// 游戏的用户头像
class YBDGameLudoUserAvatar extends StatelessWidget {
  /// 座位数据
  final YBDGameSeat gameSeat;
  final GameKindType? gameType;


  YBDGameLudoUserAvatar({
    Key? key,
    required this.gameSeat,
    this.gameType = GameKindType.ludo
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 109.dp720,
      child: Stack(
        children: [
          // 用户头像
          _hasPlayer() ? userAvatar(context) : addAvatar()!,
          // 队长标签或踢出玩家标签
          _captainOrKickOutIcon(),
          // 准备状态
          _readyGameIcon(),
        ],
      ),
    );
  }
  void build1ucDNoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget userAvatar(BuildContext context) {
    return YBDGameWidgetUtil.avatar(
      url: gameSeat.url,
      addSize: true,
      width: 101.dp720,
      radius: 50.5.dp720,
      sideBorder: true,
      sideWidth: 2.dp750,
      onTap: () {
        YBDMiniProfileDialog.showGame(
          context,
          roomId: Get.find<YBDGameRoomGetLogic>().state!.roomId,
          userId: int.tryParse(gameSeat.userId!),
        );
      },
    );
  }
  void userAvatarEP3pmoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget? addAvatar() {
    return YBDGameWidgetUtil.assetImage(
      gameType == GameKindType.ludo ? "game_join_add" : "game_add_circle",
      width: 101.dp720,
      height: 101.dp720,
      onTap: () {
        Get.find<YBDGameRoomGetLogic>().gameEventHandler(
          GameJoinStatus.join,
          index: gameSeat.index,
        );
        logger.v('clicked game_join_add index: ${gameSeat.index}');
      },
    );
  }

  /// 准备状态
  Widget _readyGameIcon() {
    // 准备状态
    if (gameSeat.ready) {
      return Positioned(
        left: 37.5.dp720,
        bottom: 0,
        child: YBDGameWidgetUtil.assetImage(
          "game_ready_icon",
          width: 26.dp720,
          height: 26.dp720,
        ),
      );
    }

    return SizedBox();
  }
  void _readyGameIconEoH97oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 队长标签或踢出玩家标签
  /// 队长标签: 该座位的玩家已上座 & 该座位的玩家是队长
  /// 踢出玩家标签: 该座位的玩家不是队长 & 当前用户是队长 & 该座位有玩家
  Widget _captainOrKickOutIcon() {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      // 队长
      final captain = logic.captainSeat();

      // 当前用户
      final currentUserId = YBDCommonUtil.storeFromContext()!.state.bean!.id;

      // 该座位的玩家已上座 & 该座位的玩家是队长
      if (gameSeat.onSeat && captain.userId == gameSeat.userId) {
        return YBDGameWidgetUtil.assetImage(
          "game_captain",
          width: 30.dp720,
          height: 30.dp720,
        );
      }

      // 该座位的玩家不是队长 & 当前用户是队长 & 该座位有玩家
      if (captain.userId == '$currentUserId' && _hasPlayer()) {
        return Positioned(
          top: 0,
          right: 0,
          child: GestureDetector(
            onTap: () {
              Get.find<YBDGameRoomGetLogic>().kickOutPlayer(gameSeat.userId!);
              logger.v('clicked kick out player index: ${gameSeat.index}');
            },
            child: YBDGameWidgetUtil.assetImage(
              "game_circle_close",
              width: 30.dp720,
              height: 30.dp720,
            ),
          ),
        );
      }

      return SizedBox();
    });
  }
  void _captainOrKickOutIconndTXpoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 该座位有玩家
  bool _hasPlayer() {
    return gameSeat.onSeat;
  }
}
