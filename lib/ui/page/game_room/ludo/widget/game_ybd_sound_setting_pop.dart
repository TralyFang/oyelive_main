import 'dart:async';


import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/method_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';

/// 即构游戏声音设置
class YBDGameSoundSettingPop extends StatefulWidget {

  static show(BuildContext context) {
    YBDEasyPopup.show(
        context,
        YBDGameSoundSettingPop(
          openEar: YBDGameUtils.getInstance()!.isOpenSound,
          openMusic: YBDGameUtils.getInstance()!.isOpenMusic,
          openVibrate: YBDGameUtils.getInstance()!.isOpenVibrate,
          gameVoidCallback: (popContext) {
            YBDEasyPopup.pop(popContext);
          },
        ),
        position: EasyPopupPosition.center,
        duration: Duration(milliseconds: 50),
        outsideTouchCancelable: true);
  }

  bool? openEar;
  bool? openMusic;
  bool? openVibrate;
  GameVoidCallback? gameVoidCallback;


  YBDGameSoundSettingPop({this.openEar, this.openMusic, this.openVibrate, this.gameVoidCallback});

  @override
  _YBDGameSoundSettingPopState createState() => _YBDGameSoundSettingPopState();
}

class _YBDGameSoundSettingPopState extends State<YBDGameSoundSettingPop> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(16.dp750 as double)),
      width: 420.dp750,
      height: 380.dp750,
      child: Column(
        children: [
          _setupTitle(),
          SizedBox(height: 25.dp750),
          _soundItem("game_sound_ear", widget.openEar!, (value){
          }),
          _soundItem("game_sound_music", widget.openMusic!, (value){
          }),
          _soundItem("game_sound_vibrate", widget.openVibrate!, (value){

          }),
        ],
      ),
    );
  }
  void buildwqVMIoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _setupTitle() {
    return Container(
      decoration: BoxDecoration(
          color: YBDHexColor("#3F8CE2"),
          borderRadius: BorderRadius.circular(16.dp750 as double)),
      height: 70.dp750,
      child: Stack(
        children: [
          Center(
            child: YBDGameWidgetUtil.textAlegreyaBlack(
                "Set up",
                fontSize: 28.dp750),
          ),
          Positioned(
              right: 29.dp720,
              top: 35.dp720,
              child: YBDGameWidgetUtil.assetImage(
                  "game_close_white",
                  minTapSize: 70.dp750, onTap: () {
                YBDGameLog.v("YBDGameSoundSettingPop tap close");
                widget.gameVoidCallback?.call(context);
              })!),
          // 需要换成positioned方式
          // Align(
          //   alignment: Alignment.centerRight,
          //   child: YBDGameWidgetUtil.assetImage(
          //       "game_close_white",
          //       minTapSize: 70.dp750, onTap: () {
          //     YBDGameLog.v("YBDGameSoundSettingPop tap close");
          //     widget.gameVoidCallback?.call(context);
          //   }),
          // )
        ],
      ),
    );
  }

  _soundItem(String name, bool mute, Function(bool mute) onChanged) {
    return Container(
      decoration: BoxDecoration(
          color: YBDHexColor("#39ADEF").withOpacity(0.1),
          borderRadius: BorderRadius.circular(16.dp750 as double)),
      height: 80.dp750,
      width: 370.dp750,
      padding: EdgeInsets.only(left: 43.dp750 as double, right: 22.dp750 as double),
      margin: EdgeInsets.only(bottom: 10.dp750 as double),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          YBDGameWidgetUtil.assetImage(
            name,
            width: 41.dp750,
            height: 41.dp750,
          )!,
          Container(
            width: 156.dp750,
            height: 47.dp750,
            child: Switch(
                value: mute,
                activeThumbImage: Image.asset(YBDGameResource.assetPadding("game_switch_on")).image,
                inactiveThumbImage: Image.asset(YBDGameResource.assetPadding("game_switch_off")).image,
                onChanged: (value){
                  YBDGameLog.v("YBDGameSoundSettingPop Switch $name change value: $value");
                  onChanged.call(value);
                  widget.gameVoidCallback?.call(context);
                }),
          ),
        ],
      ),
    );
  }
}
