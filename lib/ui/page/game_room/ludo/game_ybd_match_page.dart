import 'dart:async';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/game_ybd_ludo_page.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/game_ybd_zego_bumper_page.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/game_ybd_zego_knife_page.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/game_ybd_zego_mg_paage.dart';

/// 游戏匹配页面
class YBDGameMatchPage extends StatefulWidget {
  const YBDGameMatchPage({Key? key}) : super(key: key);

  @override
  _YBDGameMatchPageState createState() => _YBDGameMatchPageState();
}

class _YBDGameMatchPageState extends State<YBDGameMatchPage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      var gameType =  GameKindTypeValue.typeWithValue(logic.state!.gameSubCategory);
      if (gameType == GameKindType.bumper) {
        return YBDGameZegoBumperPage();
      }
      if (gameType == GameKindType.knife) {
        return YBDGameZegoKnifePage();
      }
      if (gameType == GameKindType.zegoMG) {
        return YBDGameZegoMGPage();
      }
      /// ludo
      return YBDGameLudoPage();
    });
  }
  void buildSK6pgoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
