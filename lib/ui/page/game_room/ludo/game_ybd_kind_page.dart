import 'dart:async';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/text_ybd_with_stroke.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/game_ybd_setting_item.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/game_ybd_user_avatar.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/widget/inner_shadow/flutter_ybd_inset_box_shadow.dart' as i;

/// 游戏界面的抽象类
abstract class GameKindPage extends StatefulWidget {
  GameKindType? gameType;

  GameKindPage({Key? key, this.gameType}) : super(key: key);

  @override
  GameKindPageState createState() => getState();

  GameKindPageState getState();
}

abstract class GameKindPageState<T extends GameKindPage> extends State<T> {
  @override
  void initState() {
    super.initState();
    // Bad state: Response has no Location header for redirect
  }
  void initStateLlXScoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          gameMatchPlayingView(),
          Container(), // 避免为空，定位层不显示问题
          questionAndSound(),
        ],
      ),
    );
  }
  void buildDNSGYoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 游戏匹配+开始游戏界面
  Widget gameMatchPlayingView() {
    return GetBuilder<YBDGameRoomGetLogic>(
        id: "state.gamePlaying",
        builder: (logic) {
          return Offstage(
            /// 开始游戏或还没初始化游戏，都要隐藏
            offstage: logic.state!.gamePlaying || !logic.state!.gameInit,
            child: gameMatchView(),
          );
        });
  }
  void gameMatchPlayingViewI0QInoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 开始游戏界面
  Widget gamePlayingView() {
    // 已经移到全屏游戏了YBDGameRoomScreenPlaying
    return Container();
  }

  /// 游戏匹配界面
  Widget gameMatchView() {
    return Column(
      children: [
        // logo
        gameLogo(),
        // 配置
        gameSettingItem(),
        // 参与游戏的人
        joinGameUserAvatars(),
        // 陪玩按钮
        joinPlay(),
        // 操作状态
        joinGameButtons(GameJoinStatus.start)
      ],
    );
  }
  void gameMatchVieweiBEgoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// logo
  gameLogo() {
    return Padding(
      padding: EdgeInsets.only(top: 112.dpH1559 as double),
      child: GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
        String? preLogo = logic.state!.roomStatusNotify.body?.game?.image?.logo;
        String? logo = logic.state!.configDisplay?.logo ?? preLogo;
        return YBDGameWidgetUtil.networkImage(
          url: logo,
          width: 451.dp720,
          height: 288.dp720,
        );
      }),
    );
  }

  /// 设置
  gameSettingItem() {
    return Padding(
      padding: EdgeInsets.only(top: 26.dpH1559 as double),
      child: YBDGameLudoSettingItem(gameType: widget.gameType),
    );
  }

  /// 问题，音量，
  questionAndSound() {
    return Container();
  }

  /// 参与游戏的人头像
  Widget joinGameUserAvatars() {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      // 座位列表
      final seatList = logic.seatList;

      YBDGameLog.v("joinGameUserAvatars seatList: ${seatList.map((element) => element.onSeat).toList()}");

      // 座位
      List<Widget> images = List.generate(
        seatList.length,
        (index) => Container(
          margin: EdgeInsets.only(right: 18.dp720 as double, left: 18.dp720 as double),
          child: YBDGameLudoUserAvatar(
            gameSeat: seatList[index],
            gameType: widget.gameType,
          ),
        ),
      );

      switch (widget.gameType) {
        case GameKindType.ludo:
          return Padding(
            padding: EdgeInsets.only(top: 50.dpH1559 as double),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: images,
            ),
          );
        case GameKindType.bumper:
        case GameKindType.knife:
        case GameKindType.zegoMG:
          return Padding(
            padding: EdgeInsets.only(top: 29.dpH1559 as double),
            child: Column(
              children: _joinGameUserAvatars2(images),
            ),
          );
      }
      return Container();
    });
  }
  void joinGameUserAvatarsJAT1Loyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Widget> _joinGameUserAvatars2(List<Widget> images) {
    /// 单行
    if (images.length <= 4) {
      return [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: images,
        )
      ];
    }

    /// 限制最多两行
    if (images.length > 4 && images.length <= 8) {
      return [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: images.getRange(0, 4).toList(),
        ),
        SizedBox(height: 10.dp720),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: images.getRange(4, images.length).toList(),
        ),
      ];
    }
    return images;
  }
  void _joinGameUserAvatars2R8Gnjoyelive(List<Widget> images) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 陪玩按钮
  Widget joinPlay() {
    return Container(
      // 保持间距
      width: 158.dp720,
      height: 29.dp720,
      margin: EdgeInsets.only(top: 30.dpH1559 as double),
      padding: EdgeInsets.only(left: 37.dp720 as double),
      child: Container(),
    );
  }

  /// 操作状态按钮
  Widget joinGameButtons(GameJoinStatus status) {
    return Padding(
      padding: EdgeInsets.only(top: 48.dpH1559 as double),
      child: GetBuilder<YBDGameRoomGetLogic>(
        builder: (logic) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _joinBtn(),
              _quitBtn(),
              SizedBox(width: 21.dp720),
              _readyBtn(),
              _cancelReadBtn(),
              _startReadBtn(),
            ],
          );
        },
      ),
    );
  }
  void joinGameButtonsj8ywgoyelive(GameJoinStatus status) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 加入按钮
  /// 座位列表没包含当前用户
  Widget _joinBtn() {
    if (!Get.find<YBDGameRoomGetLogic>().currentUserSeat().onSeat) {
      return _newEventBtn("join".i18n, bgName: "game_join_bg", event: GameJoinStatus.join);
    }

    return SizedBox();
  }

  /// 退出按钮
  /// 座位列表中有包含当前用户
  Widget _quitBtn() {
    if (Get.find<YBDGameRoomGetLogic>().currentUserSeat().onSeat) {
      return _newEventBtn("quit".i18n, bgName: "game_quit_bg", event: GameJoinStatus.quit);
    }

    return SizedBox();
  }

  /// 准备按钮
  /// 座位列表中包含当前用户 & 当前用户的座位状态为not-ready
  Widget _readyBtn() {
    final currentSeat = Get.find<YBDGameRoomGetLogic>().currentUserSeat();

    if (currentSeat.onSeat && !currentSeat.ready) {
      return _newEventBtn("ready".i18n, bgName: "game_join_bg", event: GameJoinStatus.ready);
    }

    return SizedBox();
  }
  void _readyBtnKekMhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消准备按钮
  /// 座位列表中包含当前用户 & 当前用户非队长 & 当前用户的座位状态为ready
  Widget _cancelReadBtn() {
    final currentSeat = Get.find<YBDGameRoomGetLogic>().currentUserSeat();

    if (currentSeat.onSeat && !currentSeat.isCaptain && currentSeat.ready) {
      return _newEventBtn("cancel_ready".i18n, bgName: "game_join_bg", event: GameJoinStatus.cancel);
    }

    return SizedBox();
  }

  /// 开始按钮
  /// 座位列表中包含当前用户 & 当前用户是队长 & 当前用户的座位状态为ready
  Widget _startReadBtn() {
    final currentSeat = Get.find<YBDGameRoomGetLogic>().currentUserSeat();

    if (currentSeat.onSeat && currentSeat.isCaptain && currentSeat.ready) {
      var bgName = 'game_waiting_bg';
      var event = GameJoinStatus.none;

      if (Get.find<YBDGameRoomGetLogic>().isAllPlayerReady()) {
        bgName = 'game_ready_bg';
        event = GameJoinStatus.start;
        return _newEventBtn("start".i18n, bgName: bgName, event: event);
      }
      return _greyBtn("start".i18n, event: event);
    }

    return SizedBox();
  }
  void _startReadBtnRFxxFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 统一事件按钮
  Widget gameEventButton(String text, {String? bgName, GestureTapCallback? onTap, GameJoinStatus? event}) {
    return YBDGameWidgetUtil.buttonTextBg(
      text,
      width: event == GameJoinStatus.join ? 280.dp720 : 239.dp720,
      height: 76.dp720 as double,
      bgName: bgName,
      color: Colors.white,
      fontSize: 33.sp720,
      onTap: () {
        YBDGameLog.v("game room tap event: $event");
        onTap?.call();
        tryCatch(() {
          Get.find<YBDGameRoomGetLogic>().gameEventHandler(event);
        });
      },
    );
  }

  Widget _newEventBtn(String text, {String? bgName, GestureTapCallback? onTap, GameJoinStatus? event}) {
    return GestureDetector(
      onTap: () {
        YBDGameLog.v("game room tap event: $event");
        onTap?.call();
        tryCatch(() {
          Get.find<YBDGameRoomGetLogic>().gameEventHandler(event);
        });
      },
      child: Container(
        height: 72.dp720,
        width: 275.dp720,
        decoration: i.BoxDecoration(
            gradient: LinearGradient(
              colors: _getColors(event),
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
            borderRadius: BorderRadius.all(Radius.circular(40.dp720 as double)),
            // color: event == GameJoinStatus.none ? Colors.white.withOpacity(0.5) : null,
            boxShadow: [
              i.BoxShadow(color: Colors.white.withOpacity(0.5), offset: Offset(0, 1.px.toDouble()), blurRadius: 2.px.toDouble(), inset: true),
              i.BoxShadow(color: _getTextShadowColor(event), offset: Offset(0, -1.px.toDouble()), blurRadius: 2.px.toDouble(), inset: true),
            ]),
        child: Center(
          child: YBDTextWithStroke(
            text: text,
            fontSize: 32.sp750.toDouble(),
            fontFamily: 'baloo',
            strokeColor: _getTextShadowColor(event),
          ),
        ),
      ),
    );
  }
  void _newEventBtn3Aw9Eoyelive(String text, {String? bgName, GestureTapCallback? onTap, GameJoinStatus? event}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _greyBtn(String text, {String? bgName, GestureTapCallback? onTap, GameJoinStatus? event}) {
    return GestureDetector(
      onTap: () {
        YBDGameLog.v("game room tap event: $event");
        onTap?.call();
        tryCatch(() {
          Get.find<YBDGameRoomGetLogic>().gameEventHandler(event);
        });
      },
      child: Container(
        height: 72.dp720,
        width: 275.dp720,
        decoration: i.BoxDecoration(
            // gradient: LinearGradient(
            //   colors: _getColors(event),
            //   begin: Alignment.topCenter,
            //   end: Alignment.bottomCenter,
            // ),
            borderRadius: BorderRadius.all(Radius.circular(40.dp720 as double)),
            color: Colors.white.withOpacity(0.5),
            boxShadow: [
              i.BoxShadow(color: Colors.white.withOpacity(0.5), offset: Offset(0, 1.px.toDouble()), blurRadius: 2.px.toDouble(), inset: true),
              i.BoxShadow(color: Color(0xff818181), offset: Offset(0, -1.px.toDouble()), blurRadius: 2.px.toDouble(), inset: true),
            ]),
        child: Center(
          child: YBDTextWithStroke(
            text: text,
            fontSize: 32.sp750.toDouble(),
            fontFamily: 'baloo',
            strokeColor: Color(0xff4b4b4b),
          ),
        ),
      ),
    );
  }
  void _greyBtnGL2Pdoyelive(String text, {String? bgName, GestureTapCallback? onTap, GameJoinStatus? event}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Color> _getColors(GameJoinStatus? event) {
    if (event == GameJoinStatus.quit) return [Color(0xffFFCE5B), Color(0xffFFBC04)];
    // if (event == GameJoinStatus.none) return [];
    return [Color(0xff34F3F6), Color(0xff1879FF)];
  }

  Color _getShadowColor(GameJoinStatus? event) {
    if (event == GameJoinStatus.quit) return Color(0xff7E5A03).withOpacity(0.5);
    // if (event == GameJoinStatus.none) return Color(0xff818181).withOpacity(0.5);
    return Color(0xff035E73).withOpacity(0.5);
  }

  Color _getTextShadowColor(GameJoinStatus? event) {
    if (event == GameJoinStatus.quit) return Color(0xff786000);
    return Color(0xff0A59A2);
  }
}
