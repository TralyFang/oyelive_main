import 'dart:async';


import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/game_ybd_kind_page.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

/**
 *  ludo游戏状态
 * 1。虚位待入座
 * 2。加入游戏待准备
 * 3。已准备
 *
 *  ludo游戏身份
 *  1。 主播
 *  2。 队长
 *  3。 参与者
 *  4。 麦上用户
 *  5。 观众
 *
 *
 *
 * */

/// 游戏房间中间部分的ludo游戏
class YBDGameLudoPage extends GameKindPage {
  @override
  _YBDGameLudoPageState createState() => _YBDGameLudoPageState();

  @override
  GameKindPageState<GameKindPage> getState() {
    return _YBDGameLudoPageState();
  }
}

class _YBDGameLudoPageState extends GameKindPageState<YBDGameLudoPage> {
  @override
  Widget build(BuildContext context) {
    widget.gameType = GameKindType.ludo;
    return super.build(context);
  }
  void buildxT1xjoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  questionAndSound() {
    return Container();
  }

  /// 陪玩按钮
  @override
  Widget joinPlay() {
    return GetBuilder<YBDGameRoomGetLogic>(builder: (logic) {
      // 只有队长且没满座才能点击加入陪玩, 需要支持陪玩
      var isCaptain = logic.state!.isCaptain;
      if (!isCaptain || logic.isSeatFull() || !logic.state!.isOpenRobot) {
        return super.joinPlay();
      }
      return YBDDelayGestureDetector(
        extraHitTestArea: EdgeInsets.all(20.dp720 as double),
        onTap: () {
          Get.find<YBDGameRoomGetLogic>().updateIsRobot();
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.GAME_ROOM_PAGE,
            itemName: "playmate",
          ));
        },
        child: Container(
          width: 158.dp720,
          height: 33.dp720,
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(top: 30.dpH1559 as double),
          padding: EdgeInsets.only(left: 37.dp720 as double),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                YBDGameResource.assetPadding(
                    Get.find<YBDGameRoomGetLogic>().state!.isRobot ? "game_joinplay_sel" : "game_joinplay_nor"),
              ),
            ),
          ),
          child: Container(
            margin: EdgeInsets.only(bottom: 5.dp720 as double),
            child: YBDGameWidgetUtil.textBalooStrokRegular(
              "join_play".i18n,
              color: Colors.white,
              fontSize: 23.sp720,
            ),
          ),
        ),
      );
    });
  }
  void joinPlayHO4D8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
