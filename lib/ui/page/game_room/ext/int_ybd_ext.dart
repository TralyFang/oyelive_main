import 'dart:async';

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
/*
import 'package:zego_express_engine/zego_express_engine.dart';

extension StringExt on int {
  ZegoEngineProfile profile() {
    return ZegoEngineProfile(this, ZegoScenario.Communication, appSign: GameConst.ZegoAppSign);
  }
}

 */

extension ColorExt on int {
  Color randomColor() {
    return Color.fromRGBO(Random().nextInt(this), Random().nextInt(this), Random().nextInt(this), 1.0);
  }
}
