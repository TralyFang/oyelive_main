import 'dart:async';


import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/room/entity/game_ybd_query_model.dart';
import 'package:oyelive_main/ui/page/room/play_center/entity/game_ybd_entity.dart';

extension GameTypeExt on GameType {
  YBDGameRecord? info() {
    if (YBDTPGlobal.model == null) return null;
    logger.v('game record: ${GameCodes[this]} this:$this');
    YBDGameRecord? re;
    YBDTPGlobal.model!.record!.forEach((element) {
      logger.v('game record compare: ${GameCodes[this] == element!.code} this:$this');
      if (GameCodes[this] == element.code){
         re = element;
       }
    });
    logger.v('game record result: $re this:$this');
    return re;
  }
}
