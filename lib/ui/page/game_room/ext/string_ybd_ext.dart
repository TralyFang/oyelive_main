import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/constant/short_ybd_route.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';

extension StringExt on String {
  String profix() {
    return (YBDRtcHelper.getInstance().getRtcType == RtcType.RtcTypeZego
            ? GameConst.zego_log_profix
            : GameConst.agora_log_profix) +
        ' ' +
        this +
        ' ';
  }

  void profixnNpOpoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 字符国际化翻译
  String get i18n {
    return translate(this);
  }

  /// 给http地址拼接参数parmas: key=value
  String httpAppending(String parmas) {
    if (this.contains("http")) {
      if (this.contains("?")) {
        return this + "&$parmas";
      }
      return this + "?$parmas";
    }
    return this;
  }

  void httpAppendingUzDXXoyelive(String parmas) {
    int needCount = 0;
    print('input result:$needCount');
  }

  ///  货币格式化
  String currencyFormat() {
    return "${YBDNumericUtil.compactNumberWithFixedDigits(this, fixedDigits: 2, rounding: false)}";
  }

  void currencyFormatbZDrnoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 数字千分位加逗号，  e.g. 12,120,000
  static String? numThousands(num? number) {
    return number == null ? "0" : YBDNumericUtil.format(number as int?);
  }

  /// 币种格式化 单位K M 10M https://www.jianshu.com/p/adfabf566335
  /// 注意单位的大小写，仅仅适用于游戏房的字体
  String currencyIntUnit({int point = 0}) {
    int value = int.tryParse(this) ?? 0;
    if (value < 10000) {
      // 小于1万
      return "$value";
    }
    if (point > 0) {
      // 只有大于零的精确度才做+1切割最后一位处理。（相当于向下取整）
      point += 1;
    }
    if (value < 1000000) {
      // 小于100万 1.37 5000 == 1.37M ==1.38M
      var result = (value / 1000).toStringAsFixed(point); // 从10k开始
      if (point > 0) {
        result = result.substring(0, result.length - 1);
        result = result.cleanFloatZero();
      }
      return "${result}k";
    }
    var result = (value / 1000000).toStringAsFixed(point); // 从100万
    if (point > 0) {
      result = result.substring(0, result.length - 1);
      result = result.cleanFloatZero();
    }
    return "${result}m";
  }

  void currencyIntUnitqB5s5oyelive({int point = 0}) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 去掉尾部没用的零0，或点.
  cleanFloatZero() {
    var fZero = this;
    // 浮点数最后的零是没用的, 最后的点也是没用的
    while (
        (fZero.contains('.') && fZero.endsWith('0')) || fZero.endsWith('.')) {
      fZero = fZero.substring(0, fZero.length - 1);
    }
    return fZero;
  }

  /// 仅仅首字母大写
  String toFirstCapitalize() {
    // YBDGameLog.v("toFirstCapitalize: $this");
    if (this.length == 1) {
      return this.toUpperCase();
    }
    if (this.length > 1) {
      return "${this[0].toUpperCase()}${this.substring(1)}";
    }
    return this;
  }

  void toFirstCapitalizeEHwVQoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  String get prefix => Const.ANALYSIS_PREFIX + this;

  String get timeTrack => 'duration_$this';

  // 获取数数埋点短路由
  String get shortRoute {
    // 语音房
    if (this.contains(YBDWebRoute.live)) return YBDShortRoute.live;
    // 关注、粉丝、好友、私信带用户ID的路由
    if (this.startsWith(YBDNavigatorHelper.follow)) {
      // flutter://follow/2100212/1
      String lastNum = this.substring(this.length - 1);
      TA.profileUserId = this.getId;
      switch (lastNum) {
        case '0':
          return YBDShortRoute.my_friends;
        case '1':
          return YBDShortRoute.my_following;
        case '2':
          return YBDShortRoute.my_followers;
      }
    } else if (this.startsWith(YBDNavigatorHelper.user_profile) ||
        this.startsWith(YBDNavigatorHelper.inbox_private_message) ||
        this.startsWith(YBDNavigatorHelper.inbox_setting)) {
      // flutter://inbox/private/2100077/c00f5680a899474abb11e4cb6c010bc6
      // flutter://inbox/setting/2/2100077
      TA.profileUserId = this.getId;
    } else {
      TA.profileUserId = '';
    }
    // webview
    if (this.contains(YBDNavigatorHelper.flutter_web_view_page_v1)) {
      // 换金币
      if (this.contains(YBDWebRoute.gold)) return YBDShortRoute.my_beans_ex;
      // 换钻石
      if (this.contains(YBDWebRoute.gem)) return YBDShortRoute.my_gems_ex;
      // 等级
      if (this.contains(YBDWebRoute.level)) return YBDShortRoute.my_level;
      // 隐私条款
      if (this.contains(YBDWebRoute.privacy)) return YBDShortRoute.privacy;
      // 代理充值
      if (this.contains(YBDWebRoute.my_top_up_ag))
        return YBDShortRoute.my_top_up_ag;
      // 代理充值奖励明细页
      if (this.contains(YBDWebRoute.my_top_up_ag_reward))
        return YBDShortRoute.my_top_up_ag_reward;
      // 官方主播规则页
      if (this.contains(YBDWebRoute.new_live_rule))
        return YBDShortRoute.new_live_rule;
      // 活动
      String title = '';
      // flutter:\/\/web_view_page_v2?url=http%3A%2F%2Fvoice-cluster-314173693.cn-northwest-1.elb.amazonaws.com.cn%3A8090%2Fevent%2FroadToIndependence2k22%2Findex.html&title=Road To Independence&shareName=Road To Independence&shareImg=1658825365570.png&entranceType=1
      late Uri u;
      try {
        u = Uri.parse(this);
      } catch (e) {
        alog.e('url trans fail : $e');
      }
      Map map = u.queryParameters;
      title = map['url']?.toString()?.key ?? ''; // 数数v2.0 title换成key
      return 'event_$title';
    }
    // 头像 photo_viewer
    if (this.contains(YBDShortRoute.photo_viewer))
      return YBDShortRoute.photo_viewer;
    // slog详情页
    // if (this.contains(YBDNavigatorHelper.status_detail)) return YBDShortRoute.slog_detail;
    if (this.contains(YBDNavigatorHelper.game_tp_room_page))
      return YBDShortRoute.game_tp_room_page;

    // other
    String output = this;
    YBDObsUtil.instance().shortRoutes.forEach((key, value) {
      if (this.startsWith(key)) output = value;
    });
    // 去掉[flutter://]前缀
    if (output.startsWith(YBDWebRoute.prefix))
      output = output.substring(YBDWebRoute.prefix.length);
    return output;
  }

  // 获取路由中的用户ID
  String get getId {
    List<String> a = this.split('/');
    for (String e in a)
      if (e.length == 7 && YBDNumericUtil.isNumeric(e)) return e;
    return this;
  }
}
