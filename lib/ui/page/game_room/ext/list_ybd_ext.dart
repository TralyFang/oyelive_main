import 'dart:async';

import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_mic_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_bloc.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';

extension ListExt on List<String?> {
  ///去除 空串，去重
  List<String> duplicate() {
    Set s = Set();
    List l = this.where((element) => element!.isNotEmpty).toList();
    l.forEach((element) {
      s.add(element.toString());
    });
    // s.addAll();
    return s.toList().cast<String>();
  }
  void duplicatemvOBRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///自己是否在麦位上
  isAborad() {
    if (this.isEmpty) return false;
    return this.contains(YBDRtcHelper.getInstance().info?.id.toString());
  }
}

extension ListMicExt on List<YBDGameMicInfoBodyMic?> {
  ///获取麦上正常用户 Id [isContainSelf] 是否包含自己
  List<String> grab({bool isContainSelf = false}) {
    List<String> serverMicUserList = map((e) {
      logger
          .v('mic user: ${e?.userId} self id:${YBDRtcHelper.getInstance().info?.id.toString()} mic status: ${e?.status}');
      if (e?.status == GameConst.Grabbed) {
        logger.v('grabbed userid : ${e?.userId} $isContainSelf');
        if (isContainSelf && e?.userId == YBDRtcHelper.getInstance().info?.id.toString()) return '';
        return e?.userId ?? '';
      }
      return '';
    }).toList();
    logger.v('grabbed userid : ${serverMicUserList.runtimeType} $isContainSelf');
    return serverMicUserList.duplicate();
  }
  void grabbDCsyoyelive({bool isContainSelf = false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///获取麦上被muted or muted-self用户 Id
  List<String> mute() {
    List<String> serverMicUserList = map((e) {
      logger.v(
          'mic mute user: ${e?.userId} self id:${YBDRtcHelper.getInstance().info?.id.toString()} mic status: ${e?.status}');
      if (e?.status == GameConst.Muted || e?.status == GameConst.MuteSelf || e?.status == GameConst.MuteDouble) {
        logger.v('mute userid : ${e?.userId}');
        return e?.userId ?? '';
      }
      return '';
    }).toList();
    return serverMicUserList.duplicate();
  }
  void muteu7pQooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  YBDGameMicBean getMicBeans(YBDGameMicInfoResources res) {
    logger.v("mic bean length :${this.length}");
    bool isBoard = false;
    List<YBDGameMicInfoBodyMic> micList = [];
    forEach((element) {
      String addAvatar = '';
      logger.v('mic beans for each: ${element?.userId} ${element?.index}');
      if (element?.userId != null && element?.userId == YBDRtcHelper.getInstance().info!.id.toString()) {
        logger.v('mic bloc is board : ${element?.userId}');
        isBoard = true;
      }

      if (element?.userId != null && (res.userMap.containsKey(element?.userId))) {
        logger.v('mic bloc avatar ${res.userMap[element?.userId!]["property"]["avatar"]}');
        addAvatar = res.userMap[element?.userId!]["property"]["avatar"] ?? '';
      }

      YBDGameMicInfoBodyMic mic = YBDGameMicInfoBodyMic()
        ..index = element?.index
        ..status = element?.status ?? GameConst.Empty
        ..userId = element?.userId ?? ''
        ..roomId = element?.roomId ?? ''
        ..avatar = addAvatar;
      logger.v(
          'mic bloc mic :$mic ${element?.index} ${element?.status ?? GameConst.Empty} ${element?.userId ?? ''} ${element?.roomId ?? ''} $addAvatar');
      micList.add(mic);
    });
    logger.v('mic bloc list:${micList.length}');
    return YBDGameMicBean(micList, isBoard);
  }
}
