import 'dart:async';

import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/send/send_ybd_message_type.dart';
import 'package:oyelive_main/ui/page/game_lobby/enter_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';

extension SendMsgTypeValue on SendMsgType? {
  String get value {
    switch (this) {
      case SendMsgType.EnterRoom:
        return GameConst.enterRoomCmd;
      case SendMsgType.SendQuickMsg:
        return GameConst.sendQuickMsgCmd;
      case SendMsgType.ExitRoom:
        return GameConst.exitRoomCmd;
      case SendMsgType.LockedMic:
      case SendMsgType.UnLockedMic:
        return this == SendMsgType.LockedMic ? GameConst.LockMicCmd : GameConst.UnlockMicCmd;
        break;
      case SendMsgType.MuteMic:
      case SendMsgType.MuteSelfMic:
        return GameConst.MuteMicCmd;
      case SendMsgType.UnMuteMic:
      case SendMsgType.UnMuteSelfMic:
        return GameConst.UnMuteMicCmd;
      case SendMsgType.DropMic:
      case SendMsgType.GrabMic:
        return this == SendMsgType.DropMic ? GameConst.DropMicCmd : GameConst.GrabMicCmd;
        break;
      case SendMsgType.Ready:
      case SendMsgType.CancelReady:
        return this == SendMsgType.Ready ? GameConst.ReadyGameCmd : GameConst.CancelReadyGameCmd;
        break;
      case SendMsgType.TakeSeat:
      case SendMsgType.LeaveSeat:
        return this == SendMsgType.TakeSeat ? GameConst.TakeSeatCmd : GameConst.LeaveSeatCmd;
        break;
      case SendMsgType.Block:
        return GameConst.BlockCmd;
        break;
      case SendMsgType.UpdateGameConfig:
        return GameConst.UpdateGameConfigCmd;
        break;
      case SendMsgType.KickOutPlayer:
        return GameConst.KickOutPlayerCmd;
        break;
      case SendMsgType.Start:
        return GameConst.StartGameCmd;
        break;
      case SendMsgType.GameReport:
        return GameConst.GameReportCmd;
        break;
      case SendMsgType.ChatMsg:
        return GameConst.chatCmd;
        break;
      case SendMsgType.RoomStatus:
        return GameConst.RoomStatusCmd;
        break;
      case SendMsgType.Gifting:
        return GameConst.GiftingCmd;
        break;
      default:
        return '';
    }
  }

  SendMessageType get type {
    switch (this) {
      case SendMsgType.EnterRoom:
        return SendMessageType.SUBSCRIBE;
      case SendMsgType.ExitRoom:
        return SendMessageType.UNSUBSCRIBE;
      case SendMsgType.GrabMic:
      case SendMsgType.DropMic:
      case SendMsgType.LockedMic:
      case SendMsgType.UnLockedMic:
      case SendMsgType.MuteMic:
      case SendMsgType.UnMuteMic:
      case SendMsgType.MuteSelfMic:
      case SendMsgType.UnMuteSelfMic:
      case SendMsgType.MuteOtherAboradMic:
      case SendMsgType.KickOutPlayer:
      case SendMsgType.CancelReady:
      case SendMsgType.Ready:
      case SendMsgType.UpdateGameConfig:
      case SendMsgType.TakeSeat:
      case SendMsgType.LeaveSeat:
      case SendMsgType.ChatMsg:
      case SendMsgType.Start:
      case SendMsgType.GameReport:
      case SendMsgType.RoomStatus:
      case SendMsgType.Gifting:
      case SendMsgType.SendQuickMsg:
        return SendMessageType.PUBLISH;
      default:
        return SendMessageType.PUBLISH;
    }
  }
}

extension EnterGameRoomTypeExt on EnterGameRoomType? {
  Future<Map<String, dynamic>> get body async {
    var game = YBDRtcHelper.getInstance().selectedGame;
    Map<String, dynamic> room = {
      'category': 'game',
      'subCategory': game?.subCategory ?? 'ludo',
      'status': 'live',
      'entryType': this.value,
      'needSeat': YBDEnterHelper.needSeat,
      'entryFrom':
          YBDRtcHelper.getInstance().roomId != GameConst.defaultValue ? YBDNavigatorHelper.game_room_page : 'gamePage',
      'matchType': YBDRtcHelper.getInstance().matchType, // ludo 匹配类型 1=人机
      'modeName': YBDRtcHelper.getInstance().modeName,
    };
    //使用后复原
    YBDEnterHelper.needSeat = YBDEnterHelper.need;
    return {'room': room, 'client': await YBDCommonUtil.generateCommonHeader()};
  }

  String get value {
    String value = '';
    switch (this) {
      case EnterGameRoomType.Join:
      case EnterGameRoomType.QuickJoin:
      case EnterGameRoomType.QuickGame:
        value = GameConst.join;
        break;
      case EnterGameRoomType.Watch:
      case EnterGameRoomType.QuickCreate:
        value = GameConst.watch;
        break;
      default:
        break;
    }
    return value;
  }
}

extension SendMsgTypeExt on SendMsgType {
  Map<String, dynamic> body(String userId, int? index) {
    return {
      'userIds': [userId],
      'indexes': [index],
    };
  }
}

extension GameCurrencyTypeExt on GameCurrencyType? {
  String get value {
    switch (this) {
      case GameCurrencyType.golds:
        return 'gold';
      case GameCurrencyType.beans:
        return 'bean';
      default:
        return 'gold';
    }
  }

  static GameCurrencyType type(String typeString) {
    var typStr = typeString.toLowerCase();
    switch (typStr) {
      case 'gold':
        return GameCurrencyType.golds;
      case 'bean':
        return GameCurrencyType.beans;
      default:
        return GameCurrencyType.golds;
    }
  }
}

extension GameModelTypeExt on GameModelType? {
  String get value {
    switch (this) {
      case GameModelType.classic:
        return 'classic';
      case GameModelType.fast:
        return 'fast';
      default:
        return 'classic';
    }
  }

  static GameModelType type(String typeString) {
    var typStr = typeString.toLowerCase();
    switch (typStr) {
      case 'classic':
        return GameModelType.classic;
      case 'fast':
        return GameModelType.fast;
      default:
        return GameModelType.classic;
    }
  }
}

extension GameKindTypeValue on GameKindType {
  static GameKindType typeWithValue(String value) {
    /// TODO: 测试
    // return GameKindType.zegoMiniGame;
    value = value.toLowerCase();
    if (value.isNotEmpty && value != 'ludo' && !YBDGameRoomGetLogic.dropZegoMiniGame) {
      if (value == 'bumper_car') {
        return GameKindType.bumper;
      }
      if (value == 'fly_cutter') {
        return GameKindType.knife;
      }
      return GameKindType.zegoMG;
    }

    /// 默认为
    return GameKindType.ludo;
  }
}

extension GameRoomStatusExt on GameRoomStatus {
  Map<String, dynamic> body(String? subCategory) {
    logger.v('game room status change: ${this.toString()} $subCategory');
    return {
      'subCategory': subCategory,
      'status': this == GameRoomStatus.offline ? GameConst.offline : GameConst.live,
    };
  }
}
