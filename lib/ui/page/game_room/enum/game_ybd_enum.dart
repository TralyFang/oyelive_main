import 'dart:async';

enum EnterGameRoomType {
  QuickJoin, //快速加入
  QuickCreate, //快速创建房间
  Join, //加入
  Watch, //进入房间观看
  QuickGame, // ludo人机局
}

enum SendMsgType {
  EnterRoom, //进房
  ExitRoom, // 退房
  LockedMic, //锁麦
  UnLockedMic, //解除锁麦
  GrabMic, //上麦
  DropMic, //下麦
  MuteMic, //禁麦
  UnMuteMic, //解除禁麦
  MuteSelfMic, // 禁自己麦
  UnMuteSelfMic, // 解禁自己麦
  LocalMute, // 静音别人
  LocalUnMute, // 取消静音别人
  MuteOtherAboradMic, //主播禁音其他主播的麦
  MiniProfile, // 查看profile
  Block, // miniprofile禁言和踢人
  ChatMsg, //公聊消息
  TakeSeat, //上座
  LeaveSeat, //下座
  Ready, //游戏准备
  CancelReady, //取消准备
  UpdateGameConfig, //修改游戏配置
  KickOutPlayer, //剔除玩家
  Start, // 开始游戏
  GameReport, // sud游戏状态上报
  SystemMsg, //系统消息
  EnterNotify, //进房通知
  RoomStatus,
  Gifting, // 发送礼物

  YBDPublish, // 公共消息类型
  TPBet, // 下注
  TPSeeCard, // 看牌
  TPCompareCard, // 比牌
  TPShowCard, // 开牌

  SendQuickMsg, //发送快捷消息
}

/// 游戏模式
enum GameModelType { fast, classic }

/// 金币类型
enum GameCurrencyType { golds, beans }

/// 游戏种类
enum GameKindType {
  ludo, // 飞行棋
  bumper, // 碰碰
  knife, // 飞刀达人
  zegoMG, // 即构小游戏
}

/// 游戏加入状态
enum GameJoinStatus {
  // 加入
  //用户： 退出，准备/取消准备
  //队长： 退出，准备/开始（所有人准备）/待开始（还有人没准备）

  /// 待加入的状态，[join]
  join,

  /// 待准备的状态，[quit, ready]
  ready,

  /// 参赛者已经准备了，可以取消, [quit, cancel]
  cancel,

  /// 队长已经准备但还有参赛者没准备, [quit, dis-start]
  waiting,

  /// 队长和参赛者都已经准备 [quit, start]
  start,

  /// 空事件
  none,

  /// 队长踢除玩家
  kickOut,

  /// 退出
  quit,
}

enum GameMicRole {
  owner, // 房主
  audience, // 观众
}

enum MicOpType {
  hide, // 隐藏
  take, // 上麦
  drop, // 下麦
}

enum MicMuteType {
  hide, // 隐藏
  ownerMute, // 房主静音
  ownerUnMute, // 房主解除静音
  userMuteSelf, // 观众静音自己
  userUnmuteSelf, // 观众解除静音自己
}

enum LocalMuteType {
  hide, // 隐藏
  localMute, // 本地静音
  localUnmute, // 本地解除静音
}

enum MicLockType {
  hide, // 隐藏
  lock, // 锁麦
  unLock, // 解锁
}

enum GameMicEvent {
  Refresh, // 刷新页面数据
}

enum GameRoomStatus { live, offline }
