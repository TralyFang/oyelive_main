import 'dart:async';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart' as i;
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_category_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_mic_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/agora_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/zego_ybd_express_wrapper.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';

enum RtcType {
  RtcTypeZego,
  RtcTypeAgora,
}

/**
 *   rtc_config
 *   maxMemberCount 房间支持加入的最大人数
 *   hardwareEncode 是否开启硬件解码
 * */

///维护 当前登录用户信息
///维护 当前用户角色
class YBDRtcHelper {
  static YBDRtcHelper? _helper;

  Future _init() async {
    _helper = await YBDRtcHelper.getInstance();
  }
  void _init7KNLZoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  YBDRtcHelper._();
  static YBDRtcHelper getInstance() {
    if (_helper == null) {
      _helper = YBDRtcHelper._();
    }
    return _helper!;
  }

  i.YBDUserInfo? info;

  RtcEngine? agoraEngine;
  String? roomId = GameConst.defaultValue;

  // 是否为人机局 1-是
  String? matchType = GameConst.matchTypePerson;

  // tpgo 匹配模式
  String modeName = '';

  // tpgo 匹配币种
  String currency = 'bean';

  /// 默认Rtc类型
  RtcType _rtcType = RtcType.RtcTypeAgora;

  /// 默认Rtc引擎 根据rtcType 去适配 具体实现
  BaseRtcApi _rtcApi = YBDAgoraRtcApi();

  ///麦位信息
  YBDGameMicInfoBody? _micInfoBody;

  ///当前用户是否是主播
  bool selfIsBoard = false;

  ///是否混流 混流目前默认走L3 不走cdn
  bool ismixed = false;

  ///最多拉取12路
  List<String?> streamIdList = List.generate(12, (index) => null);

  ///本地mic 用户列表
  List<YBDGameMicInfoBodyMic?>? micList = [];

  /// 所选的游戏类别
  YBDGameCategoryData? selectedGame;

  /// 进入游戏的方式
  EnterGameRoomType? enterRoomType;

  ///显示了离线弹窗
  bool showOffline = false;

  // ludo游戏房的背景地址
  String? ludoBgURL;

  init() {
    logger.v('engin init by login');

    ///create engine 全局维护一个实例  不建议destroy 来回create destroy浪费资源、耗时
    // YBDZegoRtcApi.createEngine();
    YBDAgoraRtcApi.createEngine();
  }

  /// 设置Rtc类型，如果没有设置类型，则默认为声网
  void setRtcType(RtcType type) {
    logger.v('set rtc type : $type');
    _rtcType = type;
    // _rtcApi = type == RtcType.RtcTypeAgora ? YBDAgoraRtcApi() : YBDZegoRtcApi();
    _rtcApi = YBDAgoraRtcApi();
    // type == RtcType.RtcTypeAgora ? YBDZegoExpressWrapper.destroyEngine() : agoraEngine?.destroy();
  }
  void setRtcTypeJtNhRoyelive(RtcType type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取Rtc类型，默认为声网
  RtcType get getRtcType {
    return _rtcType;
  }

  /// 获取Rtc对象，如果没有设置类型[setRtcType]，则默认为声网
  BaseRtcApi get getRtcApi {
    return _rtcApi;
  }

  setGameSubType(String? sub) {
    logger.v('game category: $sub');
    YBDGameCategoryData data = YBDGameCategoryData()..subCategory = sub;
    selectedGame = data;
  }

  ///get roomid
  String? getRoomId() {
    assert(YBDRtcHelper.getInstance().roomId != GameConst.defaultValue);
    return YBDRtcHelper.getInstance().roomId;
  }

  ///麦位通知下发时  更新
  set setMicInfo(YBDGameMicInfoBody body) {
    this.selfIsBoard = roomId == info!.id?.toString();

    ///YBDGameMicUtil.compareWith(body?.mic);
    this._micInfoBody = body;
  }

  login() {
    logger.v('index create engin');
    final store = YBDCommonUtil.storeFromContext(context: Get.context);
    YBDRtcHelper.getInstance().info = store?.state?.bean;
    // init();
  }

  disposeEngin() async {
    tryCatch(() async {
      /*
      if (YBDZegoExpressWrapper.instance != null) {
        YBDZegoExpressWrapper.destroyEngine();
      }

       */
      if (this.roomId != GameConst.defaultValue) {
        //进入过游戏房的 才走销毁流程
        await agoraEngine?.destroy();
      }
      agoraEngine = null;
    });
  }

  ///销毁资源
  dispose({bool islogout = false}) {
    if (islogout) {
      disposeEngin();
    }
    this.streamIdList = [];
    this.roomId = GameConst.defaultValue;
    this._micInfoBody = null;
    this.selfIsBoard = false;
    this.ismixed = false;
    this.showOffline = false;
    // this.info = null;
  }
}
