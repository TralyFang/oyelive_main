import 'dart:async';

import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/zego_ybd_express_wrapper.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
/*
class YBDZegoRtcApiHelper {
  ///mute aboardcoast
  static muteAboardcast(bool? muted, int? muteUserId) {
    muteSelf(muted, muteUserId, () {
      muteSelfIsAboradcast(muted, muteUserId, () {
        muteMixed(muted, muteUserId);
      });
    });
  }

  /// mute self
  static muteSelf(bool? muted, int? muteUserId, MicOperatorCallBack callBack) {
    if (muteUserId != YBDRtcHelper.getInstance().info?.id) {
      callBack?.call();
      return;
    }
    logger.i('mute self: $muteUserId, $muted');

    ///如果自己被房主禁麦
    YBDZegoRtcApi.mutePublishStream(muted);
  }

  ///mute with mixed
  static muteMixed(bool? muted, int? muteUserId) {
    ///如果是混流L3 服务端主动剔出这个用户
    if (!YBDRtcHelper.getInstance().ismixed) return;
    logger.i('mute mixed: $muteUserId, $muted');
    mutedOperate(muted!, muteUserId);
  }

  ///mute other, self is aboradcast
  static muteSelfIsAboradcast(bool? muted, int? muteUserId, MicOperatorCallBack callBack) {
    if (!YBDRtcHelper.getInstance().selfIsBoard) {
      callBack?.call();
      return;
    }
    logger.i('mute other, self is aboradcast: $muteUserId, $muted');
    mutedOperate(muted!, muteUserId);
  }

  /// zego api operator
  static void mutedOperate(bool muted, int? muteUserId) {
    logger.i('mute operate: $muteUserId, $muted');
    // muted ? YBDZegoRtcApi.stopPlayStream(muteUserId.toString()) : YBDZegoRtcApi.startPlayStream(muteUserId.toString());
    YBDZegoExpressWrapper.instance.setZegoClientRole(muted ? ZegoRole.Audience : ZegoRole.Guest);
  }

  ///mute by audience
  static muteAudience(bool? muted, int? muteUserId) {}

  static takeMic({RtcMicOperateType? micOperateType, required List<String> streamIds}) {
    logger.v('take mic: ${streamIds.runtimeType}');
    takeMicOperatorBySelf(micOperateType, streamIds, () {
      reloadStream(micOperateType, streamIds, () {
        isMixed(micOperateType, streamIds);
      });
    });
  }

  ///take mic operator by self
  static takeMicOperatorBySelf(RtcMicOperateType? micOperateType, List<String> streamIds, MicOperatorCallBack callBack) {
    logger.i('take mic operator type: ${streamIds.runtimeType}, $streamIds $micOperateType, ${streamIds.isNotEmpty}');
    if (micOperateType == RtcMicOperateType.up || micOperateType == RtcMicOperateType.down) {
      if (streamIds.isNotEmpty) {
        logger.i('take mic operate: $streamIds $micOperateType');
        callBack?.call();
        return;
      }
      if(micOperateType == RtcMicOperateType.up) {
        YBDZegoExpressWrapper.instance.setZegoClientRole(ZegoRole.Audience);
      }
      return;
    }
    YBDZegoExpressWrapper.instance.setZegoClientRole(micOperateType == RtcMicOperateType.selfUp ? ZegoRole.Guest : ZegoRole.Audience);

  }

  ///上下麦 是否混流
  static isMixed(RtcMicOperateType? micOperateType, List<String> streamIds) {
    //if self is audience, frist judge the stream has mixed
    logger.v('mixed: ${YBDRtcHelper.getInstance().ismixed},');
    if (YBDRtcHelper.getInstance().ismixed) {
      ///是否合理？ 每次回调时
      // YBDZegoRtcApi.startPlayStream('streamID');
      return;
    }
    logger.i(
        'take mic type role type:$micOperateType streamId: $streamIds, ismix:${YBDRtcHelper.getInstance().ismixed}  roletype: ${YBDRtcHelper.getInstance().selfIsBoard}');
    operatePublishStreamWithList(micOperateType: micOperateType, streamId: streamIds, isAboard: false);
  }

  ///start or stop stream
  static reloadStream(RtcMicOperateType? micOperateType, List<String> streamIds, MicOperatorCallBack callBack) {
    if (!streamIds.isAborad()) {
      ///如果是用户 混流只对用户有影响
      callBack?.call();
      return;
    }
    logger.i('take mic operate self: $micOperateType, $streamIds, ${streamIds.isAborad()}');

    ///if self is boardcast
    operatePublishStreamWithList(micOperateType: micOperateType, streamId: streamIds, isAboard: true);
  }

  ///麦位有用户变动时，更新推拉流信息
  /// 自己是主播角色:
  ///        上麦就推流  拉其他主播的流
  ///        下麦就停止推流 拉其他主播的流
  /// 自己是观众
  ///         拉麦位其他主播的流
  static operatePublishStreamWithList({RtcMicOperateType? micOperateType, List<String>? streamId, bool? isAboard}) {
    logger.v('operate publish stream: $micOperateType, isAborad: $isAboard, $streamId');
    if (micOperateType == RtcMicOperateType.down){
      ///如果是被禁言 直接停止拉这些用户的流 不做其他处理
      zegoApiOperate(micOperateType, streamId);
      return;
    }
    if (!isAboard!){
       ///自己是观众 直接stopPublishStream
       YBDZegoExpressWrapper.instance.setZegoClientRole(ZegoRole.Audience);
       ///mute 的当下麦出来
       zegoApiOperate(micOperateType, streamId);
       return;
    }
    if (isAboard) {
      YBDZegoExpressWrapper.instance.setZegoClientRole(micOperateType == RtcMicOperateType.up ? ZegoRole.Guest : ZegoRole.Audience);
      logger.v('operate publish stream: $micOperateType, ${YBDRtcHelper.getInstance().info?.id.toString()}');
      ///mute 的当下麦出来
      zegoApiOperate(micOperateType, streamId);
    }
  }

  static zegoApiOperate(RtcMicOperateType? micOperateType, List<String>? streamId){
    ///拉麦位其他主播的流
    // streamId.forEach((element) {
    //   logger.v('stream play: ${element} , ${element.isNotEmpty} ${element.runtimeType} ${element.toString() != YBDRtcHelper.getInstance().info?.id.toString()}');
    //   if (element.isNotEmpty && element.toString() != YBDRtcHelper.getInstance().info?.id.toString()) {
    //     logger.i('start play stream: $element, $micOperateType');
    //     YBDZegoExpressWrapper.instance.setZegoClientRole(micOperateType == RtcMicOperateType.up ? ZegoRole.Guest : ZegoRole.Audience);
    //   }
    // });
  }
}


 */