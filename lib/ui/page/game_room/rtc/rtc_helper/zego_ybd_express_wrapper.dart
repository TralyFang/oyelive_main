import 'dart:async';
import 'dart:collection';
/*
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:zego_express_engine/src/zego_express_api.dart';
import 'package:zego_express_engine/zego_express_engine.dart';


class YBDZegoExpressWrapper {
  ZegoRole mRole = ZegoRole.Audience;
  String? mRoomID;
  late ZegoUser mZegoUser;


  //本地拉流情况
  // HashSet<String> mActiveStreamSet = HashSet();
  HashMap<String, String> mActiveStreamMap = HashMap();

  List<String> localMutePlayList = [];

  /// Private constructor
  YBDZegoExpressWrapper._internal();

  /// Singleton instance
  static final YBDZegoExpressWrapper instance = YBDZegoExpressWrapper._internal();

  String _streamToUser(String streamID) {
    String userID = streamID.replaceFirst("${mRoomID}_", "");
    mActiveStreamMap.forEach((key, value) {
      if (streamID == value) {
        userID = key;
        return;
      }
    });
    return userID;
  }
  void _streamToUserukMJuoyelive(String streamID) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String? _userToStream(String userID) {
    if(userID == mZegoUser.userID){
      return "${mRoomID}_$userID";
    }
    return mActiveStreamMap[userID];
  }

  /* Main */
  // /**
  //  * 创建引擎
  //  *
  //  * @param ZegoEngineProfile(ZegoUser;ZegoScenario)
  //  */
  static Future<void> createEngineWithProfile(ZegoEngineProfile profile) async {
    ZegoExpressEngine.createEngineWithProfile(profile);
  }

  static Future<void> destroyEngine() async {
    await ZegoExpressEngine.destroyEngine();
  }

  // /**
  //  * 登陆房间
  //  *
  //  * @param roomID 房间ID
  //  * @param userID 用户ID
  //  * @param config 登陆鉴权
  //  */
  Future<void> loginRoom(String roomID, ZegoUser zegoUser,
      ZegoRoomConfig config) async {
    logger.v('login room: $mRoomID');
    if (mRoomID != null && mRoomID != roomID) {
      ZegoExpressEngine.instance.logoutRoom(mRoomID);
    }
    localMutePlayList = [];
    _setZegoEventCallback();
    mRoomID = roomID;
    mZegoUser = zegoUser;
    ZegoExpressEngine.instance.loginRoom(roomID, zegoUser, config: config);
    if (mRole == ZegoRole.Guest) {
      ZegoExpressEngine.instance
          .startPublishingStream(_userToStream(mZegoUser.userID)!);
    }
    ZegoExpressEngine.instance.setAllPlayStreamVolume(100);

  }

  // /**
  //  * 登出房间
  //  *
  //  * @param roomID 房间ID
  //  * 登出房间清空回调，待确认：客户端是否需要依赖于房间登出回调
  //  */
  Future<void> logoutRoom(String roomID) async {
    mRoomID = null;
    ZegoExpressEngine.instance.logoutRoom(roomID);
    _clearZegoEventCallback();
  }
  void logoutRoomZxxWMoyelive(String roomID)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // /**
  //  * 设置角色
  //  *
  //  * @param ZegoRole 观众/嘉宾
  //  * 默认是观众，登出房间不会重置角色。
  //  * 如果上次登陆房间设置为嘉宾，下次登陆房间就会自动推流，如果需要重置，需要手动调用setZegoClientRole重置
  //  */
  void setZegoClientRole(ZegoRole role) {
    logger.v('zego role change: $role, mRoomID:$mRoomID');
    mRole = role;
    if (mRoomID!.isEmpty) {
      return;
    }
    if (role == ZegoRole.Audience) {
      ZegoExpressEngine.instance.stopPublishingStream();
    } else {
      ZegoExpressEngine.instance
          .startPublishingStream(_userToStream(mZegoUser.userID)!);
    }
  }
  void setZegoClientRoleEHT4boyelive(ZegoRole role) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // /**
  //  * 设置音质
  //  *
  //  * @param ZegoAudioConfig 高/中/低音质
  //  */
  Future<void> setAudioConfig(ZegoAudioConfig config) async {
    ZegoExpressEngine.instance.setAudioConfig(config);
  }

  Future<void> startSoundLevelMonitor({ZegoSoundLevelConfig? config}) async {
    ZegoExpressEngine.instance.startSoundLevelMonitor(config: config);
  }
  void startSoundLevelMonitoreoU66oyelive({ZegoSoundLevelConfig? config})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // /**
  //  * 是否闭麦/锁麦/静音
  //  *
  //  * @param mute 是否闭麦/锁麦/静音
  //  * 类似于声网muteLocalAudioStream + muteLocalVideoStream，当前实现是mute就直接停止推流
  //  */
  Future<void> muteLocalUser(bool? mute) async {
    if (mRoomID!.isEmpty) {
      return;
    }
    if (mute!) {
      ZegoExpressEngine.instance.stopPublishingStream();
    } else {
      ZegoExpressEngine.instance
          .startPublishingStream(_userToStream(mZegoUser.userID)!);
    }
  }
  void muteLocalUserJZK6Loyelive(bool? mute)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // /**
  //  * 是否拉麦上用户声音
  //  *
  //  * @param userID 用户ID
  //  * @mute  是否对麦上用户静音
  //  */
  Future<void> mutePlayUserAudio(String userID, bool mute) async {
    if (mRoomID!.isEmpty) {
      return;
    }
    if (mute) {
      localMutePlayList.add(userID);
      ZegoExpressEngine.instance.stopPlayingStream(_userToStream(userID)!);
    } else {
      localMutePlayList.remove(userID);
      ZegoExpressEngine.instance.startPlayingStream(_userToStream(userID)!);
    }
  }

  // /**
  //  * 是否对全部用户静音
  //  *
  //  * @param mute 是否对全部用户静音
  //  */
  Future<void> muteAllPlayStreamAudio(bool mute) async {
    // for (String streamID in mActiveStreamMap.values) {
    //   if (mute) {
    //     ZegoExpressEngine.instance.stopPlayingStream(streamID);
    //   } else {
    //     ZegoExpressEngine.instance.startPlayingStream(streamID);
    //   }
    // }
    ZegoExpressEngine.instance.setAllPlayStreamVolume(mute ? 0 : 100);
  }
  void muteAllPlayStreamAudioyu6Mzoyelive(bool mute)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // /**
  //  * 设置麦上用户列表 用于同步麦位
  //  * @param users 服务器端同步的麦上用户列表
  //    @param muteUsers 服务器端同步麦上用户闭麦列表
  //  */
  void updatePlayUserList(List<String> users, List<String> muteUsers) {
    for (String muteUser in muteUsers) {
      users.remove(muteUser);
    }
    var userSet = HashSet();
    userSet.addAll(users);
    if (mRoomID!.isEmpty) {
      return;
    }
    mActiveStreamMap.forEach((key, value) {
      if (!userSet.contains(key)) {
        ZegoExpressEngine.instance.stopPlayingStream(value);
      }
    });
    if (userSet.contains(mZegoUser.userID)) {
      ZegoExpressEngine.instance.startPublishingStream(
          _userToStream(mZegoUser.userID)!);
    } else {
      ZegoExpressEngine.instance.stopPublishingStream();
    }
  }
  void updatePlayUserListKS1cHoyelive(List<String> users, List<String> muteUsers) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // /**
  //  * 麦上其他用户音浪回调
  //  * @param soundLevels 麦上其他用户和音浪
  //  * Map<UserID, soundLevel> soundLevels
  //  */
  static late void Function(Map<String, double> soundLevels) onRemoteSoundLevelUpdate;

  //
  // /**
  //  * 本地音浪回调
  //  * @param soundLevel 本地采集音浪
  //  */
  static late void Function(double soundLevel) onCapturedSoundLevelUpdate;

  static late void Function(String roomID, ZegoRoomState state, int errorCode,
      Map<String, dynamic> extendedData) onRoomStateUpdate;


  void _setZegoEventCallback() {
    ZegoExpressEngine.onRoomStateUpdate = (String roomID, ZegoRoomState state,
        int errorCode, Map<String, dynamic> extendedData) {
      onRoomStateUpdate(roomID, state, errorCode, extendedData);
    };
    ZegoExpressEngine.onRemoteSoundLevelUpdate =
        (Map<String, double> soundLevels) {
      // logger.v('onRemoteSoundLevelUpdate wapper: $soundLevels');
      Map<String, double> userSoundLevel = Map();
      soundLevels.forEach((key, value) {
        userSoundLevel.putIfAbsent(_streamToUser(key), () => value);
      });
      onRemoteSoundLevelUpdate(userSoundLevel);
    };
    ZegoExpressEngine.onCapturedSoundLevelUpdate = (double soundLevel) {
      onCapturedSoundLevelUpdate(soundLevel);
    };
    ZegoExpressEngine.onDebugError = (errorCode,channelName,info){
      logger.v('errorCode: $errorCode, $channelName, $info');
    };
    ZegoExpressEngine.onRoomStreamUpdate = (String roomID,
        ZegoUpdateType updateType,
        List<ZegoStream> streamList,
        Map<String, dynamic> extendedData) {
      if (updateType == ZegoUpdateType.Add) {
        for (ZegoStream zegoStream in streamList) {
          //麦位上的用户才允许拉他的流
          mActiveStreamMap.putIfAbsent(
              zegoStream.user.userID, () => zegoStream.streamID);
          // 收到流更新自动拉流，在服务器列表更新不及时时可以更快拉到流
          if(!localMutePlayList.contains(zegoStream.user.userID)) {
            ZegoExpressEngine.instance.startPlayingStream(zegoStream.streamID);
          }
        }
      } else {
        for (ZegoStream zegoStream in streamList) {
          {
            mActiveStreamMap.remove(zegoStream.user.userID);
            ZegoExpressEngine.instance.stopPlayingStream(zegoStream.streamID);
            if(localMutePlayList.contains(zegoStream.user.userID)){
                localMutePlayList.remove(zegoStream.user.userID);///下麦的时候 更新一下
            }
          }
        }
      }
    };
  }
  void _setZegoEventCallbackCL2CLoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

    // * 清空callback
    void _clearZegoEventCallback() {
      ZegoExpressEngine.onRoomStateUpdate = null;
      ZegoExpressEngine.onRemoteSoundLevelUpdate = null;
      ZegoExpressEngine.onCapturedSoundLevelUpdate = null;
      ZegoExpressEngine.onRoomStreamUpdate = null;
    }
  }

  enum

  ZegoRole

  {

  /// 观众
  Audience

  ,

  ///嘉宾
  Guest
}


 */