/*
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_event_handler.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_mic_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:zego_express_engine/zego_express_engine.dart';

typedef RoomStateUpdateCallback = void Function(
    String roomID, ZegoRoomState state, int errorCode, Map<String, dynamic>);
typedef CaptureSoundLevelUpdateCallBack = void Function(double soundLevel);
typedef RemoteSoundLevelUpdateCallBack = void Function(Map<String, double> soundLevels);
typedef RoomStreamUpdate = void Function(
    String room, ZegoUpdateType type, List<ZegoStream> streamList, Map<String, dynamic> extendedData);

class YBDZegoEventHandle extends YBDBaseRtcEventHandler {
  RoomStateUpdateCallback? _onRoomStateUpdate;
  CaptureSoundLevelUpdateCallBack? _onCaptureSoundLevelUpdate;
  RemoteSoundLevelUpdateCallBack? _onRemoteSoundLevelUpdate;

  ///房间流数量变化
  RoomStreamUpdate? _onRoomStreaUpDate;

  void _initCallBack() {
    ZegoExpressEngine.onRoomStateUpdate =
        (String roomID, ZegoRoomState state, int errorCode, Map<String, dynamic> extendedData) {
      logger.v('room state update, state: $state, errorCode: $errorCode, roomID: $roomID');
      _onRoomStateUpdate?.call(roomID, state, errorCode, extendedData);
    };

    ZegoExpressEngine.onCapturedSoundLevelUpdate = (double soundLevel) {
      logger.v('capture Sound level: $soundLevel');
      _onCaptureSoundLevelUpdate?.call(soundLevel);
    };

    ZegoExpressEngine.onRemoteSoundLevelUpdate = (Map<String, double> soundLevels) {
      logger.v('remote Sound levels: ${soundLevels}');
      _onRemoteSoundLevelUpdate?.call(soundLevels);
    };

    ZegoExpressEngine.onRoomStreamUpdate =
        (String room, ZegoUpdateType type, List<ZegoStream> streamList, Map<String, dynamic> extendedData) {
      logger.v('room stream update: $room, $type, $streamList, $extendedData');
      // YBDGameMicUtil.updateStream(room, type, streamList, extendedData);
      _onRoomStreaUpDate?.call(room, type, streamList, extendedData);
    };
    //
    // ZegoExpressEngine.onRemoteAudioSpectrumUpdate = (Map<String, List<double>> audioSpectrums) {
    //   _onRemoteAudioSpectrumUpdate?.call(audioSpectrums);
    // };
  }
  void _initCallBackKE2beoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void setZegoEventCallBack(
      {CaptureSoundLevelUpdateCallBack? onCaptureSoundLevelUpdate,
      RemoteSoundLevelUpdateCallBack? onRemoteSoundLevelUpdate}) {
    if (onCaptureSoundLevelUpdate != null) {
      _onCaptureSoundLevelUpdate = onCaptureSoundLevelUpdate;
    }

    if (onRemoteSoundLevelUpdate != null) {
      _onRemoteSoundLevelUpdate = onRemoteSoundLevelUpdate;
    }
  }
}


 */