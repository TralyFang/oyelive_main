import 'dart:async';

import 'dart:convert';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/set_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart' as v;
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_event_handler.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_mic_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/room/entity/room_ybd_operate_info.dart';

// /**
//  *  上下麦
//     禁麦（被禁麦人不能发流，只能下发消息由本人操作禁止音频采集，发送）muteLocalAudioStream，enableLocalAudio（需要配合使用达到双保险）
//     禁言（单独不外放某个人音频，不影响其他人接收）muteRemoteAudioStream
//     全部禁言 muteAllRemoteAudioStreams
//
//     频道场景：setChannelProfile
//     创建RtcEngine：createWithConfig
//     加入或切换频道自动订阅所有远端音频流
//     加入频道（房间）：joinChannel
//     切换频道（房间）：switchChannel
//     设置角色：setClientRole
//     离开频道（房间）：leaveChannel
//     销毁RtcChannel：destroy
//     销毁RtcEngine：destroy
//
//  * */

class YBDAgoraRtcApi implements BaseRtcApi {
  /// 维护上次切换的禁麦状态
  bool? _lastMuted;

  static createEngine() async {
    tryCatch(() async {
      // if (YBDRtcHelper.getInstance().agoraEngine != null) {
      //   YBDRtcHelper.getInstance().getRtcApi.leaveChannel();
      //   await YBDRtcHelper.getInstance().agoraEngine?.destroy();
      //   YBDRtcHelper.getInstance().agoraEngine = null;
      // };
      // 创建实例
      // final agoraPath = await v.YBDLogUtil.agoraLogPath();
      // LogConfig logConfig = LogConfig(filePath: agoraPath, level: LogLevel.Info);
      // logger.v('agora LogConfig path: $agoraPath');
      /*
      YBDRtcHelper.getInstance().agoraEngine = await RtcEngine.createWithConfig(RtcEngineConfig(Const.AGORA_APP_ID));

       */
      YBDRtcHelper.getInstance().agoraEngine = await RtcEngine.create(Const.AGORA_APP_ID);;
      logger.v('agora LogConfig path');
      YBDRtcHelper.getInstance().getRtcApi.setEventHandler(null);

      ///TODO  语聊房或者游戏房退出通道失败 后 可能存在一直在通道不退出的场景
      YBDRtcHelper.getInstance().agoraEngine?.leaveChannel();

      // 启用说话者音量提示，false 关闭本地人声检测
      await YBDRtcHelper.getInstance().agoraEngine!.enableAudioVolumeIndication(500, 3, false);

      // 设置频道属性
      await YBDRtcHelper.getInstance().agoraEngine!.setChannelProfile(ChannelProfile.LiveBroadcasting);

      _setAudioProfile();

      await YBDRtcHelper.getInstance().agoraEngine!.setClientRole(ClientRole.Audience);
    });
  }

  ///set audioProflie
  static _setAudioProfile() {
    final store = YBDCommonUtil.storeFromContext(context: Get.context)!;
    Map? result;
    try {
      result = jsonDecode(store.state.configs!.roomOperate!);
    } on Error catch (error) {
      logger.e('audio profile error: $error');
    }
    YBDRoomOperateInfo info = YBDRoomOperateInfo.get(result);
    //等于1 设置的媒体音量  2 通话音量
    logger.v('set audio profile :${info.audioProfile} ${YBDRtcHelper.getInstance().agoraEngine}');
    switch (info.audioProfile) {
      case 1:
      case 2:
        YBDRtcHelper.getInstance().agoraEngine?.setAudioProfile(AudioProfile.SpeechStandard,
            info.audioProfile == 1 ? AudioScenario.GameStreaming : AudioScenario.ChatRoomGaming);
        break;
      case 3:
      case 4:
        YBDRtcHelper.getInstance().agoraEngine?.setAudioProfile(
            AudioProfile.Default, info.audioProfile == 3 ? AudioScenario.GameStreaming : AudioScenario.ChatRoomGaming);
        break;
    }
  }

  @override
  void enableLocalAudio({required bool enabled}) {
    YBDRtcHelper.getInstance().agoraEngine!.enableLocalAudio(enabled);
  }
  void enableLocalAudioexZTOoyelive({required bool enabled}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void joinChannel({required String? token, required String? channelName}) async {
    if (token == null) return;
    logger.v(
        '${YBDRtcHelper.getInstance().info!.id} join channel $channelName, ${YBDRtcHelper.getInstance().agoraEngine} token: $token');
    String? optionalInfo =
        (YBDRtcHelper.getInstance().info!.id == int.tryParse(channelName!)) ? "{'owner':true,'bitrate':100};" : null;
    YBDRtcHelper.getInstance()
        .agoraEngine!
        .joinChannel(token, channelName, optionalInfo, YBDRtcHelper.getInstance().info?.id ?? 0)
        .catchError((error) {
      v.logger.v(' join channnel error: $error');
    });
    YBDRtcHelper.getInstance().agoraEngine?.setEnableSpeakerphone(true);
  }
  void joinChanneljdgaJoyelive({required String? token, required String? channelName})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void leaveChannel() async {
    logger.i('${YBDRtcHelper.getInstance().info!.id} leave channel');
    await YBDRtcHelper.getInstance().agoraEngine!.leaveChannel();
    // YBDRtcHelper.getInstance().agoraEngine.destroy();
  }
  void leaveChannel0k6BDoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void muteAllRemoteAudioStreams(bool muted) {
    // YBDRtcHelper.getInstance().agoraEngine.muteAllRemoteAudioStreams(muted);
    YBDRtcHelper.getInstance().agoraEngine!.adjustPlaybackSignalVolume(muted ? 0 : 100);
  }

  @override
  void muteLocalAudioStream(bool muted) {
    YBDRtcHelper.getInstance().agoraEngine!.muteLocalAudioStream(muted);
  }
  void muteLocalAudioStreamqmf2soyelive(bool muted) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void muteRemoteAudioStream(int uid, bool muted) {
    YBDRtcHelper.getInstance().agoraEngine!.muteRemoteAudioStream(uid, muted);
  }

  @override
  void switchChannel({required String token, required String channelName}) {
    YBDRtcHelper.getInstance().agoraEngine!.switchChannel(token, channelName);
  }

  @override
  void takeMic({RtcMicOperateType? micOperateType, List<String>? streamIds}) {
    logger.i('take mic $micOperateType $streamIds');
    //禁言的时候 自己是主播就切成观众
    if (micOperateType == RtcMicOperateType.down) {
      logger.v('mute sb: ${streamIds!.isAborad()}');
      if (streamIds.isAborad()) {
        setRole(false);
      }
      return;
    }
    //up
    if (!streamIds!.isAborad()) {
      logger.v('self is not boardcast');
      setRole(false);
      return;
    }
    logger.v('role change: ${(micOperateType == RtcMicOperateType.up || micOperateType == RtcMicOperateType.selfUp)}');
    setRole((micOperateType == RtcMicOperateType.up || micOperateType == RtcMicOperateType.selfUp));
  }
  void takeMicJRA3Woyelive({RtcMicOperateType? micOperateType, List<String>? streamIds}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  setRole(bool isBoard) {
    logger.v('set agora role: $isBoard');
    YBDRtcHelper.getInstance().agoraEngine!.setClientRole(isBoard ? ClientRole.Broadcaster : ClientRole.Audience);
  }

  @override
  void muteMic({bool? muted, int? muteUserId, bool? mutedByAboardcast}) {
    /**
     * 是否应该在这里维护频繁调用的问题？
     * 禁麦：应成本要求，需要将角色切换为观众
     * 解麦：如果还在麦位上，那就切回主播角色
     * */

    /// 该功能只能由当前用户操作，不能由别人操作； 并且是在麦位上⚠️
    if (muteUserId == YBDRtcHelper.getInstance().info?.id) {
      if (_lastMuted == muted) {
        // 避免频繁调用
        logger.v("recall don't setClientRole " +
            "muteMic: $muted, " +
            "userid: $muteUserId, " +
            "role: ${muted! ? 'ClientRole.Audience' : 'ClientRole.Broadcaster'}");
        return;
      }

      // YBDRtcHelper.getInstance().agoraEngine.muteLocalAudioStream(muted);
      // YBDRtcHelper.getInstance().agoraEngine.enableLocalAudio(!muted);
      // if (muted) {
      //   YBDRtcHelper.getInstance().agoraEngine.setClientRole(ClientRole.Audience);
      // }else {
      //   YBDRtcHelper.getInstance().agoraEngine.setClientRole(ClientRole.Broadcaster);
      // }
      _lastMuted = muted;
    }
    logger.v("muteMic: $muted, " +
        "userid: $muteUserId, " +
        "role: ${muted! ? 'ClientRole.Audience' : 'ClientRole.Broadcaster'}");
  }
  void muteMicjmf0hoyelive({bool? muted, int? muteUserId, bool? mutedByAboardcast}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void setEventHandler(YBDBaseRtcEventHandler? handler) {
    logger.v('handler init');
    YBDRtcHelper.getInstance().agoraEngine?.setEventHandler(RtcEngineEventHandler(connectionStateChanged: (type, reason) {
          /// 网络状态
          RtcConnectionState state = RtcConnectionState.failed;
          if (type == ConnectionStateType.Connected) {
            state = RtcConnectionState.connected;
          } else if (type == ConnectionStateType.Connecting) {
            state = RtcConnectionState.connecting;
          }
          // handler.connectionStateChanged?.call(state);
        }, error: (ErrorCode code) {
          /// 出现异常
          logger.v('code error: $code');
          // YBDToastUtil.toast('error code: $code');
          // handler.error?.call(code.index);
        }, clientRoleChanged: (ClientRole previous, ClientRole current) {
          /// 角色切换变更
          logger.v('role changed: $previous $current');
        }, userJoined: (uid, elapsed) {
          /// 有用户加入频道
          // handler.userJoined?.call(uid);
        }, userOffline: (uid, reason) {
          /// 有用户离开频道
          // handler.userOffline?.call(uid);
        }, audioVolumeIndication: (speakers, totalVolume) {
          /// 声浪回调
          List<YBDRtcVolumeInfo> users = speakers
              .map((user) => YBDRtcVolumeInfo(user.volume, user.uid == 0 ? YBDRtcHelper.getInstance().info!.id : user.uid))
              .toList();
          // handler.audioVolumeIndication?.call(users);
          YBDGameMicUtil.zegoSoundLevelUpdate(users);
        }, channelMediaRelayStateChanged: (state, code) {
          /// 转发流状态变更
        }, channelMediaRelayEvent: (e) {
          /// 转发流事件
        }, apiCallExecuted: (ErrorCode error, String api, String result) {
          /// api出现异常
          // handler.error?.call(error.index);
          // YBDToastUtil.toast('api error: $error, $api, $result');
        }, joinChannelSuccess: (String channel, int uid, int elapsed) {
          v.logger.v('join channel success: ${YBDObsUtil.instance().routeSets.subContain(YBDNavigatorHelper.game_room_page)}');
          // YBDToastUtil.toast('join channel success');
          if (YBDObsUtil.instance().routeSets.subContain(YBDNavigatorHelper.game_room_page) ||
              YBDRtcHelper.getInstance().roomId == GameConst.defaultValue) return;
          YBDNavigatorHelper.navigateTo(Get.context, YBDNavigatorHelper.game_room_page);
        }));
  }
  void setEventHandlerDZZnNoyelive(YBDBaseRtcEventHandler? handler) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
