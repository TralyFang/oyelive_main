import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/set_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart' as v;
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_config.dart';
import 'package:oyelive_main/ui/page/game_room/ext/int_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/zego_ybd_express_wrapper.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_event_handle/zego_ybd_event_handle.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_event_handler.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/zego_ybd_rtc_api_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_commom_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_mic_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
// import 'package:zego_express_engine/zego_express_engine.dart';

import '../server/base_ybd_event_handler.dart';

void tryCatch(VoidCallback callback) {
  try {
    callback();
  } catch (e) {
    logger.e('rtc error: $e');
  }
}

typedef MicOperatorCallBack = void Function();

/// zego rtc technological process
/// Zego engin (app 生命周期一致) ---加入房间、订阅房间..--->  login room (token 鉴权) ---> start play stream(进房就主动拉流)
///
/// 麦上主播 A B C D
///  A 主播 拉流 [ startPlayStream[StreamB]] [startPlayStream[StreamC]] [startPlayStream[StreamD]]
/// --锁麦->
///       发报文-> 操作成功更新麦位状态
/// --上麦-> 是否锁麦(app判断? ) [startPublishStream]
/// --下麦->
///       当前主播: [stopPublishStream]
///       其他主播: [stopPlayStream]
///       观众: [拉的混流ID 服务端自动剔出下麦主播]

/// ---禁麦场景:
/// 场景一: 管理员/房主禁麦其他上麦主播 (与server交互, UI更新状态)
/// 场景二: 麦上主播自己禁麦自己 (与server交互, UI更新状态)
///       双保险： 广播消息， 被禁麦的主播 [mutePublishStream]
///               其他麦上主播： [mutePlayStream]
///               麦下观众: [拉取的混流中 自动剔出这个被屏蔽用户流]
///
/// 场景三: 非房主用户(其他主播或观众) 禁麦某个主播 (异常退出)
///      主播：操作成功 [mutePlayStream]
///      观众: [混流怎么做, A用户禁麦 B主播的声音，操作成功服务端针对当前用户返回 新的剔出B主播的混流ID 其他用户还是沿用以前的]
///
/// 音浪监听:
///    loginRoom之后默认开启

/*
class YBDZegoRtcApi extends BaseRtcApi {
  ///engine init
  static createEngine() async {
    // if (ZegoExpressEngine.instance != null) return;
    tryCatch(() async {
      ///创建前先销毁 防止用户 [leaveChannel]失败 或者未调用这个 导致再次进入失败
      await YBDZegoExpressWrapper.destroyEngine();
      YBDZegoExpressWrapper.createEngineWithProfile(GameConst.ZegoAppId.profile());
      v.logger.v('zego engin create');
      YBDGameRoomConfig config = YBDGameCommomUtil.getGameConfig();
      YBDZegoExpressWrapper.instance.setAudioConfig(ZegoAudioConfig(config.bitrate, ZegoAudioChannel.Mono, ZegoAudioCodecID.Low3));

      ///开启音浪监听
      YBDZegoExpressWrapper.instance.startSoundLevelMonitor();

      // YBDRtcHelper.getInstance().getRtcApi.setEventHandler(YBDBaseRtcEventHandler());
    });
  }

  /******************登录登出房间*********************/
  ///login room
  @override
  void joinChannel({String? token, String? channelName}) {
    logger.v('enter room: $channelName, $token');
    YBDGameRoomConfig config = YBDGameCommomUtil.getGameConfig();
    logger.v(
        'login room channelName: ${config.maxMemberCount}');
    tryCatch(() async {
      logger.v(
          'login room channelName: ${YBDRtcHelper.getInstance().getRoomId()}, ${YBDRtcHelper.getInstance().info?.id.toString()}, engin:${YBDZegoExpressWrapper.instance}');
      YBDZegoExpressWrapper.instance.loginRoom(YBDRtcHelper.getInstance().getRoomId()!, ZegoUser.id(YBDRtcHelper.getInstance().info?.id.toString() ?? ''), ZegoRoomConfig(config.maxMemberCount, true, token!));
    });
  }
  void joinChannelx3ipBoyelive({String? token, String? channelName}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void leaveChannel() {
    tryCatch(() {
      logger.v('logout room: ${YBDRtcHelper.getInstance().getRoomId().toString()}');
      YBDZegoExpressWrapper.instance.logoutRoom(YBDRtcHelper.getInstance().getRoomId().toString());
    });
  }
  void leaveChannel5hyc5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /********************推拉流***********************/
  @override
  void takeMic({RtcMicOperateType? micOperateType, List<String>? streamIds}) {
    logger.i(
        'take mic type:$micOperateType streamId: $streamIds, ismix:${YBDRtcHelper.getInstance().ismixed} ${streamIds.runtimeType}');
    YBDZegoRtcApiHelper.takeMic(micOperateType: micOperateType, streamIds: streamIds!);
  }
  void takeMicS988boyelive({RtcMicOperateType? micOperateType, List<String>? streamIds}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///start publish stream
  static startPublishStream() {
    //发流前设置bitrate
    // ZegoExpressEngine.instance.setAudioConfig(ZegoAudioConfig(bitrate, channel, codecID))
    logger.i('${YBDRtcHelper.getInstance().info!.id?.toString()} start publish stream');
    ZegoExpressEngine.instance.startPublishingStream(YBDRtcHelper.getInstance().info!.id?.toString() ?? '');
  }

  ///stop publish stream
  static stopPublishStream() {
    logger.i('${YBDRtcHelper.getInstance().info!.id?.toString()} stop publish stream');
    ZegoExpressEngine.instance.stopPublishingStream();
  }

  /// start play stream
  /// [streamID] 混流后 需要定义好规则
  /// 麦上拉单流 观众走L3
  static startPlayStream(String streamID) {
    logger.i('start paly $streamID');
    ZegoExpressEngine.instance.startPlayingStream(streamID);
  }

  /// stop play stream
  ///
  static stopPlayStream(String streamID) {
    logger.i('stop play$streamID');
    ZegoExpressEngine.instance.stopPlayingStream(streamID);
  }

  /******************禁麦**************************/
  @override
  void muteMic({required bool muted, required int muteUserId, bool mutedByAboardcast = true}) {
    /// 禁麦的是主播, 通知报文默认都是禁麦主播
    mutedByAboardcast
        ? YBDZegoRtcApiHelper.muteAboardcast(muted, muteUserId)
        : YBDZegoRtcApiHelper.muteAudience(muted, muteUserId);
  }
  void muteMic1vIG5oyelive({required bool muted, required int muteUserId, bool mutedByAboardcast = true}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///mute audio stream
  /// 禁止发送流到远端, 但是会发到sdk
  static mutePublishStream(bool? ismute) {
    // ZegoExpressEngine.instance.mutePublishStreamAudio(ismute);
    YBDZegoExpressWrapper.instance.muteLocalUser(ismute);
  }

  /// mute Play Stream
  /// 是否接收指定音频流
  static mutePlayStream(String streamID, bool ismute) {
    ZegoExpressEngine.instance.mutePlayStreamAudio(streamID, ismute);
  }

  /******************开启音浪监听***********************/
  /// 开启音浪监听
  /// 不设置config 默认监听时间段100-3000ms
  // static startSoundLevelMonitor() {
  //   ZegoExpressEngine.instance.startSoundLevelMonitor();
  // }

  @override
  void enableLocalAudio({bool? enabled}) {
    // TODO: implement enableLocalAudio
  }
  void enableLocalAudioa3y2noyelive({bool? enabled}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void muteAllRemoteAudioStreams(bool muted) {
    // TODO: implement muteAllRemoteAudioStreams
    YBDZegoExpressWrapper.instance.muteAllPlayStreamAudio(muted);
  }

  @override
  void muteRemoteAudioStream(int uid, bool muted) {
    v.logger.v('muteRemoteAudioStream: $uid, $muted');
    if(uid != null) {
      YBDZegoExpressWrapper.instance.mutePlayUserAudio(uid.toString(), muted);
    }
  }

  @override
  void setEventHandler(YBDBaseRtcEventHandler? handler) {
    v.logger.v('set zego handle');
    YBDZegoExpressWrapper.onRoomStateUpdate =
        (String roomID, ZegoRoomState state, int errorCode, Map<String, dynamic> extendedData) {
      v.logger.v('🚩 🚪 Room state update, state: $state, errorCode: $errorCode, roomID: $roomID');

      /// 网络状态
      RtcConnectionState aState = RtcConnectionState.failed;
      if (state == ZegoRoomState.Connected && errorCode == 0) {
        aState = RtcConnectionState.connected;
        if (YBDObsUtil.instance().routeList.subContain(YBDNavigatorHelper.game_room_page)) return;
        YBDNavigatorHelper.navigateTo(Get.context, YBDNavigatorHelper.game_room_page);
      } else if (state == ZegoRoomState.Connecting) {
        aState = RtcConnectionState.connecting;
      }
      handler!.connectionStateChanged?.call(aState);
    };

    /// 房间音量波动
    YBDZegoExpressWrapper.onCapturedSoundLevelUpdate = (double soundLevel) {
      /// 本地音量回调 [0, 100] 同步转换为声网的取值范围 [0, 255]
      // v.logger.v('onCapturedSoundLevelUpdate : $soundLevel');
      if(soundLevel == 0)return;
      handler!.audioVolumeIndication?.call([YBDRtcVolumeInfo((soundLevel * 2.55).toInt(), 0)]);
      YBDGameMicUtil.zegoSoundLevelUpdate([YBDRtcVolumeInfo((soundLevel * 2.55).toInt(), YBDRtcHelper.getInstance().info!.id)]);
    };
    YBDZegoExpressWrapper.onRemoteSoundLevelUpdate = (Map<String, double> soundLevels) {
      /// 远程音量回调
      // v.logger.v('onRemoteSoundLevelUpdate : $soundLevels');
      List<YBDRtcVolumeInfo> users = [];
      soundLevels.forEach((key, value) {
        // v.logger.v('onRemoteSoundLevelUpdate key value: $key , $value');
        if(value > 0) {
          users.add(YBDRtcVolumeInfo((value * 2.55).toInt(), int.tryParse(key) ?? -1));
        }
      });
      // v.logger.v('onRemoteSoundLevelUpdate : $users, ${users.isEmpty}');
      handler!.audioVolumeIndication?.call(users);
      if(users.isEmpty)return;
      YBDGameMicUtil.zegoSoundLevelUpdate(users);
    };

    // ZegoExpressEngine.onRoomStreamUpdate =
    //     (String room, ZegoUpdateType type, List<ZegoStream> streamList, Map<String, dynamic> extendedData) {
    //   logger.v('room stream update: $room, $type, $streamList, $extendedData');
    //   logger.v('room stream update: ${streamList.first.streamID}');
    //   YBDGameMicUtil.updateStream(room, type, streamList, extendedData);
    // };
    //
    // ZegoExpressEngine.onPublisherStateUpdate =
    //     (String streamID, ZegoPublisherState state, int errorCode, Map<String, dynamic> extendedData) {
    //   logger.v('publish state update: $streamID, $state, $errorCode, $extendedData');
    // };

    /// 房间用户数量变更
    // ZegoExpressEngine.onRoomUserUpdate = (String roomID, ZegoUpdateType updateType, List<ZegoUser> userList) {
    //   if (updateType == ZegoUpdateType.Add) {
    //     /// 进入房间用户
    //     if (handler.userJoined != null) {
    //       userList.forEach((element) {
    //         handler.userJoined?.call(int.tryParse(element.userID) ?? -1);
    //       });
    //     }
    //   } else if (updateType == ZegoUpdateType.Add) {
    //     /// 退出房间用户
    //     if (handler.userOffline != null) {
    //       userList.forEach((element) {
    //         handler.userOffline?.call(int.tryParse(element.userID) ?? -1);
    //       });
    //     }
    //   }
    // };
  }
  void setEventHandlerpZeKpoyelive(YBDBaseRtcEventHandler? handler) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}


 */