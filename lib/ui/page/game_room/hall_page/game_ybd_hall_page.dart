import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' as sc;
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oye_tool_package/widget/draggable_float_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/route_ybd_push_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_category_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_enter_entity.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/game_ybd_hall_list.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/widget/game_ybd_hall_games.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/widget/game_ybd_hall_start.dart';
import 'package:oyelive_main/ui/page/game_room/ludo/widget/method_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/tp_ybd_cache.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/go_ybd_tab_event.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';

import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';

import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_event.dart';

final TP_GO = "tpGo";

class YBDGameHallPage extends StatefulWidget {
  final ScrollController scrollController;

  YBDGameHallPage(this.scrollController);

  @override
  _YBDGameHallPageState createState() => _YBDGameHallPageState();
}

class _YBDGameHallPageState extends State<YBDGameHallPage> with TickerProviderStateMixin {
  /// 追踪列表数组
  List<YBDGameRoomEnterDataRows> _data = [];
  int _gameSelectedIndex = 0; // 游戏选择的索引，默认选择第一个
  List<YBDGameCategoryData?> _gamesData = [];
  YBDGameCategoryData? _selectedCategory;

  //加载的页数
  int _page = 1;

  /// 是否为初始状态
  bool _isInitState = true;

  /// 列表刷新控制器
  RefreshController _refreshController = RefreshController();

  @override
  bool get wantKeepAlive => true;

  var containerKey = GlobalKey();
  var floatController = DraggableFloatController();
  TabController? _tabController;
  int _tabControllerLength = 0;

  GlobalKey<YBDGameHallGamesState> scrollKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _onRefresh(_page);
    logger.v("YBDGameHallPage initState");

    eventBus.on<YBDGoTabEvent>().listen((YBDGoTabEvent event) async {
      logger.v('game hall page scroll pos: ${event.postion}, index: ${event.subIndex}, type: ${event.type}');

      ///先判断游戏是否隐藏
      bool gameshow = await ConfigUtil.shouldShowGame(context: context, allPlatform: true);
      if (!gameshow || _gamesData.isEmpty) return;
      if (event.postion != null && event.postion == 'home' && event.type != null) {
        scrollGameType(event.type);
      }
    });

    YBDTPCache.downloadCache(update: true);
  }
  void initStateeYTeuoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  scrollGameType(String? type) {
    for (int i = 0; i < _gamesData.length; i++) {
      if (_gamesData[i]!.subCategory!.contains(type!) && _gamesData[i]!.status == 1) {
        logger.v('22.6.16---game sub category: ${_gamesData[i]!.subCategory} type: $type  index:$i');
        scrollKey.currentState!.scroller(i + 1);
        _tabController!.animateTo(i);
        _gameSelectedIndex = i;
        _onRefresh(1);
        return;
      }
    }
  }

  @override
  void dispose() {
    logger.v("YBDGameHallPage dispose");
    _tabController?.dispose();
    super.dispose();
  }
  void disposegnid2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 根据配置初始化TabController是否带game模块
  _initTabController(int length) {
    if (length == _tabControllerLength && _tabController != null) {
      return;
    }
    _tabController?.dispose();
    _tabController = null;
    _tabController = TabController(initialIndex: _gameSelectedIndex, length: length, vsync: this);
    _tabController!.addListener(() {
      if (_tabController!.indexIsChanging) {
        return; // 还是改变中，就不切换了
      }
      if (_gameSelectedIndex == _tabController!.index) {
        return; // 重复点击了
      }
      _gameSelectedIndex = _tabController!.index;
      _page = 1;
      _onRefresh(_page);
      setState(() {});
      logger.v('game hall page selectedIndexCallback Listener: $_gameSelectedIndex');
    });
    logger.v('_initTabController${_tabController.hashCode}');
    _tabControllerLength = length;
  }

  BuildContext? lockContext;

  @override
  Widget build(BuildContext context) {
    // /// Widgets that mix AutomaticKeepAliveClientMixin into their State must call super.build() but must ignore the return value of the superclass.
    // super.build(context);
    if (_isInitState) {
      return YBDLoadingCircle();
    }
    if (_gamesData.isEmpty) {
      return GestureDetector(
        onTap: () {
          _isInitState = true;
          setState(() {});
          _onRefresh(_page);
        },
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                "assets/images/status/status_empty.png",
                width: 382.px,
              ),
              SizedBox(
                height: 30.px,
              ),
              Text(
                "${translate('no_data')}, click to refresh",
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
            ],
          ),
        ),
      );
    }
    return DefaultTabController(
      length: (_gamesData.isEmpty) ? 1 : _gamesData.length,
      key: containerKey,
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.only(top: 100.px as double),
            decoration: !_gamesData.isEmpty && _gamesData[_gameSelectedIndex]!.subCategory == TP_GO
                ? null
                : YBDActivitySkinRoot().curAct().activityBgDecoration(),
            child: Column(
              children: [
                // 有数据才展示入口：游戏分类
                if (!(null == _gamesData || _gamesData.isEmpty))
                  YBDGameHallGames(
                      key: scrollKey,
                      models: _gamesData,
                      selectedIndex: _gameSelectedIndex,
                      tabController: _tabController,
                      selectedIndexCallback: (index) {
                        _tabController!.animateTo(index);
                        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                          YBDEventName.CLICK_EVENT,
                          location: YBDLocationName.GAME_HALL_PAGE,
                          itemName:
                              'select game index:$index, ${_gamesData != null ? _gamesData[index]!.category : 'unknown'}',
                        ));
                        logger.v('game hall page selectedIndexCallback: $index');

                        // if (!_gamesData.isEmpty) {
                        //   eventBus.fire(YBDGameHallBgEvent()..showTpGo = _gamesData[index].subCategory == TP_GO);
                        // }
                      }),
                // TODO: 有数据才展示入口：快速加入，创建
                // if (!(null == _gamesData || _gamesData.isEmpty))

                if (!_gamesData.isEmpty && _gamesData[_gameSelectedIndex]!.subCategory != TP_GO)
                  YBDGameHallStart(canJoinRoom: !(null == _gamesData || _gamesData.isEmpty)),
                if (!_gamesData.isEmpty)
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(top: 10.dp720 as double),
                      child: TabBarView(
                        physics: NeverScrollableScrollPhysics(),
                        controller: _tabController,
                        children: [
                          // 默认有一个空的，方便没有数据的情况下还能刷新
                          if (_gamesData.isEmpty)
                            YBDGameHallList(
                              selectedCategory: _selectedCategory,
                              gamesData: _gamesData,
                              refreshCallback: (page) {
                                _page = page;
                                _onRefresh(_page);
                              },
                            ),
                          ..._gamesData
                              .map((e) => YBDGameHallList(
                                    selectedCategory: e,
                                    gamesData: _gamesData,
                                    refreshCallback: (page) {
                                      _page = page;
                                      _onRefresh(_page);
                                    },
                                  ))
                              .toList(),
                        ],
                      ),
                    ),
                  ),
                // if (!_gamesData.isEmpty && _gamesData[_gameSelectedIndex].subCategory == TP_GO)
              ],
            ),
          ),
          draggableFloatWidget(),
        ],
      ),
    );
  }
  void buildBbBnEoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 可拖拽浮窗
  DraggableFloatWidget draggableFloatWidget() {
    return DraggableFloatWidget(
      bottom: sc.ScreenUtil().setWidth(200),
      right: sc.ScreenUtil().setWidth(20),
      child: YBDGameEvent(
        YBDLocationName.GAME_HALL_PAGE,
        didChangeWidgetCallback: () {
          floatController.updateWidget();
        },
      ),
      controller: floatController,
      containerKey: containerKey,
    );
  }

  /// 获取游戏appkey
  _getZegoMgAppKey() async {
    if (YBDGameUtils.getInstance()!.mgAppKeyData != null) {
      return;
    }
    var resp = await ApiHelper.getZegoMgAppKey(context);
    if (resp?.code == Const.HTTP_SUCCESS_NEW && resp?.data != null) {
      YBDGameUtils.getInstance()!.mgAppKeyData = resp!.data;
    }
  }

  /// 游戏分类列表
  Future<YBDGameCategoryData?> _gameCategorys() async {
    List<YBDGameCategoryData?>? gameCategory = _gamesData;
    if (_page == 1 && gameCategory.isEmpty) {
      // 刷新了就跟着刷新
      gameCategory = await ApiHelper.gameCategorySearch(context);
    }
    if (gameCategory != null && gameCategory.length > _gameSelectedIndex) {
      _gamesData = gameCategory;
      _selectedCategory = gameCategory[_gameSelectedIndex];
      _initTabController(gameCategory.length);
      return _selectedCategory;
    }
    return null;
  }

  /// 列表刷新响应方法
  _onRefresh(int page) async {
    // 获取appkey
    _getZegoMgAppKey();

    var gameCategory = await _gameCategorys();

    /// 记录所选的游戏类别
    YBDRtcHelper.getInstance().selectedGame = gameCategory;
    if (gameCategory == null) {
      _refreshController.refreshCompleted();
      _cancelInitState();
      return;
    }
    // 是当前页面则刷新界面
    _cancelInitState();
    logger.v('game hall page refresh: ${YBDRoutePushUtil.type}');

    if (YBDRoutePushUtil.type != '-1') {
      logger.v('init scroll with type: ${YBDRoutePushUtil.type}');
      Future.delayed(Duration(milliseconds: 500), () {
        scrollGameType(YBDRoutePushUtil.type);
        YBDRoutePushUtil.dispose();
      });
    }
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }
}
