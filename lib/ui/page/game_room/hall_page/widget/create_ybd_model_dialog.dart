
/*
 * @Author: William
 * @Date: 2022-11-21 15:52:00
 * @LastEditTime: 2022-12-05 11:53:12
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/game_room/hall_page/widget/create_model_dialog.dart
 */
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_lobby/entity/game_ybd_lobby_entity.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_tips.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

class YBDCreateModelDialog extends StatefulWidget {
  final Function? cancelCallback;
  final Function(String)? okCallback;
  const YBDCreateModelDialog({Key? key, this.cancelCallback, this.okCallback}) : super(key: key);

  @override
  State<YBDCreateModelDialog> createState() => _YBDCreateModelDialogState();
}

class _YBDCreateModelDialogState extends State<YBDCreateModelDialog> {
  String curModel = '';
  String? curCurrency = 'bean';
  @override
  void initState() {
    super.initState();
    curModel = 'Primary mode';
  }
  void initStateg893joyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        // mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 25.dp750),
          Container(
            width: 600.dp750,
            height: 388.dp750,
            // color: Colors.pink.withOpacity(0.3),
            child: SingleChildScrollView(
              child: Column(children: [
                Row(children: [SizedBox(width: 8.dp750), _text('Model:', size: 32)]),
                ..._items(),
                // _item(title: 'Primary Model', ante: '1', entry: '100'),
                // _item(title: 'Intermediate Model', ante: '10', entry: '1000'),
                // _item(title: 'Test1', ante: '10', entry: '1000'),
                // _item(title: 'Test2', ante: '10', entry: '1000'),
                // _item(title: 'Test3', ante: '10', entry: '1000'),
              ]),
            ),
          ),
          SizedBox(height: 40.dp750),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              YBDDelayGestureDetector(
                  onTap: () {
                    logger.v('22.11.21---YBDCreateModelDialog tap cancel');
                    widget.cancelCallback?.call();
                  },
                  child: YBDTpGradientButton(width: 275.dp750, height: 72.dp750, text: 'Cancel', confirm: false)),
              SizedBox(width: 20.dp750),
              YBDDelayGestureDetector(
                  onTap: () {
                    YBDRtcHelper.getInstance().currency = curCurrency!;
                    widget.okCallback?.call(curModel);
                  },
                  child: YBDTpGradientButton(width: 275.dp750, height: 72.dp750, text: 'OK', confirm: true)),
            ],
          ),
        ],
      ),
    );
  }
  void buildLX3teoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _item({String? title, String? ante, String? entry, String? currency}) {
    if (title == null) return Container();
    return GestureDetector(
      onTap: () {
        curModel = title;
        curCurrency = currency;
        logger.v('22.11.22---curModel:$curModel---curCurrency:$curCurrency');
        setState(() {});
      },
      child: Container(
        width: 600.dp750,
        height: 130.dp750,
        margin: EdgeInsets.only(top: 25.dp750),
        decoration: BoxDecoration(
          color: Color(0xff380A85).withOpacity(0.1),
          borderRadius: BorderRadius.all(Radius.circular(24.dp750)),
        ),
        child: Column(
          children: [
            SizedBox(height: 15.dp750),
            Container(
              // color: Colors.blue.withOpacity(0.3),
              child: Row(
                children: [
                  SizedBox(width: 25.dp750),
                  Image.asset(
                      title == curModel
                          ? 'assets/images/icon_create_select.webp'
                          : 'assets/images/icon_create_no_select.webp',
                      width: 30.dp750,
                      height: 30.dp750),
                  SizedBox(width: 10.dp750),
                  _text(title, color: Color(0xffFFE862), size: 32),
                ],
              ),
            ),
            // SizedBox(height: 25.dp750),
            Container(
              // color: Colors.red.withOpacity(0.3),
              child: Row(
                children: [
                  SizedBox(width: 68.dp750),
                  _text('Antes:'),
                  SizedBox(width: 8.dp750),
                  Image.asset(YBDCommonUtil.getCurrencyImg(currency), width: 20.dp750),
                  SizedBox(width: 8.dp750),
                  _text(ante ?? ''),
                  SizedBox(width: 50.dp750),
                  _text('Entry:'),
                  SizedBox(width: 8.dp750),
                  Image.asset(YBDCommonUtil.getCurrencyImg(currency), width: 20.dp750),
                  SizedBox(width: 8.dp750),
                  _text(entry ?? ''),
                ],
              ),
            ),
            // SizedBox(height: 5.dp750),
          ],
        ),
      ),
    );
  }
  void _itemzGil6oyelive({String? title, String? ante, String? entry, String? currency}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Widget> _items() {
    logger.v('22.11.23---dialog - YBDTPGlobal.tpgoModels:${YBDTPGlobal.tpgoModels}');
    List<Map<String, dynamic>> items = List<Map<String, dynamic>>.from(YBDTPGlobal.tpgoModels);
    List<YBDGameLobbyDataItem> data = items.map((v) => YBDGameLobbyDataItem().fromJson(v)).toList();
    if (data.isNotEmpty) {
      // 按底注从小到大排序
      data.sort((YBDGameLobbyDataItem a, YBDGameLobbyDataItem b) =>
          YBDNumericUtil.stringToInt(a.boot).compareTo(YBDNumericUtil.stringToInt(b.boot)));
      return data.map((e) => _item(title: e.modeName, ante: e.boot, entry: e.minTicket, currency: e.currency)).toList();
    }
    return [];
  }
  void _itemsilCRUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _text(String text, {Color color = Colors.white, int size = 28}) {
    return YBDGameWidgetUtil.textBalooStrokRegular(text, color: color, fontSize: size.sp750);
  }
}
