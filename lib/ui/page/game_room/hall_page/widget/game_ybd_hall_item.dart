import 'dart:async';


import 'package:flutter/material.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_lobby/enter_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_enter_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

class YBDGameHallItem extends StatefulWidget {
  final YBDGameRoomEnterDataRows? model;

  YBDGameHallItem({Key? key, this.model}) : super(key: key);

  @override
  _YBDGameHallItemState createState() => _YBDGameHallItemState();
}

class _YBDGameHallItemState extends State<YBDGameHallItem> {
  /// 游戏加入或观看
  void gameEventJoinOrWatch(bool join) {
    print("msdms$join");
    var type = join ? EnterGameRoomType.Join : EnterGameRoomType.Watch;
    if (widget.model!.subCategory == GameConst.GAME_SUB_TPGO) {
      YBDCommonTrack().tpgo(join ? 'join' : 'watch');
      YBDEnterHelper.needSeat = join ? YBDEnterHelper.need : YBDEnterHelper.noNeed;
      YBDRtcHelper.getInstance().modeName = widget.model?.property?.gameAttr?.modeName ?? '';
      YBDDialogUtil.showLoading(context);
      YBDEnterHelper().connectSocketNjoin(GameConst.GAME_SUB_TPGO, widget.model?.roomId, GameConst.matchTypePerson,
          enterType: type, onSuccess: () {
        YBDDialogUtil.hideLoading(context);
      });
      return;
    }
    logger.v('clicked game item type: $type, roomId: ${widget.model?.roomId}');
    YBDGameSocketApiUtil.quickEnterRoom(type: type, roomId: widget.model?.roomId);
  }
  void gameEventJoinOrWatchycp6moyelive(bool join) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Material(
        color: Colors.transparent,
        child: YBDDelayGestureDetector(
          onTap: () {
            // 点击列表进入游戏房间观看
            gameEventJoinOrWatch(false);
          },
          child: Container(
            height: 190.dp720,
            // width: ScreenUtil().screenWidth, // 这里可以直接获取父类的宽度
            margin: EdgeInsets.symmetric(horizontal: 24.dp720 as double),
            decoration: BoxDecoration(
                border: Border(
                    // 添加下划线
                    bottom: BorderSide(width: 1.dp750 as double, color: Colors.white.withOpacity(0.10)))),
            child: Row(
              children: [
                YBDGameWidgetUtil.avatar(
                  url: widget.model?.property?.pubAttr?.poster,
                  addSize: true,
                  userId: widget.model?.id.toString(),
                  width: 154.dp720,
                  radius: 16.dp720,
                ),
                SizedBox(width: 29.dp720),
                // 使用Container这里需要计算剩余的宽度；如果使用了Expanded就不需要计算剩余的宽度了
                // width: ScreenUtil().screenWidth - ScreenUtil().setWidth(240),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // 标题
                      YBDGameWidgetUtil.textSingleRegular(widget.model?.property?.pubAttr?.title ?? "",
                          color: Colors.white, fontSize: 27.sp720),
                      SizedBox(height: 19.dp720),
                      if (widget.model?.subCategory == GameConst.GAME_SUB_TPGO)
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                              /// 游戏模式
                              height: 29.dp720.toDouble(),
                              decoration: BoxDecoration(
                                  color: Color(0xff2e0075).withOpacity(0.4),
                                  borderRadius: BorderRadius.circular(14.5.dp720.toDouble())),
                              padding: EdgeInsets.symmetric(horizontal: 13.dp720.toDouble()),
                              child: Center(
                                child: Text.rich(
                                    TextSpan(children: [
                                      TextSpan(
                                          text: (widget.model?.property?.gameAttr?.model ?? "")
                                              .toFirstCapitalize()
                                              .substring(0, 7)),
                                      WidgetSpan(
                                        child: Image.asset(currencyIconAsset(), width: 20.dp720.toDouble(), height: 20.dp720.toDouble()),
                                      ),
                                      TextSpan(
                                          text: (widget.model?.property?.gameAttr?.model ?? "")
                                              .toFirstCapitalize()
                                              .substring(6)),
                                    ]),
                                    style: TextStyle(
                                        fontSize: 19.sp720.toDouble(), color: Colors.white, fontWeight: FontWeight.w400)),
                              ),
                            ),
                            SizedBox(width: 10.dp720.toDouble()),
                            Container(
                              /// 游戏模式
                              height: 29.dp720.toDouble(),
                              decoration: BoxDecoration(
                                  color: Color(0xff2e0075).withOpacity(0.4),
                                  borderRadius: BorderRadius.circular(14.5.dp720.toDouble())),
                              padding: EdgeInsets.symmetric(horizontal: 13.dp720.toDouble()),
                              child: Center(
                                child: Text.rich(
                                    TextSpan(children: [
                                      TextSpan(text: 'Entry: '),
                                      WidgetSpan(
                                        child: Image.asset(currencyIconAsset(), width: 20.dp720.toDouble(), height: 20.dp720.toDouble()),
                                      ),
                                      TextSpan(
                                          text: '${widget.model?.property?.gameAttr?.amount ?? 0}'.currencyIntUnit()),
                                    ]),
                                    style: TextStyle(
                                        fontSize: 19.sp720.toDouble(), color: Colors.white, fontWeight: FontWeight.w400)),
                              ),
                            ),
                            SizedBox(width: 10.dp720.toDouble()),
                          ],
                        ),
                      if (widget.model?.subCategory != GameConst.GAME_SUB_TPGO)
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                              /// 游戏模式
                              height: 29.dp720.toDouble(),
                              decoration: BoxDecoration(
                                  color: Color(0xff2e0075).withOpacity(0.4),
                                  borderRadius: BorderRadius.circular(14.5.dp720.toDouble())),
                              padding: EdgeInsets.symmetric(horizontal: 13.dp720.toDouble()),
                              child: Center(
                                child: Text((widget.model?.property?.gameAttr?.model ?? "").toFirstCapitalize(),
                                    style: TextStyle(
                                        fontSize: 19.sp720.toDouble(), color: Colors.white, fontWeight: FontWeight.w400)),
                              ),
                            ),
                            SizedBox(width: 10.dp720.toDouble()),
                            Container(
                              /// 币种
                              height: 29.dp720.toDouble(),
                              decoration: BoxDecoration(
                                  color: Color(0xFF4844CF),
                                  border: Border.fromBorderSide(BorderSide(width: 0.5.dp750.toDouble(), color: Colors.white)),
                                  borderRadius: BorderRadius.circular(14.5.dp720.toDouble())),
                              padding: EdgeInsets.symmetric(horizontal: 9.dp720.toDouble()),
                              child: Row(
                                children: [
                                  Image.asset(currencyIconAsset(), width: 20.dp720.toDouble(), height: 20.dp720.toDouble()),
                                  SizedBox(width: 2.dp720.toDouble()),
                                  Text(

                                      /// 金额
                                      '${widget.model?.property?.gameAttr?.amount ?? 0}'.currencyIntUnit(),
                                      style: TextStyle(
                                          fontSize: 19.sp720.toDouble(), color: Colors.white, fontWeight: FontWeight.w400)),
                                ],
                              ),
                            ),
                            // Spacer(),
                            // audienceLive(widget.model?.userCount ?? 0),
                          ],
                        ),
                      SizedBox(height: 19.dp720.toDouble()),
                      userImagesAndJoin()
                    ],
                  ),
                ),
                watchCountAndJoin(),
              ],
            ),
          ),
        ),
      ),
    );
  }
  void builddNPFsoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String currencyIconAsset() {
    return YBDGameResource.currencyIconAsset(widget.model?.property?.gameAttr?.currency);
  }

  /// 参与游戏的人
  Widget userImagesAndJoin() {
    List<Widget> images = List.generate(
        widget.model?.users?.length ?? 0,
        (imgIndex) => Container(
              decoration: BoxDecoration(
                  color: Color(0xFF4844CF),
                  border: Border.fromBorderSide(BorderSide(width: 1.dp720 as double, color: Colors.white)),
                  borderRadius: BorderRadius.circular(19.dp720 as double)),
              margin: EdgeInsets.only(right: 10.dp720 as double),
              child: YBDGameWidgetUtil.avatar(
                  url: widget.model!.users![imgIndex].attribute?.avatar,
                  addSize: true,
                  userId: widget.model!.users![imgIndex].userId,
                  width: 38.dp720,
                  radius: 19.dp720,
                  sideWidth: 0.5.dp720),
            ));
    if (widget.model?.subCategory == GameConst.GAME_SUB_TPGO) {
      while (images.length < 5) {
        images.add(Container(
            decoration: BoxDecoration(
                // color: Color(0xFF4844CF),
                // border: Border.fromBorderSide(BorderSide(width: 1.dp720, color: Colors.white)),
                borderRadius: BorderRadius.circular(20.dp720)),
            margin: EdgeInsets.only(right: 10.dp720),
            width: 40.dp720,
            height: 40.dp720,
            child: Image.asset('assets/images/tpgo_list_user.png', fit: BoxFit.fill)));
      }
    }

    // images.add(Spacer());
    // images.add(joinGradientButton());
    return Row(children: images);
  }
  void userImagesAndJoinv9sm3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 观看人数+加入按钮，垂直居中
  Widget watchCountAndJoin() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        audienceLive(widget.model?.userCount ?? 0),
        SizedBox(height: 12.dp750),
        joinGradientButton(),
      ],
    );
  }
  void watchCountAndJoinFevAkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 加入的线性渐变按钮 join or watch
  Widget joinGradientButton() {
    var join = widget.model!.gameJoinStatus();

    BoxDecoration decoration = BoxDecoration(
      borderRadius: BorderRadius.circular(22.dp750 as double),
      gradient: LinearGradient(
        colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
    if (!join) {
      // watch 状态是灰底
      decoration = BoxDecoration(borderRadius: BorderRadius.circular(22.dp750 as double), color: Colors.white.withOpacity(0.5));
    }

    Widget button = Container(
      decoration: decoration,
      width: 100.dp750,
      height: 44.dp750,
      alignment: Alignment.center,
      child: Text(
        join ? 'Join' : 'Watch',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 21.sp720,
          color: Colors.white,
          fontWeight: FontWeight.w400,
        ),
      ),
    );

    return YBDScaleAnimateButton(
      onTap: () {
        gameEventJoinOrWatch(join);
      },
      child: button,
    );
  }
  void joinGradientButtonttHgooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 跳动标识+观众人数
  Widget audienceLive(int count) {
    if (count <= 0) {
      return Container();
    }
    return Container(
      width: 110.dp720,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            // 音乐动图
            'assets/images/icon_live.webp',
            width: 24.dp720,
            height: 22.dp720,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.dp720),
            child: YBDGameWidgetUtil.textSingleRegular("$count", fontSize: 24.sp720, color: Colors.white),
          )
        ],
      ),
    );
  }
  void audienceLiveV4w4moyelive(int count) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
