import 'dart:async';


import 'package:flutter/material.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_game_room_track.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/match_ybd_commands.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/widget/dialog_ybd_tpg_match.dart';
import 'package:oyelive_main/ui/page/game_room/util/tp_ybd_cache.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/badge_ybd_page.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/dialog_ybd_lock.dart';

class YBDTPGOMatchPage extends StatelessWidget {
  const YBDTPGOMatchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late BuildContext lockContext;
    return Column(
      children: [
        SizedBox(
          height: 120.px,
        ),
        Image.asset(
          "assets/images/tp_go.png",
          width: 362.px,
        ),
        SizedBox(
            // height: 98.px,
            ),
        YBDDelayGestureDetector(
          onTap: () async {
            showDialog<Null>(
                context: context, //BuildContext对象
                barrierDismissible: true,
                builder: (BuildContext context) {
                  lockContext = context;
                  return YBDLockDialog(() {
                    YBDNavigatorHelper.popPage(lockContext);
                  });
                });

            void match() {
              YBDMatchCommands.Match().then((value) {
                if (value == null) {
                  YBDToastUtil.toast("No connection,please try again later!");
                  YBDNavigatorHelper.popPage(context);

                  return;
                }
                print("xd11${value.toJson()}");

                YBDNavigatorHelper.popPage(lockContext);

                if (value.code == Const.HTTP_SUCCESS) {
                  print("xd");
                  YBDMatchCommands.curMatchId = value.matchId;
                  YBDGameRoomTrack().match();
                  showDialog<Null>(
                      context: context, //BuildContext对象
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return YBDTpgMatchDialog();
                      });
                } else {
                  YBDToastUtil.toast(value.message);
                }
              });
            }

            // 检测资源是否下载完成
            YBDTPCache.downloadCache(callback: () {
              match();
            });
          },
          child: YBDBadgeAnimationView(
            520.px as double,
            height: 470.px,
            fit: BoxFit.fitWidth,
            url: YBDGameResource.assetTPSvga('tpg_poker'),
          ),
          // child: Stack(
          //   alignment: Alignment.bottomCenter,
          //   children: [
          //     Image.asset(
          //       "assets/images/tpg_poker.webp",
          //       width: 476.px,
          //     ),
          //     Padding(
          //       padding: EdgeInsets.only(bottom: 40.px),
          //       child: Text(
          //         "2&5 Players",
          //         style: TextStyle(color: Colors.white, fontSize: 32.sp, fontFamily: "baloo"),
          //       ),
          //     )
          //   ],
          // ),
        ),
      ],
    );
  }
  void buildDuMC2oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
