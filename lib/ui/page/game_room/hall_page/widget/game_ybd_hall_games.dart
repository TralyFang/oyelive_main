import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/event/game_ybd_hall_bg_event.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_category_entity.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/game_ybd_hall_page.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

class YBDGameHallGames extends StatefulWidget {
  final List<YBDGameCategoryData?>? models;
  int? selectedIndex; // 当前默认选中的游戏
  void Function(int)? selectedIndexCallback;
  TabController? tabController;

  YBDGameHallGames({key: Key, this.models, this.selectedIndex, this.selectedIndexCallback, this.tabController})
      : super(key: key);

  @override
  YBDGameHallGamesState createState() => YBDGameHallGamesState();
}

class YBDGameHallGamesState extends State<YBDGameHallGames> {
  ScrollController _controller = ScrollController();

  bool _indexIsChanging = false;

  @override
  void initState() {
    super.initState();

    widget.tabController!.addListener(() {
      if (widget.models != null && widget.models!.isNotEmpty) {
        logger.v('22.9.8--go to tp go match tab');
        eventBus.fire(YBDGameHallBgEvent()..showTpGo = widget.models![widget.tabController!.index]!.subCategory == TP_GO);
      }
      _indexIsChanging = widget.tabController!.indexIsChanging;
    });
  }
  void initStatew0WfToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  scroller(int index) {
    _controller.animateTo(192.dp720 * (index - 1), duration: Duration(milliseconds: 300), curve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    final itemHeight = 182.dp720;
    final itemWidth = 192.dp720;
    return Container(
      margin: EdgeInsets.only(left: 24.dp720 as double, right: 24.dp720 as double, bottom: 10.dp720 as double),
      height: itemHeight,
      child: ListView.separated(
        controller: _controller,
        scrollDirection: Axis.horizontal,
        itemBuilder: (_, index) {
          var model = widget.models![index];
          return YBDGameWidgetUtil.networkImage(
            url: widget.selectedIndex == index ? model!.selectedImage : model!.image,
            width: itemWidth,
            height: itemHeight,
            alignment: Alignment.bottomCenter,
            // placeholder: YBDGameWidgetUtil.assetImage(
            //     "${widget.selectedIndex==index ? 'dev_game_icon_sel':'dev_game_icon'}"),
            onTap: () {
              logger.v('game icon tap changing: $_indexIsChanging');
              // Deprecated
              // if (YBDTAClickName.gameTabMap.containsKey(model?.subCategory ?? '')) {
              //   YBDCommonTrack().commonTrack(YBDTAProps(
              //     location: YBDTAClickName.gameTabMap[model.subCategory],
              //     module: YBDTAModule.game,
              //   ));
              // }
              if (_indexIsChanging) return;
              // 如果是不可用状态，那就无需操作
              if (model.status == 0 || model.status == 3) {
                /// disable
                return;
              }

              // 如果有路由那就直接跳转路由
              if (model.redirectUrl?.isNotEmpty ?? false) {
                YBDNavigatorHelper.openUrl(context, model.redirectUrl!);
                return;
              }
              if (widget.selectedIndex == index) {
                return; //重复选择了
              }
              widget.selectedIndex = index;
              setState(() {});
              // 还需要通知刷新列表
              /// 记录所选的游戏类别
              YBDRtcHelper.getInstance().selectedGame = model;
              widget.selectedIndexCallback?.call(index);
            },
          );
        },
        separatorBuilder: (_, index) {
          return SizedBox(width: 1.dp720);
        },
        itemCount: widget.models!.length,
      ),
    );
  }
  void buildARID9oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
