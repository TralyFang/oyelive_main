import 'dart:async';


import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_game_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/entity/match_ybd_confirm_entity.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/match_ybd_commands.dart';
import 'package:oyelive_main/common/web_socket/tpg_match/topic/tpg_ybd_topic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';

import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/navigator/navigator_ybd_helper.dart';

class YBDTpgMatchDialog extends StatefulWidget {
  static BuildContext? dialogContext;
  static show() {
    showDialog<Null>(
        context: Get.context!, //BuildContext对象
        barrierDismissible: false,
        builder: (BuildContext context) {
          dialogContext = context;
          return YBDTpgMatchDialog();
        });
  }

  static dismiss() {
    if (dialogContext != null) YBDNavigatorHelper.popPage(dialogContext!);
    dialogContext = null;
  }

  @override
  YBDTpgMatchDialogState createState() => new YBDTpgMatchDialogState();
}

class YBDTpgMatchDialogState extends BaseState<YBDTpgMatchDialog> {
  late Timer _timer;
  int _seconds = 0;
  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return WillPopScope(
      onWillPop: () async => false,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Center(
            child: Image.asset("assets/images/matching.png"),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 240.px,
              ),
              Image.asset(
                "assets/images/tbg_rabbit.png",
                width: 56.px,
              ),
              SizedBox(
                height: 14.px,
              ),
              Text(
                "Waiting for other players… $_seconds",
                style: TextStyle(color: Colors.white, fontSize: 28.sp, fontFamily: "baloo"),
              ),
              SizedBox(
                height: 96.px,
              ),
              GestureDetector(
                onTap: () {
                  YBDTpgMatchDialog.dismiss();
                  YBDGameRoomTrack().match(cancel: true);
                  // YBDMatchCommands.QuitMatch().then((value) {
                  //   if (value == null) {
                  //     YBDToastUtil.toast("No connection,please try again later!");
                  //     YBDNavigatorHelper.popPage(context);
                  //
                  //     return;
                  //   }
                  //   if (value.code == Const.HTTP_SUCCESS) {
                  //     // 取消时长埋点
                  //     YBDTATrack().trackEvent(YBDTATimeTrackType.match_cancel_time,
                  //         prop: YBDTAProps(id: value.gameId.toString(), name: 'TPGO'));
                  //     YBDNavigatorHelper.popPage(context);
                  //   } else {
                  //     YBDToastUtil.toast(value.message);
                  //   }
                  // });
                },
                child: Image.asset(
                  "assets/images/tbg_close.webp",
                  width: 65.px,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
  void myBuildDDZuqoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      _seconds++;
      setState(() {});
    });
    // addSub(YBDTpgTopic.tgpTopicOutStream.listen((event) {
    //   Map data = json.decode(event.data);
    //   if (data['command'] == "matchConfirm") {
    //     YBDMatchConfirmEntity entity = YBDMatchConfirmEntity().fromJson(data);
    //
    //     if (entity.finalConfirmStatus == 0) {
    //       //关闭窗口
    //       YBDNavigatorHelper.popPage(context);
    //
    //       // YBDToastUtil.toast("${entity.matchStatus} confirmed");
    //
    //       YBDMatchCommands.EnterBussiness(entity);
    //
    //       Future.delayed(Duration(seconds: 0), () {
    //         switch (entity.matchType) {
    //           case game_tpg:
    //             //进房
    //             if (entity.connectionRoomUrl != null) {
    //               String socketUrl = Uri.encodeComponent(entity.connectionRoomUrl);
    //               YBDNavigatorHelper.navigateTo(
    //                   Get.context,
    //                   YBDNavigatorHelper.game_tp_room_page +
    //                       "?roomId=${entity.collectionId}"
    //                           "&socketUrl=$socketUrl");
    //               // 匹配成功时长埋点
    //               YBDTATrack().trackEvent(YBDTATimeTrackType.match_success_time,
    //                   prop: YBDTAProps(id: entity.gameId.toString(), name: 'TPGO'));
    //             }
    //
    //             break;
    //           default:
    //             YBDToastUtil.toast("Not support yet!");
    //             break;
    //         }
    //       });
    //     }
    //
    //     //handleBussiness
    //
    //   }
    // }));
  }
  void initStatebsMjVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    _timer.cancel();

    super.dispose();
  }

  @override
  void didUpdateWidget(YBDTpgMatchDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetaXYzaoyelive(YBDTpgMatchDialog oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
