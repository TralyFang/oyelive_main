import 'dart:async';


import 'package:flutter/material.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/ui/page/game_lobby/enter_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

import '../../../../../common/analytics/analytics_ybd_util.dart';

class YBDGameHallStart extends StatefulWidget {
  bool canJoinRoom; // 可加入房间

  bool isTpgo;
  YBDGameHallStart({Key? key, required this.canJoinRoom, this.isTpgo: false}) : super(key: key);

  @override
  _YBDGameHallStartState createState() => _YBDGameHallStartState();
}

class _YBDGameHallStartState extends State<YBDGameHallStart> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 24.dp720 as double),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [itemWidget(true), itemWidget(false)],
      ),
    );
  }
  void buildh5QKwoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是否快速加入，否则创建
  Widget itemWidget(bool quicklyJoin) {
    return YBDDelayGestureDetector(
      track: YBDAnalyticsEvent(YBDTATrackEvent.common_click,
          ta: YBDTAProps(
            location: quicklyJoin ? YBDTAClickName.quickly_join : YBDTAClickName.create_gameroom,
            game: YBDRtcHelper.getInstance().selectedGame!.subCategory,
          )),
      onTap: () {
        quickEnterRoom(quicklyJoin);
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.GAME_HALL_PAGE,
          itemName: quicklyJoin ? 'quick enter' : "create room",
        ));
      },
      child: YBDGameWidgetUtil.assetImage(
        "${quicklyJoin ? 'game_quicky_join' : 'game_create'}",
        width: 328.dp720,
        height: 110.dp720,
      ),
    );
  }
  void itemWidgetKt13Goyelive(bool quicklyJoin) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是否快速加入，否则创建
  quickEnterRoom(bool quicklyJoin) {
    if (widget.isTpgo) {
      if (quicklyJoin)
        YBDEnterHelper().quickJoin(GameConst.GAME_SUB_TPGO);
      else
        YBDEnterHelper().createRoom(GameConst.GAME_SUB_TPGO, GameConst.matchTypePerson);
      return;
    }
    var type = quicklyJoin ? EnterGameRoomType.QuickJoin : EnterGameRoomType.QuickCreate;
    YBDGameLog.v('game quickly type :$type');
    YBDGameSocketApiUtil.quickEnterRoom(type: type);
  }
}
