import 'dart:async';


import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/route_ybd_push_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_category_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_enter_entity.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/game_ybd_hall_page.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/widget/game_ybd_hall_item.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/widget/tpgo_ybd_match_page.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';

class YBDGameHallList extends StatefulWidget {
  final YBDGameCategoryData? selectedCategory;
  final List<YBDGameCategoryData?>? gamesData;
  final Function(int page)? refreshCallback;

  final String subCategory;

  const YBDGameHallList({Key? key, this.selectedCategory, this.gamesData, this.refreshCallback, this.subCategory: ""})
      : super(key: key);

  @override
  _YBDGameHallListState createState() => _YBDGameHallListState();
}

class _YBDGameHallListState extends State<YBDGameHallList> with AutomaticKeepAliveClientMixin {
  /// 列表刷新控制器
  RefreshController _refreshController = RefreshController();

  List<YBDGameRoomEnterDataRows?> _data = [];
  //加载的页数
  int _page = 1;

  @override
  bool get wantKeepAlive => true;

  bool init = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      Future.delayed(Duration(seconds: 1), () {
        _onRefresh(_page);
      });
    });
  }
  void initState8xBHNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Container(
      child: SmartRefresher(
        controller: _refreshController,
        header: YBDMyRefreshIndicator.myHeader,
        footer: YBDMyRefreshIndicator.myFooter,
        enablePullDown: true,
        enablePullUp: true,
        onRefresh: () {
          _page = 1;
          _onRefresh(_page);
        },
        onLoading: () {
          _page += 1;
          _onRefresh(_page);
        },
        // 无数据展示空的占位图
        child: init
            ? YBDLoadingCircle()
            : ((_data == null)
                ? YBDPlaceHolderView(text: 'No Data')
                : ListView.builder(
                    itemCount: _data.length, // 数据的数量
                    itemBuilder: (context, index) {
                      var model = _data[index];
                      return YBDGameHallItem(model: model);
                    }, // 类似 cellForRow 函数
                  )),
      ),
    );
  }
  void build6Rgwuoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 列表刷新响应方法
  _onRefresh(int page) async {
    widget.refreshCallback?.call(page);
    // var gameCategory = widget.selectedCategory;
    // if (gameCategory == null) {
    //   _refreshController.refreshCompleted();
    //   _cancelInitState();
    //   return;
    // }

    /// 游戏房间列表
    ApiHelper.gameRoomEnterList(
      context,
      subCategory: widget.subCategory,
      // projectCode: gameCategory.projectCode, // 项目编码，可以不传
      // tenantCode: gameCategory.tenantCode, // 项目编码，可以不传
      page: page, // 当前页数
    ).then((respList) {
      // 刷新数据失败
      _refreshController.resetNoData();
      if (_refreshController.isRefresh) _refreshController.refreshCompleted();
      if (_refreshController.isLoading) _refreshController.loadComplete();

      if (respList == null) {
        _refreshController.loadFailed();
        // 是当前页面则刷新界面
        _cancelInitState();
      } else {
        if (page == 1) {
          _data = respList;
        } else {
          _data.addAll(respList);
        }
        if (respList.length < Const.PAGE_SIZE) _refreshController.loadNoData();

        // 是当前页面则刷新界面
        _cancelInitState();
      }
    });
    logger.v('game hall page refresh: ${YBDRoutePushUtil.type}');
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      init = false;
      setState(() {});
    } else {}
  }
}
