import 'dart:async';

import 'package:oyelive_main/common/constant/const.dart';

class GameConst {
  static const int ZegoAppId = 1795301448; // : 1987799235;
  static const String ZegoAppSign = '23b2e89a75a2f8d5827777ace19c74cebd8c72f6b0f5ddc00a064abfd10c7ccc';
  //1868e28e12ab3a6496be33492c1763d4fbf483dd386e81a9c8d500be00381515

  /// 机构小游戏南亚地区
  static const String ZEGO_MG_APP_ID = "1471034050121564218";
  static const String ZEGO_MG_APP_KEY = "yM4KyCZmDY59zLHlv5rFKrPrAk9DaGeM";

  static const int zegoLogSize = 5 * 1024 * 1024;

  /// 声音阈值[0, 255], 超过50才能表示有声音
  static const int minVolume = 50;

  static const String agora_log_profix = 'agora rtc api';
  static const String zego_log_profix = 'zego_rtc_api';

  static const String defaultValue = '-1';

  static const String game_success = '0';
  static const String game_exitFail = '07013'; //游戏中的玩家不能退出
  static const String game_block = '07014'; //被踢出房间 给用户的提示 后面可能改成弹窗
  static const String game_create_room_tips = "07006";
  static const String game_insufficen = "07042";

  /********************socket cmd version**********************/
  static const String version = " 1.0"; // 前面有空格
  static const String enterRoomCmd = "Entry" + version;
  static const String sendQuickMsgCmd = "quickChat" + version;
  static const String exitRoomCmd = "Exit" + version;
  static const String chatCmd = "Chat" + version;
  static const String bodyVer = "2.0"; // body里面的ver版本号

  /***********************notify*********************************/
  ///进房通知 更新房间基本信息
  static const String EntryNotifyCmd = "EntryNotify" + version;

  ///游戏房间状态变更
  static const String RoomStatusCmd = "RoomStatus" + version;

  ///游戏房间状态变更通知
  static const String RoomStatusNotifyCmd = "RoomStatusNotify" + version;

  /// 房间信息推送
  static const String RoomInfoNotify = "RoomInfoNotify" + version;

  ///麦位 更新通知
  static const String EntryMicCmd = "MicNotify" + version;

  ///聊天 更新通知
  static const String ChatNotifyCmd = "ChatNotify" + version;

  ///房间解散通知
  static const String LeaveCmd = "Leave" + version;

  ///游戏开始 通知
  static const String GameStartResultCmd = "GameStartResult" + version;

  ///送礼通知
  static const String GiftingNotifyCmd = "GiftingNotify" + version;
  /********************mic operater**********************/
  static const String GrabMicCmd = "GrabMic" + version;
  static const String DropMicCmd = "DropMic" + version;
  static const String LockMicCmd = "LockMic" + version;
  static const String UnlockMicCmd = "UnlockMic" + version;
  static const String MuteMicCmd = "MuteMic" + version;
  static const String UnMuteMicCmd = "UnMuteMic" + version;

  /********************game cmd *************************/
  /// 游戏房间状态、配置推送
  static const String RoomStatusNotify = "RoomStatusNotify" + version;

  ///上\下座
  static const String TakeSeatCmd = "TakeSeat" + version;
  static const String LeaveSeatCmd = "LeaveSeat" + version;

  ///游戏准备/取消准备
  static const String ReadyGameCmd = "ReadyGame" + version;
  static const String CancelReadyGameCmd = "CancelReadyGame" + version;

  ///修改游戏配置
  static const String UpdateGameConfigCmd = "UpdateGameConfig" + version;

  ///剔除玩家
  static const String KickOutPlayerCmd = "KickOutPlayer" + version;

  ///开始游戏
  static const String StartGameCmd = "StartGame" + version;

  ///sud游戏上报
  static const String GameReportCmd = "GameReport" + version;

  /// 游戏结算
  static const String GameSettlementCmd = "YBDGameSettlement" + version;

  /// 送礼
  static const String GiftingCmd = "Gifting" + version;

  /// miniprofile禁言和踢人
  static const String BlockCmd = "Block" + version;

  static const String join = 'join';
  static const String watch = 'watch';

  // static const String packet_id = 'packet-id';
  static const String cmd = 'x-cmd';
  static const String code = 'x-code';
  static const String codeMsg = 'x-codeMsg';
  static const String packetId = 'packet-id';
  static const String topic = 'topic';

  static const String zego = 'zego';
  static const String agora = 'agora';

  /// 麦位状态
  static const String Locked = 'Locked';
  static const String Empty = 'Empty';
  static const String Grabbed = 'Grabbed';
  static const String Muted = 'Muted';
  static const String MuteSelf = 'Muted-Self';
  static const String MuteDouble = 'Muted-Double';

  /// 游戏状态
  // 组队中
  static const String GameGrouping = 'grouping';
  // 人机开局
  static const String RobotMatch = 'robotMatch';
  // 游戏中
  static const String GameGaming = 'gaming';

  /// 游戏玩家状态
  // 队长
  static const String CaptainRole = 'captain';
  // 玩家
  static const String PlayerRole = 'player';
  // 准备状态
  static const String MemberReady = 'ready';
  // 未准备状态
  static const String MemberNotReady = 'not-ready';
  // 默认ludo座位数
  static const int DefaultMaximum = 4;
  // 默认开始ludo的玩家数
  static const int DefaultMinimum = 2;
  // 默认座位index
  static const int DefaultSeatIndex = -1;

  static const String live = 'live';
  static const String offline = 'offline';

  static const String PublicChat = 'publicChat';
  static const String EntryChat = 'entry';
  static const String YBDGiftChat = 'gift';
  static const String GameReuslt = 'gameWin';
  static const String matchTypeBot = '1';
  static const String matchTypePerson = '2';

  static const String GAME_SUB_TPGO = 'tpgo';
  static const String GAME_NAME_TPGO = 'TP GO';
}
