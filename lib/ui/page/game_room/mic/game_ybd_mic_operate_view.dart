import 'dart:async';


/*
 * @Author: William-Zhou
 * @Date: 2022-03-11 16:56:03
 * @LastEditTime: 2022-09-29 14:46:09
 * @LastEditors: William
 * @Description: 
 */
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_game_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_mic_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_bloc.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_mic_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/room/util/room_ybd_util.dart';

class YBDGameMicOperateView extends StatefulWidget {
  final int? roomId;
  final int? userId;
  final YBDGameMicInfoBodyMic? micUser;
  final GameMicRole? role;

  const YBDGameMicOperateView({
    Key? key,
    this.roomId,
    this.userId,
    this.micUser,
    this.role,
  }) : super(key: key);

  @override
  State<YBDGameMicOperateView> createState() => _YBDGameMicOperateViewState();
}

class _YBDGameMicOperateViewState extends State<YBDGameMicOperateView> {
  /// 被用户静音的列表(任何在麦上的用户) 本地
  List<String?>? _localMuteList = [];

  /// 本地禁言当前用户
  var _mutedSelf = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _localMuteList = BlocProvider.of<YBDGameMicBloc>(context).state.localMuteUserList;
      _mutedSelf = BlocProvider.of<YBDGameMicBloc>(context).currentUserMutedSelf();

      if (mounted) setState(() {});
    });
  }
  void initStateSfl61oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: px(200),
      padding: EdgeInsets.symmetric(vertical: px(20)),
      decoration: BoxDecoration(
        color: Color(0xff042053).withOpacity(0.89),
        borderRadius: BorderRadius.vertical(top: Radius.circular(px(33))),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _speakerItem(),
          _unSpeakerItem(),
          _muteItem(),
          _unMuteItem(),
          _lockItem(),
          _unLockItem(),
          _grabItem(),
          _dropItem(),
          _profileItem(),
        ],
      ),
    );
  }
  void buildhujtLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 静音，关闭扬声器
  /// 显示条件：
  /// 麦位有人  & 麦位非当前用户 & 本地没将麦位静音
  Widget _unSpeakerItem() {
    if (_micOccupied() && !_isCurrentUser() && !_isUnSpeakerMic()) {
      return _item('assets/images/liveroom/mic_off.webp', translate('game_mute'), _onUnSpeaker);
    }

    return SizedBox();
  }
  void _unSpeakerItemUBkgSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 静音，关闭扬声器
  void _onUnSpeaker() {
    BlocProvider.of<YBDGameMicBloc>(context).muteRemote('${widget.micUser!.userId}', true);
    Navigator.pop(context);
  }

  /// 解开静音，打开扬声器
  /// 显示条件：
  /// 麦位有人  & 麦位非当前用户 & 本地有将麦位静音
  Widget _speakerItem() {
    if (_micOccupied() && !_isCurrentUser() && _isUnSpeakerMic()) {
      return _item('assets/images/liveroom/mic_on.webp', translate('game_unmute'), _onSpeaker);
    }

    return SizedBox();
  }
  void _speakerItemn1dVqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 解开静音，打开扬声器
  void _onSpeaker() {
    BlocProvider.of<YBDGameMicBloc>(context).muteRemote('${widget.micUser!.userId}', false);
    Navigator.pop(context);
  }

  /// 解开禁言，开始推流
  /// 显示条件：
  /// 麦位抓麦    & 麦位是当前用户 & 本地记录当前用户已禁言（这里是以本地标记位为准，可能和服务端状态不一致）
  /// 麦位自己禁言 & 麦位是当前用户
  /// 麦位房主禁言 & 麦位非当前用户 & 当前用户是房主
  /// 麦位双重禁言 & 麦位是当前用户
  /// 麦位双重禁言 & 麦位非当前用户 & 当前用户是房主
  Widget _unMuteItem() {
    final status = widget.micUser!.status;

    if ((status == GameConst.Grabbed && _isCurrentUser() && _mutedSelf) ||
        (status == GameConst.MuteSelf && _isCurrentUser()) ||
        (status == GameConst.Muted && !_isCurrentUser() && _isOwner()) ||
        (status == GameConst.MuteDouble && _isCurrentUser()) ||
        (status == GameConst.MuteDouble && !_isCurrentUser() && _isOwner())) {
      return _item('assets/images/game/game_unmute_mic.png', translate('unmute'), _onUnMute);
    }

    return SizedBox();
  }
  void _unMuteItemRHEpVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 解开禁言，开始推流
  void _onUnMute() {
    BlocProvider.of<YBDGameMicBloc>(context).micOperation(
      SendMsgType.UnMuteMic,
      widget.micUser!.index,
      YBDNumericUtil.stringToInt(widget.micUser!.userId),
    );
    Navigator.pop(context);
  }

  /// 禁言，停止推流
  /// 显示条件：
  /// 麦位抓麦    & 麦位是当前用户 & 本地记录当前用户解开禁言（这里是以本地标记位为准，可能和服务端状态不一致）
  /// 麦位抓麦    & 麦位非当前用户 & 当前用户是房主
  /// 麦位自己禁言 & 麦位非当前用户 & 当前用户是房主
  /// 麦位房主禁言 & 麦位是当前用户 & 当前用户非房主
  Widget _muteItem() {
    final status = widget.micUser!.status;

    if ((status == GameConst.Grabbed && _isCurrentUser() && !_mutedSelf) ||
        (status == GameConst.Grabbed && !_isCurrentUser() && _isOwner()) ||
        (status == GameConst.MuteSelf && !_isCurrentUser() && _isOwner()) ||
        (status == GameConst.Muted && _isCurrentUser() && !_isOwner())) {
      return _item('assets/images/game/game_mute_mic.png', translate('mute'), _onMute);
    }

    return SizedBox();
  }
  void _muteItemdTo0coyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 禁言，停止推流
  void _onMute() {
    print('_onMuteself');
    BlocProvider.of<YBDGameMicBloc>(context).micOperation(
      SendMsgType.MuteMic,
      widget.micUser!.index,
      YBDNumericUtil.stringToInt(widget.micUser!.userId),
    );
    Navigator.pop(context);
  }

  /// 锁麦
  /// 显示条件：
  /// 麦位为空 & 当前用户是房主
  Widget _lockItem() {
    final status = widget.micUser!.status;

    if (status == GameConst.Empty && _isOwner()) {
      return _item('assets/images/game/game_lock_mic.png', translate('lock_mic'), _onLock);
    }

    return SizedBox();
  }
  void _lockIteml2e1Aoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 锁麦
  void _onLock() {
    BlocProvider.of<YBDGameMicBloc>(context).micOperation(
      SendMsgType.LockedMic,
      widget.micUser!.index,
      YBDNumericUtil.stringToInt(widget.micUser!.userId),
    );
    Navigator.pop(context);
  }

  /// 解开锁麦
  /// 显示条件：
  /// 麦位已锁 & 当前用户是房主
  Widget _unLockItem() {
    final status = widget.micUser!.status;

    if (status == GameConst.Locked && _isOwner()) {
      return _item('assets/images/game/game_unlock_mic.png', translate('unlock'), _onUnLock);
    }

    return SizedBox();
  }

  /// 解开锁麦
  void _onUnLock() {
    BlocProvider.of<YBDGameMicBloc>(context).micOperation(
      SendMsgType.UnLockedMic,
      widget.micUser!.index,
      YBDNumericUtil.stringToInt(widget.micUser!.userId),
    );
    Navigator.pop(context);
  }
  void _onUnLock9jdE6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 上麦
  /// 显示条件：
  /// 麦位为空且自己没上麦
  Widget _grabItem() {
    final status = widget.micUser!.status;

    if (status == GameConst.Empty && !BlocProvider.of<YBDGameMicBloc>(context).isCurrentUserGrabMic()) {
      return _item('assets/images/game/game_take_mic.png', translate('take_mic'), _onGrab);
    }

    return SizedBox();
  }

  /// 上麦
  void _onGrab() {
    YBDRoomUtil.liveDurationInMillSecs = null;
    YBDGameRoomTrack().gameRoomTrack(event: YBDTATrackEvent.gameroom_mic_take);
    BlocProvider.of<YBDGameMicBloc>(context).micOperation(
      SendMsgType.GrabMic,
      widget.micUser!.index,
      widget.userId,
    );
    Navigator.pop(context);
  }

  /// 下麦
  /// 显示条件：
  /// 麦位有人  & 麦位是当前用户
  /// 麦位有人  & 麦位非当前用户 & 当前用户是房主
  Widget _dropItem() {
    if ((_micOccupied() && _isCurrentUser()) || (_micOccupied() && !_isCurrentUser() && _isOwner())) {
      return _item('assets/images/game/game_drop_mic.png', translate('drop_mic'), _onDrop);
    }

    return SizedBox();
  }
  void _dropItem1DWgRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 下麦
  void _onDrop() {
    YBDGameRoomTrack().gameRoomTrack(event: YBDTATrackEvent.gameroom_mic_drop);
    BlocProvider.of<YBDGameMicBloc>(context).micOperation(
      SendMsgType.DropMic,
      widget.micUser!.index,
      YBDNumericUtil.stringToInt(widget.micUser!.userId),
    );
    Navigator.pop(context);
  }

  /// 显示profile
  /// 显示条件：
  /// 麦位有人  & 麦位非当前用户
  Widget _profileItem() {
    if (_micOccupied() && !_isCurrentUser()) {
      return _item('assets/images/game/game_icon_profile.png', translate('profile'), _onProfile);
    }
    return SizedBox();
  }

  /// 显示profile
  void _onProfile() {
    Navigator.pop(context);
    YBDGameMicUtil.showMiniProfile(
      roomId: widget.roomId,
      userId: YBDNumericUtil.stringToInt(widget.micUser!.userId),
    );
  }

  /// item的公共样式
  Widget _item(String iconUrl, String description, Function onTap) {
    return GestureDetector(
      onTap: onTap as void Function()?,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: px(15)),
        width: px(150),
        alignment: Alignment.center,
        color: Colors.transparent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(iconUrl, width: px(50), height: px(50)),
            SizedBox(height: 16.dp750),
            Text(
              description,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(24),
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
  void _itemodhgBoyelive(String iconUrl, String description, Function onTap) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 麦位用户是当前用户
  bool _isCurrentUser() {
    return '${widget.userId}' == widget.micUser!.userId;
  }

  /// 本地有将麦位静音
  bool _isUnSpeakerMic() {
    return _localMuteList!.contains(widget.micUser!.userId);
  }

  /// 当前用户是房主
  bool _isOwner() {
    return widget.role == GameMicRole.owner;
  }

  /// 麦位是否有人
  bool _micOccupied() {
    return widget.micUser!.micOccupied;
  }

  double px(int px) {
    return ScreenUtil().setWidth(px);
  }
}
