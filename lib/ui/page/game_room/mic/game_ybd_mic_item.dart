import 'dart:async';


/*
 * @Author: William-Zhou
 * @Date: 2022-03-11 15:00:21
 * @LastEditTime: 2022-04-01 11:27:03
 * @LastEditors: William-Zhou
 * @Description: single mic widget
 */
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_mic_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_bloc.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_operate_view.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_event_handler.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/room/widget/spread_ybd_widget.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';

class YBDGameMicItem extends StatefulWidget {
  final int? roomId;
  final YBDGameMicInfoBodyMic? user;
  final bool? muteSelf;

  /// 当前用户关闭该麦位的喇叭
  final bool? muteOther;
  final YBDRtcVolumeInfo? vol;

  const YBDGameMicItem({Key? key, this.roomId, this.user, this.muteSelf, this.muteOther, this.vol}) : super(key: key);

  @override
  State<YBDGameMicItem> createState() => _YBDGameMicItemState();
}

class _YBDGameMicItemState extends State<YBDGameMicItem> {
  GameMicRole? _role;
  YBDUserInfo? _userInfo;

  @override
  void initState() {
    super.initState();
    _userInfo = YBDCommonUtil.storeFromContext()!.state.bean;
    _role = setRole();
  }
  void initStatee3sehoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  GameMicRole setRole() {
    return (_userInfo?.id ?? -1) == widget.roomId ? GameMicRole.owner : GameMicRole.audience;
  }

  @override
  Widget build(BuildContext context) {
    return _mic();
  }
  void build2exB1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _mic() {
    String _status = widget.user?.status ?? GameConst.Empty;
    bool _havePeople = _status == GameConst.Grabbed ||
        _status == GameConst.Muted ||
        _status == GameConst.MuteSelf ||
        _status == GameConst.MuteDouble;
    logger.v('hava people :$_havePeople, ${widget.user?.status}');
    return YBDDelayGestureDetector(
      onTap: () {
        String status = widget.user?.status ?? GameConst.Empty;
        if (widget.user == null) {
          YBDToastUtil.toast('not receive MicNotify');
        } else if (BlocProvider.of<YBDGameMicBloc>(context).state.gotMic! &&
            _role == GameMicRole.audience &&
            status == GameConst.Empty) {
          logger.v('22.3.16-----audience got mic click other empty mic');
        } else if (status == GameConst.Locked && _role == GameMicRole.audience) {
          logger.v('22.3.16-----user click lock mic');
        } else if (widget.user?.status == null) {
          logger.v('22.3.16-----user.status==null');
        } else {
          showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            backgroundColor: Colors.transparent,
            builder: (_) {
              return MultiBlocProvider(
                providers: [BlocProvider.value(value: BlocProvider.of<YBDGameMicBloc>(context))],
                child: YBDGameMicOperateView(
                  roomId: widget.roomId,
                  userId: _userInfo!.id,
                  micUser: widget.user,
                  role: _role,
                ),
              );
            },
          );
        }
      },
      child: Container(
        width: _px(80),
        height: _px(80),
        alignment: Alignment.center,
        child: Stack(
          alignment: Alignment.center,
          children: [
            _volumeWidget(),
            _headWidget(
              havePeople: _havePeople,
            ),
            _capHat(widget.roomId.toString() == widget.user?.userId)
          ],
        ),
      ),
    );
  }
  void _micGmJuboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 声音动效 待补充
  Widget _volumeWidget() {
    String status = widget.user?.status ?? GameConst.Empty;
    // logger.v('3.28----vol:${widget?.vol?.volume}');
    // 没有声音      || (widget?.vol?.volume ?? 0) <= 0
    if (widget.user == null ||
        widget.vol == null ||
        widget.muteOther! ||
        widget.muteSelf! ||
        status != GameConst.Grabbed) {
      return Container();
    } else {
      return Container(
        alignment: Alignment.center,
        width: _px(80),
        height: _px(80),
        child: YBDSpreadWidget(
          radius: _px(60),
          maxRadius: _px(95),
          spreadColor: Colors.white.withOpacity(0.3),
        ),
      );
    }
  }
  void _volumeWidgetqV6Uwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 背景 锁麦或者默认图片
  Widget _bgImg() {
    return Image.asset(
      (widget.user?.status ?? GameConst.Empty) == GameConst.Locked
          ? 'assets/images/game/game_mic_locked.png'
          : 'assets/images/game/game_mic_unlock.png',
      fit: BoxFit.cover,
      width: _px(65),
    );
  }

  /// 头像 icon_user_mute mic_off
  Widget _headWidget({
    required bool havePeople,
  }) {
    print('3.25----_headWidget--avatar:${widget.user?.avatar}');
    return havePeople
        ? Stack(
            children: [
              _micAvatar(),
            ],
          )
        : _bgImg();
  }

  Widget _micAvatar() {
    if (_showUnSpeakerIcon()) {
      return _unSpeakerIcon();
    }

    if (_showMutedIcon()) {
      return _mutedIcon();
    }

    if (_showMuteSelfIcon()) {
      return _mutedSelfIcon();
    }

    if (_showMutedAndMutedSelfIcon()) {
      return _mutedAndMutedSelfIcon();
    }

    if (_showMutedAndUnSpeakerIcon()) {
      return _mutedAndUnSpeakerIcon();
    }
    // 用户头像
    return YBDRoundAvatar(
      widget.user?.avatar ?? '',
      userId: _userInfo!.id,
      scene: "B",
      labelWitdh: 25,
      needNavigation: false,
      avatarWidth: 65,
    );
  }
  void _micAvatarKeKcvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 只显示喇叭图标
  Widget _unSpeakerIcon() {
    return YBDRoundAvatar(
      // 用户头像
      widget.user?.avatar ?? '',
      userId: _userInfo!.id,
      scene: "B",
      lableSrc: 'assets/images/liveroom/mic_off.webp',
      showVip: true,
      labelWitdh: 25,
      needNavigation: false,
      avatarWidth: 65,
    );
  }

  /// 只显示当前用户自己禁言图标
  Widget _mutedSelfIcon() {
    return YBDRoundAvatar(
      // 用户头像
      widget.user?.avatar ?? '',
      userId: _userInfo!.id,
      scene: "B",
      labelWitdh: 40,
      showVip: false,
      needNavigation: false,
      avatarWidth: 65,
      showRightBottomImg: true,
      rightBottomImg: 'assets/images/icon_user_mute.webp',
    );
  }
  void _mutedSelfIconpFDReoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 只显示房主禁言图标
  Widget _mutedIcon() {
    return YBDRoundAvatar(
      // 用户头像
      widget.user?.avatar ?? '',
      userId: _userInfo!.id,
      scene: "B",
      labelWitdh: 40,
      showVip: false,
      needNavigation: false,
      avatarWidth: 65,
      showRightBottomImg: true,
      rightBottomImg: 'assets/images/icon_owner_mute.webp',
    );
  }

  /// 显示房主禁言和喇叭图标
  Widget _mutedAndUnSpeakerIcon() {
    return YBDRoundAvatar(
      // 用户头像
      widget.user?.avatar ?? '',
      userId: _userInfo!.id,
      scene: "B",
      lableSrc: 'assets/images/liveroom/mic_off.webp',
      // 喇叭图标地址
      labelWitdh: 25,
      needNavigation: false,
      avatarWidth: 65,
      showVip: true,
      // 显示喇叭图标
      showLeftBottomImg: true,
      // 显示房主禁言图标
      leftBottomImg: 'assets/images/icon_owner_mute.webp',
      // 房主禁言图标
      leftBottomWidth: 40,
      leftOfLB: 10,
      bottomOfLB: 13,
    );
  }
  void _mutedAndUnSpeakerIcondamAooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 显示房主禁言和当前用户自己禁言图标
  Widget _mutedAndMutedSelfIcon() {
    return YBDRoundAvatar(
      // 用户头像
      widget.user?.avatar ?? '',
      userId: _userInfo!.id,
      scene: "B",
      labelWitdh: 25,
      needNavigation: false,
      avatarWidth: 65,
      showVip: false,
      // 不显示喇叭图标
      showLeftBottomImg: true,
      // 显示房主禁言图标
      leftBottomImg: 'assets/images/icon_owner_mute.webp',
      // 房主禁言图标
      leftBottomWidth: 40,
      leftOfLB: 10,
      bottomOfLB: 13,
      showRightBottomImg: true,
      // 显示当前用户自己禁言图标
      rightBottomImg: 'assets/images/icon_user_mute.webp', // 当前用户自己禁言图标
    );
  }

  /// 只显示喇叭图标
  /// 显示条件：
  /// (麦位抓麦 | 麦位自己禁言 ) & 麦位非当前用户 & 本地有将麦位静音
  bool _showUnSpeakerIcon() {
    final status = widget.user?.status;
    return (status == GameConst.Grabbed || status == GameConst.MuteSelf) && !_isCurrentUser() && widget.muteOther!;
  }
  void _showUnSpeakerIcon0wfFSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 显示房主禁言图标和喇叭图标
  /// 显示条件：
  /// 麦位房主禁言 & 麦位非当前用户 & 本地禁音
  /// 麦位双重禁言 & 麦位非当前用户 & 本地禁音
  bool _showMutedAndUnSpeakerIcon() {
    final status = widget.user?.status;
    return (status == GameConst.Muted && !_isCurrentUser() && widget.muteOther!) ||
        (status == GameConst.MuteDouble && !_isCurrentUser() && widget.muteOther!);
  }

  /// 显示房主禁言图标和当前用户自己禁言图标
  /// 显示条件：
  /// 麦位双重禁言 & 麦位是当前用户
  bool _showMutedAndMutedSelfIcon() {
    final status = widget.user?.status;
    return status == GameConst.MuteDouble && _isCurrentUser();
  }

  /// 只显示房主禁言图标
  /// 显示条件：
  /// 麦位房主禁言 & 麦位非当前用户 & 本地未禁音
  /// 麦位房主禁言 & 麦位是当前用户
  /// 麦位双重禁言 & 麦位非当前用户 & 本地未禁音
  bool _showMutedIcon() {
    final status = widget.user?.status;
    return (status == GameConst.Muted && !_isCurrentUser() && !widget.muteOther!) ||
        (status == GameConst.Muted && _isCurrentUser()) ||
        (status == GameConst.MuteDouble && !_isCurrentUser() && !widget.muteOther!);
  }
  void _showMutedIcone7LDRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 只显示当前用户自己禁言图标
  /// 显示条件：
  /// 麦位自己禁言 & 麦位是当前用户
  bool _showMuteSelfIcon() {
    return widget.user?.status == GameConst.MuteSelf && _isCurrentUser();
  }

  /// 麦位用户是当前用户
  bool _isCurrentUser() {
    return '${YBDCommonUtil.storeFromContext()!.state.bean!.id}' == widget.user?.userId;
  }

  /// 当前用户是房主
  bool _isOwner() {
    return _role == GameMicRole.owner;
  }

  // 左上角的房主帽子
  Widget _capHat(bool cap) {
    return Transform.translate(
        offset: Offset(-_px(27), -_px(27)),
        child: cap
            ? Container(
            child: Image.asset(
              YBDGameResource.assetPadding('game_user_cap', need2x: false),
              width: 24.dp750,
            ))
            : Container());
  }

  double _px(int px) {
    return ScreenUtil().setWidth(px);
  }
}
