import 'dart:async';


/*
 * @Author: William-Zhou
 * @Date: 2022-03-15 14:30:33
 * @LastEditTime: 2022-04-26 17:15:09
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart' as u;
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_mic_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_volume_event.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_event_handler.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/util/permission_ybd_util.dart';

/// 声音大小达到20才显示效果
const int ShowVolumeMinValue = 20;

class YBDGameMicBloc extends Bloc<GameMicEvent, YBDGameMicBlocState> {
  BuildContext? context;

  /// 房间ID
  int? _roomId;

  /// 当前登录用户信息
  u.YBDUserInfo? _userInfo;

  /// 当前登录用户是否在麦位上
  bool _gotMic = false;

  /// 麦位数据
  List<YBDGameMicInfoBodyMic?> _micUserList = List.filled(10, null);

  /// 被本地静音的用户
  List<String> _localMuteUserList = [];

  /// 订阅麦位socket消息
  StreamSubscription<YBDGameBase>? _micMsgSubscription;

  /// 在线人数
  int _userCount = 0;

  /// 水波纹信息
  List<YBDRtcVolumeInfo> _speakers = [];

  /// 订阅直播事件
  YBDGameMicBloc(YBDGameMicBlocState initialState, this.context, {int? roomIdx}) : super(initialState) {
    _roomId = int.parse(YBDRtcHelper.getInstance().roomId!);
    logger.v('YBDMicBloc constructor roomId: $_roomId');
    _userInfo = YBDCommonUtil.storeFromContext()!.state.bean;
    // 接收socket消息
    streamlisten();
  }

  @override
  Stream<YBDGameMicBlocState> mapEventToState(GameMicEvent event) async* {
    switch (event) {
      case GameMicEvent.Refresh:
        yield YBDGameMicBlocState(
          gotMic: _gotMic,
          micUserList: _micUserList,
          localMuteUserList: _localMuteUserList,
          userCount: _userCount,
          speakers: _speakers,
        );
        break;
    }
  }
  void mapEventToStateXzhqToyelive(GameMicEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  streamlisten() {
    logger.v('mic bloc streamlisten---------');
    _micMsgSubscription = YBDGameRoomSocketApi.getInstance().messageOutStream.listen((msg) {
      switch (msg.runtimeType) {
        case YBDGameMicInfoEntity:
          YBDGameMicInfoEntity entity = msg as YBDGameMicInfoEntity;
          _updateMic(entity.body!, entity.resources!);
          add(GameMicEvent.Refresh);
          break;
        case YBDGameRoomInfoEntity:
          YBDGameRoomInfoEntity e = msg as YBDGameRoomInfoEntity;
          _userCount = e.body?.userCount ?? 0;
          add(GameMicEvent.Refresh);
          break;
        case YBDGameVolumeEvent:
          YBDGameVolumeEvent e = msg as YBDGameVolumeEvent;
          _updateVolume(e);
          break;
        default:
          break;
      }
    });
  }

  /// 更新麦位UI
  _updateMic(YBDGameMicInfoBody body, YBDGameMicInfoResources res) {
    logger.v('3.23---YBDMicBloc streamlisten body: $body ${res.user}');
    YBDGameMicBean bean = body.mic!.getMicBeans(res);
    _gotMic = bean.isBoard;
    _micUserList = bean.micBeans;
    _checkMuteRemote(_micUserList);
  }

  /// 更新水波纹
  _updateVolume(YBDGameVolumeEvent vols) {
    _speakers = vols.speakers ?? [];
    bool hasVolume = false;
    if (_speakers.isNotEmpty) {
      for (YBDRtcVolumeInfo r in _speakers) {
        if (r.volume >= ShowVolumeMinValue) {
          logger.v('update volume: ${r.volume}');
          hasVolume = true;
        }
      }
    }
    if (hasVolume) {
      add(GameMicEvent.Refresh);
    }
  }

  /// 麦位操作枚举 [LockedMic] 、[GrabMic]、[DropMic]、[MuteMic]、[MuteSelfMic]
  /// [targetId] 操作的目标用户id
  micOperation(final SendMsgType type, final int? index, int? targetId) async {
    logger.i('===================_roomId:$_roomId type:$type targetId:$targetId index:$index');
    YBDPermissionUtil.microphone(() {
      YBDGameRoomSocketApi.getInstance().micOperation(targetId, index, type, onSuccess: (YBDGameBase data) {
        logger.v('3.24-----onSuccess-micOperation : type：$type  code: ');
      }, onTimeOut: () {
        logger.v('23-------onTimeOut');
      });
    });
  }

  /// 当前登录用户已上麦
  bool isCurrentUserGrabMic() {
    int? currentUserId = YBDCommonUtil.storeFromContext()?.state.bean?.id;
    YBDGameMicInfoBodyMic? currentMic = _micUserList.firstWhereOrNull(
      (YBDGameMicInfoBodyMic? element) => element?.userId == '$currentUserId',
      /*orElse: () => null*/);
    // 上麦状态
    return currentMic?.micOccupied ?? false;
  }
  void isCurrentUserGrabMicE3njvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 更新本地静音的用户列表
  _checkMuteRemote(List<YBDGameMicInfoBodyMic?> micBeans) {
    List<String?> haveMicUserIds = [];
    for (YBDGameMicInfoBodyMic? g in micBeans) {
      if (g!.status != GameConst.Empty && g.status != GameConst.Locked) {
        haveMicUserIds.add(g.userId);
      }
    }
    _localMuteUserList.removeWhere((e) {
      if (!haveMicUserIds.contains(e)) {
        ///如果用户已经不在麦位上，但是本地静音列表维护这个用户 就解除静音
        if (_localMuteUserList.contains(e)) {
          YBDRtcHelper.getInstance().getRtcApi.muteRemoteAudioStream(int.parse(e), false);
        }
        return true;
      }
      ;
      return false;
    });
  }

  /// 静音某个用户
  muteRemote(String uid, bool mute) {
    print('3.26----uid:$uid,mute:$mute');
    YBDRtcHelper.getInstance().getRtcApi.muteRemoteAudioStream(int.parse(uid), mute);
    if (mute) {
      _localMuteUserList.add(uid);
    } else {
      _localMuteUserList.remove(uid);
    }
    add(GameMicEvent.Refresh);
  }

  /// mute self
  muteSelf(bool mute, int index) {
    if (mute) {
      micOperation(SendMsgType.MuteSelfMic, index, _userInfo!.id);
    } else {
      micOperation(SendMsgType.MuteSelfMic, index, _userInfo!.id);
    }
  }

  // 当前用户禁言自己
  bool currentUserMutedSelf() {
    final currentId = YBDCommonUtil.storeFromContext()!.state.bean?.id;

    // 当前用户的麦位信息
    YBDGameMicInfoBodyMic? micInfo = _micUserList.firstWhereOrNull(
      (element) => '$currentId' == element?.userId,
      // orElse: () => null,
    );

    if (micInfo?.status == GameConst.MuteSelf) {
      logger.i('currentUserMutedSelf: $currentId');
      return true;
    }

    return false;
  }
  void currentUserMutedSelfhIGz1oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Future<Function?> close() {
    logger.v('mic_bloc close');
    _micMsgSubscription?.cancel();
    return Future.value(null);
  }
}

/// Mic消息
class YBDGameMicBlocState {
  /// 当前登录用户是否在麦位上
  bool? gotMic;

  /// 麦位信息
  List<YBDGameMicInfoBodyMic?>? micUserList;

  /// 被本地静音的用户
  List<String?>? localMuteUserList;

  /// 在线人数
  int? userCount;

  /// 水波纹信息
  List<YBDRtcVolumeInfo>? speakers;

  YBDGameMicBlocState({
    this.gotMic,
    this.micUserList,
    this.localMuteUserList,
    this.userCount,
    this.speakers,
  }) {
    if (null == gotMic) {
      gotMic = false;
    }

    if (null == micUserList) {
      micUserList = List.filled(10, null);
    }

    if (null == localMuteUserList) {
      localMuteUserList = List.filled(10, null);
    }

    if (null == userCount) {
      userCount = 0;
    }

    if (null == speakers) {
      speakers = [];
    }
  }
}

class YBDGameMicBean {
  ///是否是主播
  bool isBoard;
  List<YBDGameMicInfoBodyMic> micBeans;
  YBDGameMicBean(this.micBeans, this.isBoard);
}
