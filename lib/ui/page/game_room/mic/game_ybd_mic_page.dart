import 'dart:async';


/*
 * @Author: William-Zhou
 * @Date: 2022-03-08 10:01:36
 * @LastEditTime: 2022-10-13 18:35:51
 * @LastEditors: William
 * @Description: 
 */
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_game_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_mic_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_bloc.dart';
import 'package:oyelive_main/ui/page/game_room/mic/game_ybd_mic_item.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_event_handler.dart';

class YBDGameRoomMicPage extends StatelessWidget {
  const YBDGameRoomMicPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<YBDGameMicBloc, YBDGameMicBlocState>(
      builder: (context, state) {
        return YBDGameMicPage(
          roomId: int.parse(YBDRtcHelper.getInstance().roomId ?? '0'),
          users: state.micUserList,
          localMuteUserList: state.localMuteUserList,
          userCount: state.userCount,
          speakers: state.speakers,
        );
      },
    );
  }
  void buildeaqG0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 游戏房间麦位
class YBDGameMicPage extends StatefulWidget {
  final int? roomId;
  final int? userCount;
  final List<YBDGameMicInfoBodyMic?>? users;
  final List<String?>? localMuteUserList;
  final List<YBDRtcVolumeInfo>? speakers;

  YBDGameMicPage({this.roomId, this.users, this.localMuteUserList, this.userCount, this.speakers});

  @override
  _YBDGameMicPageState createState() => _YBDGameMicPageState();
}

class _YBDGameMicPageState extends BaseState<YBDGameMicPage> {
  YBDUserInfo? _userInfo;

  @override
  void initState() {
    super.initState();
    _userInfo = YBDUserUtil.getLoggedUser(context);
  }
  void initStateCp48Loyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    if (widget.users == null) {
      return _noDate();
    } else {
      // 埋点
      if (widget.roomId! > 0) {
        Future.delayed(Duration(seconds: 2), () {
          YBDGameRoomTrack().gameRoomTrack(event: YBDTATrackEvent.gameroom_enter);
          TA.gameRoomId = widget.roomId.toString();
        });
      }
      return Container(
        width: double.infinity,
        child: Column(
          children: [
            SizedBox(height: px(10)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _micRow(micNum: widget.users?.length ?? 0),
              ],
            ),
          ],
        ),
      );
    }
  }
  void myBuildo1l9foyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 在线人数
  Widget _onlineUsersCount(int? users) {
    return Container(
      width: px(65),
      height: px(65),
      margin: EdgeInsets.symmetric(horizontal: px(7.5)),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(px(32.5)), color: Colors.white.withOpacity(0.3)),
      child: Column(
        children: [
          Image.asset('assets/images/icon_login_user.png', width: px(40)),
          Text(users.toString(), style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(20)))
        ],
      ),
    );
  }
  void _onlineUsersCountbOkn0oyelive(int? users) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  double px(num px) {
    return ScreenUtil().setWidth(px);
  }

  // 没有数据的时候
  Widget _noDate() {
    return Container();
  }

  /// 麦位上的用户自己禁言自己
  bool _mutedSelf(YBDGameMicInfoBodyMic? mic, int? selfId) {
    // 麦位数据返回MuteDouble或MuteSelf表示自己禁言自己
    return mic?.status == GameConst.MuteDouble || mic?.status == GameConst.MuteSelf;
  }

  Widget _micRow({int micNum = 10}) {
    List<Widget> micList = List.generate(
        micNum,
        (index) => YBDGameMicItem(
              roomId: widget.roomId,
              user: widget.users![index],
              muteSelf: _mutedSelf(widget.users![index], YBDCommonUtil.storeFromContext()!.state.bean!.id),
              muteOther: widget.localMuteUserList!.contains(widget.users![index]?.userId ?? "0"),
              vol: getVol(widget.users![index]),
            ));
    return Expanded(
        child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ...micList,
                _onlineUsersCount(widget.userCount),
              ],
            )));
  }
  void _micRow2Fxcvoyelive({int micNum = 10}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  YBDRtcVolumeInfo? getVol(YBDGameMicInfoBodyMic? user) {
    String userId = user?.userId ?? '';
    // logger.v('volume info reload: $userId, ${widget.speakers}');
    if (userId.isNotEmpty && widget.speakers!.isNotEmpty) {
      for (YBDRtcVolumeInfo r in widget.speakers!) {
        if (r.uid.toString() == userId) return r;
      }
    }
    return null;
  }
}
