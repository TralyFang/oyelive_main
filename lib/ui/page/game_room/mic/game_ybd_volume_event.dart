import 'dart:async';


/*
 * @Author: William-Zhou
 * @Date: 2022-03-23 15:14:17
 * @LastEditTime: 2022-03-28 14:25:06
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'package:oyelive_main/ui/page/game_room/server/base_ybd_event_handler.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';

class YBDGameVolumeEvent extends YBDGameBase {
  List<YBDRtcVolumeInfo>? speakers;

  YBDGameVolumeEvent({this.speakers});
}
