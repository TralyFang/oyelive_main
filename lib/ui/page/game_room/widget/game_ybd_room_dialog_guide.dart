import 'dart:async';

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/service/task_service/task_ybd_service.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';

class YBDGameRoomDialogGuide {
  static bool _isShowing = false; // 是否正在展示了，避免重复展示

  /// 游戏房显示新手指引
  Future<void> showGameRoomGuide(BuildContext context) async {
    // showDialog(
    //   barrierDismissible: false,
    //   context: Get.context,
    //   builder: (BuildContext builder) {
    //     return _YBDGameRoomGuide();
    //   },
    // );
    // return;
    var showGame = await ConfigUtil.shouldShowGame(context: context, allPlatform: true);
    logger.v('_shouldShowGuide:  config: $showGame');
    if (!showGame) {
      return;
    }
    if (!(await _shouldShowGuide())) {
      logger.v('_shouldShowGuide: false');
      // 不显示引导
      return;
    }
    if (_isShowing) return;
    _isShowing = true;
    await precacheImage(AssetImage(YBDGameResource.assetPadding('ludo_tip1', need2x: false, isWebp: true)), context);
    await precacheImage(AssetImage(YBDGameResource.assetPadding('ludo_tip2', need2x: false, isWebp: true)), context);
    await precacheImage(AssetImage(YBDGameResource.assetPadding('ludo_tip3', need2x: false, isWebp: true)), context);
    showDialog(
      barrierDismissible: false,
      context: Get.context!,
      builder: (BuildContext builder) {
        return _YBDGameRoomGuide();
      },
    );

    /// 表示已经展示过了
    _saveTipGuideDisplayRecord();
  }
  void showGameRoomGuide0e0N0oyelive(BuildContext context)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是否需要显示新手指引
  /// true 显示引导，false 不显示引导
  Future<bool> _shouldShowGuide() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);
    if (guide != null && guide.contains(Const.GUIDE_GAME_ROOM)) {
      return false;
    } else {
      return true;
    }
  }
  void _shouldShowGuideEsiwhoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 保存显示记录
  /// 安装 app 后只需要显示一次
  Future<void> _saveTipGuideDisplayRecord() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);

    if (guide == null || guide.isEmpty) {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_GAME_ROOM);
    } else {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_GAME_ROOM);
    }
  }
  void _saveTipGuideDisplayRecordHPkzEoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class _YBDGameRoomGuide extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _YBDGameRoomGuideState();
}

class _YBDGameRoomGuideState extends State<_YBDGameRoomGuide> with SingleTickerProviderStateMixin {
  /// 要显示的引导队列
  List<Widget> _guideQueue = [];

  int showIndex = 0;
  @override
  void dispose() {
    YBDGameRoomDialogGuide._isShowing = false;
    super.dispose();
  }
  void dispose9yCyKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  late AnimationController _controller;

  late Animation<double> _fadeAnimation;

  @override
  void initState() {
    super.initState();
    // _guideQueue.add(_Step1()); // 第一页不要了
    _guideQueue.add(_YBDStep2());
    _guideQueue.add(_YBDStep3());
    _guideQueue.add(_YBDStep4());
    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 800));
    _fadeAnimation = Tween(begin: 1.0, end: 0.0).animate(_controller);
    _controller.addListener(() {
      setState(() {});
    });
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _popGuide();
      }
    });
    _controller.reset();
  }
  void initStateg4C5Ioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    print("valuexxxxx ${_controller.value}");
    return WillPopScope(
      onWillPop: () async {
        logger.v("clicked the room guide back btn");
        _popGuide();
        return false;
      },
      child: GestureDetector(
        onTap: () {
          logger.v("clicked the room guide bg");
          _popGuide();
        },
        child: Opacity(
          opacity: _fadeAnimation.value,
          child: Container(
            decoration: BoxDecoration(),
            width: ScreenUtil().screenWidth,
            height: ScreenUtil().screenHeight,
            child: IndexedStack(
              index: showIndex,
              children: _guideQueue,
            ),
          ),
        ),
      ),
    );
  }
  void buildcdQfdoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _currentGuideWidget() {
    if (_guideQueue.isNotEmpty) {
      return _guideQueue.first;
    }

    logger.e("game room guide empty: $_guideQueue");
    return _YBDStep2();
  }

  /// 移除已显示的引导
  void _popGuide() {
    if (showIndex < _guideQueue.length - 1) {
      // 移除已显示的引导
      // if (_guideQueue.length == 1 && _controller.status != AnimationStatus.completed) {
      //   _controller.forward();
      // } else {
      //   _guideQueue.removeAt(0);
      // }
      showIndex++;
      setState(() {});
    } else {
      YBDGameRoomDialogGuide._isShowing = false;
      Navigator.pop(context);
    }

    // if (_guideQueue.isEmpty) {
    //   // 退出引导
    //   YBDGameRoomDialogGuide._isShowing = false;
    //   Navigator.pop(context);
    // } else {
    //   // 显示下一个引导
    //   setState(() {});
    // }
  }
  void _popGuideV5XQLoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class _YBDStep2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _top = ScreenUtil().statusBarHeight + 190.px;
    return Stack(
      children: [
        Positioned(
          left: 0,
          top: 0,
          right: 0,
          child: Container(
            color: Colors.red,
            child: Image.asset(
              YBDGameResource.assetPadding('ludo_tip1', need2x: false, isWebp: true),
              fit: BoxFit.fitWidth,
              // width: 680.px,
            ),
          ),
        ),
      ],
    );
    return Column(
      children: <Widget>[
        SizedBox(
          height: _top,
        ),
        Image.asset(
          YBDGameResource.assetPadding('ludo_tip1', need2x: false, isWebp: true),
          fit: BoxFit.fitWidth,
          width: ScreenUtil().screenWidth - 20.px,
        ),
      ],
    );
  }
  void build7st6hoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class _YBDStep3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _top = ScreenUtil().statusBarHeight + (Platform.isIOS ? 540.px : 610.px);
    return Stack(
      children: [
        Positioned(
          left: 0,
          top: 0,
          right: 0,
          child: Container(
            color: Colors.red,
            child: Image.asset(
              YBDGameResource.assetPadding('ludo_tip2', need2x: false, isWebp: true),
              fit: BoxFit.fitWidth,
              // width: 680.px,
            ),
          ),
        ),
      ],
    );
    return Stack(
      children: <Widget>[
        Positioned.fill(
          top: _top,
          right: -86.px,
          child: Image.asset(
            YBDGameResource.assetPadding('ludo_tip2', need2x: false, isWebp: true),
            fit: BoxFit.fitWidth,
            width: 680.px,
          ),
        ),
      ],
    );
  }
  void buildJgrwJoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class _YBDStep4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _top = ScreenUtil().statusBarHeight + (Platform.isIOS ? 1180.px : 1280.px);
    return Stack(
      children: [
        Positioned(
          left: 0,
          top: 0,
          right: 0,
          child: Container(
            color: Colors.red,
            child: Image.asset(
              YBDGameResource.assetPadding('ludo_tip3', need2x: false, isWebp: true),
              fit: BoxFit.fitWidth,
              // width: 680.px,
            ),
          ),
        ),
      ],
    );
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned(
          top: ScreenUtil().setWidth(_top),
          right: ScreenUtil().setWidth(0),
          child: Container(
            width: ScreenUtil().setWidth(480),
            child: Image.asset(
              YBDGameResource.assetPadding('ludo_tip3', need2x: false, isWebp: true),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
      ],
    );
  }
  void build7p5y4oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class _YBDStep5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned(
          top: ScreenUtil().setWidth(427),
          child: Container(
            width: ScreenUtil().setWidth(666),
            child: Image.asset(
              YBDGameResource.assetPadding('game_room_guide_5_1', need2x: false),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        Positioned(
          top: ScreenUtil().setWidth(607),
          right: ScreenUtil().setWidth(190),
          child: Container(
            width: ScreenUtil().setWidth(222),
            child: Image.asset(
              YBDGameResource.assetPadding('game_room_guide_5_2', need2x: false),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        Positioned(
          top: ScreenUtil().setWidth(717),
          left: ScreenUtil().setWidth(200),
          child: Container(
            width: ScreenUtil().setWidth(257),
            child: Image.asset(
              YBDGameResource.assetPadding('game_room_guide_5_3', need2x: false),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
      ],
    );
  }
  void buildx56Froyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class _YBDStep6 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned(
          bottom: ScreenUtil().setWidth(560),
          right: ScreenUtil().setWidth(142),
          child: Container(
            width: ScreenUtil().setWidth(256),
            child: Image.asset(
              YBDGameResource.assetPadding('game_room_guide_6_1', need2x: false),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        Positioned(
          bottom: ScreenUtil().setWidth(380),
          right: ScreenUtil().setWidth(114),
          child: Container(
            width: ScreenUtil().setWidth(207),
            child: Image.asset(
              YBDGameResource.assetPadding('game_room_guide_6_2', need2x: false),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        Positioned(
          bottom: ScreenUtil().setWidth(280),
          right: ScreenUtil().setWidth(0),
          child: Container(
            width: ScreenUtil().setWidth(141),
            child: Image.asset(
              YBDGameResource.assetPadding('game_room_guide_6_3', need2x: false),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
      ],
    );
  }
  void buildizPqUoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
