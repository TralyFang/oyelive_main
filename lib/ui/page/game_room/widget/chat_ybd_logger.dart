import 'dart:async';

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/widget/chat_ybd_entering.dart';
import 'package:oyelive_main/ui/page/game_room/widget/chat_ybd_gift.dart';

import '../entity/chat_ybd_publish.dart';
import 'chat_ybd_game_result.dart';
import 'chat_ybd_normal.dart';

class YBDChatLogger {
  static YBDChatLogger? _instance;
  static YBDChatLogger getInstance() {
    _instance ??= YBDChatLogger();
    return _instance!;
  }

  StreamController _gameChatHistoryStreamController = StreamController<List<YBDChatPublish?>>.broadcast();
  StreamSink<List<YBDChatPublish?>> get _inAdd_gameChatHistory => _gameChatHistoryStreamController.sink as StreamSink<List<YBDChatPublish?>>;
  Stream<List<YBDChatPublish?>> get gameChatHistoryOutStream => _gameChatHistoryStreamController.stream as Stream<List<YBDChatPublish?>>;

  List<YBDChatPublish?> _chatList = [];

  add(YBDChatPublish? chat) {
    _chatList.add(chat);
    _inAdd_gameChatHistory.add(_chatList);
  }

  clear() {
    if (_chatList.isNotEmpty) _chatList.clear();
  }

  List<YBDChatPublish?> get chatList => _chatList;
}

class YBDChatItemSplit {
  static Widget getItem(YBDChatPublish data, bool isShowInSheet, {bool isLatest: false}) {
    switch (data.body!.type) {
      case GameConst.PublicChat:
        return YBDNormalChat(
          data,
          showInSheet: isShowInSheet,
          isLatest: isLatest,
        );
        break;
      case GameConst.EntryChat:
        return YBDEnteringChat(
          data,
          showInSheet: isShowInSheet,
          isLatest: isLatest,
        );
        break;
      case GameConst.YBDGiftChat:
        return YBDGiftChat(
          data: data,
          showInSheet: isShowInSheet,
          isLatest: isLatest,
        );
        break;
      case GameConst.GameReuslt:
        return YBDGameResultChat(
          data: data,
          showInSheet: isShowInSheet,
          isLatest: isLatest,
        );
        break;
      default:
        return SizedBox();
        break;
    }
  }
}
