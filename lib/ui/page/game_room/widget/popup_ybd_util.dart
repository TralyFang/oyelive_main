import 'dart:async';

import 'package:flutter/material.dart';

/// 从哪里出来
enum EasyPopupPosition { top, left, bottom, right, center }

class YBDEasyPopup {
  ///Dismiss the popup related to the specific BuildContext
  static pop(BuildContext context) {
    // 这个context只能是弹出widget的context才能根据Scaffold查到_YBD_PopRouteWidgetState
    YBDEasyPopupRoute.pop(context);
  }

  ///Show popup
  static show(
    BuildContext context,
    Widget child, {
    bool cancelable = true, // 物理返回
    bool outsideTouchCancelable = true, // 点击内容外面的区域关闭
    bool contentTouchCancelable = false, // 内容点击关闭, 只在outsideTouchCancelable=true下才生效
    bool darkEnable = true, // 是否有蒙板
    bool scaleEnable = true, // 中间弹出是否缩放(关闭操作感觉有点卡卡的)
    Duration duration = const Duration(milliseconds: 200),
    EasyPopupPosition position = EasyPopupPosition.top,
    VoidCallback? dismissCallback,
  }) {
    Navigator.of(context).push(
      YBDEasyPopupRoute(
        child: child,
        cancelable: cancelable,
        outsideTouchCancelable: outsideTouchCancelable,
        contentTouchCancelable: contentTouchCancelable,
        darkEnable: darkEnable,
        scaleEnable: scaleEnable,
        duration: duration,
        position: position,
        dismissCallback: dismissCallback,
      ),
    );
  }
}

class YBDEasyPopupRoute extends PopupRoute {
  final Widget child;
  final Duration duration;
  final bool cancelable;
  final bool outsideTouchCancelable;
  final bool contentTouchCancelable;
  final bool darkEnable;
  final bool scaleEnable;
  final EasyPopupPosition position;
  final VoidCallback? dismissCallback;

  YBDEasyPopupRoute({
    required this.child,
    this.cancelable = true,
    this.outsideTouchCancelable = true,
    this.contentTouchCancelable = false,
    this.darkEnable = true,
    this.scaleEnable = true,
    this.duration = const Duration(milliseconds: 300),
    this.position = EasyPopupPosition.top,
    this.dismissCallback,
  });

  @override
  Color? get barrierColor => null;

  @override
  bool get barrierDismissible => true;

  @override
  String? get barrierLabel => null;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    return _YBDPopRouteWidget(
      child: child,
      duration: duration,
      cancelable: cancelable,
      outsideTouchCancelable: outsideTouchCancelable,
      contentTouchCancelable: contentTouchCancelable,
      darkEnable: darkEnable,
      scaleEnable: scaleEnable,
      position: position,
      dismissCallback: dismissCallback,
    );
  }
  void buildPageTO7CGoyelive(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Duration get transitionDuration => duration;

  static pop(BuildContext context) {
    if (_YBD_PopRouteWidgetState.of(context) != null) {
      _YBD_PopRouteWidgetState.of(context)?.dismiss();
      Navigator.of(context).pop();
    }
  }
}

class _YBDPopRouteWidget extends StatefulWidget {
  final Widget? child;
  final Duration? duration;
  final bool? cancelable;
  final bool? outsideTouchCancelable;
  final bool? contentTouchCancelable;
  final bool? darkEnable;
  final bool? scaleEnable;
  final EasyPopupPosition? position;
  final VoidCallback? dismissCallback;

  _YBDPopRouteWidget({
    Key? key,
    this.child,
    this.duration,
    this.cancelable,
    this.outsideTouchCancelable,
    this.contentTouchCancelable,
    this.darkEnable,
    this.scaleEnable,
    this.position,
    this.dismissCallback,
  }) : super(key: key);

  @override
  _YBD_PopRouteWidgetState createState() => _YBD_PopRouteWidgetState();
}

class _YBD_PopRouteWidgetState extends State<_YBDPopRouteWidget> with SingleTickerProviderStateMixin {
  late Animation<double> alphaAnim;
  AnimationController? alphaController;

  late Animation<Offset> _slideAnimation;
  Animation<double>? _scaleAnimation;

  @override
  void initState() {
    super.initState();
    alphaController = AnimationController(
      vsync: this,
      duration: widget.duration,
    );
    alphaAnim = Tween<double>(begin: 0, end: 127).animate(alphaController!);

    /// 中间出来就使用缩放效果
    if (widget.position == EasyPopupPosition.center && widget.scaleEnable!) {
      _scaleAnimation = Tween<double>(begin: 0, end: 1).animate(alphaController!);
    } else {
      /// 从边上出来就移动
      _slideAnimation = Tween<Offset>(begin: positionOffset(), end: Offset.zero).animate(alphaController!);
    }
    alphaController!.forward();
  }
  void initStatejcGk8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Offset positionOffset() {
    switch (widget.position) {
      case EasyPopupPosition.top:
        return Offset(0, -1);
      case EasyPopupPosition.bottom:
        return Offset(0, 1);
      case EasyPopupPosition.left:
        return Offset(-1, 0);
      case EasyPopupPosition.right:
        return Offset(1, 0);
    }
    return Offset(0, -1);
  }

  static _YBD_PopRouteWidgetState? of(BuildContext context) {
    // 通过查找父级最近的Scaffold对应的ScaffoldState对象
    return context.findAncestorStateOfType<_YBD_PopRouteWidgetState>();
  }

  dismiss() {
    // alphaController?.reverse();
    widget.dismissCallback?.call();
  }

  @override
  void dispose() {
    dismiss();
    alphaController?.dispose();
    super.dispose();
  }
  void disposepXTLVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (widget.cancelable!) {
          dismiss();
          return true;
        }
        return false;
      },
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          if (widget.outsideTouchCancelable!) {
            dismiss();
            Navigator.of(context).pop();
          }
        },
        child: Stack(
          children: <Widget>[
            /// 是否带模版
            widget.darkEnable!
                ? AnimatedBuilder(
                    animation: alphaController!,
                    builder: (_, __) {
                      return Container(color: Colors.black.withAlpha(alphaAnim.value.toInt()));
                    },
                  )
                : Container(),

            /// 内容主体
            Scaffold(
              backgroundColor: Colors.transparent,
              body: widget.position == EasyPopupPosition.bottom
                  ? Stack(children: [
                      Positioned(
                        bottom: 0,
                        child: animationWidget(),
                      ),
                    ])
                  : animationWidget(),
            )
          ],
        ),
      ),
    );
  }
  void build5WpWtoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget animationWidget() {
    if (widget.position == EasyPopupPosition.center) {
      if (_scaleAnimation != null && widget.scaleEnable!)
        return Center(
          child: ClipRect(
            child: ScaleTransition(
              scale: _scaleAnimation!,
              child: widget.contentTouchCancelable!
                  ? widget.child
                  : GestureDetector(behavior: HitTestBehavior.opaque, onTap: () {}, child: widget.child),
            ),
          ),
        );

      /// 中间内容主体不做缩放了，直接展示
      return Center(
        child: ClipRect(
          child: widget.contentTouchCancelable!
              ? widget.child
              : GestureDetector(behavior: HitTestBehavior.opaque, onTap: () {}, child: widget.child),
        ),
      );
    }

    return ClipRect(
        child: SlideTransition(
      position: _slideAnimation,
      child: widget.contentTouchCancelable!
          ? widget.child
          : GestureDetector(behavior: HitTestBehavior.opaque, onTap: () {}, child: widget.child),
    ));
  }
  void animationWidgethib1Qoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
