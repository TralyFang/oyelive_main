import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/page/game_room/widget/item_ybd_event.dart';
import 'package:oyelive_main/ui/page/home/entity/daily_ybd_check_entity.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../home/entity/daily_ybd_task_entity.dart';

class YBDEventDialog extends StatefulWidget {
  /// 决定了跳转的处理方式
  String location;
  List<YBDDailyCheckRecord?>? data;

  Function? onUpdate;

  YBDEventDialog(this.data, this.location, {this.onUpdate(List<YBDDailyCheckRecord?> update)?});

  @override
  YBDEventDialogState createState() => new YBDEventDialogState();
}

class YBDEventDialogState extends State<YBDEventDialog> {
  List<YBDDailyCheckRecord?>? _taskList;

  /// 请求每日任务列表
  Future _requestDailyTask() async {
    YBDMyDailyTaskEntity? dailyTaskEntity = await ApiHelper.queryDailyTask(context, viewing: 2);

    if (null != dailyTaskEntity && null != dailyTaskEntity.record) {
      _taskList = dailyTaskEntity.record;
      widget.onUpdate?.call(_taskList);
      //
      // // 刷新一次性任务列表
      // add(DailyTaskEvent.RefreshRewardList);

      // 过滤出每日签到任务并刷新每日签到任务奖励列表
      // _filterDailyTask(dailyTaskEntity);

      setState(() {});
    } else {
      logger.v('game task rewardList is null');
    }
  }
  void _requestDailyTaskp4FiLoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: ScreenUtil().setWidth(634),
          height: ScreenUtil().setWidth(922),
          margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(44)),
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/images/bg_gevent.webp'), fit: BoxFit.fitWidth),
          ),
          child: Column(
            children: [
              Row(
                children: [
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Padding(
                      padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
                      child: Icon(
                        Icons.close_rounded,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: ScreenUtil().setWidth(120),
              ),
              Expanded(
                child: Container(
                  width: ScreenUtil().setWidth(605),
                  // height: ScreenUtil().setWidth(800),
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage('assets/images/bg_inside.webp'), fit: BoxFit.fill),
                  ),
                  child: ListView.separated(
                      padding: EdgeInsets.all(0),
                      itemBuilder: (_, index) => YBDEventItem(_taskList![index], widget.location),
                      separatorBuilder: (_, index) => Container(
                            margin: EdgeInsets.only(
                              left: ScreenUtil().setWidth(150),
                              right: ScreenUtil().setWidth(38),
                            ),
                            width: double.infinity,
                            height: ScreenUtil().setWidth(1),
                            color: Colors.white.withOpacity(0.2),
                          ),
                      itemCount: _taskList!.length),
                ),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(14),
              ),
            ],
          ),
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/pinch.webp',
                  width: ScreenUtil().setWidth(60),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(310),
                ),
                Image.asset(
                  'assets/images/pinch.webp',
                  width: ScreenUtil().setWidth(60),
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setWidth(922),
            )
          ],
        ),
      ],
    );
  }
  void build7Q2Vvoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _taskList = widget.data;
    _requestDailyTask();
  }
  void initStateSlDF9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDEventDialog oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciescjLDmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
