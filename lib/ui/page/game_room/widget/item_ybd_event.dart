import 'dart:async';

import 'dart:developer';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/widget/prize_ybd_dialog.dart';
import 'package:oyelive_main/ui/page/home/entity/daily_ybd_check_entity.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/go_ybd_tab_event.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

import '../../../../main.dart';
import '../util/rtc_ybd_log.dart';

class YBDEventItem extends StatefulWidget {
  /// 决定了跳转的处理方式
  String location;
  YBDDailyCheckRecord? itemData;

  YBDEventItem(this.itemData, this.location);

  @override
  YBDEventItemState createState() => new YBDEventItemState();
}

class YBDEventItemState extends State<YBDEventItem> {
  claimBtn() {
    return YBDDelayGestureDetector(
      onTap: () {
        _claimDailyReward(context, widget.itemData!.id);
      },
      child: Container(
        width: ScreenUtil().setWidth(120),
        height: ScreenUtil().setWidth(52),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(ScreenUtil().setWidth(32)),
            ),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.5),
                  offset: Offset(0, ScreenUtil().setWidth(2)),
                  blurRadius: ScreenUtil().setWidth(4))
            ],
            gradient: LinearGradient(colors: [
              Color(0xffFFFBB2),
              Color(0xffFDD863),
            ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
        child: Center(
          child: Text(
            translate("collect"),
            style: TextStyle(
                fontSize: ScreenUtil().setSp(28),
                fontWeight: FontWeight.w900,
                color: Color(0xff855E00),
                fontFamily: "alegreya"),
          ),
        ),
      ),
    );
  }

  goBtn() {
    log("okok" + widget.itemData!.toJson().toString());
    return GestureDetector(
      onTap: () {
        /// 任务事件跳转
        ///
        ///
        ///

        var url = widget.itemData?.route ?? "";
        logger.v("task event tap route: $url");
        if (url.contains(YBDNavigatorHelper.index_page) || url.contains("flutter://main")) {
          YBDNavigatorHelper.popUntilMain(context);
          int? subIndex;
          try {
            String param = url.split("&").firstWhere((element) => element.startsWith("subIndex="));
            param = param.replaceAll("subIndex=", "");
            subIndex = int.parse(param);
          } catch (e) {
            print(e);
          }
          eventBus.fire(YBDGoTabEvent(1, subIndex: subIndex));
          // Future.delayed(Duration(mi), () {
          //   eventBus.fire(YBDGoTabEvent(int.parse(dailyTask[index].route.split("/").last)));
          // });
          return;
        }

        if (widget.location != YBDLocationName.GAME_HALL_PAGE) {
          /// 非大厅限制跳转,  只支持已知可正常跳转的路由，未知路由不做处理
          Navigator.pop(context);

          if (url.startsWith("http")) {
            YBDNavigatorHelper.remoteNavigator(context, url);
          }
          return;
        }

        if (widget.itemData!.name!.contains('Play Royal Pattern')) {
          YBDTeenInfo.getInstance().enterType = TPEnterType.TPTask;
          YBDModuleCenter.instance().enterRoom(type: EnterRoomType.OpenTp);
          return;
        }
        if (widget.itemData!.route!.contains("flutter://game")) {
          // String path = "flutter://game?type=fly&postion=home&index=3";
          YBDNavigatorHelper.navigateTo(context, widget.itemData!.route);
          logger.v('navgator page: ${Get.currentRoute}');
          return;
        }
        Navigator.pop(context);
        YBDNavigatorHelper.remoteNavigator(context, url);
      },
      child: Container(
        width: ScreenUtil().setWidth(120),
        height: ScreenUtil().setWidth(52),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(ScreenUtil().setWidth(32)),
            ),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.5),
                  offset: Offset(0, ScreenUtil().setWidth(2)),
                  blurRadius: ScreenUtil().setWidth(4))
            ],
            gradient: LinearGradient(colors: [
              Color(0xff0BF6FB),
              Color(0xff2DD9FF),
            ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
        child: Center(
          child: Text(
            translate("go"),
            style: TextStyle(
                shadows: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.5),
                      offset: Offset(0, ScreenUtil().setWidth(2)),
                      blurRadius: ScreenUtil().setWidth(4))
                ],
                fontSize: ScreenUtil().setSp(28),
                fontWeight: FontWeight.w900,
                color: Colors.white,
                fontFamily: "alegreya"),
          ),
        ),
      ),
    );
  }

  /// 获取任务的奖励
  _claimDailyReward(
    BuildContext context,
    int? taskId,
  ) async {
    bool ok = await ApiHelper.claimDailyReward(context, taskId);
    // YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
    //   YBDEventName.COMPLETE_TASK,
    //   location: YBDLocationName.TASK_PAGE,
    //   itemName: YBDItemName.CLAIM_DAILY_TASK,
    // ));
    // 刷新任务状态
    if (ok) {
      widget.itemData!.receive = true;
      setState(() {});

      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.COMPLETE_TASK,
        location: YBDLocationName.TASK_PAGE,
        itemName: YBDItemName.CLAIM_DAILY_TASK_SUCCESS,
      ));

      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return YBDPrizeDialog(widget.itemData!.currencyImage, widget.itemData!.reward);
          });
      // return;
      // context.read<YBDDailyTaskBloc>().add(DailyTaskEvent.LoadTask);
    } else {
      YBDToastUtil.toast('YBDOperation failed!');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(130),
      child: new Row(
        children: [
          SizedBox(
            width: ScreenUtil().setWidth(42),
          ),
          Image.asset(
            "assets/images/event_box.webp",
            width: ScreenUtil().setWidth(74),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(30),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(286),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(widget.itemData!.name!,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style:
                        TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white, fontWeight: FontWeight.w400)),
                SizedBox(
                  height: ScreenUtil().setWidth(4),
                ),
                new Row(
                  children: [
                    YBDNetworkImage(
                      imageUrl: widget.itemData!.currencyImage!,
                      width: ScreenUtil().setWidth(22),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(10),
                    ),
                    Text("${widget.itemData!.reward}",
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(22), color: Color(0xffFFE32D), fontWeight: FontWeight.w400)),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(10),
          ),
          widget.itemData!.status == 1 && !widget.itemData!.receive! ? claimBtn() : SizedBox(),
          widget.itemData!.receive != true && widget.itemData!.status != 1 && widget.itemData!.status != 2
              ? goBtn()
              : SizedBox(),
          if (widget.itemData!.receive ?? false)
            SizedBox(
              width: ScreenUtil().setWidth(120),
              child: Center(
                child: Image.asset(
                  "assets/images/ge_checked.webp",
                  width: ScreenUtil().setWidth(44),
                ),
              ),
            )
        ],
      ),
    );
  }
  void build0PYqdoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStateS07iFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDEventItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetnK67Toyelive(YBDEventItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
