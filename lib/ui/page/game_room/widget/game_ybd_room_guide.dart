import 'dart:async';

/*
 * @Author: William-Zhou
 * @Date: 2022-03-17 11:50:45
 * @LastEditTime: 2022-03-31 10:54:23
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/service/task_service/task_ybd_service.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';

class YBDGameRoomGuide {
  BuildContext context;
  static Function? _cb;

  /// 实例对象
  static late YBDGameRoomGuide _instance;

  /// 引导
  OverlayEntry? _step1, _step2, _step3, _step4, _step5, _step6;

  YBDGameRoomGuide(this.context);

  /// 是否需要显示新手指引
  static Future<bool> showGameRoomGuide(BuildContext context, {Function? changeCallback}) async {
    _instance = YBDGameRoomGuide(context);
    _cb = changeCallback;
    if (await _instance._shouldShowGuide()) {
      _instance._showGuide1();
      await _instance._saveTipGuideDisplayRecord();
      return true;
    }
    return false;
  }

  /// 是否需要显示新手指引
  /// true 显示引导，false 不显示引导
  Future<bool> _shouldShowGuide() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);
    if (guide != null && guide.contains(Const.GUIDE_GAME_ROOM)) {
      return false;
    } else {
      return true;
    }
  }
  void _shouldShowGuide4vlskoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 保存显示记录
  /// 安装 app 后只需要显示一次
  Future<void> _saveTipGuideDisplayRecord() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);

    if (guide == null || guide.isEmpty) {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_GAME_ROOM);
    } else {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_GAME_ROOM);
    }
  }
  void _saveTipGuideDisplayRecordHQHAXoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏所有新手指引
  void _hideAllGuide() {
    _hide(_step1);
    _hide(_step2);
    _hide(_step3);
    _hide(_step4);
    _hide(_step5);
    _hide(_step6);
    _hide(_step1);
  }
  void _hideAllGuide1HtMSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 1
  void _showGuide1() {
    _step1 = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              print('3.17-----tap step1 hide');
              _hide(_step1);
              _cb?.call();
              _showGuide2();
            },
            child: Container(
              color: Colors.black.withOpacity(0.6),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                    top: ScreenUtil().setWidth(78),
                    right: ScreenUtil().setWidth(43),
                    child: Container(
                      width: ScreenUtil().setWidth(170),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_1_1', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setWidth(156),
                    right: ScreenUtil().setWidth(110),
                    child: Container(
                      width: ScreenUtil().setWidth(105),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_1_2', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setWidth(272),
                    right: ScreenUtil().setWidth(100),
                    child: Container(
                      width: ScreenUtil().setWidth(220),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_1_3', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      );
    });
    Overlay.of(context)!.insert(_step1!);
  }
  void _showGuide1pSwiSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 2
  void _showGuide2() {
    _step2 = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              print('3.17-----tap step2 hide');
              _hide(_step2);
              _showGuide3();
            },
            child: Container(
              color: Colors.black.withOpacity(0.6),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                    top: ScreenUtil().setWidth(180),
                    child: Container(
                      width: ScreenUtil().setWidth(660),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_2_1', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setWidth(340),
                    left: ScreenUtil().setWidth(90),
                    child: Container(
                      width: ScreenUtil().setWidth(135),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_2_2', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setWidth(414),
                    right: ScreenUtil().setWidth(130),
                    child: Container(
                      width: ScreenUtil().setWidth(360),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_2_3', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      );
    });
    Overlay.of(context)!.insert(_step2!);
  }
  void _showGuide2qUKu8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 3
  void _showGuide3() {
    _step3 = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              print('3.17-----tap step3 hide');
              _hide(_step3);
              _showGuide4();
            },
            child: Container(
              color: Colors.black.withOpacity(0.6),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                    top: ScreenUtil().setWidth(355),
                    left: ScreenUtil().setWidth(25),
                    child: Container(
                      width: ScreenUtil().setWidth(335),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_3_1', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setWidth(490),
                    left: ScreenUtil().setWidth(162),
                    child: Container(
                      width: ScreenUtil().setWidth(135),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_3_2', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setWidth(590),
                    right: ScreenUtil().setWidth(148),
                    child: Container(
                      width: ScreenUtil().setWidth(292),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_3_3', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      );
    });
    Overlay.of(context)!.insert(_step3!);
  }
  void _showGuide3f6O1moyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 4
  void _showGuide4() {
    _step4 = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              print('3.17-----tap step4 hide');
              _hide(_step4);
              _showGuide5();
            },
            child: Container(
              color: Colors.black.withOpacity(0.6),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                    top: ScreenUtil().setWidth(355),
                    right: ScreenUtil().setWidth(25),
                    child: Container(
                      width: ScreenUtil().setWidth(335),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_4_1', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setWidth(490),
                    right: ScreenUtil().setWidth(180),
                    child: Container(
                      width: ScreenUtil().setWidth(135),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_4_2', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setWidth(560),
                    left: ScreenUtil().setWidth(142),
                    child: Container(
                      width: ScreenUtil().setWidth(292),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_4_3', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      );
    });
    Overlay.of(context)!.insert(_step4!);
  }
  void _showGuide4854OToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 5
  void _showGuide5() {
    _step5 = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              print('3.17-----tap step5 hide');
              _hide(_step5);
              _showGuide6();
            },
            child: Container(
              width: ScreenUtil().screenWidth,
              height: ScreenUtil().screenHeight,
              color: Colors.black.withOpacity(0.6),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                    top: ScreenUtil().setWidth(485),
                    child: Container(
                      width: ScreenUtil().setWidth(666),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_5_1', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setWidth(665),
                    right: ScreenUtil().setWidth(190),
                    child: Container(
                      width: ScreenUtil().setWidth(222),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_5_2', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setWidth(775),
                    left: ScreenUtil().setWidth(200),
                    child: Container(
                      width: ScreenUtil().setWidth(257),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_5_3', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  // Padding(
                  //   padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(250)),
                  //   child: Container(
                  //     width: ScreenUtil().setWidth(666),
                  //     child: Image.asset(
                  //       'assets/images/game_room_guide_5_1.png',
                  //       fit: BoxFit.fitWidth,
                  //     ),
                  //   ),
                  // ),
                  // Padding(
                  //   padding: EdgeInsets.only(top: ScreenUtil().setWidth(150), left: ScreenUtil().setWidth(100)),
                  //   child: Container(
                  //     width: ScreenUtil().setWidth(222),
                  //     child: Image.asset(
                  //       'assets/images/game_room_guide_5_2.png',
                  //       fit: BoxFit.fitWidth,
                  //     ),
                  //   ),
                  // ),
                  // Padding(
                  //   padding: EdgeInsets.only(top: ScreenUtil().setWidth(300), right: ScreenUtil().setWidth(100)),
                  //   child: Container(
                  //     width: ScreenUtil().setWidth(257),
                  //     child: Image.asset(
                  //       'assets/images/game_room_guide_5_3.png',
                  //       fit: BoxFit.fitWidth,
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
          )
        ],
      );
    });
    Overlay.of(context)!.insert(_step5!);
  }
  void _showGuide5xGVgQoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 6
  void _showGuide6() {
    _step6 = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              print('3.17-----tap step6 hide');
              _hide(_step6);
            },
            child: Container(
              color: Colors.black.withOpacity(0.6),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                    bottom: ScreenUtil().setWidth(560) + ScreenUtil().bottomBarHeight,
                    right: ScreenUtil().setWidth(142),
                    child: Container(
                      width: ScreenUtil().setWidth(256),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_6_1', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: ScreenUtil().setWidth(380) + ScreenUtil().bottomBarHeight,
                    right: ScreenUtil().setWidth(114),
                    child: Container(
                      width: ScreenUtil().setWidth(207),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_6_2', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: ScreenUtil().setWidth(280) + ScreenUtil().bottomBarHeight,
                    right: ScreenUtil().setWidth(0),
                    child: Container(
                      width: ScreenUtil().setWidth(141),
                      child: Image.asset(
                        YBDGameResource.assetPadding('game_room_guide_6_3', need2x: false),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      );
    });
    Overlay.of(context)!.insert(_step6!);
  }
  void _showGuide6oTfMnoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏
  void _hide(OverlayEntry? o) {
    try {
      o?.remove();
    } catch (e) {
      logger.v('3.17-----remove overlay:$o error : $e');
    }
  }
}
