import 'dart:async';

import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/room/widget/svga_ybd_ctrl_player.dart';
import 'package:oyelive_main/ui/page/store/widget/svga_ybd_play.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

enum MediaCacheWidgetType {
  /// 自动根据后缀名解析
  auto,

  /// png，jpg，gif, webp
  /// Image 支持的图片格式有 JPEG、WebP、GIF、animated WebP/GIF 、PNG 、BMP、 and WBMP。
  pic,

  /// svga
  svga,

  /// mp4
  video,

  /// mp3
  audio,

  /// 透明视频动画
  alpha,

  /// 压缩包，需要解压，再根据后缀名解析
  zip,

  /// 预留类型
  other,
}

extension MediaCacheWidgetTypeValue on MediaCacheWidgetType {
  MediaCacheWidgetType autoType(String? url,
      {MediaCacheWidgetType other = MediaCacheWidgetType.other}) {
    /// 只对自动类型匹配
    if (this != MediaCacheWidgetType.auto) {
      return this;
    }
    assert(url!.isNotEmpty, 'url cannot be empty');
    var last = url!.split('.').last.toLowerCase();
    switch (last) {
      case "png":
      case "jpg":
      case "gif":
      case "webp":
      case "jpeg":
        return MediaCacheWidgetType.pic;
      case "svga":
        return MediaCacheWidgetType.svga;
      case "mp4":
        return MediaCacheWidgetType.video;
      case "mp3":
        return MediaCacheWidgetType.audio;
      // case "alpha": // 该类型无法自动匹配
      //   return MediaCacheWidgetType.alpha;
      case "zip":
        return MediaCacheWidgetType.zip;
      default:
        return other;
    }
  }
}

/// 自动根据url匹配对应的widget, 暂定全屏BoxFit.cover模式展示
class YBDMediaCacheWidget extends StatefulWidget {
  String url;
  MediaCacheWidgetType type;

  /// 点击关闭/播放完关闭，作用于svga
  bool tapDismiss;
  final Widget? placeholder;

  YBDMediaCacheWidget({
    Key? key,
    required this.url,
    this.type = MediaCacheWidgetType.auto,
    this.tapDismiss = false,
    this.placeholder,
  }) : super(key: key);

  @override
  _YBDMediaCacheWidgetState createState() => _YBDMediaCacheWidgetState();
}

class _YBDMediaCacheWidgetState extends State<YBDMediaCacheWidget> {
  String? _localPath; //下载完成的地址

  @override
  Widget build(BuildContext context) {
    return matchTypeScreenWidget();
  }
  void build0Mxc5oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 匹配全屏
  Widget matchTypeScreenWidget() {
    if (widget.url.isEmpty) {
      /// 如果是空就不处理了
      logger.v("matchTypeScreenWidget matchTypeWidget empty url: ${widget.url}");
      return Container();
    }
    logger.v("matchTypeScreenWidget matchTypeWidget type url: ${widget.url}");
    switch (widget.type.autoType(widget.url, other: MediaCacheWidgetType.pic)) {
      case MediaCacheWidgetType.pic:
        return YBDNetworkImage(
          width: SizeNum.screentWidth,
          height: SizeNum.screentHeight,
          imageUrl: widget.url,
          fit: BoxFit.cover,
          fadeOutDuration: const Duration(milliseconds: 0),
          fadeInDuration: const Duration(milliseconds: 0),
          placeholder: (context, url) => widget.placeholder ?? YBDGameWidgetUtil.placeholdImage,
          errorWidget: (context, url, error) => widget.placeholder ?? YBDGameWidgetUtil.errorImage,
        );
        break;
      case MediaCacheWidgetType.svga:
      case MediaCacheWidgetType.video:
      case MediaCacheWidgetType.alpha:
        // case MediaCacheWidgetType.zip: // 需要处理zip，svga，mp3
      return FutureBuilder<File>(
          future: YBDDownloadCacheManager().getSingleFile(widget.url),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return YBDSvgaPlayerPage(
                url: snapshot.data!.path,
                mud: widget.tapDismiss ? 0 : 1,
              );
            }
            return widget.placeholder ?? Container();
          });
        if (_localPath == null) {
          /// 下载数据然后刷新
          YBDDownloadCacheManager.instance.downloadURL(widget.url, (path) {
            if (mounted) {
              setState(() {
                _localPath = path;
              });
            }
          });
          return widget.placeholder ?? Container();
        }

        /// 需要下载好资源才能播放
        return YBDSvgaPlayerPage(
          url: _localPath,
          mud: widget.tapDismiss ? 0 : 1,
        );
        break;

      default:
        break;
    }
    logger.v("matchTypeScreenWidget matchTypeWidget not type url: ${widget.url}");
    return widget.placeholder ?? Container();
  }
  void matchTypeScreenWidget0V9Jioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(covariant YBDMediaCacheWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.url != widget.url /*&& _localPath != null*/) {
      _localPath = null;
      setState(() {});
    }
  }
  void didUpdateWidgetcjZS5oyelive(covariant YBDMediaCacheWidget oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDMediaCacheWidthWidget extends StatefulWidget {
  String? url;
  MediaCacheWidgetType type;
  final double? width;
  final double? height;
  final Widget? placeholder;
  final BoxFit? fit;

  YBDMediaCacheWidthWidget(
      {required this.url, this.type = MediaCacheWidgetType.auto, this.width, this.height, this.placeholder, this.fit});

  @override
  _YBDMediaCacheWidthWidgetState createState() => _YBDMediaCacheWidthWidgetState();
}

class _YBDMediaCacheWidthWidgetState extends State<YBDMediaCacheWidthWidget> {
  String? _localPath; //下载完成的地址

  @override
  Widget build(BuildContext context) {
    if (widget.width != null && widget.height != null) {
      return Container(width: widget.width, height: widget.height, child: matchTypeWidthWidget());
    }
    return matchTypeWidthWidget();
  }
  void buildnPjgLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _cacheNetworkImageRetry(LoadingErrorWidgetBuilder errorWidget) {
    return YBDNetworkImage(
        imageUrl: widget.url!,
        fit: widget.fit,
        fadeOutDuration: const Duration(milliseconds: 100),
        fadeInDuration: const Duration(milliseconds: 100),
        placeholder: (context, url) => widget.placeholder ?? YBDGameWidgetUtil.placeholdImage,
        errorWidget: errorWidget);
  }
  void _cacheNetworkImageRetrytE3jwoyelive(LoadingErrorWidgetBuilder errorWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 根据width+height匹配
  Widget matchTypeWidthWidget() {
    if (widget.url?.isEmpty ?? true) {
      /// 如果是空就不处理了
      logger.v("matchTypeWidthWidget matchTypeWidget empty url: ${widget.url}");
      return widget.placeholder ?? Container();
    }
    // logger.v("matchTypeWidthWidget matchTypeWidget type url: ${widget.url}");
    switch (widget.type.autoType(widget.url, other: MediaCacheWidgetType.pic)) {
      case MediaCacheWidgetType.pic:
        return _cacheNetworkImageRetry((context, url, error) {
          logger.v("YBDMediaCacheWidthWidget Handle Error for the 1st time error: $error, url: $url");
          return _cacheNetworkImageRetry((context, url, error) {
            logger.v("YBDMediaCacheWidthWidget Handle Error for the 2nd time error: $error, url: $url");
            return _cacheNetworkImageRetry((context, url, error) {
              logger.v("YBDMediaCacheWidthWidget Handle Error for the 3rd time error: $error, url: $url, ${widget.placeholder == null}");
              return widget.placeholder ?? YBDGameWidgetUtil.errorImage;
            });
          });
        });
        break;
      case MediaCacheWidgetType.svga:

        return FutureBuilder<File>(
            future: YBDDownloadCacheManager().getSingleFile(widget.url!),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return YBDSVGACtrlPlayImage(
                  file: snapshot.data,
                  repeatPlay: true,
                );
              }
              return widget.placeholder ?? Container();
            });
        if (_localPath == null) {
          /// 下载数据然后刷新
          YBDDownloadCacheManager.instance.downloadURL(widget.url!, (path) {
            if (mounted) {
              setState(() {
                _localPath = path;
              });
            }
          });
          return widget.placeholder ?? Container();
        }

        /// 需要下载好资源才能播放
        return YBDSVGACtrlPlayImage(
          file: File(_localPath!),
          repeatPlay: true,
        );
        break;

      default:
        break;
    }
    logger.v("matchTypeWidthWidget matchTypeWidget not type url: ${widget.url}");
    return widget.placeholder ?? Container();
  }
  void matchTypeWidthWidgethF1DEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(covariant YBDMediaCacheWidthWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.url != widget.url /*&& _localPath != null*/) {
      _localPath = null;
      setState(() {});
    }
  }

  @override
  void dispose() {
    logger.v("matchTypeWidthWidget dispose");
    super.dispose();
  }
}
