import 'dart:async';

import 'package:oyelive_main/ui/page/game_room/util/rtc_ybd_log.dart';

class YBDGameLog {
  static void v(message) {
    logger.v(message);
  }
  static void i(message) {
    logger.i(message);
  }
}