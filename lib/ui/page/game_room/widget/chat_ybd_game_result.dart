import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../widget/rich_ybd_text_decoder.dart';
import '../entity/chat_ybd_publish.dart';

class YBDGameResultChat extends StatelessWidget {
  bool showInSheet;
  bool isLatest;
  YBDChatPublish? data;
  YBDGameResultChat({this.data, this.showInSheet: false, this.isLatest: false});

  @override
  Widget build(BuildContext context) {
    if (false)
      return Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setWidth(6)),
                  width: ScreenUtil().setWidth(156),
                  alignment: Alignment.topRight,
                  // child: Text(
                  //   "💕Ѕωєєту💕💕Ѕωє:",
                  //   style: TextStyle(
                  //     fontSize: ScreenUtil().setSp(22),
                  //     color: Colors.white,
                  //   ),
                  //   maxLines: 1,
                  //   overflow: TextOverflow.ellipsis,
                  //   textAlign: TextAlign.right,
                  // ),
                  child: Image.asset(
                    "assets/images/gc_dice.webp",
                    width: ScreenUtil().setWidth(28),
                  ),
                ),
              ],
            ),
            Container(
                width: ScreenUtil().setWidth(516),
                margin: EdgeInsets.only(left: ScreenUtil().setWidth(164)),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
                    color: Color(0xff1d1d1d).withOpacity(0.5)),
                padding:
                    EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(6), horizontal: ScreenUtil().setWidth(18)),
                // child: Text.rich(
                //     TextSpan(style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(22)), children: [
                //   TextSpan(text: "Congratulations to "),
                //   TextSpan(text: "Ringo", style: TextStyle(color: Color(0xff3DF9F8))),
                //   TextSpan(text: " winning "),
                //   TextSpan(text: "first", style: TextStyle(color: Color(0xffFBD279))),
                //   TextSpan(text: " place in OyeLudo."),
                // ])),
                child: YBDRichTextDecode().getRichTextWidget(data!.body!.content!))
          ],
        ),
      );

    Color backGroundColor;
    if (showInSheet)
      backGroundColor = Color(0xff1d1d1d).withOpacity(0.5);
    else
      backGroundColor = isLatest ? Colors.black.withOpacity(0.3) : Colors.white.withOpacity(0.3);

    return Row(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: ScreenUtil().setWidth(10), horizontal: showInSheet ? ScreenUtil().setWidth(30) : 0),
          child: Container(
            constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(590)),
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(16), vertical: ScreenUtil().setWidth(10)),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))), color: backGroundColor),
            // child: Row(
            //   mainAxisSize: MainAxisSize.min,
            //   crossAxisAlignment: CrossAxisAlignment.start,
            //   children: [
            //     Image.asset(
            //       "assets/images/gc_dice.webp",
            //       width: ScreenUtil().setWidth(28),
            //     ),
            //     SizedBox(
            //       width: ScreenUtil().setWidth(4),
            //     ),
            //     ConstrainedBox(
            //       constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(480)),
            //       child: Text.rich(
            //           TextSpan(style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(22)), children: [
            //         TextSpan(text: "Congratulations to "),
            //         TextSpan(text: "Ringo", style: TextStyle(color: Color(0xff3DF9F8))),
            //         TextSpan(text: " winning "),
            //         TextSpan(text: "first", style: TextStyle(color: Color(0xffFBD279))),
            //         TextSpan(text: " place in OyeLudo."),
            //       ])),
            //     ),
            //   ],
            // ),
            child: YBDRichTextDecode().getRichTextWidget(data!.body!.content!, fixTextSize: 22, pre: [
              WidgetSpan(
                  child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(4)),
                    child: Image.asset(
                      "assets/images/gc_dice.webp",
                      width: ScreenUtil().setWidth(28),
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(4),
                  ),
                ],
                mainAxisSize: MainAxisSize.min,
              ))
            ]),
          ),
        ),
      ],
    );
  }
  void buildhTSn0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
