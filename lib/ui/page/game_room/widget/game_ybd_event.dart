import 'dart:async';

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/event/game_ybd_mission_event.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_commom_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/dialog_ybd_event.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/home/entity/daily_ybd_check_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/daily_ybd_task_entity.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';

class YBDGameEvent extends StatefulWidget {
  /// 决定了跳转的处理方式 只有YBDLocationName.GAME_HALL_PAGE才支持全部跳转，其他会有限制
  String location;
  VoidCallback? didChangeWidgetCallback; // 辅助浮动控件正确获取大小
  YBDGameEvent(this.location, {this.didChangeWidgetCallback});

  @override
  YBDGameEventState createState() => new YBDGameEventState();
}

class YBDGameEventState extends State<YBDGameEvent> {
  @override
  Widget build(BuildContext context) {
    if (_taskList == null) return SizedBox();

    bool hasNoClaim = false;
    if (YBDGameCommomUtil.getGameConfig().suspendLink.isEmpty) {
      //如果显示大玩家活动图标时  红点不展示 2.5.1 平华需求变更
      _taskList!.forEach((element) {
        if (element!.status == 1 && (!element.receive!)) {
          hasNoClaim = true;
        }
      });
    }

    return GestureDetector(
      onTap: () {
        /// TODO: 测试begin======
        if (Const.TEST_ENV && YBDTPRoomGetLogic.openQuickEnter) {
          // ws://voice-cluster-314173693.cn-northwest-1.elb.amazonaws.com.cn:8090/oyetalk-game-tp/socket?&userId=2100212&roomId=1571743941951238144&category=tp&region=common
          String socketUrl = Uri.encodeComponent('ws://'
              // '192.168.5.175' // 志强本地
              // '161.189.115.104' // 测试环境
              '192.168.5.198' // 华厅本地
              // '192.168.5.188' // 李蒙本地
              ':9280/oyetalk-game-tp/socket?'
              'userId=${YBDUserUtil.getUserIdSync}'
              '&roomId=2100000'
              '&category=tp'
              '&region=cn'
              '&roomUserRole=1'
              '&userTag=0'
              '&needBot=false'
              '&clientType=${Platform.operatingSystem}');
          YBDNavigatorHelper.navigateTo(
              context,
              YBDNavigatorHelper.game_tp_room_page +
                  "?roomId=${2100001}"
                      "&socketUrl=$socketUrl");
          return;
        }

        /// TODO: 测试End======
        // _requestDailyTask();
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: widget.location,
          itemName: "game event",
        ));
        YBDCommonTrack().gameLobby('task');
        if (YBDGameCommomUtil.getGameConfig().suspendLink.isNotEmpty) {
          YBDNavigatorHelper.navigateToWeb(context,
              "?url=${Uri.encodeComponent(YBDGameCommomUtil.getGameConfig().suspendLink)}&title=${YBDGameCommomUtil.getGameConfig().sudpendTitle}");
          return;
        }

        /// 任务弹窗
        showDialog<Null>(
            context: context, //BuildContext对象
            barrierDismissible: false,
            builder: (BuildContext context) {
              return YBDEventDialog(
                _taskList,
                widget.location,
                onUpdate: (data) {
                  _taskList = data;
                },
              );
            });
      },
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          YBDImage(
            path: YBDGameCommomUtil.getGameConfig().suspendIcon,
            fit: BoxFit.cover,
            width: (widget.location == YBDLocationName.GAME_HALL_PAGE ? 110 : 86).toInt(),
            height: (widget.location == YBDLocationName.GAME_HALL_PAGE ? 110 : 86).toInt(),
            color: Colors.red,
            placeholder: (
              BuildContext context,
              String url,
            ) {
              return Image.asset(
                YBDGameResource.assetPadding('game_event', need2x: false, isWebp: true),
                width: widget.location == YBDLocationName.GAME_HALL_PAGE ? 110 : 86,
              );
            },
          ),
          Container(
            margin: EdgeInsets.all(ScreenUtil().setWidth(6)),
            width: ScreenUtil().setWidth(14),
            height: ScreenUtil().setWidth(14),
            decoration: BoxDecoration(shape: BoxShape.circle, color: hasNoClaim ? Colors.red : Colors.transparent),
          )
        ],
      ),
    );
  }
  void build1gyQOoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<YBDDailyCheckRecord?>? _taskList;

  /// 请求每日任务列表
  Future _requestDailyTask() async {
    YBDMyDailyTaskEntity? dailyTaskEntity = await ApiHelper.queryDailyTask(context, viewing: 2);

    if (null != dailyTaskEntity && null != dailyTaskEntity.record) {
      _taskList = dailyTaskEntity.record;
      //
      // // 刷新一次性任务列表
      // add(DailyTaskEvent.RefreshRewardList);

      // 过滤出每日签到任务并刷新每日签到任务奖励列表
      // _filterDailyTask(dailyTaskEntity);

      if (mounted) setState(() {});
      widget.didChangeWidgetCallback?.call();
    } else {
      logger.v('game task rewardList is null');
    }
  }
  void _requestDailyTask2PVAAoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _requestDailyTask();
    eventBus.on<YBDGameMissionEvent>().listen((event) {
      _requestDailyTask();
    });
  }
  void initStatePOIXwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDGameEvent oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesy55p6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
