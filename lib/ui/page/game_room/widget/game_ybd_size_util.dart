import 'dart:async';

import 'package:flutter_screenutil/flutter_screenutil.dart';

extension SizeNum on num {
  /// 导航高度96 状态栏高度48

  /// 尺寸适配
  double get dp720 {
    return ScreenUtil().setWidth(this);
  }

  /// 尺寸适配
  double get dp640 {
    return this * SizeNum.screentWidth / 640;
  }

  /// 尺寸适配
  double get dp750 {
    return this * SizeNum.screentWidth / 750;
  }

  /// 尺寸适配根据高度适配
  double get dpH1559 {
    /// 720*1559
    return this * SizeNum.screentHeight / 1559;
  }
  /// 尺寸适配根据高度适配
  double get dpH1624 {
    /// 750*1624
    return this * SizeNum.screentHeight / 1624;
  }

  /// 尺寸适配根据高度适配
  double get dpH1136 {
    /// 720*1559
    return this * SizeNum.screentHeight / 1136;
  }


  /// 距离顶部尺寸适配，自动适配刘海屏
  double get dpTopBar {
    return dp720 - 48.dp720 + SizeNum.statusBarHeight;
  }

  /// 相对底部更大的尺寸
  double get dpBottomBarMax {
    return SizeNum.bottomBarHeight.getMaxValue(this.dp720) as double;
  }

  /// 如果有安全区域，那就取调用者，否则取括号里
  double dpBottomElse(num size) {
    var result = SizeNum.bottomBarHeight > 0 ? dpBottomBarMax : size.dp720;
    print("safe$result");
    return result;
  }

  /// kloss2222如果没有安全区域，那就取调用者，否则取括号里
  double dpBottom2Else(num size) {
    var result = this.dpBottomElse(size);
    if(result==0)
      return result;

    return result-20.dp720;
  }

  /// 字体适配
  double get sp720 {
    return ScreenUtil().setSp(this);
  }
  /// 字体适配
  double get sp750 {
    return ScreenUtil().setSp(this*750.0/720.0);
  }

  /// 状态栏高度 dp 刘海屏会更高
  static double get statusBarHeight {
    return ScreenUtil().statusBarHeight;
  }

  /// 底部安全区距离 dp
  static double get bottomBarHeight {
    return ScreenUtil().bottomBarHeight;
  }

  /// 获取屏幕宽度
  static double get screentWidth {
    return ScreenUtil().screenWidth;
  }

  /// 获取屏幕高度
  static double get screentHeight {
    return ScreenUtil().screenHeight;
  }

  /// 获取更大的值
  num getMaxValue(num size) {
    return this > size ? this : size;
  }
}
