import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/entity/chat_ybd_publish.dart';
import 'package:oyelive_main/ui/page/game_room/entity/chat_ybd_view_event.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/chat_ybd_view.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/widget/chat_ybd_logger.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../main.dart';
import 'game_ybd_log_util.dart';
import 'game_ybd_widget_util.dart';

class YBDGameChatSheet extends StatefulWidget {
  bool autoFoucus;

  YBDGameChatSheet(this.autoFoucus);

  static show(BuildContext context, bool autoFoucus) {
    showChatView = false;

    eventBus.fire(YBDChatViewEvent());

    showModalBottomSheet(
        context: context, //
        isScrollControlled: true,
        enableDrag: false,
        barrierColor: Colors.transparent,
        backgroundColor: Colors.transparent, // BuildContext对象
        builder: (BuildContext context) {
          return YBDGameChatSheet(autoFoucus);
        }).then((value) {
      showChatView = true;
      eventBus.fire(YBDChatViewEvent());
    });
  }

  @override
  YBDGameChatSheetState createState() => new YBDGameChatSheetState();
}

class YBDGameChatSheetState extends BaseState<YBDGameChatSheet> {
  bool showInput = false;
  FocusNode _focusNode = new FocusNode();

  final maxInputLength = 70;
  TextEditingController _controller = TextEditingController();

  // List<YBDChatPublish> chatList = [];

  ScrollController _scrollController = new ScrollController();

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.transparent,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () {},
              child: Container(
                height: ScreenUtil().setWidth(900) + 65.dpBottom2Else(0),
                margin: EdgeInsets.only(bottom: showInput ? 0 : ScreenUtil().setWidth(0)),
                padding: EdgeInsets.only(bottom: 65.dpBottom2Else(0) as double),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(16))),
                    color: Color(0xff1d1d1d).withOpacity(0.1)),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Spacer(),
                        GestureDetector(
                          onTap: () {
                            YBDNavigatorHelper.popPage(context);
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
                            child: Icon(
                              Icons.expand_more_rounded,
                              color: Colors.white,
                              size: ScreenUtil().setWidth(50),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(16),
                        ),
                      ],
                    ),
                    Expanded(
                      child: StreamBuilder<List<YBDChatPublish?>>(
                          stream: YBDChatLogger.getInstance().gameChatHistoryOutStream,
                          initialData: YBDChatLogger.getInstance().chatList,
                          builder: (context, snapshot) {
                            if (snapshot.hasData)
                              return ListView.builder(
                                  controller: _scrollController,
                                  shrinkWrap: true,
                                  itemCount: snapshot.data!.length,
                                  itemBuilder: (_, index) => YBDChatItemSplit.getItem(snapshot.data![index]!, true));
                            return SizedBox();
                          }),
                    ),
                    if (!showInput)
                      Container(
                        height: 100.dp720,
                        child: Row(
                          children: [
                            SizedBox(
                              width: 23.dp720,
                            ),
                            YBDGameWidgetUtil.assetImage("game_chat_icon", width: 62.dp720, height: 61.dp720, onTap: () {
                              showInput = !showInput;
                              if (showInput)
                                _focusNode.requestFocus();
                              else
                                _focusNode.unfocus();

                              setState(() {});
                            }),
                          ],
                        ),
                      ),
                    if (showInput)
                      Container(
                        height: ScreenUtil().setWidth(100),
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
                        color: Colors.white,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: TextField(
                                focusNode: _focusNode,
                                maxLength: maxInputLength,
                                controller: _controller,
                                decoration: InputDecoration(counterText: '', isDense: true),
                              ),
                              width: ScreenUtil().setWidth(600),
                              height: ScreenUtil().setWidth(80),
                            ),
                            Spacer(),
                            GestureDetector(
                              onTap: () {
                                if (_controller.text.isNotEmpty) {
                                  YBDGameRoomSocketApi.getInstance().sendPublicMessage(content: _controller.text);
                                  _controller.clear();

                                  YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                                    YBDEventName.CLICK_EVENT,
                                    location: YBDLocationName.GAME_ROOM_PAGE,
                                    itemName: "send chat",
                                  ));
                                }
                                _focusNode.unfocus();
                                showInput = false;
                                setState(() {});
                              },
                              child: Container(
                                decoration: BoxDecoration(),
                                child: Icon(
                                  Icons.send,
                                  color: Colors.black,
                                  size: ScreenUtil().setWidth(60),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    // YBDNormalChat(
                    //   showInSheet: true,
                    // ),
                    // YBDGameResultChat(
                    //   showInSheet: true,
                    // ),
                    // YBDEnteringChat(
                    //   showInSheet: true,
                    // ),
                    // YBDGiftChat(
                    //   showInSheet: true,
                    // )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuild0QHR7oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // addSub(YBDGameRoomSocketApi.getInstance().messageOutStream.listen((event) {
    //   if (event is YBDChatPublish) {
    //     chatList.add(event);
    //     setState(() {});
    //   }
    // }));

    addSub(YBDChatLogger.getInstance().gameChatHistoryOutStream.listen((event) {
      scrollToLatest();
    }));

    if (widget.autoFoucus) {
      Future.delayed(Duration(seconds: 1), () {
        showInput = !showInput;
        if (showInput)
          _focusNode.requestFocus();
        else
          _focusNode.unfocus();

        setState(() {});
      });
    }

    scrollToLatest();
  }
  void initStateHBZdooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  scrollToLatest() {
    Future.delayed(Duration(seconds: 1), () {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 200),
        curve: Curves.fastOutSlowIn,
      );
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  void disposemd1I3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDGameChatSheet oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesaHSPuoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
