import 'dart:async';

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:oye_tool_package/widget/gradient_bound.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/page/game_room/util/tp_ybd_cache.dart';
import 'package:oyelive_main/ui/page/game_room/widget/ExtraHittest/gesture_ybd_detector.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_log_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/inner_ybd_shadow.dart';
import 'package:oyelive_main/ui/page/game_room/widget/media_ybd_cache_widget.dart';
import 'package:oyelive_main/ui/page/room/widget/svga_ybd_ctrl_player.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';

class YBDGameWidgetUtil {
  /// 占位图片
  static Widget get placeholdImage {
    return placeholdImageName('assets/images/rabbit_head.png');
  }

  /// 网络出错的占位图
  static Widget get errorImage {
    return placeholdImage;
  }

  static Widget placeholdImageName(String name) {
    Widget image = Image.asset(
      YBDGameResource.assetPadding(name),
    );
    return Container(
      // color: Colors.white.withOpacity(0.2),
      alignment: Alignment.center,
      child: ConstrainedBox(constraints: BoxConstraints(maxHeight: 55.dp720 as double, maxWidth: 55.dp720 as double), child: image),
    );
  }

  /// 空数据的占位符，如果是加载状态就只显示加载状态
  static Widget notDataAndLoading({String text = 'No Data', bool isLoading = false}) {
    if (isLoading) {
      return YBDLoadingCircle();
    }
    return YBDPlaceHolderView(text: text);
  }

  /// 渐变的背景装饰
  static Decoration linearGradientBackground() {
    return BoxDecoration(
        gradient: LinearGradient(
      colors: [
        Color(0xff6C6CDF),
        Color(0xff6C6CDF),
        Color(0xff4A7AB7),
        Color(0xff6C6CDF),
        Color(0xff3D0B8D),
      ],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
    ));
  }

  /// 默认的金色渐变边框
  static Widget gradientGold({required double radius, required Widget child}) {
    return GradientBound(
      radius: radius,
      strokeWidth: 1.dp750 as double,
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [YBDHexColor('#EFCD8B'), YBDHexColor('#FFE6B6'), YBDHexColor('#E9C282')],
      ),
      child: child,
    );
  }

  /// 阴影渐变的背景装饰, 是否从开始
  static Decoration showdowGradient({bool isFromTop = true}) {
    return BoxDecoration(
        gradient: LinearGradient(
      colors: [
        Color(0xff011343).withAlpha(isFromTop ? 255 : 0),
        Color(0xff011343).withAlpha(isFromTop ? 0 : 255),
      ],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
    ));
  }

  /// 按顺序展示
  static Widget streamBuilder(Stream<Widget> stream) {
    return StreamBuilder<Widget>(
        stream: stream,
        builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
          if (snapshot.hasData) {
            return snapshot.data!;
          }
          return Container();
        });
  }

  static ImageProvider assetTPImageProvider(String name, {bool need2x = true, bool isWebp = false}) {
    // 直接读取缓存数据
    String tpPath = YBDGameResource.assetTPPadding(name, need2x: need2x, isWebp: isWebp);
    if (tpPath.startsWith(YBDGameResource.assetsImages))
      return AssetImage(YBDGameResource.assetTPPadding(name, need2x: need2x, isWebp: isWebp));
    if (tpPath.startsWith('http')) {
      try {
        String assetPath = YBDGameResource.assetTPPadding(name, need2x: false, isWebp: isWebp, check: false);
        return AssetImage(assetPath);
      }catch(e) {
        return NetworkImage(tpPath);
      }
    }
    return FileImage(File(tpPath));
  }

  static Widget assetTPSvga(String name,
      {bool? startPlay, bool? repeatPlay, AnimationStatusListener? playStateListener, BoxFit? fit}) {
    String tpPath = YBDGameResource.assetTPSvga(name);
    if (tpPath.startsWith('http')) {
      // 缓存下来
      YBDDownloadCacheManager.instance.downloadURLToPath(tpPath, YBDTPCache.cacheFileDirPath() + '/svga/$name.svga',
          (String? path) {
        logger.v('assetTPSvga downloadURLToPath: $path');
      });
    }
    return YBDSVGACtrlPlayImage(
      assetsName: tpPath.startsWith(YBDGameResource.assetsImages) ? tpPath : null,
      file: tpPath.startsWith(YBDTPCache.cacheFileDirPath()) ? File(tpPath) : null,
      resUrl: tpPath.startsWith('http') ? tpPath : null,
      startPlay: startPlay,
      repeatPlay: repeatPlay,
      playStateListener: playStateListener,
      fit: fit,
    );
  }

  /// asset图片widget加载 tp_room资源目录下的图片名字
  static Widget assetTPImage(String? name,
      {double? width = 0,
      double? height = 0,
      double? minTapSize,
      Widget? child,
      bool isWebp = true,
      String? suffix,
      Widget? placeholder,
      BoxFit? fit,
      Duration delayDuration = const Duration(milliseconds: 1000),
      GestureTapCallback? onTap}) {
    return assetImage(name,
        width: width,
        height: height,
        minTapSize: minTapSize,
        child: child,
        isWebp: isWebp,
        suffix: suffix,
        isTpRes: true,
        fit: fit,
        placeholder: placeholder,
        delayDuration: delayDuration,
        onTap: onTap);
  }

  /// asset图片widget加载 game资源目录下的@2x.png名字
  static Widget assetImage(String? name,
      {double? width = 0,
      double? height = 0,
      double? minTapSize,
      Widget? child,
      bool isWebp = false,
      String? suffix,
      bool isTpRes = false, // 是否读取的tp_room目录下的资源， 默认游戏目录下
      Widget? placeholder,
      BoxFit? fit,
      Duration delayDuration = const Duration(milliseconds: 1000),
      GestureTapCallback? onTap}) {
    var image = child;
    if (child == null) {
      // 支持http图片加载
      if (name?.contains('http') ?? false) {
        image = YBDMediaCacheWidthWidget(
          url: name,
          width: width,
          height: height,
          placeholder: placeholder,
          fit: fit,
        );
      } else {
        if (isTpRes) {
          // 直接读取缓存数据
          String tpPath = YBDGameResource.assetTPPadding(name!, isWebp: isWebp, suffix: suffix);

          Widget? hasAssets() {
            try {
              String assetPath = YBDGameResource.assetTPPadding(name, need2x: false, isWebp: isWebp, suffix: suffix, check: false);
              // 异步加载有闪屏的问题
              // return FutureBuilder<ByteData>(
              //   future: rootBundle.load(assetPath),
              //     builder: (BuildContext context, AsyncSnapshot<ByteData> snapshot){
              //     if (snapshot.data != null) {
              //       ByteData data = snapshot.data;
              //       List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
              //       return Image.memory(bytes);
              //     }
              //     return placeholder ?? placeholdImage;
              //     });
              // 地址不存在会抛出异常，但是不影响UI
              return Image.asset(
                assetPath,
                width: width,
                height: height,
                fit: fit,
              );
            }catch (e) {
              logger.v('hasAssets is not found: ${e.toString()}');
              return placeholder;
            }
          }
          if (tpPath.startsWith(YBDGameResource.assetsImages))
            image = Image.asset(
              tpPath,
              width: width,
              height: height,
              fit: fit,
            );
          else if (tpPath.startsWith(YBDTPCache.cacheFileDirPath()))
            image = Image.file(
              File(tpPath),
              width: width,
              height: height,
              fit: fit,
            );
          else if (tpPath.startsWith('http'))
            image = YBDMediaCacheWidthWidget(
              url: tpPath,
              width: width,
              height: height,
              placeholder: hasAssets(),
              fit: fit,
            );
          else
            image = placeholdImage;
        } else {
          image = Image.asset(
            YBDGameResource.assetPadding(name!, isWebp: isWebp, suffix: suffix),
            width: width,
            height: height,
            fit: fit,
          );
        }
      }
    }

    if (onTap != null) {
      // 最少可点击的范围30*30
      minTapSize = minTapSize ?? 50.0.dp750;
      var vertical = (width! < minTapSize) ? (minTapSize - width) / 2.0 : 0;
      var horizontal = (height! < minTapSize) ? (minTapSize - height) / 2.0 : 0;
      //catch: type 'int' is not a subtype of type 'double'
      // YBDGameLog.v("assetImage----name:$name, width: $width, vertical: $vertical, horizontal: $horizontal");
      // 比较小的才需要处理
      return YBDDelayGestureDetector(
        extraHitTestArea: (vertical > 0 || horizontal > 0)
            ? EdgeInsets.symmetric(vertical: vertical.toDouble(), horizontal: horizontal.toDouble())
            : null,
        behavior: HitTestBehavior.opaque,
        delayDuration: delayDuration,
        onTap: onTap,
        child: image,
      );
    }
    return image ?? Container();
  }

  /// 头像url width=height, radius, sideBorder是否有边控
  static Widget avatar({
    url: '',
    userId: '',
    addSize: true, // 给全路径拼接图片尺寸
    double? width,
    double? radius,
    GestureTapCallback? onTap,
    bool sideBorder = false,
    double? sideWidth,
    Widget? placeholder,
  }) {
    var sizedUrl = url;
    if (addSize) {
      sizedUrl = YBDImageUtil.avatar(
        Get.context,
        url,
        YBDNumericUtil.stringToInt(userId),
        scene: 'B',
      );
    }

    return Container(
      decoration: sideBorder
          ? BoxDecoration(
              border: Border.fromBorderSide(BorderSide(width: sideWidth ?? 1.dp720 as double, color: Colors.white)),
              borderRadius: BorderRadius.circular(radius ?? width! / 2))
          : BoxDecoration(),
      child: GestureDetector(
        onTap: onTap,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(radius ?? width! / 2),
          child: Container(
            color: Colors.white.withOpacity(0.2),
            child: YBDMediaCacheWidthWidget(
              url: sizedUrl,
              fit: BoxFit.cover,
              placeholder: placeholder ?? placeholdImage,
              width: width,
              height: width,
            ),
            // CachedNetworkImage(
            //   imageUrl: sizedUrl ?? '',
            //   fit: BoxFit.cover,
            //   placeholder: (context, url) => placeholder ?? placeholdImage,
            //   errorWidget: (context, url, error) =>
            //       placeholder ?? placeholdImage,
            //   width: width,
            //   height: width,
            // ),
          ),
        ),
      ),
    );
  }

  static Widget networkImage(
      {url: '',
      BoxFit fit = BoxFit.cover,
      Alignment alignment = Alignment.center,
      double? width,
      double? height,
      Widget? placeholder,
      GestureTapCallback? onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: width,
        height: height,
        alignment: alignment,
        child: YBDMediaCacheWidthWidget(
          url: url,
          fit: fit,
          placeholder: placeholder ?? placeholdImage,
        ),
        // CachedNetworkImage(
        //   imageUrl: url ?? "",
        //   fit: fit,
        //   fadeOutDuration: const Duration(milliseconds: 0),
        //   fadeInDuration: const Duration(milliseconds: 0),
        //   placeholder: (context, url) => placeholder ?? placeholdImage,
        //   errorWidget: (context, url, error) => placeholder ?? errorImage,
        // ),
      ),
    );
  }

  /// 单行的普通文本
  static Widget textBalooRegular(
    String? text, {
    Color color = Colors.white,
    TextAlign? textAlign,
    double? fontSize,
    int lines = 1,
    double? lineHeight, // 行高，不是倍率哦
    Paint? foreground,
  }) {
    double? lineHeightScale;
    if (lineHeight != null && fontSize != null) {
      lineHeightScale = lineHeight / fontSize;
    }
    return Text(text ?? '',
        overflow: TextOverflow.ellipsis,
        maxLines: lines,
        textAlign: textAlign,
        style: TextStyle(
            fontSize: fontSize,
            color: color,
            height: lineHeightScale,
            fontWeight: FontWeight.w400,
            foreground: foreground,
            fontFamily: 'baloo'));
  }

  /// 文本添加描边
  static Widget textBalooStrokRegular(
    String text, {
    Color? color,
    Color? strokeColor,
    TextAlign? textAlign,
    double? fontSize,
    int lines = 1,
    bool addStroke = false,
  }) {
    color ??= YBDHexColor('#6A6A6A');

    if (!addStroke)
      return Text(text,
          overflow: TextOverflow.ellipsis,
          maxLines: lines,
          style: TextStyle(
            fontSize: fontSize,
            color: color,
            fontWeight: FontWeight.w400,
            fontFamily: 'baloo',
          ));

    // 默认描边颜色
    strokeColor = strokeColor ?? YBDHexColor('#838383');
    return Stack(
      children: [
        // 描边
        Text(text,
            overflow: TextOverflow.ellipsis,
            maxLines: lines,
            style: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.w400,
                fontFamily: 'baloo',
                foreground: Paint()
                  ..style = PaintingStyle.stroke
                  ..strokeWidth = 2.dp750 as double
                  ..color = strokeColor)),
        // 文字
        Text(text,
            overflow: TextOverflow.ellipsis,
            maxLines: lines,
            style: TextStyle(
              fontSize: fontSize,
              color: color,
              fontWeight: FontWeight.w400,
              fontFamily: 'baloo',
            )),
      ],
    );
  }

  /// 单行的普通文本
  static Widget textSingleRegular(
    String? text, {
    Color color = Colors.white,
    TextAlign? textAlign,
    double? fontSize,
    int lines = 1,
    FontWeight fontWeight = FontWeight.w400,
    String? fontFamily,
  }) {
    return Text(text ?? '',
        overflow: TextOverflow.ellipsis,
        maxLines: lines,
        textAlign: textAlign,
        style: TextStyle(fontSize: fontSize, color: color, fontWeight: fontWeight, fontFamily: fontFamily));
  }

  // static textRich() {
  //   Text.rich(TextSpan(
  //       style: TextStyle(
  //           fontSize: ScreenUtil().setSp(24), color: Color(0xffEE5480), fontWeight: FontWeight.w400),
  //       children: [
  //         TextSpan(text: "* "),
  //         TextSpan(
  //             text: "Take a screenshot and contact our\n   Customer service",
  //             style: TextStyle(color: Colors.black.withOpacity(0.7))),
  //       ])),
  // }

  /// 单行的Medium普通文本
  static Widget textSingleMedium(String text, {Color? color, double? fontSize, int lines = 1}) {
    return Text(text,
        overflow: TextOverflow.ellipsis,
        maxLines: lines,
        style: TextStyle(fontSize: fontSize, color: color, fontWeight: FontWeight.w500));
  }

  /// 单行的AlegreyaBlack
  static Widget textAlegreyaBlack(
    String text, {
    Color color = Colors.white,
    TextOverflow overflow = TextOverflow.ellipsis,
    required double? fontSize,
    bool hasShadow = false,
  }) {
    return Text(text,
        overflow: overflow,
        maxLines: 1,
        style: TextStyle(
            fontFamily: 'alegreya',
            fontSize: fontSize,
            shadows: hasShadow
                ? [
                    Shadow(
                      offset: Offset(0.0, 3.dp750 as double),
                      blurRadius: 4.dp750 as double,
                      color: Colors.black.withOpacity(0.5),
                    )
                  ]
                : [],
            color: color));
  }

  static var _lastTapTime = DateTime.now();
  // 防重复提交
  static bool _checkClick({int milliseconds = 200}) {
    if (_lastTapTime == null || DateTime.now().difference(_lastTapTime) > Duration(milliseconds: milliseconds)) {
      _lastTapTime = DateTime.now();
      YBDGameLog.v('_checkClick can tap....');
      return true;
    }
    YBDGameLog.v('_checkClick retap....');
    return false;
  }

  /// 背景图的标题按钮
  static Widget buttonTextBg(
    String text, {
    double? width,
    required double height,
    Color color = Colors.white,
    double? fontSize,
    String? bgName, // decoration中的背景图片名称
    Decoration? decoration, // 与 bgName 互斥
    GestureTapCallback? onTap,
    Widget? textChild,
    Duration delayDuration = const Duration(seconds: 1),
  }) {
    return YBDDelayGestureDetector(
      behavior: HitTestBehavior.opaque,
      delayDuration: delayDuration,
      onTap: onTap,
      child: ConstrainedBox(
        // centerSlice was used with a BoxFit that does not guarantee that the image is fully visible.
        constraints: BoxConstraints(minHeight: height, maxHeight: height),
        child: Container(
          width: width,
          height: height,
          decoration: decoration ??
              BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      // centerSlice: Rect.fromLTRB(45, 45, 45, 45),
                      image: AssetImage(YBDGameResource.assetPadding(bgName!)))),
          child: Center(
            child: textChild ??
                YBDGameWidgetUtil.textAlegreyaBlack(
                  text,
                  color: color,
                  fontSize: fontSize,
                  hasShadow: true,
                ),
          ),
        ),
      ),
    );
  }

  /// 背景图的标题按钮 tp房的通用
  static Widget buttonTextTPBg(
    String text, {
    double? width,
    double? height,
    Color color = Colors.white,
    double? fontSize,
    bool isWebp = true,
    String? bgName, // decoration中的背景图片名称
    Decoration? decoration, // 与 bgName 互斥
    GestureTapCallback? onTap,
    Widget? textChild,
    Duration delayDuration = const Duration(seconds: 1),
  }) {
    return YBDDelayGestureDetector(
      behavior: HitTestBehavior.opaque,
      delayDuration: delayDuration,
      onTap: onTap,
      child: Container(
        width: width,
        height: height,
        decoration: decoration ??
            BoxDecoration(
                image: DecorationImage(fit: BoxFit.fill, image: assetTPImageProvider(bgName!, isWebp: isWebp))),
        child: Center(
          child: textChild ??
              YBDGameWidgetUtil.textBalooRegular(
                text,
                color: color,
                fontSize: fontSize,
              ),
        ),
      ),
    );
  }
}
