import 'dart:async';

import 'dart:io';

import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/util/tp_ybd_cache.dart';

class YBDGameResource {
  /// png改成webp
  static List<String> WEBP_IMG = [
    'game_quicky_join',
    'game_room_guide_1_1',
    'game_room_guide_1_3',
    'game_room_guide_2_1',
    'game_room_guide_2_3',
    'game_room_guide_3_1',
    'game_room_guide_3_3',
    'game_room_guide_4_1',
    'game_room_guide_4_3',
    'game_room_guide_5_1',
    'game_room_guide_5_3',
    'game_room_guide_6_1',
    'game_setting_bg',
    'game_settle_bg',
  ];

  /// asset根据游戏目录下的注意自动拼接路径, 自定义后缀，默认为空
  static String assetPadding(String name, {String path = 'game', bool need2x = true, bool isWebp = false, String? suffix}) {
    // 如果是全路径就直接返回了
    if (name.contains('assets/')) {
      return name;
    }

    // 设置webp图片的文件后缀
    suffix ??= (WEBP_IMG.contains(name) || isWebp) ? 'webp' : 'png';
    return "${assetsImages}/$path/$name${need2x ? '@2x' : ''}.$suffix";
  }

  /// asset.tp_room目录下的自动拼接路径 check: 检测本地资源做替换
  static String assetTPPadding(String name, {bool need2x = true, bool isWebp = false, String? suffix, bool check = true}) {
    String path = assetPadding(name, path: 'tp_room', need2x: need2x, isWebp: isWebp, suffix: suffix);
    if (check) {
      path = repalceTPFilePath(path);
    }
    return path;
  }

  static String assetsImages = 'assets/images';

  static String repalceTPFilePath(String path) {
    String tpRoom = assetsImages + '/tp_room';
    // 卡牌, 洗牌动画直接使用工程文件
    if (!path.startsWith(tpRoom) ||
        path.startsWith('$tpRoom/cards') ||
        path.startsWith('$tpRoom/svga/shuffle.svga')) {
      logger.v('repalceTPFilePath assetsImages:$path');
      return path;
    }
    String localPath = path.replaceFirst(tpRoom, YBDTPCache.cacheFileDirPath());
    if (File(localPath).existsSync()) {
      logger.v('repalceTPFilePath localPath:$localPath');
      return localPath;
    }
    String networkPath = path.replaceFirst(tpRoom, YBDTPCache.singlePathProfix()!);
    logger.v('repalceTPFilePath networkPath:$networkPath');
    return networkPath;
  }

  static String assetTPSvga(String name) {
    String path = '${assetsImages}/tp_room/svga/$name.svga';
    path = repalceTPFilePath(path);
    return path;
  }

  /// 根据字符串判断对应的图标
  static String currencyIconAsset(String? str) {
    var currency = str ?? 'Gold';
    if (currency.toLowerCase().contains('bean')) {
      return '${assetsImages}/tiny_bean.webp';
    }
    return YBDGameResource.assetPadding('game_icon_gold');
  }

  /// 根据类型判断对应的图标
  static String currencyIconType(GameCurrencyType? type) {
    if (type == GameCurrencyType.beans) {
      return currencyIconAsset('bean');
    }
    return currencyIconAsset('gold');
  }

  /// 卡牌名称 默认背面牌
  static String tpCardName({YBDTpRoomDataCard? card}) {
    if (card == null)
      return 'cards/tpc_0_0'; // 默认背面牌
    return 'cards/tpc_${card.number}_${card.type}';
  }



}
