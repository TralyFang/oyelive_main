import 'dart:async';

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../widget/rich_ybd_text_decoder.dart';
import '../entity/chat_ybd_publish.dart';

class YBDNormalChat extends StatelessWidget {
  bool showInSheet;
  bool isLatest;
  YBDChatPublish data;

  YBDNormalChat(this.data, {this.showInSheet: false, this.isLatest: false});

  @override
  Widget build(BuildContext context) {
    log("${data.resources!.getSpecificUserInfo(data.body!.sender)["nickName"]}");
    String? nickName = data.resources!.getSpecificUserInfo(data.body!.sender)["property"]["nickName"];
    if (false)
      return Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setWidth(2)),
                  width: ScreenUtil().setWidth(156),
                  alignment: Alignment.topRight,
                  child: Text(
                    "$nickName:",
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(22),
                      color: Colors.white,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.right,
                  ),
                ),
              ],
            ),
            Container(
              // width: ScreenUtil().setWidth(516),
              // constraints: BoxConstraints.loose(Size(ScreenUtil().setWidth(516), ScreenUtil().setWidth(200))),
              constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(516)),

              margin: EdgeInsets.only(left: ScreenUtil().setWidth(164)),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
                  color: Color(0xff1d1d1d).withOpacity(0.5)),
              padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(6), horizontal: ScreenUtil().setWidth(18)),
              child:
                  // Text(
                  //   "Also need the header animation and the game of this event",
                  //   style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(22)),
                  // ),
                  YBDRichTextDecode().getRichTextWidget(data.body!.content!, fixTextSize: 22),
            )
          ],
        ),
      );

    Color backGroundColor;
    if (showInSheet)
      backGroundColor = Color(0xff1d1d1d).withOpacity(0.5);
    else
      backGroundColor = isLatest ? Colors.black.withOpacity(0.3) : Colors.white.withOpacity(0.3);

    return Row(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: ScreenUtil().setWidth(10), horizontal: showInSheet ? ScreenUtil().setWidth(30) : 0),
          child: Container(
            constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(590)),
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(16), vertical: ScreenUtil().setWidth(10)),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))), color: backGroundColor),
            child: YBDRichTextDecode().getRichTextWidget(data.body!.content!, fixTextSize: 22, pre: [
              TextSpan(text: "$nickName: "),
            ]),
          ),
        ),
      ],
    );
  }
  void buildLps4Woyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
