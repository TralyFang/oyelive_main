import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class YBDPrizeDialog extends StatelessWidget {
  String? image, prizeName;

  YBDPrizeDialog(this.image, this.prizeName);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: ScreenUtil().setWidth(620),
        height: ScreenUtil().setWidth(610),
        decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/bg_prize.png'))),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: ScreenUtil().setWidth(260),
            ),
            YBDNetworkImage(
              imageUrl: image!,
              width: ScreenUtil().setWidth(80),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(16),
            ),
            Text(
              prizeName!,
              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28)),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(54),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Image.asset(
                "assets/images/btn_prize_confirm.webp",
                width: ScreenUtil().setWidth(174),
              ),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(96),
            ),
          ],
        ),
      ),
    );
  }
  void buildSG6XDoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
