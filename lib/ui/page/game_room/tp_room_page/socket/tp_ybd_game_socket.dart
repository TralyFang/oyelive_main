import 'dart:async';

/*
* TP房
* Socket设计：
*  考虑使用 abstract mixin
*
*
* **/

import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:package_info/package_info.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/http/http_ybd_header.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/common/web_socket/message_ybd_handler.dart';
import 'package:oyelive_main/common/web_socket/receive/received_ybd_message_type.dart';
import 'package:oyelive_main/common/web_socket/receive/received_ybd_socket_message.dart';
import 'package:oyelive_main/common/web_socket/send/send_ybd_headers.dart';
import 'package:oyelive_main/common/web_socket/send/send_ybd_socket_message.dart';
import 'package:oyelive_main/common/web_socket/socket_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base_send_resp.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/ext/game_ybd_enum_ext.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket_api.dart';
import 'package:oye_tool_package/ext/string_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_gradient_dialog.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_now_watching.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_stand_tips.dart';

class YBDTPGameSocket {
  // 工厂模式
  factory YBDTPGameSocket() => YBDTPGameSocket.instance;
  static YBDTPGameSocket? _instance;

  YBDTPGameSocket._();

  static YBDTPGameSocket get instance => _instance ??= YBDTPGameSocket._();

  /// socket 连接对象
  IOWebSocketChannel? _channel;

  /// string or uri
  var _connectUrl;
  int packetId = 0;
  Timer? _queueCheckTimer;
  Timer? _pingTimer;
  List<YBDGameBaseSendResp> messageQueue = [];

  /// 公共数据配置
  YBDTPSocketConfig? config;

  /// 重连次数
  int tryConnect = 0;

  /// 连接状态发生改变了
  SocketStatus connectsStatus = SocketStatus.TryConnecting;
  StreamController<SocketStatus> _statusStreamStroller = StreamController<SocketStatus>.broadcast();
  Stream<SocketStatus> get statusStream => _statusStreamStroller.stream;

  /// 初始化 socket
  Future<void> initSocket(url, {YBDTPSocketConfig? config}) async {
    if (url == null) {
      logger.e('YBDTPGameSocket initSocket url is empty!');
      return;
    }

    /// 默认配置项
    this.config = config != null ? config : YBDTPSocketConfig.defaultConfig();
    config = this.config;

    if (url == _connectUrl && _channel != null) {
      // 已经存在了
      logger.v('YBDTPGameSocket initSocket url: ${url.toString()} did exist!');
      return;
    }

    if (_connectUrl == null) {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();

      bool addQuestion = url.toString().contains('?');

      String? category = url.toString().valueUriForKey('category');
      if (category != null) {
        this.config!.category = category;
      }
      String addString =
          // "&userId=${config.userId}"
          '&token=${await YBDSPUtil.get(Const.SP_COOKIE_APP)}'
          // "&roomId=${config.roomId}"
          // "&category=tp"
          // "&region=cn"
          '&otVersion=${packageInfo.buildNumber}'
          '&clientType=${Platform.operatingSystem}';
      _connectUrl = url.toString() + (addQuestion ? '' : '?') + addString;
    }

    logger.v('YBDTPGameSocket initSocket url: ${_connectUrl.toString()},'
        ' category:${this.config!.category}, start connecting');

    // 监听网络状态，
    Connectivity().onConnectivityChanged.listen((ConnectivityResult event) {
      if (event == ConnectivityResult.none) {
        connectStatusSink(SocketStatus.Close);
      } else {
        if (connectsStatus == SocketStatus.Connected) {
          connectStatusSink(SocketStatus.Connected);
        }
      }
    });

    try {
      Map<String, dynamic> header = {}; //await getHeader();
      header['upgrade'] = 'websocket';

      connectStatusDidChange(SocketStatus.TryConnecting);
      _channel = IOWebSocketChannel.connect(_connectUrl, pingInterval: Duration(seconds: 10), headers: header);
      _channel!.stream.listen((message) {
        receivedMessage(message);
      }, onError: (e) {
        logger.e('YBDTPGameSocket initSocket url: ${_connectUrl.toString()}, error: $e');
        // WebSocketChannelException: WebSocketChannelException: SocketException: Connection failed
        // (OS Error: Network is unreachable, errno = 101), address = 161.189.115.104, port = 9280
        // 连接出错了
        connectStatusDidChange(SocketStatus.Close);
        _channel = null;
        delayTryReconnect();
      }, onDone: () {
        // 连接已经断开了
        logger.e('YBDTPGameSocket initSocket url: ${_connectUrl.toString()}, done');
        connectStatusDidChange(SocketStatus.Close);
        _channel = null;
        delayTryReconnect();
      }, cancelOnError: true);
    } catch (e) {
      logger.e('YBDTPGameSocket initSocket url: ${_connectUrl.toString()}, catch error: $e');
    }

    /// 连接成功上报
    // sendMessage(await YBDSendSocketMessage.connect());

    /// socket链接状态改变
    /// 开始链接，断网，done，重试重连，error，延迟10s重试重连接，error，延迟10s重试重连，链接成功
  }
  void initSocketyLyqjoyelive(url, {YBDTPSocketConfig? config})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 链接状态发生改变了
  void connectStatusDidChange(SocketStatus status) {
    connectsStatus = status;
    connectStatusSink(status);
  }
  void connectStatusDidChange2j21Aoyelive(SocketStatus status) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void connectStatusSink(SocketStatus status) {
    if (_statusStreamStroller != null && !_statusStreamStroller.isClosed) {
      _statusStreamStroller.sink.add(status);
    }
  }
  void connectStatusSink8Enjroyelive(SocketStatus status) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 延迟10s再再尝试重连
  void delayTryReconnect() {
    Future.delayed(Duration(seconds: config?.reconnectTimer ?? 10), () {
      if (_channel == null) {
        reconnect();
      }
    });
  }

  /// 断开立马尝试重连
  void reconnect() {
    logger.v('YBDTPGameSocket reconnection config.alongeconnect: ${config!.alongeconnect}, tryConnect: $tryConnect');

    if (config!.alongeconnect ?? false) {
      initSocket(_connectUrl, config: config);
      return;
    }

    if (tryConnect < config!.autoTryconnect) {
      initSocket(_connectUrl, config: config);
      tryConnect += 1;
    }
  }
  void reconnectWGRfxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 心跳
  sendHeartbeat() {
    if (_pingTimer != null) {
      _pingTimer!.cancel();
      _pingTimer = null;
    }
    _pingTimer = Timer.periodic(Duration(seconds: config?.keepLive ?? 5), (_) async {
      /// 发送心跳
      sendMessage(await YBDSendSocketMessage.pingreq(roomId: config!.roomId));
    });
  }

  /// 收到心跳
  receivedHeartbeat() {}

  /// 发送socket
  sendMessage(String message) {
    checkSocketTimeout();
    if (kDebugMode) log('YBDTPGameSocket Send message to socket: $message');
    logger.v('YBDTPGameSocket Send message to socket: $message');
    _channel?.sink.add(message);

    // 添加游戏房socket埋点
    YBDAnalyticsUtil.socketStartRequest(message);
  }

  /// 根据模型类发送消息
  sendGameMsg(YBDGameBaseSendResp msg) async {
    // 自动生成packedId
    msg.packetId = getPacketId();
    if (msg.sendTime == null) msg.sendTime = DateTime.now();
    //  加入队列
    messageQueue.add(msg);

    // 组装消息
    String message = await YBDSendSocketMessage(
        body: msg.body ?? {},
        sendMessageType: msg.msgType.type,
        sendHeaders: YBDSendHeaders(extra: {
          GameConst.topic: '/room/${config?.roomId}',
          GameConst.packetId: msg.packetId.toString(),
          'route': 'mode=upstream,rpc=req'
        })).getMessage();

    sendMessage(message);
  }

  /// 接收到socket响应模型
  receivedMessage(message) {
    if (kDebugMode) log('YBDTPGameSocket receivedMessage: $message');
    logger.v('YBDTPGameSocket receivedMessage: $message');
    _messageHandler(message);
    // 添加游戏房socket埋点
    YBDAnalyticsUtil.socketStopRequest(message);
  }

  /// 匹配到对应的模型
  void matchRespMessage(YBDReceivedSocketMessage receivedSocketMessage) {
    if (receivedSocketMessage.data != null && config!.category == game_tpg) {
      YBDTpRoomEntity entity = YBDTpRoomEntity().fromJson(json.decode(receivedSocketMessage.data));
      // {"code":"091001","command":"seeCard","message":"Battle is not exist.","success":false,"timestamp":1660037059497}
      // 被踢出房间服务器推的command：roomout 没有code，
      // app当前玩家操作超时没有收到踢出房间的命令，主动退出房间。
      if (entity.data != null && entity.command == 'publish') {
        // "command":"publish",
        YBDTPGameSocketApi.instance.publishMessage(entity);
      } else {
        logger.e('YBDTPGameSocket matchRespMessage error: ${entity.message}');
        YBDToastUtil.toast(entity.message);
        if (entity.command == TpConst.ExitRoomCmd) {
          YBDNavigatorHelper.popUntilPre(YBDNavigatorHelper.game_tp_room_page);
          // 超时退出需要弹窗提示
          if (entity.data?.code == 920002 && entity.data?.message != null)
            // YBDTPNowWatching.show(Get.context, entity.data?.message);
            YBDTPGradientDialog.show(
              Get.context!,
              title: 'Now Watching',
              height: 400,
              type: DialogType.tpWatch,
              showClose: false,
              color: ColorType.purple,
              contentMsg: entity.data?.message,
            );
          else
            YBDToastUtil.toast(entity.data?.message);
        }
      }
      if (entity.command == TpConst.SelfStandUp) {
        YBDTPGradientDialog.show(
          Get.context!,
          title: 'Now Watching',
          height: 400,
          type: DialogType.tpWatch,
          showClose: false,
          color: ColorType.green,
          contentMsg: 'stand_up_tip'.i18n,
        );
      }
      if (entity.command == TpConst.BeKicked) {
        YBDToastUtil.toast(entity.data!.message);
      }
      return;
    }
    String? cmd = receivedSocketMessage.headers[GameConst.cmd];
    String? packId = receivedSocketMessage.headers[GameConst.packetId];
    logger.v('YBDTPGameSocket matchRespMessage: $cmd, packId:$packId');
    YBDGameBaseSendResp? result = messageQueue.firstWhereOrNull(
        (YBDGameBaseSendResp element) => element.packetId.toString() == packId && element.cmd == cmd);
    if (result != null) {
      logger.v('YBDTPGameSocket matchRespMessage callSuccess: ${result.cmd}');
      result.callSuccess(json.decode(receivedSocketMessage.data));
      messageQueue.remove(result);
    }
    logger.v('YBDTPGameSocket message queue: $messageQueue');

    // subAck(receivedSocketMessage: receivedSocketMessage);
  }
  void matchRespMessagemXvLeoyelive(YBDReceivedSocketMessage receivedSocketMessage) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 检测超时回调
  checkSocketTimeout() {
    if (_queueCheckTimer == null) {
      _queueCheckTimer = Timer.periodic(Duration(seconds: config?.checkTime ?? 1), (_) {
        DateTime now = DateTime.now();
        messageQueue.removeWhere((YBDGameBaseSendResp element) {
          bool timeout =
              now.difference(element.sendTime!).inSeconds > Duration(seconds: config?.timeout ?? 15).inSeconds;
          if (timeout) {
            element.onTimeOut?.call();
          }
          return timeout;
        });
        if (messageQueue.length == 0) {
          _queueCheckTimer!.cancel();
          _queueCheckTimer = null;
        }
      });
    }
  }

  /// dispose socket
  disposeSocket() {
    logger.v('YBDTPGameSocket disposeSocket');
    _channel?.sink.close(status.goingAway);
    _channel = null;
    _connectUrl = null;
    _queueCheckTimer?.cancel();
    _queueCheckTimer = null;
    _pingTimer?.cancel();
    _pingTimer = null;
    tryConnect = 0;
    _statusStreamStroller.close();

    /// 顺便移除监听操作类
    YBDTPGameSocketApi.instance.dispose();
  }

  /// 销毁 YBDTPGameSocket
  dispose() {
    logger.v('YBDTPGameSocket dispose');
    disposeSocket();
    _instance = null;
  }

  ///*************************************///
  int getPacketId() {
    ++packetId;
    return packetId;
  }
  void getPacketIdiOVIloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 协议对应关系 -> 自动处理
// socket连接成功发送连接报文CONNECT-->CONNACK(连接成功自动订阅，发送心跳，连接失败关闭连接)
// 心跳报文PINGREQ
// 进房订阅报文SUBSCRIBE-->SUBACK
// 退房报文UNSUBSCRIBE-->PUBLISH
// 其他操作报文PUBLISH-->PUBLISH
  void _messageHandler(message) async {
    YBDReceivedSocketMessage receivedSocketMessage = await Future.value(messageDecode(message));
    switch (receivedSocketMessage.receivedMessageType) {
      case ReceivedMessageType.CONNACK: // 连接成功回调
        if (receivedSocketMessage.headers['reason-code'] != null &&
            receivedSocketMessage.headers['reason-code'] == '0') {
          if (receivedSocketMessage.headers['keep-alive'] != null) {
            config!.keepLive = int.tryParse(receivedSocketMessage.headers['keep-alive']!) ?? 5;
          }

          /// 连接成功订阅
          // sendMessage(await YBDSendSocketMessage.subscribe(userId: config.userId));

          /// 建立周期心跳
          sendHeartbeat();

          /// 已经连成功了
          connectStatusDidChange(SocketStatus.Connected);
        } else {
          /// 服务器拒绝连接了
          disposeSocket();
        }
        break;
      case ReceivedMessageType.SUBACK:
      case ReceivedMessageType.UNSUBSCRIBE:
        matchRespMessage(receivedSocketMessage);
        break;
      case ReceivedMessageType.PINGREQ: // 收到心跳
        receivedHeartbeat();
        break;
      case ReceivedMessageType.PUBLISH:
        matchRespMessage(receivedSocketMessage);
        break;
      case ReceivedMessageType.DISCONNECT:

        /// 服务器要求断开连接
        disposeSocket();
        break;
      case ReceivedMessageType.NONE:
        // no furthur action
        break;
    }
  }
  void _messageHandlerz1EzKoyelive(message)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 公共数据配置
class YBDTPSocketConfig {
  String? roomId; // 房间Id YVKKYLHfFUKuSDI6gVzU
  int? userId; // 当前登录用户Id
  /// 超时时间
  int? timeout; // 15s
  /// 检测时间间隔
  int? checkTime; // 1s
  /// 心跳时间 根据socket回调重新设置
  int? keepLive; // 5s
  /// 自动重连次数
  late int autoTryconnect; // 3
  bool? alongeconnect; // 一直尝试重连, true时 autoTryconnect 不起作用
  int? reconnectTimer; // 尝试重连间隔 10s
  /// 游戏类型
  String? category; // 默认 game_tpg

  /// 默认配置参数
  static YBDTPSocketConfig defaultConfig() {
    return YBDTPSocketConfig()
      ..userId = YBDUserUtil.getUserIdSync
      ..roomId = ''
      ..timeout = 15
      ..checkTime = 1
      ..autoTryconnect = 3
      ..alongeconnect = true
      ..category = game_tpg
      ..reconnectTimer = 5
      ..keepLive = 5;
  }
}
