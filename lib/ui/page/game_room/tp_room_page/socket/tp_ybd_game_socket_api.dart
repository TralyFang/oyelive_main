import 'dart:async';

import 'dart:async';

import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/room_socket/message/common/subscribe.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base_send_resp.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket.dart';

/// 依赖于 YBDTPGameSocket
class YBDTPGameSocketApi {
  static YBDTPGameSocketApi? _instance;
  YBDTPGameSocketApi._();
  static YBDTPGameSocketApi get instance => _instance ??= YBDTPGameSocketApi._();

  StreamController _messageStreamController = StreamController<YBDGameBase?>.broadcast();
  Stream<YBDGameBase?> get messageOutStream => _messageStreamController.stream as Stream<YBDGameBase?>;

  /// 广播消息
  void publishMessage(YBDGameBase? message) {
    _messageStreamController.sink.add(message);
  }
  void publishMessageVoEf8oyelive(YBDGameBase? message) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 跟注
  chaal(int amount) {
    logger.v('YBDTPGameSocketApi chaal');
    _tpGameCmd(TpConst.BetCmd, body: {'amount': amount});
  }

  // 弃牌
  pack() {
    logger.v('YBDTPGameSocketApi pack');
    _tpGameCmd(TpConst.PackCardCmd);
  }

  // 比牌
  compared() {
    logger.v('YBDTPGameSocketApi compared');
    _tpGameCmd(TpConst.CompareCardCmd);
  }

  // 看牌
  see() {
    logger.v('YBDTPGameSocketApi see');
    _tpGameCmd(TpConst.SeeCardCmd);
  }

  // 开牌
  open() {
    logger.v('YBDTPGameSocketApi open');
    _tpGameCmd(TpConst.ShowCardCmd);
  }

  exitRoom() {
    logger.v('YBDTPGameSocketApi exitRoom');
    _tpGameCmd(TpConst.ExitRoomCmd);
  }

  // 站立
  void standUp(int? position) {
    logger.v('YBDTPGameSocketApi standUp position:$position');
    _tpGameCmd(TpConst.StandUpCmd, body: {'position': position});
  }
  void standUpuiudLoyelive(int? position) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 坐下
  void sitDown(int? position) {
    logger.v('YBDTPGameSocketApi SitDown position:$position');
    _tpGameCmd(TpConst.SitDownCmd, body: {'position': position});
  }
  void sitDownkK294oyelive(int? position) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 房主踢人
  void kickPlayer(int? kickPlayerId) {
    logger.v('YBDTPGameSocketApi kickPlayer kickPlayerId:$kickPlayerId');
    YBDTATrack().trackEvent('TP_go_kick', prop: YBDTAProps(target: kickPlayerId.toString()));
    _tpGameCmd(TpConst.KickPlayer, body: {'kickPlayerId': kickPlayerId});
  }
  void kickPlayersJbCpoyelive(int? kickPlayerId) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _tpGameCmd(String cmd, {Map<String, dynamic>? body}) {
    body = body ?? {};
    body.addAll({
      'command': cmd,
    });
    YBDTpRoomDataBattle? battle = YBDTPRoomGetLogic.findSync()?.state?.roomData?.battle;
    if (battle != null) {
      body.addAll({
        'term': battle.term,
        'battleId': battle.id,
      });
    }
    sendGameMsg(body: body);
  }

  sendGameMsg<E>(
      {Map<String, dynamic>? body, SendMsgType type = SendMsgType.YBDPublish, onSuccess(E data)?, onTimeOut}) async {
    body = body ?? {};
    body.addAll({
      'version': 1,
      'userId': YBDTPGameSocket.instance.config!.userId,
      'roomId': YBDTPGameSocket.instance.config!.roomId,
      'category': YBDTPGameSocket.instance.config!.category,
    });

    logger.v('YBDTPGameSocketApi sendGameMsg body: $body');

    YBDGameBaseSendResp resp =
        YBDGameBaseSendResp<YBDSubscribe, E>(body: body, msgType: type, onSuccess: onSuccess, onTimeOut: onTimeOut);
    resp.roomId = YBDTPGameSocket.instance.config!.roomId;
    YBDTPGameSocket.instance.sendGameMsg(resp);
  }

  dispose() {
    logger.v('YBDTPGameSocketApi dispose');
    _messageStreamController.close();
    _instance = null;
  }
}
