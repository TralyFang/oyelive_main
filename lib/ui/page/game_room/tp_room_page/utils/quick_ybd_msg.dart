
import 'dart:convert';

class YBDQuickMsgHelper {
  static Map? qm;
  static void storeQuickMsg(String quickMsg) {
    qm = json.decode(quickMsg);
  }

  static Map? getQuickList() => qm;

  static String queryFromData(String key) => qm![key] ?? key;
}
