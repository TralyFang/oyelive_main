
import 'dart:async';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_base.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_fly_bean_playing.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_stand_tips.dart';
import 'package:oyelive_main/ui/page/game_room/util/audio_ybd_player_manager.dart';

class YBDTPRoomGetLogic extends GetxController {
  static bool openTestData = false; // 开启测试数据
  static bool openQuickEnter = false; // 开启快捷房间入口

  YBDTPRoomGetState? state = YBDTPRoomGetState();

  /// 监听消息回调
  StreamSubscription? _gameRoomSubscription;

  StreamController? _stateController = StreamController<YBDTPRoomGetState?>.broadcast();
  Stream<YBDTPRoomGetState?> get stateStream => _stateController!.stream as Stream<YBDTPRoomGetState?>;

  YBDTPRoomGetLogic() {
    // 初始化
    state!.balance.value = YBDUserUtil.getUserMoneySync!;
    updateUserBalance();

    _listenSocket();
  }

  /// 监听游戏房socket消息
  void _listenSocket() {
    _gameRoomSubscription = YBDTPGameSocketApi.instance.messageOutStream.listen((YBDGameBase? msg) {
      switch (msg.runtimeType) {
        case YBDTpRoomEntity:
          YBDTpRoomEntity entity = msg as YBDTpRoomEntity;

          // 异常报文丢弃: 中间插入了老的对局创建时间，则丢弃
          int startTime = entity.data?.battle?.startTime ?? 0;
          int battleId = entity.data!.battle?.id ?? -1;
          if (startTime < state!.startTime && battleId != -1) {
            logger.e('Exception packets are discarded: $startTime, state.startTime:${state!.startTime}, $battleId');
            return;
          }
          if (battleId != -1 && startTime != 0) {
            state!.startTime = startTime;
          }

          // standUpListen(entity);
          state!.roomData = entity.data ?? YBDTpRoomData();
          // int battleId = state.roomData.battle?.id ?? -1;
          if (state!.battleId != battleId) {
            // 不是同一局了，那就重置数据
            state!.bankerFirstUsers = null;
            state!.playedCards = false;
            state!.canCountdown = false;
            state!.battleId = battleId;
            // 顺便更新下余额
            updateUserBalance();
          }
          state!.updateFirstMeUsers();
          // 下注动画数据预处理
          logicBidStateListen();
          // 更新UI
          update();
          // 由里面控制UI是否刷新
          _stateController!.sink.add(state);
          break;
        default:
          break;
      }
    });
  }
  void _listenSocket7DqO2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 下注回调监听
  logicBidStateListen() {
    YBDTPRoomGetLogic logic = this;
    YBDTPRoomGetState logicState = state!;

    /// 下底注
    YBDTpRoomDataAnimation? countdownAnimation =
        logicState.roomData.animation?.firstWhereOrNull((YBDTpRoomDataAnimation? e) => e!.sync == 1 && e.type == 3);
    YBDTpRoomDataAnimation? dingzhuangAnimation =
        logicState.roomData.animation?.firstWhereOrNull((YBDTpRoomDataAnimation? e) => e!.sync == 1 && e.type == 4);
    if (dingzhuangAnimation != null && dingzhuangAnimation.timeout! > 1000) {
      // 底池先减掉下注的，动画执行完成就加回来
      logger.e('boot bidAnimation chaalValue: ${logicState.roomData.battle!.chaalValue}');
      int? bootBean = logicState.roomData.battle!.chaalValue;
      logicState.roomData.battle!.chaalValue = 0;
      int? lastValue = logicState.roomData.battle!.chaalValue;
      Future.delayed(Duration(milliseconds: (countdownAnimation?.timeout ?? 0) + 1000), () {
        eventBus.fire(YBDTPRoomBeansEvent()
          ..lastValue = lastValue
          ..bidValue = bootBean
          ..isBootBid = true);
      });
    }

    bool hasBidAnimation = false;

    /// 投注成功
    logicState.roomData.animation?.forEach((YBDTpRoomDataAnimation? animation) {
      if (animation!.type == 1) {
        hasBidAnimation = true;
        // 下注动画
        int bidAnimationIndex =
            logicState.roomData.users!.indexWhere((YBDTpRoomDataUser? element) => element!.id == animation.operator);
        if (bidAnimationIndex >= 0 && bidAnimationIndex < YBDTPRoomBeansEvent.playerAvatarKeys!.length) {
          // 底池先减掉下注的，动画执行完成就加回来
          logger.e('bidAnimation chaalValue: ${logicState.roomData.battle?.chaalValue}');
          int chaal = logicState.roomData.battle?.chaalValue ?? 0;
          chaal -= animation.beans ?? 0;
          logicState.roomData.battle?.chaalValue = chaal;
          int? lastValue = logicState.roomData.battle?.chaalValue;
          // 用户头像飞向底注
          eventBus.fire(YBDTPRoomBeansEvent()
            ..beginKey = YBDTPRoomBeansEvent.playerAvatarKeys![bidAnimationIndex]
            ..endKey = TPBootBeansKey
            ..lastValue = lastValue
            ..bidValue = animation.beans ?? 0);
          // 下注成功音效
          YBDTPAudio.bidBean.play();
          if (animation.operator == logicState.curUserId) {
            // 更新下余额
            logic.updateUserBalance();
          }
        } else {
          logger.e('bidAnimation error index: $bidAnimationIndex');
        }
      }
    });

    logger.v('logicState.roomData.battle?.settle:start');

    /// 结算飞豆子: 自己飞向余额，别人飞向头像
    List<YBDTpRoomDataSettle?>? settle = logicState.roomData.battle?.settle;
    logger.v('logicState.roomData.battle?.settle:$settle');
    if (settle != null && settle.length > 0) {
      List<GlobalKey> settleKeys = [];
      settle.forEach((YBDTpRoomDataSettle? settleElement) {
        int index = logicState.roomData.users!
            .indexWhere((YBDTpRoomDataUser? userElement) => userElement!.id == settleElement!.userId);
        if (index >= 0) {
          if (settleElement!.userId != logicState.curUserId) {
            // 他人的飞向头像
            settleKeys.add(YBDTPRoomBeansEvent.playerAvatarKeys![index]);
          } else if (settleElement.userId == logicState.curUserId) {
            // 自己飞向余额
            settleKeys.add(TPBalanceBeansKey);
            // // 更新下余额
            logic.updateUserBalance();
          }
        }
      });
      if (settleKeys.length > 0) {
        // 结算动画
        settleAnimation() {
          YBDTPRoomBeansEvent event = YBDTPRoomBeansEvent()
            ..beginKey = TPBootBeansKey
            ..playerSettleKeys = settleKeys
            ..bidValue = settle[0]!.revenue;

          if (settleKeys.contains(TPBalanceBeansKey)) {
            // 先减掉赢的钱，动画完成再加回来
            // logicState.balance.value -= settle[0].revenue;
            // int lastValue = logicState.balance.value;
            // event.lastValue = lastValue;
          }
          eventBus.fire(event);
          // 结算音效
          YBDTPAudio.reward.play();
        }

        // 下注完成再播放结算
        if (hasBidAnimation) {
          Future.delayed(YBDTPRoomBeansEvent.duration + YBDTPRoomBeansScaleEvent.duration, () {
            settleAnimation();
          });
        } else {
          settleAnimation();
        }
      }
    }
  }

  /// 获取用户余额
  void updateUserBalance() {
    ///获取实时金币 同步到本地用户信息
    logger.v('updateUserBalance pre state?.balance?.value: ${state?.balance.value}');
    ApiHelper.getBalance(Get.context, () async {
      state?.balance.value = YBDUserUtil.getUserMoneySync!;
    });
  }
  void updateUserBalanceb88Xhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取congtroller，自动抛出异常
  static find(void Function(YBDTPRoomGetLogic logic) callback) {
    try {
      callback.call(Get.find<YBDTPRoomGetLogic>());
    } catch (e) {
      logger.e('find YBDTPRoomGetLogic error: $e');
    }
  }

  /// 获取congtroller，自动抛出异常
  static YBDTPRoomGetLogic? findSync() {
    try {
      return Get.find<YBDTPRoomGetLogic>();
    } catch (e) {
      logger.e('findSync YBDTPRoomGetLogic error: $e');
    }
    return null;
  }

  /// 播放音频
  playAudio(String filePath) {
    if (state!.volume > 0.01) {
      if (Platform.isIOS) {
        YBDAudioPlayerManager.instance!
            .playAudio(filePath, volume: state!.volume, mode: PlayerMode.LOW_LATENCY, playerId: 'tp_player_id');
      } else {
        YBDAudioPlayerManager.instance!.dispose();
        YBDAudioPlayerManager.instance!.playAudio(filePath, volume: state!.volume);
      }
    }
  }

  /// 播放音频
  static logicPlayAudio(String filePath) {
    findSync()?.playAudio(filePath);
  }

  static exitRoom() async {
    YBDRtcHelper.getInstance().modeName = '';
    YBDRtcHelper.getInstance().currency = '';
    YBDTPGameSocketApi.instance.exitRoom();
    await Future.delayed(Duration(milliseconds: 100));
    YBDNavigatorHelper.popUntilPre(YBDNavigatorHelper.game_tp_room_page);
  }

  @override
  void onClose() {
    logger.v('YBDTPRoomGetLogic onClose');
    _gameRoomSubscription?.cancel();
    _stateController?.close();
    _stateController = null;
    state = null;
    YBDAudioPlayerManager.instance!.dispose();
    super.onClose();
  }
  void onClosegiywCoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // should show the stand up dialog ?
  void standUpListen(YBDTpRoomEntity e) {
    if (e != null && e.data != null) {
      // gameStartTrack(e);
      // 坐下的错误码
      // List<int> standCodes = [TpConst.code_insufficient, TpConst.code_seat_occupied];
      // is me in stand List ?

      bool? isInUserList = false;
      if (e.data!.standUpUser != null) {
        isInUserList = e.data!.standUpUser?.contains(state!.curUserId.toString());
      }
      // 第一次被动站起时弹出
      if (isInUserList!) {
        logger.v('standUpListen: ${state!.curUserId} stand up up up');
        YBDTPStandTips.show(Get.context);
      }
    }
  }
  void standUpListen1JH6Joyelive(YBDTpRoomEntity e) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 游戏开始埋点
  void gameStartTrack(YBDTpRoomEntity e) {
    if (e.data!.animation!.any((e) => e!.type == 3)) {
      int standCount = e.data?.standUpUser?.length ?? 0;
      int seatCount = e.data?.users?.where((e) => e!.id != null).toList().length ?? 0;
      print('22.9.29--YBDTpRoomEntity:${e.toJson()}');
      print('22.9.29--currency:${e.data!.battle?.currency},bet:${e.data!.battle?.boot},id:${e.data?.roomId}');
      YBDTAProps tp = YBDTAProps(
        name: 'TPGO',
        currency: e.data!.battle?.currency,
        bet: e.data!.battle?.boot,
        id: e.data!.battle?.roomId,
        num: standCount + seatCount,
      );
      YBDTATrack().trackEvent(YBDTATrackEvent.game_start, prop: tp);
    }
  }
  void gameStartTrackeoMM1oyelive(YBDTpRoomEntity e) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
