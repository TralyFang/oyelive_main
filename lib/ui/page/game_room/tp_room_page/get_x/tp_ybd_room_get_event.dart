import 'dart:async';

import 'package:flutter/material.dart';

/// TP房间下注和赢钱的事件
class YBDTPRoomBeansEvent {

  // 下注动画时长
  static Duration duration = const Duration(milliseconds: 1000);

  /// 下注金额
  int? bidValue;
  /// 记录动画未改变之前的值
  int? lastValue;
  /// 需要设置对应的目标key
  GlobalKey? beginKey;
  GlobalKey? endKey;
  /// 记录所有房间座位的坐标，默认5个座位
  static List<GlobalKey>? playerAvatarKeys;
  /// 所有玩家丢底注动画
  static List<Widget>? playerBootAnimations;
  /// 是下底注, 所有座位玩家下注
  bool isBootBid = false;
  /// 记录玩家赢钱动画
  List<GlobalKey>? playerSettleKeys;
  /// 记录玩家赢钱动画
  static List<Widget>? playerSettleAnimations;
  /// 平摊赢钱的用户动画，存在多个玩家，从底池飞向玩家，自己的飞向余额
}

/// beans 缩放动画
class YBDTPRoomBeansScaleEvent {
  // 缩放动画时长
  static Duration duration = const Duration(milliseconds: 500);
  bool isBoot = false; // 是底池动画
  bool isBalance = false; // 是余额动画
  int? bidValue;
  /// 记录动画未改变之前的值
  int? lastValue;
}

/// 跑庄触发
class YBDTPRoomRunBankerEvent {
  bool start = false; // 开始跑庄
}
/*
addSub(eventBus.on<YBDTPRoomBeansScaleEvent>().listen((event) {

}));
 */

/// card 发牌事件
class YBDTPRoomCardEvent {
  bool needCard = false; // 需要发牌了
}