import 'dart:async';

import 'package:get/get.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';

/*
* 站起逻辑：（第一视角的变更）
* 0. 主动站起，或被动站起
* 1. 服务器记录之前站起的 standUpUser:[{userId,position}]
* 2. 如果在玩家列表中找不到自己，则使用standUpUser中我的坐标预留出我的空位，即保留我的相对位置不变。
* 3. 如果我站起的坐标已有人，则使用坐在我位置的人作为第一视角
*
* 坐下逻辑：（纸牌坐标的变更）
* 0. 点击对应位置坐下
* 1. 判断前提：游戏中，我是观众，非站起的状态（因为不是游戏中，不涉及卡牌位置的变更）
* 2. 如果上次的纸牌坐标跟当前纸牌坐标不一样，则需要更新纸牌坐标
*
*
* **/

class YBDTPRoomGetState {
  static String dingzhuangIdentifier = 'dingzhuang';
  YBDTpRoomData roomData = YBDTpRoomData();
  int? curUserId = -1; // 当前登录的用户
  RxInt balance = 0.obs;
  int playerCount = 0;
  bool playedCards = false; // 是否已开始发牌
  bool canCountdown = false; // 是否已经发完牌了
  double volume = 1.0; // 设置音量
  List<YBDTpRoomDataUser?>? bankerFirstUsers; // 只有玩家+庄家
  YBDTpRoomDataUser? currentUser; // 当前用户信息
  int battleId = -1; // 当局Id
  int startTime = 0; // 当局开始时间

  YBDTPRoomGetState() {
    /// 初始化 这里不做数据初始化，只做默认值初始化
    /// 数据初始化在 YBDTPRoomGetLogic 类中初始化
    getUserId();
    currentUser = YBDTpRoomDataUser();
  }

  /// 转化为第一视角
  void updateFirstMeUsers() {
    /// 转化自己为第一视角
    if (roomData.users == null) return;
    List<int?> preUserIds = roomData.users!.map((YBDTpRoomDataUser? e) => e!.id).toList();
    int status = roomData.battle?.status ?? 0;
    // 开始游戏，记录下庄家+玩家的顺序，避免中途退出影响排序
    if (status == 2 && bankerFirstUsers == null) {
      bankerFirstUsers = _transitionBankerFirstUsers();
    }
    currentUser = _getCurrentUser();
    List<YBDTpRoomDataUser?> users = _transitionUsers(roomData.users!, curUserId);
    roomData.users = users;

    if (currentUser!.balance != null) {
      balance.value = currentUser!.balance!;
    }
    List<int?> userIds = roomData.users!.map((YBDTpRoomDataUser? e) => e!.id).toList();
    List<int?>? bankerIds = bankerFirstUsers?.map((YBDTpRoomDataUser? e) => e!.id).toList();

    logger.v(
        'transitionUsers preUserIds:$preUserIds, userIds:$userIds, bankerIds:$bankerIds, status:$status, battleId:$battleId}, meisStandUp:${currentUser!.isStandUp}');
  }

  /// 转化自己为第一视角
  List<YBDTpRoomDataUser?> _transitionUsers(List<YBDTpRoomDataUser?> users, int? first) {
    /// [0, 1, me, 3, 4] ==> [me, 3, 4, 0, 1]
    int meIndex = users.indexWhere((YBDTpRoomDataUser? element) => element!.id == first);

    if (meIndex > 0) {
      // 不在第一位才转换
      List<YBDTpRoomDataUser?> lastUsers = users.sublist(0, meIndex);
      List<YBDTpRoomDataUser?> firstUsers = users.sublist(meIndex);
      users = <YBDTpRoomDataUser?>[
        ...firstUsers,
        ...lastUsers,
      ];
    }else if (meIndex < 0) {
      // 我不在座位上了，是不是在站起的座位上？需要保留我的座位相对位置
      YBDTpRoomDataUser? meUser = null;
      int? mmeIndex = -1;

      YBDTpRoomStandUpUser? meStandUp = roomData.standUpUser?.firstWhereOrNull((YBDTpRoomStandUpUser? element)
      => element!.userId == curUserId) ?? null;
      logger.v('_transitionUsers meStandUp.userId:${meStandUp?.userId}, position:${meStandUp?.beforePosition},users.length:${users.length}');
      if (meStandUp != null &&
          meStandUp.beforePosition != null &&
          meStandUp.beforePosition! >= 0 &&
          meStandUp.beforePosition! < users.length) {
        mmeIndex = meStandUp.beforePosition;
        logger.v('_transitionUsers meStandUp.usersIndex:${users[mmeIndex!]!.id}');
        // 需要确保我之前的位置没有人坐
        if (users[mmeIndex]!.id == null) {
          meUser = YBDTpRoomDataUser()
            ..id = meStandUp.userId
            ..role = 0 // 改成观众角色
            ..isStandUp = true // 站起的状态
            ..position = mmeIndex;
          currentUser!.isStandUp = true;
          // users.insert(mmeIndex, meUser);
          // 数组还存在，直接替换到对应的位置，且之前的位置没有人
          users.replaceRange(mmeIndex, mmeIndex+1, <YBDTpRoomDataUser>[meUser]);
          return _transitionUsers(users, curUserId);
        }else {
          // 我的第一视角被占用了
          return _transitionUsers(users, users[mmeIndex]!.id);
        }
      }else {
        // 直接按照服务器的排序来了
        logger.v('_transitionUsers meStandUp.default set');
      }
    }
    playerCount = 0;
    users.forEach((YBDTpRoomDataUser? value) {
      if (value!.cardState != 2 && value.role != 0 && value.id != null) {
        playerCount += 1;
      }
    });

    return users;
  }

  /// 转化庄家为第一位
  List<YBDTpRoomDataUser?> _transitionBankerFirstUsers() {
    /// [0, 1, me, 3, 4] ==> [me, 3, 4, 0, 1]
    List<YBDTpRoomDataUser?>? users = roomData.users;
    int bankerIndex = users?.indexWhere((YBDTpRoomDataUser? element) => element!.role == 2) ?? -1;
    if (bankerIndex > 0) {
      // 不在第一位才转换
      List<YBDTpRoomDataUser?> lastUsers = users!.sublist(0, bankerIndex);
      List<YBDTpRoomDataUser?> firstUsers = users.sublist(bankerIndex);
      users = <YBDTpRoomDataUser?>[
        ...firstUsers,
        ...lastUsers,
      ];
    }
    // 移除非玩家的空用户
    return users?.where((YBDTpRoomDataUser? user) => !(user!.id == null || user.role == 0)).toList() ?? <YBDTpRoomDataUser>[];
  }

  /// 获取当前登录的用户座位信息
  YBDTpRoomDataUser _getCurrentUser() {
    YBDTpRoomDataUser user = roomData.users?.firstWhere(
          (YBDTpRoomDataUser? element) => element!.id == curUserId,
          orElse: () => YBDTpRoomDataUser(),
        ) ??
        YBDTpRoomDataUser();
    return user;
  }

  /// 是否是当前登录的用户
  bool get isCurrentUser {
    return currentUser!.id != null;
  }

  /// 获取登录的用户id
  void getUserId() async {
    curUserId = YBDUserUtil.getUserIdSync;
  }
  void getUserIdg2mQ5oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 只有两个人应该开牌
  bool isOpenCard() {
    Iterable<YBDTpRoomDataUser?>? list = roomData.users?.where((YBDTpRoomDataUser? element) => element!.cardState != 2);
    if (list != null && list.length == 2) {
      return true;
    }
    return false;
  }
  void isOpenCardVwwbhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取第一个动画对象
  YBDTpRoomDataAnimation? getFirstAnimation() {
    if (roomData.animation != null && roomData.animation!.length > 0) {
      return roomData.animation!.first;
    }
    return null;
  }

  /*
  https://img2.woyaogexing.com/2022/07/22/87369a8374aef7f6!400x400.jpg
  https://img2.woyaogexing.com/2022/07/22/8eb138e6958aa524!400x400.jpg
  https://img2.woyaogexing.com/2022/07/18/36684c58ee58196b!400x400.jpg
  https://img2.woyaogexing.com/2022/07/17/5156a4e99d7385df!400x400.jpg
  https://img2.woyaogexing.com/2022/07/18/6c9745e63d56af6b!400x400.jpg

   */
}
