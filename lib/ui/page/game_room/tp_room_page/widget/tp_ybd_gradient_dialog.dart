import 'dart:async';

/*
 * @Author: William
 * @Date: 2022-10-12 11:04:46
 * @LastEditTime: 2022-12-01 18:04:38
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/game_room/tp_room_page/widget/tp_gradient_dialog.dart
 */
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/widget/create_ybd_model_dialog.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_now_watching.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_menu.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_tips.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';

enum DialogType { tpMenu, tpWatch, kickTip, tpCommon, create }
enum ColorType { green, purple, blue }

class YBDTPGradientDialog extends StatefulWidget {
  final int? height;
  final int width;
  final String? title;
  final String? contentMsg;
  final Widget? child;
  final GameVoidCallback? closeCallback;
  final DialogType? type;
  final ColorType? color;
  final bool showClose;
  final int buttonNum;
  final int? kickPlayerId;
  final Function? onConfirm;
  final bool? showGridBg; // 网格背景图
  final int? textWidth;
  final double? textHeight;

  const YBDTPGradientDialog({
    Key? key,
    this.height,
    this.width = 520,
    this.title,
    this.child,
    this.closeCallback,
    this.type,
    this.contentMsg,
    this.showClose = true,
    this.color = ColorType.green,
    this.buttonNum = 1,
    this.kickPlayerId,
    this.onConfirm,
    this.showGridBg,
    this.textWidth,
    this.textHeight,
  }) : super(key: key);

  @override
  State<YBDTPGradientDialog> createState() => _YBDTPGradientDialogState();
  // 展示弹框
  static void show(
    BuildContext context, {
    int width = 520,
    int? height,
    String? title,
    String? contentMsg,
    Widget? child,
    Function? closeCallback,
    DialogType? type,
    bool? showClose,
    ColorType? color,
    int buttonNum = 1,
    int? kickPlayerId,
    Function? onConfirm,
    bool showGridBg = true,
    int textWidth = 420,
    double textHeight = 120,
  }) {
    YBDEasyPopup.show(
      context,
      YBDTPGradientDialog(
        width: width,
        height: height,
        title: title,
        child: child,
        contentMsg: contentMsg,
        showClose: showClose ?? true,
        type: type,
        color: color,
        buttonNum: buttonNum,
        kickPlayerId: kickPlayerId,
        showGridBg: showGridBg,
        textWidth: textWidth,
        textHeight: textHeight,
        closeCallback: (BuildContext popContext) {
          YBDEasyPopup.pop(popContext);
          closeCallback?.call(popContext);
        },
        onConfirm: onConfirm,
      ),
      position: EasyPopupPosition.center,
      duration: Duration(milliseconds: 50),
      outsideTouchCancelable: false,
    );
  }
}

class _YBDTPGradientDialogState extends State<YBDTPGradientDialog> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_top(), SizedBox(height: 50.dp750), widget.showClose ? _close() : Container()],
    );
  }
  void buildXuZMaoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _top() {
    return Container(
      width: widget.width.dp750,
      height: widget.height!.dp750,
      decoration: BoxDecoration(
        border: Border.all(width: 2.dp750 as double, style: BorderStyle.solid, color: Color(0xffffd94e)),
        borderRadius: BorderRadius.all(Radius.circular(32.dp750 as double)),
        gradient: LinearGradient(colors: getGradient(), begin: Alignment.topCenter, end: Alignment.bottomCenter),
      ),
      child: Column(children: [_title(), Expanded(child: _topContent())]),
    );
  }
  void _topUtNEMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _topContent() {
    int height1 = ((widget.type == DialogType.tpCommon && widget.contentMsg!.length > 100) ? 450 : widget.height)! - 84;
    int height2 = widget.height! - 94;
    return Container(
      height: height1.dp750,
      decoration: BoxDecoration(borderRadius: BorderRadius.vertical(bottom: Radius.circular(32.dp750 as double))),
      child: _content(),
    );
  }
  void _topContent3C8oJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _close() {
    if (widget.type == DialogType.create) return Container();
    return GestureDetector(
      onTap: () {
        logger.v('YBDTPRoomHelp menu close');
        widget.closeCallback?.call(context);
      },
      child: YBDGameWidgetUtil.assetTPImage('tp_green_close', width: 64.dp750, height: 64.dp750),
    );
  }
  void _closez8KABoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _content() {
    int sizeHeight = widget.type == DialogType.tpMenu ? 50 : 40;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (widget.type == DialogType.tpMenu) SizedBox(height: sizeHeight.dp750),
        // Container(height: sizeHeight.dp750, color: Colors.pink.withOpacity(0.7)),
        Expanded(child: getContent()),
        SizedBox(height: sizeHeight.dp750),
        // Container(height: sizeHeight.dp750, color: Colors.pink.withOpacity(0.7)),
      ],
    );
  }
  void _content3UVgSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _title() {
    return Container(
        width: widget.width.dp750,
        height: 80.dp750,
        decoration: BoxDecoration(
          color: getTitleColor(),
          borderRadius: BorderRadius.vertical(top: Radius.circular(32.dp750 as double)),
        ),
        child: Center(
          child: YBDGameWidgetUtil.textBalooRegular(widget.title, fontSize: 32.sp750),
        ));
  }

  Widget getContent() {
    return getChild(widget.type);
  }
  void getContente9KgGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Color getTitleColor() {
    if (widget.color == ColorType.purple || widget.type == DialogType.create) return Color(0xff8E4BFF);
    if (widget.color == ColorType.blue) return Color(0xff2F91FF);
    return Color(0xff0C7E4F);
  }

  List<Color> getGradient() {
    if (widget.color == ColorType.purple || widget.type == DialogType.create)
      return [Color(0xffCBA4FF), Color(0xff6424FE)];
    if (widget.color == ColorType.blue) return [Color(0xff51D9FF), Color(0xff1C58DB)];
    return [Color(0xff00B367), Color(0xff106542)];
  }
  void getGradient5jITEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget getChild(DialogType? type) {
    switch (type) {
      case DialogType.tpMenu:
        return YBDMenuNew(closeCallback: () {
          logger.v('22.10.12----YBDTPGradientDialog  Menu item');
          YBDEasyPopup.pop(context);
        });
        break;

      case DialogType.tpWatch:
        return YBDTPTips(
          buttonNum: widget.buttonNum,
          msg: widget.contentMsg,
          confirmCallback: () {
            logger.v('22.10.18----YBDTPGradientDialog now watching');
            YBDEasyPopup.pop(context);
          },
        );
        break;
      case DialogType.kickTip:
        return YBDTPTips(
          buttonNum: widget.buttonNum,
          msg: widget.contentMsg,
          textWidth: 481.dp750,
          textHeight: 180.dp750,
          confirmCallback: () {
            logger.v('22.10.20----YBDTPGradientDialog kickTip kickPlayerId:${widget.kickPlayerId}');
            YBDEasyPopup.pop(context);
            if (widget.kickPlayerId != null) YBDTPGameSocketApi.instance.kickPlayer(widget.kickPlayerId);
          },
          closeCallback: () {
            logger.v('22.10.20----YBDTPGradientDialog kickTip kickPlayerId:${widget.kickPlayerId}');
            YBDEasyPopup.pop(context);
          },
        );
        break;
      case DialogType.tpCommon:
        return YBDTPTips(
          buttonNum: widget.buttonNum,
          msg: widget.contentMsg,
          textWidth: widget.textWidth!.dp750,
          textHeight: widget.textHeight!.dp750,
          closeCallback: () {
            // YBDEasyPopup.pop(context);
            widget.closeCallback?.call(context);
          },
          confirmCallback: () {
            YBDEasyPopup.pop(context);
            widget.onConfirm?.call();
          },
        );
        break;
      case DialogType.create:
        return YBDCreateModelDialog(
          cancelCallback: () {
            // YBDEasyPopup.pop(context);
            widget.closeCallback?.call(context);
          },
          okCallback: (String model) {
            logger.v('22.11.21---YBDCreateModelDialog tap ok model:$model');
            YBDRtcHelper.getInstance().modeName = model;
            YBDEasyPopup.pop(context);
            widget.onConfirm?.call();
          },
        );
        break;
      default:
        return Container();
    }
  }
  void getChildiaLU8oyelive(DialogType? type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
