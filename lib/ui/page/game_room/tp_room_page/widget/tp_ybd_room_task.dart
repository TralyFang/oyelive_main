import 'dart:async';

/*
 * @Author: William
 * @Date: 2022-09-15 10:17:03
 * @LastEditTime: 2022-09-15 18:23:23
 * @LastEditors: William
 * @Description: tp go tasks
 * @FilePath: /oyetalk-flutter/lib/ui/page/game_room/tp_room_page/widget/tp_room_task.dart
 */
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';

class YBDTPRoomTask extends StatefulWidget {
  GameVoidCallback? gameVoidCallback;
  YBDTPRoomTask({this.gameVoidCallback});

  @override
  State<YBDTPRoomTask> createState() => _YBDTPRoomTaskState();

  static void show(BuildContext context) {
    YBDEasyPopup.show(
      context,
      YBDTPRoomTask(
        gameVoidCallback: (BuildContext popContext) {
          YBDEasyPopup.pop(popContext);
        },
      ),
      position: EasyPopupPosition.center,
      duration: Duration(milliseconds: 50),
      outsideTouchCancelable: true,
    );
  }
}

class _YBDTPRoomTaskState extends State<YBDTPRoomTask> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 600.dp750,
      height: 1000.dp750,
      padding: EdgeInsets.all(5.dp750 as double),
      decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(16.dp750 as double)),
      child: Column(
        children: [_title(), _content()],
      ),
    );
  }
  void buildxGOK2oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _title() {
    return Container(
      height: 70.dp750,
      child: Stack(
        children: [
          // YBDGameWidgetUtil.assetTPImage(widget.titleImgUrl,
          //     width: 530.dp750, height: 70.dp750, isWebp: false, placeholder: CupertinoActivityIndicator()),
          // Center(child: YBDGameWidgetUtil.textBalooRegular(widget.title ?? "Rules", fontSize: 32.sp750)),
          Center(child: YBDGameWidgetUtil.textBalooRegular('Task', fontSize: 32.sp750)),
          Positioned(
              right: 29.dp750,
              top: 25.dp750,
              child: YBDGameWidgetUtil.assetImage('game_close_white',
                  width: 20.dp750, height: 20.dp750, minTapSize: 70.dp750, onTap: () {
                logger.v('YBDTPRoomHelp task close');
                widget.gameVoidCallback?.call(context);
              })!),
        ],
      ),
    );
  }
  void _titleJJ29aoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _content() {
    return Expanded(
      child: Container(
        padding: EdgeInsets.only(top: 20.dp750 as double, left: 15.dp720 as double, right: 15.dp720 as double, bottom: 15.dp720 as double),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16.dp750 as double),
          child: SingleChildScrollView(
              child: Container(
            height: 900.dp750,
            color: Colors.white,
          )),
        ),
      ),
    );
  }
  void _contentKjYNQoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // Widget
}
