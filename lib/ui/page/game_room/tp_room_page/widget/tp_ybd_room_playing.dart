import 'dart:async';

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oye_tool_package/widget/should_rebuild.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/i18n_ybd_key.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_avatars_run_banker.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_boot_beans.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_card_compared_controller.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_card_controller.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_card_compared.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_card_open_controller.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_player_avatar.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

double roomPlayerHeight() {
  final double footer = 124.dp750 as double;
  final double header = 70.dp750 as double;
  final double indent = SizeNum.statusBarHeight + 20.dp750 + (65.dpBottomElse(0) as double);
  final double height = SizeNum.screentHeight - (footer + header + indent);
  return height;
}

class YBDTPRoomPlaying extends StatefulWidget {
  const YBDTPRoomPlaying({Key? key}) : super(key: key);

  @override
  State<YBDTPRoomPlaying> createState() => _YBDTPRoomPlayingState();
}

class _YBDTPRoomPlayingState extends BaseState<YBDTPRoomPlaying> {
  /// 座位的坐标key
  final GlobalKey TPPlayerAvatar0Key = GlobalKey();
  final GlobalKey TPPlayerAvatar1Key = GlobalKey();
  final GlobalKey TPPlayerAvatar2Key = GlobalKey();
  final GlobalKey TPPlayerAvatar3Key = GlobalKey();
  final GlobalKey TPPlayerAvatar4Key = GlobalKey();

  YBDTPRoomGetLogic logic = Get.find<YBDTPRoomGetLogic>();

  @override
  void initState() {
    YBDTPRoomBeansEvent.playerAvatarKeys = [
      TPPlayerAvatar0Key,
      TPPlayerAvatar1Key,
      TPPlayerAvatar2Key,
      TPPlayerAvatar3Key,
      TPPlayerAvatar4Key,
    ];

    super.initState();
  }
  void initStateSzgRuoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 区域高度
  double get playerHeight => roomPlayerHeight();

  @override
  Widget myBuild(BuildContext context) {
    logger.v('myBuild YBDTPRoomPlaying');
    return Stack(
      children: <Widget>[
        /// 中心下注信息
        Align(alignment: Alignment(0.0, -300.dpH1136 / playerHeight / 2.0), child: YBDTPBootLogoTips()),

        /// 发牌
        YBDTPCardController(),

        /// 游戏状态提示
        YBDTPGameTipsHand(),

        /// 用户头像
        YBDTPAvatarsRunBanker(),

        /// 比牌的状态展示
        YBDTPCardComparedController(),

        /// 开牌展示结果信息
        YBDTPCardOpenController(),

        GetBuilder<YBDTPRoomGetLogic>(builder: (YBDTPRoomGetLogic logic) {
          return Stack(
            children: <Widget>[
              /// 操作按钮
              packAndShowWidget(logic),

              /// 除了自己有人看牌了
              someOneSeeCard(),

              /// 自己的开牌按钮
              selfSeeWidget(),
            ],
          );
        }),
      ],
    );
  }
  void myBuildBgu09oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 有人看牌了
  Widget someOneSeeCard() {
    // 游戏中
    bool status = (logic.state!.roomData.battle?.status ?? 0) == 2;
    List<YBDTpRoomDataUser?>? users = logic.state!.roomData.users;
    List<YBDTpRoomDataUser?>? bankerFirstUsers = logic.state!.bankerFirstUsers;
    if (!status || users == null || bankerFirstUsers == null) return Container();
    List<Rect> rects = YBDTPAvatarSize.someOneSeeMyCardRects(roomPlayerHeight());
    List<Widget> someOnes = rects.mapIndexValue((int index, Rect rect) {
      if (users.length > index) {
        YBDTpRoomDataUser? user = users[index];
        // 庄家相对我所在的位置
        int cardIndex = bankerFirstUsers.indexWhere((YBDTpRoomDataUser? element) => element!.id == user!.id);
        if (cardIndex >= 0 &&
            bankerFirstUsers[cardIndex]!.cardState != 1 &&
            user!.cardState == 1 &&
            user.id != logic.state!.curUserId) {
          logger.v('someOneSeeCard user.cardState: ${user.cardState}, banker:${bankerFirstUsers[cardIndex]!.cardState}');
          return FutureBuilder<bool>(
              future: someOneSeeFuture(),
              builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                logger.v('someOneSeeCard delayed: ${snapshot.hasData}, ${snapshot.data}');
                if (snapshot.hasData && snapshot.data!) {
                  bankerFirstUsers[cardIndex]!.cardState = 1;
                  return Container();
                }
                return Positioned(
                    left: rect.left,
                    top: rect.top,
                    child: YBDTPAnimatedScale(
                      child: YBDGameWidgetUtil.assetTPImage(
                        'tp_card_eye',
                        width: rect.width,
                        height: rect.height,
                      ),
                    ));
              });
        }
      }
      return Container();
    });
    return Stack(children: someOnes);
  }
  void someOneSeeCardo8LHAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<bool> someOneSeeFuture() async {
    await Future<void>.delayed(Duration(milliseconds: 500));
    return true;
  }

  /// 弃牌，比牌
  Widget packAndShowWidget(YBDTPRoomGetLogic logic) {
    YBDTpRoomDataUser user = logic.state!.currentUser!;
    if ((user.role ?? 0) == 0) {
      // 是观众就不展示了
      return Container();
    }

    bool pack = user.optPrivilege?.canPackCard ?? false;
    bool compared = user.optPrivilege?.canComparedCard ?? false;
    bool isOpt = logic.state!.roomData.battle?.currentOperator == user.id;
    bool blind = user.cardState == 0;
    // pack = pack && isOpt && logic.state.canCountdown;
    logger.v('22.10.21--pack:$pack--isOpt:$isOpt--blind:$blind');
    pack = pack && logic.state!.canCountdown && !blind;
    compared = compared && isOpt && logic.state!.canCountdown;
    // 最后一轮的最后一个人是开牌
    bool open = user.optPrivilege?.canShowCard ?? false;
    open = open && isOpt && logic.state!.canCountdown;
    if (open) {
      compared = open;
    }
    String showString = 'Side Show';
    if (logic.state!.playerCount == 2) {
      showString = 'Show';
    }

    logger.v('packAndShowWidget:${logic.state!.canCountdown}');

    return Obx(() {
      // 用户余额判断
      int balance = logic.state?.balance.value ?? 0;
      int minBid = logic.state!.roomData.battle?.minChaalValue ?? 0;
      if (minBid > balance) {
        compared = false;
      }

      return Positioned(
        left: 0,
        bottom: 44.dp750,
        child: Container(
          width: SizeNum.screentWidth,
          padding: EdgeInsets.symmetric(horizontal: 40.dp750 as double),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // tp_grey_btn
              YBDGameWidgetUtil.buttonTextTPBg('Pack',
                  bgName: pack ? 'tp_purple_btn' : 'tp_grey_btn',
                  width: 190.dp750,
                  height: 68.dp750,
                  fontSize: 28.sp750,
                  textChild: pack
                      ? null // 灰色按钮文本添加描边
                      : YBDGameWidgetUtil.textBalooStrokRegular(
                          'Pack',
                          fontSize: 28.sp750,
                        ), onTap: () {
                if (pack) {
                  YBDTPGameSocketApi.instance.pack();
                  YBDTPAudio.pack.play();
                }
              }),
              YBDGameWidgetUtil.buttonTextTPBg(showString,
                  bgName: compared ? 'tp_blue_btn' : 'tp_grey_btn',
                  width: 190.dp750,
                  height: 68.dp750,
                  fontSize: 26.sp750,
                  textChild: compared
                      ? null // 灰色按钮文本添加描边
                      : YBDGameWidgetUtil.textBalooStrokRegular(
                          showString,
                          fontSize: 26.sp750,
                        ), onTap: () {
                if (compared) {
                  YBDTPGameSocketApi.instance.compared();
                  YBDTPAudio.compare.play();
                } else if (minBid > balance) {
                  // 余额不足的提示
                  YBDToastUtil.toast(YBDI18nKey.insufficientBalance);
                }
              }),
            ],
          ),
        ),
      );
    });
  }
  void packAndShowWidgetL8APEoyelive(YBDTPRoomGetLogic logic) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 自己的看牌按钮
  Widget selfSeeWidget() {
    YBDTpRoomDataUser user = logic.state!.currentUser!;
    bool canSee = user.optPrivilege?.canSeeCard ?? false;
    // canSee = (user.cards == null && user.id != null && user.role != 0);
    canSee = canSee && ((logic.state!.roomData.battle?.status ?? 0) == 2);
    canSee = canSee && logic.state!.canCountdown;
    if (!canSee) {
      return Container();
    }

    return Positioned(
      left: (SizeNum.screentWidth - 130.dp750) / 2.0,
      bottom: 270.dp750,
      child: YBDGameWidgetUtil.assetTPImage('tp_card_see',
          width: 130.dp750,
          height: 52.dp750,
          minTapSize: 100.dp750,
          delayDuration: const Duration(seconds: 2), onTap: () {
        YBDTPGameSocketApi.instance.see();
        YBDTPAudio.click.play();
      }),
    );
  }
  void selfSeeWidgetB8YUjoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
