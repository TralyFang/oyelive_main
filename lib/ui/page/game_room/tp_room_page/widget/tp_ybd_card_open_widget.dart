import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:oye_tool_package/widget/card_open_widget.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

class YBDTPCardOpenWidget extends StatefulWidget {

  YBDTpRoomDataUser? user; // 座位用户信息
  int? curUserId; // 当前登录的用户
  bool? isWin;

  YBDTPCardOpenWidget({Key? key, this.user, this.curUserId, this.isWin}) : super(key: key);

  @override
  _YBDTPCardOpenWidgetState createState() => _YBDTPCardOpenWidgetState();
}

class _YBDTPCardOpenWidgetState extends State<YBDTPCardOpenWidget> {

  // 别人232*139 -> 82*117
  // 自己247*158 -> 94*134

  @override
  Widget build(BuildContext context) {

    YBDTPCardOpenItem item = YBDTPCardOpenItem.otherSize();
    if (widget.user!.id == widget.curUserId) {
      item = YBDTPCardOpenItem.selfSize();
    }

    return Container(
      decoration: BoxDecoration(image: DecorationImage(image: YBDGameWidgetUtil.assetTPImageProvider("tp_mark_bg", isWebp: true))),
      width: item.bgSize!.width,
      height: item.bgSize!.height,
      child: Stack(
        children: [
          /// 牌型
          Center(child: Container(
            width: item.cardSize!.width*(1+2*1/3),
            height: item.cardSize!.height,
            child: CardOpenWidget(
              cardSize: item.cardSize!,
              children: pokerCards(),
            ),
          ),),
          // 输赢状态
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(top: item.statusRect!.top),
                child: YBDGameWidgetUtil.assetTPImage(
                    widget.isWin! ? "tp_card_win" : "tp_card_lose",
                    width: item.statusRect!.width,
                    height: item.statusRect!.height,),
              ),
            ],
          ),
        ],
      ),
    );
  }
  void buildklpy1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 用户开牌信息
  List<Widget> pokerCards() {
    return widget.user!.cards!.map((e) => YBDGameWidgetUtil.assetTPImage(
        YBDGameResource.tpCardName(card: e))).whereNotNull().toList();
  }
  void pokerCardsH47lGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDTPCardOpenItem {
  Size? bgSize;
  Size? cardSize;
  Rect? statusRect;

  YBDTPCardOpenItem({this.bgSize, this.cardSize, this.statusRect});

  static selfSize() {
    return YBDTPCardOpenItem(
        bgSize: Size(247.dp750 as double, 158.dp750 as double),
        cardSize: Size(94.dp750 as double, 134.dp750 as double),
        statusRect: Offset(0, 90.dp750 as double) & Size(200.dp750 as double, 60.dp750 as double),
    );
  }
  static otherSize() {
    return YBDTPCardOpenItem(
        bgSize: Size(232.dp750 as double, 139.dp750 as double),
        cardSize: Size(82.dp750 as double, 117.dp750 as double),
        statusRect: Offset(0, 79.dp750 as double) & Size(200.dp750 as double, 60.dp750 as double),
    );
  }
}