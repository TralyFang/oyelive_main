
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oye_tool_package/ext/size_ext.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/context_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/tp_ybd_room_page.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_fly_bean_animation.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_playing.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

/// 底池的位置key
final GlobalKey TPBootBeansKey = GlobalKey();

/// 余额的位置key
final GlobalKey TPBalanceBeansKey = GlobalKey();

/// 下注动画处理
class YBDTPFlyBeanPlaying extends StatefulWidget {
  BuildContext bodyContext;
  YBDTPFlyBeanPlaying(this.bodyContext, {Key? key}) : super(key: key);

  @override
  _YBDTPFlyBeanPlayingState createState() => _YBDTPFlyBeanPlayingState();
}

class _YBDTPFlyBeanPlayingState extends BaseState<YBDTPFlyBeanPlaying> {
  BuildContext? bodyContext;
  // YBDTPFlyBeanController flyBeanController = YBDTPFlyBeanController(); // 下注
  List<YBDTPFlyBeanAnimation> _playingItems = []; // 播放的动画
  int _itemsCount = 0;

  @override
  void initState() {
    bodyContext = widget.bodyContext;
    super.initState();

    /// 飞豆子触发监听
    bidBeansListens();
  }
  void initStateyyJLboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Stack(
      children: [
        // 下注动画
        // bidFlyBeanAnimation(controller: flyBeanController),
        ..._playingItems,
        // 底池动画
        if (YBDTPRoomBeansEvent.playerBootAnimations != null) ...YBDTPRoomBeansEvent.playerBootAnimations!,
        // 他人结算动画
        if (YBDTPRoomBeansEvent.playerSettleAnimations != null) ...YBDTPRoomBeansEvent.playerSettleAnimations!,
      ],
    );
  }
  void myBuildryfzQoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 单个下注，赢钱动画
  YBDTPFlyBeanAnimation bidFlyBeanAnimation(
      {Offset? start = Offset.zero,
      Offset? end = Offset.zero,
      Function(YBDTPFlyBeanAnimation item)? onEnd,
      double offsetY = 50.0,
      YBDTPFlyBeanController? controller,
      int? bidValue = 0,
      bool startPlaying = false}) {
    return YBDTPFlyBeanAnimation(
      controller: controller,
      start: start,
      end: end,
      duration: YBDTPRoomBeansEvent.duration,
      childBean: YBDTPContainer(
        width: 40.dp750,
        height: 40.dp750,
        child: YBDGameWidgetUtil.assetImage(YBDGameResource.currencyIconType(GameCurrencyType.beans)),
      ),
      childText: childText(bidValue),
      finishCallback: onEnd,
      startPlaying: startPlaying,
      offsetY: offsetY,
    );
  }

  YBDTPContainer childText(int? bidValue) {
    return YBDTPContainer(
      child: YBDGameWidgetUtil.textBalooRegular('+$bidValue', fontSize: 31.dp750),
    );
  }

  /// 飞豆子触发监听
  void bidBeansListens() {
    /// 监听触发下注，赢钱动画
    addSub(eventBus.on<YBDTPRoomBeansEvent>().listen((event) {
      double halfHeight = roomPlayerHeight() / 2.0;
      double offsetY = -80.dp750 as double;
      // 计算豆子+数字的宽度，居中于头像展示
      double beanWidth = SizeCalculate.boundingTextSize(
                  '+${event.bidValue}', TextStyle(fontSize: 31.dp750, fontWeight: FontWeight.w400, fontFamily: 'baloo'))
              .width +
          40.dp750;

      /// 开始下注底注
      if (event.isBootBid) {
        var animations = YBDTPRoomBeansEvent.playerAvatarKeys!.map((e) {
          Offset? start = e.currentContext?.transformOffsetFrom(bodyContext);
          logger.v('YBDTPRoomBeansEvent listen boot start: $start, half: $halfHeight');

          // 过滤不是玩家的座位
          if (start == null) {
            return null;
          }

          Size aSize = e.currentContext!.findRenderObject()!.paintBounds.size;
          double startX = (aSize.width - beanWidth) / 2.0;
          start = Offset(start.dx + startX, start.dy);

          // 开局底注
          int boot = YBDTPRoomGetLogic.findSync()!.state!.roomData.battle?.boot ?? 0;
          offsetY = -80.dp750 as double;
          if (start.dy < halfHeight) {
            // 在上面，那就从下面出
            offsetY = -offsetY;
            double spaceY = aSize.height * 1 / 2;
            start = Offset(start.dx, start.dy + spaceY);
          }
          return bidFlyBeanAnimation(
            onEnd: (_) {
              logger.v('YBDTPRoomBeansEvent listen boot end');
              YBDTPRoomBeansEvent.playerBootAnimations = null;
              if (mounted) setState(() {});
              // 飞向地池的动画
              eventBus.fire(YBDTPRoomBeansScaleEvent()
                ..isBoot = true
                ..lastValue = event.lastValue
                ..bidValue = event.bidValue);
            },
            startPlaying: true,
            start: start,
            end: TPBootBeansKey.currentContext.transformOffsetFrom(bodyContext),
            offsetY: offsetY,
            bidValue: boot, // 底注
          );
        }).toList();

        animations.removeWhere((element) => element == null);
        if (animations.length < 2) return;
        YBDTPRoomBeansEvent.playerBootAnimations = animations as List<Widget>?;
        logger.v('YBDTPRoomBeansEvent listen boot start:${animations.length}');
        if (mounted) setState(() {});
        return;
      } else if (event.playerSettleKeys != null && event.beginKey != null) {
        /// 玩家结算动画
        var animations = event.playerSettleKeys!.map((e) {
          Offset? end = e.currentContext?.transformOffsetFrom(bodyContext);
          logger.v('YBDTPRoomBeansEvent listen settle start: $end, half: $halfHeight');

          // 过滤不是玩家的座位
          if (end == null) {
            return null;
          }
          Size aSize = e.currentContext!.findRenderObject()!.paintBounds.size;
          double endX = aSize.width / 2.0;
          end = Offset(end.dx + endX, end.dy);

          return bidFlyBeanAnimation(
            onEnd: (_) {
              logger.v('YBDTPRoomBeansEvent listen settle end');
              YBDTPRoomBeansEvent.playerSettleAnimations = null;
              if (mounted) setState(() {});
              if (event.playerSettleKeys!.contains(TPBalanceBeansKey)) {
                // 飞向余额
                eventBus.fire(YBDTPRoomBeansScaleEvent()
                  ..isBalance = true
                  ..lastValue = event.lastValue
                  ..bidValue = event.bidValue);
              }
            },
            startPlaying: true,
            start: event.beginKey!.currentContext.transformOffsetFrom(bodyContext),
            end: end,
            offsetY: offsetY,
            bidValue: event.bidValue, // 底注
          );
        }).toList();

        animations.removeWhere((element) => element == null);
        if (animations.length < 0) return;
        YBDTPRoomBeansEvent.playerSettleAnimations = animations as List<Widget>?;
        if (mounted) setState(() {});
        logger.v('YBDTPRoomBeansEvent listen settle start: ${animations.length}');
        int? battleId = YBDTPRoomGetLogic.findSync()?.state?.battleId;
        Future.delayed(YBDTPRoomBeansEvent.duration, (){
          // 有延迟了，需要对比最新的battleId是否同一局
          int? getBattleId = YBDTPRoomGetLogic.findSync()?.state?.battleId;
          logger.v('YBDTPRoomBeansEvent listen settle end start: ${animations.length}, battleId:${battleId}, getBattleId:${getBattleId}');
          if (battleId == getBattleId || getBattleId == -1) {
            YBDTPRoomBeansEvent.playerSettleAnimations = null;
            if (mounted) setState(() {});
          }
        });
        return;
      }

      if (event.beginKey == null || event.endKey == null) {
        return;
      }

      /// 这里需要使用bodyContext转换才正确
      Offset start = event.beginKey!.currentContext.transformOffsetFrom(bodyContext);
      logger.v('YBDTPRoomBeansEvent listen start: $start, halfHeight: $halfHeight');

      Size aSize = event.beginKey!.currentContext!.findRenderObject()!.paintBounds.size;
      double startX = (aSize.width - beanWidth) / 2.0;
      start = Offset(start.dx + startX, start.dy);

      if (start.dy < halfHeight) {
        // 在上面，那就从下面出
        offsetY = -offsetY;
        double spaceY = aSize.height * 1 / 2;
        start = Offset(start.dx, start.dy + spaceY);
      }
      // 自己的需要高一些，避免卡牌挡住了
      if (event.beginKey == YBDTPRoomBeansEvent.playerAvatarKeys!.first) {
        offsetY = -150.dp750 as double;
      }
      Offset end = event.endKey!.currentContext.transformOffsetFrom(bodyContext);

      var animationItem = bidFlyBeanAnimation(
        onEnd: (item) {
          removeItem(item);
          logger.v('YBDTPRoomBeansEvent listen finishCallback');
          if (event.endKey == TPBootBeansKey) {
            // 飞向地池的动画
            eventBus.fire(YBDTPRoomBeansScaleEvent()
              ..isBoot = true
              ..lastValue = event.lastValue
              ..bidValue = event.bidValue);
          } else if (event.endKey == TPBalanceBeansKey) {
            // 飞向余额
            eventBus.fire(YBDTPRoomBeansScaleEvent()
              ..isBalance = true
              ..lastValue = event.lastValue
              ..bidValue = event.bidValue);
          }
        },
        startPlaying: true,
        start: start,
        end: end,
        offsetY: offsetY,
        bidValue: event.bidValue,
      );
      appendItem(animationItem);
    }));
  }
  void bidBeansListensS7oHSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 需要的动画添加到数组中
  appendItem(YBDTPFlyBeanAnimation item) {
    _playingItems.add(item);
    logger.v('YBDTPFlyBeanPlaying play animation add item:${item.hashCode}, '
        '_items:${_playingItems.length}');
    setState(() {});
  }

  /// 已经完成的动画从数组中移除
  removeItem(YBDTPFlyBeanAnimation item) {
    _itemsCount += 1; // 需要所有动画都结束再移除，避免动画失效
    if (_playingItems.length == _itemsCount) {
      _playingItems.clear();
      _itemsCount = 0;
      setState(() {});
    }
    logger.v('YBDTPFlyBeanPlaying play animation remove item:${item.hashCode}, '
        '_items:${_playingItems.length}, '
        'count:$_itemsCount');
  }

  @override
  void dispose() {
    _playingItems.clear();
    _itemsCount = 0;
    logger.v('YBDTPFlyBeanPlaying dispose: $_playingItems');
    // flyBeanController = null;
    YBDTPRoomBeansEvent.playerBootAnimations = null;
    YBDTPRoomBeansEvent.playerSettleAnimations = null;
    super.dispose();
  }
  void dispose9V4jJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
