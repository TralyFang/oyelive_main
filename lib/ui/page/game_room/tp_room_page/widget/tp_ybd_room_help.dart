
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';

class YBDTPRoomHelp extends StatefulWidget {
  static show(BuildContext context, String titleImgUrl, String bodyImgUrl, {String? title}) {
    YBDEasyPopup.show(
        context,
        YBDTPRoomHelp(
          titleImgUrl: titleImgUrl,
          bodyImgUrl: bodyImgUrl,
          title: title,
          gameVoidCallback: (popContext) {
            YBDEasyPopup.pop(popContext);
          },
        ),
        position: EasyPopupPosition.center,
        duration: Duration(milliseconds: 50),
        outsideTouchCancelable: true);
  }

  GameVoidCallback? gameVoidCallback;
  String? titleImgUrl;
  String? title;
  String? bodyImgUrl;

  YBDTPRoomHelp({this.gameVoidCallback, this.titleImgUrl, this.title, this.bodyImgUrl});

  @override
  _YBDTPRoomHelpState createState() => _YBDTPRoomHelpState();
}

class _YBDTPRoomHelpState extends State<YBDTPRoomHelp> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
            border: Border.all(width: 2.dp750 as double, style: BorderStyle.solid, color: Colors.yellow),
            borderRadius: BorderRadius.all(Radius.circular(32.dp750 as double)),
            gradient: LinearGradient(
              colors: [Color(0xff00B367), Color(0xff106542)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          width: 650.dp750,
          height: 800.dp750,
          // padding: EdgeInsets.all(5.dp720),
          child: Column(
            children: [
              _setupTitle(),
              Expanded(
                  child: Container(
                padding: EdgeInsets.only(top: 20.dp750 as double, left: 15.dp720 as double, right: 15.dp720 as double, bottom: 15.dp720 as double),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16.dp750 as double),
                  child: SingleChildScrollView(
                    child: YBDGameWidgetUtil.assetTPImage(widget.bodyImgUrl,
                        isWebp: false, width: 600.dp750, height: 1730.dp750, placeholder: CupertinoActivityIndicator()),
                  ),
                ),
              )),
            ],
          ),
        ),
        SizedBox(height: 50.dp750),
        _close()
      ],
    );
  }
  void buildsncJuoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _setupTitle() {
    return Container(
        height: 96.dp750,
        decoration: BoxDecoration(
          color: Color(0xff0C7E4F),
          borderRadius: BorderRadius.vertical(top: Radius.circular(32.dp750 as double)),
        ),
        child: Center(
          child: YBDGameWidgetUtil.textBalooRegular(widget.title ?? 'Rules', fontSize: 36.sp750),
        ));
  }
  void _setupTitleWYNknoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _close() {
    return GestureDetector(
      onTap: () {
        logger.v("YBDTPRoomHelp tap close");
        widget.gameVoidCallback?.call(context);
      },
      child: YBDGameWidgetUtil.assetTPImage('tp_green_close', width: 64.dp750, height: 64.dp750),
    );
  }
  void _closeRPxcJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
