import 'dart:async';

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:oye_tool_package/widget/gradient_bound.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/tp_ybd_room_page.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_fly_bean_playing.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_player_avatar.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/widget/inner_ybd_shadow.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

// 对局id展示
class YBDTPBattleIdTips extends StatelessWidget {
  const YBDTPBattleIdTips({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Positioned(
        left: 45.dp750,
        top: 100.dp750,
        child: GetBuilder<YBDTPRoomGetLogic>(builder: (YBDTPRoomGetLogic logic) {
          String battleId = 'ID:${logic.state!.battleId}';
          if (battleId == 'ID:-1') battleId = '';
          return YBDGameWidgetUtil.textSingleRegular(
              battleId, 
              color: Colors.white.withOpacity(0.2),
              fontSize: 20.sp750, 
              fontWeight: FontWeight.w600);
        }));
  }
  void buildp2OPIoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}


class YBDTPBootLogoTips extends StatefulWidget {

  GlobalKey? beansKey;


  YBDTPBootLogoTips({Key? key, this.beansKey}) : super(key: key);

  @override
  State<YBDTPBootLogoTips> createState() => _YBDTPBootLogoTipsState();
}

class _YBDTPBootLogoTipsState extends State<YBDTPBootLogoTips> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<YBDTPRoomGetLogic>(builder: (YBDTPRoomGetLogic logic) {
      int boot = logic.state!.roomData.battle?.boot ?? 0;
      int term = logic.state!.roomData.battle?.term ?? 0;
      int maxTerm = logic.state!.roomData.battle?.maxTerm ?? 9;
      int chaal = logic.state!.roomData.battle?.chaalValue ?? 0;

      return Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            YBDGameWidgetUtil.assetTPImage('logo_tp_go',
                width: 100.dp750, height: 100.dp750),
            SizedBox(height: 10.dp750),
            YBDTPBootBeans(
              width: 218.dp750,
              beansKey: TPBootBeansKey,
              value: chaal,
              center: true,),
            SizedBox(height: 12.dp750),
            Text.rich(
              TextSpan(
                  style: TextStyle(
                    fontSize: 20.sp750,
                    color: YBDHexColor('#0A4D34'),
                    fontWeight: FontWeight.w400,),
                  children: <TextSpan>[
                    TextSpan(text: 'Antes：'),
                    TextSpan(text: '${boot}Beans',
                        style: TextStyle(fontWeight: FontWeight.w500)),
                    TextSpan(text: '\nnumber of rounds：'),
                    TextSpan(text: '$term/$maxTerm',
                        style: TextStyle(fontWeight: FontWeight.w500)),
                  ]),
              textAlign: TextAlign.center,
              maxLines: 2,
            ),
            // YBDGameWidgetUtil.textSingleRegular(
            //     "${battleId}Antes：${boot}Beans\nnumber of rounds：$term/$maxTerm",
            //     textAlign: TextAlign.center,
            //     fontSize: 20.dp750,
            //     color: YBDHexColor("#0A4D34"),
            //     lines: 2),
          ],
        ),
      );
    });
  }
  void buildqQmn9oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 中心底注, 有设置了默认值defaultSet;
class YBDTPBootBeans extends StatefulWidget {
  double? width;
  double? height;
  double? minTextWidth;
  Color? bgColor;
  int value;
  GlobalKey? beansKey;
  bool center; // 居中显示

  YBDTPBootBeans(
      {required this.value, this.width, this.height, this.minTextWidth, this.bgColor, this.beansKey, this.center = false});

  @override
  State<YBDTPBootBeans> createState() => _YBDTPBootBeansState();
}

class _YBDTPBootBeansState extends BaseState<YBDTPBootBeans> {

  YBDTPAnimatedScaleController _scaleController = YBDTPAnimatedScaleController();

  defaultSet() {
    // widget.width ??= 218.dp750; // 可以不用，那就自适应
    widget.height ??= 54.dp750;
    widget.minTextWidth ??= 127.dp750;
    widget.bgColor ??= Colors.black.withOpacity(0.2);
  }

  @override
  void initState() {
    super.initState();
    defaultSet();

    addSub(eventBus.on<YBDTPRoomBeansScaleEvent>().listen((
        YBDTPRoomBeansScaleEvent event) {
      if ((widget.beansKey == TPBootBeansKey && (event.isBoot))
          || (widget.beansKey == TPBalanceBeansKey &&
              (event.isBalance))) {
        // 这里需要限制：如果动画还没执行完毕就已经被更新了值，那就不执行该操作了
        YBDTPRoomGetLogic? logic = YBDTPRoomGetLogic.findSync();
        int bootValue = logic?.state?.roomData.battle?.chaalValue ?? 0;
        logger.v('YBDTPRoomBeansScaleEvent event.lastValue: ${event
            .lastValue}, widget.value: ${widget.value}, isBoot:${event
            .isBoot},event.bidValue:${event.bidValue},bootValue:$bootValue');
        if (widget.beansKey == TPBootBeansKey && (event.isBoot)) {
          if (bootValue == event.lastValue) {
            widget.value += event.bidValue!;
            if (widget.beansKey == TPBootBeansKey) {
              YBDTPRoomGetLogic
                  .findSync()
                  ?.state
                  ?.roomData
                  .battle
                  ?.chaalValue = widget.value;
            }
            setState(() {});
            _scaleController.playAnimation();
            return;
          }
        }
        if (widget.value == event.lastValue) {
          widget.value += event.bidValue!;
          setState(() {});
          _scaleController.playAnimation();
        } else if (widget.beansKey == TPBalanceBeansKey &&
            (event.isBalance)) {
          setState(() {});
          _scaleController.playAnimation();
          logger.v('YBDTPRoomBeansScaleEvent playAnimation');
        }
      }
    }));
  }
  void initStatepMkR9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return YBDGameWidgetUtil.gradientGold(
      radius: widget.height! / 2.0,
      child: Container(
        width: widget.width,
        height: widget.height,
        padding: EdgeInsets.only(right: 20.dp750 as double),
        decoration: BoxDecoration(
            color: widget.bgColor,
            borderRadius: BorderRadius.circular(widget.height! / 2.0)),
        child: Row(
          children: [
            Container(
              key: widget.beansKey,
              child: YBDGameWidgetUtil.assetTPImage('icon_beans',
                  width: widget.height, height: widget.height),
            ),
            widget.center ? Container() : SizedBox(width: 16.dp750),
            // 居中展示处理
            if (widget.center)
              Container(
                width: widget.width! - widget.height! - 20.dp750,
                height: widget.height,
                padding: EdgeInsets.only(top: 2.dp750 as double),
                child: YBDTPAnimatedScale(
                  controller: _scaleController,
                  finishCallback: () {
                    if (widget.beansKey == TPBalanceBeansKey) {
                      YBDTPRoomGetLogic.findSync()?.updateUserBalance();
                    }
                  },
                  child: Center(child: YBDGameWidgetUtil.textBalooRegular(
                      StringExt.numThousands(widget.value),
                      fontSize: 32.sp750),
                  ),
                ),
              ),
            // 自适应宽度处理
            if (!widget.center)
              Container(
                padding: EdgeInsets.only(top: 3.dp750 as double),
                child: ConstrainedBox(
                    constraints: BoxConstraints(minWidth: widget.minTextWidth!),
                    child: YBDTPAnimatedScale(
                      controller: _scaleController,
                      finishCallback: () {
                        if (widget.beansKey == TPBalanceBeansKey) {
                          YBDTPRoomGetLogic.findSync()?.updateUserBalance();
                        }
                      },
                      child: YBDGameWidgetUtil.textBalooRegular(
                          StringExt.numThousands(widget.value),
                          fontSize: 32.sp750),
                    )),
              ),
          ],
        ),
      ),
    );
  }
  void myBuildD0Pp4oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(covariant YBDTPBootBeans oldWidget) {
    defaultSet();
    super.didUpdateWidget(oldWidget);
  }
}

class YBDTPGameTipsHand extends StatefulWidget {
  const YBDTPGameTipsHand({Key? key}) : super(key: key);

  @override
  _YBDTPGameTipsHandState createState() => _YBDTPGameTipsHandState();
}

class _YBDTPGameTipsHandState extends BaseState<YBDTPGameTipsHand> {

  YBDTPRoomGetLogic logic = Get.find<YBDTPRoomGetLogic>();

  bool nextTips = false;
  bool hasCoundown = false;

  @override
  void initState() {
    super.initState();
    logicListen();
  }
  void initStateGh0Looyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  logicListen() {
    addSub(logic.stateStream.listen((YBDTPRoomGetState? logicState) {
      List<YBDTpRoomDataAnimation?>? animations = logic.state!.roomData.animation;
      if (animations == null) {
        return;
      }
      // 倒计时
      YBDTpRoomDataAnimation? countdownAnimation = animations.firstWhereOrNull((
          YBDTpRoomDataAnimation? element) => element!.type == 3);
      if (countdownAnimation != null && mounted && !hasCoundown) {
        setState(() {});
      }

      // 定庄发牌的提示
      YBDTpRoomDataAnimation? tipsAnimation = animations.firstWhereOrNull((
          YBDTpRoomDataAnimation? element) => element!.type == 4);
      if (tipsAnimation != null && mounted) {
        setState(() {});
      }
    }));
  }

  @override
  Widget myBuild(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 200.dp750 as double),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          gameStatusHandler(),
        ],
      ),
    );
  }
  void myBuildFr223oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 游戏状态处理
  Widget gameStatusHandler() {
    List<YBDTpRoomDataAnimation?>? animations = logic.state!.roomData.animation;
    if (animations == null) {
      nextTips = false;
      return Container();
    }
    // 倒计时
    YBDTpRoomDataAnimation? countdownAnimation = animations.firstWhereOrNull((
        YBDTpRoomDataAnimation? element) => element!.type == 3);
    if (countdownAnimation != null && !nextTips) {
      hasCoundown = true;
      return YBDTPStartGameTips(
        tips: 'Game Starting in ',
        remainCount: countdownAnimation.timeout,
        finishCallback: () {
          hasCoundown = false;
          logger.v('countdownAnimation finish');
          setState(() {
            nextTips = true;
          });
        },
      );
    }
    // 定庄发牌的提示
    YBDTpRoomDataAnimation? tipsAnimation = animations.firstWhereOrNull((
        YBDTpRoomDataAnimation? element) => element!.type == 4);
    logger.v('tipsAnimation:$tipsAnimation,nextTips:$nextTips');
    if (tipsAnimation != null && countdownAnimation == null) {
      nextTips = true; // 没有倒计时
    }
    if (tipsAnimation != null && nextTips) {
      // 通知该发牌了
      return tipsStream();
    }
    return Container();
  }
  void gameStatusHandlerOVswtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 按顺序展示eye-compared
  Widget tipsStream() {
    return StreamBuilder<Widget>(
        stream: nextTipsWidget(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return snapshot.data;
          }
          return Container();
        });
  }

  Stream<Widget> nextTipsWidget() async* {
    // 通知该发牌了
    yield YBDTPStartGameTips(tips: 'Starting Game');
    await Future.delayed(Duration(milliseconds: 1000));
    yield YBDTPStartGameTips(tips: 'Collecting Boot');
    await Future.delayed(Duration(milliseconds: 1000));
    yield Container();
  }
}


/// 开始游戏提示
class YBDTPStartGameTips extends StatefulWidget {
  String? tips;
  int? remainCount; // 剩余时间 单位ms
  int? delayHidden; // 延迟隐藏 单位ms
  VoidCallback? finishCallback;

  YBDTPStartGameTips(
      {Key? key, this.tips, this.remainCount, this.finishCallback, this.delayHidden})
      : super(key: key);

  @override
  State<YBDTPStartGameTips> createState() => _YBDTPStartGameTipsState();
}

class _YBDTPStartGameTipsState extends State<YBDTPStartGameTips> {

  Timer? _timer;
  int? _remainCount;
  String? _currentTips;
  bool _emptyTips = false;


  @override
  void initState() {
    super.initState();
    startTimer();
  }
  void initStateq723boyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  updateData() {
    _remainCount = null;
    _emptyTips = false;
    _currentTips = null;
    cancelTimer();
    startTimer();

    logger.v('YBDTPStartGameTips widget.delayHidden: ${widget.delayHidden}');

    if (widget.delayHidden != null) {
      _currentTips = widget.tips;
      Future.delayed(Duration(milliseconds: widget.delayHidden!), () {
        if (mounted) {
          setState(() {
            _emptyTips = true;
          });
        }
        widget.finishCallback?.call();
      });
    }
  }

  // 开始倒计时
  startTimer() {
    if (_timer == null && widget.remainCount != null) {
      _remainCount = widget.remainCount! ~/ 1000;
      _timer = Timer.periodic(Duration(milliseconds: 1000), (Timer timer) {
        int count = _remainCount ?? 0;
        count -= 1;
        _remainCount = count;
        if (mounted) setState(() {});
        if (_remainCount! <= 0) {
          cancelTimer();
          widget.finishCallback?.call();
        }
      });
    }
  }

  // 结束倒计时
  cancelTimer() {
    _timer?.cancel();
    _timer = null;
  }

  @override
  void dispose() {
    cancelTimer();
    super.dispose();
  }
  void disposeqxBJNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String? tips() {
    String tips = widget.tips ?? '';
    if (_remainCount != null)
      return tips + '${_remainCount}s';
    if (_currentTips != null)
      return _currentTips;
    return tips;
  }


  @override
  Widget build(BuildContext context) {
    if (_emptyTips) return Container();

    return Container(
      width: 372.dp750,
      height: 58.dp750,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: YBDGameWidgetUtil.assetTPImageProvider(
                  'starting_game', isWebp: true))),
      child: Center(
          child: YBDGameWidgetUtil.textBalooRegular(tips(),
              fontSize: 28.dp750)),
    );
  }

  @override
  void didUpdateWidget(covariant YBDTPStartGameTips oldWidget) {
    updateData();
    super.didUpdateWidget(oldWidget);
  }
}
