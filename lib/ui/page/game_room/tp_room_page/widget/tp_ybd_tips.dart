
/*
 * @Author: William
 * @Date: 2022-10-18 15:15:41
 * @LastEditTime: 2022-11-28 14:45:46
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/game_room/tp_room_page/widget/tp_tips.dart
 */
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/text_ybd_with_stroke.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:flutter/material.dart' hide BoxDecoration, BoxShadow;
import 'package:oyelive_main/ui/widget/inner_shadow/flutter_ybd_inset_box_shadow.dart';

class YBDTPTips extends StatelessWidget {
  final GestureTapCallback? closeCallback;
  final GestureTapCallback? confirmCallback;
  final String? msg;
  final int buttonNum;
  final String? cancelText;
  final String? confirmText;
  final double? textWidth;
  final double? textHeight;

  const YBDTPTips(
      {Key? key,
      this.closeCallback,
      this.confirmCallback,
      this.msg,
      this.buttonNum = 1,
      this.confirmText,
      this.cancelText,
      this.textWidth,
      this.textHeight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Center(
            child: Container(
              width: textWidth ?? 440.dp750,
              // height: textHeight ?? 120.dp750,
              // color: Colors.black.withOpacity(0.3),
              child: Text(
                msg!,
                textAlign: TextAlign.center,
                // maxLines: 3,
                style: TextStyle(color: Colors.white, fontSize: 28.sp750),
              ),
            ),
          ),
        ),
        // SizedBox(height: 40.dp750),
        _btnRow(),
      ],
    );
  }
  void buildvf4vjoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _btnRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_newCancel(), SizedBox(width: buttonNum == 1 ? 0 : 30.dp750), _newConfirm()],
    );
  }
  void _btnRowxaPj3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _btnCancel() {
    if (buttonNum == 1) return Container();
    return GestureDetector(
      onTap: closeCallback as void Function()?,
      child: Container(
          height: 70.dp750,
          width: 200.dp750,
          decoration: BoxDecoration(
              // gradient: LinearGradient(colors: [
              //   Color(0xffFFCE5B),
              //   Color(0xffFFBC04),
              // ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
              color: Color(0xffe7e7e7).withOpacity(0.7),
              borderRadius: BorderRadius.all(Radius.circular(32.px as double)),
              boxShadow: [
                BoxShadow(color: Colors.white.withOpacity(0.5), offset: Offset(0, 1.px as double), blurRadius: 2.px as double, inset: true),
                BoxShadow(color: Color(0xff818181), offset: Offset(0, -1.px as double), blurRadius: 2.px as double, inset: true),
              ]),
          child: Center(
            child: YBDTextWithStroke(
              text: cancelText ?? 'Cancel',
              fontSize: 32.sp750 as double,
              fontFamily: 'baloo',
              strokeColor: Color(0xff4b4b4b),
            ),
          )),
    );
  }
  void _btnCancelyxNYvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _btnConfirm() {
    return GestureDetector(
      onTap: confirmCallback as void Function()?,
      child: Container(
        height: 70.dp750,
        width: buttonNum == 1 ? 300.dp750 : 200.dp750,
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              Color(0xffFFCE5B),
              Color(0xffFFBC04),
            ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
            borderRadius: BorderRadius.all(Radius.circular(32.px as double)),
            boxShadow: [
              BoxShadow(color: Colors.white.withOpacity(0.5), offset: Offset(0, 1.px as double), blurRadius: 2.px as double, inset: true),
              BoxShadow(color: Color(0xff7E5A03), offset: Offset(0, -1.px as double), blurRadius: 2.px as double, inset: true),
            ]),
        child: Center(
          child: YBDTextWithStroke(
            text: confirmText ?? 'OK',
            fontSize: 32.sp750,
            fontFamily: 'baloo',
            strokeColor: Color(0xff786000),
          ),
        ),
      ),
    );
  }
  void _btnConfirmNWWH7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _newCancel() {
    if (buttonNum == 1) return Container();
    return GestureDetector(
        onTap: closeCallback,
        child: YBDTpGradientButton(
          confirm: false,
          width: 200.dp750,
          text: cancelText ?? 'Cancel',
        ));
  }

  Widget _newConfirm() {
    return GestureDetector(
        onTap: confirmCallback, child: YBDTpGradientButton(width: buttonNum == 1 ? 300.dp750 : 200.dp750, confirm: true));
  }
  void _newConfirmZSzUxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDTpGradientButton extends StatelessWidget {
  final double? width;
  final double? height;
  final String? text;
  final bool confirm;
  const YBDTpGradientButton({Key? key, this.width, this.height, this.text, required this.confirm}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? 70.dp750,
      width: width ?? 300.dp750,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: confirm
                  ? [Color(0xffFFCE5B), Color(0xffFFBC04)]
                  : [Color(0xffe7e7e7).withOpacity(0.7), Color(0xffe7e7e7).withOpacity(0.7)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter),
          borderRadius: BorderRadius.all(Radius.circular(32.px)),
          boxShadow: [
            BoxShadow(color: Colors.white.withOpacity(0.5), offset: Offset(0, 1.px), blurRadius: 2.px, inset: true),
            BoxShadow(
                color: confirm ? Color(0xff7E5A03) : Color(0xff818181),
                offset: Offset(0, -1.px),
                blurRadius: 2.px,
                inset: true),
          ]),
      child: Center(
        child: YBDTextWithStroke(
          text: text ?? 'OK',
          fontSize: 32.sp750,
          fontFamily: 'baloo',
          strokeColor: confirm ? Color(0xff786000) : Color(0xff4b4b4b),
        ),
      ),
    );
  }
  void buildV8aZ3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
