
/*
 * @Author: William
 * @Date: 2022-09-15 11:38:41
 * @LastEditTime: 2022-11-11 14:01:28
 * @LastEditors: William
 * @Description: 
 * @FilePath: /oyetalk-flutter/lib/ui/page/game_room/tp_room_page/widget/tp_room_menu.dart
 */
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_help.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_sound.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

class YBDTPRoomMenu extends StatefulWidget {
  GameVoidCallback? gameVoidCallback;
  YBDTPRoomMenu({this.gameVoidCallback});

  @override
  State<YBDTPRoomMenu> createState() => _YBDTPRoomMenuState();
  static void show(BuildContext context) {
    YBDEasyPopup.show(
      context,
      YBDTPRoomMenu(
        gameVoidCallback: (BuildContext popContext) {
          YBDEasyPopup.pop(popContext);
        },
      ),
      position: EasyPopupPosition.center,
      duration: Duration(milliseconds: 50),
      outsideTouchCancelable: false,
    );
  }
}

class _YBDTPRoomMenuState extends State<YBDTPRoomMenu> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_top(), SizedBox(height: 40.dp750), _close()],
    );
  }
  void buildc4Oadoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _top() {
    return Stack(
      alignment: Alignment.center,
      children: [
        YBDGameWidgetUtil.assetTPImage('menu_bg_0', width: 460.dp750, height: 544.dp750),
        _content(),
      ],
    );
  }
  void _top43vKAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _close() {
    return GestureDetector(
      onTap: () {
        logger.v('YBDTPRoomHelp menu close');
        widget.gameVoidCallback?.call(context);
      },
      child: YBDGameWidgetUtil.assetTPImage('tp_purple_close', width: 64.dp750, height: 64.dp750, isWebp: false),
    );
  }
  void _close5G3m7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _content() {
    return Column(
      children: [
        SizedBox(height: 34.dp750),
        // exit
        _item('icon_exit', 'exit_to_lobby'.i18n, onTap: () {
          logger.v('YBDTPRoomMenu back tap');
          YBDTPRoomGetLogic.exitRoom();
        }),
        SizedBox(height: 34.dp750),
        // standup
        _item('icon_stand', 'stand_up'.i18n, onTap: () {
          logger.v('YBDTPRoomMenu Standup tap');
          // 站起的逻辑 发送站起报文
          YBDTPGameSocketApi.instance.standUp(YBDTPRoomGetLogic.findSync()?.state?.currentUser?.position);
        }),
        SizedBox(height: 34.dp750),
        // Set up
        _item('icon_set', 'set_up'.i18n, onTap: () {
          logger.v('YBDTPRoomMenu Set up tap');
          YBDTPRoomSound.show(context);
        }),
        SizedBox(height: 34.dp750),
        // How To play
        _item('icon_how', 'how_to_play'.i18n, onTap: () {
          logger.v('YBDTPRoomMenu help tap');
          YBDCommonTrack().tpgo('menu_how_to_play');
          YBDTPRoomHelp.show(context, 'tp_help_bar', 'tp_help_img');
        })
      ],
    );
  }
  void _content9utmZoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _item(String img, String name, {GestureTapCallback? onTap}) {
    return YBDDelayGestureDetector(
      onTap: () {
        logger.v('YBDTPRoomMenu item tap');
        YBDTPAudio.click.play();
        widget.gameVoidCallback?.call(context);
        onTap!.call();
      },
      child: Container(
        width: 390.dp750,
        child: Stack(
          alignment: Alignment.center,
          children: [
            YBDGameWidgetUtil.assetTPImage('menu_bg_2', width: 390.dp750, height: 70.dp750),
            Center(
                child: Row(
              children: [
                SizedBox(width: 30.dp750),
                YBDGameWidgetUtil.assetTPImage(img, width: 40.dp750, height: 40.dp750),
                SizedBox(width: 50.dp750),
                YBDGameWidgetUtil.textBalooRegular(name, fontSize: 28.sp750)
              ],
            )),
          ],
        ),
      ),
    );
  }
  void _itemCpcZboyelive(String img, String name, {GestureTapCallback? onTap}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDMenuNew extends StatelessWidget {
  final Function? closeCallback;
  // GameVoidCallback gameVoidCallback;
  const YBDMenuNew({Key? key, this.closeCallback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double gap = 20.dp750 as double;
    return Column(
      children: [
        // exit
        _item('icon_exit', 'exit_to_lobby'.i18n, onTap: () {
          logger.v('YBDTPRoomMenu back tap');
          YBDTPRoomGetLogic.exitRoom();
        }),
        SizedBox(height: gap),
        // standup
        _item('icon_stand', 'stand_up'.i18n, onTap: () {
          logger.v('YBDTPRoomMenu Standup tap');
          YBDCommonTrack().tpgo('menu_stand_up');
          // 站起的逻辑 发送站起报文
          YBDTPGameSocketApi.instance.standUp(YBDTPRoomGetLogic.findSync()?.state?.currentUser?.position);
        }),
        SizedBox(height: gap),
        // Set up
        _item('icon_set', 'set_up'.i18n, onTap: () {
          logger.v('YBDTPRoomMenu Set up tap');
          YBDTPRoomSound.show(context);
        }),
        SizedBox(height: gap),
        // How To play
        _item('icon_how', 'how_to_play'.i18n, onTap: () {
          logger.v('YBDTPRoomMenu help tap');
          YBDCommonTrack().tpgo('menu_how_to_play');
          YBDTPRoomHelp.show(context, 'tp_help_bar', 'tp_help_img');
        })
      ],
    );
  }
  void buildaeOp8oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _item(String img, String name, {GestureTapCallback? onTap}) {
    return YBDDelayGestureDetector(
      onTap: () {
        logger.v('YBDTPRoomMenu item tap');
        YBDTPAudio.click.play();
        closeCallback?.call();
        // gameVoidCallback?.call(Get.context);
        // widget.gameVoidCallback?.call(context);
        onTap!.call();
      },
      child: Container(
        width: 450.dp750,
        child: Stack(
          alignment: Alignment.center,
          children: [
            YBDGameWidgetUtil.assetTPImage('menu_bg_2', width: 450.dp750, height: 90.dp750),
            Center(
                child: Row(
              children: [
                SizedBox(width: 12.dp750),
                YBDGameWidgetUtil.assetTPImage(img, width: 60.dp750, height: 60.dp750),
                SizedBox(width: 50.dp750),
                YBDGameWidgetUtil.textBalooRegular(name, fontSize: 28.sp750)
              ],
            )),
          ],
        ),
      ),
    );
  }
  void _itemKW2Eooyelive(String img, String name, {GestureTapCallback? onTap}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
