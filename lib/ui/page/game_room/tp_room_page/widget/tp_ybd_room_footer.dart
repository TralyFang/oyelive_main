import 'dart:async';

import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/constant/i18n_ybd_key.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_boot_beans.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_fly_bean_playing.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/inner_ybd_shadow.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

class YBDTPRoomFooterBar extends StatelessWidget {
  const YBDTPRoomFooterBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return YBDInnerShadow(
      shadows: <Shadow>[
        // 内阴影
        BoxShadow(
          color: YBDHexColor('#F3E3FF').withOpacity(0.5),
          blurRadius: 20.dp750 as double,
          spreadRadius: 5.dp750 as double,
          offset: Offset(0, 0),
        )
      ],
      child: Container(
        height: 124.dp750+(65.dpBottomElse(0) as double),
        width: SizeNum.screentWidth,
        color: YBDHexColor('#6B4AC2').withOpacity(0.2),
      ),
    );
  }
  void buildpDssmoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}


class YBDTPRoomFooter extends StatefulWidget {
  const YBDTPRoomFooter({Key? key}) : super(key: key);

  @override
  State<YBDTPRoomFooter> createState() => _YBDTPRoomFooterState();
}

class _YBDTPRoomFooterState extends State<YBDTPRoomFooter> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 124.dp750,
      // color: YBDHexColor("#6B4AC2").withOpacity(0.2),
      padding: EdgeInsets.symmetric(horizontal: 39.dp750 as double),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // 用户余额
          YBDDelayGestureDetector(
            onTap: () {
              YBDNavigatorHelper.openTopUpPage(context).then((value)
                => YBDTPRoomGetLogic.findSync()?.updateUserBalance());
            },
            child: Obx(() {
              return YBDTPBootBeans(
                beansKey: TPBalanceBeansKey,
                height: 68.dp750,
                minTextWidth: 50.dp750,
                bgColor: YBDHexColor('#33106A'),
                value: YBDTPRoomGetLogic
                    .findSync()
                    ?.state
                    ?.balance
                    .value ?? 0,
              );
            }),
          ),
          // 下注操作
          GetBuilder<YBDTPRoomGetLogic>(
            // id: YBDTPRoomGetState.balanceIdentifier,
            builder: (YBDTPRoomGetLogic logic) {
            YBDTpRoomDataUser user = logic.state!.currentUser!;
            if ((user.role ?? 0) == 0) { // 是观众就不展示了
              return Container();
            }
            int minBid = logic.state!.roomData.battle?.minChaalValue ?? 0;
            int term = logic.state!.roomData.battle?.term ?? 0;
            // 庄家第一轮开牌可以不加倍，即底注10，看牌下注也可10；
            // if (user.cardState == 1
            //     && !(user.isBanker() &&
            //         logic.state.roomData?.battle?.term == 1)) {
            //   minBid = minBid * 2; // 看牌double
            // }
            int maxBid = minBid;
            if (user.optPrivilege?.canDoubleBet ?? false) {
              maxBid = minBid * 2; // 能够double
            }
            // 是当前下注，且能够下注
            bool enable = (user.optPrivilege?.canChaal ?? false)
                && logic.state!.canCountdown
                && (logic.state!.roomData.battle?.currentOperator == user.id);

            return Obx(() {
              // 用户余额判断
              int balance = logic.state?.balance.value ?? 0;
              if (maxBid > balance) {
                maxBid = minBid;
              }
              if (minBid > balance) {
                enable = false;
              }
              /// TODO: 测试Begin
              // enable = true;
              /// TODO: 测试End
              logger.v('YBDTPChaalBean minBid:$minBid, max:$maxBid, balance:$balance,enable: $enable');
              return YBDTPChaalBean(
                minBid: minBid,
                maxBid: maxBid,
                bidBeans: minBid,
                enable: enable,
                skip: minBid,
                bidTips: user.bidStateTips(),
                update: !logic.state!.canCountdown,
                balanceNot: minBid > balance,
                term: term,
                bidValueCallback: (int value) {
                  // 下注
                  YBDTPGameSocketApi.instance.chaal(value);
                  YBDTPAudio.click.play();

                  /// TODO: 测试Begin
                  // eventBus.fire(YBDTPRoomBeansEvent()
                  //   ..beginKey = YBDTPRoomBeansEvent.playerAvatarKeys[0]
                  //   ..endKey = TPBootBeansKey
                  //   ..bidValue = 2560);
                  /// TODO: 测试End
                },
              );
            });
          }),
        ],
      ),
    );
  }
  void buildQInl4oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

typedef BidValueCallback = void Function(int bidValue);

/// 跟注加码
class YBDTPChaalBean extends StatefulWidget {
  int minBid; // 最大值
  int maxBid; // 最小值
  int bidBeans; // 下注金额
  bool enable; // 可下注状态
  int skip; // 增量
  String bidTips; // 下注状态提示
  bool update; // 更新默认值
  bool biding; // 下注中
  bool balanceNot; // 余额不足
  int term; //第几轮
  BidValueCallback? bidValueCallback;

  YBDTPChaalBean({this.bidBeans = 100,
    this.minBid = 100,
    this.maxBid = 200,
    this.enable = true,
    this.skip = 100,
    this.bidTips = 'Blind',
    this.update = false,
    this.biding = false,
    this.balanceNot = false,
    this.term = 0,
    this.bidValueCallback});

  @override
  _YBDTPChaalBeanState createState() => _YBDTPChaalBeanState();
}

class _YBDTPChaalBeanState extends State<YBDTPChaalBean> {
  late int curBidBeans; // 下注金额
  int? _lastTerm; // 上次的轮次

  @override
  void initState() {
    super.initState();
    curBidBeans = widget.bidBeans;
  }
  void initStateZO7c4oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {

    // 下注筹码背景大小
    Size bidBgSize = Size(229.dp750 as double, 76.dp750 as double);
    logger.v('build state YBDTPChaalBean minBid:${widget.minBid}, max:${widget.maxBid},enable: ${widget.enable},biding:${widget.biding}');

    return Container(
      child: Row(
        children: [

          /// 减
          YBDGameWidgetUtil.assetTPImage(
              (curBidBeans > widget.minBid && widget.enable)
                  ? 'tp_reduce_green'
                  : 'tp_reduce_grey',
              width: 58.dp750,
              height: 58.dp750, onTap: () {
            logger.v('YBDTPChaalBean reduce tap');
            updateBidValue(false);
          }),

          SizedBox(width: 10.dp750),

          /// 筹码
          GestureDetector(
            // delayDuration: Duration(milliseconds: 500),
            onTap: () {
              logger.v('YBDTPChaalBean chaal tap value: $curBidBeans');
              // if (Const.TEST_ENV)
              //   YBDToastUtil.toast('chaal bid:$curBidBeans enable:${widget.enable} biding:${widget.biding}', duration: 1);
              if (widget.enable && !widget.biding) {
                // 延迟5s自动做超时还原处理
                setState(() {
                  widget.biding = true;
                });
                Timer(Duration(seconds: 5), () {
                  // 还没有被更新的才需要还原
                  if (mounted && _lastTerm == widget.term) {
                    setState(() {
                      widget.biding = false;
                    });
                  }
                });
                _lastTerm = widget.term;
                widget.bidValueCallback?.call(curBidBeans);
              }
              if (!widget.enable && widget.balanceNot) { // 余额不足的提示
                YBDToastUtil.toast(YBDI18nKey.insufficientBalance);
              }
            },
            child: Container(
              width: bidBgSize.width,
              height: bidBgSize.height,
              // decoration: BoxDecoration(border: Border.all(color: Colors.black12.withOpacity(0.8), width: 2)),
              child: Stack(
                alignment: Alignment.topCenter,
                children: [
                  YBDGameWidgetUtil.assetTPImage(
                    widget.enable ? 'tp_bid_red_bg' : 'tp_bid_grey_bg',
                    width: bidBgSize.width,
                    height: bidBgSize.height,
                    isWebp: true,
                  ),

                  // 下注提示
                  Container(
                    width: bidBgSize.width,
                    height: bidBgSize.height,
                    padding: EdgeInsets.only(top: 3.dp750 as double),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: widget.enable
                              ? YBDGameWidgetUtil.textBalooRegular(
                              widget.bidTips, fontSize: 24.sp750)
                              : YBDGameWidgetUtil.textBalooStrokRegular(
                              widget.bidTips, fontSize: 24.sp750),
                        ),
                        SizedBox(width: 17.dp750,),
                        // beans icon
                        YBDGameWidgetUtil.assetTPImage(
                            widget.enable
                                ? YBDGameResource.currencyIconType(
                                GameCurrencyType.beans)
                                : 'tp_beans_grey',
                            width: 16.dp750,
                            height: 18.dp750, isWebp: false),
                        SizedBox(width: 5.dp750,),
                        // beans
                        widget.enable
                            ? YBDGameWidgetUtil.textBalooRegular(
                            '$curBidBeans', fontSize: 24.sp750, color: YBDHexColor('#FFE47C'))
                            : YBDGameWidgetUtil.textBalooStrokRegular(
                            '$curBidBeans', fontSize: 24.sp750),
                      ],
                    ),
                  ),

                  /*
                  // 下注提示
                  Positioned(
                    top: 5.dp750,
                    child: Container(
                      child: widget.enable
                          ? YBDGameWidgetUtil.textBalooRegular(
                          widget.bidTips, fontSize: 24.sp750)
                          : YBDGameWidgetUtil.textBalooStrokRegular(
                          widget.bidTips, fontSize: 24.sp750),
                    ),
                  ),
                  // 下注豆子
                  Positioned(
                    bottom: 5.dp750,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // beans icon
                        YBDGameWidgetUtil.assetTPImage(
                            widget.enable
                                ? YBDGameResource.currencyIconType(
                                GameCurrencyType.beans)
                                : "tp_beans_grey",
                            width: 16.dp750,
                            height: 18.dp750, isWebp: false),
                        SizedBox(width: 5.dp750,),
                        // beans
                        widget.enable
                            ? YBDGameWidgetUtil.textBalooRegular(
                            "$curBidBeans", fontSize: 24.sp750, color: YBDHexColor("#FFE47C"))
                            : YBDGameWidgetUtil.textBalooStrokRegular(
                            "$curBidBeans", fontSize: 24.sp750),
                      ],
                    ),
                  ),

                   */
                  // 下注中
                  if (widget.biding)
                    Container(
                        width: bidBgSize.width,
                        height: bidBgSize.height,
                        child: CupertinoActivityIndicator()),

                ],
              ),
            ),
          ),
          SizedBox(width: 10.dp750),

          /// 加
          YBDGameWidgetUtil.assetTPImage(
              (curBidBeans< widget.maxBid && widget.enable)
                  ? 'tp_plus_green'
                  : 'tp_plus_grey',
              width: 58.dp750,
              height: 58.dp750, onTap: () {
            logger.v('YBDTPChaalBean plus tap');
            updateBidValue(true);
          }),
        ],
      ),
    );
  }
  void buildaoioeoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(covariant YBDTPChaalBean oldWidget) {
    // bidBeans: 20 curBidBeans: 320 / 40
    if (widget.bidBeans > curBidBeans|| curBidBeans> widget.maxBid
        || widget.update) {
      curBidBeans = widget.bidBeans;
    }
    super.didUpdateWidget(oldWidget);
  }

  updateBidValue(bool add) {
    // 不可操作
    if (!widget.enable) return;
    if (add) {
      if (curBidBeans< widget.maxBid) {
        curBidBeans += widget.skip;
        if (curBidBeans> widget.maxBid) {
          curBidBeans = widget.maxBid;
        }
        setState(() {});
        YBDTPAudio.click.play();
      }
    } else {
      if (curBidBeans> widget.minBid) {
        curBidBeans -= widget.skip;
        if (curBidBeans< widget.minBid) {
          curBidBeans = widget.minBid;
        }
        setState(() {});
        YBDTPAudio.click.play();
      }
    }
  }
}
