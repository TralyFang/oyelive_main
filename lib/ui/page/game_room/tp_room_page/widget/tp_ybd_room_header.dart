import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/event/game_ybd_mission_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/entity/quick_ybd_comment_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/utils/quick_ybd_msg.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/bubble_ybd_sheet.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_gradient_dialog.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_menu.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_commom_util.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/widget/dialog_ybd_event.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';
import 'package:oyelive_main/ui/page/home/entity/daily_ybd_check_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/daily_ybd_task_entity.dart';
import 'package:oyelive_main/main.dart';

import '../../widget/game_ybd_resource_util.dart';
import '../get_x/tp_ybd_room_get_logic.dart';

class YBDTPRoomHeader extends StatefulWidget {
  const YBDTPRoomHeader({Key? key}) : super(key: key);

  @override
  State<YBDTPRoomHeader> createState() => _YBDTPRoomHeaderState();
}

class _YBDTPRoomHeaderState extends State<YBDTPRoomHeader> {
  List<YBDDailyCheckRecord?>? _taskList;

  @override
  void initState() {
    super.initState();
    _requestDailyTask();
    eventBus.on<YBDGameMissionEvent>().listen((event) {
      _requestDailyTask();
    });
    getQuickComment();
  }
  void initStateIL2ttoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 36.dp750 as double, right: 40.dp750 as double),
      child: Row(
        children: [
          iconWidget('icon_back', onTap: () {
            logger.v('YBDTPRoomHeader back tap');
            // Navigator.pop(context);
            // YBDTPRoomMenu.show(context);
            YBDTPGradientDialog.show(context, title: 'Menu', height: 606, type: DialogType.tpMenu);
            // YBDTPRoomGetLogic.exitRoom();
            YBDTPAudio.click.play();
          }),
          Expanded(child: Container(width: 1)),
          // 任务
          YBDGameCommomUtil.getGameConfig().showTPGOTaskIcon
              ? iconWidget('icon_daily_task', onTap: () {
                  logger.v('YBDTPRoomHeader task tap');
                  YBDCommonTrack().tpgo('task');
                  // YBDTPRoomTask.show(context);
                  // YBDTPGameSocketApi.instance.sitDown(4);
                  YBDTPAudio.click.play();

                  /// 任务弹窗
                  showDialog<Null>(
                      context: context, //BuildContext对象
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return YBDEventDialog(_taskList, YBDLocationName.TPGO);
                      });
                }): Container(),

          GetBuilder<YBDTPRoomGetLogic>(builder: (YBDTPRoomGetLogic logic) {
            if (YBDQuickMsgHelper.qm != null && logic.state?.currentUser?.id != null)
              return Padding(
                  padding: EdgeInsets.only(left: 14.px),
                  child: GestureDetector(
                    onTap: () {
                      // if(YBDQuickMsgHelper.qm!=null)
                      YBDCommonTrack().tpgo('chat');
                      YBDBubbleSheet.show(YBDQuickMsgHelper.qm!, onSelected: (code) {
                        YBDGameRoomSocketApi.getInstance().sendQuickMsg(code);
                      });
                    },
                    child: Image.asset(
                      YBDGameResource.assetPadding("icon_chat", isWebp: true, need2x: false),
                      width: 72.px,
                    ),
                  ));

            return SizedBox();
          }),
        ],
      ),
    );
  }
  void buildadKONoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求每日任务列表
  Future _requestDailyTask() async {
    YBDMyDailyTaskEntity? dailyTaskEntity = await ApiHelper.queryDailyTask(context, viewing: 2);

    if (null != dailyTaskEntity && null != dailyTaskEntity.record) {
      _taskList = dailyTaskEntity.record;

      if (mounted) setState(() {});
    } else {
      logger.v('game task rewardList is null');
    }
  }
  void _requestDailyTaskX07Gioyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getQuickComment() async {
    YBDQuickCommentData? data = await ApiHelper.queryQuickComment(context);
    if (data != null) {
      YBDQuickMsgHelper.storeQuickMsg(data.data!);
      setState(() {});
    }
  }

  Widget iconWidget(String name, {GestureTapCallback? onTap}) {
    return YBDGameWidgetUtil.assetTPImage(name, width: 70.dp750, height: 70.dp750, onTap: onTap);
  }
  void iconWidgetqESN0oyelive(String name, {GestureTapCallback? onTap}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
