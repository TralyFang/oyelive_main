
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/text_ybd_with_stroke.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';
import 'package:oyelive_main/ui/widget/inner_shadow/flutter_ybd_inset_box_shadow.dart' as i;

class YBDTPRoomSound extends StatefulWidget {
  static show(BuildContext context) {
    YBDEasyPopup.show(context, YBDTPRoomSound(
      gameVoidCallback: (popContext) {
        YBDEasyPopup.pop(popContext);
      },
    ), position: EasyPopupPosition.center, duration: Duration(milliseconds: 50), outsideTouchCancelable: true);
  }

  GameVoidCallback? gameVoidCallback;

  YBDTPRoomSound({Key? key, this.gameVoidCallback}) : super(key: key);

  @override
  _YBDTPRoomSoundState createState() => _YBDTPRoomSoundState();
}

class _YBDTPRoomSoundState extends State<YBDTPRoomSound> {
  double _soundLeve = YBDTPRoomGetLogic.findSync()?.state?.volume ?? 1.0; // 音量

  @override
  void initState() {
    super.initState();
  }
  void initStateNcLu2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
            border: Border.all(width: 2.dp750 as double, style: BorderStyle.solid, color: Color(0xffffd94e)),
            borderRadius: BorderRadius.all(Radius.circular(32.dp750 as double)),
            gradient: LinearGradient(
                colors: [Color(0xff00B367), Color(0xff106542)],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter),
          ),
          width: 540.dp750,
          height: 380.dp750,
          // padding: EdgeInsets.all(5.dp720),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              _setupTitle(),
              Container(
                  child: Stack(
                children: [
                  // YBDGameWidgetUtil.assetTPImage('menu_bg_0', width: 540.dp750, height: 200.dp750, fit: BoxFit.fill),
                  Column(
                    children: [
                      SizedBox(
                        height: 60.dp750,
                      ),
                      _soundBar(),
                      SizedBox(height: 60.dp750),
                      GestureDetector(
                        onTap: () {
                          YBDCommonTrack().tpgo('menu_sound_slide');
                          YBDTPRoomGetLogic.findSync()?.state?.volume = _soundLeve;
                          widget.gameVoidCallback?.call(context);
                        },
                        child: Container(
                          height: 72.dp750,
                          width: 300.dp750,
                          decoration: i.BoxDecoration(
                              gradient: LinearGradient(colors: [
                                Color(0xffFFCE5B),
                                Color(0xffFFBC04),
                              ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                              borderRadius: BorderRadius.all(Radius.circular(40.dp750 as double)),
                              boxShadow: [
                                i.BoxShadow(
                                    color: Colors.white.withOpacity(0.5),
                                    offset: Offset(0, 1.px as double),
                                    blurRadius: 2.px as double,
                                    inset: true),
                                i.BoxShadow(
                                    color: Color(0xff818181), offset: Offset(0, -1.px as double), blurRadius: 2.px as double, inset: true),
                              ]),
                          child: Center(
                            child: YBDTextWithStroke(
                              text: 'OK',
                              fontSize: 32.sp750,
                              fontFamily: 'baloo',
                              strokeColor: Color(0xff786000),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 40.dp750),
                    ],
                  ),
                ],
              )),
            ],
          ),
        ),
        SizedBox(height: 50.dp750),
        _close()
      ],
    );
  }
  void build2yPtxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _soundBar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // SizedBox(width: 10.dp750),
        YBDGameWidgetUtil.textBalooRegular('Sound:', color: Colors.white, fontSize: 32.dp750),
        // CupertinoSlider(
        //     value: _soundLeve,
        //     thumbColor: YBDHexColor("#D8D8D8"), // 灰色
        //     activeColor: YBDHexColor("#B13DEB"), // 紫色
        //     onChanged: (value) {
        //       YBDCommonTrack().tpgo('menu_sound_slide');
        //       setState(() {
        //         _soundLeve = value;
        //       });
        //     }),
        SliderTheme(
          data: SliderTheme.of(context).copyWith(
            trackHeight: 18.dp750, // 轨道高度
            activeTrackColor: Color(0xffFFC737), // 激活的轨道颜色
            inactiveTrackColor: Color(0xffD8D8D8), // 未激活的轨道颜色
            thumbShape: YBDMySlider(),
            thumbColor: Color(0xff0C7D4F), // 滑块颜色
            overlayShape: RoundSliderOverlayShape(
              overlayRadius: 20.dp750 as double, // 滑块外圈大小
            ),
            overlayColor: Colors.white, // 滑块外圈颜色
          ),
          child: Slider(
            value: _soundLeve,
            min: 0,
            max: 1,
            onChanged: (v) {
              setState(() {
                _soundLeve = v;
              });
            },
          ),
        ),
        YBDGameWidgetUtil.assetImage(
          (_soundLeve == 0) ? 'tp_mute' : 'tp_sound',
          width: 48.dp720,
          height: 48.dp720,
        ),
      ],
    );
  }

  Widget _close() {
    return GestureDetector(
      onTap: () {
        logger.v('YBDTPRoomSound tap close');
        widget.gameVoidCallback?.call(context);
      },
      child: YBDGameWidgetUtil.assetTPImage('tp_green_close', width: 64.dp750, height: 64.dp750),
    );
  }
  void _closefUNBboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _setupTitle() {
    return Container(
        height: 80.dp750,
        decoration: BoxDecoration(
          color: Color(0xff0C7E4F),
          borderRadius: BorderRadius.vertical(top: Radius.circular(32.dp750 as double)),
        ),
        child: Center(
          child: YBDGameWidgetUtil.textBalooRegular('set_up'.i18n, fontSize: 32.sp750),
        ));
  }
  void _setupTitleYNVDFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDMySlider extends SliderComponentShape {
  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(20.dp750 as double);
  }

  @override
  void paint(PaintingContext context, Offset center,
      {Animation<double>? activationAnimation,
      Animation<double>? enableAnimation,
      bool? isDiscrete,
      TextPainter? labelPainter,
      RenderBox? parentBox,
      SliderThemeData? sliderTheme,
      TextDirection? textDirection,
      double? value,
      double? textScaleFactor,
      Size? sizeWithOverflow}) {
    final Canvas canvas = context.canvas;
    canvas.drawCircle(
      center,
      15.dp750 as double,
      Paint()..color = Colors.white,
    );
    canvas.drawCircle(
      center,
      8.dp750 as double,
      Paint()..color = Color(0xff0C7D4F),
    );
  }
}
