import 'dart:async';

import 'dart:math';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oye_tool_package/ext/int_ext.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_player_avatar.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_playing.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';

class YBDTPAvatarsRunBanker extends StatefulWidget {
  const YBDTPAvatarsRunBanker({Key? key}) : super(key: key);

  @override
  _YBDTPAvatarsRunBankerState createState() => _YBDTPAvatarsRunBankerState();
}

class _YBDTPAvatarsRunBankerState extends BaseState<YBDTPAvatarsRunBanker> with TickerProviderStateMixin {
  YBDTPRoomGetLogic logic = Get.find<YBDTPRoomGetLogic>();
  bool _runing = false;
  int _updateIndex = -1;
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();

    addSub(eventBus.on<YBDTPRoomRunBankerEvent>().listen((event) {
      if (event.start) {
        // 开始跑庄
        runingBanker();
      }
    }));
    _controller = AnimationController(vsync: this);
  }
  void initStatenrsdkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return GetBuilder<YBDTPRoomGetLogic>(builder: (logic) {
      return
      /*
        FadeOut(
        controller: (controller) => _controller = controller,
        child:
       */
        Stack(
          children: [
            /// 用户头像
            ...userAvatars(logic),
            // ...pointReds(logic),
          ],
        // ),
      );
    });
  }
  void myBuildyTkBroyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<void> runingBanker() async {
    if (!mounted || logic.state!.bankerFirstUsers == null || logic.state!.roomData.users == null) return;
    setState(() {
      _runing = true;
    });
    Future.delayed(Duration(milliseconds: 1000), () {
      if (mounted) {
        setState(() {
          _runing = false;
          _updateIndex = -1;
        });
      }
    });

    int playerCount = logic.state!.bankerFirstUsers!.length;
    // 创建玩家索引，只在玩家之间跑庄
    List<int> playerInts = List.generate(playerCount, (index) => index);
    for (int i = 0; i < playerCount; i++) {
      if (i > 0) {
        await Future.delayed(Duration(milliseconds: 200));
      }
      int randomInt = getRandomInt(playerInts);
      // 相对用户的索引
      int userIndex =
          logic.state!.roomData.users!.indexWhere((element) => element!.id == logic.state!.bankerFirstUsers![randomInt]!.id);
      logger.v("runingBanker userIndex:$userIndex,randomInt:$randomInt");
      if (userIndex >= 0) {
        setState(() {
          _updateIndex = userIndex;
        });
      }
    }
  }
  void runingBankerQKCtVoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 获取跑庄随机数
  int getRandomInt(List<int> playerInts) {
    if (playerInts.length == 1) return 0; // 只剩下一个了，那就是庄家了
    int randomInt = 1.toRandomMax(playerInts.length - 1);
    int index = playerInts[randomInt];
    playerInts.removeAt(randomInt);
    return index;
  }
  void getRandomInt6KHHeoyelive(List<int> playerInts) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Align>? _runUserAvatars;

  // 更新跑庄用户状态
  List<Align> runBankerUserAvatars() {
    int playerCount = logic.state!.bankerFirstUsers?.length ?? 0;

    if (playerCount > 1 && _updateIndex >= 0) {
      Align align = _runUserAvatars![_updateIndex];
      _runUserAvatars![_updateIndex] = Align(
        alignment: align.alignment,
        child: avatarIndex(_updateIndex, runBanker: true),
      );
      logger.v("runBankerUserAvatars _updateIndex:$_updateIndex");
    }
    return _runUserAvatars!;
  }
  void runBankerUserAvatarshS1d7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 头像坐标
  List<Align> userAvatars(YBDTPRoomGetLogic logic) {
    /*
    * 头像用来开局跑庄：
    * 1. 只有开局才跑庄
    * 2. 在玩家之间随意亮起
    * 3. 最后定在庄家亮起三次
    * */

    // 游戏进行中才能重用跑庄，避免服务器报文插入对局状态1-4-2的错乱情况，正常对局状态1-2-4
    if (_runing && _runUserAvatars != null && logic.state?.roomData.battle?.status == 2) {
      return runBankerUserAvatars();
    }

    List<Alignment> aligments = YBDTPAvatarSize.avatarAligns(roomPlayerHeight());

    List<Align> avatars = aligments.mapIndexValue((int index, Alignment value) {
      return Align(alignment: value, child: avatarIndex(index));
    });
    _runUserAvatars = avatars;
    return avatars;
  }
  void userAvatarsvZN84oyelive(YBDTPRoomGetLogic logic) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// TTTest
  List<Widget> pointReds(YBDTPRoomGetLogic logic) {
    double height = roomPlayerHeight();

    List<Widget> avatars = [];

    Alignment algin = YBDTPAvatarSize.avatarAligns(height)[2];
    Offset lefttop = algin.alongSize(Size(SizeNum.screentWidth as double, height));

    Alignment algin2 = YBDTPAvatarSize.avatarAligns(height)[1];
    Offset leftbottom = algin2.alongSize(Size(SizeNum.screentWidth as double, height));

    Widget bottomPoint = Positioned(
        left: leftbottom.dx,
        top: leftbottom.dy,
        child: Container(
          width: 20,
          height: 20,
          color: Colors.yellow,
        ));

    Widget top = Align(
        alignment: Alignment(0.0, 0.8),
        child: Container(
          width: YBDTPAvatarSize.selfMaxAvatar,
          height: YBDTPAvatarSize.selfMaxAvatar,
          color: Colors.red.withOpacity(0.5),
        ));
    Widget topPzero = Positioned(
        left: lefttop.dx,
        top: lefttop.dy,
        child: Container(
          width: 20,
          height: 20,
          color: Colors.red,
        ));

    avatars.add(top);
    avatars.add(topPzero);
    avatars.add(bottomPoint);

    return avatars;
  }
  void pointRedsk3LVhoyelive(YBDTPRoomGetLogic logic) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  YBDTPPlayerAvatar avatarIndex(int index, {bool runBanker = false}) {
    var position = YBDTPAvatarSize.avatarKingPositions;

    int? curUserId = logic.state!.curUserId;
    YBDTpRoomDataBattle? battle = logic.state!.roomData.battle;
    GlobalKey? key;
    YBDTpRoomDataUser? user = YBDTpRoomDataUser();
    if (logic.state!.roomData.users != null && logic.state!.roomData.users!.length > index) {
      user = logic.state!.roomData.users![index];
      // 不是玩家不做记录
      if (YBDTPRoomBeansEvent.playerAvatarKeys != null &&
          YBDTPRoomBeansEvent.playerAvatarKeys!.length > index &&
          user!.role != 0) {
        key = YBDTPRoomBeansEvent.playerAvatarKeys![index];
      }
    }
    return YBDTPPlayerAvatar(
      avatarKey: key,
      battle: battle,
      curUserId: curUserId,
      user: user,
      runBanker: runBanker,
      kingPosition: position[index],
      leaderId: logic.state!.roomData.leaderId,
      clickEmptySeat: () {
        List<YBDTpRoomStandUpUser?> tpStandUpUser = logic.state!.roomData.standUpUser ?? [];
        List<String> standUpUser = tpStandUpUser.map((YBDTpRoomStandUpUser? e) => '${e!.userId}').toList();
        logger.v(
            '22.9.23--TPPlayer empty Avatar tap:${user?.position}--standUpUser:$standUpUser--curUserId:$curUserId--seatId:${user?.id}');
        // 位置不为空 && 站起数组不为空 && 该座位无人 && 座位的id为null || 之前在这个座位的人已经站起
        if (user?.position != null && standUpUser != null && (user?.id == null || user!.isStandUp!)) {
          YBDCommonTrack().tpgo('join_game');
          YBDTPGameSocketApi.instance.sitDown(user!.position);
          // _controller.forward();
          // Future.delayed(Duration(milliseconds: 500), () {
          //   _controller.reverse();
          // });
        }
      },
    );
  }
}
