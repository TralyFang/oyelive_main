import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

/// 比牌结果蒙版
class YBDTPCardCompared extends StatefulWidget {
  Size bgSize;
  bool isSelf;
  bool isWin;

  YBDTPCardCompared(
      {Key? key,
      required this.bgSize,
      required this.isWin,
      required this.isSelf})
      : super(key: key);

  @override
  _YBDTPCardComparedState createState() => _YBDTPCardComparedState();
}

class _YBDTPCardComparedState extends State<YBDTPCardCompared> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: widget.isSelf
          ? null
          : BoxDecoration(
              image: DecorationImage(
                  image: YBDGameWidgetUtil.assetTPImageProvider('tp_mark_bg',
                      isWebp: true))),
      width: widget.bgSize.width,
      height: widget.bgSize.height,
      child: Stack(
        children: <Widget>[
          /// 输赢状态
          // 是别人
          if (!widget.isSelf)
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 60.dp750 as double),
                  child: YBDGameWidgetUtil.assetTPImage(
                    widget.isWin ? 'tp_card_win' : 'tp_card_lose',
                    width: 142.dp750,
                    height: 43.dp750,
                  ),
                ),
              ],
            ),
          // 是自己
          if (widget.isSelf)
            YBDGameWidgetUtil.assetTPImage(
              widget.isWin ? 'tp_card_win' : 'tp_card_lose',
              width: widget.bgSize.width,
              height: widget.bgSize.height,
            )],
      ),
    );
  }
  void build2aYFPoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
