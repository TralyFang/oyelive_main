import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';

class YBDTPNowWatching extends StatefulWidget {
  static show(BuildContext context, String timeoutMsg) {
    YBDEasyPopup.show(
        context,
        YBDTPNowWatching(
          gameVoidCallback: (popContext) {
            YBDEasyPopup.pop(popContext);
          },
          timeoutMsg: timeoutMsg,
        ),
        position: EasyPopupPosition.center,
        duration: Duration(milliseconds: 50),
        outsideTouchCancelable: true);
  }

  GameVoidCallback? gameVoidCallback;
  String? timeoutMsg;

  YBDTPNowWatching({Key? key, this.gameVoidCallback, this.timeoutMsg}) : super(key: key);

  @override
  _YBDTPNowWatchingState createState() => _YBDTPNowWatchingState();
}

class _YBDTPNowWatchingState extends State<YBDTPNowWatching> {
  @override
  void initState() {
    super.initState();
  }
  void initStateiHbkAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 499.dp750,
      height: 448.dp750,
      decoration: BoxDecoration(
          // 改背景
          image: DecorationImage(image: YBDGameWidgetUtil.assetTPImageProvider('tp_watching', isWebp: true))),
      child: Stack(
        children: [
          Column(
            children: [
              SizedBox(
                height: 189.dp750,
              ),
              Container(
                // color: Colors.yellow.withOpacity(0.3),
                padding: EdgeInsets.symmetric(horizontal: 59.dp750 as double),
                child: YBDGameWidgetUtil.textBalooRegular(
                  widget.timeoutMsg,
                  // "You are no longer sitting on the table because you missed your turn.",
                  fontSize: 28.sp750,
                  lineHeight: 36.sp750,
                  lines: 3,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 40.dp750,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // 改背景
                  YBDGameWidgetUtil.buttonTextTPBg('OK',
                      width: 260.dp750, height: 72.dp750, bgName: 'tp_btn_blue_bg', fontSize: 28.sp750, onTap: () {
                    widget.gameVoidCallback?.call(context);
                  }),
                ],
              )
            ],
          ),
          Positioned(
              right: 0,
              top: 6.dp750,
              child: YBDGameWidgetUtil.assetTPImage('tp_grey_close',
                  width: 44.dp750, height: 44.dp750, minTapSize: 70.dp750, onTap: () {
                widget.gameVoidCallback?.call(context);
              })),
        ],
      ),
    );
  }
  void buildfOjkWoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDWatchNew extends StatelessWidget {
  final Function? closeCallback;
  final String? timeoutMsg;
  const YBDWatchNew({Key? key, this.closeCallback, this.timeoutMsg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 400.dp750,
          height: 120.dp750,
          child: Text(
            timeoutMsg!,
            textAlign: TextAlign.center,
            // maxLines: 3,
            style: TextStyle(color: Colors.white, fontSize: 28.sp750),
          ),
        ),
        SizedBox(height: 40.dp750),
        _btn(),
      ],
    );
  }
  void buildrUah1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _btn() {
    Color strokeColor = Colors.black;
    String text = 'OK';
    return GestureDetector(
      onTap: closeCallback as void Function()?,
      child: Container(
          height: 70.dp750,
          child: Stack(
            alignment: Alignment.center,
            children: [
              YBDGameWidgetUtil.assetTPImage('btn_watch', width: 300.dp750, height: 70.dp750),
              YBDGameWidgetUtil.textBalooStrokRegular(
                text,
                fontSize: 32.sp750,
                color: Colors.white,
                strokeColor: strokeColor,
                addStroke: true,
              ),
            ],
          )),
    );
  }
  void _btnY9833oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
