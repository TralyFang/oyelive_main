import 'dart:async';

import 'package:flutter/material.dart';

/// 下注动画
/// 透明 --->不透明 从下面出豆/从上面出豆--->文字，豆子分离---> 文字透明，豆子飞向指定目标
///
///

class YBDTPFlyBeanAnimation extends StatefulWidget {
  Offset? start;
  Offset? end;
  YBDTPContainer? childBean; // 仅仅需要 width，用于分离动画计算
  YBDTPContainer? childText; // 无需width，height
  YBDTPFlyBeanController? controller;
  bool startPlaying; // 创建完就播放
  Function(YBDTPFlyBeanAnimation item)? finishCallback;
  double offsetY; // 偏移决定从哪里出来 负数从上面冒出，正数从下面冒出
  Duration duration; // 动画时间

  YBDTPFlyBeanAnimation(
      {this.start = Offset.zero,
      this.end = Offset.zero,
      this.childBean,
      this.childText,
      this.controller,
      this.startPlaying = false,
      this.offsetY = -50.0,
      this.duration = const Duration(milliseconds: 1000),
      this.finishCallback});

  @override
  _YBDTPFlyBeanAnimationState createState() => _YBDTPFlyBeanAnimationState();
}

class _YBDTPFlyBeanAnimationState extends State<YBDTPFlyBeanAnimation> with SingleTickerProviderStateMixin {
  AnimationController? _animationController;
  late Animation<Offset> _slideAnimation;
  late Animation<double> _opacityAnimation;
  Animation<double>? _opacityTextAnimation;
  late Animation<Offset> _slideBeanAnimation;
  late Animation<double> _opacityBeanAnimation;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(duration: widget.duration, vsync: this);

    // [0.0, 0.5]
    // 位移 [0, 1.0]
    // 渐变 时间：[0.0, 0.5]

    Offset end = Offset(widget.start!.dx, widget.start!.dy + widget.offsetY);

    // 滑动出来动画
    _slideAnimation = Tween<Offset>(begin: widget.start, end: end).animate(
      CurvedAnimation(
        parent: _animationController!,
        curve: Interval(0.0, 0.5, curve: Curves.easeOutBack),
      ),
    );

    _opacityAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _animationController!,
        curve: Interval(0.0, 0.25, curve: Curves.easeOut),
      ),
    );

    // 分离动画
    _slideBeanAnimation = Tween<Offset>(begin: end, end: widget.end).animate(
      CurvedAnimation(
        parent: _animationController!,
        curve: Interval(0.5, 1.0, curve: Curves.easeOut),
      ),
    );

    _opacityTextAnimation = Tween<double>(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(
        parent: _animationController!,
        curve: Interval(0.9, 1.0, curve: Curves.easeOut),
      ),
    );

    _opacityBeanAnimation = Tween<double>(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(
        parent: _animationController!,
        curve: Interval(0.9, 1.0, curve: Curves.easeOut),
      ),
    );

    _animationController!.addListener(() {
      if (mounted) {
        setState(() {});
      }
    });

    _animationController!.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        widget.finishCallback?.call(widget);
      }
    });

    widget.controller?.initState(this);

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      if (widget.startPlaying) _animationController!.forward();
    });
  }
  void initStateaTF1Hoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  updateTween() {
    Offset end = Offset(widget.start!.dx, widget.start!.dy + widget.offsetY);

    // 滑动出来动画
    _slideAnimation = Tween<Offset>(begin: widget.start, end: end).animate(
      CurvedAnimation(
        parent: _animationController!,
        curve: Interval(0.0, 0.5, curve: Curves.easeOutBack),
      ),
    );
    // 分离动画
    _slideBeanAnimation = Tween<Offset>(begin: end, end: widget.end).animate(
      CurvedAnimation(
        parent: _animationController!,
        curve: Interval(0.5, 1.0, curve: Curves.easeOut),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (_animationController!.value == 0.0) {
      return Container();
    }
    // 连体动画
    if (_animationController!.value < 0.5) {
      return Positioned(
        left: _slideAnimation.value.dx,
        top: _slideAnimation.value.dy,
        child: Opacity(
          opacity: _opacityAnimation.value,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              widget.childBean!,
              widget.childText!,
            ],
          ),
        ),
      );
    }
    // 分离动画
    return Stack(
      children: [
        Positioned(
          left: _slideBeanAnimation.value.dx,
          top: _slideBeanAnimation.value.dy,
          child: Opacity(
              opacity: _opacityBeanAnimation.value,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  widget.childBean!,
                  widget.childText!,
                ],
              )),
        ),
        // Positioned(
        //   left: _slideAnimation.value.dx + widget.childBean.width,
        //   top: _slideAnimation.value.dy,
        //   child: Opacity(
        //     opacity: _opacityTextAnimation.value,
        //     child: widget.childText,
        //   ),
        // ),
      ],
    );
  }
  void builduC6ofoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _animationController!.dispose();

    super.dispose();
  }
}

class YBDTPFlyBeanController {
  _YBDTPFlyBeanAnimationState? _state;

  initState(_YBDTPFlyBeanAnimationState state) {
    _state = state;
  }

  forward(
      {Offset? start,
      Offset? end,
      double? offsetY,
      YBDTPContainer? childText,
      Function(YBDTPFlyBeanAnimation item)? finishCallback}) {
    if (start != null) _state!.widget.start = start;
    if (end != null) _state!.widget.end = end;
    if (offsetY != null) _state!.widget.offsetY = offsetY;
    if (childText != null) _state!.widget.childText = childText;
    if (finishCallback != null) _state!.widget.finishCallback = finishCallback;
    _state!.updateTween();
    _state?._animationController?.reset();
    _state?._animationController?.forward();
  }
}

class YBDTPContainer extends StatelessWidget {
  double? width;
  double? height;
  Widget? child;

  YBDTPContainer({this.width, this.height, this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: child,
    );
  }
  void buildT91XZoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
