import 'dart:async';

/*
 * @Author: William
 * @Date: 2022-09-22 14:37:59
 * @LastEditTime: 2022-09-26 11:12:27
 * @LastEditors: William
 * @Description: miss your turn and enforce stand up
 * @FilePath: /oyetalk-flutter/lib/ui/page/game_room/tp_room_page/widget/tp_stand_tips.dart
 */
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/room_page/widget/game_ybd_props_pop.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/popup_ybd_util.dart';

class YBDTPStandTips extends StatefulWidget {
  GameVoidCallback? callback;
  YBDTPStandTips({this.callback});

  static bool showing = false;

  static void show(BuildContext? context) {
    if (!showing) {
      showing = true;
      YBDEasyPopup.show(
        context!,
        YBDTPStandTips(
          callback: (BuildContext popContext) {
            showing = false;
            YBDEasyPopup.pop(popContext);
          },
        ),
        position: EasyPopupPosition.center,
        duration: Duration(milliseconds: 50),
        outsideTouchCancelable: false,
      );
    } else {
      logger.v('22.9.23--tips showing! just wait a minute!');
    }
  }

  @override
  State<YBDTPStandTips> createState() => _YBDTPStandTipsState();
}

class _YBDTPStandTipsState extends State<YBDTPStandTips> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_top(), SizedBox(height: 40.dp750), _close()],
    );
  }
  void buildiV0vjoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _top() {
    return Stack(
      alignment: Alignment.center,
      children: [
        YBDGameWidgetUtil.assetTPImage('now_watching', width: 520.dp750, height: 460.dp750),
        _content(),
      ],
    );
  }
  void _topojDw2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _content() {
    return Column(
      children: [
        SizedBox(height: 150.dp750),
        Container(
          width: 450.dp750,
          height: 128.dp750,
          child: Text(
            'stand_up_tip'.i18n,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 28.sp750),
          ),
        ),
        SizedBox(height: 34.dp750),
        _button()
      ],
    );
  }
  void _contentxuqg3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _button() {
    return Stack(
      alignment: Alignment.center,
      children: [
        YBDGameWidgetUtil.assetTPImage('button_ok', width: 260.dp750, height: 81.dp750, onTap: () {
          logger.v('YBDTPStandTips close');
          widget.callback?.call(context);
        }),
        YBDGameWidgetUtil.textBalooRegular('ok'.i18n, fontSize: 32.sp750)
      ],
    );
  }
  void _buttonwnzWNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _close() {
    return GestureDetector(
      onTap: () {
        logger.v('YBDTPStandTips close');
        widget.callback?.call(context);
      },
      child: YBDGameWidgetUtil.assetTPImage('tp_purple_close', width: 64.dp750, height: 64.dp750, isWebp: false),
    );
  }
}
