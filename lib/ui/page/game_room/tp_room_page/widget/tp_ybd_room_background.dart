import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

class YBDTPRoomBackground extends StatefulWidget {
  const YBDTPRoomBackground({Key? key}) : super(key: key);

  @override
  State<YBDTPRoomBackground> createState() => _YBDTPRoomBackgroundState();
}

class _YBDTPRoomBackgroundState extends State<YBDTPRoomBackground> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(decoration: YBDGameWidgetUtil.linearGradientBackground()),
        // boardContainer(),
        // 背景
        YBDGameWidgetUtil.assetTPImage(
          "background",
          width: SizeNum.screentWidth,
          height: SizeNum.screentHeight,
          fit: BoxFit.cover,
          suffix: "jpg",
        ),
        // 桌子
        Container(
          padding: EdgeInsets.only(top: SizeNum.statusBarHeight+(24.dp750 as double)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              YBDGameWidgetUtil.assetTPImage(
                "card_desktop",
                width: 659.dp750,
                height: 1304.dp750,
              ),
            ],
          ),
        )
      ],
    );
  }
  void buildwzpfaoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
