
import 'dart:developer';

import 'package:bubble_box/bubble_box.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/ui/page/game_room/entity/chat_ybd_publish.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/utils/quick_ybd_msg.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_playing.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_room_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:synchronized/synchronized.dart';
import 'package:oyelive_main/ui/widget/rich_ybd_text_decoder.dart';

import 'tp_ybd_player_avatar.dart';

enum BubbleLocation { BottomCenter, BottomLeft, TopLeft, TopRight, BottomRight }

class YBDBubbleLayer extends StatefulWidget {
  YBDBubbleController controller;

  YBDBubbleLayer(this.controller);

  @override
  YBDBubbleLayerState createState() => new YBDBubbleLayerState();
}

class YBDBubbleLayerState extends BaseState<YBDBubbleLayer> with TickerProviderStateMixin {
  Map<BubbleLocation, YBDPlainText?> textAtLocation = {};
  Widget getBubble(BubbleLocation location) {
    if (controllers[location.index].status != AnimationStatus.forward) {
      return SizedBox();
    }

    var positon;
    var alignment;
    if (location.index == 0) {
      positon = BubblePosition.center(0);
      alignment = Alignment.bottomCenter;
    } else if (location.index < BubbleLocation.TopRight.index) {
      positon = BubblePosition.start(60.dp720);
      alignment = Alignment.bottomLeft;
    } else {
      positon = BubblePosition.end(60.dp720);
      alignment = Alignment.bottomRight;
    }
    return Transform.scale(
      scale: animations[location.index].value,
      alignment: alignment,
      child: BubbleBox(
        maxWidth: 274.dp720,

        shape: BubbleShapeBorder(
            radius: BorderRadius.all(Radius.circular(20.dp720)),
            direction: BubbleDirection.bottom,
            position: positon,
            arrowQuadraticBezierLength: 10.dp720,
            border: BubbleBoxBorder(color: Color(0xffFFE1A1), width: 1.dp720),
            arrowHeight: 20.dp720),
        backgroundColor: Color(0xff20003A).withOpacity(0.6),
        // margin: EdgeInsets.all(4),
        child: Center(
          child: YBDRichTextDecode().getRichTextWidget(textAtLocation[location]!),
        ),
      ),
    );
  }
  void getBubble90bh5oyelive(BubbleLocation location) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  final _lock = Lock();
  List<AnimationController> controllers = [];
  List animations = [];
  void makeAnis() {
    List.generate(BubbleLocation.values.length, (int index) {
      AnimationController c = AnimationController(vsync: this, duration: Duration(seconds: 5));
      controllers.add(c);
      animations.add(TweenSequence<double>(
        <TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: 1.0).chain(CurveTween(curve: Curves.ease)),
            weight: 1,
          ),
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(1.0),
            weight: 9,
          ),
        ],
      ).animate(c));

      c.addListener(() {
        if (mounted) setState(() {});
      });
    });
  }
  void makeAnisiF83eoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //userId,index
  Map<int, int> showingUserIds = {};

  Lock _idLock = Lock();
  showTextAtIndex(int index, int userId, YBDPlainText? text) async {
    if (index == -1) return;

    await _idLock.synchronized(() {
      showingUserIds.addAll({userId: index});
    });
    print("showat$index $userId $showingUserIds");

    var controller = controllers[index];
    if (controller.status != AnimationStatus.dismissed) {
      controller.reset();
    }
    await _lock.synchronized(() {
      textAtLocation.addAll({BubbleLocation.values[index]: text});
    });
    controllers[index].forward();
  }

  dismissAtIndex(int index) {
    if (index == -1) return;

    log("dissbubleat$index");
    var controller = controllers[index];
    if (controller.status != AnimationStatus.dismissed) {
      controller.reset();
    }
  }

  int findIndexByUserId(YBDChatPublish data) {
    List resultList = YBDTPRoomGetLogic.findSync()?.state?.roomData.users ?? [];
    return resultList.indexWhere((element) => element.id.toString() == data.body!.sender);
  }
  void findIndexByUserIds6i54oyelive(YBDChatPublish data) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    // TODO: implement myBuild
    print("sdjsdodj${BubbleLocation.values.length}");

    return Stack(
      children: List.generate(BubbleLocation.values.length, (index) {
        double bottomPadding;
        if (index == 0)
          bottomPadding = -10.dp720;
        else if (index == 1 || index == 4)
          bottomPadding = -10.dp720;
        else
          bottomPadding = 80.dp720;

        return Align(
          alignment: YBDTPAvatarSize.avatarAligns(roomPlayerHeight())[index],
          child: Padding(
            padding: EdgeInsets.only(
                bottom: YBDTPAvatarSize.selfMaxAvatar + bottomPadding,
                left: index == 0 ? 0 : 70.dp720,
                right: index == 0 ? 38.dp720 : 70.dp720),
            child: SizedBox(width: 274.dp720, height: 90.dp720, child: getBubble(BubbleLocation.values[index])),
          ),
        );
      }),
    );
    // return Stack(
    //   children: [
    //     Positioned(
    //       child: getBubble(BubbleLocation.TopLeft),
    //       width: 274.dp720,
    //       top: 300.dp750,
    //       left: 70.dp720,
    //     ),
    //     Positioned(
    //       child: getBubble(BubbleLocation.TopRight),
    //       width: 274.dp720,
    //       top: 300.dp750,
    //       right: 70.dp720,
    //     ),
    //     Positioned(
    //       child: getBubble(BubbleLocation.BottomLeft),
    //       width: 274.dp720,
    //       top: 730.dp750,
    //       left: 70.dp720,
    //     ),
    //     Positioned(
    //       child: getBubble(BubbleLocation.BottomRight),
    //       width: 274.dp720,
    //       top: 730.dp750,
    //       right: 70.dp720,
    //     ),
    //     Positioned(
    //       child: getBubble(BubbleLocation.BottomCenter),
    //       bottom: 400.dp750,
    //       left: 223.dp720,
    //       right: 223.dp720,
    //     ),
    //   ],
    // );
  }
  void buildqOoZxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.controller.bindState(this);
    makeAnis();
    addSub(YBDGameRoomSocketApi.getInstance().messageOutStream.listen((event) {
      if (event is YBDChatPublish) {
        showTextAtIndex(findIndexByUserId(event), int.parse(event.body!.sender!), event.body!.content);
      }
    }));
    addSub(YBDTPRoomGetLogic.findSync()!.stateStream.listen((event) {
      if (event?.roomData.users?.isNotEmpty ?? false) {
        log("showing:${showingUserIds}\nstandUser:\n${event?.roomData.toJson()}");

        _idLock.synchronized(() {
          showingUserIds.entries.forEach((element) {
            List<YBDTpRoomDataUser?> users = event?.roomData.users?.where((curUser) => element.key == curUser!.id).toList() ?? [];
            YBDTpRoomDataUser? user;
            if (users.isNotEmpty) {
              user = users.first;
            }
            if (user == null || (user.isStandUp ?? false)) {
              dismissAtIndex(element.value);
              showingUserIds.remove(element.key);
            }
          });
        });
        // event?.roomData?.standUpUser?.forEach((user) {
        //   dismissAtIndex(user.beforePosition);
        // });
      }
    }));
  }
  void initState7KbL6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDBubbleLayer oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    super.build(context);
    return Container();
  }
  void myBuildZ1ayLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDBubbleController {
  late YBDBubbleLayerState _bubbleLayerState;

  void bindState(YBDBubbleLayerState state) {
    _bubbleLayerState = state;
  }

  void showTextAtIndex(int index, int userId, YBDPlainText text) {
    _bubbleLayerState.showTextAtIndex(index, userId, text);
  }

  void dismissAtIndex(int index) {
    _bubbleLayerState.dismissAtIndex(index);
  }
}
