
import 'package:collection/collection.dart' show IterableNullableExtension;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/deal_ybd_cards.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_player_avatar.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_playing.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_commom_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';

class YBDTPCardController extends StatefulWidget {
  const YBDTPCardController({Key? key}) : super(key: key);

  @override
  _YBDTPCardControllerState createState() => _YBDTPCardControllerState();
}

class _YBDTPCardControllerState extends BaseState<YBDTPCardController> {
  YBDDealController dealController = YBDDealController(); // 看牌，弃牌
  YBDTPRoomGetLogic logic = Get.find<YBDTPRoomGetLogic>();
  int? term = -1; // 第几轮

  @override
  void initState() {
    super.initState();
    logicListen();
  }
  void initStateF2mO8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void logicListen() {
    addSub(logic.stateStream.listen((YBDTPRoomGetState? logicState) {
      /*
      * 发牌相关处理
      * 1. 即将开始，洗牌（包含定庄）
      * 2. 发牌（包含下注）
      * 3. 游戏中(包含弃牌，开牌)
      * 4. 状态操作（比牌，弃牌）
      * 5. 结算（开牌动画结束之后，全部弃牌）
      * 6. 结束
      * */
      int status = logicState!.roomData.battle?.status ?? 0;
      int battleId = logicState.roomData.battle?.id ?? -1;
      logger.v('logic.stateStream.listen.status: $status, battleId:$battleId');

      /// 开始发牌
      if (status == 2 && !logicState.playedCards) {
        logicState.playedCards = true;
        // 发牌倒计时UI type == 3， 后面改成只出现在status=1的状态下
        YBDTpRoomDataAnimation? countdownAnimation =
            logicState.roomData.animation?.firstWhereOrNull((YBDTpRoomDataAnimation? e) => e!.sync == 1 && e.type == 3);
        if (countdownAnimation != null) {
          // 重新开局了，收回所有的牌
          dealController.handCardsAllPack();
        }
        YBDTpRoomDataAnimation? dingzhuangAnimation =
            logicState.roomData.animation?.firstWhereOrNull((YBDTpRoomDataAnimation? e) => e!.sync == 1 && e.type == 4);
        logger
            .v('countdownAnimation:${countdownAnimation?.timeout},dingzhuangAnimation:${dingzhuangAnimation?.timeout}');
        // 开始发牌
        if (dingzhuangAnimation != null && dingzhuangAnimation.timeout! > 1000) {
          Future<void>.delayed(Duration(milliseconds: countdownAnimation?.timeout ?? 0), () async {
            if (!mounted) return;
            // 跑庄
            eventBus.fire(YBDTPRoomRunBankerEvent()..start = true);
            YBDTPAudio.choose.play();
          });

          // 发牌完成，用户可以开始下注动效，操作按钮状态变更
          void playedHand() {
            int getStatus = YBDTPRoomGetLogic.findSync()?.state?.roomData.battle?.status ?? 0;
            int getBattleId = YBDTPRoomGetLogic.findSync()?.state?.roomData.battle?.id ?? -1;
            logger.v('playedHand status:$status, getStatus:$getStatus, battleId:$battleId, getBattleId:$getBattleId');
            if (getStatus == 2 && battleId == getBattleId) {
              // 延迟操作需要确保是同一局，且在游戏中

              logicState.canCountdown = true;
              // 消化掉了，避免其他地方刷新再次使用了
              logicState.roomData.animation = null;
              logic.update();

              /// 到自己了
              if (logicState.roomData.battle?.currentOperator == logicState.curUserId &&
                  logicState.roomData.battle?.status == 2 &&
                  term != logicState.roomData.battle?.term) {
                term = logicState.roomData.battle?.term;
                YBDTPAudio.yourTurn.play();
              }
            }
          }

          // 延迟执行发牌的动效
          Future<void>.delayed(Duration(milliseconds: (countdownAnimation?.timeout ?? 0) + 1000), () async {
            if (!mounted) return;
            // logicState.playedCards = true;
            logic.update(<String>[YBDTPRoomGetState.dingzhuangIdentifier]);
            YBDTPAudio.dingzhuang.play();
            dealController.replayCards(cardsEndCoordinates(logic), playedCallback: () {
              logger.v(
                  'played dingzhuangAnimation:${dingzhuangAnimation.timeout}, canCountdown:${logicState.canCountdown}');
              if (!logicState.canCountdown) playedHand();
            });
            // 发牌时间已经确定，延迟对应时间更新状态
          });
          // int addTimeout = 2500;
          // Future<void>.delayed(Duration(milliseconds: (dingzhuangAnimation.timeout ?? 0) + addTimeout), () {
          //   logger.v('played dingzhuangAnimation check:${dingzhuangAnimation?.timeout}');
          //   if (!logicState.canCountdown) {
          //     // playedHand();
          //   }
          // });
        } else {
          // 已经开始了, 那就发好牌了
          // logicState.playedCards = true;
          if (!logicState.canCountdown) {
            logicState.canCountdown = true;
            logic.update();
          }
          term = logicState.roomData.battle?.term;
          logger.v('updateEndCoordinates played:${logicState.playedCards},dealController:$dealController');
          dealController.updateEndCoordinates(cardsEndCoordinates(logic), true);
        }
      } else if (status == 4 || status == 1) {
        /// 等待发牌
        logicState.playedCards = false;
        logicState.canCountdown = false;
        term = -1;
        if (status == 1) {
          // 重新开局了，收回所有的牌
          dealController.handCardsAllPack();

          Future<void>.delayed(Duration(milliseconds: 2000), () {
            int getStatus = YBDTPRoomGetLogic.findSync()?.state?.roomData.battle?.status ?? 0;
            logger.v(
                'handCardsAllPack timeout check status:$status, getStatus:$getStatus');
            if (getStatus == 1) {
              // 游戏进入等待中，延迟2s再次检测牌，避免牌没有弃完全
              dealController.handCardsAllPack();
            }
          });

        }
        if (status == 4) {
          Future<void>.delayed(Duration(milliseconds: 3000), () {
            int curStatus = logicState.roomData.battle?.status ?? 0;
            int getStatus = YBDTPRoomGetLogic.findSync()?.state?.roomData.battle?.status ?? 0;
            int getBattleId = YBDTPRoomGetLogic.findSync()?.state?.roomData.battle?.id ?? -1;
            logger.v(
                'handCardsAllPack timeout status:$status, curStatus:$curStatus, getStatus:$getStatus, battleId:$battleId, getBattleId:$getBattleId');
            if (getStatus == 4 && (battleId == getBattleId || getBattleId == -1)) {
              // 已经结束游戏了，3s后还没开始，收回所有的牌(同一局结束了，或暂无新局了)
              dealController.handCardsAllPack();
            }
          });
        }
      }

      // 我又坐下了，需要更新卡牌的相对坐标(只能是观众了)
      if (status == 2
          && !logicState.currentUser!.isStandUp!
          && logicState.currentUser!.role == 0
          && logicState.currentUser!.id != null) {

        List<YBDEndCoordinates> newEnds = cardsEndCoordinates(logic);
        // reduce 方法需要数组中必须有对象
        if (newEnds.length > 0) {
          List<YBDEndCoordinates> preEnds = dealController.endCoordinates();
          // 如果跟上次座位一样，那就不需要更新座位了。
          bool allEqual = true;
          //     newEnds.reduce((YBDEndCoordinates value,
          //     YBDEndCoordinates element) =>
          // value
          //   ..allEqual = preEnds.contains(element)).allEqual;
          newEnds.forEachIndexValue((int index, YBDEndCoordinates element) {
            if (preEnds[index] != element) {
              allEqual = false;
            }
          });
          logger.v(
              'logic.stateStream.listen.status.currentUser.isStandUp! ${newEnds
                  .length}, ${preEnds.length} $allEqual');
          if (!allEqual)
            dealController.onlyUpdateEndCoordinatesToSetState(newEnds);
        }
      }


      /// 到自己了, 播过了，就不播了
      if (logicState.roomData.battle?.currentOperator == logicState.curUserId &&
          status == 2 &&
          logicState.playedCards &&
          term != logicState.roomData.battle?.term) {
        term = logicState.roomData.battle?.term;
        YBDTPAudio.yourTurn.play();
      }

      /// 自己看牌了
      YBDTpRoomDataUser user = logicState.currentUser!;
      logger.v(
          'logic.stateStream.listen.status.user: ${user.cardState}, ${user.cards?.length},bankerFirstUsers:${logic.state!.bankerFirstUsers?.length}');
      if (user.cardState == 1 && user.cards != null && logic.state!.bankerFirstUsers != null) {
        // 庄家相对我所在的位置
        int cardIndex = logic.state!.bankerFirstUsers!.indexWhere((YBDTpRoomDataUser? element) => element!.id == user.id);
        dealController.handCardsSee(hand: cardIndex, cards: user.cards);
      }

      /// 比牌可以看到对方的牌，不需要做动画,
      bool compared = (logic.state!.roomData.battle?.subStatus ?? 0) == 1;
      if (compared) {
        YBDTpRoomDataUser? sideCardUser = logicState.roomData.users?.firstWhereIndex((int index, YBDTpRoomDataUser? element) {
          bool contain = element!.cardState == 3 && element.id != user.id && element.cards != null;
          return contain;
        });
        if (sideCardUser != null && logic.state!.bankerFirstUsers != null) {
          int cardIndex =
              logic.state!.bankerFirstUsers!.indexWhere((YBDTpRoomDataUser? element) => element!.id == sideCardUser.id);
          dealController.handCardsSee(
            hand: cardIndex,
            cards: sideCardUser.cards,
            animation: false,
          );
        }
      }

      /// 自己比牌中，需要添加蒙版
      // if (user.cardState == 3 && user.cards != null && logic.state.bankerFirstUsers != null) {
      //   // 庄家相对我所在的位置
      //   int cardIndex = logic.state.bankerFirstUsers.indexWhere((YBDTpRoomDataUser element) => element.id == user.id);
      //   dealController.handCardsSee(
      //       hand: cardIndex,
      //       cards: user.cards
      //   );
      // }
      logger.v('logic.stateStream.listen.handCardComparedMask userCardState: ${user.cardState}');

      /// 开牌了，加个蒙版
      if (user.cardState == 4 && user.cards != null && logic.state!.bankerFirstUsers != null) {
        // 庄家相对我所在的位置
        int cardIndex = logic.state!.bankerFirstUsers!.indexWhere((YBDTpRoomDataUser? element) => element!.id == user.id);
        logger.v('logic.stateStream.listen.handCardComparedMask: $cardIndex');
        if (cardIndex >= 0) {
          dealController.handCardComparedMask(
            hand: cardIndex,
            cards: user.cards,
          );
        }
      }

      /// 弃牌
      bool hasPackedAnimation = false;
      logicState.roomData.animation?.forEach((YBDTpRoomDataAnimation? animation) {
        if (animation?.type == 2 && logic.state!.bankerFirstUsers != null) {
          hasPackedAnimation = true;
          int cardIndex =
              logic.state!.bankerFirstUsers!.indexWhere((YBDTpRoomDataUser? element) => element!.id == animation!.operator);
          if (cardIndex >= 0) {
            logger.v('dealController.handCardsPack cardIndex: $cardIndex, packed:${dealController.handCardPacked(cardIndex)}');
            if (!dealController.handCardPacked(cardIndex)) {
              dealController.handCardsPack(
                hand: cardIndex,
              );
              YBDTPAudio.pack.play();
            }
          }
        }
      });

      /// 检测弃牌的, 自动弃牌
      // logicState.roomData.users.forEachIndexValue((int index, YBDTpRoomDataUser value) {
      //   if ((value.cardState == 2 || value.role == 0) && logic.state.bankerFirstUsers != null) {
      //     int cardIndex = logic.state.bankerFirstUsers.indexWhere((YBDTpRoomDataUser element) => element.id == value.id);
      //     logger.v('dealController.handCardsPack auto index: $cardIndex');
      //     if (cardIndex >= 0) {
      //       dealController.handCardsPack(
      //         hand: cardIndex,
      //       );
      //     }
      //   }
      // });

      /// 在玩家里面检测，自动弃牌，避免没有收到弃牌的动画报文
      if (!hasPackedAnimation) {
        logic.state!.bankerFirstUsers?.forEachIndexValue((int index, YBDTpRoomDataUser? value) {
          YBDTpRoomDataUser? player = logic.state!.roomData.users!.firstWhereOrNull((YBDTpRoomDataUser? element) => element!.id == value!.id);
          if (dealController.handCardPacked(index)) {
            return;
          }
          if (player == null) {
            // 已经不在座位了，直接弃牌
            logger.v('dealController.handCardsPack auto null index: $index');
            dealController.handCardsPack(
              hand: index,
            );
          }else if (player.role == 0 || player.cardState == 2) {
            // 已经弃牌了，站起坐下，变成用户了
            logger.v('dealController.handCardsPack auto player index: $index');
            dealController.handCardsPack(
              hand: index,
            );
          }
        });
      }


    }));
  }
  void logicListenlTvSWoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Stack(
      children: <Widget>[
        /// 发牌
        dealCards(),
      ],
    );
  }
  void myBuild1Nikyoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 发牌：
  Widget dealCards() {
    return YBDDealCards(
        cardHeight: YBDTPAvatarSize.cardSize().height,
        cardWidth: YBDTPAvatarSize.cardSize().width,
        startTop: 198.dp750,
        controller: dealController);
  }
  void dealCardsSbzAkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 牌的位置坐标计算
  List<YBDEndCoordinates> cardsEndCoordinates(YBDTPRoomGetLogic? logic) {
    logic = YBDTPRoomGetLogic.findSync();
    // 这里必须庄家排在第一，先发牌
    List<YBDTpRoomDataUser?>? cardUsers = logic?.state?.bankerFirstUsers;

    if (cardUsers == null) return <YBDEndCoordinates>[];
    logger.v('cardsEndCoordinates bankerFirstUsers:${cardUsers.length}');

    List<Offset>? cardOffsets = YBDTPAvatarSize.cardOffsets(roomPlayerHeight());

    return cardUsers
        .mapIndexValue((int index, YBDTpRoomDataUser? user) {
          logger
              .v('cardsEndCoordinates userid:${user!.id},cardState:${user.cardState},battleId:${logic!.state!.battleId}');

          YBDEndCoordinates end = YBDEndCoordinates(top: -100.0, left: -100.0, opacity: 0.0);
          if (user.id == null || user.role == 0 || user.cardState == 2) {
            // 空座位, 或不是玩家, 或弃牌了
            return end;
          }
          // 庄家相对我所在的位置
          int cardIndex = -1;//logic.state.roomData.users.indexWhere((YBDTpRoomDataUser element) => element.id == user.id);
          logic.state!.roomData.users!.firstWhereIndex((int index, YBDTpRoomDataUser? element) {
            if (element!.id == user!.id) {
              cardIndex = index;
              user = element;
              return true;
            }
            return false;
          });
          logger.v('cardsEndCoordinates: cardIndex:$cardIndex, cardUser.role:${user!.role}, user.cardState:${user!.cardState}');
          if (cardIndex < 0 || user!.role == 0 || user!.cardState == 2) return end;
          double top = cardOffsets[cardIndex].dy;
          double left = cardOffsets[cardIndex].dx;
          double scale = 1.0; // 缩放大小
          int rotate = 10; // 旋转角度
          double space = 12.dp750 as double;
          double spaceY = 4.dp750 as double;
          String horizontalSpeed = YBDGameCommomUtil.getGameConfig().cardSpeed.split('/').first;
          String mainSpeed = YBDGameCommomUtil.getGameConfig().cardSpeed.split('/').last;
          Duration duration = Duration(milliseconds: YBDNumericUtil.stringToInt(horizontalSpeed)); // 动画时间
          if (cardIndex == 0) {
            // 中间
            scale = 1.6;
            rotate = 10;
            space = 35.dp750 as double;
            spaceY = 6.dp750 as double;
          }
          if (top > roomPlayerHeight() / 2.0) {
            // 下边
            duration = Duration(milliseconds: YBDNumericUtil.stringToInt(mainSpeed));
          }

          logger.v('cardsEndCoordinates top:$top, left:$left, duration: $duration,scale: $scale');
          return YBDEndCoordinates(
              top: top,
              left: left,
              endAngle: 0,
              scale: scale,
              rotate: rotate,
              cards: user!.cards,
              opacity: 1.0,
              space: space,
              spaceY: spaceY,
              duration: duration);
        })
        .whereNotNull()
        .toList();
  }
  void cardsEndCoordinates7l4Oloyelive(YBDTPRoomGetLogic? logic) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
