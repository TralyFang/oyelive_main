
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';

class YBDBubbleSheet extends StatelessWidget {
  Map quickComment;
  Function? onSelected;
  static show(Map quickComments, {void onSelected(String key)?}) {
    showModalBottomSheet(
        backgroundColor: Color(0xff20003A).withOpacity(0.8),
        barrierColor: Colors.transparent,
        context: Get.context!,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.px),
        ),
        builder: (x) => YBDBubbleSheet(
              quickComments,
              onSelected: onSelected,
            ));
  }

  YBDBubbleSheet(this.quickComment, {this.onSelected(String index)?});

  @override
  Widget build(BuildContext context) {
    List qCommentList = quickComment.entries.map((entry) => entry.value).toList();
    return Padding(
      padding: EdgeInsets.only(top: 38.px, left: 34.px, right: 34.px, bottom: 58.px),
      child: SizedBox(
        height: 270.px,
        child: GridView.builder(
            itemCount: quickComment.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 20.px,
              crossAxisSpacing: 20.px,
              childAspectRatio: 317 / 61,
            ),
            itemBuilder: (b, x) {
              return GestureDetector(
                onTap: () {
                  YBDCommonTrack().commonTrack(
                      YBDTAProps(location: 'chat_send', module: YBDTAModule.TPGO, extParams: {'content': qCommentList[x]}));
                  YBDNavigatorHelper.popPage(context);
                  onSelected?.call(
                      quickComment.keys.firstWhereOrNull((k) => quickComment[k] == qCommentList[x]));
                },
                child: Container(
                  width: 317.px,
                  height: 61.px,
                  decoration: BoxDecoration(
                      border: Border.all(color: Color(0xffFFE1A1), width: 1.px),
                      borderRadius: BorderRadius.all(Radius.circular(15.px))),
                  child: Center(
                    child: Text(
                      qCommentList[x],
                      style: TextStyle(color: Colors.white, fontSize: 27.sp),
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }
  void buildEbfsgoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
