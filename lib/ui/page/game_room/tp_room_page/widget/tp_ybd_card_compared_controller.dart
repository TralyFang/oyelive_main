import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oye_tool_package/widget/should_rebuild.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_card_compared.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_player_avatar.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_playing.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

class YBDTPCardComparedController extends StatefulWidget {
  const YBDTPCardComparedController({Key? key}) : super(key: key);

  @override
  _YBDTPCardComparedControllerState createState() =>
      _YBDTPCardComparedControllerState();
}

class _YBDTPCardComparedControllerState
    extends BaseState<YBDTPCardComparedController> {

  YBDTPRoomGetLogic logic = Get.find<YBDTPRoomGetLogic>();
  bool lastCompared = false;

  @override
  Widget myBuild(BuildContext context) {
    logger.v('YBDTPCardComparedController myBuild');

    /// 比牌的状态展示
    // return YBDGameWidgetUtil.streamBuilder(comparedCardStatus());
    return GetBuilder<YBDTPRoomGetLogic>(builder: (YBDTPRoomGetLogic logic) {
      return ShouldRebuild<Widget>(
          shouldRebuild: (Widget oldWidget, Widget newWidget) {
            // 比牌中
            bool compared = (logic.state!.roomData.battle?.subStatus ?? 0) == 1;
            bool rebuild = compared != lastCompared;
            logger.v(
                'YBDTPCardComparedController ShouldRebuild:$compared, lastCompared:$lastCompared');
            if (rebuild) lastCompared = compared;
            return rebuild;
          },
          child: YBDGameWidgetUtil.streamBuilder(comparedCardStatus()));
    });
  }
  void myBuildQyqQGoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 区域高度
  double get playerHeight => roomPlayerHeight();


  /// 有人要看我的牌
  Widget someOneSeeMyCard() {
    // 比牌中
    bool compared = (logic.state!.roomData.battle?.subStatus ?? 0) == 1;
    if (!compared) return Container();
    if (logic.state!
        .currentUser!
        .cardState == 3) {
      // 我在比牌中，需要展示一下对方的眼睛
      int index = logic.state!.roomData.users!.lastIndexWhere((
          YBDTpRoomDataUser? element) =>
      element!.cardState == 3 && element.id != logic.state!.curUserId);
      if (index > 0) {
        Rect rect = YBDTPAvatarSize.someOneSeeMyCardRects(playerHeight)[index];
        return Positioned(
            left: rect.left,
            top: rect.top,
            child: YBDTPAnimatedScale(
              child: YBDGameWidgetUtil.assetTPImage(
                'tp_card_eye',
                width: rect.width,
                height: rect.height,),
            ));
      }
    }
    return Container();
  }
  void someOneSeeMyCard9SdWUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 我参与了比牌
  bool isMeCompared() {
    return logic.state!
        .currentUser!
        .cardState == 3;
  }

  /*
  * 没有比牌-Container
  * 有比牌-有我-eye-compared
  * 有比牌-没我-compared
  * */

  /// 比牌的状态展示
  Stream<Widget> comparedCardStatus() async* {
    // 比牌中
    bool compared = (logic.state!.roomData.battle?.subStatus ?? 0) == 1;
    if (!compared) yield Container();

    if (isMeCompared()) {
      logger.v('comparedCardStatus isMeCompared');
      yield someOneSeeMyCard();
      await Future<void>.delayed(Duration(milliseconds: 500));
    }
    yield comparedCardResult();
  }
  void comparedCardStatusqipzkoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget? _comparedCardAnimation;

  Widget comparedCardResult() {
    // // 有比牌状态的用户
    YBDTpRoomDataUser? comparedCardUser = logic.state!.roomData.users
        ?.firstWhereIndex((int index, YBDTpRoomDataUser? element) =>
    element!.cardState == 3);
    if (comparedCardUser == null) {
      _comparedCardAnimation = null;
      return Container();
    }

    logger.v(
        'comparedCardResult comparedCardUser:$comparedCardUser, _comparedCardAnimation:${_comparedCardAnimation !=
            null}');
    // 避免重复刷新导致的逻辑重复
    if (_comparedCardAnimation != null) {
      return _comparedCardAnimation!;
    }

    List<Rect> rects = YBDTPAvatarSize.comparedCardStatusRects(playerHeight);
    List<Widget> widgets = rects.mapIndexValue((int index, Rect? rect) {
      if (logic.state!.roomData.users != null
          && logic.state!.roomData.users!.length > index) {
        YBDTpRoomDataUser user = logic.state!.roomData.users![index]!;
        bool isWin = logic.state!.roomData.battle?.winners?.contains(user.id) ??
            false;
        if (user.cardState == 3) { // 比牌状态
          if (user.id == logic.state!.curUserId) { // 我参数了比牌结果
            isWin ? YBDTPAudio.win.play() : YBDTPAudio.lose.play();
          }
          logger.v('comparedCardStatus isWin:$isWin, userId:${user.id}');
          return Positioned(
              left: rect!.left,
              top: rect.top,
              child: YBDTPCardCompared(
                  bgSize: rect.size,
                  isWin: isWin,
                  isSelf: user.id == logic.state!.curUserId));
        }
      }
      return Container();
    }).toList();
    _comparedCardAnimation = Stack(children: widgets);
    return _comparedCardAnimation!;
  }
  void comparedCardResultltHwzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
