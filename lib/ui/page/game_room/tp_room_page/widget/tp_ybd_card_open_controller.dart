import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oye_tool_package/widget/should_rebuild.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/i18n_ybd_key.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_card_open_widget.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_player_avatar.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_playing.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

class YBDTPCardOpenController extends StatefulWidget {
  const YBDTPCardOpenController({Key? key}) : super(key: key);

  @override
  _YBDTPCardOpenControllerState createState() => _YBDTPCardOpenControllerState();
}

class _YBDTPCardOpenControllerState extends BaseState<YBDTPCardOpenController> {

  YBDTPRoomGetLogic logic = Get.find<YBDTPRoomGetLogic>();
  int lastStatus = 0;

  // @override
  // void initState() {
  //   super.initState();
  //   logicListen();
  // }
  //
  // void logicListen() {
  //   addSub(logic.stateStream.listen((YBDTPRoomGetState logicState) {
  //     int status = logicState.roomData?.battle?.status ?? 0;
  //     if (status != lastStatus) {
  //       logger.v('YBDTPCardOpenController listen status:$status');
  //       setState(() {});
  //       lastStatus = status;
  //     }
  //
  //   }));
  // }

  @override
  Widget myBuild(BuildContext context) {
    logger.v('YBDTPCardOpenController myBuild lastStatus:$lastStatus');

    /// 开牌展示结果信息
    // return YBDGameWidgetUtil.streamBuilder(settleOpenCards());
    return GetBuilder<YBDTPRoomGetLogic>(builder: (YBDTPRoomGetLogic logic) {
      return ShouldRebuild<Widget>(
          shouldRebuild: (Widget oldWidget, Widget newWidget) {
            int status = logic.state!.roomData.battle?.status ?? 0;
            bool rebuild = status != lastStatus;
            logger.v(
                'YBDTPCardOpenController ShouldRebuild:$status, lastStatus:$lastStatus');
            if (rebuild) lastStatus = status;
            return rebuild;
          },
          child: YBDGameWidgetUtil.streamBuilder(settleOpenCards()));
    });
  }
  void myBuildyS7uGoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 区域高度
  double get playerHeight => roomPlayerHeight();

  /// 结算开牌了
  Stream<Widget> settleOpenCards() async* {
    // 有开牌状态
    int status = logic.state!.roomData.battle?.status ?? 0;
    if (status != 4) yield Container();
    if (isLastSettleOpen()) {
      yield lastSettleTips();
      await Future<void>.delayed(Duration(milliseconds: 600));
    }
    logger.v('settleOpenCards status:$status, lastStatus:$lastStatus');
    yield Container(
      // yield ShouldRebuild<Widget>(
      //     shouldRebuild: (Widget oldWidget, Widget newWidget) {
      //       bool rebuild = status != lastStatus;
      //       logger.v('settleOpenCards ShouldRebuild:$status, lastStatus:$lastStatus');
      //       lastStatus = status;
      //       return rebuild;
      //     },
        child: openCards(logic));
  }
  void settleOpenCardsBy5tkoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是否是最后一轮下注的结算开牌
  bool isLastSettleOpen() {
    int action = logic.state!.roomData.battle?.finishBeforeAction ?? 0;
    if (action == 1) {
      return true;
    }
    return false;
  }
  void isLastSettleOpenzGAIwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 最后结算提示
  Widget lastSettleTips() {
    return Align(
        alignment: Alignment(0.0, -300.dpH1136 / playerHeight / 2.0),
        child: Container(
          color: Colors.black.withOpacity(0.6),
          width: SizeNum.screentWidth,
          height: 140.dp750,
          child: Center(child: YBDGameWidgetUtil.textSingleRegular(
              YBDI18nKey.potLimitReached, fontSize: 32.sp750),),
        ));
  }
  void lastSettleTipsQaYvgoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget? _openCardAnimation;

  /// 开牌：开牌位置
  Widget openCards(YBDTPRoomGetLogic logic) {
    // 有开牌状态
    YBDTpRoomDataUser? hasOpenCard = logic.state!.roomData.users?.firstWhereIndex((
        int index,
        YBDTpRoomDataUser? element) => element!.cardState == 4);
    if (hasOpenCard == null) {
      _openCardAnimation = null;
      return Container();
    }

    logger.v(
        'openCards hasOpenCard:$hasOpenCard, _openCardAnimation:${_openCardAnimation !=
            null}');
    // 避免重复刷新导致的逻辑重复
    if (_openCardAnimation != null) {
      return _openCardAnimation!;
    }
    List<Alignment> aligments = YBDTPAvatarSize.openCardAligns(playerHeight);

    _openCardAnimation = Container(
      child: Stack(
          children: aligments.mapIndexValue((int index, Alignment value) {
            if (logic.state!.roomData.users != null
                && logic.state!.roomData.users!.length > index) {
              YBDTpRoomDataUser user = logic.state!.roomData.users![index]!;
              bool isWin = logic.state!.roomData.battle?.winners?.contains(
                  user.id) ??
                  false;
              if (user.cards != null && user.cards!.length > 0 &&
                  user.cardState == 4) {
                // 开牌状态 有牌才展示
                if (user.id == logic.state!.curUserId) { // 我参数了比牌结果
                  isWin ? YBDTPAudio.win.play() : YBDTPAudio.lose.play();
                }
                logger.v('openCards isWin:$isWin, userId:${user.id}');
                return Align(
                    alignment: value,
                    child: YBDTPCardOpenWidget(
                        user: user,
                        curUserId: logic.state!.curUserId,
                        isWin: isWin));
              }
            }
            return Container();
          })),
    );
    return _openCardAnimation!;
  }
  void openCardshEEu6oyelive(YBDTPRoomGetLogic logic) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
