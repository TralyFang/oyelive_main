
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oye_tool_package/widget/pies_time_widget.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/common/web_socket/socket_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_lobby/widget/dialog_ybd_invite.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_state.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_gradient_dialog.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

enum TPKingPosition { topRight, topLeft, centerRight }

/// 玩家头像+昵称
class YBDTPPlayerAvatar extends StatefulWidget {
  YBDTpRoomDataUser? user; // 座位用户信息
  int? curUserId; // 当前登录的用户
  YBDTpRoomDataBattle? battle; // 对局信息
  GlobalKey? avatarKey; // 头像的key
  bool runBanker; // 跑庄
  TPKingPosition kingPosition; // 庄家标识的位置
  Function? clickEmptySeat; // 点击空座位的回调
  int? leaderId; // 房主ID

  YBDTPPlayerAvatar({
    this.user,
    this.curUserId,
    this.battle,
    this.avatarKey,
    this.runBanker = false,
    this.kingPosition = TPKingPosition.centerRight,
    this.clickEmptySeat,
    this.leaderId,
  });

  @override
  State<YBDTPPlayerAvatar> createState() => _YBDTPPlayerAvatarState();
}

class _YBDTPPlayerAvatarState extends BaseState<YBDTPPlayerAvatar> with TickerProviderStateMixin {
  YBDTPAvatarSize get sizeItem => YBDTPAvatarSize(isSelf: widget.user?.id == widget.curUserId, user: widget.user);

  // 有玩家, 且不是站起的状态
  bool get hasPlayer => widget.user?.id != null && !(widget.user?.isStandUp ?? false);
  YBDTPRoomGetLogic logic = Get.find<YBDTPRoomGetLogic>();

  @override
  void initState() {
    super.initState();
  }
  void initStateMMPfAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Container(
      // decoration: BoxDecoration(border: Border.all(color: Colors.blue.withOpacity(0.8), width: 2)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment:
            widget.kingPosition == TPKingPosition.topLeft ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          // 昵称+皇冠
          nicknameWidget(),
          // 头像
          playerInfoWidget(),
        ],
      ),
    );
  }
  void myBuilddfK8noyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 昵称+皇冠
  Widget nicknameWidget() {
    if (widget.curUserId != widget.user!.id) {
      Container nickname = Container(
        // color: Colors.yellow.withOpacity(0.3),
        width: sizeItem.avatarWidth,
        child: YBDGameWidgetUtil.textSingleRegular(widget.user?.nickname ?? '', textAlign: TextAlign.center),
      );
      Container empty = Container(width: sizeItem.avatarWidth);

      return GetBuilder<YBDTPRoomGetLogic>(
          id: YBDTPRoomGetState.dingzhuangIdentifier,
          builder: (YBDTPRoomGetLogic logic) {
            bool isMeInTheGame = (logic.state?.roomData.battle?.players ?? []).contains(widget.curUserId);
            bool donNotShow = !isMeInTheGame && widget.kingPosition == TPKingPosition.centerRight;
            // role=2 为庄家
            if (!logic.state!.playedCards) {
              return nickname;
            }
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (widget.kingPosition == TPKingPosition.topLeft)
                  Padding(
                    padding: EdgeInsets.only(right: 0),
                    child: Row(children: [_bottomOwner(), kingAnimation()]),
                  ),
                // nickname,
                donNotShow ? empty : nickname,
                if (widget.kingPosition == TPKingPosition.topRight)
                  Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Row(children: [_bottomOwner(), kingAnimation()]),
                  ),
              ],
            );
          });
    }

    return Container(width: 0, height: 0);
  }
  void nicknameWidgetJ6vumoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 没有用户的默认座位
  Widget defaultAddAvatar() {
    return Container(
      margin: EdgeInsets.all((sizeItem.maxHeight - sizeItem.defalutAvatarWidth) / 2.0),
      child: Opacity(
        opacity: 0.8,
        child: Stack(
          children: [
            YBDGameWidgetUtil.assetTPImage(
              'unknown_user2',
              width: sizeItem.defalutAvatarWidth,
              isWebp: false,
              height: sizeItem.defalutAvatarWidth,
              onTap: () {
                // 邀请好友
                logger.v('22.10.25----inviter id:${widget.curUserId}');
                YBDTpRoomDataUser? curUser = Get.find<YBDTPRoomGetLogic>().state!.currentUser;
                bool isPlayer = widget.battle!.players?.contains(widget.curUserId) ?? false;
                // 点击加号---我在座位上->邀请
                if (isPlayer) {
                  YBDCommonTrack().tpgo('+');
                  YBDInviteDialog.show(curUser, Get.find<YBDTPRoomGetLogic>().state!.roomData.battle!.roomId);
                } else {
                  // 我不在座位上->上座
                  widget.clickEmptySeat!.call();
                }
              },
            ),
          ],
        ),
      ),
    );
  }
  void defaultAddAvatarbo2csoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget playerInfoWidget() {
    return Container(
      width: sizeItem.maxWidth,
      height: sizeItem.maxHeight,
      // decoration: BoxDecoration(border: Border.all(color: Colors.black12.withOpacity(0.8), width: 2)),
      child: Stack(
        alignment: widget.kingPosition == TPKingPosition.topLeft
            ? AlignmentDirectional.centerEnd
            : AlignmentDirectional.centerStart,
        children: [
          hasPlayer ? playerAvatar() : defaultAddAvatar(),
          // 庄家标识
          kingWidget(),
        ],
      ),
    );
  }
  void playerInfoWidgetcUJFuoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 玩家头像
  Widget playerAvatar() {


    String iframeUrl = getImgUrl(widget.user!.iframe);
    logger.v('22.10.20--avatarKey:${widget.avatarKey}, iframeUrl:$iframeUrl');

    return Container(
      width: sizeItem.haloWidth,
      height: sizeItem.haloWidth,
      margin: EdgeInsets.all((sizeItem.maxHeight - sizeItem.haloWidth) / 2.0),
      // decoration: BoxDecoration(border: Border.all(color: Colors.yellow.withOpacity(0.8), width: 2)),
      child: Stack(
        alignment: AlignmentDirectional.centerStart,
        children: [
          Container(
            padding: EdgeInsets.all(15.dp750 as double),
            child: Stack(
              children: [
                // 玩家头像
                ClipRRect(
                  key: widget.avatarKey,
                  borderRadius: BorderRadius.circular(sizeItem.avatarWidth / 2.0),
                  child: Container(
                    width: sizeItem.avatarWidth,
                    height: sizeItem.avatarWidth,
                    child: Stack(
                      children: [
                        YBDGameWidgetUtil.assetTPImage(
                            widget.user?.avatarUrl() ??
                                YBDResourcePathUtil.defaultMaleAssets(male: widget.user?.isMale() ?? true),
                            width: sizeItem.avatarWidth,
                            height: sizeItem.avatarWidth,
                            fit: BoxFit.cover,
                            placeholder:
                                Image.asset(YBDResourcePathUtil.defaultMaleAssets(male: widget.user?.isMale() ?? true)),
                            onTap: () {
                          logger.v('YBDTPPlayerAvatar player tap');
                        }),
                        // 玩家状态
                        playerStatusWidget(),
                        // 弃牌添加灰色蒙版
                        if ((widget.user?.cardState ?? 0) == 2)
                          Container(
                            color: Colors.black.withOpacity(0.4),
                          ),
                        // 链接状态
                        if (widget.user!.id == widget.curUserId)
                          StreamBuilder(
                            stream: YBDTPGameSocket.instance.statusStream,
                            builder: (BuildContext context, AsyncSnapshot<SocketStatus> snapshot) {
                              if (snapshot.data == SocketStatus.TryConnecting || snapshot.data == SocketStatus.Close) {
                                return Container(
                                  color: Colors.black.withOpacity(0.4),
                                  child: Center(
                                    child: YBDGameWidgetUtil.textSingleRegular('Connecting...'),
                                  ),
                                );
                              }
                              return Container();
                            },
                          ),
                      ],
                    ),
                  ),
                ),

                // 花边 2.7.4 deprecated
                // if (iframeUrl.isEmpty)
                //   YBDGameWidgetUtil.assetTPImage(
                //     'mic_frame',
                //     width: sizeItem.avatarWidth,
                //     height: sizeItem.avatarWidth,
                //   ),

                // 倒计时
                peisTimeWidget(),
              ],
            ),
          ),
          // 光环
          GetBuilder<YBDTPRoomGetLogic>(
              id: YBDTPRoomGetState.dingzhuangIdentifier,
              builder: (YBDTPRoomGetLogic logic) {
                if ((widget.user!.role == 2 && widget.battle?.status == 2 && logic.state!.playedCards) ||
                    (widget.runBanker))
                  return YBDTPAvatarHalo(
                    width: sizeItem.haloWidth,
                    height: sizeItem.haloWidth,
                    breathNum: widget.user!.role == 2 ? 6 : 1,
                  );
                return Container();
              }),
          // 玩家头像框
          Transform.scale(
            scale: 1.4,
            child: Padding(
              padding: EdgeInsets.only(left: 13.dp750 as double, top: 3.dp750 as double),
              child: YBDNetworkImage(
                // http://oyevoice-test.s3.ap-south-1.amazonaws.com/image/13/1663142088614_160.png
                // http://oyevoice-test.s3.ap-south-1.amazonaws.com/image/13/e9bd25e3-ea00-4401-9cbe-430ae84ff9b0.gif
                // http://oyevoice-test.s3.ap-south-1.amazonaws.com/image/13/d6c9a435-d7e9-44e9-8bb8-ee7594d1f21e.svga
                // http://oyevoice-test.s3.ap-south-1.amazonaws.com/image/13/d6c9a435-d7e9-44e9-8bb8-ee7594d1f21e.svga
                imageUrl: iframeUrl, // 替换为边框路径，兼容图片和svga
                width: sizeItem.avatarWidth,
                height: sizeItem.avatarWidth,
              ),
            ),
          ),
          // 踢出座位按钮➖
          _kickOut(),
        ],
      ),
    );
  }
  void playerAvatarSgGPOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String getImgUrl(String? img) {
    if (img == null || img.isEmpty) return '';
    if (img.endsWith('png') || img.endsWith('jpg'))
      return YBDImageUtil.frame(context, img, 'B');
    else
      return YBDImageUtil.gif(context, img, YBDImageType.FRAME);
  }

  Widget _kickOut() {
    // 踢出按钮只有房主有
    // 我是房主，被踢者不是我
    if (widget.leaderId != null && (widget.leaderId == widget.curUserId) && (widget.curUserId != widget.user!.id))
      return Positioned(
        right: 10.dp750,
        bottom: 10.dp750,
        child: Container(
          child: YBDDelayGestureDetector(
            onTap: () {
              logger.v('22.10.19---kick out user:${widget.user!.id}   ${widget.user!.nickname}');
              YBDCommonTrack().tpgo('-');
              YBDTPGradientDialog.show(
                context,
                title: 'tip'.i18n,
                height: 460,
                type: DialogType.kickTip,
                showClose: false,
                buttonNum: 2,
                contentMsg: 'kick_up_tip'.i18n,
                kickPlayerId: widget.user!.id,
              );
            },
            child: YBDGameWidgetUtil.assetImage('icon_tichu', width: 60.dp750, height: 60.dp750, isWebp: true),
          ),
        ),
      );
    return Container();
  }
  void _kickOutr7SA7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 玩家状态
  Widget playerStatusWidget() {
    // 玩家状态, 大于底注才表示下过注，才展示状态, 直接看牌/弃牌也展示
    if ((widget.user?.cardStateTips().isNotEmpty ?? false) &&
        ((widget.user?.chaalValue ?? 0) > (widget.battle?.boot ?? 0) || widget.user!.cardState != 0) &&
        widget.battle?.id != null) {
      return Positioned(
        left: 0,
        bottom: 10.dp750,
        child: Container(
          height: 39.dp750,
          width: sizeItem.avatarWidth,
          color: Colors.black.withOpacity(0.5),
          child:
              Center(child: YBDGameWidgetUtil.textSingleRegular(widget.user?.cardStateTips() ?? '', fontSize: 19.sp750)),
        ),
      );
    }
    return Container();
  }
  void playerStatusWidget49jXtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 倒计时
  Widget peisTimeWidget() {
    // 倒计时, 到该用户下注了, 且在游戏中
    if ((widget.user?.id ?? -1) == (widget.battle?.currentOperator ?? -2) &&
        logic.state!.canCountdown &&
        (widget.battle?.status ?? 0) == 2) {
      // 获取当前时间戳
      int currentTimeMs = DateTime.now().millisecondsSinceEpoch;
      int expireTimeMs = widget.battle?.optExpireAt ?? 0;
      int remainMs = expireTimeMs - currentTimeMs;
      int timeoutMs = widget.battle?.timeout ?? 0;
      int optRemainMs = widget.battle?.optTimesRemaining ?? 0;

      logger.v('widget.battle.timeout is:${(widget.user?.id ?? -1) == (widget.battle?.currentOperator ?? -2)},'
          'userId:${widget.user?.id}, '
          'timeout:${widget.battle?.timeout}, '
          'remain:${widget.battle?.optTimesRemaining},remainMs:$remainMs');
      // 校准剩余时间
      if (remainMs <= timeoutMs && remainMs >= 0 && remainMs < optRemainMs) {
        widget.battle?.optTimesRemaining = remainMs;
      }

      return RepaintBoundary(
        child: PiesTimeWidget(
          sizeItem.avatarWidth / 2.0,
          0,
          YBDHexColor('#FFE475').withOpacity(0.6),
          YBDHexColor('#EC3530').withOpacity(0.6),
          widget.battle!.timeout!,
          remainTime: widget.battle!.optTimesRemaining,
          endTimeCallback: () {
            // 倒计时结束了，自动弃牌，
            logger.v('peisTimeWidget endTimeCallback: ${logic.state?.roomData.battle?.status}');
          },
        ),
      );
    }
    return Container();
  }
  void peisTimeWidgetZXJDSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

// 庄家标识 皇冠位置 房主位置
  Widget kingWidget() {
    return Transform.translate(
      offset: Offset(210.dp750 as double, -50.dp750 as double),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (widget.kingPosition == TPKingPosition.centerRight) _bottomOwner(),
          GetBuilder<YBDTPRoomGetLogic>(
            id: YBDTPRoomGetState.dingzhuangIdentifier,
            builder: (YBDTPRoomGetLogic logic) {
              // role=2 为庄家
              // if (widget.user.role != 2 || !logic.state.playedCards) {
              //   return Container();
              // }
              switch (widget.kingPosition) {
                case TPKingPosition.centerRight:
                  return kingAnimation();
                default:
                  return Container();
              }
            },
          )
        ],
      ),
    );
  }

  // 底部的房主标识
  Widget _bottomOwner() {
    if (hasPlayer && widget.leaderId != null && (widget.leaderId == widget.user!.id))
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [SizedBox(width: 10.dp750), _ownerIcon()!, SizedBox(width: 10.dp750)],
      );
    return Container();
  }
  void _bottomOwnerVUhPxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 房主标识
  Widget? _ownerIcon() {
    return YBDGameWidgetUtil.assetImage('icon_owner', width: 41.dp750, height: 41.dp750, isWebp: true);
  }

  Widget kingAnimation() {
    if (widget.user!.role != 2 || !logic.state!.playedCards) return Container();
    return YBDTPAnimatedScale(
        child: YBDGameWidgetUtil.assetTPImage(
      'tp_buck_king',
      isWebp: false,
      width: 41.dp750,
      height: 41.dp750,
    ));
  }
}

class YBDTPAnimatedScale extends StatefulWidget {
  Widget? child;
  bool playing; // 播放动画
  YBDTPAnimatedScaleController? controller;
  VoidCallback? finishCallback; // 动画完成
  YBDTPAnimatedScale({Key? key, this.child, this.playing = true, this.controller, this.finishCallback}) : super(key: key);

  @override
  _YBDTPAnimatedScaleState createState() => _YBDTPAnimatedScaleState();
}

class _YBDTPAnimatedScaleState extends State<YBDTPAnimatedScale> {
  double scale = 1.0;

  @override
  void initState() {
    super.initState();

    if (widget.playing) {
      WidgetsBinding.instance?.addPostFrameCallback((_) async {
        playAnimation();
      });
    }
    if (widget.controller != null) {
      widget.controller!.initState(this);
    }
  }
  void initStateVGO5doyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  playAnimation() {
    if (mounted) {
      setState(() {
        scale = 1.2;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedScale(
        duration: YBDTPRoomBeansScaleEvent.duration,
        scale: scale,
        curve: Curves.easeOutBack,
        onEnd: () {
          if (mounted) {
            setState(() {
              scale = 1.0;
            });
            widget.finishCallback?.call();
          }
        },
        child: widget.child);
  }
}

class YBDTPAnimatedScaleController {
  _YBDTPAnimatedScaleState? _state;

  initState(_YBDTPAnimatedScaleState state) {
    _state = state;
  }

  playAnimation() {
    _state?.playAnimation();
  }
}

class YBDTPAvatarSize {
  bool isSelf; // 是自己，否自是别人
  YBDTpRoomDataUser? user; // 可以为空，只做弃牌处理

  YBDTPAvatarSize({this.isSelf = false, this.user}); // 座位用户信息

  // 默认头像大小
  double get defalutAvatarWidth => 147.dp750 as double;

  // 头像大小 自己196， 别人158
  double get avatarWidth {
    bool isPack = (user?.cardState ?? 0) == 2;
    return isSelf ? 196.dp750 as double : (isPack ? 152.dp750 as double : 158.dp750 as double);
  }

  double get maxAvatarWidth {
    return isSelf ? 196.dp750 as double : 158.dp750 as double;
  }

  double get maxHeight => maxAvatarWidth + 30.dp750;

  // 边框大小 自己8， 别人7
  double get strokeWidth => isSelf ? 8.dp750 as double : 7.dp750 as double;

  // 光圈大小
  double get haloWidth => avatarWidth + 30.dp750;

  // 加了皇冠的宽度
  double get maxWidth => maxHeight + 42.dp750;

  static double get otherMaxAvatar => 188.dp750 as double;
  static double get selfMaxAvatar => 226.dp750 as double;

  // 头像的皇冠位置
  static List<TPKingPosition> get avatarKingPositions {
    return <TPKingPosition>[
      TPKingPosition.centerRight,
      TPKingPosition.topRight,
      TPKingPosition.topRight,
      TPKingPosition.topLeft,
      TPKingPosition.topLeft,
    ];
  }

  static List<Alignment>? _avatarAligns;

  /// 头像的对齐位置
  static List<Alignment> avatarAligns(double areaHeight) {
    // if (_avatarAligns != null) return _avatarAligns;
    double spaceWidth = 20.dp750 as double; // 距离两边边距
    double spaceBottom = 60.dpH1624 as double; // 距离底边边距

    Size areaSize = Size(SizeNum.screentWidth / 2.0, areaHeight / 2.0);

    double centerY = (areaSize.height - spaceBottom) / areaSize.height;
    double centerX = 33.dp750 / areaSize.width;
    double spaceX = spaceWidth / areaSize.width;
    double topY = 350.dpH1559 / areaSize.height; // 0.4
    double bottomY = 130.dpH1559 / areaSize.height; //0.3

    _avatarAligns = <Alignment>[
      Alignment(centerX, centerY),
      Alignment(spaceX - 1, bottomY), //  下左
      Alignment(spaceX - 1, -topY),
      Alignment(1 - spaceX, -topY),
      Alignment(1 - spaceX, bottomY),
    ];
    return _avatarAligns!;
  }

  // 卡牌大小
  static Size cardSize() {
    return Size(58.dp750 as double, 83.dp750 as double);
  }

  static List<Offset>? _cardOffsets;

  /// 牌所有的位置
  static List<Offset> cardOffsets(double playerHeight) {
    if (_cardOffsets != null) return _cardOffsets!;
    // 纸牌大小
    Size cardSize = YBDTPAvatarSize.cardSize();
    // 区域大小
    Size areaSize = Size(SizeNum.screentWidth as double, playerHeight);

    double centerX = (areaSize.width - cardSize.width) / 2.0;
    double centerY = areaSize.height - 180.dp750 - cardSize.height - 60.dpH1624;
    double topY = (areaSize.height / 2.0) - 350.dpH1559 + 38.dp750; // 0.4
    double bottomY = (areaSize.height / 2.0) - cardSize.height / 2.0 + 130.dpH1559;

    double leftX = 244.dp750 as double;
    double rightX = 450.dp750 as double;

    _cardOffsets = [
      Offset(centerX, centerY),
      Offset(leftX, bottomY),
      Offset(leftX, topY),
      Offset(rightX, topY),
      Offset(rightX, bottomY),
    ];

    return _cardOffsets!;
  }

  /// 比牌结果的状态
  static List<Rect> comparedCardStatusRects(double playerHeight) {
    List<Offset> cards = YBDTPAvatarSize.cardOffsets(playerHeight);
    // (175-58)/2.0=58.5
    List<Rect> list = cards.mapIndexValue((int index, Offset e) {
      if (index == 0) {
        return Rect.fromLTWH(e.dx - 72.dp750, e.dy + 50.dp750, 200.dp750 as double, 60.dp750 as double);
      }
      return Rect.fromLTWH(e.dx - 58.dp750, e.dy - 11.dp750, 175.dp750 as double, 114.dp750 as double);
    }).toList();
    return list;
  }

  /// 有人要看我的牌
  static List<Rect> someOneSeeMyCardRects(double playerHeight) {
    List<Offset> cards = YBDTPAvatarSize.cardOffsets(playerHeight);
    List<Rect> list = cards.mapIndexValue((int index, Offset e) {
      return Rect.fromLTWH(e.dx + 3.dp750, e.dy + 14.dp750, 54.dp750 as double, 54.dp750 as double);
    }).toList();
    return list;
  }

  static List<Alignment>? _openCardAligns;

  /// 开牌的对齐方式
  static List<Alignment> openCardAligns(double playerHeight) {
    if (_openCardAligns != null) return _openCardAligns!;
    double spaceBottom = 225.dpH1559 as double; // 距离底边边距
    double spaceWidth = 210.dp750 as double; // 距离两边边距
    Size areaSize = Size(SizeNum.screentWidth / 2.0, playerHeight / 2.0);

    double centerY = (areaSize.height - spaceBottom) / areaSize.height;
    double spaceX = spaceWidth / areaSize.width;
    double topY = 310.dpH1559 / areaSize.height;
    double bottomY = 140.dpH1559 / areaSize.height;

    _openCardAligns = [
      Alignment(0, centerY),
      Alignment(spaceX - 1, bottomY), //  下左
      Alignment(spaceX - 1, -topY),
      Alignment(1 - spaceX, -topY),
      Alignment(1 - spaceX, bottomY),
    ];

    return _openCardAligns!;
  }
}

/// 头像光圈
class YBDTPAvatarHalo extends StatefulWidget {
  double? width;
  double? height;
  int breathNum; // 呼吸次数[0, 1, 0]，默认2次, 如果是1表示直接亮起

  YBDTPAvatarHalo({this.width, this.height, this.breathNum = 2});

  @override
  _YBDTPAvatarHaloState createState() => _YBDTPAvatarHaloState();
}

class _YBDTPAvatarHaloState extends State<YBDTPAvatarHalo> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    Duration duration = const Duration(milliseconds: 250);

    _animationController = AnimationController(duration: duration, vsync: this);

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      if (widget.breathNum == 1) {
        _animationController.value = 1.0;
      } else {
        _animationController.repeat(reverse: true);
      }
    });

    _animationController.addListener(() {
      if (mounted) setState(() {});
    });

    Future.delayed(Duration(milliseconds: duration.inMilliseconds * widget.breathNum), () {
      if (mounted) _animationController.reset();
    });
  }
  void initStateOCLQFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Opacity(
        opacity: _animationController.value,
        child: YBDGameWidgetUtil.assetTPImage(
          'tp_user_halo',
          isWebp: false,
          width: widget.width,
          height: widget.height,
        ));
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
