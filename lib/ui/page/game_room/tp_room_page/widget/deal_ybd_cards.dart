
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/const/tp_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';

class YBDDealCards extends StatefulWidget {
  //牌的高度和宽度
  double? cardWidth, cardHeight;

  //发牌的重点坐标和角度
  List<YBDEndCoordinates>? endCoordinateList;

  //一手几张牌
  int numEachHand;

  //旋转次数
  int turnsNumber;

  //控制器
  YBDDealController? controller;

  // 开始坐标
  double? startLeft, startTop;
  // 直接开始发牌
  bool startPlaying;
  // 已经发好牌了
  bool isPlayed;

  // 58*83 -> 56*80
  YBDDealCards(
      {this.cardWidth,
      this.cardHeight,
      this.startLeft,
      this.startTop,
      this.endCoordinateList,
      this.numEachHand: 3,
      this.turnsNumber: 1,
      this.startPlaying = false,
      this.isPlayed = false,
      this.controller});

  @override
  YBDDealCardsState createState() => new YBDDealCardsState();
}

class YBDDealCardsState extends BaseState<YBDDealCards> with TickerProviderStateMixin, YBDDealController {
  late List<Animation<double>> dealAnimations;
  List<AnimationController>? dealControllers;

  List<YBDEndCoordinates>? endCoordinateList;

  List<Widget>? _makeCards;
  late List<double> _animationValues; // 当前动画完成度
  bool _animationPlaying = false; // 正在动画中
  bool _isRestAnimation = false; // 重置了动画，正在执行的动画就取消吧
  bool _isHandPack = false; // 收牌
  bool _isHandSee = false; // 看牌
  bool _isHandMask = false; // 添加蒙版
  bool _isWashAnimation = false; // 洗牌
  AnimationStatusListener? _playSgvaStateListener; // 状态监听

  double? startLeft, startTop;

  void setDefaultEndCoordinates() {
    if (widget.endCoordinateList == null) {
      endCoordinateList = <YBDEndCoordinates>[];
      // endCoordinateList
      //     .add(YBDEndCoordinates(top: 200, left: 50, endAngle: 90));
      // endCoordinateList
      //     .add(YBDEndCoordinates(top: 400, left: 50, endAngle: 90));
      // endCoordinateList
      //     .add(YBDEndCoordinates(top: 200, left: 550, endAngle: -90));
      // endCoordinateList
      //     .add(YBDEndCoordinates(top: 400, left: 550, endAngle: -90));
    } else {
      endCoordinateList = widget.endCoordinateList;
    }
  }
  void setDefaultEndCoordinatescA3Q6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    widget.cardHeight ??= 83;
    widget.cardWidth ??= 58;
    startLeft = widget.startLeft ??= (SizeNum.screentWidth - widget.cardWidth!) / 2;
    startTop = widget.startTop ??= 50;
    return Stack(
      children: <Widget>[
        Container(),
        if (_isWashAnimation) washCardSvga(),
        ...makeCards()!,
      ],
    );
  }
  void myBuildsiFKGoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 洗牌动画
  Widget washCardSvga() {
    return Positioned(
      top: startTop,
      left: startLeft! - widget.cardHeight! / 2.0,
      child: Container(
        width: widget.cardHeight! * 2,
        height: widget.cardHeight,
        // color: Colors.red.withOpacity(0.7),
        child: YBDGameWidgetUtil.assetTPSvga('shuffle',
            startPlay: true, playStateListener: _playSgvaStateListener, fit: BoxFit.fitHeight),
      ),
    );
  }
  void washCardSvgabCbRxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //制作牌堆
  List<Widget>? makeCards() {
    logger.v(
        'YBDDealCards makeCards endCoordinateList:${endCoordinateList!.length}, _isHandSee:$_isHandSee, _isHandPack:$_isHandPack,_isRestAnimation:$_isRestAnimation,_animationPlaying:$_animationPlaying');
    // return List.generate(dealAnimations.length, (index) => makeCard(index));

    if (_makeCards != null && (_isHandSee || _isHandPack || _animationPlaying)) {
      for (int vIndex = 0; vIndex < _animationValues.length; vIndex++) {
        double aValue = _animationValues[vIndex];
        int hand = vIndex % endCoordinateList!.length; // 所在位置
        if (aValue > 0.0 && aValue != 2.0) {
          // 弃牌更新操作
          if (endCoordinateList![hand].isPacking) {
            _makeCards![vIndex] = makeCardPack(vIndex);
            if (aValue == 1.0) // 运动结束了
              _animationValues[vIndex] = 2.0;
          } else if (endCoordinateList![hand].isSeeing) {
            // 看牌更新操作
            _makeCards![vIndex] = makeCardSee(vIndex);
            if (aValue == 1.0) // 运动结束了
              _animationValues[vIndex] = 2.0;
          } else if (_animationPlaying) {
            // 发牌中
            _makeCards![vIndex] = makeCard(vIndex);
            if (aValue == 1.0) // 运动结束了
              _animationValues[vIndex] = 2.0;
          }
        }
      }
      return _makeCards;
    }
    // 这里会导致挂后台无法更新卡牌
    // if (_makeCards != null && !_isRestAnimation) {
    //   return _makeCards;
    // }
    _makeCards = List<Widget>.generate(dealAnimations.length, (int index) => makeCard(index));

    return _makeCards;
  }

  //制作单张牌
  Widget makeCard(int index) {
    int cardNum = index ~/ endCoordinateList!.length; // 第几张牌
    int endIndex = index % endCoordinateList!.length; // 所在位置

    //计算中点
    double middle = (widget.numEachHand - 1) / 2;
    YBDEndCoordinates end = endCoordinateList![endIndex];
    logger.v(
        'YBDDealCards makeCard endIndex: $endIndex, cardNum:$cardNum, index: $index, end.opacity:${end.opacity}, value: ${dealAnimations[index].value}');

    //计算旋转角度
    double fixAngle = widget.turnsNumber * 360 + end.endAngle + (cardNum - middle) * end.rotate;
    double space = end.space * (cardNum - middle);
    double spaceY = end.spaceY * (cardNum - middle).abs();
    if (cardNum - middle < 0 && end.scale == 1.0) {
      spaceY = 0;
    }
    if (end.opacity == 0.0) {
      return Container();
    }

    //动画进度
    double value = dealAnimations[index].value;
    return Positioned(
      child: Opacity(
        opacity: end.opacity,
        child: ScaleTransition(
          scale: AlwaysStoppedAnimation<double>((end.scale - 1) * value + 1),
          child: RotationTransition(
              alignment: value > 0.95 ? Alignment.bottomCenter : Alignment.center,
              turns: AlwaysStoppedAnimation<double>((fixAngle / 360) * value),
              child: pokerCard(card: end.cards != null ? end.cards![cardNum] : null, mask: end.mask)),
        ),
      ),
      top: startTop! + (end.top! - startTop! + spaceY) * value,
      left: startLeft! + (end.left! - startLeft! + space) * value,
    );
  }
  void makeCardRaXGmoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //单张牌收回卡牌
  Widget makeCardPack(int index) {
    int cardNum = index ~/ endCoordinateList!.length; // 第几张牌
    int endIndex = index % endCoordinateList!.length; // 所在位置
    logger.v(
        'YBDDealCards makeCardPack endIndex: $endIndex, cardNum:$cardNum, index: $index, value: ${dealAnimations[index].value}');

    //计算中点
    double middle = (widget.numEachHand - 1) / 2;
    YBDEndCoordinates end = endCoordinateList![endIndex];

    //计算旋转角度
    double fixAngle = end.endAngle + (cardNum - middle) * end.rotate;
    //动画进度
    double value = 1.0;

    //动画进度
    double aniValue = dealAnimations[index].value; // 0, 0.5, 1

    double rotate = fixAngle / 360.0 - (fixAngle / 360.0 * animationRange(0.0, 0.5, aniValue)); // --> 0; 0~0.5
    double opacity = 1 - animationRange(0.5, 1.0, aniValue); // 1-->0; 0.5~1

    double scale = end.scale;
    double? top = end.top;
    double? left = end.left; //+ space * opacity;

    if (opacity == 0.0) {
      return Container();
    }

    return Positioned(
      child: Opacity(
        opacity: opacity,
        child: Transform.scale(
          scale: scale,
          child: RotationTransition(
            alignment: value > 0.95 ? Alignment.bottomCenter : Alignment.center,
            turns: AlwaysStoppedAnimation<double>(rotate),
            child: pokerCard(card: end.cards != null ? end.cards![cardNum] : null, mask: end.mask),
          ),
        ),
      ),
      top: top,
      left: left,
    );
  }
  void makeCardPackW4dOboyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //单张牌旋转看牌
  Widget makeCardSee(int index) {
    int cardNum = index ~/ endCoordinateList!.length; // 第几张牌
    int endIndex = index % endCoordinateList!.length; // 所在位置
    logger.v(
        'YBDDealCards makeCardSee endIndex: $endIndex, cardNum:$cardNum, index: $index, value: ${dealAnimations[index].value}');

    //计算中点
    double middle = (widget.numEachHand - 1) / 2;
    YBDEndCoordinates end = endCoordinateList![endIndex];

    //计算旋转角度 + 一圈
    double fixAngle = end.endAngle + (cardNum - middle) * end.rotate + 360;
    double space = end.space * (cardNum - middle);
    double spaceY = end.spaceY * (cardNum - middle).abs();
    if (cardNum - middle < 0 && end.scale == 1.0) {
      spaceY = 0;
    }
    //动画进度
    double value = 1.0;

    //动画进度
    double aniValue = dealAnimations[index].value;

    double rotate = fixAngle / 360.0 * aniValue;

    double scale = end.scale;
    double top = end.top! + spaceY;
    double left = end.left! + space;

    return Positioned(
      child: Transform.scale(
        scale: scale,
        child: RotationTransition(
            alignment: value > 0.95 ? Alignment.bottomCenter : Alignment.center,
            turns: AlwaysStoppedAnimation<double>(rotate),
            child: pokerCardSee(index, aniValue)),
      ),
      top: top,
      left: left,
    );
  }
  void makeCardSeezWmaxoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget pokerCardSee(int index, double value) {
    if (value > 0.9) {
      int cardNum = index ~/ endCoordinateList!.length; // 第几张牌
      int endIndex = index % endCoordinateList!.length; // 所在位置
      YBDEndCoordinates end = endCoordinateList![endIndex];
      if (end.cards != null && end.cards!.length > cardNum) return pokerCard(card: end.cards![cardNum], mask: end.mask);
    }
    return pokerCard();
  }

  // 到了这个区间才启动转换
  double animationRange(double start, double end, double value) {
    if (value > start && value < end) {
      // 0.5-0.7  0.6 ==> 0.5
      // 0.5-0.7  0.65 ==> 0.15/0.2
      return (value - start) / (end - start);
    }
    if (value < start) {
      return 0.0;
    }
    return 1.0;
  }

  void setAnimation() {
    logger.v('setAnimation endCoordinateList: ${endCoordinateList!.length},widget.isPlayed: ${widget.isPlayed}');
    int count = endCoordinateList!.length * widget.numEachHand;
    // 移除上次所有监听
    if (dealControllers != null) {
      dealControllers!.forEach((AnimationController element) {
        element.removeStatusListener((AnimationStatus status) {});
        element.removeListener(() {});
        element.clearStatusListeners();
        element.clearListeners();
      });
      logger.v('setAnimation removeStatusListener: ${dealControllers!.length}, newCount:$count');
    }
    _animationValues = List<double>.generate(count, (int index) => 0);
    dealControllers = List<AnimationController>.generate(count, (int index) {
      int hand = index % endCoordinateList!.length; // 所在位置
      YBDEndCoordinates end = endCoordinateList![hand];
      return AnimationController(duration: end.duration, vsync: this);
    });

    dealControllers!.forEachIndexValue((int index, AnimationController value) {
      value.addListener(() {
        // logger.v(
        //     'setAnimation addListener: endCoordinateList:${endCoordinateList.length},_animationValues:${_animationValues.length},dealControllers:${dealControllers.length},_animationValues:${_animationValues.length}');
        // if (dealControllers.length > index && _animationValues.length > index) {
        _animationValues[index] = dealControllers![index].value;
        if (mounted) setState(() {});
        // }
      });
      // value.addStatusListener((AnimationStatus status) {
      //   logger.v(
      //       'setAnimation addStatusListener status:$status, index:$index, _isHandSee:$_isHandSee, _isHandPack:$_isHandPack,_animationPlaying:$_animationPlaying');
      //   if (status == AnimationStatus.dismissed || status == AnimationStatus.completed) {
      //     double value = _animationValues.firstWhereIndex((int index, double element) => element != 2.0);
      //     // 所有的动画都完成了
      //     if (value == null) {
      //       logger.v('setAnimation addStatusListener end');
      //     }
      //     // 最后一个动画待完成了
      //     List<double> lastValus = _animationValues.where((double element) => element == 2.0).toList();
      //     if (lastValus.length <= 1) {
      //       logger.v('setAnimation addStatusListener end last one to ${lastValus.length}');
      //       Future<void>.delayed(Duration(milliseconds: 100), () {
      //         logger.v(
      //             'setAnimation addStatusListener end real to _isHandSee:$_isHandSee, _isHandPack:$_isHandPack,_animationPlaying:$_animationPlaying');
      //         double value = _animationValues.firstWhereIndex((int index, double element) => element != 2.0);
      //         // 所有的动画都完成了
      //         if (value == null) {
      //           logger.v(
      //               'setAnimation addStatusListener end real _isHandSee:$_isHandSee, _isHandPack:$_isHandPack,_animationPlaying:$_animationPlaying');
      //         }
      //       });
      //     }
      //   }
      // });
    });
    dealAnimations = List<Animation<double>>.generate(
        count, (int index) => Tween<double>(begin: 0.0, end: 1.0).animate(dealControllers![index]));
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      if (widget.startPlaying != null && widget.startPlaying && !widget.isPlayed) {
        endCoordinateList!.forEach((YBDEndCoordinates element) {
          element.opacity = 1.0;
        });
        playAnimations();
      }

      logger.v('addPostFrameCallback widget.isPlayed: ${widget.isPlayed}');
      if (widget.isPlayed) {
        cardsPlayed();
      }
    });
    setState(() {});
  }
  void setAnimationjrIWGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 牌已经发好了
  void cardsPlayed() {
    endCoordinateList!.forEach((YBDEndCoordinates element) {
      element.opacity = 1.0;
    });
    dealControllers!.forEach((AnimationController element) {
      element.value = 1.0;
      element.forward();
    });
  }

  Widget pokerCard({YBDTpRoomDataCard? card, bool mask = false}) {
    return Stack(
      children: <Widget>[
        YBDGameWidgetUtil.assetTPImage(YBDGameResource.tpCardName(card: card),
            width: widget.cardWidth, height: widget.cardHeight),
        if (mask)
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(3)), color: Colors.black.withOpacity(0.3)),
              width: widget.cardWidth,
              height: widget.cardHeight),
      ],
    );
  }
  void pokerCardMyEznoyelive({YBDTpRoomDataCard? card, bool mask = false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void playAnimations({VoidCallback? playedCallback, bool needWash = true}) {
    logger.v(
        'YBDDealCards playAnimations _animationPlaying: $_animationPlaying, dealControllers: ${dealControllers!.length}');
    _animationPlaying = true;
    _isRestAnimation = false;

    if (needWash) {
      _playSgvaStateListener = (AnimationStatus status) {
        logger.v('YBDDealCards playAnimations _playSgvaStateListener status: $status');
        if (status == AnimationStatus.completed || status == AnimationStatus.dismissed) {
          _isWashAnimation = false;
          playCardAnimations(playedCallback: playedCallback);
        } else if (status == AnimationStatus.forward) {
          // 洗牌音效
          YBDTPAudio.shuffle.play();
        }
      };
      _isWashAnimation = true;
      if (mounted) setState(() {});
    } else {
      playCardAnimations(playedCallback: playedCallback);
    }
  }
  void playAnimations31EaUoyelive({VoidCallback? playedCallback, bool needWash = true}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 发牌动画
  Future<void> playCardAnimations({VoidCallback? playedCallback}) async {
    List<double> opacitys = endCoordinateList!.map((YBDEndCoordinates e) => e.opacity).toList();
    logger.v('YBDDealCards playCardAnimations opacitys:$opacitys, _isHandPack:$_isHandPack');
    // 挂后台，发牌之后再进来就全部透明化了，所有需要全部还原。(其实有人弃牌了)
    // endCoordinateList.forEach((YBDEndCoordinates end) {
    //   end.opacity = 1.0;
    // });
    Duration? duration;
    int playCount = 0;
    bool played = false;
    for (int index = 0; index < dealControllers!.length; index++) {
      AnimationController ani = dealControllers![index];
      if (!mounted) return;
      if (index > 0) {
        await Future<void>.delayed(Duration(milliseconds: ani.duration!.inMilliseconds ~/ 2));
      }
      if (mounted) {
        ani.forward().orCancel;
        playCount += 1;
        // 发牌音效
        if (Platform.isIOS) {
          // 避免iOS的频繁播放导致卡顿
          if (index % 3 == 0) YBDTPAudio.dealCard.play();
        } else {
          YBDTPAudio.dealCard.play();
        }
        ani.addStatusListener((AnimationStatus status) async {
          logger.v(
              'playCardAnimations addStatusListener:$status, index:$index, playCount:$playCount, _animationPlaying:$_animationPlaying');
          if (status == AnimationStatus.completed && _animationPlaying) {
            playCount -= 1;
            // 最后一个动作完成
            if (playCount == 0 && index == dealControllers!.length - 1) {
              played = true;
              // 发牌完成给个回调
              playedCallback?.call();
              // 这里延迟是因为动画状态变更了，UI还没完更新完，所以延迟了100ms
              await Future<void>.delayed(Duration(milliseconds: 100));
              logger.v('playCardAnimations addStatusListener end:$status, end');
              // _animationPlaying = false;
            }
          }
        });
      }
      Duration? aniDuration = ani.duration;
      if (duration == null)
        duration = aniDuration;
      else
        duration = aniDuration! > duration ? aniDuration : duration;
    }
    if (duration != null) {
      Future<void>.delayed(Duration(milliseconds: duration.inMilliseconds + 200), () {
        if (!played) {
          // 如果在发牌的过程中退出，导致playCount 计算不准，这里是保底！！！
          logger.v('playCardAnimations played end await');
          _animationPlaying = false;
          // 发牌完成给个回调
          playedCallback?.call();
        }
      });
    }
  }
  void playCardAnimationsDj3pgoyelive({VoidCallback? playedCallback})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void resetAnimations() {
    _isRestAnimation = true;
    _isHandSee = false;
    _isHandPack = false;
    _animationPlaying = false;

    /// 重置牌型到初始状态
    for (YBDEndCoordinates end in endCoordinateList!) {
      end.opacity = 1.0;
      end.isSee = false;
      end.cards = null;
    }
    for (AnimationController ani in dealControllers!) {
      if (mounted) ani.reset();
      // 恢复时间
      int endIndex = dealControllers!.indexOf(ani) % endCoordinateList!.length; // 所在位置
      YBDEndCoordinates end = endCoordinateList![endIndex];
      ani.duration = end.duration;
    }
  }

  DateTime? allPackedTime;
  /// 全部收牌动画-->
  Future<void> handCardsAllPackAnimation() async {
    /*
    int packedCount = 0;
    dealControllers.forEach((AnimationController element) {
      element.removeStatusListener((AnimationStatus status) {
        logger.v('handCardsAllPackAnimation removeStatusListener:$status');
      });
    });
    */
    // 避免太快重复调用，造成卡顿，收牌异常问题
    if (allPackedTime != null &&  DateTime.now().difference(allPackedTime!) < Duration(milliseconds: 500)) {
      logger.v('handCardsAllPackAnimation too fast: ${DateTime.now().difference(allPackedTime!)}');
      return;
    }
    allPackedTime = DateTime.now();

    int nums = 3; // 每人3张牌
    int humans = endCoordinateList!.length; // 有多少玩家
    endCoordinateList!.forEachIndexValue((int hand, YBDEndCoordinates end) {
      logger.v('handCardsAllPackAnimation hand:$hand, isPacked: ${end.opacity == 0.0}, isPacking: ${end.isPacking}');
      if (!(end.opacity == 0.0 || end.isPacking)) {
        _isHandPack = true;
        end.isPacking = true;
        for (int i = 0; i < nums; i++) {
          // 012
          int index = hand + i * humans;
          dealControllers![index].value = 0.0;
          if (mounted) {
            dealControllers![index].duration = Duration(milliseconds: 100);
            dealControllers![index].forward();
          }
        }
      }
    });

    /*
    dealControllers.forEachIndexValue((int index, AnimationController ani) {
      int hand = index % endCoordinateList.length; // 所在位置
      YBDEndCoordinates end = endCoordinateList[hand];
      logger.v('handCardsAllPackAnimation hand:$hand, isPacked: ${end.opacity == 0.0}, isPacking: ${end.isPacking}');
      if (!(end.opacity == 0.0 || end.isPacking)) {
        _isHandPack = true;
        end.isPacking = true;
        ani.value = 0.0;
        ani.duration = Duration(milliseconds: 500);
        if (mounted) {
          ani.forward();
          /*
          packedCount += 1;
          ani.addStatusListener((AnimationStatus status) async {
            logger.v(
                'handCardsAllPackAnimation addStatusListener:$status, hand:$hand, index:$index,_isHandPack:$_isHandPack');
            if (status == AnimationStatus.completed && _isHandPack) {
              packedCount -= 1;
              if (packedCount == 0) {
                await Future<void>.delayed(Duration(milliseconds: 100));
                logger.v('handCardsAllPackAnimation addStatusListener:$status, end');
                _isHandPack = false;
                endCoordinateList.forEach((YBDEndCoordinates element) {
                  element.opacity = 0.0;
                  element.isPacking = false;
                });
              }
            }
          });

           */
        }
      }
    });
    */

    // 确保全部弃牌成功
    Future.delayed(Duration(milliseconds: 700),(){
      logger.v('handCardsAllPackAnimation end');
      _isHandPack = false;
      endCoordinateList!.forEach((YBDEndCoordinates element) {
        element.opacity = 0.0;
        element.isPacking = false;
      });
      // 结束动画设置到完成
      dealControllers?.forEach((AnimationController element) {
        element.value = 1.0;
      });
      _makeCards = null;
      if (mounted) setState(() {});
    });
  }
  void handCardsAllPackAnimation8RAaPoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 收牌动画-->还原-->隐藏 根据玩家索引获取手中的牌, 一手默认3张牌, 多少人5
  Future<void> handCardsPackAnimation(int hand, {int nums = 3, int? humans = 5}) async {
    if (hand >= endCoordinateList!.length) {
      logger.e('handCardsPackAnimation error hand: $hand, endCoordinateList:${endCoordinateList!.length}');
      return;
    }
    logger.v(
        'handCardsPackAnimation hand: $hand, isPacked: ${endCoordinateList![hand].opacity == 0.0}, isPacking: ${endCoordinateList![hand].isPacking}');

    if (endCoordinateList![hand].opacity == 0.0 || endCoordinateList![hand].isPacking) {
      if (mounted) setState(() {});
      return; // 已经弃牌了, 或正在弃牌中
    }
    if (endCoordinateList![hand].isSeeing) {
      // 正在看牌动画，那就直接停止，进行弃牌
      endCoordinateList![hand].isSeeing = false;
    }
    endCoordinateList![hand].opacity = 1.0;
    endCoordinateList![hand].isPacking = true;
    humans = endCoordinateList!.length;
    _isHandPack = true;
    int packedCount = 0;
    for (int i = 0; i < nums; i++) {
      // 012
      int index = hand + i * humans;
      dealControllers![index].value = 0.0;
      if (mounted) {
        dealControllers![index].duration = Duration(milliseconds: 100);
        dealControllers![index].forward();
        // packedCount += 1;
        // dealControllers[index].addStatusListener((AnimationStatus status) async {
        //   logger.v('handCardsPackAnimation addStatusListener:$status， hand:$hand, '
        //       'index:$index, packedCount:$packedCount, value:${dealAnimations[index].value},_isHandPack:$_isHandPack');
        //   if (status == AnimationStatus.completed && _isHandPack) {
        //     packedCount -= 1;
        //     if (packedCount == 0) {
        //       await Future<void>.delayed(Duration(milliseconds: 100));
        //       logger.v('handCardsPackAnimation addStatusListener:$status end');
        //       _isHandPack = false;
        //       endCoordinateList[hand].opacity = 0.0;
        //       endCoordinateList[hand].isPacking = false;
        //     }
        //   }
        // });
      }
    }
    //TODO 延迟更新状态 _isHandPack
    Future.delayed(Duration(milliseconds: 800), () {
      logger.v('handCardsPackAnimation opacity');
      _isHandPack = false;
      endCoordinateList![hand].opacity = 0.0;
      endCoordinateList![hand].isPacking = false;
      if (mounted) {
        setState(() {});
      }
    });
  }
  void handCardsPackAnimationffF7Ooyelive(int hand, {int nums = 3, int? humans = 5})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 开牌添加蒙版
  void handCardComparedMaskAnimation(int hand, List<YBDTpRoomDataCard?>? cards, {bool mask: false}) {
    logger
        .v('handCardComparedMask hand:$hand, endCoordinateList[hand].mask:${endCoordinateList![hand].mask}, mask:$mask');

    if (endCoordinateList![hand].mask == mask) {
      return;
    }
    int nums = 3; // 一手默认3张牌
    int humans = endCoordinateList!.length; //  多少人5
    endCoordinateList![hand].cards = cards;
    endCoordinateList![hand].isSee = false;
    endCoordinateList![hand].isSeeing = true;
    endCoordinateList![hand].mask = mask;
    _isHandSee = true;
    int seeCount = 0;
    for (int i = 0; i < nums; i++) {
      int index = hand + i * humans;
      logger.v('handCardComparedMask index:$index, dealControllers:${dealControllers!.length}');
      dealControllers![index].value = 1.0;
      dealControllers![index].duration = Duration(milliseconds: 100);
      if (mounted) {
        dealControllers![index].forward();
        seeCount += 1;
        dealControllers![index].addStatusListener((AnimationStatus status) {
          logger.v('handCardComparedMask addStatusListener:$status， hand:$hand, '
              'index:$index, seeCount:$seeCount, _isHandSee:$_isHandSee');
          if (status == AnimationStatus.completed && _isHandSee) {
            seeCount -= 1;
            if (seeCount == 0) {
              logger.v('handCardComparedMask addStatusListener:$status, end');
              _isHandSee = false;
              endCoordinateList![hand].isSee = true;
              endCoordinateList![hand].isSeeing = false;
            }
          }
        });
      }
    }
  }
  void handCardComparedMaskAnimation0WNIBoyelive(int hand, List<YBDTpRoomDataCard?>? cards, {bool mask: false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 看牌-->或比牌添加蒙版，无效动效
  Future<void> handCardsSeeAnimation(int hand, int? humans, List<YBDTpRoomDataCard?>? cards,
      {bool animation: true, bool mask: false}) async {
    logger.v('handCardsSeeAnimation isSee: ${endCoordinateList![hand].isSee}, '
        'isSeeing: ${endCoordinateList![hand].isSeeing}, cards:${cards?.length}, hand:$hand');
    if (endCoordinateList!.length <= hand || endCoordinateList![hand].isSee || endCoordinateList![hand].isSeeing) {
      return;
    }
    // 看牌音效
    YBDTPAudio.see.play();
    int nums = 3; // 一手默认3张牌
    // int humans = 5; //  多少人5
    endCoordinateList![hand].cards = cards;
    endCoordinateList![hand].isSee = false;
    endCoordinateList![hand].isSeeing = true;
    endCoordinateList![hand].mask = mask;
    humans = endCoordinateList!.length;
    _isHandSee = true;
    int seeCount = 0;
    for (int i = 0; i < nums; i++) {
      if (animation && i > 0) await Future<void>.delayed(Duration(milliseconds: 200));
      int index = hand + i * humans;
      logger.v('handCardsSeeAnimation index:$index, dealControllers:${dealControllers!.length}');
      dealControllers![index].value = animation ? 0.0 : 1.0;
      dealControllers![index].duration = Duration(milliseconds: 500);
      if (mounted) {
        dealControllers![index].forward();
        // seeCount += 1;
        // dealControllers[index].addStatusListener((AnimationStatus status) async {
        //   logger.v('handCardsSeeAnimation addStatusListener:$status， hand:$hand, '
        //       'index:$index, seeCount:$seeCount, _isHandSee:$_isHandSee');
        //   if (status == AnimationStatus.completed && _isHandSee) {
        //     seeCount -= 1;
        //     if (seeCount == 0) {
        //       await Future<void>.delayed(Duration(milliseconds: 100));
        //       logger.v('handCardsSeeAnimation addStatusListener:$status, end');
        //       _isHandSee = false;
        //       endCoordinateList[hand].isSee = true;
        //       endCoordinateList[hand].isSeeing = false;
        //     }
        //   }
        // });
      }
    }
    Future<void>.delayed(Duration(milliseconds: 800), () {
      logger.v('handCardsSeeAnimation delayed end');
      _isHandSee = false;
      endCoordinateList![hand].isSee = true;
      endCoordinateList![hand].isSeeing = false;
    });
  }

  @override
  void initState() {
    if (widget.controller != null) {
      widget.controller!.bindState(this);
    }
    super.initState();
    setDefaultEndCoordinates();
    setAnimation();
  }
  void initStatecsifSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void cancelAllController() {
    dealControllers!.forEach((AnimationController element) {
      element.clearListeners();
      element.clearStatusListeners();
      element.dispose();
    });
  }

  @override
  void dispose() {
    cancelAllController();
    super.dispose();
  }
}

class YBDEndCoordinates {
  double? top, left;

  int endAngle;
  double scale; // 终点的缩放比率 默认1.0
  int rotate; // 牌之间的角度 默认15度
  // 动画时间
  Duration duration;
  double opacity;
  bool isSee; // 是否看牌
  bool isSeeing; // 是否正在看牌
  bool isPacking; // 是否正在弃牌
  List<YBDTpRoomDataCard?>? cards;
  double space; // 卡牌之间的间距
  double spaceY; // 卡牌上下的间距
  bool mask; // 是否需要添加蒙版
  bool allEqual; // 是否全等于

  YBDEndCoordinates(
      {this.top,
      this.left,
      this.endAngle = 0,
      this.scale = 1.0,
      this.rotate = 15,
      this.opacity = 1.0,
      this.isSee = false,
      this.isSeeing = false,
      this.isPacking = false,
      this.mask = false,
      this.space = 5,
      this.spaceY = 2,
      this.cards,
      this.allEqual = false,
      this.duration = const Duration(milliseconds: 400)});

  @override
  bool operator ==(Object other) {
    bool result = other is YBDEndCoordinates && cards == other.cards && left == other.left && top == other.top;
    // if (other is YBDEndCoordinates) {
    //   logger.v('YBDEndCoordinates == cards:${cards == other.cards}, left:${left == other.left}, top:${top == other.top}');
    // }
    return result;
  }
}

class YBDDealController {
  YBDDealCardsState? _dealCardsState;

  void bindState(YBDDealCardsState state) {
    logger.v('YBDDealController bindState:$state');
    _dealCardsState = state;
  }

  List<YBDEndCoordinates> endCoordinates() {
    return _dealCardsState?.endCoordinateList ?? <YBDEndCoordinates>[];
  }
  void endCoordinatesiwx2zoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void updateEndCoordinates(List<YBDEndCoordinates> ends, bool isPlayed) {
    logger.v('YBDDealController bindState:$_dealCardsState, isPlayed:$isPlayed');
    if (_dealCardsState == null) return;
    _dealCardsState!.endCoordinateList = ends;
    _dealCardsState!._makeCards = null;
    _dealCardsState!._isRestAnimation = true;
    _dealCardsState!._animationPlaying = false;
    _dealCardsState!._isHandSee = false;
    _dealCardsState!._isHandPack = false;
    _dealCardsState!.setAnimation();
    if (isPlayed) {
      _dealCardsState!.cardsPlayed();
    }
  }

  void onlyUpdateEndCoordinatesToSetState(List<YBDEndCoordinates> ends) {
    logger.v('YBDDealController onlyUpdateEndCoordinatesToSetState bindState:$_dealCardsState');
    if (_dealCardsState == null) return;
    _dealCardsState!.endCoordinateList = ends;
    _dealCardsState!._makeCards = null;
    _dealCardsState!.setState(() {});
  }

  /// 重新发牌
  void replayCards(List<YBDEndCoordinates> ends, {VoidCallback? playedCallback, bool needWash = true}) async {
    logger.v('YBDDealController replayCards _dealCardsState: ${_dealCardsState!._animationPlaying}');
    if (_dealCardsState?._animationPlaying ?? false) {
      // return; // 在发牌中
    }
    updateEndCoordinates(ends, false);
    await Future<void>.delayed(Duration(milliseconds: 100));
    _dealCardsState?.playAnimations(playedCallback: playedCallback, needWash: needWash);
  }

  /// 根据玩家索引获取手中的牌, 一手默认3张牌
  List<Widget?> handCards(int hand, {int nums = 3}) {
    List<Widget?> cards = List<Widget?>.generate(nums, (int index) => null);
    for (int i = 0; i < nums; i++) {
      int index = hand * nums + i;
      cards[i] = _dealCardsState!._makeCards![index];
    }
    return cards;
  }

  /// 收牌动画-->还原-->隐藏
  void handCardsPack({
    required int hand,
    int? humans,
    int nums = 3,
  }) {
    logger.v('handCardsBack hand: $hand');
    _dealCardsState?.handCardsPackAnimation(hand, nums: nums, humans: humans);
  }

  /// 是否已经弃牌了
  bool handCardPacked(int hand) {
    if ((_dealCardsState?.endCoordinateList?.length ?? 0) <= hand) {
      return false;
    }
    if (_dealCardsState!.endCoordinateList![hand].opacity == 0.0 || _dealCardsState!.endCoordinateList![hand].isPacking) {
      return true; // 已经弃牌了, 或正在弃牌中
    }
    return false;
  }
  void handCardPacked9B2pgoyelive(int hand) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 没有收牌全部收牌
  void handCardsAllPack() {
    logger.v('handCardsAllBack');
    _dealCardsState?.handCardsAllPackAnimation();
  }

  /// 看牌动画--> 旋转--> 翻牌
  void handCardsSee({
    required int hand,
    int? humans,
    required List<YBDTpRoomDataCard?>? cards,
    bool animation: true,
    bool mask: false,
  }) {
    logger.v(
        'handCardsSee index: $hand, ${cards?.length},animation:${animation}, _dealCardsState: ${_dealCardsState != null}');
    _dealCardsState?.handCardsSeeAnimation(hand, humans, cards, animation: animation);
  }

  /// 开牌添加蒙版
  void handCardComparedMask({required int hand, required List<YBDTpRoomDataCard?>? cards, bool mask: true}) {
    logger.v('handCardComparedMask index: $hand, ${cards?.length}, _dealCardsState: ${_dealCardsState != null}');
    _dealCardsState?.handCardComparedMaskAnimation(hand, cards, mask: mask);
  }

  /// 是否看牌了
  bool handCardDidSee({required int hand}) {
    logger.v('handCardDidSee index: $hand, _dealCardsState: ${_dealCardsState != null}');
    if (_dealCardsState == null || _dealCardsState?.endCoordinateList == null) return false;
    if (_dealCardsState!.endCoordinateList!.length <= hand) {
      return false;
    }
    return _dealCardsState!.endCoordinateList![hand].isSeeing || _dealCardsState!.endCoordinateList![hand].isSee;
  }
}
