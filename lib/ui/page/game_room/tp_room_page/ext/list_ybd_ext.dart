import 'dart:async';


extension ListExt<E> on List<E> {

  List<T> mapIndexValue<T>(T toElement(int index, E value)) {
    List<T> mapList = <T>[];
    if (length == 0) return mapList;
    for (int i = 0; i < length; i++) {
      mapList.add(toElement(i, this[i]));
      // mapList[i] = toElement(i, this[i]);
    }
    return mapList;
  }

  void forEachIndexValue(toElement(int index, E value)) {
    for (int i = 0; i < length; i++) {
      toElement(i, this[i]);
    }
  }
  void forEachIndexValueUZ8aRoyelive(toElement(int index, E value)) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  E? firstWhereIndex(bool Function(int index, E element) test) {
    for (int i = 0; i < length; i++) {
      if (test(i, this[i])) return this[i];
    }
    return null;
  }

  // 移除空值，即将List<E?> 转化为List<T> 可选类型转必选类型, E必须为T的子类
  // 等价于 whereNotNull()
  List<T> removeNulls<T>() {
    List<T> list = <T>[];
    forEach((E element) {
      if (element != null) list.add(element as T);
    });
    return list;
  }

  // 添加中转运算类
  YBDListOperator<E> get valueOpt => YBDListOperator<E>(this);

}

extension SetExt<E> on Set<E> {
  Set<T> removeNulls<T>() {
    Set<T> list = Set<T>();
    forEach((E element) {
      if (element != null) list.add(element as T);
    });
    return list;
  }
}

// 允许空的返回值运算类
class YBDListOperator<E> {
  late List<E> list;
  YBDListOperator(this.list);

  E? operator [](int index) {
    if (list.length > index) return list[index];
    return null;
  }

  E? get first {
    Iterator<E> it = list.iterator;
    if (!it.moveNext()) {
      return null;
    }
    return it.current;
  }

  E? get last {
    Iterator<E> it = list.iterator;
    if (!it.moveNext()) {
      return null;
    }
    E result;
    do {
      result = it.current;
    } while (it.moveNext());
    return result;
  }

}