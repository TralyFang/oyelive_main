
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';

extension ContextExt on BuildContext? {
  Offset transformOffsetFrom(BuildContext? context) {
    if (context == null) return Offset.zero;
    logger.v("transformOffsetFrom context: $context, object: ${context.findRenderObject()}, this: $this, object: ${this!.findRenderObject()}");
    // RenderBox renderBox = this.findRenderObject();
    // return renderBox.localToGlobal(Offset.zero);

    /// this context 需要隶属同一个context节点下， 不太明白，直接抽离出来context为什么报错
    RenderObject beginRenderBox = this!.findRenderObject()!;
    var beginV3 = beginRenderBox
        .getTransformTo(context.findRenderObject())
        .getTranslation();
    return Offset(
        beginV3[0], beginV3[1]);
  }

  /// 自动根据中间值偏移坐标，不存在就返回空
  Offset? startOffsetFrom(BuildContext context, double halfHeight) {
    Offset start = this.transformOffsetFrom(context);
    logger.v("YBDTPRoomBeansEvent listen boot start: $start, half: $halfHeight");

    // 过滤不是玩家的座位
    if (start == null || start == Offset.zero) {
      return null;
    }

    double offsetY = -80.dp750 as double;
    if (start.dy < halfHeight) { // 在上面，那就从下面出
      offsetY = 80.dp750 as double;
      double spaceY = this!.findRenderObject()!.paintBounds.size.height*1/2;
      start = Offset(start.dx, start.dy + spaceY);
    }
    return start;
  }
}