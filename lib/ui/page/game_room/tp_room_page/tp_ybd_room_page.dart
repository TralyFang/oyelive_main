import 'dart:async';

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wakelock/wakelock.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_level.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/entity/tp_ybd_room_entity.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/socket/tp_ybd_game_socket_api.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_boot_beans.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_fly_bean_playing.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_background.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_footer.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_header.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/widget/tp_ybd_room_playing.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:bubble_box/bubble_box.dart';

import 'widget/bubble_ybd_layer.dart';

class YBDTPRoomPage extends StatefulWidget {
  String? roomId;
  String? socketUrl;

  YBDTPRoomPage({this.roomId, this.socketUrl});

  @override
  YBDTPRoomPageState createState() => YBDTPRoomPageState();
}

class YBDTPRoomPageState extends BaseState<YBDTPRoomPage> {
  YBDTPRoomGetLogic logic = Get.put(YBDTPRoomGetLogic());

  Timer? requestTimer;
  YBDBubbleController bubbleController = YBDBubbleController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // 784.0, width:360.0
    logger.v('screenSize.height:${SizeNum.screentHeight}, width:${SizeNum.screentWidth}');

    if (widget.roomId == null || widget.socketUrl == null) {
      logger.e('tp room page params is empty');
      Navigator.pop(context);
      return;
    }

    /// TODO: 测试begin
    if (Const.TEST_ENV && YBDTPRoomGetLogic.openTestData) {
      testReqeust();
      return;
    }

    /// TODO: 测试end

    /// 链接socket
    YBDTPGameSocket.instance.initSocket(widget.socketUrl, // "ws://192.168.5.175:8080/socket"
        config: YBDTPSocketConfig.defaultConfig()..roomId = widget.roomId);

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      // 保持屏幕常亮
      Wakelock.enable();
    });
  }
  void initStateZ140coyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  testReqeust() {
    getMockData();
    requestTimer = Timer.periodic(Duration(milliseconds: 5000), (timer) {
      getMockData();
    });
  }

  getMockData() async {
    /// 测试代码
    YBDTpRoomEntity? entity = await YBDHttpUtil.getInstance().doGet<YBDTpRoomEntity>(context, '', baseUrl: 'http://121.37.214.86:9090/mock/159/tp/room/info');
    logger.v('getMockData: ${entity?.toJson()}');
    YBDTPGameSocketApi.instance.publishMessage(entity);
  }

  getTimestamp() {
    // http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp
    // http://vv.video.qq.com/checktime?otype=json
  }

  @override
  Widget myBuild(BuildContext context) {
    logger.v('myBuild YBDTPRoomPage');

    return WillPopScope(
      onWillPop: () {
        return Future<bool>.value(false);
      },
      child: Scaffold(
        body: GestureDetector(
          onTap: () {
            logger.v('tp game room page tap');
          },
          child: Stack(
            children: [
              /// 背景图
              YBDTPRoomBackground(),

              /// 底部色块，铺满安全区域
              Positioned(
                left: 0,
                bottom: 0,
                child: IgnorePointer(child: YBDTPRoomFooterBar()),
              ),

              Container(
                /// 内容缩进
                padding: EdgeInsets.only(top: SizeNum.statusBarHeight + (20.dp750 as double), bottom: 65.dpBottomElse(0) as double),
                // color: Colors.blue.withOpacity(0.7),
                child: Builder(builder: (BuildContext context) {
                  logger.v('Builder YBDTPRoomPage');

                  return Stack(
                    children: <Widget>[
                      /// battleId展示
                      YBDTPBattleIdTips(),

                      Column(
                        children: <Widget>[
                          /// 头部主播信息
                          YBDTPRoomHeader(),

                          /// 游戏玩家区域
                          Expanded(
                            child: Stack(
                              children: [
                                YBDTPRoomPlaying(),
                                YBDBubbleLayer(bubbleController),
                              ],
                            ),
                          ),

                          /// 底部操作模块
                          YBDTPRoomFooter(),
                        ],
                      ),

                      /// 下注动画
                      YBDTPFlyBeanPlaying(context),
                    ],
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void myBuildKx01doyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    Get.delete<YBDTPRoomGetLogic>();
    requestTimer?.cancel();
    requestTimer = null;
    YBDTPGameSocket.instance.dispose();
    super.dispose();
  }
  void disposeyM0Owoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
