import 'dart:async';

import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/get_x/tp_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/util/tp_ybd_cache.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';

const tgp_topic_name = '/game/tp-match';

const game_tpg = 'tp';

class TpConst {
  static double img_x = 0.15;
  static double img_y = 0.3;
  static final double imgTY = -0.4; // 上面
  static final double imgBY = 0.3; // 下面
  static double img_center_x = 0;
  static double img_center_y = 0.83;

  ///随机color int值
  static int color_num = 255;

  ///下注启动 动效时间
  static int make_bet_millseconds = 300;

  ///下注启动 后续动效时间
  static int make_bet_second_millseconds = 600;

  /********************tp cmd *************************/
  /// 跟注/下注
  static const String BetCmd = 'bet';

  /// 看牌
  static const String SeeCardCmd = 'seeCard';

  /// 比牌
  static const String CompareCardCmd = 'compareCard';

  /// 开牌
  static const String ShowCardCmd = 'showCard';

  /// 弃牌
  static const String PackCardCmd = 'packCard';

  /// 退出房间
  static const String ExitRoomCmd = 'exitRoom';

  /// 站立
  static const String StandUpCmd = 'standUp';

  /// 坐下
  static const String SitDownCmd = 'sitDown';

  /// 房主踢人
  static const String KickPlayer = 'kickPlayer';

  /// 被系统站起的提示
  static const String SelfStandUp = 'selfStandUp';

  /// 被房主站起的提示
  static const String BeKicked = 'beKicked';

  /// 用户余额不足
  static const int code_insufficient = 091011;

  /// 座位被占
  static const int code_seat_occupied = 091012;
}

class YBDTPAudio {
  static const String _prefix = 'assets/images/tp_room/sounds/';
  static const String _suffix = '.mp3';

  /// 选庄
  static String choose = appending('choose');

  /// 自己回合倒计时
  static String yourTurn = appending('your_turn');

  /// 胜利
  static String win = appending('win');

  /// 失败
  static String lose = appending('lose');

  /// 看牌
  static String see = appending('see');

  /// 点击
  static String click = appending('click');

  /// 洗牌
  static String shuffle = appending('shuffle');

  /// 比牌
  static String compare = appending('compare');

  /// 投注
  static String bidBean = appending('bean');

  /// 弃牌
  static String pack = appending('pack');

  /// 定庄
  static String dingzhuang = appending('dingzhuang');

  /// 结算
  static String reward = appending('reward');

  /// 发牌
  static String dealCard = appending('deal_card');

  static String appending(String name) {
    return _prefix + name + _suffix;
  }
}

extension StringAudio on String {
  void play() {
    logger.v('StringAudio play: $this');
    String tpPath = YBDGameResource.repalceTPFilePath(this);
    YBDTPRoomGetLogic.findSync()?.playAudio(tpPath);
  }
  void playNkD44oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
