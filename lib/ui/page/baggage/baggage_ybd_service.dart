import 'dart:async';


import 'package:get/get.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/card_ybd_frame_store_entity.dart';

import '../../../base/base_ybd_refresh_list_state.dart';
import '../../../common/abstract/base_ybd_refresh_service.dart';
import '../../../common/constant/const.dart';

class YBDBaggageService extends BaseRefreshService {
  @override
  void refresh(
      {YBDBaseModel? model,
      int page = 1,
      int pageSize = Const.PAGE_SIZE,
      Function(List? list, bool isSuccess)? block}) async {
    int loadStart = page > 0 ? (page - 1) * pageSize : page;
    switch (this.type) {
      case HttpType.QueryBaggageVip:
        await ApiHelper.queryUserVipInfo(Get.context, (this.model as YBDBaggageQuery).userId).then((userVipInfoEntity) {
          if (userVipInfoEntity?.record == null || userVipInfoEntity!.record!.isEmpty) {
            block!([], false);
          } else {
            block!(userVipInfoEntity.record!.first!.itemList, true);
          }
        });

        break;
      case HttpType.QueryBaggageGift:
        ApiHelper.queryUserGiftsInfo(Get.context, userId: (this.model as YBDBaggageQuery).userId)
            .then((baggageGiftEntity) {
          // ApiTest.baggageUserEntranceInfo().then((userFrameEntity) {
          if (baggageGiftEntity?.record == null || baggageGiftEntity!.record!.isEmpty) {
            block!([], false);
          } else {
            block!(baggageGiftEntity.record!.first!.itemList, true);
          }
        });

        break;
      case HttpType.QueryBaggageTheme:
        ApiHelper.queryUserThemeInfo(Get.context, userId: (this.model as YBDBaggageQuery).userId).then((userThemeEntity) {
          // ApiTest.baggageUserEntranceInfo().then((userThemeEntity) {
          if (userThemeEntity?.record == null || userThemeEntity!.record!.isEmpty) {
            block!([], false);
          } else {
            block!(userThemeEntity.record!.first!.itemList, true);
          }
        });

        break;
      case HttpType.QueryBaggageFrame:
        ApiHelper.queryUserFrameInfo(Get.context, userId: (this.model as YBDBaggageQuery).userId).then((frameEntity) {
          // ApiTest.baggageUserEntranceInfo().then((userThemeEntity) {
          if (frameEntity?.record == null || frameEntity!.record!.isEmpty) {
            block!([], false);
          } else {
            block!(frameEntity.record!.first!.itemList, true);
          }
        });

        break;
      case HttpType.QueryBaggageBubble:
        ApiHelper.queryBaggageBubbleList(Get.context, userId: (this.model as YBDBaggageQuery).userId).then((bubbleEntity) {
          // ApiTest.baggageUserEntranceInfo().then((userThemeEntity) {
          if (bubbleEntity?.record == null || bubbleEntity!.record!.isEmpty) {
            block!([], false);
          } else {
            block!(bubbleEntity.record!.first!.itemList, true);
          }
        });

        break;
      case HttpType.QueryBaggageUId:
        ApiHelper.queryBaggageUidList(Get.context, userId: (this.model as YBDBaggageQuery).userId).then((uIdEntity) {
          // ApiTest.baggageUserEntranceInfo().then((userThemeEntity) {
          if (uIdEntity?.record == null || uIdEntity!.record!.isEmpty) {
            block!([], false);
          } else {
            block!(uIdEntity.record!.first!.itemList, true);
          }
        });

        break;
    }
  }
}

class YBDBaggageQuery extends YBDBaseModel {
  int? userId;
}
