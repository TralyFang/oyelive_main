import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import '../../../base/base_ybd_state.dart';
import '../../../base/base_ybd_tabbar.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../module/user/util/user_ybd_util.dart';
import '../../../redux/app_ybd_state.dart';
import '../../widget/loading_ybd_circle.dart';
import '../store/bloc/entrances_ybd_block.dart';

/// 背包页面
class YBDBaggagePage extends StatefulWidget {
  /// 默认选中的索引
  final int? tabIndex;

  /// 查询当前背包的某个用户Id
  final int? userId;

  YBDBaggagePage({this.userId, this.tabIndex});

  @override
  _YBDBaggageState createState() => _YBDBaggageState();
}

class _YBDBaggageState extends BaseState<YBDBaggagePage> with SingleTickerProviderStateMixin {
  YBDEntrancesListBloc? bloc;

  /// 用户的 [YBDUserInfo] 和 [userId] 强关联
  YBDUserInfo? _userInfo;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
      // 上个页面没有传 [userId] 默认查看自己的背包
      if (null == widget.userId || widget.userId == 0 || YBDUserUtil.isCurrent(widget.userId ?? -1)) {
        _userInfo = store?.state.bean;
        // 刷新页面
        setState(() {});
        return;
      }
      logger.v('===baggage other userId ');
      // 获取其他用户的用户信息
      _requestUserInfo();
    });
  }
  void initStateL0tZRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposeKqTQ0oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    if (bloc == null) {
      bloc = YBDEntrancesListBloc(context);
    }
    return _content();
  }
  void myBuildumyQ4oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _content() {
    if (null == _userInfo) {
      // 正在加载用户信息
      // 跳转别人的背包导致跳转黑屏过渡
      // return YBDLoadingCircle();

    }
    return YBDTabBar(
      title: translate('baggage'),
      // tabArr: YBDTabBarModel.baggage(_userInfo, context),
      tabArr: YBDTabBarModel.baggage(_userInfo ??= YBDUserInfo()..id=widget.userId, context),
      switchPageCallBack: (index) {
        print('scroll to $index page');
      },
    );
  }
  void _contentyPjKWoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询用户信息
  void _requestUserInfo() {
    assert(widget.userId != null, 'invalid userId');
    ApiHelper.queryUserInfo(context, widget.userId).then((value) {
      if (null != value) {
        if (mounted) {
          setState(() {
            _userInfo = value;
          });
        }
      }
    });
  }
}
