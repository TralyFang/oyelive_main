import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_record_extend.dart';

class YBDBaggageEntranceEntity with JsonConvert<YBDBaggageEntranceEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDBaggageEntranceRecord?>? record;
  String? recordSum;
}

class YBDBaggageEntranceRecord with JsonConvert<YBDBaggageEntranceRecord> {
  String? type;
  List<YBDBaggageEntranceRecordItemList?>? itemList;
}

class YBDBaggageEntranceRecordItemList with JsonConvert<YBDBaggageEntranceRecordItemList> {
  int? number;
  String? img;
  int? personalId;
  bool? equipped;
  int? price;
  String? name;
  int? expireAfter;
  int? id;
  YBDCarListRecordAnimationInfo? animationInfo;
  YBDCarListRecordExtend? replaceDto;
  String? currency;
  int? conditionType;
  String? conditionExtends;
}

// class BaggageEntranceRecordItemListAnimationInfo with JsonConvert<BaggageEntranceRecordItemListAnimationInfo> {
//   int animationId;
//   int animationType;
//   String foldername;
//   int framDuration;
//   String name;
//   int version;
// }
