import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDBaggageThemesEntity with JsonConvert<YBDBaggageThemesEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDBaggageThemesRecord?>? record;
  String? recordSum;
}

class YBDBaggageThemesRecord with JsonConvert<YBDBaggageThemesRecord> {
  String? type;
  List<YBDBaggageThemesRecordItemList?>? itemList;
}

class YBDBaggageThemesRecordItemList with JsonConvert<YBDBaggageThemesRecordItemList> {
  String? image;
  int? number;
  String? thumbnail;
  String? img;
  int? personalId;
  bool? equipped;
  int? price;
  String? name;
  int? expireAfter;
  int? id;
}
