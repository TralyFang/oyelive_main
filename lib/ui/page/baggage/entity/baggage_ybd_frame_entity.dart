import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDBaggageFrameEntity with JsonConvert<YBDBaggageFrameEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDBaggageFrameRecord?>? record;
  String? recordSum;
}

class YBDBaggageFrameRecord with JsonConvert<YBDBaggageFrameRecord> {
  String? type;
  List<YBDBaggageFrameRecordItemList?>? itemList;
}

class YBDBaggageFrameRecordItemList with JsonConvert<YBDBaggageFrameRecordItemList> {
  String? image;
  int? number;
  String? img;
  String? thumbnail;
  int? personalId;
  bool? equipped;
  int? price;
  String? name;
  int? expireAfter;
  int? id;
}
