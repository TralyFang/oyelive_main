import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDBaggageGiftEntity with JsonConvert<YBDBaggageGiftEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDBaggageGiftRecord?>? record;
  String? recordSum;
}

class YBDBaggageGiftRecord with JsonConvert<YBDBaggageGiftRecord> {
  String? type;
  List<YBDBaggageGiftRecordItemList?>? itemList;
}

class YBDBaggageGiftRecordItemList with JsonConvert<YBDBaggageGiftRecordItemList> {
  int? number;
  String? img;
  int? personalId;
  int? price;
  String? name;
  int? expireAfter;
  int? id;
  dynamic endTime;
  YBDBaggageGiftRecordItemListAnimationInfo? animationInfo;
}

class YBDBaggageGiftRecordItemListAnimationInfo with JsonConvert<YBDBaggageGiftRecordItemListAnimationInfo> {
  int? animationId;
  int? animationType;
  String? foldername;
  dynamic framDuration;
  String? name;
  int? version;
}
