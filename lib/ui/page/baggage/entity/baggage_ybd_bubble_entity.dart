import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDBaggageBubbleEntity with JsonConvert<YBDBaggageBubbleEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDBaggageBubbleRecord?>? record;
  String? recordSum;
}

class YBDBaggageBubbleRecord with JsonConvert<YBDBaggageBubbleRecord> {
  String? type;
  List<YBDBaggageBubbleRecordItemList?>? itemList;
}

class YBDBaggageBubbleRecordItemList with JsonConvert<YBDBaggageBubbleRecordItemList> {
  String? image;
  int? number;
  String? img;
  String? thumbnail;
  int? personalId;
  bool? equipped;
  int? price;
  String? name;
  String? currency;
  int? expireAfter;
  int? id;
}
