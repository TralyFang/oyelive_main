import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/local_ybd_animation_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../common/widget/local_ybd_animation_entity.dart';
import '../../../../main.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/result_ybd_bean.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../entity/baggage_ybd_entrance_entity.dart';
import '../../buy/buy_ybd_page.dart';
import '../../store/bloc/entrances_ybd_block.dart';
import '../../store/entity/carbean.dart';
import '../../store/handle/download_ybd_event.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';

/// 背包页面座驾列表项
class YBDBaggageEntranceItem extends StatelessWidget {
  final BuildContext context;

  /// 指定用户的 vip item
  final YBDUserInfo? userInfo;


  /// vip 信息
  final YBDBaggageEntranceRecordItemList? data;


  Function? onEquipSuccess;
  YBDCarBean? carBean;
  YBDBaggageEntranceItem(this.context, {this.userInfo, this.data, this.onEquipSuccess, this.carBean});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(30),
        vertical: ScreenUtil().setWidth(16),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
              color: Colors.white.withOpacity(0.1),
            ),
            child: Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(
                    ScreenUtil().setWidth(50),
                    ScreenUtil().setWidth(40),
                    0,
                    ScreenUtil().setWidth(40),
                  ),
                  child: _entranceImg(),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(80),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          _entranceName(),
                          SizedBox(width: ScreenUtil().setWidth(36)),
                          _playEntranceBtn(),
                        ],
                      ),
                      SizedBox(height: ScreenUtil().setWidth(30)),
                      _entranceExpiryDate(),
                      SizedBox(height: ScreenUtil().setWidth(45)),
                      Row(
                        children: <Widget>[
                          _equipButton(),
                          SizedBox(width: ScreenUtil().setWidth(10)),
                          _renewButton(),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void builda2J0Aoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 座驾标签
  Widget _entranceEquippedTag() {
    if (null != data && data!.equipped!) {
      return Image.asset(
        'assets/images/selected_flag.webp',
        width: ScreenUtil().setWidth(48),
      );
    } else {
      return Container();
    }
  }
  void _entranceEquippedTagD0n3foyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 座驾图片
  Container _entranceImg() {
    return Container(
      height: ScreenUtil().setWidth(200),
      width: ScreenUtil().setWidth(200),
      child: YBDNetworkImage(
        imageUrl: YBDImageUtil.car(context, data!.img, "D"),
        width: ScreenUtil().setWidth(48),
      ),
    );
  }

  /// 座驾名称
  Widget _entranceName() {
    return Container(
      constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(200)),
      child: Text(
        data?.name ?? '',
        style: TextStyle(
          color: Colors.white,
          fontSize: ScreenUtil().setSp(24),
          fontWeight: FontWeight.bold,
        ),
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
  void _entranceNameFufr1oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _circularDownloadProgress() {
    return GestureDetector(
      onTap: () {
        print('preview_btn------');
      },
      child: Container(
        width: ScreenUtil().setWidth(40),
        height: ScreenUtil().setWidth(40),
        child: LiquidCircularProgressIndicator(
          value: carBean!.progress!,
          //当前进度 0-1
          valueColor: AlwaysStoppedAnimation(Colors.blue[200]!),
          // 进度值的颜色.
          backgroundColor: Colors.black.withOpacity(0.3),

          direction: Axis.vertical, // 进度方向 (Axis.vertical = 从下到上, Axis.horizontal = 从左到右). 默认：Axis.vertical
        ),
      ),
    );
  }
  void _circularDownloadProgress96MAEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _playEntranceBtn() {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        YBDDelayGestureDetector(
          onTap: () async {
            logger.v('clicked play entrance btn');

            ///通过Id获取本地动画，没有则返回空，使用原来的逻辑，有的话直接播放
            YBDLocalAnimationItems? localAnimationItems = await YBDLocalAnimationUtil.getLocalAnimationById(carBean!.id);
            if (localAnimationItems != null) {
              eventBus.fire(YBDDownloadLoadEvent(localAnimationItems.svga, 0,
                  url_mp3: localAnimationItems.mp3, isLocal: true, replaceDto: data!.replaceDto));
            } else {
              // 队列下载并播放座驾动画
              context.read<YBDEntrancesListBloc>().download(carBean!.id);
            }
          },
          child: Container(
            padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
            child: Container(
              width: ScreenUtil().setWidth(40),
//          height: ScreenUtil().setWidth(40),
              child: Image.asset(
                'assets/images/profile/baggage_play.png',
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        if (carBean?.state == 3) _circularDownloadProgress()
      ],
    );
  }
  void _playEntranceBtn0CIwuoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 座驾过期时间
  Text _entranceExpiryDate() {
    var expiryDate = YBDDateUtil.getExpireDate(data!.expireAfter);

    return Text(
      '${translate('expiry_date')} : $expiryDate',
      style: TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(20),
      ),
    );
  }

  equip() async {
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.equipItem(context, personalId: data!.personalId, goodsType: "CAR");
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      onEquipSuccess?.call();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  unequip() async {
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.unequipItem(context, personalId: data!.personalId, goodsType: "CAR");
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      onEquipSuccess?.call();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  /// equip 按钮
  Widget _equipButton() {
    var isSelf = YBDUserUtil.isCurrent(userInfo?.id ?? -1);
    var equipTitle = translate('equip');
    var btnWidth = 134;

    if (data != null && (data?.equipped ?? false)) {
      equipTitle = translate('equipped');
      btnWidth = 140;
    }

    if (isSelf) {
      return YBDDelayGestureDetector(
        onTap: () {
          logger.v('clicked equip entrance btn');
          if (!data!.equipped!) {
            equip();
          } else {
            unequip();
          }
        },
        child: Container(
          // padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
          decoration: BoxDecoration(
            color: data!.equipped! ? Colors.white.withOpacity(0.2) : Colors.white,
            borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(37)))),
          ),
          width: ScreenUtil().setWidth(btnWidth),
          height: ScreenUtil().setWidth(48),
          alignment: Alignment.center,
          child: Text(
            equipTitle,
            style: TextStyle(
              color: data!.equipped! ? Colors.white : Color(0xff424242),
              fontSize: ScreenUtil().setSp(24),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }
  void _equipButtonxQZTxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Renew 按钮
  Widget _renewButton() {
    if (YBDUserUtil.isCurrent(userInfo?.id ?? -1)) {
      return YBDDelayGestureDetector(
        onTap: () {
          logger.v('clicked renew entrance btn');
          showModalBottomSheet(
            context: context,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                topRight: Radius.circular(8.0),
              ),
              side: BorderSide(style: BorderStyle.none),
            ),
            builder: (context) => YBDBuyBottomWindowPage(
              BuyType.car,
              data,
              null,
              null,
              null,
              onBuySuccess: () {
                onEquipSuccess?.call();
              },
              currency: data!.currency,
            ),
          );
        },
        child: Container(
//          padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color(0xff47CDCC),
                Color(0xff5E94E7),
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
            borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(37)))),
          ),
          width: ScreenUtil().setWidth(140),
          height: ScreenUtil().setWidth(48),
          alignment: Alignment.center,
          child: Text(
            translate('renew'),
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(24),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }
  void _renewButton9sUdkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
