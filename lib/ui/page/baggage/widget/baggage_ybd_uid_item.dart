import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/date_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/entity/baggage_ybd_uid_entity.dart';
import 'package:oyelive_main/module/entity/result_ybd_bean.dart';
import 'package:oyelive_main/module/entity/uid_ybd_list_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/entity/price_ybd_info_entity.dart';

import '../../../../module/api_ybd_helper.dart';
import '../../store/widget/bs_ybd_buy.dart';
import '../../store/widget/uid_ybd_buy_step.dart';

class YBDBaggageUidItem extends StatefulWidget {
  YBDBaggageUidRecordItemList? data;

  YBDBaggageUidItem(this.data, {this.onOperation});

  Function? onOperation;
  @override
  YBDBaggageUidItemState createState() => new YBDBaggageUidItemState();
}

class YBDBaggageUidItemState extends BaseState<YBDBaggageUidItem> {
  bool? isChosen = false;

  getDate() {
    return YBDDateUtil.getExpireDate(widget.data!.expireAfter);
  }

  equip() async {
    showLockDialog();
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.equipItem(context, personalId: widget.data!.personalId, goodsType: "UNIQUEID");
    dismissLockDialog();
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      widget.onOperation?.call();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  unequip() async {
    showLockDialog();
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.unequipItem(context, personalId: widget.data!.personalId, goodsType: "UNIQUEID");
    dismissLockDialog();
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      widget.onOperation?.call();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    isChosen = widget.data!.equipped;
    return Container(
      width: ScreenUtil().setWidth(672),
      height: ScreenUtil().setWidth(178),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
          color: Colors.white.withOpacity(isChosen! ? 0.2 : 0.16)),
      child: Row(
        children: [
          Container(
            width: ScreenUtil().setWidth(300),
            height: ScreenUtil().setWidth(178),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
              isChosen! ? "assets/images/uid_choosed.webp" : "assets/images/uid_no_choose.png",
            ))),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  "assets/images/u_crown_2x.webp",
                  width: ScreenUtil().setWidth(40),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Text(
                  widget.data!.name!,
                  style: TextStyle(
                      fontFamily: "sansita",
                      fontSize: ScreenUtil().setWidth(42),
                      color: Colors.white,
                      shadows: [
                        Shadow(
                          offset: Offset(0, ScreenUtil().setWidth(2)),
                          blurRadius: ScreenUtil().setWidth(4),
                          color: Colors.black.withOpacity(0.5),
                        ),
                      ]),
                )
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                SizedBox(
                  height: ScreenUtil().setWidth(44),
                ),
                Text(
                  "Expiry Date: ${getDate()}",
                  style: TextStyle(
                    fontSize: ScreenUtil().setWidth(20),
                    color: Colors.white.withOpacity(0.85),
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setWidth(36),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    GestureDetector(
                      onTap: () {
                        widget.data!.equipped! ? unequip() : equip();
                      },
                      child: Container(
                        width: ScreenUtil().setWidth(140),
                        height: ScreenUtil().setWidth(48),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
                            color: isChosen! ? Colors.white.withOpacity(0.3) : Colors.white),
                        child: Center(
                          child: Text(
                            translate(isChosen! ? "equipped" : "equip"),
                            style: TextStyle(
                                color: isChosen! ? Colors.white : Color(0xff7125C2), fontSize: ScreenUtil().setSp(24)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(28),
                    ),
                    GestureDetector(
                      onTap: () async {
                        List<YBDPriceInfoEntity?>? uidDiscout = await YBDSPUtil.getUidDiscount();
                        // YBDUserInfo userInfo = await YBDUserUtil.userInfo();
                        // showModalBottomSheet(
                        //   context: context,
                        //   shape: RoundedRectangleBorder(
                        //     borderRadius: BorderRadius.only(
                        //       topLeft: Radius.circular(ScreenUtil().setWidth(16)),
                        //       topRight: Radius.circular(ScreenUtil().setWidth(16)),
                        //     ),
                        //     side: BorderSide(style: BorderStyle.none),
                        //   ),
                        //   builder: (context) => BuyUidSheet(
                        //     YBDUidListRecord()
                        //       ..id = widget.data.id
                        //       ..name = widget.data.name
                        //       ..price = widget.data.price,
                        //     uidDiscout,
                        //     userInfo,
                        //     onBuySuccess: () {
                        //       widget.onOperation?.call();
                        //     },
                        //     isRenew: true,
                        //   ),
                        // );

                        YBDUidBuyStep uidBuyStep = new YBDUidBuyStep(
                            uidDiscout,
                            YBDUidListRecord()
                              ..id = widget.data!.id
                              ..name = widget.data!.name
                              ..price = widget.data!.price,
                            true);
                        showModalBottomSheet(
                          context: context,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0),
                            ),
                            side: BorderSide(style: BorderStyle.none),
                          ),
                          builder: (context) => YBDBuySheet(
                            buyStep: uidBuyStep,
                            onSuccessBuy: () {
                              widget.onOperation?.call();
                            },
                            currency: widget.data!.currency,
                          ),
                        );
                      },
                      child: Container(
                        width: ScreenUtil().setWidth(140),
                        height: ScreenUtil().setWidth(48),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
                          gradient: LinearGradient(
                            colors: [
                              Color(0xff47CDCC),
                              Color(0xff5E94E7),
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                          ),
                        ),
                        child: Center(
                          child: Text(
                            translate("renew"),
                            style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void myBuildMykWjoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatephA3moyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDBaggageUidItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetUX01royelive(YBDBaggageUidItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
