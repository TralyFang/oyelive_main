import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/widget/svga_ybd_icon.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../entity/baggage_ybd_themes_entity.dart';

class YBDBaggageThemesItem extends StatefulWidget {
  YBDBaggageThemesRecordItemList? data;

  YBDBaggageThemesItem(this.data);

  @override
  YBDBaggageThemesItemState createState() => new YBDBaggageThemesItemState();
}

class YBDBaggageThemesItemState extends BaseState<YBDBaggageThemesItem> {
  @override
  Widget myBuild(BuildContext context) {
    return Container(
      width: 335.px,
      height: 535.px,
      child: Stack(
        children: [
          Center(
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
              child: Container(
                width: ScreenUtil().setWidth(330),
                height: ScreenUtil().setWidth(530),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
                ),
                child: Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: Column(
                        children: [
                          _itemImg(),
                          Expanded(
                            child: Container(
                              color: Colors.white.withOpacity(0.1),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: ScreenUtil().setWidth(24),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: ScreenUtil().setWidth(12),
                                      ),
                                      Text(
                                        widget.data!.name!,
                                        style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: ScreenUtil().setWidth(4),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: ScreenUtil().setWidth(12),
                                      ),
                                      Text(
                                        '${translate('expiry_date')} : ${YBDDateUtil.getExpireDate(widget.data!.expireAfter)}',
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(20), color: Colors.white.withOpacity(0.7)),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          if (widget.data!.equipped!)
            Positioned(
              right: -2.px,
              top: 7.px,
              child: Image.asset('assets/images/icon_equip.webp', width: 75.px),
            )
        ],
      ),
    );
  }
  void myBuildSB1SXoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item图片
  Widget _itemImg() {
    return Stack(
      children: [
        YBDNetworkImage(
          imageUrl: _imgUrl(),
          fit: BoxFit.cover,
          width: ScreenUtil().setWidth(330),
          height: ScreenUtil().setWidth(420),
        ),
        Positioned(
          top: ScreenUtil().setWidth(20),
          right: ScreenUtil().setWidth(20),
          child: YBDSvgaIcon(imgUrl: widget.data!.image ?? widget.data!.img),
        ),
      ],
    );
  }
  void _itemImgbkcZBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 返回预览图url
  String _imgUrl() {
    if (YBDCommonUtil.isSvgaFile(widget.data!.img)) {
      return YBDImageUtil.themeTh(context, widget.data!.thumbnail, "A");
    }

    return YBDImageUtil.theme(context, widget.data!.img, "A");
  }
  void _imgUrl3C50Royelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDBaggageThemesItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetayP22oyelive(YBDBaggageThemesItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
