import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/ui/page/baggage/baggage_ybd_service.dart';
import 'package:oyelive_main/ui/page/baggage/widget/base_ybd_baggage_content.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/result_ybd_bean.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../entity/baggage_ybd_frame_entity.dart';
import 'baggage_ybd_micframe_item.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';

/// 背包 entrance 标签内容
class YBDBaggageMicframeContent extends StatefulWidget with BaseBaggageContent {
  /// 指定用户的 entrance 页面
  final YBDUserInfo userInfo;

  YBDBaggageMicframeContent(this.userInfo);

  @override
  State<StatefulWidget> createState() => _YBDBaggageMicframeContentState();
}

class _YBDBaggageMicframeContentState extends BaseRefreshListState<YBDBaggageMicframeContent> {
  @override
  void initState() {
    super.initState();
  }
  void initStatetwrTIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposeZOfT7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool asFragmentPage = true;

  equip(index) async {
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.equipItem(context, personalId: list[index].personalId, goodsType: "FRAME");
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      refresh();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  unequip(index) async {
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.unequipItem(context, personalId: list[index].personalId, goodsType: "FRAME");
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      refresh();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  @override
  customItemLayout(List<Widget> children) {
    return GridView.count(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10), vertical: ScreenUtil().setWidth(20)),
      crossAxisCount: 2,
      childAspectRatio: 1,
      mainAxisSpacing: ScreenUtil().setWidth(10),
      crossAxisSpacing: ScreenUtil().setWidth(10),
      children: children,
    );
  }

  @override
  Widget baseBuild(BuildContext context, int index) {
    // TODO: implement baseBuild
    return GestureDetector(
        onTap: () {
          if (!list[index].equipped) {
            equip(index);
          } else {
            unequip(index);
          }
        },
        child: YBDBaggageFrameItem(list[index]));
  }
  void baseBuildByxq8oyelive(BuildContext context, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  refresh() {
    // TODO: implement refresh
    service(YBDBaggageService()
      ..type = HttpType.QueryBaggageFrame
      ..model = (YBDBaggageQuery()..userId = (widget as YBDBaggageMicframeContent).userInfo.id));
  }

  @override
  Widget emptyView() {
    // TODO: implement emptyView

    if ((widget as YBDBaggageMicframeContent).userInfo.id == YBDCommonUtil.getUserInfoInstanly()!.id)
      return baggageEmptyView(index: widget.storeTabIndex());
    return super.emptyView();
  }
  void emptyViewoP4JKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
