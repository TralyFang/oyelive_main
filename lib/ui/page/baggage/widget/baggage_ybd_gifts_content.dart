import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/ui/page/baggage/widget/base_ybd_baggage_content.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../baggage_ybd_service.dart';
import '../entity/baggage_ybd_gift_entity.dart';
import 'baggage_ybd_gifts_item.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';

/// 背包 gift 标签内容
class YBDBaggageGiftContent extends StatefulWidget with BaseBaggageContent {
  /// 指定用户的 entrance 页面
  final YBDUserInfo userInfo;

  YBDBaggageGiftContent(this.userInfo);

  @override
  State<StatefulWidget> createState() => _YBDBaggageGiftContentState();
}

class _YBDBaggageGiftContentState extends BaseRefreshListState<YBDBaggageGiftContent> {
  @override
  void initState() {
    super.initState();
  }
  void initStateLsZZcoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposeWprrcoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool asFragmentPage = true;

  @override
  customItemLayout(List<Widget> children) {
    return GridView.count(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24), vertical: ScreenUtil().setWidth(20)),
      crossAxisCount: 2,
      childAspectRatio: 1,
      mainAxisSpacing: ScreenUtil().setWidth(10),
      crossAxisSpacing: ScreenUtil().setWidth(10),
      children: children,
    );
  }

  @override
  Widget baseBuild(BuildContext context, int index) {
    // TODO: implement baseBuild
    return GestureDetector(
        child: GestureDetector(
            onTap: () {
              logger.v("baggage gift content pull down refresh");
            },
            child: YBDDiscountListItem(index, list[index])));
  }
  void baseBuildsWFKfoyelive(BuildContext context, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  refresh() {
    // TODO: implement refresh
    service(YBDBaggageService()
      ..type = HttpType.QueryBaggageGift
      ..model = (YBDBaggageQuery()..userId = (widget as YBDBaggageGiftContent).userInfo.id));
  }

  @override
  Widget emptyView() {
    // TODO: implement emptyView

    if ((widget as YBDBaggageGiftContent).userInfo.id == YBDCommonUtil.getUserInfoInstanly()!.id)
      return baggageEmptyView(index: widget.storeTabIndex());
    return super.emptyView();
  }
  void emptyViewnG7mLoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
