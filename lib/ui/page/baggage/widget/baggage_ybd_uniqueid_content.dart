import 'dart:async';


import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/entity/baggage_ybd_uid_entity.dart';
import 'package:oyelive_main/ui/page/baggage/baggage_ybd_service.dart';
import 'package:oyelive_main/ui/page/baggage/widget/baggage_ybd_uid_item.dart';
import 'package:oyelive_main/ui/page/baggage/widget/base_ybd_baggage_content.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';
import 'package:oyelive_main/ui/page/store/entity/discount_ybd_entity.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/unique_ybd_id_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';

class YBDBaggageUniqueIdContent extends StatefulWidget with BaseBaggageContent {
  final YBDUserInfo userInfo;

  YBDBaggageUniqueIdContent(this.userInfo);

  @override
  YBDBaggageUniqueIdContentState createState() => new YBDBaggageUniqueIdContentState();
}

class YBDBaggageUniqueIdContentState extends BaseRefreshListState<YBDBaggageUniqueIdContent> {
  _discountInfo() async {
    //获取折扣信息
    YBDDiscountEntity? discountEntity = await ApiHelper.queryDiscountList(context, "UNIQUEID");
    if (discountEntity != null && discountEntity.returnCode == Const.HTTP_SUCCESS) {
      YBDSPUtil.save(Const.SP_KEY_DISCOUNT_UID, json.encode(discountEntity.record!.toJson()));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _discountInfo();
  }
  void initStateptVERoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  void disposeIByXJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDBaggageUniqueIdContent oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetpJnoPoyelive(YBDBaggageUniqueIdContent oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    throw UnimplementedError();
  }
  void myBuildQ6yH7oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool asFragmentPage = true;

  @override
  Widget baseBuild(BuildContext context, int index) {
    // TODO: implement baseBuild
    return YBDBaggageUidItem(
      list[index],
      onOperation: () {
        refresh();
      },
    );
  }

  @override
  Widget separatorItem(int index) {
    // TODO: implement separatorItem
    return SizedBox(
      height: ScreenUtil().setWidth(20),
    );
  }
  void separatorItemNMpvyoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  EdgeInsetsGeometry lvPadding() {
    // TODO: implement lvPadding
    return EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24), vertical: ScreenUtil().setWidth(20));
  }

  @override
  refresh() {
    // TODO: implement refresh
    service(YBDBaggageService()
      ..type = HttpType.QueryBaggageUId
      ..model = (YBDBaggageQuery()..userId = (widget as YBDBaggageUniqueIdContent).userInfo.id));
  }

  @override
  Widget emptyView() {
    // TODO: implement emptyView

    if ((widget as YBDBaggageUniqueIdContent).userInfo.id == YBDCommonUtil.getUserInfoInstanly()!.id)
      return baggageEmptyView(index: widget.storeTabIndex());
    return super.emptyView();
  }
  void emptyViewYy00royelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
