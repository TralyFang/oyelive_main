import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../entity/baggage_ybd_gift_entity.dart';

/// Discount列表 item
class YBDDiscountListItem extends StatefulWidget {
  /// item 在列表中的位置
  final int curIndex;

  /// 列表数据源
  final YBDBaggageGiftRecordItemList? baggageGiftRecordItemList;

  YBDDiscountListItem(this.curIndex, this.baggageGiftRecordItemList);

  @override
  _YBDDiscountListItemState createState() => _YBDDiscountListItemState();
}

class _YBDDiscountListItemState extends BaseState<YBDDiscountListItem> {
  @override
  void initState() {
    super.initState();
  }
  void initStatevH2Saoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String? url;

  @override
  Widget myBuild(BuildContext context) {
    if (widget.baggageGiftRecordItemList == null) {
      return Container();
    }

    url = YBDImageUtil.getDiscountImgUrl(context, 4, widget.baggageGiftRecordItemList?.img);
    // print("-----------------url :$url");
    return Container(
      width: ScreenUtil().setWidth(350),
      decoration: BoxDecoration(
        color: Color(0x26FFFFFF),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
      ),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              // 弹出底部购买弹框
              /*    showModalBottomSheet(
                context: context,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8.0),
                    topRight: Radius.circular(8.0),
                  ),
                  side: BorderSide(style: BorderStyle.none),
                ),
                builder: (context) => YBDBuyGiftsBottomWindowPage(
                  BuyType.discount,
                  discountListRecord: widget.discountListRecord,
                ),
              );*/
            },
            child: Container(
              // item 图片
              height: ScreenUtil().setWidth(237),
              alignment: Alignment.bottomCenter,
              child: Stack(
                alignment: Alignment.topCenter,
                children: <Widget>[
                  Positioned(
                    top: ScreenUtil().setWidth(40),
                    child: Center(
                      child: YBDNetworkImage(
                        height: ScreenUtil().setWidth(200),
                        width: ScreenUtil().setWidth(140),
                        imageUrl: url!,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Positioned(
                      right: ScreenUtil().setWidth(14),
                      top: ScreenUtil().setWidth(18),
                      child: (widget.baggageGiftRecordItemList?.expireAfter == -1)
                          ? Container()
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                  Image.asset(
                                    'assets/images/icon_clocks.webp',
                                    width: ScreenUtil().setWidth(20),
                                    height: ScreenUtil().setWidth(20),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(10)),
                                  Text(
                                    ///到期时间
                                    '${YBDDateUtil.getExpireDateDD(widget.baggageGiftRecordItemList?.expireAfter ?? 0)}',
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(18),
                                      color: Color(0xffeaeaea),
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )
                                ]))
                ],
              ),
            ),
          ),
          Container(
            // 图片底部部分
            height: ScreenUtil().setWidth(93),
            decoration: BoxDecoration(
              color: Color(0x26FFFFFF),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
              ),
            ),
            child: Column(
              children: [
                Container(
                  height: ScreenUtil().setWidth(93),
                  padding: EdgeInsets.only(left: ScreenUtil().setWidth(22), right: ScreenUtil().setWidth(22)),
                  // color: Colors.amberAccent,
                  child: Row(
                    children: [
                      Text(
                        // 名称
                        widget.baggageGiftRecordItemList!.name ?? '',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(22),
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Expanded(child: Container()),
                      Text(
                        // 名称
                        'x ${widget.baggageGiftRecordItemList!.number ?? ''}',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(30),
                          color: Color(0xffffe927),
                          fontWeight: FontWeight.w400,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void myBuildnnPZloyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
