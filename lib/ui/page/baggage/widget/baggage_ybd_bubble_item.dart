import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../widget/svga_ybd_icon.dart';
import '../entity/baggage_ybd_bubble_entity.dart';

class YBDBaggageBubbleItem extends StatefulWidget {
  YBDBaggageBubbleRecordItemList? data;

  YBDBaggageBubbleItem(this.data);

  @override
  YBDBaggageBubbleItemState createState() => new YBDBaggageBubbleItemState();
}

class YBDBaggageBubbleItemState extends BaseState<YBDBaggageBubbleItem> {
  @override
  Widget myBuild(BuildContext context) {
    return Container(
      width: 335.px,
      height: 335.px,
      child: Stack(
        children: [
          Center(
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
              child: Container(
                width: ScreenUtil().setWidth(330),
                height: ScreenUtil().setWidth(330),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
                ),
                child: Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: ScreenUtil().setWidth(330),
                                height: ScreenUtil().setWidth(236),
                                color: Colors.white.withOpacity(0.1),
                                padding: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(55),
                                  vertical: ScreenUtil().setWidth(50),
                                ),
                                child: YBDNetworkImage(
                                  imageUrl: YBDImageUtil.frame(context, widget.data!.thumbnail, "C"),
                                  fit: BoxFit.fitWidth,
                                ),
                              ),
                              Positioned(
                                top: ScreenUtil().setWidth(20),
                                right: ScreenUtil().setWidth(20),
                                child: YBDSvgaIcon(imgUrl: widget.data!.image),
                              ),
                            ],
                          ),
                          Expanded(
                            child: Container(
                              color: Colors.white.withOpacity(0.2),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: ScreenUtil().setWidth(14),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: ScreenUtil().setWidth(12),
                                      ),
                                      Text(
                                        widget.data!.name!,
                                        style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: ScreenUtil().setWidth(4),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: ScreenUtil().setWidth(12),
                                      ),
                                      Text(
                                        '${translate('expiry_date')} : ${YBDDateUtil.getExpireDate(widget.data!.expireAfter)}',
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(20), color: Colors.white.withOpacity(0.7)),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          if (widget.data!.equipped!)
            Positioned(
              right: -2.px,
              top: 1.px,
              child: Image.asset('assets/images/icon_equip.webp', width: 75.px),
            )
        ],
      ),
    );
  }
  void myBuildi3qe0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatewgbKnoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDBaggageBubbleItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetTvapuoyelive(YBDBaggageBubbleItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
