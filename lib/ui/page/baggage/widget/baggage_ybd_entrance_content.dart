import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/baggage/widget/base_ybd_baggage_content.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_record_extend.dart';

import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../../store/bloc/entrances_ybd_block.dart';
import '../../store/handle/download_ybd_event.dart';
import '../../store/widget/svga_ybd_play.dart';
import '../entity/baggage_ybd_entrance_entity.dart';
import 'baggage_ybd_entrance_item.dart';

/// 背包 entrance 标签内容
class YBDBaggageEntranceContent extends StatefulWidget with BaseBaggageContent {
  /// 指定用户的 entrance 页面
  final YBDUserInfo userInfo;

  YBDBaggageEntranceContent(this.userInfo);

  @override
  State<StatefulWidget> createState() => _YBDBaggageEntranceContentState();
}

class _YBDBaggageEntranceContentState extends State<YBDBaggageEntranceContent> with AutomaticKeepAliveClientMixin {
  List<YBDBaggageEntranceRecordItemList?>? _data;

  /// 是否为初始状态
  bool _isInitState = true;

  /// 列表刷新控制器
  RefreshController _refreshController = RefreshController();

  /// 切换标签时保持标签内容的状态
  @override
  bool get wantKeepAlive => true;

  bool _isPlaying = false;

  _playSvga(String? path, String? mp3file, {bool isLocal = false, YBDCarListRecordExtend? replaceDto}) async {
    logger.v('playSvga path : $path mp3 file $mp3file isLocal:$isLocal');

    if (mounted) {
      showDialog(
          barrierDismissible: true,
          context: context,
          builder: (BuildContext builder) {
            return YBDSvgaPlayerPage(
              url: path,
              mp3File: mp3file,
              callBack: playerStateCallBack,
              isLocal: isLocal,
              replaceSvgaEntity: replaceDto,
            );
          });
    }
  }

  /// 动画播放状态
  Future playerStateCallBack(bool isPlaying) async {
    logger.v('playerStateCallBack isPlaying:$_isPlaying');
    _isPlaying = isPlaying;
  }
  void playerStateCallBackR4hFyoyelive(bool isPlaying)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    _onRefresh();
    eventBus.on<YBDDownloadLoadEvent>().listen((YBDDownloadLoadEvent event) {
      if (event.result == 0) {
        logger.v('_isPlaying 0001:${_isPlaying}  event.url_svga:${event.url_svga}');
        if (_isPlaying) {
          return;
        }
        try {
          _playSvga(event.url_svga, event.url_mp3, isLocal: event.isLocal, replaceDto: event.replaceDto);
        } catch (e) {
          logger.v(e);
        }
      }
    });
  }
  void initStaterUHfBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == _data || _data!.isEmpty) {
      logger.v('entrance list is empty show placeholder');
      return _isInitState ? YBDLoadingCircle() : _placeHoldPage();
    }

    return Container(
      child: SmartRefresher(
        controller: _refreshController,
        header: YBDMyRefreshIndicator.myHeader,
        enablePullDown: true,
        enablePullUp: false,
        onRefresh: () {
          logger.v("baggage entrance content pull down refresh");
          _onRefresh();
        },
        child: BlocBuilder<YBDEntrancesListBloc, YBDEntrancesBlocState>(
          builder: (context, state) {
            return ListView.builder(
              itemCount: _data!.length,
              itemBuilder: (context, index) {
                return YBDBaggageEntranceItem(
                  context,
                  userInfo: widget.userInfo,
                  data: _data![index],
                  carBean: state.carBeans![index],
                  onEquipSuccess: () {
                    _onRefresh();
                  },
                );
              },
            );
          },
        ),
      ),
    );
  }
  void buildRiWi7oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _placeHoldPage() {
    if (YBDUserUtil.isCurrent(widget.userInfo.id ?? -1))
      return YBDPlaceHolderView(
        text: translate("no_item"),
        touchLister: () {
          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.store + '/2');
        },
      );
    else
      return YBDPlaceHolderView(
        text: 'No Data',
      );
  }
  void _placeHoldPage7YQSeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 刷新列表
  _onRefresh() async {
    ApiHelper.queryUserEntranceInfo(context, widget.userInfo.id).then((userEntranceInfoEntity) {
      // ApiTest.baggageUserEntranceInfo().then((userEntranceInfoEntity) {
      if (userEntranceInfoEntity?.record == null || userEntranceInfoEntity!.record!.isEmpty) {
        logger.v('user entrance info entity is null');
      } else {
        _data = userEntranceInfoEntity.record!.first!.itemList;
        // 加载座驾列表数据
        context.read<YBDEntrancesListBloc>().inertDataFromBaggage(_data);
      }
      if (_refreshController.isRefresh) _refreshController.refreshCompleted();
      _cancelInitState();
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }
}
