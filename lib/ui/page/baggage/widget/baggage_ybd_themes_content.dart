import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/baggage/baggage_ybd_service.dart';
import 'package:oyelive_main/ui/page/baggage/widget/base_ybd_baggage_content.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/result_ybd_bean.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../entity/baggage_ybd_themes_entity.dart';
import 'baggage_ybd_themes_item.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';

/// 背包 entrance 标签内容
class YBDBaggageThemesContent extends StatefulWidget with BaseBaggageContent {
  /// 指定用户的 entrance 页面
  final YBDUserInfo userInfo;

  YBDBaggageThemesContent(this.userInfo);

  @override
  State<StatefulWidget> createState() => _YBDBaggageThemesContentState();
}

class _YBDBaggageThemesContentState extends BaseRefreshListState<YBDBaggageThemesContent> {
  @override
  void initState() {
    super.initState();
  }
  void initStatezJjp7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposeuzkOUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  equip(index) async {
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.equipItem(context, personalId: list[index].personalId, goodsType: "THEME");
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      refresh();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  @override
  bool asFragmentPage = true;

  @override
  customItemLayout(List<Widget> children) {
    return GridView.count(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10), vertical: ScreenUtil().setWidth(20)),
      crossAxisCount: 2,
      childAspectRatio: 33 / 53,
      mainAxisSpacing: ScreenUtil().setWidth(10),
      crossAxisSpacing: ScreenUtil().setWidth(10),
      children: children,
    );
  }

  @override
  Widget baseBuild(BuildContext context, int index) {
    // TODO: implement baseBuild
    return GestureDetector(
        onTap: () {
          if (!list[index].equipped) {
            equip(index);
          }
        },
        child: YBDBaggageThemesItem(list[index]));
  }
  void baseBuildE4rgXoyelive(BuildContext context, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  refresh() {
    // TODO: implement refresh
    service(YBDBaggageService()
      ..type = HttpType.QueryBaggageTheme
      ..model = (YBDBaggageQuery()..userId = (widget as YBDBaggageThemesContent).userInfo.id));
  }

  @override
  Widget emptyView() {
    // TODO: implement emptyView

    if ((widget as YBDBaggageThemesContent).userInfo.id == YBDCommonUtil.getUserInfoInstanly()!.id)
      return baggageEmptyView(index: widget.storeTabIndex());
    return super.emptyView();
  }
  void emptyViewSey80oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
