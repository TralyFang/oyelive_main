import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/entity/vip_ybd_valid_info_entity.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';

/// 背包页面 vip 列表项
class YBDBaggageVipItem extends StatelessWidget {
  final BuildContext context;

  /// 当前用户的 id
  final int? mUserId;

  /// vip 信息
  final YBDVipValidInfoRecordItemList? data;

  YBDBaggageVipItem(this.context, {this.mUserId, this.data});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(380),
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(30),
        vertical: ScreenUtil().setWidth(16),
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
          color: Colors.white.withOpacity(0.1),
        ),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(50)),
              child: _vipIcon(),
            ),
            Expanded(child: Container()),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  _vipName(),
                  SizedBox(height: ScreenUtil().setWidth(30)),
                  _vipExpiryDate(),
                  SizedBox(height: ScreenUtil().setWidth(45)),
                  YBDScaleAnimateButton(
                    onTap: () {
                      logger.v('clicked renew btn');
                    },
                    child: _renewButton(),
                  ),
                ],
              ),
            ),
            SizedBox(width: ScreenUtil().setWidth(50)),
          ],
        ),
      ),
    );
  }
  void buildPrRv0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 是否为 pirate vip
  bool _isPirateVip() {
    if (data?.id == 2) {
      return true;
    } else {
      return false;
    }
  }
  void _isPirateVip6zKF6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// vip 图标
  Widget _vipIcon() {
    return CachedNetworkImage(
      imageUrl: data?.vipIcon ?? "",
      width: ScreenUtil().setWidth(220),
    );
    var icon = 'assets/images/ic_vip_prince.png';

    if (_isPirateVip()) {
      icon = 'assets/images/ic_vip_pirate.png';
    }

    return Container(
      height: ScreenUtil().setWidth(183),
      child: Image.asset(
        icon,
        fit: BoxFit.cover,
      ),
    );
  }
  void _vipIcon5HLjBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// vip 名称
  Text _vipName() {
    return Text(
      data?.vipName ?? '',
      style: TextStyle(
        color: Color(0xffffe927),
        fontSize: ScreenUtil().setSp(28),
      ),
    );
  }

  /// vip 过期时间
  Text _vipExpiryDate() {
    var expiryDate = YBDDateUtil.getExpireDate(data?.expireAfter);

    return Text(
      '${translate('expiry_date')} : $expiryDate',
      style: TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(24),
      ),
    );
  }

  /// Renew 按钮
  Widget _renewButton() {
    if (YBDUserUtil.isCurrent(mUserId ?? -1)) {
      return YBDDelayGestureDetector(
        onTap: () {
          logger.v('clicked renew vip btn');
          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.vip + "/${data?.index}");
        },
        child: Container(
          padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0xff5E94E7), Color(0xff47CDCC)],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
            borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(37)))),
          ),
          width: ScreenUtil().setWidth(200),
          height: ScreenUtil().setWidth(56),
          alignment: Alignment.center,
          child: Text(
            translate('renew'),
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(24),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }
  void _renewButtonS5u6Moyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
