import 'dart:async';


import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/baggage/baggage_ybd_service.dart';
import 'package:oyelive_main/ui/page/baggage/widget/baggage_ybd_bubble_item.dart';
import 'package:oyelive_main/ui/page/baggage/widget/base_ybd_baggage_content.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/result_ybd_bean.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../entity/baggage_ybd_bubble_entity.dart';

class YBDBaggageBubbleContent extends StatefulWidget with BaseBaggageContent {
  final YBDUserInfo userInfo;

  YBDBaggageBubbleContent(this.userInfo);

  @override
  YBDBaggageBubbleContentState createState() => new YBDBaggageBubbleContentState();
}

class YBDBaggageBubbleContentState extends BaseRefreshListState<YBDBaggageBubbleContent> {
  @override
  void initState() {
    super.initState();
  }
  void initStatediePVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposeFjyHcoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  equip(index) async {
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.equipItem(context, personalId: list[index].personalId, goodsType: "BUBBLE");
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      refresh();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  unequip(index) async {
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.unequipItem(context, personalId: list[index].personalId, goodsType: "BUBBLE");
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      refresh();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  @override
  void didUpdateWidget(YBDBaggageBubbleContent oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgettKwUKoyelive(YBDBaggageBubbleContent oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    throw UnimplementedError();
  }
  void myBuildSNxoxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool asFragmentPage = true;

  @override
  customItemLayout(List<Widget> children) {
    return GridView.count(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10), vertical: ScreenUtil().setWidth(20)),
      crossAxisCount: 2,
      childAspectRatio: 1,
      mainAxisSpacing: ScreenUtil().setWidth(10),
      crossAxisSpacing: ScreenUtil().setWidth(10),
      children: children,
    );
  }

  @override
  Widget baseBuild(BuildContext context, int index) {
    // TODO: implement baseBuild
    return GestureDetector(
        onTap: () {
          if (!list[index].equipped) {
            equip(index);
          } else {
            unequip(index);
          }
        },
        child: YBDBaggageBubbleItem(list[index]));
  }
  void baseBuildYkB7Aoyelive(BuildContext context, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  refresh() {
    // TODO: implement refresh
    service(YBDBaggageService()
      ..type = HttpType.QueryBaggageBubble
      ..model = (YBDBaggageQuery()..userId = (widget as YBDBaggageBubbleContent).userInfo.id));
  }

  @override
  Widget emptyView() {
    // TODO: implement emptyView

    if ((widget as YBDBaggageBubbleContent).userInfo.id == YBDCommonUtil.getUserInfoInstanly()!.id)
      return baggageEmptyView(index: widget.storeTabIndex());
    return super.emptyView();
  }
  void emptyViewR1kyCoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
