import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../common/constant/const.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../home/widget/bubble_ybd_widget.dart';

OverlayEntry? overlayEntryBack, overlayEntryMore, overlayEntryChange;
List<OverlayEntry?> overlayEntryList = <OverlayEntry?>[];
var overIndex, overListIndex;

///SignOrDialog 引导页面
itemGuideSignOrDialog(BuildContext context, Size? size) async {
  String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);
  print("_itemGuide SignOrDialog: $guide");
  if (guide != null && guide.contains(Const.GUIDE_SING_DIALOGUE)) {
    return;
  }

  if (guide == null || guide.isEmpty) {
    YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_SING_DIALOGUE);
  } else {
    YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_SING_DIALOGUE);
  }
  overIndex = 3;
  overlayEntryList.clear();
  overlayEntryBack = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      GestureDetector(
          onTap: () {
            --overIndex;
            if (overIndex == 2) {
              overlayEntryMore!.remove();
              overlayEntryList.removeLast();
              Overlay.of(context)!.insert(overlayEntryChange!);
            } else if (overIndex == 1) {
              overlayEntryChange!.remove();
              overlayEntryBack!.remove();
              overlayEntryList.removeLast();
            }
          },
          child: Container(color: Colors.black.withOpacity(0.4)))
    ]);
  });

  overlayEntryMore = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      Positioned(
          top: size!.height + ScreenUtil().statusBarHeight + ScreenUtil().setWidth(80),
          left: ScreenUtil().setWidth(50),
          child: GestureDetector(
              onTap: () {
                --overIndex;
                overlayEntryMore!.remove();
                overlayEntryList.removeLast();
                Overlay.of(context)!.insert(overlayEntryChange!);
              },
              child: _itemGuideMore()))
    ]);
  });
  overlayEntryList.add(overlayEntryBack);
  overlayEntryList.add(overlayEntryMore);
  overlayEntryChange = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      Positioned(
          top: size!.height + ScreenUtil().statusBarHeight - ScreenUtil().setWidth(80),
          right: ScreenUtil().setWidth(47),
          child: GestureDetector(
              onTap: () {
                --overIndex;
                overlayEntryChange!.remove();
                overlayEntryBack!.remove();
                overlayEntryList.removeLast();
              },
              child: _itemGuideChange()))
    ]);
  });
  Overlay.of(context)!.insertAll(overlayEntryList as Iterable<OverlayEntry>);
}

///Guide mood
_itemGuideMore() {
  return Material(
      color: Color.fromARGB(0, 0, 0, 0),
      child: Padding(
          padding: EdgeInsets.only(right: 1.0),
          child: Container(
              alignment: Alignment.topLeft,
              child: Row(
                children: [
                  YBDBubbleWidget(ScreenUtil().setWidth(327), ScreenUtil().setWidth(106), Color(0xFFFFFFFF),
                      BubbleArrowDirection.top,
                      length: 13.0,
                      child: Text(translate('status_guide_more'),
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Color(0xFF1C1C1C), fontSize: ScreenUtil().setSp(22)))),
                  Column(
                    children: [
                      SizedBox(
                        height: ScreenUtil().setWidth(10),
                      ),
                      Image.asset(
                        "assets/images/guide_icon.webp",
                        width: ScreenUtil().setWidth(104),
                        height: ScreenUtil().setWidth(86),
                      )
                    ],
                  ),
                ],
              ))));
}

///Guide Change
_itemGuideChange() {
  return Material(
      color: Color.fromARGB(0, 0, 0, 0),
      child: Padding(
          padding: EdgeInsets.only(right: 1.0),
          child: Container(
              alignment: Alignment.topLeft,
              child: Row(
                children: [
                  Column(
                    children: [
                      Image.asset(
                        "assets/images/guide_icon.webp",
                        width: ScreenUtil().setWidth(104),
                        height: ScreenUtil().setWidth(86),
                      ),
                      SizedBox(
                        height: ScreenUtil().setWidth(10),
                      )
                    ],
                  ),
                  YBDBubbleWidget(ScreenUtil().setWidth(216), ScreenUtil().setWidth(94), Color(0xFFFFFFFF),
                      BubbleArrowDirection.bottom,
                      child: Text(translate('status_guide_change_music'),
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Color(0xFF1C1C1C), fontSize: ScreenUtil().setSp(22)))),
                ],
              ))));
}

///Mood引导页面
OverlayEntry? overlayEntryMood;

itemGuideMood(BuildContext context) async {
  String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);
  print("_itemGuide guide: $guide");
  if (guide != null && guide.contains(Const.GUIDE_MOOD)) {
    return;
  }
  if (guide == null || guide.isEmpty) {
    YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_MOOD);
  } else {
    YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_MOOD);
  }
  overlayEntryList.clear();
  overIndex = 1;
  overlayEntryBack = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      GestureDetector(
          onTap: () {
            --overIndex;
            overlayEntryMood!.remove();
            overlayEntryBack!.remove();
            overlayEntryList.removeLast();
          },
          child: Container(color: Colors.black.withOpacity(0.4)))
    ]);
  });

  overlayEntryMood = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      Positioned(
          bottom: ScreenUtil().setWidth(134),
          left: ScreenUtil().setWidth(170),
          child: GestureDetector(
              onTap: () {
                --overIndex;
                overlayEntryMood!.remove();
                overlayEntryBack!.remove();
                overlayEntryList.removeLast();
              },
              child: _itemGuideMood()))
    ]);
  });
  overlayEntryList.add(overlayEntryBack);
  overlayEntryList.add(overlayEntryMood);
  Overlay.of(context)!.insertAll(overlayEntryList as Iterable<OverlayEntry>);
}

///Guide mood
_itemGuideMood() {
  return Material(
      color: Color.fromARGB(0, 0, 0, 0),
      child: Padding(
          padding: EdgeInsets.only(right: 4.0),
          child: Container(
              alignment: Alignment.topLeft,
              child: Row(
                children: [
                  YBDBubbleWidget(ScreenUtil().setWidth(380), ScreenUtil().setWidth(104), Color(0xFFFFFFFF),
                      BubbleArrowDirection.bottom,
                      child: Text(translate('status_guide_type'),
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Color(0xFF1C1C1C), fontSize: ScreenUtil().setSp(22)))),
                  Column(
                    children: [
                      Image.asset(
                        "assets/images/guide_icon.webp",
                        width: ScreenUtil().setWidth(104),
                        height: ScreenUtil().setWidth(86),
                      ),
                      SizedBox(
                        height: ScreenUtil().setWidth(10),
                      )
                    ],
                  ),
                ],
              ))));
}

///搜索  引导页面

OverlayEntry? overlayEntryOpload;

itemGuideUpload(BuildContext context) async {
  String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);
  print("_itemGuide guide upload: $guide");
  if (guide != null && guide.contains(Const.GUIDE_UPLOAD_MUSIC)) {
    return;
  }

  if (guide == null || guide.isEmpty) {
    YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_UPLOAD_MUSIC);
  } else {
    YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_UPLOAD_MUSIC);
  }
  overlayEntryList.clear();
  overIndex = 1;
  overlayEntryBack = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      GestureDetector(
          onTap: () {
            --overIndex;
            overlayEntryOpload!.remove();
            overlayEntryBack!.remove();
            overlayEntryList.removeLast();
          },
          child: Container(color: Colors.black.withOpacity(0.4)))
    ]);
  });

  overlayEntryOpload = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      Positioned(
          top: ScreenUtil().statusBarHeight + ScreenUtil().setWidth(60),
          right: ScreenUtil().setWidth(12),
          child: GestureDetector(
              onTap: () {
                --overIndex;
                overlayEntryOpload!.remove();
                overlayEntryBack!.remove();
                overlayEntryList.removeLast();
              },
              child: _itemGuideUpload()))
    ]);
  });
  overlayEntryList.add(overlayEntryBack);
  overlayEntryList.add(overlayEntryOpload);
  Overlay.of(context)!.insertAll(overlayEntryList as Iterable<OverlayEntry>);
}

_itemGuideUpload() {
  return Material(
      color: Color.fromARGB(0, 0, 0, 0),
      child: Padding(
          padding: EdgeInsets.only(right: 1.0),
          child: Container(
              alignment: Alignment.topLeft,
              child: Row(
                children: [
                  Image.asset(
                    "assets/images/guide_icon.webp",
                    width: ScreenUtil().setWidth(104),
                    height: ScreenUtil().setWidth(86),
                  ),
                  YBDBubbleWidget(ScreenUtil().setWidth(234), ScreenUtil().setWidth(71), Color(0xFFFFFFFF),
                      BubbleArrowDirection.top,
                      length: ScreenUtil().setWidth(190),
                      child: Text(translate('status_guide_local_music'),
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Color(0xFF1C1C1C), fontSize: ScreenUtil().setSp(22)))),
                ],
              ))));
}

///音频提交页面 Sing引导

OverlayEntry? overlayEntryAudioBackGround;

itemGuideAudioSing(BuildContext context, Size? size) async {
  String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);
  print("itemGuideAudioSing guide: $guide");
  if (guide != null && guide.contains(Const.GUIDE_BACKGROUND)) {
    return;
  }

  if (guide == null || guide.isEmpty) {
    YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_BACKGROUND);
  } else {
    YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_BACKGROUND);
  }
  overlayEntryList.clear();
  overIndex = 2;
  overlayEntryBack = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      GestureDetector(
          onTap: () {
            --overIndex;
            overlayEntryAudioBackGround!.remove();
            overlayEntryBack!.remove();
            overlayEntryList.removeLast();
          },
          child: Container(color: Colors.black.withOpacity(0.4)))
    ]);
  });

  overlayEntryAudioBackGround = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      Positioned(
          top: size!.height + ScreenUtil().statusBarHeight - ScreenUtil().setWidth(125),
          right: ScreenUtil().setWidth(40),
          child: GestureDetector(
              onTap: () {
                --overIndex;
                overlayEntryAudioBackGround!.remove();
                overlayEntryBack!.remove();
                overlayEntryList.removeLast();
              },
              child: _itemGuideAudioSing()))
    ]);
  });
  overlayEntryList.add(overlayEntryBack);
  overlayEntryList.add(overlayEntryAudioBackGround);
  Overlay.of(context)!.insertAll(overlayEntryList as Iterable<OverlayEntry>);
}

_itemGuideAudioSing() {
  return Material(
      color: Color.fromARGB(0, 0, 0, 0),
      child: Padding(
          padding: EdgeInsets.only(right: 1.0),
          child: Container(
              alignment: Alignment.topLeft,
              child: Row(
                children: [
                  Image.asset(
                    "assets/images/guide_icon.webp",
                    width: ScreenUtil().setWidth(104),
                    height: ScreenUtil().setWidth(86),
                  ),
                  YBDBubbleWidget(ScreenUtil().setWidth(161), ScreenUtil().setWidth(94), Color(0xFFFFFFFF),
                      BubbleArrowDirection.bottom,
                      child: Text(translate('status_guide_bg'),
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Color(0xFF1C1C1C), fontSize: ScreenUtil().setSp(22)))),
                ],
              ))));
}

/// 音频提交页面 音频提交页面  Mood or Dialogue引导

OverlayEntry? overlayEntryAudioMood;
bool? mood;

///mood---music   Dialogue--->volume？
itemGuideAudioMoodOrDialog(BuildContext context, Size? size) async {
  String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);
  print("_itemGuide AudioMoodOrDialog: $guide");
  if (guide != null && guide.contains(Const.GUIDE_MUSIC)) {
    return;
  }

  if (guide == null || guide.isEmpty) {
    YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_MUSIC);
  } else {
    YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_MUSIC);
  }
  /* //判断类型
  if (YBDAudioHandler.getInstance().audioStatusType == AudioStatusType.Mood) {
    mood = true;
  } else {
    mood = false;
  }
  //根据类型查找sb记录，查询是否打开过
  if (guide != null && mood && guide.contains(Const.GUIDE_UPLOAD_MUSIC)) {
    return;
  } else if (guide == null && !mood && guide.contains(Const.GUIDE_SING_DIALOGUE)) {
    return;
  } else if (mood) {
    if (guide == null || guide.isEmpty) {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_UPLOAD_MUSIC);
    } else {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_UPLOAD_MUSIC);
    }
  } else {
    if (guide == null || guide.isEmpty) {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_SING_DIALOGUE);
    } else {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_SING_DIALOGUE);
    }
  }*/
  overlayEntryList.clear();
  overIndex = 3;
  overlayEntryBack = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      GestureDetector(
          onTap: () {
            --overIndex;
            if (overIndex == 2) {
              overlayEntryAudioMood!.remove();
              overlayEntryList.removeLast();
              Overlay.of(context)!.insert(overlayEntryAudioBackGround!);
            } else if (overIndex == 1) {
              overlayEntryAudioBackGround!.remove();
              overlayEntryBack!.remove();
              overlayEntryList.removeLast();
            }
          },
          child: Container(color: Colors.black.withOpacity(0.4)))
    ]);
  });

  overlayEntryAudioMood = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      Positioned(
          top: size!.height + ScreenUtil().statusBarHeight - ScreenUtil().setWidth(138),
          right: ScreenUtil().setWidth(118),
          child: GestureDetector(
              onTap: () {
                --overIndex;
                overlayEntryAudioMood!.remove();
                overlayEntryList.removeLast();
                Overlay.of(context)!.insert(overlayEntryAudioBackGround!);
              },
              child: _itemGuideMusic()))
    ]);
  });
  overlayEntryList.add(overlayEntryBack);
  overlayEntryList.add(overlayEntryAudioMood);
  overlayEntryAudioBackGround = OverlayEntry(builder: (context) {
    return Stack(children: <Widget>[
      Positioned(
          top: size!.height + ScreenUtil().statusBarHeight - ScreenUtil().setWidth(125),
          right: ScreenUtil().setWidth(40),
          child: GestureDetector(
              onTap: () {
                --overIndex;
                overlayEntryAudioBackGround!.remove();
                overlayEntryBack!.remove();
                overlayEntryList.removeLast();
              },
              child: _itemGuideAudioSing()))
    ]);
  });
  Overlay.of(context)!.insertAll(overlayEntryList as Iterable<OverlayEntry>);
}

///Guide Music
_itemGuideMusic() {
  return Material(
      color: Color.fromARGB(0, 0, 0, 0),
      child: Padding(
          padding: EdgeInsets.only(right: 1.0),
          child: Container(
              alignment: Alignment.topLeft,
              child: Row(
                children: [
                  Image.asset(
                    "assets/images/guide_icon.webp",
                    width: ScreenUtil().setWidth(124),
                    height: ScreenUtil().setWidth(100),
                  ),
                  YBDBubbleWidget(ScreenUtil().setWidth(327), ScreenUtil().setWidth(118), Color(0xFFFFFFFF),
                      BubbleArrowDirection.bottom,
                      length: 40.0,
                      child: Text(translate('status_guide_choose_music'),
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Color(0xFF1C1C1C), fontSize: ScreenUtil().setSp(22)))),
                ],
              ))));
}

_itemGuideTop() {
  return Material(
      color: Color.fromARGB(0, 0, 0, 0),
      child: Padding(
          padding: EdgeInsets.only(right: 4.0),
          child: Container(
              child: YBDBubbleWidget(
                  ScreenUtil().setWidth(400), ScreenUtil().setWidth(120), Color(0xFF813bff), BubbleArrowDirection.top,
                  length: 10.0,
                  child: Text(translate('home_tip_support'),
                      textAlign: TextAlign.left, style: TextStyle(color: Colors.white, fontSize: 15.0))))));
}

_itemGuideBottom() {
  return Material(
      color: Color.fromARGB(0, 0, 0, 0),
      child: Padding(
          padding: EdgeInsets.only(right: 4.0),
          child: Container(
              child: YBDBubbleWidget(ScreenUtil().setWidth(400), ScreenUtil().setWidth(120), Color(0xFF813bff),
                  BubbleArrowDirection.bottom,
                  child: Text(translate('home_tip_live'),
                      textAlign: TextAlign.left, style: TextStyle(color: Colors.white, fontSize: 15.0))))));
}

OverlayEntry? overlayEntry, overlayEntry2, overlayEntry3, overlayEntry0;

/// 首页新手引导
// itemGuide(BuildContext context) async {
// String guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);
// print("_itemGuide itemGuide main: $guide");
// YBDTaskService.instance.end(TASK_SHOW_HOME_PAGE_GUIDE);
// return;
//   if (guide != null && guide.contains(Const.GUIDE_MAIN)) {
//     // 已完成新手引导任务
//     YBDTaskService.instance.end(TASK_SHOW_HOME_PAGE_GUIDE);
//     return;
//   }
//
//   if (guide == null || guide.isEmpty) {
//     YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_MAIN);
//   } else {
//     YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_MAIN);
//   }
// /*  if (go_live != null && go_live == true) {
//     return;
//   }*/
//   YBDSPUtil.save(Const.SP_KEY_TIP_GO_LIVE, true);
//   overIndex = 3;
//   overlayEntryList.clear();
//
//   // 新手引导蒙板
//   overlayEntry0 = OverlayEntry(builder: (context) {
//     return Stack(
//       children: <Widget>[
//         GestureDetector(
//           onTap: () {
//             --overIndex;
//             if (overIndex == 2) {
//               // 移除客服引导提示
//               overlayEntry.remove();
//               overlayEntryList.removeLast();
//               Overlay.of(context).insert(overlayEntry3);
//             } else if (overIndex == 1) {
//               overlayEntry3.remove();
//               overlayEntry0.remove();
//               // 已完成新手引导任务
//               YBDTaskService.instance.end(TASK_SHOW_HOME_PAGE_GUIDE);
//               overlayEntryList.removeLast();
//             }
//           },
//           child: Container(color: Colors.black.withOpacity(0.4)),
//         )
//       ],
//     );
//   });
//
//   // 新手引导：首页客服按钮
//   overlayEntry = OverlayEntry(builder: (context) {
//     return Stack(
//       children: <Widget>[
//         Positioned(
//             top: ScreenUtil().setWidth(480),
//             left: ScreenUtil().setWidth(32),
//             child: GestureDetector(
//                 onTap: () {
//                   --overIndex;
//                   overlayEntry.remove();
//                   overlayEntryList.removeLast();
//                   Overlay.of(context).insert(overlayEntry3);
//                 },
//                 child: _itemGuideTop()))
//       ],
//     );
//   });
//
//   // 新手引导：首页开播按钮
//   overlayEntryList.add(overlayEntry0);
//
//   overlayEntry3 = OverlayEntry(builder: (context) {
//     return Stack(
//       children: <Widget>[
//         Positioned(
//           bottom: ScreenUtil().setWidth(120),
//           left: ScreenUtil().setWidth(160),
//           child: GestureDetector(
//             onTap: () {
//               --overIndex;
//               overlayEntry3.remove();
//               overlayEntry0.remove();
//               // 已完成新手引导任务
//               YBDTaskService.instance.end(TASK_SHOW_HOME_PAGE_GUIDE);
//               overlayEntryList.removeLast();
//             },
//             child: _itemGuideBottom(),
//           ),
//         ),
//       ],
//     );
//   });
//   overlayEntryList.add(overlayEntry3);
//   // 添加首页新手引导
//   Overlay.of(context).insertAll(overlayEntryList);
// }
