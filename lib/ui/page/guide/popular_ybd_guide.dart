import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../common/constant/const.dart';
import '../../../common/service/task_service/task_ybd_service.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../home/widget/bubble_ybd_widget.dart';

class YBDPopularGuide {
  BuildContext context;

  /// 实例对象
  static YBDPopularGuide? _instance;

  /// 蒙板
  late OverlayEntry _maskOverlay;

  /// 开播按钮引导
  late OverlayEntry _liveOverLay;

  YBDPopularGuide(this.context);

  /// 显示新手指引
  static Future<bool> showPopularGuide(BuildContext context) async {
    // v2.0.3 的需求：不显示首页引导
    // 已完成引导任务
    YBDTaskService.instance!.end(TASK_SHOW_HOME_PAGE_GUIDE);
    return false;

    // 显示首页引导参考 [RoomGuide.dart] 的实现
  }

  /// 是否需要显示新手指引
  Future<bool> _shouldShowGuide() async {
    // 已完成引导任务
    YBDTaskService.instance!.end(TASK_SHOW_HOME_PAGE_GUIDE);
    return false;

    // v2.0.2 需求：不显示首页引导
    // String guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);

    // if (guide != null && guide.contains(Const.GUIDE_MAIN)) {
    //   logger.v('=================guide false ');
    //  // 已完成引导任务
    //   YBDTaskService.instance.end(TASK_SHOW_HOME_PAGE_GUIDE);
    //   return false;
    // } else {
    //   logger.v('=================guide true ');
    //   return true;
    // }
  }
  void _shouldShowGuiderNtz4oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 保存显示记录
  /// 安装 app 后只需要显示一次
  void saveTipGuideDisplayRecord() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);

    if (guide == null || guide.isEmpty) {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_MAIN);
    } else {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_MAIN);
    }
  }
  void saveTipGuideDisplayRecordjYY6toyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏新手指引
  void _hideGuide() {
    _hideLiveOverlay();
    _hideMaskOverlay();
    YBDTaskService.instance!.end(TASK_SHOW_HOME_PAGE_GUIDE);
  }
  void _hideGuidex8q93oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 蒙板
  void _showMaskOverlay() {
    _maskOverlay = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              // 隐藏新手指引界面
              _hideGuide();
            },
            child: Container(color: Colors.black.withOpacity(0.8)),
          )
        ],
      );
    });

    // 显示蒙板
    Overlay.of(context)!.insert(_maskOverlay);
  }
  void _showMaskOverlay47C66oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏蒙板
  void _hideMaskOverlay() {
    try {
      _maskOverlay.remove();
    } catch (e) {
      logger.v('remove mask overlay error : $e');
    }
  }

  /// 开播按钮
  void _showLiveOverlay() {
    _liveOverLay = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          Positioned(
            bottom: ScreenUtil().setWidth(120),
            left: ScreenUtil().setWidth(160),
            child: GestureDetector(
              onTap: () {
                // 隐藏开播按钮指引
                _hideLiveOverlay();
              },
              child: Material(
                color: Color.fromARGB(0, 0, 0, 0),
                child: Padding(
                  padding: EdgeInsets.only(right: 4.0),
                  child: Container(
                    child: YBDBubbleWidget(
                      ScreenUtil().setWidth(400),
                      ScreenUtil().setWidth(120),
                      Color(0xFF813bff),
                      BubbleArrowDirection.bottom,
                      child: Text(
                        translate('home_tip_live'),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    });

    // 显示开播指引
    Overlay.of(context)!.insert(_liveOverLay);
  }
  void _showLiveOverlay7epxnoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏开播指引
  void _hideLiveOverlay() {
    try {
      _liveOverLay.remove();
    } catch (e) {
      logger.v('remove live overlay error : $e');
    }
  }
}
