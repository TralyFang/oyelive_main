import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../common/constant/const.dart';
import '../../../common/service/task_service/task_ybd_service.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../home/widget/bubble_ybd_widget.dart';

/// 索引页新手指引
class YBDIndexPageGuide {
  BuildContext context;

  /// 蒙板
  late OverlayEntry _maskOverlay;

  /// 客服
  late OverlayEntry _supportOverlay;

  /// 开播
  OverlayEntry? _goLiveOverlay;

  YBDIndexPageGuide(this.context);

  /// 是否需要显示首页新手指引
  Future<bool> shouldShowGuide() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);

    if (guide != null && guide.contains(Const.GUIDE_MAIN)) {
      return false;
    } else {
      return true;
    }
  }
  void shouldShowGuidewx9I1oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 保存显示记录
  /// 安装 app 后只需要显示一次
  void saveTipGuideDisplayRecord() async {
    String? guide = await YBDSPUtil.get(Const.SP_KEY_TIP_GUIDE);

    if (guide == null || guide.isEmpty) {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, Const.GUIDE_MAIN);
    } else {
      YBDSPUtil.save(Const.SP_KEY_TIP_GUIDE, guide + '|' + Const.GUIDE_MAIN);
    }
  }
  void saveTipGuideDisplayRecordXVddkoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 显示新手指引
  void showIndexPageGuide() {
    _showMaskOverlay();
    _showSupportOverlay();
  }

  /// 隐藏新手指引
  void _hideGuide() {
    _hideSupportOverlay();
    _hideGoLiveOverlay();
    _hideMaskOverlay();
  }
  void _hideGuideB6X09oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 蒙板
  void _showMaskOverlay() {
    _maskOverlay = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              if (null == _goLiveOverlay) {
                // 隐藏客服引导
                _hideSupportOverlay();
                // 显示开播指引
                _showGoLiveOverlay();
              } else {
                // 隐藏新手指引界面
                _hideGuide();
              }
            },
            child: Container(color: Colors.black.withOpacity(0.4)),
          )
        ],
      );
    });

    // 显示蒙板
    Overlay.of(context)!.insert(_maskOverlay);
  }
  void _showMaskOverlaybVojhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏蒙板
  void _hideMaskOverlay() {
    try {
      _maskOverlay.remove();
      // 已完成新手引导任务
      YBDTaskService.instance!.end(TASK_SHOW_HOME_PAGE_GUIDE);
    } catch (e) {
      logger.v('remove mask overlay error : $e');
    }
  }

  /// 客服
  void _showSupportOverlay() {
    _supportOverlay = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          Positioned(
            top: ScreenUtil().setWidth(480),
            left: ScreenUtil().setWidth(32),
            child: GestureDetector(
              onTap: () {
                // 隐藏客服引导
                _hideSupportOverlay();
                // 显示开播指引
                _showGoLiveOverlay();
              },
              child: Material(
                color: Color.fromARGB(0, 0, 0, 0),
                child: Padding(
                  padding: EdgeInsets.only(right: 4.0),
                  child: Container(
                    child: YBDBubbleWidget(
                      ScreenUtil().setWidth(400),
                      ScreenUtil().setWidth(120),
                      Color(0xFF813bff),
                      BubbleArrowDirection.top,
                      length: 10.0,
                      child: Text(
                        translate('home_tip_support'),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    });

    /// 显示客服指引
    Overlay.of(context)!.insert(_supportOverlay);
  }
  void _showSupportOverlaymn3WToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏客服指引
  void _hideSupportOverlay() {
    try {
      _supportOverlay.remove();
    } catch (e) {
      logger.v('remove support overlay error : $e');
    }
  }

  /// 开播按钮
  void _showGoLiveOverlay() {
    _goLiveOverlay = OverlayEntry(builder: (context) {
      return Stack(
        children: <Widget>[
          Positioned(
            bottom: ScreenUtil().setWidth(120),
            left: ScreenUtil().setWidth(160),
            child: GestureDetector(
              onTap: () {
                // 隐藏指引页面
                _hideGuide();
              },
              child: Material(
                color: Color.fromARGB(0, 0, 0, 0),
                child: Padding(
                  padding: EdgeInsets.only(right: 4.0),
                  child: Container(
                    child: YBDBubbleWidget(
                      ScreenUtil().setWidth(400),
                      ScreenUtil().setWidth(120),
                      Color(0xFF813bff),
                      BubbleArrowDirection.bottom,
                      child: Text(
                        translate('home_tip_live'),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    });

    // 显示开播指引
    Overlay.of(context)!.insert(_goLiveOverlay!);
  }
  void _showGoLiveOverlay9NBvhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 隐藏开播指引
  void _hideGoLiveOverlay() {
    try {
      _goLiveOverlay!.remove();
    } catch (e) {
      logger.v('remove go live overlay error : $e');
    }
  }
}
