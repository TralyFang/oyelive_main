import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/round_ybd_avatar.dart';

/// 榜单第二名或第三名头像
class YBDLeaderboardSecondAvatar extends StatelessWidget {
  /// 用户信息
  final YBDRoomInfo userInfo;

  /// 边框图片
  final String frameImg;

  YBDLeaderboardSecondAvatar(this.userInfo, this.frameImg);

  @override
  Widget build(BuildContext context) {
    double frameWidth = 172;
    double avatarWidth = 98;

    return GestureDetector(
      onTap: () async {
        YBDUserInfo? myInfo = await YBDSPUtil.getUserInfo();
        if (null != userInfo && '${userInfo.id}' == '${myInfo!.id}') {
          logger.v('jump to my profile page');
          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.myprofile);
        } else {
          logger.v('jump to other user profile page : ${userInfo.id}');
          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.user_profile + "/${userInfo.id}");
        }
      },
      child: Container(
        // color: Colors.yellow,
        width: ScreenUtil().setWidth(frameWidth),
        height: ScreenUtil().setWidth(170),
        child: Stack(
          children: <Widget>[
            Center(
              child: Container(
                width: ScreenUtil().setWidth(avatarWidth),
                height: ScreenUtil().setWidth(avatarWidth),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(avatarWidth))),
                  border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(4)),
                ),
                child: YBDRoundAvatar(
                  // 用户头像
                  userInfo.headimg ?? '',
                  sex: userInfo.sex,
                  userId: userInfo.id,
                  avatarWidth: avatarWidth - 8,
                  scene: "B",
                ),
              ),
            ),
            Positioned(
              // 边框
              bottom: ScreenUtil().setWidth(0),
              child: Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(frameWidth),
                child: YBDImage(
                  path: frameImg,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
                bottom: ScreenUtil().setWidth(36),
                right: ScreenUtil().setWidth(22),
                child: Transform.scale(
                  scale: 1.2,
                  child: YBDVipIcon(
                    userInfo.vipIcon ?? '',
                    padding: EdgeInsets.all(0),
                  ),
                )),
          ],
        ),
      ),
    );
  }
  void buildBvpfCoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
