import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../define/leaderboard_ybd_def.dart';
import 'leaderboard_ybd_period_content.dart';

/// 榜单分类 tab 内容
class YBDLeaderboardCategoryContent extends StatefulWidget {
  final LeaderboardCategory category;

  YBDLeaderboardCategoryContent(this.category);

  @override
  State<StatefulWidget> createState() => _YBDLeaderboardCategoryContentState();
}

class _YBDLeaderboardCategoryContentState extends State<YBDLeaderboardCategoryContent>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  TabController? _tabController;

  /// 当前选中的标签索引
  int _selectedTabIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
    _tabController!.addListener(() {
      logger.v('combo tab index : ${_tabController!.index}');
      setState(() {
        _selectedTabIndex = _tabController!.index;
      });
    });
  }
  void initStateXym1toyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: Column(
        children: <Widget>[
          _itemsTabBar(),
          _tabContentView(),
        ],
      ),
    );
  }
  void buildYCY8Aoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// tab 内容页面
  Widget _tabContentView() {
    switch (widget.category) {
      case LeaderboardCategory.Streamers:
      case LeaderboardCategory.Gifts:
        return Expanded(
          child: TabBarView(
            controller: _tabController,
            children: <Widget>[
              YBDLeaderboardPeriodContent(widget.category, LeaderboardPeriod.Today),
              YBDLeaderboardPeriodContent(widget.category, LeaderboardPeriod.Weekly),
              YBDLeaderboardPeriodContent(widget.category, LeaderboardPeriod.Monthly),
              YBDLeaderboardPeriodContent(widget.category, LeaderboardPeriod.Total),
            ],
          ),
        );
      case LeaderboardCategory.Followers:
        return Expanded(
          child: Container(
            width: ScreenUtil().screenWidth,
            child: YBDLeaderboardPeriodContent(widget.category, LeaderboardPeriod.Monthly),
          ),
        );
      default:
        return Container();
    }
  }
  void _tabContentViewFbFEYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Daily, Weekly, Monthly 标签栏
  Widget _itemsTabBar() {
    if (widget.category == LeaderboardCategory.Followers) {
      return Container();
    }

    BoxDecoration selectedDecoration = BoxDecoration(
      borderRadius: BorderRadius.circular(
        ScreenUtil().setWidth(80),
      ),
      gradient: LinearGradient(
        colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );

    BoxDecoration normalDecoration = BoxDecoration(
      borderRadius: BorderRadius.circular(ScreenUtil().setWidth(80)),
      color: Colors.white.withOpacity(0.1),
    );

    return Container(
      padding: EdgeInsets.fromLTRB(
        ScreenUtil().setWidth(20),
        ScreenUtil().setWidth(30),
        ScreenUtil().setWidth(20),
        ScreenUtil().setWidth(10),
      ),
      child: TabBar(
        // 去掉下划线
        indicator: BoxDecoration(),
        labelPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(0)),
        labelColor: Colors.white,
        controller: _tabController,
        labelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(24),
        ),
        tabs: [
          Container(
              decoration: _selectedTabIndex == 0 ? selectedDecoration : normalDecoration,
              height: ScreenUtil().setWidth(56),
              width: ScreenUtil().setWidth(143),
              child: Tab(text: translate('today'))),
          Container(
              decoration: _selectedTabIndex == 1 ? selectedDecoration : normalDecoration,
              height: ScreenUtil().setWidth(56),
              width: ScreenUtil().setWidth(143),
              child: Tab(text: translate('weekly'))),
          Container(
              decoration: _selectedTabIndex == 2 ? selectedDecoration : normalDecoration,
              height: ScreenUtil().setWidth(56),
              width: ScreenUtil().setWidth(143),
              child: Tab(text: translate('monthly'))),
          Container(
              decoration: _selectedTabIndex == 3 ? selectedDecoration : normalDecoration,
              height: ScreenUtil().setWidth(56),
              width: ScreenUtil().setWidth(143),
              child: Tab(text: translate('total'))),
        ],
      ),
    );
  }
  void _itemsTabBarl5OUioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
