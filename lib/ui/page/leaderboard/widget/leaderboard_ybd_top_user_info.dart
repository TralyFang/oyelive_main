import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../define/leaderboard_ybd_def.dart';
import '../../../widget/level_ybd_tag.dart';

/// 榜单页面前三名的用户信息
class YBDLeaderboardTopUserInfo extends StatelessWidget {
  /// 是否为第一名
  final bool isFirst;

  /// 分类
  final LeaderboardCategory? category;

  /// 用户信息
  final YBDRoomInfo userInfo;
  bool showDiamond;
  YBDLeaderboardTopUserInfo(this.userInfo, {this.isFirst = false, this.category, this.showDiamond: false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setHeight(128),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
        color: Colors.white.withOpacity(0.2),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: ScreenUtil().setWidth(isFirst ? 18 : 10)),
          Container(
            // 昵称
            height: ScreenUtil().setWidth(40),
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(12)),
            child: Text(
              userInfo.nickname ?? '',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: ScreenUtil().setSp(28),
              ),
            ),
          ),
          userInfo.money != null
              ? SizedBox(height: ScreenUtil().setWidth(3))
              : SizedBox(height: ScreenUtil().setWidth(14)),
          // 用户等级
          YBDLevelTag(userInfo.level),
          SizedBox(height: ScreenUtil().setWidth(7)),
          // 金币栏
          _coinRow(),
          SizedBox(height: ScreenUtil().setWidth(7)),
        ],
      ),
    );
  }
  void buildyn9rKoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 金币栏或粉丝栏
  Widget _coinRow() {
    // 金币数或粉丝数
    var amount = userInfo.money;
    if (category == LeaderboardCategory.Followers) {
      // 粉丝榜显示粉丝数
      amount = userInfo.fans;
    }

    if (amount != null) {
      return Row(
        // 金币数
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          showDiamond
              ? Image.asset(
                  "assets/images/icon_gems.webp",
                  width: ScreenUtil().setWidth(16),
                )
              : Container(
                  // width: ScreenUtil().setWidth(15),
                  height: ScreenUtil().setWidth(20),
                  child: Image.asset(category == LeaderboardCategory.Followers
                      ? 'assets/images/cp/leaderboard_fans_icon.png'
                      : 'assets/images/topup/y_top_up_beans@2x.webp'),
                ),
          SizedBox(width: ScreenUtil().setWidth(3)),
          Text(
            '${YBDNumericUtil.format(amount)}',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(20),
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      );
    } else {
      return SizedBox(height: 0);
    }
  }
  void _coinRowH5Df5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
