import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/query_ybd_star_resp_entity.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../../home/entity/rank_ybd_info_entity.dart';
import '../define/leaderboard_ybd_def.dart';
import 'leaderboard_ybd_first_avatar.dart';
import 'leaderboard_ybd_list_item.dart';
import 'leaderboard_ybd_second_avatar.dart';
import 'leaderboard_ybd_top_user_info.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';

/// Daily, Weekly, Monthly 标签页内容
class YBDLeaderboardPeriodContent extends StatefulWidget {
  final LeaderboardPeriod period;
  final LeaderboardCategory category;

  YBDLeaderboardPeriodContent(this.category, this.period);

  @override
  State<StatefulWidget> createState() => _YBDLeaderboardPeriodContentState();
}

class _YBDLeaderboardPeriodContentState extends State<YBDLeaderboardPeriodContent> with AutomaticKeepAliveClientMixin {
  /// 排行榜数据
  List<YBDRoomInfo> _data = [];

  /// 列表数据
  List<YBDRoomInfo> _listData = [];

  /// 是否完成网络请求
  bool _doneRequest = false;

  /// 列表刷新控制器
  RefreshController _refreshController = RefreshController();

  /// 前三名用户 item 的宽度
  final _topUserItemWidth = 170;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

    // 请求排行榜数据
    _requestLeaderboardData();
  }
  void initStateb4vdboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      padding: EdgeInsets.all(ScreenUtil().setWidth(30)),
      child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
            color: Colors.white.withOpacity(0.1),
          ),
          child: _contentView()),
    );
  }
  void buildKKhOuoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 页面内容
  Widget _contentView() {
    if (_doneRequest) {
      if (_data.isEmpty) {
        // 没有数据
        return YBDPlaceHolderView(text: translate('no_data'));
      } else {
        return Column(
          children: <Widget>[
            // 头部
            _topThree(),
            Expanded(
              // 榜单列表
              child: _leaderboardList(),
            ),
          ],
        );
      }
    } else {
      return YBDLoadingCircle();
    }
  }
  void _contentViewi09LJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 榜单列表
  Widget _leaderboardList() {
    return SmartRefresher(
      controller: _refreshController,
      footer: YBDMyRefreshIndicator.myFooter,
      enablePullDown: false,
      enablePullUp: false,
      child: ListView.builder(
        padding: EdgeInsets.only(top: ScreenUtil().setWidth(20)),
        itemBuilder: (_, index) {
          return YBDLeaderboardListItem(
            _listData[index],
            index + 4,
            category: widget.category,
          );
        },
        itemCount: _listData.length,
      ),
    );
  }
  void _leaderboardListIjzSvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 前三名
  Widget _topThree() {
    // 显示前三名的容器高度
    double topContainerHeader = 400;
    return Container(
      alignment: Alignment.topCenter,
      height: ScreenUtil().setWidth(topContainerHeader),
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(36)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _data.length > 1
              ? Padding(
                  // 第二名
                  padding: EdgeInsets.only(top: ScreenUtil().setWidth(90)),
                  child: _secondOrThirdUser('assets/images/cp/leaderboard_second.webp', 1),
                )
              : Container(width: ScreenUtil().setWidth(_topUserItemWidth)),
          _data.length > 0
              ? Padding(
                  // 第一名
                  padding: EdgeInsets.only(top: ScreenUtil().setWidth(24)),
                  child: _firstUser(),
                )
              : Container(width: ScreenUtil().setWidth(_topUserItemWidth)),
          _data.length > 2
              ? Padding(
                  // 第三名
                  padding: EdgeInsets.only(top: ScreenUtil().setWidth(90)),
                  child: _secondOrThirdUser('assets/images/cp/leaderboard_third.webp', 2),
                )
              : Container(width: ScreenUtil().setWidth(_topUserItemWidth)),
        ],
      ),
    );
  }
  void _topThree6nN41oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 第一名
  Widget _firstUser() {
    return Container(
      height: ScreenUtil().setWidth(300),
      width: ScreenUtil().setWidth(_topUserItemWidth),
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Positioned(
            top: 0,
            child: YBDLeaderboardFirstAvatar(_data[0]),
          ),
          Positioned(
            top: ScreenUtil().setWidth(150),
            child: YBDLeaderboardTopUserInfo(
              _data[0],
              isFirst: true,
              category: widget.category,
              showDiamond: widget.category == LeaderboardCategory.Streamers,
            ),
          ),
        ],
      ),
    );
  }
  void _firstUserP8PjCoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 第二名或第三名
  /// [rank] 1: 第二名，2：第三名
  Widget _secondOrThirdUser(String frameImg, int rank) {
    return Container(
      height: ScreenUtil().setWidth(300),
      width: ScreenUtil().setWidth(_topUserItemWidth),
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Positioned(
            top: 0,
            child: YBDLeaderboardSecondAvatar(_data[rank], frameImg),
          ),
          Positioned(
            top: ScreenUtil().setWidth(156),
            child: YBDLeaderboardTopUserInfo(_data[rank],
                category: widget.category, showDiamond: widget.category == LeaderboardCategory.Streamers),
          ),
        ],
      ),
    );
  }
  void _secondOrThirdUserGzLUooyelive(String frameImg, int rank) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求排行榜数据
  /// return: 数据条数
  Future<int?> _requestLeaderboardData() async {
    var rankInfoEntity;

    switch (widget.category) {
      case LeaderboardCategory.Streamers:
        rankInfoEntity = await _requestStreamsData();
        break;
      case LeaderboardCategory.Gifts:
        rankInfoEntity = await _requestGiftsData();
        break;
      case LeaderboardCategory.Followers:
        rankInfoEntity = await _requestFollowersData();
        break;
    }
    if (!mounted) return 0;

    if (!mounted) return 0;
    if (rankInfoEntity?.record?.rank != null) {
      // 保存请求到的数据
      _data.addAll(_userInfoListFromResponse(rankInfoEntity));

      // 配置 [ListView] 显示的数据
      _configListData();
      setState(() {
        _doneRequest = true;
      });
      return rankInfoEntity.record.rank.length;
    } else {
      setState(() {
        _doneRequest = true;
      });
      return 0;
    }
  }

  /// 配置了 [money] 字段的 [YBDRoomInfo] 数组
  /// [rankInfoEntity] 接口返回的数据
  List<YBDRoomInfo> _userInfoListFromResponse(rankInfoEntity) {
    assert(null != rankInfoEntity);

    List<YBDRoomInfo?> roomInfoList = [];
    roomInfoList.addAll(rankInfoEntity.record.rank);

    if (widget.category != LeaderboardCategory.Followers) {
      // Streamers 和 Gifts 接口数据要从 scores 列表中获取 money 信息
      for (int i = 0; i < roomInfoList.length; i++) {
        try {
          roomInfoList[i]?.money = YBDNumericUtil.doubleToInt(rankInfoEntity.record.scores[i]);
        } catch (e) {
          logger.v('get score error : $e');
        }
      }
    }

    return roomInfoList.removeNulls();
  }
  void _userInfoListFromResponseAdqkIoyelive(rankInfoEntity) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求 Gifts 排行榜数据
  Future<dynamic> _requestGiftsData() async {
    YBDRankInfoEntity? rankInfoEntity = await ApiHelper.giftersList(
      context,
      _data.length,
      30,
      _timeType(),
    );

    return rankInfoEntity;
  }

  /// 请求 Streamers 排行榜数据
  Future<dynamic> _requestStreamsData() async {
    YBDQueryStarRespEntity? rankInfoEntity = await ApiHelper.queryStar(
      context,
      _data.length,
      30,
      _timeType(),
    );

    return rankInfoEntity;
  }

  /// 请求 Followers 排行榜数据
  Future<dynamic> _requestFollowersData() async {
    YBDRankInfoEntity? rankInfoEntity = await ApiHelper.followersList(
      context,
      _data.length,
      30,
      _timeType(),
    );

    return rankInfoEntity;
  }

  /// 配置列表数据
  _configListData() {
    if (_data.length > 3) {
      _listData = _data.sublist(3, _data.length);
    } else {
      _listData = [];
    }
  }

  /// 请求参数 [timeType]
  int _timeType() {
    switch (widget.period) {
      case LeaderboardPeriod.Today:
        return 1;
      case LeaderboardPeriod.Weekly:
        return 2;
      case LeaderboardPeriod.Monthly:
        return 3;
      case LeaderboardPeriod.Total:
        return 4;
      default:
        return 1;
    }
  }
  void _timeTypecDmyLoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
