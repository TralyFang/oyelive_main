import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/round_ybd_avatar.dart';

/// 榜单第一名的用户头像
class YBDLeaderboardFirstAvatar extends StatelessWidget {
  /// 用户信息
  final YBDRoomInfo userInfo;
  YBDLeaderboardFirstAvatar(this.userInfo);

  @override
  Widget build(BuildContext context) {
    double frameWidth = 172;
    double avatarWidth = 120;

    return GestureDetector(
      onTap: () async {
        YBDUserInfo? myInfo = await YBDSPUtil.getUserInfo();
        if (null != userInfo && '${userInfo.id}' == '${myInfo!.id}') {
          logger.v('jump to my profile page');
          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.myprofile);
        } else {
          logger.v('jump to other user profile page : ${userInfo.id}');
          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.user_profile + "/${userInfo.id}");
        }
      },
      child: Container(
        width: ScreenUtil().setWidth(frameWidth),
        height: ScreenUtil().setWidth(170),
        child: Stack(
          children: <Widget>[
            Center(
              child: Container(
                width: ScreenUtil().setWidth(avatarWidth),
                height: ScreenUtil().setWidth(avatarWidth),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(avatarWidth))),
                  border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(4)),
                ),
                child: YBDRoundAvatar(
                  // 用户头像
                  userInfo.headimg ?? '',
                  sex: userInfo.sex,
                  needNavigation: true,
                  userId: userInfo.id,
                  avatarWidth: avatarWidth - 8,
                  scene: "B",
                  // vipIcon: userInfo.vipIcon,
                ),
              ),
            ),
            Positioned(
              // 皇冠
              top: 0,
              child: Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(frameWidth),
                child: Container(
                  height: ScreenUtil().setWidth(36),
                  child: Image.asset(
                    'assets/images/icon_crown.webp',
                  ),
                ),
              ),
            ),
            Positioned(
              // 边框
              bottom: ScreenUtil().setWidth(0),
              child: Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(frameWidth),
                child: Image.asset(
                  'assets/images/cp/leaderboard_first.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
                bottom: ScreenUtil().setWidth(36),
                right: ScreenUtil().setWidth(18),
                child: Transform.scale(
                  scale: 1.2,
                  child: YBDVipIcon(
                    userInfo.vipIcon ?? '',
                    padding: EdgeInsets.all(0),
                  ),
                )),
          ],
        ),
      ),
    );
  }
  void buildlAzTmoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
