import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/icon_ybd_gender.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../define/leaderboard_ybd_def.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

/// 榜单列表项
class YBDLeaderboardListItem extends StatelessWidget {
  final YBDRoomInfo userInfo;
  final int rank;
  final LeaderboardCategory? category;

  YBDLeaderboardListItem(this.userInfo, this.rank, {this.category});

  @override
  Widget build(BuildContext context) {
    double itemHeight = 122;
    return Container(
      height: ScreenUtil().setWidth(itemHeight),
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            // item 内容
            height: ScreenUtil().setWidth(itemHeight - 1),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // 头像
                _userAvatar(),
                // 昵称，用户等级，性别
                _userInfoColumn(),
                Expanded(
                  child: Container(),
                ),
                // 豆子
                userInfo.money != null ? _userBeans() : Container(),
              ],
            ),
          ),
          Container(
            // 底部分割线
            color: Colors.white.withOpacity(0.1),
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
            height: ScreenUtil().setWidth(1),
          ),
        ],
      ),
    );
  }
  void buildfTHAfoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 用户分数
  Widget _userBeans() {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(32),
            child: Image.asset(
              category == LeaderboardCategory.Streamers
                  ? 'assets/images/icon_gems.webp'
                  : 'assets/images/topup/y_top_up_beans@2x.webp',
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(4)),
          Text(
            '${YBDNumericUtil.format(userInfo.money)}',
            style: TextStyle(
              color: Color(0xffFFE586),
              fontSize: ScreenUtil().setSp(24),
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }
  void _userBeansho6Qzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 昵称，用户等级，性别
  Widget _userInfoColumn() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: [
              Padding(
                padding: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
                child: Container(
                  constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(340)),
                  child: Text(
                    // 昵称
                    userInfo.nickname?.removeBlankSpace() ?? '',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: ScreenUtil().setSp(24),
                    ),
                  ),
                ),
              ),
              YBDGenderIcon(userInfo.sex ?? 0)
            ],
          ),
          SizedBox(height: ScreenUtil().setWidth(userInfo.vipIcon != null ? 10 : 20)),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              YBDVipIcon(userInfo.vipIcon ?? ''),
              YBDLevelTag(userInfo.level),
              SizedBox(width: ScreenUtil().setWidth(8)),
              // 性别标签 0未知，1女2男
              if (category == LeaderboardCategory.Followers) _followsAmount()
            ],
          ),
        ],
      ),
    );
  }
  void _userInfoColumn176uLoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 性别标签
  Widget _sexTag() {
    return Container(
      width: ScreenUtil().setWidth(60),
      height: ScreenUtil().setWidth(24),
      child: Image.asset(
        userInfo.sex == 2 ? "assets/images/tag_male.webp" : "assets/images/tag_female.webp",
        fit: BoxFit.fill,
      ),
    );
  }
  void _sexTagQOSMeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 粉丝数
  Widget _followsAmount() {
    return Container(
      height: ScreenUtil().setWidth(25),
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(9)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
        gradient: LinearGradient(
          colors: [Color(0xff99B9FF), Color(0xff7B85D7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Row(
        children: <Widget>[
          Container(
            height: ScreenUtil().setWidth(20),
            child: Image.asset(
              'assets/images/cp/leaderboard_fans_icon.png',
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(9)),
          Text(
            '${userInfo.fans}',
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(18),
            ),
          ),
        ],
      ),
    );
  }
  void _followsAmount0ULt0oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 头像
  Widget _userAvatar() {
    double avatarWidth = 80;
    return Container(
      width: ScreenUtil().setWidth(avatarWidth),
      height: ScreenUtil().setWidth(avatarWidth + 20),
      child: Stack(
        children: <Widget>[
          Center(
            child: Container(
              width: ScreenUtil().setWidth(avatarWidth),
              height: ScreenUtil().setWidth(avatarWidth),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(avatarWidth))),
                border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(2)),
              ),
              child: YBDRoundAvatar(
                // 用户头像
                userInfo.headimg ?? '',
                sex: userInfo.sex,
                needNavigation: true,
                lableSrc: '',
                userId: userInfo.id,
                avatarWidth: avatarWidth - 4,
                scene: "B",
              ),
            ),
          ),
          Positioned(
            bottom: ScreenUtil().setWidth(0),
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(avatarWidth),
              child: Container(
                width: ScreenUtil().setWidth(50),
                height: ScreenUtil().setWidth(30),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/cp/leaderboard_rank_bg.webp'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Text(
                  '$rank',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: ScreenUtil().setSp(20),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void _userAvataronDpXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
