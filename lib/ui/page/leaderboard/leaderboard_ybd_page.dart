import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/constant/short_ybd_route.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import '../../../base/base_ybd_state.dart';
import 'define/leaderboard_ybd_def.dart';
import 'widget/leaderboard_ybd_category_content.dart';
import '../../widget/my_ybd_app_bar.dart';
import '../../widget/scaffold/nav_ybd_back_button.dart';

/// 榜单页面
class YBDLeaderboardPage extends StatefulWidget {
  final LeaderboardCategory category;

  bool showPop;
  YBDLeaderboardPage(this.category, {this.showPop: true});

  @override
  State<StatefulWidget> createState() => _YBDLeaderboardPageState();
}

class _YBDLeaderboardPageState extends BaseState<YBDLeaderboardPage> with SingleTickerProviderStateMixin {
  TabController? _tabController;

  /// 选中的标签索引
  int? _selectedTabIndex;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _selectedTabIndex = widget.category.index;
    _tabController!.addListener(() {
      print('22.7.29----index:${_tabController!.index}');
      if (_tabController!.index != _selectedTabIndex) {
        YBDObsUtil.instance().tabRouteChange(
          routeList: YBDShortRoute.rank_list,
          curIndex: _tabController!.index,
          preIndex: _selectedTabIndex!,
          preRoute: YBDShortRoute.home_pop_rank,
        );
        _selectedTabIndex = _tabController!.index;
        setState(() {});
      }
    });
    switch (widget.category) {
      case LeaderboardCategory.Gifts:
        _tabController!.animateTo(1);
        break;
      case LeaderboardCategory.Followers:
        _tabController!.animateTo(2);
        break;
      default:
        break;
    }
  }
  void initStatezdXN6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      // extendBodyBehindAppBar: true,
      appBar: YBDMyAppBar(
        title: Text(
          getSettingsTitle() ?? translate('Rank'),
          style: TextStyle(color: Colors.white, fontSize: YBDTPStyle.spNav),
        ),
        backgroundColor: YBDTPStyle.heliotrope,
        leading: widget.showPop ? YBDNavBackButton(color: Colors.white) : null,
        elevation: 0.0,
      ),
      body: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: <Widget>[
            _itemsTabBar(),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  YBDLeaderboardCategoryContent(LeaderboardCategory.Streamers),
                  YBDLeaderboardCategoryContent(LeaderboardCategory.Gifts),
                  YBDLeaderboardCategoryContent(LeaderboardCategory.Followers),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  void myBuildpo72Xoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Streamers, Gifts, Followers 标签栏
  Widget _itemsTabBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(50)),
      child: TabBar(
        indicator: UnderlineTabIndicator(
          borderSide: BorderSide(
            width: ScreenUtil().setWidth(4),
            color: Color(0xff47CCCB),
          ),
          insets: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(70),
            vertical: ScreenUtil().setWidth(16),
          ),
        ),
        labelPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(0)),
        labelColor: Colors.white,
        unselectedLabelColor: Colors.white.withOpacity(0.49),
        controller: _tabController,
        labelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(32),
        ),
        unselectedLabelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(28),
        ),
        tabs: [
          Container(
              padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
              height: ScreenUtil().setWidth(80),
              child: Tab(text: translate('streamers'))),
          Container(
              padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
              height: ScreenUtil().setWidth(80),
              child: Tab(text: translate('gifters'))),
          Container(
              padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
              height: ScreenUtil().setWidth(80),
              child: Tab(text: translate('followers'))),
        ],
      ),
    );
  }
  void _itemsTabBar35DP2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
