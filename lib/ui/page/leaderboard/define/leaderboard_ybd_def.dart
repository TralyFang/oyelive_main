import 'dart:async';

/// 自定义数据类型
enum LeaderboardCategory {
  Streamers,
  Gifts,
  Followers,
}

enum LeaderboardPeriod { Today, Weekly, Monthly, Total }
