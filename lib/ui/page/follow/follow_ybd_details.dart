import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/log_ybd_util.dart';
import 'widget/follower_ybd_content.dart';
import 'widget/following_ybd_content.dart';
import 'widget/friend_ybd_content.dart';
import '../../widget/scaffold/nav_ybd_back_button.dart';

class YBDFollowDetailsListPage extends StatefulWidget {
  /// 查看指定用户的关注列表和粉丝列表
  final int userId;

  /// 0，好友列表； 1，选中关注列表；2，选中粉丝列表；
  final int tabIndex;

  YBDFollowDetailsListPage(this.userId, this.tabIndex);

  @override
  YBDFollowDetailsListPageState createState() => new YBDFollowDetailsListPageState();
}

class YBDFollowDetailsListPageState extends BaseState<YBDFollowDetailsListPage> with SingleTickerProviderStateMixin {
  int tabSelectingIndex = 0;
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
//    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE, location: "featured_status_page"));
    _tabController!.animateTo(widget.tabIndex);

    _tabController!.addListener(() {
      if (_tabController!.index == 0) {
//        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE,
//            location: "featured_status_page"));
      } else {
//        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE,
//            location: "following_status_page"));
      }
      logger.v('index : ${_tabController!.index}');
    });
  }
  void initStatepvte7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance?.removeObserver(this);
    _tabController!.dispose();
  }
  void dispose3fkWFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      body: Container(
        // 页面背景色
        padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
        width: double.infinity,
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: [
            Container(
              width: ScreenUtil().screenWidth,
              height: ScreenUtil().setWidth(116),
              child: Stack(
                children: <Widget>[
                  Positioned(
                      // 导航栏返回按钮
                      top: ScreenUtil().setWidth(15),
                      left: 0,
                      child: YBDNavBackButton()),
                  Center(
                    // 好友，关注和粉丝选项卡
                    child: navTabView(),
                  ),
                  Positioned(
                    // 导航栏返回按钮
                    top: ScreenUtil().setWidth(15),
                    right: 0,
                    child: Container(
                      width: ScreenUtil().setWidth(88),
                      height: ScreenUtil().setWidth(88),
                      child: TextButton(
                        onPressed: () {
                          logger.v('add friend... go to search page');
                          YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.search);
                          YBDAnalyticsUtil.logEvent(
                              YBDAnalyticsEvent(YBDEventName.CLICK_EVENT, location: 'contacts_page', itemName: 'add_friend'));
                        },
                        style: TextButton.styleFrom(padding: EdgeInsets.all(0)),
                        // padding: EdgeInsets.all(0),
                        child: Image.asset('assets/images/icon_add_friend.png', width: ScreenUtil().setWidth(44)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  YBDFriendContent(widget.userId),
                  YBDFollowingContent(widget.userId),
                  YBDFollowerContent(widget.userId),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildUIflDoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 导航栏上的三个选项卡
  Widget navTabView() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(80)),
      child: TabBar(
        indicator: UnderlineTabIndicator(
          borderSide: BorderSide(
            width: ScreenUtil().setWidth(4),
            color: Color(0xff47CCCB),
          ),
          insets: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(70)),
        ),
        labelPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(0)),
        labelColor: Colors.white,
        unselectedLabelColor: Colors.white.withOpacity(0.49),
        controller: _tabController,
        labelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(28),
        ),
        unselectedLabelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(24),
        ),
        tabs: [
          Tab(text: translate("friends")),
          Tab(text: translate("Following")),
          Tab(text: translate("Followers")),
        ],
      ),
    );
  }
  void navTabVieww17Lsoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
