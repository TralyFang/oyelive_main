import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDQueryFriendEntity with JsonConvert<YBDQueryFriendEntity> {
  String? returnCode;
  String? returnMsg;
  YBDQueryFriendRecord? record;
}

class YBDQueryFriendRecord with JsonConvert<YBDQueryFriendRecord> {
  int? id;
  int? fromid;
  int? toid;
  int? createtime;
  dynamic type;
  dynamic notice;
}
