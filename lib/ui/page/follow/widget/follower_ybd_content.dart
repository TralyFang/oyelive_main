import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import 'follower_ybd_list_item.dart';
import 'place_ybd_holder_view.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';

/// 粉丝列表内容
class YBDFollowerContent extends StatefulWidget {
  /// 该用户的粉丝列表
  final int userId;

  YBDFollowerContent(this.userId);

  @override
  _YBDFollowerContentState createState() => _YBDFollowerContentState();
}

class _YBDFollowerContentState extends State<YBDFollowerContent> with AutomaticKeepAliveClientMixin {
  final int _pageSize = 30;

  /// 粉丝列表数组
  List<YBDUserInfo?> _data = [];

  //加载的页数
  int _page = 0;

  /// 是否为初始状态
  bool _isInitState = true;

  /// 自己的粉丝列表显示 follower 按钮和 friend 按钮
  /// 其他人的粉丝列表不显示这两个按钮
  bool _isMyFollowerContent = false;

  /// 列表刷新控制器
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    logger.v('userId : ${widget.userId}');
    _onRefresh();

    // 判断是否为自己的追踪列表
    YBDUserUtil.userId().then((value) {
      setState(() {
        _isMyFollowerContent = value == '${widget.userId}';
      });
    });
  }
  void initStaterWrEOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (null == _data || _data.isEmpty) {
      logger.v('followers list is empty show placeholder');
      return _isInitState ? YBDLoadingCircle() : YBDPlaceHolderView(text: translate('no_follower'));
    }

    return Container(
      child: SmartRefresher(
        controller: _refreshController,
        header: YBDMyRefreshIndicator.myHeader,
        footer: YBDMyRefreshIndicator.myFooter,
        enablePullDown: true,
        enablePullUp: true,
        onRefresh: () {
          logger.v("follower page pull down refresh");
          _onRefresh();
        },
        onLoading: () {
          logger.v("follower page pull up load more");
          _onLoading();
        },
        // 粉丝列表
        child: ListView.builder(
          itemCount: _data.length, // 数据的数量
          itemBuilder: (context, index) {
            return YBDFollowerListItem(_data[index], isMyContent: _isMyFollowerContent);
          }, // 类似 cellForRow 函数
        ),
      ),
    );
  }
  void buildZvWIFoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  /// 列表刷新响应方法
  _onRefresh() async {
    ApiHelper.queryFollowersConcern(context, _page, _pageSize, widget.userId).then((result) {
      // 刷新数据失败
      if (result == null) {
        _refreshController.loadFailed();
        _cancelInitState();
      } else {
        _data = result;

        // 没有更多数据
        if (_data != null && _data.length < _pageSize) {
          // 没有更多数据
          _refreshController.loadNoData();
        } else {
          _refreshController.resetNoData();
        }

        _refreshController.refreshCompleted();

        // 是当前页面则刷新界面
        _cancelInitState();
      }
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }

  /// 上拉加载更多响应方法
  _onLoading() async {
    List<YBDUserInfo?>? result = await ApiHelper.queryFollowersConcern(
      context,
      ++_page * _pageSize,
      _pageSize,
      widget.userId,
    );

    if (result == null) {
      // 没有更多数据
      _refreshController.loadComplete();
      _refreshController.loadNoData();
      return;
    } else {
      if (result.length < _pageSize) {
        // 没有更多数据
        _refreshController.loadNoData();
      } else {
        _refreshController.loadComplete();
      }

      _data.addAll(result);
    }

    if (mounted) setState(() {});
  }
}
