import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

typedef noDataFunc = void Function();

/// 粉丝列表缺省图
class YBDPlaceHolderView extends StatelessWidget {
  /// 缺省图
  final String img;

  /// 缺省图底部的文字
  final String text;

  /// 文字颜色
  final Color textColor;

  ///点击事件
  final noDataFunc? touchLister;

  YBDPlaceHolderView({
    this.img = 'assets/images/empty/empty_my_profile.webp',
    this.text = '',
    this.textColor = Colors.white,
    this.touchLister,
  });

  @override
  Widget build(BuildContext context) {
    // 宽度和高度默认是父组件的宽度和高度
    return Container(
      child: (touchLister != null)
          ? GestureDetector(
              onTap: () {
                if (touchLister != null) {
                  touchLister!();
                }
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    img,
                    width: ScreenUtil().setWidth(382),
                  ),
                  SizedBox(height: ScreenUtil().setWidth(30)),
                  Text(
                    text,
                    style: TextStyle(color: textColor),
                  ),
                ],
              ),
            )
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  img,
                  width: ScreenUtil().setWidth(382),
                ),
                SizedBox(height: ScreenUtil().setWidth(30)),
                Text(
                  text,
                  style: TextStyle(color: textColor),
                ),
              ],
            ),
    );
  }
  void buildUr2vfoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
