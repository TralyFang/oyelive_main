import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../util/firend_ybd_util.dart';
import 'friend_ybd_list_item.dart';
import 'place_ybd_holder_view.dart';
import '../../room/room_ybd_helper.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';

/// 好友列表内容
class YBDFriendContent extends StatefulWidget {
  /// 该用户的好友列表
  final int userId;

  YBDFriendContent(this.userId);

  @override
  _YBDFriendContentState createState() => _YBDFriendContentState();
}

class _YBDFriendContentState extends State<YBDFriendContent> with AutomaticKeepAliveClientMixin {
  /// 好友列表数组
  List<YBDRoomInfo?>? _data = [];

  /// 是否为初始状态
  bool _isInitState = true;

  /// 自己的好友列表
  /// 暂时没用到，先保留，粉丝列表和关注列表都有这个字段
  bool _isMyContent = false;

  /// 列表刷新控制器
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    logger.v('userId : ${widget.userId}');
    _onRefresh();
  }
  void initStateSaaDAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }
  void disposeGpsUVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == _data || _data!.isEmpty) {
      logger.v('friends list is empty show placeholder');
      return _isInitState ? YBDLoadingCircle() : YBDPlaceHolderView(text: translate('no_friend'));
    }

    return Container(
      child: SmartRefresher(
        controller: _refreshController,
        header: YBDMyRefreshIndicator.myHeader,
        footer: YBDMyRefreshIndicator.myFooter,
        enablePullDown: true,
        enablePullUp: true,
        onRefresh: () {
          logger.v("friend page pull down refresh");
          _onRefresh();
        },
        child: ListView.builder(
          itemCount: _data!.length, // 数据的数量
          padding: EdgeInsets.all(0),
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                logger.v('clicked friend item');
                if (_data![index]!.live!) {
                  logger.v('friend is live go room');
                  YBDRoomHelper.enterRoom(context, _data![index]!.id, location: 'friends_page');
                } else {
                  logger.v('friend is not live go profile');
                  YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.user_profile + "/${_data![index]!.id}");
                }
              },
              child: YBDFriendListItem(_data![index]),
            );
          }, // 类似 cellForRow 函数
        ),
      ),
    );
  }
  void buildlZ2nhoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  /// 列表刷新响应方法
  _onRefresh() async {
    ApiHelper.friendList(context, fromId: '${widget.userId}').then((friendListEntity) {
      // 刷新数据失败
      if (friendListEntity == null || friendListEntity.returnCode != Const.HTTP_SUCCESS) {
        _refreshController.loadFailed();
        _cancelInitState();
      } else {
        // 好友排序
        _data = YBDFriendUtil.sortFriends(friendListEntity.record);

        // 一次拉取所有好友，显示没有更多数据
        _refreshController.loadNoData();
        _refreshController.refreshCompleted();

        // 是当前页面则刷新界面
        _cancelInitState();
      }
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }
}
