import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/icon_ybd_gender.dart';
import '../../../../common/event/common_ybd_event.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/room_ybd_level_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

/// 已关注用户列表 item
class YBDFollowingListItem extends StatefulWidget {
  /// 已关注用户信息
  final YBDRoomInfo? roomInfo;

  /// 当前登录用户的好友列表才显示 follow 按钮和 friend 按钮
  final bool isMyContent;

  YBDFollowingListItem(this.roomInfo, {this.isMyContent = false});

  @override
  YBDFollowingListItemState createState() => YBDFollowingListItemState();
}

class YBDFollowingListItemState extends State<YBDFollowingListItem> {
  @override
  void initState() {
    super.initState();
  }
  void initState5zSmhoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(shape: BoxShape.rectangle),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
        child: Column(
          children: [
            Container(
              height: ScreenUtil().setWidth(130),
              child: Row(
                children: [
                  YBDRoundAvatar(
                    // 头像
                    widget.roomInfo!.headimg,
                    // 房间信息没有 sex 字段，默认为 1： 女
                    sex: widget.roomInfo?.sex ?? 1,
                    lableSrc: (widget.roomInfo?.vip ?? 0) == 0 ? "" : "assets/images/vip_lv${widget.roomInfo!.vip}.png",
                    userId: widget.roomInfo!.id,
                    scene: "B",
                    labelWitdh: 22,
                    labelPadding: EdgeInsets.only(right: ScreenUtil().setWidth(5), bottom: ScreenUtil().setWidth(5)),
                    avatarWidth: 80,
                    sideBorder: false,
                  ),
                  SizedBox(width: ScreenUtil().setWidth(10)),
                  Container(
                    width: ScreenUtil().setWidth(360),
                    // padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            SizedBox(width: ScreenUtil().setWidth(10)),
                            ConstrainedBox(
                              constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(350)),
                              child: Text(
                                widget.roomInfo?.nickname?.removeBlankSpace() ?? '',
                                style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                softWrap: true,
                              ),
                            ),
                            YBDGenderIcon(widget.roomInfo?.sex ?? 0)
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil().setWidth(44),
                          child: Row(
                            // 房间标签
                            children: <Widget>[
                              SizedBox(
                                width: ScreenUtil().setWidth(6),
                              ),
                              YBDVipIcon(
                                widget.roomInfo?.vipIcon ?? '',
                                padding: EdgeInsets.all(0),
                              ),

                              // TODO: 暂时不做
//                            _sexTagView(),
                              // 用户级别标签
                              widget.roomInfo?.level != null
                                  ? YBDLevelTag(
                                      widget.roomInfo?.level,
                                      horizontalPadding: 6,
                                    )
                                  : Container(),
                              // TODO：房间级别标签，效果图上没有
                              widget.roomInfo?.roomlevel != null ? YBDRoomLevelTag(widget.roomInfo?.roomlevel) : Container(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(child: SizedBox(width: 1)),
                  // 好友标签
                  widget.isMyContent
                      ? (widget.roomInfo?.friend ?? false)
                          ? _friendButton()
                          : Container()
                      : Container(),
                ],
              ),
            ),
            // SizedBox(height: ScreenUtil().setWidth(10)),
          ],
        ),
      ),
    );
  }
  void buildsVALHoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 好友标签按钮
  Widget _friendButton() {
    return Padding(
      padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
      child: Image.asset(
        "assets/images/n_friends.png",
        width: ScreenUtil().setWidth(40),
      ),
    );
    return Container(
      width: ScreenUtil().setWidth(125),
      height: ScreenUtil().setWidth(56),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
        color: Colors.white.withOpacity(0.3),
      ),
      child: Center(
        child: Text(
          translate('friends'),
          style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
        ),
      ),
    );
  }
  void _friendButtonHqdCUoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // TODO: 房间信息没有性别字段 0未知，1女2男
  Widget _sexTagView() {
    if (null != widget.roomInfo?.sex) {
      return Image.asset(
        widget.roomInfo!.sex == 2 ? "assets/images/tag_male.webp" : "assets/images/tag_female.webp",
        fit: BoxFit.fill,
        width: ScreenUtil().setWidth(60),
        height: ScreenUtil().setWidth(24),
      );
    } else {
      return Container();
    }
  }
  void _sexTagVieweLCnmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
