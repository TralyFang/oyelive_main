import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/icon_ybd_gender.dart';
import '../../../../common/event/common_ybd_event.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/room_ybd_level_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

/// 粉丝列表 item
class YBDFollowerListItem extends StatefulWidget {
  /// 粉丝信息
  final YBDUserInfo? userInfo;

  /// 当前登录用户的好友列表才显示 follow 按钮和 friend 按钮
  final bool isMyContent;

  YBDFollowerListItem(this.userInfo, {this.isMyContent = false});

  @override
  _YBDFollowerListItemState createState() => _YBDFollowerListItemState();
}

class _YBDFollowerListItemState extends State<YBDFollowerListItem> {
  @override
  void initState() {
    super.initState();

    eventBus.on<YBDFollowBusEvent>().listen((YBDFollowBusEvent event) async {
      logger.v('received follow user event : ${event.userId}');

      // 关注该粉丝后与粉丝成为好友
      if (widget.isMyContent && event.userId == widget.userInfo!.id) {
        widget.userInfo!.friend = true;
        setState(() {});
      }
    });

    eventBus.on<YBDUnFollowBusEvent>().listen((YBDUnFollowBusEvent event) async {
      logger.v('received unFollow user event : ${event.userId}');

      // 取关该粉丝后与粉丝取消好友关系
      if (widget.isMyContent && event.userId == widget.userInfo!.id) {
        widget.userInfo!.friend = false;
        setState(() {});
      }
    });
  }
  void initStateRutnqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(shape: BoxShape.rectangle),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
            child: Column(
              children: [
                Container(
                  height: ScreenUtil().setWidth(130),
                  child: Row(
                    children: [
                      YBDRoundAvatar(
                        // 用户头像
                        widget.userInfo!.headimg,
                        sex: widget.userInfo!.sex,
                        lableSrc:
                            (widget.userInfo?.vip ?? 0) == 0 ? "" : "assets/images/vip_lv${widget.userInfo!.vip}.png",
                        userId: widget.userInfo!.id,
                        scene: "B",
                        labelWitdh: 22,
                        labelPadding:
                            EdgeInsets.only(right: ScreenUtil().setWidth(5), bottom: ScreenUtil().setWidth(5)),
                        avatarWidth: 80,
                        sideBorder: false,
                      ),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      Container(
                        width: ScreenUtil().setWidth(360),
                        // padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                SizedBox(width: ScreenUtil().setWidth(10)),
                                ConstrainedBox(
                                  constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(350)),
                                  child: Text(
                                    widget.userInfo?.nickname?.removeBlankSpace() ?? '',
                                    style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: true,
                                  ),
                                ),
                                YBDGenderIcon(widget.userInfo?.sex ?? 0)
                              ],
                            ),
                            SizedBox(
                              height: ScreenUtil().setWidth(44),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(width: ScreenUtil().setWidth(6)),
                                  YBDVipIcon(widget.userInfo?.vipIcon ?? ''),
                                  // 性别标签 0未知，1女2男

                                  // 用户级别标签
                                  widget.userInfo?.level != null ? YBDLevelTag(widget.userInfo?.level) : Container(),
                                  widget.userInfo?.roomlevel != null
                                      ? YBDRoomLevelTag(widget.userInfo?.roomlevel)
                                      : Container(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(child: SizedBox(width: 1)),
                      // 自己的粉丝列表才显示
                      widget.isMyContent
                          ? (widget.userInfo?.friend ?? false)
                              ? _friendButton()
                              : _followButton()
                          : Container(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void build6iXmgoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 好友标签按钮
  Widget _friendButton() {
    return Padding(
      padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
      child: Image.asset(
        "assets/images/n_friends.png",
        width: ScreenUtil().setWidth(40),
      ),
    );
    return Container(
      width: ScreenUtil().setWidth(125),
      height: ScreenUtil().setWidth(56),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
        color: Colors.white.withOpacity(0.3),
      ),
      child: Center(
        child: Text(
          translate('friends'),
          style: TextStyle(
            fontSize: ScreenUtil().setSp(24),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
  void _friendButtonMHBRpoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求关注接口
  _requestFollow(String roomId) {
    ApiHelper.followUser(context, roomId).then((bool value) {
      logger.v('follow user : $roomId result : $value');
    });
  }

  /// 追踪按钮
  Widget _followButton() {
    return YBDScaleAnimateButton(
      onTap: () {
        logger.v('clicked follow btn');
        _requestFollow('${widget.userInfo!.id}');
      },
      child: Padding(
        padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
        child: Image.asset(
          "assets/images/n_follow.png",
          width: ScreenUtil().setWidth(40),
        ),
      ),
      // child: Container(
      //   width: ScreenUtil().setWidth(125),
      //   height: ScreenUtil().setWidth(56),
      //   decoration: BoxDecoration(
      //     borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
      //     color: Color(0xff5694FA),
      //   ),
      //   child: Center(
      //     child: Text(
      //       translate('follow'),
      //       style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
      //     ),
      //   ),
      // ),
    );
  }
  void _followButtonyJyVkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
