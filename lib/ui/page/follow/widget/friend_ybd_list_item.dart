import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/icon_ybd_gender.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../home/widget/list_ybd_item_amount.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/room_ybd_level_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

/// 好友列表 item
class YBDFriendListItem extends StatefulWidget {
  /// 好友信息
  final YBDRoomInfo? roomInfo;

  YBDFriendListItem(this.roomInfo);

  @override
  _YBDFriendListItemState createState() => _YBDFriendListItemState();
}

class _YBDFriendListItemState extends State<YBDFriendListItem> {
  @override
  void initState() {
    super.initState();
  }
  void initStateBaM8joyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(130),
      alignment: Alignment.center,
      // color: Colors.black,
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
      decoration: BoxDecoration(shape: BoxShape.rectangle),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            YBDRoundAvatar(
              // 用户头像
              widget.roomInfo?.headimg,
              // 房间信息没有 sex 字段，默认为 1： 女
              sex: widget.roomInfo?.sex ?? 1,
              lableSrc: (widget.roomInfo?.vip ?? 0) == 0 ? "" : "assets/images/vip_lv${widget.roomInfo?.vip}.png",
              userId: widget.roomInfo?.id,
              scene: "B",
              labelWitdh: 22,
              labelPadding: EdgeInsets.only(
                right: ScreenUtil().setWidth(5),
                bottom: ScreenUtil().setWidth(5),
              ),
              avatarWidth: 80,
              sideBorder: false,
            ),
            SizedBox(width: ScreenUtil().setWidth(10)),
            Container(
              width: ScreenUtil().setWidth(360),
              // padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(350)),
                        child: Text(
                          widget.roomInfo?.nickname?.removeBlankSpace() ?? '',
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(24),
                            color: Colors.white,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      YBDGenderIcon(widget.roomInfo?.sex ?? 0)
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil().setWidth(44),
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: ScreenUtil().setWidth(8)),
                        YBDVipIcon(
                          widget.roomInfo?.vipIcon ?? '',
                          padding: EdgeInsets.all(0),
                        ),
                        // 性别标签 0未知，1女2男

                        // 用户级别标签
                        widget.roomInfo?.level != null ? YBDLevelTag(widget.roomInfo?.level) : Container(),
                        // TODO: 设计图没有房间标签
                        widget.roomInfo?.roomlevel != null ? YBDRoomLevelTag(widget.roomInfo?.roomlevel) : Container(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(child: SizedBox(width: 1)),
            (widget.roomInfo?.live ?? false) ? YBDListItemAmount(widget.roomInfo) : Container(),
          ],
        ),
      ),
    );
  }
  void buildqff4Soyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
