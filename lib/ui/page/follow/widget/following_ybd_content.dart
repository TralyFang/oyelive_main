import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/query_ybd_followed_resp_entity.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import 'following_ybd_list_item.dart';
import 'place_ybd_holder_view.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';

/// 追踪列表内容
class YBDFollowingContent extends StatefulWidget {
  /// 该用户的追踪的主播列表
  final int userId;

  YBDFollowingContent(this.userId);

  @override
  _YBDFollowingContentState createState() => _YBDFollowingContentState();
}

class _YBDFollowingContentState extends State<YBDFollowingContent> with AutomaticKeepAliveClientMixin {
  final int _pageSize = 30;

  /// 追踪列表数组
  List<YBDRoomInfo?>? _data = [];

  /// 加载的页数
  int _page = 0;

  /// 是否为初始状态
  bool _isInitState = true;

  /// 自己的粉丝列表显示 follower 按钮和 friend 按钮
  /// 其他人的粉丝列表不显示这两个按钮
  bool _isMyContent = false;

  /// 列表刷新控制器
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    logger.v('userId : ${widget.userId}');
    _onRefresh();

    // 判断是否为自己的追踪列表
    YBDUserUtil.userId().then((value) {
      setState(() {
        _isMyContent = value == '${widget.userId}';
      });
    });
  }
  void initStateEnmd2oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (null == _data || _data!.isEmpty) {
      logger.v('following list is empty show placeholder');
      return _isInitState ? YBDLoadingCircle() : YBDPlaceHolderView(text: translate('no_following'));
    }

    return Container(
      child: SmartRefresher(
        controller: _refreshController,
        header: YBDMyRefreshIndicator.myHeader,
        footer: YBDMyRefreshIndicator.myFooter,
        enablePullDown: true,
        enablePullUp: true,
        onRefresh: () {
          logger.v("following page pull down refresh");
          _onRefresh();
        },
        onLoading: () {
          logger.v("following page pull up load more");
          _onLoading();
        },
        child: ListView.builder(
          itemCount: _data!.length, // 数据的数量
          itemBuilder: (context, index) {
            return YBDFollowingListItem(_data![index], isMyContent: _isMyContent);
          }, // 类似 cellForRow 函数
        ),
      ),
    );
  }
  void buildYYDwloyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  /// 列表刷新响应方法
  void _onRefresh() async {
    // 刷新时从0开始查数据
    _page = 0;
    print('_onRefresh _page: $_page');
    ApiHelper.queryFollowed(
      context,
      2,
      _page,
      _pageSize,
      widget.userId,
      1,
    ).then((queryFollowedRespEntity) {
      // 刷新数据失败
      if (queryFollowedRespEntity == null || queryFollowedRespEntity.returnCode != Const.HTTP_SUCCESS) {
        _refreshController.loadFailed();
        // 是当前页面则刷新界面
        _cancelInitState();
      } else {
        _data = queryFollowedRespEntity.record?.users;

        if (_data != null && _data!.length < _pageSize) {
          // 没有更多数据
          _refreshController.loadNoData();
        } else {
          _refreshController.resetNoData();
        }

        _refreshController.refreshCompleted();

        // 是当前页面则刷新界面
        _cancelInitState();
      }
    });
  }
  void _onRefreshFVi5Goyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }

  /// 上拉加载更多响应方法
  _onLoading() async {
    YBDQueryFollowedRespEntity? queryFollowedRespEntity = await ApiHelper.queryFollowed(
      context,
      2,
      ++_page * _pageSize,
      _pageSize,
      widget.userId,
      1,
    );

    if (queryFollowedRespEntity == null || queryFollowedRespEntity.returnCode != Const.HTTP_SUCCESS) {
      // 加载数据失败
      _refreshController.loadComplete();
      _refreshController.loadFailed();
      return;
    } else {
      List<YBDRoomInfo?>? moreData = queryFollowedRespEntity.record?.users;
      if (moreData == null) {
        // 没有更多数据
        _refreshController.loadComplete();
        _refreshController.loadNoData();
        return;
      } else {
        if (moreData != null && moreData.length < _pageSize) {
          // 没有更多数据
          _refreshController.loadNoData();
        } else {
          _refreshController.loadComplete();
        }

        _data!.addAll(moreData);
      }
    }

    if (mounted) setState(() {});
  }
}
