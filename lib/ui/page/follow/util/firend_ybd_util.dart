import 'dart:async';


import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';

class YBDFriendUtil {
  static List LETTERS = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
  ];

  /// 好友排序
  static List<YBDRoomInfo?>? sortFriends(List<YBDRoomInfo?>? data) {
    if (null == data) {
      logger.v('friend list is null');
      return null;
    }

    List<YBDRoomInfo?> result = [];

    // 正在开播的好友
    List<YBDRoomInfo?> onlineFriends = data.where((element) => element!.live!).toList();

    // 正在开播的好友按在线人数降序排序
    onlineFriends.sort((a, b) => b!.count!.compareTo(a!.count!));

    if (null != onlineFriends && onlineFriends.isNotEmpty) {
      result.addAll(onlineFriends);
    } else {
      logger.v('online friends is null');
    }

    // 没开播的好友
    List<YBDRoomInfo?> offlineFriends = data.where((element) => !element!.live!).toList();

    if (null != offlineFriends && offlineFriends.isNotEmpty) {
      // 按昵称首字母排序（A~Z~数字~特殊字符）
      result.addAll(startWithLetter(offlineFriends));
      result.addAll(startWithDigit(offlineFriends));
      result.addAll(startWithOther(offlineFriends));
    } else {
      logger.v('offline friends is null');
    }

    return result;
  }

  /// 以字母开头的的好友，已排序
  static List<YBDRoomInfo?> startWithLetter(List<YBDRoomInfo?> sourceList) {
    List<YBDRoomInfo?> result = [];

    for (int i = 0; i < LETTERS.length; i++) {
      // 按指定的首字母顺序排序
      List<YBDRoomInfo?> letterNames = sourceList.where((roomInfo) {
        if (null != roomInfo!.nickname) {
          return roomInfo.nickname!.toUpperCase().startsWith(LETTERS[i]);
        } else {
          return false;
        }
      }).toList();

      if (null != letterNames) {
        // 按 ASCII 编码排序
        result.addAll(sortByASCII(letterNames));
      }
    }

    return result;
  }

  /// 以数字开头的的好友，已排序
  static List<YBDRoomInfo?> startWithDigit(List<YBDRoomInfo?> sourceList) {
    // 昵称以数字开头的好友
    List<YBDRoomInfo?> digitNames = sourceList.where((roomInfo) => nickNameStartWithDigit(roomInfo!)).toList();

    // 按 ASCII 编码排序
    return sortByASCII(digitNames);
  }

  /// 以特殊字符开头的好友，已排序
  static List<YBDRoomInfo?> startWithOther(List<YBDRoomInfo?> sourceList) {
    List<YBDRoomInfo?> otherNames = sourceList.where((roomInfo) {
      if (nickNameStartWithDigit(roomInfo!) || nickNameStartWithLetter(roomInfo)) {
        return false;
      } else {
        return true;
      }
    }).toList();

    return sortByASCII(otherNames);
  }

  /// 昵称以数字开头
  static bool nickNameStartWithDigit(YBDRoomInfo roomInfo) {
    if (null != roomInfo.nickname && roomInfo.nickname!.isNotEmpty) {
      return YBDNumericUtil.isNumeric(roomInfo.nickname!.substring(0, 1));
    } else {
      return false;
    }
  }

  /// 昵称以字母开头
  static bool nickNameStartWithLetter(YBDRoomInfo roomInfo) {
    if (null != roomInfo.nickname && roomInfo.nickname!.isNotEmpty) {
      return LETTERS.contains(roomInfo.nickname!.substring(0, 1).toUpperCase());
    } else {
      return false;
    }
  }

  /// 好友按 ASCII 编码排序
  static List<YBDRoomInfo?> sortByASCII(List<YBDRoomInfo?> sourceList) {
    List<YBDRoomInfo?> result = [];

    if (null != sourceList) {
      result.addAll(sourceList);

      // 按 ASCII 编码排序
      result.sort((a, b) {
        if ((a!.nickname ?? '').isNotEmpty && (b!.nickname ?? '').isNotEmpty) {
          return a.nickname!.compareTo(b.nickname!);
        } else {
          return 1;
        }
      });
    }

    return result;
  }
}
