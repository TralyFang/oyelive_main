import 'dart:async';

import 'dart:convert' as convert;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/file_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/zip_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../baggage/entity/baggage_ybd_entrance_entity.dart';
import '../../home/entity/car_ybd_list_entity.dart';
import '../entity/carbean.dart';
import '../entity/downloadbean.dart';
import '../handle/download_ybd_event.dart';

/// 事件类型
enum EntrancesListEvent {
  // 初始状态
  Init,
  // 第一次加载完成
  InitFinish,
  // 加载页面数据
  Refresh,
  // 下一页
  NextPage,
  // 上拉加载完成
  PullUpDone,
  // 刷新完成
  RefreshDone,

  // 背包加载页面数据
  RefreshBaggage,
  // 更新下载进度
  Progress,
  Cancel,
  Pause,
  Resume,
  Download,
  Retry,
}

enum DownloadStatus {
  complete,
  error,
  inProgress,
}

/// 座驾列表
class YBDEntrancesListBloc extends Bloc<EntrancesListEvent, YBDEntrancesBlocState> {
  BuildContext context;

  /// 座驾动画下载列表
  List<YBDCarBean>? _carBeanList = [];

  /// 座驾列表
  List? _carRecordList = [];

  /// 最后点击播放的动画（自动下载完后播放否者不播放）
  int? lastDownloadId;

  /// 开始加载的位置start
  int _start = 0;

  /// 控制器状态 0:初始 1:正在刷新 2:已完成 3:上拉加载完毕 4:下拉无数据
  int _status = 2;

  @override
  YBDEntrancesListBloc(this.context) : super(YBDEntrancesBlocState(isInit: true));

  @override
  Stream<YBDEntrancesBlocState> mapEventToState(EntrancesListEvent event) async* {
    switch (event) {
      case EntrancesListEvent.Init:
        _refresh(init: true);
        break;
      case EntrancesListEvent.InitFinish:
        yield YBDEntrancesBlocState(carBeans: _carBeanList, carRecords: _carRecordList);
        break;
      case EntrancesListEvent.Refresh:
        yield YBDEntrancesBlocState(carBeans: _carBeanList, carRecords: _carRecordList, refreshStatus: 1);
        _refresh();
        break;
      case EntrancesListEvent.NextPage:
        _start++;
        _refresh(load: true, start: _start * Const.PAGE_SIZE);
        // yield YBDEntrancesBlocState(carBeans: _carBeanList, carRecords: _carRecordList, refreshStatus: _status);
        break;
      case EntrancesListEvent.PullUpDone:
        yield YBDEntrancesBlocState(carBeans: _carBeanList, carRecords: _carRecordList, refreshStatus: _status);
        break;
      case EntrancesListEvent.RefreshDone:
        yield YBDEntrancesBlocState(carBeans: _carBeanList, carRecords: _carRecordList, refreshStatus: 2);
        break;
      case EntrancesListEvent.RefreshBaggage:
        yield YBDEntrancesBlocState(carBeans: _carBeanList, carRecords: _carRecordList);
        break;
      case EntrancesListEvent.Progress:
        yield YBDEntrancesBlocState(carBeans: _carBeanList, carRecords: _carRecordList);
        break;
    }
  }
  void mapEventToStateUnlt0oyelive(EntrancesListEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<void> _refresh({bool init = false, bool load = false, int start = 0}) async {
    _requestDiscountList();
    if (load) {
      List<YBDCarListRecord?>? _temp = await _requestCarList(init: init, load: load, start: start);
      _carRecordList!.addAll(_temp ?? []);
    } else {
      _start = 0;
      _carRecordList = await _requestCarList(init: init, start: start);
    }
    _carBeanList = _carBeanListFrom(context, _carRecordList);
  }
  void _refreshEgtzWoyelive({bool init = false, bool load = false, int start = 0})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求折扣信息数据
  _requestDiscountList() {
    ApiHelper.queryDiscountList(context, 'CAR').then((discountEntity) async {
      if (discountEntity == null) {
        // 刷新数据失败
        logger.v('queryCARDiscount failed');
      } else {
        logger.v('queryCARDiscount SUCCESS');
        String frameJson = convert.jsonEncode(discountEntity.record);
        YBDSPUtil.save(Const.SP_KEY_DISCOUNT_CAR, frameJson);
      }
    });
  }

  /// 请求座驾列表数据
  Future<List<YBDCarListRecord?>?> _requestCarList({bool init = false, bool load = false, int start = 0}) async {
    YBDCarListEntity? carListEntity = await ApiHelper.carList(context, start: start);

    if (null != carListEntity && carListEntity.returnCode == Const.HTTP_SUCCESS) {
      logger.v('queryCAR SUCCESS');
      logger.v('22.8.25--car:start:$start--len:${carListEntity.record!.length}--load:$load');
      if (load) {
        _status = carListEntity.record!.length < Const.PAGE_SIZE ? 4 : 3;
        add(EntrancesListEvent.PullUpDone);
      } else {
        init ? add(EntrancesListEvent.InitFinish) : add(EntrancesListEvent.RefreshDone);
      }
      return carListEntity.record;
    } else {
      logger.v('queryCAR failed');
      if (load) {
        _status = 4;
        add(EntrancesListEvent.PullUpDone);
      } else {
        init ? add(EntrancesListEvent.InitFinish) : add(EntrancesListEvent.RefreshDone);
      }
      return null;
    }
  }

  /// 从背包拿座驾列表
  /// 在[_YBDBaggageEntranceContentState]里调用
  void inertDataFromBaggage(List<YBDBaggageEntranceRecordItemList?>? data) async {
    _requestDiscountList();
    _carRecordList = data;
    _carBeanList = _carBeanListFrom(context, _carRecordList);
    add(EntrancesListEvent.RefreshBaggage);
  }
  void inertDataFromBaggageMwKNloyelive(List<YBDBaggageEntranceRecordItemList?>? data)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 从座驾列表中获取座驾动画下载列表
  List<YBDCarBean>? _carBeanListFrom(BuildContext context, List? records) {
    if (records != null && records.isNotEmpty) {
      List<YBDCarBean> carBeanList = [];

      // 组装 car bean 对象
      for (int i = 0; i < records.length; i++) {
        // 保存动画文件的目录：Animation/audi_garage/2
        String savePath = 'Animation/${records[i].animationInfo.foldername}/${records[i].animationInfo.version}';
        YBDCarBean carBean = YBDCarBean(
          records[i].name,
          YBDImageUtil.animZipFile(context, records[i].animationInfo.foldername).toString(),
          records[i].id,
          savePath: savePath,
        );
        carBeanList.add(carBean);
        // logger.v('url : ${carBean.url} id : ${carBean.id} savePath: ${carBean.savePath}');
      }

      return carBeanList;
    } else {
      return null;
    }
  }

  /// 下载进度
  void _updateProgress({
    DownloadStatus? downloadStatus,
    double? downloadProgress,
    String? originUrl,
    String? downloadedFilePath,
  }) {
    if (downloadStatus == DownloadStatus.inProgress) {
      add(EntrancesListEvent.Progress);
    }

    for (int i = 0; i < _carBeanList!.length; i++) {
      logger.v('updateProgress i:$i taskId:${_carBeanList![i].url}');

      if (_carBeanList![i].url != null && _carBeanList![i].url.compareTo(originUrl!) == 0) {
        if (downloadStatus == DownloadStatus.complete) {
          _carBeanList![i].progress = downloadProgress;
          _unzipSvga(i, downloadedFilePath);
        } else if (downloadStatus == DownloadStatus.error) {
          _carBeanList![i].state = 2;
        } else if (downloadStatus == DownloadStatus.inProgress) {
          double progress = YBDCommonUtil.formatDoubleNum(downloadProgress, 2);
          logger.v('progress :$progress  arg.progress: $downloadProgress');
          _carBeanList![i].progress = progress;
          _carBeanList![i].state = 3;
        } else {
          _carBeanList![i].state = 0;
        }

        break;
      }
    }
  }

  /// 队列下载
  void download(int? id) async {
    for (int index = 0; index < _carBeanList!.length; index++) {
      if (_carBeanList![index].id == id) {
        bool isSave = await YBDFileUtil.isAnimationDownloaded(
          _carRecordList![index].animationInfo,
          downLoadType: DownLoadType.svg_entrances,
        );
        // 如果该目录下文件被删了，那也需要重新下载
        List path = await YBDFileUtil.getSvgPlayPath(_carRecordList![index].animationInfo);
        isSave = (path[0] != null);

        lastDownloadId = id;
        if (isSave) {
          // 本地要是已经下载好  就直接播放
          List path = await YBDFileUtil.getSvgPlayPath(_carRecordList![index].animationInfo);
          logger.v('download Animation is exists path:$path  start play');
          eventBus.fire(YBDDownloadLoadEvent(
            path[0],
            0,
            url_mp3: path[1],
            replaceDto: _carRecordList![index].replaceDto,
          ));
        } else {
          logger.v('_carBeanList[index].url: ${_carBeanList![index]}');
          // 用缓存下载文件
          DefaultCacheManager().getFileStream(_carBeanList![index].url, withProgress: true).listen((event) async {
            if (event is FileInfo) {
              logger.v('getFileStream FileInfo: ${event.file.path}');
              final filePath = '${_carBeanList![index].savePath}/${_carBeanList![index].name}';
              logger.v('file filePath: $filePath');

              // 获取文件报错
              if (null == event.file) {
                logger.w('download background audio file is empty');
                _updateProgress(
                  downloadStatus: DownloadStatus.error,
                  downloadProgress: 0,
                  originUrl: event.originalUrl,
                );
                return;
              }

              _updateProgress(
                downloadStatus: DownloadStatus.complete,
                downloadProgress: 100,
                originUrl: event.originalUrl,
                downloadedFilePath: event.file.path,
              );
            } else if (event is DownloadProgress) {
              logger.v('DownloadProgress: ${event.progress}');
              _updateProgress(
                downloadStatus: DownloadStatus.inProgress,
                downloadProgress: event.progress,
                originUrl: event.originalUrl,
              );
            } else {
              logger.w('down load slog audio file stream type error');
            }
          });
        }
      }
    }
  }
  void downloadwIpTYoyelive(int? id)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 解压座驾动画文件
  void _unzipSvga(int unzipIndex, String? zipFilePath) async {
    if (unzipIndex >= 0 && _carRecordList != null && _carRecordList!.length > unzipIndex) {
      String outputPath =
          'Animation/${_carRecordList![unzipIndex].animationInfo.foldername}/${_carRecordList![unzipIndex].animationInfo.version}';
      logger.v('outputPath: $outputPath');
      outputPath = await YBDCommonUtil.getResourceDir(outputPath);
      // 手机目录：/storage/emulated/0/Android/data/com.oyetalk.tv/files/OyeTalk/Animation
      bool unzip = await YBDZipUtil.asyncUnZip(outputPath, zipFilePath);

      // 解压完才通知页面  状态修改
      if (unzip) {
        _carBeanList![unzipIndex].state = 1;
        add(EntrancesListEvent.Progress);
      }

      // 判断是否最后点击下载的动画  是的话  自动播放  否者不播放
      if (lastDownloadId == _carRecordList![unzipIndex].id) {
        List path = await YBDFileUtil.getSvgPlayPath(_carRecordList![unzipIndex].animationInfo);
        eventBus.fire(YBDDownloadLoadEvent(
          path[0],
          0,
          url_mp3: path[1],
          replaceDto: _carRecordList![unzipIndex].replaceDto,
        ));
      }
    }
  }
  void _unzipSvgaAxHIYoyelive(int unzipIndex, String? zipFilePath)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// special 列表数据状态
class YBDEntrancesBlocState {
  /// 初始状态
  bool isInit;

  /// 下载信息 数据
  List<YBDCarBean>? carBeans;

  /// 座驾 数据
  List? carRecords;

  /// 下拉刷新状态 0:初始 1:正在刷新 2:已完成 3:上拉加载完毕 4:下拉无数据
  int refreshStatus;

  YBDEntrancesBlocState({this.isInit: false, this.carBeans, this.carRecords, this.refreshStatus = 0});
}
