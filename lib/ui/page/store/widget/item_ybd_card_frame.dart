import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/module/entity/card_ybd_frame_store_entity.dart';
import 'package:oyelive_main/ui/page/store/widget/card_ybd_frame_buy_step.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/result_ybd_bean.dart';
import '../../../widget/svga_ybd_icon.dart';
import '../entity/price_ybd_info_entity.dart';
import 'bs_ybd_buy.dart';

class YBDCardFrameItem extends StatefulWidget {
  YBDCardFrameStoreRecord? entity;

  Function? onSuccess;

  bool isBag;
  YBDCardFrameItem(this.entity, {this.onSuccess, this.isBag: false});
  @override
  YBDCardFrameItemState createState() => new YBDCardFrameItemState();
}

class YBDCardFrameItemState extends BaseState<YBDCardFrameItem> {
  equip() async {
    showLockDialog();
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.equipItem(context, personalId: widget.entity!.personalId, goodsType: "CARD_FRAME");
    dismissLockDialog();
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      widget.onSuccess?.call();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  unequip() async {
    showLockDialog();
    YBDResultBeanEntity? resultBeanEntity =
        await ApiHelper.unequipItem(context, personalId: widget.entity!.personalId, goodsType: "CARD_FRAME");
    dismissLockDialog();
    if (resultBeanEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDToastUtil.toast('success');
      widget.onSuccess?.call();
    } else {
      YBDToastUtil.toast(resultBeanEntity?.returnMsg);
    }
  }

  @override
  Widget myBuild(BuildContext context) {
    bool _stint = YBDCommonUtil.stintBuy(type: widget.entity!.conditionType, lv: widget.entity!.conditionExtends);
    return GestureDetector(
      onTap: () async {
        if (widget.isBag) {
          widget.entity!.equipped! ? unequip() : equip();
          return;
        }

        // 弹出底部购买弹框
        List<YBDPriceInfoEntity?>? cardFrameDiscout = await YBDSPUtil.getDiscount(Const.SP_KEY_DISCOUNT_CARD_FRAME);

        YBDCardFrameBuyStep cardFrameBuyStep = new YBDCardFrameBuyStep(cardFrameDiscout, widget.entity);
        showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.0),
              topRight: Radius.circular(8.0),
            ),
            side: BorderSide(style: BorderStyle.none),
          ),
          builder: (context) => YBDBuySheet(
            buyStep: cardFrameBuyStep,
            onSuccessBuy: () {
              widget.onSuccess?.call();
            },
            currency: widget.entity!.currency,
            stintBuy: _stint,
          ),
        );
      },
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
            child: Column(
              children: [
                ColorFiltered(
                  colorFilter: YBDTPGlobal.getGreyFilter(_stint),
                  child: Container(
                    width: ScreenUtil().setWidth(330),
                    height: ScreenUtil().setWidth(237),
                    color: Colors.white.withOpacity(0.1),
                    child: Stack(
                      children: [
                        Center(
                          child: YBDNetworkImage(
                            imageUrl: YBDImageUtil.frame(
                                context, widget.isBag ? widget.entity!.thumbnail : widget.entity!.image, "C"),
                            width: ScreenUtil().setWidth(182),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: ScreenUtil().setWidth(93),
                  width: ScreenUtil().setWidth(330),
                  color: Colors.white.withOpacity(0.2),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: ScreenUtil().setWidth(14)),
                        child: Text(
                          widget.entity!.name!,
                          style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setWidth(14),
                      ),
                      widget.isBag
                          ? Padding(
                              padding: EdgeInsets.only(left: ScreenUtil().setWidth(14)),
                              child: Text(
                                "Expiry Date: ${YBDDateUtil.getExpireDate(widget.entity!.expireAfter)}",
                                style:
                                    TextStyle(fontSize: ScreenUtil().setSp(20), color: Colors.white.withOpacity(0.7)),
                              ),
                            )
                          : Row(
                              children: [
                                SizedBox(
                                  width: ScreenUtil().setWidth(14),
                                ),
                                Image.asset(
                                  YBDCommonUtil.getCurrencyImg(widget.entity!.currency),
                                  width: ScreenUtil().setWidth(22),
                                ),
                                SizedBox(
                                  width: ScreenUtil().setWidth(8),
                                ),
                                Text(
                                  YBDNumericUtil.format(widget.entity!.price! * 10)!,
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setWidth(24),
                                    color: Color(0xffFFC450),
                                  ),
                                ),
                                Spacer(),
                                Image.asset(
                                  'assets/images/icon_clocks.webp',
                                  width: ScreenUtil().setWidth(20),
                                  height: ScreenUtil().setWidth(20),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(10)),
                                Text(
                                  // 有效天数
                                  '10 days',
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(20),
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(18)),
                              ],
                            ),
                    ],
                  ),
                )
              ],
            ),
          ),
          if (!widget.isBag)
            Positioned(
              top: 15.px,
              left: 10.px,
              child: YBDLevelStintTag(
                stintType: widget.entity?.conditionType,
                stintLv: widget.entity?.conditionExtends,
                lock: _stint,
              ),
            ),
          Positioned(
            top: ScreenUtil().setWidth(20),
            right: ScreenUtil().setWidth(20),
            child: YBDSvgaIcon(
              imgUrl: widget.isBag
                  ? (widget.entity?.image?.split(',')?.first ?? '')
                  : (widget.entity?.animation?.split(',')?.first ?? ''),
              white: true,
              grey: !widget.isBag && _stint,
            ),
          ),
          if (widget.entity!.equipped ?? false)
            Positioned(
                top: 0,
                right: 0,
                child: Transform.scale(
                  scale: 1.25,
                  child: Image.asset(
                    "assets/images/icon_equip.webp",
                    width: ScreenUtil().setWidth(60),
                  ),
                )),
        ],
      ),
    );
  }
  void myBuildpdZbioyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void initStatehqL3soyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDCardFrameItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetM2ZTIoyelive(YBDCardFrameItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
