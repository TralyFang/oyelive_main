import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_record_extend.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';
import '../../../../common/util/download_ybd_manager.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../bloc/entrances_ybd_block.dart';
import '../handle/download_ybd_event.dart';
import 'entrances_ybd_list_item.dart';
import 'svga_ybd_play.dart';
import '../../../widget/loading_ybd_circle.dart';

import '../../../../main.dart';

/// 商店 座驾
class YBDEntrancesContent extends StatefulWidget {
  YBDEntrancesContent();

  @override
  _YBDEntrancesContentState createState() => _YBDEntrancesContentState();
}

class _YBDEntrancesContentState extends State<YBDEntrancesContent> {
  /// 获取全局单例的downLoadBus
  var downLoadBus = YBDDownLoadManager.instance;
  bool _isPlaying = false;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    eventBus.on<YBDDownloadLoadEvent>().listen((YBDDownloadLoadEvent event) {
      if (event.result == 0) {
        logger.v('_isPlaying 0001:${_isPlaying}  event.url_svga:${event.url_svga}');
        if (_isPlaying) {
          return;
        }
        try {
          _playSvga(event.url_svga, event.url_mp3, isLocal: event.isLocal, replaceDto: event.replaceDto);
        } catch (e) {
          logger.v(e);
        }
      }
    });
  }
  void initStatej2zlLoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _playSvga(String? path, String? mp3file, {bool isLocal = false, YBDCarListRecordExtend? replaceDto}) async {
    logger.v('playSvga path : $path mp3 file $mp3file isLocal:$isLocal');

    if (mounted) {
      showDialog(
          barrierDismissible: true,
          context: context,
          builder: (BuildContext builder) {
            return YBDSvgaPlayerPage(
              url: path,
              mp3File: mp3file,
              callBack: playerStateCallBack,
              isLocal: isLocal,
              replaceSvgaEntity: replaceDto,
            );
          });
    }
  }

  /// 动画播放状态
  Future playerStateCallBack(bool isPlaying) async {
    logger.v('playerStateCallBack isPlaying:$_isPlaying');
    _isPlaying = isPlaying;
  }
  void playerStateCallBackqIthooyelive(bool isPlaying)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<YBDEntrancesListBloc, YBDEntrancesBlocState>(
        builder: (context, state) {
          if (state.isInit) {
            // 页面加载框
            return YBDLoadingCircle();
          } else {
            _refreshController.resetNoData();
            if (state.refreshStatus == 2) _refreshController.refreshCompleted();
            if (state.refreshStatus == 3) _refreshController.loadComplete();
            if (state.refreshStatus == 4) _refreshController.loadNoData();
            return SmartRefresher(
              controller: _refreshController,
              header: YBDMyRefreshIndicator.myHeader,
              footer: YBDMyRefreshIndicator.myFooter,
              enablePullDown: true,
              enablePullUp: true,
              onRefresh: () {
                logger.v("YBDEntrancesContent page pull down refresh");
                BlocProvider.of<YBDEntrancesListBloc>(context).add(EntrancesListEvent.Refresh);
              },
              onLoading: () {
                logger.v("YBDEntrancesContent page pull up refresh");
                BlocProvider.of<YBDEntrancesListBloc>(context).add(EntrancesListEvent.NextPage);
              },
              child: (null == state.carRecords || state.carRecords!.isEmpty)
                  ? YBDPlaceHolderView(text: translate('no_data'))
                  : ListView.builder(
                      itemCount: state.carRecords!.length,
                      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(24)),
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            logger.v('clicked Entrances item');
                          },
                          child: YBDEntrancesListItem(state.carRecords![index], state.carBeans![index]),
                        );
                      },
                    ),
            );
          }
        },
      ),
    );
  }
  void buildEz9Oloyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _refreshController.dispose();
    downLoadBus!.clean();
    super.dispose();
  }
}
