import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class YBDRoundCheckBox extends StatefulWidget {
  var value = false;

  Function(bool)? onChanged;

  YBDRoundCheckBox({Key? key, required this.value, this.onChanged}) : super(key: key);

  @override
  _YBDRoundCheckBoxState createState() => _YBDRoundCheckBoxState();
}

class _YBDRoundCheckBoxState extends State<YBDRoundCheckBox> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
          onTap: () {
            widget.value = !widget.value;
            widget.onChanged!(widget.value);
          },
          child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: widget.value
                  ? Image.asset(
                      "assets/images/login/agree_chose.webp",
                      width: ScreenUtil().setWidth(68),
                    )
                  : Image.asset(
                      "assets/images/login/agree_no_chose.webp",
                      width: ScreenUtil().setWidth(68),
                    ))),
    );
  }
  void buildI65zmoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
