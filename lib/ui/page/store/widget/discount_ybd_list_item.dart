import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../buy/buy_ybd_gifts_page.dart';
import '../../home/entity/discount_ybd_list_entity.dart';

/// Discount列表 item
class YBDDiscountListItem extends StatefulWidget {
  /// item 在列表中的位置
  final int curIndex;

  /// 列表数据源
  final YBDDiscountListRecord? discountListRecord;
  final Function? onBuycallBack;

  YBDDiscountListItem(this.curIndex, this.discountListRecord, {this.onBuycallBack});

  @override
  _YBDDiscountListItemState createState() => _YBDDiscountListItemState();
}

class _YBDDiscountListItemState extends BaseState<YBDDiscountListItem> {
  @override
  void initState() {
    super.initState();
  }
  void initState9gX89oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String? url;

  @override
  Widget myBuild(BuildContext context) {
    if (widget.discountListRecord == null) {
      return Container();
    }

    url =
        YBDImageUtil.getDiscountImgUrl(context, widget.discountListRecord!.itemType, widget.discountListRecord?.thumbnail);
    // print("-----------------url :$url");
    return Container(
      width: ScreenUtil().setWidth(350),
      decoration: BoxDecoration(
        color: Color(0x26FFFFFF),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
      ),
      child: GestureDetector(
        onTap: () {
          // 弹出底部购买弹框
          showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                topRight: Radius.circular(8.0),
              ),
              side: BorderSide(style: BorderStyle.none),
            ),
            builder: (context) => AnimatedPadding(
              padding: EdgeInsets.only(left: 0, right: 0, bottom: 0, top: 0),
              duration: const Duration(milliseconds: 2500),
              child: YBDBuyGiftsBottomWindowPage(
                BuyType.discount,
                discountListRecord: widget.discountListRecord,
                onBuyCallBack: () {
                  widget.onBuycallBack!();
                },
              ),
            ),
          );
        },
        child: Column(
          children: [
            Container(
              // item 图片
              height: ScreenUtil().setWidth(237),
              alignment: Alignment.bottomCenter,
              child: Stack(
                alignment: Alignment.topCenter,
                children: <Widget>[
                  Positioned(
                    top: ScreenUtil().setWidth(40),
                    child: Center(
                      child: YBDNetworkImage(
                        height: ScreenUtil().setWidth(200),
                        width: ScreenUtil().setWidth(140),
                        imageUrl: url ?? '',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Positioned(
                      right: ScreenUtil().setWidth(14),
                      top: ScreenUtil().setWidth(18),
                      child: (YBDCommonUtil.isShowPeriodDay(widget.discountListRecord?.endTime, 29345252839000))
                          ? (Container())
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                  Image.asset(
                                    'assets/images/icon_clocks.webp',
                                    width: ScreenUtil().setWidth(20),
                                    height: ScreenUtil().setWidth(20),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(10)),
                                  Text(
                                    ///到期时间
                                    '${YBDDateUtil.getDiscountExpireDate(widget.discountListRecord?.endTimestamp)}',
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(18),
                                      color: Color(0xffeaeaea),
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )
                                ]))
                ],
              ),
            ),
            Container(
              // 图片底部部分
              decoration: BoxDecoration(
                color: Color(0x26FFFFFF),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(8.0),
                  bottomRight: Radius.circular(8.0),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(height: ScreenUtil().setWidth(15)),
                  Container(
                    height: ScreenUtil().setWidth(25),
                    padding: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
                    // color: Colors.amberAccent,
                    child: Row(
                      children: [
                        Text(
                          // 名称
                          widget.discountListRecord!.name ?? '',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(22),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Expanded(child: Container()),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setWidth(15)),
                  Row(
                    children: [
                      SizedBox(width: ScreenUtil().setWidth(16)),
                      Image.asset(
                        'assets/images/topup/y_top_up_beans@2x.webp',
                        width: ScreenUtil().setWidth(20),
                        height: ScreenUtil().setWidth(22),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      Text(
                        // 价格
                        '${YBDCommonUtil.formatNum(widget.discountListRecord?.originalPrice)}',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          color: Color(0xffEBB01E),
                          fontWeight: FontWeight.w400,
                          decoration: TextDecoration.lineThrough,
                          decorationColor: const Color(0xffEBB01E),
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      Text(
                        // 价格
                        '${YBDCommonUtil.formatNum(widget.discountListRecord?.discountPrice)}',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          color: Color(0xffffe927),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setWidth(15)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildM3szhoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
