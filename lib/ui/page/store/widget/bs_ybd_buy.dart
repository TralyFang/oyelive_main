import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/buy/buy_ybd_step.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../entity/price_ybd_info_entity.dart';
import 'package:redux/redux.dart';

class YBDBuySheet extends StatefulWidget {
  BuyStep? buyStep;

  Function? onSuccessBuy;

  String? currency;

  bool stintBuy;

  YBDBuySheet({this.buyStep, this.onSuccessBuy, this.currency = Const.CURRENCY_BEAN, this.stintBuy = false});

  @override
  YBDBuySheetState createState() => new YBDBuySheetState();
}

class YBDBuySheetState extends BaseState<YBDBuySheet> {
  YBDUserInfo? userInfo;

  int curItem = 0;

  int currentPrice = 0;

  bool insufficientBalance = true;

  List<YBDPriceInfoEntity?>? getPriceInfo() {
    return widget.buyStep!.data!.priceInfo;
  }

  String? getOriginalPrice() {
    return widget.buyStep!.data!.items![curItem].price;
  }

  String? getDiscoutPrice() {
    return widget.buyStep!.data!.items![curItem].discountPrice;
  }

  bool hasDisCount() {
    bool result = getPriceInfo()![curItem]!.discount != "1.0";
    return result;
  }
  void hasDisCountjxaavoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  buyNow() async {
    if (widget.stintBuy) {
      logger.v('stint buy!');
      return;
    }
    if (insufficientBalance) {
      YBDCommonUtil.gotoRechargePage(widget.currency);
      YBDToastUtil.toast(translate('insufficient_balance'));
      return;
    }
    showLockDialog(info: "Buying");
    bool isSuccess = await widget.buyStep!.onBuy(context, curItem, currency: widget.currency);
    dismissLockDialog();
    if (isSuccess) {
      YBDToastUtil.toast(translate('success'));
      Navigator.pop(context);
      widget.onSuccessBuy?.call();
    }
  }

  _choseItem(int i) {
    curItem = i;
    currentPrice = int.parse(hasDisCount() ? getDiscoutPrice()! : getOriginalPrice()!);
    insufficientBalance = !YBDCommonUtil.checkIsEnoughByCurrency(widget.currency, currentPrice.toString());
    setState(() {});
  }

  Widget _getItemWidget(int i) {
    String unitTxt = "";
    switch (getPriceInfo()![i]!.unitType) {
      case 1: // day
        if (int.parse(getPriceInfo()![i]!.unit!) > 1) {
          unitTxt = " days";
        } else {
          unitTxt = " day";
        }
        break;
      case 3: // month
      default:
        if (int.parse(getPriceInfo()![i]!.unit!) > 1) {
          unitTxt = " months";
        } else {
          unitTxt = " month";
        }
        break;
    }

    return GestureDetector(
        onTap: () {
          print('_getItemWidget------$i');
          _choseItem(i);
        },
        child: (i == curItem)
            ? Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(142),
                height: ScreenUtil().setWidth(60),
                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
                  border: Border.all(color: Color(0xff5B9CE3), width: 1.px as double),
                  color: Color(0xffD7F5FB),
                ),
                child: Text('${widget.buyStep!.data!.items![curItem].itemName}',
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xff333333),
                      fontWeight: FontWeight.w400,
                    )))
            : Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(142),
                height: ScreenUtil().setWidth(60),
                margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(14)),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
                    border: Border.all(color: Color(0xff5B9CE3), width: ScreenUtil().setWidth(1))),
                child: Text('${widget.buyStep!.data!.items![i].itemName}',
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(24),
                      color: Color(0xff333333),
                      fontWeight: FontWeight.w400,
                    ))));
  }
  void _getItemWidgetnsKbjoyelive(int i) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);

    if (userInfo == null) {
      userInfo = store!.state.bean;

      _choseItem(0);
    }
    if (getPriceInfo() == null || getPriceInfo()!.isEmpty) {
      return Container(
          height: ScreenUtil().setWidth(560),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(16))),
            color: Colors.white,
          ));
    }
    return Container(
      height: ScreenUtil().setWidth(widget.buyStep!.data!.bigLayout ? 600 : 560),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(16))),
        color: Colors.white,
      ),
      child: Column(
        children: [
          Container(
            height: ScreenUtil().setWidth(widget.buyStep!.data!.bigLayout ? 280 : 240),
            child: Row(
              children: [
                SizedBox(
                  width: ScreenUtil().setWidth(16),
                ),
                if (widget.buyStep!.data!.showImage != null) ...[
                  widget.buyStep!.data!.showImage!,
                  if (widget.buyStep!.getInfoLayout() == InfoLayout.classic)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: ScreenUtil().setWidth(80),
                        ),
                        Row(
                          children: [
                            Image.asset(
                              YBDCommonUtil.getCurrencyImg(widget.currency),
                              width: ScreenUtil().setWidth(29),
                              height: ScreenUtil().setWidth(31),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(1),
                            ),
                            Text(
                              '${YBDCommonUtil.formatNum(hasDisCount() ? getDiscoutPrice() : getOriginalPrice())}',
                              style: TextStyle(
                                  fontSize: ScreenUtil().setSp(36),
                                  color: Color(0xffEBB01E),
                                  fontWeight: FontWeight.w600),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(10),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil().setWidth(30),
                          child: hasDisCount()
                              ? Row(children: [
                                  SizedBox(
                                    width: ScreenUtil().setWidth(4),
                                  ),
                                  Image.asset(
                                    YBDCommonUtil.getCurrencyGreyImg(widget.currency),
                                    width: ScreenUtil().setWidth(18),
                                    height: ScreenUtil().setWidth(18),
                                  ),
                                  Text(
                                    '${getOriginalPrice()}',
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(22),
                                      color: Colors.grey,
                                      // color: Color(0xffEBB01E),
                                      fontWeight: FontWeight.w400,
                                      decoration: TextDecoration.lineThrough,
                                      decorationColor: Colors.grey,
                                    ),
                                  ),
                                ])
                              : SizedBox(),
                        ),
                        SizedBox(
                          height: ScreenUtil().setWidth(56),
                        ),
                        Text(
                          ' ${widget.buyStep!.data!.productName}',
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(24), color: Colors.black, fontWeight: FontWeight.w400),
                        )
                      ],
                    ),
                ],
                if (widget.buyStep!.makeLeftWidget(hasDisCount(), getDiscoutPrice(), getOriginalPrice()) != null)
                  widget.buyStep!.makeLeftWidget(hasDisCount(), getDiscoutPrice(), getOriginalPrice())!,
                if (widget.buyStep!.getInfoLayout() == InfoLayout.tight)
                  Container(
                    height: ScreenUtil().setWidth(168),
                    margin: EdgeInsets.only(
                        top: ScreenUtil().setWidth(widget.buyStep!.data!.bigLayout ? 100 : 60),
                        left: ScreenUtil().setWidth(widget.buyStep!.data!.bigLayout ? 20 : 44)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisSize: MainAxisSize.min,
                      children: [
                        Spacer(
                          flex: 13,
                        ),
                        Row(
                          children: [
                            Image.asset(
                              YBDCommonUtil.getCurrencyImg(widget.currency),
                              width: ScreenUtil().setWidth(29),
                              height: ScreenUtil().setWidth(31),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(1),
                            ),
                            Text(
                              '${YBDCommonUtil.formatNum(hasDisCount() ? getDiscoutPrice() : getOriginalPrice())}',
                              style: TextStyle(
                                  fontSize: ScreenUtil().setSp(36),
                                  color: Color(0xffEBB01E),
                                  fontWeight: FontWeight.w600),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(10),
                            ),
                            hasDisCount()
                                ? Row(children: [
                                    SizedBox(
                                      width: ScreenUtil().setWidth(4),
                                    ),
                                    Image.asset(
                                      YBDCommonUtil.getCurrencyGreyImg(widget.currency),
                                      width: ScreenUtil().setWidth(18),
                                      height: ScreenUtil().setWidth(18),
                                    ),
                                    Text(
                                      '${getOriginalPrice()}',
                                      style: TextStyle(
                                        fontSize: ScreenUtil().setSp(22),
                                        color: Colors.grey,
                                        // color: Color(0xffEBB01E),
                                        fontWeight: FontWeight.w400,
                                        decoration: TextDecoration.lineThrough,
                                        decorationColor: Colors.grey,
                                      ),
                                    ),
                                  ])
                                : SizedBox()
                          ],
                        ),
                        Spacer(
                          flex: 29,
                        ),
                        Row(
                          children: [
                            Text(
                              ' ${widget.buyStep!.data!.productName}',
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: ScreenUtil().setSp(24), color: Colors.black, fontWeight: FontWeight.w400),
                            ),
                            YBDTPGlobal.wSizedBox(10),
                            YBDLevelStintTag(
                              stintType: widget.buyStep!.data!.conditionType,
                              stintLv: widget.buyStep!.data!.conditionExtends,
                            ),
                          ],
                        ),
                        Spacer(
                          flex: 20,
                        ),
                        Text.rich(TextSpan(
                            style: TextStyle(fontSize: ScreenUtil().setWidth(24), color: Color(0xff717171)),
                            children: [
                              TextSpan(text: "Balance: "),
                              TextSpan(
                                  text: YBDCommonUtil.getBalanceByCurrency(widget.currency),
                                  style: TextStyle(color: Color(0xffEBB01E)))
                            ]))
                      ],
                    ),
                  ),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.clear,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    Spacer(),
                    if (widget.buyStep!.getInfoLayout() == InfoLayout.classic)
                      Padding(
                        padding: EdgeInsets.only(right: ScreenUtil().setWidth(30)),
                        child: Text.rich(TextSpan(
                            style: TextStyle(fontSize: ScreenUtil().setWidth(24), color: Color(0xff717171)),
                            children: [
                              TextSpan(text: "Balance: "),
                              TextSpan(
                                  text: YBDCommonUtil.getBalanceByCurrency(widget.currency),
                                  style: TextStyle(color: Color(0xffEBB01E)))
                            ])),
                      ),
                  ],
                ),
              ],
            ),
          ),
          // Row(
          //   children: [
          //     SizedBox(
          //       width: ScreenUtil().setWidth(20),
          //     ),
          //     Padding(
          //       padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(33)),
          //       child: Container(
          //         width: ScreenUtil().setWidth(308),
          //         height: ScreenUtil().setWidth(130),
          //         alignment: Alignment.center,
          //         decoration: BoxDecoration(
          //             image: DecorationImage(
          //                 image: AssetImage(
          //           "assets/images/bg_buyu.webp",
          //         ))),
          //         child: Row(
          //           mainAxisSize: MainAxisSize.min,
          //           children: [
          //             Image.asset(
          //               "assets/images/u_crown_2x.webp",
          //               width: ScreenUtil().setWidth(40),
          //             ),
          //             SizedBox(
          //               width: ScreenUtil().setWidth(10),
          //             ),
          //             // Text(
          //             //   widget.data.name,
          //             //   style: TextStyle(
          //             //       fontFamily: "sansita",
          //             //       fontSize: ScreenUtil().setWidth(40),
          //             //       color: Colors.white,
          //             //       shadows: [
          //             //         Shadow(
          //             //           offset: Offset(0, ScreenUtil().setWidth(2)),
          //             //           blurRadius: ScreenUtil().setWidth(4),
          //             //           color: Colors.black.withOpacity(0.5),
          //             //         ),
          //             //       ]),
          //             // )
          //           ],
          //         ),
          //       ),
          //     ),
          //     Spacer(),
          //     Column(
          //       children: [
          //         SizedBox(
          //           height: ScreenUtil().setWidth(20),
          //         ),
          //         GestureDetector(
          //           onTap: () {
          //             Navigator.pop(context);
          //           },
          //           child: Icon(
          //             Icons.clear,
          //             color: Colors.black,
          //           ),
          //         ),
          //         SizedBox(
          //           height: ScreenUtil().setWidth(140),
          //         ),
          //       ],
          //     ),
          //     SizedBox(
          //       width: ScreenUtil().setWidth(20),
          //     ),
          //   ],
          // ),
          // Row(
          //   children: [
          //     SizedBox(
          //       width: ScreenUtil().setWidth(34),
          //     ),
          //     Image.asset(
          //       "assets/images/topup/y_top_up_beans@2x.webp",
          //       width: ScreenUtil().setWidth(30),
          //     ),
          //     SizedBox(
          //       width: ScreenUtil().setWidth(4),
          //     ),
          //     Text(
          //       YBDNumericUtil.format(YBDNumericUtil.dynamicToInt(hasDisCount() ? getDiscoutPrice() : getOriginalPrice())),
          //       style: TextStyle(
          //           fontSize: ScreenUtil().setWidth(36), color: Color(0xffEBB01E), fontWeight: FontWeight.bold),
          //     ),
          //     SizedBox(
          //       width: ScreenUtil().setWidth(12),
          //     ),
          //     if (hasDisCount())
          //       Image.asset(
          //         "assets/images/beans_grey.webp",
          //         width: ScreenUtil().setWidth(18),
          //       ),
          //     SizedBox(
          //       width: ScreenUtil().setWidth(2),
          //     ),
          //     if (hasDisCount())
          //       Text(
          //         YBDNumericUtil.format(YBDNumericUtil.dynamicToInt(getOriginalPrice())),
          //         style: TextStyle(
          //             fontSize: ScreenUtil().setWidth(22),
          //             color: Colors.black.withOpacity(0.2),
          //             decoration: TextDecoration.lineThrough,
          //             fontWeight: FontWeight.bold),
          //       ),
          //     Spacer(),
          //     Text.rich(
          //         TextSpan(style: TextStyle(fontSize: ScreenUtil().setWidth(24), color: Color(0xff717171)), children: [
          //       TextSpan(text: "Balance: "),
          //       TextSpan(text: YBDNumericUtil.format(store.state.bean.money), style: TextStyle(color: Color(0xffEBB01E)))
          //     ])),
          //     SizedBox(
          //       width: ScreenUtil().setWidth(30),
          //     ),
          //   ],
          // ),
          SizedBox(
            height: ScreenUtil().setWidth(60),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(getPriceInfo()!.length, (index) => _getItemWidget(index)),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(62),
          ),
          Container(
            alignment: Alignment.center,
            width: ScreenUtil().setWidth(440),
            height: ScreenUtil().setWidth(72),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              gradient: widget.stintBuy ? YBDTPGlobal.greyGradient : YBDTPGlobal.blueGradient,
            ),
            child: TextButton(
                child: Container(
                    alignment: Alignment.center,
                    width: ScreenUtil().setWidth(600),
                    height: ScreenUtil().setWidth(75),
                    child: Text(translate('buy'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(32),
                            color: widget.stintBuy ? Color(0xff717171) : Colors.white,
                            fontWeight: FontWeight.w600))),
                onPressed: buyNow),
          ),
        ],
      ),
    );
  }
  void myBuilddDOuVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDBuySheet oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgettqTttoyelive(YBDBuySheet oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
