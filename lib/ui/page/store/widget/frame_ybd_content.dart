import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/store/store_ybd_service.dart';
import 'frame_ybd_list_item.dart';

/// 商店 Frame
class YBDFrameContent extends StatefulWidget {
  YBDFrameContent();

  @override
  _YBDFrameContentState createState() => _YBDFrameContentState();
}

class _YBDFrameContentState extends BaseRefreshListState<YBDFrameContent> {
  @override
  bool asFragmentPage = true;

  @override
  customItemLayout(List<Widget> children) {
    return GridView.builder(
        shrinkWrap: true,
        itemCount: children.length,
        padding: EdgeInsets.symmetric(horizontal: 24.px as double, vertical: 24.px as double),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10.px as double,
          crossAxisSpacing: 10.px as double,
          childAspectRatio: 0.9,
        ),
        itemBuilder: (context, index) => children[index]);
  }

  @override
  Widget baseBuild(BuildContext context, int index) => YBDFrameListItem(list[index]);

  @override
  refresh() {
    service(YBDStoreService()
      ..type = HttpType.QueryStoreFrame
      ..needLoadMore = true
     );
  }
}
