import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';
import 'package:oyelive_main/ui/widget/svga_ybd_icon.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../buy/buy_ybd_page.dart';
import '../entity/mic_ybd_frame_entity.dart';
import 'package:path/path.dart' as p;

/// 座驾列表 item
class YBDFrameListItem extends StatefulWidget {
  final YBDMicFrameRecord? micFrameRecord;

  YBDFrameListItem(this.micFrameRecord);

  @override
  _YBDFrameListItemItemState createState() => _YBDFrameListItemItemState();
}

class _YBDFrameListItemItemState extends BaseState<YBDFrameListItem> {
  @override
  Widget myBuild(BuildContext context) {
    bool _stint = YBDCommonUtil.stintBuy(
      type: widget.micFrameRecord!.conditionType,
      lv: widget.micFrameRecord!.conditionExtends,
    );
    if (widget.micFrameRecord == null) {
      return Container();
    }

    String url = YBDImageUtil.frame(context, widget.micFrameRecord!.image, "B");
    double itemW = 350;
    return Container(
      width: ScreenUtil().setWidth(itemW),
      decoration: BoxDecoration(
        color: Color(0x26FFFFFF),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
      ),
      child: GestureDetector(
        onTap: () {
          // 弹出底部购买弹框
          showModalBottomSheet(
            context: context,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                topRight: Radius.circular(8.0),
              ),
              side: BorderSide(style: BorderStyle.none),
            ),
            builder: (context) => YBDBuyBottomWindowPage(
              BuyType.frame,
              null,
              widget.micFrameRecord,
              null,
              null,
              currency: widget.micFrameRecord!.currency,
              stintBuy: _stint,
            ),
          );
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.STORE_MIC_FRAMES_LIST_PAGE,
            itemName: 'preview',
            value: '${p.extension(widget.micFrameRecord!.image ?? '')}',
          ));
        },
        child: Column(
          children: [
            Stack(
              children: [
                ColorFiltered(
                  colorFilter: YBDTPGlobal.getGreyFilter(_stint),
                  child: Container(
                    // item 图片
                    width: ScreenUtil().setWidth(itemW),
                    height: ScreenUtil().setWidth(260),
                    color: Colors.transparent,
                    alignment: Alignment.bottomCenter,
                    child: Center(
                      child: _itemImg(url),
                    ),
                  ),
                ),
                Positioned(
                  top: ScreenUtil().setWidth(20),
                  right: ScreenUtil().setWidth(20),
                  child: YBDSvgaIcon(imgUrl: widget.micFrameRecord!.animationFile, white: true, grey: _stint),
                ),
                Positioned(
                  top: 15.px,
                  left: 10.px,
                  child: YBDLevelStintTag(
                      stintType: widget.micFrameRecord?.conditionType,
                      stintLv: widget.micFrameRecord?.conditionExtends,
                      lock: _stint),
                ),
              ],
            ),
            Expanded(child: SizedBox()),
            Container(
              // 图片底部部分
              decoration: BoxDecoration(
                color: Color(0x26FFFFFF),
                // color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(8.0),
                  bottomRight: Radius.circular(8.0),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(height: ScreenUtil().setWidth(15)),
                  Container(
                    height: ScreenUtil().setWidth(25),
                    padding: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
                    // color: Colors.amberAccent,
                    child: Row(
                      children: [
                        Text(
                          // 名称
                          widget.micFrameRecord!.name ?? '',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(22),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Expanded(child: Container()),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setWidth(15)),
                  Row(
                    children: [
                      SizedBox(width: ScreenUtil().setWidth(16)),
                      Image.asset(
                        YBDCommonUtil.getCurrencyImg(widget.micFrameRecord!.currency),
                        width: ScreenUtil().setWidth(20),
                        height: ScreenUtil().setWidth(20),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      Text(
                        // 价格
                        '${widget.micFrameRecord!.price! * 10}',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          color: Colors.amberAccent,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Expanded(child: Container()),
                      Image.asset(
                        'assets/images/icon_clocks.webp',
                        width: ScreenUtil().setWidth(20),
                        height: ScreenUtil().setWidth(20),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      Text(
                        // 有效天数
                        '10 days',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(20)),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setWidth(15)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuilduZO5goyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item图片
  Widget _itemImg(String imgUrl) {
    return YBDNetworkImage(
      width: ScreenUtil().setWidth(200),
      height: ScreenUtil().setWidth(260),
      imageUrl: imgUrl,
      fit: BoxFit.contain,
    );
  }
  void _itemImgguqQJoyelive(String imgUrl) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
