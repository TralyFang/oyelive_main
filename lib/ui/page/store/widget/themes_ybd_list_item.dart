import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:path/path.dart' as p;
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';
import 'package:oyelive_main/ui/widget/svga_ybd_icon.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../entity/theme_ybd_entity.dart';

/// 座驾列表 item
class YBDThemesListItem extends StatefulWidget {
  /// item 在列表中的位置
  final int curIndex;

  /// 列表数据源
  final List<YBDThemeRecord?> themeRecordList;

  YBDThemesListItem(this.curIndex, this.themeRecordList);

  @override
  _YBDThemesListItemItemState createState() => _YBDThemesListItemItemState();
}

class _YBDThemesListItemItemState extends BaseState<YBDThemesListItem> {
  @override
  Widget myBuild(BuildContext context) {
    if (widget.themeRecordList == null) {
      return Container();
    }
    // item 数据
    YBDThemeRecord _themeRecord = widget.themeRecordList[widget.curIndex]!;
    bool _stint = YBDCommonUtil.stintBuy(type: _themeRecord.conditionType, lv: _themeRecord.conditionExtends);

    String url;
    if (_themeRecord.image!.endsWith("gift") ||
        _themeRecord.image!.endsWith("gif") ||
        _themeRecord.image!.endsWith("svga")) {
      url = YBDImageUtil.themeTh(context, _themeRecord.thumbnail, "A");
    } else {
      url = YBDImageUtil.themeTh(context, _themeRecord.thumbnail, "A");
    }
    return Container(
      width: ScreenUtil().setWidth(330),
      height: ScreenUtil().setWidth(535),
      decoration: BoxDecoration(
        color: Color(0x26FFFFFF),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
      ),
      child: GestureDetector(
        onTap: () {
          // 弹出预览弹框
          YBDDialogUtil.showBuyPreviewDialog(context, widget.themeRecordList, widget.curIndex);
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.STORE_THEMES_LIST_PAGE,
            itemName: 'preview',
            value: '${p.extension(_themeRecord.image ?? '')}',
          ));
        },
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              child: Container(
                child: Column(
                  children: [
                    _itemImg(url, _themeRecord.image, _stint),
                    SizedBox(
                      height: ScreenUtil().setWidth(12),
                    ),
                    Container(
                      height: ScreenUtil().setWidth(40),
                      padding: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
                      child: Row(
                        children: [
                          Text(
                            // 名称
                            _themeRecord.name ?? '',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(22),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Expanded(child: Container()),
                        ],
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setWidth(10)),
                    Row(
                      children: [
                        SizedBox(width: ScreenUtil().setWidth(16)),
                        Image.asset(
                          YBDCommonUtil.getCurrencyImg(_themeRecord.currency),
                          width: ScreenUtil().setWidth(20),
                          height: ScreenUtil().setWidth(24),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(10),
                        ),
                        Text(
                          // 价格
                          '${_themeRecord.price! * 10}',
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(20),
                            color: Colors.amberAccent,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Expanded(child: Container()),
                        Image.asset(
                          'assets/images/icon_clocks.webp',
                          width: ScreenUtil().setWidth(20),
                          height: ScreenUtil().setWidth(20),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(10)),
                        Text(
                          // 有效天数
                          '10 days',
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(20),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(20)),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setWidth(10)),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 15.px,
              left: 10.px,
              child: YBDLevelStintTag(
                stintType: _themeRecord.conditionType,
                stintLv: _themeRecord.conditionExtends,
                lock: _stint,
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildomdY1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item图片
  /// [url] 静态图链接
  /// [image] 原图链接
  Widget _itemImg(String url, String? image, bool stint) {
    return Stack(
      children: [
        YBDNetworkImage(
          // 从网络获取房间图像
          imageUrl: url,
          width: ScreenUtil().setWidth(330),
          height: ScreenUtil().setWidth(420),
          fit: BoxFit.cover,
        ),
        Positioned(
          top: ScreenUtil().setWidth(20),
          right: ScreenUtil().setWidth(20),
          child: YBDSvgaIcon(imgUrl: image, white: true, grey: stint),
        ),
        stint
            ? Container(
                width: ScreenUtil().setWidth(330),
                height: ScreenUtil().setWidth(420),
                color: Colors.black.withOpacity(0.5))
            : Container()
      ],
    );
  }
  void _itemImgeWQ2eoyelive(String url, String? image, bool stint) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
