import 'dart:async';

import 'dart:io';
import 'dart:typed_data';

import 'package:alpha_player_plugin/platform_video_gift_view.dart';
// import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:svgaplayer_flutter/dynamic_entity.dart';
import 'package:svgaplayer_flutter/proto/svga.pb.dart';
import 'package:svgaplayer_flutter/svgaplayer_flutter.dart';
import 'package:oyelive_main/common/room_socket/message/common/gift.dart';
import 'package:oyelive_main/common/util/cache_ybd_util.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/util/download_ybd_cache_manager.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_record_extend.dart';
import 'package:oyelive_main/ui/page/splash/splash_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../home/entity/car_ybd_list_entity.dart';

enum AnimationPlayerType { svga, mp4 }

///播放svga页面
class YBDSvgaPlayerPage extends StatefulWidget {
  /// 动画类型  默认svga
  AnimationPlayerType type = AnimationPlayerType.svga;

  /// 动画文件下载地址
  final String? url;

  final String? mp3File;

  /// 座驾信息
  final YBDCarListRecord? carInfo;

  /// 播放状态
  final callBack;

  ///模块 1：房间动画  其它：商店动画   商店的时候通过dialog播放  结束时pop退出，进房动画只是一个普通widget不能pop
  final int mud;

  ///本地动画标识
  final bool isLocal;

  /// 需要替换svga的模型
  final YBDCarListRecordExtend? replaceSvgaEntity;

  /// 需要替换svga的模型数组
  final List<YBDGiftCustomizedData?>? replaceSvgaEntitys;

  YBDSvgaPlayerPage({
    this.type: AnimationPlayerType.svga,
    this.url,
    this.mp3File,
    this.carInfo,
    this.callBack,
    this.mud = 0,
    this.isLocal = false,
    this.replaceSvgaEntity,
    this.replaceSvgaEntitys,
      });

  @override
  _YBDSvgaPlayerPageState createState() => _YBDSvgaPlayerPageState();
}

class _YBDSvgaPlayerPageState extends State<YBDSvgaPlayerPage> with SingleTickerProviderStateMixin {
  /// svga 动画控制器
  SVGAAnimationController? _animationController;

  /// mp4 透明底动画
  GlobalKey<PlatformVideoGiftViewState> platformVideoGiftViewKey = GlobalKey<PlatformVideoGiftViewState>();

  /// 音频播放器
  AudioPlayer? audioPlayer;

  AudioCache? assetsPlayer;

  ///是否已经调用了结束 避免重复调用导致多次pop[主要是iOS视频动画问题]
  bool _isEnd = false;

  @override
  void initState() {
    super.initState();
    logger.v('_enterRoomAnimation initState path:${widget.url} mp3file:${widget.mp3File}');

    // The type is determined by the suffix name
    urlTypePair(widget.url);

    _animationController = SVGAAnimationController(vsync: this);
    audioPlayer = AudioPlayer();
    // AudioPlayer.logEnabled = true;
    audioPlayer!.onPlayerCompletion.listen((event) async {
      if (audioPlayer!.state == PlayerState.COMPLETED) {
//        await _playAudio();
      }
    });
    audioPlayer!.onPlayerStateChanged.listen((event) {
      logger.v('room player state : $event');
    });
    /*
    audioPlayer.onPlayerError.listen((event) {
      logger.v('room player state : $event');
    });

     */
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      _playSvga();
    });
  }
  void initStatefqcfSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 根据后缀判断类型
  urlTypePair(String? url) {
    if (url?.toLowerCase().endsWith(".mp4") ?? false) {
      widget.type = AnimationPlayerType.mp4;
    }
  }

  @override
  void dispose() {
    if (audioPlayer != null) {
      audioPlayer!.pause();
      audioPlayer!.dispose();
      audioPlayer = null;
    }
    if (_animationController != null) {
      _animationController!.stop();
      _animationController!.clear();
      _animationController = null;
    }
    widget.callBack(false);
    logger.v('svga play Complete dispose: mud:${widget.mud}');
    super.dispose();
  }
  void disposexPRtNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _end() {
    if (_isEnd) {
      return;
    }
    _isEnd = true;
    _animationController!.videoItem = null;
    audioPlayer?.stop();
    audioPlayer = null;
    if (widget.mud != 1) {
      Navigator.pop(context);
    }
    widget.callBack?.call(false);
    platformVideoGiftViewKey.currentState?.stop();
    logger.v('svga play Complete end: mud:${widget.mud}');
  }
  void _endQXSTioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    print('_enterRoomAnimation build path:${widget.url} mp3file:${widget.mp3File} type:${widget.type}');
    return GestureDetector(
      onTap: () {
        logger.v("clicked the combo dialog bg");
        if (widget.mud != 1) {
          _end();
        }
      },
      child: Opacity(
        opacity: widget.mud != 1 ? 0.6 : 1,
        child: Container(
          color: Colors.transparent,
          width: ScreenUtil().screenWidth,
          height: ScreenUtil().screenHeight,
          child: _contentView(context),
        ),
      ),
    );
//    );
  }
  void buildSccyuoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///SVGAImage view
  Widget _contentView(BuildContext context) {
    // Avoid refreshing the widget type is not right, you need to get the type again
    urlTypePair(widget.url);
    if (widget.type == AnimationPlayerType.mp4) {
      // 限权判断
     return FutureBuilder<bool>(
        future: YBDSplashUtil.requestStoragePermission(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data!) {
              _playAudio();
              return PlatformVideoGiftView(
                url: widget.url,
                key: platformVideoGiftViewKey,
                width: ScreenUtil().screenWidth,
                height: ScreenUtil().screenHeight,
                finishCallBack: () {
                  _end();
                },
              );
            }else {
              // 没有权限播放直接结束
              _end();
            }
          }
          return Container();
        },
      );
    }

    return Container(
      width: ScreenUtil().screenWidth,
      height: ScreenUtil().screenHeight,
      child: SVGAImage(_animationController!, fit: BoxFit.cover),
//      child: _isLoading ? YBDLoadingCircle() : SVGAImage(_animationController),
    );
  }
  void _contentView2LxKloyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _playSvga() async {
    widget.callBack?.call(true);

    if (widget.type == AnimationPlayerType.mp4) {
      return;
    }
    // 即使是svaga文件，却不带.svga后缀的文件，解析失败，无法播放。
    if (!(widget.url!.toLowerCase().endsWith(".svga"))) {
      logger.e("svga url is not end with .svga: ${widget.url}");
      return;
    }

    if (widget.isLocal) {
      await _loadAssetsSvga(widget.url!);
    } else {
      await _loadLoaclSvga(widget.url);
    }
  }

  /// 替换目标图片，追加到svga
  _appendSVGADynamic(MovieEntity item) async {
    logger.v("svga replace info ${widget.replaceSvgaEntity?.toJson()}, svgas: ${widget.replaceSvgaEntitys?.length}");
    var dynamic = SVGADynamicEntity();
    if (widget.replaceSvgaEntity?.replacedSvgaImageKey != null) {
      var imgKey = widget.replaceSvgaEntity?.replacedSvgaImageKey;
      var imgPath = widget.replaceSvgaEntity?.replacedSvgaImage;
      _replaceSvgaImageDefault(dynamic, imgKey, imgPath);
    }
    if (widget.replaceSvgaEntitys != null) {
      widget.replaceSvgaEntitys!.forEach((element) {
        _replaceSvgaImage(dynamic, element!.imgKey, element.imgPath);
      });
    }
    item.dynamicItem = dynamic;
  }

  /// 替换单个图片到svga对应的imgkey：imgPath, 地址不存在使用当前登录用户头像地址
  _replaceSvgaImageDefault(SVGADynamicEntity dynamic, String? imgKey, String? imgPath) async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
    logger.v("svga replace info avatart: ${userInfo?.headimg}, key: $imgKey, imgPath:$imgPath");
    // If there is no avatar, the default avatar
    var defaultImage = YBDResourcePathUtil.defaultMaleAssets(male: userInfo?.sex == 2);
    String? avatar = "";
    // use imgPath if imgPath is not empty
    if (imgPath?.isNotEmpty ?? false) {
      avatar = imgPath;
    } else if (userInfo?.headimg?.isNotEmpty ?? false) {
      avatar = YBDImageUtil.avatar(context, userInfo!.headimg, userInfo.id, scene: 'C');
    }
    if (avatar?.startsWith('http') ?? false) {
      _replaceSvgaImage(dynamic, imgKey, avatar);
    } else {
      var img = AssetImage(defaultImage);
      var uiImage = await YBDImageUtil.loadUIImageByProvider(img);
      dynamic.setImage(uiImage, imgKey!);
    }
  }

  /// 替换单个图片到svga对应的imgkey：imgPath
  _replaceSvgaImage(SVGADynamicEntity dynamic, String? imgKey, String? imgPath) async {
    logger.v("svga replace key: $imgKey, imgPath:$imgPath");
    if (imgPath?.startsWith('http') ?? false) {
      var cacheImg = await YBDCacheUtil.instance!.getBitmapCache(imgPath);
      if (cacheImg == null) {
        try {
          Uint8List bytes = await YBDDownloadCacheManager.instance.downloadUint8ListURL(imgPath!);
          var img = await decodeImageFromList(bytes);
          cacheImg = await YBDImageUtil.clipCircularImage(img);
          YBDCacheUtil.instance!.bitmapCache2Location(imgPath, cacheImg);
        } catch (e) {
          logger.v("svga replace error: $e}");
        }
      }
      if (cacheImg != null) {
        dynamic.setImage(cacheImg, imgKey!);
      }
    }
  }

  /// 加载本地 svga 文件
  _loadLoaclSvga(String? path) async {
    if (null == path) {
      logger.w('path is null');
      return;
    }

    File svgFile = File(path);
    logger
        .v('_loadLoaclSvga : $path, data.length ${svgFile.readAsBytesSync().length}, exists: ${svgFile.existsSync()}');
    if (svgFile.existsSync()) {
      Uint8List bytes = svgFile.readAsBytesSync();

      // 播放动画
      SVGAParser.shared.decodeFromBuffer(bytes).then((videoItem) {
        /* setState(() {
          _playAudio();
        });*/
        try {
          _playAudio();
          _appendSVGADynamic(videoItem);
          _animationController!.videoItem = videoItem;
          _animationController!.forward().whenComplete(() {
            _end();
          });
        } catch (e) {
          logger.v("_loadLoaclSvga e:${e.toString()}");
        }
      });
    } else {
      logger.v('svga file not exists : $svgFile');
    }
  }

  /// 播放 Assets里面的svga 文件
  _loadAssetsSvga(String path) async {
    logger.v('_loadAssetsSvga : $path');
    dynamic videoItem = await SVGAParser.shared.decodeFromAssets(path);
    try {
      _playAudio();
      _appendSVGADynamic(videoItem);
      _animationController!.videoItem = videoItem;
      _animationController!.forward().whenComplete(() {
        _end();
        logger.v('svga play Complete mud:${widget.mud}');
      });
    } catch (e) {
      logger.v("_loadLoaclSvga e:${e.toString()}");
    }
  }

  /// 播放 mp3
  _playAudio() async {
    if (widget.mp3File == null) {
      return;
    }

    logger.v('room play path: ${widget.mp3File} isLocal:${widget.isLocal}');

    ///本地assets文件
    if (widget.isLocal) {
      if (assetsPlayer == null) {
        assetsPlayer = AudioCache();
      }

      // 需要在entrance_local.json中配置"mp3": "animation/audio.mp3",
      try {
        final result = await assetsPlayer!.play(widget.mp3File!);
        logger.v('room play _playAudio result: $result');
      } catch (e) {
        // 捕获异常：FileSystemException: Cannot create file, path = '/data/user/0/com.oyetalk.tv/cache/' (OS Error: Is a directory, errno = 21)
        logger.v('play mp3 error: $e');
      }
    } else {
      ///本地磁盘文件
      File audioFile = File(widget.mp3File!);
      logger.v('room play path: ${widget.mp3File}');
      if (await audioFile.exists()) {
        final result = (await audioPlayer?.play(audioFile.path, isLocal: true))!;
      } else {
        logger.v('no audio file : ${audioFile.path}');
      }
    }
  }
}
