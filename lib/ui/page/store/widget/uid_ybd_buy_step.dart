import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/page/buy/buy_ybd_step.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/buy_ybd_vip_result_entity.dart';
import '../../../../module/entity/uid_ybd_list_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../../redux/user_ybd_redux.dart';
import '../entity/price_ybd_info_entity.dart';
import 'package:redux/redux.dart';

class YBDUidBuyStep extends BuyStep {
  List<YBDPriceInfoEntity?>? priceList;

  YBDUidListRecord? uidListRecord;

  bool isRenew;

  YBDUidBuyStep(this.priceList, this.uidListRecord, this.isRenew) {
    this.priceList = priceList;
    this.uidListRecord = uidListRecord;
    makeData();
  }

  @override
  YBDBuySheetData? makeData() {
    List<YBDDisplayItemData> items = [];

    priceList!.forEach((element) {
      YBDDisplayItemData data = YBDDisplayItemData();
      int unitCount = int.parse(element!.unit!);

      data.unitName = YBDCommonUtil.getUnitName(element.unitType, element.unit);

      data.price = (unitCount * uidListRecord!.price!).toInt().toString();
      if (element.price != null) {
        data.discountPrice = element.price;
      } else {
        data.discountPrice = (unitCount * uidListRecord!.price! * double.parse(element.discount!)).toInt().toString();
      }
      data.itemName = "$unitCount${data.unitName}";
      items.add(data);
    });

    data = YBDBuySheetData()
      ..productName = this.uidListRecord!.name
      ..conditionType = this.uidListRecord!.conditionType
      ..conditionExtends = this.uidListRecord!.conditionExtends
      ..items = items
      ..priceInfo = priceList
      ..showImage = SizedBox(
        width: ScreenUtil().setWidth(220),
        height: ScreenUtil().setWidth(180),
        child: Center(
          child: Container(
            width: ScreenUtil().setWidth(186),
            height: ScreenUtil().setWidth(78),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
              "assets/images/bg_buyu.webp",
            ))),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  "assets/images/u_crown_2x.webp",
                  width: ScreenUtil().setWidth(24),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(4),
                ),
                Text(
                  uidListRecord!.name!,
                  style: TextStyle(
                      fontFamily: "sansita",
                      fontSize: ScreenUtil().setWidth(24),
                      color: Colors.white,
                      shadows: [
                        Shadow(
                          offset: Offset(0, ScreenUtil().setWidth(2)),
                          blurRadius: ScreenUtil().setWidth(4),
                          color: Colors.black.withOpacity(0.5),
                        ),
                      ]),
                )
              ],
            ),
          ),
        ),
      );
    return data;
  }

  @override
  Future<bool> onBuy(BuildContext context, int index, {String? currency = Const.CURRENCY_BEAN}) async {
    YBDBuyVipResultEntity? resultEntity =
        await ApiHelper.buyUid(context, uidListRecord!.id, priceList![index]!.unit, isRenew: isRenew);

    if (resultEntity == null || resultEntity.returnCode != Const.HTTP_SUCCESS) {
      YBDToastUtil.toast(resultEntity!.returnMsg);
      return false;
    } else {
      YBDUserInfo? info = await YBDUserUtil.userInfo();
      switch (currency) {
        case Const.CURRENCY_BEAN:
          info!.money = resultEntity.record!.money;
          break;
        case Const.CURRENCY_GEM:
          info!.gems = resultEntity.record!.money;
          break;
        case Const.CURRENCY_GOLD:
          info!.golds = resultEntity.record!.money;
          break;
        default:
          info!.money = resultEntity.record!.money;
          break;
      }
      await YBDSPUtil.save(Const.SP_USER_INFO, info);
      final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
      store.dispatch(YBDUpdateUserAction(info));
      return true;
    }
  }
  void onBuymX5nyoyelive(BuildContext context, int index, {String? currency = Const.CURRENCY_BEAN})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget? makeLeftWidget(bool hasDisCount, discoutPrice, originalPrice) {
    // TODO: implement makeLeftWidget
    return null;
    return Padding(
      padding: EdgeInsets.only(left: ScreenUtil().setWidth(14)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: ScreenUtil().setWidth(33),
            ),
            child: Container(
              width: ScreenUtil().setWidth(308),
              height: ScreenUtil().setWidth(130),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                "assets/images/bg_buyu.webp",
              ))),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset(
                    "assets/images/u_crown_2x.webp",
                    width: ScreenUtil().setWidth(40),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(10),
                  ),
                  Text(
                    uidListRecord!.name!,
                    style: TextStyle(
                        fontFamily: "sansita",
                        fontSize: ScreenUtil().setWidth(40),
                        color: Colors.white,
                        shadows: [
                          Shadow(
                            offset: Offset(0, ScreenUtil().setWidth(2)),
                            blurRadius: ScreenUtil().setWidth(4),
                            color: Colors.black.withOpacity(0.5),
                          ),
                        ]),
                  )
                ],
              ),
            ),
          ),
          Spacer(),
          Row(
            children: [
              Image.asset(
                YBDCommonUtil.getCurrencyImg(uidListRecord!.currency),
                width: ScreenUtil().setWidth(30),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(4),
              ),
              Text(
                YBDNumericUtil.format(YBDNumericUtil.dynamicToInt(hasDisCount ? discoutPrice : originalPrice))!,
                style: TextStyle(
                    fontSize: ScreenUtil().setWidth(36), color: Color(0xffEBB01E), fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(12),
              ),
              if (hasDisCount)
                Image.asset(
                  YBDCommonUtil.getCurrencyGreyImg(uidListRecord!.currency),
                  width: ScreenUtil().setWidth(18),
                ),
              SizedBox(
                width: ScreenUtil().setWidth(2),
              ),
              if (hasDisCount)
                Text(
                  YBDNumericUtil.format(YBDNumericUtil.dynamicToInt(originalPrice))!,
                  style: TextStyle(
                      fontSize: ScreenUtil().setWidth(22),
                      color: Colors.black.withOpacity(0.2),
                      decoration: TextDecoration.lineThrough,
                      fontWeight: FontWeight.bold),
                ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  InfoLayout getInfoLayout() {
    return InfoLayout.tight;
  }
}
