import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/widget/bs_ybd_buy.dart';
import 'package:oyelive_main/ui/page/store/widget/bubble_ybd_buy_step.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../module/entity/bubble_ybd_entity.dart';
import '../../../widget/svga_ybd_icon.dart';
import '../entity/price_ybd_info_entity.dart';

class YBDBubbleItem extends StatefulWidget {
  YBDBubbleRecord? data;
  Function? onSuccessBuy;

  YBDBubbleItem(this.data, {this.onSuccessBuy});

  @override
  YBDBubbleItemState createState() => new YBDBubbleItemState();
}

class YBDBubbleItemState extends BaseState<YBDBubbleItem> {
  @override
  Widget myBuild(BuildContext context) {
    bool _stint = YBDCommonUtil.stintBuy(type: widget.data!.conditionType, lv: widget.data!.conditionExtends);
    String url = YBDImageUtil.frame(context, widget.data!.image, "C");
    double itemW = 350;

    return Container(
      width: ScreenUtil().setWidth(itemW),
      decoration: BoxDecoration(
        color: Color(0x26FFFFFF),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
      ),
      child: GestureDetector(
        onTap: () async {
          // 弹出底部购买弹框
          List<YBDPriceInfoEntity?>? bubbleDiscout = await YBDSPUtil.getDiscount(Const.SP_KEY_DISCOUNT_BUBBLE);

          YBDBubbleBuyStep bubbleBuyStep = new YBDBubbleBuyStep(bubbleDiscout, widget.data);
          showModalBottomSheet(
            context: context,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                topRight: Radius.circular(8.0),
              ),
              side: BorderSide(style: BorderStyle.none),
            ),
            builder: (context) => YBDBuySheet(
              buyStep: bubbleBuyStep,
              onSuccessBuy: () {
                widget.onSuccessBuy?.call();
              },
              currency: widget.data!.currency,
              stintBuy: _stint,
            ),
          );

          // YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          //   YBDEventName.CLICK_EVENT,
          //   location: YBDLocationName.STORE_MIC_FRAMES_LIST_PAGE,
          //   itemName: 'preview',
          //   value: '{p.extension(widget.micFrameRecord.image ?? '')}',
          // ));
        },
        child: Column(
          children: [
            Stack(
              children: [
                ColorFiltered(
                  colorFilter: YBDTPGlobal.getGreyFilter(_stint),
                  child: Container(
                    // item 图片
                    width: ScreenUtil().setWidth(itemW),
                    height: ScreenUtil().setWidth(260),
                    color: Colors.transparent,
                    alignment: Alignment.bottomCenter,
                    child: Stack(
                      children: [
                        Center(
                          child: _itemImg(url),
                        ),
                        Center(
                          child: Text(
                            "Hello~",
                            style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: ScreenUtil().setWidth(20),
                  right: ScreenUtil().setWidth(20),
                  child: YBDSvgaIcon(imgUrl: widget.data!.animation, white: true, grey: _stint),
                ),
                Positioned(
                  top: 12.px,
                  left: ScreenUtil().setWidth(10),
                  child: YBDLevelStintTag(
                    stintType: widget.data?.conditionType,
                    stintLv: widget.data?.conditionExtends,
                    lock: _stint,
                  ),
                ),
              ],
            ),
            Expanded(child: SizedBox()),
            Container(
              // 图片底部部分
              decoration: BoxDecoration(
                color: Color(0x26FFFFFF),
                // color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(8.0),
                  bottomRight: Radius.circular(8.0),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(height: ScreenUtil().setWidth(15)),
                  Container(
                    // height: ScreenUtil().setWidth(25),
                    padding: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
                    // color: Colors.amberAccent,
                    child: Row(
                      children: [
                        Text(
                          // 名称
                          widget.data!.name ?? '',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(22),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Expanded(child: Container()),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setWidth(15)),
                  Row(
                    children: [
                      SizedBox(width: ScreenUtil().setWidth(16)),
                      Image.asset(
                        YBDCommonUtil.getCurrencyImg(widget.data!.currency),
                        width: ScreenUtil().setWidth(20),
                        height: ScreenUtil().setWidth(20),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      Text(
                        // 价格
                        '${widget.data!.price! * 10}',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          color: Colors.amberAccent,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Expanded(child: Container()),
                      Image.asset(
                        'assets/images/icon_clocks.webp',
                        width: ScreenUtil().setWidth(20),
                        height: ScreenUtil().setWidth(20),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      Text(
                        // 有效天数
                        '10 days',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(20),
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(20)),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setWidth(15)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuild0leQfoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item图片
  Widget _itemImg(String imgUrl) {
    return YBDNetworkImage(
      width: ScreenUtil().setWidth(220),
      // height: ScreenUtil().setWidth(260),
      imageUrl: imgUrl,
      fit: BoxFit.fitWidth,
    );
  }
  void _itemImgAmTjIoyelive(String imgUrl) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  void disposeZtJU9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDBubbleItem oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesE1Rptoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
