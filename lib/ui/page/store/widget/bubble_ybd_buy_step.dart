import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/ui/page/buy/buy_ybd_step.dart';
import 'package:oyelive_main/ui/page/store/entity/price_ybd_info_entity.dart';
import 'package:redux/redux.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/bubble_ybd_entity.dart';
import '../../../../module/entity/buy_ybd_vip_result_entity.dart';
import '../../../../module/entity/result_ybd_bean.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../../redux/user_ybd_redux.dart';

class YBDBubbleBuyStep extends BuyStep {
  List<YBDPriceInfoEntity?>? priceList;
  YBDBubbleRecord? bubbleRecord;

  YBDBubbleBuyStep(List<YBDPriceInfoEntity?>? priceList, YBDBubbleRecord? bubbleRecord) {
    this.priceList = priceList;
    this.bubbleRecord = bubbleRecord;
    makeData();
  }

  @override
  YBDBuySheetData? makeData() {
    // TODO: implement makeData
    List<YBDDisplayItemData> items = [];

    priceList!.forEach((element) {
      YBDDisplayItemData data = YBDDisplayItemData();
      int unitCount = int.parse(element!.unit!);

      data.unitName = YBDCommonUtil.getUnitName(element.unitType, element.unit);

      data.price = (unitCount * bubbleRecord!.price!).toInt().toString();
      if (element.price != null) {
        data.discountPrice = element.price;
      } else {
        data.discountPrice = (unitCount * bubbleRecord!.price! * double.parse(element.discount!)).toInt().toString();
      }
      data.itemName = "$unitCount${data.unitName}";
      items.add(data);
    });

    data = YBDBuySheetData()
      ..productName = this.bubbleRecord!.name
      ..conditionType = this.bubbleRecord!.conditionType
      ..conditionExtends = this.bubbleRecord!.conditionExtends
      ..items = items
      ..priceInfo = priceList
      ..showImage = Container(
        margin: EdgeInsets.only(top: ScreenUtil().setWidth(60)),
        child: Container(
          width: ScreenUtil().setWidth(220),
          // decoration: BoxDecoration(
          //     color: Color(0xfff6f6f6), borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
          child: Stack(
            children: [
              Center(
                child: YBDNetworkImage(
                  imageUrl: YBDImageUtil.frame(Get.context, bubbleRecord!.image, "C"),
                  width: ScreenUtil().setWidth(200),
                  fit: BoxFit.fitWidth,
                ),
              ),
              Center(
                child: Text(
                  "Hello~",
                  style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(24)),
                ),
              ),
            ],
          ),
        ),
      );
    return data;
  }

  @override
  Future<bool> onBuy(BuildContext context, int index, {String? currency = Const.CURRENCY_BEAN}) async {
    YBDBuyVipResultEntity? resultEntity = await ApiHelper.buyBubble(
      context,
      bubbleRecord!.id,
      priceList![index]!.unit,
    );

    if (resultEntity == null || resultEntity.returnCode != Const.HTTP_SUCCESS) {
      YBDToastUtil.toast(resultEntity!.returnMsg);
      return false;
    } else {
      YBDUserInfo? info = await YBDUserUtil.userInfo();
      switch (currency) {
        case Const.CURRENCY_BEAN:
          info!.money = resultEntity.record!.money;
          break;
        case Const.CURRENCY_GEM:
          info!.gems = resultEntity.record!.money;
          break;
        case Const.CURRENCY_GOLD:
          info!.golds = resultEntity.record!.money;
          break;
        default:
          info!.money = resultEntity.record!.money;
          break;
      }
      await YBDSPUtil.save(Const.SP_USER_INFO, info);
      final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
      store.dispatch(YBDUpdateUserAction(info));
      return true;
    }
  }
  void onBuyrgsTdoyelive(BuildContext context, int index, {String? currency = Const.CURRENCY_BEAN})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget? makeLeftWidget(bool hasDisCount, discoutPrice, originalPrice) {
    // TODO: implement makeLeftWidget
    return null;
  }

  @override
  InfoLayout getInfoLayout() {
    // TODO: implement getInfoLayout
    return InfoLayout.tight;
  }
}
