import 'dart:async';

import 'dart:developer';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:svgaplayer_flutter/player.dart';
import 'package:oyelive_main/module/entity/card_ybd_frame_store_entity.dart';
import 'package:oyelive_main/ui/page/buy/buy_ybd_step.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/buy_ybd_vip_result_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../../redux/user_ybd_redux.dart';
import '../../room/widget/room_ybd_svga_theme.dart';
import '../entity/price_ybd_info_entity.dart';
import 'package:redux/redux.dart';

class YBDCardFrameBuyStep extends BuyStep {
  List<YBDPriceInfoEntity?>? priceList;
  YBDCardFrameStoreRecord? cardFrameStoreRecord;

  YBDCardFrameBuyStep(List<YBDPriceInfoEntity?>? priceList, YBDCardFrameStoreRecord? cardFrameStoreRecord) {
    this.priceList = priceList;
    this.cardFrameStoreRecord = cardFrameStoreRecord;
    makeData();
  }
  @override
  InfoLayout getInfoLayout() {
    // TODO: implement getInfoLayout
    return InfoLayout.tight;
  }

  @override
  YBDBuySheetData? makeData() {
    // TODO: implement makeData
    List<YBDDisplayItemData> items = [];

    priceList!.forEach((element) {
      YBDDisplayItemData data = YBDDisplayItemData();
      int unitCount = int.parse(element!.unit!);

      data.unitName = YBDCommonUtil.getUnitName(element.unitType, element.unit);

      data.price = (unitCount * cardFrameStoreRecord!.price!).toInt().toString();
      if (element.price != null) {
        data.discountPrice = element.price;
      } else {
        data.discountPrice =
            (unitCount * cardFrameStoreRecord!.price! * double.parse(element.discount!)).toInt().toString();
      }
      data.itemName = "$unitCount${data.unitName}";
      items.add(data);
    });

    String imageUrl = cardFrameStoreRecord!.animation!.split(",").last;
    String fullUrl = YBDImageUtil.getFullSize(Get.context, imageUrl);

    data = YBDBuySheetData()
      ..productName = this.cardFrameStoreRecord!.name
      ..conditionType = this.cardFrameStoreRecord!.conditionType
      ..conditionExtends = this.cardFrameStoreRecord!.conditionExtends
      ..items = items
      ..bigLayout = true
      ..priceInfo = priceList
      ..showImage = Container(
        // margin: EdgeInsets.only(top: ScreenUtil().setWidth(20)),
        child: Transform.scale(
          scale: 1.2,
          child: Container(
            margin: EdgeInsets.only(top: ScreenUtil().setWidth(40)),
            // width: ScreenUtil().setWidth(220),
            // decoration: BoxDecoration(
            //     color: Color(0xfff6f6f6), borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Center(
                  child: Image.asset(
                    'assets/images/cf_bg.webp',
                    width: ScreenUtil().setWidth(235),
                  ),
                ),
                Center(
                  child: fullUrl.endsWith("svga")
                      ? YBDRoomSvgaTheme(
                          fullUrl,
                          showLoading: true,
                          width: ScreenUtil().setWidth(280),
                        )
                      : YBDNetworkImage(
                          imageUrl: fullUrl,
                          width: ScreenUtil().setWidth(280),
                        ),
                ),
              ],
            ),
          ),
        ),
      );
    return data;
  }

  @override
  Widget? makeLeftWidget(bool hasDisCount, discountPrice, originalPrice) {
    // TODO: implement makeLeftWidget
    return null;
  }

  @override
  Future<bool> onBuy(BuildContext context, int index, {String? currency = Const.CURRENCY_BEAN}) async {
    // TODO: implement onBuy
    YBDBuyVipResultEntity? resultEntity = await ApiHelper.buyCardFrame(
      context,
      cardFrameStoreRecord!.id,
      priceList![index]!.unit,
    );

    if (resultEntity == null || resultEntity.returnCode != Const.HTTP_SUCCESS) {
      YBDToastUtil.toast(resultEntity!.returnMsg);
      return false;
    } else {
      YBDUserInfo? info = await YBDUserUtil.userInfo();
      switch (currency) {
        case Const.CURRENCY_BEAN:
          info!.money = resultEntity.record!.money;
          break;
        case Const.CURRENCY_GEM:
          info!.gems = resultEntity.record!.money;
          break;
        case Const.CURRENCY_GOLD:
          info!.golds = resultEntity.record!.money;
          break;
        default:
          info!.money = resultEntity.record!.money;
          break;
      }
      await YBDSPUtil.save(Const.SP_USER_INFO, info);
      final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
      store.dispatch(YBDUpdateUserAction(info));
      return true;
    }
  }
  void onBuyGRnV4oyelive(BuildContext context, int index, {String? currency = Const.CURRENCY_BEAN})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
