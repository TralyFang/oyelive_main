import 'dart:async';

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/baggage/widget/base_ybd_baggage_content.dart';
import 'package:oyelive_main/ui/page/store/store_ybd_service.dart';
import 'package:oyelive_main/ui/page/store/widget/item_ybd_card_frame.dart';

import '../../../../base/base_ybd_refresh_list_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../entity/discount_ybd_entity.dart';

class YBDCardFrameContent extends StatefulWidget with BaseBaggageContent {
  //背包需要userId
  int? userId;

  YBDCardFrameContent({this.userId});

  @override
  YBDCardFrameContentState createState() => new YBDCardFrameContentState();
}

class YBDCardFrameContentState extends BaseRefreshListState<YBDCardFrameContent> {
  bool isBag = false;
  @override
  Widget baseBuild(BuildContext context, int index) {
    // TODO: implement baseBuild
    return YBDCardFrameItem(
      list[index],
      isBag: isBag,
      onSuccess: () {
        if (isBag) refresh();
      },
    );
  }
  void baseBuildoE99hoyelive(BuildContext context, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void refresh() async {
    YBDDiscountEntity? discountEntity = await ApiHelper.queryDiscountList(context, "CARD_FRAME");
    if (discountEntity != null && discountEntity.returnCode == Const.HTTP_SUCCESS) {
      YBDSPUtil.save(Const.SP_KEY_DISCOUNT_CARD_FRAME, json.encode(discountEntity.record!.toJson()));
    }

    int? userId = (widget as YBDCardFrameContent).userId;
    isBag = userId != null;

    if (isBag)
      service(YBDStoreService()
        ..type = HttpType.QueryCardFrameBag
        ..model = (YBDBagQuery()..userId = userId));
    else
      service(YBDStoreService()..type = HttpType.QueryCardFrameStore);
  }
  void refreshBUzGPoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget emptyView() {
    // TODO: implement emptyView
    int? userId = (widget as YBDCardFrameContent).userId;
    int? myId = YBDUserUtil.getUserIdSync;
    if (isBag && (userId == myId)) return baggageEmptyView(index: widget.storeTabIndex());
    return super.emptyView();
  }
  void emptyViewn0iEzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool asFragmentPage = true;

  @override
  customItemLayout(List<Widget> children) {
    return GridView.count(
      padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
      crossAxisCount: 2,
      crossAxisSpacing: ScreenUtil().setWidth(20),
      mainAxisSpacing: ScreenUtil().setWidth(20),
      childAspectRatio: 1,
      children: children,
    );
  }
}
