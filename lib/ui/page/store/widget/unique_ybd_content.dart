import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/store/store_ybd_service.dart';
import 'package:oyelive_main/ui/page/store/widget/item_ybd_unique_id.dart';

/// 商店 unique
class YBDUniqueContent extends StatefulWidget {
  YBDUniqueContent();

  @override
  _YBDUniqueContentState createState() => _YBDUniqueContentState();
}

class _YBDUniqueContentState extends BaseRefreshListState<YBDUniqueContent> {
  @override
  bool asFragmentPage = true;

  @override
  customItemLayout(List<Widget> children) {
    return GridView.count(
      padding: EdgeInsets.all(20.px as double),
      crossAxisCount: 2,
      crossAxisSpacing: 20.px as double,
      mainAxisSpacing: 20.px as double,
      childAspectRatio: 1,
      children: children,
    );
  }

  @override
  Widget baseBuild(BuildContext context, int index) {
    return YBDUniqueIdItem(list[index], onSuccessBuy: refresh);
  }
  void baseBuildfa6r8oyelive(BuildContext context, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  refresh() {
    service(YBDStoreService()
      ..type = HttpType.QueryStoreUId
      ..needLoadMore = true);
  }
}
