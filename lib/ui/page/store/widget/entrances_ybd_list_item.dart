import 'dart:async';

import 'dart:io';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/local_ybd_animation_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/widget/local_ybd_animation_entity.dart';
import '../../../../main.dart';
import '../../buy/buy_ybd_page.dart';
import '../../home/entity/car_ybd_list_entity.dart';
import '../bloc/entrances_ybd_block.dart';
import '../entity/carbean.dart';
import '../handle/download_ybd_event.dart';

/// 座驾列表 item
class YBDEntrancesListItem extends StatefulWidget {
  /// item 数据
  final YBDCarListRecord? carRecord;

  /// 动画数据
  final YBDCarBean carBean;

  Dialog? dialog;

  bool _isCan = true;

  /// 正在播放动画
  bool _isPlaying = false;

  List<YBDDownloadLoadEvent> downloadLoadEventList = [];

  YBDDownloadLoadEvent? curDownloadLoadEvent;

  YBDEntrancesListItem(this.carRecord, this.carBean);

  @override
  _YBDEntrancesListItemState createState() => _YBDEntrancesListItemState();
}

class _YBDEntrancesListItemState extends BaseState<YBDEntrancesListItem> {
  @override
  Widget myBuild(BuildContext context) {
    bool _stint = YBDCommonUtil.stintBuy(type: widget.carRecord!.conditionType, lv: widget.carRecord!.conditionExtends);
    return Container(
      width: ScreenUtil().screenWidth,
      height: ScreenUtil().setWidth(350),
      child: Stack(
        children: <Widget>[
          Positioned(
            // item 背景
            top: ScreenUtil().setWidth(0),
            left: ScreenUtil().setWidth(15),
            right: ScreenUtil().setWidth(15),
            child: _itemBg(),
          ),
          Positioned(
            // 汽车名
            top: ScreenUtil().setWidth(70),
            left: ScreenUtil().setWidth(50),
            child: Text(
              translate('${widget.carRecord!.name}'),
              style: TextStyle(
                fontSize: ScreenUtil().setSp(32),
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Positioned(
            // 豆子，时间 行
            top: ScreenUtil().setWidth(140),
            left: ScreenUtil().setWidth(50),
            child: _carPriceRow(),
          ),
          if (!_stint)
            Positioned(
              // 购买按钮
              top: ScreenUtil().setWidth(60),
              right: ScreenUtil().setWidth(50),
              child: _buyCarButton(_stint),
            ),
          (widget.carBean.state != 0)
              ? Positioned(
                  bottom: ScreenUtil().setWidth(80),
                  left: ScreenUtil().setWidth(50),
                  child: _percentDownloadProgress(),
                )
              : Container(),
          Positioned(
            // 播放按钮
            bottom: ScreenUtil().setWidth(0),
            left: ScreenUtil().setWidth(110),
            child: _playButton(widget.carRecord),
          ),
          (widget.carBean.state == 3)
              ? Positioned(
                  // 圆形下载进度条
                  bottom: ScreenUtil().setWidth(25),
                  left: ScreenUtil().setWidth(122),
                  child: _circularDownloadProgress(),
                )
              : Container(),
          Positioned(
            // 座驾图片
            bottom: ScreenUtil().setWidth(1),
            right: ScreenUtil().setWidth(30),
            child: _carImage(),
          ),
          // 等级限制图标
          Positioned(
            top: ScreenUtil().setWidth(17),
            left: ScreenUtil().setWidth(33),
            child: YBDLevelStintTag(
              stintType: widget.carRecord?.conditionType,
              stintLv: widget.carRecord?.conditionExtends,
            ),
          ),
          // 等级限制白锁图标
          if (_stint)
            Positioned(
                top: ScreenUtil().setWidth(27),
                right: ScreenUtil().setWidth(44),
                child: Image.asset('assets/images/icon_key@2x.webp', width: 20.px)),
        ],
      ),
    );
  }
  void myBuildzMoS1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item 背景
  Widget _itemBg() {
    return Card(
      color: Colors.black87,
      // 设置 card 的阴影
      elevation: 10.0,
      // 设置shape，这里设置成了R角
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
      // 对 Widget 截取的行为，比如这里 Clip.antiAlias 指抗锯齿
      clipBehavior: Clip.antiAlias,
      semanticContainer: false,
      child: Container(
        width: ScreenUtil().setWidth(720),
        height: ScreenUtil().setWidth(275),
        alignment: Alignment.center,
        child: Image.asset(
          'assets/images/profile/store_item_design_bg_hover.png',
          width: ScreenUtil().setWidth(720),
          height: ScreenUtil().setWidth(275),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
  void _itemBg5ujftoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 豆子，时间 所在的行
  Widget _carPriceRow() {
    return Row(
      children: [
        Image.asset(
          YBDCommonUtil.getCurrencyImg(widget.carRecord!.currency),
          width: ScreenUtil().setWidth(30),
          height: ScreenUtil().setWidth(35),
        ),
        SizedBox(width: ScreenUtil().setWidth(10)),
        Text(
          translate('${widget.carRecord!.price}'),
          style: TextStyle(
            fontSize: ScreenUtil().setSp(24),
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
        SizedBox(width: ScreenUtil().setWidth(10)),
        Image.asset(
          'assets/images/period_ico_store.png',
          width: ScreenUtil().setWidth(30),
          height: ScreenUtil().setWidth(35),
        ),
        SizedBox(width: ScreenUtil().setWidth(10)),
        Text(
          // 有效时间
          '1 month',
          style: TextStyle(
            fontSize: ScreenUtil().setSp(24),
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
      ],
    );
  }
  void _carPriceRowagSZ3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 座驾购买按钮和响应事件
  Widget _buyCarButton(bool stint) {
    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.0),
              topRight: Radius.circular(8.0),
            ),
            side: BorderSide(style: BorderStyle.none),
          ),
          builder: (context) => YBDBuyBottomWindowPage(
            BuyType.car,
            widget.carRecord,
            null,
            null,
            null,
            currency: widget.carRecord!.currency,
            stintBuy: stint,
          ),
        );
      },
      child: Image.asset(
        'assets/images/profile/store_item_design_buy_btn.png',
        width: ScreenUtil().setWidth(170),
        height: ScreenUtil().setWidth(85),
      ),
    );
  }
  void _buyCarButtonNY4OToyelive(bool stint) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 百分比动画下载进度条
  // TODO: jackie - 暂时没用到
  Widget _percentDownloadProgress() {
    return GestureDetector(
      onTap: () {
        logger.v('preview_btn------');
      },
      child: Text(
        (widget.carBean.state == 1)
            ? '100%'
            : (widget.carBean.state == 2)
                ? translate("entrances_download_failed")
                : '${((widget.carBean.progress! * 100).toInt())}%',
        style: TextStyle(
          fontSize: ScreenUtil().setSp(24),
          color: Colors.cyan,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
  void _percentDownloadProgressLWydnoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 播放按钮和响应事件
  Widget _playButton(YBDCarListRecord? carInfo) {
    return GestureDetector(
      onTap: () async {
        if (!widget._isCan) {
          return;
        }
        logger.v('preview_btn------download _isCan ${widget._isCan}');
        widget._isCan = false;
        // 500 毫秒内 不能多次点击
        Future.delayed(Duration(milliseconds: 1000), () {
          widget._isCan = true;
        });
        // 下载并播放座驾动画
//        YBDDialogUtil.showPlayCarDialog(context, carInfo);
        ///通过Id获取本地动画，没有则返回空，使用原来的逻辑，有的话直接播放
        YBDLocalAnimationItems? localAnimationItems = await YBDLocalAnimationUtil.getLocalAnimationById(widget.carBean.id);
        if (localAnimationItems != null) {
          eventBus.fire(YBDDownloadLoadEvent(localAnimationItems.svga, 0,
              url_mp3: localAnimationItems.mp3, isLocal: true, replaceDto: widget.carRecord!.replaceDto));
        } else {
          // 队列下载并播放座驾动画
          context.read<YBDEntrancesListBloc>().download(widget.carBean.id);
        }
      },
      child: Image.asset(
        'assets/images/profile/store_item_design_preview_btn.png',
        width: ScreenUtil().setWidth(130),
        height: ScreenUtil().setWidth(130),
      ),
    );
  }
  void _playButtonDSP7Goyelive(YBDCarListRecord? carInfo) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /* /// 播放按钮和响应事件
  Widget _playButtonList(YBDCarListRecord carInfo) {
    return BlocBuilder<YBDEntrancesListBloc, YBDEntrancesBlocState>(builder: (context, state) {
      child:
      return GestureDetector(
        onTap: () {
          logger.v('preview_btn------download');
          context.read<YBDEntrancesListBloc>().download(widget.carBean.id);
        },
        child: Image.asset(
          'assets/images/profile/store_item_design_preview_btn.png',
          width: ScreenUtil().setWidth(130),
          height: ScreenUtil().setWidth(130),
        ),
      );
    });
  }*/

  /// 播放按钮波浪
  // TODO: jackie - 暂时没用到
  Widget _circularDownloadProgress() {
    return GestureDetector(
      onTap: () {
        logger.v('preview_btn------');
      },
      child: Container(
        width: ScreenUtil().setWidth(93),
        height: ScreenUtil().setWidth(93),
        child: LiquidCircularProgressIndicator(
          value: widget.carBean.progress!,
          //当前进度 0-1
          valueColor: AlwaysStoppedAnimation(Colors.blue[200]!),
          // 进度值的颜色.
          backgroundColor: Colors.black.withOpacity(0.3),

          direction: Axis.vertical, // 进度方向 (Axis.vertical = 从下到上, Axis.horizontal = 从左到右). 默认：Axis.vertical
        ),
      ),
    );
  }
  void _circularDownloadProgressWES32oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 座驾图片
  Widget _carImage() {
    return GestureDetector(
      onTap: () {
        logger.v('preview_btn------');
        if (Platform.isAndroid) {}
      },
      child: YBDNetworkImage(
//                  imageUrl: YBDImageUtil.car(context, widget.carRecord.img, 'D'),
        width: ScreenUtil().setWidth(360),
        height: ScreenUtil().setWidth(200),
        imageUrl: YBDImageUtil.car(context, widget.carRecord!.img, 'D'),
//                  imageUrl: 'http://s3-us-west-2.amazonaws.com/thankyotest/image/2/1503986217345_480.png',
        fit: BoxFit.scaleDown,
      ),
    );
  }
}
