import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/entity/uid_ybd_list_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/store/entity/price_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';
import 'package:oyelive_main/ui/page/store/widget/uid_ybd_buy_step.dart';

import 'bs_ybd_buy.dart';

class YBDUniqueIdItem extends StatelessWidget {
  YBDUidListRecord? data;
  Function? onSuccessBuy;
  YBDUniqueIdItem(this.data, {this.onSuccessBuy});

  @override
  Widget build(BuildContext context) {
    bool _stint = YBDCommonUtil.stintBuy(type: data!.conditionType, lv: data!.conditionExtends);
    return GestureDetector(
      onTap: () async {
        // 弹出底部购买弹框

        List<YBDPriceInfoEntity?>? uidDiscout = await YBDSPUtil.getUidDiscount();

        YBDUidBuyStep uidBuyStep = new YBDUidBuyStep(uidDiscout, data, false);
        showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.0),
              topRight: Radius.circular(8.0),
            ),
            side: BorderSide(style: BorderStyle.none),
          ),
          builder: (context) => YBDBuySheet(
            buyStep: uidBuyStep,
            onSuccessBuy: () {
              onSuccessBuy?.call();
            },
            currency: data!.currency,
            stintBuy: _stint,
          ),
        );
      },
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
        child: Column(
          children: [
            Container(
              width: ScreenUtil().setWidth(330),
              height: ScreenUtil().setWidth(237),
              color: Colors.white.withOpacity(0.1),
              child: Stack(
                children: [
                  Center(
                    child: ColorFiltered(
                      colorFilter: YBDTPGlobal.getGreyFilter(_stint),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Image.asset(
                            "assets/images/u_crown_2x.webp",
                            width: ScreenUtil().setWidth(40),
                          ),
                          SizedBox(
                            width: ScreenUtil().setWidth(6),
                          ),
                          Text(
                            data!.name!,
                            style: TextStyle(
                                fontFamily: "sansita",
                                fontSize: ScreenUtil().setWidth(38),
                                color: Colors.white,
                                shadows: [
                                  Shadow(
                                    offset: Offset(0, ScreenUtil().setWidth(2)),
                                    blurRadius: ScreenUtil().setWidth(4),
                                    color: Colors.black.withOpacity(0.5),
                                  ),
                                ]),
                          )
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 15.px,
                    left: 10.px,
                    child: YBDLevelStintTag(
                      stintType: data?.conditionType,
                      stintLv: data?.conditionExtends,
                      lock: _stint,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: ScreenUtil().setWidth(93),
              width: ScreenUtil().setWidth(330),
              color: Colors.white.withOpacity(0.2),
              child: Row(
                children: [
                  SizedBox(
                    width: ScreenUtil().setWidth(14),
                  ),
                  Image.asset(
                    YBDCommonUtil.getCurrencyImg(data!.currency),
                    width: ScreenUtil().setWidth(22),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(8),
                  ),
                  Text(
                    YBDNumericUtil.format(data!.price)!,
                    style: TextStyle(
                      fontSize: ScreenUtil().setWidth(24),
                      color: Color(0xffFFC450),
                    ),
                  ),
                  Spacer(),
                  Image.asset(
                    'assets/images/icon_clocks.webp',
                    width: ScreenUtil().setWidth(20),
                    height: ScreenUtil().setWidth(20),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(10)),
                  Text(
                    // 有效天数
                    '30 days',
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(20),
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(18)),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  void buildAecNXoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
