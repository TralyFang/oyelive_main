import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/store/store_ybd_service.dart';
import 'discount_ybd_list_item.dart';

/// 商店 Discount
class YBDDiscountContent extends StatefulWidget {
  YBDDiscountContent();

  @override
  _YBDDiscountContentState createState() => _YBDDiscountContentState();
}

class _YBDDiscountContentState extends BaseRefreshListState<YBDDiscountContent> {
  @override
  bool asFragmentPage = true;

  @override
  customItemLayout(List<Widget> children) {
    return GridView.builder(
        shrinkWrap: true,
        itemCount: children.length,
        padding: EdgeInsets.symmetric(horizontal: 24.px as double, vertical: 24.px as double),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10.px as double,
          crossAxisSpacing: 10.px as double,
          childAspectRatio: 1,
        ),
        itemBuilder: (context, index) => children[index]);
  }

  @override
  Widget baseBuild(BuildContext context, int index) => YBDDiscountListItem(
        index,
        list[index],
        onBuycallBack: () {
          // 获取页面数据
          logger.v('onBuycallBack queryDiscount data');
          refresh();
        },
      );

  @override
  refresh() {
    service(YBDStoreService()..type = HttpType.QueryStoreDiscount);
  }
}
