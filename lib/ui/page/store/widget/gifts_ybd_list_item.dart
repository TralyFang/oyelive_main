import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/store/widget/level_ybd_stint_buy_tag.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../buy/buy_ybd_gifts_page.dart';
import '../../home/entity/gift_ybd_list_entity.dart';

/// 礼物列表 item
class YBDGiftsListItem extends StatefulWidget {
  /// item 在列表中的位置
  final int curIndex;

  /// 列表数据源
  final YBDGiftListRecordGift? giftListRecordGift;

  YBDGiftsListItem(this.curIndex, this.giftListRecordGift);

  @override
  _YBDGiftsListItemState createState() => _YBDGiftsListItemState();
}

class _YBDGiftsListItemState extends BaseState<YBDGiftsListItem> {
  @override
  void initState() {
    super.initState();
  }
  void initStateBIoiGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    if (widget.giftListRecordGift == null) {
      return Container();
    }

    String url;
    url = YBDImageUtil.gift(context, widget.giftListRecordGift?.img, 'B');
    // print("-----------------url :$url");
    bool _stint = YBDCommonUtil.stintBuy(
      type: widget.giftListRecordGift!.conditionType,
      lv: widget.giftListRecordGift!.conditionExtends,
    );
    return Stack(
      children: [
        Container(
          width: ScreenUtil().setWidth(350),
          decoration: BoxDecoration(
            color: Color(0x26FFFFFF),
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
          child: GestureDetector(
            onTap: () {
              // 弹出底部购买弹框
              showModalBottomSheet(
                context: context,
                isScrollControlled: true,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8.0),
                    topRight: Radius.circular(8.0),
                  ),
                  side: BorderSide(style: BorderStyle.none),
                ),
                builder: (context) => AnimatedPadding(
                  padding: EdgeInsets.only(left: 0, right: 0, bottom: 0, top: 0),
                  duration: const Duration(milliseconds: 2500),
                  child: YBDBuyGiftsBottomWindowPage(
                    BuyType.gifts,
                    giftListRecordGift: widget.giftListRecordGift,
                    stintBuy: _stint,
                  ),
                ),
              );
            },
            child: Column(
              children: [
                ColorFiltered(
                  colorFilter: YBDTPGlobal.getGreyFilter(_stint),
                  child: Container(
                    // item 图片
                    height: ScreenUtil().setWidth(237),
                    color: Colors.transparent,
                    alignment: Alignment.bottomCenter,
                    child: Center(
                      child: YBDNetworkImage(
                        height: ScreenUtil().setWidth(200),
                        width: ScreenUtil().setWidth(140),
                        imageUrl: url,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),

                // SizedBox(
                //   height: ScreenUtil().setWidth(12),
                // ),
                Container(
                  // 图片底部部分
                  decoration: BoxDecoration(
                    color: Color(0x26FFFFFF),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(8.0),
                      bottomRight: Radius.circular(8.0),
                    ),
                  ),
                  child: Column(
                    children: [
                      SizedBox(height: ScreenUtil().setWidth(15)),
                      Container(
                        height: ScreenUtil().setWidth(25),
                        padding: EdgeInsets.only(left: ScreenUtil().setWidth(20)),
                        // color: Colors.amberAccent,
                        child: Row(
                          children: [
                            Text(
                              // 名称
                              widget.giftListRecordGift!.name ?? '',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(22),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                              ),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Expanded(child: Container()),
                          ],
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(15)),
                      Row(
                        children: [
                          SizedBox(width: ScreenUtil().setWidth(16)),
                          Image.asset(
                            'assets/images/topup/y_top_up_beans@2x.webp',
                            width: ScreenUtil().setWidth(20),
                            height: ScreenUtil().setWidth(20),
                          ),
                          SizedBox(width: ScreenUtil().setWidth(10)),
                          Text(
                            // 价格
                            YBDCommonUtil.formatNum(widget.giftListRecordGift?.price),
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(20),
                              color: Colors.amberAccent,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: ScreenUtil().setWidth(13)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          top: 15.px,
          left: ScreenUtil().setWidth(15),
          child: YBDLevelStintTag(
            stintType: widget.giftListRecordGift?.conditionType,
            stintLv: widget.giftListRecordGift?.conditionExtends,
            lock: _stint,
          ),
        ),
      ],
    );
  }
  void myBuildEIV4soyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
