import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/store/entity/theme_ybd_entity.dart';
import 'package:oyelive_main/ui/page/store/store_ybd_service.dart';
import 'themes_ybd_list_item.dart';

/// 商店 Theme
class YBDThemesContent extends StatefulWidget {
  YBDThemesContent();

  @override
  _YBDThemesContentState createState() => _YBDThemesContentState();
}

class _YBDThemesContentState extends BaseRefreshListState<YBDThemesContent> {
  @override
  bool asFragmentPage = true;

  @override
  customItemLayout(List<Widget> children) {
    return GridView.builder(
        shrinkWrap: true,
        itemCount: children.length,
        padding: EdgeInsets.symmetric(horizontal: 24.px as double, vertical: 24.px as double),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10.px as double,
          crossAxisSpacing: 10.px as double,
          childAspectRatio: 0.63,
        ),
        itemBuilder: (context, index) => children[index]);
  }

  @override
  Widget baseBuild(BuildContext context, int index) => YBDThemesListItem(index, list as List<YBDThemeRecord?>);

  @override
  refresh() {
    service(YBDStoreService()
      ..type = HttpType.QueryStoreTheme
      ..needLoadMore = true);
  }
}
