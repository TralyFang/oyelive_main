import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/store/store_ybd_service.dart';
import 'gifts_ybd_list_item.dart';

/// 商店 Gifts
class YBDGiftsContent extends StatefulWidget {
  YBDGiftsContent();

  @override
  _YBDGiftsContentState createState() => _YBDGiftsContentState();
}

class _YBDGiftsContentState extends BaseRefreshListState<YBDGiftsContent> {
  @override
  bool asFragmentPage = true;

  @override
  customItemLayout(List<Widget> children) {
    return GridView.builder(
        shrinkWrap: true,
        itemCount: children.length,
        padding: EdgeInsets.symmetric(horizontal: 24.px as double, vertical: 24.px as double),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10.px as double,
          crossAxisSpacing: 10.px as double,
          childAspectRatio: 1,
        ),
        itemBuilder: (context, index) => children[index]);
  }

  @override
  Widget baseBuild(BuildContext context, int index) => YBDGiftsListItem(index, list[index]);

  @override
  refresh() {
    service(YBDStoreService()..type = HttpType.QueryStoreGift);
  }
}
