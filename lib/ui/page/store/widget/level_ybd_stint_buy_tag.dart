import 'dart:async';

/*
 * @Author: William
 * @Date: 2022-05-05 18:19:12
 * @LastEditTime: 2022-07-15 15:36:31
 * @LastEditors: William
 * @Description: Support the purchase of permissions for user level and room level, that is how many levels can be purchased
 * @FilePath: /oyetalk-flutter/lib/ui/page/store/widget/level_stint_buy_tag.dart
 */
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/int_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/img_ybd_resource_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/ui/widget/level_ybd_tag.dart';

/// 限制购买的类型: [不限制，用户等级, 主播等级,用户等级和主播等级， VIP等级, 徽章等级]
enum StintType { none, level, room, level7Room, vip, badge }
const int NO_STINT = 1;

/// 等级权限购买标签
class YBDLevelStintTag extends StatelessWidget {
  /// 限制购买的类型
  final int? stintType;

  /// 限制购买的等级
  final String? stintLv;

  /// 是否显示限制购买的白锁🔒
  final bool lock;

  /// 是否显示限制购买的蓝锁🔒
  final bool blueLock;

  /// 缩放比例
  final double scale;

  const YBDLevelStintTag({
    Key? key,
    this.stintType = NO_STINT,
    this.stintLv = '0',
    this.lock = false,
    this.blueLock = false,
    this.scale = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (stintType == NO_STINT || stintType == null || stintLv == null) return SizedBox(width: 0);
    return Transform.scale(
      scale: scale,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          ColorFiltered(colorFilter: YBDTPGlobal.getGreyFilter(lock), child: _tag()),
          YBDTPGlobal.wSizedBox(10),
          lock
              ? Padding(
                  padding: EdgeInsets.only(bottom: 4.px as double),
                  child: Image.asset('assets/images/icon_key@2x.webp', width: 20.px),
                )
              : SizedBox(width: 0),
          blueLock ? Image.asset('assets/images/icon_blue_lock.webp', width: 20.px) : SizedBox(width: 0),
        ],
      ),
    );
  }
  void buildfziKUoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _tag() {
    if (stintType.stintType() == StintType.level) {
      return YBDLevelTag(YBDNumericUtil.stringToInt(stintLv));
    }
    return Stack(
      children: [
        Image.asset(_getBgImg(stintType.stintType()), width: 80.px, height: 36.px),
        Positioned(
          top: 10.px,
          left: 30.px,
          child: Text(
            _getStintText(stintType.stintType(), stintLv),
            style: TextStyle(color: Colors.white, fontSize: 13.sp),
          ),
        ),
      ],
    );
  }
  void _tag2tOrtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String _getStintText(StintType type, String? lv) {
    // YBDUserInfo u = YBDUserUtil.getLoggedUser(Get.context);
    switch (type) {
      case StintType.level:
        return 'LV$lv';
      case StintType.room:
        return 'RLv$lv';
      case StintType.vip:
        return 'VIP$lv';
      case StintType.badge:
        return 'BGE$lv';
      default:
        return 'Lv$lv';
    }
  }
  void _getStintTextaIs42oyelive(StintType type, String? lv) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String _getBgImg(StintType type) {
    switch (type) {
      case StintType.level:
        return _getLevelBg(YBDNumericUtil.stringToInt(stintLv));
      case StintType.room:
        //todo-fake data need to replace
        return _getLevelBg(YBDNumericUtil.stringToInt(stintLv));
      case StintType.vip:
        return 'assets/images/bg_buyu.webp';
      case StintType.badge:
        return 'assets/images/btn_background.webp';
      default:
        return 'assets/images/explore_tab_bg_img.webp';
    }
  }
  void _getBgImga95F7oyelive(StintType type) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String _getLevelBg(int lv) {
    if (lv >= 10 && lv <= 100) {
      String lvString = (lv ~/ 10).toString();
      return (YBDImg.level + lvString).lv;
    }
    return (YBDImg.level + "0").lv;
  }
}
