import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/store/store_ybd_service.dart';
import 'package:oyelive_main/ui/page/store/widget/item_ybd_bubble.dart';

class YBDBubbleContent extends StatefulWidget {
  @override
  YBDBubbleContentState createState() => new YBDBubbleContentState();
}

class YBDBubbleContentState extends BaseRefreshListState<YBDBubbleContent> {
  @override
  bool asFragmentPage = true;

  @override
  customItemLayout(List<Widget> children) {
    return GridView.builder(
        shrinkWrap: true,
        itemCount: children.length,
        padding: EdgeInsets.symmetric(horizontal: 24.px as double, vertical: 24.px as double),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10.px as double,
          crossAxisSpacing: 10.px as double,
          childAspectRatio: 0.9,
        ),
        itemBuilder: (context, index) => children[index]);
  }

  @override
  Widget baseBuild(BuildContext context, int index) {
    return YBDBubbleItem(list[index], onSuccessBuy: refresh);
  }
  void baseBuildvPgZYoyelive(BuildContext context, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  refresh() {
    service(YBDStoreService()..type = HttpType.QueryStoreBubble);
  }
}
