import 'dart:async';

import '../../../../generated/json/base/json_convert_content.dart';

class YBDThemeEntity with JsonConvert<YBDThemeEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDThemeRecord?>? record;
  String? recordSum;
}

class YBDThemeRecord with JsonConvert<YBDThemeRecord> {
  int? id;
  String? name;
  int? status;
  int? label;
  int? index;
  int? price;
  String? thumbnail;
  String? image;
  int? display;
  String? attribute;
  int? createTime;
  int? updateTime;
  bool? modify;
  String? currency;
  int? conditionType;
  String? conditionExtends;
}
