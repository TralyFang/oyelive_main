import 'dart:async';

import '../../../../generated/json/base/json_convert_content.dart';

class YBDDiscountEntity with JsonConvert<YBDDiscountEntity> {
  String? returnCode;
  String? returnMsg;
  YBDDiscountRecord? record;
}

class YBDDiscountRecord with JsonConvert<YBDDiscountRecord> {
  int? unitType;
  dynamic priceInfo;
}
/*"priceInfo":{
"-1":[
{
"unit":"1",
"discount":"1.0"
},
{
"unit":"3",
"discount":"1.0"
},
{
"unit":"6",
"discount":"0.8"
},
{
"unit":"12",
"discount":"0.8"
}
]
}*/

/*class DiscountRecordPriceInfo with JsonConvert<DiscountRecordPriceInfo> {
  List<YBDDiscountRecordPriceInfo_1> -1;//
}*/

class YBDDiscountRecordPriceInfo_1 with JsonConvert<YBDDiscountRecordPriceInfo_1> {
  String? unit;
  String? discount;
}
