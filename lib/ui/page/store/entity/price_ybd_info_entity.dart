import 'dart:async';

import '../../../../generated/json/base/json_convert_content.dart';

class YBDPriceInfoEntity with JsonConvert<YBDPriceInfoEntity> {
	String? unit;
	String? discount;
	String? price;
	int? unitType;
}
