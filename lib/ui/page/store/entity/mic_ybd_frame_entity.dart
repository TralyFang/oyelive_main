import 'dart:async';

import '../../../../generated/json/base/json_convert_content.dart';

class YBDMicFrameEntity with JsonConvert<YBDMicFrameEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDMicFrameRecord?>? record;
  String? recordSum;
}

class YBDMicFrameRecord with JsonConvert<YBDMicFrameRecord> {
  int? id;
  String? name;
  int? status;
  int? label;
  int? index;
  int? price;
  String? image;
  String? animationFile;
  int? display;
  String? attribute;
  int? createTime;
  int? updateTime;
  bool? modify;
  String? currency;
  int? conditionType;
  String? conditionExtends;
}
