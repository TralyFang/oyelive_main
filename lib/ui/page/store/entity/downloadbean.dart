import 'dart:async';

enum DownLoadStatus { normal, loading, pause, cancel, success, failed }
enum DownLoadType { svg_frame, svg_emoji,svg_entrances,svg_room_bg,gift}

class YBDDownLoadBean {
  String? fileUrl;
  String? savePath;
  String? fileName;
  String? taskId;
  DownLoadStatus? state;
  DownLoadType? type;
  int? fromUser;

  YBDDownLoadBean(
    this.fileUrl, {
    this.savePath,
    this.fileName,
    this.state,
    this.taskId,
    this.type,
    this.fromUser,
  });
}
