import 'dart:async';

import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_record_extend.dart';

class YBDDownloadLoadEvent {
  String? url_svga;
  String? url_mp3;
  int result; //-1 下载失败  0下载成功
  bool isLocal; //是否本地动画 默认不是
  YBDCarListRecordExtend? replaceDto; // 记录需要替换的动画参数，可能为空

  YBDDownloadLoadEvent(this.url_svga, this.result, {this.url_mp3, this.isLocal = false, this.replaceDto});
}
