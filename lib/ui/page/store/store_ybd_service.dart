import 'dart:convert';

import 'package:get/get.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/bubble_ybd_entity.dart';
import 'package:oyelive_main/module/entity/card_ybd_frame_store_entity.dart';
import 'package:oyelive_main/module/entity/uid_ybd_list_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/discount_ybd_list_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/gift_ybd_list_entity.dart';
import 'package:oyelive_main/ui/page/store/entity/discount_ybd_entity.dart';
import 'package:oyelive_main/ui/page/store/entity/mic_ybd_frame_entity.dart';
import 'package:oyelive_main/ui/page/store/entity/theme_ybd_entity.dart';

import '../../../base/base_ybd_refresh_list_state.dart';
import '../../../common/abstract/base_ybd_refresh_service.dart';
import '../../../common/constant/const.dart';
import '../../../module/entity/card_ybd_frame_bag_entity.dart';

class YBDStoreService extends BaseRefreshService {
  @override
  void refresh(
      {YBDBaseModel? model,
      int page = 1,
      int pageSize = Const.PAGE_SIZE,
      Function(List? list, bool isSuccess)? block}) async {
    int loadStart = page > 0 ? (page - 1) * pageSize : page;
    switch (this.type) {
      case HttpType.QueryCardFrameStore:
        // 无分页
        YBDCardFrameStoreEntity? entity = await ApiHelper.queryCardFrameStore(Get.context);
        if (entity == null || entity.returnCode != Const.HTTP_SUCCESS) {
          // 加载数据失败
          block!(null, false);
        } else {
          block!(entity.record, true);
        }
        break;
      case HttpType.QueryCardFrameBag:
        YBDCardFrameBagEntity? entity = await ApiHelper.queryCardFrameBag(Get.context, (this.model as YBDBagQuery).userId);
        if (entity == null || entity.returnCode != Const.HTTP_SUCCESS) {
          // 加载数据失败
          block!(null, false);
        } else {
          List? result = [];
          try {
            result = entity.record?.first?.itemList;
          } catch (e) {
            print(e);
          }
          block!(result, true);
        }
        break;

      case HttpType.QueryStoreUId:
        YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
        if (userInfo == null) block!(null, false);
        List<YBDUidListRecord?>? _dataList;
        // 获取折扣信息
        YBDDiscountEntity? discountEntity = await ApiHelper.queryDiscountList(Get.context, "UNIQUEID");
        if (discountEntity != null && discountEntity.returnCode == Const.HTTP_SUCCESS) {
          YBDSPUtil.save(Const.SP_KEY_DISCOUNT_UID, json.encode(discountEntity.record!.toJson()));
        }
        // 无分页
        YBDUidListEntity? uidListEntity = await ApiHelper.queryUidList(Get.context);
        if (uidListEntity == null || uidListEntity.returnCode != Const.HTTP_SUCCESS) {
          block!(null, false);
        } else {
          _dataList = uidListEntity.record!..retainWhere((element) => element!.stock! > 0);
          block!(_dataList, true);
        }
        break;

      case HttpType.QueryStoreBubble:
        // 列表数据源
        List<YBDBubbleRecord?>? _data = [];
        ApiHelper.queryDiscountList(Get.context, 'BUBBLE').then((discountEntity) async {
          if (discountEntity == null) {
            // 刷新数据失败
            logger.v('queryBubbleDiscount failed');
          } else {
            logger.v('queryBubbleDiscount SUCCESS');
            String bubbleJson = jsonEncode(discountEntity.record);
            YBDSPUtil.save(Const.SP_KEY_DISCOUNT_BUBBLE, bubbleJson);
          }
        });
        // 无分页
        ApiHelper.queryBubbleList(Get.context).then((bubbleEntity) {
          // 刷新数据失败
          if (bubbleEntity == null) {
            logger.v('queryBubble failed');
            block!(null, false);
          } else {
            logger.v('queryBubble SUCCESS');
            _data = bubbleEntity.record;

            block!(_data ?? [], true);
          }
        });
        break;

      case HttpType.QueryStoreFrame:
        // 列表数据源
        List<YBDMicFrameRecord?>? _data = [];
        ApiHelper.queryDiscountList(Get.context, 'FRAME').then((discountEntity) async {
          if (discountEntity == null) {
            // 刷新数据失败
            logger.v('queryFrameDiscount failed');
          } else {
            logger.v('queryFrameDiscount SUCCESS');
            String frameJson = jsonEncode(discountEntity.record);
            YBDSPUtil.save(Const.SP_KEY_DISCOUNT_MIC_FRAME, frameJson);
          }
        });
        ApiHelper.queryFrameList(Get.context, start: loadStart).then((micFrameEntity) {
          // 刷新数据失败
          if (micFrameEntity == null) {
            logger.v('queryFrame failed');
            block!(null, false);
          } else {
            logger.v('queryFrame SUCCESS');
            _data = micFrameEntity.record;
            block!(_data ?? [], true);
          }
        });
        break;

      case HttpType.QueryStoreTheme:
        // 列表数据源
        List<YBDThemeRecord?>? _data = [];
        ApiHelper.queryDiscountList(Get.context, 'THEME').then((discountEntity) async {
          if (discountEntity == null) {
            // 刷新数据失败
            logger.v('queryThemeDiscount failed');
          } else {
            logger.v('queryThemeDiscount SUCCESS');
            YBDSPUtil.save(Const.SP_KEY_DISCOUNT_THEME, jsonEncode(discountEntity.record));
          }
        });
        ApiHelper.queryThemeList(Get.context, start: loadStart).then((themeEntity) {
          if (themeEntity == null) {
            // 刷新数据失败
            logger.v('queryTheme failed');
            block!(null, false);
          } else {
            logger.v('queryTheme SUCCESS');
            _data = themeEntity.record;
            block!(_data ?? [], true);
          }
        });
        break;

      case HttpType.QueryStoreGift:
        // 列表数据源
        List<YBDGiftListRecordGift?> _data = [];
        List<YBDGiftListRecordGift?>? _dataPopular = [];
        List<YBDGiftListRecordGift?>? _dataEvent = [];
        List<YBDGiftListRecordGift?>? _dataLuxury = [];
        // 无分页
        ApiHelper.giftList(Get.context).then((giftListEntity) async {
          if (giftListEntity == null) {
            // 刷新数据失败
            logger.v('queryGift failed');
            block!(null, false);
          } else {
            logger.v('queryGift SUCCESS');
            try {
              if (giftListEntity != null) {
                for (int i = 0; i < (giftListEntity.record?.length ?? 0); i++) {
                  YBDGiftListRecord? giftListRecord = giftListEntity.record![i];
                  logger.v('giftListRecord?.name:${giftListRecord?.name}');
                  if (giftListRecord?.name == 'Popular') {
                    _dataPopular = giftListRecord!.gift;
                  } else if (giftListRecord?.name == 'Event') {
                    _dataEvent = giftListRecord!.gift;
                  } else if (giftListRecord?.name == 'Luxury') {
                    _dataLuxury = giftListRecord!.gift;
                  }
                }
                _data.addAll(_dataEvent!);
                _data.addAll(_dataPopular!);
                _data.addAll(_dataLuxury!);
                block!(_data, true);
              }
            } catch (e) {
              logger.e('_fetchGifts error: $e');
            }
          }
        });
        break;

      case HttpType.QueryStoreDiscount:
        List<YBDDiscountListRecord?>? _data = [];
        ApiHelper.discountList(Get.context).then((discountListEntity) async {
          if (discountListEntity == null) {
            // 刷新数据失败
            logger.v('queryDiscount failed');
            block!(null, false);
          } else {
            logger.v('queryDiscount SUCCESS');
            try {
              if (discountListEntity != null) {
                _data = discountListEntity.record;
                block!(_data ?? [], true);
              }
            } catch (e) {
              logger.e('_fetchGifts error: $e');
            }
          }
        });
        break;
    }
  }
}

class YBDBagQuery extends YBDBaseModel {
  int? userId;
}
