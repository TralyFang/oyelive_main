import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/base/base_ybd_tabbar.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/util/log_ybd_util.dart';
import 'bloc/entrances_ybd_block.dart';

class YBDStorePage extends StatefulWidget {
  final int? initIndex;

  const YBDStorePage({Key? key, this.initIndex}) : super(key: key);

  @override
  YBDStorePageState createState() => YBDStorePageState();
}

class YBDStorePageState extends BaseState<YBDStorePage> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }
  void initStateubyQzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    logger.v('dispose  YBDDownloadUtil.unbindBackgroundIsolate');
  }
  void disposekH5wVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<YBDEntrancesListBloc>(create: (context) {
            logger.v('create entrance list bloc');
            YBDEntrancesListBloc bloc = YBDEntrancesListBloc(context);
            // 加载座驾列表数据
            bloc.add(EntrancesListEvent.Init);
            return bloc;
          }),
        ],
        child: YBDTabBar(
          initIndex: widget.initIndex ?? 0,
          title: translate('store'),
          tabArr: YBDTabBarModel.store(),
          rightItem: Padding(
            padding: EdgeInsets.all(ScreenUtil().setWidth(24)),
            child: Image.asset(
              "assets/images/shortcut_store.webp",
              width: ScreenUtil().setWidth(48),
            ),
          ),
          rightItemCallBack: () {
            YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.baggage + "/");
          },
          switchPageCallBack: (index) {
            print('scroll to $index page');
          },
        ));
  }
  void myBuildV3Glyoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
