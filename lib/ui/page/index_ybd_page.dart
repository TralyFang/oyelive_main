import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:advertising_id/advertising_id.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/constant/short_ybd_route.dart';
import 'package:oyelive_main/common/event/game_ybd_hall_bg_event.dart';
import 'package:oyelive_main/common/event/refresh_ybd_status_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/common/util/purchase_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_file.dart';
import 'package:oyelive_main/ui/page/activty/widget/gif_ybd_image.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/util/tp_ybd_cache.dart';
import 'package:oyelive_main/ui/page/room/pk/pk_ybd_result_dialog.dart';
// import 'package:oyelive_main/ui/page/status/widget/status_ybd_page_bg.dart';
import 'package:oyelive_main/ui/widget/colored_ybd_safe_area.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import 'package:package_info/package_info.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:vibration/vibration.dart';

import '../../base/base_ybd_state.dart';
import '../../common/analytics/analytics_ybd_util.dart';
import '../../common/db_ybd_config.dart';
import '../../common/event/status_ybd_change_bus_event.dart';
import '../../common/handler/back_ybd_action_handler.dart';
import '../../common/room_socket/room_ybd_socket_config.dart';
import '../../common/room_socket/room_ybd_socket_util.dart';
import '../../common/service/iap_service/iap_ybd_service.dart';
import '../../common/service/task_service/task_ybd_service.dart';
import '../../common/util/common_ybd_util.dart';
import '../../common/util/config_ybd_util.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/notification_ybd_util.dart';
import '../../common/util/purchase_ybd_util.dart';
import '../../common/util/remote_ybd_config_service.dart';
import '../../common/util/sp_ybd_util.dart';
import '../../common/util/version_update/check_ybd_update_util.dart';
import '../../common/web_socket/socket_ybd_util.dart';
import '../../main.dart';
import '../../module/agency/agency_ybd_api_helper.dart';
import '../../module/entity/room_ybd_info_entity.dart';
import '../../module/inbox/db/model_ybd_conversation.dart';
import '../../module/inbox/message_ybd_helper.dart';
import '../../module/status/entity/status_ybd_entity.dart';
import '../../module/status/status_ybd_util.dart';
import '../../module/user/util/user_ybd_util.dart';
import '../widget/colored_ybd_safe_area.dart';
import 'activty/activity_ybd_handler.dart';
import 'game_room/widget/game_ybd_resource_util.dart';
import 'game_room/widget/game_ybd_room_dialog_guide.dart';
import 'guide/popular_ybd_guide.dart';
import 'home/home_ybd_page.dart';
import 'inbox/inbox_ybd_page.dart';
import 'profile/my_profile/go_ybd_tab_event.dart';
import 'profile/my_profile/my_ybd_profile_page.dart';
import 'recharge/google/google_ybd_purchase_handler.dart';
import 'room/pk/pk_ybd_rtm_helper.dart';
import 'status/status_ybd_player.dart';
// import 'status/widget/status_ybd_page_bg.dart';

@JsonSerializable()
class YBDIndexPage extends StatefulWidget {
  final int defaultIndex;

  YBDIndexPage({this.defaultIndex = 0});

  @override
  YBDIndexPageState createState() => YBDIndexPageState();
}

OverlayEntry? entry1, entry;

class YBDIndexPageState extends BaseState<YBDIndexPage>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  /// 动态列表索引
  static const int FeatureTabIndex = 1;

  List<YBDRoomInfo>? inviteRooms;

  bool? showTpGoBg = false;

  /// 当前选中的标签索引
  int _selectingIndex = 0;

  late List<YBDGifController> gifControllers;
  SelectBloc? bloc;

  // 第一次点击 tab 图标时显示默认图标
  List<bool> showBackgrounds = List.generate(4, (_) => true);

  OverlayEntry? overlayEnt;
  int inboxUnreadCount = 0;

  /// 选中 tab 时显示的动图
  late List<String> gifs;

  /// 未选中的 tab 图标
  late List<String> defaults;

  /// 选中的 tab 图标
  late List<String> _selectedTabIcon;

  /// 开播图标超出底部栏的高度
  late double _overFlowHeight;

  /// 双击退出 app 的事件处理器
  YBDBackActionHandler _backActionHandler = YBDBackActionHandler();

  /// 首页活动弹框处理器
  YBDActivityHandler? _activityHandler;

  /// 启动 app 时检查谷歌支付漏单
  late YBDGooglePurchaseHandler _googlePurchaseHandler;

  /// 是否签约主播
  bool isOfficialTalent = false;

  /// 动态信息，切换到动态标签时传给[YBDStatusPageBg]的初始动态信息
  YBDStatusInfo? _statusInfo;
  late StreamSubscription<YBDStatusChangeBusEvent> _statusInfoSubscription;

  /// 异步查询房间标签
  /// 在 init() 方法里或刷新列表时调用，得到结果后赋值给实例变量
  Future<void> _asyncRequestOfficialTalent() async {
    YBDSPUtil.getUserInfo().then((userInfo) async {
      if (YBDStatusUtil.isUserLevelEnable(
          userInfo, await ConfigUtil.loadConfigInfo(context))) {
        YBDLogUtil.d('no request, user level is enable');
      } else {
        YBDLogUtil.d('request, user level is not enable');

        isOfficialTalent =
            await YBDAgencyApiHelper.isOfficialTalent(context, userInfo!.id);
        YBDTPGlobal.isOfficialTalent = isOfficialTalent;
      }
    });
  }

  void _RequestOfficialTalentmzSYBoyelive() async {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void initState() {
    super.initState();
    // 初始化内购买服务
    YBDIapService.instance.initConfig();
    YBDIapService.instance.context = context;

    // 日志弹框
    YBDLogUtil.popLogViewOnShake(context);

    /// 充值送礼活动弹框倒计时
    /// 超过一分钟后自动弹框
    Future.delayed(Duration(seconds: 60), () async {
      logger.v('after 60 seconds showTopUpGift');
      _activityHandler?.showTopUpGiftIgnoreTask();
    });

    // 要判断是否签约主播
    _asyncRequestOfficialTalent();

    // 根据配置信息上传日志
    YBDRemoteConfigService.getInstance().then((remoteConfigService) async {
      YBDLogUtil.uploadLogWithRemoteConfig(remoteConfigService!);
    });

    // 设置用户id
    YBDAnalyticsUtil.setUserId(
        '${YBDCommonUtil.storeFromContext()!.state.bean!.id}');
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE_MAIN));

    _overFlowHeight = 36;

    // 配置默认的 tab 图标
    _configDefaultTabIcon();

    // 配置选中的 tab 动图
    _configSelectedGifTabIcon();

    /// 初始化 tab 动图控制器
    _configGifController();

    // 播放第一个 tab 动图
    _playTabGif(
      index: _selectingIndex,
      gifList: gifs,
      controllerList: gifControllers,
    );

    // 初始化动态播放器
    YBDStatusPlayer.instance;

    // 添加动图播放结束后的处理方法
    _addGifCompleteListener();

    // 监听系统事件
    WidgetsBinding.instance?.addObserver(this);

    // 配置私信 bloc
    _configInboxBloc();

    YBDMessageHelper.setRoutineInterval();

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      // Get the mini game address configuration information and check the game status
      YBDCommonUtil.setGreedyGameUrl();
      // 开始轮询新手引导，推荐主播弹框，签到框的显示状态
      YBDTaskService.instance!.start();

      // 首页引导
      YBDPopularGuide.showPopularGuide(context);

      // 每 48 小时仅提示一次检查版本更新
      YBDCheckUpdateUtil.checkUpdateWithPeriod(context, 48);

      YBDPKRTMHelper.isInPk(context);

      // 同步 sp 数据需要时间
      Future.delayed(Duration(seconds: 3), () {
        // 首页检查当前用户是否首次打开 app
        YBDUserUtil.checkIsTodayFirstOpenApp();

        // 延迟检查弹框任务，同步 sp 数据需要时间
        Future.delayed(Duration(seconds: 3), () {
          _activityHandler = YBDActivityHandler(context);
          _activityHandler!.showActivity();
        });
      });

      eventBus.on<YBDGoTabEvent>().listen((YBDGoTabEvent event) async {
        // 请求用户信息
        _clickedTab(event.index!);
      });
      eventBus
          .on<YBDGameHallBgEvent>()
          .listen((YBDGameHallBgEvent event) async {
        //切换背景

        showTpGoBg = event.showTpGo;
        print("ggets$showTpGoBg");
        if (mounted) setState(() {});
      });
      _statusInfoSubscription = eventBus
          .on<YBDStatusChangeBusEvent>()
          .listen((YBDStatusChangeBusEvent event) async {
        logger.v(
            'YBDStatusChangeBusEvent statusId : ${event.statusInfo?.id}, current: ${_statusInfo?.id}');
        if (_statusInfo?.id != event.statusInfo?.id) {
          logger.v('YBDStatusChangeBusEvent eventBus not same');
          // 保存动态背景数据
          _statusInfo = event.statusInfo;
        }
      });

      ///初始化
      YBDRtcHelper.getInstance().login();

      /// pk资源提前预加载
      PkResultTypeExt.predownloadPKSvgas();

      /// 预下载
      if (YBDCommonUtil.getRoomOperateInfo().launchDownloadTpgo == 1)
        YBDTPCache.downloadCache();
    });

    // firebase messaging 推送，subscribe topic
    YBDNotificationUtil.init(context);
    YBDMessageHelper.setFetchMessageRoutine(context);

    YBDSocketUtil.getInstance()!.initSocket();
    YBDSocketUtil.getInstance()!.setSocketCheckRoutine();

    if (Platform.isAndroid) {
      YBDPurchaseUtil.inAppDeleteCompleteOrder();

      ///校验 google 支付订单  只要不是校验成功或者订单完成都要去校验
      YBDPurchaseUtil.googleVerify(context);

      /// 检查 google 漏单  未消耗的订单
      _googlePurchaseHandler = YBDGooglePurchaseHandler(context);
      _googlePurchaseHandler.initStoreInfo();
    } else if (Platform.isIOS) {
      // 初始化支付配置
      YBDIapService.instance.initConfig();
    }

    _sentPhoneInfo(context);

    ApiHelper.uploadFcmToken(context);

    // 模拟皮肤数据
    // YBDActivityFile.instance.setConfigEntity(YBDActivitySkinEntity.demoData());
    logger.v('if shouldDownloadZip');
    // 根据需要下载皮肤zip
    if (YBDActivityFile.instance!.shouldDownloadZip()) {
      logger.v('true shouldDownloadZip');
      YBDActivityFile.instance!.downloadSkinZip();
      // 监听下载进度
      YBDActivityFile.instance!.onDownloadEvent();
    }
    updateTPGlobal();

    ApiHelper.getSelfEmoji();
  }

  void initStatexkhL8oyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  void updateTPGlobal() {
    YBDTPGlobal.openTrack = YBDAnalyticsUtil.openTrack();
  }

  @override
  void dispose() {
    YBDRoomSocketUtil.getInstance().logOutSocket();
    WidgetsBinding.instance?.removeObserver(this);
    _statusInfoSubscription.cancel();
    super.dispose();
  }

  void disposezhFByoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        YBDMessageHelper.setFetchMessageRoutine(context);
        YBDSocketUtil.getInstance()!.setSocketCheckRoutine();
        setUpRoomSocket();
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
    }
  }

  void didChangeAppLifecycleStateQHpXmoyelive(AppLifecycleState state) {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  Widget myBuild(BuildContext context) {
    // YBDLaunchUserCheck.checkUserGameStatus();

    return Scaffold(
      body: WillPopScope(
        onWillPop: _backActionHandler.onWillPop,
        child: Container(
            // padding: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight),
            decoration: _pageContainerDecoration(),
            child: Stack(
              children: [
                _indexPageBg(),
                Column(
                  children: [
                    Expanded(
                      child: IndexedStack(
                        index: _selectingIndex,
                        children: <Widget>[
                          YBDHomePage(),
                          YBDInboxPage(),
                          YBDMyProfilePage(),
                        ],
                      ),
                    ),
                    // 底部标签栏
                    _bottomTabBar(),
                  ],
                ),
              ],
            )),
      ),
    );
  }

  void myBuild1VAW0oyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 动态列表背景
  Widget _indexPageBg() {
    if (_selectingIndex == FeatureTabIndex)
      return Container(
        // padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight + 114.px),
        child: Stack(
          children: [
            Positioned.fill(
                child: Image.asset(
                    YBDGameResource.assetPadding('game_lobby_bg',
                        need2x: false, isWebp: false, suffix: "jpg"),
                    fit: BoxFit.cover)),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Image.asset(
                    YBDGameResource.assetPadding('lobby_bottom',
                        need2x: false, isWebp: true),
                    fit: BoxFit.fitWidth)),
          ],
        ),
      );
    else
      return SizedBox();
  }

  void _indexPageBgZ9zAdoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 页面容器 decoration
  BoxDecoration _pageContainerDecoration() {
    // if (_selectingIndex == FeatureTabIndex) {
    //   return BoxDecoration();
    // }

    return YBDTPStyle.gradientDecoration;
  }

  /// 底部 tab bar
  Widget _bottomTabBar() {
    return YBDColoredSafeArea(
      color: YBDActivitySkinRoot().curAct().indexBgColor(),
      top: false,
      bottom: true,
      child: Container(
        height: ScreenUtil().setWidth(Const.BOTTOM_BAR_HEIGHT),
        color: YBDActivitySkinRoot().curAct().indexBgColor(),
        child: OverflowBox(
          maxHeight: ScreenUtil().setWidth(96 + _overFlowHeight),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _getIcon(0, 0),
              _getIcon(1, 0),
              _getIcon(2, 0),
            ],
          ),
        ),
      ),
    );
  }

  void _bottomTabBarxi79Uoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// tab 图标
  _getIcon(int index, int unreadCount) {
    return GestureDetector(
      onTap: () {
        _clickedTab(index);

        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.MAIN_PAGE,
          itemName: 'BottomTab index:$index',
        ));
      },
      child: Container(
        height: ScreenUtil().setWidth(96),
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(12),
          right: ScreenUtil().setWidth(12),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Align(
              // 点击 tab 时显示的图片
              alignment: Alignment.center,
              child:
                  _selectingIndex == index ? _selectingTab(index) : Container(),
            ),
            // 第一次点击或未选中的 tab
            showBackgrounds[index] || _selectingIndex != index
                ? _defaultTab(defaults[index])
                : Container(),
            // 有未读消息的私信 tab
            index == 1 && inboxUnreadCount != 0
                ? _inboxTabWithUnreadCount(inboxUnreadCount)
                : Container()
          ],
        ),
      ),
    );
  }

  /// 点击 tab 的响应事件
  _clickedTab(int index) async {
    logger.v('clicked tab index: $index, current: $_selectingIndex');
    YBDCommonTrack().commonTrack(YBDTAProps(
        location: YBDShortRoute.index_names[index], module: YBDTAModule.index));
    if (index == FeatureTabIndex) {
      YBDGameRoomDialogGuide().showGameRoomGuide(context);
      eventBus.fire(YBDRefreshStatusEvent());

      if (_selectingIndex == FeatureTabIndex) {
        logger.v('fire YBDRefreshStatusEvent');
        // 在动态页点击动态标签按钮时触发刷新列表的事件
        // 播放动态标签动图
        _playTabGif(
          index: FeatureTabIndex,
          gifList: gifs,
          controllerList: gifControllers,
        );
      }
    }

    // 震动效果
    if (Platform.isAndroid) {
      Vibration.vibrate(duration: 30);
    }

    if (_selectingIndex != index) {
      YBDObsUtil.instance().tabRouteChange(
        routeList: YBDShortRoute.index_list,
        curIndex: index,
        preIndex: _selectingIndex,
        preRoute: YBDShortRoute.index_page,
      );
      // 切换tab时刷新页面
      setState(() {
        _selectingIndex = index;
      });

      if (gifs.isNotEmpty) {
        //  切换tab时播放动图
        for (int gifIndex = 0; gifIndex < gifControllers.length; gifIndex++) {
          if (gifIndex == index) {
            _playTabGif(
                index: gifIndex, gifList: gifs, controllerList: gifControllers);
          } else {
            _stopTabGif(index: gifIndex, controllerList: gifControllers);
          }
        }
      }
    }
  }

  /// 播放底部标签动图
  /// [index]要播放的动图索引
  /// [gifList]gif图片数组
  /// [controllerList] YBDGifController gif动图控制器数组
  void _playTabGif({
    required int index,
    required List<String> gifList,
    required List<YBDGifController> controllerList,
  }) {
    logger.v('_playTabGif: $index');
    assert(index >= 0, 'index is invalid');
    assert(controllerList.length > index, 'gif not exist');
    assert(gifList.isNotEmpty, 'gif list is empty');
    controllerList[index].reset();
    controllerList[index].animateTo(29);
  }

  /// 停止底部标签动图
  /// [index]要播放的动图索引
  /// [controllerList] YBDGifController gif动图控制器数组
  void _stopTabGif({
    required int index,
    List<String>? gifList,
    required List<YBDGifController> controllerList,
  }) {
    logger.v('_stopTabGif: $index');
    assert(controllerList.length > index, 'gif not exist');
    assert(index >= 0, 'index is invalid');
    controllerList[index].reset();
  }

  /// 默认 tab 解决第一次点击 tab 时图标缺省的问题
  Widget _defaultTab(String img) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        width: ScreenUtil().setWidth(80),
        child: YBDImage(path: img, fit: BoxFit.cover),
      ),
    );
  }

  void _defaultTab8IWfHoyelive(String img) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 点击 tab 时显示的图片
  Widget _selectingTab(int index) {
    if (gifs.isNotEmpty) {
      // logger.v('12.13-----gifs[$index]:${gifs[index]}');
      return YBDGifImage(
        image: YBDActivitySkinRoot().curAct().getDecorationImage(gifs[index]),
        controller: gifControllers[index],
        width: ScreenUtil().setWidth(80),
        fit: BoxFit.fitWidth,
      );
    } else {
      return Align(
        alignment: Alignment.center,
        child: Container(
          width: ScreenUtil().setWidth(70),
          child: Image.asset(
            // 这个_selectedTabIcon为null，如果走到这里会报错
            _selectedTabIcon[index],
          ),
        ),
      );
    }
  }

  /// 有未读消息的私信红点
  Widget _inboxTabWithUnreadCount(int unreadCount) {
    assert(null != unreadCount && unreadCount > 0);

    return Align(
      alignment: Alignment.topRight,
      child: Transform.translate(
        offset: Offset(ScreenUtil().setWidth(20), ScreenUtil().setWidth(10)),
        child: Container(
          height: ScreenUtil().setWidth(40),
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(6),
          ),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Color(0xffE6497A),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                YBDCommonUtil.getUnreadCount(unreadCount),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(18),
                  color: Colors.white,
                  height: 1,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _inboxTabWithUnreadCountOqElYoyelive(int unreadCount) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 配置选中 tab 动图
  _configSelectedGifTabIcon() {
    gifs = YBDActivitySkinRoot().curAct().getTabIcon();
  }

  /// 配置未选中的 tab 图标
  _configDefaultTabIcon() {
    defaults = YBDActivitySkinRoot().curAct().getTabDefaultIcon();
  }

  /// 配置动画控制器
  _configGifController() {
    if (gifs.isNotEmpty) {
      gifControllers = List.generate(
        4,
        (_) => YBDGifController(
          vsync: this,
          duration: Duration(milliseconds: 1000),
        ),
      );
    }
  }

  /// 监听动画结束事件
  _addGifCompleteListener() {
    if (gifs.isNotEmpty) {
      gifControllers.forEach((element) {
        element.addStatusListener((status) {
          if (status == AnimationStatus.completed) {
            int index = gifControllers.indexOf(element);
            print("set compelete compelete $index");
            showBackgrounds[index] = false;
            //偶现 点击事件里的setstate 会在这里之前
            setState(() {});
          }
        });
      });
    }
  }

  /// 配置私信 bloc
  _configInboxBloc() {
    try {
      bloc = SelectBloc(
        database: messageDb!,
        table: YBDConversationModel.conversationTableName,
        reactive: true,
      );

      // TODO: 测试代码
      // insertEventNoticeMessage(YBDMessageModel());
    } catch (e) {
      YBDLogUtil.d('init SelectBloc error : $e');
    }

    if (null != bloc) {
      bloc!.items.listen((data) {
        if (data != null && mounted) {
          List<YBDConversationModel> updateConversation = List.generate(
              data.length,
              (index) => YBDConversationModel().fromDb(data[index]));
          updateConversation.removeWhere((element) =>
              element.unreadCount == 0 || element.lastMessage!.isEmpty);
          int count = 0;
          updateConversation.forEach((element) {
            count += element.unreadCount!;
          });

          inboxUnreadCount = count;
          setState(() {});
        }
      });
    } else {
      YBDLogUtil.d('bloc is null');
    }

    YBDMessageHelper.setRoutineInterval();

    YBDMessageHelper.setFetchMessageRoutine(context);

    YBDSocketUtil.getInstance()!.initSocket();
    YBDSocketUtil.getInstance()!.setSocketCheckRoutine();

    setUpRoomSocket();
  }

  setUpRoomSocket() async {
    logger.v('setUpRoomSocket');
    await YBDRoomSocketUtil.getInstance().initSocket();
    await YBDRoomSocketUtil.getInstance().setSocketCheckRoutine();
    await YBDRoomSocketUtil.getInstance().setPingRoutine(Keep_Live_Interval);
  }

  ///上传设备信息，不阻塞
  static _sentPhoneInfo(BuildContext context) async {
    logger.i('sentPhoneInfo params data');
    print('sentPhoneInfo params data');
    String? advertisingId;
    String? flyerUID;
    PackageInfo? packageInfo;
    try {
      advertisingId = await AdvertisingId.id(true);
      packageInfo = await PackageInfo.fromPlatform();
      flyerUID = await YBDAnalyticsUtil.appsflyerSdk?.getAppsFlyerUID();
    } on PlatformException catch (e) {
      logger.d('get phone info error:' + e.toString());
    }
    Map<String, dynamic> params = {
      'appsflyer_id': flyerUID,
      'advertising_id': advertisingId,
      'app_version_name': packageInfo?.version,
    };
    print('sentPhoneInfo params data:$params');
    ApiHelper.sentPhoneInfo(context, params);
  }
}
