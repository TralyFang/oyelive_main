import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/db_ybd_config.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/inbox/db/model_ybd_message.dart';
import '../../status/local_audio/top_ybd_navigate_bar.dart';
import 'widget/event_ybd_list_item.dart';

/// 活动消息
class YBDEventNoticePage extends StatefulWidget {
  /// 消息队列 id
  final String queueId;

  YBDEventNoticePage(this.queueId);

  @override
  YBDEventNoticePageState createState() => YBDEventNoticePageState();
}

class YBDEventNoticePageState extends BaseState<YBDEventNoticePage> {
  /// 数据库更新管理器
  late SelectBloc _bloc;

  /// 系统消息列表
  List<YBDMessageModel> _messageList = [];

  @override
  void initState() {
    super.initState();
    logger.v('queue id : ${widget.queueId}');

    // 从数据库消息表中查系统消息
    _bloc = SelectBloc(
      database: messageDb!,
      where: "queue_id = \"${widget.queueId}\" ",
      table: YBDMessageModel.messageTableName,
      orderBy: 'send_time DESC',
      reactive: true,
      verbose: true,
    );

    /// 监听数据库中系统消息更新
    _bloc.items.listen((data) {
      if (data != null && mounted) {
        logger.v('update event msg from database');
        List<YBDMessageModel> msgList = List.generate(data.length, (index) => YBDMessageModel().fromDb(data[index]));

        // 更新消息列表
        addMessageRemoveReplicate(msgList);
        setState(() {});
      }
    });

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE, location: "event_notice_page"));
  }
  void initStateGEo0xoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: <Widget>[
            // 顶部导航栏
            YBDTopNavigateBar(title: translate('events_notice')),
            Expanded(
              child: Container(
                // 活动消息列表
                width: ScreenUtil().screenWidth,
                padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                child: eventMsgListView(_messageList),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void myBuildS0zVJoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 消息列表
  Widget eventMsgListView(List<dynamic> data) {
    if (null != data && data.isNotEmpty) {
      logger.v('event msg amount : ${data.length}');
      return ListView.separated(
          padding: EdgeInsets.only(
            top: 0,
            bottom: ScreenUtil().setWidth(30),
          ),
          itemBuilder: (_, index) {
            return Container(
              // 列表 item
              child: YBDEventListItem(
                data[index],
                (jsonContent) {
                  logger.v('clicked event msg item callback');
                  YBDNavigatorHelper.onTapShare(context, jsonContent, apiMsg: true);
                },
                () {
                  logger.v('delete event msg item callback');
                  _messageList.removeAt(index);
                },
              ),
            );
          },
          separatorBuilder: (_, index) {
            // 列表分割线
            return SizedBox(height: ScreenUtil().setWidth(0));
          },
          itemCount: data.length);
    } else {
      logger.v('event msg is empty, show empty view');
      // 数据为空显示缺省图
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/empty/empty_my_profile.webp",
              width: ScreenUtil().setWidth(382),
            ),
            SizedBox(height: ScreenUtil().setWidth(30)),
            Text(
              translate('no_message'),
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      );
    }
  }
  void eventMsgListViewEQcetoyelive(List<dynamic> data) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 更新消息列表
  addMessageRemoveReplicate(List<YBDMessageModel> newMessage) {
    if (null == _messageList) {
      _messageList = [];
    }

    for (YBDMessageModel message in newMessage) {
      bool isReplace = false;
      for (int index = 0; index < _messageList.length; index++) {
        if (_messageList[index].messageId == message.messageId || _messageList[index].requestId == message.requestId) {
          _messageList[index] = message;
          isReplace = true;
          break;
        }
      }

      if (!isReplace) {
        _messageList.add(message);
      }
    }

    _messageList.sort((a, b) => b.sendTime!.compareTo(a.sendTime!));
  }
}
