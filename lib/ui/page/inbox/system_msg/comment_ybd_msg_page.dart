import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/db_ybd_config.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/inbox/db/model_ybd_message.dart';
import '../../../../module/inbox/message_ybd_helper.dart';
import 'widget/like_ybd_msg_list_item.dart';
import '../../status/local_audio/top_ybd_navigate_bar.dart';
import '../../../widget/loading_ybd_circle.dart';

/// 评论消息页面
class YBDCommentMsgPage extends StatefulWidget {
  /// 消息队列 id
  final String queueId;

  /// 未读消息条数
  final int unreadAmount;

  YBDCommentMsgPage(this.queueId, {this.unreadAmount = 0});

  @override
  YBDCommentMsgPageState createState() => YBDCommentMsgPageState();
}

class YBDCommentMsgPageState extends BaseState<YBDCommentMsgPage> {
  /// 数据库更新管理器
  late SelectBloc _bloc;

  /// 系统消息列表
  List<YBDMessageModel> _messageList = [];

  /// 是否为初始状态
  bool _isInitState = true;

  @override
  void initState() {
    super.initState();
    logger.v('queue id : ${widget.queueId}');

    // 从数据库消息表中查系统消息
    _bloc = SelectBloc(
      database: messageDb!,
      where: "queue_id = \"${widget.queueId}\" ",
      table: YBDMessageModel.messageTableName,
      orderBy: 'send_time DESC',
      reactive: true,
      verbose: true,
    );

    /// 监听数据库中系统消息更新
    _bloc.items.listen((data) {
      if (data != null && mounted) {
        logger.v('update comment msg from database');
        // 有数据更新后设置为非初始状态
        _isInitState = false;

        List<YBDMessageModel> msgList = List.generate(data.length, (index) => YBDMessageModel().fromDb(data[index]));

        // 更新消息列表
        addMessageRemoveReplicate(msgList);

        // 刷新界面
        setState(() {});
      }
    });

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE, location: "comment_message_page"));
  }
  void initState4L65goyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: <Widget>[
            // 顶部导航栏
            YBDTopNavigateBar(title: translate('comment')),
            widget.unreadAmount > 0
                ? Container(
                    // 未读消息条数
                    height: ScreenUtil().setWidth(80),
                    padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(20)),
                    child: Text(
                      "${translate('you_received')} ${widget.unreadAmount} ${translate('comments')}",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        color: Colors.white,
                      ),
                    ),
                  )
                : Container(),
            Expanded(child: messagePageContentView()),
          ],
        ),
      ),
    );
  }
  void myBuildtjPGnoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 消息列表
  Widget commentMsgListView(List<YBDMessageModel> data) {
    if (null != data && data.isNotEmpty) {
      logger.v('comment msg amount : ${data.length}');
      return ListView.separated(
          padding: EdgeInsets.only(top: 0),
          itemBuilder: (_, index) {
            return Container(
              // 列表 item
              child: YBDLikeMsgListItem(
                MsgItemType.Comment,
                data[index],
                () {
                  logger.v('clicked comment msg item callback');
                  // item 里实现了跳转逻辑
                },
                () {
                  logger.v('delete comment msg item callback');
                  _messageList.removeAt(index);
                },
              ),
            );
          },
          separatorBuilder: (_, index) {
            return Container(
              // 列表分割线
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(112)),
              child: Divider(
                height: ScreenUtil().setWidth(1),
                thickness: ScreenUtil().setWidth(1),
                color: Colors.white.withOpacity(0.2),
              ),
            );
          },
          itemCount: data.length);
    } else {
      logger.v('comment msg is empty, show empty view');
      return Container();
    }
  }
  void commentMsgListViewW2WmDoyelive(List<YBDMessageModel> data) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 消息页面内容
  Widget messagePageContentView() {
    if (_isInitState) {
      logger.v('init loading');
      // 初始状态显示加载图
      return YBDLoadingCircle();
    } else {
      if (_messageList != null && _messageList.isNotEmpty) {
        // 有数据显示消息列表
        return Container(
          // 点赞消息列表
          width: ScreenUtil().screenWidth,
          child: commentMsgListView(_messageList),
        );
      } else {
        logger.v('empty msg list');
        // 数据为空显示缺省图
        return Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // TODO: 换图片
              Image.asset(
                "assets/images/empty/empty_my_profile.webp",
                width: ScreenUtil().setWidth(382),
              ),
              SizedBox(height: ScreenUtil().setWidth(30)),
              Text(
                translate('no_message'),
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        );
      }
    }
  }
  void messagePageContentViewcpbcHoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 更新消息列表
  addMessageRemoveReplicate(List<YBDMessageModel> newMessage) {
    if (null == _messageList) {
      _messageList = [];
    }

    for (YBDMessageModel message in newMessage) {
      bool isReplace = false;
      for (int index = 0; index < _messageList.length; index++) {
        if (_messageList[index].messageId == message.messageId || _messageList[index].requestId == message.requestId) {
          _messageList[index] = message;
          isReplace = true;
          break;
        }
      }

      if (!isReplace) {
        _messageList.add(message);
      }
    }

    _messageList.sort((a, b) => b.sendTime!.compareTo(a.sendTime!));
  }

  @override
  @mustCallSuper
  dispose() {
    YBDMessageHelper.quitReading(context);
    super.dispose();
  }
}
