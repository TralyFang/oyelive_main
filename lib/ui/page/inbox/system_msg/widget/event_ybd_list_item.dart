import 'dart:async';

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import '../../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../../common/util/date_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../common/widget/expandable_ybd_text.dart';
import '../../../../../generated/json/base/json_convert_content.dart';
import '../../../../../module/inbox/db/model_ybd_message.dart';
import '../../../../../module/inbox/entity/message_ybd_list_entity.dart';
import '../../../../../module/inbox/message_ybd_helper.dart';
import 'opacity_ybd_bg_net_img.dart';
import '../../../status/local_audio/scale_ybd_animate_button.dart';
import '../../../../widget/pop_menu/pop_ybd_menu.dart';

/// /// 点击活动消息的回调，参数为 item 的json数据用来判断点击 item 跳转到哪个页面
typedef EventMsgItemCallback = Function(dynamic);

/// 活动消息列表 item
class YBDEventListItem extends StatefulWidget {
  /// item 数据
  final YBDMessageModel data;

  /// 删除消息的回调
  final Function onDelete;

  /// 点击 item 的回调
  final EventMsgItemCallback clickCallback;

  YBDEventListItem(this.data, this.clickCallback, this.onDelete);

  @override
  YBDEventListItemState createState() => YBDEventListItemState();
}

class YBDEventListItemState extends State<YBDEventListItem> {
  /// 是否弹出了菜单选项
  bool _showedPopMenu = false;

  List<String> actions = [
    translate('Copy'),
    translate('delete'),
  ];

  @override
  Widget build(BuildContext context) {
    var jsonContent;

    try {
      jsonContent = json.decode(widget.data.jsonContent!);
    } catch (e) {
      logger.v('decode msg json content error : $e');
    }

    return YBDWPopupMenu(
      menuHeight: ScreenUtil().setWidth(54),
      menuWidth: ScreenUtil().setWidth(200),
      onValueChanged: (int value) {
        logger.v('popup menu changed : $value');

        // 操作后的回调
        if (value == 0) {
          logger.v('copied event msg : ${widget.data.text ?? ''}');
          Clipboard.setData(ClipboardData(text: widget.data.text ?? ''));
          YBDToastUtil.toast(translate('copied_to_clipboard'));
        } else {
          logger.v('deleted event msg : ${widget.data.text}');
          YBDMessageHelper.deleteSingleChat(widget.data.messageId, widget.data.queueId);
          widget.onDelete.call();
        }
      },
      startPress: () {
        setState(() {
          _showedPopMenu = true;
        });
      },
      endPress: () {
        if (_showedPopMenu) {
          setState(() {
            _showedPopMenu = false;
          });
        }
      },
      showAtCenter: true,
      pressType: PressType.longPress,
      actions: actions,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.transparent,
        ),
        child: _messageContent(jsonContent),
      ),
    );
  }
  void buildGe7BIoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _messageContent(jsonContent) {
    String iconUrl = '';
    String title = '';
    String text = '';
    YBDMessageListDataPullMessage? messageEntity;

    if (null != jsonContent) {
      messageEntity = JsonConvert.fromJsonAsT<YBDMessageListDataPullMessage>(jsonContent);
    }

    iconUrl = messageEntity?.content?.resource ?? '';
    title = messageEntity?.content?.title ?? '';
    text = messageEntity?.content?.text ?? '';

    // 圆角
    return GestureDetector(
      onTap: () {
        logger.v('clicked msg item');
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.EVENT_NOTICE_PAGE,
          itemName: title,
        ));
        widget.clickCallback(jsonContent);
      },
      child: Column(
        children: <Widget>[
          Container(
            // 活动日期
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(36)),
            child: Center(
              child: Text(
                YBDDateUtil.timeFromMilliseconds(
                  widget.data.sendTime ?? 0,
                  monthFormat: "dd MMM 'at' HH:mm",
                  yearFormat: "dd MMM yyyy 'at' HH:mm",
                ),
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  color: Color(0xffCECECE),
                ),
              ),
            ),
          ),
          // 没有活动图时隐藏图片组件
          iconUrl.isNotEmpty
              ? YBDOpacityBgNetImg(
                  iconUrl: iconUrl,
                  height: ScreenUtil().setWidth(210), // 2021/7/1 10:35 瑶瑶要求更改的
                  // width: ScreenUtil().screenWidth,
                  width: ScreenUtil().setWidth(670),
                  radius: BorderRadius.only(
                    topLeft: Radius.circular(ScreenUtil().setWidth(15)),
                    topRight: Radius.circular(ScreenUtil().setWidth(15)),
                  ),
                )
              : Container(),
          Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(ScreenUtil().setWidth(15)),
                bottomRight: Radius.circular(ScreenUtil().setWidth(15)),
              ),
              onTap: () {
                logger.v('clicked msg item');
                YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                  YBDEventName.CLICK_EVENT,
                  location: YBDLocationName.EVENT_NOTICE_PAGE,
                  itemName: title,
                ));
                widget.clickCallback(jsonContent);
              },
              child: _noticeTextContent(title, text),
            ),
          ),
        ],
      ),
    );
  }
  void _messageContent7jUfmoyelive(jsonContent) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 活动标题和内容
  Widget _noticeTextContent(String title, String text) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(ScreenUtil().setWidth(15)),
          bottomRight: Radius.circular(ScreenUtil().setWidth(15)),
        ),
        color: _showedPopMenu ? Colors.black.withOpacity(0.15) : Colors.white.withOpacity(0.15),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setWidth(20)),
          Row(
            children: <Widget>[
              SizedBox(width: ScreenUtil().setWidth(18)),
              Container(
                // 活动标题
                // width: ScreenUtil().setWidth(620),
                constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(470)),
                child: Text(
                  title,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(28),
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(child: SizedBox(width: 1)),
              Text(
                DateFormat('yyyy/MM/dd').format(DateTime.fromMillisecondsSinceEpoch(widget.data.sendTime ?? 0)),
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(24),
                  color: Color(0xffCECECE),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(20)),
            ],
          ),
          SizedBox(height: ScreenUtil().setWidth(15)),
          Row(
            children: <Widget>[
              SizedBox(width: ScreenUtil().setWidth(18)),
              Container(
                // 活动导读文字
                width: ScreenUtil().setWidth(620),
                child: YBDDescriptionTextWidget(text: text),
              ),
              SizedBox(width: ScreenUtil().setWidth(15)),
            ],
          ),
          SizedBox(height: ScreenUtil().setWidth(15)),
        ],
      ),
    );
  }
  void _noticeTextContentwEh8foyelive(String title, String text) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 可以通过点击按钮扩展/合并文本的组件
class YBDDescriptionTextWidget extends StatefulWidget {
  final String text;

  YBDDescriptionTextWidget({required this.text});

  @override
  _YBDDescriptionTextWidgetState createState() => new _YBDDescriptionTextWidgetState();
}

class _YBDDescriptionTextWidgetState extends State<YBDDescriptionTextWidget> {
  bool flag = true;

  @override
  void initState() {
    super.initState();
  }
  void initStateDu466oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: double.infinity,
      child: YBDExpandableText(
        widget.text,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(24),
          color: Color(0xffCECECE),
        ),
        collapseText: 'Less',
        expandText: 'More',
        textAlign: TextAlign.left,
      ),
    );
  }
  void buildmEiq0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
