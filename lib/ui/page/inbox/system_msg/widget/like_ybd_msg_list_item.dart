import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../../../common/constant/const.dart';
import '../../../../../common/util/date_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../generated/json/base/json_convert_content.dart';
import '../../../../../module/inbox/db/model_ybd_message.dart';
import '../../../../../module/inbox/entity/message_ybd_list_entity.dart';
import '../../../../../module/inbox/message_ybd_helper.dart';
import '../../../../widget/pop_menu/pop_ybd_menu.dart';
import '../../../../widget/round_ybd_avatar.dart';
import 'opacity_ybd_bg_net_img.dart';

/// item 类型
enum MsgItemType {
  Like,
  Comment,
}

/// 点赞消息列表 item
class YBDLikeMsgListItem extends StatefulWidget {
  final MsgItemType type;

  /// item 数据
  final YBDMessageModel data;

  /// 删除消息的回调
  final Function onDelete;

  /// 点击 item 的回调
  final Function clickCallback;

  YBDLikeMsgListItem(this.type, this.data, this.clickCallback, this.onDelete);

  @override
  YBDLikeMsgListItemState createState() => YBDLikeMsgListItemState();
}

class YBDLikeMsgListItemState extends State<YBDLikeMsgListItem> {
  /// 是否弹出了菜单选项
  bool _showedPopMenu = false;

  List<String> actions = [
    translate('Copy'),
  ];

  @override
  Widget build(BuildContext context) {
    return YBDWPopupMenu(
      menuHeight: ScreenUtil().setWidth(54),
      menuWidth: ScreenUtil().setWidth(100),
      onValueChanged: (int value) {
        logger.v('popup menu changed : $value');

        // 操作后的回调
        if (value == 0) {
          logger.v('copied comment msg : ${widget.data.text}');
          Clipboard.setData(ClipboardData(text: widget.data.text));
          YBDToastUtil.toast(translate('copied_to_clipboard'));
        } else {
          logger.v('deleted comment msg : ${widget.data.text}');
          YBDMessageHelper.deleteSingleChat(
              widget.data.messageId, widget.data.queueId);
          widget.onDelete.call();
        }
      },
      startPress: () {
        setState(() {
          _showedPopMenu = true;
        });
      },
      endPress: () {
        if (_showedPopMenu) {
          setState(() {
            _showedPopMenu = false;
          });
        }
      },
      showAtCenter: true,
      pressType: PressType.longPress,
      actions: actions,
      child: Container(
        color: _showedPopMenu
            ? Colors.black.withOpacity(0.15)
            : Colors.transparent,
        child: Material(
          color: Colors.transparent,
          child: _messageContent(),
        ),
      ),
    );
  }

  void buildhpLaZoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  Widget _messageContent() {
    String iconUrl = '';
    String title = '';
    String subTitle = '';
    String resource = '';
    int? senderId = 0;
    int? receiver = 0;
    String statusId = '';
    String? layout = '';
    YBDMessageQuoteMessage? quoteMsg;

    try {
      Map? mapData = json.decode(widget.data.jsonContent!);
      logger.l('_msgImg url mapData: $mapData');
      YBDMessageListDataPullMessage messageEntity =
          JsonConvert.fromJsonAsT<YBDMessageListDataPullMessage>(mapData);
      iconUrl = messageEntity.content!.icon ?? '';
      title = messageEntity.content!.title ?? '';
      subTitle = messageEntity.content!.subtitle ?? '';
      resource = messageEntity.content!.resource ?? '';
      senderId = messageEntity.sender!.id;
      statusId = messageEntity.attributes['id'] ?? '';
      layout = messageEntity.content!.layout;
      quoteMsg = messageEntity.content!.quoteMessage;
      receiver = messageEntity.receivers!.first!.id;
    } catch (e) {
      logger.v('decode msg json content error : $e');
    }

    // 显示动态图片
    bool showImg = null != resource && resource.isNotEmpty;

    return InkWell(
      onTap: () {
        logger.v('clicked comment msg item');
        widget.clickCallback();
        // YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.status_detail + "/$statusId");
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: <Widget>[
              SizedBox(width: ScreenUtil().setWidth(25)),
              YBDRoundAvatar(
                // 个人头像图标
                iconUrl,
                userId: senderId,
                scene: 'B',
                avatarWidth: 90,
              ),
              SizedBox(width: ScreenUtil().setWidth(40)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setWidth(30)),
                  // 点赞人的昵称
                  _nickName(title),
                  SizedBox(height: ScreenUtil().setWidth(14)),
                  // 消息副标题栏
                  _subTitleRow(subTitle, showImg),
                  SizedBox(height: ScreenUtil().setWidth(30)),
                  // 消息引用栏
                  _quoteMsgRow(quoteMsg, receiver, layout),
                  SizedBox(height: ScreenUtil().setWidth(30)),
                ],
              ),
              Expanded(child: Container()),
              // 消息图片
              ..._msgImg(showImg, resource, layout),
              SizedBox(width: ScreenUtil().setWidth(30)),
            ],
          ),
        ],
      ),
    );
  }

  void _messageContentD020joyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 点赞人的昵称
  Widget _nickName(String title) {
    return Container(
      // 点赞人的昵称
      constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(390)),
      height: ScreenUtil().setWidth(30),
      child: Text(
        title,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(24),
          color: Colors.white.withOpacity(0.6),
        ),
      ),
    );
  }

  void _nickName59rENoyelive(String title) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 消息副标题栏
  Widget _subTitleRow(String subTitle, bool showImg) {
    return Row(
      children: <Widget>[
        Container(
          // 消息内容
          height: (subTitle).isEmpty ? 0 : ScreenUtil().setWidth(30),
          child: Text(
            subTitle,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(20),
              color: Colors.white.withOpacity(0.6),
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
        SizedBox(width: ScreenUtil().setWidth(14)),
        Container(
          child: Text(
            // 消息时间
            YBDDateUtil.timeFromMilliseconds(widget.data.sendTime ?? '' as int),
            style: TextStyle(
              fontSize: ScreenUtil().setSp(20),
              color: Colors.white.withOpacity(0.6),
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
      ],
    );
  }

  void _subTitleRowSuEIboyelive(String subTitle, bool showImg) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 引用的评论栏
  Widget _quoteMsgRow(
    YBDMessageQuoteMessage? quoteMsg,
    int? receiver,
    String? layout,
  ) {
    if (layout != Const.INBOX_LAYOUT_COMMENT_LIKE) {
      // 非点赞评论消息
      return Container();
    }

    return Container(
      height: ScreenUtil().setWidth(55),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.white.withOpacity(0.1),
      ),
      child: Row(
        children: <Widget>[
          SizedBox(width: ScreenUtil().setWidth(14)),
          YBDRoundAvatar(
            // 个人头像图标
            quoteMsg?.icon,
            userId: '$receiver',
            scene: 'B',
            avatarWidth: 40,
          ),
          // SizedBox(width: ScreenUtil().setWidth(7)),
          Text(
            ' : ${quoteMsg?.text ?? ''}',
            style: TextStyle(
              color: Colors.white.withOpacity(0.6),
              fontSize: ScreenUtil().setSp(20),
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(24)),
        ],
      ),
    );
  }

  /// 消息图片
  List<Widget> _msgImg(bool showImg, String resource, String? layout) {
    logger.v('_msgImg url : $resource');
    var emptyView = [
      SizedBox(width: ScreenUtil().setWidth(0)),
      SizedBox(width: ScreenUtil().setWidth(25)),
    ];

    if (layout == Const.INBOX_LAYOUT_COMMENT_LIKE) {
      return emptyView;
    }

    if (showImg) {
      return [
        SizedBox(width: ScreenUtil().setWidth(30)),
        YBDOpacityBgNetImg(
          // 动态图片和占位图
          iconUrl: resource,
          height: ScreenUtil().setWidth(100),
          width: ScreenUtil().setWidth(100),
          radius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
        ),
      ];
    } else {
      return emptyView;
    }
  }

  void _msgImg5wsJSoyelive(bool showImg, String resource, String? layout) {
    int needCount = 0;
    print('input result:$needCount');
  }
}
