import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// 网络图片显示透明背景色的占位图
class YBDOpacityBgNetImg extends StatelessWidget {
  final String? iconUrl;
  final double height;
  final double width;
  final BorderRadius? radius;
  final BoxFit boxFit;

  Widget? defaultWidget;
  YBDOpacityBgNetImg(
      {this.iconUrl = '',
      this.height = 0,
      this.width = 0,
      this.radius,
      this.boxFit = BoxFit.cover,
      this.defaultWidget});

  @override
  Widget build(BuildContext context) {
    var img;

    if ((iconUrl ?? '').contains('assets/images/')) {
      // 本地图片
      img = Image.asset(
        iconUrl!,
        fit: boxFit,
      );
    } else {
      img = YBDNetworkImage(
        imageUrl: iconUrl ?? '',
        placeholder: (context, url) => defaultWidget ?? Container(color: Colors.white.withOpacity(0.3)),
        errorWidget: (context, url, err) => defaultWidget ?? Container(color: Colors.white.withOpacity(0.3)),
        fit: boxFit,
      );
    }

    return Container(
      // 占位图和实际图片显示的宽高
      constraints: BoxConstraints(maxHeight: height, minHeight: height, maxWidth: width, minWidth: width),
      child: ClipRRect(
        borderRadius: radius ?? BorderRadius.all(Radius.circular(0)),
        child: img,
      ),
    );
  }
  void buildlN2j1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
