import 'dart:convert';
import 'dart:io';
import 'dart:ui' as ui show ParagraphBuilder, PlaceholderAlignment;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oye_tool_package/widget/hight_text_widget.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../common/util/date_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../generated/json/base/json_convert_content.dart';
import '../../../../../module/inbox/db/model_ybd_message.dart';
import '../../../../../module/inbox/entity/message_ybd_list_entity.dart';
import '../../../../../module/inbox/message_ybd_helper.dart';
import '../../../../widget/pop_menu/pop_ybd_menu.dart';
import 'opacity_ybd_bg_net_img.dart';

/// 点击系统消息的回调，参数为 item 的json数据用来判断点击 item 跳转到哪个页面
typedef SystemMsgItemCallback = Function(dynamic);

/// 系统消息列表 item
class YBDSystemMsgListItem extends StatefulWidget {
  /// item 数据
  final YBDMessageModel data;

  /// 删除消息的回调
  final Function onDelete;

  /// 点击 item 的回调
  final SystemMsgItemCallback clickCallback;

  YBDSystemMsgListItem(this.data, this.clickCallback, this.onDelete);

  @override
  YBDSystemMsgListItemState createState() => YBDSystemMsgListItemState();
}

class YBDSystemMsgListItemState extends State<YBDSystemMsgListItem> {
  /// 是否弹出了菜单选项，用来显示系统消息会话的深色背景
  bool _showedPopMenu = false;

  List<String> actions = [
    translate('Copy'),
  ];

  bool showGo = false;
  @override
  Widget build(BuildContext context) {
    var jsonContent;

    try {
      jsonContent = json.decode(widget.data.jsonContent!);
      showGo = jsonContent['content']['click'] != null &&
          (!jsonContent['content']['click']['url'].contains('native'));
    } catch (e) {
      logger.v('decode msg json content error : $e');
    }

    return YBDWPopupMenu(
      menuHeight: ScreenUtil().setWidth(54),
      menuWidth: ScreenUtil().setWidth(100),
      onValueChanged: (int value) {
        logger.v('popup menu changed : $value');
        if (value == 0) {
          logger.v('copied system msg : ${widget.data.text}');
          Clipboard.setData(ClipboardData(text: widget.data.text));
          YBDToastUtil.toast(translate('copied_to_clipboard'));
        } else {
          logger.v('deleted system msg : ${widget.data.text}');
          YBDMessageHelper.deleteSingleChat(
              widget.data.messageId, widget.data.queueId);
          widget.onDelete.call();
        }
      },
      startPress: () {
        setState(() {
          _showedPopMenu = true;
        });
      },
      endPress: () {
        if (_showedPopMenu) {
          setState(() {
            _showedPopMenu = false;
          });
        }
      },
      showAtCenter: true,
      pressType: PressType.longPress,
      actions: actions,
      child: Container(
        color: _showedPopMenu
            ? Colors.black.withOpacity(0.15)
            : Colors.transparent,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              logger.v('clicked system msg item');
              widget.clickCallback(jsonContent);
            },
            child: _messageContent(jsonContent),
          ),
        ),
      ),
    );
  }

  void buildHqHUZoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  Widget _messageContent(jsonContent) {
    String iconUrl = '';
    String title = '';
    YBDMessageListDataPullMessage? messageEntity;

    if (null != jsonContent) {
      messageEntity =
          JsonConvert.fromJsonAsT<YBDMessageListDataPullMessage>(jsonContent);
    }

    iconUrl = messageEntity?.content?.icon ?? '';
    title = messageEntity?.content?.title ?? '';

    return Padding(
      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(20)),
      child: Column(
        children: <Widget>[
          Row(
            children: [
              SizedBox(
                width: ScreenUtil().setWidth(24),
              ),
              YBDOpacityBgNetImg(
                // 系统消息图标
                iconUrl: iconUrl,
                height: ScreenUtil().setWidth(90),
                width: ScreenUtil().setWidth(90),
                radius: BorderRadius.all(
                    Radius.circular(ScreenUtil().setWidth(45))),
              ),
              SizedBox(width: ScreenUtil().setWidth(40)),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          // 消息标题
                          width: ScreenUtil().setWidth(420),
                          child: Text(
                            title.trim(),
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(28),
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Spacer(),
                        Text(
                          // 消息时间
                          YBDDateUtil.timeFromMilliseconds(
                              widget.data.sendTime ?? 0,
                              yearFormat: 'yyyy/MM/dd'),
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(20),
                            color: Colors.white.withOpacity(0.8),
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(24),
                        )
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil().setWidth(12),
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          // 消息内容
                          width: ScreenUtil().setWidth(530),
                          child: Text.rich(
                            TextSpan(
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(24),
                                  color: Colors.white.withOpacity(0.7),
                                  fontWeight: FontWeight.w300,
                                ),
                                children: [
                                  ..._lightTextSpans()!,
                                  /*
                                  TextSpan(
                                    text: widget.data.text ?? '',
//                      overflow: TextOverflow.ellipsis,
//                      maxLines: 10,
                                  ),
                                  */
                                  if (showGo)
                                    TextSpan(
                                        text: "  Go ",
                                        style: TextStyle(
                                            color: Color(0xff7DFAFF))),
                                  if (showGo)
                                    WidgetSpan(
                                      alignment: ui.PlaceholderAlignment.middle,
                                      child: Icon(
                                        Icons.arrow_forward_ios_rounded,
                                        color: Color(0xff7DFAFF),
                                        size: ScreenUtil().setWidth(14),
                                      ),
                                    )
                                ]),
                          ),
                        ),
                      ],
                    ),
                    ..._serviceContent(jsonContent)
                  ],
                ),
              ),
            ],
          ),
          _statusMiniContent(jsonContent),
        ],
      ),
    );
  }

  void _messageContentrZu1coyelive(jsonContent) {
    int needCount = 0;
    print('input result:$needCount');
  }

  List<TextSpan>? _cacheLightTextSpans; // 设置缓存避免重复计算

  /// 给目标字符设置高亮
  List<TextSpan>? _lightTextSpans() {
    // if (_cacheLightTextSpans != null) {
    //   print('22.11.14--_cacheLightTextSpans0000');
    //   return _cacheLightTextSpans;
    // }

    // logger.v("_lightTextspans jsonContent: ${widget.data?.jsonContent}");
    var map = jsonDecode(widget.data.jsonContent ?? "");

    if (map != null &&
        map["content"] != null &&
        map["content"]["attributes"] != null &&
        map["content"]["attributes"]["font"] != null) {
      var fonts = map["content"]["attributes"]["font"];
      logger.v("_lightTextspans jsonContentFonts: $fonts");
      var fontEntitys = (fonts as List).map((v) {
        var e = YBDMessageFontColorEntity().fromJson(v);
        return FontColorTextEntity(
            e.target ?? "",
            ScreenUtil().setSp(e.fontSize ?? 24),
            e.fontWeight!,
            YBDHexColor(e.color));
      }).toList();

      _cacheLightTextSpans = HighlightTextUtil.lightTextEntitysSpans(
        text: widget.data.text ?? '',
        lightEntitys: fontEntitys,
      );
    } else {
      _cacheLightTextSpans = [
        TextSpan(
          text: widget.data.text ?? '',
//                      overflow: TextOverflow.ellipsis,
//                      maxLines: 10,
        )
      ];
    }
    return _cacheLightTextSpans;
  }

  /// 动态缩略内容
  Widget _statusMiniContent(jsonData) {
    return Container();
  }

  void _statusMiniContentQ3xvEoyelive(jsonData) {
    int needCount = 0;
    print('input result:$needCount');
  }

  List<Widget> _serviceContent(jsonData) {
    bool? showSerivice = false;
    // print(widget.data.text + "10101010" + jsonData['content']['layout']);
    if (null != jsonData &&
        jsonData['content']['layout'] == layout_service &&
        jsonData['attributes'] != null) {
      if (jsonData['attributes']['showService'] != null)
        showSerivice = jsonData['attributes']['showService'];
    }

    if (showSerivice!)
      return [
        SizedBox(
          height: ScreenUtil().setWidth(20),
        ),
        GestureDetector(
          onTap: () async {
            String url = Platform.isAndroid
                ? "https://wa.me/${YBDCommonUtil.getRoomOperateInfo().wsaRoman}"
                : "https://api.whatsapp.com/send?phone=${YBDCommonUtil.getRoomOperateInfo().wsaRoman}";

            await launch(url);
            // if (await canLaunch(url)) {
            //   await launch(url);
            // }
          },
          child: Container(
            decoration: BoxDecoration(),
            child: Row(
              children: [
                Text.rich(TextSpan(
                    style: TextStyle(fontSize: ScreenUtil().setSp(22)),
                    children: [
                      TextSpan(
                          text: "${translate('roman_english')}：",
                          style: TextStyle(
                              color: Color(0xffffffff).withOpacity(0.5))),
                      WidgetSpan(
                          child: Image.asset(
                        'assets/images/wsa.webp',
                        width: ScreenUtil().setWidth(36),
                      )),
                      TextSpan(
                          text:
                              "  +${YBDCommonUtil.getRoomOperateInfo().wsaRoman}",
                          style: TextStyle(color: Color(0xFF7DFAFF))),
                    ]))
              ],
            ),
          ),
        ),
        SizedBox(
          height: ScreenUtil().setWidth(20),
        ),
        GestureDetector(
          onTap: () async {
            String url = Platform.isAndroid
                ? "https://wa.me/${YBDCommonUtil.getRoomOperateInfo().wsaEnglish}"
                : "https://api.whatsapp.com/send?phone=${YBDCommonUtil.getRoomOperateInfo().wsaEnglish}";

            await launch(url);
            // if (await canLaunch(url)) {
            //   await launch(url);
            // }
          },
          child: Container(
            decoration: BoxDecoration(),
            child: Row(
              children: [
                Text.rich(TextSpan(
                    style: TextStyle(fontSize: ScreenUtil().setSp(22)),
                    children: [
                      TextSpan(
                          text: "English：",
                          style: TextStyle(
                              color: Color(0xffffffff).withOpacity(0.5))),
                      WidgetSpan(
                          child: Image.asset(
                        'assets/images/wsa.webp',
                        width: ScreenUtil().setWidth(36),
                      )),
                      TextSpan(
                          text:
                              "  +${YBDCommonUtil.getRoomOperateInfo().wsaEnglish}",
                          style: TextStyle(color: Color(0xFF7DFAFF))),
                    ]))
              ],
            ),
          ),
        ),
      ];
    else
      return [];
  }

  void _serviceContentzX8aWoyelive(jsonData) {
    int needCount = 0;
    print('input result:$needCount');
  }
}

const layout_status = 'image_text_status#2';
const layout_service = 'text_level_reach_exclusive';
