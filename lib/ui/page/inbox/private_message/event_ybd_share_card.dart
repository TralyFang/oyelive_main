import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

class YBDEventShareCard extends StatelessWidget {
  String? title, picUrl;

  YBDEventShareCard(this.title, this.picUrl);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(10))), color: Color(0x32ffffff)),
      width: ScreenUtil().setWidth(476),
      child: Column(
        children: [
          SizedBox(
            height: ScreenUtil().setWidth(26),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(22)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  title ?? '',
                  style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                )
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(16),
          ),
          YBDNetworkImage(
            imageUrl: picUrl ?? '',
            fit: BoxFit.cover,
            placeholder: (context, url) => Container(
              width: ScreenUtil().setWidth(432),
              height: ScreenUtil().setWidth(180),
              decoration: BoxDecoration(color: Color(0x44ffffff)),
            ),
            width: ScreenUtil().setWidth(432),
            height: ScreenUtil().setWidth(180),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(15),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(22)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  translate("check_detail"),
                  style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff80FFF9)),
                )
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(15),
          ),
        ],
      ),
    );
  }
  void buildXB6pMoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
