import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/home/entity/event_ybd_center_entity.dart';
import 'package:oyelive_main/ui/page/home/widget/event_center/event_ybd_item.dart';

class YBDEventCenterShareCard extends StatelessWidget {
  final String? title, picUrl, detail;

  const YBDEventCenterShareCard({
    this.title,
    this.picUrl,
    this.detail,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var data = YBDEventCenterDataEventInfos();
    data.eventImg = picUrl;
    data.eventName = title;
    data.eventDetail = detail;
    return Container(
      width: 470.px,
      child: YBDEventItem(data, isInboxItem: true),
    );
  }
  void buildnIs1Woyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
