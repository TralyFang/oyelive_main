import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

class YBDTimeLine extends StatelessWidget {
  int? timeStamp;

  YBDTimeLine(this.timeStamp);
  timeConvert(int? millsecs) {
    if (millsecs == null) {
      return "";
    }

    DateTime nowTime = DateTime.now();
    if (nowTime.year != DateTime.fromMillisecondsSinceEpoch(millsecs).year) {
      return DateFormat("dd MMM yyyy 'at' HH:mm").format(DateTime.fromMillisecondsSinceEpoch(millsecs));
    }
    if (nowTime.day != DateTime.fromMillisecondsSinceEpoch(millsecs).day) {
      return DateFormat("dd MMM 'at' HH:mm").format(DateTime.fromMillisecondsSinceEpoch(millsecs));
    }
    return DateFormat("HH:mm").format(DateTime.fromMillisecondsSinceEpoch(millsecs));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: ScreenUtil().setWidth(40),
        ),
        Text(timeConvert(timeStamp), style: TextStyle(fontSize: ScreenUtil().setSp(22), color: Color(0x99ffffff))),
        SizedBox(
          height: ScreenUtil().setWidth(30),
        ),
      ],
    );
  }
  void buildsNXOVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
