import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/certification_ybd_util.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/db_ybd_config.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/inbox/db/model_ybd_conversation.dart';
import '../../../../module/inbox/db/model_ybd_message.dart';
import '../../../../module/inbox/entity/send_ybd_entity.dart';
import '../../../../module/inbox/message_ybd_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/colored_ybd_safe_area.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../room/room_ybd_helper.dart';
import 'item_ybd_message.dart';

class YBDPrivateMessagePage extends StatefulWidget {
  int userId;

  String queueId;

  YBDPrivateMessagePage(this.userId, this.queueId);

  @override
  YBDPrivateMessagePageState createState() => new YBDPrivateMessagePageState();
}

class YBDPrivateMessagePageState extends BaseState<YBDPrivateMessagePage> {
  YBDUserInfo? othersInfo;
  String? name = "", avatar = "";
  List<YBDMessageModel>? messageList;

  int page = 0;

  TextEditingController textEditingController = new TextEditingController();
  FocusNode _focusNode = new FocusNode();
  RefreshController refreshController = new RefreshController();
  late SelectBloc bloc;

  addMessageRemoveReplicate(newMessage) {
    if (messageList == null) {
      messageList = [];
    }
    for (YBDMessageModel message in newMessage) {
      bool isReplace = false;
      for (int index = 0; index < messageList!.length; index++) {
        if (messageList![index].messageId == message.messageId || messageList![index].requestId == message.requestId) {
          messageList![index] = message;
          isReplace = true;
          break;
        }
      }
      if (!isReplace) {
        messageList!.add(message);
      }
    }
    messageList!.sort((a, b) => b.sendTime!.compareTo(a.sendTime!));
  }

  showTime(int index, List<YBDMessageModel> data) {
    if (index == data.length - 1 || data.length < 2) {
      return true;
    }
    DateTime earlyTime = DateTime.fromMillisecondsSinceEpoch(data[index].sendTime!);
    DateTime laterTime = DateTime.fromMillisecondsSinceEpoch(data[index + 1].sendTime!);
    return earlyTime.difference(laterTime).inMinutes >= 1 || earlyTime.minute != laterTime.minute;
  }

  loadMore() async {
    var data = await YBDMessageModel().sqlSelect(
        where:
            "(sender_id = \"${widget.userId}\" or receiver_id = \"${widget.userId}\" or queue_id = \"${widget.queueId}\") and category =\"friend\" ",
        orderBy: "send_time DESC",
        limit: YBDMessageHelper.maxPageSize,
        offset: YBDMessageHelper.maxPageSize * (++page));
    if (data.length == 0) {
      refreshController.loadNoData();
    } else {
      refreshController.loadComplete();

      addMessageRemoveReplicate(data);
      setState(() {});
    }
  }

  sendMessage({String? textMessage, bool isJson: false, String? contentType}) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      YBDToastUtil.toast(translate('no_network'));
      return;
    }
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();

    if (textEditingController.text.isEmpty) {
      _focusNode.unfocus();
    }

    if (textEditingController.text.isNotEmpty || textMessage != null) {
      YBDMessageModel message = YBDMessageHelper.sendMessageNow(
          context,
          YBDSendEntity(
              content: YBDContent(
                  text: isJson ? '' : (textMessage ?? textEditingController.text),
                  isJson: isJson,
                  jsonText: isJson ? textMessage : ''),
              contentType: contentType ?? YBDContentType.text,
              category: Category.friend,
              toId: widget.userId),
          userInfo!.id);
      messageList = [message]..addAll(messageList!);
      textEditingController.clear();
      setState(() {});

      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
          location: "private_message_page", itemID: '${widget.userId}', itemName: 'send_message'));
    }
  }

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff32249D),
      appBar: AppBar(
        title: StreamBuilder<Map<int?, YBDUserCertificationRecordCertificationInfos?>>(
            stream: YBDCertificationUtil.getInstance()!.certificationCacheOutStream,
            initialData: YBDCertificationUtil.getInstance()!.certificationCache,
            builder: (context, snapshot) {
              bool hasCer = false;
              YBDUserCertificationRecordCertificationInfos? info;
              if (snapshot.hasData) {
                int currentUserId = widget.userId;
                info = snapshot.data![currentUserId];
                hasCer = info != null;
              }
              return Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  if (hasCer)
                    SizedBox(
                      width: ScreenUtil().setWidth(38),
                    ),
                  ConstrainedBox(
                    child: Text(
                      name!,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: YBDTPStyle.spNav,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.fade,
                    ),
                    constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(200)),
                  ),
                  if (hasCer)
                    SizedBox(
                      width: ScreenUtil().setWidth(8),
                    ),
                  if (hasCer)
                    YBDNetworkImage(
                      imageUrl: info!.icon!,
                      width: ScreenUtil().setWidth(36),
                    )
                ],
              );
            }),
        centerTitle: true,
        elevation: 0,
        backgroundColor: YBDTPStyle.heliotrope,
        leading: Container(
          width: ScreenUtil().setWidth(88),
          child: TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            style: TextButton.styleFrom(padding: EdgeInsets.all(ScreenUtil().setWidth(20))),
            // padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
          ),
        ),
        actions: <Widget>[
          YBDMessageHelper.READING_CONVERSATION != null
              ? GestureDetector(
                  onTap: () {
                    YBDNavigatorHelper.navigateTo(
                      context,
                      YBDNavigatorHelper.inbox_setting +
                          "/${YBDMessageHelper.READING_CONVERSATION!.senderGender}/${widget.userId}",
                    );
                  },
                  child: Container(
                    width: ScreenUtil().setWidth(48),
                    height: ScreenUtil().setWidth(48),
                    decoration: BoxDecoration(),
                    child: Icon(
                      Icons.more_vert,
                      color: Colors.white,
                      size: ScreenUtil().setWidth(38),
                    ),
                  ),
                )
              : Container(),
          SizedBox(
            width: ScreenUtil().setWidth(24),
          )
        ],
      ),
      body: YBDColoredSafeArea(
        child: Container(
          decoration: YBDTPStyle.gradientDecoration,
          child: Column(
            children: [
              Expanded(
                child: messageList == null
                    ? YBDLoadingCircle()
                    : SmartRefresher(
                        enablePullDown: false,
                        enablePullUp: messageList!.length > 40,
                        onLoading: loadMore,
                        controller: refreshController,
                        child: ListView.builder(
                            shrinkWrap: true,
                            reverse: true,
                            padding: EdgeInsets.all(0),
                            itemBuilder: (_, index) => YBDMessageItem(
                                  data: messageList![index],
                                  others: othersInfo,
                                  showTime: showTime(index, messageList!),
                                  onDelete: () {
                                    messageList!.removeAt(index);
                                  },
                                  onClickResend: () {
                                    bool isJson = (messageList![index].text == null || messageList![index].text!.isEmpty);
                                    sendMessage(
                                        textMessage: isJson
                                            ? json.encode(json.decode(messageList![index].jsonContent!)['content'])
                                            : messageList![index].text,
                                        isJson: isJson,
                                        contentType: isJson ? YBDContentType.cell : YBDContentType.text);

                                    YBDMessageHelper.deleteMessageModel(messageList![index]);
                                    messageList!.removeAt(index);
                                  },
                                ),
                            itemCount: messageList!.length),
                      ),
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setWidth(20)),
                child: Row(
                  children: [
                    SizedBox(
                      width: ScreenUtil().setWidth(24),
                    ),
                    Container(
                      width: ScreenUtil().setWidth(600),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40)))),
                      child: TextField(
                        controller: textEditingController,
                        focusNode: _focusNode,
                        scrollPadding: EdgeInsets.all(0),
                        style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.black),
                        maxLength: 150,
                        maxLines: null,
                        textInputAction: Platform.isIOS ? TextInputAction.newline : TextInputAction.none,
                        decoration: InputDecoration(
                            isDense: true,
                            counterText: '',
                            hintText: translate('type_hint'),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: ScreenUtil().setWidth(20), horizontal: ScreenUtil().setWidth(36)),
                            border: InputBorder.none),
                      ),
                    ),
                    Container(
                      width: ScreenUtil().setWidth(90),
                      height: ScreenUtil().setWidth(90),
                      child: TextButton(
                        style: TextButton.styleFrom(padding: EdgeInsets.all(ScreenUtil().setWidth(0))),
                        // padding: EdgeInsets.all(0),
                        child: Icon(
                          Icons.send,
                          color: Colors.white,
                          size: ScreenUtil().setWidth(48),
                        ),
                        onPressed: () {
                          sendMessage();
                        },
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: GestureDetector(
        onTap: () {
          print("click click clikc");
          YBDRoomHelper.enterRoom(context, widget.userId, location: 'private_message_page');
        },
        child: Container(
          height: ScreenUtil().setWidth(200),
          padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(65)),
          child: Transform.translate(
            offset: Offset(16, ScreenUtil().setWidth(70)),
            child: Container(
//        margin: EdgeInsets.only(right: -16),
              width: ScreenUtil().setWidth(200),
              height: ScreenUtil().setWidth(70),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.horizontal(left: Radius.circular(ScreenUtil().setWidth(35))),
                  color: Color(0x65000000)),
//        padding: EdgeInsets.only(bottom: ),
              child: Row(
                children: [
                  SizedBox(
                    width: ScreenUtil().setWidth(5),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(60),
                    height: ScreenUtil().setWidth(60),
                    child: ClipOval(
                      child: YBDNetworkImage(
                        imageUrl: YBDImageUtil.avatar(context, avatar, widget.userId, scene: 'A'),
                        fit: BoxFit.cover,
                        placeholder: (context, url) => YBDResourcePathUtil.defaultMaleImage(male: othersInfo?.sex == 2),
                        width: ScreenUtil().setWidth(60),
                        height: ScreenUtil().setWidth(60),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(10),
                  ),
                  Text(
                    translate("Room"),
                    style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(5),
                  ),
                  Image.asset(
                    "assets/images/icon_little_mic.webp",
                    width: ScreenUtil().setWidth(19),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }
  void myBuildvZLHvoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  queryUserInfo() async {
    YBDUserInfo? userInfo = await ApiHelper.queryUserInfo(context, widget.userId);
    if (userInfo != null) {
      othersInfo = userInfo;
      avatar = othersInfo!.headimg;
      name = othersInfo!.nickname;
      if (mounted) setState(() {});

//      YBDMessageHelper.tempConversation(userInfo);
    }

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE,
        location: "private_message_page", itemID: '${othersInfo!.id}', itemName: othersInfo!.nickname ?? 'null'));
  }

  queryConversation(String userId) async {
    YBDConversationModel? conversationModel = await YBDMessageHelper.getConversationByUserId(userId);
    if (conversationModel != null) {
      YBDMessageHelper.setReading(context, conversationModel);
      if (mounted) setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();

    queryUserInfo();

    Future.delayed(Duration(seconds: 3), () {
      if (mounted) YBDMessageHelper.checkSingleContact(context, widget.userId);
    });
    try {
      bloc = SelectBloc(
          database: messageDb!,
          where:
              "(sender_id = \"${widget.userId}\" or receiver_id = \"${widget.userId}\" or queue_id = \"${widget.queueId}\") and category =\"friend\" ",
          table: YBDMessageModel.messageTableName,
          orderBy: "send_time DESC",
          reactive: true,
          limit: YBDMessageHelper.maxPageSize,
          offset: 0);
      bloc.items.listen((data) {
        if (data != null && mounted) {
          print("update from dataBase");
          List<YBDMessageModel> updateMessage = List.generate(data.length, (index) => YBDMessageModel().fromDb(data[index]));
          addMessageRemoveReplicate(updateMessage);

          if (YBDMessageHelper.READING_CONVERSATION == null && mounted) {
            queryConversation(widget.userId.toString());
          }
          setState(() {});
        }
      });
    } catch (e) {
      print(e);
    }

    queryConversation(widget.userId.toString());

    YBDCertificationUtil.getInstance()!.addUserCache([widget.userId]);
  }
  void initState6ULqToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    YBDMessageHelper.quitReading(context);
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDPrivateMessagePage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidget24432oyelive(YBDPrivateMessagePage oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
