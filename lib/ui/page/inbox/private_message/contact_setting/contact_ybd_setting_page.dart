import 'dart:async';

import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/icon_ybd_gender.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/certification_ybd_util.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';

import '../../../../../base/base_ybd_state.dart';
import '../../../../../common/util/image_ybd_util.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../common/util/sp_ybd_util.dart';
import '../../../../../common/util/toast_ybd_util.dart';
import '../../../../../module/api_ybd_helper.dart';
import '../../../../../module/inbox/inbox_ybd_api_helper.dart';
import '../../../../../module/inbox/message_ybd_helper.dart';
import '../../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../widget/level_ybd_tag.dart';
import '../../../../widget/profile_ybd_report_dialog.dart';
import '../../../../widget/round_ybd_avatar.dart';
import '../../../status/follow_ybd_button.dart';

class YBDContactSettingPage extends StatefulWidget {
  int gender, userId;

  YBDContactSettingPage(this.gender, this.userId);

  @override
  YBDContactSettingPageState createState() => new YBDContactSettingPageState();
}

class YBDContactSettingPageState extends BaseState<YBDContactSettingPage> {
  bool? noDisturb = YBDMessageHelper.READING_CONVERSATION!.isNotDisturb,
      topChat = YBDMessageHelper.READING_CONVERSATION!.isAtTop,
      blackList = YBDMessageHelper.READING_CONVERSATION!.isBlocked;
  bool isFollowed = true;

  YBDUserInfo? othersInfo;

  getLine(String title, {bool? switchSign, Function? onTap}) {
    return GestureDetector(
      onTap: onTap as void Function()? ?? null,
      child: Container(
        height: ScreenUtil().setWidth(96),
        padding: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
        decoration: BoxDecoration(),
        child: Row(
          children: [
            Text(
              title,
              style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white),
            ),
            Spacer(),
            switchSign != null
                ? Switch(
                    inactiveThumbColor: Color(0xffC5C5C5),
                    value: switchSign,
                    activeTrackColor: Color(0xffB6FAFA),
                    inactiveTrackColor: Colors.white,
                    activeColor: Color(0xff1CD1D2),
                    onChanged: (s) {
                      onTap?.call();
                    })
                : Container()
          ],
        ),
      ),
    );
  }

  /// 检查网络连接状态
  Future<bool> _checkNetWork() async {
    var connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.none) {
      YBDToastUtil.toast(translate("no_network"));
      return false;
    } else {
      logger.v("network is available");
      return true;
    }
  }
  void _checkNetWork3jUUJoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _getMaxClip(String text) {
    if (text.length > 12) return text.substring(0, 12) + "...";
    return text;
  }

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Container(
            width: ScreenUtil().setWidth(88),
            child: TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              style: TextButton.styleFrom(padding: EdgeInsets.all(ScreenUtil().setWidth(20))),
              // padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
            ),
          ),
          title: Text(
            translate('inbox_setting'),
            style: TextStyle(
              color: Colors.white,
              fontSize: YBDTPStyle.spNav,
            ),
          ),
          centerTitle: true,
          elevation: 0,
          backgroundColor: YBDTPStyle.heliotrope,
        ),
        body: Container(
          decoration: YBDTPStyle.gradientDecoration,
          child: Column(
            children: [
              SizedBox(
                height: ScreenUtil().setWidth(40),
              ),
              StreamBuilder<Map<int?, YBDUserCertificationRecordCertificationInfos?>>(
                stream: YBDCertificationUtil.getInstance()!.certificationCacheOutStream,
                initialData: YBDCertificationUtil.getInstance()!.certificationCache,
                builder: (context, snapshot) {
                  bool hasCer = false;
                  YBDUserCertificationRecordCertificationInfos? info;
                  if (snapshot.hasData) {
                    int currentUserId = widget.userId;
                    info = snapshot.data![currentUserId];
                    hasCer = info != null;
                  }
                  bool isSecondLineDisappear = hasCer && info!.hidGender == 1 && info.hidUserLevel == 1;
                  return Container(
                    padding: EdgeInsets.only(top: ScreenUtil().setWidth(10)),
                    height: ScreenUtil().setWidth(130),
                    child: Row(
                      children: [
                        SizedBox(
                          width: ScreenUtil().setWidth(25),
                        ),
                        YBDRoundAvatar(
                          YBDImageUtil.avatar(context, YBDMessageHelper.READING_CONVERSATION?.avatar ?? '', widget.userId,
                              scene: 'A'),
                          avatarWidth: 90,
                          userId: YBDMessageHelper.READING_CONVERSATION?.senderID,
                          sex: widget.gender,
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(30),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(360),
                          height: ScreenUtil().setWidth(86),
                          child: Stack(
                            children: [
                              Align(
                                child: Row(
                                  children: [
                                    Container(
                                        child: Text(
                                          _getMaxClip(YBDMessageHelper.READING_CONVERSATION?.title?.removeBlankSpace() ?? '') ??
                                              '',
                                          style: TextStyle(fontSize: ScreenUtil().setSp(32), color: Colors.white),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          softWrap: true,
                                        ),
                                        constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(300))),
                                    if (!hasCer || info?.hidGender == 0)
                                      YBDGenderIcon(
                                        widget.gender,
                                        padding: EdgeInsets.only(left: ScreenUtil().setWidth(8)),
                                      ),
                                    if (hasCer)
                                      SizedBox(
                                        width: ScreenUtil().setWidth(4),
                                      ),
                                    if (hasCer)
                                      YBDNetworkImage(
                                        imageUrl: info!.icon!,
                                        width: ScreenUtil().setWidth(36),
                                      )
                                  ],
                                  mainAxisSize: MainAxisSize.min,
                                ),
                                alignment: isSecondLineDisappear ? Alignment.centerLeft : Alignment.topLeft,
                              ),
                              if (!isSecondLineDisappear)
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Row(
                                    children: <Widget>[
                                      YBDVipIcon(
                                        othersInfo?.vipIcon ?? '',
                                        padding: EdgeInsets.only(right: ScreenUtil().setWidth(2)),
                                      ),

                                      othersInfo != null && (!hasCer || info?.hidUserLevel == 0)
                                          ? YBDLevelTag(othersInfo!.level)
                                          : Container(
                                              height: 1,
                                            ),
//                                widget.data.talentLevel != null ? YBDRoomLevelTag(widget.data.talentLevel) : Container(),
                                    ],
                                  ),
                                )

                              //0未知，1女2男
                            ],
                          ),
                        ),
                        Expanded(child: Container()),
                        YBDFollowButton(
                          isFollowed,
                          YBDMessageHelper.READING_CONVERSATION?.senderID ?? '',
                          "inbox_setting",
                          key: new Key("inbox_setting"),
                          onChange: (x) {
                            isFollowed = x;
                            setState(() {});
                          },
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(25),
                        ),
                      ],
                    ),
                  );
                },
              ),
              Container(
                height: ScreenUtil().setWidth(1),
                color: Color(0x22EAEAEA),
              ),
//               "do_not_disturb": "Don't disturb the news",
//  "top_chat": "Top chat",
//  "blacklist": "Blacklist",
//  "clear_chat": "Clear Chat"
              getLine(translate("do_not_disturb"), switchSign: noDisturb, onTap: () async {
                showLockDialog();
                bool success = await YBDMessageHelper.setDisturbContact(context,
                    YBDMessageHelper.READING_CONVERSATION!.senderID, noDisturb! ? EditAction.Delete : EditAction.Add);
                dismissLockDialog();
                if (success) {
                  setState(() {
                    noDisturb = !noDisturb!;
                    YBDMessageHelper.READING_CONVERSATION!.isNotDisturb = noDisturb;
                  });
                } else {
                  if (await _checkNetWork()) YBDToastUtil.toast(translate('failed'));
                }
              }),
              Container(
                height: ScreenUtil().setWidth(1),
                color: Color(0x22EAEAEA),
              ),
              getLine(translate("stick_to_top"), switchSign: topChat, onTap: () {
                YBDMessageHelper.setTopChat(
                    YBDMessageHelper.READING_CONVERSATION!.queueID, !YBDMessageHelper.READING_CONVERSATION!.isAtTop!);

                setState(() {
                  topChat = !topChat!;
                });
              }),
              Container(
                height: ScreenUtil().setWidth(1),
                color: Color(0x22EAEAEA),
              ),
              getLine(translate("inbox_blocklist"), switchSign: blackList, onTap: () async {
                showLockDialog();
                bool success = await YBDMessageHelper.blockContact(context, YBDMessageHelper.READING_CONVERSATION!.senderID,
                    blackList! ? EditAction.Delete : EditAction.Add);
                dismissLockDialog();
                if (success) {
                  setState(() {
                    blackList = !blackList!;
                    YBDMessageHelper.READING_CONVERSATION!.isBlocked = blackList;
                  });
                } else {
                  if (await _checkNetWork()) YBDToastUtil.toast(translate('failed'));
                }
              }),
              Container(
                height: ScreenUtil().setWidth(1),
                color: Color(0x22EAEAEA),
              ),
              getLine(translate("report"), onTap: () {
                showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (context) {
                      return YBDProfileReportDialog((String reportTitle) async {
                        logger.v('report dialog ok button callback : $reportTitle');
                        // 隐藏举报弹框
                        Navigator.pop(context);
                        String? nickName = '';
                        if (null != YBDMessageHelper.READING_CONVERSATION?.title) {
                          nickName = YBDMessageHelper.READING_CONVERSATION?.title;
                        }
                        bool result = await ApiHelper.complain(
                            context,
                            'user',
                            YBDMessageHelper.READING_CONVERSATION?.senderID,
                            '$reportTitle|${YBDMessageHelper.READING_CONVERSATION?.senderID}|$nickName');
                        if (result) {
                          YBDToastUtil.toast('Report successful, under review');
                        } else {
                          YBDToastUtil.toast('The system is busy, please try again later');
                        }
                      }, () {
                        logger.v('report dialog cancel button callback');
                        // 隐藏举报弹框
                        Navigator.pop(context);
                      });
                    });
              }),
              Container(
                height: ScreenUtil().setWidth(1),
                color: Color(0x22EAEAEA),
              ),
              getLine(translate("clear_chat"), onTap: () async {
                await YBDMessageHelper.deleteChatHistory(
                    YBDMessageHelper.READING_CONVERSATION!.queueID, YBDMessageHelper.READING_CONVERSATION!.senderID,
                    isFriend: true);
                Navigator.pop(context);
                Navigator.pop(context);
              }),
              Container(
                height: ScreenUtil().setWidth(1),
                color: Color(0x22EAEAEA),
              ),
            ],
          ),
        ));
  }
  void myBuildMPRU4oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  queryIsFollowed() async {
    YBDUserInfo mine = await YBDSPUtil.getUserInfo() ?? YBDUserInfo();
    isFollowed = await ApiHelper.queryConcern(context, mine.id?.toString(), widget.userId.toString());
    setState(() {});
  }

  queryUserInfo() async {
    YBDUserInfo? userInfo = await ApiHelper.queryUserInfo(context, widget.userId);
    if (userInfo != null) {
      othersInfo = userInfo;

      setState(() {});

//      YBDMessageHelper.tempConversation(userInfo);
    }
  }

  @override
  void initState() {
    super.initState();
    queryIsFollowed();
    queryUserInfo();
  }
  void initStatekplWloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDContactSettingPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
  void didChangeDependencies2o5Mmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
