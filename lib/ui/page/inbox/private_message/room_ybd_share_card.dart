import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';

class YBDRoomShareCard extends StatelessWidget {
  YBDRoomInfo roomInfo;
  bool isTpgo;
  YBDRoomShareCard(this.roomInfo, this.isTpgo);

  @override
  Widget build(BuildContext context) {
    return new Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(10))), color: Color(0x32ffffff)),
      width: ScreenUtil().setWidth(476),
      height: ScreenUtil().setWidth(170),
      child: Row(
        children: [
          SizedBox(
            width: ScreenUtil().setWidth(14),
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
                color: Color(0x32ffffff),
                image: DecorationImage(
                    image: (isTpgo
                        ? AssetImage(YBDGameResource.assetPadding("gamble", isWebp: true, need2x: false))
                        : NetworkImage(roomInfo.roomimg ?? "")) as ImageProvider<Object>,
                    fit: BoxFit.cover)),
            width: ScreenUtil().setWidth(142),
            height: ScreenUtil().setWidth(142),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(14),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  (isTpgo ? "[🔥TP-GO🔥] ${roomInfo.nickname}" : roomInfo.nickname) ?? "",
                  style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                // SizedBox(
                //   height: ScreenUtil().setWidth(10),
                // ),
                Text(
                  roomInfo.roomMark ?? "",
                  style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                // SizedBox(
                //   height: ScreenUtil().setWidth(26),
                // ),
                Text(
                  translate('room_share_hint'),
                  style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff80FFF9)),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void buildi8SbXoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
