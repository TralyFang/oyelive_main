import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_lobby/enter_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/inbox/private_message/event_ybd_center_share_card.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:redux/redux.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/inbox/db/model_ybd_message.dart';
import '../../../../module/inbox/message_ybd_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/pop_menu/pop_ybd_menu.dart';
import '../../../widget/round_ybd_avatar.dart';
import '../../profile/my_profile/badge_ybd_page.dart';
import 'event_ybd_share_card.dart';
import 'item_ybd_timeline.dart';
import 'room_ybd_share_card.dart';

class YBDMessageItem extends StatefulWidget {
  YBDUserInfo? others;
  YBDMessageModel? data;
  bool? showTime;

  String? avatar;

  Function? onDelete;
  Function? onClickResend;

  YBDMessageItem(
      {this.others,
      this.data,
      this.showTime,
      this.avatar,
      this.onDelete,
      this.onClickResend});

  @override
  YBDMessageItemState createState() => new YBDMessageItemState();
}

class YBDMessageItemState extends BaseState<YBDMessageItem> {
  copyMessage() {
    Clipboard.setData(ClipboardData(text: widget.data!.text));
    YBDToastUtil.toast(translate('copied_to_clipboard'));
  }

  final List<String> actions = [
    translate('Copy'),
    translate('delete'),
  ];

  getText() {
    // 注释这部分代码修复房间给全部好友分享显示 unsupport messsage 的问题
    logger.v('===share get text ${widget.data!.jsonContent}');
    if (widget.data!.text == null || widget.data!.text!.isEmpty) {
      logger.v('===share get text return unSupport');
      return translate("not_support");
    }
    return widget.data!.text;
  }

  getBadges(Map map) {
    return Container(
      width: ScreenUtil().setWidth(358),
      height: ScreenUtil().setWidth(290),
      child: Stack(
        children: [
          Positioned(
              child: Container(
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: Image.asset(
                      'assets/images/profile/badge_show_bottom.webp'))),
          Column(
            children: [
              Container(
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: map['image'].toString().endsWith('.svga')
                      ? YBDBadgeAnimationView(
                          ScreenUtil().setWidth(209),
                          url: map['image'].toString(),
                        )
                      : YBDNetworkImage(
                          imageUrl: map['image'].toString(),
                          width: ScreenUtil().setWidth(209),
                          height: ScreenUtil().setWidth(209),
                          fit: BoxFit.cover,
                        )),
              SizedBox(
                height: ScreenUtil().setHeight(20),
              ),
              Text(
                'I have got the Welsh ${map['title'].toString()} Badge',
                style: TextStyle(
                    color: Colors.white, fontSize: ScreenUtil().setSp(20)),
              )
            ],
          )
        ],
      ),
    );
  }

  /// 消息内容widget
  getContent(bool isSelf) {
    logger.v('===share get content');
    if (widget.data!.text!.isEmpty) {
      if (widget.data!.jsonContent != null) {
        var jsonData = json.decode(widget.data!.jsonContent!);
        Map? content;
        if (jsonData['content'] is String) {
          content = json.decode(jsonData['content']);
        } else {
          content = jsonData['content'];
        }

        log("xxxxxxxxxxxxxxxxxxxx$content===$jsonData");
        if (content!['layout'] != null && content['attributes'] != null) {
          switch (content['layout']) {
            // case Const.SHARE_STATUS:
            //   YBDStatusInfo? statusInfo = YBDStatusInfo().fromJson(content['attributes']);
            //   return _shareStatusItem(statusInfo, jsonData);
            case Const.SHAERE_GAME_ROOM:
              logger.v('===share GAME room');
              return GestureDetector(
                onTap: () {
                  YBDNavigatorHelper.onTapShare(context, jsonData);
                },
                child: gameMessageItem(content as Map<String, dynamic>, false),
              );
              break;
            case Const.SHAERE_TPGO_ROOM:
              logger.v('===share tpgo room');
              return GestureDetector(
                onTap: () {
                  // YBDNavigatorHelper.onTapShare(context, jsonData);
                  //默认自己的，如果没有就进自己房间，相当于创建
                  String? roomId = YBDUserUtil.getUserIdSync.toString();
                  if (content!["attributes"] is Map<String, dynamic>) {
                    roomId = content["attributes"]["roomId"];
                  }
                  YBDDialogUtil.showLoading(context);

                  print("sdjoisjdois$roomId ${content["attributes"]}");
                  YBDEnterHelper().connectSocketNjoin(
                      GameConst.GAME_SUB_TPGO,
                      roomId.toString(),
                      GameConst.matchTypePerson, onSuccess: () {
                    YBDDialogUtil.hideLoading(context);
                  });
                },
                child: gameMessageItem(content as Map<String, dynamic>, true),
              );
              break;
            case Const.SHARE_ROOM:
              logger.v('===share share room');
              var roomInfo = YBDRoomInfo().fromJson(content['attributes']);
              roomInfo.roomMark = translate('room_share_info');
              return GestureDetector(
                onTap: () {
                  YBDNavigatorHelper.onTapShare(context, jsonData);
                },
                child: YBDRoomShareCard(roomInfo, false),
              );
              break;
            case Const.SHARE_EVENT:
              return GestureDetector(
                onTap: () {
                  YBDNavigatorHelper.onTapShare(context, jsonData);
                },
                child: YBDEventShareCard(
                  content['attributes']['adName'],
                  content['attributes']['adImg'],
                ),
              );
              break;
            case Const.SHARE_EVENT_CENTER:
              return GestureDetector(
                onTap: () {
                  YBDNavigatorHelper.onTapShare(context, jsonData);
                },
                child: YBDEventCenterShareCard(
                  title: content['attributes']['adName'],
                  picUrl: content['attributes']['adImg'],
                  detail: content['attributes']['adDetail'],
                ),
              );
              break;
            case Const.Share_Badges:
              return getBadges(json.decode(content['attributes'].toString()));
              break;
          }
        }
      }
    }
    return Container(
      constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(400)),
      decoration: isSelf
          ? BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color(0xff5E95E7),
                  Color(0xff48CBCD),
                ],
              ),
              borderRadius:
                  BorderRadius.all(Radius.circular(ScreenUtil().setWidth(10))))
          : BoxDecoration(
              color: Color(0xffF3F3F3),
              borderRadius:
                  BorderRadius.all(Radius.circular(ScreenUtil().setWidth(10)))),
      padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
      child: Linkify(
        text: getText(),
        softWrap: true,
        maxLines: null,
        style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            color: isSelf ? Colors.white : Colors.black),
        linkStyle: TextStyle(color: Color(0xff1830F9)),
        onOpen: (link) async {
          if (await canLaunch(link.url)) {
            await launch(link.url);
          } else {
            throw 'Could not launch $link';
          }
        },
      ),
    );
  }

  /// 游戏房分享消息
  Widget gameMessageItem(Map<String, dynamic> jsonContent, bool isTpgo) {
    String? roomImg = "";
    String? content = "";
    if (jsonContent["attributes"] is Map<String, dynamic>) {
      roomImg = jsonContent["attributes"]["roomimg"];
      content = jsonContent["attributes"]["content"];
    }

    var roomInfo = YBDRoomInfo()
      ..nickname = jsonContent["title"]
      ..roomimg = roomImg
      ..roomMark = content;

    return YBDRoomShareCard(roomInfo, isTpgo);
  }

  void gameMessageItemYu1Bvoyelive(
      Map<String, dynamic> jsonContent, bool isTpgo) {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  Widget myBuild(BuildContext context) {
    try {
      var jsonData = json.decode(widget.data!.jsonContent!);
      if (jsonData != null && jsonData['content'] != null) {
        if (jsonData['content']['layout'] == Const.FRIEND_GREETING) {
          return Column(
            children: <Widget>[
              widget.showTime!
                  ? YBDTimeLine(widget.data!.sendTime)
                  : SizedBox(
                      height: ScreenUtil().setWidth(30),
                    ),
              Padding(
                padding:
                    EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(10)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      translate('friend_greeting'),
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(22),
                          color: Color(0x99ffffff)),
                    ),
                  ],
                ),
              ),
            ],
          );
        }
      }
    } catch (e) {
      print(e);
    }
    Store<YBDAppState> store =
        YBDCommonUtil.storeFromContext(context: context)!;
    bool isSelf = widget.data!.senderID == store.state.bean!.id.toString();

    var avatar;
    if (isSelf) {
      avatar = YBDRoundAvatar(
        store.state.bean!.headimg,
        avatarWidth: 70,
        userId: store.state.bean!.id,
        sex: store.state.bean!.sex,
        scene: "A",
      );
    } else {
      bool needNav = true;

      if (null == widget.others?.id) {
        logger.v('user id is null');
        needNav = false;
      }

      avatar = YBDRoundAvatar(
        widget.others?.headimg ?? widget.avatar,
        avatarWidth: 70,
        userId: widget.others?.id ?? "",
        needNavigation: needNav,
        sex: widget.others?.sex,
        scene: "A",
      );
    }

    /// 自己消息体
    var myMessage = YBDWPopupMenu(
        menuHeight: ScreenUtil().setWidth(54),
        menuWidth: ScreenUtil().setWidth(186),
        onValueChanged: (int value) {
          if (value == 0) {
            copyMessage();
          } else {
            YBDMessageHelper.deleteMessageModel(widget.data!);
            widget.onDelete?.call();
          }
        },
        showAtCenter: true,
        pressType: PressType.longPress,
        actions: actions,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            widget.data!.messageStatus == MessageStatus.send_failed ||
                    widget.data!.messageStatus == MessageStatus.reject ||
                    widget.data!.messageStatus == MessageStatus.blocking
                ? GestureDetector(
                    onTap: () {
                      widget.onClickResend?.call();
                    },
                    child: Container(
                      width: ScreenUtil().setWidth(60),
                      alignment: Alignment.centerRight,
                      padding:
                          EdgeInsets.only(right: ScreenUtil().setWidth(30)),
                      child: Icon(
                        Icons.error,
                        color: Color(0xffEC4A99),
                      ),
                    ),
                  )
                : Container(
                    width: ScreenUtil().setWidth(60),
                  ),
            getContent(isSelf),
          ],
        ));

    /// 别人的消息体
    var othersMessage = YBDWPopupMenu(
        menuHeight: ScreenUtil().setWidth(54),
        menuWidth: ScreenUtil().setWidth(186),
        onValueChanged: (int value) {
          if (value == 0) {
            copyMessage();
          } else {
            YBDMessageHelper.deleteMessageModel(widget.data!);
            widget.onDelete?.call();
          }
        },
        showAtCenter: true,
        pressType: PressType.longPress,
        actions: actions,
        child: getContent(isSelf));

    var messageContent;
    if (isSelf) {
      /// 自己发的信息在右边👉
      messageContent = Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: ScreenUtil().setWidth(60),
          ),
          Expanded(child: myMessage),
          SizedBox(
            width: ScreenUtil().setWidth(20),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(6)),
            child: avatar,
          ),
          SizedBox(
            width: ScreenUtil().setWidth(25),
          ),
        ],
      );
    } else {
      /// 别人发的信息在左边👈
      messageContent = Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: ScreenUtil().setWidth(25),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(6)),
            child: avatar,
          ),
          SizedBox(
            width: ScreenUtil().setWidth(20),
          ),
          Expanded(
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: othersMessage,
                )
              ],
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(60),
          ),
        ],
      );
    }

    return Column(
      children: [
        widget.showTime!
            ? YBDTimeLine(widget.data!.sendTime)
            : SizedBox(
                height: ScreenUtil().setWidth(30),
              ),
        messageContent,
        widget.data!.messageStatus == MessageStatus.reject
            ? Padding(
                padding: EdgeInsets.only(
                    top: ScreenUtil().setWidth(30),
                    bottom: ScreenUtil().setWidth(20)),
                child: Text(
                  translate("block_message"),
                  style: TextStyle(color: Color(0x99ffffff)),
                ),
              )
            : Container()
      ],
    );
  }

  void myBuildjxiUVoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
