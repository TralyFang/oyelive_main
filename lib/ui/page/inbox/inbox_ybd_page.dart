import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/inbox/empty_ybd_view.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/db_ybd_config.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../module/inbox/db/model_ybd_conversation.dart';
import '../../../module/inbox/queue_ybd_sync.dart';
import '../../widget/loading_ybd_circle.dart';
import 'item_ybd_conversation.dart';

class YBDInboxPage extends StatefulWidget {
  @override
  YBDInboxPageState createState() => new YBDInboxPageState();
}

class YBDInboxPageState extends BaseState<YBDInboxPage> {
  SelectBloc? bloc;

  bool isEmptyConversation = false;

  List<YBDConversationModel> sortConversationList(List<YBDConversationModel> list) {
    List<YBDConversationModel> top = list.where((element) => element.isAtTop!).toList();
    List<YBDConversationModel> normal = list.where((element) => !element.isAtTop!).toList();
    return top
      ..addAll(normal)
      ..removeWhere((element) => (element.lastMessage?.isEmpty ?? true) && element.conversationType == "friend");
  }
  void sortConversationListxEOKzoyelive(List<YBDConversationModel> list) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<YBDUserCertificationRecord?>? _certificationInfoList;

  int lastSortLength = 0;
  _queryCer(List<YBDConversationModel> source) async {
    //查询过的就不再继续查询，防止循环setState
    if (source.length == 0 || source.length == lastSortLength) return;
    //记录一下
    lastSortLength = source.length;

    List<int> userIds = [];
    source.forEach((element) {
      if (element.conversationType == "friend") userIds.add(YBDNumericUtil.stringToInt(element.senderID));
    });
    YBDUserCertificationEntity? userCertificationEntity = await ApiHelper.queryCertification(context, userIds);
    if (userCertificationEntity?.returnCode == Const.HTTP_SUCCESS) {
      _certificationInfoList = userCertificationEntity!.record;
      setState(() {});
    }
  }

  bool? hasLoadData;

  @override
  Widget myBuild(BuildContext context) {
    return Container(
      decoration: YBDActivitySkinRoot().curAct().activityBgDecoration(),
      child: Column(
        children: [
          Container(
            height: ScreenUtil().setWidth(96),
            margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    onTap: () {
                      YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.gift_msg_page + "/1111");
                    },
                    child: Text(
                      translate('Inbox'),
                      style: TextStyle(
                        fontSize: YBDTPStyle.spNav,
                        color: YBDActivitySkinRoot().curAct().userProfileFontColor(),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    width: ScreenUtil().setWidth(96),
                    child: TextButton(
                      onPressed: () async {
                        logger.v('clicked inbox_contact button');
                        String? userId = await YBDSPUtil.getUserId();
                        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.follow + "/$userId/0");
                      },
                      style: TextButton.styleFrom(padding: EdgeInsets.all(0)),
                      // padding: EdgeInsets.all(0),
                      child: Image.asset(
                        "assets/images/inbox/inbox_contact.webp",
                        width: ScreenUtil().setWidth(48),
                        color: YBDActivitySkinRoot().curAct().userProfileFontColor(),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(15),
          ),
          Expanded(
            child: StreamBuilder<List<Map<String, dynamic>>?>(
              stream: bloc?.items,
              builder: (BuildContext context, AsyncSnapshot<List<Map<String, dynamic>>?> snapshot) {
                if (snapshot.hasData) {
                  List<YBDConversationModel> conversationList = sortConversationList(
                      List.generate(snapshot.data!.length, (x) => YBDConversationModel().fromDb(snapshot.data![x])));
                  if (conversationList.length == 0) {
                    return Center(child: YBDEmptyView());
                  }

                  _queryCer(conversationList);
                  return ListView.separated(
                      padding: EdgeInsets.all(0),
                      itemBuilder: (_, index) => YBDConversationItem(
                          conversationList[index],
                          YBDCommonUtil.getUsersSingleCer(
                              _certificationInfoList, YBDNumericUtil.stringToInt(conversationList[index].senderID))),
                      separatorBuilder: (_, index) => Padding(
                            padding: EdgeInsets.only(
                              left: ScreenUtil().setWidth(112),
                            ),
                            child: Container(
                              height: ScreenUtil().setWidth(1),
                              color: YBDActivitySkinRoot().curAct().userProfileNameColor(context).withOpacity(0.1),
                            ),
                          ),
                      itemCount: conversationList.length);
                }

                if (hasLoadData != null && !hasLoadData!) {
                  return Center(child: YBDEmptyView());
                }
                return Container(
                    width: ScreenUtil().setWidth(100), height: ScreenUtil().setWidth(100), child: YBDLoadingCircle());
              },
            ),
          )
        ],
      ),
    );
  }
  void myBuildmleGToyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE, location: "inbox_page"));
    try {
      bloc = SelectBloc(
        database: messageDb!,
        table: YBDConversationModel.conversationTableName,
        orderBy: "update_time DESC",
        reactive: true,
      );
    } catch (e) {
      logger.v('init SelectBloc error : $e');
    }

    fetchConversationList(context)?.then((value) {
      hasLoadData = value;
      setState(() {});
    });
    fetchMessageQueue(context, needSyncQueueId: true);
  }
  void initStateyEDzYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDInboxPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
