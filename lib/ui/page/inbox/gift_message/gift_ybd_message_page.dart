import 'dart:async';

import 'dart:convert';
import 'dart:developer';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/db_ybd_config.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/inbox/db/model_ybd_message.dart';
import '../../../../module/inbox/entity/gift_ybd_message_entity.dart';
import '../../../../module/inbox/message_ybd_helper.dart';
import 'item_ybd_gift_message.dart';
import '../system_msg/widget/opacity_ybd_bg_net_img.dart';
import '../../status/local_audio/top_ybd_navigate_bar.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/pop_menu/pop_ybd_menu.dart';

class YBDGiftMessagePage extends StatefulWidget {
  String queueId;

  YBDGiftMessagePage(this.queueId);

  @override
  YBDGiftMessagePageState createState() => new YBDGiftMessagePageState();
}

class YBDGiftMessagePageState extends BaseState<YBDGiftMessagePage> {
  /// 数据库更新管理器
  late SelectBloc _bloc;

  /// 系统消息列表
  List<YBDGiftMessageEntity?> _messageList = [];

  /// 是否为初始状态
  bool _isInitState = true;

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return new Scaffold(
      body: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: [YBDTopNavigateBar(title: translate('slog_gift')), Expanded(child: messagePageContentView())],
        ),
      ),
    );
  }
  void myBuildRNIyvoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

// 更新消息列表
  addMessageRemoveReplicate(List<Map> newMessage) {
    if (null == _messageList) {
      _messageList = [];
    }

    for (Map message in newMessage) {
      bool isReplace = false;
      for (int index = 0; index < _messageList.length; index++) {
        if (_messageList[index]!.messageId == message['message_id'] ||
            _messageList[index]!.requestId == message['request_id']) {
          _messageList[index] = YBDGiftMessageEntity().fromJson(json.decode(message['json_content']));
          isReplace = true;
          break;
        }
      }

      if (!isReplace) {
        _messageList.add(YBDGiftMessageEntity().fromJson(json.decode(message['json_content'])));
      }

      log(message.toString());
    }

    _messageList.sort((a, b) => b!.sendTime!.compareTo(a!.sendTime!));
  }

  /// 消息页面内容
  Widget messagePageContentView() {
    if (_isInitState) {
      logger.v('init loading');
      // 初始状态显示加载图
      return YBDLoadingCircle();
    } else {
      if (_messageList != null && _messageList.isNotEmpty) {
        // 有数据显示消息列表
        return Container(
          // 点赞消息列表
          width: ScreenUtil().screenWidth,
          child: giftMsgListView(),
        );
      } else {
        logger.v('empty msg list');
        // 数据为空显示缺省图
        return Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // TODO: 换图片
                Image.asset(
                  "assets/images/empty/empty_my_profile.webp",
                  width: ScreenUtil().setWidth(382),
                ),
                SizedBox(height: ScreenUtil().setWidth(30)),
                Text(
                  translate('no_message'),
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
          ),
        );
      }
    }
  }
  void messagePageContentViewvCOgjoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 消息列表
  Widget giftMsgListView() {
    if (null != _messageList && _messageList.isNotEmpty) {
      return ListView.builder(
        itemCount: _messageList.length,
        padding: EdgeInsets.all(0),
        // separatorBuilder: (_, index) => Container(
        //   height: ScreenUtil().setWidth(1),
        //   margin: EdgeInsets.only(left: ScreenUtil().setWidth(130)),
        //   color: Colors.white.withOpacity(0.15),
        // ),
        itemBuilder: (_, index) => YBDWPopupMenu(
          menuHeight: ScreenUtil().setWidth(54),
          menuWidth: ScreenUtil().setWidth(100),
          onValueChanged: (int value) {
            YBDMessageHelper.deleteSingleChat(_messageList[index]!.messageId, _messageList[index]!.queueId);
            _messageList.removeAt(index);
            setState(() {});
          },
//          startPress: () {
//            setState(() {
//              _showedPopMenu = true;
//            });
//          },
//          endPress: () {
//            if (_showedPopMenu) {
//              setState(() {
//                _showedPopMenu = false;
//              });
//            }
//          },
          showAtCenter: true,
          pressType: PressType.longPress,
          actions: [
            translate('delete'),
          ],
          child: GestureDetector(
            onTap: () {
              YBDNavigatorHelper.navigateTo(context, _messageList[index]!.content!.click!.url);
            },
            child: YBDGiftMessageItem(
              avatarUrl: _messageList[index]!.content!.icon,
              giftIconUrl: _messageList[index]!.content!.resource,
              senderName: _messageList[index]!.content!.title,
              gender: _messageList[index]!.attributes!.gender,
              level: _messageList[index]!.attributes!.level,
              count: _messageList[index]!.attributes!.giftNumber,
              userId: _messageList[index]!.attributes!.userId,
              isFontBlack: false,
              vipIcon: _messageList[index]!.attributes!.vipIcon,
            ),
          ),
          // child: Container(
          //   height: ScreenUtil().setWidth(140),
          //   child: Row(
          //     children: [
          //       SizedBox(
          //         width: ScreenUtil().setWidth(26),
          //       ),
          //       YBDOpacityBgNetImg(
          //         // 动态图片和占位图
          //         iconUrl: _messageList[index].content.icon,
          //         height: ScreenUtil().setWidth(80),
          //         width: ScreenUtil().setWidth(80),
          //         radius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40))),
          //       ),
          //       SizedBox(
          //         width: ScreenUtil().setWidth(22),
          //       ),
          //       Column(
          //         mainAxisAlignment: MainAxisAlignment.center,
          //         crossAxisAlignment: CrossAxisAlignment.start,
          //         children: [
          //           Text(
          //             _messageList[index].content.title,
          //             style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
          //           ),
          //           SizedBox(
          //             height: ScreenUtil().setWidth(10),
          //           ),
          //           Row(
          //             children: [
          //               Container(
          //                 child: Center(
          //                   child: Image.asset(
          //                     _messageList[index].attributes.gender == 1
          //                         ? 'assets/images/female.png'
          //                         : 'assets/images/male.png',
          //                     width: ScreenUtil().setWidth(10),
          //                   ),
          //                 ),
          //                 decoration: BoxDecoration(
          //                     borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(10))),
          //                     gradient: LinearGradient(
          //                         colors: _messageList[index].attributes.gender == 1 ? female : male,
          //                         begin: Alignment.topCenter,
          //                         end: Alignment.bottomCenter)),
          //                 width: ScreenUtil().setWidth(38),
          //                 height: ScreenUtil().setWidth(14),
          //               ),
          //               YBDLevelTag(
          //                 _messageList[index].attributes.level,
          //                 scale: 0.58,
          //               )
          //             ],
          //           ),
          //         ],
          //       ),
          //       Spacer(),
          //       YBDNetworkImage(
          //         imageUrl: _messageList[index].content.resource,
          //         width: ScreenUtil().setWidth(70),
          //       ),
          //       SizedBox(
          //         width: ScreenUtil().setWidth(64),
          //       ),
          //       Text(
          //         'X${_messageList[index].attributes.giftNumber}',
          //         style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
          //       ),
          //       SizedBox(
          //         width: ScreenUtil().setWidth(64),
          //       ),
          //     ],
          //   ),
          // ),
        ),
        shrinkWrap: true,
      );
    } else {
      logger.v('system msg is empty, show empty view');
      // TODO: 显示缺省图
      return Container();
    }
  }
  void giftMsgListViewF5mZuoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    logger.v('queue id : ${widget.queueId}');

    // 从数据库消息表中查系统消息
    _bloc = SelectBloc(
      database: messageDb!,
      where: "queue_id = \"${widget.queueId}\" ",
      table: YBDMessageModel.messageTableName,
      orderBy: 'send_time DESC',
      reactive: true,
      verbose: true,
    );

    /// 监听数据库中系统消息更新
    _bloc.items.listen((data) {
      if (data != null && mounted) {
        logger.v('update system msg from database');
        // 有数据更新后设置为非初始状态
        _isInitState = false;

        // 更新消息列表
        addMessageRemoveReplicate(data);
        setState(() {});
      }
    });

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE, location: "gift_message_page"));
  }
  void initStateAgsLEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDGiftMessagePage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  void didChangeDependenciesSHxmwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
