import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/icon_ybd_gender.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/certification_ybd_util.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../system_msg/widget/opacity_ybd_bg_net_img.dart';
import '../../../widget/level_ybd_tag.dart';

class YBDGiftMessageItem extends StatelessWidget {
  String? avatarUrl, giftIconUrl, senderName, userId, vipIcon;
  int? gender, level, count;
  bool? isFontBlack;
  bool smallHead;
  var female = [Color(0xffE37BBC), Color(0xffFF877D)];
  var male = [Color(0xff5E94E7), Color(0xff47CDCC)];
  YBDGiftMessageItem(
      {this.avatarUrl,
      this.giftIconUrl,
      this.senderName,
      this.gender,
      this.level,
      this.count,
      this.isFontBlack,
      this.userId,
      this.vipIcon,
      this.smallHead: false});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(140),
      decoration: BoxDecoration(),
      child: Row(
        children: [
          SizedBox(
            width: ScreenUtil().setWidth(26),
          ),
          GestureDetector(
            onTap: () {
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.user_profile + "/${userId}");
            },
            child: YBDOpacityBgNetImg(
              // 动态图片和占位图
              iconUrl: avatarUrl,
              height: ScreenUtil().setWidth(smallHead ? 64 : 80),
              width: ScreenUtil().setWidth(smallHead ? 64 : 80),
              radius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(40))),
              defaultWidget: YBDResourcePathUtil.defaultMaleImage(male: gender == 2),
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(22),
          ),
          StreamBuilder<Map<int?, YBDUserCertificationRecordCertificationInfos?>>(
              stream: YBDCertificationUtil.getInstance()!.certificationCacheOutStream,
              initialData: YBDCertificationUtil.getInstance()!.certificationCache,
              builder: (context, snapshot) {
                bool hasCer = false;
                YBDUserCertificationRecordCertificationInfos? info;
                if (snapshot.hasData) {
                  int? currentUserId = int.tryParse(userId!);
                  info = snapshot.data![currentUserId];
                  hasCer = info != null;
                }

                bool isSecondLineDisappear = hasCer && info!.hidGender == 1 && info.hidUserLevel == 1;
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ConstrainedBox(
                          child: Text(
                            senderName?.removeBlankSpace() ?? "",
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: ScreenUtil().setSp(24),
                                color: isFontBlack! ? Colors.black.withOpacity(0.7) : Colors.white),
                          ),
                          constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(300)),
                        ),
                        if (!hasCer || info?.hidGender == 0) YBDGenderIcon(gender ?? 0),
                        if (hasCer)
                          SizedBox(
                            width: ScreenUtil().setWidth(8),
                          ),
                        if (hasCer)
                          YBDNetworkImage(
                            imageUrl: info!.icon!,
                            width: ScreenUtil().setWidth(30),
                          )
                      ],
                    ),
                    if (!isSecondLineDisappear)
                      Container(
                        margin: EdgeInsets.only(top: ScreenUtil().setWidth(vipIcon != null ? 2 : 10)),
                        child: Row(
                          children: [
                            YBDVipIcon(
                              vipIcon ?? '',
                              padding: EdgeInsets.only(right: ScreenUtil().setWidth(0)),
                            ),
                            if (!hasCer || info?.hidUserLevel == 0)
                              YBDLevelTag(
                                level,
                                scale: 0.7,
                                horizontalPadding: 0,
                              )
                          ],
                        ),
                      ),
                  ],
                );
              }),
          Spacer(),
          YBDNetworkImage(
            imageUrl: giftIconUrl ?? '',
            width: ScreenUtil().setWidth(70),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(64),
          ),
          Container(
            width: ScreenUtil().setWidth(100),
            child: Text(
              'X$count',
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(24), color: isFontBlack! ? Colors.black.withOpacity(0.7) : Colors.white),
            ),
          ),
        ],
      ),
    );
  }
  void buildOAeitoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
