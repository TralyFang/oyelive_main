import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_inbox_track.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/date_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../module/inbox/db/model_ybd_conversation.dart';
import '../../../module/inbox/message_ybd_helper.dart';
import '../../widget/pop_menu/pop_ybd_menu.dart';
import '../status/widget/upgrade_ybd_notice_dialog.dart';

class YBDConversationItem extends StatefulWidget {
  YBDConversationModel data;
  bool isBlackText;
  YBDUserCertificationRecordCertificationInfos? info;
  YBDConversationItem(this.data, this.info, {this.isBlackText: false});

  @override
  YBDConversationItemState createState() => new YBDConversationItemState();
}

class YBDConversationItemState extends BaseState<YBDConversationItem> {
  List<String>? actions;

  avatarWithMessageCount() {
//    print("avatar: ${widget.data.avatar}");
    return Container(
      height: ScreenUtil().setWidth(130),
      width: ScreenUtil().setWidth(130),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Container(
              width: ScreenUtil().setWidth(90),
              height: ScreenUtil().setWidth(90),
              child: ClipOval(
                child: YBDNetworkImage(
                  imageUrl: widget.data.avatar ?? "",
                  fit: BoxFit.cover,
                  placeholder: (context, url) => YBDResourcePathUtil.defaultMaleImage(male: widget.data.senderGender == 2),
                  errorWidget: (context, url, err) =>
                      YBDResourcePathUtil.defaultMaleImage(male: widget.data.senderGender == 2),
                  width: ScreenUtil().setWidth(90),
                  height: ScreenUtil().setWidth(90),
                ),
              ),
            ),
          ),
          widget.data.unreadCount != 0
              ? Transform.translate(
                  offset: Offset(
                      ScreenUtil().setWidth(YBDCommonUtil.getUnreadCount(widget.data.unreadCount!).length > 1 ? 80 : 86),
                      ScreenUtil().setWidth(20)),
                  child: Container(
                    height: ScreenUtil().setWidth(34),
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(12),
                    ),
                    decoration: BoxDecoration(color: Color(0xffE6497A), shape: BoxShape.circle),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          YBDCommonUtil.getUnreadCount(widget.data.unreadCount!),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(20),
                            color: Colors.white,
                            height: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  timeConvert(int? millsecs) {
    if (millsecs == null || millsecs == 0) {
      return "";
    }

    return YBDDateUtil.timeFromMilliseconds(millsecs);
  }

  @override
  Widget myBuild(BuildContext context) {
    actions = [
      widget.data.isAtTop! ? translate('cancel_stick_to_top') : translate('stick_to_top'),
      translate('delete'),
    ];
    // logger.v("senderId${widget.data.senderID}  type=${widget.data.conversationType}");
    return YBDWPopupMenu(
      menuHeight: ScreenUtil().setWidth(54),
      menuWidth: ScreenUtil().setWidth(widget.data.isAtTop! ? 310 : 260),
      onValueChanged: (int value) {
        if (value == 0) {
          YBDMessageHelper.setTopChat(widget.data.queueID, !widget.data.isAtTop!);
        } else {
          YBDMessageHelper.deleteChatHistory(widget.data.queueID, widget.data.senderID,
              isFriend: widget.data.conversationType == "friend");
        }
      },
      showAtCenter: true,
      pressType: PressType.longPress,
      actions: actions,
      child: GestureDetector(
        onTap: () {
          // 后台返回的会话类型
          //private message, like, event, system, comment
          YBDMessageHelper.setReading(context, widget.data);
          YBDMessageHelper.readConversation(widget.data.queueID);
          YBDInboxTrack().clickMessage(type: widget.data.conversationType);
          switch (widget.data.conversationType) {
            case "friend":
              YBDNavigatorHelper.navigateTo(
                  context, YBDNavigatorHelper.inbox_private_message + "/${widget.data.senderID}/${widget.data.queueID}");
              break;
            case "system":
              logger.v('clicked system conversation, queueId : ${widget.data.queueID}');
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.system_msg_page + "/${widget.data.queueID}");
              break;
            case "comment":
              logger.v('clicked comment conversation, queueId : ${widget.data.queueID}');
              YBDNavigatorHelper.navigateTo(
                context,
                YBDNavigatorHelper.comment_msg_page + "/${widget.data.queueID}/${widget.data.unreadCount}",
              );
              break;
            case "like":
              logger.v('clicked like conversation, queueId : ${widget.data.queueID}');
              YBDNavigatorHelper.navigateTo(
                context,
                YBDNavigatorHelper.like_msg_page + "/${widget.data.queueID}/${widget.data.unreadCount}",
              );
              break;
            case "event":
              logger.v('clicked event conversation, queueId : ${widget.data.queueID}');
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.event_msg_page + "/${widget.data.queueID}");
              break;
            case "gift":
              logger.v('clicked event conversation, queueId : ${widget.data.queueID}');
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.gift_msg_page + "/${widget.data.queueID}");
              break;
            default:
              showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (context) {
                    return YBDUpgradeNoticeDialog();
                  });
              break;
          }
        },
        child: Slidable(
            actionPane: SlidableDrawerActionPane(),
            actionExtentRatio: 0.19,
            child: Container(
              height: ScreenUtil().setWidth(130),
              decoration: widget.data.isAtTop!
                  ? BoxDecoration(color: Color(0x08ffffff), shape: BoxShape.rectangle)
                  : BoxDecoration(),
              child: Row(
                children: [
                  avatarWithMessageCount(),
                  SizedBox(width: ScreenUtil().setWidth(10)),
                  Container(
                    height: ScreenUtil().setWidth(90),
                    width: ScreenUtil().setWidth(560),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: <Widget>[
                            // Container(
                            //   width: ScreenUtil().setWidth(370),
                            //   child: Text.rich(
                            //     TextSpan(
                            //         style: TextStyle(
                            //           fontSize: ScreenUtil().setSp(28),
                            //           fontWeight: FontWeight.w500,
                            //           color: widget.isBlackText ? Colors.black : ActivitySkinExt.userProfileFontColor(),
                            //         ),
                            //         children: [
                            //           TextSpan(text: widget.data.title ?? ''),
                            //           if (widget?.info != null)
                            //             WidgetSpan(
                            //                 child: Row(
                            //               mainAxisSize: MainAxisSize.min,
                            //               children: [
                            //                 SizedBox(
                            //                   width: ScreenUtil().setWidth(6),
                            //                 ),
                            //                 YBDNetworkImage(
                            //                   imageUrl: widget?.info?.icon,
                            //                   width: ScreenUtil().setWidth(38),
                            //                 )
                            //               ],
                            //             ))
                            //         ]),
                            //     maxLines: 1,
                            //   ),
                            // ),
                            Container(
                              width: ScreenUtil().setWidth(370),
                              child: Row(mainAxisSize: MainAxisSize.min, children: [
                                Container(
                                  child: Text(
                                    widget.data.title?.removeBlankSpace() ?? '',
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(28),
                                      fontWeight: FontWeight.w500,
                                      color: widget.isBlackText
                                          ? Colors.black
                                          : YBDActivitySkinRoot().curAct().userProfileFontColor(),
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                  constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(320), minWidth: 0),
                                ),
                                if (widget.info != null) ...[
                                  SizedBox(
                                    width: ScreenUtil().setWidth(6),
                                  ),
                                  YBDNetworkImage(
                                    imageUrl: widget.info?.icon ?? '',
                                    width: ScreenUtil().setWidth(36),
                                  )
                                ]
                              ]),
                            ),
                            Spacer(),
                            Text(
                              timeConvert(widget.data.updateTime),
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(20),
                                fontWeight: FontWeight.w300,
                                color: widget.isBlackText
                                    ? Colors.black
                                    : YBDActivitySkinRoot().curAct().userProfileFontColor(),
                              ),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(24),
                            )
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              width: ScreenUtil().setWidth(460),
                              child: Text(
                                getHint(),
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(24),
                                  fontWeight: FontWeight.w300,
                                  color: widget.isBlackText
                                      ? Colors.black
                                      : YBDActivitySkinRoot().curAct().userProfileNameColor(context).withOpacity(0.5),
                                ),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(40),
                            ),
                            widget.data.isNotDisturb!
                                ? Icon(
                                    Icons.notifications_off,
                                    color: widget.isBlackText
                                        ? Colors.black
                                        : YBDActivitySkinRoot().curAct().userProfileFontColor(),
                                    size: ScreenUtil().setWidth(20),
                                  )
                                : Container()
                          ],
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
            secondaryActions: <Widget>[
              IconSlideAction(
                color: Color(0xff393939),
                icon: widget.data.isAtTop! ? Icons.vertical_align_bottom : Icons.vertical_align_top,
                onTap: () {
                  YBDMessageHelper.setTopChat(widget.data.queueID, !widget.data.isAtTop!);
                },
              ),
              IconSlideAction(
                color: Color(0xffE6497A),
                icon: Icons.delete,
                onTap: () {
                  YBDMessageHelper.deleteChatHistory(widget.data.queueID, widget.data.senderID,
                      isFriend: widget.data.conversationType == "friend");
                },
              ),
            ]),
      ),
    );
  }
  void myBuildfOsZRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getHint() {
    switch (widget.data.conversationType) {
      case "comment":
        if (widget.data.unreadCount! > 1) {
          return "You've received ${widget.data.unreadCount} comments.";
        } else {
          if (widget.data.lastMessage!.isEmpty) {
            return translate('no_message');
          }
          return widget.data.lastMessage;
        }

        break;
      case "like":
        if (widget.data.unreadCount! > 1) {
          return "You've received ${widget.data.unreadCount} likes.";
        } else {
          if (widget.data.lastMessage!.isEmpty) {
            return translate('no_message');
          }
          return widget.data.lastMessage;
        }
        break;

      case "gift":
        if (widget.data.lastMessage!.isEmpty) {
          return translate('no_message');
        }
        return widget.data.lastMessage;
        if (widget.data.unreadCount! > 1) {
          return "You've received ${widget.data.unreadCount} likes.";
        } else {
          if (widget.data.lastMessage!.isEmpty) {
            return translate('no_message');
          }
          return widget.data.lastMessage;
        }
        break;
      default:
        if (widget.data.lastMessage!.isEmpty) {
          return translate('no_message');
        }
        return widget.data.lastMessage;
        break;
    }
  }

  @override
  void initState() {
    super.initState();
  }
  void initState6KgUVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDConversationItem oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetgfi2xoyelive(YBDConversationItem oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
