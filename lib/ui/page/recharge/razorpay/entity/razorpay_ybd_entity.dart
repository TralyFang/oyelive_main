import 'dart:async';

import '../../../../../generated/json/base/json_convert_content.dart';
import '../../../../../generated/json/base/json_field.dart';

class YBDRazorpayResultEntity with JsonConvert<YBDRazorpayResultEntity> {
  String? code;
  String? message;
  YBDRazorpayEntity? data;
}

class YBDRazorpayEntity with JsonConvert<YBDRazorpayEntity> {
  int? amount;
  @JSONField(name: "amount_paid")
  int? amountPaid;
  List<dynamic>? notes;
  @JSONField(name: "created_at")
  int? createdAt;
  @JSONField(name: "amount_due")
  int? amountDue;
  String? currency;
  String? receipt;
  String? id;
  String? entity;
  @JSONField(name: "offer_id")
  dynamic offerId;
  int? attempts;
  String? tradeNo;
  String? orderId;
  String? requestUrl;
  String? sign;
  String? status;
}
