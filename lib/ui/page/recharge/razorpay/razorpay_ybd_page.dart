import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/entity/result_ybd_bean.dart';
import '../../../../module/payment/payment_ybd_api_helper.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../widget/loading_ybd_circle.dart';

///Razorpa支付页面
class YBDRazorpayPage extends StatefulWidget {
  // final String targetId;
  // final String money;
  final String amount;
  final String currency;
  final String order_id;
  final String order_ybd_id;

  YBDRazorpayPage(this.amount, this.currency, this.order_id, this.order_ybd_id);

  @override
  _YBDRazorpayState createState() => _YBDRazorpayState();
}

class _YBDRazorpayState extends BaseState<YBDRazorpayPage> {
  late Razorpay _razorpay;

  /// 是否为初始状态
  bool _isInitState = true;

  YBDUserInfo? userInfo;

  @override
  Widget myBuild(BuildContext context) {
    if (_isInitState) {
      return YBDLoadingCircle();
    } else {
      return Container();
    }
  }
  void myBuild62zzCoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();

    ///初始化Razor pay 添加结果监听
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);

    _initData();
  }
  void initStateUQyStoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _initData() async {
    _checkUser();
    YBDLogUtil.d(
        'YBDRazorpayPage currency:${widget.currency} amount:${widget.amount} order_id:${widget.order_id}  order_ybd_id:${widget.order_ybd_id}');
    if (widget.currency != null && widget.amount != null && widget.order_id != null) {
      openCheckout(
        Const.TEST_ENV ? Const.RAZOR_TEST_KEY : Const.RAZOR_KEY,
        widget.amount.toString(),
        widget.currency,
        widget.order_id,
      );
    } else {
      YBDToastUtil.toast('${translate('pay_error')} YBDRazorpayEntity is null');
      _cancelInitState();
      Navigator.pop(context);
    }
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }
  void disposeugcuYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///设置参数 调起支付sdk
  void openCheckout(String key, String amount, String currency, String order_id) async {
//    var options = {
//    'key': '<YOUR_KEY_ID>',
//    'amount': 50000, //in the smallest currency sub-unit.
//    'name': 'Acme Corp.',
//    'order_id': 'order_EMBFqjDHEEn80l', // Generate order_id using Orders API
//    'description': 'Fine T-Shirt',
//    'timeout': 60, // in seconds
//    'prefill': {    'contact': '9123456789',    'email': 'gaurav.kumar@example.com'  }
//    };
    YBDLogUtil.d('openCheckout key:$key amount:$amount currency:$currency order_id:$order_id ');
    var options = {
      'key': key,
      'amount': amount,
      'name': 'oyetalk', //TODO  暂定oyetalk  后期做成可配置
      'currency': currency != null ? currency : 'INR', //USD
      'order_id': order_id,
      'timeout': 60 * 10, // in seconds
    };

    try {
      _razorpay.open(options);

      ///提交razorPay
    } catch (e) {
      debugPrint(e.toString());
      YBDLogUtil.d('_razorpay.open : ${e.toString()}');
    }
  }
  void openCheckoutO3EyAoyelive(String key, String amount, String currency, String order_id)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///支付成功回调
  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    Fluttertoast.showToast(msg: "SUCCESS: " + response.paymentId!, timeInSecForIosWeb: 4);
    YBDLogUtil.d(
        '_handlePaymentSuccess paymentId: ${response.paymentId} orderId:${response.orderId} order_id:${widget.order_id} signature:${response.signature} order_ybd_id:${widget.order_ybd_id}');
    Map<String, dynamic> params = {
      'payType': 'razorpay',
      'userId': userInfo?.id,
      'command': 'success',
      'razorpay_payment_id': response.paymentId,
      'razorpay_order_id': response.orderId,
      'razorpay_signature': response.signature,
    };
    _callBack(widget.order_ybd_id, params);
    YBDCommonUtil.storeOtherOrder(userInfo?.id, widget.order_id,
        'PaymentSuccess code:${response.orderId} message:${response.paymentId}', 'Razorpay',
        otOrderId: widget.order_ybd_id);
  }
  void _handlePaymentSuccessNdsJfoyelive(PaymentSuccessResponse response) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///支付错误回调
  void _handlePaymentError(PaymentFailureResponse response) {
    // Fluttertoast.showToast(msg: "ERROR: " + response.code.toString() + " - " + response.message, timeInSecForIosWeb: 4);
    if (response.code == 2) {
      YBDToastUtil.toast(translate('deal_canceled'));
    } else {
      YBDToastUtil.toast(translate('network_error'));
    }
    YBDLogUtil.d('_handlePaymentError : code: ${response.code}  message: ${response.message}');
    //_handlePaymentError : code: 2  message: {"error":{"code":"BAD_REQUEST_ERROR","description":"Payment processing cancelled by user","source":"customer","step":"payment_authentication","reason":"payment_cancelled"}}
    Map<String, dynamic> errorMsg = {
      'code': response.code,
      'message': response.message,
    };

    Map<String, dynamic> params = {
      'payType': 'razorpay',
      'userId': userInfo?.id,
      'command': 'failure',
      'orderId': widget.order_ybd_id,
      'errorMsg': errorMsg,
    };
    _callBack(widget.order_ybd_id, params);
    YBDCommonUtil.storeOtherOrder(
        userInfo?.id, widget.order_id, 'PaymentError code:${response.code} message:${response.message}', 'Razorpay',
        otOrderId: widget.order_ybd_id);
  }
  void _handlePaymentError8Axtzoyelive(PaymentFailureResponse response) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///外部钱包
  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(msg: "EXTERNAL_WALLET: " + response.walletName!, timeInSecForIosWeb: 4);
    YBDLogUtil.d('_handleExternalWallet : walletName ${response.walletName}');
    Map<String, dynamic> params = {
      'payType': 'razorpay',
      'userId': userInfo?.id,
      'command': 'externalWallet',

      ///外部钱包 后台暂时不动 使用 razorpay_signature保存
      'razorpay_walletName': response.walletName,
    };
    _callBack(widget.order_ybd_id, params);
    YBDCommonUtil.storeOtherOrder(userInfo?.id, widget.order_id, 'ExternalWallet', 'Razorpay',
        otOrderId: widget.order_ybd_id);
  }
  void _handleExternalWallet5W2fWoyelive(ExternalWalletResponse response) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _callBack(String orderID, Map<String, dynamic> params) async {
    YBDResultBeanEntity result = await YBDPaymentApiHelper.callbackPurchase(context, orderId: orderID, params: params);
    YBDLogUtil.d('_callBack result orderID:$orderID code:${result.code} returnMsg:${result.returnMsg}');
    _cancelInitState();
    Navigator.pop(context);
  }

  void _checkUser() async {
    if (userInfo == null) {
      userInfo = await YBDSPUtil.getUserInfo();
    }
  }
}
