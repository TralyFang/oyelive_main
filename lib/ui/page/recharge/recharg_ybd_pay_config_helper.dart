import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/payment/entity/pay_ybd_config_entity.dart';
import 'package:oyelive_main/module/payment/payment_ybd_api_helper.dart';

/// 2.2。4 支付详情辅助类
class YBDPayConfigHelper {
  ///获取支付配置 根据用户国家码
  static Future<YBDPayConfigEntity?> getPayConfig(BuildContext context) async {
    String? country = (await YBDSPUtil.getUserInfo())!.country;
    YBDPayConfigEntity? payConfigsEntity = await YBDPaymentApiHelper.queryPayConfigs(context, 'pay_configs', country);
    if (payConfigsEntity != null && payConfigsEntity.code == Const.HTTP_SUCCESS_NEW) {
      return payConfigsEntity;
    } else {
      return null;
    }
  }

  ///获取支付具体信息 充值活动使用 默认ep
  static Future<YBDPayConfigData?> getTopUpTypeData(BuildContext context, {String payType = 'easypaisa'}) async {
    YBDPayConfigEntity? payConfigsEntity = await getPayConfig(context);
    if (payConfigsEntity == null) {
      return null;
    }
    for (int index = 0; index < (payConfigsEntity.data?.length ?? 0); index++) {
      if (payConfigsEntity.data![index]!.name == payType) {
        YBDPayConfigData? payConfigData = payConfigsEntity.data![index];
        return payConfigData;
      }
    }
    return null;
  }
}
