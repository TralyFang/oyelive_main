import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';
import '../../../../base/base_ybd_state.dart';
import '../../../../base/page_ybd_bloc_provider.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../entity/query_ybd_recharge_record_entity.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';

import 'date_range_picker/date_ybd_range_picker.dart';
import 'recharge_ybd_record_page_bloc.dart';

class YBDTopupRecordPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => YBDTopupRecordPageState();
}

class YBDTopupRecordPageState extends BaseState<YBDTopupRecordPage> with SingleTickerProviderStateMixin {
  RefreshController _refreshController = RefreshController();

  // 默认一年前
  String startDate = YBDDateUtil.getSpecificYearDate(-1);

  // 默认今天
  String endDate = YBDDateUtil.currentDate(yearFormat: 'MM/dd/yyyy');

  @override
  void initState() {
    super.initState();
    // YBDRechargeRecordPageBloc bloc = YBDRechargeRecordPageBloc;
    // bloc.loadRechargeRecord(context, startDate, endDate);
  }
  void initStateZ46mjoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    // YBDRechargeRecordPageBloc bloc = YBDPageBlocProvider.of<YBDRechargeRecordPageBloc>(context);
    // return StreamBuilder(
    // stream: bloc.stream,
    // initialData: YBDRechargeRecordPageBlocState.initial(),
    // builder: (BuildContext context, AsyncSnapshot<YBDRechargeRecordPageBlocState> snapshot) {
    return BlocProvider(
      create: (_) {
        var recordBloc = YBDRechargeRecordPageBloc();
        recordBloc.loadRechargeRecord(context, startDate, endDate);
        return recordBloc;
      },
      child: Builder(
        builder: (builderContext) {
          return _contentWidget(builderContext);
        },
      ),
    );
  }

  Widget _contentWidget(BuildContext context) {
    return Scaffold(
      backgroundColor: YBDTPStyle.heliotrope,
      appBar: AppBar(
        centerTitle: true,
        leading: YBDNavBackButton(),
        title: Text(
          translate('top_up_record'),
          style: TextStyle(color: Colors.white, fontSize: YBDTPStyle.spNav),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Container(
        width: double.infinity,
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: <Widget>[
            InkWell(
              onTap: () async {
                final List<DateTime?>? picked = await YBDDateRangePicker.showDatePicker(
                    context: context,
                    initialFirstDate: YBDDateUtil.string2DateTime(startDate, format: 'MM/dd/yyyy'),
                    initialLastDate: YBDDateUtil.string2DateTime(endDate, format: 'MM/dd/yyyy'),
                    // firstDate: 当前时间往前5年
                    firstDate: YBDDateUtil.string2DateTime(YBDDateUtil.getSpecificYearDate(-5), format: 'MM/dd/yyyy'),
                    lastDate: DateTime.now()); // 今天
                if (picked != null && picked.length == 2) {
                  setState(() {
                    startDate = YBDDateUtil.formatStringWithDate(picked[0]!, format: 'MM/dd/yyyy');
                    endDate = YBDDateUtil.formatStringWithDate(picked[1]!, format: 'MM/dd/yyyy');
                  });
                  logger.v('picke date');
                  // 选择日期后再刷新一次列表
                  BlocProvider.of<YBDRechargeRecordPageBloc>(context)
                      .loadRechargeRecord(context, startDate, endDate, refresh: true);
                }
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
                height: ScreenUtil().setWidth(90),
                child: Row(
                  children: <Widget>[
                    Image.asset('assets/images/icon_calendar.png', width: ScreenUtil().setWidth(48)),
                    Text(translate('filter'), style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28))),
                    Expanded(child: Container()),
                    Text('$startDate - $endDate',
                        style: TextStyle(color: Colors.white.withOpacity(0.6), fontSize: ScreenUtil().setSp(28))),
                    SizedBox(width: ScreenUtil().setWidth(10)),
                    Icon(Icons.arrow_forward_ios, color: Colors.white.withOpacity(0.6), size: ScreenUtil().setWidth(22))
                  ],
                ),
              ),
            ),
            Divider(
                color: Colors.white.withOpacity(0.1),
                height: ScreenUtil().setWidth(1),
                thickness: ScreenUtil().setWidth(1)),
            BlocConsumer<YBDRechargeRecordPageBloc, YBDRechargeRecordPageBlocState>(
              listener: (_, state) {
                logger.v(
                    'loadState: ${state.loadState}, noMore: ${state.noMore}, page: ${state.page}, data length:${state.data?.length}');
                // if (state.loadState == LoadState.loaded) {
                if (_refreshController.isRefresh) {
                  _refreshController.refreshCompleted(resetFooterState: true);
                }

                if (_refreshController.isLoading) {
                  _refreshController.loadComplete();
                }

                if (state.noMore ?? false) {
                  _refreshController.loadNoData();
                }
                // }
              },
              builder: (_, state) {
                logger.v(
                    'loadState: ${state.loadState}, noMore: ${state.noMore}, page: ${state.page}, data length:${state.data?.length}');

                return Expanded(
                    child: state.loadState == LoadState.init
                        ? YBDLoadingCircle()
                        : (state.data == null || state.data!.isEmpty)
                            ? Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Image.asset(
                                      "assets/images/empty/empty_my_profile.webp",
                                      width: ScreenUtil().setWidth(320),
                                    ),
                                    SizedBox(
                                      height: ScreenUtil().setWidth(30),
                                    ),
                                    Text(
                                      translate('no_data'),
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    SizedBox(
                                      height: ScreenUtil().setWidth(200),
                                    ),
                                  ],
                                ),
                              )
                            : SmartRefresher(
                                controller: _refreshController,
                                header: YBDMyRefreshIndicator.myHeader,
                                footer: YBDMyRefreshIndicator.myFooter,
                                enablePullDown: true,
                                enablePullUp: true,
                                onRefresh: () async {
                                  logger.v('onRefresh');
                                  BlocProvider.of<YBDRechargeRecordPageBloc>(context)
                                      .loadRechargeRecord(context, startDate, endDate, refresh: true);
                                },
                                onLoading: () async {
                                  logger.v('onLoading');
                                  BlocProvider.of<YBDRechargeRecordPageBloc>(context)
                                      .loadRechargeRecord(context, startDate, endDate, loadMore: true);
                                },
                                child: ListView.separated(
                                    itemBuilder: (_, index) => recordItem(state.data![index]!),
                                    separatorBuilder: (_, index) => Container(
                                          margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
                                          height: ScreenUtil().setWidth(1),
                                          color: Colors.white.withOpacity(0.1),
                                        ),
                                    itemCount: state.data == null ? 0 : state.data!.length),
                              ));
              },
            )
          ],
        ),
      ),
    );
  }

  List<Color> statusColor = [
    Color(0xff7DFAFF),
    Color(0xffFEE97F),
    Color(0xffFC4C31),
    Color(0xffFF841A),
    Color(0xffFC4C31),
    Color(0xffFC4C31),
  ];

  List<String> statusText = [
    translate('success').cutSuffix('!'),
    translate('pending'),
    translate('fail'),
    translate('refund'),
    translate('cancel'),
    translate('unknown'),
  ];

  int getStatusIndex(String? status) {
    switch (status) {
      case "SUCCESS":
        return 0;
        break;
      case "PENDING":
        return 1;
        break;
      case "FAILURE":
        return 2;
        break;
      case "REFUND":
        return 3;
        break;
      case "CANCEL":
        return 4;
        break;
      default:
        return 5;
        break;
    }
  }

  Widget recordItem(YBDRechargeRecord record) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
          splashColor: Colors.white10,
          highlightColor: Colors.white12,
          onTap: () async {
            logger.v('record item click ...');
            onItemClick(record);
          },
          child: Container(
            height: ScreenUtil().setWidth(130),
            margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
              width: ScreenUtil().setWidth(1),
              color: Colors.white.withOpacity(0.1),
            ))),
            child: Row(
              children: <Widget>[
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                  Expanded(child: Container()),
                  Text('${YBDDateUtil.dateWithFormat(record.createTime, yearFormat: 'MM-dd-yyyy HH:mm:ss')}',
                      style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28))),
                  SizedBox(height: ScreenUtil().setWidth(10)),
                  Text(
                    '${record.type}',
                    style: TextStyle(fontSize: ScreenUtil().setSp(20), color: Colors.white.withOpacity(0.5)),
                  ),
                  Expanded(child: Container())
                ]),
                Expanded(child: Container()),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          'assets/images/l_beans.webp',
                          width: ScreenUtil().setWidth(24),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(8)),
                        Text(
                            '${getStatusIndex(record.status) == 0 ? '+' : ''}${YBDNumericUtil.format(record.virtualCurrencyAmount)}',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: ScreenUtil().setSp(28),
                            ))
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil().setWidth(6),
                    ),
                    Text(
                      statusText[getStatusIndex(record.status)],
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(20), color: statusColor[getStatusIndex(record.status)]),
                    ),
                  ],
                ),
              ],
            ),
          )),
    );
  }

  onItemClick(YBDRechargeRecord record) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(ScreenUtil().setWidth(16)),
                topRight: Radius.circular(ScreenUtil().setWidth(16)))),
        builder: (BuildContext context) {
          return ClipRRect(
            key: Key(record.transid!),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(ScreenUtil().setWidth(16)),
                topRight: Radius.circular(ScreenUtil().setWidth(16))),
            child: Container(
              color: Colors.white,
              height: ScreenUtil().setWidth(370),
              padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(48)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(children: <Widget>[
                    Text('${translate('time')}:',
                        style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(28))),
                    SizedBox(width: ScreenUtil().setWidth(18)),
                    Text('${YBDDateUtil.dateWithFormat(record.createTime, yearFormat: 'MM/dd/yyyy HH:mm:ss')}',
                        style: TextStyle(color: Color(0xff5A5A5A), fontSize: ScreenUtil().setSp(28)))
                  ]),
                  Row(children: <Widget>[
                    Text('${translate('beans')}:',
                        style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(28))),
                    SizedBox(width: ScreenUtil().setWidth(18)),
                    Text('${YBDNumericUtil.format(record.virtualCurrencyAmount)}',
                        style: TextStyle(color: Color(0xff5A5A5A), fontSize: ScreenUtil().setSp(28))),
                    SizedBox(width: ScreenUtil().setWidth(18)),
                    Image.asset('assets/images/topup/y_top_up_beans@2x.webp', height: ScreenUtil().setWidth(32))
                  ]),
                  Row(children: <Widget>[
                    Text('${translate('top_up_method')}:',
                        style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(28))),
                    SizedBox(width: ScreenUtil().setWidth(18)),
                    Text('${record.type}', style: TextStyle(color: Color(0xff5A5A5A), fontSize: ScreenUtil().setSp(28)))
                  ]),
                  Row(children: <Widget>[
                    Text('${translate('order_id')}:',
                        style: TextStyle(color: Colors.black, fontSize: ScreenUtil().setSp(28))),
                    SizedBox(width: ScreenUtil().setWidth(18)),
                    Text('${record.transid ?? '/'}',
                        style: TextStyle(color: Color(0xff5A5A5A), fontSize: ScreenUtil().setSp(28)))
                  ]),
                ],
              ),
            ),
          );
        });
  }
}
  void myBuildv8Wh3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
