import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import '../../../../base/base_ybd_bloc.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../entity/query_ybd_recharge_record_entity.dart';

class YBDRechargeRecordPageBloc extends Bloc<LoadState, YBDRechargeRecordPageBlocState> {
  final int pageSize = 10;
  late YBDRechargeRecordPageBlocState state;

  YBDRechargeRecordPageBloc() : super(YBDRechargeRecordPageBlocState.initial()) {
    state = YBDRechargeRecordPageBlocState.initial();
  }

  loadRechargeRecord(BuildContext context, String startDate, String endDate,
      {bool refresh = false, bool loadMore = false}) async {
    if (refresh) {
      logger.v('refresh');
      state.page = 0;
      state.data = [];
      state.noMore = false;
      state.loadState = LoadState.init;
    } else if (loadMore) {
      logger.v('loadMore');
      state.page = (state.page! + 1) * pageSize;
    }

    var startParam = YBDDateUtil.formatStringWithDate(YBDDateUtil.string2DateTime(startDate, format: 'MM/dd/yyyy'));
    var endParma = YBDDateUtil.formatStringWithDate(YBDDateUtil.string2DateTime(endDate, format: 'MM/dd/yyyy'));
    logger.v('startDate: $startParam, endDate: $endParma, page: ${state.page}, pageSize: $pageSize');

    int? lastDate;
    if (loadMore && state.data != null && state.data?.length != 0) {
      lastDate = state.data?.last?.createTime;
    }
    // 查询充值记录
    YBDQueryRechargeRecordEntity? result = await ApiHelper.queryRechargeRecord(context, startParam, endParma, lastDate);

    if (result == null || result.code != Const.HTTP_SUCCESS_NEW) {
      logger.v('state.data = null');
      state.data = [];
    } else {
      if (result.data == null || result.data!.length == 0) {
        logger.v('state.noMore = true');
        state.noMore = true;
      }

      logger.v('addAll: ${result.data?.length}');
      state.data!.addAll(result.data ?? []);
    }

    state.loadState = LoadState.loaded;
    logger.v(
        'loadState: ${state.loadState}, noMore: ${state.noMore}, page: ${state.page}, data length:${state.data?.length}');
    add(LoadState.loaded);
  }

  @override
  Stream<YBDRechargeRecordPageBlocState> mapEventToState(LoadState event) async* {
    logger.v(
        'loadState: ${state.loadState}, noMore: ${state.noMore}, page: ${state.page}, data length:${state.data?.length}');
    yield (YBDRechargeRecordPageBlocState.copyWith(state));
  }
  void mapEventToStatejEqMSoyelive(LoadState event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

enum LoadState { init, loaded }

class YBDRechargeRecordPageBlocState {
  List<YBDRechargeRecord?>? data;
  LoadState? loadState;
  int? page = 0; // 查询页码
  bool? noMore = false; // 页面无数据

  YBDRechargeRecordPageBlocState({this.data, this.loadState, this.page, this.noMore});

  factory YBDRechargeRecordPageBlocState.initial() => YBDRechargeRecordPageBlocState(
        data: [],
        loadState: LoadState.init,
        page: 0,
        noMore: false,
      );

  static YBDRechargeRecordPageBlocState copyWith(YBDRechargeRecordPageBlocState value) {
    var state = YBDRechargeRecordPageBlocState();
    state.data = [];

    if (value.data != null && value.data!.isNotEmpty) {
      state.data = List.from(value.data!);
    }

    state.loadState = value.loadState;
    state.page = value.page;
    state.noMore = value.noMore;

    logger.v(
        'loadState: ${state.loadState}, noMore: ${state.noMore}, page: ${state.page}, data length:${state.data?.length}');
    return state;
  }
}
