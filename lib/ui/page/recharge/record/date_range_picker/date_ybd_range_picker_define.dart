import 'dart:async';

import 'package:flutter/cupertino.dart';

/// 自定义数据类型
/// Initial display mode of the date picker dialog.
///
/// Date picker UI mode for either showing a list of available years or a
/// monthly calendar initially in the dialog shown by calling [showDatePicker].
///
/// Also see:
///
///  * <https://material.io/guidelines/components/pickers.html#pickers-date-pickers>
enum TPDatePickerMode {
  /// Show a date picker UI for choosing a month and day.
  day,

  /// Show a date picker UI for choosing a year.
  year,
}

const double kDatePickerHeaderPortraitHeight = 72.0;
const double kDatePickerHeaderLandscapeWidth = 168.0;

const Duration kMonthScrollDuration = Duration(milliseconds: 200);
const double kDayPickerRowHeight = 42.0;
const int kMaxDayPickerRowCount = 6; // A 31 day month that starts on Saturday.
// Two extra rows: one for the day-of-week header and one for the month header.
const double kMaxDayPickerHeight = kDayPickerRowHeight * (kMaxDayPickerRowCount + 2);

const double kMonthPickerPortraitWidth = 330.0;
const double kMonthPickerLandscapeWidth = 344.0;

const double kDialogActionBarHeight = 52.0;
const double kDatePickerLandscapeHeight = kMaxDayPickerHeight + kDialogActionBarHeight;
typedef TPSelectableDayPredicate = bool Function(DateTime);

// 日期位置
enum DateIndex {
  First,
  Second,
}

/// 日期数据
class YBDDateItem {
  /// 日期
  DateTime? date;

  /// 开始日期或结束日期
  DateIndex index;

  /// 日期模式
  TPDatePickerMode? mode;

  /// 是否被选中
  bool isSelected;

  YBDDateItem({
    this.index = DateIndex.First,
    this.date,
    this.isSelected = false,
    this.mode = TPDatePickerMode.day,
  });
}
