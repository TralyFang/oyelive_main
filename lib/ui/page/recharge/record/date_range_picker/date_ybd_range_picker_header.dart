import 'dart:async';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../date_range_picker/date_ybd_header_button.dart';
import '../date_range_picker/date_ybd_range_picker_define.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// 日期范围选择器头部
// Shows the selected date in large font and toggles between year and day mode
class YBDDatePickerHeader extends StatelessWidget {
  YBDDatePickerHeader({
    Key? key,
    required this.firstItem,
    required this.lastItem,
    required this.onHeaderBtnTaped,
    required this.orientation,
  })  : assert(firstItem != null),
        assert(lastItem != null),
        assert(orientation != null),
        super(key: key);

  final YBDDateItem firstItem;
  final YBDDateItem lastItem;
  final ValueChanged<YBDDateItem> onHeaderBtnTaped;
  final Orientation orientation;

  void _onHeaderBtnTaped(YBDDateItem dateItem) {
    onHeaderBtnTaped(dateItem);
  }
  void _onHeaderBtnTapedzFJcYoyelive(YBDDateItem dateItem) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    if (orientation == Orientation.portrait) {
      return _portraitHeader(context);
    } else {
      return _landscapeHeader(context);
    }
  }
  void builddnEceoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 竖屏头部
  Widget _portraitHeader(BuildContext context) {
    return Container(
      width: kMonthPickerPortraitWidth,
      height: kDatePickerHeaderPortraitHeight,
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(15)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(ScreenUtil().setWidth(16)), topRight: Radius.circular(ScreenUtil().setWidth(16))),
        gradient: LinearGradient(
          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _startHeader(context),
          _endHeader(context),
        ],
      ),
    );
  }
  void _portraitHeaderbKXR0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 横屏头部
  Widget _landscapeHeader(BuildContext context) {
    return Container(
      width: kDatePickerHeaderLandscapeWidth,
      height: kDatePickerLandscapeHeight,
      padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
      color: Colors.white,
      child: Column(
        children: [
          Container(
            width: kDatePickerHeaderLandscapeWidth,
            child: _startHeader(context),
          ),
          Container(
            width: kDatePickerHeaderLandscapeWidth,
            child: _endHeader(context),
          ),
        ],
      ),
    );
  }
  void _landscapeHeadervrMPjoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 开始日期
  Widget _startHeader(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: ScreenUtil().setWidth(12),
        ),
        renderYearButton(context, firstItem),
        SizedBox(
          height: ScreenUtil().setWidth(12),
        ),
        renderDayButton(context, firstItem),
        SizedBox(
          height: ScreenUtil().setWidth(9),
        ),
      ],
    );
  }
  void _startHeader32F35oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 结束日期
  Widget _endHeader(BuildContext context) {
    return lastItem != null
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              SizedBox(
                height: ScreenUtil().setWidth(12),
              ),
              renderYearButton(context, lastItem),
              SizedBox(
                height: ScreenUtil().setWidth(12),
              ),
              renderDayButton(context, lastItem),
              SizedBox(
                height: ScreenUtil().setWidth(9),
              ),
            ],
          )
        : Container();
  }
  void _endHeaderQ7hXloyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 年份按钮
  Widget renderYearButton(BuildContext context, YBDDateItem dateItem) {
    final ThemeData themeData = Theme.of(context);
    final TextTheme headerTextTheme = themeData.primaryTextTheme;
    final TextStyle yearStyle = headerTextTheme.subtitle1!.copyWith(
      color: Colors.white,
      height: 1.4,
    );
    final MaterialLocalizations localizations = MaterialLocalizations.of(context);

    return IgnorePointer(
      ignoring: dateItem.mode == TPDatePickerMode.year && dateItem.isSelected,
      ignoringSemantics: false,
      child: YBDDateHeaderButton(
        color: Colors.transparent,
        onTap: Feedback.wrapForTap(() {
          dateItem.mode = TPDatePickerMode.year;
          _onHeaderBtnTaped(dateItem);
        }, context),
        child: Semantics(
          selected: dateItem.mode == TPDatePickerMode.year,
          child: Text(localizations.formatYear(dateItem.date!), style: yearStyle),
        ),
      ),
    );
  }
  void renderYearButton5ckvKoyelive(BuildContext context, YBDDateItem dateItem) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 日期按钮
  Widget renderDayButton(BuildContext context, YBDDateItem dateItem) {
    final ThemeData themeData = Theme.of(context);
    final TextTheme headerTextTheme = themeData.primaryTextTheme;
    final TextStyle dayStyle = headerTextTheme.headline4!.copyWith(
      color: Colors.white,
      height: 1.4,
    );

    final MaterialLocalizations localizations = MaterialLocalizations.of(context);
    return IgnorePointer(
      ignoring: dateItem.mode == TPDatePickerMode.day && dateItem.isSelected,
      ignoringSemantics: false,
      child: Container(
        decoration: dateItem.isSelected
            ? BoxDecoration(
                border: Border.all(color: Colors.white),
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))))
            : null,
        child: YBDDateHeaderButton(
          color: Colors.transparent,
          onTap: Feedback.wrapForTap(
            () {
              dateItem.mode = TPDatePickerMode.day;
              _onHeaderBtnTaped(dateItem);
            },
            context,
          ),
          child: Semantics(
              selected: dateItem.mode == TPDatePickerMode.day,
              child: Text(
                localizations.formatMediumDate(dateItem.date!),
                style: dayStyle,
                textScaleFactor: 0.5,
              )),
        ),
      ),
    );
  }
  void renderDayButtonXAjywoyelive(BuildContext context, YBDDateItem dateItem) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 按钮背景色
  Color? _backgroundColor(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    Color? backgroundColor;

    switch (themeData.brightness) {
      case Brightness.light:
        backgroundColor = themeData.primaryColor;
        break;
      case Brightness.dark:
        backgroundColor = themeData.backgroundColor;
        break;
    }

    return backgroundColor;
  }

  /// 文字颜色
  Color? _textColorWithMode(BuildContext context, YBDDateItem dateItem, TPDatePickerMode textMode) {
    Color? textColor;
    final ThemeData themeData = Theme.of(context);
    switch (themeData.primaryColorBrightness) {
      case Brightness.light:
        if (dateItem.isSelected && dateItem.mode == textMode) {
          textColor = Colors.black87;
        } else {
          textColor = Colors.black54;
        }
        break;
      case Brightness.dark:
        if (dateItem.isSelected && dateItem.mode == textMode) {
          textColor = Colors.white;
        } else {
          textColor = Colors.white70;
        }
        break;
    }

    return textColor;
  }
}
