import 'dart:async';

import '../date_range_picker/date_ybd_range_picker_define.dart';
import '../date_range_picker/date_ybd_range_picker_header.dart';
import '../date_range_picker/tp_ybd_month_picker.dart';
import '../date_range_picker/tp_ybd_year_picker.dart';
import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/semantics.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class YBDDateRangePickerDialog extends StatefulWidget {
  final initialFirstDate;
  final initialLastDate;
  final DateTime? firstDate;
  final DateTime? lastDate;
  final TPSelectableDayPredicate? selectableDayPredicate;
  final TPDatePickerMode? initialDateRangePickerMode;

  YBDDateRangePickerDialog({
    Key? key,
    required this.initialFirstDate,
    required this.initialLastDate,
    this.firstDate,
    this.lastDate,
    this.selectableDayPredicate,
    this.initialDateRangePickerMode,
  })  : assert(null != initialFirstDate),
        assert(null != initialLastDate),
        super(key: key);

  @override
  _YBDDateRangePickerDialogState createState() => _YBDDateRangePickerDialogState();
}

class _YBDDateRangePickerDialogState extends State<YBDDateRangePickerDialog> {
  bool _announcedInitialDate = false;
  late MaterialLocalizations localizations;
  late TextDirection textDirection;

  /// 日期数组
  List<YBDDateItem> _dateItemList = [
    YBDDateItem(index: DateIndex.First, isSelected: true),
    YBDDateItem(index: DateIndex.Second, isSelected: false),
  ];

  final GlobalKey _pickerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    // 开始日期
    _dateItemList[0].date = widget.initialFirstDate;
    _dateItemList[0].mode = widget.initialDateRangePickerMode;
    // 结束日期
    _dateItemList[1].date = widget.initialLastDate;
    _dateItemList[1].mode = widget.initialDateRangePickerMode;
  }
  void initStatetn1OEoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    localizations = MaterialLocalizations.of(context);
    textDirection = Directionality.of(context);

    if (!_announcedInitialDate) {
      _announcedInitialDate = true;
      SemanticsService.announce(
        localizations.formatFullDate(_dateItemList[0].date!),
        textDirection,
      );

      SemanticsService.announce(
        localizations.formatFullDate(_dateItemList[1].date!),
        textDirection,
      );
    }
  }
  void didChangeDependenciesI78Zeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Theme(
      data: theme.copyWith(
        dialogBackgroundColor: Colors.transparent,
      ),
      child: Dialog(
        child: OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {
            assert(orientation != null);
            switch (orientation) {
              case Orientation.portrait:
                return _portraitContent();
              case Orientation.landscape:
                return _landscapeContent();
              default:
                return Container();
            }
          },
        ),
      ),
    );
  }
  void buildjx9egoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 竖屏弹框内容
  Widget _portraitContent() {
    final ThemeData theme = Theme.of(context);
    return SizedBox(
      // 竖屏弹框内容
      width: kMonthPickerPortraitWidth,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _dialogHeader(Orientation.portrait),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(14.0), bottomRight: Radius.circular(14.0)),
              color: Colors.white,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _dialogPicker(),
                _dialogActions(),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void _portraitContento1Ah3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 横屏弹框内容
  Widget _landscapeContent() {
    final ThemeData theme = Theme.of(context);
    return SizedBox(
      // 横屏弹框内容
      height: kDatePickerLandscapeHeight,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _dialogHeader(Orientation.landscape),
          Flexible(
            child: Container(
              width: kMonthPickerLandscapeWidth,
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  _dialogPicker(),
                  _dialogActions(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  void _landscapeContentaMwrSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 对话框头部
  Widget _dialogHeader(Orientation orientation) {
    return YBDDatePickerHeader(
      firstItem: _dateItemList[0],
      lastItem: _dateItemList[1],
      onHeaderBtnTaped: _handleHeaderBtnTaped,
      orientation: orientation,
    );
  }

  /// 头部按钮点击事件响应方法
  void _handleHeaderBtnTaped(YBDDateItem dateItem) {
    _dateItemList.forEach((element) {
      if (element.index == dateItem.index) {
        element.isSelected = true;
        element.mode = dateItem.mode;
      } else {
        element.isSelected = false;
      }
    });

    setState(() {
      if (dateItem.mode == TPDatePickerMode.day) {
        SemanticsService.announce(
          localizations.formatMonthYear(_dateItemList[0].date!),
          textDirection,
        );
        SemanticsService.announce(
          localizations.formatMonthYear(_dateItemList[1].date!),
          textDirection,
        );
      } else {
        SemanticsService.announce(
          localizations.formatYear(_dateItemList[0].date!),
          textDirection,
        );
        SemanticsService.announce(
          localizations.formatYear(_dateItemList[1].date!),
          textDirection,
        );
      }
    });
  }
  void _handleHeaderBtnTaped7IJNaoyelive(YBDDateItem dateItem) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 日期选择器
  Widget _dialogPicker() {
    return Flexible(
      child: SizedBox(
        height: kMaxDayPickerHeight,
        child: _buildPicker(),
      ),
    );
  }

  /// 年份选择器和日期选择器
  Widget? _buildPicker() {
    YBDDateItem selectedItem = _dateItemList.firstWhereOrNull(
      (element) => element.isSelected,
    )!;

    assert(selectedItem.mode != null);
    switch (selectedItem.mode) {
      case TPDatePickerMode.day:
        return YBDTPMonthPicker(
          key: _pickerKey,
          selectedDate: selectedItem,
          onChanged: _changeDateWithItem,
          firstDate: selectedItem.index == DateIndex.First ? widget.firstDate! : _dateItemList[0].date!,
          lastDate: selectedItem.index == DateIndex.Second ? widget.lastDate! : _dateItemList[1].date!,
          selectableDayPredicate: widget.selectableDayPredicate,
        );
      case TPDatePickerMode.year:
        return YBDTPYearPicker(
          key: _pickerKey,
          selectedDate: selectedItem,
          onChanged: _changeDateWithItem,
          firstDate: selectedItem.index == DateIndex.First ? widget.firstDate! : _dateItemList[0].date!,
          lastDate: selectedItem.index == DateIndex.Second ? widget.lastDate! : _dateItemList[1].date!,
        );
      default:
        return null;
    }
  }

  /// 根据日期索引给对应位置的日期赋值
  void _changeDateWithItem(YBDDateItem dateItem) {
    _vibrate();
    _dateItemList.forEach((element) {
      if (element.index == dateItem.index) {
        element.date = dateItem.date;
      }
    });

    if (_dateItemList[0].date!.isAfter(_dateItemList[1].date!)) {
      // 开始日期晚于结束日期时把结束日期和开始日期设置为同一天
      _dateItemList[1].date = _dateItemList[0].date;
    }

    setState(() {});
  }
  void _changeDateWithItemtnAq8oyelive(YBDDateItem dateItem) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消和确认按钮
  Widget _dialogActions() {
    return Container(
      padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(16)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          //ButtonBarTheme是ButtonBar的主题，可以全局改主题样式
          ButtonBarTheme(
            data: ButtonBarThemeData(buttonTextTheme: ButtonTextTheme.accent),
            child: ButtonBar(
              children: <Widget>[
                GestureDetector(
                  onTap: _handleCancel,
                  child: Container(
                    width: ScreenUtil().setWidth(210),
                    height: ScreenUtil().setWidth(75),
                    padding: EdgeInsets.all(ScreenUtil().setWidth(6)),
                    decoration:
                        BoxDecoration(color: Color(0xffCCCCCC), borderRadius: BorderRadius.all(Radius.circular(50))),
                    alignment: Alignment.center,
                    child: Text(
                      localizations.cancelButtonLabel,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: ScreenUtil().setSp(28),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                GestureDetector(
                  onTap: _handleOk,
                  child: Container(
                    width: ScreenUtil().setWidth(210),
                    height: ScreenUtil().setWidth(75),
                    padding: EdgeInsets.all(ScreenUtil().setWidth(6)),
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter),
                        borderRadius: BorderRadius.all(Radius.circular(50))),
                    alignment: Alignment.center,
                    child: Text(
                      localizations.okButtonLabel,
                      style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void _dialogActionsSxIz7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消按钮事件处理器
  void _handleCancel() {
    Navigator.pop(context);
  }

  /// 确认按钮事件处理器
  void _handleOk() {
    List<DateTime?> result = [];
    if (_dateItemList[0] != null) {
      result.add(_dateItemList[0].date);
      result.add(_dateItemList[1].date);
    }
    Navigator.pop(context, result);
  }

  /// 设备震动效果
  void _vibrate() {
    switch (Theme.of(context).platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        HapticFeedback.vibrate();
        break;
      case TargetPlatform.iOS:
        break;
    }
  }
}
