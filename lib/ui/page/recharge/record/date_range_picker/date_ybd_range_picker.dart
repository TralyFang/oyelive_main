import 'dart:async';

// Copyright 2015 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

import '../date_range_picker/date_ybd_range_picker_define.dart';
import '../date_range_picker/date_ybd_range_picker_dialog.dart' as DatePicker;

/// Shows a dialog containing a material design date picker.
///
/// The returned [Future] resolves to the date selected by the user when the
/// user closes the dialog. If the user cancels the dialog, null is returned.
///
/// An optional [selectableDayPredicate] function can be passed in to customize
/// the days to enable for selection. If provided, only the days that
/// [selectableDayPredicate] returned true for will be selectable.
///
/// An optional [initialDateRangePickerMode] argument can be used to display the
/// date picker initially in the year or month+day picker mode. It defaults
/// to month+day, and must not be null.
///
/// An optional [locale] argument can be used to set the locale for the date
/// picker. It defaults to the ambient locale provided by [Localizations].
///
/// An optional [textDirection] argument can be used to set the text direction
/// (RTL or LTR) for the date picker. It defaults to the ambient text direction
/// provided by [Directionality]. If both [locale] and [textDirection] are not
/// null, [textDirection] overrides the direction chosen for the [locale].
///
/// The `context` argument is passed to [showDialog], the documentation for
/// which discusses how it is used.
///
/// See also:
///
///  * [showTimePicker]
///  * <https://material.google.com/components/pickers.html#pickers-date-pickers>

class YBDDateRangePicker {
  static Future<List<DateTime?>?> showDatePicker({
    required BuildContext context,
    required DateTime initialFirstDate,
    required DateTime initialLastDate,
    required DateTime firstDate,
    required DateTime lastDate,
    TPSelectableDayPredicate? selectableDayPredicate,
    TPDatePickerMode initialDateRangePickerMode = TPDatePickerMode.day,
    Locale? locale,
    TextDirection? textDirection,
  }) async {
    assert(!initialFirstDate.isBefore(firstDate), 'initialDate must be on or after firstDate');
    assert(!initialLastDate.isAfter(lastDate), 'initialDate must be on or before lastDate');
    assert(!initialFirstDate.isAfter(initialLastDate), 'initialFirstDate must be on or before initialLastDate');
    assert(!firstDate.isAfter(lastDate), 'lastDate must be on or after firstDate');
    assert(
        selectableDayPredicate == null ||
            selectableDayPredicate(initialFirstDate) ||
            selectableDayPredicate(initialLastDate),
        'Provided initialDate must satisfy provided selectableDayPredicate');
    assert(initialDateRangePickerMode != null, 'initialDateRangePickerMode must not be null');

    Widget child = DatePicker.YBDDateRangePickerDialog(
      initialFirstDate: initialFirstDate,
      initialLastDate: initialLastDate,
      firstDate: firstDate,
      lastDate: lastDate,
      selectableDayPredicate: selectableDayPredicate,
      initialDateRangePickerMode: initialDateRangePickerMode,
    );

    if (textDirection != null) {
      child = Directionality(
        textDirection: textDirection,
        child: child,
      );
    }

    if (locale != null) {
      child = Localizations.override(
        context: context,
        locale: locale,
        child: child,
      );
    }

    return await showDialog<List<DateTime?>>(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => child,
    );
  }
}
