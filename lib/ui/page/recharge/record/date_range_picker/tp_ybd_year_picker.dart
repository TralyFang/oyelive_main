import 'dart:async';

import '../date_range_picker/date_ybd_range_picker_define.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// A scrollable list of years to allow picking a year.
///
/// The year picker widget is rarely used directly. Instead, consider using
/// [showDatePicker], which creates a date picker dialog.
///
/// Requires one of its ancestors to be a [Material] widget.
///
/// See also:
///
///  * [showDatePicker]
///  * <https://material.google.com/components/pickers.html#pickers-date-pickers>
class YBDTPYearPicker extends StatefulWidget {
  /// Creates a year picker.
  ///
  /// The [selectedDate] and [onChanged] arguments must not be null. The
  /// [lastDate] must be after the [firstDate].
  ///
  /// Rarely used directly. Instead, typically used as part of the dialog shown
  /// by [showDatePicker].
  YBDTPYearPicker({
    Key? key,
    required this.selectedDate,
    required this.onChanged,
    required this.firstDate,
    required this.lastDate,
  })  : assert(selectedDate != null),
        assert(onChanged != null),
        assert(!firstDate.isAfter(lastDate)),
        super(key: key);

  /// The currently selected date.
  ///
  /// This date is highlighted in the picker.
  final YBDDateItem selectedDate;

  /// Called when the user picks a year.
  final ValueChanged<YBDDateItem> onChanged;

  /// The earliest date the user is permitted to pick.
  final DateTime firstDate;

  /// The latest date the user is permitted to pick.
  final DateTime lastDate;

  @override
  _YBDYearPickerState createState() => _YBDYearPickerState();
}

class _YBDYearPickerState extends State<YBDTPYearPicker> {
  static double _itemExtent = 50.0;
  ScrollController? scrollController;

  @override
  void initState() {
    super.initState();
    int offset = widget.selectedDate.date!.year - widget.firstDate.year;
    // Move the initial scroll position to the currently selected date's year.
    scrollController = ScrollController(initialScrollOffset: offset * _itemExtent);
  }
  void initStatepeyZtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterial(context));
    final ThemeData themeData = Theme.of(context);
    final TextStyle style = themeData.textTheme.bodyText2!.copyWith(color: Color(0xff8D8D8D));
    final TextStyle selectedStyle = themeData.textTheme.headline5!.copyWith(
      color: Color(0xff5E94E7),
    );

    return ListView.builder(
      controller: scrollController,
      itemExtent: _itemExtent,
      itemCount: widget.lastDate.year - widget.firstDate.year + 1,
      itemBuilder: (BuildContext context, int index) {
        final int year = widget.firstDate.year + index;
        final bool isSelected = year == widget.selectedDate.date!.year;
        final TextStyle itemStyle = isSelected ? selectedStyle : style;
        return InkWell(
          key: ValueKey<int>(year),
          onTap: () {
            YBDDateItem item = YBDDateItem();
            item.date = DateTime(year, widget.selectedDate.date!.month, widget.selectedDate.date!.day);
            item.index = widget.selectedDate.index;
            widget.onChanged(item);
          },
          child: Center(
            child: Semantics(
              selected: isSelected,
              child: Text(year.toString(), style: itemStyle),
            ),
          ),
        );
      },
    );
  }
  void build54neYoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
