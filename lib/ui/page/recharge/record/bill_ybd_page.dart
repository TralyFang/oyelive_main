import 'dart:async';

/*
 * @Author: William-Zhou
 * @Date: 2021-08-09 11:10:19
 * @LastEditTime: 2021-11-25 18:03:36
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/ui/widget/my_ybd_app_bar.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class YBDBillPage extends StatelessWidget {
  const YBDBillPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: YBDMyAppBar(
        backgroundColor: YBDTPStyle.heliotrope,
        leading: YBDNavBackButton(),
        elevation: 0.0,
        title: Text(
          translate('bill'),
          style: TextStyle(color: Colors.white, fontSize: YBDTPStyle.spNav),
        ),
      ),
      body: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: <Widget>[
            _hSizedBox(15),
            _buildRowItem("assets/images/icon_bankcard.webp", translate('top_up_record'), () {
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.top_up_record);
            }),
            _buildRowItem("assets/images/l_beans.webp", translate('beans_record'), () {
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.currency_record + "/${Const.CURRENCY_BEAN}");
            }),
            _buildRowItem("assets/images/icon_gems.webp", translate('gems_record'), () {
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.currency_record + "/${Const.CURRENCY_GEM}");
            }),
            _buildRowItem("assets/images/icon_gold.webp", translate('gold_record'), () {
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.currency_record + "/${Const.CURRENCY_GOLD}");
            }),
          ],
        ),
      ),
    );
  }
  void buildf7mk4oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _buildRowItem(String imgPath, String itemName, Function tap) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: tap as void Function()?,
        child: Container(
          height: ScreenUtil().setWidth(120),
          child: Column(
            children: [
              _hSizedBox(40),
              Row(
                children: [
                  _wSizedBox(40),
                  Image.asset(imgPath, width: ScreenUtil().setWidth(35)),
                  _wSizedBox(30),
                  Text(itemName, style: TextStyle(fontSize: ScreenUtil().setSp(28))),
                  Spacer(),
                  Image.asset(
                    "assets/images/icon_more_grey.png",
                    width: ScreenUtil().setWidth(48),
                    color: Colors.white,
                  ),
                  _wSizedBox(25),
                ],
              ),
              Spacer(),
              Container(
                height: ScreenUtil().setWidth(1),
                color: Colors.white.withOpacity(0.1),
              )
            ],
          ),
        ),
      ),
    );
  }
  void _buildRowItemh6vHloyelive(String imgPath, String itemName, Function tap) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _hSizedBox(int size) {
    return SizedBox(height: ScreenUtil().setWidth(size));
  }

  Widget _wSizedBox(int size) {
    return SizedBox(width: ScreenUtil().setWidth(size));
  }
  void _wSizedBoxNZ0Lhoyelive(int size) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
