import 'dart:async';

/*
 * @Author: William-Zhou
 * @Date: 2021-11-25 10:14:51
 * @LastEditTime: 2021-12-01 14:17:23
 * @LastEditors: William-Zhou
 * @Description: Beans & Gems & Golds Record Page
 */
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/date_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/inbox/empty_ybd_view.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/my_ybd_app_bar.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';

class YBDCurrencyRecord extends StatefulWidget {
  // 货币类型
  final String? currency;
  const YBDCurrencyRecord({Key? key, this.currency}) : super(key: key);

  @override
  _YBDCurrencyRecordState createState() => _YBDCurrencyRecordState();
}

class _YBDCurrencyRecordState extends State<YBDCurrencyRecord> {
  RefreshController _refreshController = RefreshController();
  final int _pageSize = 10;
  int _page = 1;
  bool _isTimeout = false;
  bool _isReseted = false;
  List? _dataList;

  @override
  void initState() {
    super.initState();
    _getRecord();
  }
  void initStateLiUU8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 获取记录
  void _getRecord({int? setPage}) async {
    if (setPage == null) {
      _page = 1;
    }
    var recordEntity;
    switch (widget.currency) {
      case Const.CURRENCY_BEAN:
        recordEntity = await ApiHelper.getBeansDetail(context, pageSize: _pageSize, page: _page);
        break;
      case Const.CURRENCY_GEM:
        recordEntity = await ApiHelper.getGemsDetail(context, pageSize: _pageSize, page: _page);
        break;
      case Const.CURRENCY_GOLD:
        recordEntity = await ApiHelper.getGoldsDetail(context, pageSize: _pageSize, page: _page);
        break;
      default:
        recordEntity = await ApiHelper.getBeansDetail(context, pageSize: _pageSize, page: _page);
        break;
    }
    if (_refreshController.isLoading) _refreshController.loadComplete();
    if (_refreshController.isRefresh) _refreshController.refreshCompleted();
    if (recordEntity != null && recordEntity.returnCode == Const.HTTP_SUCCESS) {
      if (_page == 1) {
        _dataList = recordEntity.record;
      } else {
        _dataList!.addAll(recordEntity.record);
      }
      if (recordEntity.record.length < _pageSize) {
        _refreshController.loadNoData();
      } else {
        _refreshController.resetNoData();
      }
      setState(() {});
    } else {
      _isTimeout = true;
    }
  }
  void _getRecordq6nXmoyelive({int? setPage})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _nextPage() {
    _getRecord(setPage: ++_page);
  }

  String _getDate(int? millSec) {
    return YBDDateUtil.dateWithFormat(millSec, yearFormat: 'MM-dd-yyyy HH:mm');
  }
  void _getDateMXQYxoyelive(int? millSec) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String _getTitle() {
    switch (widget.currency) {
      case Const.CURRENCY_BEAN:
        return translate('beans_record');
        break;
      case Const.CURRENCY_GEM:
        return translate('gems_record');
        break;
      case Const.CURRENCY_GOLD:
        return translate('gold_record');
        break;
      default:
        return translate('beans_record');
        break;
    }
  }
  void _getTitleDboKboyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _getCurrencyIcon() {
    switch (widget.currency) {
      case Const.CURRENCY_BEAN:
        return Image.asset('assets/images/l_beans.webp', width: _w(24));
        break;
      case Const.CURRENCY_GEM:
        return Image.asset('assets/images/icon_gems.webp', width: _w(33));
        break;
      case Const.CURRENCY_GOLD:
        return Image.asset('assets/images/icon_gold.webp', width: _w(29));
        break;
      default:
        return Image.asset('assets/images/l_beans.webp', width: _w(24));
        break;
    }
  }
  void _getCurrencyIconnVSBmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  String? _getCurrencyNums(int index) {
    switch (widget.currency) {
      case Const.CURRENCY_BEAN:
        return "+${YBDNumericUtil.format(_dataList![index].beans)}";
        break;
      case Const.CURRENCY_GEM:
        return "+${YBDNumericUtil.format(_dataList![index].gems)}";
        break;
      case Const.CURRENCY_GOLD:
        String? gold = YBDNumericUtil.compactNumberWithFixedDigits(
          '${_dataList![index].golds ?? 0}',
          fixedDigits: 2,
          removeZero: false,
          rounding: false,
          goldType: true,
        );
        return "+$gold";
        break;
      default:
        return _dataList![index].beans;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: YBDMyAppBar(
        title: Text(
          _getTitle(),
          style: TextStyle(color: Colors.white, fontSize: YBDTPStyle.spNav),
        ),
        backgroundColor: YBDTPStyle.heliotrope,
        leading: YBDNavBackButton(),
        elevation: 0.0,
      ),
      body: Container(
        width: double.infinity,
        decoration: YBDTPStyle.gradientDecoration,
        child: _recordBody(),
      ),
    );
  }
  void buildaXbkBoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _separateLine() {
    return Container(
      height: _w(1),
      color: Colors.white.withOpacity(0.1),
      margin: EdgeInsets.symmetric(horizontal: _w(30)),
    );
  }

  Widget _recordBody() {
    Future.delayed(Duration(seconds: 5), () {
      if (_isTimeout && !_isReseted) {
        setState(() {
          _isReseted = true;
        });
      }
    });
    if (_dataList == null) return Center(child: YBDLoadingCircle());
    return _dataList!.length == 0
        ? YBDEmptyView()
        : SmartRefresher(
            controller: _refreshController,
            header: YBDMyRefreshIndicator.myHeader,
            footer: YBDMyRefreshIndicator.myFooter,
            enablePullDown: true,
            enablePullUp: true,
            onRefresh: _getRecord,
            onLoading: _nextPage,
            child: ListView.builder(
                itemCount: _dataList!.length,
                itemBuilder: (_, index) => Column(
                      children: [
                        Container(
                          height: _w(130),
                          padding: EdgeInsets.symmetric(horizontal: _w(30)),
                          child: Row(
                            children: [
                              Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    _dataList![index].type,
                                    style: TextStyle(fontSize: _sp(28), color: Colors.white),
                                  ),
                                  SizedBox(height: _w(6)),
                                  Text(
                                    _getDate(_dataList![index].createTime),
                                    style: TextStyle(fontSize: _sp(22), color: Colors.white.withOpacity(0.6)),
                                  ),
                                ],
                              ),
                              Spacer(),
                              _getCurrencyIcon(),
                              SizedBox(width: _w(12)),
                              Text(
                                _getCurrencyNums(index)!,
                                style: TextStyle(fontSize: _sp(28), color: Colors.white),
                              )
                            ],
                          ),
                        ),
                        _separateLine(),
                      ],
                    )),
          );
  }
  void _recordBodyh5ZyBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  double _w(int width) {
    return ScreenUtil().setWidth(width);
  }

  double _sp(int sp) {
    return ScreenUtil().setSp(sp);
  }
}
