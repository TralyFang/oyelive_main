import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/constant/const.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/image_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../home/entity/banner_ybd_advertise_entity.dart';

class YBDTopUpBanner extends StatefulWidget {
  @override
  YBDTopUpBannerState createState() => new YBDTopUpBannerState();
}

class YBDTopUpBannerState extends BaseState<YBDTopUpBanner> {
  List<YBDBannerAdvertiseRecord?>? banners;

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    if (null != banners && banners!.isNotEmpty) {
//      logger.v('ad banners amount : ${banners.length}');
      return Container(
        height: ScreenUtil().setWidth(238),
        child: Swiper(
          itemCount: banners!.length,
          autoplay: true,
          autoplayDelay: 5000,
          outer: false,
          onTap: (index) {
            logger.v('clicked top up banner : $index, ${banners![index]!.toJson()}');
            String url = banners![index]!.adurl!;
            if (url.startsWith('flutter')) {
              logger.v('url startsWith : $url');
              YBDNavigatorHelper.navigateTo(context, url);
            }

            if (url.startsWith('http')) {
              // if (Platform.isIOS) {

              YBDNavigatorHelper.navigateToWeb(context,
                  "?url=${Uri.encodeComponent(url)}&title=${banners![index]!.adname}&shareImg=${banners![index]!.img}");

              // } else {
              // FlutterBoost.singleton.open(getNativePageUrl(NativePageName.WEB_VIEW), urlParams: {
              //   'EVENT_KEY': 'event',
              //   'TITLE_KEY': banners[index].adname,
              //   'URL_KEY': url,
              //   'AD_IMG': banners[index].img,
              //   'HIDE_TITLE': false
              // });
              // }
            }

            YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
                location: YBDLocationName.RECHARGE_PAGE, itemName: banners![index]!.adname));
          },
          pagination: SwiperPagination(
              alignment: Alignment.bottomRight,
              margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30), vertical: ScreenUtil().setWidth(10)),
              builder: DotSwiperPaginationBuilder(
                  size: ScreenUtil().setWidth(10),
                  activeSize: ScreenUtil().setWidth(10),
                  color: Colors.white,
                  activeColor: YBDTPStyle.heliotrope)),
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(0))),
                  child: YBDNetworkImage(
                    imageUrl: YBDImageUtil.ad(context, banners![index]!.img, 'A'),
                    width: ScreenUtil().setWidth(670),
                    height: ScreenUtil().setWidth(238),
                    fit: BoxFit.cover,
                  )),
            );
          },
        ),
      );
    } else {
      return Container(
          // child: Text('empty ad view'),
          );
    }
  }
  void myBuildwJ3GGoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getBanner() async {
    YBDBannerAdvertiseEntity? bannerAdvertiseEntity = await ApiHelper.homeBannerAdvertise(context, adType: 6);
    if (bannerAdvertiseEntity?.returnCode == Const.HTTP_SUCCESS) {
      banners = bannerAdvertiseEntity?.record ?? [];
      setState(() {});
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getBanner();
  }
  void initStateRRJRFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDTopUpBanner oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetw6aNroyelive(YBDTopUpBanner oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
