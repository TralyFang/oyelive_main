import 'dart:async';

import '../../../../generated/json/base/json_convert_content.dart';

class YBDQueryRechargeRecordEntity with JsonConvert<YBDQueryRechargeRecordEntity> {
  String? code;
  String? returnMsg;
  List<YBDRechargeRecord?>? data;
}

class YBDRechargeRecord with JsonConvert<YBDRechargeRecord> {
  int? id;
  int? createTime;
  int? money;
  int? virtualCurrencyAmount;
  String? type;
  String? transid;
  String? status;
}
