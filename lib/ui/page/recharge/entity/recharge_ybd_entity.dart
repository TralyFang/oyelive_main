import 'dart:async';

enum RechargeType {
  Google,
  Paypal,
  Easypaisa,
  Agents,
  Razorpay,
  PayMAX,
}

class YBDRechargeEntity {
  String name;
  RechargeType type;
  String iconUrl;
  bool support;

  YBDRechargeEntity(this.name, this.type, this.iconUrl, this.support);
}
