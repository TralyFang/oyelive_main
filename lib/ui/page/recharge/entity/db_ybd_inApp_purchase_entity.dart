

final String inAppPurchaseTableName = 'inApp_purchase_history';
final String inAppPurchaseTemTableName = 'inApp_purchase_tem_history';
final String cUserID = 'userID';
final String cPurchaseUserID = 'purchaseUserID';

///己方服务器生成的订单ID
final String cOrderID = 'orderID';
final String cSignedData = 'signedData';
final String cSignature = 'signature';

///服务方(google)提供的订单ID
final String cSPOrderID = 'spOrderID';

///创建时间
final String cCreateTime = 'createTime';

/// 服务端状态  0：默认（未校验） 1：校验成功 2：校验失败
final String cServiceState = 'serviceState';

///渠道状态 0 购买成功 1 消耗成功 2消耗失败
final String cChannelState = 'channelState';

///渠道 0、google（默认） 1、Apple
final String cChannel = 'channel';

const int CHANNEL_STATE_PURCHASE = 0;
const int CHANNEL_STATE_CONSUME_SUCCESS = 1;
const int CHANNEL_STATE_CONSUME_FAILED = 2;

const int STATE_DEFAULT = 0;
const int STATE_VERIFY_SUCCESS = 1;
const int STATE_VERIFY_FAILED = 2;

class YBDDBInAppPurchaseEntity {
  int? usrId;
  String? purchaseUserID;
  String? orderID;
  String? signedData;
  String? signature;
  String? spOrderID;
  int? serviceState;
  int? createTime;
  int? channel;
  int? channelState;

  YBDDBInAppPurchaseEntity(this.usrId, this.purchaseUserID, this.orderID, this.signedData, this.signature, this.spOrderID,
      this.serviceState, this.createTime, this.channel, this.channelState);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
//      columnId: id,
      cUserID: usrId,
      cPurchaseUserID: purchaseUserID,
      cOrderID: orderID,
      cSignedData: signedData,
      cSignature: signature,
      cSPOrderID: spOrderID,
      cServiceState: serviceState,
      cCreateTime: createTime,
      cChannel: channel,
      cChannelState: channelState,
    };
    return map;
  }

  YBDDBInAppPurchaseEntity.fromMap(Map<String, dynamic> map) {
    usrId = map[cUserID];
    purchaseUserID = map[cPurchaseUserID];
    orderID = map[cOrderID];
    signedData = map[cSignedData];
    signature = map[cSignature];
    spOrderID = map[cSPOrderID];
    serviceState = map[cServiceState];
    createTime = map[cCreateTime];
    channel = map[cChannel];
    channelState = map[cChannelState];
  }

  @override
  String toString() {
    return 'YBDDBInAppPurchaseEntity{usrId: $usrId, purchaseUserID: $purchaseUserID, orderID: $orderID, signedData: $signedData, signature: $signature, spOrderID: $spOrderID, serviceState: $serviceState, createTime: $createTime, channel: $channel, channelState: $channelState}';
  }
  void toStringgBC5Coyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
