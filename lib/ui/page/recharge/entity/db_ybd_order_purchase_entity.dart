

final String orderPurchaseTableName = 'order_purchase_history';
final String orderPurchaseTemTableName = 'order_purchase_tem_history';
final String orderUserID = 'userID';

///己方服务器生成的订单ID
final String orderOrderID = 'orderID';
//google产品ID
final String orderProductID = 'productID';
//预订单创建时间
final String orderCreateTime = 'createTime';

final String orderSignedData = 'signedData';

final String orderSignature = 'signature';

///服务方(google 苹果)提供的订单ID
final String orderSPOrderID = 'spOrderID';

///渠道 0 google（默认） 1、Apple
final String orderChannel = 'channel';

/// 0 创建 1 成功绑定第三方订单 2 订单结束（支付完成）
final String orderState = 'state';

class YBDDBOrderPurchaseEntity {
  int? usrId;
  String? orderID;
  String? productID;
  int? createTime;
  String? signedData;
  String? signature;
  String? spOrderID;
  String? channel;
  String? state;

  YBDDBOrderPurchaseEntity(this.usrId, this.orderID, this.productID, this.createTime, this.signedData, this.signature,
      this.spOrderID, this.channel, this.state);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
//      columnId: id,
      orderUserID: usrId,
      orderOrderID: orderID,
      orderProductID: productID,
      orderCreateTime: createTime,
      orderSignedData: signedData,
      orderSignature: signature,
      orderSPOrderID: spOrderID,
      orderChannel: channel,
      orderState: state,
    };
    return map;
  }

  YBDDBOrderPurchaseEntity.fromMap(Map<String, dynamic> map) {
    usrId = map[orderUserID];
    orderID = map[orderOrderID];
    productID = map[orderProductID];
    createTime = map[orderCreateTime];
    signedData = map[orderSignedData];
    signature = map[orderSignature];
    spOrderID = map[orderSPOrderID];
    channel = '${map[orderChannel]}';
    state = '${map[orderState]}';
  }

  @override
  String toString() {
    return 'YBDDBOrderPurchaseEntity{usrId: $usrId, orderID: $orderID, productID: $productID, createTime: $createTime, signedData: $signedData, signature: $signature, spOrderID: $spOrderID, channel: $channel, state: $state}';
  }
  void toStringkXkZeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
