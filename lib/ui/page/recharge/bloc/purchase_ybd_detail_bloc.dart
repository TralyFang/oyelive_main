import 'dart:async';

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

import '../../../../common/service/iap_service/iap_ybd_service.dart';
import '../../../../common/util/log_ybd_util.dart';

enum PurchaseDetailEvent {
  // 初始化数据
  LoadProduct,
  // 已获取到商品列表
  LoadedProduct,
}

/// 购买详情页面 bloc
class YBDPurchaseDetailBloc extends Bloc<PurchaseDetailEvent, YBDPurchaseDetailBlocState> {
  BuildContext context;

  /// 商品详情列表
  List<ProductDetails> _products = [];

  YBDPurchaseDetailBloc(this.context) : super(YBDPurchaseDetailBlocState(isLoading: true));

  @override
  Stream<YBDPurchaseDetailBlocState> mapEventToState(PurchaseDetailEvent event) async* {
    switch (event) {
      case PurchaseDetailEvent.LoadProduct:
        logger.v('===load product event');
        // 初始化商品列表数据
        await _queryProductList();
        break;
      case PurchaseDetailEvent.LoadedProduct:
        yield (YBDPurchaseDetailBlocState(products: _products));
        break;
    }
  }
  void mapEventToStateestOsoyelive(PurchaseDetailEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 初始化商店信息
  Future<void> _queryProductList() async {
    // 没有商品列表再刷新一次
    if (null == YBDIapService.instance.products || YBDIapService.instance.products.isEmpty) {
      logger.v('===product list is empty refresh');
      await YBDIapService.instance.configProductList();
    }

    _products.addAll(YBDIapService.instance.products);

    // 已设置好商品列表, 刷新页面
    add(PurchaseDetailEvent.LoadedProduct);
  }
  void _queryProductListbgjgGoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDPurchaseDetailBlocState {
  /// 是否正在加载数据
  bool isLoading;

  /// 商品详情列表
  List<ProductDetails>? products = [];

  YBDPurchaseDetailBlocState({
    this.isLoading = false,
    this.products,
  });
}
