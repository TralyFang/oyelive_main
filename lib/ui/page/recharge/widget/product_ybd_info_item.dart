import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 商品详情 item
class YBDProductInfoItem extends StatelessWidget {
  /// 是否被选中
  final bool isSelected;

  /// 豆子数
  final String? beans;

  /// 价格
  final String price;

  YBDProductInfoItem({
    this.isSelected = false,
    this.beans = '-',
    this.price = '-',
  });

  @override
  Widget build(BuildContext context) {
    BoxDecoration decoration;

    if (isSelected) {
      decoration = BoxDecoration(
        borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
        gradient: LinearGradient(
          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      );
    } else {
      decoration = BoxDecoration(
        borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
        color: Colors.white.withOpacity(0.1),
      );
    }

    // return Container(
    //   color: Colors.yellow,
    // );
    return Container(
      width: ScreenUtil().setWidth(213),
      height: ScreenUtil().setWidth(168),
      decoration: decoration,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // 豆子
          _beansRow(),
          SizedBox(
            height: ScreenUtil().setWidth(14),
          ),
          // 价格
          _priceRow(),
        ],
      ),
    );
  }
  void buildsTZoAoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 豆子
  Widget _beansRow() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: ScreenUtil().setWidth(25),
//            height: ScreenUtil().setWidth(50),
            child: Image.asset('assets/images/topup/y_top_up_beans@2x.webp'),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(2),
          ),
          Text(
            beans ?? '',
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(32),
            ),
          ),
        ],
      ),
    );
  }
  void _beansRowf57cGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 价格
  _priceRow() {
    return Container(
      child: Text(
        price,
        style: TextStyle(
          color: Colors.white.withOpacity(0.8),
          fontSize: ScreenUtil().setSp(24),
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }
}
