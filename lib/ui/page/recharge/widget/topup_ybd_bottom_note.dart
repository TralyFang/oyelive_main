import 'dart:async';

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/config_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';

/// ios 充值页面底部提示
class YBDTopupBottomNote extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _YBDTopupBottomNoteState();
}

class _YBDTopupBottomNoteState extends State<YBDTopupBottomNote> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: ScreenUtil().setWidth(36)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            translate('note'),
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(24),
            ),
          ),
          SizedBox(height: ScreenUtil().setWidth(10)),
          if (Platform.isIOS) _contactUsRow(),
          _supportRow(),
          SizedBox(height: ScreenUtil().setWidth(10)),
          _delayWarnRow(),
          SizedBox(height: ScreenUtil().setWidth(20)),
        ],
      ),
    );
  }
  void buildzIp6uoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// contact us 栏
  Widget _contactUsRow() {
    return Container(
      child: Row(
        children: [
          _dot(),
          SizedBox(width: ScreenUtil().setWidth(10)),
          _richText(translate('contact_us'), translate('contact_us_link'), () async {
            logger.v('clicked contact us btn');
            final title = translate('contact_us_link');
            final url = await ConfigUtil.resellerUrl(context);
            YBDNavigatorHelper.navigateToWeb(
              context,
              "?url=${Uri.encodeComponent(url)}&title=$title",
            );
          }),
        ],
      ),
    );
  }
  void _contactUsRowvCUU0oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 原点
  Widget _dot() {
    return Container(
      width: ScreenUtil().setWidth(6),
      height: ScreenUtil().setWidth(6),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white.withOpacity(0.7),
      ),
    );
  }
  void _dot9nfaYoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// support 栏
  Widget _supportRow() {
    return Container(
      child: Row(
        children: [
          _dot(),
          SizedBox(width: ScreenUtil().setWidth(10)),
          _richText(translate('top_up_help'), translate('support'), () {
            logger.v('clicked support btn');
            YBDCommonUtil.showSupport(context);
          }),
        ],
      ),
    );
  }
  void _supportRow8xcFdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// _delayWarn 栏
  Widget _delayWarnRow() {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(8)),
            child: _dot(),
          ),
          SizedBox(width: ScreenUtil().setWidth(10)),
          _richText(translate('top_up_delay_warn'), '', () {}),
        ],
      ),
    );
  }
  void _delayWarnRowi9Kveoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 文字
  Widget _richText(String whiteText, String blueText, Function onTap) {
    return Text.rich(
      TextSpan(
        style: TextStyle(
          fontSize: ScreenUtil().setSp(22),
          color: Colors.white.withOpacity(0.7),
        ),
        children: [
          TextSpan(
            text: whiteText,
          ),
          TextSpan(
            text: translate('  '),
          ),
          WidgetSpan(
            child: GestureDetector(
              onTap: () {
                onTap();
              },
              child: Text(
                blueText,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(22),
                  color: Color(0xFF7DFAFF),
                  height: 1,
                  decoration: TextDecoration.underline,
                ),
              ),
            ),
          ),
        ],
      ),
      maxLines: 2,
    );
  }
  void _richTextB1QAgoyelive(String whiteText, String blueText, Function onTap) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
