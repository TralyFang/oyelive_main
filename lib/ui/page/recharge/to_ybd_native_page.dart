import 'dart:async';

// import 'package:flutter_boost/flutter_boost.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

class YBDRechargeToNative {
  /// 跳转原生webview
  static void toNativeWebView(String title, String url, {String? postValue}) {
    logger.v('toNativeWebView title:$title url:$url');

    // TODO: 测试代码 添加跳转
    // FlutterBoost.singleton.open(getNativePageUrl(NativePageName.WEB_VIEW_PAY), urlParams: {
    //   'title': title,
    //   'url': url,
    //   'postValue': postValue,
    // });
  }

  ///原生支付页面  Razorpay支付使用原生SDK，混合包不能使用flutter插件：crash
  static void toNativeRecharge(int topUpType, double amount, String channel, String id, String orderId) {
    // TODO: 测试代码 添加跳转
    // FlutterBoost.singleton.open(getNativePageUrl(NativePageName.TOP_UP), urlParams: {
    //   'topUpType': topUpType,
    //   'amount': amount,
    //
    //   /// 暂不需要productId  ：-1
    //   'productId': '-1',
    //   'channel': channel,
    //   'id': id,
    //   'orderId': orderId,
    // });
  }
}
