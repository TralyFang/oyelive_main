import 'dart:async';

import 'dart:convert';

import 'package:advertising_id/advertising_id.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:package_info/package_info.dart';
import 'package:pay_web/pay_web.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/config_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../common/widget/input_ybd_box_container.dart';
import '../../../../common/widget/square_ybd_checkbox.dart';
import '../../../../module/payment/payment_ybd_api.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';
import '../../status/local_audio/blue_ybd_gradient_button.dart';
import 'entity/easypaisa_ybd_pay_body_entity.dart';

class YBDEasypaisaPage extends StatefulWidget {
  ///_easyPaisaType = "MA"; OTC
  final String easypaisaType;
  final int amount;
  final String? accountNumber;
  final String? currency;

  YBDEasypaisaPage(this.easypaisaType, this.amount, this.currency, {this.accountNumber});

  @override
  _YBDEasypaisaPageState createState() => new _YBDEasypaisaPageState();
}

class _YBDEasypaisaPageState extends BaseState<YBDEasypaisaPage> with SingleTickerProviderStateMixin {
  TextEditingController? emailController;
  TextEditingController? accountNumberController;

  bool? agreeTerms;

  @override
  void initState() {
    super.initState();
    emailController = new TextEditingController(); //声明controller
    accountNumberController = new TextEditingController(text: widget.accountNumber); //声明controller
    agreeTerms = false;
    logger.v(
        'YBDEasypaisaPage  easypaisaType:${widget.easypaisaType} amount:${widget.amount} widget.accountNumber:${widget.accountNumber}');
  }
  void initStatehmN34oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposeSJjFToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    logger.v('refresh YBDRechargePage with state : $state');
  }
  void didChangeAppLifecycleStategXiTjoyelive(AppLifecycleState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        decoration: YBDTPStyle.gradientDecoration,
        child: SingleChildScrollView(
          child: Container(
            // 页面背景色
//          padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
            width: double.infinity,
            padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
            child: Column(
              children: [
                Container(
                  width: ScreenUtil().screenWidth,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage('assets/images/bg_red.png'), fit: BoxFit.fitWidth),
                  ),
                  child: Column(
                    children: [
                      Container(
                        width: ScreenUtil().screenWidth,
                        height: ScreenUtil().setWidth(116),
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                                // 导航栏返回按钮
                                top: ScreenUtil().setWidth(15),
                                left: 0,
                                child: YBDNavBackButton()),
                            Center(
                              child: Text(
                                widget.easypaisaType == 'OTC'
                                    ? translate('easypaisa_shop')
                                    : translate('easypaisa_mobile_account'),
                                style: TextStyle(fontSize: ScreenUtil().setSp(32), color: Colors.white),
                              ),
                            ),
                            Positioned(
                                top: ScreenUtil().setWidth(35),
                                right: ScreenUtil().setWidth(24),
                                child: GestureDetector(
                                  onTap: () {
                                    print('more------');
                                    YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.top_up_record);
                                  },
                                  child: Image.asset('assets/images/icon_more.png',
                                      width: ScreenUtil().setWidth(48), height: ScreenUtil().setWidth(48)),
                                )),
                          ],
                        ),
                      ),
                      Container(
                        width: ScreenUtil().screenWidth,
                        height: ScreenUtil().setWidth(570),
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(30),
                            right: ScreenUtil().setWidth(30),
                            top: ScreenUtil().setWidth(50)),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(ScreenUtil().setWidth(16)),
                          color: Colors.white.withOpacity(0.1),
                        ),
                        child: Container(
                          alignment: Alignment.center, //指定未定位或部分
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(
                                  top: ScreenUtil().setWidth(79),
                                ),
                                child: Image.asset('assets/images/icon_dunpai.webp',
                                    height: ScreenUtil().setWidth(138),
                                    width: ScreenUtil().setWidth(120),
                                    fit: BoxFit.contain),
                              ),
                              SizedBox(height: ScreenUtil().setWidth(58)),
                              Container(
                                height: ScreenUtil().setWidth(100),
                                padding: EdgeInsets.only(
                                  right: ScreenUtil().setWidth(49),
                                  left: ScreenUtil().setWidth(49),
                                ),
                                child: Text(
                                    'We do not save your personal information, neither it will be used for any wrong purpose or harmful for you',
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(24),
                                      color: Colors.white,
                                    )),
                              ),
                              SizedBox(height: ScreenUtil().setWidth(14)),
                              Container(
                                padding: EdgeInsets.only(
                                  right: ScreenUtil().setWidth(49),
                                  left: ScreenUtil().setWidth(59),
                                ),
                                child: Text(
                                  'We promise to keep all the information you share with us confidential and safe.',
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(24),
                                    color: Color(0xFF7DFAFF),
                                  ),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setWidth(45)),
                              Container(
                                margin: EdgeInsets.only(
                                  right: ScreenUtil().setWidth(40),
                                  left: ScreenUtil().setWidth(50),
                                ),
                                child: Container(
                                  height: ScreenUtil().setWidth(60),
                                  // color: Colors.tealAccent,
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(
                                        // width: ScreenUtil().setWidth(5),
                                        height: ScreenUtil().setWidth(60),
                                      ),
                                      Container(
                                        height: ScreenUtil().setWidth(50),
                                        width: ScreenUtil().setWidth(50),
                                        child: YBDSquareCheckBox(
                                            value: (agreeTerms),
                                            onChanged: (check) {
                                              setState(() {
                                                agreeTerms = check;
                                                logger.v("YBDSquareCheckBox check $check  agreeTerms:$agreeTerms");
                                              });
                                            }),
                                      ),
                                      Text('I agree to the Terms and Conditions',
                                          style: TextStyle(
                                            color: Colors.white.withOpacity(0.5),
                                            fontSize: ScreenUtil().setSp(20),
                                          )),
                                    ],
                                  ),
                                ),
                                // ),
                              ),
                              SizedBox(height: ScreenUtil().setWidth(10)),
                            ],
                          ), // 定位widget的对齐方式
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(50)),
                      Container(
                        margin: EdgeInsets.only(
                          right: ScreenUtil().setWidth(30),
                          left: ScreenUtil().setWidth(30),
                        ),
                        width: ScreenUtil().setWidth(660),
                        height: ScreenUtil().setWidth(80),
                        child: YBDInputBoxContainer(
                          controller: accountNumberController,
                          hintText: 'Account Number',
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(30)),
                      Container(
                        margin: EdgeInsets.only(
                          right: ScreenUtil().setWidth(30),
                          left: ScreenUtil().setWidth(30),
                        ),
                        width: ScreenUtil().setWidth(660),
                        height: ScreenUtil().setWidth(80),
                        child: YBDInputBoxContainer(
                          controller: emailController,
                          hintText: 'Email(Optional)',
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(90)),
                      _submitBtn(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  void myBuildlokweoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 提交按钮
  Widget _submitBtn() {
    return Container(
      child: YBDDelayGestureDetector(
        // 提交按钮
        child: YBDBlueGradientButton(
          title: 'Next',
          width: 500,
          height: 74,
        ),
        onTap: submit,
      ),
    );
  }

  submit() async {
    YBDLogUtil.d(
        'EP submit accountNumber:${accountNumberController!.text} Email:${emailController!.text} agreeTerms:$agreeTerms');

    ///基本条件校验
    if (accountNumberController!.text.isEmpty || accountNumberController!.text.length < 3) {
      YBDToastUtil.toast('Please enter a valid number');
      return;
    }
    if (!agreeTerms!) {
      YBDToastUtil.toast('Please agree to the terms and conditions to continue');
      return;
    }
    await YBDSPUtil.saveEPAccountNUmber(accountNumberController!.text);
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    String paymentServerUrl = await ConfigUtil.razorpayServerUrl(context);
    bool isTest = await isTestEvn();
    String url = await paymentServerUrlWithEnv(isTest, paymentServerUrl);
    int? userId = store.state.bean!.id;

    String? advertisingId;
    String? flyerUID;
    PackageInfo? packageInfo;
    //订单添加flyerId atid version
    try {
      packageInfo = await PackageInfo.fromPlatform();
      flyerUID = await YBDAnalyticsUtil.appsflyerSdk?.getAppsFlyerUID();
      advertisingId = await AdvertisingId.id(true);
    } on PlatformException catch (e) {
      logger.d('get phone info error:' + e.toString());
    }
    YBDEasypaisaPayBodyEntity easypaisaPayBodyEntity = new YBDEasypaisaPayBodyEntity();
    easypaisaPayBodyEntity.userId = userId;
    easypaisaPayBodyEntity.money = widget.amount;
    easypaisaPayBodyEntity.currency = widget.currency;
    easypaisaPayBodyEntity.channel = widget.easypaisaType;
    easypaisaPayBodyEntity.country = await YBDSPUtil.get(Const.SP_TOP_UP_COUNTRY_CODE);
    easypaisaPayBodyEntity.msisdn = accountNumberController!.text;
    easypaisaPayBodyEntity.mobileAccountNo = accountNumberController!.text;
    easypaisaPayBodyEntity.email = emailController!.text;
    easypaisaPayBodyEntity.advertisingId = advertisingId;
    easypaisaPayBodyEntity.appsflyerId = flyerUID;
    easypaisaPayBodyEntity.appVersionName = packageInfo?.version;

    String postValue = json.encode(easypaisaPayBodyEntity.toJson());
    logger.d('EP submit paymentServerUrl:$paymentServerUrl isTest:$isTest url:$url, postValue: $postValue');
    logger.d(
        'EP submit paymentServerUrl advertisingId:$advertisingId flyerUID:$flyerUID appVersionName:${packageInfo?.version}, postValue: ${easypaisaPayBodyEntity.appsflyerId}');
    // YBDRechargeToNative.toNativeWebView('Easypaisa', url + YBDPaymentApi.EPPAY_ORDERS, postValue: postValue);
    PayWeb.openWebPayView(url + YBDPaymentApi.EPPAY_ORDERS, 'Easypaisa', postValue);
  }

  /// 判断环境
  Future<bool> isTestEvn() async {
    dynamic isTestEnv = await YBDSPUtil.get(Const.SP_TEST_ENV);
    if (isTestEnv == null) {
      return Const.TEST_ENV;
    } else {
      return isTestEnv;
    }
  }
  void isTestEvnzxM6boyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取地址
  /// [paymentServerUrl] 从配置项获取的值，要屏蔽测试环境的 url
  Future<String> paymentServerUrlWithEnv(bool isTestEnv, String paymentServerUrl) async {
    String url = paymentServerUrl;
    if (isTestEnv) {
      if (paymentServerUrl == null) {
        url = Const.TEST_RPS_URL;
      }
    } else {
      if (paymentServerUrl == null || paymentServerUrl.contains("9020")) {
        url = Const.RPS_URL;
      }
    }
    return url;
  }
}
