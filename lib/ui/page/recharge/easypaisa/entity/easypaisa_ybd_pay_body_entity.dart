import 'dart:async';

import '../../../../../generated/json/base/json_convert_content.dart';

class YBDEasypaisaPayBodyEntity with JsonConvert<YBDEasypaisaPayBodyEntity> {
  int? userId;
  int? money;
  String? currency;
  String? channel;
  String? country;
  String? msisdn;
  String? mobileAccountNo;
  String? email;
  String? advertisingId;
  String? appsflyerId;
  String? appVersionName;
}
