import 'dart:async';

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/constant/const.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/image_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import '../../../module/payment/entity/pay_ybd_config_entity.dart';
import '../../../module/payment/payment_ybd_api_helper.dart';
import '../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../module/user/util/user_ybd_util.dart';
import '../../widget/loading_ybd_circle.dart';
import '../../widget/my_ybd_refresh_indicator.dart';
import '../../widget/scaffold/nav_ybd_back_button.dart';
import '../../widget/select_ybd_country.dart';
import '../home/entity/banner_ybd_advertise_entity.dart';
import '../status/local_audio/scale_ybd_animate_button.dart';
import 'recharge_ybd_choose_item.dart';
import 'recharge_ybd_choose_parent.dart';
import 'top_ybd_up_banner.dart';
import 'widget/topup_ybd_bottom_note.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';

class YBDRechargePage extends StatefulWidget {
  String location = '';

  YBDRechargePage({this.location: ''});

  @override
  _YBDRechargePageState createState() => new _YBDRechargePageState();
}

class _YBDRechargePageState extends BaseState<YBDRechargePage> with SingleTickerProviderStateMixin {
  YBDUserInfo? userInfo;
  String? countryName;

  String? countryCode;
  YBDPayConfigEntity? _payConfigsEntity;

  getCountryCode() async {
    String? topupCountryCode = await YBDSPUtil.get(Const.SP_TOP_UP_COUNTRY_CODE);
    if (topupCountryCode == null || topupCountryCode.isEmpty) {
      topupCountryCode = (await YBDSPUtil.getUserInfo())!.country;
    }
    await YBDSPUtil.save(Const.SP_TOP_UP_COUNTRY_CODE, topupCountryCode);
    countryName = YBDCommonUtil.getCountryByCode(topupCountryCode);
    countryCode = topupCountryCode;
    setState(() {});
  }

  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    YBDCommonTrack().commonTrack(YBDTAProps(
      location: YBDTAClickName.topup,
      module: YBDTAModule.common,
      previous_route: Get.previousRoute.shortRoute,
    ));
    _getData();
    YBDUserUtil.userInfo().then((value) {
      userInfo = value;
      if (mounted) {
        setState(() {});
      }
    });
  }
  void initStatejxMH1oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposeXoFT8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    logger.v('refresh YBDRechargePage with state ----: $state');
    switch (state) {
      case AppLifecycleState.resumed:
        Future.delayed(Duration(milliseconds: 2000), () async {
          ///原生支付回来后  更新金币  ---在上一层的resume已经请求
          userInfo = await YBDUserUtil.userInfo();
          logger.v('refresh YBDRechargePage userInfo.money: ${userInfo!.money}');
          if (mounted) {
            setState(() {});
          }
        });
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
    }
  }
  void didChangeAppLifecycleStateuKp6Aoyelive(AppLifecycleState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _getData() async {
    await getCountryCode();
    YBDPayConfigEntity? payConfigsEntity = await YBDPaymentApiHelper.queryPayConfigs(context, 'pay_configs', countryCode);
    if (payConfigsEntity != null && payConfigsEntity.code == Const.HTTP_SUCCESS_NEW) {
      _payConfigsEntity = payConfigsEntity;
    }

    ///获取实时金币 同步到本地用户信息
    ApiHelper.getBalance(context, () async {
      userInfo = await YBDUserUtil.userInfo();
      if (mounted) {
        setState(() {});
      }
    });
  }

  getPayLayout() {
    if (_payConfigsEntity == null) {
      return Container(
        height: ScreenUtil().setWidth(400),
        child: Center(
          child: YBDLoadingCircle(),
        ),
      );
    }
    if (_payConfigsEntity!.data == null) {
      return Container();
    }
    return Column(
      children: <Widget>[
        ListView.separated(
          shrinkWrap: true,
          padding: EdgeInsets.all(0),
          physics: NeverScrollableScrollPhysics(),
          itemCount: _payConfigsEntity!.data!.length,
          itemBuilder: (_, index) {
            if (_payConfigsEntity!.data![index]!.child == null) {
              logger.v('topup gift location recharge page : ${widget.location}');
              return YBDRechargeChooseItem(
                _payConfigsEntity!.data![index]!.nickName,
                _payConfigsEntity!.data![index]!.icon,
                _payConfigsEntity!.data![index]!.suffixIcon,
                _payConfigsEntity!.data![index]!.currency,
                _payConfigsEntity!.data![index]!.name,
                items: _payConfigsEntity!.data![index]!.items?.removeNulls(),
                rate: _payConfigsEntity!.data![index]!.rate,
                payload: _payConfigsEntity!.data![index]!.payload,
                location: widget.location,
                backCallbackFunc: (i) {
                  _onRefresh();
                },
              );
            } else {
              return YBDRechargeChooseParent(
                _payConfigsEntity!.data![index],
                backCallbackFunc: (i) {
                  _onRefresh();
                },
              );
            }
          },
          separatorBuilder: (_, index) => Padding(
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
            child: Divider(
              height: ScreenUtil().setWidth(1),
              thickness: ScreenUtil().setWidth(1),
              color: Colors.white.withOpacity(0.15),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
          child: Divider(
            height: ScreenUtil().setWidth(1),
            thickness: ScreenUtil().setWidth(1),
            color: Colors.white.withOpacity(0.15),
          ),
        )
      ],
    );
  }

  _onTapChat() async {
    print('_onTapChat');
    YBDCommonUtil.showSupport(context);
  }

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      floatingActionButton: Container(
        margin: EdgeInsets.only(
          bottom: ScreenUtil().setWidth(ScreenUtil().setWidth(50)),
        ),
        child: YBDScaleAnimateButton(
          onTap: _onTapChat,
          child: Image.asset(
            'assets/images/customer_care_ico.webp',
            width: ScreenUtil().setWidth(100),
            height: ScreenUtil().setWidth(100),
            fit: BoxFit.cover,
          ),
        ),
      ),
      body: Container(
          height: double.infinity,
          decoration: YBDTPStyle.gradientDecoration,
          child: SmartRefresher(
              controller: _refreshController,
              header: YBDMyRefreshIndicator.myHeader,
              footer: YBDMyRefreshIndicator.myFooter,
              enablePullDown: true,
              enablePullUp: false,
              onRefresh: () {
                logger.v("YBDRechargePage page pull down refresh");
                _onRefresh();
              },
              child: _content())

//         SingleChildScrollView(
//           child: Container(
//             // 页面背景色
// //          padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
//             width: double.infinity,
//             padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
//             child: Column(
//               children: [
//                 Container(
//                   width: ScreenUtil().screenWidth,
//                   height: ScreenUtil().setWidth(402),
//                   decoration: BoxDecoration(
//                     image: DecorationImage(image: AssetImage('assets/images/bg_red.png'), fit: BoxFit.fitWidth),
//                   ),
//                   child: Column(
//                     children: [
//                       Container(
//                         width: ScreenUtil().screenWidth,
//                         height: ScreenUtil().setWidth(116),
//                         child: Stack(
//                           children: <Widget>[
//                             Positioned(
//                                 // 导航栏返回按钮
//                                 top: ScreenUtil().setWidth(15),
//                                 left: 0,
//                                 child: YBDNavBackButton()),
//                             Center(
//                               // 关注和粉丝选项卡
//                               child: Text(
//                                 translate("top_up"),
//                                 style: TextStyle(fontSize: ScreenUtil().setSp(32), color: Colors.white),
//                               ),
//                             ),
//                             Positioned(
//                                 top: ScreenUtil().setWidth(35),
//                                 right: ScreenUtil().setWidth(24),
//                                 child: GestureDetector(
//                                   onTap: () {
//                                     print('more------');
//                                     YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.top_up_record);
//                                   },
//                                   child: Image.asset('assets/images/icon_more.png',
//                                       width: ScreenUtil().setWidth(48), height: ScreenUtil().setWidth(48)),
//                                 )),
//                           ],
//                         ),
//                       ),
//                       Container(
//                         width: ScreenUtil().screenWidth,
//                         height: ScreenUtil().setWidth(286),
//                         child: Stack(
//                           alignment: Alignment.center, //指定未定位或部分定位widget的对齐方式
//                           children: <Widget>[
//                             Positioned(
//                               top: ScreenUtil().setWidth(70),
//                               child: Image.asset('assets/images/topup/y_top_up_beans@2x.webp',
//                                   height: ScreenUtil().setWidth(130), fit: BoxFit.fitHeight),
//                             ),
//                             Positioned(
//                                 top: ScreenUtil().setWidth(209),
//                                 child: Align(
//                                   alignment: Alignment.center,
//                                   child: Row(
//                                     children: [
//                                       Text(translate('available_beans'),
//                                           style: TextStyle(
//                                               fontSize: ScreenUtil().setSp(24),
//                                               color: Colors.white.withOpacity(0.6),
//                                               fontWeight: FontWeight.w400)),
//                                       SizedBox(width: ScreenUtil().setWidth(24)),
//                                       Text(
//                                         '${YBDCommonUtil.formatNum(userInfo?.money)}',
//                                         style: TextStyle(
//                                             fontSize: ScreenUtil().setSp(36),
//                                             color: Color(0xFFFFFFFF),
//                                             fontWeight: FontWeight.w600),
//                                       ),
//                                     ],
//                                   ),
//                                 )),
//                           ],
//                         ),
//                       ),
//                       Expanded(child: Container()),
//                     ],
//                   ),
//                 ),
//
//                 /// YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.leaderboard + "/${getLeaderboardCategory()}");
//                 GestureDetector(
//                   onTap: () {
//                     YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.top_up_gift + "/true");
//                   },
//                   child: Container(
//                     padding: EdgeInsets.all(ScreenUtil().setWidth(14)),
//                     width: ScreenUtil().screenWidth,
//                     child: Image.asset('assets/images/recharge_banner.webp', fit: BoxFit.fitWidth),
//                   ),
//                 ),
//                 _showRecharge()
//               ],
//             ),
//           ),
//         ),
          ),
    );
  }
  void myBuildPy7Bxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _content() {
    return SingleChildScrollView(
      child: Container(
        // 页面背景色
//          padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
        width: double.infinity,
        padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
        child: Column(
          children: [
            Container(
              width: ScreenUtil().screenWidth,
              height: ScreenUtil().setWidth(402),
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/images/bg_red.png'), fit: BoxFit.fitWidth),
              ),
              child: Column(
                children: [
                  Container(
                    width: ScreenUtil().screenWidth,
                    height: ScreenUtil().setWidth(116),
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                            // 导航栏返回按钮
                            top: ScreenUtil().setWidth(15),
                            left: 0,
                            child: YBDNavBackButton()),
                        Center(
                          // 关注和粉丝选项卡
                          child: Text(
                            translate("top_up"),
                            style: TextStyle(fontSize: YBDTPStyle.spNav, color: Colors.white),
                          ),
                        ),
                        Positioned(
                            top: ScreenUtil().setWidth(35),
                            right: ScreenUtil().setWidth(24),
                            child: GestureDetector(
                              onTap: () {
                                print('more------');
                                // YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.top_up_record);
                                //* 2.4.0换成bill页
                                YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.bill);
                              },
                              child: Image.asset('assets/images/icon_top_up_record.webp',
                                  width: ScreenUtil().setWidth(48), height: ScreenUtil().setWidth(48)),
                            )),
                      ],
                    ),
                  ),
                  Container(
                    width: ScreenUtil().screenWidth,
                    height: ScreenUtil().setWidth(286),
                    child: Stack(
                      alignment: Alignment.center, //指定未定位或部分定位widget的对齐方式
                      children: <Widget>[
                        Positioned(
                          top: ScreenUtil().setWidth(70),
                          child: Image.asset('assets/images/topup/y_top_up_beans@2x.webp',
                              height: ScreenUtil().setWidth(130), fit: BoxFit.fitHeight),
                        ),
                        Positioned(
                            top: ScreenUtil().setWidth(209),
                            child: Align(
                              alignment: Alignment.center,
                              child: Row(
                                children: [
                                  Text(translate('available_beans'),
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(24),
                                          color: Colors.white.withOpacity(0.6),
                                          fontWeight: FontWeight.w400)),
                                  SizedBox(width: ScreenUtil().setWidth(24)),
                                  Text(
                                    '${YBDCommonUtil.formatNum(userInfo?.money)}',
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(36),
                                        color: Color(0xFFFFFFFF),
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
                  Expanded(child: Container()),
                ],
              ),
            ),

            /// YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.leaderboard + "/${getLeaderboardCategory()}");
            SizedBox(
              height: ScreenUtil().setWidth(30),
            ),
            YBDTopUpBanner(),
            SizedBox(
              height: ScreenUtil().setWidth(20),
            ),
            _showRecharge()
          ],
        ),
      ),
    );
  }
  void _contentHNVMvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 广告栏翻页视图
  Widget bannerPageView(List<YBDBannerAdvertiseRecord> banners) {
    if (null != banners && banners.isNotEmpty) {
//      logger.v('ad banners amount : ${banners.length}');
      return Container(
        height: ScreenUtil().setWidth(368),
        child: Swiper(
          itemCount: banners.length,
          autoplay: true,
          autoplayDelay: 5000,
          outer: false,
          onTap: (index) {
            String url = banners[index].adurl!;
            // String url = 'https://www.baidu.com';

            if (url.startsWith('flutter')) {
              YBDNavigatorHelper.navigateTo(context, url);
            }

            if (url.startsWith('http')) {
              // if (Platform.isIOS) {

              YBDNavigatorHelper.navigateToWeb(context,
                  "?url=${Uri.encodeComponent(url)}&title=${banners[index].adname}&shareImg=${banners[index].img}");

              // } else {
              // FlutterBoost.singleton.open(getNativePageUrl(NativePageName.WEB_VIEW), urlParams: {
              //   'EVENT_KEY': 'event',
              //   'TITLE_KEY': banners[index].adname,
              //   'URL_KEY': url,
              //   'AD_IMG': banners[index].img,
              //   'HIDE_TITLE': false
              // });
              // }
            }
          },
          pagination: SwiperPagination(
              alignment: Alignment.bottomRight,
              margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
              builder: DotSwiperPaginationBuilder(
                  size: ScreenUtil().setWidth(10),
                  activeSize: ScreenUtil().setWidth(10),
                  color: Colors.white,
                  activeColor: YBDTPStyle.heliotrope)),
          itemBuilder: (BuildContext context, int index) {
            return ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(0))),
                child: YBDNetworkImage(
                  imageUrl: YBDImageUtil.ad(context, banners[index].img, 'A'),
                  width: ScreenUtil().setWidth(670),
                  height: ScreenUtil().setWidth(238),
                ));
          },
        ),
      );
    } else {
      return Container(
          // child: Text('empty ad view'),
          );
    }
  }
  void bannerPageViewNQKlloyelive(List<YBDBannerAdvertiseRecord> banners) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _topUpHelp() {
    if (Platform.isAndroid) {
      return Text.rich(
          TextSpan(style: TextStyle(fontSize: ScreenUtil().setSp(22), color: Colors.white.withOpacity(0.7)), children: [
        TextSpan(
          text: translate('top_up_help'),
        ),
        TextSpan(
          text: translate('  '),
        ),
        WidgetSpan(
            child: GestureDetector(
          onTap: () {
            YBDCommonUtil.showSupport(context);
          },
          child: Text(
            translate('support'),
            style: TextStyle(
                fontSize: ScreenUtil().setSp(22), color: Color(0xFF7DFAFF), decoration: TextDecoration.underline),
          ),
        ))
      ]));
    } else {
      return SizedBox();
    }
  }

  Widget _showRecharge() {
    return Column(
      children: [
        Container(
          height: ScreenUtil().setHeight(78),
          alignment: Alignment.center,
          child: Row(
            children: [
              SizedBox(width: ScreenUtil().setWidth(33)),
              Text(
                translate('top_up_for'),
                style:
                    TextStyle(fontSize: ScreenUtil().setSp(28), color: Color(0xFFFFFFFF), fontWeight: FontWeight.w400),
              ),
              Expanded(child: Text('')),
              Container(
                alignment: Alignment.centerRight,
                width: ScreenUtil().setWidth(460),
                child: Text(
                  '${userInfo?.nickname}(${userInfo?.id})',
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(28), color: Color(0xFFFFFFFF), fontWeight: FontWeight.w400),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(33)),
            ],
          ),
        ),
        Divider(
          height: ScreenUtil().setWidth(1),
          thickness: ScreenUtil().setWidth(1),
          color: Colors.white.withOpacity(0.2),
        ),
        GestureDetector(
          onTap: () {
            showDialog<Null>(
                context: context, //BuildContext对象
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return YBDSelectCountry((Map<String, String> selected) {
                    countryName = selected['name'];
                    YBDSPUtil.save(Const.SP_TOP_UP_COUNTRY_CODE, selected['code']);
                    _payConfigsEntity = null;
                    setState(() {});
                    _getData();
                  });
                });
          },
          child: Container(
              height: ScreenUtil().setHeight(110),
              alignment: Alignment.center,
              decoration: BoxDecoration(),
              child: Row(
                children: [
                  SizedBox(width: ScreenUtil().setWidth(33)),
                  Text(
                    translate('top_up_methods'),
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(28), color: Color(0xFFFFFFFF), fontWeight: FontWeight.w400),
                  ),
                  Expanded(child: Text('')),
                  Image.asset('assets/images/location.png',
                      width: ScreenUtil().setWidth(18), height: ScreenUtil().setWidth(22)),
                  SizedBox(width: ScreenUtil().setWidth(10)),
                  Text(
                    '$countryName',
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(28), color: Color(0xFFFFFFFF), fontWeight: FontWeight.w400),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(33)),
                ],
              )),
        ),
        SizedBox(height: ScreenUtil().setWidth(24)),
        getPayLayout(),
        SizedBox(height: ScreenUtil().setWidth(80)),
        YBDTopupBottomNote(),

//        Text(
//          translate('top_up_help'),
//          style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xFFD8D8D8), fontWeight: FontWeight.w400),
//        ),
//        SizedBox(height: ScreenUtil().setWidth(16)),
//        GestureDetector(
//          onTap: () async {
//            String url = Platform.isAndroid
//                ? "https://wa.me/${Const.WHATSAPP_CONTACT}"
//                : "https://api.whatsapp.com/send?phone=${Const.WHATSAPP_CONTACT}";
//            if (await canLaunch(url)) {
//              await launch(url);
//            }
//          },
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: [
//              Image.asset('assets/images/wa_ico.png',
//                  width: ScreenUtil().setWidth(26), height: ScreenUtil().setWidth(26)),
//              SizedBox(width: ScreenUtil().setWidth(8)),
//              Text('+${Const.WHATSAPP_CONTACT}',
//                  style: TextStyle(
//                      fontSize: ScreenUtil().setSp(24), color: Color(0xFFFFFFFF), fontWeight: FontWeight.w400)),
//            ],
//          ),
//        ),
        SizedBox(height: Platform.isIOS ? ScreenUtil().setWidth(200) : ScreenUtil().setWidth(80)),
      ],
    );
  }
  void _showRecharge9Xp97oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _onRefresh() async {
    logger.v("check login info---recharge_page");
    await ApiHelper.checkLogin(context);
    _refreshController.refreshCompleted();
    logger.v('received _onRefresh:');

    YBDUserUtil.userInfo().then((value) {
      userInfo = value;
      logger.v('received _onRefresh money :${userInfo!.money}');
      if (mounted) {
        setState(() {});
      }
    });
  }
}
