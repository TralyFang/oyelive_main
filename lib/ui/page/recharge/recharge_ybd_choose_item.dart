import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../module/payment/entity/pay_ybd_config_entity.dart';
import 'google/google_ybd_purchase_detail_page.dart';
import 'purchase_ybd_detail_page.dart';

// 返回上一页回调方法
typedef BackCallbackFunc = Function(int i);

class YBDRechargeChooseItem extends StatelessWidget {
  final String? nickName;
  final String? iconUrl;
  final String location;
  final String? sideIconUrl;
  bool isChild;
  YBDPayConfigDataPayload? payload;
  List<YBDPayConfigDataItem>? items;
  double? rate;
  String? currency;
  String? parentName;

  ///比对支付类别的名字   上面的 nickName 用于显示，后台可修改
  String? name;

  /// 点击返回按钮回调
  BackCallbackFunc? backCallbackFunc;

  YBDRechargeChooseItem(
    this.nickName,
    this.iconUrl,
    this.sideIconUrl,
    this.currency,
    this.name, {
    this.isChild: false,
    this.location: '',
    this.items,
    this.payload,
    this.rate,
    this.parentName,
    this.backCallbackFunc,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (payload?.action != null && payload!.action!.startsWith("http")) {
          logger.v("payload.action : ${payload?.action}");
          YBDNavigatorHelper.navigateToWeb(context, "?url=${Uri.encodeComponent(payload!.action!)}&title=$nickName");
          // YBDNavigatorHelper.navigateTo(
          //     context, YBDNavigatorHelper.flutter_web_view_page + "/${Uri.encodeComponent(payload.action)}/${nickName}/");
          // YBDRechargeToNative.toNativeWebView('OyeTalk', 'https://oyetalkapp.wixsite.com/mysite/?portalType=201');
          // FlutterBoost.singleton.open(getNativePageUrl(NativePageName.WEB_VIEW), urlParams: {
          //   'EVENT_KEY': 'event',
          //   'TITLE_KEY': 'OyeTalk',
          //   'URL_KEY': 'https://oyetalkapp.wixsite.com/mysite/?portalType=201',
          //   'AD_IMG': '',
          //   'HIDE_TITLE': false
          // });
          return;
        }

        ///google支付直接跳转  原生 暂时不从配置里取金额（存在差异）
        if (name == 'google') {
          /*FlutterBoost.singleton.open(getNativePageUrl(NativePageName.TOP_UP), urlParams: {
            'topUpType': 1,
          });*/
          // YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.google_pay);
          // return;
          logger.v('topup gift location google choose item : $location');
          Navigator.push(
              context,
              CupertinoPageRoute(
                  builder: (_) => YBDGooglePurchaseDetailPage(
                        nickName,
                        payload,
                        items,
                        rate,
                        currency,
                        name,
                        parentName: parentName,
                        location: location,
                      ))).then(
            (value) => {backCallbackFunc?.call(0)},
          );
        } else {
          logger.v('topup gift location other choose item : $location ,$nickName ,$currency ,$payload ,$rate');
          Navigator.push(
              context,
              CupertinoPageRoute(
                  builder: (_) => YBDPurchaseDetailPage(
                        nickName: nickName,
                        payload: payload,
                        items: items,
                        rate: rate,
                        currency: currency,
                        name: name,
                        parentName: parentName,
                        location: location,
                      ))).then(
            (value) => {backCallbackFunc?.call(0)},
          );
        }
      },
      child: Container(
        alignment: Alignment.center,
        height: ScreenUtil().setWidth(isChild ? 90 : 110),
        decoration: BoxDecoration(),
        margin: EdgeInsets.only(left: ScreenUtil().setWidth(30), right: ScreenUtil().setWidth(26)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                YBDNetworkImage(
                  imageUrl: iconUrl ?? '',
                  width: ScreenUtil().setWidth(50),
                  height: ScreenUtil().setWidth(50),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(42),
                ),
                Text(
                  translate('$nickName'),
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(28), color: Color(0xFFFFFFFF), fontWeight: FontWeight.w400),
                ),
                Expanded(
                  child: Text(''), // 中间用Expanded控件
                ),
                YBDNetworkImage(
                  imageUrl: sideIconUrl ?? '',
                  height: ScreenUtil().setWidth(50),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
  void buildeGeH1oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
