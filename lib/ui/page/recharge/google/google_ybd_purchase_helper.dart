
import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/database_ybd_until.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/result_ybd_bean.dart';
import 'package:oyelive_main/module/payment/payment_ybd_api_helper.dart';
import 'package:oyelive_main/ui/page/recharge/entity/db_ybd_inApp_purchase_entity.dart';
import 'package:oyelive_main/ui/page/recharge/entity/db_ybd_order_purchase_entity.dart';

import 'purchase_ybd_entity.dart';

class YBDGooglePurchaseHelper {
  ///google透传策略
  ///由于google提供的透传是用来打击不规范支付，不是完全给用户自由使用（https://developer.android.com/reference/com/android/billingclient/api/BillingFlowParams.Builder#setObfuscatedProfileId(java.lang.String)），
  ///为防止以后出现不可以情况，添加透传策略
  ///0 默认obfuscatedAccountid和obfuscatedProfileId都传，其中obfuscatedAccountid是用户Id，obfuscatedProfileId是预订单号
  ///1 只传obfuscatedAccountid，用户号预订单号拼接  (U2099955|O88888)
  ///可配置
  static int transparentMethod = 0;

  ///回调服务器  确认结果
  ///result 第三方支付结果
  ///source 0、支付回调 1、重释
  ///error 第三方支付失败的时候的错误信息
  ///coins 购买的币值
  static verifyPurchase(BuildContext context, int? userId, YBDPurchaseEntity purchaseEntity, String result, int source,
      {String error = "", String? coins}) async {
    try {
      logger.i(
          '_verifyPurchase... obfuscatedProfileId: ${purchaseEntity.obfuscatedProfileId} result:$result error:$error ,source:$source');
      YBDResultBeanEntity resultBeanEntity = await YBDPaymentApiHelper.requestCallbacksPurchaseAll(context,
          orderId: purchaseEntity.obfuscatedProfileId ?? '',
          payType: 'google',
          command: result,
          errorMsg: error,
          signedData: purchaseEntity.originalJson,
          signature: purchaseEntity.signature);
      logger.i(
          'orderId:${purchaseEntity.obfuscatedProfileId},re:${resultBeanEntity.code},source:$source,coins:$coins,result$result');

      YBDCommonUtil.storeLostGoogleOrder(
        userId,
        purchaseEntity.obfuscatedAccountId,
        purchaseEntity.orderId ?? '',
        'consumePurchase_verify',
        signature: purchaseEntity.signature,
        purchaseToken: purchaseEntity.purchaseToken,
        otOrderId:
            'orderId:${purchaseEntity.obfuscatedProfileId},re:${resultBeanEntity.code},source:$source,coins:$coins,result$result',
      );

      if (result == 'success') {
        if (resultBeanEntity.code == Const.HTTP_SUCCESS_NEW) {
          YBDDataBaseUtil.getInstance().inAppUpdate(
            YBDDBInAppPurchaseEntity(
                userId,
                purchaseEntity.obfuscatedAccountId,
                purchaseEntity.obfuscatedProfileId,
                purchaseEntity.originalJson,
                purchaseEntity.signature,
                purchaseEntity.orderId,
                STATE_VERIFY_SUCCESS,
                null,
                0,
                null),
            stateType: 0,
          );
          ApiHelper.checkLogin(context);

          if (source == 0 && coins != null) {
            ///弹框提示购买到的币
            YBDDialogUtil.showGooglePurchaseDialog(context, coins);
          }
        } else {
          // API 返回失败
          /// firebase database 记录Google订单, 跟踪掉单问题
          YBDCommonUtil.storeLostGoogleOrder(
            userId,
            purchaseEntity.obfuscatedAccountId,
            purchaseEntity.orderId ?? '',
            'lost_$source',
            signature: purchaseEntity.originalJson,
            purchaseToken: purchaseEntity.purchaseToken,
            otOrderId: purchaseEntity.obfuscatedProfileId,
          );

          YBDDataBaseUtil.getInstance().inAppUpdate(
            YBDDBInAppPurchaseEntity(
                userId,
                purchaseEntity.obfuscatedAccountId,
                purchaseEntity.obfuscatedProfileId,
                purchaseEntity.originalJson,
                purchaseEntity.signature,
                purchaseEntity.orderId,
                STATE_VERIFY_FAILED,
                null,
                0,
                null),
            stateType: 0,
          );
        }
      }
    } catch (e) {
      YBDLogUtil.e('verifyPurchase error：' + e.toString());
    }
  }

  ///获取最合理的预订单
  static getRightOrder(YBDPurchaseEntity purchaseEntity) async {
    try {
      if (purchaseEntity == null) {
        logger.i('getRightOrder purchaseDetails is null');
        return null;
      }
      // TODO: 测试代码 区分 oyetalk 平台的用户
      List<YBDDBOrderPurchaseEntity>? orderMessage = await YBDDataBaseUtil.getInstance().orderQuery(purchaseEntity.productID ?? '');
      YBDDBOrderPurchaseEntity? dbOrderPurchaseEntity;
      if (orderMessage != null) {
        logger.i('orderMessage length:${orderMessage.length}');

        /// 先通过orderId（google订单ID来匹配预订单）
        if (purchaseEntity.orderId != null) {
          for (int i = 0; i < orderMessage.length; i++) {
            logger.i('getRightOrder orderId:${purchaseEntity.orderId} orderId:${orderMessage[i].spOrderID}');
            if (purchaseEntity.orderId == orderMessage[i].spOrderID) {
              logger.i('getRightOrder orderId:${purchaseEntity.orderId}');
              return orderMessage[i];
            }
          }
        }

        ///orderId匹配不上，再通过时间匹配
        dbOrderPurchaseEntity = await getOrderByTime(orderMessage, purchaseEntity);
        logger.i('getRightOrderId dbOrderPurchaseEntity orderID:${dbOrderPurchaseEntity?.orderID}');
      } else {
        logger.i('orderMessage is null');
      }
      return dbOrderPurchaseEntity;
    } catch (e) {
      YBDLogUtil.e('getRightOrder error:' + e.toString());
    }
  }

  static Future<YBDDBOrderPurchaseEntity?> getOrderByTime(
      List<YBDDBOrderPurchaseEntity> orderMessage, YBDPurchaseEntity purchaseEntity) async {
    if (orderMessage == null) {
      logger.i('getOrderByTime orderMessage is null');
      return null;
    }

    if (purchaseEntity == null) {
      logger.i('getOrderByTime purchaseDetails is null');
      return null;
    }

    // TODO: 测试代码 添加初始值
    late int intervalsTime;
    YBDDBOrderPurchaseEntity? dbOrderPurchaseEntity;
    for (int i = 0; i < orderMessage.length; i++) {
      logger.i(
          'getOrderByTime... orderMessage: ${orderMessage[i].toMap()} ,orderMessage: ${orderMessage[i].toString()}');

      ///TODO 时间标准？？？
      int temTime = purchaseEntity.purchaseTime ?? 0 - orderMessage[i].createTime!;
      if (i == 0) {
        intervalsTime = temTime;
        dbOrderPurchaseEntity = orderMessage[i];
      }
      logger.i(
          'getOrderByTime... purchaseTime: ${purchaseEntity.purchaseTime} createTime:${orderMessage[i].createTime} temTime:$temTime');
      logger.i(
          'getOrderByTime temTime $temTime ,intervalsTime:$intervalsTime temTime.abs():${temTime.abs()} ,intervalsTime.abs():${intervalsTime.abs()}');
      if (temTime.abs() < intervalsTime.abs()) {
        intervalsTime = temTime;
        dbOrderPurchaseEntity = orderMessage[i];
      }
    }
    return dbOrderPurchaseEntity;
  }
}
