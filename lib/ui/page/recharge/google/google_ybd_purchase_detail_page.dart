
import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:in_app_purchase_android/billing_client_wrappers.dart';
import 'package:in_app_purchase_android/in_app_purchase_android.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
// import 'package:in_app_purchase/in_ybd_app_purchase.dart';
import 'package:oyelive_main/common/analytics/crashlytics_ybd_util.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/common/util/purchase_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/payment/entity/pay_ybd_config_entity.dart';
import 'package:oyelive_main/module/payment/payment_ybd_api_helper.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/ui/page/recharge/google/google_ybd_purchase_helper.dart';
import 'package:oyelive_main/ui/page/recharge/google/in_ybd_app_purchase.dart';
import 'package:oyelive_main/ui/page/recharge/razorpay/entity/razorpay_ybd_entity.dart';
import 'package:oyelive_main/ui/page/recharge/widget/product_ybd_info_item.dart';
import 'package:oyelive_main/ui/page/status/local_audio/blue_ybd_gradient_button.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/my_ybd_app_bar.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/database_ybd_until.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/payment/entity/pay_ybd_config_entity.dart';
import '../../../../module/payment/payment_ybd_api_helper.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';
import '../../../widget/my_ybd_app_bar.dart';
import '../../../widget/scaffold/nav_ybd_back_button.dart';
import '../../status/local_audio/blue_ybd_gradient_button.dart';
import '../entity/db_ybd_inApp_purchase_entity.dart';
import '../entity/db_ybd_order_purchase_entity.dart';
import '../razorpay/entity/razorpay_ybd_entity.dart';
import '../widget/product_ybd_info_item.dart';
import 'google_ybd_purchase_handler.dart';
import 'google_ybd_purchase_helper.dart';
import 'purchase_ybd_entity.dart';

/// google支付购买页面
class YBDGooglePurchaseDetailPage extends StatefulWidget {
  YBDPayConfigDataPayload? payload;
  List<YBDPayConfigDataItem>? items;
  double? rate;
  String? currency;
  String? nickName;
  String? location;
  String? name;

  ///有child的选项  使用parentName来判断类型：PayMax
  String? parentName;

  YBDGooglePurchaseDetailPage(
    this.nickName,
    this.payload,
    this.items,
    this.rate,
    this.currency,
    this.name, {
    this.parentName,
    this.location = '',
  }) {
    logger.i('GooglePayPage enablePendingPurchases 33333');
  }

  @override
  State<StatefulWidget> createState() => _YBDGooglePurchaseDetailPageState();
}

class _YBDGooglePurchaseDetailPageState extends State<YBDGooglePurchaseDetailPage> {
  /// 选中的商品索引号，默认选中第一个商品
  int _selectedIndex = 0;

  final InAppPurchase _connection = InAppPurchase.instance;
  late StreamSubscription<List<PurchaseDetails>> _subscription;
  List<String?> _kProductIds = [];

  List<ProductDetails> _productDetails = [];
  int? userId;
  String? _orderId; //服务器生成的订单ID
  String? _productId; //预订单ID对应的productId

  String? googleCurrency;

  @override
  void initState() {
    logger.v('topup gift location purchase detail page: ${widget.location}');
    final Stream<List<PurchaseDetails>> purchaseUpdated = _connection.purchaseStream;
    _subscription = purchaseUpdated.listen((purchaseDetailsList) {
      _listenToPurchaseUpdated(purchaseDetailsList);
    }, onDone: () {
      _subscription.cancel();
    }, onError: (error) {
      // handle error here.
    });
    initStoreInfo();
    super.initState();
  }
  void initStateqo1mjoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }
  void disposek40Exoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  submit() async {
    logger.i('submit  _selectedIndex:$_selectedIndex   currency${widget.currency} googleCurrency:$googleCurrency');
    if (googleCurrency == null) {
      logger.i('googleCurrency is null');
      return;
    }
    if (_selectedIndex != -1) {
      await getOrder(widget.items?[_selectedIndex] ?? YBDPayConfigDataItem(), googleCurrency);
      logger.v('topup gift location google purchase detail submit : ${widget.location}');
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.CLICK_EVENT,
        location: YBDLocationName.GOOGLE_PURCHASE_DETAIL_PAGE,
        itemName: 'submit',
        adUnitName: widget.location,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    try {
      Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
      YBDGooglePurchaseHelper.transparentMethod = int.parse(store.state.configs?.transparentMethod ?? '');
      logger.i('transparentMethod : ${store.state.configs?.transparentMethod}');
      logger.i(
          'YBDGooglePurchaseDetailPage store.state.configs?.transparentMethod : ${store.state.configs?.transparentMethod}');
      logger.v(
          'YBDGooglePurchaseDetailPage YBDGooglePurchaseHelper.transparentMethod : ${YBDGooglePurchaseHelper.transparentMethod}');
    } catch (e) {
      logger.i('YBDGooglePurchaseDetailPage build error: $e');
      YBDGooglePurchaseHelper.transparentMethod = 0;
      print(e);
    }
    return Scaffold(
      appBar: YBDMyAppBar(
        title: Text(
          widget.nickName ?? '',
          style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(32)),
        ),
        backgroundColor: YBDTPStyle.heliotrope,
        leading: YBDNavBackButton(color: Colors.white),
        elevation: 0.0,
      ),
      body: Container(
        width: ScreenUtil().screenWidth,
        decoration: YBDTPStyle.gradientDecoration,
        child: _contentView(),
      ),
    );
  }
  void buildjVvXLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 页面内容
  Widget _contentView() {
    return (_productDetails.isNotEmpty) ? _purchaseView() : Container();
  }

  /// 展示商品购买页面内容
  Widget _purchaseView() {
    return Container(
      child: Column(
        children: <Widget>[
          // 商品列表
          _productsListView(),
          SizedBox(height: ScreenUtil().setWidth(60)),
          // 提交按钮
          _submitBtn(),
        ],
      ),
    );
  }
  void _purchaseViewbMpuToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 商品列表
  Widget _productsListView() {
    return Container(
      alignment: Alignment.center,
      // height: ScreenUtil().setWidth(430),
      width: ScreenUtil().screenWidth,
      child: Wrap(
        spacing: ScreenUtil().setWidth(15),
        runSpacing: ScreenUtil().setWidth(15),
        children: List.generate(widget.items?.length ?? 0, (index) {
          return GestureDetector(
            onTap: () {
              logger.i('select product index : $index');
              setState(() {
                _selectedIndex = index;
              });
            },
            child: YBDProductInfoItem(
              isSelected: _selectedIndex == index,
              beans: YBDNumericUtil.format(widget.items?[index].value ?? 0),
              price: '$googleCurrency ${YBDNumericUtil.formatWithDot(widget.items?[index].money ?? 0)}',
            ),
          );
        }),
      ),
    );
  }
  void _productsListViewnqkoZoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 提交按钮
  Widget _submitBtn() {
    return Container(
      child: YBDDelayGestureDetector(
        // 提交按钮
        child: YBDBlueGradientButton(title: 'Submit'),
        onTap: submit,
      ),
    );
  }

  ///获取订单
  getOrder(YBDPayConfigDataItem payConfigDataItem, String? currency) async {
    logger.i('getOrder amount:${payConfigDataItem.money} currency:$currency productId:${payConfigDataItem.productId}');
    YBDDialogUtil.showLoading(context);

    YBDRazorpayResultEntity? payOrder = await YBDPaymentApiHelper.payOrders(
        context, payConfigDataItem.money, 'google', currency,
        channel: 'google', productId: payConfigDataItem.productId);

    if (payOrder?.code == '0') {
      logger.i('googlePay order Su: payOrder.data:${payOrder?.data?.toJson()}');
      // logger.i(
      //     'payermax toNativeWebView title:${widget.nickName} requestUrl:${payOrder?.data?.requestUrl} -orderId:${payOrder?.data?.orderId}');
      // RechargeToNativoe.toNativeWebView(widget.nickName, payOrder.data.requestUrl);
      _orderId = payOrder?.data?.orderId;
      _productId = payConfigDataItem.productId;

      _productDetails.forEach((f) {
        logger.i('googlePay order Su: productDetails.forEach :${payConfigDataItem.productId} id:${f.id}');
        if (payConfigDataItem.productId == f.id) {
          _buyConsumable(f);
          return;
        }
      });
    } else if (payOrder != null) {
      YBDLogUtil.e('getPayGoogleOrder Error:-->code:${payOrder.code}  message:${payOrder.message}');

      ///充值渠道金额限制提示 fix bug 4065
      // String code = payOrder?.code == '13383' ? '' : '[${payOrder?.code}]';
      YBDToastUtil.toast('Failed! ${payOrder.message}');
    } else {
      YBDToastUtil.toast('Failed');
    }
    YBDDialogUtil.hideLoading(context);
  }

  Future<void> initStoreInfo() async {
    final bool isAvailable = await _connection.isAvailable();
    logger.i('initStoreInfo isAvailable:$isAvailable');
    if (!isAvailable) {
      YBDToastUtil.toast('Google Play cannot be reached!');
      return;
    }

    ///产品ID列表
    await getSkuId();

    ///获取支付列表具体信息
    ProductDetailsResponse productDetailResponse = await _connection.queryProductDetails(_kProductIds.toSet().removeNulls());
    logger.i('initStoreInfo _kProductIds.toSet():${_kProductIds.toSet()}');

    /// 判断是否获取错误
    if (productDetailResponse.error != null) {
      setState(() {
        logger.i('initStoreInfo error.message:${productDetailResponse.error!.message}');
      });
      YBDToastUtil.toast("Failed! ${productDetailResponse.error!.message}");
      return;
    }

    ///判断获取的产品信息是否为空
    _productDetails = productDetailResponse.productDetails;
    if (_productDetails.isEmpty) {
      logger.i('initStoreInfo productDetailResponse.productDetails.isEmpty');
      YBDToastUtil.toast("No product, please try again later! [Empty]");
      setState(() {});
      return;
    }

    ///更新支付列表
    print('initStoreInfo productDetails _productDetails:$_productDetails');
    _productDetails.forEach((f) {
      setState(() {
        // googleCurrency = f.skuDetail.priceCurrencyCode;
        googleCurrency = f.currencyCode;
        logger.i('initStoreInfo productDetails googleCurrency:$googleCurrency code:${f.currencyCode}');

        for (int i = 0; i < (widget.items?.length ?? 0); i++) {
          if (f.id == widget.items?[i].productId) {
            logger.i('initStoreInfo productDetails price :${f.price} rawPrice:${f.rawPrice}');
            widget.items?[i].money = f.rawPrice;
            break;
          }
        }
      });
    });
  }
  void initStoreInfoVXbU3oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///商品ID
  getSkuId() async {
    _kProductIds.clear();
    widget.items?.forEach((element) {
      _kProductIds.add(element.productId);
    });
  }

  _buyConsumable(ProductDetails purchaseDetails) async {
    if (userId == null) {
      userId = int.parse(await YBDUserUtil.userId() ?? '-1');
    }

    PurchaseParam purchaseParam = GooglePlayPurchaseParam(
        productDetails: purchaseDetails,
        applicationUserName: (YBDGooglePurchaseHelper.transparentMethod == 0) ? 'U$userId' : 'U$userId|O$_orderId',
        obfuscatedProfileId: (YBDGooglePurchaseHelper.transparentMethod == 0) ? 'O$_orderId' : null,
        changeSubscriptionParam: null);

    ///改为手动消费
    _connection.buyConsumable(purchaseParam: purchaseParam, autoConsume: false);
  }

  ///支付结果监听
  void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList) {
    purchaseDetailsList.forEach((PurchaseDetails purchaseDetails) async {
      PurchaseWrapper? billingClientPurchase;

      ///---测试代码--借用uLevelGoLive来区分场景-----------------
      /// 目的 1、正常支付流程测试  2、正常支付中断，走重释流程 3、兼容（APP新老版本进兼容（兼容数据））、数据表兼容，打一个老版本）
      /// 4不走消耗和校验--重启app走重释
      /// 5走校验不走消耗--重启app走重释
      /// 6走消耗不走校验--重启app走重释
      /// 切账号（没消耗的会取到数据，正常校验，消耗）
      /// 数据库升级
      /// app新老版本兼容
      /* Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context);
      int uLevelGoLive = YBDNumericUtil.stringToInt(store.state.configs?.ulevelGoLive ?? '3');
      logger.i('_listenToPurchaseUpdated uLevelGoLive :$uLevelGoLive');
      if (uLevelGoLive == 4) {
        logger.i('uLevelGoLive == 4 $uLevelGoLive 4不走消耗和校验--重启app走重释');
        return;
      }*/

      try {
        if (userId == null) {
          userId = int.parse(await YBDUserUtil.userId() ?? '-1');
        }
        if (purchaseDetails is GooglePlayPurchaseDetails) {
          billingClientPurchase = purchaseDetails.billingClientPurchase;
        }
        logger.i(
            '_listenToPurchaseUpdated purchaseDetails.status:${purchaseDetails.status} obfuscatedAccountId:${billingClientPurchase?.obfuscatedAccountId} obfuscatedProfileId:${billingClientPurchase?.obfuscatedProfileId}');
        YBDPurchaseEntity purchaseEntity = YBDPurchaseEntity(
            YBDPurchaseUtil.getAccountId(billingClientPurchase?.obfuscatedAccountId ?? ''),
            YBDPurchaseUtil.getProfileId(
                billingClientPurchase?.obfuscatedAccountId ?? '', billingClientPurchase?.obfuscatedProfileId ?? ''),
            billingClientPurchase?.orderId,
            billingClientPurchase?.signature,
            billingClientPurchase?.originalJson,
            billingClientPurchase?.purchaseToken,
            purchaseDetails.purchaseID!,
            purchaseDetails.productID,
            billingClientPurchase?.purchaseTime);

        if (purchaseDetails.status == PurchaseStatus.pending) {
          logger.i('_listenToPurchaseUpdated purchaseEntity:${purchaseEntity.toString()}');

          /// firebase database 记录pending Google订单
          YBDCommonUtil.storeLostGoogleOrder(
            userId,
            purchaseEntity.obfuscatedAccountId,
            purchaseEntity.orderId ?? '',
            'pending',
            signature: purchaseEntity.signature,
            purchaseToken: purchaseEntity.purchaseToken,
            otOrderId: purchaseEntity.obfuscatedProfileId,
          );
        } else if (purchaseDetails.status == PurchaseStatus.error) {
          logger.i('_listenToPurchaseUpdated purchaseEntity:${purchaseEntity.toString()}');
          Map<String, dynamic> params = {};
          params.addAll({"errorMsg": '${purchaseDetails.error!.message}'});
          params.addAll({"errorCode": purchaseDetails.error!.code});
          params.addAll({"purchaseID": purchaseEntity.purchaseID});
          params.addAll({"productID": purchaseEntity.productID});
          params.addAll({"orderId": purchaseEntity.obfuscatedProfileId});
          params.addAll({"userId": purchaseEntity.obfuscatedAccountId});
          params.addAll({"transparentMethod": YBDGooglePurchaseHelper.transparentMethod});

          ///TODO 是否能获取到传进来的obfuscatedProfileId 待确认
          ///如果支付错误的时候没有取到透传的预订单ID，则使用缓存的预订单ID _productId
          if (purchaseEntity.obfuscatedProfileId == null) {
            purchaseEntity.obfuscatedProfileId = _orderId!;
          }
          // _verifyPurchase(purchaseEntity, "failure", error: jsonEncode(params));
          YBDGooglePurchaseHelper.verifyPurchase(context, userId, purchaseEntity, "failure", 0, error: jsonEncode(params));

          ///YBDDataBaseUtil.getInstance().orderDeleteByOrderId(_orderId);
        } else if (purchaseDetails.status == PurchaseStatus.purchased) {
          YBDGooglePurchaseHandler.listenToPurchased(context, _connection, userId, purchaseDetails, 0,
              coins: '${widget.items?[_selectedIndex].value}');
        } else {
          logger.i(
              '_listenToPurchaseUpdated PurchaseStatus.unknown---- , originalJson:${billingClientPurchase?.originalJson}');

          /// firebase database 记录Google订单, 跟踪掉单问题
          YBDCommonUtil.storeLostGoogleOrder(
            userId,
            purchaseEntity.obfuscatedAccountId,
            purchaseEntity.orderId ?? '',
            'unknown',
            signature: purchaseEntity.signature,
            purchaseToken: purchaseEntity.purchaseToken,
            otOrderId: purchaseEntity.obfuscatedAccountId,
          );
        }
      } catch (e, s) {
        YBDLogUtil.e('google inapp purchase error:$e');
        YBDCrashlyticsUtil.instance!.report(
          e,
          extra: {
            'order_id': billingClientPurchase?.orderId,
            'payload': billingClientPurchase?.developerPayload,
            'signature': billingClientPurchase?.signature
          },
        );
      }
    });
  }
  void _listenToPurchaseUpdatednQcgfoyelive(List<PurchaseDetails> purchaseDetailsList) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

/*  ///取出价格
  String _getDoublePrice(String price) {
    for (int i = 0; i < price.length; i++) {
      if (YBDNumericUtil.isNumeric(price[i])) {
        logger.i('_getDoublePrice price:$price doublePrice:${price.substring(i, price.length)}');
        return price.substring(i, price.length).replaceAll(',', '');
      }
    }
    return '0';
  }*/
}
