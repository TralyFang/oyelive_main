
import 'package:flutter/cupertino.dart';
import 'package:in_app_purchase_android/billing_client_wrappers.dart';
import 'package:in_app_purchase_android/in_app_purchase_android.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/database_ybd_until.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/purchase_ybd_util.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/recharge/entity/db_ybd_inApp_purchase_entity.dart';
import 'package:oyelive_main/ui/page/recharge/entity/db_ybd_order_purchase_entity.dart';
import 'package:oyelive_main/ui/page/recharge/google/google_ybd_purchase_helper.dart';

import 'in_ybd_app_purchase.dart';
import 'purchase_ybd_entity.dart';

/// 启动 app 时检查 google 漏单
class YBDGooglePurchaseHandler {
  BuildContext context;
  InAppPurchase _connection = InAppPurchase.instance;

  YBDGooglePurchaseHandler(this.context);

  ///查询未消费商品
  Future<void> initStoreInfo() async {
    final bool isAvailable = await _connection.isAvailable();
    // logger.i('home page initStoreInfo isAvailable:$isAvailable');
    if (!isAvailable) {
      logger.i('Google Play cannot be reached! isAvailable:$isAvailable');
      return;
    }

    ///查询以前所有购买 （不包含已经消费的商品）
    InAppPurchaseAndroidPlatformAddition androidAddition =
        _connection.getPlatformAddition<InAppPurchaseAndroidPlatformAddition>();
    final QueryPurchaseDetailsResponse purchaseResponse = await androidAddition.queryPastPurchases();

    if (purchaseResponse == null) {
      logger.i('home page purchaseResponse is null');
      return;
    }

    if (purchaseResponse.error != null) {
      logger.i('home page PurchaseDetails PastPurchases error:${purchaseResponse.error}');
      return;
    }
    logger
        .i('home page PurchaseDetails PastPurchases pastPurchases?.length:${purchaseResponse.pastPurchases.length}');

    YBDUserInfo? userInfo = await YBDUserUtil.userInfo();

    // logger.i('home page PurchaseDetails PastPurchases puserInfo?.id:${userInfo?.id}');

    for (PurchaseDetails purchaseDetails in purchaseResponse.pastPurchases) {
      logger.i(
          'home page PurchaseDetails PastPurchases productID::${purchaseDetails.productID} purchaseID:${purchaseDetails.purchaseID} purchase?.status:${purchaseDetails.status}');
      if (purchaseDetails.status != PurchaseStatus.purchased) {
        logger.i('home page purchase?.status not purchased:${purchaseDetails.status}');
        break;
      }
      listenToPurchased(context, _connection, userInfo!.id, purchaseDetails, 1);
    }
  }
  void initStoreInfoURjjSoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///更新订单状态
  static addOrder(int userId, YBDPurchaseEntity purchaseEntity) async {
    try {
      YBDDBInAppPurchaseEntity dbInAppPurchaseEntity = YBDDBInAppPurchaseEntity(
        userId,
        purchaseEntity.obfuscatedAccountId,
        purchaseEntity.obfuscatedProfileId,
        purchaseEntity.originalJson,
        purchaseEntity.signature,
        purchaseEntity.orderId,
        STATE_DEFAULT,
        null,
        0,
        CHANNEL_STATE_PURCHASE,
      );

      ///没有透传的字段说明是老版本
      if (dbInAppPurchaseEntity.orderID == null) {
        return;
      }

      ///   判断订单是否存在数据库
      bool exist = await YBDDataBaseUtil.getInstance().isOrderExist(
        dbInAppPurchaseEntity,
      );

      ///没有存在就加入
      if (!exist) {
        var dbResult = await YBDDataBaseUtil.getInstance().inAppAdd(
          dbInAppPurchaseEntity,
        );
        logger.i('addOrder inAppAdd dbResult:$dbResult');
      }
    } catch (e) {
      print('addOrder error:' + e.toString());
    }
  }

  ///监听 支付成功回调
  ///source 0、支付回调 1、重释
  ///uLevelGoLive测试使用参数
  static void listenToPurchased(
      BuildContext context, InAppPurchase? _connection, int? userId, PurchaseDetails purchaseDetails, int source,
      {String? coins, int uLevelGoLive = 0}) async {
    if (purchaseDetails == null) {
      return;
    }
    if (userId == null) {
      userId = int.parse(await YBDUserUtil.userId() ?? '');
    }
    late PurchaseWrapper billingClientPurchase;
    if (purchaseDetails is GooglePlayPurchaseDetails) {
      billingClientPurchase = purchaseDetails.billingClientPurchase;
    }
    logger.i(
        '_listenToPurchaseUpdated purchaseDetails.status:${purchaseDetails.status} obfuscatedAccountId:${billingClientPurchase.obfuscatedAccountId} obfuscatedProfileId:${billingClientPurchase.obfuscatedProfileId},source:$source');
    YBDPurchaseEntity purchaseEntity = YBDPurchaseEntity(
      YBDPurchaseUtil.getAccountId(billingClientPurchase.obfuscatedAccountId ?? ''),
      YBDPurchaseUtil.getProfileId(billingClientPurchase.obfuscatedAccountId ?? '', billingClientPurchase.obfuscatedProfileId ?? ''),
      billingClientPurchase.orderId,
      billingClientPurchase.signature,
      billingClientPurchase.originalJson,
      billingClientPurchase.purchaseToken,
      purchaseDetails.purchaseID!,
      purchaseDetails.productID,
      billingClientPurchase.purchaseTime,
    );
    logger.i('listenToPurchased purchaseEntity:${purchaseEntity.toString()}');
    logger.i('addOrder-------0 millisecondsSinceEpoch:${DateTime.now().millisecondsSinceEpoch}');

    ///老版本支付，新版本重释场景(Android)
    if (purchaseEntity.obfuscatedProfileId == null) {
      YBDDBOrderPurchaseEntity dbOrderPurchaseEntity = await YBDGooglePurchaseHelper.getRightOrder(purchaseEntity);
      if (dbOrderPurchaseEntity == null) {
        ///找不到预订单ID
        logger.i('no service orderId,google orderId is  ${billingClientPurchase.orderId}');
      } else {
        purchaseEntity.obfuscatedProfileId = dbOrderPurchaseEntity.orderID!;
        logger.i(
            'find service orderId ${purchaseEntity.obfuscatedProfileId},google orderId is  ${billingClientPurchase.orderId}');
      }
    }

    logger.i('listenToPurchased obfuscatedProfileId :${purchaseEntity.obfuscatedProfileId}');

    ///存数据库
    addOrder(userId, purchaseEntity);
    logger.i('addOrder-------1');

    /// firebase database 记录Google订单, 跟踪掉单问题
    YBDCommonUtil.storeLostGoogleOrder(
      userId,
      purchaseEntity.obfuscatedAccountId,
      purchaseEntity.orderId ?? '',
      'purchased',
      signature: purchaseEntity.signature,
      purchaseToken: purchaseEntity.purchaseToken,
      otOrderId: purchaseEntity.obfuscatedProfileId,
    );
    logger.i('addOrder-------2');

    ///验证购买的商品校验给币
    YBDGooglePurchaseHelper.verifyPurchase(context, userId, purchaseEntity, "success", source, coins: coins);
    /* if (uLevelGoLive != 6) {
      logger.i('uLevelGoLive != 6 $uLevelGoLive');

      ///验证购买的商品校验给币
      YBDGooglePurchaseHelper.verifyPurchase(context, userId, purchaseEntity, "success", source, coins: coins);
    } else {
      logger.i('uLevelGoLive == 6 6走消耗不走校验--重启app走重释');
    }
    if (uLevelGoLive == 5) {
      logger.i('uLevelGoLive == 5 走校验不走消耗--重启app走重释');
      return;
    }*/

    ///消耗商品
    final InAppPurchaseAndroidPlatformAddition addition =
        _connection!.getPlatformAddition<InAppPurchaseAndroidPlatformAddition>();

    BillingResultWrapper? billingResultWrapper = await addition.consumePurchase(purchaseDetails);
    logger.i(
        'consumePurchase billingResultWrapper responseCode:${billingResultWrapper.responseCode}  ,debugMessage:${billingResultWrapper.debugMessage}');
    if (billingResultWrapper.responseCode == BillingResponse.ok) {
      ///消耗成功
      await YBDDataBaseUtil.getInstance().inAppUpdate(
        YBDDBInAppPurchaseEntity(
          userId,
          purchaseEntity.obfuscatedAccountId,
          purchaseEntity.obfuscatedProfileId,
          purchaseEntity.originalJson,
          purchaseEntity.signature,
          purchaseEntity.orderId,
          null,
          null,
          0,
          CHANNEL_STATE_CONSUME_SUCCESS,
        ),
        stateType: 1,
      );
      logger.i('consumePurchase su');
    } else {
      logger.i('consumePurchase failed');

      /// firebase database 记录Google订单, 跟踪掉单问题
      YBDCommonUtil.storeLostGoogleOrder(
        userId,
        purchaseEntity.obfuscatedAccountId,
        purchaseEntity.orderId ?? '',
        'consumeFailed_1_code:${billingResultWrapper.responseCode}message:${billingResultWrapper.debugMessage}',
        signature: purchaseEntity.signature,
        purchaseToken: purchaseEntity.purchaseToken,
        otOrderId: purchaseEntity.obfuscatedProfileId,
      );

      ///消耗失败
      await YBDDataBaseUtil.getInstance().inAppUpdate(
        YBDDBInAppPurchaseEntity(
          userId,
          purchaseEntity.obfuscatedAccountId,
          purchaseEntity.obfuscatedProfileId,
          purchaseEntity.originalJson,
          purchaseEntity.signature,
          purchaseEntity.orderId,
          null,
          null,
          0,
          CHANNEL_STATE_CONSUME_FAILED,
        ),
        stateType: 1,
      );
    }
    logger.i('pendingCompletePurchase completePurchase: ${purchaseDetails.pendingCompletePurchase}');

    ///告知google服务 订单完成
    ///Mark that purchased content has been delivered to the user
    if (purchaseDetails.pendingCompletePurchase) {
      logger.i('pendingCompletePurchase completePurchase');
      await _connection.completePurchase(purchaseDetails);
    }
  }
}
