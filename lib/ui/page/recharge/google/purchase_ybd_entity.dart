///Google支付回调数据对象
class YBDPurchaseEntity {
  ///透传的用户ID
  String? obfuscatedAccountId;

  ///透传的预订单ID
  String? obfuscatedProfileId;

  ///google的订单ID
  String? orderId;

  ///google的加密验证数据
  String? signature;
  String? originalJson;

  ///google token
  String? purchaseToken;
  String? purchaseID;

  ///预订单Id
  String? productID;

  ///google支付时间（google服务器生成的，与本地时间可能存在差异）
  int? purchaseTime;

  YBDPurchaseEntity(this.obfuscatedAccountId, this.obfuscatedProfileId, this.orderId, this.signature, this.originalJson,
      this.purchaseToken, this.purchaseID, this.productID, this.purchaseTime);

  @override
  String toString() {
    return 'PurchaseBean{obfuscatedAccountId: $obfuscatedAccountId, obfuscatedProfileId: $obfuscatedProfileId, orderId: $orderId, signature: $signature, originalJson: $originalJson, purchaseToken: $purchaseToken, purchaseID: $purchaseID, productID: $productID purchaseTime:$purchaseTime}';
  }
  void toStringziUM5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
