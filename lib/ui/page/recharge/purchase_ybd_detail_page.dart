
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:intl/intl.dart';
import 'package:pay_web/pay_web.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';

import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/navigator/navigator_ybd_helper.dart';
import '../../../common/service/iap_service/iap_ybd_def.dart';
import '../../../common/service/iap_service/iap_ybd_service.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/dialog_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/numeric_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../common/util/toast_ybd_util.dart';
import '../../../module/payment/entity/pay_ybd_config_entity.dart';
import '../../../module/payment/payment_ybd_api_helper.dart';
import '../../../redux/app_ybd_state.dart';
import '../../widget/button/delay_ybd_gesture_detector.dart';
import '../../widget/loading_ybd_circle.dart';
import '../../widget/my_ybd_app_bar.dart';
import '../../widget/scaffold/nav_ybd_back_button.dart';
import '../follow/widget/place_ybd_holder_view.dart';
import 'bloc/purchase_ybd_detail_bloc.dart';
import 'easypaisa/easypaisa_ybd_page.dart';
import 'razorpay/entity/razorpay_ybd_entity.dart';
import 'recharg_ybd_pay_config_helper.dart';
import 'top_ybd_up_banner.dart';
import 'widget/product_ybd_info_item.dart';
import 'widget/topup_ybd_bottom_note.dart';

/// 苹果支付购买页面
class YBDPurchaseDetailPage extends StatefulWidget {
  YBDPayConfigDataPayload? payload;
  List<YBDPayConfigDataItem?>? items;
  double? rate;
  String? currency;
  String? nickName;
  String? name;
  String location;

  ///充值类型：活动的时候使用（ep）
  String topUpType;

  ///有child的选项  使用parentName来判断类型：PayMax
  String? parentName;

  YBDPurchaseDetailPage({
    this.nickName,
    this.payload,
    this.items,
    this.rate,
    this.currency,
    this.name,
    this.parentName,
    this.location = '',
    this.topUpType = '',
  });

  @override
  State<StatefulWidget> createState() => _YBDPurchaseDetailPageState();
}

class _YBDPurchaseDetailPageState extends State<YBDPurchaseDetailPage> {
  /// 选中的商品索引号，默认选中第一个商品
  int _selectedIndex = 0;

  /// 是否显示了弹框
  bool _isShowedDialog = false;

  /// 页面 bloc
  late YBDPurchaseDetailBloc _bloc;

  TextEditingController _controller = TextEditingController();

  /// 是否为初始状态
  bool _isInitState = true;

  ///充值ep 缓存
  YBDPayConfigData? payConfigData;

  @override
  void initState() {
    super.initState();
    YBDCommonTrack().commonTrack(YBDTAProps(
      location: YBDTAClickName.topup,
      module: YBDTAModule.common,
      previous_route: Get.previousRoute.shortRoute,
    ));
    _bloc = YBDPurchaseDetailBloc(context);
    _bloc.add(PurchaseDetailEvent.LoadProduct);

    YBDIapService.instance.setIapStatusCallback((status, productId) {
      switch (status) {
        case IapStatus.Pending:
          // 正在购买
          YBDDialogUtil.showPurchaseLoading(context);
          break;
        case IapStatus.PurchaseError:
          YBDDialogUtil.hidePurchaseLoading(context);
          break;
        case IapStatus.Success:
          YBDDialogUtil.hidePurchaseLoading(context);
          YBDToastUtil.toast(translate('success'));
          Navigator.pop(context);
          break;
        case IapStatus.DeliverError:
          YBDDialogUtil.hidePurchaseLoading(context);
          // TODO: 翻译
          // YBDToastUtil.toast('Transaction is being processed');
          Navigator.pop(context);
          break;
        default:
          break;
      }
    });
    logger.v('topup gift location purchase detail initState : ${widget.location}');
    if (widget.topUpType.isNotEmpty) {
      _getPurchaseDate();
    }

    ///获取实时金币 同步到本地用户信息
    ApiHelper.getBalance(context, () async {
      if (mounted) {
        setState(() {});
      }
    });
  }
  void initStatepA5oJoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _bloc.close();

    // 移除回调
    YBDIapService.instance.setIapStatusCallback(null);
    super.dispose();
  }
  void dispose3VgKIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  submit() {
    print('submit  _selectedIndex:$_selectedIndex  _controller.text:${_controller.text} currency${widget.currency}');
    if (_selectedIndex == -1) {
      if (_controller.text.isEmpty) {
        print('submit  _selectedIndex:_controller.text.isEmpty');
        YBDToastUtil.toast(getHint());
        return;
      }
      if (!RegExp(widget.payload!.regex!).hasMatch(_controller.text)) {
        print('submit  _selectedIndex:!RegExp(widget.payload.regex).hasMatch(_controller.text)');
        YBDToastUtil.toast(getHint());
        return;
      }

      ///手动 选择金额
      getOrderAndUrl(double.parse(_controller.text), widget.currency);

      ///手动输入金额
      /* FlutterBoost.singleton.open(getNativePageUrl(NativePageName.TOP_UP), urlParams: {
        'topUpType': submitTypeChange(),

        ///支付类型
        'amount': double.parse(_controller.text),

        ///支付金额
        'productId': '-1',

        ///支付productId  （Google  使用）
        'channel': widget.name,

        ///支付通道  PayMax使用
      });*/
    } else {
      getOrderAndUrl(widget.items![_selectedIndex]!.money, widget.currency,
          productId: widget.items![_selectedIndex]?.productId);

      ///item 选择金额
      // FlutterBoost.singleton.open(getNativePageUrl(NativePageName.TOP_UP), urlParams: {
      //   'topUpType': submitTypeChange(),
      //   'amount': widget.items[_selectedIndex].money,
      //   'productId': widget.items[_selectedIndex].productId,
      //   'channel': widget.name,
      // });
    }
    print('input legal');

    logger.v('topup gift location purchase detail submit : ${widget.location}');
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: YBDLocationName.PURCHASE_DETAIL_PAGE,
      itemName: 'submit',
      adUnitName: widget.location,
    ));
  }

  ///根据name判断支付类型
  int submitTypeChange() {
    print("submitTypeChange widget.name:${widget.name} parentName:${widget.parentName}");
    int type = 1;
    if (widget.parentName?.toUpperCase() == 'PAYERMAX') {
      logger.v('PAYERMAX name:${widget.name} parentName:${widget.parentName}');
      type = 6;
      return type;
    }
    switch (widget.name?.toUpperCase()) {
      case 'GOOGLE':
        type = 1;
        break;
      case 'EASYPAISA':
        type = 2;
        break;
      case 'PAYPAL':
        type = 3;
        break;
      case 'RAZORPAY':
        type = 4;
        break;
      case 'PAYERMAX':
      case 'VISA/MASTERCARD':
      case 'ETISALAT':
      case 'E_WALLET':
      case 'UPI_CB':
      case 'NET_BANK':
      case 'STRIP':
        type = 6;
        break;
    }
    return type;
  }
  void submitTypeChangeGTboAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: YBDMyAppBar(
        title: Text(
          // TODO: 翻译
          widget.nickName ?? 'Top up',
          style: TextStyle(
            color: Colors.white,
            fontSize: ScreenUtil().setSp(32),
          ),
        ),
        backgroundColor: YBDTPStyle.heliotrope,
        leading: YBDNavBackButton(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Container(
              width: ScreenUtil().setWidth(48),
              child: Image.asset('assets/images/icon_top_up_record.webp'),
            ),
            onPressed: () {
              logger.v('clicked recharge record btn');
              // YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.top_up_record);
              // 跳转到账单页面
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.bill);
            },
          ),
        ],
        elevation: 0.0,
      ),
      body: Container(
        width: ScreenUtil().screenWidth,
        height: ScreenUtil().screenHeight,
        decoration: YBDTPStyle.gradientDecoration,
        child: BlocProvider<YBDPurchaseDetailBloc>(
          create: (context) {
            return _bloc;
          },
          child: _contentView(),
        ),
      ),
    );
  }
  void buildBjqGroyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 页面内容
  Widget _contentView() {
    logger.v('widget.topUpType---1: ${widget.topUpType}');
    if (widget.topUpType.isNotEmpty && (null == widget.items || widget.items!.isEmpty)) {
      logger.v('widget.topUpType---2: ${widget.topUpType}');
      if (_isInitState) {
        return Container(
          child: YBDLoadingCircle(),
        );
      } else {
        return Container(
          child: YBDPlaceHolderView(text: translate('no_data')),
        );
      }
    }
    return BlocBuilder<YBDPurchaseDetailBloc, YBDPurchaseDetailBlocState>(
      builder: (context, state) {
        if (state.isLoading) {
          // 显示加载框
          return YBDLoadingCircle();
        } else {
          if (Platform.isIOS && state.products!.isNotEmpty) {
            // 展示商品
            return SingleChildScrollView(
              child: Column(
                children: [
                  // GestureDetector(
                  //   onTap: () {
                  //     logger.v('clicked in-app top up banner');
                  //     YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.top_up_gift + "/true");
                  //   },
                  //   child: Container(
                  //     padding: EdgeInsets.all(ScreenUtil().setWidth(14)),
                  //     width: ScreenUtil().screenWidth,
                  //     child: Image.asset('assets/images/recharge_banner.webp', fit: BoxFit.fitWidth),
                  //   ),
                  // ),
                  _topBeansContainer(),
                  _purchaseView(state.products),
                ],
              ),
            );
          } else if (Platform.isAndroid && widget.items!.isNotEmpty) {
            return SingleChildScrollView(
              child: Column(
                children: [
                  _topBeansContainer(),
                  _purchaseView(null),
                ],
              ),
            );
          } else {
            // 没有商品显示缺省页
            // TODO: 翻译
            return YBDPlaceHolderView(text: 'No Data');
          }
        }
      },
    );
  }
  void _contentViewZjlYnoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///顶部beans
  Widget _topBeansContainer() {
    final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    return Stack(children: [
      Container(
        child: Image.asset('assets/images/topup/y_top_up_pirate@2x.webp'),
      ),
      Positioned(
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        child: Container(
          padding: EdgeInsets.only(top: ScreenUtil().setHeight(56), bottom: ScreenUtil().setHeight(45)),
          child: Column(
            children: [
              Image.asset(
                'assets/images/topup/y_top_up_beans@2x.webp',
                width: ScreenUtil().setWidth(82),
                height: ScreenUtil().setHeight(89),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(27),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    translate('available_beans'),
                    style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white.withOpacity(0.6)),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(25),
                  ),
                  Text(
                    store.state.bean!.money! < 1000
                        ? store.state.bean!.money.toString()
                        : NumberFormat('0,000').format(store.state.bean!.money),
                    style: TextStyle(fontSize: ScreenUtil().setSp(36), color: Colors.white),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    ]);
  }
  void _topBeansContainercgCOmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 展示商品购买页面内容
  Widget _purchaseView(List<ProductDetails>? products) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setWidth(40)),
          // 轮播图
          YBDTopUpBanner(),
          SizedBox(height: ScreenUtil().setWidth(30)),
          // 商品列表
          _productsListView(products),
          // 其他金额段落
          _otherAmountSection(),
          SizedBox(height: ScreenUtil().setWidth(60)),
          // 提交按钮
          _submitBtn(products),
          SizedBox(height: ScreenUtil().setWidth(50)),
          // 底部提示语
          YBDTopupBottomNote(),
          SizedBox(height: Platform.isIOS ? ScreenUtil().setWidth(200) : ScreenUtil().setWidth(80)),
        ],
      ),
    );
  }
  void _purchaseViewm3MiEoyelive(List<ProductDetails>? products) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 商品列表
  Widget _productsListView(List<ProductDetails>? products) {
    var productsList;

    if (Platform.isIOS) {
      productsList = products;
    } else {
      productsList = widget.items;
    }

    return Container(
      alignment: Alignment.center,
      // height: ScreenUtil().setWidth(430),
      width: ScreenUtil().screenWidth,
      child: Wrap(
        spacing: ScreenUtil().setWidth(15),
        runSpacing: ScreenUtil().setWidth(15),
        children: List.generate(productsList?.length ?? 0, (index) {
          String? beans;
          String price;

          if (Platform.isIOS) {
            beans = products![index].title;
            price = products[index].price;
          } else {
            beans = YBDNumericUtil.format(productsList[index].value);
            price = '${widget.currency} ${YBDNumericUtil.formatWithDot(productsList[index].money)}';
          }

          if (Platform.isAndroid) {
            beans = widget.items![index]!.value.toString();
            price = '${widget.currency} ${widget.items![index]!.money!.toStringAsFixed(2)}';
          }

          return GestureDetector(
            onTap: () {
              logger.v('select product index : $index');
              _controller.clear();
              setState(() {
                _selectedIndex = index;
              });
            },
            child: YBDProductInfoItem(
              isSelected: _selectedIndex == index,
              beans: beans,
              price: price,
            ),
          );
        }),
      ),
    );
  }
  void _productsListViewa5mMAoyelive(List<ProductDetails>? products) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getChange() {
    try {
      return (int.parse(_controller.text) * widget.rate!).toInt().toString();
    } catch (e) {
      print(e);
      return '0';
    }
  }

  getHint() {
    try {
      return widget.payload!.desc[Localizations.localeOf(context).toString().toLowerCase()];
    } catch (e) {
      print(e);
      return '0';
    }
  }

  /// 输入其他金额的段落
  Widget _otherAmountSection() {
    if (Platform.isIOS) {
      return Container();
    }

    if (widget.payload != null) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
        child: Column(
          children: [
            SizedBox(
              height: ScreenUtil().setWidth(38),
            ),
            Row(
              children: [
                Text(
                  translate('other_amount'),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: ScreenUtil().setSp(28),
                  ),
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setWidth(38),
            ),
            Row(
              children: [
                Container(
                  width: ScreenUtil().setWidth(80),
                  child: Text(
                    widget.currency!,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: ScreenUtil().setSp(28),
                    ),
                  ),
                ),
                Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white.withOpacity(0.5), width: ScreenUtil().setWidth(0.5)),
                        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(4)))),
                    width: ScreenUtil().setWidth(324),
                    height: ScreenUtil().setWidth(74),
                    child: TextField(
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: ScreenUtil().setSp(28),
                      ),
                      onChanged: (_) {
                        if (_.isNotEmpty) {
                          _selectedIndex = -1;
                        }
                        setState(() {});
                      },
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                      controller: _controller,
                      decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(14), vertical: ScreenUtil().setWidth(10)),
                          border: InputBorder.none,
                          counterText: '',
                          hintText: translate('other_amount'),
                          hintStyle: TextStyle(
                            color: Colors.white.withOpacity(0.5),
                          )),
                    )),
                Text(
                  // TODO: 翻译
                  "  =  ",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: ScreenUtil().setSp(42),
                  ),
                ),
                Container(
                  width: ScreenUtil().setWidth(30),
//                  height: ScreenUtil().setWidth(50),
                  child: Image.asset('assets/images/topup/y_top_up_beans@2x.webp'),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(12),
                ),
                Text(
                  getChange(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: ScreenUtil().setSp(32),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: ScreenUtil().setWidth(14),
            ),
            Row(children: [
              Container(
                width: ScreenUtil().setWidth(80),
              ),
              Expanded(
                child: Text(
                  getHint(),
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.7),
                    fontWeight: FontWeight.w300,
                    fontSize: ScreenUtil().setSp(24),
                  ),
                ),
              )
            ])
          ],
        ),
      );
    }
    return Container();
  }
  void _otherAmountSectionIr27Hoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 提交按钮
  Widget _submitBtn(List<ProductDetails>? products) {
    return Container(
      child: YBDDelayGestureDetector(
        // 提交按钮
        // child: YBDBlueGradientButton(title: 'Submit'),
        child: Container(
          width: ScreenUtil().screenWidth,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(580),
                height: ScreenUtil().setWidth(88),
                decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                    Color(0xff5E94E7),
                    Color(0xff47CDCC),
                  ], begin: Alignment.centerLeft, end: Alignment.centerRight),
                  borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(50)))),
                ),
                child: Text(
                  translate('Submit'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(28),
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
        onTap: () async {
          if (Platform.isIOS) {
            logger.v('clicked submit product index : $_selectedIndex');
            final product = products![_selectedIndex];

            // 点击购买按钮事件的埋点
            YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
              YBDEventName.CLICK_EVENT,
              location: YBDLocationName.PURCHASE_DETAIL_PAGE,
              itemName: product.id,
              adUnitName: widget.location,
            ));

            YBDDialogUtil.showPurchaseLoading(context);
            await YBDIapService.instance.purchaseProduct(product);
            YBDDialogUtil.hidePurchaseLoading(context);
          } else {
            submit();
          }
        },
      ),
    );
  }
  void _submitBtnJR3CEoyelive(List<ProductDetails>? products) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///获取订单和网页Url
  getOrderAndUrl(double? amount, String? currency, {String? productId}) async {
    int topUpType = submitTypeChange();
    logger.v('getOrderAndUrl amount:$amount currency:$currency name:${widget.name} productId:$productId');
    print('getOrderAndUrl amount:$amount currency:$currency name:${widget.name} productId:$productId');
    if (topUpType == 2) {
      int amountInt = amount!.toInt();
      YBDDialogUtil.showEPBottomSheet(
        context,
        translate('easypaisa_mobile_account'),
        translate('easypaisa_shop'),
        itemOneCallback: () async {
          String? accountNumber = await YBDSPUtil.getEPAccountNUmber();
          Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (_) => YBDEasypaisaPage(
                'MA',
                amountInt,
                currency,
                accountNumber: accountNumber,
              ),
            ),
          ).then((value) {
            logger.v('payermax OTC 000000 name:${payConfigData?.name} currency:${payConfigData?.currency}');
            _getPurchaseDate();
          });
        },
        itemTwoCallback: () async {
          String? accountNumber = await YBDSPUtil.getEPAccountNUmber();
          Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (_) => YBDEasypaisaPage(
                'OTC',
                amountInt,
                currency,
                accountNumber: accountNumber,
              ),
            ),
          ).then((value) {
            logger.v('payermax OTC 000001');
            _getPurchaseDate();
          });
        },
      );
      return;
    } else if (topUpType == 6) {
      logger.v('payermax amount:$amount currency:$currency channel:${widget.name}');
      YBDDialogUtil.showLoading(context);
      YBDRazorpayResultEntity? payOrder = await YBDPaymentApiHelper.payOrders(
          context, amount, widget.name == "strip" ? widget.name : "payermax", currency,
          channel: widget.name, productId: productId);
      if (payOrder?.code == '0') {
        logger.v('payermax order Su: payOrder.data:${payOrder!.data!.toJson()}');
        logger.v(
            'payermax toNativeWebView title:${widget.nickName} requestUrl:${payOrder.data!.requestUrl} -orderId:${payOrder.data!.orderId}');
        // YBDRechargeToNative.toNativeWebView(widget.nickName, payOrder.data.requestUrl);
        PayWeb.openWebPayView(payOrder.data!.requestUrl!, widget.nickName!, '');
        print('clicked openWebPayView btn');
      } else if (payOrder != null) {
        YBDLogUtil.e('getPayMaxOrder Error:-->code:${payOrder.code}  message:${payOrder.message}');
        YBDToastUtil.toast('${payOrder.message}');
      } else {
        YBDToastUtil.toast('Failed!');
      }
      YBDDialogUtil.hideLoading(context);
    } else if (topUpType == 4) {
      logger.v('razorpay amount:$amount currency:$currency channel:${widget.name}');
      YBDDialogUtil.showLoading(context);
      YBDRazorpayResultEntity? payOrder = await YBDPaymentApiHelper.payOrders(context, amount, 'razorpay', currency,
          channel: widget.name, productId: productId);
      YBDDialogUtil.hideLoading(context);
      if (payOrder?.code == '0') {
        /*  YBDRechargeToNative.toNativeRecharge(
            4, payOrder?.data?.amount?.toDouble(), 'razorpay', payOrder?.data?.id, payOrder?.data?.orderId);*/
        YBDNavigatorHelper.navigateTo(
            context,
            YBDNavigatorHelper.razor_pay +
                "/${payOrder?.data?.amount}/$currency/${payOrder?.data?.id}/${payOrder?.data?.orderId}");
      } else if (payOrder != null) {
        YBDLogUtil.e('razorpay Error:-->code:${payOrder.code}  message:${payOrder.message}');
        YBDToastUtil.toast('${payOrder.message}');
      } else {
        YBDToastUtil.toast('Failed!');
      }
    } else {
      YBDToastUtil.toast(translate('upgrade_notice'));
    }
  }

  ///充值活动需要 在这个页面获取具体数据
  _getPurchaseDate() async {
    if (payConfigData == null) {
      payConfigData = await YBDPayConfigHelper.getTopUpTypeData(context);
    }
    _isInitState = false;
    if (payConfigData != null) {
      widget.nickName = payConfigData!.nickName;
      widget.payload = payConfigData!.payload;
      widget.items = payConfigData!.items;
      widget.rate = payConfigData!.rate;
      widget.currency = payConfigData!.currency;
      widget.name = payConfigData!.name;
      if (mounted) setState(() {});
    }
  }

  @override
  void deactivate() {
    bool isBack = ModalRoute.of(context)!.isCurrent;
    if (isBack) {
      // 限于从其他页面返回到当前页面时执行，首次进入当前页面不执行
      // 注：此方法在iOS手势返回时，不执行此处
      // print('从其他页面返回到${widget.runtimeType}页');
      logger.v('payermax amount: 000001');
    } else {
      // 离开当前页面或退出当前页面时执行
      // print('离开或退出${widget.runtimeType}页');
      logger.v('payermax amount: 000002');
    }
    super.deactivate();
  }
  void deactivatekcY1eoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  toNativeTopUp() {
    // TODO: 测试代码 添加跳转
    ///手动输入金额
    // FlutterBoost.singleton.open(getNativePageUrl(NativePageName.TOP_UP), urlParams: {
    //   'topUpType': submitTypeChange(),
    //
    //   ///支付类型
    //   'amount': double.parse(_controller.text),
    //
    //   ///支付金额
    //   'productId': '-1',
    //
    //   ///支付productId  （Google  使用）
    //   'channel': widget.name,
    //
    //   ///支付通道  PayMax使用
    // });
  }
}
