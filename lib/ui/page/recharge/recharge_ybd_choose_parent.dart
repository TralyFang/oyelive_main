import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../base/base_ybd_state.dart';
import '../../../module/payment/entity/pay_ybd_config_entity.dart';
import 'recharge_ybd_choose_item.dart';
import 'package:oyelive_main/ui/page/game_room/tp_room_page/ext/list_ybd_ext.dart';

class YBDRechargeChooseParent extends StatefulWidget {
  YBDPayConfigData? payConfigData;

  /// 点击返回按钮回调
  BackCallbackFunc? backCallbackFunc;

  YBDRechargeChooseParent(this.payConfigData, {this.backCallbackFunc});

  @override
  YBDRechargeChooseParentState createState() => new YBDRechargeChooseParentState();
}

class YBDRechargeChooseParentState extends BaseState<YBDRechargeChooseParent> with SingleTickerProviderStateMixin {
  bool isExpaned = true;

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    return Column(
      children: [
        Container(
          alignment: Alignment.center,
          height: ScreenUtil().setWidth(110),
          decoration: BoxDecoration(),
          margin: EdgeInsets.only(left: ScreenUtil().setWidth(30), right: ScreenUtil().setWidth(26)),
          child: Row(
            children: [
              YBDNetworkImage(
                imageUrl: widget.payConfigData!.icon ?? "",
                width: ScreenUtil().setWidth(50),
                height: ScreenUtil().setWidth(50),
              ),
              SizedBox(
                width: ScreenUtil().setWidth(42),
              ),
              Text(
                widget.payConfigData!.nickName!,
                style:
                    TextStyle(fontSize: ScreenUtil().setSp(28), color: Color(0xFFFFFFFF), fontWeight: FontWeight.w400),
              ),
              Expanded(
                child: Text(''), // 中间用Expanded控件
              ),
              YBDNetworkImage(
                imageUrl: widget.payConfigData!.suffixIcon ?? "",
                width: ScreenUtil().setWidth(50),
                height: ScreenUtil().setWidth(50),
              ),
//                RotationTransition(
//                  turns: Tween(begin: 0.0, end: 0.25).animate(_controller),
//                  child: Icon(
//                    Icons.navigate_next,
//                    color: Colors.white,
//                  ),
//                ),
            ],
          ),
        ),
        AnimatedContainer(
          duration: const Duration(milliseconds: 1000),
          height: isExpaned ? null : 0,
          margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(32), right: ScreenUtil().setWidth(32), bottom: ScreenUtil().setWidth(20)),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
              color: Colors.white.withOpacity(0.1)),
          child: ListView.separated(
            padding: EdgeInsets.all(0),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: widget.payConfigData!.child!.length,
            itemBuilder: (_, index) => YBDRechargeChooseItem(
              widget.payConfigData!.child![index]!.nickName,
              widget.payConfigData!.child![index]!.icon,
              '',
              widget.payConfigData!.child![index]!.currency,
              widget.payConfigData!.child![index]!.name,
              isChild: true,
              payload: widget.payConfigData!.child![index]!.payload,
              items: widget.payConfigData!.child![index]!.items?.removeNulls(),
              rate: widget.payConfigData!.child![index]!.rate,
              parentName: widget.payConfigData!.name,
              backCallbackFunc: widget.backCallbackFunc,
            ),
            separatorBuilder: (_, index) => Padding(
              padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
              child: Divider(
                height: ScreenUtil().setWidth(1),
                thickness: ScreenUtil().setWidth(1),
                color: Colors.white.withOpacity(0.15),
              ),
            ),
          ),
        )
      ],
    );
  }
  void myBuild3ebNLoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  late AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    );
    _controller.addListener(() {
      setState(() {});
    });
    super.initState();
  }
  void initStateaO1qWoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDRechargeChooseParent oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetKzaB3oyelive(YBDRechargeChooseParent oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
