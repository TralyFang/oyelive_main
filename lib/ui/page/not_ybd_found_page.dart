import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class YBDNotFoundPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color(0xffA33CCA),
        leading: Container(
          width: ScreenUtil().setWidth(88),
          child: TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            style: TextButton.styleFrom(padding: EdgeInsets.all(ScreenUtil().setWidth(20))),
            // padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/empty/not_found.webp",
              width: ScreenUtil().setWidth(235),
            ),
            SizedBox(
              height: ScreenUtil().setWidth(34),
            ),
            Text(
              "404 page not found",
              style: TextStyle(
                color: Colors.black,
                fontSize: ScreenUtil().setSp(24),
              ),
            )
          ],
        ),
      ),
    );
  }
  void buildWfXhSoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
