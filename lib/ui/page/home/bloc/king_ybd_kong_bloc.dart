import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oyelive_main/ui/page/home/entity/king_ybd_kong_entity.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';

/// 事件类型
enum KingKongEvent {
  Refresh,
}

/// 主页广告栏事件和状态管理器
class YBDKingKongBloc extends Bloc<KingKongEvent, YBDKingKongState> {
  BuildContext context;

  /// 国家码
  // String _countryCode;

  YBDKingKongBloc(this.context) : super(YBDKingKongState(isInit: true));

  @override
  Stream<YBDKingKongState> mapEventToState(KingKongEvent event) async* {
    logger.v('map advertise event : $event');
    switch (event) {
      case KingKongEvent.Refresh:
        logger.v('event : $event, request king kong');
        YBDKingKongEntity? entity = await ApiHelper.kingKongList(context);

        if (null != entity && entity.record != null) {
          logger.v('king kong list amount : ${entity.record?.length}');
          yield YBDKingKongState(records: entity.record);
        } else {
          logger.v('request no entity list, not yield');
        }
        break;
    }
  }
  void mapEventToStateI74ksoyelive(KingKongEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDKingKongState {
  /// 初始状态
  bool isInit;

  /// 金刚区数据
  List<YBDKingKongRecord?>? records;

  YBDKingKongState({this.records, this.isInit: false});
}
