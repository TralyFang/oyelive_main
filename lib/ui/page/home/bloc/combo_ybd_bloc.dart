import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../generated/json/base/json_convert_content.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../entity/combo_ybd_rank_entity.dart';

enum ComboEvent {
  Refresh,
}

/// explor 页面 combo 排行列表数据
class YBDComboBloc extends Bloc<ComboEvent, YBDComboBlocState> {
  BuildContext context;

  YBDComboBloc(this.context) : super(YBDComboBlocState());

  @override
  Stream<YBDComboBlocState> mapEventToState(ComboEvent event) async* {
    logger.v('map combo event : $event');

    switch (event) {
      case ComboEvent.Refresh:
        {
          List<YBDRoomInfo?>? rankList = await requestFollowers();

          if (null != rankList && rankList.isNotEmpty) {
            logger.v('yield combo list length : ${rankList.length}');
          } else {
            logger.v('request null rank data, no yield');
          }

          yield YBDComboBlocState(users: rankList);
        }
    }
  }
  void mapEventToStateADNVNoyelive(ComboEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求 combo 排行榜数据
  Future<List<YBDRoomInfo?>?> requestFollowers() async {
    YBDComboRankEntity? rankInfoEntity = await ApiHelper.comboRank(context, 1);

    if (null != rankInfoEntity && null != rankInfoEntity.record && null != rankInfoEntity.record!.rank) {
      logger.v('combo rank list amount : ${rankInfoEntity.record!.rank!.length}');
      List<YBDRoomInfo?> userInfoList = rankInfoEntity.record!.rank!.map((e) => e!.sender).toList();
      return userInfoList;
    } else {
      logger.v('combo rank list is null');
      return null;
    }
  }
}

// combo 排行榜状态
class YBDComboBlocState {
  /// 排行榜数据
  List<YBDRoomInfo?>? users;

  YBDComboBlocState({this.users});
}
