import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../entity/rank_ybd_info_entity.dart';

enum GiftersEvent {
  Refresh,
}

/// 礼物排行榜状态管理器
class YBDGiftersBloc extends Bloc<GiftersEvent, YBDGiftersBlocState> {
  BuildContext context;

  YBDGiftersBloc(this.context) : super(YBDGiftersBlocState());

  @override
  Stream<YBDGiftersBlocState> mapEventToState(GiftersEvent event) async* {
    logger.v('map gifters event : $event');

    switch (event) {
      case GiftersEvent.Refresh:
        {
          List<YBDRoomInfo?>? rankList = await requestGifters();

          if (null != rankList && rankList.isNotEmpty) {
            logger.v('yield gifters list length : ${rankList.length}');
          } else {
            logger.v('request no rank data, not yield');
          }

          yield YBDGiftersBlocState(gifters: rankList);
        }
    }
  }
  void mapEventToStateDReTaoyelive(GiftersEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求礼物排行榜数据
  Future<List<YBDRoomInfo?>?> requestGifters() async {
    YBDRankInfoEntity? rankInfoEntity = await ApiHelper.giftersList(context, 0, 3, 3);

    if (null != rankInfoEntity && null != rankInfoEntity.record && null != rankInfoEntity.record!.rank) {
      logger.v('gifters rank list amount : ${rankInfoEntity.record!.rank!.length}');
      return rankInfoEntity.record!.rank;
    } else {
      logger.v('gifters rank list is null');
      return null;
    }
  }
}

/// 礼物排行榜状态
class YBDGiftersBlocState {
  /// 排行榜数据
  List<YBDRoomInfo?>? gifters;

  YBDGiftersBlocState({this.gifters});
}
