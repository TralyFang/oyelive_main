import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../entity/banner_ybd_advertise_entity.dart';

/// 事件类型
enum AdvertiseEvent {
  Refresh,
}

/// 主页广告栏事件和状态管理器
class YBDAdvertiseBloc extends Bloc<AdvertiseEvent, YBDAdvertiseBlocState> {
  BuildContext context;

  /// 国家码
  // String _countryCode;

  YBDAdvertiseBloc(this.context) : super(YBDAdvertiseBlocState(isInit: true));

  @override
  Stream<YBDAdvertiseBlocState> mapEventToState(AdvertiseEvent event) async* {
    logger.v('map advertise event : $event');
    switch (event) {
      case AdvertiseEvent.Refresh:
        logger.v('event : $event, request ad');

        // if (null == _countryCode || _countryCode.isEmpty) {
        //   logger.v('country code is empty, request country code');
        //   try {
        //     _countryCode = await YBDCommonUtil.getDefaultCountryCode(context);
        //   } catch (e) {
        //     logger.v('get country code error : $e');
        //   }
        //   logger.v('get country code : $_countryCode');
        // }

        List<YBDBannerAdvertiseRecord?>? advertiseList = await requestBannerAdvertise();

        if (null != advertiseList) {
          logger.v('advertise list amount : ${advertiseList.length}');

          /// 根据国家码过滤广告
          List<YBDBannerAdvertiseRecord?> filterAd = advertiseList.where((ad) {
            if ((ad!.adurl!.contains('countrycode=PK') && YBDUserUtil.getLoggedUser(context)?.country == 'PK')) return true;
            if ((ad.adurl!.contains('countrycode=IN') && YBDUserUtil.getLoggedUser(context)?.country == 'IN')) return true;
            if (!ad.adurl!.contains('countrycode=PK') && !ad.adurl!.contains('countrycode=IN')) return true;

            return false;
          }).toList();
          logger.v('yield filter ad amount : ${filterAd.length}');
          yield YBDAdvertiseBlocState(banners: filterAd);
        } else {
          logger.v('request no banner list, not yield');
          // 可能是网络原因返回 null，显示旧数据
        }
        break;
    }
  }
  void mapEventToStateSt9LQoyelive(AdvertiseEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  int _type = 1;

  int get type => _type;

  set type(int value) {
    _type = value;
  }

  /// 请求广告数据
  Future<List<YBDBannerAdvertiseRecord?>?> requestBannerAdvertise() async {
    logger.v('request banner advertise');
    YBDBannerAdvertiseEntity? advertiseEntity = await ApiHelper.homeBannerAdvertise(context, adType: type);

    logger.v('request banner advertise done');
    if (null != advertiseEntity && advertiseEntity.record != null) {
      logger.v('advertise amount : ${advertiseEntity.record!.length}');
      return advertiseEntity.record;
    } else {
      logger.v('banner advertise is null');
      return null;
    }
  }
}

/// 广告状态
class YBDAdvertiseBlocState {
  /// 初始状态
  bool isInit;

  /// 广告数据
  List<YBDBannerAdvertiseRecord?>? banners;

  YBDAdvertiseBlocState({this.banners, this.isInit: false});
}
