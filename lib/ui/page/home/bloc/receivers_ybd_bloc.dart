import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../entity/rank_ybd_info_entity.dart';

/// 事件类型
enum ReceiversEvent {
  Refresh,
}

/// 金币排行榜状态管理器
class YBDReceiversBloc extends Bloc<ReceiversEvent, YBDReceiversBlocState> {
  BuildContext context;

  YBDReceiversBloc(this.context) : super(YBDReceiversBlocState());

  @override
  Stream<YBDReceiversBlocState> mapEventToState(ReceiversEvent event) async* {
    logger.v('map receivers event : $event');

    switch (event) {
      case ReceiversEvent.Refresh:
        {
          List<YBDRoomInfo?>? rankList = await requestReceivers();

          if (null != rankList && rankList.isNotEmpty) {
            logger.v('yield receivers list length : ${rankList.length}');
          } else {
            logger.v('request no rank data, not yield');
          }

          yield YBDReceiversBlocState(receivers: rankList);
        }
    }
  }
  void mapEventToStateshoZloyelive(ReceiversEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求金币排行榜数据
  Future<List<YBDRoomInfo?>?> requestReceivers() async {
    YBDRankInfoEntity? rankInfoEntity = await ApiHelper.receiversList(context, 0, 3, 3);

    if (null != rankInfoEntity && null != rankInfoEntity.record && null != rankInfoEntity.record!.rank) {
      logger.v('receivers rank list amount : ${rankInfoEntity.record!.rank!.length}');
      return rankInfoEntity.record!.rank;
    } else {
      logger.v('receivers rank list is null');
      return null;
    }
  }
}

/// 金币排行榜状态
class YBDReceiversBlocState {
  /// 排行榜数据
  List<YBDRoomInfo?>? receivers;

  YBDReceiversBlocState({this.receivers});
}
