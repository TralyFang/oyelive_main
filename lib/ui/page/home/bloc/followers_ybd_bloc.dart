import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../entity/rank_ybd_info_entity.dart';

enum FollowersEvent {
  Refresh,
}

/// 粉丝排行榜状态管理器
class YBDFollowersBloc extends Bloc<FollowersEvent, YBDFollowersBlocState> {
  BuildContext context;

  YBDFollowersBloc(this.context) : super(YBDFollowersBlocState());

  @override
  Stream<YBDFollowersBlocState> mapEventToState(FollowersEvent event) async* {
    logger.v('map followers event : $event');

    switch (event) {
      case FollowersEvent.Refresh:
        {
          List<YBDRoomInfo?>? rankList = await requestFollowers();

          if (null != rankList && rankList.isNotEmpty) {
            logger.v('yield followers list length : ${rankList.length}');
          } else {
            logger.v('request no rank data, not yield');
          }

          yield YBDFollowersBlocState(followers: rankList);
        }
    }
  }
  void mapEventToStatebYZxBoyelive(FollowersEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求粉丝排行榜数据
  Future<List<YBDRoomInfo?>?> requestFollowers() async {
    YBDRankInfoEntity? rankInfoEntity = await ApiHelper.followersList(context, 0, 3, 3);

    if (null != rankInfoEntity && null != rankInfoEntity.record && null != rankInfoEntity.record!.rank) {
      logger.v('followers rank list amount : ${rankInfoEntity.record!.rank!.length}');
      return rankInfoEntity.record!.rank;
    } else {
      logger.v('followers rank list is null');
      return null;
    }
  }
}

// 粉丝排行榜状态
class YBDFollowersBlocState {
  /// 排行榜数据
  List<YBDRoomInfo?>? followers;

  YBDFollowersBlocState({this.followers});
}
