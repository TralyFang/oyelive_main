import 'dart:async';


import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/query_ybd_label_rooms_resp_entity.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../util/home_ybd_util.dart';

/// 事件类型
enum ExploreEvent {
  Refresh,
  NextPage,
}

// 加载 explore 房间列表 bloc
class YBDExploreBloc extends Bloc<ExploreEvent, YBDExploreBlocState> {
  BuildContext context;

  /// 列表头部和尾部刷新控制器
  late RefreshController refreshController;

  /// 查询接口数据的 start 索引
  int _apiListIndex = 0;

  /// 一页返回 20 条数据
  final int _pageSize = 20;

  /// 列表数据
  List<YBDRoomInfo?> _roomList = [];

  YBDExploreBloc(this.context) : super(YBDExploreBlocState(isInit: true));

  @override
  Stream<YBDExploreBlocState> mapEventToState(ExploreEvent event) async* {
    logger.v('map explore page event : $event');
    switch (event) {
      case ExploreEvent.Refresh:
        {
          logger.v('event : $event, refresh explore list');
          // 重新加载第一页数据
          List<YBDRoomInfo?>? exploreList = await requestExploreList(0);

          // 刷新完成
          refreshController.refreshCompleted();

          if (null != exploreList) {
            _roomList.clear();

            // 保存房间列表数据
            _roomList.addAll(exploreList);
//            logger.v('yield explore amount : ${_roomList?.length}');

            // 房间去重
            _roomList = YBDHomeUtil.removeDuplicateRoom(_roomList) ?? [];

            if (_roomList.length < _pageSize) {
              logger.v('no more explore data : ${exploreList.length} < $_pageSize');
              // 没有更多数据
              refreshController.loadNoData();
              yield YBDExploreBlocState(rooms: _roomList, noMoreData: true);
            } else {
              // 隐藏没有更多数据的提示
              refreshController.resetNoData();
              yield YBDExploreBlocState(rooms: _roomList, noMoreData: false);
            }
          } else {
            logger.v('explore list is null show empty view');
            // 隐藏没有更多数据的提示
            refreshController.resetNoData();

            // 可能是网络加载错误显示旧数据
            // 更新刷新头部
            yield YBDExploreBlocState(rooms: _roomList);
          }
          break;
        }
      case ExploreEvent.NextPage:
        {
          logger.v('event : $event, next page explore list');
          // 请求下一页数据
          List<YBDRoomInfo?>? exploreList = await requestExploreList(_modifiedStartIndex());

          // 加载完成
          refreshController.loadComplete();

          if (null != exploreList && exploreList.isNotEmpty) {
            // 追加列表数据
            _roomList.addAll(exploreList);
//            logger.v('yield explore amount : ${_roomList?.length}');

            // 房间去重
            _roomList = YBDHomeUtil.removeDuplicateRoom(_roomList) ?? [];

            if (exploreList.length < _pageSize) {
              logger.v('no more explore data : ${exploreList.length} < $_pageSize');
              // 没有更多数据
              refreshController.loadNoData();
              yield YBDExploreBlocState(rooms: _roomList, noMoreData: true);
            } else {
              yield YBDExploreBlocState(rooms: _roomList, noMoreData: false);
            }
          } else {
            logger.v('request no explore list, update refresher');
            yield YBDExploreBlocState(rooms: _roomList);
          }
          break;
        }
    }
  }
  void mapEventToStateL7rwxoyelive(ExploreEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求一页数据
  Future<List<YBDRoomInfo?>?> requestExploreList(int page) async {
    logger.v('request explore list page : $page');
    // TODO: 207 定义为常量 LABEL_ID_NEWTALENT
    YBDQueryLabelRoomsRespEntity? exploreListEntity = await ApiHelper.exploreList(context, page, _pageSize, 207);

    if (null != exploreListEntity) {
      if (null != exploreListEntity.record && null != exploreListEntity.record!.rank) {
        logger.v('explore list length : ${exploreListEntity.record!.rank!.length}');

        if (page == 0) {
          // 刷新时重置索引
          _apiListIndex = 0;
        }

        _apiListIndex += exploreListEntity.record!.rank!.length;
        return exploreListEntity.record!.rank;
      } else {
        logger.v('explore list is null');
        return null;
      }
    } else {
      logger.v('explore entity is null');
      return null;
    }
  }

  /// 修正 start 值
  int _modifiedStartIndex() {
    // 第一次查询 start = 0, offset = 20 返回 23 条数据
    // 第二次查询为了避免漏数据 start = 20, offset = 20
    // print('=================index : ${(_apiListIndex ~/ _pageSize) * _pageSize}');
    return (_apiListIndex ~/ _pageSize) * _pageSize;
  }
  void _modifiedStartIndexOMpl3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// explore 房间列表状态
class YBDExploreBlocState {
  /// 初始状态
  bool isInit;

  /// 房间列表数据
  List<YBDRoomInfo?>? rooms;

  /// 数据已经全部加载完
  bool noMoreData;

  YBDExploreBlocState({
    this.isInit: false,
    this.rooms,
    this.noMoreData: false,
  });
}
