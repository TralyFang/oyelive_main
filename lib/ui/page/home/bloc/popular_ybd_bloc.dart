import 'dart:async';


import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../main.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/query_ybd_label_rooms_resp_entity.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../entity/popular_ybd_rooms_event.dart';
import '../util/home_ybd_util.dart';

/// 事件类型
enum PopularEvent {
  Refresh,
  NextPage,
}

/// 加载 Popular 房间列表事件和状态管理器
class YBDPopularBloc extends Bloc<PopularEvent, YBDPopularBlocState> {
  BuildContext context;

  /// 列表头部和尾部刷新控制器
  late RefreshController refreshController;

  /// 查询接口数据的 start 索引
  int _apiListIndex = 0;

  /// 一页返回 20 条数据
  final int _pageSize = 20;

  /// 列表数据
  List<YBDRoomInfo?> _roomList = [];

  YBDPopularBloc(this.context) : super(YBDPopularBlocState(isInit: true));

  @override
  Stream<YBDPopularBlocState> mapEventToState(PopularEvent event) async* {
    switch (event) {
      case PopularEvent.Refresh:
        {
          logger.v('event : $event, refresh popular list');
          // 重新加载第一页数据
          List<YBDRoomInfo?>? popularList = await requestPopularList(0);

          // 刷新完成
          refreshController.refreshCompleted();
          if (null != popularList) {
            // 刷新已有数据
            _roomList.clear();
            _roomList.addAll(popularList);
//            logger.v('yield popular amount : ${_roomList.length}');

            // 房间去重
            _roomList = YBDHomeUtil.removeDuplicateRoom(_roomList) ?? [];

            /// popular rooms前10条 发给首页，对新用户进行房间推荐
            eventBus.fire(YBDPopularRoomsEvent(_roomList.sublist(0, _roomList.length <= 10 ? _roomList.length : 10)));

            if (_roomList.length < _pageSize) {
              logger.v('no more popular data : ${popularList.length} < $_pageSize');

              // 没有更多数据
              refreshController.loadNoData();
              yield YBDPopularBlocState(rooms: _roomList, noMoreData: true);
            } else {
              // 隐藏没有更多数据的提示
              refreshController.resetNoData();
              yield YBDPopularBlocState(rooms: _roomList);
            }
          } else {
            logger.v('refresh popular list is null show old data : ${_roomList.length}');
            // 隐藏没有更多数据的提示
            refreshController.resetNoData();

            // 可能是网络加载错误显示旧数据
            // 更新刷新头部
            yield YBDPopularBlocState(rooms: _roomList);
          }
          break;
        }
      case PopularEvent.NextPage:
        {
          logger.v('event : $event, next page popular list');
          // 请求下一页数据
          List<YBDRoomInfo?>? popularList = await requestPopularList(_modifiedStartIndex());

          // 加载完成
          refreshController.loadComplete();

          if (null != popularList && popularList.isNotEmpty) {
            // 追加房间列表数据
            _roomList.addAll(popularList);

            // 房间去重
            _roomList = YBDHomeUtil.removeDuplicateRoom(_roomList) ?? [];

//            logger.v('yield popular amount : ${_roomList.length}');
            if (popularList.length < _pageSize) {
              logger.v('no more popular data : ${popularList.length} < $_pageSize');
              // 没有更多数据
              refreshController.loadNoData();
              yield YBDPopularBlocState(rooms: _roomList, noMoreData: true);
            } else {
              yield YBDPopularBlocState(rooms: _roomList);
            }
          } else {
            logger.v('request no popular list, update refresher');
            yield YBDPopularBlocState(rooms: _roomList);
          }
          break;
        }
    }
  }
  void mapEventToStatejILf6oyelive(PopularEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求一页数据
  Future<List<YBDRoomInfo?>?> requestPopularList(int page) async {
    logger.v('request popular list page : $page');
    YBDQueryLabelRoomsRespEntity? popularListEntity = await ApiHelper.queryPopularRooms(
      context,
      page,
      _pageSize,
      timeout: 30 * 1000,
    );

    if (null != popularListEntity) {
      if (null != popularListEntity.record && null != popularListEntity.record!.rank) {
        logger.v('request popular list length : ${popularListEntity.record!.rank!.length}');

        if (page == 0) {
          // 刷新时重置索引
          _apiListIndex = 0;
        }

        _apiListIndex += popularListEntity.record!.rank!.length;
        // print('=================================');
        // final idList = popularListEntity.record.rank.map((e) => e.id).toList();
        // ApiTest.printWrappedListInt(idList);
        // print('=================================');
        return popularListEntity.record!.rank;
      } else {
        logger.v('popular list is null');
        return null;
      }
    } else {
      logger.v('popular entity is null');
      return null;
    }
  }

  /// 修正 start 值
  int _modifiedStartIndex() {
    // 第一次查询 start = 0, offset = 20 返回 23 条数据
    // 第二次查询为了避免漏数据 start = 20, offset = 20
    // print('=================index : ${(_apiListIndex ~/ _pageSize) * _pageSize}');
    return (_apiListIndex ~/ _pageSize) * _pageSize;
  }
  void _modifiedStartIndexs4NPAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// popular 房间列表状态
class YBDPopularBlocState {
  /// 初始状态
  bool isInit;

  /// 房间列表数据
  List<YBDRoomInfo?>? rooms;

  /// 数据已经全部加载完
  bool noMoreData;

  YBDPopularBlocState({
    this.isInit: false,
    this.rooms,
    this.noMoreData: false,
  });
}
