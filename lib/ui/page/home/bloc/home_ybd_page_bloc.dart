import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../module/api_ybd_helper.dart';
import '../entity/rank_ybd_info_entity.dart';

/// 事件类型
enum HomePageEvent {
  // 查询 stars 数据
  RequestStars,
  // 请求到 stars 数据后刷新页面
  RefreshStars,
}

/// 加载 Popular 房间列表事件和状态管理器
class YBDHomePageBloc extends Bloc<HomePageEvent, YBDHomePageBlocState> {
  BuildContext context;
  YBDHomePageBloc(this.context) : super(YBDHomePageBlocState());

  /// super stars 列表
  List<int?> _starList = [];

  @override
  Stream<YBDHomePageBlocState> mapEventToState(HomePageEvent event) async* {
    switch (event) {
      case HomePageEvent.RequestStars:
        _querySuperStars();
        break;
      case HomePageEvent.RefreshStars:
        yield YBDHomePageBlocState(starList: _starList);
        break;
    }
  }
  void mapEventToStater8mbdoyelive(HomePageEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询礼物排行榜
  _querySuperStars() async {
    YBDRankInfoEntity? rankInfoEntity = await ApiHelper.giftersList(context, 0, 9, 3);
    List<int?> starArr = [];

    if (rankInfoEntity?.record?.rank != null) {
      rankInfoEntity!.record!.rank!.forEach((element) {
        starArr.add(element!.id);
      });
    }

    _starList.clear();
    _starList.addAll(starArr);
    add(HomePageEvent.RefreshStars);
  }
}

/// 首页状态
class YBDHomePageBlocState {
  /// super stars 列表
  List<int?>? starList;

  YBDHomePageBlocState({
    this.starList,
  });
}
