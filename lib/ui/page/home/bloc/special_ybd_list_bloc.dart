import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../entity/rank_ybd_info_entity.dart';

/// 事件类型
enum SpecialListEvent {
  Refresh,
  NextPage,
  // 点关注按钮关注成功的事件
  FollowedSuccess,
}

/// special 列表状态管理器
class YBDSpecialListBloc extends Bloc<SpecialListEvent, YBDSpecialListBlocState> {
  BuildContext context;

  /// 一页返回 10 条数据
  final int _pageSize = 10;

  /// 排行榜数据
  List<YBDRoomInfo?> _rankList = [];

  /// 已关注用户数据
  List<int?> _followedIds = [];

  YBDSpecialListBloc(this.context) : super(YBDSpecialListBlocState(isInit: true));

  @override
  Stream<YBDSpecialListBlocState> mapEventToState(SpecialListEvent event) async* {
    logger.v('map special bloc event : $event');

    switch (event) {
      case SpecialListEvent.Refresh:
        {
          logger.v('event : $event, refresh special list');
          // 重新加载第一页数据
          List<YBDRoomInfo?>? starList = await requestStarRankList(0);

          if (null != starList) {
            // 刷新已有数据
            _rankList.clear();
            _rankList.addAll(starList);
            logger.v('all star amount : ${_rankList.length}');

            List<YBDRoomInfo?> specialList = _rankList.where((YBDRoomInfo? info) {
              return !_followedIds.contains(info!.id);
            }).toList();
            logger.v('yield special list amount : ${specialList.length}');
            yield YBDSpecialListBlocState(speicalList: specialList);
          } else {
            logger.v('popular list is null show old data}');
            // 可能是网络加载错误显示已有数据
            // 如果是初始状态取消非初始状态
            List<YBDRoomInfo?> specialList = _rankList.where((YBDRoomInfo? info) {
              return !_followedIds.contains(info!.id);
            }).toList();
            logger.v('yield special list amount : ${specialList.length}');
            yield YBDSpecialListBlocState(speicalList: specialList);
          }
          break;
        }
      case SpecialListEvent.NextPage:
        {
          logger.v('event : $event, next page special list');
          // 请求下一页数据
          List<YBDRoomInfo?>? popularList = await requestStarRankList(_rankList.length);

          if (null != popularList && popularList.isNotEmpty) {
            // 追加房间列表数据
            _rankList.addAll(popularList);
            logger.v('all star list amount : ${_rankList.length}');
            List<YBDRoomInfo?> specialList = _rankList.where((YBDRoomInfo? info) {
              return !_followedIds.contains(info!.id);
            }).toList();
            logger.v('yield special list amount : ${specialList.length}');
            yield YBDSpecialListBlocState(speicalList: specialList);
          } else {
            logger.v('request no star list, show old special list');
            // 可能是网络加载错误显示已有数据
            // 不用更新界面
          }
          break;
        }
      case SpecialListEvent.FollowedSuccess:
        {
          logger.v('event : $event, refresh special list');
          List<YBDRoomInfo?> specialList = _rankList.where((YBDRoomInfo? info) {
            return !_followedIds.contains(info!.id);
          }).toList();
          logger.v('yield special list amount : ${specialList.length}');
          yield YBDSpecialListBlocState(speicalList: specialList);
          break;
        }
    }
  void mapEventToStateywYWyoyelive(SpecialListEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
  }

  /// 请求金币排行榜用户列表
  Future<List<YBDRoomInfo?>?> requestStarRankList(int start) async {
    logger.v('request star list start : $start');

    // 3: 月榜
    YBDRankInfoEntity? rankInfoEntity = await ApiHelper.receiversList(context, start, _pageSize, 3);

    if (null != rankInfoEntity && null != rankInfoEntity.record && null != rankInfoEntity.record!.rank) {
      logger.v('star list amount : ${rankInfoEntity.record!.rank!.length}');
      return rankInfoEntity.record!.rank;
    } else {
      logger.v('star list is null');
      return null;
    }
  }

  /// 关注用户
  followUser(int? userId) {
    logger.v('request follow user');
    ApiHelper.followUser(context, '$userId').then((result) {
      logger.v('follow user result : $result');
      if (result) {
        _followedIds.add(userId);
        add(SpecialListEvent.FollowedSuccess);
      }
    });
  }
}

/// special 列表数据状态
class YBDSpecialListBlocState {
  /// 初始状态
  bool isInit;

  /// speical 数据
  List<YBDRoomInfo?>? speicalList;

  YBDSpecialListBlocState({this.isInit: false, this.speicalList});
}
