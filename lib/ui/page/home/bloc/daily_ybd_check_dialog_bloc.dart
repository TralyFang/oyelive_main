import 'dart:async';


import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/service/task_service/task_ybd_service.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../generated/json/base/json_convert_content.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../room/service/live_ybd_service.dart';
import '../entity/car_ybd_list_entity.dart';
import '../entity/daily_ybd_check_entity.dart';
import '../entity/gift_ybd_list_entity.dart';

/// 签到状态
enum DailyCheckDialogEvent {
  // 查询当天是否已经签到
  QueryCheckInStatus,
  // 已查询到数据
  DoneCheckInData,
  // 签到中
  CheckIn,
  // 签到成功
  Success,
  // 签到报错
  Error,
  // 关闭弹框
  Close,
}

/// 签到弹框 bloc
class YBDDailyCheckDialogBloc extends Bloc<DailyCheckDialogEvent, YBDDailyCheckDialogBlocState> {
  BuildContext context;

  /// 7 个签到任务
  List<YBDDailyCheckRecordExtendRewardListDailyReward?>? _dailyCheckTasks;

  /// 礼物列表
  List<YBDGiftListRecordGift?>? _giftList;

  /// 汽车列表
  List<YBDCarListRecord?>? _carList;

  /// 保存签到任务，在调完成每日签到任务的接口时要用到
  YBDDailyCheckRecord? _dailyCheckRecord;

  /// 是否为新用户
  bool _isNewUser = false;

  /// 签到成功获得奖励类型 关联[YBDDailyCheckRecordExtendRewardListDailyReward.item]
  String? _checkType;

  YBDDailyCheckDialogBloc(this.context) : super(YBDDailyCheckDialogBlocState());

  @override
  Stream<YBDDailyCheckDialogBlocState> mapEventToState(DailyCheckDialogEvent event) async* {
    switch (event) {
      case DailyCheckDialogEvent.QueryCheckInStatus:
        // 从 sp 中查询当天是否已签到
        bool isCheckedIn = await _querySpCheckInStatus();
        // bool isCheckedIn = TestTaskUtil.querySpCheckInStatus();
        logger.v("query checked in status $isCheckedIn");
        if (!isCheckedIn) {
          // 没有签到要弹签到框
          // 是否当日新注册用户
          String? spNewUsers = await YBDSPUtil.get(Const.SP_NEW_USER);
          String? userId = await YBDSPUtil.getUserId();
          Map<String, dynamic>? map = jsonDecode(spNewUsers ?? '{}');
          _isNewUser = userId != null && DateFormat('yyyy-MM-dd').format(DateTime.now()) == map![userId];
          // _isNewUser = TestTaskUtil.isNewUser();

          // 查询每日签到数据后弹签到框
          _queryDailyCheckInData();
        } else {
          // 已完成签到任务
          YBDTaskService.instance!.end(TASK_DAILY_CHECK_IN);
        }
        break;
      case DailyCheckDialogEvent.DoneCheckInData:
        // 记录当天已经弹了签到框
        YBDSPUtil.saveCheckInDate();
        logger.v('DoneCheckInData _isNewUser: $_isNewUser');

        if (_isNewUser) {
          // 新用户自动签到
          final result = await _requestCheckIn();
          // final result = TestTaskUtil.requestCheckIn();
          logger.v('_requestCheckIn result: $result');

          if (result) {
            // 新用户签到成功显示奖励弹框
            yield YBDDailyCheckDialogBlocState(
              dailyCheckTasks: _dailyCheckTasks,
              giftList: _giftList,
              carList: _carList,
              showDialog: true,
              isNewUser: true,
              success: true,
              checkType: _checkType,
            );
          }
        } else {
          // 显示签到弹框
          yield YBDDailyCheckDialogBlocState(
            dailyCheckTasks: _dailyCheckTasks,
            giftList: _giftList,
            carList: _carList,
            showDialog: true,
          );
        }
        break;
      case DailyCheckDialogEvent.CheckIn:
        // 签到
        _completeDailyTask();

        // 签到中
        yield YBDDailyCheckDialogBlocState(
          dailyCheckTasks: _dailyCheckTasks,
          giftList: _giftList,
          carList: _carList,
          isChecking: true,
          showDialog: true,
        );
        break;
      case DailyCheckDialogEvent.Success:
        // 签到完成显示签到奖励
        yield YBDDailyCheckDialogBlocState(
          dailyCheckTasks: _dailyCheckTasks,
          giftList: _giftList,
          carList: _carList,
          success: true,
          showDialog: true,
          checkType: _checkType,
        );
        break;
      case DailyCheckDialogEvent.Error:
      case DailyCheckDialogEvent.Close:
        // 已完成签到任务
        YBDTaskService.instance!.end(TASK_DAILY_CHECK_IN);

        // 关闭弹框
        yield YBDDailyCheckDialogBlocState(showDialog: false);
        break;
    }
  }
  void mapEventToStateLVqzfoyelive(DailyCheckDialogEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 从 sp 中查询是否做完了每日任务
  Future<bool> _querySpCheckInStatus() async {
    int? checkedInDate = await YBDSPUtil.getCheckInDate();

    if (null == checkedInDate) {
      logger.v('sp checked-in data is null');
      // null 值说明取数据时有错误
      return false;
    }

    bool isToday = YBDDateUtil.sameDateFromMilliseconds(checkedInDate);

    if (isToday) {
      logger.v('today complete daily task');
      return true;
    } else {
      logger.v('today not complete daily task');
      return false;
    }
  }
  void _querySpCheckInStatusfDgoroyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取每日签到的数据
  Future<void> _queryDailyCheckInData() async {
    // 7 个签到任务
    List<YBDDailyCheckRecordExtendRewardList?>? rewardList = await _requestDailyTaskRewardList();
    logger.v('YBDDailyCheckRecordExtendRewardList: ${rewardList?.length}');

    if (null != rewardList && rewardList.isNotEmpty) {
      // 去掉多余的数据 7 个签到任务
      _dailyCheckTasks = rewardList.map((e) {
        return e!.dailyReward![0];
      }).toList();

      _giftList = await requestGiftList();
      _carList = await _requestCarList();
      add(DailyCheckDialogEvent.DoneCheckInData);
    } else {
      add(DailyCheckDialogEvent.Error);
      logger.v('daily tasks is empty');
    }
  }
  void _queryDailyCheckInDatavOeswoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 7 个任务
  Future<List<YBDDailyCheckRecordExtendRewardList?>?> _requestDailyTaskRewardList() async {
    YBDDailyTaskEntity? dailyTaskEntity = await ApiHelper.queryTask(context);
    // YBDDailyTaskEntity dailyTaskEntity = TestTaskUtil.queryTask();
    logger.v('dailyTaskEntity.record: ${dailyTaskEntity?.record?.length}');

    if (null != dailyTaskEntity && null != dailyTaskEntity.record) {
      // 每日签到任务
      YBDDailyCheckRecord? dailyCheckRecord = dailyTaskEntity.record!.firstWhereOrNull((record) {
        logger.v('record name : ${record!.name}');
        if (record.name == Const.DAILY_CHECKED_IN) {
          return true;
        } else {
          return false;
        }
      }, /*orElse: () => null*/);

      // 没有签到任务
      if (null == dailyCheckRecord) {
        logger.v('daily check record is null');
        return null;
      }

      // 签到任务 [receive] 为 null 时过滤掉
      if (null == dailyCheckRecord.receive) {
        logger.v('daily check record receive is null');
        return null;
      }

      // 签到任务已完成不弹签到框，返回 null
      if (dailyCheckRecord.receive!) {
        logger.v('daily check record receive is true');
        return null;
      }

      // 保存签到任务，在调完成签到任务接口时要用到
      _dailyCheckRecord = dailyCheckRecord;

      // 返回每日签到任务奖励列表
      YBDDailyCheckRecordExtend extendObj =
          await Future.value(JsonConvert.fromJsonAsT<YBDDailyCheckRecordExtend>(json.decode(dailyCheckRecord.extend!)));
      logger.v('daily task amount : ${extendObj.rewardList!.length}');
      return extendObj.rewardList;
    } else {
      logger.v('daily task rewardList is null');
      return null;
    }
  }

  /// 请求礼物列表数据
  Future<List<YBDGiftListRecordGift?>?> requestGiftList() async {
    if ((YBDLiveService.instance.giftList ?? []).isNotEmpty) {
      return YBDLiveService.instance.giftList;
    }

    YBDGiftListEntity? giftListEntity = await ApiHelper.giftList(context);

    if (null != giftListEntity && null != giftListEntity.record && giftListEntity.record!.isNotEmpty) {
      return giftListEntity.record![0]!.gift;
    } else {
      logger.v('gift list is null');
      return null;
    }
  }

  /// 请求座驾列表数据
  Future<List<YBDCarListRecord?>?> _requestCarList() async {
    if ((YBDLiveService.instance.carList ?? []).isNotEmpty) {
      return YBDLiveService.instance.carList;
    }

    YBDCarListEntity? carListEntity = await ApiHelper.carList(context, queryAll: true);

    if (null != carListEntity && null != carListEntity.record) {
      return carListEntity.record;
    } else {
      logger.v('car list is null');
      return null;
    }
  }

  /// 完成每日签到任务
  Future<void> _completeDailyTask() async {
    bool result = await _requestCheckIn();

    if (result) {
      add(DailyCheckDialogEvent.Success);
    } else {
      add(DailyCheckDialogEvent.Error);
    }
  }
  void _completeDailyTask8i0ztoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求签到的接口
  Future<bool> _requestCheckIn() async {
    int whichDay = 1;
    YBDDailyCheckRecordExtend extendObj =
        await Future.value(JsonConvert.fromJsonAsT<YBDDailyCheckRecordExtend>(json.decode(_dailyCheckRecord!.extend!)));
    extendObj.rewardList!.forEach((element) {
      if (element!.dailyReward![0]!.receive ?? false) {
        whichDay++;
      }
    });
    logger.v("request check in extendObj: ${extendObj.toJson()}");
    //记录签到的奖励类型
    if ((extendObj.rewardList?.length ?? 0) > (whichDay - 1) && whichDay > 0) {
      var dailyRewards = extendObj.rewardList![whichDay - 1]!.dailyReward;
      if (dailyRewards?.first?.item != null) {
        _checkType = dailyRewards!.first!.item;
      }
    }
    logger.v("check in type: $_checkType");
    // 数数埋点
    alog.v('22.8.1---day:$whichDay');
    YBDTATrack().trackEvent(YBDTATrackEvent.sign_complete, prop: YBDTAProps(num: whichDay));
    return await ApiHelper.claimReward(context, _dailyCheckRecord?.id, taskName: 'Check-In Day $whichDay');
  }
  void _requestCheckInHaWt5oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

/// 首页状态
class YBDDailyCheckDialogBlocState {
  /// 7 个签到任务
  final List<YBDDailyCheckRecordExtendRewardListDailyReward?>? dailyCheckTasks;

  /// 礼物列表
  final List<YBDGiftListRecordGift?>? giftList;

  /// 汽车列表
  final List<YBDCarListRecord?>? carList;

  /// 是否显示签到框
  final bool showDialog;

  /// 正在请求签到接口
  final bool isChecking;

  /// 签到成功
  final bool success;

  /// 签到成功获得奖励类型 关联[YBDDailyCheckRecordExtendRewardListDailyReward.item]
  final String? checkType;

  /// 是否为新用户
  final bool isNewUser;

  YBDDailyCheckDialogBlocState({
    this.dailyCheckTasks,
    this.giftList,
    this.carList,
    this.showDialog = false,
    this.isChecking = false,
    this.success = false,
    this.checkType = 'coin',
    this.isNewUser = false,
  });
}
