import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/util/dialog_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../module/api_ybd_helper.dart';
import 'entity/cp_ybd_board_entity.dart';
import 'widget/cp/cp_ybd_content.dart';
import '../../widget/my_ybd_app_bar.dart';
import '../../widget/scaffold/nav_ybd_back_button.dart';

/// cp 列表页面
class YBDCpListPage extends StatefulWidget {
  @override
  _YBDCpListPageState createState() => _YBDCpListPageState();
}

class _YBDCpListPageState extends BaseState<YBDCpListPage> with SingleTickerProviderStateMixin {
  TabController? _tabController;

  /// cp 榜数据
  YBDCpBoardEntity? _cpRankEntity;

  /// 是否为初始状态
  /// 初始状态没有数据时显示加载框，非初始状态没数据显示缺省图
  var _isInitState = true;

  @override
  void initState() {
    super.initState();

    // 查询 cp 榜
    _queryCpBoard();
    _tabController = TabController(length: 4, vsync: this);
    _tabController!.addListener(() {
      logger.v('cp tab index : ${_tabController!.index}');
    });
  }
  void initState87Sdmoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    _tabController!.dispose();
  }
  void disposelRNIBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      appBar: YBDMyAppBar(
        title: Text(
          getSettingsTitle() ?? translate('cp'),
          style: TextStyle(
            color: Colors.white,
            fontSize: YBDTPStyle.spNav,
          ),
        ),
        backgroundColor: YBDTPStyle.heliotrope,
        leading: YBDNavBackButton(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Image.asset(
              'assets/images/question_mark.png',
              fit: BoxFit.cover,
              width: ScreenUtil().setWidth(48),
            ),
            onPressed: () {
              logger.v('clicked question nav action');
              YBDDialogUtil.showCpDescriptionDialog(context);
            },
          ),
        ],
        elevation: 0.0,
      ),
      body: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: <Widget>[
            _itemsTabBar(),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  YBDCpContent(_cpRankEntity?.record?.days, isInitState: _isInitState),
                  YBDCpContent(_cpRankEntity?.record?.weeks, isInitState: _isInitState),
                  YBDCpContent(_cpRankEntity?.record?.months, isInitState: _isInitState),
                  YBDCpContent(_cpRankEntity?.record?.threeYears, isInitState: _isInitState),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  void myBuildOzFIRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Today, Weekly, Monthly 选项卡
  Widget _itemsTabBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(80)),
      child: TabBar(
        indicator: UnderlineTabIndicator(
          borderSide: BorderSide(
            width: ScreenUtil().setWidth(4),
            color: Color(0xff47CCCB),
          ),
          insets: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(50),
            vertical: ScreenUtil().setWidth(16),
          ),
        ),
        labelPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(0)),
        labelColor: Colors.white,
        unselectedLabelColor: Colors.white.withOpacity(0.49),
        controller: _tabController,
        labelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(32),
        ),
        unselectedLabelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(28),
        ),
        tabs: [
          Container(
            padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
            height: ScreenUtil().setWidth(80),
            child: Tab(text: '${translate('today')}'),
          ),
          Container(
            padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
            height: ScreenUtil().setWidth(80),
            child: Tab(text: '${translate('weekly')}'),
          ),
          Container(
            padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
            height: ScreenUtil().setWidth(80),
            child: Tab(text: translate('monthly')),
          ),
          Container(
            padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
            height: ScreenUtil().setWidth(80),
            child: Tab(text: translate('total')),
          ),
        ],
      ),
    );
  }
  void _itemsTabBarc1MYXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询 cp 榜
  Future<void> _queryCpBoard() async {
    final data = await ApiHelper.cpRank(context);

    // 请求结束后标记为非初始状态
    _isInitState = false;

    if (data == null) {
      logger.v('cp board entity is null');
    } else {
      _cpRankEntity = data;
    }

    if (mounted) {
      setState(() {});
    }
  }
}
