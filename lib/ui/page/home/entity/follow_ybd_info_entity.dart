import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDFollowInfoEntity with JsonConvert<YBDFollowInfoEntity> {
  String? returnCode;
  String? returnMsg;
  YBDFollowInfoRecord? record;
  dynamic recordSum;
}

class YBDFollowInfoRecord with JsonConvert<YBDFollowInfoRecord> {
  dynamic users;
  List<int>? followed;
  dynamic fromid;
  dynamic toid;
}
