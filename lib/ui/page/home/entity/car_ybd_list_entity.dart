import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/home/entity/car_ybd_list_record_extend.dart';

class YBDCarListEntity with JsonConvert<YBDCarListEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDCarListRecord?>? record;
  String? recordSum;
}

class YBDCarListRecord with JsonConvert<YBDCarListRecord> {
  int? id;
  String? name;
  dynamic createtime;
  int? status;
  int? price;
  String? img;
  String? animation;
  int? display;
  int? animationDisplay;
  String? animationFile;
  YBDCarListRecordAnimationInfo? animationInfo;
  bool? modify;
  int? fromUser;
  YBDCarListRecordExtend? replaceDto;
  String? currency;
  int? conditionType;
  String? conditionExtends;
}

class YBDCarListRecordAnimationInfo with JsonConvert<YBDCarListRecordAnimationInfo> {
  int? animationId;
  int? animationType;
  String? foldername;
  int? framDuration;
  String? name;
  int? version;

  @override
  String toString() {
    return 'YBDCarListRecordAnimationInfo{animationId: $animationId, animationType: $animationType, foldername: $foldername, framDuration: $framDuration, name: $name, version: $version}';
  }
  void toStringKDvJCoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
