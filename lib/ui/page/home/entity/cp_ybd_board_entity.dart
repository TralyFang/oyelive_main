import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDCpBoardEntity with JsonConvert<YBDCpBoardEntity> {
  String? returnCode;
  String? returnMsg;
  YBDCpBoardRecord? record;
}

class YBDCpBoardRecord with JsonConvert<YBDCpBoardRecord> {
  List<YBDCpBoardRecordItem?>? months;
  List<YBDCpBoardRecordItem?>? weeks;
  List<YBDCpBoardRecordItem?>? days;
  List<YBDCpBoardRecordItem?>? threeYears;
}

class YBDCpBoardRecordItem with JsonConvert<YBDCpBoardRecordItem> {
  int? talentId;
  String? talentName;
  String? talentHead;
  int? talentGender;
  int? beans;
  int? userId;
  String? userName;
  String? userHead;
  int? userGender;
}
