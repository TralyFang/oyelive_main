import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

class YBDRankInfoEntity with JsonConvert<YBDRankInfoEntity> {
  String? returnCode;
  String? returnMsg;
  YBDRankInfoRecord? record;
  dynamic recordSum;
}

class YBDRankInfoRecord with JsonConvert<YBDRankInfoRecord> {
  String? name;
  int? dateType;
  dynamic labelId;
  List<YBDRoomInfo?>? rank;
  List<double>? scores;
}
