import 'dart:async';



import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDDailyTaskEntity with JsonConvert<YBDDailyTaskEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDDailyCheckRecord?>? record;
  String? recordSum;
}

class YBDDailyCheckRecord with JsonConvert<YBDDailyCheckRecord> {
  int? id;
  String? name;
  String? reward;
  int? status;
  bool? receive;
  String? extend;
  String? route;
  String? currencyImage;
  YBDDailyCheckRecordExtend? extendObj;
}

/// 每日任务中一周的任务
class YBDDailyCheckRecordExtend with JsonConvert<YBDDailyCheckRecordExtend> {
  int? version;
  List<YBDDailyCheckRecordExtendRewardList?>? rewardList;
}

/// 每日任务中一天的任务
class YBDDailyCheckRecordExtendRewardList with JsonConvert<YBDDailyCheckRecordExtendRewardList> {
  List<YBDDailyCheckRecordExtendRewardListDailyReward?>? dailyReward;
}

// 一个任务
class YBDDailyCheckRecordExtendRewardListDailyReward with JsonConvert<YBDDailyCheckRecordExtendRewardListDailyReward> {
  String? name;
  String? item;
  int? itemId;
  int? days;
  int? number;
  String? icon;
  dynamic receive;
  dynamic claimTime;
}
