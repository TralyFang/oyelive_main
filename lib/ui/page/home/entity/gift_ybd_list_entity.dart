import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDGiftListEntity with JsonConvert<YBDGiftListEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDGiftListRecord?>? record;
  String? recordSum;
}

class YBDGiftListRecord with JsonConvert<YBDGiftListRecord> {
  List<YBDGiftListRecordGift?>? gift;
  int? id;
  int? index;
  String? name;
}

class YBDGiftListRecordGift with JsonConvert<YBDGiftListRecordGift> {
  int? id;
  String? name;
  int? status;
  int? label;
  int? index;
  int? price;
  int? earn;
  int? agencyearn;
  String? img;
  dynamic animation;
  int? display;
  String? attribute;
  String? channel;
  int? animationDisplay;
  String? animationFile;
  YBDGiftListRecordGiftAnimationInfo? animationInfo;
  int? conditionType;
  String? conditionExtends;
}

class YBDGiftListRecordGiftAnimationInfo with JsonConvert<YBDGiftListRecordGiftAnimationInfo> {
  dynamic animationId;
  dynamic animationType;
  dynamic foldername;
  dynamic framDuration;
  dynamic name;
  int? version;
}
