import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDKingKongEntity with JsonConvert<YBDKingKongEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDKingKongRecord?>? record;
  dynamic recordSum;
}

class YBDKingKongRecord with JsonConvert<YBDKingKongRecord> {
  int? id;
  String? route;
  String? name;
  String? img;
  int? rank;
}
