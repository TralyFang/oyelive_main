import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/home/entity/daily_ybd_check_entity.dart';

class YBDMyDailyTaskEntity with JsonConvert<YBDMyDailyTaskEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDDailyCheckRecord?>? record;
  String? recordSum;
}
