import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDBannerAdvertiseEntity with JsonConvert<YBDBannerAdvertiseEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDBannerAdvertiseRecord?>? record;
  String? recordSum;
}

class YBDBannerAdvertiseRecord with JsonConvert<YBDBannerAdvertiseRecord> {
  int? id;
  String? adname;
  String? title;
  String? detail;
  String? adurl;
  String? img;
  int? adtype;
  int? index;
  String? channel;

  YBDBannerAdvertiseRecord({
    this.id,
    this.adname,
    this.adurl,
    this.img,
    this.detail,
  });
}
