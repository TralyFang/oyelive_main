import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDNewUserStatusEntity with JsonConvert<YBDNewUserStatusEntity> {
  String? returnCode;
  String? returnMsg;
  YBDNewUserStatusRecord? record;
}
class YBDNewUserStatusRecord with JsonConvert<YBDNewUserStatusRecord> {
  bool? enabled; // 是否激活
  String? route; // 路由
  String? resourceUrl; // 图片资源地址
  int? remainingTime; // 剩余时间ms
}