import 'dart:async';


import '../../../../module/entity/room_ybd_info_entity.dart';

class YBDPopularRoomsEvent {
  List<YBDRoomInfo?> rooms;

  YBDPopularRoomsEvent(this.rooms);
}
