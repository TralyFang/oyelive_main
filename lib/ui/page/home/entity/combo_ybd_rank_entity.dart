import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

class YBDComboRankEntity with JsonConvert<YBDComboRankEntity> {
  String? returnCode;
  String? returnMsg;
  YBDComboRankRecord? record;
  dynamic recordSum;
}

class YBDComboRankRecord with JsonConvert<YBDComboRankRecord> {
  String? name;
  int? dateType;
  dynamic labelId;
  List<YBDComboRankRecordRank?>? rank;
  dynamic scores;
}

class YBDComboRankRecordRank with JsonConvert<YBDComboRankRecordRank> {
  int? giftId;
  String? giftName;
  String? giftImg;
  int? combo;
  int? sendTime;
  YBDRoomInfo? sender;
  YBDRoomInfo? receiver;
}
