import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDTpActivityEntity with JsonConvert<YBDTpActivityEntity> {
  /// 活动地址
  String? url;

  /// 活动皮肤 zip 文件下载地址
  String? zip;

  /// 是否显示活动皮肤
  bool? showSkin;

  /// 活动名称
  String? name;

  /// 活动图片
  String? img;

  /// 主题数据
  /// TODO: 测试代码 v1.12.0 配置主题色
  String? primaryColor;
  String? dividerColor;
  String? backgroundColor;
  String? lightPrimaryColor;
  String? lightSecondaryColor;
  String? bodyText2Color;
}
