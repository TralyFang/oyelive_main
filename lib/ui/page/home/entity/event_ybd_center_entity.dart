import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/ui/page/home/controller/event_ybd_dot_controller.dart' as eventCtr;

class YBDEventCenterEntity with JsonConvert<YBDEventCenterEntity> {
  String? code;
  YBDEventCenterData? data;
  bool? success;
}

class YBDEventCenterData with JsonConvert<YBDEventCenterData> {
  List<YBDEventCenterDataEventInfos?>? eventInfos;
  int? recordSum;
}

class YBDEventCenterDataEventInfos with JsonConvert<YBDEventCenterDataEventInfos> {
  int? id;
  String? eventKey;
  String? eventName;
  String? eventDetail;
  String? eventUrl;
  String? eventImg;
  String? eventType;
  int? order;
  String? beginTime;
  String? endTime;
  String? endDetail;
  String? country;
  dynamic tenantId;
  dynamic projectId;
  String? eventStatus;

  /// 用eventKey做唯一标记
  String get idStr => '$eventKey';

  /// 已读活动
  Future<bool> isRead() async {
    return await eventCtr.isReadId(idStr);
  }
  void isReadS7JzGoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
