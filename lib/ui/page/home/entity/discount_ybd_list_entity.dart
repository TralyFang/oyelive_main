import 'dart:async';


import '../../../../generated/json/base/json_convert_content.dart';

class YBDDiscountListEntity with JsonConvert<YBDDiscountListEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDDiscountListRecord?>? record;
  String? recordSum;
}

class YBDDiscountListRecord with JsonConvert<YBDDiscountListRecord> {
  int? id;
  int? itemId;
  int? itemType;
  int? sold;
  int? stock;
  int? frozen;
  int? originalPrice;
  int? discountPrice;
  dynamic startTime;
  dynamic endTime;
  int? startTimestamp;
  int? endTimestamp;
  int? rank;
  dynamic count;
  int? status;
  int? createTime;
  int? deleted;
  int? version;
  dynamic projectCode;
  dynamic tenantCode;
  String? thumbnail;
  dynamic animation;
  dynamic animationDisplay;
  dynamic animationFile;
  dynamic animationInfo;
  dynamic name;
}
