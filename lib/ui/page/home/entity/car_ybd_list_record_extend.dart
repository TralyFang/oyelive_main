import 'dart:async';


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDCarListRecordExtend with JsonConvert<YBDCarListRecordExtend> {
  String? replacedSvgaImageKey;
  String? replacedSvgaImage;
  String? replacedStaticPictureKey;
  String? replacedStaticPicture;
}
