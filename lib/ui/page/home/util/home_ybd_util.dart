import 'dart:async';

import 'dart:convert';
import 'dart:math';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/ui/page/game_room/enum/game_ybd_enum.dart';
import 'package:oyelive_main/ui/page/game_room/util/game_ybd_socket_api_util.dart';

import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/config_ybd_util.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../module/entity/query_ybd_configs_resp_entity.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/entity/unique_ybd_id_entity.dart';
import '../../../../module/status/entity/random_ybd_lyric_entity.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../../redux/configs_ybd_redux.dart';
import '../../room/room_ybd_helper.dart';

/// 首页 ui 工具类
class YBDHomeUtil {
  /// 解析 js 跳原生页面的字符串，参数为 json 格式
  /// _parseNativePagePath("native://live_now?{\"roomId\":\"11111\",\"test\":\"aaaaa\"}");
  static Map<String, dynamic>? parseNativePathWithJsonParams(String jsMsg) {
    Map<String, dynamic> result = {};
    List<String> components = jsMsg.split('?');

    if (null == components || components.isEmpty) {
      logger.v('error js msg : $jsMsg');
      return null;
    }

    result['path'] = components[0];

    if (components.length > 1) {
      try {
        // 解析 json 参数
        Map<String, dynamic>? params = json.decode(components[1]);
        result['params'] = params;
      } catch (e) {
        logger.v('decode json error : $e');
      }
    } else {
      logger.v('no native params');
    }

    return result;
  }

  /// 解析 js 跳原生页面的字符串，参数为 url 格式
  /// _parseNativePagePath("native://live_now?roomId=11111&test=aaaaa}");
  static Map<String, dynamic>? parseNativePathWithUrlParams(String jsMsg) {
    Map<String, dynamic> result = {};
    List<String> components = jsMsg.split('?');

    if (null == components || components.isEmpty) {
      logger.v('error js msg : $jsMsg');
      return null;
    }

    result['path'] = components[0];

    if (components.length > 1) {
      Map<String, dynamic> params = {};

      // 解析 url 格式参数
      try {
        if (components[1].contains('&')) {
          List paramsList = components[1].split('&');
          for (int i = 0; i < paramsList.length; i++) {
            List keyValue = paramsList[i].split('=');
            params[keyValue[0]] = keyValue[1];
          }
        } else {
          List keyValue = components[1].split('=');
          params[keyValue[0]] = keyValue[1];
        }

        result['params'] = params;
      } catch (e) {
        logger.v('parse url params error : $e');
      }
    } else {
      logger.v('no native params');
    }

    return result;
  }

  /// 获取周榜房间
  static List<String> weeklyStarRooms(BuildContext context) {
    List<String> superRooms = [];
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    YBDConfigInfo config = store.state.configs!;
    String? appTopCoinRanking = config.appTopcoinRanking;

    if (null != appTopCoinRanking && appTopCoinRanking.isNotEmpty) {
      if (appTopCoinRanking.contains(',')) {
        superRooms = appTopCoinRanking.split(',');
      } else {
        superRooms.add(appTopCoinRanking);
      }
    } else {
      logger.v('no weekly star rooms');
    }

    return superRooms;
  }

  /// 移除重复房间
  static List<YBDRoomInfo?>? removeDuplicateRoom(List<YBDRoomInfo?> rooms) {
    if (null == rooms) {
      logger.v('rooms is null');
      return null;
    } else {
      List<YBDRoomInfo?> result = [];
      final ids = rooms.map((e) => e!.id).toSet();
      rooms.retainWhere((element) => ids.remove(element!.id));
      result.addAll(rooms);
      return result;
    }
  }

  /// 移除重复歌词
  static List<YBDRandomLyricDataLyric>? removeDuplicateLyric(List<YBDRandomLyricDataLyric> lyrics) {
    if (null == lyrics) {
      logger.v('lyrics is null');
      return null;
    } else {
      List<YBDRandomLyricDataLyric> result = [];
      final ids = lyrics.map((e) => e.id).toSet();
      lyrics.retainWhere((element) => ids.remove(element.id));
      result.addAll(lyrics);
      return result;
    }
  }

  /// 跳转到开播页面
  static Future<void> goLive(BuildContext? context, {String? location}) async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
    YBDRoomHelper.enterRoom(context, userInfo?.id, location: location);
  }

  /// 显示对话框
  /// type: 1, 跳转到编辑个人信息页面；0，不跳转
  static showAlertDialog(BuildContext context, String message,
      {bool goSettings = false, bool showLater = false, VoidCallback? onPressedLater}) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text(message),
          actions: [
            if (showLater) ...[
              TextButton(
                child: Text(
                  translate('later'),
                  style: TextStyle(color: Color(0xff5E94E7)),
                ),
                onPressed: () {
                  Navigator.pop(context);
                  // 跳过设置海报，开始开播
                  onPressedLater?.call();
                },
              ),
            ],
            TextButton(
              child: Text(
                translate('ok'),
                style: TextStyle(color: Color(0xff5E94E7)),
              ),
              onPressed: () {
                Navigator.pop(context);
                if (goSettings) {
                  //跳转设置页面
                  YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.edit_profile);
                }
              },
            ),
          ],
        );
      },
    );
  }

  /// 当天是否已经弹出活动弹框
  static Future<bool> _showedActivityDialog() async {
// 上次显示活动弹框的日期
    int? showedDate = await YBDSPUtil.activityDialogDate();

    if (null == showedDate) {
      logger.v('sp show activity dialog date is null');
      return false;
    }

    bool isToday = YBDDateUtil.sameDateFromMilliseconds(showedDate);

    if (isToday) {
      logger.v('today showed activity dialog');
      return true;
    } else {
      logger.v('today not showed activity dialog');
      return false;
    }
  }

  /// 从 firebase 获取靓号数据
  static configUniqueIDsWithStore(BuildContext context, Store<YBDAppState> store) async {
    logger.v("Start getting Unique ID list from firebase database");
    /*
    DataSnapshot snapshot = await FirebaseDatabase.instance.reference().child("luckyIdList").once();

     */
    DataSnapshot snapshot = (await FirebaseDatabase.instance.ref().child('luckyIdList').once()).snapshot;
    logger.v("Unique ID list from firebase database: ${snapshot.value}");

    List<YBDUniqueIDEntity> uniqueIDs = [];
/*
    if (snapshot?.value != null && snapshot.value.isNotEmpty) {
      snapshot.value.forEach((k, v) {
        try {
          YBDUniqueIDEntity uniqueIDItem = YBDUniqueIDEntity();
          uniqueIDItem.uniqueId = k is int ? k : int.parse(k);
          uniqueIDItem.userId = v["userId"] is int ? v["userId"] : int.parse(v["userId"]);
          uniqueIDItem.validity = v["validity"] ?? v["validity "];
          uniqueIDItem.validity = YBDCommonUtil.parseValidity(uniqueIDItem); // 格式化Firebase数据库保存的validity字段
          uniqueIDs.add(uniqueIDItem);
        } catch (e) {
          logger.v("parse unique id from firebase error: $e, k: $k, v: $v");
        }
      });
    }

 */
    if (snapshot.value != null && snapshot.value is Map) {
      (snapshot.value as Map).forEach((k, v) {
        try {
          YBDUniqueIDEntity uniqueIDItem = YBDUniqueIDEntity();
          uniqueIDItem.uniqueId = k is int ? k : int.parse(k);
          uniqueIDItem.userId = v["userId"] is int ? v["userId"] : int.parse(v["userId"]);
          uniqueIDItem.validity = v["validity"] ?? v["validity "];
          uniqueIDItem.validity = YBDCommonUtil.parseValidity(uniqueIDItem); // 格式化Firebase数据库保存的validity字段
          uniqueIDs.add(uniqueIDItem);
        } catch (e) {
          logger.v("parse unique id from firebase error: $e, k: $k, v: $v");
        }
      });
    }

    // 保存到 redux
    try {
      YBDConfigInfo _configInfo = await ConfigUtil.loadConfigInfo(context);
      _configInfo.uniqueIds = uniqueIDs;
      store.dispatch(YBDUpdateConfigsAction(_configInfo));
    } catch (e) {
      logger.v("store dispatch unique ids error: $e");
    }
  }

  /// 从房间列表里获取一个没加锁的随机房间
  static YBDRoomInfo? randomPublicRoom(List<YBDRoomInfo?>? roomList, int range) {
    YBDRoomInfo? roomInfo;

    if (null != roomList && roomList.isNotEmpty) {
      final publicRooms = roomList.where((element) => element!.protectMode != 1).toList();

      if (null != publicRooms && publicRooms.isNotEmpty) {
        roomInfo = publicRooms[Random().nextInt(publicRooms.length < range ? publicRooms.length : range)];
      }
    }

    return roomInfo;
  }
}
