import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import '../../../base/base_ybd_state.dart';
import '../../../common/util/dialog_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import 'widget/combo_ybd_content.dart';
import '../../widget/my_ybd_app_bar.dart';
import '../../widget/scaffold/nav_ybd_back_button.dart';

/// combo 列表页面
class YBDComboListPage extends StatefulWidget {
  @override
  _YBDComboListPageState createState() => _YBDComboListPageState();
}

class _YBDComboListPageState extends BaseState<YBDComboListPage> with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabController!.addListener(() {
      logger.v('combo tab index : ${_tabController!.index}');
    });
  }
  void initStatezw3laoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    _tabController!.dispose();
  }
  void disposeGDRQHoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      appBar: YBDMyAppBar(
        title: Text(
          getSettingsTitle() ?? translate('combo'),
          style: TextStyle(
            color: Colors.white,
            fontSize: YBDTPStyle.spNav,
          ),
        ),
        backgroundColor: YBDTPStyle.heliotrope,
        leading: YBDNavBackButton(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Image.asset(
              'assets/images/question_mark.png',
              fit: BoxFit.cover,
              width: ScreenUtil().setWidth(48),
            ),
            onPressed: () {
              YBDDialogUtil.showComboDescriptionDialog(context);
              logger.v('clicked question nav action');
            },
          ),
        ],
        elevation: 0.0,
      ),
      body: Container(
        decoration: YBDTPStyle.gradientDecoration,
        child: Column(
          children: <Widget>[
            _itemsTabBar(),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  YBDComboContent(ComboRankType.Weekly),
                  YBDComboContent(ComboRankType.Monthly),
                  YBDComboContent(ComboRankType.Total),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  void myBuildlZknxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 7 day, 30 day, Total 选项卡
  _itemsTabBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(80)),
      child: TabBar(
        indicator: UnderlineTabIndicator(
          borderSide: BorderSide(
            width: ScreenUtil().setWidth(4),
            color: Color(0xff47CCCB),
          ),
          insets: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(70),
            vertical: ScreenUtil().setWidth(16),
          ),
        ),
        labelPadding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(0)),
        labelColor: Colors.white,
        unselectedLabelColor: Colors.white.withOpacity(0.49),
        controller: _tabController,
        labelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(32),
        ),
        unselectedLabelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(28),
        ),
        tabs: [
          Container(
            padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
            height: ScreenUtil().setWidth(80),
            child: Tab(text: '7 ${translate('days')}'),
          ),
          Container(
            padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
            height: ScreenUtil().setWidth(80),
            child: Tab(text: '30 ${translate('days')}'),
          ),
          Container(
            padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
            height: ScreenUtil().setWidth(80),
            child: Tab(text: translate('total')),
          ),
        ],
      ),
    );
  }
}
