import 'dart:async';


import 'package:flutter/material.dart';
import 'package:spritewidget/spritewidget.dart';
import 'coin_ybd_rain_animation.dart';

enum RainAnimationType { bean, gold }

/// bean雨动画
class YBDCoinRain extends NodeWithSize {
  late YBDCoinRainAnimation _rain;
  late YBDCoinRainAnimation _star;

  RainAnimationType type;

  YBDCoinRain({this.type = RainAnimationType.bean}) : super(const Size(2048.0, 2048.0)) {
    if (type == RainAnimationType.bean) {
      _rain = YBDCoinRainAnimation('assets/images/topup/y_top_up_beans@2x.webp');
      addChild(_rain);
    } else if (type == RainAnimationType.gold) {
      _rain = YBDCoinRainAnimation('assets/images/icon_gold.webp');
      addChild(_rain);
    }
    _star = YBDCoinRainAnimation('assets/images/star_one.png');
    addChild(_star);
  }

  stopAnimation(bool active) {
    _rain.active = active;
    _star.active = active;
  }
}
