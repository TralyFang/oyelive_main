import 'dart:async';


import 'dart:ui' as ui show Image;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:spritewidget/spritewidget.dart';

/// 雨点动画
class YBDCoinRainAnimation extends Node {
  /// 雨点图片
  final String rainImg;

  List<ParticleSystem> _particles = <ParticleSystem>[];

  YBDCoinRainAnimation(this.rainImg) {
    _addParticles(1.0);
    _addParticles(1.5);
    _addParticles(2.0);
  }

  void _addParticles(double distance) async {
    ImageMap images = ImageMap(bundle: rootBundle);
    ui.Image image = await images.loadImage(rainImg);
    SpriteTexture texture1 = SpriteTexture(image);

    ParticleSystem particles = ParticleSystem(
      texture: texture1,
      blendMode: BlendMode.srcATop,
      posVar: Offset(1300.0, 0.0),
      direction: 90.0,
      directionVar: 0.0,
      speed: 1000.0 / distance,
      speedVar: 100.0 / distance,
      startSize: 1.2 / distance,
      startSizeVar: 0.2 / distance,
      endSize: 1.2 / distance,
      endSizeVar: 0.2 / distance,
      life: 1.5 * distance,
      lifeVar: 1.0 * distance,
    );
    particles.position = Offset(1024.0, -200.0);
    particles.rotation = 10.0;
    particles.opacity = 1.0;

    _particles.add(particles);
    addChild(particles);
  }
  void _addParticlesZPC8Foyelive(double distance)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  set active(bool active) {
    motions.stopAll();
    for (ParticleSystem particleElement in _particles) {
      if (active) {
        motions.run(MotionTween<double>(setter: (a) => particleElement.opacity = a, start: particleElement.opacity, end: 1.0, duration: 2.0));
      } else {
        motions.run(MotionTween<double>(setter: (a) => particleElement.opacity = a, start: particleElement.opacity, end: 0.0, duration: 0.5));
      }
    }
  }
}
