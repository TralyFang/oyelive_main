import 'dart:async';


import 'dart:convert';

import 'package:oyelive_main/common/util/log_ybd_util.dart';

const String JS_SHARE_NAME = 'name';
const String JS_SHARE_URL = 'url';
const String JS_SHARE_IMG = 'img';
const String JS_SHARE_DESC = 'desc';

/// {"name":"Magical globe","url":"http://121.37.214.86/event/magicalGlobe/index.html","img":"http://s3-us-west-2.amazonaws.com/thankyotest/image/1/1658915936278_800.jpg","desc":"Magical globe, The event will start soon, so stay tuned!"}
Map<String, dynamic>? parseEventCenterShareMsg(String jsMsg) {
  if (null != jsMsg && jsMsg.isNotEmpty) {
    try {
      var mapData = json.decode(jsMsg);
      return mapData;
    } catch (e) {
      logger.e('parseEventCenterShareMsg error: $e ');
    }
  }

  return {};
}
