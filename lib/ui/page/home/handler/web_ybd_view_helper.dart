import 'dart:async';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/widget/sharing_ybd_widget.dart';

import 'js_ybd_msg_parser.dart';

void shareEventCenter(Map<String, dynamic> shareMap) {
  showModalBottomSheet(
      context: Get.context!,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(16.px as double)),
      ),
      builder: (BuildContext context) {
        return YBDSharingWidget(
          key: Key('event_center_${shareMap[JS_SHARE_NAME]}'),
          adName: shareMap[JS_SHARE_NAME],
          adUrl: shareMap[JS_SHARE_URL],
          adImg: shareMap[JS_SHARE_IMG],
        );
      });
}
