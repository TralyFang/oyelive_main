import 'dart:async';


import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:package_info/package_info.dart';
import 'package:redux/redux.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:oyelive_main/common/analytics/crashlytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/room_socket/room_ybd_socket_api.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/common/util/dialog_ybd_util.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/version_update/check_ybd_update_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/query_ybd_configs_resp_entity.dart';
import 'package:oyelive_main/module/room/entity/balance_ybd_record_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/redux/followed_ybd_ids_redux.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gifting_notify_entity.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/home/handler/js_ybd_msg_parser.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/widget/dialog_ybd_wsa_support.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/bus_event/event_ybd_fn.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';
import 'package:oyelive_main/ui/page/splash/splash_ybd_util.dart';
import 'package:oyelive_main/ui/widget/sharing_ybd_widget.dart';

import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/room_socket/room_ybd_socket_api.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/user/entity/user_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';

class YBDWebUIModel {
  bool resizeToAvoidBottomInset;
  bool bottom;
  YBDWebUIModel(this.resizeToAvoidBottomInset, this.bottom);
  static YBDWebUIModel initModel(Map m) {
    bool resizeToAvoidBottomInset = (m["resizeToAvoidBottomInset"] ?? '0').toString().toBool();
    bool bottom = (m["bottom"] ?? '1').toString().toBool();
    return YBDWebUIModel(resizeToAvoidBottomInset, bottom);
  }
}

/// js 设置网页标题时的回调
/// [title] js 设置的网页标题
typedef void JsSetTitleCallback(String title);

/// 开始加载游戏的回调
typedef void JsStartGameLoadCallback();

/// 游戏加载结束的回调
typedef void JsLoadedGameCallback();

/// 把用户信息传给 h5 时的回调
/// [jsCode] 要执行的 js 代码
typedef void JsUserInfoCallback(String jsCode);

///键盘事件回调
typedef void JsUICallback(YBDWebUIModel model);

///web 查询余额
typedef void JsLoadBeansCallback(String jsCode);

///web 获取游戏房battleId
typedef void JsGameRoomBattleIdCallback(String jsCode);

///web 是否关闭加载弹窗
typedef void JsIsCloseLoadingCallback(bool close);

///web view will appear
typedef void JsWebViewWillAppearCallback();

/// 活动中心分享
typedef void JsWebViewEventCenterShareCallback(Map<String, dynamic>? shareMap);

///checkUpdate
typedef void JsCheckUpdateCallback();

/// 添加 web view js 通道方法的类
class YBDJsChannelHandler {
  BuildContext? _context;

  /// 游戏加载结束的回调
  JsLoadedGameCallback? _loadedGameCallback;

  /// 设置标题的回调
  JsSetTitleCallback? _jsTitleCallback;

  /// 把用户信息传给 h5 时的回调
  JsUserInfoCallback? _userInfoCallback;

  /// 开始加载游戏的回调
  JsStartGameLoadCallback? _startGameLoadCallback;

  /// UI、键盘事件回调
  JsUICallback? _uiCallback;

  /// web 查询余额
  JsLoadBeansCallback? _jsLoadBeansCallback;

  ///web 获取游戏房battleId
  JsGameRoomBattleIdCallback? _jsGameRoomBattleIdCallback;

  /// web 关闭loading
  JsIsCloseLoadingCallback? _jsCloseLoadingCallback;

  /// web view will appear
  late JsWebViewWillAppearCallback _jsWebViewWillAppearCallback;

  /// 活动中心分享
  JsWebViewEventCenterShareCallback? _jsWebViewEventCenterShareCallback;

  /// check
  JsCheckUpdateCallback? _jsCheckUpdate;

  YBDJsChannelHandler({
    required context,
    required jsTitleCallback,
    required startGameLoadCallback,
    required loadedGameCallback,
    required userInfoCallback,
    required uiCallback,
    required jsLoadBeansCallback,
    required jsGameRoomBattleIdCallback,
    required jsCloseLoadingCallback,
    required jsWebViewWillAppearCallback,
    required eventCenterShareCallback,
    required jsCheckUpdate,
  }) {
    _context = context;
    _jsTitleCallback = jsTitleCallback;
    _startGameLoadCallback = startGameLoadCallback;
    _loadedGameCallback = loadedGameCallback;
    _startGameLoadCallback = startGameLoadCallback;
    _userInfoCallback = userInfoCallback;
    _uiCallback = uiCallback;
    _jsLoadBeansCallback = jsLoadBeansCallback;
    _jsGameRoomBattleIdCallback = jsGameRoomBattleIdCallback;
    _jsCloseLoadingCallback = jsCloseLoadingCallback;
    _jsWebViewWillAppearCallback = jsWebViewWillAppearCallback;
    _jsWebViewEventCenterShareCallback = eventCenterShareCallback;
    _jsCheckUpdate = jsCheckUpdate;
  }

  /// js 通道
  Set<JavascriptChannel> jsChannels() {
    return [
      // 退出网页容器
      _popWebViewJavascriptChannel(_context),
      // 游戏加载前埋点
      _startGameWebViewJavascriptChannel(_context),
      // 游戏加载成功的埋点
      _loadedGameWebViewJavascriptChannel(_context),
      // 打印日志
      _logWebViewJavascriptChannel(_context),
      // toast 提示语
      _toasterJavascriptChannel(_context),
      // 设置导航栏标题
      _webTitleJavascriptChannel(_context),
      // 跳转到 flutter 页面
      _jumpToFlutterPageJavascriptChannel(_context),
      // 跳转到 native 页面
      _jumpToNativePageJavascriptChannel(_context),
      // 跳转自己的 profile 或其他人的 profile 页面
      _profileJavascriptChannel(_context),
      // 获取用户信息
      _userInfoJavascriptChannel(_context),

      ///send gift
      _sendGiftJavascriptChannel(_context),
      // TODO: 测试代码 ios
      // // 获取用户信息或 app 信息
      // _getInfoJavascriptChannel(context)
      _onTokenInvalidJavascriptChannel(_context),

      //头部信息
      _headerInfo(_context),

      //活动相关信息
      _activeInfo(_context),

      _uiCallbackChannel(_context),

      // web 跨域问题 走客户端获取余额
      _reloadBeans(_context),
      _openWsaDialog(_context),

      // 打开游戏房的miniprofile弹窗
      _openGameRoomMiniprofile(_context),
      // 返回游戏battleId
      _gameRoomLudoBattleId(_context),
      // 游戏玩家坐标
      _gameRoomPlayerPosition(_context),
      // 关闭加载弹窗
      _closeLoading(_context),

      // 网页分享
      _eventCenterShare(_context),
      //check
      _jsCheckUpdateChannel(_context)
    ].toSet();
  }

  /// js 通道：提示语
  JavascriptChannel _toasterJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'Toaster',
      onMessageReceived: (JavascriptMessage message) {
        logger.v('js channel toast msg : ${message.message}');
        YBDToastUtil.toast(message.message);
      },
    );
  }

  /// js 通道：提示语
  JavascriptChannel _eventCenterShare(BuildContext? context) {
    return JavascriptChannel(
      name: 'shareWeb',
      onMessageReceived: (JavascriptMessage message) {
        logger.v('js channel shareWeb msg : ${message.message}');
        var shareMap = parseEventCenterShareMsg(message.message);
        _jsWebViewEventCenterShareCallback?.call(shareMap);
      },
    );
  }

  /// 查询最新配置
  _refreshConfig(BuildContext? context) async {
    YBDDialogUtil.showLoading(context);
    YBDQueryConfigsRespEntity? _resp = await ApiHelper.queryConfigs(context);
    YBDDialogUtil.hideLoading(context);

    if (_resp?.returnCode == Const.HTTP_SUCCESS && _resp?.record != null) {
      // 刷新 store
      final store = YBDCommonUtil.storeFromContext(context: context);

      // 从 response 获取配置项
      YBDConfigInfo? configInfo = YBDSplashUtil.addUniqueIdsFromStore(_resp!, store);

      // 刷新 store 里的配置信息
      YBDSplashUtil.refreshStoreWithConfig(store!, configInfo!);

      // 把配置信息保存到本地 json 文件
      ConfigUtil.writeConfig(_resp);
    }
  }

  /// 检查更新
  _checkUpdate(BuildContext? context) async {
    await _refreshConfig(context);
    YBDCheckUpdateUtil.checkUpdate(context!, showLatestVersionMessage: true);
  }

  /// js 通道：检查更新
  JavascriptChannel _jsCheckUpdateChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'checkUpdate',
      onMessageReceived: (JavascriptMessage message) {
        logger.v('js channel checkUpdate msg : ${message.message}');

        _checkUpdate(context);

        _jsCheckUpdate?.call();
      },
    );
  }

  /// js 通道: 退出网页容器
  JavascriptChannel _popWebViewJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'Pop',
      onMessageReceived: (JavascriptMessage message) {
        logger.v('js channel pop web view : ${message.message}');
        if (message.message.contains('close?')) {
          logger.v('js channel open func with tag');
          Uri u = Uri.parse(message.message);
          int? tag = u.queryParameters.keyToInt('tag');
          YBDTeenInfo.getInstance().enterType = TPEnterType.TPETActive;
          eventBus.fire(YBDOpenTp()..tag = tag);
        }
        YBDNavigatorHelper.popPage(context!);
      },
    );
  }

  /// js 通道: 游戏加载前埋点
  JavascriptChannel _startGameWebViewJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'StartGame',
      onMessageReceived: (JavascriptMessage message) {
        logger.v('js channel start game : ${message.message}');

        if (null != _startGameLoadCallback) {
          _startGameLoadCallback!();
        }
      },
    );
  }

  /// js 通道: 游戏加载成功的埋点
  JavascriptChannel _loadedGameWebViewJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'LoadedGame',
      onMessageReceived: (JavascriptMessage message) {
        logger.v('js channel loaded game : ${message.message}');

        if (null != _loadedGameCallback) {
          _loadedGameCallback!();
        }
      },
    );
  }

  /// js 通道: 打印日志
  JavascriptChannel _logWebViewJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'WebviewLog',
      onMessageReceived: (JavascriptMessage message) {
        logger.v('js channel log : ${message.message}');
      },
    );
  }

  /// js 通道：设置导航栏标题
  JavascriptChannel _webTitleJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'Title',
      onMessageReceived: (JavascriptMessage message) {
        logger.v('js channel set title : ${message.message}');

        if (null != _jsTitleCallback) {
          _jsTitleCallback!(message.message);
        }
      },
    );
  }

  /// js 通道：获取用户信息通道
  JavascriptChannel _userInfoJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'YBDUserInfo',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel get userInfo : ${message.message}');

        // 用户信息
        YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
        // 修改成全路径的头像地址
        final image = YBDImageUtil.avatar(context, userInfo?.headimg, userInfo?.id, scene: 'B');
        userInfo?.headimg = image;

        // 添加 app 内选择的语言，没有选择语言时取默认语言
        userInfo?.spLocal = (await YBDSPUtil.get(Const.SP_LOCALE)) ?? Const.DEFAULT_LANG;

        String jsessionId = (await YBDSPUtil.get(Const.SP_JSESSION_ID)) ?? '';
        String cookieApp = (await YBDSPUtil.get(Const.SP_COOKIE_APP)) ?? '';
        PackageInfo packageInfo = await PackageInfo.fromPlatform();
        String a = json.encode({"roomId": (YBDRoomSocketApi.roomId ?? '')});

        if (null != _userInfoCallback) {
          _userInfoCallback!(
              "${message.message}('${jsonEncode(userInfo)}', '$jsessionId', '$cookieApp', '${int.parse(packageInfo.buildNumber)}','$a')");
        }
        logger.v(
            "===js call ${message.message}('${jsonEncode(userInfo)}', '$jsessionId', '$cookieApp', '${int.parse(packageInfo.buildNumber)}', '$a')");
      },
    );
  }

  /// js 通道：获取 getInfo
  // TODO: 测试代码 暂时没用到
  JavascriptChannel _getInfoJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
      name: 'AppInfo',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel getInfo : ${message.message}');
        var dataMap = <String, dynamic>{
          "dataType": message.message,
        };
        switch (message.message) {
          case "YBDUserInfo":
            dataMap.addAll({
              "userInfo": jsonEncode(await YBDSPUtil.getUserInfo()),
              "jSessionId": await YBDSPUtil.get(Const.SP_JSESSION_ID) ?? '',
              "cookieApp": await YBDSPUtil.get(Const.SP_COOKIE_APP) ?? ''
            });
            break;
          case "AppInfo":
            PackageInfo packageInfo = await PackageInfo.fromPlatform();
            dataMap.addAll({"appVersion": int.parse(packageInfo.buildNumber)});
            break;
        }
        // _webViewController.evaluateJavascript("onAppInfo('${jsonEncode(dataMap)}')");
      },
    );
  }

  /// 跳转到 profile 页面
  Future<void> jumpToProfile(String userId) async {
    final mUserId = await YBDUserUtil.userId();

    if (mUserId == userId) {
      YBDNavigatorHelper.navigateTo(_context, YBDNavigatorHelper.myprofile);
    } else {
      YBDNavigatorHelper.navigateTo(_context, YBDNavigatorHelper.user_profile + "/$userId");
    }
  }
  void jumpToProfileGGBfioyelive(String userId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// js 通道：跳转自己的 profile 或其他人的 profile 页面
  JavascriptChannel _profileJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'Profile',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel go to profile : ${message.message}');
        jumpToProfile(message.message);
      },
    );
  }

  /*
   * js 通道：跳转到 flutter 页面   格式老版本已写死，只能是flutter://room?tag=1&roomId=xx 新增参数只能?和roomid之间,单纯的跳转房间tag参数可以不传
   * tag = 1 跳转房间打开teenpatti
   * tag = 2 跳转房间打开水果游戏
   * tag = 3 跳转房间打开送礼框
   * tag = 4 跳转房间上麦
   **/
  JavascriptChannel _jumpToFlutterPageJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'FlutterPage',
      onMessageReceived: (JavascriptMessage message) {
        try {
          logger.v('js channel jump flutter route path : ${message.message}');
          String url = message.message;

          if (url.contains('flutter://room')) {
            // var urlSub = url.split('roomId=');
            late Uri u;
            try {
              u = Uri.parse(url);
            } catch (e) {
              logger.e('url trans fail : $e');
            }

            int roomId = u.queryParameters.keyToInt('roomId') ?? -1;
            int? tag = u.queryParameters.keyToInt('tag');

            logger.v('===h5 roomId $roomId, tag = $tag');
            YBDTeenInfo.getInstance().enterType = TPEnterType.TPETActive;
            YBDModuleCenter.instance().enterRoom(type: YBDModuleCenter.enterRoomType(tag), roomId: roomId);
          } else {
            logger.v('===h5 not room');

            if (url == "flutter://top_up" || url == "flutter://top_up_type/easypaisa") {
              if (Platform.isIOS) {
                url = "flutter://purchase_detail";
              }
            }
            YBDNavigatorHelper.navigateTo(context, url).then((value) => _jsWebViewWillAppearCallback());
          }
        } catch (e) {
          logger.v('js channel jump to flutter page error : $e');
        }
      },
    );
  }

  /// js 通道: sendGift
  JavascriptChannel _sendGiftJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'SendGift',
      onMessageReceived: (JavascriptMessage message) {
        logger.v('js channel SendGift : ${message.message}');
        // YBDNavigatorHelper.popPage(context);
        Map m = jsonDecode(message.message.toString());
        // roomId toUserId  num  personalId  giftId
        // 查询背包信息 如果有这个id 就直接发 没有 就更新背包
        YBDRoomSocketApi.getInstance().sendGift(
            roomId: m["roomId"] ?? YBDRoomSocketApi.roomId,
            to: m["toUserId"] ?? YBDRoomSocketApi.roomId,
            code: m["giftId"],
            num: m["num"] ?? 1,
            combo: 1,
            personalId: m["personalId"],
            onSuccess: (data) async {});
      },
    );
  }

  /// js 通道：跳转到 native 页面
  // TODO: 测试代码 纯 flutter 版本应该用不到这个方法了
  JavascriptChannel _jumpToNativePageJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'NativePage',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel jump native path : ${message.message}');
        YBDNavigatorHelper.openNativePage(context, message.message, location: 'webview_page');
      },
    );
  }

  /// token过期重新登录
  JavascriptChannel _onTokenInvalidJavascriptChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'OnTokenInvalid',
      onMessageReceived: (JavascriptMessage message) async {
        try {
          // 清空数据
          await YBDSPUtil.remove(Const.SP_USER_INFO);
          await YBDSPUtil.remove(Const.SP_COOKIE_APP);
          await YBDSPUtil.remove(Const.SP_JSESSION_ID);
          Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
          store.dispatch(YBDUpdateUserAction(YBDUserInfo()));
          store.dispatch(YBDUpdateFollowedIdsAction(Set()));
        } catch (e) {
          logger.e('remove store error : $e');
        }

        try {
          if (context != null) YBDDialogUtil.showReloginDialog(context);
        } catch (e) {
          logger.e('show relogin dialog error : $e');
          YBDCrashlyticsUtil.instance!.report(
            e,
            extra: 'webView error: $e',
            module: YBDCrashlyticsModule.WEB_VIEW,
          );
        }
      },
    );
  }

  ///头部信息
  JavascriptChannel _headerInfo(BuildContext? context) {
    return JavascriptChannel(
      name: 'HeaderInfo',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel get headerInofo : ${message.message}');

        PackageInfo packageInfo = await PackageInfo.fromPlatform();
        String locale = (await YBDSPUtil.get(Const.SP_LOCALE)) ?? Const.DEFAULT_LANG;
        String? ip = await YBDSPUtil.get(Const.SP_IP_COUNTRY_CODE);
        Map<String, dynamic> m = {
          'ot_vc': packageInfo.buildNumber,
          'ot_ipl': ip,
          'ot_v': packageInfo.version,
          'ot_p': Platform.operatingSystem,
          'ot_lang': locale,
        };
        // var headrString =
        //     'document.ot-vc = ${packageInfo.buildNumber};document.ot-ipl = ${await YBDSPUtil.get(Const.SP_IP_COUNTRY_CODE)};ot-v = ${packageInfo.version};ot-p = ${Platform.operatingSystem};ot-lang = $locale;ot-c = Google;ot-an = OT;';

        if (null != _userInfoCallback) {
          _userInfoCallback!("${message.message}('${jsonEncode(m)}', '')");
        }
      },
    );
  }

  ///活动相关信息
  JavascriptChannel _activeInfo(BuildContext? context) {
    return JavascriptChannel(
      name: 'activeInfo',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel get activeInfo : ${message.message}');
        Map<String, dynamic> m = {
          'entrance': (_context!.widget as YBDTPWebView).entranceType.index.toString(),
        };
        if (null != _userInfoCallback) {
          _userInfoCallback!("${message.message}('${jsonEncode(m)}', '')");
        }
      },
    );
  }

  /*
   * keyboard 是否滚动视图回调
   **/
  JavascriptChannel _uiCallbackChannel(BuildContext? context) {
    return JavascriptChannel(
      name: 'uievent',
      onMessageReceived: (JavascriptMessage message) {
        try {
          Map? m = jsonDecode(message.message.toString());
          logger.v('js channel key board  : ${message.message}  $m');
          if (_uiCallback != null) {
            _uiCallback!(YBDWebUIModel.initModel(m!));
          }
        } catch (e) {
          logger.v('js channel key board : $e');
        }
      },
    );
  }

  /*
   *  web 跨域问题 走客户端获取余额
   **/
  JavascriptChannel _reloadBeans(BuildContext? context) {
    return JavascriptChannel(
      name: 'getBalance',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel getBalance');
        try {
          YBDBalanceRecordEntity? entity = await ApiHelper.getBalanceApi(_context);
          logger.v('js channel getBalance  : ${message.message}  ${jsonEncode(entity?.record)}');
          if (_jsLoadBeansCallback != null) {
            _jsLoadBeansCallback!(jsonEncode(entity?.record));
          }
        } catch (e) {
          logger.v('js channel balance : $e');
        }
      },
    );
  }

  /// js 通道：打开whatsapp联系方式弹框
  JavascriptChannel _openWsaDialog(BuildContext? context) {
    return JavascriptChannel(
      name: 'wsaDialog',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel go to openWsaDialog : ${message.message}');
        YBDCommonUtil.showMyDialog(YBDWSASupportDialog());
      },
    );
  }

  /// js 通道：打开ludo房间的YBDGiftDialog弹窗
  JavascriptChannel _openGameRoomMiniprofile(BuildContext? context) {
    return JavascriptChannel(
      name: 'gameRoomGiftDialog',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel gameRoomGiftDialog : ${message.message}');
        /*
         {
         "userId":12
         "roomId":12
         }
         */
        /// 不处理事件了，由客户端处理了
        // if (message.message == null) return;
        // var jsonString = message.message;
        // var msgMap = jsonDecode(jsonString) as Map;
        // if (msgMap["userId"] != null) {
        //   var userId = int.tryParse(msgMap["userId"].toString()) ?? 0;
        //   eventBus.fire(YBDGameRoomJSChannelEvent()
        //     ..userId = userId
        //     ..eventName = YBDGameRoomJSChannelEvent.openGiftDialog,
        //   );
        // }
      },
    );
  }

  /// js 通道：由H5触发 getGameRoomBattleId, app回调 ludo游戏的 battleId 给H5
  JavascriptChannel _gameRoomLudoBattleId(BuildContext? context) {
    return JavascriptChannel(
      name: 'getGameRoomBattleId',
      onMessageReceived: (JavascriptMessage message) async {
        try {
          var gameConfig = Get.find<YBDGameRoomGetLogic>().state!.gameJSConfig ?? "";
          logger.v('js channel getGameRoomBattleId : $gameConfig');
          if (gameConfig.isNotEmpty) {
            _jsGameRoomBattleIdCallback?.call("$gameConfig");
          }
        } catch (e) {
          logger.v('js channel getGameRoomBattleId fail : $e');
        }
      },
    );
  }

  /// js 通道：由H5触发 返回四位玩家的位置
  JavascriptChannel _gameRoomPlayerPosition(BuildContext? context) {
    return JavascriptChannel(
      name: 'gameRoomPlayerPosition',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel gameRoomPlayerPosition : ${message.message}');
        /* 数组必须有4个
        [{
        "centerX":12,
        "centerY":10,
        "userId":123,
        },{},{},{}]
         */
        try {
          Get.find<YBDGameRoomGetLogic>().state!.playerPositionChange.value = message.message != null;
        } catch (e) {
          logger.v('js channel gameRoomPlayerPosition fail : $e');
        }
        if (message.message == null) return;
        var jsonString = message.message;
        var msgList = jsonDecode(jsonString) as List;
        var positionList = msgList.map((v) => YBDGamePlayerPosition().fromJson(v)).toList();
        try {
          Get.find<YBDGameRoomGetLogic>().state!.playerPositions = positionList;
          Get.find<YBDGameRoomGetLogic>().state!.playerPositionChangeStr.value = message.message;
        } catch (e) {
          logger.v('js channel gameRoomPlayerPosition fail : $e');
        }
      },
    );
  }

  /// js 通道：由H5触发 控制加载弹窗 参数："0": 显示，"1"：隐藏，默认隐藏
  JavascriptChannel _closeLoading(BuildContext? context) {
    return JavascriptChannel(
      name: 'isCloseLoading',
      onMessageReceived: (JavascriptMessage message) async {
        logger.v('js channel closeLoading : ${message.message}');
        bool close = message.message != "0";
        _jsCloseLoadingCallback?.call(close);
      },
    );
  }
}
