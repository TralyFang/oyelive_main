import 'dart:async';


import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../generated/json/base/json_convert_content.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../entity/car_ybd_list_entity.dart';
import '../entity/daily_ybd_check_entity.dart';
import '../entity/gift_ybd_list_entity.dart';
import '../../room/service/live_ybd_service.dart';

typedef ShowDailyTask = Function(
  List<YBDDailyCheckRecordExtendRewardListDailyReward?> dailyTasks,
  List<YBDGiftListRecordGift?>? giftList,
  List<YBDCarListRecord?>? carList,
);

/// 处理首页的每日任务的业务逻辑
class YBDHomePageHandler {
  BuildContext context;

  /// 保存每日任务的数据，在调完成每日任务的接口时要用到
  YBDDailyCheckRecord? _dailyCheckRecord;

  /// 检查完后的回调显示对话框
  ShowDailyTask? _showDialogCallback;

  YBDHomePageHandler(this.context, showDialogCallback) {
    this._showDialogCallback = showDialogCallback;
  }

  /// 检查每日任务
  checkDailyTask() {
    _checkLocalDailyTask();
  }

  /// 本地检查每日任务
  _checkLocalDailyTask() {
    _doneDailyTask().then((doneTask) {
      if (!doneTask) {
        logger.v('not done daily task, request daily task');
        _checkNetworkDailyTask();
      } else {
        logger.v('already done daily task');
      }
    });
  }

  /// 网络接口检查每日任务
  _checkNetworkDailyTask() async {
    List<YBDDailyCheckRecordExtendRewardList?>? rewardList = await _requestDailyTaskRewardList();

    if (null != rewardList && rewardList.isNotEmpty) {
      List<YBDDailyCheckRecordExtendRewardListDailyReward?> dailyTasks = rewardList.map((e) {
        return e!.dailyReward![0];
      }).toList();

      List<YBDGiftListRecordGift?>? giftList = await requestGiftList();
      List<YBDCarListRecord?>? carList = await _requestCarList();
      logger.v('daily tasks amount : ${dailyTasks?.length}');
      logger.v('gift list amount : ${giftList?.length}');
      logger.v('car list amount ${carList?.length}');

      if (null != YBDUserUtil.userInfo()) {
        if (null != _showDialogCallback) {
          _showDialogCallback!(dailyTasks, giftList, carList);
        } else {
          logger.v('show dialog callback is null');
        }
      } else {
        logger.v('user info is null no daily dialog');
      }
    } else {
      logger.v('daily tasks is empty');
    }
  }

  /// 从 sp 中查询是否做完了每日任务
  Future<bool> _doneDailyTask() async {
    int? checkedInDate = await YBDSPUtil.getCheckInDate();

    if (null == checkedInDate) {
      logger.v('sp checked-in data is null');
      return false;
    }

    bool isToday = YBDDateUtil.sameDateFromMilliseconds(checkedInDate);

    if (isToday) {
      logger.v('today complete daily task');
      return true;
    } else {
      logger.v('today not complete daily task');
      return false;
    }
  }
  void _doneDailyTaskuEIwFoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取每日任务奖励列表
  Future<List<YBDDailyCheckRecordExtendRewardList?>?> _requestDailyTaskRewardList() async {
    YBDDailyTaskEntity? dailyTaskEntity = await ApiHelper.queryTask(context);

    if (null != dailyTaskEntity && null != dailyTaskEntity.record) {
      // 找出每日签到任务数据
      YBDDailyCheckRecord? dailyCheckRecord = dailyTaskEntity.record!.firstWhereOrNull((record) {
        logger.v('record name : ${record!.name}');
        if (record.name == Const.DAILY_CHECKED_IN) {
          return true;
        } else {
          return false;
        }
      });

      if (null == dailyCheckRecord) {
        logger.v('daily check record is null');
        return null;
      }

      // 非奖励列表数据
      if (null == dailyCheckRecord.receive) {
        logger.v('daily check record receive is null');
        return null;
      }

      // 已签到
      if (dailyCheckRecord.receive!) {
        logger.v('daily check record receive is true');
        return null;
      }

      // 保存每日签到任务数据
      _dailyCheckRecord = dailyCheckRecord;

      // 返回每日签到任务奖励列表
      YBDDailyCheckRecordExtend extendObj =
          await Future.value(JsonConvert.fromJsonAsT<YBDDailyCheckRecordExtend>(json.decode(dailyCheckRecord.extend!)));
      logger.v('daily task amount : ${extendObj.rewardList!.length}');
      return extendObj.rewardList;
    } else {
      logger.v('daily task rewardList is null');
      return null;
    }
  }

  /// 请求礼物列表数据
  Future<List<YBDGiftListRecordGift?>?> requestGiftList() async {
    if ((YBDLiveService.instance.giftList ?? []).isNotEmpty) {
      return YBDLiveService.instance.giftList;
    }

    YBDGiftListEntity? giftListEntity = await ApiHelper.giftList(context);

    if (null != giftListEntity && null != giftListEntity.record && giftListEntity.record!.isNotEmpty) {
      return giftListEntity.record![0]!.gift;
    } else {
      logger.v('gift list is null');
      return null;
    }
  }

  /// 请求座驾列表数据
  Future<List<YBDCarListRecord?>?> _requestCarList() async {
    if ((YBDLiveService.instance.carList ?? []).isNotEmpty) {
      return YBDLiveService.instance.carList;
    }

    YBDCarListEntity? giftListEntity = await ApiHelper.carList(context);

    if (null != giftListEntity && null != giftListEntity.record) {
      return giftListEntity.record;
    } else {
      logger.v('car list is null');
      return null;
    }
  }
}
