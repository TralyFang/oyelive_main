import 'dart:async';


import 'dart:async';

import 'package:disk_space/disk_space.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import 'package:spritewidget/spritewidget.dart';
import 'package:system_info/system_info.dart';
import 'package:oyelive_main/common/constant/short_ybd_route.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/common/util/img_ybd_resource_util.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/game_room/hall_page/game_ybd_hall_page.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_room_dialog_guide.dart';
import 'package:oyelive_main/ui/page/home/bloc/king_ybd_kong_bloc.dart';
import 'package:oyelive_main/ui/page/home/widget/related_ybd_tab.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/go_ybd_tab_event.dart';

import '../../../base/base_ybd_state.dart';
import '../../../common/analytics/analytics_ybd_util.dart';
import '../../../common/constant/const.dart';
import '../../../common/service/task_service/task_ybd_service.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../module/entity/room_ybd_info_entity.dart';
import '../room/service/live_ybd_service.dart';
import 'animation/conin_ybd_rain.dart';
import 'bloc/advertise_ybd_bloc.dart';
import 'bloc/combo_ybd_bloc.dart';
import 'bloc/daily_ybd_check_dialog_bloc.dart';
import 'bloc/explore_ybd_bloc.dart';
import 'bloc/followers_ybd_bloc.dart';
import 'bloc/gifters_ybd_bloc.dart';
import 'bloc/home_ybd_page_bloc.dart';
import 'bloc/popular_ybd_bloc.dart';
import 'bloc/receivers_ybd_bloc.dart';
import 'bloc/special_ybd_list_bloc.dart';
import 'util/home_ybd_util.dart';
import 'widget/daily_ybd_check_dialog.dart';
import 'widget/daily_ybd_check_new_user_dialog.dart';
import 'widget/explore_ybd_tab.dart';
import 'widget/home_ybd_nav_bar.dart';
import 'widget/popular_ybd_tab.dart';

/// app 首页
class YBDHomePage extends StatefulWidget {
  YBDHomePage({Key? key}) : super(key: key);

  @override
  YBDHomePageState createState() => YBDHomePageState();
}

class YBDHomePageState extends BaseState<YBDHomePage> with TickerProviderStateMixin {
  /// popular 标签栏索引
  int _currentTabIndex = Const.POPULAR_FEATURED_INDEX;

  TabController? _tabController;
  ScrollController? _scrollController;

  /// 是否显示金币雨动画
  bool _showCoinRain = false;

  /// 是否检查过签到状态
  bool _queriedCheckInStatus = false;

  /// 金币雨动画实例
  YBDCoinRain _coinRain = YBDCoinRain();

  /// Popular 页面 bloc
  late YBDPopularBloc _popularBloc;

  @override
  void initState() {
    super.initState();
    print('22.7.25-----home page');
    YBDLiveService.instance.mainContext = context;

    // 设置签到任务自动结束
    YBDTaskService.instance!.autoEnd(TASK_DAILY_CHECK_IN, timeOut: TASK_DAILY_CHECK_IN_TIMEOUT);

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      // 从 firebase 获取靓号数据
      YBDHomeUtil.configUniqueIDsWithStore(context, YBDCommonUtil.storeFromContext(context: context)!);
    });

    _scrollController = ScrollController();

    // 提前加载 popular 列表数据
    logger.v('create popular bloc');
    _popularBloc = YBDPopularBloc(context);
    _popularBloc.add(PopularEvent.Refresh);

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      // 提前加载图片解决 Related 标签栏选中背景图偶尔不显示的问题
      precacheImage(AssetImage(YBDImg.explore_tab_bg_img.webp), context);

      // 手机分辨率埋点
      final dpResolution = '${ScreenUtil().screenWidth}x${ScreenUtil().screenHeight}';
      final pixResolution = '${ScreenUtil().screenWidth}x${ScreenUtil().screenHeight}';
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.OPEN_PAGE,
        location: 'ScreenResolution',
        itemName: '${dpResolution}_$pixResolution',
        value: '${DateTime.now().timeZoneName}_${YBDCommonUtil.getIpCountryCode(context)}',
      ));
      logger.v('ScreenResolution: ${dpResolution}_$pixResolution');
    });

    eventBus.on<YBDGoTabEvent>().listen((YBDGoTabEvent event) async {
      logger.v('home page scroll pos: ${event.postion}, index: ${event.subIndex}, length: $_tabControllerLength');
      bool isHome = event.postion != null && event.postion == 'home' && event.index == 0;
      bool subLegal = event.subIndex != null && event.subIndex! >= 0 && event.subIndex! < _tabControllerLength;
      if (isHome && subLegal) {
        Future.delayed(Duration(milliseconds: 100), () {
          _currentTabIndex = event.subIndex ?? 1;
          _tabController!.index = event.subIndex ?? 1;
          setState(() {});
        });
      }
    });
  }
  void initState07ju0oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  int _tabControllerLength = 0;

  /// 根据配置初始化TabController是否带game模块
  _initTabController(int length) {
    if (length == _tabControllerLength) {
      return;
    }
    _tabController?.dispose();
    _tabController = null;
    _tabController = TabController(initialIndex: 1, length: length, vsync: this);
    _tabController!.addListener(() {
      if (_tabController!.indexIsChanging) {
        return; // 还是改变中，就不切换了
      }
      if (_currentTabIndex == _tabController!.index) {
        return; // 重复点击了
      }
      // 埋点
      YBDObsUtil.instance().tabRouteChange(
        routeList: YBDShortRoute.index_tab_list,
        curIndex: _tabController!.index,
        preIndex: _currentTabIndex,
        preRoute: YBDShortRoute.home,
      );
      _currentTabIndex = _tabController!.index;
      setState(() {});

      logger.v('clicked tab index : ${_tabController!.index}');

      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.CLICK_EVENT,
        location: YBDLocationName.MAIN_PAGE,
        itemName: 'TopTab index:${_tabController!.index}',
      ));
    });
    logger.v('_initTabController${_tabController.hashCode}');
    _tabControllerLength = length;
  }

  @override
  void dispose() {
    _tabController!.dispose();
    _scrollController!.dispose();
    super.dispose();
  }
  void disposevsum8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return MultiBlocProvider(
      providers: _pageBlocs(),
      child: Scaffold(
        body: Material(
          child: Stack(
            children: <Widget>[
              // 首页页面内容
              _homePageBody(),
              // 金币雨动画
              _showCoinRain ? IgnorePointer(child: SpriteWidget(_coinRain)) : Container(),
              // 签到弹框监听
              _dailyCheckInBlocListener(),
            ],
          ),
        ),
        // 开播按钮
        floatingActionButton: _startLiveIcon(),
      ),
    );
  }

  /// 开播图标
  _startLiveIcon() {
    return GestureDetector(
      onTap: showAction,
      child: Image.asset(
        'assets/images/live_now.webp',
        width: ScreenUtil().setWidth(104),
      ),
    );
  }

  void showAction() async {
    YBDCommonTrack().commonTrack(YBDTAProps(location: 'room', module: YBDTAModule.plus));
    await YBDHomeUtil.goLive(context, location: YBDTAEventLocation.new_live);
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
        location: YBDLocationName.MAIN_PAGE, itemName: YBDItemName.PLUS_LIVE));
  }

  void myBuildZ0WKcoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 签到弹框监听
  BlocListener<YBDPopularBloc, YBDPopularBlocState> _dailyCheckInBlocListener() {
    return BlocListener<YBDPopularBloc, YBDPopularBlocState>(
      listener: (context, state) async {
        // 查询到 Popular 数据后弹签到框
        // 在测试环境下没有房间也可以弹出签到弹框
        logger.v('show check in start dialog');
        if ((!_queriedCheckInStatus && null != state.rooms && state.rooms!.isNotEmpty) || Const.TEST_ENV) {
          // 只执行一次
          _queriedCheckInStatus = true;

          YBDTaskService.instance!.onEvent.listen((taskId) {
            if (taskId == TASK_DAILY_CHECK_IN) {
              logger.v('show check in dialog');
              // 开始签到弹框任务
              context.read<YBDDailyCheckDialogBloc>().add(DailyCheckDialogEvent.QueryCheckInStatus);
            }
          });
          // 等待弹出签到框
          YBDTaskService.instance!.waiting(TASK_DAILY_CHECK_IN);
        }
      },
      child: BlocListener<YBDDailyCheckDialogBloc, YBDDailyCheckDialogBlocState>(
        listener: (context, state) {
          if (state.success) {
            if (state.checkType?.toLowerCase() == "gold") {
              _coinRain = YBDCoinRain(type: RainAnimationType.gold);
            }
            // 签到成功显示金币雨
            showCoinRain();
          }
        },
        child: BlocBuilder<YBDDailyCheckDialogBloc, YBDDailyCheckDialogBlocState>(
          builder: (builderContext, state) {
            logger.v('state.showDialog: ${state.showDialog}');
            if (state.showDialog) {
              // 签到天数 -> 7天
              if ((state.dailyCheckTasks?.length ?? 0) < 7) {
                // 后台数据异常
                YBDTaskService.instance!.end(TASK_DAILY_CHECK_IN);
                return Container();
              } else {
                // 显示签到弹框
                YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                  YBDEventName.OPEN_PAGE,
                  location: YBDLocationName.HOME_POPULAR_PAGE,
                  itemName: YBDItemName.CHECKIN_POPUP,
                ));
                return _dailyCheckInDialog(state);
              }
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }

  /// 签到弹框
  Widget _dailyCheckInDialog(YBDDailyCheckDialogBlocState state) {
    if (state.isNewUser) {
      // 从前三个公开房间中随机获取一个房间
      YBDRoomInfo? roomInfo = YBDHomeUtil.randomPublicRoom(_popularBloc.state.rooms, 3);
      // 新用户自动签到弹框
      return YBDDailyCheckNewUserDialog(roomInfo: roomInfo);
    } else {
      return YBDDailyCheckDialog();
    }
  }
  void _dailyCheckInDialog4xsJQoyelive(YBDDailyCheckDialogBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 页面内容
  Widget _homePageBody() {
    return FutureBuilder<bool>(
        future: ConfigUtil.shouldShowGame(context: context, allPlatform: true),
        builder: (context, snapshot) {
          List<Widget> controllers = [
            YBDRelatedTab(),
            YBDPopularTab(_scrollController),
            YBDExploreTab(_scrollController),
          ];
          bool openGame = false;

          /// 如果开启了游戏，那就展示
          // if (snapshot.data != null && snapshot.data == true) {
          //   controllers.add(YBDGameHallPage(_scrollController));
          //
          //   /// 开启了游戏，而没有展示那就重新创建
          //   openGame = true;
          // }
          _initTabController(controllers.length);
          return Container(
            decoration: YBDActivitySkinRoot().curAct().activityBgDecoration(),
            child: Stack(
              children: [
                _popularBannerBg(),
                Column(
                  children: <Widget>[
                    YBDHomeNavBar(_tabController, _scrollController, _currentTabIndex, openGame),
                    Expanded(child: BlocBuilder<YBDHomePageBloc, YBDHomePageBlocState>(builder: (context, state) {
                      return TabBarView(
                        controller: _tabController,
                        children: controllers,
                      );
                    })),
                  ],
                ),
              ],
            ),
          );
        });
  }
  void _homePageBody8BiKqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// banner 背景图
  Widget _popularBannerBg() {
    if (_currentTabIndex == Const.POPULAR_FEATURED_INDEX) {
      return YBDActivitySkinRoot().curAct().popularBannerBg();
    }

    return SizedBox();
  }

  /// checked-in 每日任务后显示金币雨
  void showCoinRain() {
    logger.v('show coin rain');
    setState(() {
      _showCoinRain = true;
    });

    // 显示 5 秒金币雨动画
    Future.delayed(Duration(seconds: 5), () {
      _coinRain.stopAnimation(false);

      // 从 widget 树移除金币雨
      Future.delayed(Duration(seconds: 2), () {
        setState(() {
          _showCoinRain = false;
        });
      });
    });
  }
  void showCoinRainKF4LLoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 页面 bloc
  List<BlocProvider> _pageBlocs() {
    return [
      BlocProvider<YBDHomePageBloc>(create: (context) {
        logger.v('create home page bloc');
        YBDHomePageBloc bloc = YBDHomePageBloc(context);
        bloc.add(HomePageEvent.RequestStars);
        return bloc;
      }),
      BlocProvider<YBDPopularBloc>(create: (context) {
        return _popularBloc;
      }),
      BlocProvider<YBDExploreBloc>(create: (context) {
        logger.v('create explore bloc');
        YBDExploreBloc bloc = YBDExploreBloc(context);
        return bloc;
      }),
      BlocProvider<YBDAdvertiseBloc>(create: (context) {
        logger.v('create advertise bloc');
        YBDAdvertiseBloc bloc = YBDAdvertiseBloc(context);
        return bloc;
      }),
      BlocProvider<YBDReceiversBloc>(create: (context) {
        logger.v('create receivers bloc');
        YBDReceiversBloc bloc = YBDReceiversBloc(context);
        return bloc;
      }),
      BlocProvider<YBDGiftersBloc>(create: (context) {
        logger.v('create gifters bloc');
        YBDGiftersBloc bloc = YBDGiftersBloc(context);
        return bloc;
      }),
      BlocProvider<YBDFollowersBloc>(create: (context) {
        logger.v('create followers bloc');
        YBDFollowersBloc bloc = YBDFollowersBloc(context);
        return bloc;
      }),
      BlocProvider<YBDComboBloc>(create: (context) {
        logger.v('create combo bloc');
        YBDComboBloc bloc = YBDComboBloc(context);
        return bloc;
      }),
      BlocProvider<YBDSpecialListBloc>(create: (context) {
        logger.v('create special list bloc');
        YBDSpecialListBloc bloc = YBDSpecialListBloc(context);
        return bloc;
      }),
      BlocProvider<YBDDailyCheckDialogBloc>(create: (context) {
        logger.v('create daily check bloc');
        YBDDailyCheckDialogBloc bloc = YBDDailyCheckDialogBloc(context);
        return bloc;
      }),
      BlocProvider<YBDKingKongBloc>(create: (context) {
        logger.v('create king kong bloc');
        YBDKingKongBloc bloc = YBDKingKongBloc(context);
        return bloc;
      }),
    ];
  }
  void _pageBlocs4iQ9foyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
