import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../../common/util/log_ybd_util.dart';

/// 封设备弹框
class YBDDeviceBlockDialog extends StatelessWidget {
  final String? content;

  YBDDeviceBlockDialog(this.content);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black.withOpacity(0.4),
      child: Container(
        width: ScreenUtil().setWidth(500),
        height: ScreenUtil().setWidth(350),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          _contentView(),
        ]),
      ),
    );
  }
  void buildDnvAEoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 对话框内容
  Widget _contentView() {
    return Container(
      padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
      width: ScreenUtil().setWidth(500),
      height: ScreenUtil().setWidth(350),
      color: Colors.white,
      child: Column(children: <Widget>[
        SizedBox(
          height: ScreenUtil().setWidth(10),
        ),
        Container(
          width: ScreenUtil().setWidth(450),
          height: ScreenUtil().setWidth(240),
          alignment: Alignment.center,
          child: Text(
            '$content',
            style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.black),
            overflow: TextOverflow.ellipsis,
            maxLines: 5,
          ),
        ),
        Expanded(
          child: Container(),
        ),
        GestureDetector(
          onTap: () {
            logger.v("clicked the device Block dialog");
            SystemNavigator.pop();

            ///封设备  退出app
          },
          child: Container(
            width: ScreenUtil().setWidth(130),
            height: ScreenUtil().setWidth(70),
            alignment: Alignment.center,
            padding: EdgeInsets.all(ScreenUtil().setWidth(5)),
            child: Text(
              translate('ok'),
              style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.black),
            ),
          ),
        ),
        SizedBox(
          height: ScreenUtil().setWidth(10),
        ),
      ]),
    );
  }
  void _contentView587GGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
