import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../widget/room_ybd_level_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

/// combo 接收者头像
class YBDComboReceiverAvatar extends StatelessWidget {
  /// 礼物接收者信息
  final YBDRoomInfo? userInfo;

  YBDComboReceiverAvatar(this.userInfo);

  @override
  Widget build(BuildContext context) {
    return Container(
//      color: Colors.black,
      width: ScreenUtil().setWidth(180),
      height: ScreenUtil().setWidth(140),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(154),
            // height: ScreenUtil().setWidth(90),
            // padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(28)),
            child: Image.asset(
              'assets/images/combo/combo_wing.webp',
              fit: BoxFit.cover,
            ),
          ),
          Center(
            child: Container(
              width: ScreenUtil().setWidth(106),
              height: ScreenUtil().setWidth(106),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(60))),
                border: Border.all(color: Color(0xff66FEFC), width: ScreenUtil().setWidth(3)),
              ),
              child: YBDRoundAvatar(
                // 用户头像
                userInfo!.headimg ?? '',
                sex: userInfo!.sex,
                lableSrc: '',
                userId: userInfo!.id,
                needNavigation: true,
                avatarWidth: 106,
                scene: "B",
              ),
            ),
          ),
          Positioned(
            bottom: ScreenUtil().setWidth(10),
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(180),
              child: Row(children: <Widget>[
                Expanded(child: Container()),
                YBDRoomLevelTag(userInfo!.roomlevel ?? 1, scale: 0.6),
                Expanded(child: Container()),
              ]),
            ),
          ),
        ],
      ),
    );
  }
  void buildttxOyoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
