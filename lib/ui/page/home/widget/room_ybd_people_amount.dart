import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';

/// 房间人数带音乐动图
class YBDRoomPeopleAmount extends StatefulWidget {
  /// item 数据
  final YBDRoomInfo? data;

  YBDRoomPeopleAmount(this.data);

  @override
  YBDRoomPeopleAmountState createState() => new YBDRoomPeopleAmountState();
}

class YBDRoomPeopleAmountState extends State<YBDRoomPeopleAmount> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(30),
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        color: Colors.black.withOpacity(0.5),
      ),
      child: Row(
        children: <Widget>[
          Image.asset(
            // 音乐动图
            'assets/images/icon_live.webp',
            width: ScreenUtil().setWidth(24),
            height: ScreenUtil().setWidth(22),
          ),
          Container(
            // 房间人数
            padding: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
            child: Text(
              widget.data!.count?.toString() ?? '-',
              // 可以限制最大宽度
              // constraints: BoxConstraints(
              // maxHeight: maxWidth: ScreenUtil().setWidth(450),
              style: TextStyle(
                color: Colors.white.withOpacity(0.9),
                fontSize: ScreenUtil().setSp(24),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void buildJKl2qoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
