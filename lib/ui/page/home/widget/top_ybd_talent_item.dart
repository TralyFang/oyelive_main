import 'dart:async';


import 'dart:core';
import 'package:redux/redux.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../module/entity/query_ybd_configs_resp_entity.dart';
import '../../../../redux/app_ybd_state.dart';

/// 2.0.3 需求   最热主播活动  实时排名 前三名 、3-30名 主播加个头像框(两种样式)
/// 模块: Slog动态界面，个人主页的头像上，Popular 主播头像
class YBDTopTalentItem extends StatelessWidget {
  int? sortNum = 0;
  double? height;
  double font;
  double? width;

  YBDTopTalentItem(this.sortNum, {this.width, this.height, this.font = 24});
  @override
  Widget build(BuildContext context) {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    YBDConfigInfo config = store.state.configs!;
    List<String>? icons = config.toptalentRankingImages?.split('|');
    //配置项里面图片不存在的 就不显示了
    if (sortNum == 0 || icons == null || icons.length == 0) return Container();
    return Container(
        width: width ?? 0,
        height: height ?? 0,
        child: Stack(alignment: Alignment.bottomCenter, children: [
          YBDNetworkImage(
            imageUrl: sortNum! <= 3 ? icons.first : icons.last,
            fit: BoxFit.cover,
            // placeholder: (context, url) =>
            //     Image.asset('assets/images/top_talent_${sortNum <= 3 ? 'pre' : 'suf'}.png'),
          ),
          Positioned(
              child: Container(
                  padding: EdgeInsets.only(bottom: 2),
                  width: double.infinity,
                  height: double.infinity,
                  alignment: Alignment.center,
                  child: Text(
                    sortNum.toString(),
                    style:
                        TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(font), fontWeight: FontWeight.w700),
                  )))
        ]));
  }
  void buildORUbAoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
