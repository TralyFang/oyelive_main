import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/numeric_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/query_ybd_followed_resp_entity.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/room_ybd_list_item.dart';
import '../../activty/activity_ybd_skin.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../../room/room_ybd_helper.dart';
import '../bloc/home_ybd_page_bloc.dart';
import '../util/home_ybd_util.dart';

enum RelatedContentType {
  Recently,
  Following,
}

/// 最近观看房间历史记录
class YBDRelatedTabContent extends StatefulWidget {
  /// 查询接口地址
  final RelatedContentType type;

  YBDRelatedTabContent({required this.type});

  @override
  _YBDRelatedTabContentState createState() => _YBDRelatedTabContentState();
}

class _YBDRelatedTabContentState extends State<YBDRelatedTabContent> with AutomaticKeepAliveClientMixin {
  final int _pageSize = 10;

  /// 房间列表
  List<YBDRoomInfo?>? _data = [];

  /// 加载的页数
  int _page = 0;

  /// 是否为初始状态
  bool _isInitState = true;

  /// 列表刷新控制器
  RefreshController _refreshController = RefreshController();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _onRefresh();

    if (widget.type == RelatedContentType.Following) {
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE_RELATED_FOLLOWING));
    } else {
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE_RELATED_RECENTLY));
    }
  }
  void initStateCQXa1oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SmartRefresher(
      controller: _refreshController,
      header: YBDActivitySkinRoot().curAct().activityRefreshHeader(),
      footer: YBDActivitySkinRoot().curAct().activityRefreshFooter(),
      enablePullDown: true,
      enablePullUp: true,
      onRefresh: () {
        logger.v('refresh page');
        // 刷新页面
        _onRefresh();
      },
      onLoading: () {
        logger.v('on loading page');
        // 加载下一页数据
        _onLoading();
      },
      child: _listContent(),
    );
  }
  void buildz8j6voyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 列表内容
  Widget _listContent() {
    if (null == _data || _data!.isEmpty) {
      logger.v('recently list is empty show placeholder');
      if (_isInitState) {
        return Container(
          child: YBDLoadingCircle(),
        );
      } else {
        return Container(
          child: YBDPlaceHolderView(text: translate('no_data')),
        );
      }
    } else {
      return ListView.builder(
        itemCount: _data!.length,
        // shrinkWrap: false,
        itemBuilder: (context, index) {
          return Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                logger.v('clicked recently list view item');
                YBDRoomHelper.enterRoom(
                  context,
                  _data![index]!.id,
                  roomList: _data,
                  tag: _data![index]!.roomCategoryIndex,
                  location:
                      widget.type == RelatedContentType.Following ? 'related_following_page' : 'related_recently_page',
                );
              },
              child: YBDRoomListItem(
                _data![index],
                YBDActivitySkinRoot().curAct().activityTextColor(),
                superRooms: YBDHomeUtil.weeklyStarRooms(context),
                superStars: context.read<YBDHomePageBloc>().state.starList,
                isExtendVo: true,
              ),
            ),
          );
        }, // 类似 cellForRow 函数
      );
    }
  }
  void _listContenttFgODoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 根据 RelatedContentType 获取接口参数
  int _concernType() {
    final concernType = widget.type == RelatedContentType.Recently ? 2 : 1;
    return concernType;
  }

  /// 列表刷新响应方法
  Future<void> _onRefresh() async {
    // 刷新时从0开始查数据
    _page = 0;

    ApiHelper.queryFollowed(
      context,
      2,
      _page,
      _pageSize,
      YBDCommonUtil.storeFromContext()!.state.bean!.id,
      _concernType(),
    ).then((queryFollowedRespEntity) {
      // 刷新数据失败
      if (queryFollowedRespEntity == null || queryFollowedRespEntity.returnCode != Const.HTTP_SUCCESS) {
        logger.v('refresh related data is null');
      } else {
        _data = queryFollowedRespEntity.record?.users;

        if (_data != null && _data!.length < _pageSize) {
          // 没有更多数据
          _refreshController.loadNoData();
        } else {
          _refreshController.resetNoData();
        }
      }

      // 隐藏列表头部刷新动画
      _refreshController.refreshCompleted();
      _cancelInitState();
    });
  }
  void _onRefresh6eP4Soyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消初始状态并刷新页面
  void _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }

  /// 上拉加载更多响应方法
  Future<void> _onLoading() async {
    final userId = await YBDUserUtil.userId();
    YBDQueryFollowedRespEntity? queryFollowedRespEntity = await ApiHelper.queryFollowed(
      context,
      2,
      ++_page * _pageSize,
      _pageSize,
      YBDNumericUtil.stringToInt(userId),
      _concernType(),
    );

    if (queryFollowedRespEntity == null || queryFollowedRespEntity.returnCode != Const.HTTP_SUCCESS) {
      // 加载数据失败
      _refreshController.loadComplete();
      _refreshController.loadFailed();
      return;
    } else {
      List<YBDRoomInfo?>? moreData = queryFollowedRespEntity.record?.users;
      if (moreData == null) {
        // 没有更多数据
        _refreshController.loadComplete();
        _refreshController.loadNoData();
        return;
      } else {
        if (moreData != null && moreData.length < _pageSize) {
          // 没有更多数据
          _refreshController.loadNoData();
        } else {
          _refreshController.loadComplete();
        }

        _data!.addAll(moreData);
      }
    }

    if (mounted) setState(() {});
  }
  void _onLoadingSgLhPoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
