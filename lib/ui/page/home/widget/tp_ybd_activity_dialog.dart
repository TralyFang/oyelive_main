import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/global/tp_ybd_global.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';

/// 首页活动弹框
class YBDTPActivityDialog extends StatelessWidget {
  /// 对话框宽度
  final double _dialogWidth = 540;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the activity dialog bg");
        Navigator.pop(context);
      },
      child: Material(
        color: Colors.black.withOpacity(0.4),
        child: Container(
          width: ScreenUtil().screenWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _contentView(context),
            ],
          ),
        ),
      ),
    );
  }
  void build0el3toyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 对话框内容
  Widget _contentView(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // 闭免点击图片时关闭弹框
      },
      child: Container(
        padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
        width: ScreenUtil().setWidth(_dialogWidth),
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                _bgImage(),
                SizedBox(height: ScreenUtil().setWidth(26)),
                // null != YBDTPGlobal.activityEntity?.url && YBDTPGlobal.activityEntity.url.isNotEmpty
                //     ? _goBtn(context)
                //     : Container()
              ],
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Transform.translate(
                offset: Offset(
                  ScreenUtil().setWidth(_dialogWidth / 2 - 36),
                  ScreenUtil().setWidth(46),
                  // ScreenUtil().setWidth(0),
                ),
                child: Container(
                  width: ScreenUtil().setWidth(60),
                  child: Image.asset(
                    'assets/images/icon_close_opaque.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: _closeBtn(context),
            ),
          ],
        ),
      ),
    );
  }
  void _contentViewilAHJoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 背景图片
  Widget _bgImage() {
    return Container(
      width: ScreenUtil().setWidth(_dialogWidth),
      child: Image.asset(
        'assets/images/activity_dialog_bg.png',
        fit: BoxFit.cover,
      ),
    );
  }
  void _bgImageXkzXDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 跳转按钮
  // Widget _goBtn(BuildContext context) {
  //   return YBDDelayGestureDetector(
  //     onTap: () {
  //       logger.v('clicked activity dialog go btn');
  //       YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.ACTIVITY_GO_CLICK));
  //       String encodedUrl = Uri.encodeComponent(YBDTPGlobal.activityEntity.url);
  //       String encodedImg = Uri.encodeComponent(YBDTPGlobal.activityEntity?.img);
  //       YBDNavigatorHelper.navigateToWeb(
  //         context,
  //         "?url=$encodedUrl&shareName=${YBDTPGlobal.activityEntity?.name}&shareImg=$encodedImg",
  //       );
  //     },
  //     child: Container(
  //       width: ScreenUtil().setWidth(250),
  //       height: ScreenUtil().setWidth(76),
  //       child: Image.asset(
  //         'assets/images/activity_go.png',
  //         fit: BoxFit.cover,
  //       ),
  //     ),
  //   );
  // }

  /// 关闭按钮
  Widget _closeBtn(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v('clicked close btn');
        Navigator.pop(context);
      },
      child: Container(
        width: ScreenUtil().setWidth(96),
        height: ScreenUtil().setWidth(120),
        decoration: BoxDecoration(color: Colors.transparent),
      ),
    );
  }
  void _closeBtnmUzkPoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
