import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../widget/level_ybd_tag.dart';

/// Explore 页面 Special 列表中的头像
class YBDSpecialItemAvatar extends StatelessWidget {
  /// 圆形头像的宽度
  final double? width;

  /// 头像 url 地址
  final String img;

  /// 用户等级
  final int? level;

  YBDSpecialItemAvatar(this.img, this.level, this.width);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      // 高度相对圆形图片越长，级别标签就越靠底部
      height: (width ?? 0) + ScreenUtil().setWidth(10),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            // 外周圆环
            width: width,
            height: width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular((width ?? 0) / 2)),
            ),
            padding: EdgeInsets.all(ScreenUtil().setWidth(2)),
            child: ClipOval(
              // 用户头像
              child: YBDNetworkImage(
                imageUrl: img,
                fit: BoxFit.cover,
                placeholder: (context, url) {
                  return YBDResourcePathUtil.defaultMaleImage(male: false);
                },
                width: width,
                height: width,
                errorWidget: (context, url, err) => YBDResourcePathUtil.defaultMaleImage(male: false),
              ),
            ),
          ),
          Column(
            // 级别标签
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              YBDLevelTag(
                level,
                scale: 0.5,
                horizontalPadding: 0,
              ),
            ],
          ),
        ],
      ),
    );
  }
  void buildjGNgCoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
