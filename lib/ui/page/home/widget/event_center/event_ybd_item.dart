import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_activity_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/page/home/controller/event_ybd_dot_controller.dart';
import 'package:oyelive_main/ui/page/home/entity/event_ybd_center_entity.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

import '../../controller/custom_ybd_type.dart';
import 'red_ybd_icon.dart';

class YBDEventItem extends StatefulWidget {
  final YBDEventCenterDataEventInfos data;
  final EventCenterTabIndex? tabIndex;
  final isInboxItem;

  YBDEventItem(
    this.data, {
    this.isInboxItem = false,
    this.tabIndex,
    Key? key,
  }) : super(key: key);

  @override
  State<YBDEventItem> createState() => _YBDEventItemState();
}

class _YBDEventItemState extends State<YBDEventItem> {
  @override
  Widget build(BuildContext context) {
    ClipRRect item = ClipRRect(
      borderRadius: BorderRadius.all(
        Radius.circular(16.px as double),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.15),
        ),
        child: Column(
          children: [
            Container(
              height: 210.px * _scale(),
              child: YBDImage(
                path: widget.data.eventImg,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              padding: EdgeInsets.all(18.px as double),
              child: Column(
                children: <Widget>[
                  Row(
                    children: [
                      Container(
                        constraints: BoxConstraints(maxWidth: 400.px as double),
                        child: Text(
                          widget.data.eventName ?? '',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 28.sp,
                            overflow: TextOverflow.ellipsis,
                          ),
                          maxLines: 1,
                        ),
                      ),
                      SizedBox(width: 10.px),
                      _newIcon(),
                      Expanded(child: SizedBox()),
                      _endDetail(),
                    ],
                  ),
                  SizedBox(height: 20.px),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      child: Text(
                        widget.data.eventDetail ?? '',
                        style: TextStyle(
                          color: Colors.white.withOpacity(0.6),
                          fontSize: 24.sp,
                        ),
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

    if (widget.isInboxItem) {
      return item;
    }
    return GestureDetector(
      onTap: () {
        _onTapItem(context);
      },
      child: item,
    );
  }
  void buildZJp7Ioyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _endDetail() {
    if (_hideEndDetail()) {
      return SizedBox(width: 0);
    }

    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 8.px as double),
      height: 40.px,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.px as double,
          color: Color(0xff63FFDF),
        ),
        borderRadius: BorderRadius.all(Radius.circular(40.px as double)),
      ),
      child: Center(
        child: Text(
          widget.data.endDetail ?? '',
          style: TextStyle(
            color: Color(0xff63FFDF),
            fontSize: 20.sp,
          ),
        ),
      ),
    );
  }
  void _endDetailLNVsDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 新活动图标
  Widget _newIcon() {
    if (_hideNewIcon()) {
      return SizedBox(width: 0);
    }

    return FutureBuilder<bool>(
      future: widget.data.isRead(),
      builder: (BuildContext context, AsyncSnapshot<bool> isRead) {
        if (isRead.data == true) {
          return SizedBox();
        }

        return YBDRedIcon();
      },
    );
  }
  void _newIcondSuLCoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  bool _hideEndDetail() {
    if (widget.tabIndex == EventCenterTabIndex.ComingSoon || widget.isInboxItem) {
      return true;
    }

    return false;
  }

  bool _hideNewIcon() {
    if (widget.tabIndex != EventCenterTabIndex.InProgress || widget.isInboxItem) {
      return true;
    }

    return false;
  }
  void _hideNewIconBQ5vSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  double _scale() {
    if (widget.isInboxItem) {
      return 0.7;
    }

    return 1.0;
  }

  /// 跳转到活动页面
  void _onTapItem(BuildContext context) async {
    if (null != widget.data.eventUrl && widget.data.eventUrl!.isNotEmpty) {
      if (widget.data.eventStatus == 'Online') {
        await Get.find<YBDEventDotController>().addRead(widget.data.idStr);
      }
      // 进入活动的埋点
      YBDActivityInTrack().activityIn(YBDTAProps(location: YBDLocationName.EVENT_CENTER_PAGE, url: widget.data.eventUrl));
      YBDCommonTrack().commonTrack(YBDTAProps(location: 'detail', module: YBDTAModule.event, name: widget.data.eventUrl?.key));
      YBDNavigatorHelper.navigateToWeb(context,
          '?url=${Uri.encodeComponent(widget.data.eventUrl!)}&entranceType=${WebEntranceType.EventCenter.index}&showNavBar=true');
    } else {
      if (widget.data.eventStatus == 'Offline') {
        YBDToastUtil.toast(translate('event_ended_toast'));
      } else if (widget.data.eventStatus == 'Pending') {
        YBDToastUtil.toast(translate('coming_soon_toast'));
      } else {
        // Online 点击无反应
      }
    }

    if (mounted) {
      setState(() {});
    }

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: YBDLocationName.EVENT_CENTER_PAGE,
      itemName: 'status_${widget.data.eventStatus}',
    ));
  }
  void _onTapItem4uqPNoyelive(BuildContext context)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
