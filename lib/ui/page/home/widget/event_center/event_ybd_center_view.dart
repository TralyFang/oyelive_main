import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/widget/refresher/refresh_ybd_container.dart';
import 'package:oyelive_main/ui/page/follow/widget/place_ybd_holder_view.dart';
import 'package:oyelive_main/ui/page/home/controller/event_ybd_center_controller.dart';
import 'package:oyelive_main/ui/page/home/entity/event_ybd_center_entity.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';
import 'package:oyelive_main/ui/widget/my_ybd_refresh_indicator.dart';

import '../../controller/custom_ybd_type.dart';
import 'event_ybd_item.dart';

class YBDEventCenterView extends StatefulWidget {
  final EventCenterTabIndex? tabIndex;

  const YBDEventCenterView({
    this.tabIndex,
    Key? key,
  }) : super(key: key);

  @override
  State<YBDEventCenterView> createState() => _YBDEventCenterViewState();
}

class _YBDEventCenterViewState extends State<YBDEventCenterView> with AutomaticKeepAliveClientMixin {
  final RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    Get.find<YBDEventCenterController>().requestEventList(tabIndex: widget.tabIndex);
  }
  void initStateqBRwsoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }
  void disposeBXMh5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GetBuilder<YBDEventCenterController>(builder: (YBDEventCenterController eventCenterCtr) {
      List<YBDEventCenterDataEventInfos>? data = eventCenterCtr.getDataWithIndex(widget.tabIndex);

      if (null == data) {
        return YBDLoadingCircle();
      }

      if (data.isEmpty) {
        return Transform.translate(
          offset: Offset(0, -200.px as double),
          child: YBDPlaceHolderView(text: translate('no_data_empty')),
        );
      }

      // 上拉加载
      bool enablePullUp = eventCenterCtr.enablePage(widget.tabIndex);

      // 显示自定义footer
      bool showCustomFooter = eventCenterCtr.showCustomFooter(widget.tabIndex);

      ListView child = ListView.separated(
        padding: EdgeInsets.only(bottom: 160.px + kBottomBarHeight),
        // 增加列表长度显示自定义 footer
        itemCount: showCustomFooter ? data.length + 1 : data.length,
        itemBuilder: (_, int index) {
          if (showCustomFooter && index == data.length) {
            // 最后一行显示自定义footer：no more data
            return _customNoMoreData();
          }

          return YBDEventItem(
            data[index],
            tabIndex: widget.tabIndex,
          );
        },
        separatorBuilder: (_, int index) {
          return SizedBox(height: 25.px);
        },
      );

      if (eventCenterCtr.enablePage(widget.tabIndex)) {
        return SmartRefresher(
          controller: _refreshController,
          enablePullDown: false,
          enablePullUp: enablePullUp,
          footer: YBDMyRefreshIndicator.myFooter,
          onLoading: () async {
            await eventCenterCtr.nextPage(tabIndex: widget.tabIndex);
            _refreshController.loadComplete();

            if (eventCenterCtr.getNoMoreDataWithIndex(widget.tabIndex)) {
              _refreshController.loadNoData();
            }
          },
          child: child,
        );
      }

      return child;
    });
  }
  void buildQFU5soyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _customNoMoreData() {
    return Container(
      width: kScreenWidthDp,
      child: Center(
        child: Text(
          'No more data',
          style: TextStyle(
            color: Colors.white.withOpacity(0.6),
          ),
        ),
      ),
    );
  }
  void _customNoMoreData8rUiroyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
