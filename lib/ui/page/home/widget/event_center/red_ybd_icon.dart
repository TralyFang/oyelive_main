import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

class YBDRedIcon extends StatelessWidget {
  final double scale;
  const YBDRedIcon({this.scale = 1.0, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: scale,
      child: Container(
        width: 60.px,
        height: 30.px,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20.px as double)),
          color: Color(0xffE6497A),
        ),
        child: Center(
          child: Text(
            translate('new'),
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.sp,
            ),
          ),
        ),
      ),
    );
  }
  void buildoFSdgoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
