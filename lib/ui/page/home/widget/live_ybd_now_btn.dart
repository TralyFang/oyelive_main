import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

/// 首页开始直播的按钮
class YBDLiveNowButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: ScreenUtil().setWidth(180),
        height: ScreenUtil().setWidth(60),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(30)),
          gradient: LinearGradient(colors: [Color(0xffA3C8F9), Color(0xff8566F6), Color(0xffC03BF7)]),
        ),
        child: Row(
          children: <Widget>[
            SizedBox(width: ScreenUtil().setWidth(12)),
            Container(
              width: ScreenUtil().setWidth(40),
              height: ScreenUtil().setWidth(40),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(2)),
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
              ),
              child: Image.asset(
                // 音乐动图
                'assets/images/icon_live.webp',
                width: ScreenUtil().setWidth(24),
                height: ScreenUtil().setWidth(22),
              ),
            ),
            SizedBox(width: ScreenUtil().setWidth(10)),
            Text(
              translate('live_now'),
              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24), fontWeight: FontWeight.bold),
            )
          ],
        ));
  }
  void buildHzFGxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
