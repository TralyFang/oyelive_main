import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_activity_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../room/room_ybd_helper.dart';
import '../bloc/advertise_ybd_bloc.dart';
import '../entity/banner_ybd_advertise_entity.dart';

/// Popular 的广告栏
class YBDPopularBanner extends StatefulWidget {
  int? type;

  YBDPopularBanner({this.type});

  @override
  YBDPopularBannerState createState() => new YBDPopularBannerState();
}

class YBDPopularBannerState extends State<YBDPopularBanner> {
  /// 按钮已经被点击的标识位
  bool _isButtonTapped = false;

  @override
  void initState() {
    super.initState();
  }
  void initStatekrEIfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
  void disposeHaTYwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(24))),
      child: Container(
        width: ScreenUtil().setWidth(670),
        height: ScreenUtil().setWidth(210), //2021/7/1 瑶瑶 改了
        child: BlocBuilder<YBDAdvertiseBloc, YBDAdvertiseBlocState>(builder: (context, state) {
          if (state.isInit) {
            logger.v('show init ad banner view');
            return Container(
              color: Color(0x33ffffff),
              width: ScreenUtil().screenWidth,
              height: ScreenUtil().setWidth(220),
            );
          } else {
//          logger.v('show ad banner view');
            return bannerPageView(state.banners);
          }
        }),
      ),
    );
  }
  void buildcQpAmoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 广告栏翻页视图
  Widget bannerPageView(List<YBDBannerAdvertiseRecord?>? banners) {
    if (null != banners && banners.isNotEmpty) {
//      logger.v('ad banners amount : ${banners.length}');

      return NotificationListener(
        onNotification: (ScrollNotification notification) {
          if (notification is ScrollStartNotification) {
            if (notification.dragDetails != null) {
              if (banners.first!.adtype == 1)
                YBDCommonTrack().commonTrack(YBDTAProps(location: 'banner_slide', module: YBDTAModule.home));
            }
          }
          return false;
        },
        child: Swiper(
          key: ValueKey('${banners.length}'),
          itemCount: banners.length,
          autoplay: true,
          autoplayDelay: 5000,
          outer: false,
          pagination: SwiperPagination(
              alignment: Alignment.bottomRight,
              margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
              builder: DotSwiperPaginationBuilder(
                  size: ScreenUtil().setWidth(10),
                  activeSize: ScreenUtil().setWidth(10),
                  color: Colors.white,
                  activeColor: YBDTPStyle.heliotrope)),
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(child: bannerImage(banners[index]));
          },
        ),
      );
    } else {
      return Container(
        // margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
        decoration: BoxDecoration(color: Color(0xff5B02AE).withOpacity(0.3)),
      );
    }
  }
  void bannerPageViewxciLkoyelive(List<YBDBannerAdvertiseRecord?>? banners) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 广告栏图片
  Widget bannerImage(YBDBannerAdvertiseRecord? ad) {
    String imgUrl;
    if (null != ad) {
      imgUrl = YBDImageUtil.ad(context, ad.img, 'A');
    } else {
      logger.v('ad is null show default ad img');
      imgUrl = YBDImageUtil.getDefaultImg(context);
    }
    return Container(
      decoration: BoxDecoration(color: Colors.transparent),
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: YBDNetworkImage(imageUrl: imgUrl, fit: BoxFit.cover),
          ),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  bool isHomeBanner = ad!.adtype == 1;
                  YBDCommonTrack().commonTrack(YBDTAProps(
                    location: isHomeBanner ? 'banner_click' : 'banner',
                    module: isHomeBanner ? YBDTAModule.home : YBDTAModule.game,
                  ));
                  // false 才能跳转
                  if (!_isButtonTapped) {
                    // 标记为 true，防止重复跳转
                    _isButtonTapped = true;

                    // 1 秒后恢复标识位
                    Future.delayed(Duration(seconds: 1), () {
                      _isButtonTapped = false;
                    });

                    // 跳转到另一个页面
                    clickedBannerImage(ad);
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
  void bannerImagepfILZoyelive(YBDBannerAdvertiseRecord? ad) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击广告图片的响应事件
  clickedBannerImage(YBDBannerAdvertiseRecord? ad) {
    logger.v("Click banner : ${ad?.toJson()}");
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: YBDLocationName.HOME_PAGE,
      itemName: YBDItemName.HOME_PAGE_BANNER,
      adUnitName: ad?.adname,
    ));

    if ((ad?.adurl ?? '').isNotEmpty) {
      String? adUrl = ad!.adurl;
      List<String> urls = ad.adurl!.split('|');
      if (urls.length >= 2 && urls[1].isNotEmpty) {
        adUrl = urls[1];
      }

      logger.v("Click banner, adurl: $adUrl ad.img: ${ad.img}");
      // 进入活动埋点
      YBDActivityInTrack().activityIn(YBDTAProps(location: YBDTAEventLocation.popular_banner, url: adUrl));
      if (adUrl!.startsWith('http')) {
        YBDNavigatorHelper.navigateToWeb(
          context,
          "?url=${Uri.encodeComponent(adUrl)}&title=${ad.adname}&shareName=${ad.adname}&shareImg=${ad.img}&entranceType=${WebEntranceType.Banner.index}",
        );
      } else if (adUrl.startsWith('talent_')) {
        YBDRoomHelper.enterRoom(context, int.parse(adUrl.substring(7)), location: 'home_popular_banner');
      } else if (adUrl.toUpperCase() == 'LIVE') {
        YBDRoomHelper.enterRoom(context, null, location: 'home_popular_banner');
      } else {
        if (adUrl.isEmpty || adUrl == '#') return;
        YBDNavigatorHelper.navigateTo(context, adUrl);
      }
    }
  }
}
