import 'dart:async';


import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:package_info/package_info.dart';
import 'package:redux/redux.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/common_ybd_util.dart';
import '../../../../common/util/config_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/user/entity/topup_ybd_summary_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import 'language_ybd_list_dialog.dart';

/// Popular 页面的功能入口列表
class YBDPopularFeatureList extends StatefulWidget {
  @override
  YBDPopularFeatureListState createState() => YBDPopularFeatureListState();
}

class YBDPopularFeatureListState extends BaseState<YBDPopularFeatureList> {
  String topUpIcon = 'assets/images/topup/y_top_up.png';

  @override
  Widget myBuild(BuildContext context) {
    return Container(
      // 活动背景高度
      padding: EdgeInsets.only(bottom: 0),
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            child: _topImage(),
          ),
          _listViewContainer(),
        ],
      ),
    );
  }
  void myBuildVid8Koyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 活动背景
  Widget _topImage() {
    return Container();
  }
  void _topImageirIqRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 横向列表
  Widget _listViewContainer() {
    return Container(
      padding: EdgeInsets.only(top: 0),
      height: ScreenUtil().setWidth(160),
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          featureItem(
            'assets/images/icon_support.png',
            translate('support'),
            () async {
              YBDCommonUtil.showSupport(context);
              YBDAnalyticsUtil.logEvent(
                  YBDAnalyticsEvent(YBDEventName.CLICK_EVENT, location: YBDLocationName.HOME_PAGE, itemName: 'support'));
            },
          ),
          featureItem(
            'assets/images/dc/daily_task.png',
            translate('daily_tasks'),
            () {
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.daily_task);
              YBDAnalyticsUtil.logEvent(
                  YBDAnalyticsEvent(YBDEventName.CLICK_EVENT, location: YBDLocationName.HOME_PAGE, itemName: 'daily_task'));
            },
          ),
          featureItem(
            topUpIcon,
            translate('top_up'),
            () {
              logger.v('go to native top up page');
              YBDNavigatorHelper.openTopUpPage(context);
              YBDAnalyticsUtil.logEvent(
                  YBDAnalyticsEvent(YBDEventName.CLICK_EVENT, location: YBDLocationName.HOME_PAGE, itemName: 'top_up'));
            },
          ),
          featureItem(
            'assets/images/buy_vip.png',
            translate('vip'),
            () {
              logger.v('go to native vip page');
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.vip);
              YBDAnalyticsUtil.logEvent(
                  YBDAnalyticsEvent(YBDEventName.CLICK_EVENT, location: YBDLocationName.HOME_PAGE, itemName: 'vip'));
            },
          ),
          featureItem(
            'assets/images/language.png',
            translate('language_text'),
            () async {
              String locale = (await YBDSPUtil.get(Const.SP_LOCALE)) ?? Const.DEFAULT_LANG;

              showDialog(
                  context: context,
                  barrierDismissible: true,
                  builder: (context) {
                    return YBDLanguageListDialog(locale);
                  });
              YBDAnalyticsUtil.logEvent(
                  YBDAnalyticsEvent(YBDEventName.CLICK_EVENT, location: YBDLocationName.HOME_PAGE, itemName: 'language'));
            },
          ),
        ],
      ),
    );
  }
  void _listViewContainer1CFY4oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 功能按钮
  Widget featureItem(String img, String text, VoidCallback onTap) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Colors.black12,
        highlightColor: Colors.black26,
        onTap: () {
          logger.v('clicked feature item : $text');
          onTap();
        },
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                img,
                width: ScreenUtil().screenWidth / 5,
                height: ScreenUtil().setWidth(80),
              ),
              SizedBox(height: ScreenUtil().setWidth(6)),
              Text(
                text,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(22),
                  color: Colors.white.withOpacity(0.8),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void featureItemurdEjoyelive(String img, String text, VoidCallback onTap) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      renderTopUpIcon().then((value) async {
        /// call addlocation api, 保存设备国家、城市
        String? deviceCountry = await YBDSPUtil.get(Const.SP_DEVICE_COUNTRY_CODE);
        String? deviceCity = await YBDSPUtil.get(Const.SP_DEVICE_CITY_NAME);

        /// 如果用户设置国家为空，设置默认国家  SP.select > ip > device
        String? selectCountry;
        String? userCountry = YBDUserUtil.getLoggedUser(context)?.country;
        if (userCountry == null || userCountry.isEmpty) {
          selectCountry = (await YBDSPUtil.get(Const.SP_SELECT_COUNTRY_CODE)) ??
              await YBDCommonUtil.getIpCountryCode(context) ??
              deviceCountry;
        }

        ApiHelper.updateLocation(context, selectCountry, deviceCountry: deviceCountry, deviceCity: deviceCity)
            .then((value) {
          if (value?.returnCode == Const.HTTP_SUCCESS) {
            // 更新成功，更新本地用户信息国家
            YBDUserUtil.updateCountry(context, selectCountry);
          }
        });
      });
    });
  }
  void initStateHfwcMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 支持第三方充值方式条件 （首页充值按钮是否显示成 Easypaisa 或 Paytm），需要同时满足以下三个条件：
  /// 1. app_android 第四个字段为 -1 ，或者 >= 当前app version code；
  /// 2. 用户所在国家为白名单国家 ， 或者用户为已充值用户
  /// 3. Android 版本
  Future<void> renderTopUpIcon() async {
    Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);

    /// 检查version code
    int validVersionCode = -1;
    String? appAndroid = await ConfigUtil.appAndroid(context);

    // 检查数组越界
    if (appAndroid?.split('|') != null && appAndroid!.split('|').length > 4) {
      if (appAndroid.split('|')[4] != null) {
        validVersionCode = int.parse(appAndroid.split('|')[4]);
      }
    }
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    if (validVersionCode != -1 && int.parse(packageInfo.buildNumber) > validVersionCode) {
      return;
    }

    /// 检查是否在白名单国家
    final whitelistCountries = await ConfigUtil.whitelistCountries(context);
    List<String> whiteListCountries = whitelistCountries?.split('|') ?? [];
    String ipCountryCode = await YBDCommonUtil.getIpCountryCode(context); // 查询IP所在国家，并存入SP
    if (!whiteListCountries.contains(ipCountryCode)) {
      /// 检查是否充值用户
      YBDTopUpSummaryEntity? result = await ApiHelper.queryTopUpSummary(context);
      bool isToppedUpUser = null != result?.record?.recharge && result!.record!.recharge!;
      if (!isToppedUpUser) return;
    }

    if (Platform.isAndroid && mounted) {
      setState(() {
        if ('PK' == YBDUserUtil.getLoggedUser(context)?.country) {
          topUpIcon = 'assets/images/icon_easypaisa.webp';
        }
        // else if ('IN' == selectCountryCode) {
        // 印度不再支持 paytm
        // topUpIcon = 'assets/images/icon_paytm.webp';
        // }
      });
    }
  }
  void renderTopUpIconUWF3Goyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
