import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_skin.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../bloc/gifters_ybd_bloc.dart';
import '../bloc/receivers_ybd_bloc.dart';
import '../bloc/special_ybd_list_bloc.dart';
import 'explore_ybd_record_item.dart';
import 'explore_ybd_special_item.dart';
import '../../../widget/loading_ybd_circle.dart';

/// Explore 中的排行榜和 special
class YBDExploreRankTab extends StatefulWidget {
  @override
  YBDExploreRankTabState createState() => YBDExploreRankTabState();
}

/// record 标签栏的高度
const RECORD_HEIGHT = 220;

/// record 标签栏的索引号
const RECORD_INDEX = 0;

/// special 标签栏的高度
const SPECIAL_HEIGHT = 340;

/// record 标签栏的索引号
const SPECIAL_INDEX = 1;

class YBDExploreRankTabState extends State<YBDExploreRankTab> with SingleTickerProviderStateMixin {
  /// tab 控制器
  TabController? _tabController;

  /// special 列表滚动控制器
  ScrollController? _scrollController;

  /// 选中的标签索引
  int _selectedTabIndex = RECORD_INDEX;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _tabController!.addListener(() {
      _selectedTabIndex = _tabController!.index;
      setState(() {});
    });
    _scrollController = ScrollController();
    _scrollController!.addListener(() {
      if (_scrollController!.offset >= _scrollController!.position.maxScrollExtent &&
          !_scrollController!.position.outOfRange) {
        logger.v('special list reach the end, load next page special data');
        context.read<YBDSpecialListBloc>().add(SpecialListEvent.NextPage);
      }
    });
  }
  void initStateVXcIzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _tabController!.dispose();
    _scrollController!.dispose();
    super.dispose();
  }
  void disposeURrFSoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(_selectedTabIndex == RECORD_INDEX ? RECORD_HEIGHT : SPECIAL_HEIGHT),
      child: Column(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(360),
            child: TabBar(
//                indicator: BoxDecoration(
//                  image: DecorationImage(
//                    // 选中时的 tab 背景图
//                    fit: BoxFit.contain,
//                    image: AssetImage('assets/images/explore_tab_bg_img.webp'),
//                  ),
//                ),

              indicator: BoxDecoration(),
              labelColor: Colors.white,
              unselectedLabelColor: Colors.white,
              controller: _tabController,
              labelStyle: TextStyle(
                // 选中样式
                fontWeight: FontWeight.w400,
                height: 1,
                fontSize: ScreenUtil().setSp(30),
              ),
              unselectedLabelStyle: TextStyle(
                // 非选中样式
                fontWeight: FontWeight.w400,
                height: 1,
                fontSize: ScreenUtil().setSp(28),
              ),
              tabs: [
                Column(
                  children: <Widget>[
                    Container(
                      height: ScreenUtil().setWidth(60),
                      child: Tab(
                        child: Text(
                          translate('record'),
                          style: TextStyle(
                              color: _selectedTabIndex == RECORD_INDEX
                                  ? YBDActivitySkinRoot().curAct().activityTextColor()
                                  : YBDActivitySkinRoot().curAct().activityTextColor().withOpacity(0.7),
                              fontSize: ScreenUtil().setSp(28)),
                        ),
                      ),
                    ),
                    if (_selectedTabIndex == RECORD_INDEX)
                      Container(
                        width: ScreenUtil().setWidth(40),
                        height: ScreenUtil().setWidth(4),
                        decoration: BoxDecoration(
                            color: Color(0xff63FFDF),
                            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(4)))),
                      )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Container(
                      height: ScreenUtil().setWidth(60),
                      child: Tab(
                        child: Text(
                          translate('special'),
                          style: TextStyle(
                              color: _selectedTabIndex == SPECIAL_INDEX
                                  ? YBDActivitySkinRoot().curAct().activityTextColor()
                                  : YBDActivitySkinRoot().curAct().activityTextColor().withOpacity(0.7),
                              fontSize: ScreenUtil().setSp(28)),
                        ),
                      ),
                    ),
                    if (_selectedTabIndex == SPECIAL_INDEX)
                      Container(
                        width: ScreenUtil().setWidth(40),
                        height: ScreenUtil().setWidth(4),
                        decoration: BoxDecoration(
                            color: Color(0xff63FFDF),
                            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(4)))),
                      )
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
//            height: ScreenUtil().setWidth(280),
              width: ScreenUtil().screenWidth,
              // tab 内容横向缩进
              padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(24)),
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  Column(
                    // Record tab 内容
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setWidth(20)),
                      recordListView(),
                    ],
                  ),
                  Column(
                    // Special tab 内容
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setWidth(20)),
                      specialListView(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  void buildsif0Foyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// special 横向列表
  Widget specialListView() {
    return BlocBuilder<YBDSpecialListBloc, YBDSpecialListBlocState>(builder: (context, state) {
      logger.v('build special list view');
      if (state.isInit) {
        logger.v('show init special list view');
        return YBDLoadingCircle();
      } else {
        logger.v('special list view length : ${state.speicalList?.length}');
        // 没有数据时显示占位 item
        var showPlaceHolder = ((state.speicalList?.length ?? 0) == 0);
        logger.v('special list view length :showPlaceHolder $showPlaceHolder');

        if (showPlaceHolder) {
          // 显示缺省图
          return _placeHoldItem();
        }

        return Container(
          height: ScreenUtil().setWidth(240),
          child: ListView.separated(
            controller: _scrollController,
            scrollDirection: Axis.horizontal,
            itemBuilder: (_, index) {
              return Container(
                width: ScreenUtil().setWidth(160),
                height: ScreenUtil().setWidth(240),
                decoration: BoxDecoration(
                  color: YBDActivitySkinRoot().curAct().exploreRecordBgColor(),
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
                ),
                child: showPlaceHolder ? _placeHoldItem() : YBDExploreSpecialItem(state.speicalList![index]),
              );
            },
            separatorBuilder: (_, index) {
              return SizedBox(width: ScreenUtil().setWidth(10));
            },
            itemCount: state.speicalList!.length,
          ),
        );
      }
    });
  }
  void specialListView3G0Yqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 没有数据时显示透明背景
  Widget _placeHoldItem() {
    return Column(
      children: [
        SizedBox(
          height: ScreenUtil().setWidth(20),
        ),
        Container(
          width: ScreenUtil().setWidth(200),
          // height: ,
          child: Image.asset(
            'assets/images/special_empty.png',
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(
          height: ScreenUtil().setWidth(16),
        ),
        Text(
          translate('no_data'),
          style: TextStyle(
            color: YBDActivitySkinRoot().curAct().activityTextColor().withOpacity(0.5),
            fontSize: ScreenUtil().setSp(24),
          ),
        ),
      ],
    );
  }
  void _placeHoldItemJsTgqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Record 图标
  Widget recordListView() {
    return Container(
      height: ScreenUtil().setWidth(120),
      child: Wrap(
        spacing: ScreenUtil().setWidth(10),
        runSpacing: ScreenUtil().setWidth(10),
        children: <Widget>[
          Container(
            // Gifters
            width: ScreenUtil().setWidth(330),
            height: ScreenUtil().setWidth(114),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
            ),
            child: BlocBuilder<YBDGiftersBloc, YBDGiftersBlocState>(
              builder: (context, state) {
                return YBDExploreRecordItem(RankType.Gifters, state.gifters);
              },
            ),
          ),
          Container(
            // Receivers
            width: ScreenUtil().setWidth(330),
            height: ScreenUtil().setWidth(114),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
            ),
            child: BlocBuilder<YBDReceiversBloc, YBDReceiversBlocState>(
              builder: (context, state) {
                return YBDExploreRecordItem(RankType.Receivers, state.receivers);
              },
            ),
          ),
        ],
      ),
    );
  }
  void recordListViewEQpDvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
