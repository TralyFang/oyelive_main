import 'dart:async';


/*
 * @Date: 2021-07-02 10:20:28
 * @LastEditors: William
 * @LastEditTime: 2022-07-07 11:20:34
 * @FilePath: /oyetalk-flutter/lib/ui/page/home/widget/pk_label_item.dart
 */
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';

/// PK标签 从配置项取
class YBDPKLabelItem extends StatelessWidget {
  double? width;
  double? height;
  YBDPKLabelItem({this.width, this.height});
  @override
  Widget build(BuildContext context) {
    //从配置项取
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    String? label = store.state.configs?.pkLabelImage;
    if (label == null || label.isEmpty) return Container();
    return Container(
      width: width ?? 0,
      height: height ?? 0,
      child: YBDNetworkImage(
        imageUrl: label,
        fit: BoxFit.cover,
        // 要不要加缺省值？
        // placeholder: (context, url) => Image.asset('assets/images/'),
        // errorWidget: (context, url, error) => Image.asset('assets/images/'),
      ),
    );
  }
  void buildNG7vXoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
