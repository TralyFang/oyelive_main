import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

/// 房间周榜标签
class YBDWeeklyStarItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(6),
        vertical: ScreenUtil().setWidth(4),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(14)),
        color: Color(0xffE6497A),
        boxShadow: [
          BoxShadow(
            color: Color(0xff3C3C3C).withOpacity(0.6),
            blurRadius: 1,
            offset: Offset(0, ScreenUtil().setWidth(2)),
          )
        ],
      ),
      child: Row(
        children: <Widget>[
          // TODO: 测试数据
          Image.asset(
            'assets/images/weekly_star.png',
            width: ScreenUtil().setWidth(32),
            height: ScreenUtil().setWidth(20),
          ),
          Text(
            translate('weekly_star'),
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(18),
            ),
          ),
        ],
      ),
    );
  }
  void buildXIGs2oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
