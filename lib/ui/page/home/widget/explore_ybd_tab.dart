import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/util/config_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/room_ybd_list_item.dart';
import '../../activty/activity_ybd_skin.dart';
import '../../room/room_ybd_helper.dart';
import '../bloc/combo_ybd_bloc.dart';
import '../bloc/explore_ybd_bloc.dart';
import '../bloc/followers_ybd_bloc.dart';
import '../bloc/gifters_ybd_bloc.dart';
import '../bloc/home_ybd_page_bloc.dart';
import '../bloc/receivers_ybd_bloc.dart';
import '../bloc/special_ybd_list_bloc.dart';
import '../util/home_ybd_util.dart';
import '../widget/home_ybd_list_header.dart';
import 'explore_ybd_game_block.dart';
import 'explore_ybd_rank_tab.dart';
import 'home_ybd_grid_item.dart';
// import 'package:http_proxy/http_proxy.dart';

/// 首页 Explore tab 的内容, 包含列表， Record 和 special
class YBDExploreTab extends StatefulWidget {
  /// 滚动控制器
  /// 双击 tab 滚动到顶部
  final ScrollController? scrollController;

  YBDExploreTab(this.scrollController);

  @override
  YBDExploreTabState createState() => YBDExploreTabState();
}

class YBDExploreTabState extends State<YBDExploreTab> with AutomaticKeepAliveClientMixin {
  /// 下拉刷新控制器
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  /// 列表样式标志位
  bool _isGrid = true;

  /// 是否显示游戏
  bool _shouldShowGame = false;

  @override
  void initState() {
    super.initState();

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE, location: YBDLocationName.HOME_EXPLORE_PAGE));

    context.read<YBDExploreBloc>().add(ExploreEvent.Refresh);
    // 在 bloc 里更新刷新 ui
    context.read<YBDExploreBloc>().refreshController = _refreshController;
    context.read<YBDReceiversBloc>().add(ReceiversEvent.Refresh);
    context.read<YBDGiftersBloc>().add(GiftersEvent.Refresh);
    context.read<YBDFollowersBloc>().add(FollowersEvent.Refresh);
    // context.read<YBDComboBloc>().add(ComboEvent.Refresh);
    context.read<YBDSpecialListBloc>().add(SpecialListEvent.Refresh);

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      // 根据配置项判断是否显示游戏
      _shouldShowGame = await ConfigUtil.shouldShowGame(context: context, allPlatform: true);
      setState(() {});
    });
  }
  void initStatezLe2Ooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }
  void dispose99pkOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: SmartRefresher(
        controller: _refreshController,
        header: YBDActivitySkinRoot().curAct().activityRefreshHeader(),
        footer: YBDActivitySkinRoot().curAct().activityRefreshFooter(),
        enablePullDown: true,
        enablePullUp: true,
        onRefresh: () async {
          logger.v('refresh explore page');
          context.read<YBDExploreBloc>().add(ExploreEvent.Refresh);
          context.read<YBDReceiversBloc>().add(ReceiversEvent.Refresh);
          context.read<YBDGiftersBloc>().add(GiftersEvent.Refresh);
          context.read<YBDFollowersBloc>().add(FollowersEvent.Refresh);
          // context.read<YBDComboBloc>().add(ComboEvent.Refresh);
          context.read<YBDSpecialListBloc>().add(SpecialListEvent.Refresh);
          // if (Const.TEST_ENV) {
          //   // 用来主动刷新代理域名和端口
          //   HttpProxy httpProxy = await HttpProxy.createHttpProxy();
          //   Const.PROXY_HOST = httpProxy.host ?? '';
          //   Const.PROXY_PORT = httpProxy.port ?? '';
          //   print('9.6---refresh---PROXY_HOST:${Const.PROXY_HOST}----PROXY_PORT:${Const.PROXY_PORT}');
          // }
        },
        onLoading: () {
          logger.v('on loading explore page');
          context.read<YBDExploreBloc>().add(ExploreEvent.NextPage);
        },
        child: StoreBuilder<YBDAppState>(
          builder: (context, store) {
            return contentView();
          },
        ),
      ),
    );
  }
  void buildF5djsoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 页面内容
  Widget contentView() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          YBDExploreRankTab(),
          _shouldShowGame ? YBDExploreGameBlock() : Container(),
          YBDHomeListHeader(
            () {
              logger.v('changed list style callback');
              setState(() {
                _isGrid = !_isGrid;
              });
            },
            title: 'New Talents',
          ),
          BlocBuilder<YBDExploreBloc, YBDExploreBlocState>(builder: (context, state) {
            if (_isGrid) {
              if (state.isInit) {
                logger.v('show grid view init state');
                return Container(
                  height: ScreenUtil().setWidth(600),
                  child: YBDLoadingCircle(),
                );
              } else {
                logger.v('show grid view');
                return exploreGridView(state.rooms, YBDHomeUtil.weeklyStarRooms(context));
              }
            } else {
              if (state.isInit) {
                logger.v('show list view init state');
                return Container(
                  height: ScreenUtil().setWidth(600),
                  child: YBDLoadingCircle(),
                );
              } else {
                return exploreListView(state.rooms, YBDHomeUtil.weeklyStarRooms(context));
              }
            }
          }),
        ],
      ),
    );
  }
  void contentViewKSBtToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 线性列表
  Widget exploreListView(List<YBDRoomInfo?>? rooms, List<String> superRooms) {
    if (null != rooms && rooms.isNotEmpty) {
      logger.v('explore list amount : ${rooms.length}');
      return ListView.separated(
        padding: EdgeInsets.all(0),
        controller: widget.scrollController,
        itemBuilder: (_, index) {
          return Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                logger.v('clicked explore list view item');
                YBDRoomHelper.enterRoom(
                  context,
                  rooms[index]!.id,
                  roomList: rooms,
                  tag: rooms[index]!.roomCategoryIndex,
                  forceEnter: true,
                  location: YBDLocationName.HOME_EXPLORE_PAGE,
                );
              },
              child: YBDRoomListItem(
                rooms[index],
                YBDActivitySkinRoot().curAct().activityTextColor(),
                superRooms: superRooms,
                superStars: context.read<YBDHomePageBloc>().state.starList,
              ),
            ),
          );
        },
        separatorBuilder: (_, index) => Container(
          margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
          height: ScreenUtil().setWidth(1),
          color: Theme.of(context).primaryColor.withOpacity(0.1),
        ),
        shrinkWrap: true,
        itemCount: rooms.length,
      );
    } else {
      logger.v('explore list is empty, show empty view');
      // TODO: 显示缺省图
      return Container();
    }
  }
  void exploreListViewGRRSCoyelive(List<YBDRoomInfo?>? rooms, List<String> superRooms) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 网格列表
  Widget exploreGridView(List<YBDRoomInfo?>? rooms, List<String> superRooms) {
    if (null != rooms && rooms.isNotEmpty) {
      logger.v('explore grid amount : ${rooms.length}');
      return GridView.count(
        padding: EdgeInsets.only(
          top: ScreenUtil().setWidth(10),
          left: ScreenUtil().setWidth(24),
          right: ScreenUtil().setWidth(24),
        ),
        crossAxisCount: 2,
        controller: widget.scrollController,
        shrinkWrap: true,
        crossAxisSpacing: ScreenUtil().setWidth(10),
        mainAxisSpacing: ScreenUtil().setWidth(10),
        children: List.generate(rooms.length, (index) {
          // 奇数网格项
          // bool isOdd = (((index + 1) % 2) != 0);
          return Container(
            // margin: EdgeInsets.fromLTRB(
            //   isOdd ? ScreenUtil().setWidth(10) : ScreenUtil().setWidth(5),
            //   0,
            //   isOdd ? ScreenUtil().setWidth(5) : ScreenUtil().setWidth(10),
            //   ScreenUtil().setWidth(10),
            // ),
            child: YBDHomeGridItem(rooms[index], superRooms, context.read<YBDHomePageBloc>().state.starList, (info) {
              logger.v('explore grid item callback');
              YBDRoomHelper.enterRoom(
                context,
                rooms[index]!.id,
                roomList: rooms,
                tag: rooms[index]!.roomCategoryIndex,
                forceEnter: true,
                location: YBDLocationName.HOME_EXPLORE_PAGE,
              );
            }),
          );
        }),
      );
    } else {
      logger.v('explore grid is empty, show empty view');
      // TODO: 显示缺省图
      return Container();
    }
  }
  void exploreGridViewt9M8Boyelive(List<YBDRoomInfo?>? rooms, List<String> superRooms) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

}
