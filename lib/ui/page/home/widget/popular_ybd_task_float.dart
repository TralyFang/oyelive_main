import 'dart:async';


import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_activity_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import 'package:oyelive_main/ui/page/home/entity/new_ybd_user_status.dart';
import 'package:oyelive_main/ui/page/home/widget/tp_ybd_web_view.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';

class YBDPopularTaskFloat extends StatefulWidget {
  const YBDPopularTaskFloat({Key? key}) : super(key: key);

  @override
  _YBDPopularTaskFloatState createState() => _YBDPopularTaskFloatState();
}

class _YBDPopularTaskFloatState extends State<YBDPopularTaskFloat> {
  bool _visible = false;
  YBDNewUserStatusRecord? _userEntity;
  int _remainSecond = 0; // 剩余秒数

  @override
  void initState() {
    super.initState();

    request();
  }
  void initStateZHf4royelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  request() async {
    // 获取相关数据
    var entity = await ApiHelper.queryNewUserStauts(context);
    logger.v("YBDPopularTaskFloat entity: ${entity?.toJson()}");
    if (entity?.record?.enabled ?? false) {
      _userEntity = entity?.record;
      _visible = entity?.record?.enabled ?? false;
      _remainSecond = (entity?.record?.remainingTime ?? 0) ~/ 1000;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: _visible,
      child: YBDDelayGestureDetector(
        onTap: () {
          // 点击事件
          logger.v('YBDPopularTaskFloat tap route: ${_userEntity?.route}');
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
            YBDEventName.CLICK_EVENT,
            location: YBDLocationName.HOME_POPULAR_PAGE,
            itemName: 'new_bie',
          ));
          if (_userEntity?.route?.isNotEmpty ?? false) {
            if (_userEntity?.route?.startsWith('http') ?? false) {
              YBDActivityInTrack().activityIn(YBDTAProps(location: YBDTAEventLocation.new_bie, url: _userEntity?.route));
            }
            YBDNavigatorHelper.openUrl(context, _userEntity?.route ?? '', type: WebEntranceType.Float);
          }
        },
        child: Column(
          children: [
            // 图片
            YBDGameWidgetUtil.networkImage(
              url: _userEntity?.resourceUrl ?? '',
              width: 120.dp720,
              height: 120.dp720,
            ),
            // 倒计时 有才显示
            if (_remainSecond > 0)
              YBDTaskFloatCountdown(
                remainTime: _remainSecond,
                finishCallback: () {
                  _visible = false;
                  setState(() {});
                },
              )
          ],
        ),
      ),
    );
  }
  void buildErMPboyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDTaskFloatCountdown extends StatefulWidget {
  final int remainTime; //剩余时间 格式： D:H:M
  final int damping; // 倒计时的衰减值默认10s
  final VoidCallback? finishCallback; // 完成回调
  final bool isEndTime; // remainTime是否为结束时间戳
  final double scale; // 缩放比例

  const YBDTaskFloatCountdown({
    Key? key,
    required this.remainTime,
    this.damping = 10,
    this.finishCallback,
    this.isEndTime = false,
    this.scale = 1.0,
  }) : super(key: key);

  @override
  _YBDTaskFloatCountdownState createState() => _YBDTaskFloatCountdownState();
}

class _YBDTaskFloatCountdownState extends State<YBDTaskFloatCountdown> {
  Timer? _timer;
  late int _countDown;

  @override
  void initState() {
    super.initState();
    startTimer();
  }
  void initState6XnaNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  startTimer() {
    if (widget.isEndTime) {
      // remainTime使用了UTC时间
      // utc:12 loca:20 offset:8
      // utc:12 local:10 offset:-2
      // 根据当前时间，获取utc时间 utc=local-offset
      /*
      * DateTime.now().toUtc().millisecondsSinceEpoch 获取的UTC时间戳最终还是会转化本地时间戳，
      * 即：DateTime.now().toUtc().millisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch
      * */
      int remain =
          widget.remainTime - (DateTime.now().millisecondsSinceEpoch - DateTime.now().timeZoneOffset.inMilliseconds);
      if (remain < 0) {
        _countDown = 0;
      } else {
        _countDown = Duration(milliseconds: remain).inSeconds;
      }
      logger.v('DateTime.now(): ${DateTime.now()}, ${DateTime.now().toUtc()},'
          '${DateTime.fromMillisecondsSinceEpoch(widget.remainTime, isUtc: true)},'
          '${DateTime.fromMillisecondsSinceEpoch(widget.remainTime)},'
          '${widget.remainTime}, ${DateTime.now().toUtc().millisecondsSinceEpoch},'
          '${DateTime.now().millisecondsSinceEpoch},${DateTime.now().isUtc},$remain,${formatTime()}');
      logger.v('DateTime.now().timeZoneOffset: ${DateTime.now().timeZoneOffset}');
    } else {
      _countDown = widget.remainTime;
    }

    if (_countDown < 1 || _countDown < widget.damping) {
      stopTimer();
      widget.finishCallback?.call();
      return;
    }

    if (_timer != null) stopTimer();

    _timer = Timer.periodic(Duration(seconds: widget.damping.abs()), (timer) {
      if (!mounted) {
        return;
      }
      _countDown -= widget.damping;
      setState(() {});

      if (_countDown < 1) {
        widget.finishCallback?.call();
      }
    });
  }

  stopTimer() {
    _timer?.cancel();
    _timer = null;
  }

  /// 格式化时间
  String formatTime() {
    int day = _countDown ~/ 86400; // 天数
    int hour = _countDown % 86400 ~/ 3600; // 小时
    int minute = _countDown % 86400 % 3600 ~/ 60; // 分钟

    String dayStr = '$day'; // 用不补零了
    String hourStr = hour < 10 ? '0$hour' : '$hour';
    String minuteStr = minute < 10 ? '0$minute' : '$minute';

    return '${dayStr}D:${hourStr}H:$minuteStr';
  }
  void formatTimeC7yOKoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    stopTimer();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant YBDTaskFloatCountdown oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.remainTime != oldWidget.remainTime) {
      startTimer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: widget.scale,
      child: Container(
        height: 30.dp720,
        padding: EdgeInsets.symmetric(horizontal: 10.dp720, vertical: 5.dp720),
        constraints: BoxConstraints(minWidth: 100.dp720),
        decoration: BoxDecoration(
          color: YBDHexColor('#BB54CC'),
          borderRadius: BorderRadius.circular(15.dp720),
        ),
        child: Text(
          formatTime(),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18.sp720,
            color: Colors.white,
            fontWeight: FontWeight.w600,
            shadows: [
              Shadow(
                offset: Offset(0.0, 2.dp720),
                blurRadius: 2.dp720,
                color: Colors.black.withOpacity(0.8),
              )
            ],
          ),
        ),
      ),
    );
  }
  void buildqCw37oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
