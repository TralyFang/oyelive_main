import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';
import '../../../widget/button/dialog_ybd_close_button.dart';
import '../../profile/my_profile/util/reward_ybd_data_util.dart';
import '../../room/room_ybd_helper.dart';
import '../bloc/daily_ybd_check_dialog_bloc.dart';

/// 新用户自动签到赠送奖励的弹框
class YBDDailyCheckNewUserDialog extends StatefulWidget {
  /// 随机选取的房间，点 Go 按钮跳转到该房间
  final YBDRoomInfo? roomInfo;

  YBDDailyCheckNewUserDialog({this.roomInfo});

  @override
  State<YBDDailyCheckNewUserDialog> createState() => _YBDDailyCheckNewUserDialogState();
}

class _YBDDailyCheckNewUserDialogState extends State<YBDDailyCheckNewUserDialog> {
  @override
  void initState() {
    super.initState();
    YBDAnalyticsUtil.logEvent(
      YBDAnalyticsEvent(
        YBDEventName.OPEN_PAGE,
        location: YBDLocationName.HOME_PAGE,
        itemName: 'YBDDailyCheckNewUserDialog',
      ),
    );
  }
  void initStateI9ItToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked new user daily check dialog bg");
      },
      child: Material(
        color: Colors.black.withOpacity(0.4),
        child: Container(
          width: ScreenUtil().screenWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  _bgContainer(),
                  Positioned(
                    // 对话框内容背景
                    top: ScreenUtil().setWidth(130),
                    child: _contentView(context),
                  ),
                  Positioned(
                    // 顶部图片
                    top: ScreenUtil().setWidth(0),
                    child: _dialogTitle(),
                  ),
                  Positioned(
                    // 关闭按钮
                    top: ScreenUtil().setWidth(140),
                    right: ScreenUtil().setWidth(15),
                    child: YBDDialogCloseButton(
                      width: ScreenUtil().setWidth(100),
                      height: ScreenUtil().setWidth(80),
                      onTap: () {
                        logger.v('dismiss dialog');
                        YBDCommonTrack().commonTrack(YBDTAProps(
                          location: YBDTAClickName.guide_wc_close,
                          module: YBDTAModule.guide,
                        ));
                        // 关闭弹框
                        context.read<YBDDailyCheckDialogBloc>().add(DailyCheckDialogEvent.Close);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildM6o6Eoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 对话框内容背景
  Widget _bgContainer() {
    return Container(
      width: ScreenUtil().setWidth(550),
      height: ScreenUtil().setWidth(740),
    );
  }
  void _bgContainerh1Hn3oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 弹框内容
  Widget _contentView(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      width: ScreenUtil().setWidth(560),
      // height: ScreenUtil().setWidth(770),
      child: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setWidth(148)),
          Container(
            // 文字标题
            height: ScreenUtil().setWidth(68),
            child: Text(
              '${translate('title_welcome')} !',
              style: TextStyle(
                color: Color(0xff008E82),
                fontSize: ScreenUtil().setSp(40),
              ),
            ),
          ),
          // 奖励内容
          _rewardRow(context),
          SizedBox(height: ScreenUtil().setWidth(7)),
          // 使用奖励
          _rewardInstruction(),
          SizedBox(height: ScreenUtil().setWidth(44)),
          // Go 按钮和底部空白区域
          _bottomPart(context),
        ],
      ),
    );
  }
  void _contentViewDJIv0oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 顶部图片
  Widget _dialogTitle() {
    return Container(
      height: ScreenUtil().setWidth(258),
      child: Image.asset(
        'assets/images/dc/dc_dialog_new_user_title.webp',
        fit: BoxFit.cover,
      ),
    );
  }

  /// 奖励内容
  Widget _rewardRow(BuildContext context) {
    return BlocBuilder<YBDDailyCheckDialogBloc, YBDDailyCheckDialogBlocState>(
      builder: (builderContext, state) {
        // 第一天签到的数据
        final rewardData = YBDRewardDataUtil.rewardData(
          context,
          dailyCheckTask: state.dailyCheckTasks!.first!,
          giftList: state.giftList,
          carList: state.carList,
        );

        return Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text(
                  translate('we_sent_you'),
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(32),
                    color: Colors.black,
                  ),
                ),
              ),
              Container(
                height: ScreenUtil().setWidth(116),
                child: rewardData.isLocalImg
                    ? YBDImage(
                        path: rewardData.img ?? '',
                        fit: BoxFit.contain,
                      )
                    : YBDNetworkImage(
                        imageUrl: rewardData.img ?? '',
                        fit: BoxFit.cover,
                      ),
              ),
              Container(
                child: Text(
                  '${rewardData.name ?? ''}',
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(32),
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
  void _rewardRowvRLb8oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 奖励说明
  Widget _rewardInstruction() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            translate('send_reward'),
            style: TextStyle(
              fontSize: ScreenUtil().setSp(30),
              color: Colors.black.withOpacity(0.6),
            ),
          ),
          Text(
            '${translate('room_now')}!',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(30),
              color: Colors.black.withOpacity(0.6),
            ),
          ),
        ],
      ),
    );
  }
  void _rewardInstructionoTUn7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Go 按钮和底部空白区域
  Widget _bottomPart(BuildContext context) {
    if (null != widget.roomInfo) {
      return Column(
        children: <Widget>[_goBtn(context), SizedBox(height: ScreenUtil().setWidth(45))],
      );
    } else {
      return Container();
    }
  }

  /// Go 按钮
  Widget _goBtn(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(350),
      height: ScreenUtil().setWidth(64),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(ScreenUtil().setWidth(64)),
        ),
        gradient: LinearGradient(
          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: YBDDelayGestureDetector(
        onTap: () {
          logger.v('clicked go button : ${widget.roomInfo?.id}');
          YBDCommonTrack().commonTrack(YBDTAProps(
            location: YBDTAClickName.guide_wc_go,
            module: YBDTAModule.guide,
          ));
          YBDRoomHelper.enterRoom(context, widget.roomInfo?.id, location: 'check_in_recommended_room');
          // 关闭弹框
          context.read<YBDDailyCheckDialogBloc>().add(DailyCheckDialogEvent.Close);
        },
        child: Center(
          child: Text(
            '${translate('go')}!',
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(28),
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
  void _goBtnsnABtoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
