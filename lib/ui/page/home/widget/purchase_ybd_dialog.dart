import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../widget/button/delay_ybd_gesture_detector.dart';
import '../../profile/my_profile/util/top_ybd_up_badges_alert_util.dart';
import '../../status/local_audio/blue_ybd_gradient_button.dart';

///   google充值成功弹框
class YBDPurchaseDialog extends StatelessWidget {
  final String? coins;

  YBDPurchaseDialog({this.coins});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black.withOpacity(0.4),
      child: Container(
        width: ScreenUtil().setWidth(500),
        height: ScreenUtil().setWidth(291),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          _contentView(context),
        ]),
      ),
    );
  }
  void build09fGxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 对话框内容
  Widget _contentView(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(500),
      height: ScreenUtil().setWidth(291),
      decoration: BoxDecoration(
        color: Colors.white,
        // gradient: LinearGradient(colors: [
        //   Colors.white,
        // ], begin: Alignment.centerLeft, end: Alignment.centerRight),
        borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(32)))),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text(
              'Top-up Amount',
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: ScreenUtil().setSp(30), color: Colors.black),
            ),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(29),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/images/topup/y_top_up_beans@2x.webp',
                  height: ScreenUtil().setWidth(28),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(8),
                ),
                Container(
                  child: Text(
                    coins!,
                    style:
                        TextStyle(fontWeight: FontWeight.w400, fontSize: ScreenUtil().setSp(30), color: Colors.black),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil().setWidth(40),
          ),
          _submitBtn(context),
        ],
      ),
    );
  }
  void _contentViewmZDxioyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 确认按钮
  Widget _submitBtn(BuildContext context) {
    return Container(
      child: YBDDelayGestureDetector(
        child: YBDBlueGradientButton(
          title: 'ok',
          height: 65,
          width: 300,
        ),
        onTap: () {
          Navigator.pop(context);
          YBDTopUpBadgesAlertUtil.changeGoogleAlertState(false);
        },
      ),
    );
  }
  void _submitBtndkLZeoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
