import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../../../common/util/date_ybd_util.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../entity/combo_ybd_rank_entity.dart';
import 'combo_ybd_receiver_avatar.dart';
import 'combo_ybd_sender_avatar.dart';

/// combo 列表 item
class YBDComboListItem extends StatelessWidget {
  /// item 数据
  final YBDComboRankRecordRank? itemData;
  final BuildContext context;

  YBDComboListItem(this.context, this.itemData);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.1),
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setWidth(30)),
          Row(
            children: <Widget>[
              SizedBox(width: ScreenUtil().setWidth(30)),
              Container(
                // combo 名称
                constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(160)),
                child: Text(
                  itemData!.giftName ?? '',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: ScreenUtil().setSp(24),
                  ),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(10)),
              Text(
                translate('combo'),
                style: TextStyle(
                  color: Color(0xffFFE731),
                  fontSize: ScreenUtil().setSp(24),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(10)),
              Text(
                'X',
                style: TextStyle(
                  color: Color(0xffFFE731),
                  fontSize: ScreenUtil().setSp(34),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(10)),
              Text(
                '${itemData!.combo ?? 0}',
                style: TextStyle(
                  color: Color(0xffFFE731),
                  fontSize: ScreenUtil().setSp(34),
                ),
              ),
              Expanded(child: SizedBox(width: 1)),
              Text(
                YBDDateUtil.dateWithFormat(itemData!.sendTime, yearFormat: 'MM/dd/yyyy HH:mm'),
                style: TextStyle(
                  color: Color(0xffE7E7E7),
                  fontSize: ScreenUtil().setSp(24),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(30)),
            ],
          ),
          SizedBox(height: ScreenUtil().setWidth(30)),
          Row(
            children: <Widget>[
              SizedBox(width: ScreenUtil().setWidth(15)),
              Column(
                children: <Widget>[
                  // 头像
                  YBDComboSenderAvatar(itemData!.sender),
                  SizedBox(height: ScreenUtil().setWidth(13)),
                  Container(
                    // 送礼者昵称
                    width: ScreenUtil().setWidth(240),
                    child: Text(
                      itemData!.sender?.nickname ?? '',
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: ScreenUtil().setSp(18),
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(child: SizedBox(width: 1)),
              // 礼物图片 sent to
              _giftItem(),
              Expanded(child: SizedBox(width: 1)),
              Column(
                children: <Widget>[
                  YBDComboReceiverAvatar(itemData!.receiver),
                  SizedBox(height: ScreenUtil().setWidth(13)),
                  Container(
                    // 送礼者昵称
                    width: ScreenUtil().setWidth(230),
                    child: Text(
                      // 接收者昵称
                      itemData!.receiver?.nickname ?? '',
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: ScreenUtil().setSp(18),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(width: ScreenUtil().setWidth(15)),
            ],
          ),
        ],
      ),
    );
  }
  void buildYFRFpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 礼物 item
  Widget _giftItem() {
    // sent to
    final text = Text(
      translate('sent_to'),
      style: TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(22),
      ),
    );

    if (null != itemData?.giftImg && itemData!.giftImg!.isEmpty) {
      return text;
    }

    return Container(
      // color: Colors.green,
      height: ScreenUtil().setWidth(160),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(90),
            height: ScreenUtil().setWidth(90),
            child: YBDNetworkImage(
              imageUrl: YBDImageUtil.gift(context, itemData!.giftImg, "B"),
              fit: BoxFit.contain,
            ),
          ),
          SizedBox(height: ScreenUtil().setWidth(10)),
          // sent to
          text,
        ],
      ),
    );
  }
  void _giftItemOYBlDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
