import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../widget/button/dialog_ybd_close_button.dart';

/// 点 combo 页面右上角弹出的描述对话框
class YBDComboDescriptionDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the combo dialog bg");
        Navigator.pop(context);
      },
      child: Material(
        color: Colors.black.withOpacity(0.4),
        child: Container(
          width: ScreenUtil().screenWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  _contentView(context),
                  Positioned(
                    // 关闭按钮
                    right: 0,
                    top: 0,
                    child: YBDDialogCloseButton(
                      width: ScreenUtil().setWidth(100),
                      height: ScreenUtil().setWidth(80),
                      onTap: () {
                        logger.v('dismiss dialog');
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildFsKZooyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 对话框内容
  Widget _contentView(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(600),
      height: ScreenUtil().setWidth(860),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(13)),
        gradient: LinearGradient(
          colors: [Color(0xff47CDCC), Color(0xff3E76CC)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            // 顶部标题和关闭按钮
            _topRow(context),
            // 第一段描述
            _descriptionText(),
            // 第二段描述
            _howToSendText(),
            // 第三段描述
            _howToAchieveText(),
          ],
        ),
      ),
    );
  }
  void _contentViewD3GOEoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 顶部标题
  Widget _topRow(BuildContext context) {
    return Container(
      width: double.infinity,
      height: ScreenUtil().setWidth(98),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            // 标题
            top: ScreenUtil().setWidth(20),
            child: Container(
              width: ScreenUtil().setWidth(524),
              child: Image.asset(
                'assets/images/combo/combo_dialog_title.png',
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }
  void _topRowHQe0uoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 第一段描述
  Widget _descriptionText() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(40),
        vertical: ScreenUtil().setWidth(20),
      ),
      child: Text(
        translate('combo_desc'),
        style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
      ),
    );
  }
  void _descriptionTextYrFgPoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 第二段描述
  Widget _howToSendText() {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(20),
        vertical: ScreenUtil().setWidth(4),
      ),
      child: Container(
        padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          color: Colors.white.withOpacity(0.1),
        ),
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Text(
                translate('combo_desc_1'),
                textAlign: TextAlign.left,
                style:
                    TextStyle(color: Color(0xffFFE288), fontSize: ScreenUtil().setSp(24), fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(height: ScreenUtil().setWidth(20)),
            Text(
              translate('combo_desc_2'),
              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
            ),
          ],
        ),
      ),
    );
  }
  void _howToSendTextcNOEkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 第三段描述
  Widget _howToAchieveText() {
    return Padding(
      padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
      child: Container(
        padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          color: Colors.white.withOpacity(0.1),
        ),
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Text(
                translate('combo_desc_3'),
                textAlign: TextAlign.left,
                style:
                    TextStyle(color: Color(0xffFFE288), fontSize: ScreenUtil().setSp(24), fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(height: ScreenUtil().setWidth(20)),
            Text(
              translate('combo_desc_4'),
              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
            ),
            Text(
              translate('combo_desc_5'),
              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
            ),
          ],
        ),
      ),
    );
  }
  void _howToAchieveTextgWfSBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
