import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../widget/level_ybd_tag.dart';
import '../../../widget/round_ybd_avatar.dart';

/// combo 发送者的头像
class YBDComboSenderAvatar extends StatelessWidget {
  /// 送礼者信息
  final YBDRoomInfo? userInfo;

  YBDComboSenderAvatar(this.userInfo);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(180),
      height: ScreenUtil().setWidth(140),
      child: Stack(
        children: <Widget>[
          Center(
            child: Container(
              width: ScreenUtil().setWidth(106),
              height: ScreenUtil().setWidth(106),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(60))),
                border: Border.all(color: Color(0xffFFCA86), width: ScreenUtil().setWidth(3)),
              ),
              child: YBDRoundAvatar(
                // 用户头像
                userInfo?.headimg ?? '',
                sex: userInfo?.sex,
                needNavigation: true,
                lableSrc: '',
                userId: userInfo?.id,
                avatarWidth: 106,
                scene: "B",
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().setWidth(0),
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(180),
              child: Image.asset(
                'assets/images/icon_crown.webp',
                width: ScreenUtil().setWidth(53),
                height: ScreenUtil().setWidth(25),
              ),
            ),
          ),
          Positioned(
            bottom: ScreenUtil().setWidth(10),
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(180),
              child: YBDLevelTag(
                userInfo?.level ?? 1,
                scale: 0.6,
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().setWidth(30),
            left: ScreenUtil().setWidth(10),
            child: Image.asset(
              'assets/images/combo/combo_mini_star.png',
              width: ScreenUtil().setWidth(16),
            ),
          ),
          Positioned(
            top: ScreenUtil().setWidth(30),
            left: ScreenUtil().setWidth(150),
            child: Image.asset(
              'assets/images/combo/combo_big_star.png',
              width: ScreenUtil().setWidth(28),
            ),
          ),
          Positioned(
            top: ScreenUtil().setWidth(100),
            left: ScreenUtil().setWidth(16),
            child: Image.asset(
              'assets/images/combo/combo_mid_star.png',
              width: ScreenUtil().setWidth(16),
            ),
          ),
        ],
      ),
    );
  }
  void buildoEXxEoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
