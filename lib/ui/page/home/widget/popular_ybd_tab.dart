import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oye_tool_package/widget/draggable_float_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/home/bloc/king_ybd_kong_bloc.dart';
import 'package:oyelive_main/ui/page/home/widget/popular_ybd_task_float.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/remote_ybd_config_service.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/room_ybd_list_item.dart';
import '../../room/room_ybd_helper.dart';
import '../bloc/advertise_ybd_bloc.dart';
import '../bloc/home_ybd_page_bloc.dart';
import '../bloc/popular_ybd_bloc.dart';
import '../util/home_ybd_util.dart';
import 'home_ybd_grid_item.dart';
import 'home_ybd_list_header.dart';
import 'popular_ybd_banner.dart';
import 'popular_ybd_board.dart';

/// Popular tab 的内容，包括广告栏和列表
class YBDPopularTab extends StatefulWidget {
  /// 和 tab 公用的滚动控制器
  /// 双击 tab 滚动到顶部
  final ScrollController? scrollController;

  YBDPopularTab(this.scrollController);

  @override
  YBDPopularTabState createState() => new YBDPopularTabState();
}

class YBDPopularTabState extends State<YBDPopularTab> with AutomaticKeepAliveClientMixin {
  /// 下拉刷新控制器
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  /// 列表样式标志位
  bool _isGrid = false;

  @override
  void initState() {
    super.initState();

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE, location: YBDLocationName.HOME_POPULAR_PAGE));

    // 在 bloc 里更新刷新 header 和 footer
    context.read<YBDPopularBloc>().refreshController = _refreshController;
    context.read<YBDAdvertiseBloc>().add(AdvertiseEvent.Refresh);
    logger.v('init state pop:');
    context.read<YBDKingKongBloc>().add(KingKongEvent.Refresh);

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      popularListStyle();
    });
  }
  void initStatezv7NBoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  bool get wantKeepAlive => true;

  final containerKey = GlobalKey();

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }
  void disposehG9z7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      key: containerKey,
      child: Stack(
        children: [
          // 列表
          buildSmartRefresher(context),
          // 任务浮动入口
          DraggableFloatWidget(
            right: 10.dp720,
            bottom: 60.dp720,
            containerKey: containerKey,
            child: YBDPopularTaskFloat(),
          ),
        ],
      ),
    );
  }
  void buildhqq3Ooyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  SmartRefresher buildSmartRefresher(BuildContext context) {
    return SmartRefresher(
          controller: _refreshController,
          header: YBDActivitySkinRoot().curAct().activityRefreshHeader(),
          footer: YBDActivitySkinRoot().curAct().activityRefreshFooter(),
          enablePullDown: true,
          enablePullUp: true,
          onRefresh: () {
            logger.v('refresh popular page');
            context.read<YBDPopularBloc>().add(PopularEvent.Refresh);
            context.read<YBDAdvertiseBloc>().add(AdvertiseEvent.Refresh);
            context.read<YBDKingKongBloc>().add(KingKongEvent.Refresh);
          },
          onLoading: () {
            logger.v('on loading popular page');
            context.read<YBDPopularBloc>().add(PopularEvent.NextPage);
          },
          child: StoreBuilder<YBDAppState>(
            builder: (context, store) {
              return contentView();
            },
          ),
        );
  }

  /// 页面内容
  Widget contentView() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          YBDPopularBanner(),
          BlocBuilder<YBDKingKongBloc, YBDKingKongState>(
            builder: (context, state) {
              logger.v('init state pop:1');
              return YBDPopularBoard(list: state.records);
            },
          ),
          SizedBox(height: ScreenUtil().setWidth(20)),
          YBDHomeListHeader(
            () {
              logger.v('changed list style callback');
              setState(() {
                _isGrid = !_isGrid;
              });
              YBDSPUtil.save(Const.SP_HOME_POPULAR_GRID, _isGrid); // 记住用户设置的Popular显示样式
              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
                  location: YBDLocationName.HOME_PAGE,
                  itemName: 'popular_list_style',
                  value: _isGrid ? 'grid' : 'list')); // value: 切换后的值 list-列表； grid-栅格
            },
            title: translate('popular_rooms'),
          ),
          BlocBuilder<YBDPopularBloc, YBDPopularBlocState>(builder: (context, state) {
            if (_isGrid) {
              if (state.isInit) {
                logger.v('show grid view init state');
                return Container(
                  height: ScreenUtil().setWidth(600),
                  child: YBDLoadingCircle(),
                );
              } else {
                logger.v('show grid view');
                return popularGridView(state.rooms, YBDHomeUtil.weeklyStarRooms(context));
              }
            } else {
              if (state.isInit) {
                logger.v('show list view init state');
                return Container(
                  height: ScreenUtil().setWidth(600),
                  child: YBDLoadingCircle(),
                );
              } else {
                return popularListView(state.rooms, YBDHomeUtil.weeklyStarRooms(context));
              }
            }
          }),
        ],
      ),
    );
  }
  void contentViewJf2Ovoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 线性列表
  Widget popularListView(List<YBDRoomInfo?>? rooms, List<String> superRooms) {
    if (null != rooms && rooms.isNotEmpty) {
//      logger.v('popular list amount : ${rooms.length}');
      return ListView.separated(
        padding: EdgeInsets.all(0),
        controller: widget.scrollController,
        itemBuilder: (_, index) {
          return Material(
            color: Colors.transparent,
            child: YBDDelayGestureDetector(
              onTap: () {
                logger.v('clicked popular list view item');
                YBDRoomHelper.enterRoom(
                  context,
                  rooms[index]!.id,
                  roomList: rooms,
                  tag: rooms[index]!.roomCategoryIndex,
                  forceEnter: true,
                  location: YBDLocationName.HOME_POPULAR_PAGE,
                );
              },
              child: YBDRoomListItem(
                rooms[index],
                YBDActivitySkinRoot().curAct().activityTextColor(),
                superRooms: superRooms,
                superStars: context.read<YBDHomePageBloc>().state.starList,
              ),
            ),
          );
        },
        separatorBuilder: (_, index) => Container(
          margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
          height: ScreenUtil().setWidth(1),
          color: YBDActivitySkinRoot().curAct().activitySeparatorColor(),
        ),
        shrinkWrap: true,
        itemCount: rooms.length,
      );
    } else {
      logger.v('popular list is empty, show empty view');
      // TODO: 显示缺省图
      return Container();
    }
  }
  void popularListViewCV4pToyelive(List<YBDRoomInfo?>? rooms, List<String> superRooms) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 网格列表
  Widget popularGridView(List<YBDRoomInfo?>? rooms, List<String> superRooms) {
    if (null != rooms && rooms.isNotEmpty) {
      logger.v('popular grid amount : ${rooms.length}');
      return GridView.count(
        padding: EdgeInsets.only(
          top: ScreenUtil().setWidth(10),
          left: ScreenUtil().setWidth(24),
          right: ScreenUtil().setWidth(24),
        ),
        crossAxisCount: 2,
        controller: widget.scrollController,
        shrinkWrap: true,
        crossAxisSpacing: ScreenUtil().setWidth(10),
        mainAxisSpacing: ScreenUtil().setWidth(10),
        children: List.generate(rooms.length, (index) {
          // 奇数网格项
          // bool isOdd = (((index + 1) % 2) != 0);
          return Container(
//            margin: EdgeInsets.fromLTRB(
//              isOdd ? ScreenUtil().setWidth(10) : ScreenUtil().setWidth(5),
//              0,
//              isOdd ? ScreenUtil().setWidth(5) : ScreenUtil().setWidth(10),
//              ScreenUtil().setWidth(10),
//            ),
            child: YBDHomeGridItem(rooms[index], superRooms, context.read<YBDHomePageBloc>().state.starList, (info) {
              logger.v('popular grid item callback');
              YBDRoomHelper.enterRoom(
                context,
                rooms[index]!.id,
                roomList: rooms,
                tag: rooms[index]!.roomCategoryIndex,
                forceEnter: true,
                location: YBDLocationName.HOME_POPULAR_PAGE,
              );
            }),
          );
        }),
      );
    } else {
      logger.v('popular grid is empty, show empty view');
      // TODO: 显示缺省图
      return Container();
    }
  }
  void popularGridViewwKM4Joyelive(List<YBDRoomInfo?>? rooms, List<String> superRooms) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  popularListStyle() async {
    /// 上次用户设置的Popular显示样式 true: Grid  false: List
    /// Firebase配置项控制默认展现样式:	popular_list_default_style 值：grid, list
    bool? isGrid = await YBDSPUtil.getBool(Const.SP_HOME_POPULAR_GRID);
    logger.v(" --- query sp_home_popular_grid : $isGrid");
    if (isGrid == null) {
      YBDRemoteConfigService? r = await YBDRemoteConfigService.getInstance();
      String defaultStyle = r?.getConfig()?.getString("popular_list_default_style") ?? '';
      logger.v(" --- query firebase remote config , popular_list_default_style : $defaultStyle");
      isGrid = defaultStyle == 'grid';
      YBDSPUtil.save(Const.SP_HOME_POPULAR_GRID, isGrid);
    }

    if (mounted && isGrid) {
      _isGrid = isGrid;
    }
  }
}
