import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 房间周榜标签
class YBDSuperStarItem extends StatelessWidget {
  final int? roomId;
  final List<int?>? superStars;

  YBDSuperStarItem({this.roomId, this.superStars});

  @override
  Widget build(BuildContext context) {
    if (roomId == null || superStars == null || !superStars!.contains(roomId)) {
      return Container();
    }

    int _i = superStars!.indexOf(roomId);
    if (_i >= 0 && _i <= 2) {
      return Image.asset(
        'assets/images/star_super.webp',
        width: ScreenUtil().setWidth(120),
        height: ScreenUtil().setWidth(30),
      );
    } else if (_i >= 3 && _i <= 5) {
      return Image.asset(
        'assets/images/star_senior.png',
        width: ScreenUtil().setWidth(120),
        height: ScreenUtil().setWidth(30),
      );
    } else if (_i >= 6 && _i <= 8) {
      return Image.asset(
        'assets/images/star_junior.png',
        width: ScreenUtil().setWidth(120),
        height: ScreenUtil().setWidth(30),
      );
    } else {
      return Container();
    }
  }
  void buildnDk8aoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
