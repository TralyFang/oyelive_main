import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_skin.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import 'related_ybd_tab_content.dart';

/// 首页 Related 标签
class YBDRelatedTab extends StatefulWidget {
  /// 双击标签滚到顶部的滚动控制器
  /// 暂时没用到
  final ScrollController? scrollController;

  YBDRelatedTab({this.scrollController});

  @override
  YBDRelatedTabState createState() => YBDRelatedTabState();
}

class YBDRelatedTabState extends State<YBDRelatedTab> with SingleTickerProviderStateMixin {
  /// tab 控制器
  TabController? _tabController;

  /// 选中的标签索引
  int _selectedTabIndex = 0;

  @override
  void initState() {
    super.initState();

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.OPEN_PAGE, location: YBDLocationName.HOME_RELATED_PAGE));

    _tabController = TabController(initialIndex: 0, length: 2, vsync: this);
    _tabController!.addListener(() {
      _selectedTabIndex = _tabController!.index;
      setState(() {});
    });

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      Future.delayed(Duration(seconds: 1), () {
        setState(() {
          _selectedTabIndex = 0;
          _tabController!.index = _selectedTabIndex;
        });
      });
    });
  }
  void initState6xvEIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _tabController!.dispose();
    super.dispose();
  }
  void disposelG4ugoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    // return YBDExploreRankTab();
    return Container(
      child: Column(
        children: <Widget>[
//          Stack(
//            children: [
//              Positioned(
//                top: ScreenUtil().setWidth(6),
//                child: Container(
//                  width: ScreenUtil().screenWidth,
//                  height: ScreenUtil().setWidth(70),
//                  decoration: BoxDecoration(color: Colors.white.withOpacity(0.1)),
//                ),
//              ),
//
//            ],
//          ),
          _tabBar(),
          Expanded(
            child: _tabBarView(),
          ),
        ],
      ),
    );
  }
  void buildqXyPdoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _tabBarView() {
    return TabBarView(
      controller: _tabController,
      children: <Widget>[
        YBDRelatedTabContent(type: RelatedContentType.Recently),
        YBDRelatedTabContent(type: RelatedContentType.Following),
      ],
    );
  }
  void _tabBarView6gc6Moyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _tabBar() {
    return Container(
      width: ScreenUtil().setWidth(400),
      child: TabBar(
//                indicator: BoxDecoration(
//                  image: DecorationImage(
//                    // 选中时的 tab 背景图
//                    fit: BoxFit.contain,
//                    image: AssetImage('assets/images/explore_tab_bg_img.webp'),
//                  ),
//                ),

        indicator: BoxDecoration(),
        labelColor: Colors.white,
        unselectedLabelColor: Colors.white,
        controller: _tabController,
        labelStyle: TextStyle(
          // 选中样式
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(30),
        ),
        unselectedLabelStyle: TextStyle(
          // 非选中样式
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(28),
        ),
        tabs: [
          Column(
            children: <Widget>[
              Container(
                height: ScreenUtil().setWidth(60),
                child: Tab(
                  child: Text(
                    translate('recently'),
                    style: TextStyle(
                        color: _selectedTabIndex == 0
                            ? YBDActivitySkinRoot().curAct().activityTextColor()
                            : YBDActivitySkinRoot().curAct().activityTextColor().withOpacity(0.7),
                        fontSize: ScreenUtil().setSp(28)),
                  ),
                ),
              ),
              if (_selectedTabIndex == 0)
                Container(
                  width: ScreenUtil().setWidth(40),
                  height: ScreenUtil().setWidth(4),
                  decoration: BoxDecoration(
                      color: Color(0xff63FFDF),
                      borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(4)))),
                )
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                height: ScreenUtil().setWidth(60),
                child: Tab(
                  child: Text(
                    translate('following'),
                    style: TextStyle(
                        color: _selectedTabIndex == 1
                            ? YBDActivitySkinRoot().curAct().activityTextColor()
                            : YBDActivitySkinRoot().curAct().activityTextColor().withOpacity(0.7),
                        fontSize: ScreenUtil().setSp(28)),
                  ),
                ),
              ),
              if (_selectedTabIndex == 1)
                Container(
                  width: ScreenUtil().setWidth(40),
                  height: ScreenUtil().setWidth(4),
                  decoration: BoxDecoration(
                      color: Color(0xff63FFDF),
                      borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(4)))),
                )
            ],
          ),
        ],
      ),
    );

    return Container(
      child: TabBar(
        indicator: BoxDecoration(
          image: DecorationImage(
            // 选中时的 tab 背景图
            fit: BoxFit.contain,
            image: AssetImage('assets/images/explore_tab_bg_img.webp'),
          ),
        ),
        labelColor: Colors.white,
        unselectedLabelColor: Colors.white,
        controller: _tabController,
        labelStyle: TextStyle(
            // 选中样式
            fontWeight: FontWeight.w400,
            height: 1,
            fontSize: ScreenUtil().setSp(30)),
        unselectedLabelStyle: TextStyle(
            // 非选中样式
            fontWeight: FontWeight.w400,
            height: 1,
            fontSize: ScreenUtil().setSp(28)),
        tabs: [
          Container(
            height: ScreenUtil().setWidth(88),
            child: Tab(
              child: Text(
                translate('recently'),
                style: TextStyle(color: _selectedTabIndex == 0 ? Colors.white : Theme.of(context).primaryColor),
              ),
            ),
          ),
          Container(
            height: ScreenUtil().setWidth(88),
            child: Tab(
              child: Text(
                translate('following'),
                style: TextStyle(color: _selectedTabIndex == 1 ? Colors.white : Theme.of(context).primaryColor),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void _tabBarPwVfloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
