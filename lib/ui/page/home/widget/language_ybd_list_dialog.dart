import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/util/intl_ybd_lang_util.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';
import '../../../../redux/app_ybd_state.dart';
import '../../../../redux/locale_ybd_redux.dart';

/// 切换语言的列表对话框
class YBDLanguageListDialog extends StatefulWidget {
  /// 当前语言
  final String locale;

  YBDLanguageListDialog(this.locale);

  @override
  YBDLanguageListDialogState createState() => YBDLanguageListDialogState();
}

class YBDLanguageListDialogState extends State<YBDLanguageListDialog> {
  final List<String> _languages = [
    translate('language.en'),
    translate('language.urdu'),
    translate('language.hindi'),
    translate('language.arabic'),
    translate('language.turkish'),
    translate('language.indonesia')
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v('clicked change language dialog bg');
        Navigator.pop(context);
      },
      child: Material(
        color: Colors.transparent,
        child: Container(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            contentView(),
          ]),
        ),
      ),
    );
  }
  void build5zdhooyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 对话框内容
  Widget contentView() {
    // 容器宽度
    double width = ScreenUtil().setWidth(600);

    // 容器高度
    double outHeight = ScreenUtil().setWidth(750);

    // 头部背景高度
    double headHeight = ScreenUtil().setWidth(140);

    // 白色背景高度
    double innerHeight = outHeight - headHeight / 2;

    // 对话框头部背景图
    String imgStr = 'assets/images/profile/profile_input_dialog_bg.png';

    return Container(
      width: width,
      height: outHeight,
      child: Stack(
        children: <Widget>[
          Positioned(
            // 白色背景
            top: headHeight / 2,
            child: Container(
              width: width,
              height: innerHeight,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(16)))),
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setWidth(80)),
                  languageRow(_languages[0], {'language': YBDIntlLangUtil.en}),
                  dividerView(),
                  languageRow(_languages[1], {'language': YBDIntlLangUtil.ur}),
                  dividerView(),
                  languageRow(_languages[2], {'language': YBDIntlLangUtil.hi}),
                  dividerView(),
                  languageRow(_languages[3], {'language': YBDIntlLangUtil.ar}),
                  dividerView(),
                  languageRow(_languages[4], {'language': YBDIntlLangUtil.tr}),
                  dividerView(),
                  languageRow(_languages[5], {'language': YBDIntlLangUtil.id}, isBottom: true),
                ],
              ),
            ),
          ),
          // 头部背景
          Positioned(
            top: ScreenUtil().setWidth(0),
            left: ScreenUtil().setWidth(53),
            child: Image(
              image: AssetImage(imgStr),
              fit: BoxFit.cover,
              width: ScreenUtil().setWidth(520),
              height: headHeight,
            ),
          ),
        ],
      ),
    );
  }
  void contentViewGzGEtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 分割线
  Widget dividerView() {
    return Divider(
      color: Colors.black.withOpacity(0.2),
      height: ScreenUtil().setWidth(1),
      thickness: ScreenUtil().setWidth(1),
    );
  }
  void dividerViewZyFhFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 语言栏
  Widget languageRow(String title, Map<String, dynamic> params, {bool isBottom = false}) {
    // 是否为当前语言
    bool isCurrentLang = params['language'] == widget.locale;

    // 底部一栏加点击圆角效果
    num radius = isBottom ? 16 : 0;

    return Material(
      color: Colors.transparent,
      child: InkWell(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular((ScreenUtil().setWidth(radius))),
          bottomRight: Radius.circular((ScreenUtil().setWidth(radius))),
        ),
        onTap: () async {
          logger.v('change language to $title');

          // await changeLocale(context, localeFromString(params['language']).countryCode);
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
              location: YBDLocationName.SETTING_PAGE, itemName: params['language'] ?? 'en'));
          Store<YBDAppState> store = StoreProvider.of<YBDAppState>(context);

          store.dispatch(YBDUpdateLocaleAction(localeFromString(params['language'])));
          logger.v('locale debug --- save sp locale: ${params['language']}');
          YBDSPUtil.save(Const.SP_LOCALE, params['language']);
          // var localizationDelegate = LocalizedApp.of(context).delegate;
          Get.updateLocale(localeFromString(params['language']));
          Navigator.pop(context);
        },
        child: Container(
          height: ScreenUtil().setWidth(99),
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                color: isCurrentLang ? Colors.black : Colors.blue,
                fontSize: ScreenUtil().setSp(28),
              ),
            ),
          ),
        ),
      ),
    );
  }
  void languageRowa6zLgoyelive(String title, Map<String, dynamic> params, {bool isBottom = false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
