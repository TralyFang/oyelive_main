import 'package:flutter_redux/flutter_redux.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';

import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../widget/button/delay_ybd_gesture_detector.dart';
import '../../profile/my_profile/util/reward_ybd_data_util.dart';
import '../bloc/daily_ybd_check_dialog_bloc.dart';
import 'daily_ybd_check_dialog_big_item.dart';
import 'daily_ybd_check_dialog_item.dart';

/// 首页每日任务弹框
class YBDDailyCheckDialog extends StatefulWidget {
  @override
  YBDDailyCheckDialogState createState() => new YBDDailyCheckDialogState();

  YBDDailyCheckDialog();
}

class YBDDailyCheckDialogState extends State<YBDDailyCheckDialog> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the daily check dialog bg");
      },
      child: Material(
        color: Colors.black.withOpacity(0.4),
        child: Container(
          width: ScreenUtil().screenWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // 弹框居中
              SizedBox(height: ScreenUtil().setWidth(40)),
              GestureDetector(
                onTap: () {
                  // 避免点白色背景隐藏弹框
                },
                child: Stack(alignment: Alignment.center, children: <Widget>[
                  _bgContainer(),
                  Positioned(
                    // 对话框内容背景
                    top: ScreenUtil().setWidth(70),
                    child: _contentView(),
                  ),
                  Positioned(
                    // 顶部图片
                    top: ScreenUtil().setWidth(-14),
                    child: _dialogTitleImg(),
                  ),
                  Positioned(
                    // 关闭按钮
                    top: ScreenUtil().setWidth(76),
                    right: ScreenUtil().setWidth(10),
                    child: _closeBtn(),
                  ),
                ]),
              )
            ],
          ),
        ),
      ),
    );
  }
  void buildIukIkoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 对话框内容背景
  Widget _bgContainer() {
    return Container(
      width: ScreenUtil().setWidth(570),
      height: ScreenUtil().setWidth(800),
    );
  }
  void _bgContainerycSBAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 顶部图片
  Widget _dialogTitleImg() {
    return Container(
      height: ScreenUtil().setWidth(160),
      child: Image.asset(
        'assets/images/dc/dc_dialog_title.png',
        fit: BoxFit.cover,
      ),
    );
  }
  void _dialogTitleImgj8GA5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关闭按钮
  Widget _closeBtn() {
    return GestureDetector(
      onTap: () {
        logger.v('dismiss dialog');
        // 关闭弹框
        context.read<YBDDailyCheckDialogBloc>().add(DailyCheckDialogEvent.Close);
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.HOME_POPULAR_PAGE,
          itemName: YBDItemName.CHECKIN_POPUP_CLOSE,
        ));
      },
      child: Container(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              // 增加点击区域
              decoration: BoxDecoration(),
              width: ScreenUtil().setWidth(60),
              height: ScreenUtil().setWidth(60),
            ),
            Container(
              // 关闭按钮
              width: ScreenUtil().setWidth(48),
              height: ScreenUtil().setWidth(48),
              child: Image.asset(
                'assets/images/dc/daily_check_close_btn.png',
                color: Colors.grey,
                fit: BoxFit.cover,
              ),
            ),
          ],
        ),
      ),
    );
  }
  void _closeBtn0DyY4oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 弹框内容
  Widget _contentView() {
    return BlocBuilder<YBDDailyCheckDialogBloc, YBDDailyCheckDialogBlocState>(
      builder: (builderContext, state) {
        return Container(
          padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
          ),
          width: ScreenUtil().setWidth(560),
          // height: ScreenUtil().setWidth(770),
          child: Column(
            children: <Widget>[
              SizedBox(height: ScreenUtil().setWidth(87)),
              Container(
                height: ScreenUtil().setWidth(25),
                child: Image.asset(
                  'assets/images/dc/dc_dialog_title_text.png',
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(30)),
              // 签到成功的奖励内容或签到状态的列表内容
              state.success ? _rewardContent(state) : _checkInItemsContent(state),
            ],
          ),
        );
      },
    );
  }
  void _contentViewpv1Fooyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// Check In 按钮
  Widget _checkInBtn(YBDDailyCheckDialogBlocState state) {
    var decoration = BoxDecoration(
      borderRadius: BorderRadius.all(
        Radius.circular(ScreenUtil().setWidth(64)),
      ),
      gradient: LinearGradient(
        colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );

    // 签到中显示灰色背景
    if (state.isChecking) {
      decoration = BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(ScreenUtil().setWidth(64)),
        ),
        color: Colors.grey,
      );
    }

    return StoreBuilder<YBDAppState>(builder: (context, store) {
      bool hasExtra =
          store.state.bean?.vipDailyCheckRewardBeans != null && store.state.bean?.vipDailyCheckRewardBeans != 0;
      return Container(
        width: ScreenUtil().setWidth(hasExtra ? 434 : 350),
        height: ScreenUtil().setWidth(64),
        decoration: decoration,
        child: YBDDelayGestureDetector(
          onTap: () {
            if (state.isChecking) {
              logger.v('check in ing');
            } else {
              logger.v('clicked check-in button');
              // 签到
              context.read<YBDDailyCheckDialogBloc>().add(DailyCheckDialogEvent.CheckIn);
              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
                YBDEventName.CLICK_EVENT,
                location: YBDLocationName.HOME_POPULAR_PAGE,
                itemName: YBDItemName.CHECKIN_POPUP_CHECKIN,
              ));
            }
          },
          child: Center(
            child: Text.rich(
              TextSpan(
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: ScreenUtil().setSp(28),
                    fontWeight: FontWeight.w600,
                  ),
                  children: [
                    TextSpan(text: translate('check_in')),
                    if (hasExtra) ...[
                      TextSpan(text: " to receive "),
                      WidgetSpan(
                          child: Padding(
                        padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(2)),
                        child: Image.asset(
                          "assets/images/topup/y_top_up_beans@2x.webp",
                          width: ScreenUtil().setWidth(24),
                        ),
                      )),
                      TextSpan(
                          text: " " + (store.state.bean?.vipDailyCheckRewardBeans?.toString() ?? ''),
                          style: TextStyle(color: Color(0xffFFE951))),
                    ]
                  ]),
            ),
          ),
        ),
      );
    });
  }
  void _checkInBtnS29dsoyelive(YBDDailyCheckDialogBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 签到状态列表内容
  Widget _checkInItemsContent(YBDDailyCheckDialogBlocState state) {
    return Column(
      children: <Widget>[
        Text(
          translate('check_in_7_days'),
          style: TextStyle(
            fontSize: ScreenUtil().setSp(24),
            color: Colors.black,
          ),
        ),
        SizedBox(height: ScreenUtil().setWidth(50)),
        // 签到成功的奖励内容或签到状态的 7 个 item
        _items(state),
        SizedBox(height: ScreenUtil().setWidth(60)),
        // Check In 按钮
        _checkInBtn(state),
        SizedBox(height: ScreenUtil().setWidth(28)),

        moreReward(),
        SizedBox(height: ScreenUtil().setWidth(30)),
      ],
    );
  }
  void _checkInItemsContentnBwyEoyelive(YBDDailyCheckDialogBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget moreReward({isBold: false}) {
    return GestureDetector(
      onTap: () {
        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.daily_task);
      },
      child: Text(
        translate("more_reward"),
        style: isBold
            ? TextStyle(color: Color(0xff008E82), fontSize: ScreenUtil().setSp(26), fontWeight: FontWeight.w500)
            : TextStyle(fontSize: ScreenUtil().setSp(20), color: Color(0xff6F6F6F)),
      ),
    );
  }

  /// 7 个 item
  Widget _items(YBDDailyCheckDialogBlocState state) {
    return Row(
      children: <Widget>[
        Container(
          width: ScreenUtil().setWidth(364),
          padding: EdgeInsets.only(left: ScreenUtil().setWidth(18)),
          child: Column(
            children: <Widget>[
              Row(
                children: _itemList(state, 0, 2),
              ),
              SizedBox(height: ScreenUtil().setWidth(5)),
              Row(
                children: _itemList(state, 3, 5),
              ),
            ],
          ),
        ),
        YBDDailyCheckDialogBigItem(
          dailyCheckTask: state.dailyCheckTasks![6],
          dayName: '7',
          giftList: state.giftList,
          carList: state.carList,
        ),
      ],
    );
  }
  void _itemsSe6Wjoyelive(YBDDailyCheckDialogBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item 数组
  List<Widget> _itemList(YBDDailyCheckDialogBlocState state, int start, int end) {
    List<Widget> items = [];

    for (int i = start; i <= end; i++) {
      final item = YBDDailyCheckDialogItem(
        dailyCheckTask: state.dailyCheckTasks![i],
        dayName: '${i + 1}',
        giftList: state.giftList,
        carList: state.carList,
      );
      items.add(item);

      if (i < end) {
        // 两个 item 的间隔
        items.add(SizedBox(width: ScreenUtil().setWidth(5)));
      }
    }

    return items;
  }

  /// 签到成功的奖励内容
  Widget _rewardContent(YBDDailyCheckDialogBlocState state) {
    // 当前签到成功的数据
    final rewardData = YBDRewardDataUtil.currentDayRewardData(
      context,
      dailyCheckTasks: state.dailyCheckTasks,
      giftList: state.giftList,
      carList: state.carList,
    )!;

    return Column(
      children: <Widget>[
        Text(
          '${translate('you_have_checked')} ${rewardData.index + 1} ${translate('days')}',
          style: TextStyle(
            fontSize: ScreenUtil().setSp(24),
            color: Colors.black,
          ),
        ),
        SizedBox(height: ScreenUtil().setWidth(50)),
        _checkInRewardImg(rewardData),
        SizedBox(height: ScreenUtil().setWidth(50)),
        _rewardName(rewardData),
        SizedBox(height: ScreenUtil().setWidth(46)),
        moreReward(isBold: true),
        SizedBox(height: ScreenUtil().setWidth(38)),
      ],
    );
  }
  void _rewardContentn5fNmoyelive(YBDDailyCheckDialogBlocState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 签到成功的奖励图片
  Widget _checkInRewardImg(YBDRewardData rewardData) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          width: ScreenUtil().setWidth(491),
          height: ScreenUtil().setWidth(236),
          child: Image.asset(
            'assets/images/dc/dc_dialog_reward.png',
            fit: BoxFit.cover,
          ),
        ),
        Container(
          width: ScreenUtil().setWidth(186),
          height: ScreenUtil().setWidth(186),
          padding: EdgeInsets.all(ScreenUtil().setWidth(30)),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(
                ScreenUtil().setWidth(8),
              ),
            ),
            color: Colors.black.withOpacity(0.1),
          ),
          child: rewardData.isLocalImg
              ? YBDImage(
                  path: rewardData.img ?? '',
                  fit: BoxFit.contain,
                )
              : YBDNetworkImage(
                  imageUrl: rewardData.img ?? '',
                  fit: BoxFit.contain,
                ),
        ),
      ],
    );
  }
  void _checkInRewardImgYqcPuoyelive(YBDRewardData rewardData) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 签到成功的奖励名称
  Widget _rewardName(YBDRewardData rewardData) {
    return Container(
      width: ScreenUtil().setWidth(550),
      child: Text(
        '${translate('you_got')} ${rewardData.name}',
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(30),
          color: Colors.black,
        ),
      ),
    );
  }
}
