import 'dart:async';


// import 'dart:convert';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_inappwebview/flutter_inappwebview.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_translate/flutter_translate.dart';
// import 'package:package_info/package_info.dart';
// import 'package:url_launcher/url_launcher.dart';
//
// import '../../../../common/navigator/navigator_helper.dart';
// import '../../../../common/util/log_util.dart';
// import '../../../../common/util/numeric_util.dart';
// import '../../../../common/util/sp_util.dart';
// import '../../../../common/util/toast_util.dart';
// import '../../../../module/user/entity/user_info_entity.dart';
// import '../../../../module/user/util/user_util.dart';
// import '../../../widget/sharing_widget.dart';
// import '../../room/room_helper.dart';
//
// /// 能够返回值给 js 的 webVew
// class TPInAppWebView extends StatefulWidget {
//   /// 网页地址
//   final String urlStr;
//
//   /// 页面标题 ，app传值title
//   final String pageTitle;
//
//   /// 分享名称
//   final String shareName;
//
//   /// 分享图片
//   final String shareImg;
//
//   /// 是否显示顶部导航栏
//   final showNavBar;
//
//   TPInAppWebView(
//     this.urlStr, {
//     this.shareName,
//     this.shareImg,
//     this.pageTitle,
//     this.showNavBar = true,
//   });
//
//   @override
//   TPInAppWebViewState createState() => TPInAppWebViewState();
// }
//
// class TPInAppWebViewState extends State<TPInAppWebView> {
//   InAppWebViewController _webViewController;
//
//   /// 网页标题，可通过 js 通道修改
//   String _jsTitle;
//
//   /// 页面 html <title>
//   String _title;
//
//   /// 网页加载失败，显示重新加载页
//   bool _loadError = false;
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: _topNavBar(),
//       body: Builder(
//         builder: (BuildContext context) {
//           return _loadError ? _errorPageContent() : _webViewContent();
//         },
//       ),
//     );
//   }
//
//   Widget _webViewContent() {
//     return Container(
//       color: Colors.white,
//       child: Column(
//         children: <Widget>[
//           Expanded(
//             child: InAppWebView(
//               // initialUrl: widget.urlStr,
//               onWebViewCreated: (InAppWebViewController controller) {
//                 _webViewController = controller;
//                 _addSetTitle();
//                 _addToast();
//                 _addClose();
//                 _addGetUserInfo();
//                 _addGetUserToken();
//                 _addGetUserMoney();
//                 _addGetAppInfo();
//                 _addJumpProfile();
//                 _addEnterRoom();
//                 _addJumpPage();
//                 _addGetAllInfo();
//               },
//               onTitleChanged: (_, value) {
//                 if (widget.pageTitle == null && _jsTitle == null) {
//                   logger.v('web view on title changed: $value');
//                   setState(() {
//                     _title = value;
//                   });
//                 }
//               },
//               onConsoleMessage: (controller, consoleMessage) {
//                 logger.v('js console msg: $consoleMessage');
//               },
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   /// 设置导航栏标题
//   void _addSetTitle() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'setTitle',
//       callback: (args) {
//         if (((args?.first ?? '') as String).isNotEmpty) {
//           logger.v('js channel set title : ${args?.first}');
//           setState(() {
//             _jsTitle = args?.first;
//           });
//         }
//       },
//     );
//   }
//
//   /// 提示语
//   void _addToast() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'toast',
//       callback: (args) {
//         if (((args?.first ?? '') as String).isNotEmpty) {
//           YBDToastUtil.toast(args.first);
//         }
//       },
//     );
//   }
//
//   /// 退出网页
//   void _addClose() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'close',
//       callback: (args) {
//         Navigator.pop(context);
//       },
//     );
//   }
//
//   /// 获取用户信息
//   void _addGetUserInfo() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'getUserInfo',
//       callback: (args) async {
//         YBDUserInfo userInfo = await YBDSPUtil.getUserInfo();
//         return jsonEncode(userInfo.toJson());
//       },
//     );
//   }
//
//   /// 获取登录 token
//   void _addGetUserToken() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'getUserToken',
//       callback: (args) async {
//         String token = await YBDUserUtil.userToken();
//         return token;
//       },
//     );
//   }
//
//   /// 获取用户 money
//   void _addGetUserMoney() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'getMoney',
//       callback: (args) async {
//         YBDUserInfo userInfo = await YBDUserUtil.userInfo();
//         return '${userInfo.money}';
//       },
//     );
//   }
//
//   /// 获取用户信息和 token 信息
//   void _addGetAllInfo() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'getAllInfo',
//       callback: (args) async {
//         // token
//         String token = await YBDUserUtil.userToken();
//
//         // app 信息
//         PackageInfo packageInfo = await PackageInfo.fromPlatform();
//
//         // 用户信息
//         YBDUserInfo userInfo = await YBDSPUtil.getUserInfo();
//         Map<String, dynamic> userInfoMap = userInfo.toJson();
//
//         // 组装成一个 map 返回给 js
//         Map<String, dynamic> info = {
//           'userInfo': userInfoMap,
//           'token': token,
//           'appName': packageInfo.appName,
//           'packageName': packageInfo.packageName,
//           'version': packageInfo.version,
//           'buildNumber': packageInfo.buildNumber,
//         };
//
//         String result = '';
//         try {
//           result = jsonEncode(info);
//         } catch (e) {
//           logger.v('construct all info data error : $e');
//         }
//
//         return result;
//       },
//     );
//   }
//
//   /// 获取登录应用信息
//   void _addGetAppInfo() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'getAppInfo',
//       callback: (args) async {
//         PackageInfo packageInfo = await PackageInfo.fromPlatform();
//         final infoMap = {
//           'appName': packageInfo.appName,
//           'packageName': packageInfo.packageName,
//           'version': packageInfo.version,
//           'buildNumber': packageInfo.buildNumber,
//         };
//         return jsonEncode(infoMap);
//       },
//     );
//   }
//
//   /// 跳转自己的 profile 或其他人的 profile 页面
//   void _addJumpProfile() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'jumpProfile',
//       callback: (args) async {
//         String targetUserId = args?.first ?? '';
//         String mUserId = await YBDSPUtil.getUserId();
//
//         if (mUserId == targetUserId) {
//           YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.myprofile);
//         } else {
//           YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.user_profile + "/targetUserId");
//         }
//       },
//     );
//   }
//
//   /// 进房间
//   void _addEnterRoom() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'enterRoom',
//       callback: (args) async {
//         String roomId = args?.first ?? '';
//
//         if (roomId.isNotEmpty) {
//           YBDRoomHelper.enterRoom(context, YBDNumericUtil.stringToInt(roomId) ?? 0);
//         } else {
//           logger.v('roomId is empty : $args');
//         }
//       },
//     );
//   }
//
//   /// 跳转页面
//   void _addJumpPage() {
//     _webViewController.addJavaScriptHandler(
//       handlerName: 'jumpPage',
//       callback: (args) async {
//         String route = args?.first ?? '';
//
//         if (route.isNotEmpty) {
//           YBDNavigatorHelper.navigateTo(context, route);
//         } else {
//           logger.v('jump page route is empty : $args');
//         }
//       },
//     );
//   }
//
//   /// 错误页面
//   Widget _errorPageContent() {
//     return GestureDetector(
//       onTap: () {
//         setState(() {
//           _loadError = false;
//         });
//       },
//       child: Container(
//         width: double.infinity,
//         height: double.infinity,
//         alignment: Alignment.center,
//         decoration: BoxDecoration(shape: BoxShape.rectangle),
//         child: Text(
//           translate('tap_retry'),
//           style: TextStyle(color: Colors.black),
//         ),
//       ),
//     );
//   }
//
//   /// 顶部导航栏
//   Widget _topNavBar() {
//     if (widget.showNavBar) {
//       return PreferredSize(
//         preferredSize: Size.fromHeight(ScreenUtil().setWidth(96)),
//         child: AppBar(
//           leading: Container(
//             width: ScreenUtil().setWidth(88),
//             height: ScreenUtil().setWidth(88),
//             child: TextButton(
//               onPressed: () async {
//                 logger.v('clicked back button');
//                 if (_webViewController != null && await _webViewController.canGoBack()) {
//                   _webViewController.goBack();
//                 } else {
//                   Navigator.pop(context);
//                 }
//               },
//               child: Icon(Icons.arrow_back, color: Colors.white),
//             ),
//           ),
//           actions: <Widget>[
//             // TODO: 测试代码
//             // 暂时屏蔽操作菜单
//             // _navPopMenu(),
//           ],
//           centerTitle: true,
//           // app传值title 和  js传值title 为空时，取页面html <title> 值
//           title: Text(
//             _jsTitle ?? widget.pageTitle ?? _title ?? '',
//             style: TextStyle(color: Colors.white),
//           ),
//           flexibleSpace: Container(
//             decoration: BoxDecoration(
//               color: Color(0xffD251F9),
//             ),
//           ),
//         ),
//       );
//     } else {
//       return null;
//     }
//   }
//
//   /// 导航栏下拉菜单
//   Widget _navPopMenu() {
//     return PopupMenuButton(
//       padding: EdgeInsets.all(0.0),
//       icon: Icon(
//         Icons.more_vert,
//         color: Colors.white,
//       ),
//       onSelected: ((String result) {
//         switch (result) {
//           case 'share':
//             showModalBottomSheet(
//                 context: context,
//                 shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.only(
//                       topLeft: Radius.circular(ScreenUtil().setWidth(16)),
//                       topRight: Radius.circular(ScreenUtil().setWidth(16))),
//                 ),
//                 builder: (BuildContext context) {
//                   return YBDSharingWidget(adName: widget.shareName, adUrl: widget.urlStr, adImg: widget.shareImg);
//                 });
//             break;
//           case 'refresh':
//             // showLockDialog(barrierDismissible: true);
//             _webViewController?.reload();
//             break;
//           case 'browser':
//             launch(widget.urlStr);
//             break;
//         }
//       }),
//       itemBuilder: (context) => [
//         PopupMenuItem<String>(
//             value: 'share',
//             child: Row(
//               children: <Widget>[
//                 Icon(Icons.share, color: Colors.blueGrey),
//                 SizedBox(width: ScreenUtil().setWidth(20)),
//                 Text(translate('share'))
//               ],
//             )),
//         PopupMenuItem<String>(
//             value: 'refresh',
//             child: Row(
//               children: <Widget>[
//                 Icon(Icons.refresh, color: Colors.blueGrey),
//                 SizedBox(width: ScreenUtil().setWidth(20)),
//                 Text(translate('refresh'))
//               ],
//             )),
//         PopupMenuItem<String>(
//             value: 'browser',
//             child: Row(
//               children: <Widget>[
//                 Icon(Icons.public, color: Colors.blueGrey),
//                 SizedBox(width: ScreenUtil().setWidth(20)),
//                 Text(translate('open_in_browser'))
//               ],
//             )),
//       ],
//     );
//   }
// }
