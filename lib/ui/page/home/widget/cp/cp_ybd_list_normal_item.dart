import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../../common/util/numeric_ybd_util.dart';
import '../../entity/cp_ybd_board_entity.dart';
import '../../../../widget/round_ybd_avatar.dart';

/// cp 榜第一般 item
class YBDCpListNormalItem extends StatelessWidget {
  /// 是否显示顶部圆角
  final bool firstItem;

  /// 是否显示底部分割线
  final bool lastItem;

  /// item 数据
  final YBDCpBoardRecordItem? itemData;

  /// 排行榜名次
  final int cpNumber;

  YBDCpListNormalItem(
    this.itemData,
    this.cpNumber, {
    this.firstItem = false,
    this.lastItem = true,
  });

  @override
  Widget build(BuildContext context) {
    var radius;

    // 第一个 item 要显示圆角
    if (firstItem) {
      radius = BorderRadius.only(
        topLeft: Radius.circular(ScreenUtil().setWidth(8)),
        topRight: Radius.circular(ScreenUtil().setWidth(8)),
      );
    } else if (lastItem) {
      // 最后一个 item 显示底部圆角，隐藏分割线
      radius = BorderRadius.only(
        bottomLeft: Radius.circular(ScreenUtil().setWidth(8)),
        bottomRight: Radius.circular(ScreenUtil().setWidth(8)),
      );
    } else {
      // 默认没有圆角
      radius = BorderRadius.all(Radius.circular(0));
    }

    return Container(
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(25)),
      decoration: BoxDecoration(
        borderRadius: radius,
        color: Colors.white.withOpacity(0.2),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setWidth(32)),
          Row(
            children: <Widget>[
              SizedBox(width: ScreenUtil().setWidth(30)),
              // 排行榜名次
              _cpBoardNumber(),
              SizedBox(width: ScreenUtil().setWidth(20)),
              // 主播头像
              _talentAvatar(),
              // 粉丝头像
              _userAvatar(),
              SizedBox(width: ScreenUtil().setWidth(36)),
              // 中间的昵称和性别
              _midInfo(),
              Spacer(),
              // 点赞数
              _likeNumber(),
              SizedBox(width: ScreenUtil().setWidth(30)),
            ],
          ),
          SizedBox(height: ScreenUtil().setWidth(32)),
          lastItem
              ? SizedBox()
              : Container(
                  // 分割线
                  height: ScreenUtil().setWidth(1),
                  margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(50)),
                  color: Colors.white.withOpacity(0.1),
                ),
        ],
      ),
    );
  }
  void buildE6faNoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 排行榜名次
  Widget _cpBoardNumber() {
    return Container(
      width: ScreenUtil().setWidth(40),
      child: Text(
        '${cpNumber + 1}',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontSize: ScreenUtil().setSp(26),
        ),
      ),
    );
  }
  void _cpBoardNumberj0B34oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 主播头像
  Widget _talentAvatar() {
    return YBDRoundAvatar(
      // 用户头像
      itemData!.talentHead ?? '',
      sex: itemData!.talentGender,
      lableSrc: '',
      userId: itemData!.talentId,
      needNavigation: true,
      avatarWidth: 86,
      labelWitdh: 0,
      scene: "B",
    );
  }
  void _talentAvatartEE7Royelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 粉丝头像
  Widget _userAvatar() {
    return YBDRoundAvatar(
      // 用户头像
      itemData!.userHead ?? '',
      sex: itemData!.userGender,
      lableSrc: '',
      userId: itemData!.userId,
      needNavigation: true,
      avatarWidth: 86,
      labelWitdh: 0,
      scene: "B",
    );
  }
  void _userAvatarxC6Suoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 主播和粉丝的姓名、性别
  Widget _midInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // 主播
        _nameAndSex(itemData!.talentName, itemData!.talentGender),
        // 粉丝
        _nameAndSex(itemData!.userName, itemData!.userGender),
      ],
    );
  }

  /// 姓名和性别
  /// [sex] 0未知, 1女, 2男
  Widget _nameAndSex(String? name, int? sex) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          // 昵称
          constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(220)),
          child: Text(
            name ?? '',
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(18),
            ),
          ),
        ),
        SizedBox(width: ScreenUtil().setWidth(5)),
        // 性别
        Container(
          width: ScreenUtil().setWidth(40),
          // height: ScreenUtil().setWidth(24),
          child: Image.asset(
            // 0未知, 1女, 2男
            sex == 2 ? "assets/images/tag_male.webp" : "assets/images/tag_female.webp",
            fit: BoxFit.cover,
          ),
        ),
      ],
    );
  }
  void _nameAndSexgzbzKoyelive(String? name, int? sex) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点赞数
  Widget _likeNumber() {
    return Row(
      children: <Widget>[
        Container(
          width: ScreenUtil().setWidth(22),
          child: Image.asset(
            'assets/images/cp/cp_like_icon.webp',
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(width: ScreenUtil().setWidth(4)),
        Text(
          YBDNumericUtil.compactNumberWithFixedDigits('${itemData?.beans ?? 0}')!,
          style: TextStyle(
            color: Colors.white,
            fontSize: ScreenUtil().setSp(20),
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
  void _likeNumberPUTZfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
