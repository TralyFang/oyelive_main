import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import '../../../../../common/util/numeric_ybd_util.dart';
import '../../../../../module/entity/room_ybd_info_entity.dart';
import '../../entity/cp_ybd_board_entity.dart';
import 'cp_ybd_avatar.dart';

/// cp 榜列表 item
class YBDCpListTopItem extends StatelessWidget {
  final BuildContext context;

  /// item 数据
  final YBDCpBoardRecordItem? itemData;

  /// 排行榜名次
  final int cpNumber;

  YBDCpListTopItem(this.context, this.itemData, this.cpNumber);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(
        ScreenUtil().setWidth(25),
        ScreenUtil().setWidth(0),
        ScreenUtil().setWidth(25),
        ScreenUtil().setWidth(20),
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/cp/cp_top_item_bg.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setWidth(10)),
          Row(
            children: <Widget>[
              SizedBox(width: ScreenUtil().setWidth(26)),
              Column(
                children: <Widget>[
                  // 主播头像
                  YBDCpAvatar(_receiverUserInfo(), cpNumber),
                  Row(
                    children: <Widget>[
                      Container(
                        // 主播昵称
                        constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(120)),
                        child: Text(
                          // 主播昵称
                          itemData!.talentName ?? '',
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(24),
                          ),
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(5)),
                      // 性别
                      _sexIcon(_receiverUserInfo()),
                    ],
                  )
                ],
              ),
              Expanded(child: SizedBox(width: 1)),
              // 礼物图片 sent to
              _giftItem(),
              Expanded(child: SizedBox(width: 1)),
              Column(
                children: <Widget>[
                  // 粉丝头像
                  YBDCpAvatar(_senderUserInfo(), cpNumber),
                  Row(
                    children: <Widget>[
                      Container(
                        // 粉丝昵称
                        constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(120)),
                        child: Text(
                          itemData!.userName ?? '',
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(24),
                          ),
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(5)),
                      // 性别
                      _sexIcon(_senderUserInfo()),
                    ],
                  ),
                ],
              ),
              SizedBox(width: ScreenUtil().setWidth(26)),
            ],
          ),
          SizedBox(height: ScreenUtil().setWidth(10)),
        ],
      ),
    );
  }
  void buildFdI3xoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 性别图标
  Widget _sexIcon(YBDRoomInfo receiver) {
    return Container(
      width: ScreenUtil().setWidth(40),
      // height: ScreenUtil().setWidth(24),
      child: Image.asset(
        // 0未知, 1女, 2男
        receiver.sex == 2 ? "assets/images/tag_male.webp" : "assets/images/tag_female.webp",
        fit: BoxFit.cover,
      ),
    );
  }
  void _sexIconwGXO9oyelive(YBDRoomInfo receiver) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 名次和点赞数
  Widget _giftItem() {
    return Container(
      width: ScreenUtil().setWidth(120),
      height: ScreenUtil().setWidth(100),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            // 名次
            top: ScreenUtil().setWidth(10),
            left: ScreenUtil().setWidth(7),
            child: _cpNumberImg(cpNumber),
          ),
          Positioned(
            // 点赞数
            bottom: 0,
            child: _likeRounder(),
          ),
        ],
      ),
    );
  }
  void _giftItemRtSCdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// cp 榜名次
  /// [cpNo] 名次
  Widget _cpNumberImg(int cpNo) {
    var img = '';

    if (cpNo == 0) {
      img = 'assets/images/cp/cp_first_number.webp';
    } else if (cpNo == 1) {
      img = 'assets/images/cp/cp_second_number.webp';
    } else if (cpNo == 2) {
      img = 'assets/images/cp/cp_third_number.webp';
    }

    return Container(
      // width: ScreenUtil().setWidth(80),
      height: ScreenUtil().setWidth(68),
      child: YBDImage(
        path: img,
        fit: BoxFit.cover,
      ),
    );
  }
  void _cpNumberImgIOSHLoyelive(int cpNo) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点赞数
  Widget _likeRounder() {
    return Container(
      width: ScreenUtil().setWidth(112),
      height: ScreenUtil().setWidth(34),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(ScreenUtil().setWidth(30)),
        ),
        gradient: LinearGradient(
          colors: [Color(0xffE37BBC), Color(0xffF06E63)],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(16),
            child: Image.asset(
              'assets/images/cp/cp_white_like_icon.png',
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(4)),
          Text(
            YBDNumericUtil.compactNumberWithFixedDigits('${itemData?.beans ?? 0}')!,
            style: TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(18),
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      ),
    );
  }
  void _likeRounderCMsGCoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 粉丝信息
  YBDRoomInfo _senderUserInfo() {
    final info = YBDRoomInfo();
    info.headimg = itemData!.userHead;
    info.sex = itemData!.userGender;
    info.id = itemData!.userId;
    return info;
  }

  /// 主播信息
  YBDRoomInfo _receiverUserInfo() {
    final info = YBDRoomInfo();
    info.headimg = itemData!.talentHead;
    info.sex = itemData!.talentGender;
    info.id = itemData!.talentId;
    return info;
  }
}
