import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../widget/button/dialog_ybd_close_button.dart';
import '../../../../widget/colored_ybd_safe_area.dart';

/// cp 榜页描述对话框
class YBDCpDescriptionDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return YBDColoredSafeArea(
      child: GestureDetector(
        onTap: () {
          logger.v("clicked the cp dialog bg");
          Navigator.pop(context);
        },
        child: Material(
          color: Colors.black.withOpacity(0.4),
          child: Container(
            width: ScreenUtil().screenWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    _contentView(context),
                    Positioned(
                      // 关闭按钮
                      right: ScreenUtil().setWidth(5),
                      top: ScreenUtil().setWidth(5),
                      child: YBDDialogCloseButton(
                        width: ScreenUtil().setWidth(100),
                        height: ScreenUtil().setWidth(80),
                        scale: 0.8,
                        color: Colors.white,
                        onTap: () {
                          logger.v('dismiss dialog');
                          Navigator.pop(context);
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  void buildWX7XHoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 对话框内容
  Widget _contentView(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(600),
      height: ScreenUtil().setWidth(250),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(13)),
        gradient: LinearGradient(
          colors: [Color(0xff47CDCC), Color(0xff3E76CC)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: ScreenUtil().setWidth(10)),
            // 顶部标题和关闭按钮
            _topRow(context),
            // 第一段描述
            _descriptionText(),
          ],
        ),
      ),
    );
  }
  void _contentView6yUVpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 顶部标题
  Widget _topRow(BuildContext context) {
    return Container(
      width: double.infinity,
      height: ScreenUtil().setWidth(98),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            // 标题
            top: ScreenUtil().setWidth(20),
            child: Container(
              width: ScreenUtil().setWidth(524),
              child: Image.asset(
                'assets/images/cp/cp_dialog_title.png',
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }
  void _topRowM4h2Loyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 第一段描述
  Widget _descriptionText() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(40),
        vertical: ScreenUtil().setWidth(20),
      ),
      child: Text(
        translate('cp_desc'),
        style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
      ),
    );
  }
  void _descriptionText0etyGoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
