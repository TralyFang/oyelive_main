import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../../common/util/log_ybd_util.dart';
import '../../../../widget/loading_ybd_circle.dart';
import '../../../follow/widget/place_ybd_holder_view.dart';
import '../../entity/cp_ybd_board_entity.dart';
import 'cp_ybd_list_normal_item.dart';
import 'cp_ybd_list_top_item.dart';

/// cp 列表页面的 tab 内容
class YBDCpContent extends StatefulWidget {
  /// cp 列表数组
  final List<YBDCpBoardRecordItem?>? data;

  /// 是否为初始状态
  /// 初始状态没有数据时显示加载框，非初始状态没数据显示缺省图
  final bool isInitState;

  YBDCpContent(this.data, {this.isInitState = false});

  @override
  _YBDCpContentState createState() => _YBDCpContentState();
}

class _YBDCpContentState extends State<YBDCpContent> with AutomaticKeepAliveClientMixin {
  /// 列表刷新控制器
  /// 先保留，暂时没用到
  RefreshController _refreshController = RefreshController();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
  }
  void initStateas1TFoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == widget.data || widget.data!.isEmpty) {
      logger.v('cp list is empty show placeholder');
      return widget.isInitState ? YBDLoadingCircle() : YBDPlaceHolderView(text: translate('no_data'));
    }

    return Container(
      child: SmartRefresher(
        controller: _refreshController,
        enablePullDown: false,
        enablePullUp: false,
        child: ListView.builder(
          itemCount: widget.data!.length, // 数据的数量
          padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(30)),
          itemBuilder: (context, index) {
            var item;

            if (index < 3) {
              // 前三名
              item = Container(
                width: ScreenUtil().screenWidth,
                child: YBDCpListTopItem(context, widget.data![index], index),
              );
            } else {
              // 其他排名
              item = YBDCpListNormalItem(
                widget.data![index],
                index,
                // 第一个 item 要显示圆角
                firstItem: index == 3,

                /// 最后一个 item 显示底部圆角，隐藏分割线
                lastItem: index == widget.data!.length - 1,
              );
            }

            return GestureDetector(
              onTap: () {
                logger.v('clicked cp item');
              },
              child: item,
            );
          },
          // 类似 cellForRow 函数
        ),
      ),
    );
  }
  void buildCSwiToyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }
}
