import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../../common/util/log_ybd_util.dart';
import '../../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../../module/user/util/user_ybd_util.dart';
import '../../../../widget/round_ybd_avatar.dart';

/// cp 列表头像
class YBDCpAvatar extends StatelessWidget {
  /// 礼物接收者信息
  final YBDRoomInfo userInfo;

  /// 排行榜名次
  final int cpNumber;

  YBDCpAvatar(this.userInfo, this.cpNumber);

  @override
  Widget build(BuildContext context) {
    return Container(
//      color: Colors.black,
      width: ScreenUtil().setWidth(235),
      height: ScreenUtil().setWidth(174),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          YBDRoundAvatar(
            // 用户头像
            userInfo.headimg ?? '',
            sex: userInfo.sex,
            userId: userInfo.id,
            needNavigation: true,
            avatarWidth: 125,
            scene: "B",
          ),
          GestureDetector(
            // 覆盖在头像上的皇冠图片
            onTap: () async {
              final spUserId = await YBDUserUtil.userId();

              // 根据 sp 的用户 id 判断跳转的其人的 profile 或者跳转到个人中心
              if ('${userInfo.id}' == spUserId) {
                logger.v('jump to my profile page');
                YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.myprofile);
              } else {
                logger.v('jump to other user profile page : ${userInfo.id}');
                YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.user_profile + "/${userInfo.id}");
              }
            },
            child: _avatarImg(),
          ),
        ],
      ),
    );
  }
  void buildQOc8Toyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 覆盖在头像上的图片
  Widget _avatarImg() {
    // print('avatar cp number : $cpNumber');

    // cp 榜第一名
    if (cpNumber == 0) {
      return Transform.translate(
        offset: Offset(0, -ScreenUtil().setWidth(4)),
        child: Container(
          width: ScreenUtil().setWidth(235),
          // height: ScreenUtil().setWidth(174),
          // padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(28)),
          child: Image.asset(
            'assets/images/cp/cp_first_crown.webp',
            fit: BoxFit.cover,
          ),
        ),
      );
    }

    // cp 榜第二名
    if (cpNumber == 1) {
      return Transform.translate(
        offset: Offset(-ScreenUtil().setWidth(0), ScreenUtil().setWidth(8)),
        child: Container(
          width: ScreenUtil().setWidth(235),
          // height: ScreenUtil().setWidth(174),
          // padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(28)),
          child: Image.asset(
            'assets/images/cp/cp_second_crown.webp',
            fit: BoxFit.cover,
          ),
        ),
      );
    }

    // cp 榜第三名
    if (cpNumber == 2) {
      return Transform.translate(
        offset: Offset(ScreenUtil().setWidth(0), ScreenUtil().setWidth(6)),
        child: Container(
          width: ScreenUtil().setWidth(235),
          // height: ScreenUtil().setWidth(174),
          // padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(28)),
          child: Image.asset(
            'assets/images/cp/cp_third_crown.png',
            fit: BoxFit.cover,
          ),
        ),
      );
    }

    return SizedBox();
  }
  void _avatarImgEMkkHoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
