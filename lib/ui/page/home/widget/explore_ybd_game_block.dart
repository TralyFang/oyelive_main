import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_skin.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_resource_util.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/toast_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../../module/entity/game_ybd_info_entity.dart';
import '../bloc/gifters_ybd_bloc.dart';

class YBDExploreGameBlock extends StatefulWidget {
  @override
  YBDExploreGameBlockState createState() => new YBDExploreGameBlockState();
}

class YBDExploreGameBlockState extends BaseState<YBDExploreGameBlock> {
  YBDGameInfoEntity? _gameInfoEntity;
  BoxDecoration decoration = BoxDecoration(
    color: Colors.transparent,
    borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
  );

  closeLayer(index, {required Widget child}) {
    bool close;
    try {
      close = _gameInfoEntity?.record![index]?.shieldStatus != 0;
    } catch (e) {
      print(e);
      close = true;
    }
    return GestureDetector(
      onTap: () {
        if (close) {
          YBDToastUtil.toast(translate('game_not_available'));
          return;
        }

        try {
          final url = Uri.encodeComponent(_gameInfoEntity!.record![index]!.url!);
          YBDNavigatorHelper.navigateToWeb(context, "?url=$url&showNavBar=false");
          YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
              location: YBDLocationName.HOME_EXPLORE_PAGE,
              itemName: YBDItemName.HOT_GAMES,
              adUnitName: _gameInfoEntity!.record![index]!.name));
        } catch (e) {
          print(e);
          YBDToastUtil.toast(translate('game_not_available'));
        }
      },
      child: Stack(
        children: <Widget>[
          Positioned.fill(child: child),
          if (close)
            Positioned(
                right: 0,
                top: 0,
                child: Image.asset(
                  YBDGameResource.assetPadding('game_close', need2x: false),
                  width: ScreenUtil().setWidth(56),
                )),
        ],
      ),
    );
  }

  getGameInfo() async {
    YBDGameInfoEntity? gameInfoEntity = await ApiHelper.getGameInfo(context);
    if (gameInfoEntity != null && gameInfoEntity.returnCode == Const.HTTP_SUCCESS) {
      _gameInfoEntity = gameInfoEntity;
      setState(() {});
    }
  }

  getGameBg(index) {
    try {
      return _gameInfoEntity?.record![index]?.icon ?? '';
    } catch (e) {
      print(e);
      return '';
    }
  }

  getGameName(index) {
    try {
      return _gameInfoEntity?.record![index]?.name ?? '';
    } catch (e) {
      print(e);
      return '';
    }
  }

  getTextStyle(Size) {
    return TextStyle(fontSize: ScreenUtil().setSp(Size), fontWeight: FontWeight.bold, shadows: [
      Shadow(
          color: Colors.black.withOpacity(0.8),
          offset: Offset(0, ScreenUtil().setWidth(2)),
          blurRadius: ScreenUtil().setWidth(4))
    ]);
  }

  getBigGame() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
      child: Stack(
        children: <Widget>[
          Positioned.fill(
              child: Image.asset(
            YBDGameResource.assetPadding('game_big', need2x: false),
            fit: BoxFit.fill,
          )),
          Positioned(
            left: ScreenUtil().setWidth(100),
            right: ScreenUtil().setWidth(106),
            top: ScreenUtil().setWidth(92),
            child: YBDNetworkImage(
                height: ScreenUtil().setWidth(124),
                width: ScreenUtil().setWidth(124),
                fit: BoxFit.fill,
                placeholder: (_, x) => Container(
                      decoration: decoration,
                    ),
                imageUrl: getGameBg(0)),
          ),
          Positioned(
            left: ScreenUtil().setWidth(20),
            top: ScreenUtil().setWidth(30),
            child: Container(
              width: ScreenUtil().setWidth(306),
              child: Text(
                getGameName(0),
                style: getTextStyle(30),
              ),
            ),
          )
        ],
      ),
    );
  }

  getMiddleGameLeft() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
      child: Stack(
        children: <Widget>[
          Positioned.fill(
              child: Image.asset(
            YBDGameResource.assetPadding('game_middle_1', need2x: false),
            fit: BoxFit.fill,
          )),
          Positioned(
            left: ScreenUtil().setWidth(174),
            right: ScreenUtil().setWidth(32),
            child: YBDNetworkImage(
                height: ScreenUtil().setWidth(124),
                width: ScreenUtil().setWidth(124),
                fit: BoxFit.fill,
                placeholder: (_, x) => Container(
                      decoration: decoration,
                    ),
                imageUrl: getGameBg(1)),
          ),
          Positioned(
            left: ScreenUtil().setWidth(34),
            top: ScreenUtil().setWidth(20),
            child: Container(
              width: ScreenUtil().setWidth(114),
              child: Text(
                getGameName(1),
                style: getTextStyle(28),
              ),
            ),
          )
        ],
      ),
    );
  }

  getMiddleGameRight() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
      child: Stack(
        children: <Widget>[
          Positioned.fill(
              child: Image.asset(
            YBDGameResource.assetPadding('game_middle_2', need2x: false),
            fit: BoxFit.fill,
          )),
          Positioned(
            left: ScreenUtil().setWidth(44),
            right: ScreenUtil().setWidth(170),
            child: YBDNetworkImage(
              height: ScreenUtil().setWidth(124),
              width: ScreenUtil().setWidth(124),
              fit: BoxFit.fill,
              placeholder: (_, x) => Container(
                decoration: decoration,
              ),
              imageUrl: getGameBg(2),
            ),
          ),
          Positioned(
            right: ScreenUtil().setWidth(28),
            top: ScreenUtil().setWidth(20),
            child: Container(
              width: ScreenUtil().setWidth(114),
              child: Text(
                getGameName(2),
                textAlign: TextAlign.right,
                style: getTextStyle(28),
              ),
            ),
          )
        ],
      ),
    );
  }

  getSmallGame(index) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
      child: Stack(
        children: <Widget>[
          Positioned.fill(
              child:
                  Image.asset(YBDGameResource.assetPadding('game_small_${index - 2}', need2x: false), fit: BoxFit.fill)),
          Positioned(
            left: ScreenUtil().setWidth(8),
            right: ScreenUtil().setWidth(108),
            top: ScreenUtil().setWidth(15),
            bottom: ScreenUtil().setWidth(5),
            child: YBDNetworkImage(
                height: ScreenUtil().setWidth(100),
                width: ScreenUtil().setWidth(100),
                fit: BoxFit.fill,
                placeholder: (_, x) => Container(
                      decoration: decoration,
                    ),
                imageUrl: getGameBg(index)),
          ),
          Positioned(
            right: ScreenUtil().setWidth(12),
            top: ScreenUtil().setWidth(12),
            child: Container(
              width: ScreenUtil().setWidth(114),
              child: Text(
                getGameName(index),
                textAlign: TextAlign.right,
                style: getTextStyle(22),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget myBuild(BuildContext context) {
    if (_gameInfoEntity == null || _gameInfoEntity!.record!.length == 0) {
      return Container();
    }
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(25)),
      child: Column(
        children: [
          SizedBox(height: ScreenUtil().setWidth(20)),
          Row(
            children: [
              Text(
                translate('hot_game'),
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(28),
                  color: YBDActivitySkinRoot().curAct().activityTextColor(),
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(10)),
              Image.asset(
                'assets/images/icon_hot.png',
                width: ScreenUtil().setWidth(20),
              )
            ],
          ),
          SizedBox(height: ScreenUtil().setWidth(24)),
          Container(
            height: ScreenUtil().setWidth(260),
            child: Row(
              children: [
                Expanded(
                  child: closeLayer(0,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)), child: getBigGame())),
                ),
                SizedBox(width: ScreenUtil().setWidth(10)),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: closeLayer(1,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
                                child: getMiddleGameLeft())),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(10)),
                      Expanded(
                        child: closeLayer(2,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
                                child: getMiddleGameRight())),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: ScreenUtil().setWidth(10)),
          Container(
            height: ScreenUtil().setWidth(120),
            child: Row(
              children: List.generate(5, (index) {
                if (index % 2 == 0)
                  return Expanded(
                    child: closeLayer(
                      index ~/ 2 + 3,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
                          child: getSmallGame(index ~/ 2 + 3)),
                    ),
                  );
                else
                  return SizedBox(width: ScreenUtil().setWidth(10));
              }),
            ),
          ),
          SizedBox(height: ScreenUtil().setWidth(36)),
        ],
      ),
    );
  }
  void myBuildF2scRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    getGameInfo();
    context.read<YBDGiftersBloc>().stream.listen((data) {
      if (_gameInfoEntity != null) getGameInfo();
    });
  }
  void initStatenxS6Noyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
