import 'dart:async';


import 'package:oyelive_main/ui/widget/ybd_image.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../entity/car_ybd_list_entity.dart';
import '../entity/daily_ybd_check_entity.dart';
import '../entity/gift_ybd_list_entity.dart';
import '../../profile/my_profile/util/reward_ybd_data_util.dart';

/// 签到弹框大 item
class YBDDailyCheckDialogBigItem extends StatelessWidget {
  /// 第几天签到
  final String dayName;

  /// 奖励 item 的数据
  final YBDDailyCheckRecordExtendRewardListDailyReward? dailyCheckTask;

  /// 礼物列表
  final List<YBDGiftListRecordGift?>? giftList;

  /// 座驾列表
  final List<YBDCarListRecord?>? carList;

  YBDDailyCheckDialogBigItem({
    required this.dailyCheckTask,
    required this.dayName,
    required this.giftList,
    required this.carList,
  });

  @override
  Widget build(BuildContext context) {
    // 单次签到的奖励数据
    final rewardData = YBDRewardDataUtil.rewardData(
      context,
      dailyCheckTask: dailyCheckTask!,
      giftList: giftList,
      carList: carList,
    );

    return Container(
      width: ScreenUtil().setWidth(160),
      height: ScreenUtil().setWidth(275),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/dc/dc_dialog_big_gift_bg.webp'),
          fit: BoxFit.cover,
        ),
      ),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _itemImg(rewardData),
          Positioned(
            bottom: ScreenUtil().setWidth(30),
            child: _itemName(rewardData),
          ),
          Positioned(
            top: ScreenUtil().setWidth(10),
            left: 0,
            child: _number(),
          ),
        ],
      ),
    );
  }
  void buildmDwyxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 数字
  Widget _number() {
    return Container(
      width: ScreenUtil().setWidth(36),
      child: Text(
        '7',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontSize: ScreenUtil().setSp(18),
        ),
      ),
    );
  }
  void _numberNXOjxoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 图片
  Widget _itemImg(YBDRewardData rewardData) {
    return Container(
      width: ScreenUtil().setWidth(160),
      child: rewardData.isLocalImg
          ? YBDImage(
              path: rewardData.img ?? '',
              fit: BoxFit.contain,
            )
          : YBDNetworkImage(
              imageUrl: rewardData.img ?? '',
              fit: BoxFit.cover,
            ),
    );
  }
  void _itemImg3ViZwoyelive(YBDRewardData rewardData) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 名称
  Widget _itemName(YBDRewardData rewardData) {
    return Container(
      width: ScreenUtil().setWidth(160),
      child: Text(
        '${rewardData.name}',
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: _rewardNameFont(name: '${rewardData.name}'),
          color: Colors.black,
        ),
      ),
    );
  }
  void _itemNameFQUXKoyelive(YBDRewardData rewardData) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 文字太长缩小字体
  /// [maxWidth] 文字显示的最大宽度
  /// return 字体大小
  double _rewardNameFont({required String name}) {
    if (name.length >= 10) {
      return ScreenUtil().setSp(14);
    } else {
      return ScreenUtil().setSp(18);
    }
  }
}
