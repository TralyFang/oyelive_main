import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/event/game_ybd_mission_event.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/set_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_file.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';
import 'package:oyelive_main/ui/page/home/entity/daily_ybd_task_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/king_ybd_kong_entity.dart';
import 'package:oyelive_main/ui/page/home/widget/event_center/red_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../controller/event_ybd_dot_controller.dart';

/// popular 页面的排行榜图标
class YBDPopularBoard extends StatefulWidget {
  List<YBDKingKongRecord?>? list;
  YBDPopularBoard({this.list});

  @override
  State<StatefulWidget> createState() => _YBDPopularBoardState();
}

class _YBDPopularBoardState extends State<YBDPopularBoard> {
  YBDRedController controller = Get.put(YBDRedController());
  YBDEventDotController eventIconCtr = Get.put(YBDEventDotController());

  @override
  void initState() {
    super.initState();

    // 本地没有记录活动列表时默认显示红点
    eventIconCtr.checkEventList();

    _requestDailyTask();

    // 初始红点状态
    eventIconCtr.showRedDot();

    eventBus.on<YBDGameMissionEvent>().listen((event) {
      Future.delayed(Duration(milliseconds: 200), () {
        _requestDailyTask();
      });
    });
  }
  void initStatekJh72oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 请求每日任务列表
  Future _requestDailyTask() async {
    if (mounted) {
      YBDMyDailyTaskEntity? dailyTaskEntity = await ApiHelper.queryDailyTask(context, viewing: 1);

      if (null != dailyTaskEntity && null != dailyTaskEntity.record) {
        bool needReload = false;
        dailyTaskEntity.record!.forEach((element) {
          if (element!.status == 1 && (!element.receive!)) {
            needReload = true;
          }
        });
        Get.find<YBDRedController>().showRed.value = needReload;
        Get.find<YBDRedController>().update(['popular_get']);
      } else {
        logger.v('game task rewardList is null');
      }
    }
  }
  void _requestDailyTask3THbNoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(135),
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
      child: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setWidth(20)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: itemList(),
          ),
        ],
      ),
    );
  }
  void build7Fu94oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Widget> itemList() {
    List<Widget> ls = [];
    logger.v('king kong list: ${widget.list.def()?.length}');
    for (int i = 0; i < widget.list.def()!.length; i++) {
      ls.add(Expanded(child: _item(i)));
      if (widget.list.def()!.length <= 3) {
        ls.add(SizedBox(width: ScreenUtil().setWidth(10)));
      }
    }
    return ls;
  }
  void itemListKHcBioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _item(int index) {
    return GestureDetector(
      onTap: () {
        logger.v('clicked popular ${widget.list.def()![index]!.name} route: ${widget.list.def()![index]!.route}');
        YBDCommonTrack().commonTrack(YBDTAProps(location: widget.list.def()![index]!.name, module: YBDTAModule.kingkong));
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
          YBDEventName.CLICK_EVENT,
          location: YBDLocationName.HOME_POPULAR_PAGE,
          itemName: 'popular_${widget.list.def()![index]!.name}',
        ));
        YBDNavigatorHelper.navigateTo(context, widget.list.def()![index]!.route,
            routeSettings: RouteSettings(arguments: {
              "title": translate(widget.list.def()![index]!.name!),
            }));
      },
      child: Container(
        height: ScreenUtil().setWidth(112),
        decoration: widget.list.def()!.length > 3
            ? BoxDecoration()
            : BoxDecoration(
                borderRadius: BorderRadius.circular(ScreenUtil().setWidth(8)),
                color: Colors.white.withOpacity(0.1),
              ),
        child: Container(
          child: _imgItem(
              // _popularBoardIcons()[index],
              widget.list.def()![index]!.img,
              translate(widget.list.def()![index]!.name!),
              index),
        ),
      ),
    );
  }
  void _itemlzRxZoyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 单个 item
  Widget _imgItem(String? img, String text, int index) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: ScreenUtil().setWidth(
                  YBDActivityFile.instance!.canShowSkin && YBDActivityFile.CURRENT_TEMPLATE == 'Christmas_2021' ? 82 : 80),
              child: YBDImage(
                path: img,
                fit: BoxFit.cover,
                placeholder: (
                  BuildContext context,
                  String url,
                ) {
                  return Image.asset('assets/images/default/popular_default.webp');
                },
              ),
            ),
            Positioned(
              right: 10.dp720,
              top: 12.dp720,
              child: _redDot(index),
            ),
          ],
        ),
        // SizedBox(height: ScreenUtil().setWidth(4)),
        Text(
          text,
          style: TextStyle(
            color: YBDActivityFile.instance!.canShowSkin
                ? YBDActivitySkinRoot().curAct().activityTextColor()
                : Colors.white.withOpacity(0.8),
            fontSize: ScreenUtil().setSp(24),
          ),
        ),
      ],
    );
  }
  void _imgItemXrmedoyelive(String? img, String text, int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 红点
  Widget _redDot(int index) {
    var dot = Container(
      width: 18.dp720,
      height: 18.dp720,
      decoration: BoxDecoration(
        color: Color(0xffE6497A),
        borderRadius: BorderRadius.all(
          Radius.circular(9.dp720 as double),
        ),
      ),
    );

    // index == 3 任务红点
    if (index == 3) {
      return GetBuilder(
          init: YBDRedController(),
          id: 'popular_get',
          builder: (dynamic ctr) {
            if (ctr.showRed.value) {
              return dot;
            }

            return SizedBox();
          });
    } else if (index == 4) {
      // index == 4 活动红点
      return GetBuilder<YBDEventDotController>(
        init: YBDEventDotController(),
        builder: (eventIconCtr) {
          var newIcon = Transform.translate(
            offset: Offset(24.px as double, -4.px as double),
            child: YBDRedIcon(scale: 0.6),
          );

          // 本地没有活动列表记录显示红点
          if (eventIconCtr.showEventRed.value || eventIconCtr.eventListNull.value) {
            return newIcon;
          }

          return SizedBox();
        },
      );
    }

    return SizedBox();
  }
  void _redDotPo3V7oyelive(int index) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    Get.delete<YBDRedController>();
    Get.delete<YBDEventDotController>();
    super.dispose();
  }
}

class YBDRedController extends GetxController {
  RxBool showRed = false.obs;
}
