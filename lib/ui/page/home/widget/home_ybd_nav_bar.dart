import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_size_util.dart';

import '../../../../common/analytics/analytics_ybd_util.dart';
import '../../../../common/constant/const.dart';
import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/sp_ybd_util.dart';

/// home 页面的导航栏
class YBDHomeNavBar extends StatefulWidget {
  /// 顶部标签栏控制器
  final TabController? controller;

  /// 滚动控制器
  final ScrollController? scrollController;

  /// 选中的标签栏索引
  final int currentTabIndex;

  final bool openGame;

  YBDHomeNavBar(
    this.controller,
    this.scrollController,
    this.currentTabIndex,
    this.openGame,
  );

  @override
  YBDHomeNavBarState createState() => YBDHomeNavBarState();
}

class YBDHomeNavBarState extends State<YBDHomeNavBar> {
  bool showHot = false;
  @override
  Widget build(BuildContext context) {
    Decoration decoration = BoxDecoration();

    return Container(
      decoration: decoration,
      height: ScreenUtil().statusBarHeight +
          ScreenUtil().setWidth(96), // androind96 ios88
      padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
      child: Row(
        children: <Widget>[
          searchButton(),
          Expanded(
            child: homeNavTabBar(),
          ),
          SizedBox(width: ScreenUtil().setWidth(70))
        ],
      ),
    );
  }

  void buildibnoaoyelive(BuildContext context) {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// tab bar 栏
  Widget homeNavTabBar() {
    List<Widget> _tabs = [
      GestureDetector(
        // Related 标签
        onDoubleTap: () {
          logger.v('double click to top');
          widget.scrollController!.animateTo(
            0,
            duration: Duration(milliseconds: 500),
            curve: Curves.ease,
          );
        },
        child: Tab(
          text: translate('related'),
        ),
      ),
      GestureDetector(
        // Popular 标签
        onDoubleTap: () {
          logger.v('double click to top');
          widget.scrollController!.animateTo(
            0,
            duration: Duration(milliseconds: 500),
            curve: Curves.ease,
          );
        },
        child: Tab(text: translate('popular')),
      ),
      GestureDetector(
        // Explore 标签
        onDoubleTap: () {
          widget.scrollController!.animateTo(
            0,
            duration: Duration(milliseconds: 500),
            curve: Curves.ease,
          );
        },
        child: Tab(text: translate('explore')),
      ),
    ];

    /// 如果开启了游戏，那就展示
    // Capture from onError Controller's length property (4) does not match
    // the number of tabs (5) present in TabBar's tabs property.
    if (widget.openGame) {
      _tabs.add(GestureDetector(
        // Popular 标签
        onDoubleTap: () {
          logger.v('double click to top');
          widget.scrollController!.animateTo(
            0,
            duration: Duration(milliseconds: 500),
            curve: Curves.ease,
          );
        },
        child: Padding(
          padding: EdgeInsets.only(
              left: (showHot ? 7.dp720 as double : 27.dp720 as double)),
          child: Tab(
            child: Row(
              children: [
                Text("game".i18n),
                if (showHot)
                  Padding(
                    padding: EdgeInsets.only(
                        left: 8.dp720 as double, bottom: 5.dp720 as double),
                    child: Image.asset(
                      'assets/images/icon_hot.png',
                      width: ScreenUtil().setWidth(20),
                    ),
                  )
              ],
            ),
          ),
        ),
      ));
    }
    return Container(
      child: TabBar(
        onTap: (index) {
          if (index == 2) YBDSPUtil.save(Const.SP_HOT_MARK, '1');
        },
        indicator: BoxDecoration(),
        labelColor: _labelColor(),
        unselectedLabelColor: _unselectedLabelColor(),
        controller: widget.controller,
        labelStyle: TextStyle(
          fontWeight: FontWeight.bold,
          height: 1,
          fontSize: ScreenUtil().setSp(32),
        ),
        unselectedLabelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          height: 1,
          fontSize: ScreenUtil().setSp(28),
        ),
        labelPadding: EdgeInsets.all(0),
        tabs: _tabs,
      ),
    );
  }

  void homeNavTabBarW2mFPoyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 搜索按钮
  Widget searchButton() {
    return GestureDetector(
      onTap: () {
        logger.v('clicked home nav search button');
        YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.search);
        YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
            location: YBDLocationName.HOME_PAGE, itemName: 'search'));
      },
      child: Container(
        width: ScreenUtil().setWidth(88),
        height: ScreenUtil().setWidth(88),
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(),
        child: Image.asset(
          'assets/images/icon_search.png',
          color: _labelColor(),
        ),
      ),
    );
  }

  void searchButtondYl5Joyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }

  /// 选中标签栏的文字颜色
  Color _labelColor() {
    if (widget.currentTabIndex == Const.POPULAR_FEATURED_INDEX) {
      return YBDActivitySkinRoot().curAct().popularNavTitleColor();
    }

    return YBDActivitySkinRoot().curAct().activityTextColor();
  }

  /// 未选中标签栏的文字颜色
  Color _unselectedLabelColor() {
    if (widget.currentTabIndex == Const.POPULAR_FEATURED_INDEX) {
      return YBDActivitySkinRoot()
          .curAct()
          .popularNavTitleColor()
          .withOpacity(0.7);
    }

    return YBDActivitySkinRoot().curAct().activityTextColor().withOpacity(0.7);
  }

  @override
  void initState() {
    YBDSPUtil.get(Const.SP_HOT_MARK).then((value) {
      if (value == null) {
        showHot = true;
        setState(() {});
      }
    });
    super.initState();
  }

  void initState7gmr5oyelive() {
    int needCount = 0;
    print('input result:$needCount');
  }
}
