import 'dart:async';


import 'dart:io';

import 'package:firebase_performance/firebase_performance.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/platform_interface.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:oyelive_main/common/abstract/analytics/analysis_ybd_log.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_live_room_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_loading_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_event.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/util/intercept_ybd_record_manager.dart';
import 'package:oyelive_main/common/util/obs_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/user/entity/login_ybd_resp_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_event.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/zego_ybd_rtc_api.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/widget/loading_ybd_circle.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/dialog_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../common/util/string_ybd_util.dart';
import '../../../../common/widget/network_ybd_error_view.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../widget/colored_ybd_safe_area.dart';
import '../../../widget/sharing_ybd_widget.dart';
import '../handler/js_ybd_channel_handler.dart';
import '../handler/js_ybd_msg_parser.dart';

/**
 * 缓存加载比较方案：
 * 1。 记录每次加载对应url的时间
 * 2。 图片资源拦截开启的情况加载时长
 * 3。 js，css资源拦截开启情况加载时长
 * 4。 iOS，Android加载方式区别
 *
 * */

///兼容后续游戏
enum WebGameType {
  Default,
  Greedy,

  /// 水果
  TurnTable,

  /// 转盘
  Ludo,

  /// 房间底部栏可配置的游戏
  RoomBottom,
}

enum WebEntranceType {
  Default,
  Banner, //Banner
  Slider, //房间slider
  AlertView, //弹出窗
  EventNotice, // 私信活动消息
  EventCenter, // 活动中心
  Float, // 悬浮窗
}

/// 网页容器页面
class YBDTPWebView extends StatefulWidget {
  /// 网页地址
  final String? urlStr;

  /// 页面标题 ，app传值title
  final String? pageTitle;

  /// 分享名称
  final String? shareName;

  /// 分享图片
  final String? shareImg;

  /// 是否显示顶部导航栏
  final bool showNavBar;

  /// 是否为透明色
  /// true: 不透明， false： 透明
  final bool opaque;

  /// 水果游戏添加埋点代码
  /// TODO: 兼容其他游戏
  final WebGameType gameType;

  ///入口类型
  final WebEntranceType entranceType;

  /// 页面关闭的回调
  final VoidCallback? closeCallback;

  /// 启用混合合成
  final bool enableHybrid;

  /// webview加载完毕
  final VoidCallback? finishedCallback;

  /// 提供给外面加载url来的
  final WebViewCreatedCallback? createdCallback;

  /// 显示开始加载动画 默认true
  final bool showStartLoading;

  YBDTPWebView(
    this.urlStr, {
    Key? key,
    this.shareName,
    this.shareImg,
    this.pageTitle,
    this.showNavBar = true,
    this.opaque = true,
    this.enableHybrid = true,
    this.gameType = WebGameType.Default,
    this.entranceType = WebEntranceType.Default,
    this.closeCallback,
    this.finishedCallback,
    this.createdCallback,
    this.showStartLoading = true,
  }) : super(key: key);

  @override
  YBDTPWebViewState createState() => YBDTPWebViewState();
}

class YBDTPWebViewState extends BaseState<YBDTPWebView> {
  WebViewController? _webViewController;

  /// 网页标题，可通过 js 通道修改
  String? _jsTitle;

  /// 页面html <title>
  String? _title;

  /// 网页加载失败，显示重新加载页
  bool _loadError = false;

  // 水果游戏load finished 后才游戏进度条
  // 网页开始加载到开始显示游戏进度条的时长
  /// TODO: 兼容猎游游戏的时长的分析
  late Trace _loadingWebTrace; // = FirebasePerformance.instance.newTrace("greedy_game_load_web");

  /// 游戏加载资源的时长
  /// TODO: 兼容猎游游戏的时长的分析
  late Trace _loadingResourceTrace;

  /// 水果游戏加页面停留的时长
  /// TODO: 兼容猎游游戏的时长的分析
  late Trace _playTimeTrace; // = FirebasePerformance.instance.newTrace("greedy_game_play_time");

  /// 添加 js 通道方法
  YBDJsChannelHandler? _channelHandler;

  ///网页地址
  String? webUrl;

  /// 是否为初始状态
  bool _isInitState = true;

  /// 弹起键盘是否需要视图滚动 默认不动
  bool resizeToAvoidBottomInset = false;
  bool bottom = false;

  late int webStartTime;

  /// 网页给的分享内容
  Map<String, dynamic> _eventCenterShare = {};

  @override
  void initState() {
    super.initState();
    logger.v('web view init url : ${widget.urlStr}');
    webStartTime = DateTime.now().millisecondsSinceEpoch;

    reloadUrl();

    // 初始化 js channel 处理类
    _configJsChannelHandler();

    // 游戏埋点
    if (widget.gameType.index > WebGameType.Default.index) {
      logger.v('===greedy performance');
      _loadingWebTrace = FirebasePerformance.instance.newTrace("greedy_game_load_web");
      _loadingResourceTrace = FirebasePerformance.instance.newTrace('greedy_game_load_resource');
      _playTimeTrace = FirebasePerformance.instance.newTrace("greedy_game_play_time");

      // 打开网页开始加载游戏网页
      _loadingWebTrace.start();
      _playTimeTrace.start();
    }

    // 启用混合合成 参考 https://blog.csdn.net/whs867712232/article/details/111030393
    if (Platform.isAndroid && widget.enableHybrid) WebView.platform = SurfaceAndroidWebView();

    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.OPEN_PAGE,
      location: YBDLocationName.WEB_PAGE,
      itemName: maxParamValue(widget.urlStr),
    ));
  }
  void initState2Wm9soyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///客户端url 后面加cookie
  reloadUrl() async {
    String urlStr = await widget.urlStr!.webCookie();

    _getWebUrl(urlStr);
  }

  /// 配置 js channel 处理类
  void _configJsChannelHandler() {
    _channelHandler = YBDJsChannelHandler(
        context: context,
        jsTitleCallback: (title) {
          _jsTitle = title;

          if (mounted) {
            setState(() {});
          }
        },
        startGameLoadCallback: () {
          if (widget.gameType.index > WebGameType.Default.index) {
            // 加载游戏资源表示网页已经加载完成
            _loadingWebTrace.stop();

            // 开始加载游戏资源
            _loadingResourceTrace.start();
          }
        },
        loadedGameCallback: () {
          // Deprecated
          // 埋点
          // YBDTALiveRoomTrack().taRoom(
          //   event: YBDTATrackEvent.liveroom_game_load_finish,
          //   record: YBDLiveService.instance.data.gameRecord,
          // );
          if (widget.gameType.index > WebGameType.Default.index) {
            // 游戏加载成功
            _loadingResourceTrace.stop();
          }
        },
        userInfoCallback: (jsCode) {
          logger.v('user info call back js code : $jsCode');
          _webViewController!.evaluateJavascript(jsCode);
        },
        uiCallback: (a) {
          setState(() {
            this.resizeToAvoidBottomInset = (a as YBDWebUIModel).resizeToAvoidBottomInset;
            this.bottom = a.bottom;
          });
        },
        jsLoadBeansCallback: (jsCode) {
          _webViewController!.evaluateJavascript('onGetBalance($jsCode)');
        },
        jsGameRoomBattleIdCallback: (jsCode) {
          _webViewController!.evaluateJavascript('onGameRoomBattleId($jsCode)');
        },
        jsCloseLoadingCallback: (close) {
          print('22.8.30--close:$close');
          if (close)
            YBDDialogUtil.hideLoading(context);
          else
            YBDDialogUtil.showLoading(context, barrierDismissible: true);
        },
        jsWebViewWillAppearCallback: () {
          _webViewController!.evaluateJavascript('webviewWillAppear()');
        },
        eventCenterShareCallback: (shareMap) {
          _eventCenterShare = Map.from(shareMap);
          setState(() {});
        }, jsCheckUpdate: null);

    /// 监听需要透传的battleId
    logger.v("YBDGameRoomJSChannelEvent add eventbus");
    addSub(eventBus.on<YBDGameRoomJSChannelEvent>().listen((YBDGameRoomJSChannelEvent event) {
      logger.v("YBDGameRoomJSChannelEvent battleId: ${event.gameJSConfig}, eventname: ${event.eventName}");
      switch (event.eventName) {
        case YBDGameRoomJSChannelEvent.deliverBattleId:
          {
            // 透传ludo游戏相关配置信息
            _webViewController!.evaluateJavascript('onGameRoomBattleId(${event.gameJSConfig})');
          }
          break;
        case YBDGameRoomJSChannelEvent.muteGameVoice:
          {
            _webViewController!.evaluateJavascript('onGameRoomMute(${event.isMute})');
          }
          break;
      }
    }));
  }
  void _configJsChannelHandlerEEAZloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    if (widget.gameType.index > WebGameType.Default.index) {
      YBDTALiveRoomTrack()
          .taRoom(event: YBDTATrackEvent.liveroom_game_close, record: YBDLiveService.instance.data.gameRecord);
      // 离开游戏网页
      _playTimeTrace.stop();
    }
    widget.closeCallback?.call();
    logger.v('web view dispose');
    ApiHelper.checkLogin(context).then((YBDLoginRecord? value) {});
    // WebView.platform = null;
    _webViewController = null;
    _channelHandler = null;
    int endTime = DateTime.now().millisecondsSinceEpoch;
    YBDInterceptManager.instance!.updateInterceptWebItem(
      widget.urlStr,
      webUrl,
      YBDInterceptWebItem()..remainTime = endTime - webStartTime,
    );
    YBDInterceptManager.instance!.logMap();
    super.dispose();
  }
  void dispose2meeuoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    BoxDecoration decoration = YBDTPStyle.gradientDecoration;

    if (widget.opaque == false) {
      decoration = BoxDecoration(
        color: Colors.transparent,
      );
    }

    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: _topNavBar() as PreferredSizeWidget?,
      resizeToAvoidBottomInset: this.resizeToAvoidBottomInset,
      body: Builder(
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () {
              return _goBack(context);
            },
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: decoration,
              child: _loadError ? errorPageContent() : webViewContent(),
            ),
          );
        },
      ),
    );
  }
  void myBuild149ghoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Future<bool> _goBack(BuildContext context) async {
    if (_webViewController != null && (await _webViewController!.canGoBack() ?? false)) {
      try {
        logger.v('===web web view go back');
        _webViewController!.goBack();
        return false;
      } catch (e) {
        logger.v('===web go back error $e');
        return true;
      }
    }
    // 退出埋点
    _taActivityWeb(event: YBDTATrackEvent.activity_out);
    return true;
  }
  void _goBackpFfL4oyelive(BuildContext context)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 顶部导航栏
  Widget? _topNavBar() {
    if (widget.showNavBar) {
      return PreferredSize(
        preferredSize: Size.fromHeight(ScreenUtil().setWidth(96)),
        child: AppBar(
          leading: Container(
            width: ScreenUtil().setWidth(88),
            height: ScreenUtil().setWidth(88),
            child: TextButton(
              onPressed: () async {
                logger.v('===web clicked go back btn');
                // 退出埋点
                _taActivityWeb(event: YBDTATrackEvent.activity_out);
                if (_loadError) {
                  // 页面加载报错时网页 goBack 方法不生效
                  logger.v('===web load error pop web view');
                  Navigator.pop(context);
                  return;
                }

                if (_webViewController != null && (await _webViewController!.canGoBack() ?? false)) {
                  try {
                    logger.v('===web web view go back');
                    _webViewController!.goBack();
                  } catch (e) {
                    logger.v('===web go back error $e');
                    Navigator.pop(context);
                  }
                } else {
                  logger.v('===web flutter go back');
                  Navigator.pop(context);
                }
              },
              child: Icon(Icons.arrow_back, color: Colors.white),
            ),
          ),

          actions: <Widget>[
            _navPopMenu(),
          ],
          centerTitle: true,
          // app传值title 和  js传值title 为空时，取页面html <title> 值
          title: Text(
            _jsTitle ?? widget.pageTitle ?? _title ?? '',
            style: TextStyle(color: Colors.white, fontSize: 30.sp2),
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              color: YBDTPStyle.heliotrope,
            ),
          ),
        ),
      );
    } else {
      return null;
    }
  }

  Widget _barIcon(String img, Function onTap) {
    return GestureDetector(
      onTap: onTap as void Function()?,
      child: Container(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 24.px as double, horizontal: 10.px as double),
          child: Image.asset(
            img,
            width: ScreenUtil().setWidth(48),
          ),
        ),
      ),
    );
  }
  void _barIconBp9fDoyelive(String img, Function onTap) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 导航栏下拉菜单
  Widget _navPopMenu() {
    if (_eventCenterShare.isEmpty) {
      return SizedBox();
    }

    return Row(
      children: [
        _barIcon("assets/images/event_center_share.png", () {
          YBDCommonTrack().commonTrack(YBDTAProps(
            location: 'share',
            module: YBDTAModule.event,
            name: _eventCenterShare[JS_SHARE_URL]?.toString().key,
          ));
          showModalBottomSheet(
              context: context,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(ScreenUtil().setWidth(16)),
                  topRight: Radius.circular(ScreenUtil().setWidth(16)),
                ),
              ),
              builder: (BuildContext context) {
                return YBDSharingWidget(
                  adName: _eventCenterShare[JS_SHARE_NAME],
                  adUrl: _eventCenterShare[JS_SHARE_URL],
                  adImg: _eventCenterShare[JS_SHARE_IMG],
                  adDetail: _eventCenterShare[JS_SHARE_DESC],
                );
              });
        }),
        _barIcon("assets/images/event_center_icon.webp", () {
          String routName = YBDNavigatorHelper.event_center;

          if (YBDObsUtil.instance().containsRoute(routName)) {
            YBDNavigatorHelper.popUntilPage(context, routName);
          } else {
            YBDNavigatorHelper.navigateTo(context, routName);
          }
          YBDCommonTrack().commonTrack(YBDTAProps(location: 'back_to_event_center', module: YBDTAModule.event));
          YBDTATrack().trackEvent(
            YBDEventName.CLICK_EVENT,
            prop: YBDTAProps(
              location: YBDLocationName.WEB_PAGE,
              name: 'event_center_icon',
            ),
          );
        }),
        SizedBox(width: 10.px),
      ],
    );
  }
  void _navPopMenuYd3a6oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 错误页面
  Widget errorPageContent() {
    return GestureDetector(
      onTap: () {
        logger.v('clicked web view error view');
        YBDDialogUtil.showLoading(context, barrierDismissible: true);
      },
      child: Container(
        decoration: BoxDecoration(),
        width: double.infinity,
        child: YBDNetworkErrorView(text: translate('tap_retry')),
      ),
    );
  }

  setWebCookie() async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();

    String jsessionId = (await YBDSPUtil.get(Const.SP_JSESSION_ID)) ?? '';
    String cookieApp = (await YBDSPUtil.get(Const.SP_COOKIE_APP)) ?? '';
    logger.v('current web jsessionId: $jsessionId, $cookieApp');
    _webViewController!
        .evaluateJavascript(
            "document.cookie = 'JSESSIONID = $jsessionId ;path=/';document.cookie = 'app = $cookieApp;path=/'")
        .then((String? value) {
      logger.v('current web jsessionId: $value');
    });
  }

  /// 网页
  Widget webViewContent() {
    if (_isInitState) {
      return Container(
        child: YBDLoadingCircle(),
      );
    }
    return YBDColoredSafeArea(
      bottom: this.bottom,
      child: Container(
        /// 添加测试环境透明背景来证明游戏房webview存在
        // color: (Const.TEST_ENV && widget.createdCallback!=null)
        //     ? Colors.red.withOpacity(0.3) : Colors.transparent,
        child: WebView(
          initialUrl: webUrl,
          opaque: widget.opaque,
          // initialUrl: "http://121.37.214.86/lucky1000/index.html",
          // initialUrl: "http://192.168.50.209:8848/practice/c.html",
          // initialUrl: "http://192.168.50.11:3000/index.html",
          // initialUrl: "http://121.37.23.135/talent/",
          // initialUrl: "http://www.oyetalk.live/event/talent/",
          // initialUrl: "http://192.168.50.209:8080/#/",
          // TODO: 测试代码 ios
          // 测试 js 通道的本地链接
          // initialUrl: "http://192.168.50.64:8848/practice/c.html",
          // initialUrl: "http://192.168.50.11:3000/index.html",
          javascriptMode: JavascriptMode.unrestricted,
          debuggingEnabled: Const.TEST_ENV,
          initialMediaPlaybackPolicy: AutoMediaPlaybackPolicy.always_allow,
          // TODO: 测试代码 修改 web view agent
          // userAgent: 'platform:${Platform.isIOS ? 'iOS' : 'Android'};flutter:true;',
          onWebViewCreated: (WebViewController webViewController) {
            if (widget.showStartLoading) YBDDialogUtil.showLoading(context);
            _webViewController = webViewController;
            widget.createdCallback?.call(webViewController);
          },
          javascriptChannels: _channelHandler!.jsChannels(),
          navigationDelegate: (NavigationRequest request) {
            logger.v('===web navigation to ${request.url}');
            if (Platform.isIOS && request.url!.contains('play.google.com')) {
              final String userId = YBDStringUtil.userIdFromUrl(request.url);
              logger.v('===web jump user profile $userId');
              _channelHandler!.jumpToProfile(userId);
              return NavigationDecision.prevent;
            }

            if (request.url!.startsWith('whatsapp:')) {
              logger.v('===web view url : ${request.url}');
              launch(request.url!);
              return NavigationDecision.prevent;
            }

            return NavigationDecision.navigate;
          },
          onPageStarted: (String url) {
            logger.v('page started loading: $url');
            setWebCookie();
          },
          onPageFinished: (String? url) {
            url ??= '';
            int endTime = DateTime.now().millisecondsSinceEpoch;

            logger.v(
                'page finished loading: $url , pageTitle: ${widget.pageTitle} , jsTitle: $_jsTitle, loadTime: ${endTime - webStartTime}ms');
            // 加载完成埋点
            _taActivityWeb(event: YBDTATrackEvent.activity_load_finish);
            tryCatch(() {
              if (_isGame()) {
                // 语音房游戏加载时长由web端埋点
                // YBDLoadTrack().loadingTrack(YBDTAProps(
                //   type: YBDTATimeTrackType?.liveroom_game_load,
                //   id: YBDLiveService.instance.data.gameRecord?.id?.toString(),
                //   name: YBDLiveService.instance.data.gameRecord?.name,
                // ));
              } else {
                YBDLoadTrack().loadingTrack(YBDTAProps(type: YBDTATimeTrackType.event_load, id: url, name: url?.key));
              }
            });

            YBDInterceptManager.instance!.updateInterceptWebItem(
              widget.urlStr,
              webUrl,
              YBDInterceptWebItem()
                ..url = url
                ..time = endTime - webStartTime
                ..bytes = 0,
            );

            widget.finishedCallback?.call();
            YBDDialogUtil.hideLoading(context);
            _loadError = false;

            if (mounted) {
              setState(() {});
            }

            // TODO: 测试代码
            // _webViewController.evaluateJavascript("window.haha = 'aaa'");
            // _webViewController.evaluateJavascript("sessionStorage.setItem('c', '666')");
            // 未设置页面Title时，获取HTML页面title
            if (widget.pageTitle == null && _jsTitle == null && _webViewController != null) {
              _webViewController!.getTitle().then((String? value) {
                logger.v('_webViewController.getTitle, title: $value');
                if (mounted) {
                  setState(() {});
                }
              });
            }
          },
          onWebResourceError: (WebResourceError error) {
            logger.v('load web view error : ${error.description}');
            // 加载失败埋点
            _taActivityWeb(event: YBDTATrackEvent.activity_load_fail, reason: error.description);
            if (_isGame()) {
              YBDTALiveRoomTrack().taRoom(
                event: YBDTATrackEvent.liveroom_game_load_fail,
                record: YBDLiveService.instance.data.gameRecord,
                reason: error.description,
              );
            }
            // YBDDialogUtil.hideLoading(context);
            // _loadError = true;
            //
            // if (mounted) {
            //   setState(() {});
            // }
          },
          shouldInterceptRequest: (String? url) => _shouldInterceptRequest(url ?? ''),
          gestureNavigationEnabled: true,
        ),
      ),
    );
  }
  void webViewContenti6C48oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 请求拦截
  Future<Response?> _shouldInterceptRequest(String url) async {
    if (!YBDInterceptManager.shouldIntercept(url)) return null;
    return YBDInterceptManager.shouldInterceptRequest(url, widget.urlStr, webUrl);
  }

  ///url 如果是ep充值，加app版本
  _getWebUrl(String url) async {
    // 防止有空格或中文无法加载地址的问题
    url = Uri.encodeFull(url);

    webUrl = url;

    ///充值活动 ep加app版本
    if (url.contains('EasypaisaTopup')) {
      /* YBDAppVersionConfig appVersionConfig = await ConfigUtil.appVersionInfo(context);
      YBDLogUtil.v("_getWebUrl EasypaisaTopup：" + url + '?version=${appVersionConfig.buildNumber}');
      webUrl = url + '?version=${appVersionConfig.buildNumber}';*/

      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      webUrl = url + '&version=${packageInfo.buildNumber}&ot-ipl=${await YBDSPUtil.get(Const.SP_IP_COUNTRY_CODE)}';
    }
    YBDLogUtil.v("_getWebUrl not EasypaisaTopup：" + webUrl!);
    _isInitState = false;
    if (mounted) setState(() {});
  }

  static reloadWebViewURL(WebViewController webViewController, String newUrl) async {
    if (webViewController == null || newUrl == null) {
      logger.v("loadWebViewURL fail! controller: $webViewController, newUrl: $newUrl");
      return;
    }

    /// 追加app信息
    newUrl = await newUrl.webCookie();
    String? preUrl = await webViewController.currentUrl();
    if (newUrl != preUrl) {
      /// 加载新的url
      webViewController.loadUrl(newUrl);
      logger.v("loadWebViewURL load newUrl: $newUrl");
    }
  }

  /// 数数活动埋点
  void _taActivityWeb({String? event, String? reason}) {
    alog.v('ta web view Buried event:$event entranceType:${widget.entranceType}');
    if (widget.entranceType != WebEntranceType.Default) {
      YBDTAProps prop = YBDTAProps();
      prop.url = widget.urlStr;
      prop.name = widget.urlStr?.key;
      if (reason != null) prop.reason = reason;
      YBDTATrack().trackEvent(event, prop: prop);
      if (event == YBDTATrackEvent.activity_out) {
        YBDStayTrack().stayTrack(YBDTATimeTrackType.activity_stay_time, prop);
      }
    }
  }
  void _taActivityWebneyb5oyelive({String? event, String? reason}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// web游戏
  bool _isGame() {
    return widget.gameType.index > WebGameType.Default.index;
  }
}
