import 'dart:async';


import 'dart:math';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/home/widget/pk_ybd_label_item.dart';
import 'package:oyelive_main/ui/page/home/widget/room_ybd_people_amount.dart';
import 'package:oyelive_main/ui/page/home/widget/super_ybd_star_item.dart';
import 'package:oyelive_main/ui/page/home/widget/top_ybd_talent_item.dart';
import 'package:oyelive_main/ui/page/home/widget/weekly_ybd_star_item.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/intl_input/countries.dart';
import 'package:oyelive_main/ui/widget/room_ybd_category_widget.dart';
import 'package:oyelive_main/ui/widget/room_ybd_level_tag.dart';

typedef void TapCallback(YBDRoomInfo? info);

/// 主页面网格列表项
class YBDHomeGridItem extends StatefulWidget {
  /// 房间信息
  final YBDRoomInfo? data;

  /// weekly star 房间列表
  List<String> superRooms;

  /// super star 列表
  List<int?>? superStars;

  /// 点击事件回调
  final TapCallback tapCallback;

  YBDHomeGridItem(this.data, this.superRooms, this.superStars, this.tapCallback);

  @override
  YBDHomeGridItemState createState() => new YBDHomeGridItemState();
}

class YBDHomeGridItemState extends State<YBDHomeGridItem> {
  // Map<String, String> _selectedCountry = countries.firstWhere((item) => item['code'] == 'US');
  // List<Map<String, String>> filteredCountries = countries;

  @override
  Widget build(BuildContext context) {
    // 是否为 weekly star 房间
    bool isWeeklyStar = false;

    if (null != widget.superRooms && widget.superRooms.isNotEmpty) {
      if (null != widget.data && widget.superRooms.contains('${widget.data!.id}')) {
        logger.v('is weekly star room : ${widget.data!.id}');
        isWeeklyStar = true;
      }
    }

    // 是否加锁
    bool isLocked = false;

    if (null != widget.data && widget.data!.protectMode == 1) {
      logger.v('room ${widget.data!.id} is locked');
      isLocked = true;
    }

    return Container(
      decoration: _boxDecoration(),
      child: Material(
        color: Colors.transparent,
        child: YBDDelayGestureDetector(
          onTap: () {
            logger.v('clicked grid view item');
            widget.tapCallback(widget.data);
          },
          child: InkWell(
            borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
            // onTap: () {
            //   logger.v('clicked grid view item');
            //   widget.tapCallback(widget.data);
            // },
            child: Stack(children: [
              Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setWidth(10)),
                  Row(
                    // 人数和房间类型
                    children: <Widget>[
                      SizedBox(width: ScreenUtil().setWidth(10)),
                      YBDRoomPeopleAmount(widget.data),
                      Expanded(child: SizedBox(width: 1)),
                      YBDRoomCategoryWidget(widget.data!.roomCategory),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setWidth(10)),
                  Row(
                    // Weekly Star 标签
                    children: <Widget>[
                      Expanded(child: SizedBox(width: 1)),
                      if (widget.data!.playingLudo ?? false) ...[
                        YBDNetworkImage(
                          imageUrl: widget.data!.ludoIconUrl ?? "",
                          width: ScreenUtil().setWidth(70),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(10)),
                      ],
                      if (isWeeklyStar) ...[
                        YBDWeeklyStarItem(),
                        SizedBox(width: ScreenUtil().setWidth(10)),
                      ]
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setWidth(10)),
                  Row(
                    // Weekly Star 标签
                    children: <Widget>[
                      Expanded(child: SizedBox(width: 1)),
                      YBDSuperStarItem(roomId: widget.data!.id, superStars: widget.superStars),
                      SizedBox(width: ScreenUtil().setWidth(10)),
                    ],
                  ),
                  // Container(
                  //   alignment: Alignment.centerLeft,
                  //   child: YBDTopTalentItem(widget.data.aRanking ?? 0,width: ScreenUtil().setWidth(106),height: ScreenUtil().setWidth(46),font: 13,),
                  // ),
                  Expanded(child: SizedBox(height: 1)),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
                    width: double.infinity,
                    height: ScreenUtil().setWidth(148),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.vertical(bottom: Radius.circular(ScreenUtil().setWidth(16))),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.white.withOpacity(0), Colors.black.withOpacity(0.79)],
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              // 房间名称
                              width: ScreenUtil().setWidth(280),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(height: ScreenUtil().setWidth(20)),
                                  Text(
                                    widget.data!.roomTitle!.isNotEmpty ? widget.data!.roomTitle! : '-',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: ScreenUtil().setSp(22),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(child: SizedBox(width: 1)),
                            Column(
                              children: <Widget>[
                                // 房间加锁的标记
                                isLocked
                                    ? Image.asset(
                                        'assets/images/liveroom/room_icon_lock.png',
                                        width: ScreenUtil().setWidth(44),
                                        height: ScreenUtil().setWidth(44),
                                      )
                                    : Container(height: ScreenUtil().setWidth(44)),
                                SizedBox(height: ScreenUtil().setWidth(30)),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          // 主播名称，国旗，级别
                          children: <Widget>[
                            Container(
                              width: ScreenUtil().setWidth(228),
                              child: Text(
                                // 昵称
                                widget.data!.nickname ?? '-',
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: ScreenUtil().setSp(22),
                                ),
                              ),
                            ),
                            Expanded(child: SizedBox(width: 1)),
//                        Text(
//                          // TODO: 对接口显示国旗
//                          _selectedCountry['flag'],
//                          style: TextStyle(fontSize: ScreenUtil().setSp(40)),
//                        ),
                            Expanded(child: SizedBox(width: ScreenUtil().setWidth(1))),
                            YBDRoomLevelTag(widget.data!.roomlevel, addHorizontalMargin: false),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setWidth(10)),
                      ],
                    ),
                  ),
                ],
              ),
              Positioned(
                top: 20,
                child: Container(
                  child: YBDTopTalentItem(
                    widget.data!.aRanking ?? 0,
                    // 1,
                    width: ScreenUtil().setWidth(106),
                    height: ScreenUtil().setWidth(46),
                    font: 13,
                  ),
                ),
              ),
              //* pk标签 要先判断是否在pk 然后判断前面是否有最热主播标签
              widget.data!.isPking ?? false
                  ? Positioned(
                      left: ScreenUtil().setWidth(widget.data!.aRanking! > 0 ? 107 : 13),
                      top: ScreenUtil().setWidth(50),
                      child: YBDPKLabelItem(
                        width: ScreenUtil().setWidth(90),
                        height: ScreenUtil().setWidth(35),
                      ))
                  : Container(),
            ]),
          ),
        ),
      ),
    );
  }
  void buildMIzkaoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 网格背景
  BoxDecoration _boxDecoration() {
    final imgUrl = YBDImageUtil.cover(
      context,
      widget.data!.roomimg,
      widget.data!.id,
      scene: 'D',
    );

    if (imgUrl.isNotEmpty) {
      return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
        color: Color(0x33ffffff),
        image: DecorationImage(
            image: NetworkImage(
              imgUrl,
            ),
            fit: BoxFit.cover),
      );
    } else {
      return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16))),
        color: Color(0x33ffffff),
      );
    }
  }
}
