import 'dart:async';


import 'dart:async';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_skin.dart';

import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/image_ybd_util.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../../module/user/util/user_ybd_util.dart';
import '../../status/local_audio/scale_ybd_animate_button.dart';
import '../bloc/special_ybd_list_bloc.dart';
import 'explore_ybd_special_avatar.dart';

/// Special 列表项
class YBDExploreSpecialItem extends StatefulWidget {
  /// 列表项数据
  final YBDRoomInfo? data;

  YBDExploreSpecialItem(this.data);

  @override
  YBDExploreSpecialItemState createState() => new YBDExploreSpecialItemState();
}

class YBDExploreSpecialItemState extends State<YBDExploreSpecialItem> {
  /// follow 按钮不可点击
  bool _disableFollow = false;

  @override
  Widget build(BuildContext context) {
    num bgImgHeight = 90;
    num avatarHeight = 60;
    num avatarTop = bgImgHeight - avatarHeight / 2;
    return Container(
      child: Stack(
        children: <Widget>[
          Column(
            // 圆形头像底部的布局
            children: <Widget>[
//              Container(
//                // 背景图
//                height: ScreenUtil().setWidth(bgImgHeight),
//                decoration: BoxDecoration(
//                  borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(20))),
//                  image: DecorationImage(
//                    image: NetworkImage(
//                      YBDImageUtil.cover(
//                            context,
//                            widget.data.roomimg,
//                            widget.data.id,
//                            scene: 'C',
//                          ) ??
//                          '',
//                    ),
//                    fit: BoxFit.cover,
//                  ),
//                ),
//              ),
              ClipRRect(
                borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(20))),
                child: YBDNetworkImage(
                  height: ScreenUtil().setWidth(bgImgHeight),
                  width: double.maxFinite,
                  imageUrl: YBDImageUtil.cover(
                        context,
                        widget.data!.roomimg,
                        widget.data!.id,
                        scene: 'C',
                      ),
                  fit: BoxFit.cover,
                  placeholder: (_, x) {
                    return Container(
                      height: ScreenUtil().setWidth(bgImgHeight),
                      decoration: BoxDecoration(color: Colors.white.withOpacity(0.2)),
                    );
                  },
                  errorWidget: (_, x, e) => Container(
                    height: ScreenUtil().setWidth(bgImgHeight),
                    decoration: BoxDecoration(color: Colors.white.withOpacity(0.2)),
                  ),
                ),
              ),
              Padding(
                // 昵称
                padding: EdgeInsets.only(top: ScreenUtil().setWidth(50)),
                child: Container(
                  height: ScreenUtil().setWidth(30),
                  padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
                  child: Text(
                    widget.data!.nickname ?? '-',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: YBDActivitySkinRoot().curAct().activityTextColor(),
                      fontSize: ScreenUtil().setSp(22),
                    ),
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setWidth(15)),
              YBDScaleAnimateButton(
                // Follow 按钮
                onTap: () {
                  logger.v('clicked follow button');
                  if (_disableFollow) {
                    logger.v('disabled follow button');
                  } else {
                    context.read<YBDSpecialListBloc>().followUser(widget.data!.id);
                    _disableFollow = true;
                    Timer(Duration(seconds: 2), () {
                      _disableFollow = false;
                    });
                  }
                },
                child: Container(
                  width: ScreenUtil().setWidth(130),
                  height: ScreenUtil().setWidth(48),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    gradient: LinearGradient(
                      colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  child: Center(
                    child: Text(translate('follow'),
                        style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(22))),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            // YBDSpecialItemAvatar 与顶部的偏移量
            padding: EdgeInsets.only(top: ScreenUtil().setWidth(avatarTop)),
            child: Row(
              children: <Widget>[
                Expanded(child: SizedBox(width: 1)),
                YBDScaleAnimateButton(
                  onTap: () {
                    logger.v('clicked user avatar');
                    if (widget.data!.id == YBDUserUtil.getLoggedUserID(context)) {
                      logger.v('jump to my profile page');
                      YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.myprofile);
                    } else {
                      logger.v('jump to other user profile page : ${widget.data!.id}');
                      YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.user_profile + "/${widget.data!.id}");
                    }
                  },
                  child: YBDSpecialItemAvatar(
                    // 圆形头像
                    YBDImageUtil.avatar(
                          context,
                          widget.data!.headimg,
                          widget.data!.id,
                          scene: 'B',
                        ),
                    widget.data!.level,
                    ScreenUtil().setWidth(avatarHeight),
                  ),
                ),
                Expanded(child: SizedBox(width: 1)),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void buildYKmXzoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
