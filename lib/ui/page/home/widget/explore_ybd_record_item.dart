import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_skin.dart';

import '../../../../common/navigator/navigator_ybd_helper.dart';
import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/entity/room_ybd_info_entity.dart';
import '../../../widget/button/delay_ybd_ink_well.dart';
import '../../../widget/round_ybd_avatar.dart';

enum RankType {
  Receivers,
  Gifters,
  Followers,
  Combo,
}

/// 主页 Explore Record 列表项
class YBDExploreRecordItem extends StatefulWidget {
  /// item 的类型
  final RankType type;

  /// 排行榜数据
  final List<YBDRoomInfo?>? rankList;

  YBDExploreRecordItem(this.type, this.rankList);

  @override
  YBDExploreRecordItemState createState() => new YBDExploreRecordItemState();
}

class YBDExploreRecordItemState extends State<YBDExploreRecordItem> {
  @override
  Widget build(BuildContext context) {
    String? firstHeadImg = '';
    int? firstId = 0;
    String? secondHeadImg = '';
    int? secondId = 0;
    String? thirdHeadImg = '';
    int? thirdId = 0;

    if (null != widget.rankList && widget.rankList!.length > 0) {
      firstHeadImg = widget.rankList![0]!.headimg;
      firstId = widget.rankList![0]!.id;
    }

    if (null != widget.rankList && widget.rankList!.length > 1) {
      secondHeadImg = widget.rankList![1]!.headimg;
      secondId = widget.rankList![1]!.id;
    }

    if (null != widget.rankList && widget.rankList!.length > 2) {
      thirdHeadImg = widget.rankList![2]!.headimg;
      thirdId = widget.rankList![2]!.id;
    }

    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
//        image: DecorationImage(
//          image: AssetImage(getItemBg()),
//          fit: BoxFit.cover,
//
//        ),
          color: YBDActivitySkinRoot().curAct().exploreRecordBgColor()),
      child: Material(
        color: Colors.transparent,
        child: YBDDelayInkWell(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
          onTap: () {
            logger.v('clicked record item');
            if (widget.type == RankType.Combo) {
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.combo_list);
            } else {
              logger.v('go to leader boarder page');
              YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.leaderboard + "/${getLeaderboardCategory()}");
            }
          },
          child: Row(
//            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(width: ScreenUtil().setWidth(28)),
              Container(
                width: ScreenUtil().setWidth(46),
                child: Image.asset(
                  getItemIcon(),
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(8)),
              Text(
                getItemTitle(),
                style: TextStyle(
                  color: YBDActivitySkinRoot().curAct().activityTextColor(),
                  fontSize: ScreenUtil().setSp(24),
                ),
              ),
              Spacer(),
              Container(
//                height: ScreenUtil().setWidth(100),
                padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(5)),
                child: Stack(
                  children: <Widget>[
//                    Expanded(child: SizedBox(width: ScreenUtil().setWidth(12))),
                    YBDRoundAvatar(
                      firstHeadImg,
                      userId: firstId,
                      scene: 'B',
                      avatarWidth: 64,
                      needNavigation: false,
                    ),
                    // v2.0 只显示一个头像
                    // Container(
                    //   margin: EdgeInsets.only(left: ScreenUtil().setWidth(36)),
                    //   child: YBDRoundAvatar(
                    //     secondHeadImg,
                    //     userId: secondId,
                    //     scene: 'B',
                    //     avatarWidth: 46,
                    //     needNavigation: false,
                    //   ),
                    // ),
                    // Container(
                    //   margin: EdgeInsets.only(left: ScreenUtil().setWidth(72)),
                    //   child: YBDRoundAvatar(
                    //     thirdHeadImg,
                    //     userId: thirdId,
                    //     scene: 'B',
                    //     avatarWidth: 46,
                    //     needNavigation: false,
                    //   ),
                    // ),
                  ],
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(38)),
            ],
          ),
        ),
      ),
    );
  }
  void buildcNgjWoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item 名称
  String getItemTitle() {
    switch (widget.type) {
      case RankType.Receivers:
        return 'Receivers';
      case RankType.Gifters:
        return 'Gifters';
      case RankType.Followers:
        return 'Followers';
      case RankType.Combo:
        return 'Combo';
      default:
        logger.v('${widget.type} is not exist');
        return '';
    }
  }
  void getItemTitlefUuKioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item 分类
  String getItemCategory() {
    switch (widget.type) {
      case RankType.Receivers:
        return 'coins';
      case RankType.Gifters:
        return 'topGifters';
      case RankType.Followers:
        return 'trending';
      case RankType.Combo:
        // TODO: jackie - combo 不用跳转到原生不需要这个字段
        return 'combo';
      default:
        logger.v('${widget.type} is not exist');
        return '';
    }
  }
  void getItemCategoryheJxnoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item 分类
  String getLeaderboardCategory() {
    switch (widget.type) {
      case RankType.Receivers:
        return '0';
      case RankType.Gifters:
        return '1';
      case RankType.Followers:
        return '2';
      default:
        return '0';
    }
  }
  void getLeaderboardCategoryV9h8yoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// item 背景图
  String getItemIcon() {
    switch (widget.type) {
      case RankType.Receivers:
        return 'assets/images/record/record_receiver.webp';
      case RankType.Gifters:
        return 'assets/images/record/record_gifter.webp';
      case RankType.Followers:
        return 'assets/images/record/record_follower.webp';
      case RankType.Combo:
        return 'assets/images/record/record_combo.webp';
      default:
        logger.v('${widget.type} is not exist');
        return '';
    }
  }
  void getItemIconKhuA9oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
