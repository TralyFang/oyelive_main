import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/log_ybd_util.dart';

/// 主页列表头，用来切换列表样式的
class YBDHomeListHeader extends StatefulWidget {
  /// 点击改变样式按钮的回调函数
  final VoidCallback callbackChangedStyle;

  /// 表头标题
  final String title;

  YBDHomeListHeader(this.callbackChangedStyle, {this.title: ''});

  @override
  YBDHomeListHeaderState createState() => YBDHomeListHeaderState();
}

class YBDHomeListHeaderState extends BaseState<YBDHomeListHeader> {
  @override
  Widget myBuild(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(48),
      child: Row(
        children: <Widget>[
          SizedBox(width: ScreenUtil().setWidth(26)),
          Text(
            widget.title,
            style: TextStyle(
              color: YBDActivitySkinRoot().curAct().activityTextColor(),
              fontWeight: FontWeight.normal,
              fontSize: ScreenUtil().setSp(24),
            ),
          ),
          Expanded(child: SizedBox(width: 1)),
          SizedBox(
            width: ScreenUtil().setWidth(70),
            height: ScreenUtil().setWidth(48),
            child: TextButton(
              style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.zero),
              ),
              onPressed: () {
                logger.v('clicked list style button');
                widget.callbackChangedStyle();
              },
              child: Image.asset(
                'assets/images/icon_grid.webp',
                width: ScreenUtil().setWidth(32),
                color: YBDActivitySkinRoot().curAct().activityTextColor(),
              ),
            ),
          ),
          SizedBox(width: ScreenUtil().setWidth(20))
        ],
      ),
    );
  }
  void myBuildPbSUToyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
