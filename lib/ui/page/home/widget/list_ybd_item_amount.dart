import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../module/entity/room_ybd_info_entity.dart';

/// 线性列表房间人数和音乐动图
class YBDListItemAmount extends StatefulWidget {
  /// item 数据
  final YBDRoomInfo? data;

  YBDListItemAmount(this.data);

  @override
  _YBDListItemAmountState createState() => new _YBDListItemAmountState();
}

class _YBDListItemAmountState extends State<YBDListItemAmount> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(50),
      child: Row(
        children: <Widget>[
          Image.asset(
            // 音乐动图
            'assets/images/icon_live.webp',
            width: ScreenUtil().setWidth(36),
            height: ScreenUtil().setWidth(33),
          ),
          Container(
            // 房间人数
            padding: EdgeInsets.fromLTRB(
              ScreenUtil().setWidth(10),
              0,
              0,
              ScreenUtil().setWidth(7),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  widget.data!.count?.toString() ?? '-',
                  // 可以限制最大宽度
                  // constraints: BoxConstraints(
                  // maxHeight: maxWidth: ScreenUtil().setWidth(450),
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.9),
                    fontSize: ScreenUtil().setSp(24),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  void buildsSv47oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
