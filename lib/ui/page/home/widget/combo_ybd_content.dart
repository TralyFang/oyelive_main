import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../common/util/log_ybd_util.dart';
import '../../../../module/api_ybd_helper.dart';
import '../../../widget/loading_ybd_circle.dart';
import '../../../widget/my_ybd_refresh_indicator.dart';
import '../../follow/widget/place_ybd_holder_view.dart';
import '../entity/combo_ybd_rank_entity.dart';
import 'combo_ybd_list_item.dart';

enum ComboRankType {
  // 周榜
  Weekly,
  // 月榜
  Monthly,
  // 全榜
  Total,
}

/// combo 列表页面的 tab 内容
class YBDComboContent extends StatefulWidget {
  /// combo 排行榜类别
  final ComboRankType type;

  YBDComboContent(this.type);

  @override
  _YBDComboContentState createState() => _YBDComboContentState();
}

class _YBDComboContentState extends State<YBDComboContent> with AutomaticKeepAliveClientMixin {
  /// combo 列表数组
  List<YBDComboRankRecordRank?>? _data;

  /// 是否为初始状态
  bool _isInitState = true;

  /// 列表刷新控制器
  RefreshController _refreshController = RefreshController();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _onRefresh();
  }
  void initStateDJk7Poyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (null == _data || _data!.isEmpty) {
      logger.v('combo list is empty show placeholder');
      return _isInitState ? YBDLoadingCircle() : YBDPlaceHolderView(text: translate('no_data'));
    }

    return Container(
      child: SmartRefresher(
        controller: _refreshController,
        header: YBDMyRefreshIndicator.myHeader,
        enablePullDown: true,
        enablePullUp: false,
        onRefresh: () {
          logger.v("combo page pull down refresh");
          _onRefresh();
        },
        child: ListView.builder(
          itemCount: _data!.length, // 数据的数量
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                logger.v('clicked combo item');
              },
              child: Container(
                width: ScreenUtil().screenWidth,
                height: ScreenUtil().setWidth(328),
                padding: EdgeInsets.fromLTRB(
                  ScreenUtil().setWidth(25),
                  ScreenUtil().setWidth(0),
                  ScreenUtil().setWidth(25),
                  ScreenUtil().setWidth(20),
                ),
                child: YBDComboListItem(context, _data![index]),
              ),
            );
          }, // 类似 cellForRow 函数
        ),
      ),
    );
  }
  void buildO8hpVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  /// 列表刷新响应方法
  _onRefresh() async {
    // 查询的排行榜类 1-总榜， 2-30天榜，3-七天榜
    int timeType = 1;

    switch (widget.type) {
      case ComboRankType.Weekly:
        timeType = 3;
        break;
      case ComboRankType.Monthly:
        timeType = 2;
        break;
      case ComboRankType.Total:
        timeType = 1;
        break;
    }

    ApiHelper.comboRank(context, timeType).then((comboRankEntity) {
      if (comboRankEntity == null) {
        logger.v('top up gift entity is null');
      } else {
        _data = comboRankEntity.record!.rank;
      }
      _refreshController.refreshCompleted();
      _cancelInitState();
    });
  }

  /// 取消初始状态并刷新页面
  _cancelInitState() {
    if (mounted) {
      setState(() {
        _isInitState = false;
      });
    } else {
      _isInitState = false;
    }
  }
}
