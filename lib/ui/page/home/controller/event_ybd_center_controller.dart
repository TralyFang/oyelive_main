import 'dart:async';


import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/ui/page/home/controller/event_ybd_dot_controller.dart';
import 'package:oyelive_main/ui/page/home/entity/event_ybd_center_entity.dart';

import '../widget/event_center/event_ybd_center_view.dart';
import 'custom_ybd_type.dart';

class YBDEventCenterController extends GetxController {
  static const int pageSize = 10;

  /// tab列表的数据
  List<YBDEventCenterDataEventInfos>? _inProgressList;
  List<YBDEventCenterDataEventInfos>? _comingSoonList;
  List<YBDEventCenterDataEventInfos>? _endedList;

  /// 请求分页数据要用到，进行中的活动不分页
  int _endedPageIndex = 1;
  bool _noMoreEndedData = false;
  int _comingSoonIndex = 1;
  bool _noMoreComingSoonData = false;

  /// 当前选中的标签页
  int _selectedIndex = -1;
  set tabIndex(int index) {
    if (_selectedIndex != index) {
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
        YBDEventName.CLICK_EVENT,
        location: YBDLocationName.EVENT_CENTER_PAGE,
        itemName: 'index_$index',
      ));
    }
  }

  List<YBDEventCenterDataEventInfos>? getDataWithIndex(EventCenterTabIndex? tabIndex) {
    switch (tabIndex) {
      case EventCenterTabIndex.InProgress:
        return _inProgressList;
      case EventCenterTabIndex.ComingSoon:
        return _comingSoonList;
      case EventCenterTabIndex.Ended:
        return _endedList;
      default:
        return [];
    }
  }

  void _setDataWithIndex(EventCenterTabIndex? tabIndex, List<YBDEventCenterDataEventInfos?> data) {
    switch (tabIndex) {
      case EventCenterTabIndex.InProgress:
        _inProgressList = List.from(data);

        // 记录在线活动列表
        Get.find<YBDEventDotController>().addApiEvents(_inProgressList);
        break;
      case EventCenterTabIndex.ComingSoon:
        _comingSoonList = List.from(data);
        break;
      case EventCenterTabIndex.Ended:
        _endedList = List.from(data);
        break;
      default:
        break;
    }
  }
  void _setDataWithIndexAsdaZoyelive(EventCenterTabIndex? tabIndex, List<YBDEventCenterDataEventInfos?> data) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  int _getPageIndex(EventCenterTabIndex? tabIndex) {
    switch (tabIndex) {
      case EventCenterTabIndex.InProgress:
        return 1;
      case EventCenterTabIndex.ComingSoon:
        return _comingSoonIndex;
      case EventCenterTabIndex.Ended:
        return _endedPageIndex;
      default:
        return 1;
    }
  }
  void _getPageIndexl0b1eoyelive(EventCenterTabIndex? tabIndex) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _setPageIndex(EventCenterTabIndex? tabIndex) {
    switch (tabIndex) {
      case EventCenterTabIndex.InProgress:
        break;
      case EventCenterTabIndex.ComingSoon:
        _comingSoonIndex++;
        break;
      case EventCenterTabIndex.Ended:
        _endedPageIndex++;
        break;
      default:
        break;
    }
  }
  void _setPageIndexA8cvjoyelive(EventCenterTabIndex? tabIndex) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  bool getNoMoreDataWithIndex(EventCenterTabIndex? tabIndex) {
    switch (tabIndex) {
      case EventCenterTabIndex.InProgress:
        return true;
      case EventCenterTabIndex.ComingSoon:
        return _noMoreComingSoonData;
      case EventCenterTabIndex.Ended:
        return _noMoreEndedData;
      default:
        return true;
    }
  }
  void getNoMoreDataWithIndexcH5pooyelive(EventCenterTabIndex? tabIndex) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _setNoMoreDataWithIndex(
    EventCenterTabIndex? tabIndex,
    int actualDataLength,
    int expectedLength,
  ) {
    switch (tabIndex) {
      case EventCenterTabIndex.InProgress:
        break;
      case EventCenterTabIndex.ComingSoon:
        _noMoreComingSoonData = actualDataLength < expectedLength;
        break;
      case EventCenterTabIndex.Ended:
        _noMoreEndedData = actualDataLength < expectedLength;
        break;
      default:
        break;
    }
  }

  /// [EventCenterTabIndex.ComingSoon]和[EventCenterTabIndex.Ended]无更多数据时显示自定义footer
  bool showCustomFooter(EventCenterTabIndex? tabIndex) {
    switch (tabIndex) {
      case EventCenterTabIndex.InProgress:
        return false;
      case EventCenterTabIndex.ComingSoon:
      case EventCenterTabIndex.Ended:
        if (getNoMoreDataWithIndex(tabIndex)) {
          // 无更多数据时显示自定义footer
          return true;
        }

        return false;
      default:
        return false;
    }
  }
  void showCustomFooterCu8NBoyelive(EventCenterTabIndex? tabIndex) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 列表是否分页
  bool enablePage(EventCenterTabIndex? tabIndex) {
    switch (tabIndex) {
      case EventCenterTabIndex.InProgress:
        return false;
      case EventCenterTabIndex.ComingSoon:
      case EventCenterTabIndex.Ended:
        if (getNoMoreDataWithIndex(tabIndex)) {
          // 无更多数据时显示自定义footer, 不分页
          return false;
        }

        return true;
      default:
        return false;
    }
  }

  Future<int> requestEventList({
    EventCenterTabIndex? tabIndex,
    int? start,
    int? offset,
    bool isLoadMore = false,
  }) async {
    int? queryStart = start;
    int? queryOffset = offset;

    switch (tabIndex) {
      case EventCenterTabIndex.InProgress:
        break;
      case EventCenterTabIndex.ComingSoon:
      case EventCenterTabIndex.Ended:
        queryOffset = offset ?? pageSize;
        break;
      default:
        break;
    }

    List<YBDEventCenterDataEventInfos?> data = await _apiHelper(
      eventStatus: tabIndex.value,
      start: queryStart,
      offset: queryOffset,
    ) ?? [];

    if (data.isNotEmpty) {
      _setPageIndex(tabIndex);
    }

    if (isLoadMore) {
      List<YBDEventCenterDataEventInfos?> allData = [];
      allData.addAll(getDataWithIndex(tabIndex)!);
      allData.addAll(data);
      _setDataWithIndex(tabIndex, allData);
      update();
      return data.length;
    } else {
      _setDataWithIndex(tabIndex, data);
      update();
      return data.length;
    }
  }

  /// 单元测试要用到
  static bool mockApi = false;
  static dynamic mockData;
  Future<List<YBDEventCenterDataEventInfos?>?> _apiHelper({
    int? start,
    int? offset,
    String? eventStatus,
  }) async {
    if (mockApi) {
      // 模拟数据
      return mockData;
    }

    return await ApiHelper.queryEventCenterList(
      start: start ?? 1,
      offset: offset ?? 50,
      eventStatus: eventStatus,
    );
  }

  Future<void> nextPage({EventCenterTabIndex? tabIndex}) async {
    switch (tabIndex) {
      case EventCenterTabIndex.InProgress:
        break;
      case EventCenterTabIndex.ComingSoon:
      case EventCenterTabIndex.Ended:
        int start = _getPageIndex(tabIndex);
        int offset = pageSize;

        await requestEventList(
          tabIndex: tabIndex,
          start: start,
          offset: offset,
          isLoadMore: true,
        );

        _setNoMoreDataWithIndex(
          tabIndex,
          getDataWithIndex(tabIndex)!.length,
          offset * start,
        );

        update();
        break;
      default:
        break;
    }
  }
  void nextPagexHLjSoyelive({EventCenterTabIndex? tabIndex})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// tab 个数
  static const int kTabLength = 3;

  /// 数组长度和[kTabLength]保持一致
  List<String> tabList() {
    return <String>[
      translate('in_progress'),
      translate('coming_soon'),
      translate('ended'),
    ];
  }

  /// 数组长度和[kTabLength]保持一致
  List<YBDEventCenterView> tabViewList() {
    return <YBDEventCenterView>[
      YBDEventCenterView(tabIndex: EventCenterTabIndex.InProgress),
      YBDEventCenterView(tabIndex: EventCenterTabIndex.ComingSoon),
      YBDEventCenterView(tabIndex: EventCenterTabIndex.Ended),
    ];
  }
}
