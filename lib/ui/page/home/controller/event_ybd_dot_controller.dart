import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/ui/page/home/entity/event_ybd_center_entity.dart';

/// 根据本地记录判断是否有未读活动，如果有则显示红点
/// 1. 请求活动列表数据 + 推送的新活动ID
///   a. 刷新进行中活动 [addApiEvents]
///   b. 刷新已读活动并记录 [_deleteUnOnlineEvents]
///   c. 刷新页面红点 [showRedDot]
/// 2. 点击活动详情
///   a. 刷新已读活动并记录 [addRead]
///   c. 刷新页面红点 [showRedDot]

class YBDEventDotController extends GetxController {
  /// 请求活动列表和点击活动详情时回调
  /// 在回调里刷新页面 [showRedDot]
  RxBool showEventRed = false.obs;

  /// sp中没有保存活动列表
  RxBool eventListNull = false.obs;

  /// 没有保存过活动中心列表记录
  Future<void> checkEventList() async {
    var isNull = await YBDSPUtil.getStringList(Const.SP_KEY_ONLINE_LIST) == null;
    eventListNull.value = isNull;
    update();
  }
  void checkEventListGAtmdoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 有未读活动时显示红点
  Future<bool> showRedDot() async {
    bool show = (await _getUnread()).isNotEmpty;
    showEventRed.value = show;
    return show;
  }
  void showRedDotTCa9Goyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 添加接口获取的活动列表，设置进行中的活动并且从已读活动ID列表中清空非在线活动
  Future<void> addApiEvents(List<YBDEventCenterDataEventInfos?>? events) async {
    if (null == events || events.isEmpty) {
      return;
    }

    // 在线活动id列表
    var eventIdList = events.map((e) => e!.idStr).toList();
    await _configEvents(eventIdList);
  }
  void addApiEventsCpROEoyelive(List<YBDEventCenterDataEventInfos?>? events)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 推送新的在线活动ID并保存到本地
  Future<void> addPushEvent(List<String> eventIdList) async {
    if (null == eventIdList || eventIdList.isEmpty) {
      return;
    }

    var onlineList = await _getEventIdList();
    onlineList.addAll(eventIdList);
    await _configEvents(onlineList);
    update();
  }
  void addPushEvent37U4Yoyelive(List<String> eventIdList)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 设置进行中的活动并且从已读活动ID列表中清空非在线活动
  Future<void> _configEvents(List<String> eventIdList) async {
    // 去重
    final setList = Set.from(eventIdList).map((e) => e.toString()).toList();
    await _setEventIdList(setList);
    await _deleteUnOnlineEvents();
    await showRedDot();
  }

  /// 保存活动列表ID
  Future<void> _setEventIdList(List<String> eventIdList) async {
    await YBDSPUtil.setStringList(Const.SP_KEY_ONLINE_LIST, eventIdList);
    await checkEventList();
  }
  void _setEventIdListu3J2Goyelive(List<String> eventIdList)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 获取活动列表ID
  Future<List<String>> _getEventIdList() async {
    return (await YBDSPUtil.getStringList(Const.SP_KEY_ONLINE_LIST)) ?? [];
  }

  /// 点击活动详情时添加到已读活动
  Future<void> addRead(String eventId) async {
    var readEventIdList = await getReadIdList();

    if (!readEventIdList.contains(eventId)) {
      readEventIdList.add(eventId);
      setReadIdList(readEventIdList);
      await showRedDot();
    }
  }

  /// 过滤非进行中的活动
  Future<void> _deleteUnOnlineEvents() async {
    var onLineEventIdList = await _getEventIdList();
    var readEventIdList = await getReadIdList();
    readEventIdList.removeWhere((element) => !onLineEventIdList.contains(element));
    setReadIdList(readEventIdList);
  }
  void _deleteUnOnlineEventsskCMFoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 未读活动 = 进行中活动 - 已读活动
  Future<List<String>> _getUnread() async {
    var onLineEventIdList = await _getEventIdList();
    var readEventIdList = await getReadIdList();
    var unreadEventIdList = onLineEventIdList.where((element) => !readEventIdList.contains(element)).toList();
    return unreadEventIdList;
  }
}

/// 已读id
Future<bool> isReadId(String id) async {
  List<String> readList = await getReadIdList();
  return readList.contains(id);
}

/// 从sp获取已读活动
Future<List<String>> getReadIdList() async {
  return (await YBDSPUtil.getStringList(Const.SP_KEY_READ_LIST)) ?? [];
}

/// 保存已读活动
Future<void> setReadIdList(List<String> readIdList) async {
  await YBDSPUtil.setStringList(Const.SP_KEY_READ_LIST, readIdList);
}
