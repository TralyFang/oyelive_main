import 'dart:async';


enum EventCenterTabIndex {
  InProgress,
  ComingSoon,
  Ended,
}

extension TabIndexExtension on EventCenterTabIndex? {
  String get value {
    switch (this) {
      case EventCenterTabIndex.InProgress:
        return '1';
      case EventCenterTabIndex.ComingSoon:
        return '3';
      case EventCenterTabIndex.Ended:
        return '2';
      default:
        return '0';
    }
  }
}
