import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';
import 'package:oyelive_main/common/widget/tp_ybd_rounded_btn.dart';
import 'package:oyelive_main/ui/page/home/controller/event_ybd_center_controller.dart';
import 'package:oyelive_main/ui/page/home/widget/event_center/event_ybd_center_view.dart';
import 'package:oyelive_main/ui/widget/scaffold/bg_ybd_container.dart';
import 'package:oyelive_main/ui/widget/scaffold/custom_ybd_nav_bar.dart';
import 'package:oyelive_main/ui/widget/tab/custom_ybd_tab_bar.dart';

class YBDEventCenterPage extends StatefulWidget {
  const YBDEventCenterPage({Key? key}) : super(key: key);

  @override
  State<YBDEventCenterPage> createState() => _YBDEventCenterPageState();
}

class _YBDEventCenterPageState extends State<YBDEventCenterPage> with SingleTickerProviderStateMixin {
  TabController? _tabController;
  YBDEventCenterController _eventCenterCtr = Get.put(YBDEventCenterController());

  @override
  void initState() {
    super.initState();
    _createTabController();
  }
  void initStatepYw3toyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    Get.delete<YBDEventCenterController>();

    if (null != _tabController) {
      _tabController!.dispose();
    }
  }
  void disposeLPXKwoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: YBDCustomNavBar(title: translate('event')),
      body: Stack(
        children: [
          YBDBgContainer(
            padding: YBDTPStyle.pagePadding,
            child: Stack(
              alignment: Alignment.center,
              children: [
                GetBuilder<YBDEventCenterController>(builder: (YBDEventCenterController eventCenterCtr) {
                  List<String> tabs = eventCenterCtr.tabList();
                  final List<YBDEventCenterView> tabViews = eventCenterCtr.tabViewList();

                  return Column(
                    children: [
                      YBDCustomTabBar(
                        tabs,
                        controller: _tabController,
                      ),
                      SizedBox(height: 30.px),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: tabViews,
                        ),
                      ),
                    ],
                  );
                }),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Container(
              width: kScreenWidthDp,
              height: 160.px + kBottomBarHeight,
              decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
                  Colors.black.withOpacity(0.0),
                  Colors.black.withOpacity(0.5),
                ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
              ),
              child: Center(
                child: YBDTPRoundedBtn(
                  width: 440.px as double,
                  height: 72.px as double,
                  title: translate('daily_tasks'),
                  titleSp: 32.sp as double,
                  titleColor: Colors.white,
                  disableTitleColor: Color(0xff717171),
                  enableGradient: YBDTPGlobal.blueGradient,
                  disableGradient: YBDTPGlobal.greyGradient,
                  onPressed: _onPressedDailyTaskBtn,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void buildrzYqOoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _onPressedDailyTaskBtn() {
    YBDCommonTrack().commonTrack(YBDTAProps(location: 'daily_task', module: YBDTAModule.event));
    YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.daily_task);
    YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(
      YBDEventName.CLICK_EVENT,
      location: YBDLocationName.EVENT_CENTER_PAGE,
      itemName: 'daily_task',
    ));
  }
  void _onPressedDailyTaskBtnemPqNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _createTabController() {
    _tabController = TabController(length: YBDEventCenterController.kTabLength, vsync: this);
    // 默认显示第一个标签页
    _eventCenterCtr.tabIndex = 0;
    _tabController!.addListener(() {
      _eventCenterCtr.tabIndex = _tabController!.index;
    });
  }
}
