
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../common/util/log_ybd_util.dart';
import 'profile_ybd_edit_row.dart';

/// 输入对话框保存回调
typedef InputSaveCallbackFunc = Function(String);

/// profile 编辑页面输入框弹框
class YBDProfileInputDialog extends StatefulWidget {
  /// 旧输入值
  String? oldValue;
  final EditType dialogType;
  final InputSaveCallbackFunc saveCallback;
  int? inputMaxLength;
  YBDProfileInputDialog(this.dialogType, this.saveCallback, {this.oldValue, this.inputMaxLength});

  @override
  YBDProfileInputDialogState createState() => new YBDProfileInputDialogState();
}

class YBDProfileInputDialogState extends State<YBDProfileInputDialog> {
  FocusNode _focus = new FocusNode();
  bool inputFocused = false;

  TextEditingController? _textController;

  @override
  void initState() {
    super.initState();

    if (null != widget.oldValue && widget.oldValue!.isNotEmpty) {
      _textController = new TextEditingController(text: widget.oldValue);
    } else {
      _textController = new TextEditingController();
    }

    _focus.addListener(() {
      setState(() {
        if (_focus.hasFocus) {
          logger.v('text input has focus');
          inputFocused = true;
        } else {
          logger.v('text input has no focus');
          inputFocused = false;
        }
      });
    });
  }
  void initStatebv2QNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    // 底部边距
    double bottomPadding = inputFocused ? ScreenUtil().setWidth(500) : ScreenUtil().setWidth(0);

    // 容器宽度
    double width = ScreenUtil().setWidth(600);

    // 容器高度
    double outHeight = ScreenUtil().setWidth(530);

    // 头部背景高度
    double headHeight = ScreenUtil().setWidth(140);

    // 白色背景高度
    double innerHeight = outHeight - headHeight / 2;

    late String imgStr;
    late String title;
    String? placeholder;

    switch (widget.dialogType) {
      case EditType.Email:
        {
          title = translate('email');
          placeholder = translate('email_placeholder');
          imgStr = 'assets/images/profile/profile_input_dialog_email_bg.png';
          break;
        }
      case EditType.NickName:
        {
          title = translate('nick_name');
          placeholder = translate('nick_name_placeholder');
          imgStr = 'assets/images/profile/profile_input_dialog_bg.png';
          break;
        }
      case EditType.Bio:
        {
          title = translate('bio');
          placeholder = translate('bio_placeholder');
          imgStr = 'assets/images/profile/profile_input_dialog_bg.png';
          break;
        }
    }

    return GestureDetector(
      onTap: () {
        logger.v("clicked the input dialog bg");
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          padding: EdgeInsets.only(bottom: bottomPadding),
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Container(
              width: width,
              height: outHeight,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    // 白色背景
                    top: headHeight / 2,
                    child: Container(
                      width: width,
                      height: innerHeight,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(16)))),
                          color: Colors.white),
                      child: Column(children: <Widget>[
                        SizedBox(height: ScreenUtil().setWidth(112)),
                        Center(
                          // 标题
                          child: Text(
                            title,
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(32),
                              color: Colors.black,
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(20)),
                        Container(
                          // 文字内容
                          width: ScreenUtil().screenWidth,
                          padding: EdgeInsets.fromLTRB(
                            ScreenUtil().setWidth(32),
                            ScreenUtil().setWidth(25),
                            ScreenUtil().setWidth(32),
                            0,
                          ),
                          child: TextField(
                            focusNode: _focus,
                            maxLines: 1,
                            controller: _textController,
                            cursorColor: Color(0xffE422C7),
                            maxLength: widget.inputMaxLength,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: ScreenUtil().setSp(28),
                              fontWeight: FontWeight.normal,
                            ),
                            decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xffDFDEDE)),
                                ),
                                hintStyle: TextStyle(
                                  color: Color(0xff909399),
                                  fontWeight: FontWeight.normal,
                                ),
                                hintText: placeholder,
                                border: InputBorder.none,
                                counterText: ''),
                            onChanged: (text) {
                              logger.v('input text : $text');
                            },
                          ),
                        ),
                        Container(
                          height: ScreenUtil().setWidth(160),
                          child: Row(
                            children: <Widget>[
                              GestureDetector(
                                // 取消
                                onTap: () {
                                  logger.v('clicked cancel');
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  color: Colors.white,
                                  width: width / 2,
                                  alignment: Alignment.center,
                                  height: ScreenUtil().setWidth(120),
                                  child: Text(
                                    translate('cancel'),
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(32),
                                      color: Color(0xffE6497A),
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                // 保存
                                onTap: () {
                                  logger.v('clicked save : ${_textController!.text}');
                                  if (null != widget.saveCallback) {
                                    widget.saveCallback(_textController!.text);
                                  }

                                  // 隐藏对话框
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  color: Colors.white,
                                  width: width / 2,
                                  alignment: Alignment.center,
                                  height: ScreenUtil().setWidth(120),
                                  child: Text(
                                    translate('save'),
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(32),
                                      color: Color(0xff6BA3FE),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ]),
                    ),
                  ),
                  // 头部背景
                  Positioned(
                    top: ScreenUtil().setWidth(0),
                    left: ScreenUtil().setWidth(53),
                    child: Image(
                      image: AssetImage(imgStr),
                      fit: BoxFit.cover,
                      width: ScreenUtil().setWidth(520),
                      height: headHeight,
                    ),
                  ),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }
  void buildiCZz9oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
