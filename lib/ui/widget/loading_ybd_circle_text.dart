
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../common/util/log_ybd_util.dart';

/// 带文字的加载框
class YBDLoadingCircleText extends StatefulWidget {
  final String text;

  @override
  State<YBDLoadingCircleText> createState() => _YBDLoadingCircleTextState();

  YBDLoadingCircleText(this.text);
}

class _YBDLoadingCircleTextState extends State<YBDLoadingCircleText> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the progress dialog bg");
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Center(
                    child: SizedBox(
                      width: ScreenUtil().setWidth(96),
                      height: ScreenUtil().setWidth(96),
                      child: CircularProgressIndicator(
                        strokeWidth: ScreenUtil().setWidth(4),
                        valueColor: AlwaysStoppedAnimation<Color>(Color(0xffB770FA)),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      padding: EdgeInsets.all(ScreenUtil().setWidth(24)),
                      child: Image.asset(
                        'assets/images/status/status_upload_progress.webp',
                        width: ScreenUtil().setWidth(54),
                        height: ScreenUtil().setWidth(54),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: ScreenUtil().setWidth(5),
              ),
              Container(
                width: 260,
                height: 40,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '${widget.text}',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.normal,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildktPajoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
