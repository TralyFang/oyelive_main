
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class YBDRoomCategoryWidget extends StatelessWidget {
  String category;

  YBDRoomCategoryWidget(this.category);

  @override
  Widget build(BuildContext context) {
    Color? bgColor;
    switch (category) {
      case "Special":
        bgColor = Color(0xfff5b160);
        break;
      case "Heart":
        bgColor = Color(0xfff06292);
        break;
      case "Party":
        bgColor = Color(0xff6796d7);
        break;
      case "Friends":
        bgColor = Color(0xff45a9a0);
        break;
      case "Exclusive":
        bgColor = Color(0xff9c27b0);
        break;
      case "Royal":
        bgColor = Color(0xffe91e63);
        break;
      case "Standard":
        bgColor = Color(0xff69d1df);
        break;
    }

    return ClipRRect(
      borderRadius: BorderRadius.circular(ScreenUtil().setWidth(15)),
      child: Container(
          width: ScreenUtil().setWidth(108),
          height: ScreenUtil().setWidth(30),
          color: bgColor,
          alignment: Alignment.center,
          child: Text(
            category,
            style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(20)),
          )),
    );
  }
  void buildZAlsuoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
