
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:permission_handler/permission_handler.dart';

import 'button/delay_ybd_gesture_detector.dart';

/// 跳转设置页面弹框
class YBDGoSetNowDialog extends StatelessWidget {
  YBDGoSetNowDialog();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  // 避免点白色背景隐藏弹框
                },
                child: Container(
                  width: ScreenUtil().setWidth(500),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(32)))),
                    color: Colors.white,
                    border: Border.all(
                      color: Color(0xff47CDCC),
                      width: ScreenUtil().setWidth(3),
                    ),
                  ),
                  height: ScreenUtil().setWidth(372),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      // 标题
                      Container(
                        margin: EdgeInsets.only(top: ScreenUtil().setWidth(41), bottom: ScreenUtil().setWidth(28)),
                        child: Center(
                          child: Text(
                            translate('access_microphone_title'),
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(28),
                              fontWeight: FontWeight.w500,
                              color: Colors.black,
                              height: 1.5,
                            ),
                          ),
                        ),
                      ),
                      // 内容
                      Center(
                        child: Text(
                          translate('access_microphone_content_one'),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(26),
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            height: 1.5,
                          ),
                        ),
                      ),
                      Center(
                        child: Text(
                          translate('access_microphone_content_two'),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(26),
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            height: 1.5,
                          ),
                        ),
                      ),
                      Center(
                        child: Text(
                          translate('access_microphone_content_three'),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(26),
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            height: 1.5,
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setWidth(36)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          _okBtn(context),
                          _setNowBtn(context),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildoEjHzoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// ok 按钮
  Widget _okBtn(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(ScreenUtil().setWidth(33)),
          ),
          color: Color(0xffCCCCCC),
        ),
        width: ScreenUtil().setWidth(150),
        height: ScreenUtil().setWidth(65),
        alignment: Alignment.center,
        child: Text(
          translate('ok'),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
  void _okBtnxOxDJoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// set now 按钮
  Widget _setNowBtn(BuildContext context) {
    return YBDDelayGestureDetector(
      onTap: () {
        Navigator.pop(context);
        openAppSettings();
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(ScreenUtil().setWidth(33)),
          ),
          gradient: LinearGradient(
            colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        constraints: BoxConstraints(
          minWidth: ScreenUtil().setWidth(250),
        ),
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
        height: ScreenUtil().setWidth(65),
        width: ScreenUtil().setWidth(270),
        alignment: Alignment.center,
        child: Text(
          translate('go_to_set'),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
  void _setNowBtna0YyNoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
