
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class YBDMyRefreshIndicator {
  static final myHeader = ClassicHeader(
//    refreshingText: "正在刷新...",
//    releaseText: "松开以刷新",
//    completeText: "刷新完成",
//    failedText: "刷新失败",
//    idleText: "下拉刷新",
//    idleIcon: Container(),
    textStyle: TextStyle(color: Colors.white.withOpacity(0.6)),
  );

  static final blackHeader = ClassicHeader(
//    refreshingText: "正在刷新...",
//    releaseText: "松开以刷新",
//    completeText: "刷新完成",
//    failedText: "刷新失败",
//    idleText: "下拉刷新",
//    idleIcon: Container(),
    textStyle: TextStyle(color: Colors.black.withOpacity(0.6)),
  );

  static final LoadIndicator myFooter = ClassicFooter(
//    idleText: "加载更多",
//    loadingText: "正在加载...",
//    noDataText: "无更多数据",
    textStyle: TextStyle(
      color: Colors.white.withOpacity(0.6),
    ),
  );

  static final LoadIndicator myFooterBlack = ClassicFooter(
    textStyle: TextStyle(color: Colors.black),
  );

  /// 根据数据状态返回 footer
  /// [noData] true : 有数据，false ：没有数据
  static ClassicFooter myFooterWithDataStatus(
    bool noData, {
    BuildContext? context,
  }) {
    if (noData) {
      return myFooter as ClassicFooter;
    } else {
      return ClassicFooter(
        // 隐藏 No more data
        noDataText: "",
        textStyle: TextStyle(color: Colors.white.withOpacity(0.6)),
      );
    }
  }
}
