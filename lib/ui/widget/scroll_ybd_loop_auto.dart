import 'package:flutter/material.dart';

class YBDScrollLoopAutoScroll extends StatefulWidget {
  const YBDScrollLoopAutoScroll({
    required this.child,
    required this.scrollDirection,
    Key? key,
    this.delay = const Duration(seconds: 0),
    this.duration = const Duration(seconds: 50),
    this.gap = 0,
    this.reverseScroll = false,
    this.duplicateChild = 2,
    this.enableScrollInput = true,
    this.delayAfterScrollInput = const Duration(seconds: 1),
  }) : super(key: key);

  /// Widget to display in loop
  ///
  /// required
  final Widget child;

  /// Duration to wait before starting animation
  ///
  /// Default set to Duration(seconds: 1).
  ///
  final Duration delay;

  /// Duration of animation
  ///
  /// Default set to Duration(seconds: 30).
  final Duration duration;

  /// Sized between end of child and beginning of next child instance
  ///
  /// Default set to 25.
  final double gap;

  /// The axis along which the scroll view scrolls.
  ///
  /// required
  final Axis scrollDirection;

  ///
  /// true : Right to Left
  ///
  // |___________________________<--Scrollbar-Starting-Right-->|
  ///
  /// fasle : Left to Right (Default)
  ///
  // |<--Scrollbar-Starting-Left-->____________________________|
  final bool reverseScroll;

  /// The number of times duplicates child. So when the user scrolls then, he can't find the end.
  ///
  /// Default set to 25.
  ///
  final int duplicateChild;

  ///User scroll input
  ///
  ///Default set to true
  final bool enableScrollInput;

  /// Duration to wait before starting animation, after user scroll Input.
  ///
  /// Default set to Duration(seconds: 1).
  ///
  final Duration delayAfterScrollInput;
  @override
  State<YBDScrollLoopAutoScroll> createState() => _YBDScrollLoopAutoScrollState();
}

class _YBDScrollLoopAutoScrollState extends State<YBDScrollLoopAutoScroll>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  AnimationController? animationController;
  late Animation<Offset> offset;

  ValueNotifier<bool> shouldScroll = ValueNotifier<bool>(false);
  ScrollController? scrollController;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addObserver(this);
    scrollController = ScrollController();

    scrollController!.addListener(() async {
      if (widget.enableScrollInput) {
        if (animationController!.isAnimating) {
          animationController!.stop();
        } else {
          await Future.delayed(widget.delayAfterScrollInput);
          animationHandler();
        }
      }
    });

    animationController = AnimationController(
      duration: widget.duration,
      vsync: this,
    );

    offset = Tween<Offset>(
      begin: Offset.zero,
      end: widget.scrollDirection == Axis.horizontal
          ? widget.reverseScroll
              ? const Offset(.5, 0)
              : const Offset(-.5, 0)
          : widget.reverseScroll
              ? const Offset(0, .5)
              : const Offset(0, -.5),
    ).animate(animationController!);

    WidgetsBinding.instance?.addPostFrameCallback((Duration timeStamp) async {
      await Future.delayed(widget.delay);
      animationHandler();
    });
  }
  void initState6YdD1oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void animationHandler() async {
    print('22.12.2---animationHandler---begin');
    if (scrollController!.position.maxScrollExtent > 0) {
      shouldScroll.value = true;
      if (shouldScroll.value && mounted) {
        print('22.12.2---animationHandler---forward');
        animationController!.forward().then((_) async {
          print('22.12.2---animationHandler---reset');
          animationController!.reset();

          if (shouldScroll.value && mounted) {
            animationHandler();
          }
        });
      }
    }
  }
  void animationHandlerxJPyxoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: widget.enableScrollInput ? const BouncingScrollPhysics() : const NeverScrollableScrollPhysics(),
      controller: scrollController,
      scrollDirection: widget.scrollDirection,
      reverse: widget.reverseScroll,
      child: SlideTransition(
        position: offset,
        child: ValueListenableBuilder<bool>(
          valueListenable: shouldScroll,
          builder: (BuildContext context, bool shouldScroll, _) {
            return widget.scrollDirection == Axis.horizontal
                ? Row(
                    children: List.generate(
                        widget.duplicateChild,
                        (int index) => Padding(
                              padding: EdgeInsets.only(
                                  right: shouldScroll && !widget.reverseScroll ? widget.gap : 0,
                                  left: shouldScroll && widget.reverseScroll ? widget.gap : 0),
                              child: widget.child,
                            )))
                : Column(
                    children: List.generate(
                    widget.duplicateChild,
                    (int index) => Padding(
                      padding: EdgeInsets.only(
                          bottom: shouldScroll && !widget.reverseScroll ? widget.gap : 0,
                          top: shouldScroll && widget.reverseScroll ? widget.gap : 0),
                      child: widget.child,
                    ),
                  ));
          },
        ),
      ),
    );
  }
  void buildQsIeFoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('22.12.6-didChangeAppLifecycleState-' + state.toString());
    switch (state) {
      case AppLifecycleState.inactive: // 处于这种状态的应用程序应该假设它们可能在任何时候暂停。
        animationController?.stop();
        break;
      case AppLifecycleState.resumed: // 从后台切换前台，界面可见
        animationHandler();
        break;
      case AppLifecycleState.paused: // 界面不可见，后台
        animationController?.stop();
        break;
      case AppLifecycleState.detached: // APP结束时调用
        animationController?.stop();
        break;
    }
  }
  void didChangeAppLifecycleStateMfWo5oyelive(AppLifecycleState state) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    animationController!.dispose();
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }
}
