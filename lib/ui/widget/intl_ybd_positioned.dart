


import 'package:flutter/material.dart';
import 'package:oyelive_main/common/util/intl_ybd_lang_util.dart';

class YBDIntlPositioned extends StatelessWidget{

  final Widget child;

  final double? left, top, bottom, right, width, height;
  const YBDIntlPositioned({Key? key, required this.child, this.top, this.bottom, this.right, this.left, this.width, this.height})
      : super(key: key);

    @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Positioned(
        child: child,
        left: YBDIntlLangUtil.isArabic() ? right : left,
        right:YBDIntlLangUtil.isArabic() ? left : right,
        top: top,
        bottom: bottom,
         width: width,
        height: height,
    );
  }
  void buildlLE0eoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

///国际化使用的  局限性强
class YBDIntlOffset extends Offset{
  YBDIntlOffset(double dx, double dy) : super(YBDIntlLangUtil.isArabic() ? -dx:dx, dy);

}

class YBDIntlBorderRadius extends BorderRadius{
    YBDIntlBorderRadius.horizontal({Radius left = Radius.zero, Radius right = Radius.zero,}) : super.horizontal(left: YBDIntlLangUtil.isArabic() ? right : left,right: YBDIntlLangUtil.isArabic() ? left : right);
}