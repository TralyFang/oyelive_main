
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/ui/page/profile/user_profile/getx/block_ybd_controller.dart';

import '../../common/util/log_ybd_util.dart';

// 点 title 按钮的回调方法
typedef Callback = Function(String title);
final String report = translate('report');
final String blockUser = translate('block');
final String unblockUser = translate('unblock');

/// 举报弹框
class YBDProfileOperatorDialog extends StatefulWidget {
  final Callback? callback;

  YBDProfileOperatorDialog({this.callback});

  @override
  YBDProfileOperatorDialogState createState() => new YBDProfileOperatorDialogState();
}

class YBDProfileOperatorDialogState extends State<YBDProfileOperatorDialog> {
  @override
  void initState() {
    super.initState();
  }
  void initStateIKnhioyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the report dialog bg");
        // 隐藏举报弹框
        Navigator.pop(context);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          padding: EdgeInsets.only(right: 20, top: 44),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Container(
                width: ScreenUtil().setWidth(220),
                height: ScreenUtil().setWidth(160),
                padding: EdgeInsets.only(top: 1),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular((ScreenUtil().setWidth(16))),
                    ),
                    color: Colors.white),
                child: Column(
                  children: <Widget>[
                    // harassment
                    GestureDetector(
                      onTap: () {
                        logger.v('clicked report btn');
                        widget.callback!(report);
                      },
                      child: reportContentRow(report),
                    ),
                    rowDivider(),
                    // scam
                    GestureDetector(
                      onTap: () {
                        logger.v('clicked slog block btn');
                        clickedReportButton(blockUser);
                      },
                      child: reportContentRow(Get.find<YBDBlockController>().state == 1 ? unblockUser : blockUser),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildYZQSxoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击举报内容按钮的响应事件
  clickedReportButton(String content) {
    logger.v('button action : $content');
    Navigator.pop(context);
    Get.find<YBDBlockController>().reLoad();
  }

  /// 举报行
  Container reportContentRow(String content) {
    return Container(
      height: ScreenUtil().setWidth(78),
      width: ScreenUtil().setWidth(200),
      color: Colors.white,
      child: Center(
        child: Text(
          content,
          style: TextStyle(
            color: Color(0xff333333),
            fontSize: ScreenUtil().setSp(28),
            fontWeight: FontWeight.normal,
          ),
        ),
      ),
    );
  }

  /// 分割线
  Divider rowDivider() {
    return Divider(
      height: ScreenUtil().setWidth(1),
      thickness: ScreenUtil().setWidth(1),
      color: Color(0xffDEDCDC),
    );
  }
}
