
import 'package:flutter/cupertino.dart';

/// 心跳动画
class YBDBounceAnimation extends StatefulWidget {
  final Widget? child;

  const YBDBounceAnimation({
    Key? key,
    this.child,
  }) : super(key: key);

  @override
  State<YBDBounceAnimation> createState() => _YBDBounceAnimationState();
}

class _YBDBounceAnimationState extends State<YBDBounceAnimation> with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController animController;

  @override
  void initState() {
    super.initState();
    animController = AnimationController(
      reverseDuration: const Duration(milliseconds: 200),
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );

    animation = Tween<double>(
      begin: 1.1,
      end: 0.8,
    ).animate(animController)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          animController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          animController.forward();
        }
      });

    animController.forward();
  }
  void initStateQeByzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: animation.value,
      child: widget.child,
    );
  }
  void buildQHce3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    animController.dispose();
    super.dispose();
  }
}
