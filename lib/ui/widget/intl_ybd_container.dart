


import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/common/util/intl_ybd_lang_util.dart';

class YBDIntlContainer extends StatelessWidget{

  final Widget child;

  final EdgeInsets? margin, padding;
  final double? height;
  final BoxDecoration? decoration;
  const YBDIntlContainer({Key? key, required this.child, this.margin, this.height, this.padding,this.decoration})
      : super(key: key);
    @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        height: this.height,
        margin: EdgeInsets.only(left: YBDIntlLangUtil.isArabic() ? margin!.right : margin!.left, right: YBDIntlLangUtil.isArabic() ? margin!.left : margin!.right,top: this.margin!.top,bottom: this.margin!.bottom),
        padding: padding,
        child: this.child,
        decoration: decoration,
    );
  }
  void build6eTavoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}