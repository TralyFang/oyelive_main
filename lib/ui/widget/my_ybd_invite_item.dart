
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import '../../base/base_ybd_state.dart';
import '../../module/user/entity/query_ybd_my_invites_resp_entity.dart';
import '../page/status/follow_ybd_button.dart';

import 'level_ybd_tag.dart';
import 'round_ybd_avatar.dart';

class YBDMyInviteItem extends StatefulWidget {
  YBDInviteeInfo? data;

  YBDMyInviteItem(
    this.data, {
    Key? key,
  }) : super(key: key);

  @override
  YBDMyInviteItemState createState() => YBDMyInviteItemState();
}

class YBDMyInviteItemState extends BaseState<YBDMyInviteItem> {
  @override
  Widget myBuild(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(20), horizontal: ScreenUtil().setWidth(25)),
      child: Row(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(ScreenUtil().setWidth(18)),
            child: YBDRoundAvatar(
              widget.data!.inviteeAvatar,
              scene: "B",
              sex: widget.data!.inviteeSex,
              avatarWidth: 100,
              userId: widget.data!.invitee,
              needNavigation: true,
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.data!.inviteeNickname!,
                    style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28)),
                  ),
                  SizedBox(height: ScreenUtil().setWidth(20)),
                  Row(
                    children: <Widget>[
                      Image.asset(
                        widget.data!.inviteeSex == 2 ? "assets/images/tag_male.webp" : "assets/images/tag_female.webp",
                        fit: BoxFit.fill,
                        width: ScreenUtil().setWidth(64),
                        height: ScreenUtil().setWidth(28),
                      ),
                      YBDLevelTag(widget.data!.inviteeLevel),
                    ],
                  )
                ],
              ),
            ),
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              widget.data!.follow!
                  ? Container()
                  : YBDFollowButton(
                      widget.data!.follow,
                      widget.data!.invitee.toString(),
                      'my_invites_page',
                      key: new Key(widget.data!.invitee.toString()),
                      onChange: (x) {
                        widget.data!.follow = x;
                        setState(() {});
                      },
                    ),
              SizedBox(height: ScreenUtil().setWidth(10)),
              Text(
                '${DateFormat("dd MMM yyyy 'at' HH:mm").format(DateTime.fromMillisecondsSinceEpoch(widget.data!.inviteeRegisterTime!))}',
                style: TextStyle(color: Color(0xCCEAEAEA), fontSize: ScreenUtil().setSp(24)),
              )
            ],
          ))
        ],
      ),
    );
  }
  void myBuildhRFfboyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
