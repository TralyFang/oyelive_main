
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_common_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';

class YBDCustomTabBar extends StatefulWidget {
  final List<String> labelText;
  final TabController? controller;

  const YBDCustomTabBar(
    this.labelText, {
    this.controller,
    Key? key,
  }) : super(key: key);

  @override
  State<YBDCustomTabBar> createState() => _YBDCustomTabBarState();
}

class _YBDCustomTabBarState extends State<YBDCustomTabBar> {
  /// 选中的标签索引
  int _selectedTabIndex = 0;

  @override
  void initState() {
    super.initState();
    widget.controller!.addListener(() {
      _selectedTabIndex = widget.controller!.index;
      setState(() {});
    });
  }
  void initStateFuGFtoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TabBar(
        isScrollable: true,
        controller: widget.controller,
        indicator: BoxDecoration(),
        labelPadding: EdgeInsets.symmetric(horizontal: 0),
        labelColor: YBDTPStyle.labelColor,
        labelStyle: YBDTPStyle.labelStyle,
        unselectedLabelColor: YBDTPStyle.unselectedLabelColor,
        unselectedLabelStyle: YBDTPStyle.unselectedLabelStyle,
        onTap: (int index) {
          YBDCommonTrack().commonTrack(YBDTAProps(location: widget.labelText[index], module: YBDTAModule.event));
        },
        tabs: List.generate(
          widget.labelText.length,
          (index) => Column(
            children: [
              Container(
                padding: _paddingWithIndex(index),
                height: 60.px,
                child: Tab(text: widget.labelText[index]),
              ),
              if (_selectedTabIndex == index)
                Container(
                  width: 40.px,
                  height: 4.px,
                  decoration: BoxDecoration(
                    color: YBDTPStyle.cyan,
                    borderRadius: BorderRadius.all(Radius.circular(4.px as double)),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
  void buildkG2puoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  EdgeInsetsGeometry _paddingWithIndex(int index) {
    return EdgeInsets.symmetric(horizontal: 30.px as double);
  }
}
