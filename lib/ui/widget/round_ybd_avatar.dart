
// import 'dart:math';

import 'dart:developer';

import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/ybd_image.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/ui/page/game_room/widget/game_ybd_widget_util.dart';
import '../../base/base_ybd_state.dart';
import '../../common/analytics/analytics_ybd_util.dart';
import '../../common/navigator/navigator_ybd_helper.dart';
import '../../common/util/image_ybd_util.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/sp_ybd_util.dart';
import '../../module/user/entity/user_ybd_info_entity.dart';
import '../page/home/widget/top_ybd_talent_item.dart';
import 'button/delay_ybd_gesture_detector.dart';

enum RoundAvatarType {
  OtherProfile,
  MyProfile,
  EditProfile,
}

class YBDRoundAvatar extends StatefulWidget {
  RoundAvatarType? avatarType;

  var avatarWidth;
  int labelWitdh;
  String? src;
  String? lableSrc;
  final String? hatImg;

  String? levelFrame;
  double levelFrameScale;

  bool showVip;

  String? locationForAnalytics;

  String? vipIcon;

  /// 0未知，1女, 2男
  int? sex;

  bool needNavigation;
  EdgeInsetsGeometry labelPadding;

  Function? onTap;
  Function? closeCallback;

  // TODO: 用户 id，可能是字符串或 int 类型
  var userId;
  String? scene;
  bool showLeftBottomImg; // 是否展示左下角的图标
  String? leftBottomImg; // 左下角的图标路径
  int? leftBottomWidth; // 左下角的图标大小
  int? leftOfLB; // 左下角的图标向左的偏移量
  int? bottomOfLB; // 左下角的图标向下的偏移量
  bool showRightBottomImg; // 是否展示右下角的图标
  String? rightBottomImg; // 右下角图标路径
  int? rightBottomWidth; // 右下角的图标大小

  YBDRoundAvatar(this.src,
      {this.avatarWidth: 96,
      this.lableSrc,
      this.hatImg,
      this.labelWitdh: 18,
      this.sex,
      this.userId,
      this.scene,
      this.needNavigation: true,
      this.labelPadding: const EdgeInsets.all(0),
      this.avatarType,
      this.locationForAnalytics,
      this.showVip = false,
      this.onTap,
      this.closeCallback,
      this.showLeftBottomImg = false,
      this.leftBottomImg,
      this.leftBottomWidth,
      this.leftOfLB,
      this.bottomOfLB,
      this.showRightBottomImg = false,
      this.rightBottomImg,
      this.rightBottomWidth,
      this.levelFrame,
      this.levelFrameScale: 1.4,
      this.sideBorder = true,
      this.vipIcon});

  bool sideBorder; // 是否显示头像白边

  @override
  YBDRoundAvatarState createState() => new YBDRoundAvatarState();
}

class YBDRoundAvatarState extends BaseState<YBDRoundAvatar> {
  getInt(var text) {
    if (text == null) {
      return 0;
    }

    if (text is int) {
      return text;
    } else {
      try {
        return int.parse(text);
      } catch (e) {
        logger.v(e);
        return 0;
      }
    }
  }

  @override
  Widget myBuild(BuildContext context) {
    // log("wisslevelframe:${widget.levelFrame}");
    var avatar = Stack(
      children: [
        Container(
          width: ScreenUtil().setWidth(widget.avatarWidth),
          height: ScreenUtil().setWidth(widget.avatarWidth),
          decoration: BoxDecoration(shape: BoxShape.rectangle),
          child: Stack(
            alignment: widget.showVip ? Alignment.bottomRight : Alignment.bottomCenter,
            children: <Widget>[
              YBDGameWidgetUtil.avatar(
                url: YBDImageUtil.avatar(context, widget.src, getInt(widget.userId), scene: widget.scene),
                addSize: false,
                placeholder: Image.asset(YBDResourcePathUtil.defaultMaleAssets(male: widget.sex == 2)),
                width: ScreenUtil().setWidth(widget.avatarWidth),
                sideBorder: widget.sideBorder,
              ),

              //最热主播 位置不够放 profile lv一行也有这个图标 所以注释掉 位置留给talent
              if (widget.showVip && !widget.showRightBottomImg) levelIcon(),
              if (widget.showLeftBottomImg) _leftBottom(),
              if (widget.showRightBottomImg && !widget.showVip) _rightBottom(),
              widget.hatImg != null ? _avatarHat() : Container(),
            ],
          ),
        ),
        if (widget.levelFrame != null)
          Transform.scale(
            scale: widget.levelFrameScale,
            child: YBDNetworkImage(
              imageUrl: widget.levelFrame!,
              width: ScreenUtil().setWidth(widget.avatarWidth),
              height: ScreenUtil().setWidth(widget.avatarWidth),
            ),
          ),
        if (widget.vipIcon != null)
          Positioned(
              bottom: ScreenUtil().setWidth(0),
              right: 0,
              child: Transform.scale(
                scale: 1.2,
                child: YBDVipIcon(
                  widget.vipIcon,
                  padding: EdgeInsets.all(0),
                ),
              )),
      ],
    );
    if (widget.needNavigation) {
      return YBDDelayGestureDetector(
        onTap: () async {
          YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();

          logger.v('clicked round avatar button');
          widget.closeCallback?.call();

          if (null != userInfo && '${userInfo.id}' == '${widget.userId}') {
            logger.v('jump to my profile page');

            YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.myprofile);
          } else {
            if (widget.locationForAnalytics != null)
              YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.CLICK_EVENT,
                  location: widget.locationForAnalytics, itemName: "user_profile"));
            logger.v('jump to other user profile page : ${widget.userId}');
            YBDNavigatorHelper.navigateTo(context, YBDNavigatorHelper.user_profile + "/${widget.userId}");
          }

          widget.onTap?.call();
        },
        child: avatar,
      );
    } else if (widget.onTap != null)
      return YBDDelayGestureDetector(
          onTap: () {
            widget.onTap?.call();
          },
          child: avatar);
    else
      return avatar;
  }
  void myBuildespD2oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget levelIcon() {
    if (widget.lableSrc?.contains("vip_lv") ?? false || widget.avatarType == RoundAvatarType.EditProfile) {
      return Container();
    } else {
      return (widget.lableSrc ?? "") == ""
          ? Container()
          : Padding(
              padding: widget.labelPadding,
              child: YBDImage(
                path: widget.lableSrc ?? "",
                width: widget.labelWitdh,
              ),
            );
    }
  }
  void levelIconIKyEfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget _avatarHat() {
    return Align(
      alignment: Alignment.topRight,
      child: Transform.translate(
        offset: Offset(ScreenUtil().setWidth(20), -ScreenUtil().setWidth(30)),
        child: Container(
          width: ScreenUtil().setWidth(widget.avatarWidth * 0.6),
          child: Image.asset(
            widget.hatImg!,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
  void _avatarHatyuyXAoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // icon_owner_mute   icon_user_mute    mic_off
  /// 左下角的图标 默认是禁言图标尺寸
  Widget _leftBottom() {
    return Positioned(
      left: -ScreenUtil().setWidth(widget.bottomOfLB ?? 10),
      bottom: -ScreenUtil().setWidth(widget.leftOfLB ?? 13),
      child: Image.asset(
        widget.leftBottomImg ?? 'assets/images/icon_owner_mute.webp',
        width: ScreenUtil().setWidth(widget.leftBottomWidth ?? 40),
        fit: BoxFit.cover,
      ),
    );
  }
  void _leftBottomS3sTMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 右下角的图标
  Widget _rightBottom() {
    return Positioned(
      bottom: -ScreenUtil().setWidth(10),
      right: -ScreenUtil().setWidth(7),
      child: Image.asset(
        widget.rightBottomImg ?? 'assets/images/icon_user_mute.webp',
        width: ScreenUtil().setWidth(widget.rightBottomWidth ?? 40),
        fit: BoxFit.cover,
      ),
    );
  }
  void _rightBottomftHfzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
