
/*
 * @Author: William
 * @Date: 2022-07-06 17:55:07
 * @LastEditTime: 2022-10-17 17:26:10
 * @LastEditors: William
 * @Description: avoid pass empty imageUrl to [CachedNetworkImage]
 * @FilePath: /oyetalk-flutter/lib/ui/widget/ybd_network_image.dart
 */
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:svgaplayer_flutter/player.dart';

class YBDNetworkImage extends CachedNetworkImage {
  YBDNetworkImage({
    required String imageUrl,
    double? width,
    double? height,
    BoxFit? fit,
    Color? color,
    ImageWidgetBuilder? imageBuilder,
    PlaceholderWidgetBuilder? placeholder,
    LoadingErrorWidgetBuilder? errorWidget,
    Duration fadeInDuration = const Duration(milliseconds: 500),
    Duration fadeOutDuration = const Duration(milliseconds: 1000),
  }) : super(
          imageUrl: imageUrl,
          width: width,
          height: height,
          fit: fit,
          color: color,
          imageBuilder: imageBuilder,
          placeholder: placeholder,
          errorWidget: errorWidget,
          fadeInDuration: fadeInDuration,
          fadeOutDuration: fadeOutDuration,
        );

  @override
  Widget build(BuildContext context) {
    if (imageUrl.isEmpty && placeholder == null && errorWidget == null) return Container(width: width, height: height);
    if (imageUrl.endsWith('svga')) {
      return SizedBox(width: width, height: height, child: SVGASimpleImage(resUrl: imageUrl));
    }
    return CachedNetworkImage(
      imageUrl: imageUrl,
      width: width,
      height: height,
      fit: fit,
      color: color,
      imageBuilder: imageBuilder,
      placeholder: placeholder,
      errorWidget: errorWidget,
      fadeInDuration: fadeInDuration,
      fadeOutDuration: fadeOutDuration,
    );
  }
  void buildIGZ8Royelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
