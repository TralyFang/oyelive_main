import 'package:oyelive_main/ui/page/game_room/tp_room_page/utils/quick_ybd_msg.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/color_ybd_ext.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/module/module_ybd_center.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/go_ybd_tab_event.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/entity/ybd_teen_info.dart';
import 'package:oyelive_main/ui/page/room/teen_patti/pages/teen_ybd_patti_picker_view.dart';

class YBDRichTextDecode {
  final defaultSize = 16;

  Text getRichTextWidget(YBDPlainText plainText, {num? fixTextSize, List<InlineSpan>? pre}) {
    //
    num? theFinalTextSize;
    if (fixTextSize != null)
      theFinalTextSize = fixTextSize;
    else if (plainText.defaultSize != null)
      theFinalTextSize = plainText.defaultSize;
    else
      theFinalTextSize = defaultSize;

    TextStyle defaultStyle = TextStyle(
        fontSize: ScreenUtil().setSp(theFinalTextSize!),
        color: YBDHexColor(plainText.defaultColor),
        fontFamily: plainText.defaultFamily,
        fontWeight: FontWeight.values[plainText.defaultWeight ?? 3]);
    List<InlineSpan> lines = [];
    if (pre != null) lines = pre;
    if (plainText.richLines != null) {
      plainText.richLines!.forEach((element) {
        switch (element!.type) {
          case "text":
          case "quickChat":
            lines.add(TextSpan(
                text: element.type == "quickChat" ? YBDQuickMsgHelper.queryFromData(element.content!) : element.content,
                recognizer: new TapGestureRecognizer()..onTap = onTap(element.destination),
                style: TextStyle(
                    decoration: element.destination != null ? TextDecoration.underline : null,
                    fontSize: element.fontSize != null ? ScreenUtil().setSp(element.fontSize!) : null,
                    fontWeight: element.fontWeight != null ? FontWeight.values[element.fontWeight as int] : null,
                    fontFamily: element.fontFamily != null ? element.fontFamily : null,
                    color: element.color != null ? YBDHexColor(element.color) : null)));
            break;
          case "image":
            late Widget child;
            if (element.source != null) {
              if (element.source == 'Network') {
                child = GestureDetector(
                  onTap: onTap(element.destination),
                  child: YBDNetworkImage(
                    imageUrl: element.path!,
                    width: ScreenUtil().setWidth(element.width ?? 38),
                    // height: ScreenUtil().setWidth(element?.height ?? 20),
                    fit: BoxFit.cover,
                  ),
                );
              } else if (element.source == 'Assets') {
                child = Image.asset(
                  element.path!,
                  width: ScreenUtil().setWidth(element.width ?? 38),
                  // height: ScreenUtil().setWidth(element?.height ?? 20),
                  fit: BoxFit.cover,
                );
              }
            } else {
              child = SizedBox();
            }
            lines.add(WidgetSpan(child: child));
            break;
          default:
            break;
        }
      });
    }
    return Text.rich(
      TextSpan(children: lines),
      style: defaultStyle,
    );
  }

  onTap(String? destination) {
    if (destination == null) return;
    if (destination.contains('Play Royal Pattern')) {
      YBDTeenInfo.getInstance().enterType = TPEnterType.TPTask;
      YBDModuleCenter.instance().enterRoom(type: EnterRoomType.OpenTp);
      return;
    }
    if (destination.contains(YBDNavigatorHelper.index_page) || destination.contains("flutter://main")) {
      YBDNavigatorHelper.popUntilMain(Get.context!);

      eventBus.fire(YBDGoTabEvent(1));
      // Future.delayed(Duration(mi), () {
      //   eventBus.fire(YBDGoTabEvent(int.parse(dailyTask[index].route.split("/").last)));
      // });
      return;
    }
    YBDNavigatorHelper.remoteNavigator(Get.context, destination);
  }
}

class YBDPlainText with JsonConvert<YBDPlainText> {
  num? defaultSize;
  String? defaultColor;
  String? defaultFamily;
  int? defaultWeight;
  List<YBDRichLines?>? richLines;
}

class YBDRichLines with JsonConvert<YBDRichLines> {
  String? destination;
  String? type;

  /// text
  num? fontSize;
  num? fontWeight;
  String? fontFamily;
  String? color;
  String? content;

  ///image
  num? width;
  num? height;
  String? source;
  String? path;
}

class YBDResources with JsonConvert<YBDResources> {
  dynamic user;

  dynamic getSpecificUserInfo(String? id) {
    // Map dataMap = new Map<String, dynamic>.from(user);
    // print("xdxdd${user[id]}");
    return user[id];
  }
}
