
import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import '../../common/util/log_ybd_util.dart';
//import 'package:flutteroyetalk/commn/network/http/result_bean.dart';

enum PageStatus { Loading, NetworkError, Loaded, LoadError }

enum DataEvent {
  Loaded,
  NetworkError,
  LoadError,
}

class YBDLoadingPageBloc extends Bloc<DataEvent, PageStatus> {
  //api load funcs
  List<Function>? _loadList;

  //already loaded counter
  int loadedCounter = 0;

  //set and run;
  initloadList(List<Function>? value) {
    _loadList = value;
    _loadList!.forEach((load) {
      load();
    });
  }

  YBDLoadingPageBloc() : super(PageStatus.Loading);

  @override
  Stream<PageStatus> mapEventToState(DataEvent event) async* {
    switch (event) {
      case DataEvent.Loaded:
        loadedCounter++;
        logger.v("loadedCounter++:$loadedCounter");
        //When page not error
        if (this.state == PageStatus.Loading) {
          if (loadedCounter == _loadList!.length) {
            yield PageStatus.Loaded;
          } else {
            yield PageStatus.Loading;
          }
        }

        break;
      case DataEvent.NetworkError:
        yield PageStatus.NetworkError;
        break;
      case DataEvent.LoadError:
        yield PageStatus.LoadError;

        break;
    }
    throw UnimplementedError();
  }
  void mapEventToState5nuQ9oyelive(DataEvent event)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
