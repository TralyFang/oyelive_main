
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:oyelive_main/common/constant/i18n_ybd_key.dart';
import 'package:oyelive_main/common/util/custom_dialog/custom_ybd_dialog.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/page/game_room/const/game_ybd_const.dart';
import 'package:oyelive_main/ui/page/game_room/ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';

enum ConfirmDialogType {
  /// 默认形式
  normal,
  /// 需要key的形式，调用同类方法[closeKey]关闭指定key的dialog
  key,
  /// 自定义overlay形式的dialog，有个问题👇🏻
  overlay
}

class YBDConfirmDialog extends StatefulWidget {
  /// 确认弹框
  static show(
      BuildContext? context,
     {
       String? title, // widget is the bold title, please assign a value '' to empty
       String? cancelTitle, // If you don't want a cancelTitle to exist, you can assign the value ''
       String? sureTitle,
       bool barrierDismissible = false, // Click Mask to close the dialog.
        String? content, // content
        Function? onSureCallback,
        Function? onCancelCallback,
        int endTimeAutoClose = -1, // close dialog time, invoke onCancelCallback
        ConfirmDialogType type = ConfirmDialogType.normal,  // 指定弹窗类型
        String? key, // 设置对应的key，用于关闭指定key的dialog, 只在[ConfirmDialogType.key]下有效
      }) {
    title = title ?? YBDI18nKey.tip;
    cancelTitle = cancelTitle ?? YBDI18nKey.cancel;
    sureTitle = sureTitle ?? YBDI18nKey.confirm;
    if (type == ConfirmDialogType.normal){
      showDialog(
          barrierDismissible: barrierDismissible,
          context: context!,
          builder: (_) {
            return YBDConfirmDialog(title, cancelTitle, sureTitle, content: content, onCancelCallback: () {
              // 回调里面做了处理，这里可以放心pop
              Navigator.pop(context); // 不仅要避免多次pop，还要可以指定pop
              if (onCancelCallback != null) onCancelCallback();
            }, onSureCallback: () {
              Navigator.pop(context);
              if (onSureCallback != null) onSureCallback();
            }, endTimeAutoClose: endTimeAutoClose);
          });
    }else if (type == ConfirmDialogType.key){
      /// 必须带key的确认弹窗，使用同类方法[closeKey]关闭指定key的dialog
      SmartDialog.show(
        alignment: Alignment.center,
        tag: key,
        clickMaskDismiss:barrierDismissible,
        builder: (BuildContext context) {
          return YBDConfirmDialog(title, cancelTitle, sureTitle, content: content, onCancelCallback: () {
          SmartDialog.dismiss(tag: key);
          if (onCancelCallback != null) onCancelCallback();
        }, onSureCallback: () {
          SmartDialog.dismiss(tag: key);
          if (onSureCallback != null) onSureCallback();
        }, endTimeAutoClose: endTimeAutoClose);},
      );
    }else if (type == ConfirmDialogType.overlay) {
      /// 有个问题：物理返回可以穿透返回，overlay一直在上面！！！
      showDialogs(context: context,
          barrierDismissible: barrierDismissible,
          child: YBDConfirmDialog(title, cancelTitle, sureTitle, content: content, onCancelCallback: () {
            YBDCustomDialog.pop();
            if (onCancelCallback != null) onCancelCallback();
          }, onSureCallback: () {
            YBDCustomDialog.pop();
            if (onSureCallback != null) onSureCallback();
          }, endTimeAutoClose: endTimeAutoClose)
      );
    }

  }

  /// 关闭指定key的dialog, 只在[ConfirmDialogType.key]下有效
  static closeKey({required String key}) {
    SmartDialog.dismiss(tag: key);
  }

  String? title; // 标题
  String? content; // 内容
  String? cancelTitle; // Cancels the button title, null value does not exist
  String? sureTitle; // Confirm the button title
  Function? onSureCallback;
  Function? onCancelCallback;
  int? endTimeAutoClose = -1; // 自动关闭时间ms

  YBDConfirmDialog(this.title, this.cancelTitle, this.sureTitle,
      {this.content, this.onSureCallback, this.onCancelCallback, this.endTimeAutoClose});

  @override
  _YBDConfirmDialogState createState() => _YBDConfirmDialogState();
}

/// 确认的弹框
class _YBDConfirmDialogState extends State<YBDConfirmDialog> {
  @override
  void initState() {
    super.initState();
    logger.v('22.3.5--widget.endTimeAutoClose:${widget.endTimeAutoClose}');

    if (widget.endTimeAutoClose! > 0) {
      logger.v("confirm_dialog auto close countdown: ${widget.endTimeAutoClose}");
      Future.delayed(Duration(milliseconds: widget.endTimeAutoClose!), () {
        logger.v("confirm_dialog auto close! mounted: $mounted");
        if (widget.onCancelCallback != null && mounted) {
          widget.onCancelCallback!();
        }
      });
    }
  }
  void initStateOqpoVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {},
                child: Container(
                  width: ScreenUtil().setWidth(500),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(ScreenUtil().setWidth(32)),
                      ),
                      color: Colors.white),
                  child: Column(
                    children: <Widget>[
                      Container(
                        constraints: BoxConstraints(minHeight: ScreenUtil().setWidth(200)), // Set the minimum height
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(height: ScreenUtil().setWidth(20)),
                            _title(),
                            _content(),
                            SizedBox(height: ScreenUtil().setWidth(30))
                          ],
                        ),
                      ),
                      _actionButtons(context),
                      SizedBox(height: ScreenUtil().setWidth(40)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildCdPrPoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消和确定按钮
  Widget _actionButtons(BuildContext context) {
    // In case of only sureTitle
    if (widget.sureTitle!.isNotEmpty && (widget.cancelTitle == null || widget.cancelTitle!.isEmpty)) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          YBDScaleAnimateButton(
            onTap: () {
              if (widget.onSureCallback != null && mounted) {
                widget.onSureCallback!();
              }
            },
            child: _bottomBtn(widget.sureTitle!, 1, longbtn: true),
          ),
        ],
      );
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        YBDScaleAnimateButton(
          onTap: () {
            if (widget.onCancelCallback != null && mounted) {
              widget.onCancelCallback!();
            }
          },
          child: _bottomBtn(widget.cancelTitle!, 0),
        ),
        SizedBox(width: ScreenUtil().setWidth(40)),
        YBDScaleAnimateButton(
          onTap: () {
            if (widget.onSureCallback != null && mounted) {
              widget.onSureCallback!();
            }
          },
          child: _bottomBtn(widget.sureTitle!, 1),
        ),
      ],
    );
  }
  void _actionButtonstI823oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 标题
  Widget _title() {
    if (widget.title == null || widget.title!.isEmpty) {
      return Container();
    }
    return Container(
      margin: EdgeInsets.fromLTRB(
          ScreenUtil().setWidth(30), ScreenUtil().setWidth(20), ScreenUtil().setWidth(30), ScreenUtil().setWidth(10)),
      child: Center(
        child: Text(
          // 对话框标题
          widget.title!,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            fontWeight: FontWeight.w500,
            height: 1.5,
            color: Color.fromRGBO(0, 0, 0, 0.85),
          ),
        ),
      ),
    );
  }
  void _title0aL96oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 内容
  Widget _content() {
    if (widget.content == null || widget.content!.isEmpty) {
      return Container();
    }
    return Container(
      margin: EdgeInsets.fromLTRB(
          ScreenUtil().setWidth(30), ScreenUtil().setWidth(20), ScreenUtil().setWidth(30), ScreenUtil().setWidth(10)),
      child: Center(
        child: Text(
          // 对话框内容
          widget.content!,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            fontWeight: FontWeight.w400,
            height: 1.5,
            color: Color.fromRGBO(0, 0, 0, 0.85),
          ),
        ),
      ),
    );
  }
  void _contentRQuLOoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 底部按钮 index==0为灰色，否则为渐变蓝色 longbtn=true 则较长；longbtn=false则常规
  Widget _bottomBtn(String title, int index, {bool longbtn = false}) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
        gradient: LinearGradient(
          colors: index == 0 ? [Color(0xffCCCCCC), Color(0xffCCCCCC)] : [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      width: ScreenUtil().setWidth(longbtn ? 300 : 200),
      height: ScreenUtil().setWidth(64),
      alignment: Alignment.center,
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(28),
          color: Colors.white,
        ),
      ),
    );
  }
  void _bottomBtnZjYVZoyelive(String title, int index, {bool longbtn = false}) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
