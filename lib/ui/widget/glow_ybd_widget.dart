
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class YBDGlowWidget extends StatefulWidget {
  final Widget? child;
  final double? endRadius;
  final BoxShape shape;
  final Duration duration;
  final bool repeat;
  final bool animate;
  final Duration repeatPauseDuration;
  final Curve curve;
  final bool showGlows;
  final Color glowColor;
  final Duration? startDelay;

  const YBDGlowWidget({
    Key? key,
    this.child,
    this.endRadius,
    this.shape = BoxShape.circle,
    this.duration = const Duration(milliseconds: 2000),
    this.repeat = true,
    this.animate = true,
    this.repeatPauseDuration = const Duration(milliseconds: 100),
    this.curve = Curves.fastOutSlowIn,
    this.showGlows = true,
    this.glowColor = Colors.white,
    this.startDelay,
  }) : super(key: key);

  @override
  _YBDGlowWidgetState createState() => _YBDGlowWidgetState();
}

class _YBDGlowWidgetState extends State<YBDGlowWidget> with SingleTickerProviderStateMixin {
  late var controller;
  late var _curve;
  late Animation<double> _smallDiscAnimation;
  late Animation<double> _bigDiscAnimation;
  late Animation<double> _alphaAnimation;

  Function(AnimationStatus status)? _statusListener;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: widget.duration,
      vsync: this,
    );
    _curve = CurvedAnimation(
      parent: controller,
      curve: widget.curve,
    );
    _smallDiscAnimation = Tween(
      begin: (widget.endRadius! * 2) / 6,
      end: (widget.endRadius! * 2) * (3 / 4),
    ).animate(_curve);
    _bigDiscAnimation = Tween(
      begin: 0.0,
      end: (widget.endRadius! * 2),
    ).animate(_curve);
    _alphaAnimation = Tween(
      begin: 0.30,
      end: 0.0,
    ).animate(controller);

    _statusListener = (_) async {
      if (controller.status == AnimationStatus.completed) {
        await Future.delayed(widget.repeatPauseDuration);
        if (mounted && widget.repeat && widget.animate) {
          controller.reset();
          controller.forward();
        }
      }
    };
    if (widget.animate) {
      _startAnimation();
    }
  }
  void initState3t05Hoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDGlowWidget oldWidget) {
    if (widget.animate != oldWidget.animate) {
      if (widget.animate) {
        _startAnimation();
      } else {
        _stopAnimation();
      }
    }
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetiuYepoyelive(YBDGlowWidget oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _startAnimation() async {
    controller.addStatusListener(_statusListener);
    if (widget.startDelay != null) {
      await Future.delayed(widget.startDelay ?? Duration());
    }
    if (mounted) {
      controller.reset();
      controller.forward();
    }
  }
  void _startAnimationkS47Loyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _stopAnimation() async {
    controller.removeStatusListener(_statusListener);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _alphaAnimation,
      child: widget.child,
      builder: (context, widgetChild) {
        final decoration = BoxDecoration(
            shape: widget.shape,
            // If the user picks a curve that goes below 0 or above 1
            // this opacity will have unexpected effects without clamping
            color: widget.glowColor.withOpacity(
              _alphaAnimation.value.clamp(
                0.0,
                1.0,
              ),
            ),
            borderRadius:
                widget.shape == BoxShape.circle ? null : BorderRadius.all(Radius.circular(ScreenUtil().setWidth(26))));
        return Container(
          height: widget.endRadius! * 2,
          width: widget.endRadius! * (widget.shape == BoxShape.circle ? 2 : 4),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              if (widget.showGlows) ...[
                AnimatedBuilder(
                  animation: _bigDiscAnimation,
                  builder: (context, x) {
                    // If the user picks a curve that goes below 0,
                    // this will throw without clamping
                    final num size = _bigDiscAnimation.value.clamp(
                      0.0,
                      double.infinity,
                    );
                    return Container(
                      height: size as double,
                      width: widget.shape == BoxShape.circle ? size : (size * 2),
                      decoration: decoration,
                      // color: Colors.red,
                    );
                  },
                ),
                AnimatedBuilder(
                  animation: _smallDiscAnimation,
                  builder: (context, x) {
                    final num size = _smallDiscAnimation.value.clamp(
                      0.0,
                      double.infinity,
                    );

                    return Container(
                      height: ((size as double) * (widget.shape == BoxShape.circle ? 1.2 : 1)),
                      width: (size * (widget.shape == BoxShape.circle ? 1.2 : 2.3)),
                      decoration: decoration,
                      // color: Colors.white,
                    );
                  },
                )
              ],
              widgetChild ?? Container(),
            ],
          ),
        );
      },
    );
  }
  void build1YszRoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
