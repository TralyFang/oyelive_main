
/*
 * @Author: William-Zhou
 * @Date: 2022-03-28 10:56:59
 * @LastEditTime: 2022-03-28 16:42:26
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';

class YBDSearchIdTag extends StatelessWidget {
  YBDUserInfo? user;
  YBDSearchIdTag({Key? key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (user != null && user!.uniqueNum != null)
      return Row(
        children: [
          Stack(
            alignment: Alignment.centerLeft,
            children: [
              Container(
                height: ScreenUtil().setWidth(30),
                margin: EdgeInsets.only(right: ScreenUtil().setWidth(10)),
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(10),
                  vertical: ScreenUtil().setWidth(5),
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: ScreenUtil().setWidth(1)),
                  borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
                  gradient: LinearGradient(
                    colors: [Color(0xffFBBD1A), Color(0xffFCD323)],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                ),
                child: Row(
                  children: [
                    YBDTPGlobal.wSizedBox(18),
                    Text(
                      'ID:${user!.uniqueNum}',
                      style: TextStyle(fontSize: ScreenUtil().setSp(16), color: Colors.white),
                    ),
                  ],
                ),
              ),
              Image.asset(
                "assets/images/u_crown.webp",
                width: ScreenUtil().setWidth(24),
                fit: BoxFit.fitWidth,
              ),
            ],
          ),
        ],
      );
    return Container(
      height: ScreenUtil().setWidth(30),
      margin: EdgeInsets.only(right: ScreenUtil().setWidth(10)),
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10), vertical: ScreenUtil().setWidth(5)),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(20))),
          color: Colors.black.withOpacity(0.2)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            'ID:${user!.id}',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(16),
              color: YBDActivitySkinRoot().curAct().userProfileIDColor(0.79),
            ),
          ),
        ],
      ),
    );
  }
  void buildIsxs3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
