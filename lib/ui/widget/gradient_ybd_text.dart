import 'package:flutter/material.dart';

class YBDGradientText extends StatelessWidget {
  String? text;
  String? fontFamily;
  double? fontSize;
  List<Color>? textColors;
  FontWeight? fontWeight;
  YBDGradientText({this.text, this.fontFamily, this.fontSize, this.textColors, this.fontWeight});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(text ?? '',
        style: TextStyle(
            fontSize: fontSize,
            fontWeight: fontWeight,
            foreground: Paint()
              ..shader = LinearGradient(colors: textColors ?? [], begin: Alignment.topCenter, end: Alignment.bottomCenter)
                  .createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 100.0))));
  }
  void buildomH5poyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
