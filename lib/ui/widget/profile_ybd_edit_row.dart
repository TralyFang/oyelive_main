
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/intl_ybd_lang_util.dart';
import 'package:oyelive_main/common/util/resource_ybd_path_util.dart';
import 'package:oyelive_main/ui/page/profile/setting/setting_ybd_item.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';
import '../../common/util/common_ybd_util.dart';
import '../../common/util/image_ybd_util.dart';
import '../../common/util/log_ybd_util.dart';
import '../../module/user/entity/user_ybd_info_entity.dart';
import '../../redux/app_ybd_state.dart';
import '../page/status/local_audio/scale_ybd_animate_button.dart';

/// 行类型
enum EditType {
  ID,
  NickName,
  Avatar,
  Language,
  Location,
  Email,
  Gender,
  Birthday,
  Bio,
  Cover,
  Phone,
  Background,
  ReplaceBg
}

// 点击事件回调方法
typedef ClickedRightCallbackFunc = Function();

// 点击事件回调方法
typedef ClickedSexCallbackFunc = Function(int sex);

/// 编辑 profile 行
class YBDProfileEditRow extends StatefulWidget {
  /// 行标题
  final String title;

  /// 行类型
  final EditType type;

  /// 用户信息
  final YBDUserInfo? userInfo;

  /// title右边的问号按钮
  final bool showInfoBtn;

  /// 问号按钮的点击事件
  final Function? infoBtnClick;

  /// 选择的生日
  String? birthday;

  /// 点击右侧部分回调
  ClickedRightCallbackFunc? clickedCallback;

  /// 点击性别按钮回调
  ClickedSexCallbackFunc? clickedSexCallback;

  YBDProfileEditRow(
    this.title,
    this.type,
    this.userInfo, {
    this.clickedCallback,
    this.clickedSexCallback,
    this.birthday,
    this.showInfoBtn = false,
    this.infoBtnClick,
  });

  @override
  YBDProfileEditRowState createState() => new YBDProfileEditRowState();
}

class YBDProfileEditRowState extends State<YBDProfileEditRow> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth,
      // height: ScreenUtil().setWidth(96),
      constraints: BoxConstraints(minHeight: ScreenUtil().setWidth(96)),
      color: Colors.white,
      child: Column(children: <Widget>[
        widget.type == EditType.Gender ? rowContentView() : clickableRow(),
        Divider(height: ScreenUtil().setWidth(1), thickness: ScreenUtil().setWidth(1), color: Color(0xffEAEAEA)),
      ]),
    );
  }
  void buildD8VSUoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 有点击效果的行
  Widget clickableRow() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          logger.v('clicked ${widget.title}');
          // rightSubviewAction();
          widget.clickedCallback?.call();
        },
        child: rowContentView(),
      ),
    );
  }
  void clickableRowJjiNZoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 行内容
  Widget rowContentView() {
    Widget? rightSub = getRightSubview();
    return Container(
      constraints: BoxConstraints(minHeight: ScreenUtil().setWidth(96)),
      padding: EdgeInsets.fromLTRB(ScreenUtil().setWidth(30), 0, ScreenUtil().setWidth(30), 0),
      child: Column(children: <Widget>[
        Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
          Text(
            widget.title,
            style: TextStyle(
              color: Color(0xff333333),
              fontSize: ScreenUtil().setSp(28),
            ),
          ),
          if (widget.showInfoBtn && widget.infoBtnClick != null) ...[
            GestureDetector(
                onTap: widget.infoBtnClick as void Function()?,
                child: Image.asset(
                  "assets/images/liveroom/room_guardian_question.png",
                  color: Color(0xffB3B3B3),
                  width: ScreenUtil().setWidth(50),
                )),
          ],
          Expanded(child: Container()),
          Container(
              alignment: Alignment.centerRight,
              // height: ScreenUtil().setWidth(94),
              constraints: BoxConstraints(minHeight: ScreenUtil().setWidth(96)),
              // width: ScreenUtil().setWidth(370),
              child: rightSub),
        ]),
      ]),
    );
  }
  void rowContentViewAAkrNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 性别按钮响应事件
  clickedGenderButtonAction(int sexValue) {
    logger.v('gender button action');

    if (null != widget.clickedSexCallback) {
      widget.clickedSexCallback!(sexValue);
    } else {
      logger.v('sex callback is empty');
    }
  }

  /// 返回性别按钮的装饰
  /// type 1 表示 female, 2 表示 male
  BoxDecoration genderBoxDecoration(int type) {
    // 选中状态
    if (widget.userInfo!.sex == type) {
      return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
        gradient: LinearGradient(
          colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      );
    } else {
      return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30))),
        border: Border.all(color: Color(0xffC5C5C5), width: ScreenUtil().setWidth(1)),
        color: Colors.white,
      );
    }
  }

  /// 返回性别按钮的装饰
  /// type 1 表示 female, 2 表示 male
  TextStyle genderTextStyle(int type) {
    // 选中状态
    if (widget.userInfo!.sex == type) {
      return TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(28),
      );
    } else {
      return TextStyle(
        color: Color(0xff717171),
        fontSize: ScreenUtil().setSp(28),
      );
    }
  }

  /// 行右侧视图
  Widget? getRightSubview() {
    Widget? rightSub;
    Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);

    switch (widget.type) {
      case EditType.Avatar:
        rightSub = Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(vertical: 20.px as double),
              child: YBDRoundAvatar(
                store!.state.bean!.headimg,
                sex: store.state.bean!.sex,
                avatarWidth: 100,
                userId: store.state.bean!.id,
                needNavigation: false,
                labelWitdh: 50,
                lableSrc: (store.state.bean?.vip ?? 0) == 0 ? "" : "assets/images/vip_lv${store.state.bean!.vip}.png",
                scene: "C",
                avatarType: RoundAvatarType.EditProfile,
                labelPadding: EdgeInsets.only(right: ScreenUtil().setWidth(18)),
              ),
            ),
            Icon(Icons.keyboard_arrow_right, color: Color(0xffADAFB2))
          ],
        );
        break;
      case EditType.ID:
        rightSub = Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text('${widget.userInfo!.id}', style: TextStyle(color: Color(0xffADAFB2), fontSize: ScreenUtil().setSp(28))),
            Image.asset("assets/images/icon_copy_black.png", width: ScreenUtil().setWidth(44))
          ],
        );
        break;
      case EditType.NickName:
        rightSub = Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              widget.userInfo!.nickname ?? '',
              maxLines: 2,
              style: TextStyle(color: Color(0xffADAFB2), fontSize: 28.sp2),
            ),
            YBDTPGlobal.wSizedBox(45)
          ],
        );
        break;
      case EditType.Language:
        rightSub = Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              getLanguage(store!.state.locale!.languageCode) ?? '',
              maxLines: 2,
              style: TextStyle(color: Color(0xffADAFB2), fontSize: ScreenUtil().setSp(28)),
            ),
            Icon(
              Icons.keyboard_arrow_right,
              color: Color(0xffADAFB2),
            )
          ],
        );
        break;
      case EditType.Location:
        rightSub = Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            if ((widget.userInfo!.country ?? '').isNotEmpty) ...[
              // Icon(Icons.location_on, color: Color(0xff3A3A3A)),
              Text(
                YBDCommonUtil.getCountryFlagByCode(widget.userInfo!.country)!,
                style: TextStyle(fontSize: 24),
              ),
              SizedBox(width: ScreenUtil().setWidth(8)),
              Text(
                YBDCommonUtil.getCountryByCode(widget.userInfo!.country)!,
                style: TextStyle(color: Color(0xffADAFB2), fontSize: ScreenUtil().setSp(28)),
              )
            ],
            Icon(Icons.keyboard_arrow_right, color: Color(0xffADAFB2))
          ],
        );
        break;
      case EditType.Email:
        rightSub = Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              widget.userInfo!.email == null || widget.userInfo!.email!.isEmpty
                  ? translate('complete_now')
                  : widget.userInfo!.email!,
              style: TextStyle(
                  color: widget.userInfo!.email == null || widget.userInfo!.email!.isEmpty
                      ? Color(0xff49C7CE)
                      : Color(0xffADAFB2),
                  fontSize: 28.sp2),
            ),
            YBDTPGlobal.wSizedBox(45)
          ],
        );
        break;
      case EditType.Phone:
        rightSub = Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              widget.userInfo!.cellphone == null || widget.userInfo!.cellphone!.isEmpty
                  ? translate('complete_now')
                  : YBDSettingItem.cellphoneStar(store!.state.bean!.cellphone ?? ''),
              style: TextStyle(
                  color: widget.userInfo!.cellphone == null || widget.userInfo!.cellphone!.isEmpty
                      ? Color(0xff49C7CE)
                      : Color(0xffADAFB2),
                  fontSize: 28.sp2),
            ),
            YBDTPGlobal.wSizedBox(45)
          ],
        );
        break;
      case EditType.Gender:
        rightSub = Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            YBDScaleAnimateButton(
              onTap: () {
                logger.v('clicked female button');
                clickedGenderButtonAction(1);
              },
              child: Container(
                width: ScreenUtil().setWidth(160),
                height: ScreenUtil().setWidth(60),
                decoration: genderBoxDecoration(1),
                child: Center(
                  child: Text(translate('female'), style: genderTextStyle(1)),
                ),
              ),
            ),
            SizedBox(width: ScreenUtil().setWidth(27)),
            YBDScaleAnimateButton(
              onTap: () {
                logger.v('clicked male button');
                clickedGenderButtonAction(2);
              },
              child: Container(
                width: ScreenUtil().setWidth(160),
                height: ScreenUtil().setWidth(60),
                decoration: genderBoxDecoration(2),
                child: Center(
                  child: Text(translate('male'), style: genderTextStyle(2)),
                ),
              ),
            ),
            YBDTPGlobal.wSizedBox(45)
          ],
        );
        break;
      case EditType.Birthday:
        rightSub = Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              null != widget.userInfo!.birthday
                  ? DateFormat('yyyy-MM-dd').format(DateTime.fromMillisecondsSinceEpoch(widget.userInfo!.birthday!))
                  : '',
              style: TextStyle(color: Color(0xffADAFB2), fontSize: ScreenUtil().setSp(28)),
            ),
            YBDTPGlobal.wSizedBox(45)
          ],
        );
        break;
      case EditType.Bio:
        rightSub = Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              alignment: Alignment.centerRight,
              width: 555.px,
              child: Text(
                widget.userInfo!.comment ?? 'Hey, I am an OyeTalk member.',
                style: TextStyle(color: Color(0xffADAFB2), fontSize: 28.sp2),
              ),
            ),
            YBDTPGlobal.wSizedBox(45)
          ],
        );
        break;
      case EditType.Cover:
        // TODO: 海报有没有默认图像
        rightSub = Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(vertical: 20.px as double),
              width: ScreenUtil().setWidth(100),
              height: ScreenUtil().setWidth(100),
              child: ClipOval(
                child: YBDNetworkImage(
                  imageUrl: YBDImageUtil.cover(context, widget.userInfo!.roomimg, widget.userInfo!.id, scene: 'A'),
                  placeholder: (context, url) =>
                      Image.asset(YBDResourcePathUtil.defaultMaleAssets(male: widget.userInfo!.sex == 2)),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Icon(Icons.keyboard_arrow_right, color: Color(0xffADAFB2))
          ],
        );
        break;
    }

    return rightSub;
  }

  getLanguage(code) {
    String language;
    switch (code) {
      case YBDIntlLangUtil.ar:
        language = translate('language.arabic');
        break;
      case YBDIntlLangUtil.ur:
        language = translate('language.urdu');
        break;
      case YBDIntlLangUtil.hi:
        language = translate('language.hindi');
        break;
      case YBDIntlLangUtil.tr:
        language = translate('language.turkish');
        break;
      case YBDIntlLangUtil.id:
        language = translate('language.indonesia');
        break;
      case YBDIntlLangUtil.en:
      default:
        language = translate('language.en');
        break;
    }
    return language;
  }
}
