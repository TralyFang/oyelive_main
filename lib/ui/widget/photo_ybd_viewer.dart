
import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

// import 'package:access_photo/access_photo.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/remote_ybd_config_service.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/ui/widget/scaffold/nav_ybd_back_button.dart';

class YBDPhotoViewer extends StatefulWidget {
  List<String> imagePath;
  int initIndex;
  bool? download;

  YBDPhotoViewer(this.imagePath, this.initIndex, {this.download});

  @override
  YBDPhotoViewState createState() => new YBDPhotoViewState();
}

class YBDPhotoViewState extends BaseState<YBDPhotoViewer> {
  late int _viewingIndex;
  bool showTitleBar = true;

  ReceivePort _port = ReceivePort();
  late String _localPath;
  String _picSrc = 'original';

  /// iOS 相册名称
  String albumName = 'Media';
  bool showDownloadIcon = false;

  onPageChanged(int index) {
    setState(() {
      _viewingIndex = index;
    });

    checkIfLocalExistViewingImage();
  }

  double getProgress(event) {
    if (event == null) {
      return 0.02;
    }
    double actualProgress = event.cumulativeBytesLoaded / event.expectedTotalBytes;
    if (actualProgress < 0.02) {
      return 0.02;
    }

    return actualProgress;
  }

  @override
  Widget myBuild(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      floatingActionButton: showDownloadIcon && widget.download!
          ? GestureDetector(
              onTap: () async {
                logger.v('clicked download button');
                await FlutterDownloader.enqueue(
                    url: widget.imagePath[_viewingIndex],
                    savedDir: _localPath,
                    showNotification: true,
                    openFileFromNotification: true,
                    fileName: getFileName());
              },
              child: Container(
                padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
                margin: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(80)),
                decoration: BoxDecoration(
                    border: Border.all(color: Color(0x66ffffff), width: ScreenUtil().setWidth(1)),
                    color: Color(0x66000000),
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(4)))),
                child: Icon(
                  Icons.file_download,
                  color: Colors.white,
                ),
              ),
            )
          : Container(),
      body: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              logger.v('clicked image');
              setState(() {
                showTitleBar = !showTitleBar;
              });
            },
            child: Container(
                color: Colors.black,
                child: (PhotoViewGallery.builder(
                  scrollPhysics: const ClampingScrollPhysics(),
                  builder: (BuildContext context, int index) {
                    return PhotoViewGalleryPageOptions(
                      imageProvider: CachedNetworkImageProvider(
                        _picSrc == 'scene_b'
                            ? YBDImageUtil.status(context, widget.imagePath[index], scene: "B")!
                            : YBDImageUtil.status(
                                context,
                                widget.imagePath[index],
                              )!,
                      ),
                      initialScale: PhotoViewComputedScale.contained * 1,
                    );
                  },
                  itemCount: widget.imagePath.length,
                  loadingBuilder: (context, event) => Center(
                    child: Container(
                      width: 20.0,
                      height: 20.0,
                      color: Colors.black,
                      child: CircularProgressIndicator(
                        value: getProgress(event),
                      ),
                    ),
                  ),
                  pageController: new PageController(initialPage: widget.initIndex),
                  onPageChanged: onPageChanged,
                ))),
          ),
          showTitleBar
              ? Container(
                  height: ScreenUtil().statusBarHeight + ScreenUtil().setWidth(100),
                  child: AppBar(
                    leading: YBDNavBackButton(),
                    title:
                        Text('${_viewingIndex + 1}/${widget.imagePath.length}', style: TextStyle(color: Colors.white)),
                    backgroundColor: Color(0x66000000),
                    centerTitle: true,
                    elevation: 0.0,
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
  void myBuild6juwnoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  static void downloadCallback(String id, DownloadTaskStatus status, int progress) {
    logger.v('Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    final SendPort send = IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  void _unbindBackgroundIsolate() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
  }
  void _unbindBackgroundIsolaterBjOHoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void _bindBackgroundIsolate() {
    bool isSuccess = IsolateNameServer.registerPortWithName(_port.sendPort, 'downloader_send_port');
    if (!isSuccess) {
      _unbindBackgroundIsolate();
      _bindBackgroundIsolate();
      return;
    }
    _port.listen((dynamic data) {
      debugPrint('UI Isolate Callback: $data');
      DownloadTaskStatus? status = data[1];

      if (status == DownloadTaskStatus.complete) {
        logger.v('image download complete');
        showDownloadIcon = false;
        String imgName = '$_localPath${Platform.pathSeparator}${getFileName()}';
        logger.v('image saved to : $imgName');

        // 保存下载好的图片到相册
        saveImageToMediaAlbum(imgName);

        if (Platform.isIOS) {
          // TODO: 翻译
          YBDToastUtil.toast('Picture saved successfully', gravity: ToastGravity.CENTER);
        } else {
          YBDToastUtil.toast('${translate('save_to')}$imgName}');
        }
      }

      setState(() {});
    });
  }

  /// iOS 平台保存本地图片到相册
  saveImageToMediaAlbum(String imgPath) async {
    if (Platform.isIOS) {
      logger.v('save album from local path : $imgPath');
      // final result = await AccessPhoto.saveImage(imgPath);
      // logger.v('save image result : $result');
    }
  }

  getFileName() {
    return widget.imagePath[_viewingIndex].split('/').last;
  }

  /// 检查文件是否已经存在，不存在则显示下载按钮
  checkIfLocalExistViewingImage() async {
    var filePath = File('$_localPath${Platform.pathSeparator}${getFileName()}');
    showDownloadIcon = !(await filePath.exists());
    logger.v("${filePath.path} exist: ${!showDownloadIcon}");
    logger.v('should show download icon : $showDownloadIcon');
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _viewingIndex = widget.initIndex;

    _bindBackgroundIsolate();

    /// 下载状态监听
    FlutterDownloader.registerCallback(downloadCallback);
    _prepare();
  }
  void initState1KyqIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _prepare() async {
    /// 动态图片大图预览时图片地址 scene_b: 取场景B图片地址；      original：取原始图片地址
    YBDRemoteConfigService? r = await YBDRemoteConfigService.getInstance();
    _picSrc = r?.getConfig()?.getString("status_picture_preview_src") ?? 'original';

    String baseDir;
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    if (Platform.isAndroid) {
      baseDir = await ExtStorage.getExternalStoragePublicDirectory(ExtStorage.DIRECTORY_PICTURES);
    } else {
      baseDir = (await getApplicationDocumentsDirectory()).path;
    }
    logger.v('base photo viewer download path : $baseDir');

    if (null != baseDir) {
      logger.v('platform path separator : ${Platform.pathSeparator}');
      logger.v('package info app name : ${packageInfo.appName}');
      _localPath = '$baseDir${Platform.pathSeparator}${packageInfo.appName}';
      logger.v('local path : $_localPath');
    } else {
      logger.v('base url is empty');
    }

    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    } else {
      logger.v('photo save directory already exist');
    }

    await checkIfLocalExistViewingImage();
    logger.v("_localPath: $_localPath exist: ${await savedDir.exists()}");
  }

  @override
  void dispose() {
    _unbindBackgroundIsolate();
    super.dispose();
  }
  void disposerFavkoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didUpdateWidget(YBDPhotoViewer oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
