
import 'package:flutter/material.dart';
import '../page/room/entity/mini_ybd_game_info.dart';
import '../page/room/mic/mic_ybd_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// 帧动画Image
class YBDFrameAnimationImage extends StatefulWidget {
  final List<String> _assetList;
  final double? width;
  final double? height;
  int? interval = 200;
  int? hidedelayTime = 100;

  /// 播放回调
  final VoidCallback? playCallback;

  YBDFrameAnimationImage(this._assetList, {this.width, this.height, this.interval, this.playCallback, this.hidedelayTime});

  @override
  State<StatefulWidget> createState() {
    return _YBDFrameAnimationImageState();
  }
}

class _YBDFrameAnimationImageState extends State<YBDFrameAnimationImage> with SingleTickerProviderStateMixin {
  // 动画控制
  late Animation<double> _animation;
  late AnimationController _controller;
  int? interval = 200;

  List<Widget> images = [];

  int playing = 0; //0  初始值   1 播放中   2播放完成

  @override
  void initState() {
    super.initState();

    if (widget.interval != null) {
      interval = widget.interval;
    }
    final int imageCount = widget._assetList.length;
    final int maxTime = interval! * imageCount;

    // 启动动画controller
    _controller = new AnimationController(duration: Duration(milliseconds: maxTime), vsync: this);
    _controller.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
//        _controller.forward(from: 0.0); // 完成后重新开始
        print('AnimationStatus.completed');
        Future.delayed(Duration(milliseconds: widget.hidedelayTime!), () {
          widget.playCallback!();
        });
        _controller.stop(); // 完成后停止
        if (mounted) {
          setState(() {
            playing = 2;
          });
        }
      }
    });

    _animation = new Tween<double>(begin: 0, end: imageCount.toDouble()).animate(_controller)
      ..addListener(() {
        setState(() {
          // the state that has changed here is the animation object’s value
        });
      });

    _controller.forward();
    playing = 1;
    print('AnimationStatus.forward');
  }
  void initStatererV7oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  void disposehUnp8oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    if (playing == 2) {
      return Image.asset(
        widget._assetList[widget._assetList.length - 1],
        fit: BoxFit.cover,
        width: ScreenUtil().setWidth(100),
        height: ScreenUtil().setWidth(100),
      );
    } else if (playing == 1) {
      int ix = _animation.value.floor() % widget._assetList.length;

      List<Widget> images = [];
      // 把所有图片都加载进内容，否则每一帧加载时会卡顿
      for (int i = 0; i < widget._assetList.length; ++i) {
        if (i != ix) {
          images.add(Image.asset(
            widget._assetList[i],
            width: 0,
            height: 0,
          ));
        }
      }

      images.add(Image.asset(
        widget._assetList[ix],
        width: widget.width,
        height: widget.height,
      ));
      return Stack(alignment: AlignmentDirectional.center, children: images);
    } else if (playing == 0) {}
    return Container();
  }
  void buildWQiUYoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
