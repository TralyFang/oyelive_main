
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/base/base_ybd_refresh_list_state.dart';
import 'package:oyelive_main/common/ext/enum_ext/type_ybd_ext.dart';
import 'package:oyelive_main/ui/widget/round_ybd_avatar.dart';

import '../../../../base/base_ybd_state.dart';
import '../../../../common/util/log_ybd_util.dart';

class YBDBlockItem extends StatefulWidget {
  final String? avatar;
  final String? nickName;
  final Function? callBack;
  final int? vip;
  final int? userId;
  final int? sex;
  final int? blackId;
  final HttpType type;

  YBDBlockItem(this.avatar, this.nickName, this.userId, this.type, {this.sex, this.vip, this.blackId, this.callBack});

  @override
  YBDBlockItemState createState() => new YBDBlockItemState();
}

class YBDBlockItemState extends BaseState<YBDBlockItem> {
  @override
  Widget myBuild(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
      margin: EdgeInsets.all(ScreenUtil().setWidth(15)),
      child: Row(
        children: [
          YBDRoundAvatar(
            // 头像
            widget.avatar,
            // 没有 sex 字段，默认为 1： 女
            sex: widget.sex ?? 1,
            lableSrc: (widget.vip ?? 0) == 0 ? "" : "assets/images/vip_lv${widget.vip}.png",
            userId: widget.userId,
            scene: "B",
            labelWitdh: 22,
            labelPadding: EdgeInsets.only(right: ScreenUtil().setWidth(5), bottom: ScreenUtil().setWidth(5)),
            avatarWidth: 96,
          ),
          SizedBox(width: ScreenUtil().setWidth(10)),
          Container(
            width: ScreenUtil().setWidth(400),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center, //就是与当前控件方向一致的轴
              crossAxisAlignment: CrossAxisAlignment.start, //就是与当前控件方向垂直的轴
              children: [
                Container(
                  child: Text(
                    widget.nickName!,
                    style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis, //内容超出控件  省略号
                    softWrap: true,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(10),
          ),
          GestureDetector(
              onTap: () {
                logger.v("Unblock onTap ${widget.userId}");
                removeBlackUser(widget.blackId ?? widget.userId);
              },
              child: Container(
                height: ScreenUtil().setWidth(56),
                width: ScreenUtil().setWidth(125),
                decoration: BoxDecoration(
                    color: Color(0xffffff).withOpacity(0.29),
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(30)))),
                alignment: Alignment.center,
                child: Text(
                  translate('Unblock'),
                  style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.white),
                ),
              )),
        ],
      ),
    );
  }
  void myBuildu7eW6oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  removeBlackUser(int? userId) async {
    showLockDialog();
    widget.type.operator(userId, (result) {
      dismissLockDialog();
      if (result) {
        widget.callBack!();
      }
    });
  }
}
