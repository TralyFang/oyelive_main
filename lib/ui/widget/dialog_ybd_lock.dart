
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../common/util/log_ybd_util.dart';

class YBDLockDialog extends StatefulWidget {
  String info;
  bool barrierDismissible;
  VoidCallback dismissCallback;

  YBDLockDialog(this.dismissCallback, {this.info: '', this.barrierDismissible = true});

  @override
  YBDLockDialogState createState() => new YBDLockDialogState();
}

class YBDLockDialogState extends State<YBDLockDialog> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v('clicked dialog background');
        if (widget.barrierDismissible) {
          logger.v('dismiss dialog');
          widget.dismissCallback();
        } else {
          logger.v('ignore');
        }
      },
      child: Material(
        type: MaterialType.transparency,
        child: Center(
          child: Container(
            // 加载框和文字的容器
            width: ScreenUtil().setWidth(200),
            height: ScreenUtil().setWidth(200),
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(6))),
            ),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Positioned(
                  bottom: 0,
                  child: Container(
                    // 文字
                    child: Text(
                      widget.info,
                      style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(22)),
                    ),
                  ),
                ),
                Container(
                  // 加载框
                  width: ScreenUtil().setWidth(100),
                  height: ScreenUtil().setWidth(100),
                  child: CircularProgressIndicator(
                    strokeWidth: ScreenUtil().setWidth(4),
                    valueColor: AlwaysStoppedAnimation<Color>(Color(0xffB770FA)),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(ScreenUtil().setWidth(24)),
                  child: Image.asset(
                    'assets/images/status/status_upload_progress.webp',
                    width: ScreenUtil().setWidth(54),
                    height: ScreenUtil().setWidth(54),
                    fit: BoxFit.contain,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  void buildDvQRzoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
  }
  void initStatepOkXToyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDLockDialog oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidget8KmQ2oyelive(YBDLockDialog oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
