
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:oyelive_main/common/util/file_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

import '../../base/base_ybd_state.dart';
import '../../common/util/toast_ybd_util.dart';

class YBDDownloadDialog extends StatefulWidget {
  String? downloadUrl;
  String? savePath, fileName;
  Function? onDownloadComplete;

  YBDDownloadDialog(this.downloadUrl, {this.onDownloadComplete(String filePath)?, this.savePath, this.fileName});

  @override
  YBDDownloadDialogState createState() => new YBDDownloadDialogState();
}

class YBDDownloadDialogState extends BaseState<YBDDownloadDialog> {
  ReceivePort _port = ReceivePort();
  String? _downloadPath, _fileName;

  double? downloadProgress = 0.0;

  // static void downloadCallback(String id, DownloadTaskStatus status, int progress) {
  //   debugPrint('Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
  //   final SendPort send = IsolateNameServer.lookupPortByName('downloader_send_port');
  //   send.send([id, status, progress]);
  // }
  //
  // void _unbindBackgroundIsolate() {
  //   IsolateNameServer.removePortNameMapping('downloader_send_port');
  // }

  void _bindBackgroundIsolate() async {
    // bool isSuccess = IsolateNameServer.registerPortWithName(_port.sendPort, 'downloader_send_port');
    // if (!isSuccess) {
    //   debugPrint('RSSSS failed');
    //   _unbindBackgroundIsolate();
    //   _bindBackgroundIsolate();
    //   return;
    // }
    // 用缓存下载文件
    DefaultCacheManager().getFileStream(widget.downloadUrl!, withProgress: true).listen((event) {
      if (event is FileInfo) {
        logger.v('getFileStream FileInfo: ${event.file.path}');
        File? file = YBDFileUtil.moveFileSync(event.file, '$_downloadPath/$_fileName');
        logger.v('getFileStream moved file: ${file?.path}');

        // 获取文件报错
        if (null == file) {
          logger.w('download background audio file is empty');
          if (ModalRoute.of(context)!.isCurrent) Navigator.pop(context);
          YBDToastUtil.toast(translate('download_failed'));
          return;
        }

        widget.onDownloadComplete?.call(file.path);
        if (ModalRoute.of(context)!.isCurrent) Navigator.pop(context);
      } else if (event is DownloadProgress) {
        logger.v('DownloadProgress: ${event.progress}');
        downloadProgress = event.progress;
        setState(() {});
      } else {
        logger.w('down load slog audio file stream type error');
      }
    });

    // _port.listen((dynamic data) {
    //   debugPrint('UI Isolate Callback: $data');
    //   DownloadTaskStatus status = data[1];
    //   int progress = data[2];
    //   downloadProgress = progress / 100;
    //   if (status == DownloadTaskStatus.complete) {
    //     widget.onDownloadComplete?.call(_downloadPath + Platform.pathSeparator + _fileName);
    //     if (ModalRoute.of(context).isCurrent) Navigator.pop(context);
    //   } else if (status == DownloadTaskStatus.failed) {
    //     if (ModalRoute.of(context).isCurrent) Navigator.pop(context);
    //     YBDToastUtil.toast(translate('download_failed'));
    //   }
    //
    //   setState(() {});
    // });
  }
  void _bindBackgroundIsolatexrQ4Zoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: new Material(
        type: MaterialType.transparency,
        child: new Center(
          child: Container(
            width: ScreenUtil().setWidth(160),
            height: ScreenUtil().setWidth(160),
            padding: EdgeInsets.all(ScreenUtil().setWidth(30)),
            decoration: BoxDecoration(
              color: Color(0xff000000),
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(6))),
            ),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                SizedBox(
                  width: ScreenUtil().setWidth(100),
                  height: ScreenUtil().setWidth(100),
                  child: CircularProgressIndicator(
                    strokeWidth: ScreenUtil().setWidth(4),
                    //backgroundColor: Colors.white,
                    value: downloadProgress,
                    semanticsLabel: downloadProgress.toString(),
                    valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                    backgroundColor: Color(0x88000000),
                    //值越大  圆圈越大
                  ),
                ),
                Text(
                  '${(downloadProgress! * 100).toInt()}%',
                  style: TextStyle(color: Colors.white),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  void myBuildZU7AYoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  setDownLoadInfo() async {
    if (widget.fileName == null) {
      _fileName = widget.downloadUrl!.split('/').last.replaceAll(".", ".");
    } else {
      _fileName = widget.fileName;
    }
    if (widget.savePath == null) {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      _downloadPath = (Platform.isAndroid
              ? await ExtStorage.getExternalStoragePublicDirectory(ExtStorage.DIRECTORY_DOWNLOADS)
              : (await getApplicationDocumentsDirectory()).path) +
          Platform.pathSeparator +
          packageInfo.appName;
    } else {
      _downloadPath = widget.savePath;
    }

    //check folder exist n create
    final savedDir = Directory(_downloadPath!);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      await savedDir.create(recursive: true);
    }

    logger.v('start download ----------url ${widget.downloadUrl} savedir $_downloadPath filename $_fileName');

    // FlutterDownloader.enqueue(
    //     url: widget.downloadUrl,
    //     savedDir: _downloadPath,
    //     showNotification: false,
    //     openFileFromNotification: true,
    //     fileName: _fileName);
  }

  @override
  void initState() {
    super.initState();
    _bindBackgroundIsolate();
    // FlutterDownloader.registerCallback(downloadCallback);
    setDownLoadInfo();
  }
  void initState3NwPMoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
    // _unbindBackgroundIsolate();
  }

  @override
  void didUpdateWidget(YBDDownloadDialog oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
  void didChangeDependenciestwcYeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
