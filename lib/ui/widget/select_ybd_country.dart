
/*
 * @Date: 2020-12-03 10:31:13
 * @LastEditors: William-Zhou
 * @LastEditTime: 2021-07-27 16:31:05
 * @FilePath: \oyetalk_flutter\lib\ui\widget\select_country.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'intl_input/countries.dart';

// 选择国家回调方法
typedef SelectedCallbackFunc = Function(Map<String, String> selected);

/// 选择国家弹框
class YBDSelectCountry extends StatefulWidget {
  final SelectedCallbackFunc onCountryChange;
  final bool editPhone;

  YBDSelectCountry(this.onCountryChange, {this.editPhone = false});

  @override
  YBDSelectCountryState createState() => new YBDSelectCountryState();
}

class YBDSelectCountryState extends State<YBDSelectCountry> {
  Map<String, String> _selectedCountry =
      YBDCountries.list().firstWhere((item) => item['code'] == 'US', orElse: () => YBDCountries.emptyCounty);
  List<Map<String, String>> filteredCountries = YBDCountries.list();

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (ctx, setState) => Dialog(
        child: Container(
          height: widget.editPhone ? ScreenUtil().setWidth(600) : null,
          child: Column(
            children: <Widget>[
              TextField(
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.search),
                    labelText: translate('search_by_country'),
                    isDense: true,
                    contentPadding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(20), vertical: ScreenUtil().setWidth(10)),
                  ),
                  onChanged: (value) {
                    setState(() {
                      filteredCountries = YBDCountries.list()
                          .where((country) => country['name']!.toLowerCase().contains(value.toLowerCase()))
                          .toList();
                    });
                  },
                  cursorColor: widget.editPhone ? Color(0xff7DFAFF) : null),
              SizedBox(height: ScreenUtil().setWidth(4)),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: filteredCountries.length,
                  itemBuilder: (ctx, index) => Column(
                    children: <Widget>[
                      ListTile(
                        leading: Text(
                          filteredCountries[index]['flag']!,
                          style: TextStyle(fontSize: 30),
                        ),
                        title: Text(
                          filteredCountries[index]['name']!,
                          maxLines: 2,
                          style: TextStyle(fontWeight: FontWeight.w700, color: Colors.black),
                        ),
                        trailing: Text(
                          filteredCountries[index]['dial_code']!,
                          style: TextStyle(fontWeight: FontWeight.w700, color: Colors.black),
                        ),
                        onTap: () {
                          _selectedCountry = filteredCountries[index];
                          widget.onCountryChange.call(_selectedCountry);
                          Navigator.of(context).pop();
                        },
                      ),
                      Divider(height: ScreenUtil().setWidth(1), thickness: ScreenUtil().setWidth(1)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildK7cLJoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
