
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:social_share/social_share.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_activity_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track_base.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/share/widget/game_ybd_friend_list_dialog.dart';

import '../../common/util/common_ybd_util.dart';
import '../../common/util/database_ybd_until.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/share_ybd_util.dart';
import '../../common/util/toast_ybd_util.dart';
import '../../main.dart';
import '../../module/entity/room_ybd_info_entity.dart';
import '../../module/status/entity/status_ybd_entity.dart';
import '../../module/status/status_ybd_api_helper.dart';
import '../../redux/app_ybd_state.dart';
import '../page/home/entity/banner_ybd_advertise_entity.dart';
import '../page/share/util/friend_ybd_share_util.dart';
import '../page/share/widget/friend_ybd_list_dialog.dart';
import '../page/status/handler/status_ybd_change_event.dart';
import '../page/status/handler/status_ybd_delete_event.dart';
import 'colored_ybd_safe_area.dart';

class YBDSharingWidget extends StatefulWidget {
  final String? title;
  final YBDStatusInfo? status;
  final YBDBannerAdvertiseRecord advertise;
  final YBDRoomInfo? room;
  final String? badgeUrl;
  bool shareInside;
  final Key? key;
  final onlyFriends; // 仅仅站内分享
  /// 游戏分享map里面必须带的以下参数
  // var title = data["title"];
  // var roomId = data["roomId"];
  // var content = data["content"];
  // var roomImg = data["roomImg"];
  final Map<String, dynamic>? gameShareData;

  // 游戏分享弹框
  final GameShareType? type;

  /// 尽游戏分享的数据

  YBDSharingWidget({
    this.title = 'Share with',
    this.status,
    this.key,
    adName,
    adUrl,
    adImg,
    adDetail,
    this.shareInside: true,
    this.room,
    this.badgeUrl,
    this.onlyFriends: false,
    this.gameShareData,
    this.type,
  })  : advertise = YBDBannerAdvertiseRecord(
          adname: adName,
          adurl: adUrl,
          img: adImg,
          detail: adDetail,
        ),
        super(key: key);

  @override
  State<StatefulWidget> createState() => _YBDSharingWidgetState();
}

class _YBDSharingWidgetState extends State<YBDSharingWidget> {
  static const WHATSAPP = 'WhatsApp';
  static const FACEBOOK = 'Facebook';
  static const INSTAGRAM = 'Instagram';
  static const MESSENGER = 'Messenger';
  static const SNAPCHAT = 'Snapchat';
  static const TWITTER = 'Twitter';
  static const SKYPE = 'Skype';

  /// 社交软件在手机上的安装信息
  /// 比如 {twitter: true, facebook: true, sms: true,
  /// whatsapp: true, instagram: true, telegram: false}
  Map<dynamic, dynamic>? _appsForShare = {};

  @override
  void initState() {
    super.initState();
    _checkAppsForShare();
  }
  void initStatefwTcqoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    final Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);

    return ClipRRect(
      key: widget.key,
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(ScreenUtil().setWidth(16)), topRight: Radius.circular(ScreenUtil().setWidth(16))),
      child: Container(
        color: Colors.white,
        height: ScreenUtil().setWidth(widget.shareInside ? 460 : 320),
        child: YBDColoredSafeArea(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: ScreenUtil().setWidth(36), bottom: ScreenUtil().setWidth(54)),
                child: GestureDetector(
                  onDoubleTap: () {
                    YBDDataBaseUtil.instance!.clearDynamicLink();
                  },
                  child: Text(
                    widget.title!,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(30),
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              if (widget.onlyFriends) // 只是站内分享
                Row(
                  children: [
                    _shareItem(context, 'assets/images/share_friend_icon.png', translate('friends'), onTap: (text) {
                      showShareToFriendsDialog(context);
                    }, needCheckInstalled: false)
                  ],
                ),
              if (!widget.onlyFriends) // 之前的逻辑
                ...shareItems(store),
            ],
          ),
        ),
      ),
    );
  }
  void builduzcFUoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  List<Widget> shareItems(Store<YBDAppState>? store) {
    return [
      Expanded(
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            if (widget.shareInside)
              _shareItem(context, 'assets/images/share_friend_icon.png', translate('friends'), onTap: (text) {
                showShareToFriendsDialog(context);
              }, needCheckInstalled: false),
            ...getThirdShareList()
          ],
        ),
      ),
      if (widget.shareInside)
        Expanded(
          child: Row(
            children: <Widget>[
              _shareItem(context, 'assets/images/icon_copylink.png', 'Copy Link', needCheckInstalled: false),
              widget.status != null && int.parse(widget.status!.userId!) != store!.state.bean?.id // 其他人发布的动态显示 举报按钮
                  ? _shareItem(context, 'assets/images/icon_report.png', 'Report', onTap: (text) {
                      showReportDialog(context);
                    }, needCheckInstalled: false)
                  : Container(),
              widget.status != null && int.parse(widget.status!.userId!) == store!.state.bean?.id // 自己发布的动态显示 删除按钮
                  ? _shareItem(context, 'assets/images/icon_delete.png', 'Delete', onTap: (text) {
                      showDeleteDialog(
                        context,
                        () {
                          Navigator.pop(context);
                        },
                      );
                    }, needCheckInstalled: false)
                  : Container(),
            ],
          ),
        ),
    ];
  }
  void shareItems5Gd6Moyelive(Store<YBDAppState>? store) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///2.0需求 站内勋章分享 不需要显示第三方分享的图标
  getThirdShareList() {
    if (widget.badgeUrl != null) return [];

    final Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    Map? shareSwitch;
    try {
      shareSwitch = json.decode(store.state.configs!.shareSwitch ?? "{}");
      print("ssdex$shareSwitch");
    } catch (e) {
      print(e);
    }
    bool configHasValue = shareSwitch?.isNotEmpty ?? false;
    List<Widget> ls = [];
    ls
      ..add(!configHasValue || (configHasValue && shareSwitch!['whatsapp'] == 1) ? _whatsappItem() : SizedBox())
      ..add(!configHasValue || (configHasValue && shareSwitch!['facebook'] == 1) ? _facebookItem() : SizedBox())
      ..add(!configHasValue || (configHasValue && shareSwitch!['instagram'] == 1) ? _instagramItem() : SizedBox())
      ..add(!configHasValue || (configHasValue && shareSwitch!['messenger'] == 1) ? _messengerItem() : SizedBox())
      ..add(!configHasValue || (configHasValue && shareSwitch!['snapchat'] == 1) ? _snapchatItem() : SizedBox())
      ..add(!configHasValue || (configHasValue && shareSwitch!['twitter'] == 1) ? _twitterItem() : SizedBox())
      ..add(!configHasValue || (configHasValue && shareSwitch!['skype'] == 1) ? _skypeItem() : SizedBox());
    // ..add(_shareItem(context, 'assets/images/icon_more.png', translate('more_hc'), needCheckInstalled: false));
    return ls;
  }

  /// 获取社交软件的安装信息
  void _checkAppsForShare() {
    // if (Platform.isIOS) {
    // 安卓全部显示（需求如此）
    SocialShare.checkInstalledAppsForShare().then((value) {
      logger.v('===apps for share : $value');

      if (mounted) {
        setState(() {
          _appsForShare = value;
        });
      }
    });
    // }
  }
  void _checkAppsForShareD0B7Zoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// whatsapp 分享
  Widget _whatsappItem() {
    var item = _shareItem(context, 'assets/images/icon_whatsapp_green.png', WHATSAPP);
    if (Platform.isIOS) {
      if (_appsForShare![WHATSAPP.toLowerCase()] ?? false) {
        return item;
      } else {
        return SizedBox();
      }
    } else {
      return item;
    }
  }

  /// facebook 分享
  Widget _facebookItem() {
    var item = _shareItem(context, 'assets/images/login/number_fb_icon.png', FACEBOOK);
    if (Platform.isIOS) {
      if (_appsForShare![FACEBOOK.toLowerCase()] ?? false) {
        return item;
      } else {
        return SizedBox();
      }
    } else {
      return item;
    }
  }
  void _facebookItemtaBQnoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// instagram 分享
  Widget _instagramItem() {
    var item = _shareItem(context, 'assets/images/login/number_instagram_icon.webp', INSTAGRAM);
    if (Platform.isIOS) {
      // iOS 暂时不支持 instagram 分享
      return SizedBox();
      // TODO: 测试代码
      // if (_appsForShare[INSTAGRAM.toLowerCase()] ?? false) {
      //   return item;
      // } else {
      //   return SizedBox();
      // }
    } else {
      return item;
    }
  }

  /// twitter 分享
  Widget _twitterItem() {
    var item = _shareItem(context, 'assets/images/setting/number_twitter_icon.png', TWITTER);
    if (Platform.isIOS) {
      if (_appsForShare![TWITTER.toLowerCase()] ?? false) {
        return item;
      } else {
        return SizedBox();
      }
    } else {
      return item;
    }
  }
  void _twitterItemh32sdoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// messenger 分享
  Widget _messengerItem() {
    var item = _shareItem(context, 'assets/images/icon_messenger.png', MESSENGER);
    if (Platform.isIOS) {
      // ios 不支持 messenger 分享
      return SizedBox();
    } else {
      return item;
    }
  }

  /// snapchat 分享
  Widget _snapchatItem() {
    var item = _shareItem(context, 'assets/images/icon_snapchat.png', SNAPCHAT);
    if (Platform.isIOS) {
      // ios 不支持 snapchat 分享
      return SizedBox();
    } else {
      return item;
    }
  }

  /// skype 分享
  Widget _skypeItem() {
    var item = _shareItem(context, 'assets/images/icon_skype.png', SKYPE);
    if (Platform.isIOS) {
      // ios 不支持 skype 分享
      return SizedBox();
    } else {
      return item;
    }
  }
  void _skypeItemkRRdRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  _shareItem(BuildContext context, String icon, String spf, {Function? onTap, bool needCheckInstalled: true}) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Colors.black12,
        highlightColor: Colors.black26,
        onTap: () async {
          if (widget.advertise.adurl != null && spf != translate('friends')) {
            print('22.9.28--target:$spf--id:${widget.advertise.adurl} name:${widget.advertise.adurl!.key}');
            YBDActivityInTrack().shareAct(url: widget.advertise.adurl, target: spf);
          }
          // FlutterShareMe插件只实现了Android端
          if (Platform.isAndroid) {
            // 检查是否安装app
            if (needCheckInstalled && !(await FlutterShareMe().isAppInstalled(app: spf))) {
              YBDToastUtil.toast('$spf not installed yet!');
              return;
            }
          }
          // 点击就上报用户分享行为
          reportAudioRoomShare(spf);

          if (onTap != null) {
            onTap.call(spf);
          } else {
            bool result = await YBDShareUtil.share(
              context,
              spf,
              status: widget.status,
              advertise: widget.advertise,
              room: widget.room,
            );

            if (result && widget.status != null) {
              // 分享动态成功，调用分享动态接口
              bool shared = await YBDStatusApiHelper.shareStatus(context, widget.status!.id);

              if (shared) {
                int count = widget.status!.shareCount ?? 0;
                ++count;
                eventBus.fire(YBDStatusChangeEvent(widget.status!.id, shareCount: count));
                widget.status!.shareCount = count;
              }
            }
          }
        },
        child: Container(
          width: ScreenUtil().setWidth(160),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                icon,
                width: ScreenUtil().setWidth(60),
              ),
              Padding(
                padding: EdgeInsets.only(top: ScreenUtil().setWidth(20)),
                child: Text(spf, style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Color(0xff5e5e5e))),
              )
            ],
          ),
        ),
      ),
    );
  }

  /// 弹出好友列表
  showShareToFriendsDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext builder) {
        ShareCallbackFunc shareCallback = (selectedFriends) async {
          FriendShareType type;
          dynamic data;

          if ((widget.advertise.adurl ?? '').isNotEmpty) {
            // 分享活动
            type = FriendShareType.ShareAd;
            data = {
              'adUrl': widget.advertise.adurl,
              'adName': widget.advertise.adname,
              'adImg': widget.advertise.img,
              'adDetail': widget.advertise.detail,
            };
            YBDActivityInTrack().shareAct(url: widget.advertise.adurl, target: 'friends');
          } else if (widget.room != null) {
            // 分享房间
            type = FriendShareType.ShareRoom;
            data = widget.room;
          } else if (widget.badgeUrl != null) {
            type = FriendShareType.ShareBadge;
            data = {"image": widget.badgeUrl, "title": widget.title};
          } else if (widget.gameShareData != null) {
            /// 分享游戏房
            type = FriendShareType.ShareGameRoom;
            data = widget.gameShareData;
          } else if (widget.status != null) {
            // 分享动态
            type = FriendShareType.ShareStatus;
            data = widget.status;
          } else {
            YBDToastUtil.toast('unknown_error');
            return;
          }

          // 分享给好友
          bool isShared = await YBDFriendShareUtil.shareToFriends(context, type, data, selectedFriends);
          if (isShared) {
            if (type == FriendShareType.ShareGameRoom) {
              YBDToastUtil.toast(translate("share_success"));
            } else {
              YBDToastUtil.toast(translate("sharing_succeeded"));
            }
            if (Navigator.canPop(context)) Navigator.pop(context);
          }
          if ((widget.advertise.adurl ?? '').isEmpty && widget.status != null && isShared) {
            bool shared = await YBDStatusApiHelper.shareStatus(context, widget.status!.id);
            if (shared) {
              int count = widget.status!.shareCount ?? 0;
              ++count;
              eventBus.fire(YBDStatusChangeEvent(widget.status!.id, shareCount: count));
              widget.status!.shareCount = count;
            }
          }
        };
        if (widget.type != null) return YBDGameFriendListDialog(shareCallback: shareCallback, type: widget.type);
        return YBDFriendListDialog(shareCallback);
      },
    );
  }

  showReportDialog(BuildContext context) {
    var reportDialog = SimpleDialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
      title: Container(height: ScreenUtil().setWidth(99), child: Center(child: new Text(translate('report_for')))),
      contentPadding: EdgeInsets.all(0),
      titlePadding: EdgeInsets.all(0),
      children: <Widget>[
        _line(),
        _reportItem(context, translate('pornography')),
        _line(),
        _reportItem(context, translate('spam')),
        _line(),
        _reportItem(context, translate('illegal')),
        _line(),
        _reportItem(context, translate('unhealthy')),
        _line(),
        _reportItem(context, translate('other')),
      ],
    );

    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return reportDialog;
      },
    );
  }

  _line() {
    return Container(
      height: ScreenUtil().setWidth(1),
      color: Color(0xffDEDCDC),
    );
  }

  _reportItem(BuildContext context, String content) {
    return Container(
      height: ScreenUtil().setWidth(80),
      child: SimpleDialogOption(
          child: Center(
            child: Text(
              content,
              style: TextStyle(color: Color(0xff5F97DA), fontSize: ScreenUtil().setSp(28)),
            ),
          ),
          onPressed: () {
            YBDStatusApiHelper.reportStatus(context, widget.status!.id, content);

            Navigator.pop(context);
          }),
    );
  }

  showDeleteDialog(BuildContext context, Function onDelete) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text(
        translate('cancel'),
        style: TextStyle(color: Color(0xff5E94E7)),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text(
        translate('ok'),
        style: TextStyle(color: Color(0xff5E94E7)),
      ),
      onPressed: () async {
        Navigator.pop(context);
        bool isSuccess = await YBDStatusApiHelper.deleteStatus(context, widget.status!.id);
        if (isSuccess) {
          eventBus.fire(YBDStatusDeleteEvent(widget.status!.id));
          onDelete.call();
        }
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text(translate('status_delete')),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  /// 上报房间分享行为
  reportAudioRoomShare(String type) async {
    if (widget.room == null) return;

    String? userId = await YBDSPUtil.getUserId();
    logger.v("reportAudioRoomShare: userId: $userId, roomId: ${widget.room!.id}, type: $type");
    YBDTATrack().trackEvent('share', prop: YBDTAProps(name: 'liveroom', id: widget.room!.id?.toString(), target: type));
    ApiHelper.reportAudioRoomShare(
      context,
      roomId: "${widget.room!.id}",
      userId: userId,
      type: type,
    );
  }
}
