
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import '../../common/util/common_ybd_util.dart';
import '../../common/util/log_ybd_util.dart';
import '../../module/user/entity/user_ybd_info_entity.dart';
import '../page/profile/my_profile/badge_ybd_page.dart';
import '../page/profile/my_profile/wear_ybd_badge_item.dart';
import 'room_ybd_level_tag.dart';

import 'level_ybd_tag.dart';

/// 用户 id，级别，房间级别，年龄，等信息栏
class YBDProfileTagInfo extends StatelessWidget {
  final YBDUserInfo userInfo;

  /// 是否为签约主播
  final bool? isOfficialTalent;

  bool isCerRequested;

  YBDUserCertificationRecordCertificationInfos? cerInfos;
  YBDProfileTagInfo(this.userInfo, this.isOfficialTalent, this.isCerRequested, this.cerInfos);

  @override
  Widget build(BuildContext context) {
    var femaleColor = [Color(0xffE37BBC), Color(0xffFF877D)];
    var maleColor = [Color(0xff5E94E7), Color(0xff47CDCC)];
    List<Color> sexColors = userInfo.sex == 2 ? maleColor : femaleColor;
    String age = calculateAge(userInfo.birthday);
    String sexImg = userInfo.sex == 2 ? 'assets/images/male.png' : 'assets/images/female.png';

    var id = [Color(0xff3792C0), Color(0xff78DFBD)];
    var level = [Color(0xff995DFF), Color(0xffABDEFF)];

    // 暂时不做房间信息
//    var roomLevel = [Color(0xff6A60E9), Color(0xffCB2CFC)];

    List<Color> constellationColors = [Color(0xffFFD972), Color(0xffFF6161)];
    String constellation = YBDCommonUtil.getZodicaSign(DateTime.fromMillisecondsSinceEpoch(userInfo.birthday ?? 0));
    List<Widget> rows = [];
    if (cerInfos == null || cerInfos?.hidUserLevel == 0)
      rows.add(YBDLevelTag(
        userInfo.level ?? 1,
        horizontalPadding: 10,
      ));
    if (cerInfos == null || cerInfos?.hidTalentLevel == 0)
      rows.add(Padding(
        padding: EdgeInsets.only(right: ScreenUtil().setWidth(3)),
        child: YBDRoomLevelTag(
          userInfo.roomlevel ?? 1,
          addPadding: 6,
        ),
      ));
    if (cerInfos == null || cerInfos?.hidGender == 0) rows.add(getTag(sexColors, age, image: sexImg));
    if (cerInfos == null || cerInfos?.hidVip == 0)
      rows.add(Padding(
        padding: EdgeInsets.only(right: ScreenUtil().setWidth(2)),
        child: vipTag(),
      ));
    if (cerInfos == null || cerInfos?.hidBadge == 0) rows.add(YBDWearBadgeItem(userInfo.badges));
    if (cerInfos == null || cerInfos?.hidTalent == 0) rows.add(talentTag());

    if (!isCerRequested || rows.isEmpty) return SizedBox();
    return Container(
      width: ScreenUtil().setWidth(520),
      margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
      padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
      child: Wrap(
        alignment: WrapAlignment.start,
        crossAxisAlignment: WrapCrossAlignment.center,
        runSpacing: ScreenUtil().setWidth(15),
        children: rows,
      ),
    );
  }
  void buildmwBcsoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget vipTag() {
    return Container(
      width: ScreenUtil().setWidth(0),
    );
  }
  void vipTagklzrNoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  Widget talentTag() {
    if (isOfficialTalent == true) {
      logger.v('show talent tag');
      return Padding(
        padding: EdgeInsets.only(left: ScreenUtil().setWidth(6)),
        child: Image.asset(
          "assets/images/icon_talent.webp",
          width: ScreenUtil().setWidth(30),
        ),
      );
    }

    logger.v('talent tag is empty');
    return Container(
      width: ScreenUtil().setWidth(0),
    );
  }
  void talentTagWFObXoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  getTag(List<Color> colors, String text, {String? image}) {
    logger.v('get tag : $text');
    return Container(
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10), vertical: ScreenUtil().setWidth(3)),
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(0)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(18))),
        gradient: LinearGradient(colors: colors, begin: Alignment.centerLeft, end: Alignment.centerRight),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          image != null
              ? Image.asset(
                  image,
                  height: ScreenUtil().setWidth(15),
                )
              : Container(),
          image != null
              ? SizedBox(
                  width: ScreenUtil().setWidth(2),
                )
              : Container(),
          Text(
            text,
            style: TextStyle(fontSize: ScreenUtil().setSp(16), height: 1.2, color: Colors.white),
          ),
        ],
      ),
    );
  }

  String calculateAge(int? timestamp) {
    if (null == timestamp) {
      logger.v('time stamp is empty');
      return '0';
    }

    DateTime birthDate = DateTime.fromMillisecondsSinceEpoch(timestamp);
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age.toString();
  }
  void calculateAge6IS1noyelive(int? timestamp) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
