
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';

class YBDSvgaIcon extends StatelessWidget {
  final String? imgUrl;

  /// 白色图标
  final bool white;

  /// 白色预览图标变灰
  final bool grey;

  YBDSvgaIcon({this.imgUrl = '', Key? key, this.white = false, this.grey = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _svgaIcon(imgUrl);
  }
  void buildhWZ9ooyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 动图标记
  Widget _svgaIcon(String? imgUrl) {
    if (YBDCommonUtil.isSvgaFile(imgUrl)) {
      return Container(
        width: ScreenUtil().setWidth(26),
        height: ScreenUtil().setWidth(26),
        child: white
            ? ColorFiltered(
                colorFilter: YBDTPGlobal.getGreyFilter(grey),
                child: Image.asset('assets/images/icon_svga_white.webp'),
              )
            : Image.asset('assets/images/play_svga_icon.webp'),
      );
    }

    return SizedBox();
  }
  void _svgaIconpWPaIoyelive(String? imgUrl) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
