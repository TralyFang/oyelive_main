
/*
 * @Author: William
 * @Date: 2022-05-10 11:12:59
 * @LastEditTime: 2022-05-18 14:21:34
 * @LastEditors: William
 * @Description: profile swiper
 * 
 * @FilePath: /oyetalk-flutter/lib/ui/widget/profile_bg_swiper.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

import '../../common/util/log_ybd_util.dart';

class YBDBGSwiper extends StatelessWidget {
  final List<String>? data;
  final bool haveDot;

  ///区分他人和自己的profile 默认背景图不一样
  final bool myProfile;
  const YBDBGSwiper({Key? key, this.data, this.haveDot = false, this.myProfile = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (data == null || data!.isEmpty) {
      return YBDTPGlobal.bgImg(isMe: this.myProfile);
    }
    return Swiper(
      key: ValueKey('${data!.length}'),
      itemCount: data!.length,
      autoplay: data!.length > 1,
      autoplayDelay: 5000,
      outer: false,
      pagination: haveDot && data!.length > 1
          ? SwiperPagination(
              alignment: Alignment(0.9, 0.85),
              margin: EdgeInsets.all(10.px as double),
              builder: DotSwiperPaginationBuilder(
                size: 14.px as double,
                activeSize: 15.px as double,
                color: Colors.white.withOpacity(0.4),
                activeColor: Color(0xff7dfaff),
              ))
          : null,
      itemBuilder: (BuildContext context, int index) {
        return YBDNetworkImage(
          imageUrl: data![index],
          fit: BoxFit.cover,
          placeholder: (context, url) => YBDTPGlobal.bgImg(isMe: this.myProfile),
        );
      },
      onIndexChanged: (int index) {
        logger.v('22.5.10------index:${index + 1}/${data!.length}');
      },
    );
  }
  void buildwCRveoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
