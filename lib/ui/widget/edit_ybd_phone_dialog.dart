
/*
 * @Date: 2021-07-08 17:14:24
 * @LastEditors: William
 * @LastEditTime: 2022-08-30 16:44:15
 * @FilePath: /oyetalk-flutter/lib/ui/widget/edit_phone_dialog.dart
 */
import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/event/safe_ybd_account_bus_event.dart';
import 'package:oyelive_main/common/http/http_ybd_util.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import 'package:oyelive_main/common/util/connectivity_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/module/api.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/result_ybd_bean.dart';
import 'package:oyelive_main/module/user/entity/mobile_ybd_check_resp_entity.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/login/bloc/phonenumber_ybd_input_bloc.dart';
import 'package:oyelive_main/ui/page/status/local_audio/scale_ybd_animate_button.dart';
import 'package:oyelive_main/ui/widget/center_ybd_toast.dart';

import '../page/login/widget/dialog_ybd_graphic_code.dart';
import 'intl_input/intl_ybd_phone_field.dart';

class YBDEditPhoneDialog extends StatefulWidget {
  const YBDEditPhoneDialog({Key? key}) : super(key: key);

  @override
  _YBDEditPhoneDialogState createState() => _YBDEditPhoneDialogState();
}

final countDownSecs = 60;

class _YBDEditPhoneDialogState extends BaseState<YBDEditPhoneDialog> {
  /// 初始国家
  String? _initialCountryCode;

  /// 数据框控制器，用来获取输入内容
  TextEditingController _phoneNumerController = TextEditingController();
  TextEditingController _authCodeController = TextEditingController();

  /// 包含国家码的电话号码
  String? _phoneNumberComplete;

  /// 焦点
  FocusNode _phoneFocusNode = new FocusNode();
  FocusNode _authFocusNode = new FocusNode();

  Timer? _authCodeTimer;
  int _seconds = countDownSecs;
  bool _resend = false;
  bool _phoneReadOnly = false;
  int _sendTimes = 0;
  bool _haveSended = false;
  bool _okBtnAvailable = false;
  int? _tokenId;
  @override
  void initState() {
    super.initState();
    YBDCommonUtil.getDefaultCountryCode(context).then((value) {
      if (mounted) {
        setState(() {
          _initialCountryCode = value;
        });
      }
      return YBDSPUtil.get(Const.SP_LOGIN_PHONE_CODE);
    }).then((value) {
      if (value != null) {
        _initialCountryCode = value;
        setState(() {});
      }
      return YBDSPUtil.get(Const.SP_LOGIN_PHONE);
    });
    _authCodeController.addListener(() {
      if (_authCodeController.text.length < 6) {
        _okBtnAvailable = false;
        setState(() {});
      } else {
        if (!_okBtnAvailable) {
          //iphone 上无法删除问题
          _authFocusNode.unfocus();
        }
        _okBtnAvailable = true;
        setState(() {});
      }
    });
  }
  void initStateNwB25oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget myBuild(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the edit phone dialog bg");
        _authFocusNode.unfocus();
      },
      child: Material(
        type: MaterialType.transparency,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
                width: ScreenUtil().setWidth(600),
                height: ScreenUtil().setWidth(500),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(30)))),
                  color: Colors.white,
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: ScreenUtil().setWidth(10)),
                    //* 关闭弹窗
                    _closeBtn(),
                    Center(
                      child: Text(
                        translate('phone_number'),
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(28),
                          color: Colors.black,
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(50)),
                    //* 手机号输入
                    BlocBuilder<YBDPhoneNumberInputBloc, InputStep>(builder: (_, state) {
                      return Container(
                        width: ScreenUtil().setWidth(550),
                        height: ScreenUtil().setWidth(80),
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(8)))),
                          color: Color(0xffEDEDED),
                        ),
                        child: Row(
                          children: [
                            Expanded(child: inputPhoneNumber(_phoneReadOnly)),
                          ],
                        ),
                      );
                    }),
                    SizedBox(height: ScreenUtil().setHeight(30)),
                    //* 验证码输入框
                    BlocBuilder<YBDPhoneNumberInputBloc, InputStep>(builder: (_, state) {
                      return Container(
                        width: ScreenUtil().setWidth(550),
                        height: ScreenUtil().setWidth(80),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular((ScreenUtil().setWidth(8)))),
                          color: Color(0xffEDEDED),
                        ),
                        // padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
                        child: inputAuthCode(),
                      );
                    }),
                    SizedBox(height: ScreenUtil().setHeight(33)),
                    //* 底部OK按钮
                    BlocBuilder<YBDPhoneNumberInputBloc, InputStep>(builder: (_, state) {
                      return _bottomBtn();
                    })
                  ],
                )),
          ],
        ),
      ),
    );
  }
  void myBuildt1Pizoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 输入框字体
  TextStyle _inputTextStyle() {
    return TextStyle(
      color: Color(0xff9A9A9A),
      fontSize: ScreenUtil().setSp(30),
      fontWeight: FontWeight.w500,
    );
  }

  /// 输入框占位字符字体
  TextStyle _inputTextHintStyle() {
    return TextStyle(
      color: Color(0xff9A9A9A),
      fontSize: ScreenUtil().setSp(24),
      fontWeight: FontWeight.w400,
    );
  }

  /// 手机号输入框
  /// readOnly: 发送验证码成功后显示为只读状态
  inputPhoneNumber(bool readOnly) {
    return YBDIntlPhoneField(
      editPhone: true,
      style: _inputTextStyle(),
      decoration: InputDecoration(
          hintText: translate('edit_phone_hint'),
          hintStyle: _inputTextHintStyle(),
          border: InputBorder.none,
          floatingLabelBehavior: FloatingLabelBehavior.never),
      initialCountryCode: _initialCountryCode,
      autoValidate: false,
      readOnly: readOnly,
      focusNode: _phoneFocusNode,
      maxLength: 20,
      keyboardType: TextInputType.phone,
      controller: _phoneNumerController,
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      onChanged: (phone) {
        _initialCountryCode = phone.countryCode;
        _phoneNumberComplete = phone.completeNumber.trim();
        logger.v('input phone number : $_phoneNumberComplete');
      },
      onCountryChange: (selectedCountryName, selectedCountryCode, phone) {
        _initialCountryCode = selectedCountryCode;
        _phoneNumberComplete = phone.completeNumber.trim();
        logger.v('on change country code : $_phoneNumberComplete');
      },
    );
  }

  /// 验证码输入框
  inputAuthCode() {
    return Row(
      children: [
        SizedBox(width: ScreenUtil().setWidth(8)),
        Expanded(
          child: TextField(
              controller: _authCodeController,
              style: _inputTextStyle(),
              inputFormatters: [LengthLimitingTextInputFormatter(6), FilteringTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
              focusNode: _authFocusNode,
              decoration: InputDecoration(
                hintStyle: _inputTextHintStyle(),
                hintText: translate('edit_phone_verification_hint'),
                border: InputBorder.none,
              ),
              cursorColor: Color(0xff7DFAFF)),
        ),
        authCodeBtn(),
      ],
    );
  }

  /// 验证码按钮
  authCodeBtn() {
    bool isAvailable = _seconds == countDownSecs;
    return YBDScaleAnimateButton(
      onTap: () {
        if (isAvailable) {
          sendAuthCode();
        }
      },
      child: Container(
        width: ScreenUtil().setWidth(200),
        height: ScreenUtil().setWidth(80),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
          gradient: LinearGradient(
            colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Text(
          isAvailable
              ? _resend
                  ? translate('edit_phone_resend')
                  : translate('verification_code')
              : '${_seconds}s',
          style: TextStyle(
            fontSize: ScreenUtil().setSp(_haveSended ? 28 : 24),
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  /// 发送验证码按钮的样式
  sendAuthCodeDecoration(bool isAvailable) {
    return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(8))),
        gradient: isAvailable
            ? LinearGradient(colors: [
                Color(0xff47CDCC),
                Color(0xff5E94E7),
              ], begin: Alignment.topCenter, end: Alignment.bottomCenter)
            : null,
        color: isAvailable ? null : Color(0xffCECECE).withOpacity(0.3));
  }

  /// 发送验证码
  Future sendAuthCode() async {
    logger.v('input phone number : _phoneNumberComplete $_phoneNumberComplete');
    if (_phoneNumerController.text.isEmpty) {
      YBDToastUtil.toast(translate('edit_phone_hint'));
      return;
    }
    showLockDialog();
    YBDMobileCheckResp? data = await ApiHelper.getSMSCode(
      context,
      phone: _phoneNumberComplete,
      operation: YBDOperation.LinkPhone,
    );
    if (data != null) {
      /// 如果已经注册过的手机号
      if (data.returnCode == "902068") {
        YBDToastUtil.toast(translate('edit_phone_already_bound'));
        return false;
      } else if (data.returnCode == Const.HTTP_SUCCESS) {
        _tokenId = data.record?.tokenId;
        if (_tokenId == null) {
          YBDToastUtil.toast(data.returnMsg);
        }
        logger.v("tokenId : $_tokenId");
        _phoneReadOnly = true;
        _phoneFocusNode.unfocus();
        _authFocusNode.requestFocus();
        startTimer();
        _sendTimes++;
        logger.v('Edit Phone send Verification Code $_sendTimes times');
      } else {
        YBDToastUtil.toast(data.returnMsg);
      }
    }
    dismissLockDialog();
  }
  void sendAuthCodebla5soyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 开始倒计时
  startTimer() {
    _authCodeTimer?.cancel();
    _authCodeTimer = Timer.periodic(Duration(seconds: 1), (_) {
      if (_seconds > 0) {
        _seconds--;
        _haveSended = true;
      } else {
        _seconds = countDownSecs;
        _resend = true;
        _phoneReadOnly = false;
        _.cancel();
      }
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  /// 底部按钮
  Widget _bottomBtn() {
    return YBDScaleAnimateButton(
      onTap: _okBtnAvailable
          ? () async {
              logger.v('clicked ok btn');
              FocusScope.of(context).requestFocus(FocusNode());

              if (YBDConnectivityUtil.getInstance()!.rs == ConnectivityResult.none) {
                YBDToastUtil.toast('You are facing the internet issue, please check.');
                return;
              }
              if (_phoneNumerController.text.isEmpty) {
                YBDToastUtil.toast(translate('edit_phone_hint'));
                return;
              }
              if (_authCodeController.text.isEmpty) {
                YBDToastUtil.toast(translate('enter_code'));
                return;
              }
              if (_sendTimes == 0) {
                YBDToastUtil.toast(translate('get_code'));
                return;
              }
              showLockDialog();
              // String token = await YBDUserUtil.userToken();
              YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(
                context,
                YBDApi.BIND_PHONE,
                params: {
                  'tokenId': _tokenId,
                  'token': _authCodeController.text,
                  'phone': _phoneNumberComplete,
                },
              );
              dismissLockDialog();
              if (resultBean?.returnCode == '900005') {
                showDialog<Null>(
                    context: context,
                    barrierDismissible: false,
                    barrierColor: Colors.transparent,
                    builder: (BuildContext x) {
                      return YBDCenterToast(toastText: translate('edit_phone_already_bound'));
                    });
              } else if (resultBean?.returnCode == '902067') {
                showDialog<Null>(
                    context: context,
                    barrierDismissible: false,
                    barrierColor: Colors.transparent,
                    builder: (BuildContext x) {
                      return YBDCenterToast(toastText: translate('edit_phone_code_error'));
                    });
              } else if (resultBean?.returnCode == Const.HTTP_SUCCESS) {
                YBDToastUtil.toast('auth code verify success');
                logger.v('auth code verify success');
                YBDSPUtil.save(Const.SP_LOGIN_PHONE, _phoneNumerController.text);
                YBDSPUtil.save(Const.SP_LOGIN_PHONE_CODE, _initialCountryCode);
                // YBDUserInfo _userInfo = await YBDSPUtil.getUserInfo();
                // _userInfo.cellphone = _phoneNumberComplete;
                // // 更新 SP
                // YBDSPUtil.save(Const.SP_USER_INFO, _userInfo);
                // // 更新 redux
                // Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context);
                // store.dispatch(YBDUpdateUserAction(_userInfo));
                ApiHelper.checkLogin(context);
                eventBus.fire(YBDSafeAccountBusEvent());
                // true 绑定手机号成功
                Navigator.pop(context, _phoneNumberComplete);
              } else {
                YBDToastUtil.toast(resultBean?.returnMsg);
                logger.v('auth code verify failed');
              }
            }
          : null,
      child: Container(
        decoration: _okBtnAvailable
            ? BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(33),
                ),
                gradient: LinearGradient(
                  colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              )
            : BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(33),
                ),
                color: Colors.grey),
        width: ScreenUtil().setWidth(300),
        height: ScreenUtil().setWidth(70),
        alignment: Alignment.center,
        child: Text(
          translate('ok'),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(28),
            color: Colors.white,
          ),
        ),
      ),
    );
  }
  void _bottomBtnYiY2aoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关闭按钮
  Widget _closeBtn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        GestureDetector(
          onTap: () {
            logger.v('clicked close btn');
            Navigator.pop(context);
          },
          child: Container(
            width: ScreenUtil().setWidth(48),
            // height: ScreenUtil().setWidth(58),
            child: Image.asset(
              'assets/images/dc/daily_check_close_btn.png',
              color: Color(0xff626262),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(width: ScreenUtil().setWidth(10)),
      ],
    );
  }
  void _closeBtn6r20Coyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    _authCodeTimer?.cancel();
    super.dispose();
  }
}
