
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../common/util/log_ybd_util.dart';

/// 导航栏的返回按钮
class YBDNavBackButton extends StatelessWidget {
  final Color color;

  YBDNavBackButton({this.color: Colors.white});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(88),
      height: ScreenUtil().setWidth(88),
      child: TextButton(
        onPressed: () {
          logger.v('clicked back button');
          Navigator.pop(context);
        },
        child: Icon(Icons.arrow_back, color: color),
      ),
    );
  }
  void buildj7op3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
