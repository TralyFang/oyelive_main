
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';

import 'nav_ybd_back_button.dart';

class YBDCustomNavBar extends StatelessWidget with PreferredSizeWidget {
  final String? title;
  const YBDCustomNavBar({this.title, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(
        title ?? '',
        style: YBDTPStyle.navTitle,
      ),
      backgroundColor: YBDTPStyle.heliotrope,
      leading: YBDNavBackButton(),
      elevation: 0.0,
    );
  }
  void build4YrQjoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
