
import 'package:flutter/material.dart';
import 'package:oyelive_main/common/style/tp_ybd_style.dart';

/// 工程里默认花里胡哨的背景色
class YBDBgContainer extends Container {
  final List<Color> colors;
  final Widget? child;
  final EdgeInsetsGeometry? padding;

  YBDBgContainer({
    this.child,
    this.colors = YBDTPStyle.colorList,
    this.padding,
  }) : super(
          padding: padding,
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: colors,
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: child,
        );
}
