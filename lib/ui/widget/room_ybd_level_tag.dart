
import 'package:flutter/material.dart';
import '../../common/util/screen_ybd_expand_util.dart';

class YBDRoomLevelTag extends StatelessWidget {
  /// 是否加水平边距，默认要加
  final bool addHorizontalMargin;

  final int? roomLevel;
  double scale;
  int? addPadding;
  YBDRoomLevelTag(this.roomLevel, {this.addHorizontalMargin: true, this.scale = 1, this.addPadding});

  @override
  Widget build(BuildContext context) {
    Color bgC;
    String imagePath;

    if (null == roomLevel) {
      bgC = Color(0xff3196eb);
      imagePath = "assets/images/level1_4.png";
    } else if (roomLevel! <= 10) {
      bgC = Color(0xff3196eb);
      imagePath = "assets/images/level1_4.png";
    } else if (roomLevel! >= 11 && roomLevel! <= 20) {
      bgC = Color(0xffff9d45);
      imagePath = "assets/images/level5_7.png";
    } else if (roomLevel! >= 21 && roomLevel! <= 30) {
      bgC = Color(0xffF86B2D);
      imagePath = "assets/images/level8_9.png";
    } else if (roomLevel! >= 31 && roomLevel! <= 40) {
      bgC = Color(0xffff3c65);
      imagePath = "assets/images/level10_11.png";
    } else if (roomLevel! >= 41 && roomLevel! <= 60) {
      bgC = Color(0xff6d15b7);
      imagePath = "assets/images/level12_13.png";
    } else if (roomLevel! >= 61 && roomLevel! <= 80) {
      bgC = Color(0xffe238b4);
      imagePath = "assets/images/level14_15.webp";
    } else {
      bgC = Color(0xffe238b4);
      imagePath = "assets/images/level14_15.webp";
    }

    TextStyle textStyle = TextStyle(
      fontSize: YBDScreenExpandUtil.getInstance().setScaleSp(16, forceScale: scale),
      height: 1.2,
      color: Colors.white,
    );

    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: addHorizontalMargin
              ? YBDScreenExpandUtil.getInstance().setScaleWidth(addPadding ?? 10, forceScale: scale)
              : 0),
      height: YBDScreenExpandUtil.getInstance().setScaleWidth(25, forceScale: scale),
      padding: EdgeInsets.symmetric(
        horizontal: YBDScreenExpandUtil.getInstance().setScaleWidth(10, forceScale: scale),
        vertical: YBDScreenExpandUtil.getInstance().setScaleWidth(3, forceScale: scale),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(14)),
        color: bgC,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(imagePath),
          SizedBox(width: YBDScreenExpandUtil.getInstance().setScaleWidth(3, forceScale: scale)),
          Text(
            "RLv",
            style: textStyle,
          ),
          SizedBox(width: YBDScreenExpandUtil.getInstance().setScaleWidth(1, forceScale: scale)),
          Text("${roomLevel ?? '1'}", style: textStyle),
        ],
      ),
    );
  }
  void buildXGdMvoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
