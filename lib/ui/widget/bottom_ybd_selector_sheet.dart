
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';

import 'colored_ybd_safe_area.dart';

class YBDBottomSelectorSheet extends StatefulWidget {
  List<String>? opts;
  Function? onSelectAt;
  bool fitWidth;

  YBDBottomSelectorSheet({this.opts, this.onSelectAt(int index)?, this.fitWidth = false});

  @override
  State<YBDBottomSelectorSheet> createState() => _YBDBottomSelectorSheetState();
}

class _YBDBottomSelectorSheetState extends State<YBDBottomSelectorSheet> {
  // 是否点击item
  bool _click = false;
  // 当前点击item的下标
  int _curIdx = 0;
  BorderRadius getBordorRadius(int index) {
    Radius _radius = Radius.circular(ScreenUtil().setWidth(16));
    if (widget.opts!.length == 1) return BorderRadius.all(_radius);
    if (index == 0) return BorderRadius.vertical(top: _radius);
    if (index == widget.opts!.length - 1) return BorderRadius.vertical(bottom: _radius);
    return BorderRadius.all(Radius.circular(0));
  }

  @override
  Widget build(BuildContext context) {
    double width = widget.fitWidth ? ScreenUtil().screenWidth : ScreenUtil().setWidth(680);
    return YBDColoredSafeArea(
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: width,
              child: ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (_, index) => GestureDetector(
                        onTapDown: (x) {
                          setState(() {
                            _click = true;
                            _curIdx = index;
                          });
                        },
                        onTapCancel: () {
                          setState(() {
                            _click = false;
                          });
                        },
                        onTapUp: (x) {
                          setState(() {
                            _click = false;
                          });
                          Navigator.pop(context);
                          widget.onSelectAt!(index);
                        },
                        child: Container(
                          height: ScreenUtil().setWidth(100),
                          width: width,
                          decoration: BoxDecoration(
                            borderRadius: getBordorRadius(index),
                            color: _click && _curIdx == index ? Color(0xffF3F3F3) : Colors.white,
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            widget.opts![index],
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(28),
                              color: _click && _curIdx == index ? Color(0xff5F97DA) : Color(0xff2D2D2D),
                            ),
                          ),
                        ),
                      ),
                  separatorBuilder: (_, index) => Container(
                        height: ScreenUtil().setWidth(1),
                        color: Color(0xffDEDCDC),
                      ),
                  itemCount: widget.opts!.length),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                height: ScreenUtil().setWidth(100),
                width: width,
                margin: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(14)),
                decoration: BoxDecoration(
                    color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
                alignment: Alignment.center,
                child: Text(
                  translate('cancel'),
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(28), color: widget.fitWidth ? Color(0xff959595) : Color(0xff5F97DA)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  void buildTqZPgoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
