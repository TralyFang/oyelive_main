
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 其他人的 profile 页面动态加载框
class YBDStatusLoadingCircle extends StatefulWidget {
  // 超时时间, 默认 60 秒
  final int duration;
  final VoidCallback timeoutCallback;

  YBDStatusLoadingCircle(this.duration, this.timeoutCallback);

  @override
  YBDStatusLoadingCircleState createState() => new YBDStatusLoadingCircleState();
}

class YBDStatusLoadingCircleState extends State<YBDStatusLoadingCircle> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: widget.duration), () {
      // 在 widget 树中则调用回调
      if (mounted && null != widget.timeoutCallback) {
        widget.timeoutCallback();
      }
    });
  }
  void initStatewrKkVoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(50),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Center(
                child: SizedBox(
                  width: ScreenUtil().setWidth(96),
                  height: ScreenUtil().setWidth(96),
                  child: CircularProgressIndicator(
                    strokeWidth: ScreenUtil().setWidth(4),
                    //backgroundColor: Colors.white,
                    valueColor: AlwaysStoppedAnimation<Color>(Color(0xffB770FA)),
                    //值越大  圆圈越大
                  ),
                ),
              ),
              Center(
                child: Container(
                  padding: EdgeInsets.all(ScreenUtil().setWidth(24)),
                  child: Image.asset(
                    'assets/images/status/status_upload_progress.webp',
                    width: ScreenUtil().setWidth(54),
                    height: ScreenUtil().setWidth(54),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
  void buildfAtvnoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
