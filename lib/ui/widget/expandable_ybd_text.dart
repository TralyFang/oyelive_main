
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../../base/base_ybd_state.dart';

class YBDExpandableText extends StatefulWidget {
  String text;
  TextStyle? textStyle;
  Widget? moreText;
  int? maxLines;

  YBDExpandableText(this.text, {this.moreText, this.maxLines, this.textStyle});

  @override
  YBDExpandableTextState createState() => new YBDExpandableTextState();
}

class YBDExpandableTextState extends BaseState<YBDExpandableText> {
  bool isExpanded = false;

  @override
  Widget myBuild(BuildContext context) {
    return isExpanded
        ? Text(
            widget.text,
            style: widget.textStyle,
          )
        : AutoSizeText(
            widget.text,
            style: widget.textStyle,
            maxLines: widget.maxLines,
            overflowReplacement: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  widget.text,
                  style: widget.textStyle,
                  maxLines: widget.maxLines,
                ),
                GestureDetector(
                    onTap: () {
                      isExpanded = true;
                      setState(() {});
                    },
                    child: widget.moreText),
              ],
            ),
          );
  }
  void myBuild8BYKWoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
  }
  void initStateqcFNIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDExpandableText oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetMBGM5oyelive(YBDExpandableText oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
