
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class YBDCommonButton extends StatelessWidget {
  int width, height;
  String text;

  YBDCommonButton({this.width: 440, this.height: 74, this.text: ""});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(width),
      height: ScreenUtil().setWidth(height),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            Color(0xff5E94E7),
            Color(0xff47CDCC),
          ]),
          borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(100)))),
      child: Text(
        text,
        style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white, fontWeight: FontWeight.bold),
      ),
    );
  }
  void buildVA6AVoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
