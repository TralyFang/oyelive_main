
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/util/sp_ybd_util.dart';

class YBDRiskWarnDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    YBDSPUtil.save(Const.SP_NEED_FIX_PWD, "yes");
    return Center(
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
        child: Container(
          width: ScreenUtil().setWidth(500),
          height: ScreenUtil().setWidth(366),
          padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(34)),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))), color: Colors.white),
          child: Column(
            children: [
              SizedBox(
                height: ScreenUtil().setWidth(32),
              ),
              Text(
                translate('risk_warning'),
                style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.black),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(52),
              ),
              Text(
                translate('high_risk'),
                style: TextStyle(fontSize: ScreenUtil().setSp(24), color: Colors.black),
              ),
              SizedBox(
                height: ScreenUtil().setWidth(38),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  YBDNavigatorHelper.navigateTo(
                    context,
                    YBDNavigatorHelper.fix_password + "/true" + "/${YBDLocationName.RISK_DIALOG_WARN}",
                  );
                },
                child: Container(
                  width: ScreenUtil().setWidth(282),
                  height: ScreenUtil().setWidth(66),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(32))),
                    gradient: LinearGradient(
                        colors: [Color(0xff47CDCC), Color(0xff5E94E7)],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter),
                  ),
                  child: Center(
                    child: Text(
                      translate('go_on'),
                      style: TextStyle(fontSize: ScreenUtil().setSp(28), color: Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  void buildnMSvDoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
