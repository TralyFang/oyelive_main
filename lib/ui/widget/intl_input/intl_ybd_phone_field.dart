
library intl_phone_field;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/ui/widget/select_ybd_country.dart';

import './countries.dart';
import './phone_ybd_number.dart';

class YBDIntlPhoneField extends StatefulWidget {
  final bool obscureText;
  final TextAlign textAlign;
  final VoidCallback? onTap;
  final bool readOnly;
  final FormFieldSetter<YBDPhoneNumber>? onSaved;
  final ValueChanged<YBDPhoneNumber>? onChanged;
  final Function? onCountryChange;
  final FormFieldValidator<String>? validator;
  final bool autoValidate;
  final TextInputType keyboardType;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final void Function(String)? onSubmitted;
  final int maxLength;
  final bool enabled;
  final Brightness keyboardAppearance;

  /// 2 Letter ISO Code
  final String? initialCountryCode;
  final InputDecoration? decoration;
  final TextStyle? style;
  final bool showDropdownIcon;

  final BoxDecoration dropdownDecoration;
  final List<TextInputFormatter>? inputFormatters;
  bool showIsoCode;
  final bool editPhone;
  Color themeColor;
  Map<String, String> _selectedCountry =
      YBDCountries.list().firstWhere((item) => item['code'] == 'PK', orElse: () => YBDCountries.emptyCounty);

  YBDIntlPhoneField({
    this.initialCountryCode,
    this.obscureText = false,
    this.textAlign = TextAlign.left,
    this.onTap,
    this.showIsoCode: true,
    this.readOnly = false,
    this.keyboardType = TextInputType.number,
    this.autoValidate = true,
    this.controller,
    this.focusNode,
    this.decoration,
    this.style,
    this.onCountryChange(String name, String code, YBDPhoneNumber phone)?,
    this.onSubmitted,
    this.validator,
    this.themeColor: const Color(0xffDCDCDC),
    this.onChanged,
    this.onSaved,
    this.showDropdownIcon = true,
    this.dropdownDecoration = const BoxDecoration(),
    this.inputFormatters,
    this.maxLength = 10,
    this.enabled = true,
    this.keyboardAppearance = Brightness.light,
    this.editPhone = false,
  }) {
    if (this.initialCountryCode != null) {
      this._selectedCountry = YBDCountries.list()
          .firstWhere((item) => item['code'] == this.initialCountryCode, orElse: () => YBDCountries.emptyCounty);
    }
  }

  @override
  _YBDIntlPhoneFieldState createState() => _YBDIntlPhoneFieldState();
}

class _YBDIntlPhoneFieldState extends State<YBDIntlPhoneField> {
  FormFieldValidator<String>? validator;
  String? inputPhone;

  /// 已选中的国家
  // Map<String, String> _currentContry;

  @override
  void initState() {
    super.initState();
    // _currentContry = widget._selectedCountry;
    validator = widget.autoValidate ? ((value) => value.length != 10 ? 'Invalid Mobile Number' : null) as String? Function(String?)? : widget.validator;
  }
  void initStateZEszuoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  //* 在这里修改选择国家的UI
  Future<void> _changeCountry() async {
    await showDialog(
        context: context,
        builder: (context) {
          return YBDSelectCountry((Map<String, String> selected) {
            logger.v('selected country : $selected');
            setState(() {
              widget._selectedCountry = selected;
            });

            /// 去掉电话号码开头的 0
            logger.v('selected country... input number : $inputPhone');
            String subString = inputPhone?.replaceFirst(RegExp(r"[0^]*"), '') ?? '';
            widget.onCountryChange?.call(
                widget._selectedCountry['name'],
                widget._selectedCountry['code'],
                YBDPhoneNumber(
                    countryCode: widget._selectedCountry['code'],
                    countryName: widget._selectedCountry['name'],
                    dialCode: widget._selectedCountry['dial_code'],
                    number: subString));
          }, editPhone: widget.editPhone);
        });
  }
  void _changeCountryXQXo9oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        _buildFlagsButton(widget.editPhone),
        SizedBox(width: 8),
        Expanded(
          child: Container(
            height: ScreenUtil().setWidth(80),
            alignment: Alignment.center,
            child: TextFormField(
                readOnly: widget.readOnly,
                obscureText: widget.obscureText,
                textAlign: widget.textAlign,
                onTap: () {
                  if (widget.onTap != null) widget.onTap!();
                },
                controller: widget.controller,
                focusNode: widget.focusNode,
                onFieldSubmitted: (s) {
                  if (widget.onSubmitted != null) widget.onSubmitted!(s);
                },
                decoration: widget.decoration!.copyWith(isDense: true, contentPadding: EdgeInsets.zero),
                maxLines: null,
                style: widget.style,
                onSaved: (value) {
                  if (widget.onSaved != null)
                    widget.onSaved!(
                      YBDPhoneNumber(
                          countryCode: widget._selectedCountry['code'],
                          countryName: widget._selectedCountry['name'],
                          dialCode: widget._selectedCountry['dial_code'],
                          number: value),
                    );
                },
                onChanged: (value) {
                  inputPhone = value;
                  if (widget.onChanged != null) {
                    /// 去掉电话号码开头的 0
                    String subString = value.replaceFirst(RegExp(r"[0^]*"), "");
                    widget.onChanged!(
                      YBDPhoneNumber(
                          countryCode: widget._selectedCountry['code'],
                          countryName: widget._selectedCountry['name'],
                          dialCode: widget._selectedCountry['dial_code'],
                          number: subString),
                    );
                  }
                },
                validator: validator,
                keyboardType: widget.keyboardType,
                inputFormatters: widget.inputFormatters,
                maxLength: null,
                enabled: widget.enabled,
                keyboardAppearance: widget.keyboardAppearance,
                cursorColor: widget.editPhone ? Color(0xff7DFAFF) : null),
          ),
        ),
      ],
    );
  }
  void buildUBjGOoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  DecoratedBox _buildFlagsButton(bool editPhone) {
    if (editPhone)
      return DecoratedBox(
        decoration: widget.dropdownDecoration,
        child: InkWell(
          borderRadius: widget.dropdownDecoration.borderRadius as BorderRadius?,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                widget._selectedCountry['flag']!,
                style: TextStyle(fontSize: 22),
              ),
              Icon(
                Icons.keyboard_arrow_down,
                color: Colors.black.withOpacity(0.3),
                size: ScreenUtil().setWidth(55),
              ),
              FittedBox(
                child: Text(
                  widget._selectedCountry['dial_code']!,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    color: Color(0xff9A9A9A),
                    fontSize: ScreenUtil().setSp(24),
                  ),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(8)),
              Container(
                width: ScreenUtil().setWidth(2),
                height: ScreenUtil().setWidth(50),
                color: Color(0xffC4C4C4),
              ),
              // SizedBox(width: 8),
            ],
          ),
          onTap: widget.readOnly ? null : _changeCountry,
        ),
      );
    return DecoratedBox(
      decoration: widget.dropdownDecoration,
      child: InkWell(
        borderRadius: widget.dropdownDecoration.borderRadius as BorderRadius?,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if (widget.showDropdownIcon) ...[
              Icon(
                Icons.arrow_drop_down,
                color: widget.themeColor,
                size: ScreenUtil().setWidth(62),
              ),
              SizedBox(width: 4)
            ],
            Text(
              widget._selectedCountry['flag']!,
              style: TextStyle(fontSize: 22),
            ),
            SizedBox(width: 8),
            widget.showIsoCode
                ? FittedBox(
                    child: Text(
                      widget._selectedCountry['dial_code']!,
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        color: widget.themeColor,
                      ),
                    ),
                  )
                : Container(),
            SizedBox(width: 8),
          ],
        ),
        onTap: _changeCountry,
      ),
    );
  }
}
