
import 'package:flutter/foundation.dart';

class YBDPhoneNumber {
  String? countryCode;
  String? countryName;
  String? dialCode;
  String? number;

  YBDPhoneNumber({
    required this.countryCode,
    required this.countryName,
    required this.dialCode,
    required this.number,
  });

  String get completeNumber {
    return dialCode! + number!;
  }
}
