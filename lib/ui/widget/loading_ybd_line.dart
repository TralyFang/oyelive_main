
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 网络加载线条动画
class YBDLoadingLine extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _YBDLoadingLineState();
}

class _YBDLoadingLineState extends State<YBDLoadingLine> with SingleTickerProviderStateMixin {
  late Animation<double> _animation;
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    // 设置动画时长
    _animationController = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );

    // 设置动画值
    _animation = Tween(
      begin: _minWidth(),
      end: _maxWidth(),
    ).animate(_animationController);

    // 重复执行动画
    _animationController.repeat();
  }
  void initStateEGiudoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    try {
      print('before _animationController.dispose()');
      _animationController.dispose();
      print('after _animationController.dispose()');
    } catch (e) {
      print('_animationController.dispose(): $e');
    }
    super.dispose();
  }
  void disposeMBqiIoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animation,
      builder: (_, __) {
        return Center(
          child: Container(
            color: Colors.white.withOpacity(_opacityValue(_animation.value)),
            height: ScreenUtil().setWidth(2),
            width: _animation.value,
          ),
        );
      },
    );
  }
  void buildSUWJ3oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 根据当前的宽度计算透明度
  /// 宽度越大越透明
  /// [width] 当前宽度
  double _opacityValue(double width) {
    return 1 - width / _maxWidth();
  }

  /// 最小宽度
  double _minWidth() {
    return ScreenUtil().setWidth(20);
  }

  /// 最大宽度
  double _maxWidth() {
    // 和屏幕保留间距
    return ScreenUtil().screenWidth - ScreenUtil().setWidth(20);
  }
}
