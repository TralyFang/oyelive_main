
/*
 * @Author: William-Zhou
 * @Date: 2021-12-10 10:30:29
 * @LastEditTime: 2022-07-07 15:08:49
 * @LastEditors: William
 * @Description: Return the corresponding image component according to the type
 */
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/ui/page/activty/constants/act_ybd_consts.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

enum YBDImageType { asset, file, network, assetImgContainer, fileImgContainer }

class YBDImage extends StatelessWidget {
  final String? path;
  final YBDImageType? type;
  final int? width;
  final int? height;
  final BoxFit? fit;
  final ImageWidgetBuilder? imageBuilder;
  final PlaceholderWidgetBuilder? placeholder;
  final LoadingErrorWidgetBuilder? errorWidget;
  final EdgeInsetsGeometry? padding;
  final BorderRadiusGeometry? borderRadius;
  final Color? color;
  final AlignmentGeometry? alignment;
  final ColorFilter? colorFilter;
  final List<BoxShadow>? boxShadow;
  final Widget? child;
  const YBDImage({
    Key? key,
    required this.path,
    this.type,
    this.width,
    this.height,
    this.fit,
    this.imageBuilder,
    this.placeholder,
    this.errorWidget,
    this.padding,
    this.borderRadius,
    this.color,
    this.alignment,
    this.colorFilter,
    this.boxShadow,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (path == null) return Container();
    // 如果是活动图片
    if (path!.contains(ActImagePrefix)) {
      // print('12.10------path is skin:$path');
      return Image.file(
        // skinFile(path),
        File(path!),
        // File('/data/user/0/com.oyetalk.tv/app_flutter/EidUlAzha_v1/act_baggage.png'),
        width: width == null ? null : ScreenUtil().setWidth(width!),
        height: height == null ? null : ScreenUtil().setWidth(height!),
        fit: fit,
        color: color,
      );
    }
    switch (type) {
      case YBDImageType.file:
        return Image.file(
          File(path!),
          width: width == null ? null : ScreenUtil().setWidth(width!),
          height: height == null ? null : ScreenUtil().setWidth(height!),
          fit: fit,
          color: color,
        );
        break;
      case YBDImageType.assetImgContainer:
        return Container(
          width: width == null ? null : ScreenUtil().setWidth(width!),
          height: height == null ? null : ScreenUtil().setWidth(height!),
          padding: padding,
          alignment: alignment,
          decoration: BoxDecoration(
            boxShadow: boxShadow,
            borderRadius: borderRadius,
            color: color,
            image: DecorationImage(
              image: AssetImage(path!),
              colorFilter: colorFilter,
              fit: fit,
            ),
          ),
          child: child,
        );
        break;
      case YBDImageType.fileImgContainer:
        return Container(
          width: width == null ? null : ScreenUtil().setWidth(width!),
          height: height == null ? null : ScreenUtil().setWidth(height!),
          padding: padding,
          alignment: alignment,
          decoration: BoxDecoration(
            boxShadow: boxShadow,
            borderRadius: borderRadius,
            color: color,
            image: DecorationImage(
              image: FileImage(File(path!)),
              colorFilter: colorFilter,
              fit: fit,
            ),
          ),
          child: child,
        );
        break;
      default:
        if (path!.isEmpty)
          return Container(width: width == null ? null : width?.px, height: height == null ? null : height?.px);
        if (YBDImageUtil.isFullImagePath(path!))
          return YBDNetworkImage(
            imageUrl: path!,
            width: width == null ? null : ScreenUtil().setWidth(width!),
            height: height == null ? null : ScreenUtil().setWidth(height!),
            fit: fit,
            imageBuilder: imageBuilder,
            placeholder: placeholder,
            errorWidget: errorWidget,
          );
        return Image.asset(
          path!,
          width: width == null ? null : ScreenUtil().setWidth(width!),
          height: height == null ? null : ScreenUtil().setWidth(height!),
          fit: fit,
          color: color,
        );
    }
  }
  void buildCfN8Hoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
