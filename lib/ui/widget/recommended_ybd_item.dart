
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';

import '../../base/base_ybd_state.dart';
import '../../module/entity/room_ybd_info_entity.dart';
import '../page/status/follow_ybd_button.dart';
import 'level_ybd_tag.dart';
import 'room_ybd_level_tag.dart';
import 'round_ybd_avatar.dart';

class YBDRecommendedItem extends StatefulWidget {
  YBDRoomInfo? data;
  bool? followed;

  Function? onChange;
  YBDRecommendedItem(this.data, {this.followed, Key? key, this.onChange}) : super(key: key);

  @override
  YBDRecommendedItemState createState() => new YBDRecommendedItemState();
}

class YBDRecommendedItemState extends BaseState<YBDRecommendedItem> {
  bool? followed;
  @override
  Widget myBuild(BuildContext context) {
    return Padding(
      key: Key(widget.data!.id.toString()),
      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(20), horizontal: ScreenUtil().setWidth(25)),
      child: Row(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(ScreenUtil().setWidth(18)),
            child: YBDRoundAvatar(
              widget.data!.headimg,
              scene: "B",
              sex: 1,
              avatarWidth: 80,
              userId: widget.data!.id,
              lableSrc: (widget.data?.vip ?? 0) == 0 ? "" : "assets/images/vip_lv${widget.data!.vip}.png",
              needNavigation: false,
              sideBorder: false,
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.data!.nickname ?? '',
                    style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28)),
                  ),
                  Padding(padding: EdgeInsets.only(top: ScreenUtil().setWidth(widget.data!.vipIcon == null ? 16 : 6))),
                  Row(
                    children: <Widget>[
                      YBDVipIcon(widget.data!.vipIcon),
                      YBDLevelTag(widget.data!.level),
                      YBDRoomLevelTag(widget.data!.roomlevel),
                    ],
                  )
                ],
              ),
            ),
          ),
          YBDFollowButton(
            followed,
            widget.data!.id.toString(),
            'recommended_page',
            key: Key(widget.data!.id.toString()),
            onChange: (x) {
              followed = x;
              setState(() {});
              widget.onChange?.call(widget.data!.id);
            },
          )
        ],
      ),
    );
  }
  void myBuild56weToyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
    followed = widget.followed;
  }
  void initStateWj5Xfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
