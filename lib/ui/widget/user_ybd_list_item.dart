import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/vip_ybd_icon.dart';
import 'package:oyelive_main/ui/widget/icon_ybd_gender.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/certification_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/module/entity/user_ybd_certification_entity.dart';
import 'package:oyelive_main/ui/widget/search_ybd_id_tag.dart';
import '../../base/base_ybd_state.dart';
import '../page/profile/my_profile/wear_ybd_badge_item.dart';
import 'room_ybd_level_tag.dart';

import '../../module/user/entity/user_ybd_info_entity.dart';
import 'level_ybd_tag.dart';
import 'round_ybd_avatar.dart';

class YBDUserListItem extends StatefulWidget {
  YBDUserInfo? data;

  YBDUserListItem(
    this.data, {
    Key? key,
  }) : super(key: key);

  @override
  YBDUserListItemState createState() => new YBDUserListItemState();
}

YBDUserCertificationRecordCertificationInfos? iInfo;

class YBDUserListItemState extends BaseState<YBDUserListItem> {
  @override
  Widget myBuild(BuildContext context) {
    return Container(
      height: ScreenUtil().setWidth(130),
      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(16), horizontal: ScreenUtil().setWidth(25)),
      child: Row(
        children: <Widget>[
          YBDRoundAvatar(
            widget.data?.headimg,
            scene: "B",
            sex: widget.data?.sex,
            avatarWidth: 80,
            userId: widget.data?.id,
            lableSrc: (widget.data?.vip ?? 0) == 0 || iInfo?.hidUserLevel == 1
                ? ""
                : "assets/images/vip_lv${widget.data!.vip}.png",
            needNavigation: true,
            labelWitdh: 30,
            levelFrame: widget.data?.headFrame,
            sideBorder: false,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: StreamBuilder<Map<int?, YBDUserCertificationRecordCertificationInfos?>>(
                  stream: YBDCertificationUtil.getInstance()!.certificationCacheOutStream,
                  initialData: YBDCertificationUtil.getInstance()!.certificationCache,
                  builder: (context, snapshot) {
                    bool hasCer = false;
                    YBDUserCertificationRecordCertificationInfos? info;
                    if (snapshot.hasData) {
                      int? currentUserId = widget.data!.id;
                      info = snapshot.data![currentUserId];
                      hasCer = info != null;
                      iInfo = info;
                    }
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        // Text.rich(
                        //   TextSpan(style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(28)), children: [
                        //     TextSpan(text: widget.data.nickname ?? ''),
                        //     if (hasCer)
                        //       WidgetSpan(
                        //           child: Row(
                        //         mainAxisSize: MainAxisSize.min,
                        //         children: [
                        //           SizedBox(
                        //             width: ScreenUtil().setWidth(10),
                        //           ),
                        //           YBDNetworkImage(
                        //             imageUrl: info.icon,
                        //             width: ScreenUtil().setWidth(36),
                        //           )
                        //         ],
                        //       ))
                        //   ]),
                        //   maxLines: 1,
                        //   overflow: TextOverflow.ellipsis,
                        //   softWrap: false,
                        // ),
                        Row(children: [
                          ConstrainedBox(
                            child: Text(
                              widget.data?.nickname?.removeBlankSpace() ?? '',
                              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(24)),
                              overflow: TextOverflow.ellipsis,
                              softWrap: true,
                            ),
                            constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(340)),
                          ),
                          if (!hasCer || info?.hidGender == 0) YBDGenderIcon(widget.data?.sex),
                          YBDSearchIdTag(user: widget.data),
                          if (hasCer) ...[
                            SizedBox(
                              width: ScreenUtil().setWidth(6),
                            ),
                            YBDNetworkImage(
                              imageUrl: info!.icon!,
                              width: ScreenUtil().setWidth(34),
                            )
                          ]
                        ]),
                        Padding(
                            padding: EdgeInsets.only(top: ScreenUtil().setWidth(widget.data?.vipIcon != null ? 6 : 16))),
                        Row(
                          children: <Widget>[
                            YBDVipIcon(
                              widget.data?.vipIcon,
                              padding: EdgeInsets.only(right: ScreenUtil().setWidth(4)),
                            ),
                            if (!hasCer || info?.hidUserLevel == 0) YBDLevelTag(widget.data?.level),
                            if (!hasCer || info?.hidTalentLevel == 0) YBDRoomLevelTag(widget.data?.roomlevel),
                            if (!hasCer || info?.hidBadge == 0) YBDWearBadgeItem(widget.data?.badges)
                          ],
                        )
                      ],
                    );
                  }),
            ),
          ),
        ],
      ),
    );
  }
  void myBuild0Fbdooyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
