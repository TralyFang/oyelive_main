import 'package:flutter/material.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';

class YBDGenderIcon extends StatelessWidget {
  int? gender;
  EdgeInsetsGeometry padding;
  YBDGenderIcon(this.gender, {this.padding = EdgeInsets.zero});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.px),
      child: Container(
        // 性别标签
        height: 23.px,
        width: 23.px,
        child: Image.asset(
          gender == 2 ? 'assets/images/liveroom/icon_male@2x.webp' : 'assets/images/liveroom/icon_female@2x.webp',
          fit: BoxFit.cover,
        ),
      ),
    );
  }
  void buildu8sYzoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
