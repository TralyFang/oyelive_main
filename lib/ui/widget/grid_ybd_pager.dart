
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../base/base_ybd_state.dart';
import '../page/room/bloc/gift_ybd_sheet_bloc.dart';
import '../page/status/bloc/status_ybd_detail_gift_bloc.dart';

class YBDGridPager extends StatefulWidget {
  List<Widget>? itemWidgets;
  int pageWidth, pageHeight;
  int column, row;
  bool isStatus;

  YBDGiftSheetBloc? blc;

  bool isWhite;

  YBDGridPager(
      {this.itemWidgets,
      this.pageWidth: 720,
      this.pageHeight: 400,
      this.column: 4,
      this.row: 2,
      this.isStatus: false,
      this.blc,
      this.isWhite = false});

  @override
  YBDGridPagerState createState() => new YBDGridPagerState();
}

class YBDGridPagerState extends BaseState<YBDGridPager> with TickerProviderStateMixin {
  int currentViewPageIndex = 0;
  TabController? _tabController;

  late int count;
  late int column;
  late int row;

  int? left;
  int? page;

  setIndex(int index) {
    currentViewPageIndex = index;
  }

  initPager() {
//    currentViewPageIndex = 0;
    count = widget.itemWidgets!.length;
    column = widget.column;
    row = widget.row;

    left = count % (column * row);
    page = count ~/ (column * row) + (left == 0 ? 0 : 1);
    if (_tabController != null) {
      _tabController = null;
    }
    _tabController = new TabController(length: page!, vsync: this, initialIndex: currentViewPageIndex);
    _tabController!.addListener(() {
      currentViewPageIndex = _tabController!.index;
      setState(() {});
    });
  }

  buildGrid(List<Widget> list) {
    return Container(
      height: ScreenUtil().setWidth(widget.pageHeight),
      width: ScreenUtil().setWidth(widget.pageWidth),
      child: GridView.count(
        shrinkWrap: true,
        crossAxisCount: column,
        children: list,
        childAspectRatio: 9 / 10,
      ),
    );
  }

  makeItemPager() {
    if (widget.itemWidgets!.length == 0) {
      return Container(
        height: ScreenUtil().setWidth(widget.pageHeight + 20),
        width: ScreenUtil().setWidth(widget.pageWidth),
        child: Center(
          child: Text(
            "No items",
            style: TextStyle(color: widget.isWhite ? Colors.black : Colors.white),
          ),
        ),
      );
    }
    Widget grid = Container(
      height: ScreenUtil().setWidth(widget.pageHeight),
      width: ScreenUtil().setWidth(widget.pageWidth),
      child: TabBarView(
        controller: _tabController,
        children: List.generate(page!, (index) {
          return buildGrid(List.generate(index + 1 == page && left != 0 ? left! : column * row,
              (itemIndex) => widget.itemWidgets![index * column * row + itemIndex]));
        }),
      ),
    );

    Widget indicator = Container(
      height: ScreenUtil().setWidth(10),
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemBuilder: (_, index) => Container(
                width: ScreenUtil().setWidth(10),
                height: ScreenUtil().setWidth(10),
                decoration: currentViewPageIndex == index
                    ? BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(colors: [
                          Color(0xff47CDCC),
                          Color(0xff5E94E7),
                        ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                      )
                    : BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                            width: ScreenUtil().setWidth(1), color: widget.isStatus ? Colors.black : Colors.white),
                      ),
              ),
          separatorBuilder: (_, index) => SizedBox(
                width: ScreenUtil().setWidth(10),
              ),
          itemCount: page!),
    );
    return Column(
      children: [
        grid,
        SizedBox(
          height: ScreenUtil().setWidth(10),
        ),
        indicator
      ],
    );
  }

  @override
  Widget myBuild(BuildContext context) {
    // TODO: implement myBuild
    initPager();

    return makeItemPager();
  }
  void myBuildUfrz7oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    initPager();
    if (widget.isStatus) {
      context.read<YBDGiftBloc>().stream.listen((state) {
        if (state.sheetAction == StatusSheetAction.ChangeTab) {
          currentViewPageIndex = 0;
          _tabController!.animateTo(0);
        }
      });
      if (context.read<YBDGiftBloc>().state.selectingGiftIndex != null) {
        int pageCount = ((context.read<YBDGiftBloc>().state.selectingGiftIndex! + 1) / (widget.column * widget.row)).ceil();
        _tabController!.animateTo(pageCount - 1);
      }
    } else {
      widget.blc!.stream.listen((state) {
        if (state.sheetAction == SheetAction.ChangeTab) {
          currentViewPageIndex = 0;
          _tabController!.animateTo(0);
        }
      });

      if (widget.blc!.state.selectingGiftIndex != null) {
        int pageCount = ((widget.blc!.state.selectingGiftIndex! + 1) / (widget.column * widget.row)).ceil();
        _tabController!.animateTo(pageCount - 1);
      }
      // context.read<YBDGiftSheetBloc>().listen((state) {
      //   if (state.sheetAction == SheetAction.ChangeTab) {
      //     currentViewPageIndex = 0;
      //     _tabController.animateTo(0);
      //   }
      // });
      //
      // if (context.read<YBDGiftSheetBloc>().state.selectingGiftIndex != null) {
      //   int pageCount =
      //       ((context.read<YBDGiftSheetBloc>().state.selectingGiftIndex + 1) / (widget.column * widget.row)).ceil();
      //   _tabController.animateTo(pageCount - 1);
      // }
    }
  }
  void initStateU5Msfoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDGridPager oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetohvLOoyelive(YBDGridPager oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
}
