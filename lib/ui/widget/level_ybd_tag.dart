
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/common/util/img_ybd_resource_util.dart';
import '../../common/util/screen_ybd_expand_util.dart';

class YBDLevelTag extends StatelessWidget {
  int? level;
  double scale;
  double horizontalPadding;
  YBDLevelTag(this.level, {this.scale = 1, this.horizontalPadding: 10});

  @override
  Widget build(BuildContext context) {
    if (level == null) level = 0;
    int index = level! ~/ 10;
    if (index > 10) index = 10;
    double lvHeight = ScreenUtil().setWidth(24);
    return Transform.scale(
      scale: 1.2 * scale,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: YBDScreenExpandUtil.getInstance().setScaleWidth(horizontalPadding, forceScale: scale)),
            child: (YBDImg.level + index.toString()).lv.asset(height: lvHeight, fit: BoxFit.fitWidth),
          ),
          Positioned(
            left: ScreenUtil().setWidth(40 * scale),
            top: ScreenUtil().setWidth(5),
            bottom: 0,
            child: Text(
              "LV$level",
              style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(13), fontWeight: FontWeight.w400),
            ),
          )
        ],
      ),
    );
  }
  void buildNomcpoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
