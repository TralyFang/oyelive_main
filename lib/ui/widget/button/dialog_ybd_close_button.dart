
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../common/util/log_ybd_util.dart';

/// 增大点击区域大的按钮
class YBDDialogCloseButton extends StatelessWidget {
  /// 点击响应方法
  final VoidCallback? onTap;

  /// 宽度
  final double? width;

  /// 高度
  final double? height;

  /// 按钮颜色
  final Color color;

  /// 缩放比例
  final double scale;

  YBDDialogCloseButton({
    this.width,
    this.height,
    this.scale = 1,
    this.color = Colors.grey,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v('clicked close btn');
        if (null != this.onTap) {
          onTap!();
        }
      },
      child: Container(
        child: Stack(
          children: <Widget>[
            Container(
              width: width,
              height: height,
            ),
            Positioned(
              right: 0,
              child: Container(
                // 关闭按钮
                width: ScreenUtil().setWidth(60 * scale),
                height: ScreenUtil().setWidth(60 * scale),
                child: Image.asset(
                  'assets/images/dialog_close_icon.png',
                  color: color,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void buildezdoroyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
