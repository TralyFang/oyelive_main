
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../common/util/log_ybd_util.dart';

/// 间隔两秒才能点击的按钮
class YBDDelayInkWell extends StatefulWidget {
  // 子组件
  final Widget? child;

  /// 点击效果圆角
  final BorderRadius? borderRadius;

  /// 点击回调事件
  final VoidCallback? onTap;

  YBDDelayInkWell({this.child, this.borderRadius, this.onTap});

  @override
  YBDDelayInkWellState createState() => YBDDelayInkWellState();
}

class YBDDelayInkWellState extends State<YBDDelayInkWell> {
  /// 按钮可点击
  bool enabled = true;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: widget.borderRadius,
      onTap: () {
        Timer(Duration(seconds: 2), () {
          enabled = true;
        });

        if (enabled) {
          enabled = false;
          widget.onTap!();
        } else {
          logger.v('button is not enable');
        }
      },
      child: widget.child,
    );
  }
  void buildFSrmGoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
