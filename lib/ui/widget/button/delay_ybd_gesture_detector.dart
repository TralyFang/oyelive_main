
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/ui/page/game_room/widget/ExtraHittest/gesture_ybd_detector.dart';

import '../../../common/util/log_ybd_util.dart';

/// 间隔两秒才能点击的按钮
class YBDDelayGestureDetector extends StatefulWidget {
  // 子组件
  final Widget? child;

  /// 点击回调事件
  final VoidCallback? onTap;

  /// 点击超时回调事件, 即恢复了可点击状态的回调
  final VoidCallback? onTapTimeOut;

  /// 点击回调事件
  final GestureTapDownCallback? onTapDown;

  /// 点击回调事件
  final VoidCallback? onTapCancel;

  /// 点击回调事件
  final GestureTapUpCallback? onTapUp;

  final HitTestBehavior behavior;

  final Duration delayDuration; // ms

  // 扩展点击事件
  final EdgeInsets? extraHitTestArea;

  final YBDAnalyticsEvent? track;

  YBDDelayGestureDetector({
    this.child,
    this.onTap,
    this.onTapTimeOut,
    this.onTapCancel,
    this.onTapDown,
    this.onTapUp,
    this.delayDuration = const Duration(seconds: 2),
    this.behavior = HitTestBehavior.deferToChild,
    this.extraHitTestArea,
    this.track,
  });

  @override
  YBDDelayButtonState createState() => YBDDelayButtonState();
}

class YBDDelayButtonState extends State<YBDDelayGestureDetector> {
  /// 按钮可点击
  bool enabled = true;

  @override
  Widget build(BuildContext context) {
    // 有扩展区域才使用！
    if (widget.extraHitTestArea != null && widget.extraHitTestArea != EdgeInsets.zero) {
      return YBDGestureDetectorHitTestWithoutSizeLimit(
        // debugHitTestAreaColor: Colors.white.withOpacity(0.7),
        extraHitTestArea: widget.extraHitTestArea,
        onTap: _onTap,
        onTapDown: widget.onTapDown,
        onTapCancel: widget.onTapCancel,
        onTapUp: widget.onTapUp,
        child: _childContainer(),
      );
    }
    return GestureDetector(
      behavior: widget.behavior,
      onTap: _onTap,
      onTapDown: widget.onTapDown,
      onTapCancel: widget.onTapCancel,
      onTapUp: widget.onTapUp,
      child: _childContainer(),
    );
  }
  void build3GxzFoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  DateTime? callTime;

  _onTap() {
    if (callTime == null) {
      callTime = DateTime.now();
      widget.onTap?.call();
    } else {
      if (DateTime.now().difference(callTime!) > widget.delayDuration) {
        callTime = DateTime.now();
        // track
        if (widget.track != null && widget.track?.eventName != null) {
          YBDTATrack().trackMap(widget.track!.eventName, properties: widget.track?.getParams());
        }
        return widget.onTap?.call();
      } else {
        print("not responed!");
      }
    }
    // Timer(widget.delayDuration, () {
    //   enabled = true;
    //   return widget.onTapTimeOut?.call();
    // });
    // if (enabled) {
    //   enabled = false;
    //   return widget.onTap?.call();
    //   // track
    //   if (widget.track != null && widget.track.eventName != null) {
    //     YBDTATrack().trackMap(widget.track.eventName, properties: widget.track.getParams());
    //   }
    // } else {
    //   logger.v('button is not enable');
    // }
  }

  Widget _childContainer() {
    return Container(
      decoration: BoxDecoration(color: Colors.transparent),
      child: widget.child,
    );
  }
  void _childContainergZTEzoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
