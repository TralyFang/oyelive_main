
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../common/util/log_ybd_util.dart';

// 点 ok 按钮的回调方法
typedef OkCallbackFunc = Function(String reportTitle);

/// 举报弹框
class YBDProfileReportDialog extends StatefulWidget {
  final OkCallbackFunc okCallback;
  final Function cancelCallback;

  /// 苹果审核要有 block 的按钮
  final bool showBlockBtn;

  YBDProfileReportDialog(this.okCallback, this.cancelCallback, {this.showBlockBtn = false});

  @override
  YBDProfileReportDialogState createState() => new YBDProfileReportDialogState();
}

class YBDProfileReportDialogState extends State<YBDProfileReportDialog> {
  // TODO: 翻译
  static final String harassment = translate('harassment');
  static final String scam = translate('scam');
  static final String pornography = translate('pornography');
  static final String advertisement = translate('advertisement');
  static final String blockUser = translate('block');

  @override
  void initState() {
    super.initState();
  }
  void initStateslWr4oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        logger.v("clicked the report dialog bg");
        // 隐藏举报弹框
        Navigator.pop(context);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: ScreenUtil().setWidth(468),
                height: widget.showBlockBtn ? ScreenUtil().setWidth(501) : ScreenUtil().setWidth(421),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular((ScreenUtil().setWidth(16))),
                    ),
                    color: Colors.white),
                child: Column(
                  children: <Widget>[
                    // 标题
                    Container(
                      height: ScreenUtil().setWidth(99),
                      child: Center(
                        child: Text(
                          translate('report'),
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: ScreenUtil().setSp(30),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    rowDivider(),
                    if (widget.showBlockBtn) ...[
                      GestureDetector(
                        onTap: () {
                          logger.v('clicked report block user');
                          clickedReportButton(blockUser);
                        },
                        child: reportContentRow(blockUser),
                      ),
                      rowDivider(),
                    ],
                    // harassment
                    GestureDetector(
                      onTap: () {
                        logger.v('clicked report harassment');
                        clickedReportButton(harassment);
                      },
                      child: reportContentRow(harassment),
                    ),
                    rowDivider(),
                    // scam
                    GestureDetector(
                      onTap: () {
                        logger.v('clicked report scam');
                        clickedReportButton(scam);
                      },
                      child: reportContentRow(scam),
                    ),
                    rowDivider(),
                    // pornography
                    GestureDetector(
                      onTap: () {
                        logger.v('clicked report pornography');
                        clickedReportButton(pornography);
                      },
                      child: reportContentRow(pornography),
                    ),
                    rowDivider(),
                    // advertisement
                    GestureDetector(
                      onTap: () {
                        logger.v('clicked report advertisement');
                        clickedReportButton(advertisement);
                      },
                      child: reportContentRow(advertisement),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildxQ8OYoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 点击举报内容按钮的响应事件
  clickedReportButton(String content) {
    logger.v('button action : $content');

    if (null != widget.okCallback) {
      widget.okCallback(content);
    } else {
      logger.v('report callback is empty');
    }
  }

  /// 举报行
  Container reportContentRow(String content) {
    return Container(
      height: ScreenUtil().setWidth(79),
      width: ScreenUtil().setWidth(400),
      color: Colors.white,
      child: Center(
        child: Text(
          content,
          style: TextStyle(
            color: Color(0xff5F97DA),
            fontSize: ScreenUtil().setSp(28),
            fontWeight: FontWeight.normal,
          ),
        ),
      ),
    );
  }

  /// 分割线
  Divider rowDivider() {
    return Divider(
      height: ScreenUtil().setWidth(1),
      thickness: ScreenUtil().setWidth(1),
      color: Color(0xffDEDCDC),
    );
  }
}
