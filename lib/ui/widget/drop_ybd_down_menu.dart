
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../base/base_ybd_state.dart';
import '../../common/util/log_ybd_util.dart';
import '../../main.dart';
import '../../module/status/status_ybd_api_helper.dart';
import '../page/status/handler/status_ybd_delete_event.dart';

import 'bottom_ybd_selector_sheet.dart';

enum ShowTo { Author, User }

class YBDDropDownMenu extends StatefulWidget {
  ShowTo showTo;
  var iconWidth;

  var paddingWith;
  String statusId;
  Function? onSuccessDelete;

  YBDDropDownMenu(this.statusId, this.showTo, this.iconWidth, {this.onSuccessDelete, this.paddingWith: 0});

  @override
  YBDDropDownMenuState createState() => new YBDDropDownMenuState();
}

class YBDDropDownMenuState extends BaseState<YBDDropDownMenu> {
  var authorString = [translate('delete')];
  var userString = [translate('report')];

  var selectingValue;

  showAlertDialog() {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text(
        translate('cancel'),
        style: TextStyle(color: Color(0xff5E94E7)),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text(
        translate('ok'),
        style: TextStyle(color: Color(0xff5E94E7)),
      ),
      onPressed: () async {
        Navigator.pop(context);
        bool isSuccess = await YBDStatusApiHelper.deleteStatus(context, widget.statusId);
        if (isSuccess) {
          eventBus.fire(YBDStatusDeleteEvent(widget.statusId));
        }
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
//      title: Text("AlertDialog"),
      content: Text(translate('status_delete')),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    logger.v('showdia');

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showReportDialog() {
    var reportDialog = SimpleDialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(ScreenUtil().setWidth(16)))),
      title: Container(height: ScreenUtil().setWidth(99), child: Center(child: new Text(translate('report_for')))),
      contentPadding: EdgeInsets.all(0),
      titlePadding: EdgeInsets.all(0),
      children: <Widget>[
        Container(
          height: ScreenUtil().setWidth(1),
          color: Color(0xffDEDCDC),
        ),
        Container(
          height: ScreenUtil().setWidth(80),
          child: SimpleDialogOption(
            child: Center(
              child: Text(
                translate('pornography'),
                style: TextStyle(color: Color(0xff5F97DA), fontSize: ScreenUtil().setSp(28)),
              ),
            ),
            onPressed: () {
              YBDStatusApiHelper.reportStatus(context, widget.statusId, translate('pornography'));
              Navigator.pop(context);
            },
          ),
        ),
        Container(
          height: ScreenUtil().setWidth(1),
          color: Color(0xffDEDCDC),
        ),
        Container(
          height: ScreenUtil().setWidth(80),
          child: SimpleDialogOption(
              child: Center(
                child: Text(
                  translate('spam'),
                  style: TextStyle(color: Color(0xff5F97DA), fontSize: ScreenUtil().setSp(28)),
                ),
              ),
              onPressed: () {
                YBDStatusApiHelper.reportStatus(context, widget.statusId, translate('spam'));
                Navigator.pop(context);
              }),
        ),
        Container(
          height: ScreenUtil().setWidth(1),
          color: Color(0xffDEDCDC),
        ),
        Container(
          height: ScreenUtil().setWidth(80),
          child: SimpleDialogOption(
              child: Center(
                child: Text(
                  translate('illegal'),
                  style: TextStyle(color: Color(0xff5F97DA), fontSize: ScreenUtil().setSp(28)),
                ),
              ),
              onPressed: () {
                YBDStatusApiHelper.reportStatus(context, widget.statusId, translate('illegal'));

                Navigator.pop(context);
              }),
        ),
        Container(
          height: ScreenUtil().setWidth(1),
          color: Color(0xffDEDCDC),
        ),
        Container(
          height: ScreenUtil().setWidth(80),
          child: SimpleDialogOption(
              child: Center(
                child: Text(
                  translate('unhealthy'),
                  style: TextStyle(color: Color(0xff5F97DA), fontSize: ScreenUtil().setSp(28)),
                ),
              ),
              onPressed: () {
                YBDStatusApiHelper.reportStatus(context, widget.statusId, translate('unhealthy'));

                Navigator.pop(context);
              }),
        ),
        Container(
          height: ScreenUtil().setWidth(1),
          color: Color(0xffDEDCDC),
        ),
        Container(
          height: ScreenUtil().setWidth(80),
          child: SimpleDialogOption(
              child: Center(
                child: Text(
                  translate('other'),
                  style: TextStyle(color: Color(0xff5F97DA), fontSize: ScreenUtil().setSp(28)),
                ),
              ),
              onPressed: () {
                YBDStatusApiHelper.reportStatus(context, widget.statusId, translate('other'));

                Navigator.pop(context);
              }),
        ),
      ],
    );

    showCupertinoDialog(
      context: context,
      builder: (BuildContext context) {
        return reportDialog;
      },
    );
  }

  onSelect(int index) {}

  onClick() {
    showModalBottomSheet(
//            shape: RoundedRectangleBorder(
//                borderRadius: BorderRadius.vertical(top: Radius.circular(ScreenUtil().setWidth(16)))),
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return YBDBottomSelectorSheet(
            opts: [widget.showTo == ShowTo.User ? translate('report') : translate('delete')],
            onSelectAt: (index) {
              switch (widget.showTo) {
                case ShowTo.Author:
                  showAlertDialog();
                  break;
                case ShowTo.User:
                  showReportDialog();
                  break;
              }
            },
          );
        });
  }

  @override
  Widget myBuild(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Container(
        width: ScreenUtil().setWidth(widget.iconWidth + 2 * widget.paddingWith),
        height: ScreenUtil().setWidth(widget.iconWidth + widget.paddingWith + 20),
        decoration: BoxDecoration(),
        padding: EdgeInsets.symmetric(
            vertical: ScreenUtil().setWidth(widget.paddingWith),
            horizontal: ScreenUtil().setWidth(widget.paddingWith + 10)),
        child: Icon(
          Icons.more_vert,
          color: Colors.white,
          size: ScreenUtil().setWidth(widget.iconWidth),
        ),
      ),
    );

    return Container(
      height: ScreenUtil().setWidth(widget.iconWidth),
      child: PopupMenuButton<int>(
        padding: EdgeInsets.all(0),
        color: Colors.white,
        icon: Icon(
          Icons.more_vert,
          color: Colors.white,
          size: ScreenUtil().setWidth(widget.iconWidth),
        ),
        onSelected: onSelect,
        itemBuilder: (context) => [
          PopupMenuItem(
            value: 1,
            child: Text(widget.showTo == ShowTo.Author ? authorString[0] : userString[0]),
          ),
        ],
      ),
    );
  }
  void myBuildF9tWeoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void initState() {
    super.initState();
  }
  void initStaterpiBjoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(YBDDropDownMenu oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
  void didUpdateWidgetnOPL0oyelive(YBDDropDownMenu oldWidget) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
}
