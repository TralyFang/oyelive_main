
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:oyelive_main/base/base_ybd_state.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/activty/act_ybd_root.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_skin.dart';
import 'package:oyelive_main/ui/page/home/widget/pk_ybd_label_item.dart';
import 'package:oyelive_main/ui/page/home/widget/super_ybd_star_item.dart';
import 'package:oyelive_main/ui/page/home/widget/top_ybd_talent_item.dart';
import 'package:oyelive_main/ui/page/home/widget/weekly_ybd_star_item.dart';
import 'package:oyelive_main/ui/widget/room_ybd_category_widget.dart';
import 'package:oyelive_main/ui/widget/room_ybd_level_tag.dart';

class YBDRoomListItem extends StatefulWidget {
  /// 房间信息
  final YBDRoomInfo? data;

  /// weekly star 房间列表
  final List<String>? superRooms;

  /// super star 列表
  final List<int?>? superStars;

  /// 房间标题颜色
  final Color textColor;

  /// 昵称和在线人数颜色
  final Color? nameColor;

  final bool isExtendVo;
  YBDRoomListItem(
    this.data,
    this.textColor, {
    this.superRooms,
    this.superStars,
    this.nameColor,
    this.isExtendVo: false,
    Key? key,
  }) : super(key: key);

  @override
  YBDRoomListItemState createState() => new YBDRoomListItemState();
}

class YBDRoomListItemState extends BaseState<YBDRoomListItem> {
  @override
  Widget myBuild(BuildContext context) {
    // 是否为 weekly star 房间
    bool isWeeklyStar = false;
    if (null != widget.superRooms && widget.superRooms!.isNotEmpty) {
      if (null != widget.data && widget.superRooms!.contains('${widget.data!.id}')) {
        logger.v('is weekly star room : ${widget.data!.id}');
        isWeeklyStar = true;
      }
    }

    // 是否加锁
    bool isLocked = false;

    if (null != widget.data && widget.data!.protectMode == 1) {
      logger.v('room ${widget.data!.id} is locked');
      isLocked = true;
    }
    // logger.v('roomlistitem.roomimg:${YBDImageUtil.cover(
    //   context,
    //   widget.data.roomimg,
    //   widget.data.id,
    //   scene: 'C',
    // )}');

    return Stack(children: [
      Padding(
        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(20), horizontal: ScreenUtil().setWidth(24)),
        child: Row(
          children: <Widget>[
            ClipRRect(
              // 房间图像
              borderRadius: BorderRadius.circular(ScreenUtil().setWidth(18)),
              child: SizedBox(
                width: ScreenUtil().setWidth(160),
                child: YBDNetworkImage(
                  // 从网络获取房间图像
                  imageUrl: YBDImageUtil.cover(
                        context,
                        widget.data!.roomimg,
                        widget.data!.id,
                        scene: 'C',
                      ),
                  fit: BoxFit.cover,
                  placeholder: (context, url) => Container(
                      // 房间图像占位图
                      width: ScreenUtil().setWidth(160),
                      height: ScreenUtil().setWidth(160),
                      color: YBDActivitySkinRoot().curAct().exploreRecordBgColor(),
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(50), vertical: ScreenUtil().setWidth(49)),
                      child: Image.asset('assets/images/rabbit_head.png')),
                  width: ScreenUtil().setWidth(160),
                  height: ScreenUtil().setWidth(160),
                ),
              ),
            ),
            Expanded(
              // 中间可拉伸区域
              child: Padding(
                padding: EdgeInsets.only(left: ScreenUtil().setWidth(25)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          // 房间名称
                          width: ScreenUtil().setWidth(320),
                          child: Text(
                            widget.data!.roomTitle!.isNotEmpty ? widget.data!.roomTitle! : 'Offline',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: widget.textColor,
                              fontSize: ScreenUtil().setSp(28),
                            ),
                          ),
                        ),
                        Expanded(child: SizedBox(width: ScreenUtil().setWidth(1))),
                        isLocked
                            ? Image.asset('assets/images/liveroom/room_icon_lock.png',
                                width: ScreenUtil().setWidth(44), height: ScreenUtil().setWidth(44))
                            : Container(),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: ScreenUtil().setWidth(12)),
                      child: Row(
                        children: <Widget>[
                          // 房间分类
                          YBDRoomCategoryWidget(widget.data!.roomCategory),
                          SizedBox(width: ScreenUtil().setWidth(6)),
                          // 周榜之星
                          isWeeklyStar ? YBDWeeklyStarItem() : Container(),
                          SizedBox(width: ScreenUtil().setWidth(6)),

                          YBDSuperStarItem(roomId: widget.data!.id, superStars: widget.superStars),
                          SizedBox(width: ScreenUtil().setWidth(6)),
                          if ((widget.isExtendVo && (widget.data!.extendVo!.playingLudo ?? false)) ||
                              (widget.data!.playingLudo ?? false))
                            Padding(
                              padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(10)),
                              child: YBDNetworkImage(
                                imageUrl:
                                    (widget.isExtendVo ? widget.data!.extendVo!.ludoIconUrl : widget.data!.ludoIconUrl) ??
                                        "",
                                width: ScreenUtil().setWidth(70),
                              ),
                            ),
                          Expanded(child: SizedBox(width: ScreenUtil().setWidth(1))),
                          // 房间人数
                          (widget.data!.live ?? false)
                              ? Container(
                                  height: ScreenUtil().setWidth(50),
                                  padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(10)),
                                  child: Row(
                                    children: <Widget>[
                                      Image.asset(
                                        // 音乐动图
                                        'assets/images/icon_live.webp',
                                        width: ScreenUtil().setWidth(36),
                                        height: ScreenUtil().setWidth(33),
                                      ),
                                      Container(
                                        // 房间人数
                                        padding: EdgeInsets.fromLTRB(
                                            ScreenUtil().setWidth(10), 0, 0, ScreenUtil().setWidth(7)),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[
                                            Text(
                                              widget.data!.count?.toString() ?? '-',
                                              // 可以限制最大宽度
                                              // constraints: BoxConstraints(
                                              // maxHeight: maxWidth: ScreenUtil().setWidth(450),
                                              style: TextStyle(
                                                color: widget.nameColor ??
                                                    YBDActivitySkinRoot().curAct().roomListItemTextColor(),
                                                fontSize: ScreenUtil().setSp(24),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : Container(height: ScreenUtil().setWidth(50))
                        ],
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          constraints: BoxConstraints(maxWidth: ScreenUtil().setWidth(370)),
                          child: Text(
                            // 昵称
                            widget.data!.nickname ?? '-',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: widget.nameColor ?? YBDActivitySkinRoot().curAct().roomListItemTextColor(),
                              fontSize: ScreenUtil().setSp(24),
                            ),
                          ),
                        ),
                        // 房间级别
                        YBDRoomLevelTag(widget.data!.roomlevel),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      Positioned(
          left: ScreenUtil().setWidth(6),
          bottom: ScreenUtil().setWidth(5),
          child: YBDTopTalentItem(
            widget.data!.aRanking ?? 0,
            width: ScreenUtil().setWidth(196),
            height: ScreenUtil().setWidth(86),
          )),
      // pk标签 要先判断是否在pk  从roomInfo中取数据
      widget.data!.isPking ?? false
          ? Positioned(
              left: ScreenUtil().setWidth(23),
              top: ScreenUtil().setWidth(22),
              child: YBDPKLabelItem(
                width: ScreenUtil().setWidth(90),
                height: ScreenUtil().setWidth(35),
              ))
          : Container(),
    ]);
  }
  void myBuildwLfWGoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
