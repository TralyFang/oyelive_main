
import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import '../../page/room/live_chat/item_ybd_bullet.dart';

import 'barrage_ybd_transition.dart';

const Duration _kDuration = Duration(seconds: 3);

class YBDBarrage extends StatefulWidget {
  YBDBarrage({Key? key, this.showCount = 3, this.padding = 20, this.randomOffset = 0}) : super(key: key);

  ///
  /// 显示的行数
  ///
  final int showCount;

  ///
  /// 水平弹幕：表示top、bottom的padding
  /// 垂直弹幕：表示left、right的padding
  ///
  final double padding;

  ///
  /// 随机偏移量
  ///
  final int randomOffset;

  @override
  State<StatefulWidget> createState() => YBDBarrageState();
}

class YBDBarrageState extends State<YBDBarrage> {
  ///
  /// 弹幕列表
  ///
  List<_YBDBarrageTransitionItem> _barrageList = [];

  List<_YBDBarrageTransitionItem> _barrageListAll = [];

  List<Widget> _barrageWidget = [];

  ///
  /// 定时清除弹幕
  ///
  Timer? _timer;
  Random _random = Random();
  late double _height;
  double? _width;
  int barrageIndex = 0;

  var lastTop;

  addBarrageWidget(Widget child) {
    print(
        'YBDBarrageState addBarrageWidget _barrageList.length:${_barrageList.length} _barrageWidget.length:${_barrageWidget.length}');
    if (_barrageList.length < 2) {
      addBarrage(child);
    } else {
      _barrageWidget.add(child);
    }
  }

  ///
  /// 添加弹幕
  ///
  addBarrage(Widget child, {Duration duration = _kDuration}) {
    print(
        'YBDBarrageState addBarrage _barrageList.length:${_barrageList.length} _barrageWidget.length:${_barrageWidget.length}');
    double perRowHeight = (_height - 2 * widget.padding) / widget.showCount;
    //计算距离顶部的偏移，
    // 不直接使用_barrageList.length的原因：弹幕结束会删除列表中此项，如果
    // 此时正好有一个弹幕来，会造成此弹幕和上一个弹幕同行
    var index = 0;
    if (_barrageList.length == 0) {
      //屏幕中没有弹幕，从顶部开始
      index = 0;
      barrageIndex++;
    } else {
      index = barrageIndex++;
    }
    var top2 = _computeTop(index, perRowHeight);
    var top;
    if ((barrageIndex % 2 == 0)) {
      top = perRowHeight + 2 * widget.padding;
    } else if ((barrageIndex % 3 == 0)) {
      top = perRowHeight * 2 + 2 * widget.padding;
    } else {
      top = 5.0;
    }
    print('barrageIndex :$barrageIndex top :$top');
    print('barrageIndex :$barrageIndex top :$top perRowHeight:$perRowHeight top2:$top2');
    if (barrageIndex > 100000) {
      //避免弹幕数量一直累加超过int的最大值
      barrageIndex = 0;
    }
    var bottom = _height - top - perRowHeight;
    //给每一项生成一个唯一id，用于删除
    String id = '${DateTime.now().toIso8601String()}:${_random.nextInt(1000)}';
    Duration _duration;
    if (child is YBDItembullet) {
      _duration = Duration(milliseconds: _getTime(child.data.content?.length ?? 1));
    } else {
      _duration = _kDuration;
    }
    var item = _YBDBarrageTransitionItem(
      id: id,
      top: top,
      bottom: bottom,
      child: child,
      onComplete: _onComplete,
      duration: _duration,
    );

    if (_barrageList.length >= 3) {
      print('_barrageListAll add');
      _barrageListAll.add(item);
    } else {
      print('_barrageList add');
      _barrageList.add(item);
    }
    print('_barrageList: ${_barrageList.length} ,_barrageListAll: ${_barrageListAll.length}');
    setState(() {});
  }

  int _getTime(int length) {
    if (length < 3) return 2000;
    if (length >= 3 && length <= 10) return 5000;
    if (length > 10 && length <= 15) return 6000;
    if (length > 15 && length <= 20) return 7000;
    if (length > 20 && length <= 25) return 8000;
    if (length > 25) return 9000;
    return 0;
  }
  void _getTime1dEtZoyelive(int length) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///
  /// 动画执行完毕删除
  ///
  _onComplete(id) {
    //动画执行完毕删除
    print('_barrageListAll----- ${_barrageListAll.length} _barrageList ${_barrageList.length}');
    _barrageList.removeAt(0);
    if (_barrageWidget.length > 0 && _barrageList.length < 3) {
      addBarrageWidget(_barrageWidget.removeAt(0));
      setState(() {});
    }
  }

  @override
  void initState() {
    _timer = Timer.periodic(Duration(milliseconds: 1000), (timer) {
      _barrageList.removeWhere((f) {
        print('_onComplete 0000002');
        return false;
      });
    });
    super.initState();
  }
  void initStateicB82oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraintType) {
        _height = constraintType.maxHeight;
        _width = constraintType.maxWidth;

        return ClipRRect(
          borderRadius: BorderRadius.circular(0),
          child: Stack(
            children: []..addAll(_barrageList),
          ),
        );
      },
    );
  }
  void buildRSX4hoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///
  /// 计算每一行距顶部的偏移，将显示弹幕分为[showCount]份，如果弹幕的比[showCount]多，
  /// 第二轮显示在2个【111111】中间，如下：
  ///
  /// 11111111111111
  ///             2222222
  /// 11111111111111
  ///           2222222
  /// 11111111111111
  ///           2222222
  /// 11111111111111
  ///
  _computeTop(int index, double perRowHeight) {
    //第几轮弹幕
    int num = (index / widget.showCount).floor();
    var top;
    top = (index % widget.showCount) * perRowHeight + widget.padding;

    if (num % 2 == 1 && index % widget.showCount != widget.showCount - 1) {
      //第二轮在第一轮2行弹幕中间
      top += perRowHeight / 2;
    }
    if (widget.randomOffset != 0 && top > widget.randomOffset) {
      top += _random.nextInt(widget.randomOffset) * 2 - widget.randomOffset;
    }
    return top;
  }

  @override
  void dispose() {
    _timer?.cancel();
    _barrageList.clear();
    super.dispose();
  }
  void disposesJ7WLoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class _YBDBarrageTransitionItem extends StatelessWidget {
  _YBDBarrageTransitionItem({this.id, this.top, this.bottom, this.child, this.onComplete, this.duration});

  final String? id;
  final double? top;
  final double? bottom;
  final Widget? child;
  final ValueChanged? onComplete;
  final Duration? duration;
  var _key = GlobalKey<YBDBarrageTransitionState>();

  bool get isComplete => _key.currentState!.isComplete;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top,
      bottom: bottom,
      child: YBDBarrageTransition(
        key: _key,
        child: child,
        onComplete: (v) {
          onComplete!(id);
        },
        duration: duration,
      ),
    );
  }
  void buildKqJw2oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

///
/// des:
///
/*
const Duration _kDuration = Duration(seconds: 5);

class YBDBarrage extends StatefulWidget {
  YBDBarrage({Key key, this.showCount = 3, this.padding = 5, this.randomOffset = 0}) : super(key: key);

  ///
  /// 显示的行数
  ///
  final int showCount;

  ///
  /// 水平弹幕：表示top、bottom的padding
  /// 垂直弹幕：表示left、right的padding
  ///
  final double padding;

  ///
  /// 随机偏移量
  ///
  final int randomOffset;

  @override
  State<StatefulWidget> createState() => YBDBarrageState();
}

class YBDBarrageState extends State<YBDBarrage> {
  ///
  /// 弹幕列表
  ///
  List<_YBDBarrageTransitionItem> _barrageList = [];

  /// 弹幕播放列表
  ///
  // List<_YBDBarrageTransitionItem> _barragePlayList = [];

  ///
  /// 定时清除弹幕
  ///
  Timer _timer;
  Random _random = Random();
  double _height;
  double _width;
  int barrageIndex = 0;

  ///
  /// 添加弹幕
  ///
  addBarrage(Widget child, {Duration duration = _kDuration}) {
    double perRowHeight = (_height - 2 * widget.padding) / widget.showCount;
    //计算距离顶部的偏移，
    // 不直接使用_barrageList.length的原因：弹幕结束会删除列表中此项，如果
    // 此时正好有一个弹幕来，会造成此弹幕和上一个弹幕同行
    var index = 0;
    if (_barrageList.length == 0) {
      //屏幕中没有弹幕，从顶部开始
      index = 0;
      barrageIndex++;
    } else {
      index = barrageIndex++;
    }
    var top = _computeTop(index, perRowHeight);
    if (barrageIndex > 100) {
      //避免弹幕数量一直累加超过int的最大值
      barrageIndex = 0;
    }
    var bottom = _height - top - perRowHeight;
    //给每一项生成一个唯一id，用于删除
    String id = '${DateTime.now().toIso8601String()}:${_random.nextInt(1000)}';
    var item = _YBDBarrageTransitionItem(
      id: id,
      top: top,
      bottom: bottom,
      child: child,
      onComplete: _onComplete,
      duration: duration,
    );
    _barrageList.add(item);

    // if (_barragePlayList.isEmpty) {
    //   _barragePlayList.add(item);
    // }
    setState(() {});
  }

  ///
  /// 动画执行完毕删除
  ///
  _onComplete(id) {
    //动画执行完毕删除
    _barrageList.removeWhere((f) {
      return f.id == id;
    });
    */
/* _barragePlayList.clear();
    if (_barrageList.length > 0) {
      setState(() {
        _barragePlayList.add(_barrageList[0]);
      });
    }*/ /*

  }

  @override
  void initState() {
    _timer = Timer.periodic(Duration(milliseconds: 1000), (timer) {
      _barrageList.removeWhere((f) {
        return false;
      });
    });
    super.initState();
  }
  void initState1JYVjoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraintType) {
        _height = constraintType.maxHeight;
        _width = constraintType.minWidth;

        return ClipRRect(
          borderRadius: BorderRadius.circular(2),
          child: Column(
            children: []..addAll(_barrageList),
            // children: []..addAll(_barragePlayList),
          ),
        );
      },
    );
  }

  ///
  /// 计算每一行距顶部的偏移，将显示弹幕分为[showCount]份，如果弹幕的比[showCount]多，
  /// 第二轮显示在2个【111111】中间，如下：
  ///
  /// 11111111111111
  ///             2222222
  /// 11111111111111
  ///           2222222
  /// 11111111111111
  ///           2222222
  /// 11111111111111
  ///
  _computeTop(int index, double perRowHeight) {
    //第几轮弹幕
    int num = (index / widget.showCount).floor();
    var top;
    top = (index % widget.showCount) * perRowHeight + widget.padding;

    if (num % 2 == 1 && index % widget.showCount != widget.showCount - 1) {
      //第二轮在第一轮2行弹幕中间
      top += perRowHeight / 2;
    }
    if (widget.randomOffset != 0 && top > widget.randomOffset) {
      top += _random.nextInt(widget.randomOffset) * 2 - widget.randomOffset;
    }
    return top;
  }

  @override
  void dispose() {
    _timer?.cancel();
    _barrageList.clear();
    super.dispose();
  }
  void disposezboSRoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class _YBDBarrageTransitionItem extends StatelessWidget {
  _YBDBarrageTransitionItem({this.id, this.top, this.bottom, this.child, this.onComplete, this.duration});

  final String id;
  final double top;
  final double bottom;
  final Widget child;
  final ValueChanged onComplete;
  final Duration duration;
  var _key = GlobalKey<YBDBarrageTransitionState>();

  bool get isComplete => _key.currentState.isComplete;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top,
      bottom: bottom,
      child: Container(
        child: YBDBarrageTransition(
          key: _key,
          child: Container(
            // width: ScreenUtil().setWidth(100),
            child: child,
          ),
          onComplete: (v) {
            onComplete(id);
          },
          duration: duration,
        ),
      ),
    );
  }
  void buildA0A97oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
*/
