
/*
 * @Author: William
 * @Date: 2022-05-07 11:11:07
 * @LastEditTime: 2022-06-08 18:06:12
 * @LastEditors: William
 * @Description: Personal Profile page, support multiple image display
 * @FilePath: /oyetalk-flutter/lib/ui/widget/profile_upload_bg.dart
 */
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:oyelive_main/common/constant/const.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/common/util/toast_ybd_util.dart';
import 'package:oyelive_main/module/api_ybd_helper.dart';
import 'package:oyelive_main/module/entity/base_ybd_resp_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/widget/bottom_ybd_selector_sheet.dart';
import 'package:oyelive_main/ui/widget/button/delay_ybd_gesture_detector.dart';
import 'package:oyelive_main/ui/widget/ybd_network_image.dart';

class YBDProfileUploadBgRow extends StatefulWidget {
  final List<String>? data;
  final Function? uploadCallback;
  final Function(String newKey)? replaceCallback;
  const YBDProfileUploadBgRow({Key? key, this.uploadCallback, this.replaceCallback, this.data}) : super(key: key);

  @override
  State<YBDProfileUploadBgRow> createState() => _YBDProfileUploadBgRowState();
}

class _YBDProfileUploadBgRowState extends State<YBDProfileUploadBgRow> {
  // 用户等级达到了10级后，就能上传1张图片,到了15级后，就能上传5张图片
  int _canUpload1Level = 10;
  int _canUpload5Level = 15;
  int _uploadTimes = 0;
  int _uploadHint = 10;
  bool _countCheck = false;
  bool _frequencyCheck = false;

  @override
  void initState() {
    super.initState();
    _setTimes();
    _queryFrequency(needToast: false);
  }
  void initStateDHRIvoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: Row(children: [..._picList(widget.data!), _uploadIcon()]),
      ),
    );
  }
  void buildjpiPaoyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 缩略图列表
  List<Widget> _picList(List<String> data) {
    if (data.length == 0) return [SizedBox(width: 0)];
    return List.generate(data.length, (i) => _thumbnail(i, data[i]));
  }
  void _picListmfSyaoyelive(List<String> data) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 上传图片的按钮
  Widget _uploadIcon() {
    _countCheck = widget.data!.length < _uploadTimes;
    if (widget.data!.length > 4) {
      return Container();
    }
    return YBDDelayGestureDetector(
      onTap: _clickUpload,
      child: Container(
        width: 115.px,
        height: 115.px,
        child: Stack(
          children: [
            Center(
              child: Container(
                width: 100.px,
                height: 100.px,
                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(8.px as double)),
                child: Center(child: Image.asset('assets/images/icon_camera.webp', width: 30.px, fit: BoxFit.fitWidth)),
              ),
            ),
            _countCheck && _frequencyCheck
                ? Container()
                : Positioned(
                    right: 0,
                    top: 0,
                    child: Image.asset('assets/images/icon_picture_lock@2x.webp', width: 30.px, fit: BoxFit.fitWidth))
          ],
        ),
      ),
    );
  }
  void _uploadIconsr6Vloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 点击上传背景图按钮的事件
  Future<void> _clickUpload() async {
    _countCheck = widget.data!.length < _uploadTimes;
    if (_countCheck) {
      await _queryFrequency();
      if (_frequencyCheck) {
        widget.uploadCallback!.call();
      } else {
        logger.v('22.5.7---upload frequency check deny!');
      }
    } else {
      logger.v('22.5.7---can not upload!');
      YBDToastUtil.toast("${translate('upload_img_limit')} $_uploadHint");
    }
  }
  void _clickUploadIlYpBoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 缩略图
  Widget _thumbnail(int index, String path) {
    return GestureDetector(
      onTap: () {
        _clickThumbnail(index);
      },
      child: Container(
        width: 115.px,
        height: 115.px,
        child: Center(
          child: Container(
            width: 100.px,
            height: 100.px,
            margin: EdgeInsets.symmetric(horizontal: 10.px as double),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white, width: 1.px as double),
              borderRadius: BorderRadius.circular(8.px as double),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.px as double),
              child: YBDNetworkImage(
                imageUrl: path,
                fit: BoxFit.cover,
                placeholder: (context, url) => YBDTPGlobal.bgImg(),
                errorWidget: (context, url, a) => YBDTPGlobal.bgImg(),
              ),
            ),
          ),
        ),
      ),
    );
  }
  void _thumbnailK2WMloyelive(int index, String path) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  // 点击缩略图的事件
  _clickThumbnail(int i) {
    logger.v('22.5.7---clickThumbnail:$i/${widget.data!.length}');
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return YBDBottomSelectorSheet(
            opts: [translate('remove'), translate('replace')],
            fitWidth: true,
            onSelectAt: (index) async {
              if (index == 0) {
                // 删除
                logger.v('22.5.7---clickThumbnail remove $i');
                YBDBaseRespEntity? res = await ApiHelper.deleteBg(context, widget.data![i].toString().getS3Key());
                if (res?.returnCode == Const.HTTP_SUCCESS) {
                  logger.v('22.5.17---delete success:${widget.data!.length}');
                  ApiHelper.checkLogin(context);
                }
                YBDToastUtil.toast(res?.returnMsg);
              }
              if (index == 1) {
                // 替换
                logger.v('22.5.7---clickThumbnail replace $i');
                await _queryFrequency();
                if (_frequencyCheck) {
                  widget.replaceCallback!.call(widget.data![i].toString().getS3Key());
                } else {
                  logger.v('22.5.7---replace frequency check deny!');
                }
              }
            },
          );
        });
  }

  // 上传背景图片频率检查
  _queryFrequency({bool needToast = true}) async {
    // 上传次数限制，1分钟上传超过5张，1天不得超过20张
    YBDBaseRespEntity? res = await ApiHelper.queryUploadBgRestrict(context);
    // frequencyCheck等于true表示当前上传不受限制，false表示当前上传受限制
    bool frequencyCheck = res?.returnCode == Const.HTTP_SUCCESS;
    if (!frequencyCheck && needToast) YBDToastUtil.toast(res?.returnMsg);
    setState(() {
      _frequencyCheck = frequencyCheck;
    });
  }

  // 设置初始上传图片次数
  void _setTimes() {
    int level = YBDUserUtil.getLoggedUser(context)?.level ?? 0;
    logger.v('22.5.11---level:$level');
    //todo--请求配置项获取_canUpload1Level和_canUpload5Level
    if (level >= _canUpload1Level) _uploadHint = 15;
    if (level < _canUpload1Level) {
      _uploadTimes = 0;
    } else if (level >= _canUpload1Level && level < _canUpload5Level) {
      _uploadTimes = 1;
    } else if (level >= _canUpload5Level) {
      _uploadTimes = 5;
    }
    // _countCheck等于true表示当前上传不受限制，false表示当前上传受限制
    _countCheck = widget.data!.length < _uploadTimes;
    logger.v('22.5.11---uploadTimes:$_uploadTimes');
  }
  void _setTimestGxHeoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
