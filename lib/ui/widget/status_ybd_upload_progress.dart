
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_translate/flutter_translate.dart';
import '../../common/util/log_ybd_util.dart';

/// 显示图片上传进度
class YBDStatusUploadProgress extends StatefulWidget {
  final int total;
  final String? current;
  Function? onCompelete;
  @override
  YBDStatusUploadState createState() => new YBDStatusUploadState();

  YBDStatusUploadProgress(this.current, this.total, {this.onCompelete});
}

class YBDStatusUploadState extends State<YBDStatusUploadProgress> {
  @override
  Widget build(BuildContext context) {
    if (widget.current == widget.total.toString()) widget.onCompelete?.call();
    return GestureDetector(
      onTap: () {
        logger.v("clicked the progress dialog bg");
//        Navigator.pop(context);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Center(
                    child: SizedBox(
                      width: ScreenUtil().setWidth(96),
                      height: ScreenUtil().setWidth(96),
                      child: CircularProgressIndicator(
                        strokeWidth: ScreenUtil().setWidth(4),
                        valueColor: AlwaysStoppedAnimation<Color>(Color(0xffB770FA)),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      padding: EdgeInsets.all(ScreenUtil().setWidth(24)),
                      child: Image.asset(
                        'assets/images/status/status_upload_progress.webp',
                        width: ScreenUtil().setWidth(54),
                        height: ScreenUtil().setWidth(54),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: ScreenUtil().setWidth(5),
              ),
              Container(
                width: 260,
                height: 40,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '${translate('uploading')}…',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.normal,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(10),
                    ),
                    Text(
                      '${widget.current}',
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(48),
                        fontWeight: FontWeight.bold,
                        color: Color(0xffCF4FF6),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: ScreenUtil().setWidth(14)),
                      child: Text(
                        '/${widget.total}',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(24),
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void buildhZb5Soyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
