
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class YBDLoadingCircle extends StatelessWidget {
  Color color;
  final height;

  YBDLoadingCircle({this.color: Colors.white, this.height});

  @override
  Widget build(BuildContext context) {
    final child = Center(
      child: SizedBox(
        width: ScreenUtil().setWidth(100),
        height: ScreenUtil().setWidth(100),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              width: ScreenUtil().setWidth(100),
              height: ScreenUtil().setWidth(100),
              child: CircularProgressIndicator(
                strokeWidth: ScreenUtil().setWidth(4),
                valueColor: AlwaysStoppedAnimation<Color>(Color(0xffB770FA)),
              ),
            ),
            Container(
              padding: EdgeInsets.all(ScreenUtil().setWidth(24)),
              child: Image.asset(
                'assets/images/status/status_upload_progress.webp',
                width: ScreenUtil().setWidth(54),
                height: ScreenUtil().setWidth(54),
                fit: BoxFit.contain,
              ),
            ),
          ],
        ),
      ),
    );

    // 添加高度
    if (null != height) {
      return Container(
        height: ScreenUtil().setWidth(height),
        child: YBDLoadingCircle(),
      );
    }

    return child;
  }
  void buildNFhAloyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
