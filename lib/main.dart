import 'dart:async';
import 'dart:io';

import 'package:event_bus/event_bus.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:flutter_translate/flutter_translate.dart';
// import 'package:flutter_translate/localization_provider.dart';
import 'package:get/get.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:package_info/package_info.dart';
import 'package:redux/redux.dart';
import 'package:sqlcool/sqlcool.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';
import 'package:oyelive_main/common/analytics/crashlytics_ybd_util.dart';
import 'package:oyelive_main/common/db_ybd_config.dart';
import 'package:oyelive_main/common/extend/observer_ybd_obs.dart';
import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/common/http/environment_ybd_config.dart';
import 'package:oyelive_main/common/navigator/flutter_ybd_boost_navigator.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/common/reload_ybd_sp_event.dart';
import 'package:oyelive_main/common/service/iap_service/iap_ybd_service.dart';
import 'package:oyelive_main/common/util/database_ybd_until.dart';
import 'package:oyelive_main/common/util/download_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/firebase_options.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';
import 'package:oyelive_main/redux/app_ybd_state.dart';
import 'package:oyelive_main/redux/init_ybd_middleware.dart';
import 'package:oyelive_main/redux/locale_ybd_redux.dart';
import 'package:oyelive_main/redux/user_ybd_redux.dart';
import 'package:oyelive_main/ui/page/activty/activity_ybd_file.dart';
import 'package:oyelive_main/ui/page/game_room/util/tp_ybd_cache.dart';
import 'package:oyelive_main/ui/page/room/service/live_ybd_service.dart';
import 'package:oyelive_main/ui/page/splash/splash_ybd_page.dart';
// import 'package:dart_ping_ios/dart_ping_ios.dart';
import 'common/constant/const.dart';
import 'common/util/remote_ybd_config_service.dart';
import 'common/util/sp_ybd_util.dart';
import 'redux/init_ybd_redux.dart';

// import 'package:http_proxy/http_proxy.dart';

final EventBus eventBus = EventBus();

void main() {
  // 调试app时显示基线和边框
  // debugPaintBaselinesEnabled = true;
  // debugPaintSizeEnabled = true;
  // debugPaintLayerBordersEnabled = true;
  YBDCrashlyticsUtil.instance!.listenIsolateError();
  runZonedGuarded(() async {
    ErrorWidget.builder = (FlutterErrorDetails details) {
      logger.e('ErrorWidget.builder exception: ${details.exceptionAsString()}, stack:${details.stack}');
      YBDCrashlyticsUtil.instance?.report(
        details.exception,
        stack: details.stack,
        reason: CrashlyticsReason.caughtZone,
      );
      return Center(
        child: Container(
          width: 60,
          height: 30,
          child: Center(
            child: Text("Error!"),
          ),
        ),
      );
    };

    var delegate = await LocalizationDelegate.create(
      fallbackLocale: Const.DEFAULT_LANG,
      supportedLocales: ["en", "ar", "ur", "hi", "tr", "id"],
    );

    /// 初始化下载控件
    WidgetsFlutterBinding.ensureInitialized();

    await FlutterDownloader.initialize(debug: false);

    try {
      await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
    } catch (e) {
      debugPrint('Firebase.initializeApp error: $e');
    }
    YBDCrashlyticsUtil.instance!.listenFlutterError();

    // 设置皮肤根路径
    await YBDActivityFile.instance!.setBasePath();

    initCommonDb(db: commonDb);
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
    if (userInfo != null) {
      messageDb = Db();
      initDb(db: messageDb!, userId: userInfo.id.toString());
    }

    // 数数埋点初始化
    TA.initThinkingDataState();
    // DartPingIOS.register();
    // 支持待定购买
    // InAppPurchaseConnection.enablePendingPurchases();

    //* 代理
    // if (Const.TEST_ENV) {
    //   WidgetsFlutterBinding.ensureInitialized();
    //   HttpProxy httpProxy = await HttpProxy.createHttpProxy();
    //   print('9.6---init--proxyHost:${httpProxy.host}---proxyPort:${httpProxy.port}');
    //   Const.PROXY_HOST = httpProxy.host ?? '';
    //   Const.PROXY_PORT = httpProxy.port ?? '';
    //   print('9.6---first--set--PROXY_HOST:${Const.PROXY_HOST}----PROXY_PORT:${Const.PROXY_PORT}');
    //   HttpOverrides.global = httpProxy;
    // }

    // 禁止横竖屏
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) async {
      runApp(LocalizedApp(delegate, MyApp()));
    });
  }, (Object error, StackTrace stackTrace) async {
    YBDCrashlyticsUtil.instance!.report(
      error,
      stack: stackTrace,
      reason: CrashlyticsReason.caughtZone,
    );
  });
}

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late StreamSubscription<List<PurchaseDetails>> _subscription;

  final store = Store<YBDAppState>(appReducer, middleware: [YBDInitMiddleware()], initialState: YBDAppState.initial());
  final router = FluroRouter();

  static FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  static FirebaseAnalyticsObserver observer = FirebaseAnalyticsObserver(analytics: analytics);
  static YBDRouteObserver obs = YBDRouteObserver();

  @override
  void initState() {
    super.initState();

    YBDFlutterBoostNavigator().initNavigator();
    YBDNavigatorHelper.configureRoutes(router);
    YBDTPGlobal.store = store;
    // YBDTPGlobal.launchTime = DateTime.now().millisecondsSinceEpoch;

    // 初始化Firebase Analytics; AppsFlyer
    YBDAnalyticsUtil.init(analytics, observer);

    // Enables performance monitoring
    FirebasePerformance.instance.setPerformanceCollectionEnabled(true);

    YBDDataBaseUtil.getInstance().initDataBase();

    // 初始化日志输出格式
    YBDLogUtil.init();

    store.dispatch(YBDInitAction());

    // 初始化直播服务
    YBDLiveService.instance.initConfig();

    // 初始化当前环境配置
    initConfig();

    store.dispatch(YBDInitAction());
    YBDRemoteConfigService.getInstance();

    eventBus.on<YBDReloadSpEvent>().listen((YBDReloadSpEvent event) async {
      switch (event.key) {
        case Const.SP_LOCALE:
          store.dispatch(YBDUpdateLocaleAction(localeFromString(event.value!)));
          break;
        case Const.SP_USER_INFO:
          store.dispatch(YBDUpdateUserAction(await YBDSPUtil.getUserInfo()));
          break;
      }
    });

    print('main bindBackgroundIsolate');
    YBDDownloadUtil.bindBackgroundIsolate();

    /// 存放APP版本名称
    PackageInfo.fromPlatform().then((value) {
      YBDTPGlobal.APP_VERSION = value.version;
    });

    Future.delayed(Duration(milliseconds: 200), () {
      Get.updateLocale(store.state.locale!);
    });

    if (Platform.isIOS) {
      // 监听支付队列，app启动时保存已完成的购买
      YBDIapService.instance.listenPurchaseQueue();
    }
  }
  void initStatehfL2xoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  void initConfig() async {
    YBDEnvConfig.currentSelectType = EnvTypeExt.getType((await YBDSPUtil.get(YBDEnvConfig.envKey)).toString());
    YBDTPCache.preCachePathInit();
  }
  void initConfigihySYoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  void dispose() {
    YBDDownloadUtil.unbindBackgroundIsolate();
    _subscription.cancel();
    super.dispose();
  }
  void disposeqKZI5oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @override
  Widget build(BuildContext context) {
    var localizationDelegate = LocalizedApp.of(context).delegate;
    return StoreProvider<YBDAppState>(
      store: store,
      child: StoreBuilder<YBDAppState>(
        builder: (context, store) {
          return LocalizationProvider(
            state: LocalizationProvider.of(context).state,
            child: GetMaterialApp(
              navigatorKey: navigatorKey,
              // 隐藏 debug 标签
              // debugShowCheckedModeBanner: false,
              theme: store.state.themeData ?? YBDTPGlobal.DEFAULT_THEME_DATA,
              locale: store.state.locale,
              onGenerateRoute: router.generator,
              home: YBDSplashPage(),
              localizationsDelegates: [
                localizationDelegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: localizationDelegate.supportedLocales,
              // checkerboardOffscreenLayers: true,
              // checkerboardRasterCacheImages: true,
              // showPerformanceOverlay: true,
              navigatorObservers: <NavigatorObserver>[observer, obs, FlutterSmartDialog.observer],
              builder: FlutterSmartDialog.init(builder: (context, widget) {
                return MediaQuery(
                  ///设置文字大小不随系统设置改变
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                  child: widget!,
                );
              }),
            ),
          );
        },
      ),
    );
  }
  void buildauto5oyelive(BuildContext context) {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
