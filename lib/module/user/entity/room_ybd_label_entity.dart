

import '../../../generated/json/base/json_convert_content.dart';

class YBDRoomLabelEntity with JsonConvert<YBDRoomLabelEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDRoomLabelRecord?>? record;
}

class YBDRoomLabelRecord with JsonConvert<YBDRoomLabelRecord> {
  int? id;
  String? name;
  String? img;
  int? parent;
  int? type;
  int? display;
  int? index;
}
