

/*
 * @Date: 2020-12-03 10:31:13
 * @LastEditors: William
 * @LastEditTime: 2022-05-18 14:55:51
 * @FilePath: /oyetalk-flutter/lib/module/user/entity/user_info_entity.dart
 */
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/image_ybd_util.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/base/json_field.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';
import 'package:oyelive_main/ui/page/login/handler/login_ybd_handler.dart';
import 'package:oyelive_main/ui/page/profile/setting/setting_ybd_item.dart';

class YBDUserInfo with JsonConvert<YBDUserInfo> {
  int? id;

  /// 可能为第三方用户名，手机号，id
  String? account;
  int? agency;
  String? email;
  String? nickname;
  String? password;
  int? status;
  String? headimg;
  int? money;
  int? gems;
  int? golds;
  int? level;
  int? experience;
  int? roomlevel;
  int? roomexper;
  int? vip;
  String? privateword;
  String? notice;
  String? roomimg;
  int? concern;
  int? fans;
  int? friends;
  bool? friend; // 是否为好友
  int? birthday;

  int? vipDailyCheckRewardBeans;
  String? vipIcon;

  /// 0.unknown, 1.female, 2.male
  int? sex;
  String? comment;
  int? classify;
  int? loginType;
  String? tenant;
  String? country;
  String? spLocal;
  String? portalType;
  List<YBDUserTag?>? tags;
  List<YBDSnsInfo?>? snsInfos;
  int? count;
  bool? live;

  /// 2.4.12：后台逻辑cellphone 是个备注字段，由account同步；account 可用于登录
  String? cellphone;
  String? uniqueNum;
  YBDExtendVo? extendVo;
  List<YBDBadgeInfo?>? badges;
  YBDBackgroundInfo? backgrounds;
  YBDEquipGoods? equipGoods;

  String? headFrame;

  /// 拷贝用户信息
  static YBDUserInfo? copyWithUserInfo(YBDUserInfo value) {
    if (null == value) {
      logger.v('userInfo value is null');
      return null;
    }

    try {
      return JsonConvert.fromJsonAsT<YBDUserInfo>(value.toJson());
    } catch (e) {
      logger.v('parse userInfo error : $e');
      return null;
    }
  }

  // Whether a third-party account is bound
  YBDSnsInfo? snsLinkInfo(LoginType type) {
    try {
      var model = snsInfos!.firstWhere((element) => element!.type == YBDSettingItem.loginTypeToInt(type));
      return model;
    } catch (e) {
      return null;
    }
  }

  // 是否绑定手机号
  bool isBindPhone() {
    var bindPhone = this.snsLinkInfo(LoginType.phone)?.snsId;
    return !(bindPhone == null || bindPhone.isEmpty);
  }
  void isBindPhoneXslRDoyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 头像地址
  String avatarURL() {
    return YBDImageUtil.avatar(Get.context, this.headimg, this.id, scene: 'B');
  }
  void avatarURLFvbyloyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}

class YBDEquipGoods with JsonConvert<YBDEquipGoods> {
  @JSONField(name: "CARD_FRAME")
  YBDCardFrame? cardFrame;
}

class YBDCardFrame with JsonConvert<YBDCardFrame> {
  String? image;
  String? animation;
}

class YBDUserTag with JsonConvert<YBDUserTag> {
  String? name;
  String? value;
  String? expireAt;
}

class YBDSnsInfo with JsonConvert<YBDSnsInfo> {
  int? id;
  int? userId;
  int? type;
  dynamic imei;
  int? createTime;
  String? nickname;
  String? snsId; //第三方Id，或手机号
}

class YBDBadgeInfo with JsonConvert<YBDBadgeInfo> {
  String? badgeCode;
  String? icon;
  String? iconPng;
  int? userId;
}

class YBDBackgroundInfo with JsonConvert<YBDBackgroundInfo> {
  String? host;
  List<YBDBgInfoRes?>? resources;

  List<String> get bgs {
    if (host == null || resources == null) return [];
    List<String> paths = [];
    for (var res in resources!) paths.add(host! + res!.resource!);
    return paths;
  }
}

class YBDBgInfoRes with JsonConvert<YBDBgInfoRes> {
  String? resource;
}
