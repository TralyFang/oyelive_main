

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

class YBDLoginResp with JsonConvert<YBDLoginResp> {
  YBDLoginRecord? record;
  String? returnCode;
  String? returnMsg;
}

class YBDLoginRecord with JsonConvert<YBDLoginRecord> {
  YBDUserInfo? userInfo;
  String? randomKey;
  int? seed;
  bool? newUser;
  String? type;
  String? title;
  String? content;
  String? button;
  String? extend;
}
