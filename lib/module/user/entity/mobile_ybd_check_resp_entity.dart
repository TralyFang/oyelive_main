

import '../../../generated/json/base/json_convert_content.dart';

class YBDMobileCheckResp with JsonConvert<YBDMobileCheckResp> {
  String? returnCode;
  String? returnMsg;
  YBDMobileCheckRecord? record;
}

class YBDMobileCheckRecord with JsonConvert<YBDMobileCheckRecord> {
  int? tokenId;
}
