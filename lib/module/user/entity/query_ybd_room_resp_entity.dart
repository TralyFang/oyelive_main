

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

class YBDQueryRoomRespEntity with JsonConvert<YBDQueryRoomRespEntity> {
  String? returnCode;
  String? returnMsg;
  YBDRoomInfo? record;
  // String recordSum;
}
