

import '../../../generated/json/base/json_convert_content.dart';

class YBDQueryMyInvitesRespEntity with JsonConvert<YBDQueryMyInvitesRespEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDInviteeInfo?>? record;
}

class YBDInviteeInfo with JsonConvert<YBDInviteeInfo> {
  int? inviter; // 邀请者ID
  int? invitee; // 被邀请者ID
  String? sharePlatform; // 邀请平台
  String? inviteeAvatar; // 被邀请者头像
  String? inviteeNickname; // 被邀请者昵称
  int? inviteeLevel; // 被邀请者等级
  int? inviteeRegisterTime; // 被邀请者注册时间
  int? inviteeSex; // 被邀请性别
  bool? follow; // 是否已经关注被邀请者
}
