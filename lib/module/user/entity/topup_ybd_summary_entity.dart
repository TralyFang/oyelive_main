

import '../../../generated/json/base/json_convert_content.dart';

class YBDTopUpSummaryEntity with JsonConvert<YBDTopUpSummaryEntity> {
  String? returnCode;
  String? returnMsg;
  YBDTopUpSummaryRecord? record;
}

class YBDTopUpSummaryRecord with JsonConvert<YBDTopUpSummaryRecord> {
  int? userId;
  bool? recharge;
}
