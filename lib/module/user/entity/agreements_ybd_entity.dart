

import '../../../generated/json/base/json_convert_content.dart';

class YBDAgreementsEntity with JsonConvert<YBDAgreementsEntity> {
	String? code;
	YBDAgreementsData? data;
}

class YBDAgreementsData with JsonConvert<YBDAgreementsData> {
	String? status;
}
