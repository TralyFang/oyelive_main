

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDAccountDeleteRespEntity with JsonConvert<YBDAccountDeleteRespEntity> {
	String? returnCode;
	String? returnMsg;
	YBDAccountDeleteRespRecord? record;
}

class YBDAccountDeleteRespRecord with JsonConvert<YBDAccountDeleteRespRecord> {
	String? message; // 错误信息
	String? status; // pending/approved/rejected
}
