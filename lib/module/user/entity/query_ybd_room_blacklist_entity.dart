

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

class YBDQueryRoomBlacklistRespEntity with JsonConvert<YBDQueryRoomBlacklistRespEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDRoomBlacklist?>? record;
  String? recordSum;
}

class YBDRoomBlacklist with JsonConvert<YBDRoomBlacklist> {
  int? id;
  int? roomid;
  YBDOperator? operator;
  YBDUserInfo? user;
  int? type;
  int? createtime;
  int? validdate;
}

class YBDOperator with JsonConvert<YBDOperator> {
  int? id;
  int? agency;
  int? crest;
  String? nickname;
  String? headimg;
  int? level;
  int? roomlevel;
  int? vip;
  int? sex;
}
