


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

class YBDQueryUserRespEntity with JsonConvert<YBDQueryUserRespEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDUserInfo?>? record;
  String? recordSum;
}
