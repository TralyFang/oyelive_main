

import 'package:sqlcool/sqlcool.dart';
import 'package:oyelive_main/common/util/log_ybd_util.dart';

import '../../../common/db_ybd_config.dart';
import '../../../common/util/common_ybd_util.dart';

/// 保存搜索历史记录的表，在[YBDSearchHistoryModel]的基础上添加了[userId]字段
class YBDSearchHistoryModelV2 with DbModel {
  static final searchHistoryTableNameV2 = "t_search_history_v2";
  final searchHistoryTableV2 = DbTable(searchHistoryTableNameV2)
    ..varchar("search_key", nullable: true)
    ..varchar("time")
    ..varchar('user_id');

  /// 搜索关键字
  String? searchKey;

  /// 保存记录的时间
  int? time;

  /// 用户 id
  String? userId;

  YBDSearchHistoryModelV2({
    this.id,
    this.searchKey,
    this.time,
    this.userId,
  });

  @override
  int? id;

  @override
  Db get db => commonDb;

  @override
  DbTable get table => searchHistoryTableV2;

  @override
  YBDSearchHistoryModelV2 fromDb(Map<String, dynamic> map) {
    final history = YBDSearchHistoryModelV2(
      id: map["id"] as int?,
      userId: map['user_id'] as String?,
      searchKey: map["search_key"] as String?,
      time: YBDCommonUtil.getIntFormMap(map["time"]),
    );

    return history;
  }

  @override
  Map<String, dynamic> toDb() {
    final row = <String, dynamic>{
      "search_key": searchKey,
      "time": time,
      'user_id': '$userId',
    };

    // 参考文档里没有添加 id 的语句 https://pub.dev/packages/sqlcool
    if (id != null) row.addAll({"id": id});

    // 删掉 map 里的空值
    row.removeWhere((key, value) => value == null);
    return row;
  }

  /// 插入搜索记录
  Future<void> insert() async {
    logger.v('===begin===');
    // 删除相同的搜索记录
    await sqlDelete(
      where: "search_key=\'${this.searchKey}\' and user_id=\'${this.userId}\'",
      verbose: true,
    ).catchError((dynamic e) {
      logger.e('insert $searchKey, id=$userId error: $e');
    });

    // 将模型插入数据库
    await sqlInsert(verbose: true).catchError((dynamic e) {
      logger.e('insert model error: $e');
    });

    List<dynamic> history = await sqlSelect(
      orderBy: "time DESC",
      limit: 100,
      offset: 10,
      verbose: true,
    ).catchError((dynamic e) {
      logger.e('select history id=$userId error: $e');
    });

    if (history != null && history.isNotEmpty) {
      history.forEach((element) {
        // 删除相同的搜索记录
        sqlDelete(
          where: "search_key=\'${element.searchKey}\' and user_id=\'${this.userId}\'",
          verbose: true,
        ).catchError((dynamic e) {
          logger.e('history delete ${element?.searchKey} userId= $userId error: $e');
        });
      });
    }
    logger.v('===end===');
  }
  void insert62bv7oyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
