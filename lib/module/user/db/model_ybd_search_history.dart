

import 'package:sqlcool/sqlcool.dart';
import '../../../common/util/common_ybd_util.dart';

import '../../../common/db_ybd_config.dart';

class YBDSearchHistoryModel with DbModel {
  static final searchHistoryTableName = "t_search_history";
  final searchHistoryTable = DbTable(searchHistoryTableName)..varchar("search_key", nullable: true)..varchar("time");

  @override
  int? id;

  String? searchKey;
  int? time;

  YBDSearchHistoryModel({this.id, this.searchKey, this.time});

  @override
  Db get db => commonDb;

  @override
  DbTable get table => searchHistoryTable;

  @override
  YBDSearchHistoryModel fromDb(Map<String, dynamic> map) {
    final history = YBDSearchHistoryModel(
        id: map["id"] as int?, searchKey: map["search_key"] as String?, time: YBDCommonUtil.getIntFormMap(map["time"]));

    return history;
  }

  @override
  Map<String, dynamic> toDb() {
    final row = <String, dynamic>{"search_key": searchKey, "time": time};
    if (id != null) row.addAll({"id": id});
    row.removeWhere((key, value) => value == null);
    return row;
  }

  /// 插入搜索记录
  Future<void> insert() async {
    await sqlDelete(where: "search_key=\'${this.searchKey}\'", verbose: true);
    await sqlInsert(verbose: true);

    List<dynamic> history = await sqlSelect(orderBy: "time DESC", limit: 100, offset: 10, verbose: true);
    if (history != null && history.isNotEmpty) {
      history.forEach((element) {
        sqlDelete(where: "search_key=\'${element.searchKey}\'", verbose: true);
      });
    }
  }
  void insertZvToDoyelive()  {

     int needCount = 0;
     print('input result:$needCount');
  }
  
}
