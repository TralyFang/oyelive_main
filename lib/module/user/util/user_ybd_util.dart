

import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';

import '../../../common/constant/const.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/date_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import '../../../common/util/sp_ybd_util.dart';
import '../../../redux/app_ybd_state.dart';
import '../../../redux/user_ybd_redux.dart';
import '../../api_ybd_helper.dart';
import '../entity/topup_ybd_summary_entity.dart';
import '../entity/user_ybd_info_entity.dart';

enum Relationship { SELF, FRIEND, FOLLOWED, NONE }

/// 用户信息相关的工具类
class YBDUserUtil {
  /// 获取用户登录的 token
  static Future<String> userToken() async {
    String jsessionId = (await YBDSPUtil.get(Const.SP_JSESSION_ID)) ?? '';
    String cookieApp = (await YBDSPUtil.get(Const.SP_COOKIE_APP)) ?? '';
    String cookieCombine = '';

    // cookie 要判空
    if (cookieApp.isNotEmpty && jsessionId.isNotEmpty) {
      cookieCombine = 'JSESSIONID=$jsessionId;app=$cookieApp';
    }

    return cookieCombine;
  }

  /// 获取用户信息
  static Future<YBDUserInfo?> userInfo() async {
    String? spUser = await YBDSPUtil.get(Const.SP_USER_INFO);

    try {
      YBDUserInfo? bean;
      if (null != spUser && spUser.isNotEmpty) {
        bean = YBDUserInfo().fromJson(json.decode(spUser));
        return bean;
      } else {
        logger.v('sp user info is null');
        return null;
      }
    } catch (e) {
      logger.v('get user info error : $e');
      return null;
    }
  }

  /// 获取用户 id
  static Future<String?> userId() async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();

    if (null != userInfo && null != userInfo.id) {
      return '${userInfo.id}';
    } else {
      logger.v('sp user id is null');
      return null;
    }
  }

  /// 返回用户 id 的 int 值
  static Future<int?> userIdInt() async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();

    if (null != userInfo && null != userInfo.id) {
      return userInfo.id;
    } else {
      logger.v('sp user id is null');
      return null;
    }
  }

  /// 先从store获取userId获，取不到再从sp取
  static Future<int?> id() async {
    // 先从store获取用户信息
    var userInfo = YBDCommonUtil.storeFromContext()?.state.bean;

    if (null != userInfo && null != userInfo.id) {
      // store中有用户id则返回
      return userInfo.id;
    }

    // store中的用户信息为空则从sp中获取
    userInfo = await YBDSPUtil.getUserInfo();

    if (null != userInfo && null != userInfo.id) {
      // sp中有用户id则返回
      return userInfo.id;
    }

    logger.v('user id is null');
    return null;
  }

  /// 获取用户头像
  static Future<String?> headImage(BuildContext context) async {
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();

    if (null != userInfo && null != userInfo.headimg) {
      return userInfo.headimg;
    } else {
      logger.v('head image is null');
      return null;
    }
  }

  /// 查询和用户关系
  static Relationship relationship(BuildContext context, YBDUserInfo _user) {
    if (_user == null) {
      return Relationship.NONE;
    } else if (_user.friend ?? false) {
      return Relationship.FRIEND;
    }

    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    if ((store.state.followedIds ?? Set()).contains(_user.id)) {
      return Relationship.FOLLOWED;
    } else {
      return Relationship.NONE;
    }
  }

  /// 从redux获取登录的用户信息
  /// 减少对SP的耗时操作
  static YBDUserInfo? getLoggedUser(BuildContext? context) {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    return store.state.bean;
  }

  /// 从redux获取登录的用户ID
  /// 减少对SP的耗时操作
  static int? getLoggedUserID(BuildContext? context) {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
    return store.state.bean?.id;
  }

  /// 更新国家信息后，刷新redux和SP
  static updateCountry(BuildContext? context, String? countryCode) async {
    Store<YBDAppState>? store = YBDCommonUtil.storeFromContext(context: context);
    YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();
    if (userInfo == null) return;

    userInfo.country = countryCode;
    store!.dispatch(YBDUpdateUserAction(userInfo));
    YBDSPUtil.save(Const.SP_USER_INFO, userInfo);
  }

  /// 更新 vip 信息
  static void updateVip(int vip) {
    logger.v('update vip : $vip');

    // 刷新 store
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext()!;
    final userInfo = store.state.bean!;
    userInfo.vip = vip;
    store.dispatch(YBDUpdateUserAction(userInfo));

    // 刷新 sp
    YBDSPUtil.save(Const.SP_USER_INFO, userInfo);
  }

  /// 是否为新用户
  static Future<bool> isNewUser() async {
    // 是否当日新注册用户
    String? spNewUsers = await YBDSPUtil.get(Const.SP_NEW_USER);
    String? userId = await YBDUserUtil.userId();
    Map<String, dynamic>? map = jsonDecode(spNewUsers ?? '{}');
    return userId != null && DateFormat('yyyy-MM-dd').format(DateTime.now()) == map![userId];
  }

  /// 是否当天首次打开 app
  static Future<bool> isTodayFirstOpenApp() async {
    String? userId = await YBDUserUtil.userId();

    // 从 sp 取当前用户第一次打开 app 的记录
    String? firstOpenStr = await YBDSPUtil.get(Const.SP_TODAY_FIRST_OPEN_APP);
    logger.v('get firstOpenStr string : $firstOpenStr');

    if (null == firstOpenStr) {
      logger.v('get firstOpenStr is null');

      // 没有记录默认未登录
      return true;
    }

    try {
      Map<String, dynamic> map = jsonDecode(firstOpenStr);
      logger.v('get firstOpenStr with userId : $userId : $firstOpenStr');

      // 没有当前用户的记录默认未登录
      return map[userId!] ?? true;
    } catch (e) {
      logger.v('get firstOpenStr error : $e');
      return false;
    }
  }

  /// 检查当前用否是否为当天首次打开 app，并保存检查结果
  /// 在首页调用
  /// [_getOpenAppTime] 取上次打开 app 的时间
  /// [_isTodayFirstLoginUser] 根据取上次打开 app 的时间判断是否为当日第一次打开 app
  /// 保存检查结果
  /// [_saveOpenAppTime] 保存本次打开 app 的时间
  static Future<void> checkIsTodayFirstOpenApp() async {
    // 从 sp 取当前用户的登录时间
    int loginDate = await _getOpenAppTime();

    logger.v('_getOpenAppTime loginDate : $loginDate');

    // 检查是否为当天第一次打开 app
    logger.v('_isTodayFirstLoginUser : $loginDate');
    bool isTodayOpen = await _isTodayFirstLoginUser(loginDate);

    logger.v('_getOpenAppTime isTodayOpen : $isTodayOpen, $loginDate');

    // 根据用户 id 保存登录记录
    String userId = (await YBDUserUtil.userId()) ?? '-1';

    // 已保存的用户是否登录的记录
    String? firstOpenStr = await YBDSPUtil.get(Const.SP_TODAY_FIRST_OPEN_APP);
    logger.v('jsonDecode firstOpenStr firstOpenStr : $firstOpenStr');

    Map<String, dynamic> map = {};

    try {
      map = jsonDecode(firstOpenStr ?? '{}');
      map.addAll({userId: isTodayOpen});

      // 保存检查结果
      await YBDSPUtil.save(Const.SP_TODAY_FIRST_OPEN_APP, jsonEncode(map));
      logger.v('jsonDecode firstOpenStr save : ${jsonEncode(map)}');
    } catch (e) {
      logger.v('jsonDecode firstOpenStr error : $e, $firstOpenStr');
    }

    // 保存本次打开 app 的时间
    await _saveOpenAppTime();
  }

  /// 是否为当日首次打开 app
  /// true: 当日首次打开，false：当日已经打开过
  static Future<bool> _isTodayFirstLoginUser(int loginDate) async {
    if (0 == loginDate) {
      logger.v('sp open app time is 0');
      // 返回 0 表示用户之前没有登录过
      return true;
    }

    if (null == loginDate) {
      logger.v('sp open app time is null');
      // 没有记录到用户的登录时间, 默认为第一次登录
      return true;
    }

    // 判断 [loginDate] 与当前时间是否在同一天
    bool isToday = YBDDateUtil.sameDateFromMilliseconds(loginDate);

    // 登录日期与当前日期为同一天
    if (isToday) {
      logger.v('is not first login user : $loginDate');
      return false;
    } else {
      logger.v('is first login user : : $loginDate');
      return true;
    }
  }

  /// 保存当前用户本次打开 app 的时间
  static _saveOpenAppTime() async {
    String? userId = await YBDUserUtil.userId();

    if (null == userId) {
      logger.v('userId is null');
      return;
    }

    int date = DateTime.now().millisecondsSinceEpoch;

    // 已保存的用户登录时间
    String? userLoginTime = await YBDSPUtil.get(Const.SP_USER_OPEN_APP_TIME);
    Map<String, dynamic>? map = {};

    try {
      map = jsonDecode(userLoginTime ?? '{}');
      map!.addAll({userId: date});
      await YBDSPUtil.save(Const.SP_USER_OPEN_APP_TIME, jsonEncode(map));
      logger.v('jsonDecode SP_USER_OPEN_APP_TIME save : ${jsonEncode(map)}');
    } catch (e) {
      logger.v('jsonDecode _saveOpenAppTime error : $e, $userLoginTime');
    }
  }

  /// 获取当前用户上次登录的时间
  /// 返回 null 表示获取错误
  /// 返回 0 表示用户之前没有登录过
  static Future<int> _getOpenAppTime() async {
    // 获取对应用户的登录时间
    String? userId = await YBDUserUtil.userId();

    if (null == userId) {
      logger.v('userId is null');
      return 0;
    }

    // 从 sp 取用户登录时间的字符串
    String? userLoginTime = await YBDSPUtil.get(Const.SP_USER_OPEN_APP_TIME);

    if (null == userLoginTime) {
      logger.v('userLoginTime is null');
      return 0;
    }

    try {
      // 用户登录时间转 map
      Map<String, dynamic> map = jsonDecode(userLoginTime);
      logger.v('get userLoginTime map : $map');
      // 取对应的用户登录时间
      return map[userId] ?? 0;
    } catch (e) {
      logger.v('get userLoginTime error : $e');
      return 0;
    }
  }

  /// 检查当前用否是否为当天首次打开安全弹框
  static Future<bool> todayFirstOpenSafeAlert() async {
    // 取上次打开安全弹框的时间
    int lastOpenDate = await _lastSafeAlertTime(Const.SP_USER_OPEN_SAFE_ALERT_TIME);
    logger.v('_lastSafeAlertTime : $lastOpenDate');

    // 检查是否为当天
    bool isTodayOpen = await _isTodayFirstLoginUser(lastOpenDate);
    logger.v('_lastSafeAlertTime isTodayOpen : $isTodayOpen, $lastOpenDate');
    return isTodayOpen;
  }

  /// 获取当前用户上次打开安全弹框的时间
  /// 返回 0 表示用户之前没有登录过
  /// Const.SP_USER_OPEN_SAFE_ALERT_TIME
  static Future<int> _lastSafeAlertTime(String spKey) async {
    String? userId = await YBDUserUtil.userId();

    // 从 sp 取用户上次打开安全弹框时间的字符串
    String? lastOpenTime = await YBDSPUtil.get(spKey);

    if (null == lastOpenTime) {
      logger.v('lastOpenTime is null');
      return 0;
    }

    try {
      // 转 map
      Map<String, dynamic> map = jsonDecode(lastOpenTime);
      logger.v('get lastOpenTime map : $map');
      // 取对应的用户打开安全弹框时间
      return map['$userId'] ?? 0;
    } catch (e) {
      logger.v('get lastOpenTime error : $e');
      return 0;
    }
  }

  /// 保存当前用户打开安全弹框的时间
  /// 在显示安全弹框后调用
  /// Const.SP_USER_OPEN_SAFE_ALERT_TIME
  static saveSafeAlertTime(String spKey) async {
    String? userId = await YBDUserUtil.userId();

    if (null == userId) {
      logger.v('userId is null');
      return;
    }

    int date = DateTime.now().millisecondsSinceEpoch;

    // 已保存的用户打开安全弹框的时间
    String? lastTime = await YBDSPUtil.get(spKey);
    Map<String, dynamic>? map = {};

    try {
      map = jsonDecode(lastTime ?? '{}');
      map!.addAll({userId: date});
      await YBDSPUtil.save(spKey, jsonEncode(map));
      logger.v('jsonDecode $spKey save : ${jsonEncode(map)}');
    } catch (e) {
      logger.v('jsonDecode _saveOpenSafeAlertTime error : $e, $lastTime');
    }
  }

  /// 检查是否充值用户
  static Future<bool> isToppedUpUser(BuildContext context) async {
    YBDTopUpSummaryEntity? result = await ApiHelper.queryTopUpSummary(context);
    bool isToppedUpUser = null != result?.record?.recharge && result!.record!.recharge!;
    return isToppedUpUser;
  }

  /// 更新 用户等级 信息
  static void updateUserLevel(int? userLevel) {
    logger.v('updateUserLevel userLevel : $userLevel');

    // 刷新 store
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext()!;
    final userInfo = store.state.bean!;
    userInfo.level = userLevel;
    store.dispatch(YBDUpdateUserAction(userInfo));

    // 刷新 sp
    YBDSPUtil.save(Const.SP_USER_INFO, userInfo);
  }

  /// teenpatti 赢钱更新余额
  static void updateUserMoney(int moeny) {
    logger.v('updateUserMoney userMoney : $moeny');

    // 刷新 store
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext()!;
    final userInfo = store.state.bean!;
    userInfo.money = moeny;
    store.dispatch(YBDUpdateUserAction(userInfo));

    // 刷新 sp
    YBDSPUtil.save(Const.SP_USER_INFO, userInfo);
  }

  static int? get getUserMoneySync {
    return getUserInfoSync.money;
  }

  static int? get getUserIdSync {
    return getUserInfoSync.id;
  }

  static YBDUserInfo get getUserInfoSync {
    Store<YBDAppState>? store = YBDCommonUtil.storeFromContext();
    final YBDUserInfo userInfo = store?.state?.bean ?? YBDUserInfo();
    return userInfo;
  }

  /// 检查用户是否登录
  /// true: 已登录，false：未登录
  static Future<bool> isUserLoggedIn() async {
    var userInfo = YBDCommonUtil
        .storeFromContext()
        ?.state
        .bean;
    if (null != userInfo?.id && userInfo!.id! > 0) {
      // store中有用户信息表示已经登录
      return true;
    }

    userInfo = await YBDSPUtil.getUserInfo();
    if (null != userInfo?.id && userInfo!.id! > 0) {
      // sp中有用户信息表示已经登录
      return true;
    }

    // store和sp中都没有用户信息时表示没有登录
    return false;
  }
  /// 时间戳转年龄
  static String calculateAge(int? timestamp) {
    if (null == timestamp) {
      logger.v('time stamp is empty');
      return '0';
    }

    DateTime birthDate = DateTime.fromMillisecondsSinceEpoch(timestamp);
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age.toString();
  }

  /// 是否是自己
  static bool isCurrent(int userId) {
    return getUserIdSync == userId;
  }
}
