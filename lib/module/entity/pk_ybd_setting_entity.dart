

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDPkSettingEntity with JsonConvert<YBDPkSettingEntity> {
  String? returnCode;
  String? returnMsg;
  YBDPkSettingRecord? record;
}

class YBDPkSettingRecord with JsonConvert<YBDPkSettingRecord> {
  bool? rejectFriends;
  bool? rejectSystem;
  dynamic timeZone;
}
