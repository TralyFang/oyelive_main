

import '../../generated/json/base/json_convert_content.dart';

class YBDGameInfoEntity with JsonConvert<YBDGameInfoEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDGameInfoRecord?>? record;
  String? recordSum;
}

class YBDGameInfoRecord with JsonConvert<YBDGameInfoRecord> {
  int? id;
  String? name;
  String? icon;
  String? url;
  int? shieldStatus;
}
