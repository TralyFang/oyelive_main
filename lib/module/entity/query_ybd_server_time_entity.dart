

/*
 * @Author: William
 * @Date: 2022-09-15 14:52:16
 * @LastEditTime: 2022-09-15 15:10:42
 * @LastEditors: William
 * @Description: YBDServerTimeEntity
 * @FilePath: /oyetalk-flutter/lib/module/entity/query_server_time_entity.dart
 */
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDServerTimeEntity with JsonConvert<YBDServerTimeEntity> {
  String? returnCode;
  String? returnMsg;
  int? record;
}

