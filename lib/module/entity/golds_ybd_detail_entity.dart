

/*
 * @Author: William-Zhou
 * @Date: 2021-11-26 15:57:17
 * @LastEditTime: 2021-11-26 15:57:17
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDGoldsDetailEntity with JsonConvert<YBDGoldsDetailEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDGoldsDetailRecord?>? record;
  String? recordSum;
}

class YBDGoldsDetailRecord with JsonConvert<YBDGoldsDetailRecord> {
  int? createTime;
  int? golds;
  String? type;
  int? userId;
}