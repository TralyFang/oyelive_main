

import '../../generated/json/base/json_convert_content.dart';

class YBDInviteInfoEntity with JsonConvert<YBDInviteInfoEntity> {
  String? returnCode;
  String? returnMsg;
  String? record;
}
