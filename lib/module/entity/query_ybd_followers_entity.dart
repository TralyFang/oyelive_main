


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

class YBDQueryFollowersEntity with JsonConvert<YBDQueryFollowersEntity> {
	String? returnCode;
	String? returnMsg;
	YBDQueryFollowersRecord? record;
	String? recordSum;
}

class YBDQueryFollowersRecord with JsonConvert<YBDQueryFollowersRecord> {
	List<YBDUserInfo?>? users;
	List<int>? followed;
	dynamic fromid;
	dynamic toid;
}
