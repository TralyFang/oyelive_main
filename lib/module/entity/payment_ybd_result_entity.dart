

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDPaymentResultEntity with JsonConvert<YBDPaymentResultEntity> {
  YBDPayResultInfo? payResult;
}

class YBDPayResultInfo with JsonConvert<YBDPayResultInfo> {
  int? pay;
  int? userExp;
  int? money;
  int? earn;
}
