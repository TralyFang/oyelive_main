

import '../../generated/json/base/json_convert_content.dart';

class YBDBeanDetailEntity with JsonConvert<YBDBeanDetailEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDBeanDetailRecord?>? record;
  String? recordSum;
}

class YBDBeanDetailRecord with JsonConvert<YBDBeanDetailRecord> {
  int? createTime;
  int? beans;
  String? type;
  int? userId;
}
