

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDImageCodeEntity with JsonConvert<YBDImageCodeEntity> {
  String? returnCode;
  String? returnMsg;
  YBDImageCodeRecord? record;
}

class YBDImageCodeRecord with JsonConvert<YBDImageCodeRecord> {
  String? imageCode;
}
