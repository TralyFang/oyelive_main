

import '../../generated/json/base/json_convert_content.dart';

class YBDBuyVipResultEntity with JsonConvert<YBDBuyVipResultEntity> {
  String? returnCode;
  String? returnMsg;
  YBDBuyVipResultRecord? record;
}

class YBDBuyVipResultRecord with JsonConvert<YBDBuyVipResultRecord> {
  int? pay;
  int? userExp;
  int? money;
  int? earn;
}
