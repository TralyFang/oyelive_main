

import 'dart:convert';

import '../../generated/json/base/json_convert_content.dart';
import '../../generated/json/base/json_field.dart';
import '../../ui/page/profile/my_profile/ctrl/badge_ybd_color.dart';

class YBDQueryBadgeEntity with JsonConvert<YBDQueryBadgeEntity> {
	String? returnCode;
	String? returnMsg;
	YBDQueryBadgeRecord? record;
}

class YBDQueryBadgeRecord with JsonConvert<YBDQueryBadgeRecord> {
	List<YBDQueryBadgeRecordBadges?>? badges;
	List<YBDQueryBadgeRecordUserBadge?>? userBadge;
	//用& 分割 可能佩戴多个勋章的场景 2.0.3 版本只支持佩戴一个 但是其他的list返回的是数组
	String? wearBadge;
}

class YBDQueryBadgeRecordBadges with JsonConvert<YBDQueryBadgeRecordBadges> {
	int? id;
	String? badgeCode;
	String? type;
	String? title;
	String? context;
	String? icon;
	String? status;
	dynamic beginTime;
	dynamic endTime;
	String? business;
	dynamic extend;
	dynamic remarks;
	int? sort;
	int? version;
	int? createTime;
	int? updateTime;
	YBDQueryBadgeRecordBadgesSpeed? speed;
	@JSONField(name: "get")
	bool? xGet;
	// 是否获得这个勋章
	bool? isRecive = false;

	YBDBadgeColor get iconObj{
		if (icon == null){
			return YBDBadgeColor();
		}
		return YBDBadgeColor.get(jsonDecode(icon.toString()));
	}
}

class YBDQueryBadgeRecordBadgesSpeed with JsonConvert<YBDQueryBadgeRecordBadgesSpeed> {
	int? max;
	int? beans;
}

class YBDQueryBadgeRecordUserBadge with JsonConvert<YBDQueryBadgeRecordUserBadge> {
	String? orderId;
	String? name;
	String? channel;
	String? status;
}
