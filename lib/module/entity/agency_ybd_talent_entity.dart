

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDAgencyTalentEntity with JsonConvert<YBDAgencyTalentEntity> {
  String? code;
  YBDAgencyTalentData? data;
  bool? success;
}

class YBDAgencyTalentData with JsonConvert<YBDAgencyTalentData> {
  int? id;
  String? agencyId;
  int? talentId;
  String? registerTime;
  String? joinTime;
}
