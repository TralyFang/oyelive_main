

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDFirebaseLogEntity with JsonConvert<YBDFirebaseLogEntity> {
  /// 管理后台配置需要上传日志的开关
  bool? isOpen;

  /// 管理后台配置需要上传日志的用户id
  List<String>? userIds;

  /// 管理后台配置需要上传日志的app模块
  List<String>? modules;

  factory YBDFirebaseLogEntity.fromString(String configStr) {
    YBDFirebaseLogEntity? result = YBDFirebaseLogEntity(isOpen: false);

    try {
      result = JsonConvert.fromJsonAsT<YBDFirebaseLogEntity>(jsonDecode(configStr));
    } catch (e) {
      debugPrint('decode YBDFirebaseLogEntity from json error : $e');
    }

    return result!;
  }

  YBDFirebaseLogEntity({
    this.isOpen,
    this.userIds,
    this.modules,
  });
}
