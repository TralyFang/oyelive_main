

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/entity/card_ybd_frame_store_entity.dart';

class YBDCardFrameBagEntity with JsonConvert<YBDCardFrameBagEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDCardFrameBagRecord?>? record;
  String? recordSum;
}

class YBDCardFrameBagRecord with JsonConvert<YBDCardFrameBagRecord> {
  String? type;
  List<YBDCardFrameStoreRecord?>? itemList;
}
