

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDCardFrameStoreEntity with JsonConvert<YBDCardFrameStoreEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDCardFrameStoreRecord?>? record;
  String? recordSum;
}

class YBDCardFrameStoreRecord with JsonConvert<YBDCardFrameStoreRecord> {
  int? id;
  dynamic ids;
  String? name;
  int? status;
  int? label;
  int? index;
  int? price;
  String? image;
  String? animation;
  int? display;
  String? attribute;
  int? createTime;
  int? updateTime;
  String? currency;
  dynamic conditionType;
  dynamic conditionExtends;
  bool? modify;
  String? thumbnail;
  bool? equipped;
  int? expireAfter;
  int? personalId;
}
