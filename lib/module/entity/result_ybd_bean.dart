

import '../../generated/json/base/json_convert_content.dart';

class YBDResultBeanEntity with JsonConvert<YBDResultBeanEntity> {
  String? code;
  String? returnCode;
  String? returnMsg;
  dynamic record;
  dynamic data;
}
