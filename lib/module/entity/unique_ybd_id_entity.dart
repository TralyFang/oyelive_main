


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDUniqueIDEntity with JsonConvert<YBDUniqueIDEntity> {
  int? userId;
  int? uniqueId;
  String? validity;
}
