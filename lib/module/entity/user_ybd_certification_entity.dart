

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/base/json_field.dart';

class YBDUserCertificationEntity with JsonConvert<YBDUserCertificationEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDUserCertificationRecord?>? record;
}

class YBDUserCertificationRecord with JsonConvert<YBDUserCertificationRecord> {
  int? userId;
  List<YBDUserCertificationRecordCertificationInfos?>? certificationInfos;
}

class YBDUserCertificationRecordCertificationInfos with JsonConvert<YBDUserCertificationRecordCertificationInfos> {
  int? id;
  int? userId;
  dynamic userName;
  int? type;
  String? icon;
  String? title;
  int? hidUserLevel;
  int? hidTalentLevel;
  int? hidVip;
  int? hidBadge;
  int? hidGender;
  int? hidTalent;
  dynamic createTime;
  dynamic updateTime;
  @JSONField(name: "operator")
  dynamic xOperator;
  int? status;
  dynamic extend;
}
