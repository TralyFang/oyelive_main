

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDBubbleEntity with JsonConvert<YBDBubbleEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDBubbleRecord?>? record;
  String? recordSum;
}

class YBDBubbleRecord with JsonConvert<YBDBubbleRecord> {
  int? id;
  dynamic ids;
  String? name;
  int? status;
  int? label;
  int? index;
  int? price;
  String? image;
  String? animation;
  int? display;
  String? attribute;
  int? createTime;
  int? updateTime;
  String? currency;
  String? creator;
  String? updater;
  bool? modify;
  int? conditionType;
  String? conditionExtends;
}
