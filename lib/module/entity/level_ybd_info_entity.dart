

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDLevelInfoEntity with JsonConvert<YBDLevelInfoEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDLevelInfoRecord?>? record;
  String? recordSum;
}

class YBDLevelInfoRecord with JsonConvert<YBDLevelInfoRecord> {
  int? id;
  int? level;
  int? type;
  int? rights;
  dynamic experience;

  int? experienceInt() {
    if (experience != null) {
      return experience is String
          ? int.tryParse(experience)
          : experience.toInt();
    }
    return 0;
  }
}
