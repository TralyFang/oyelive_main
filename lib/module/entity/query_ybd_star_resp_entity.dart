

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

class YBDQueryStarRespEntity with JsonConvert<YBDQueryStarRespEntity> {
  String? returnCode;
  String? returnMsg;
  YBDQueryStarRespRecord? record;
  dynamic recordSum;
}

class YBDQueryStarRespRecord with JsonConvert<YBDQueryStarRespRecord> {
  String? name;
  int? dateType;
  dynamic labelId;
  List<YBDRoomInfo?>? rank;
  List<double>? scores;
}
