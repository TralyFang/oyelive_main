

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDUidListEntity with JsonConvert<YBDUidListEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDUidListRecord?>? record;
  String? recordSum;
}

class YBDUidListRecord with JsonConvert<YBDUidListRecord> {
  int? id;
  String? number;
  int? status;
  int? price;
  int? display;
  dynamic createtime;
  dynamic extend;
  int? stock;
  String? name;
  bool? modify;
  String? currency;
  int? conditionType;
  String? conditionExtends;
}
