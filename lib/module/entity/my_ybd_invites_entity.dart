

import '../../generated/json/base/json_convert_content.dart';

class YBDMyInvitesEntity with JsonConvert<YBDMyInvitesEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDMyInvitesRecord?>? record;
  String? recordSum;
}

class YBDMyInvitesRecord with JsonConvert<YBDMyInvitesRecord> {
  int? id;
  String? type;
  int? parentId;
  int? userId;
  int? counts;
  int? beans;
  int? rewards;
  int? parentRewards;
  int? version;
  dynamic extend;
  int? createTime;
  int? updateTime;
  dynamic maxId;
  dynamic supCounts;
  dynamic supRewards;
  dynamic beginTime;
  dynamic endTime;
  String? userHead;
  String? userName;
  bool? concern;
  int? page;
  int? pageSize;
}
