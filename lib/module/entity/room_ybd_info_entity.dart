

import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDRoomInfo with JsonConvert<YBDRoomInfo> {
  int? id;
  String? nickname;
  String? title;
  int? vip;
  int? level;
  int? roomlevel;
  String? headimg;
  String? roomimg;
  int? count; // if on live, viewers
  int? time; // go live time
  dynamic plat;
  dynamic comet;
  List<YBDRoomCategory?>? category;
  List<YBDRoomCategory?>? subcategory;
  int? screen;
  int? fans;
  int? sex;
  int? media;
  int? money;
  int? protectMode; //1 是密码房
  int? roomexper;
  List<YBDRoomTag?>? tags;
  bool? live;
  bool? friend;
  String? tag; // Room Tag: Friends , Party , Heart , etc.
  int? micRequestEnable; //  3级以下用户 不需要申请即可以上麦    0-不需要申请麦位，即可上麦    1-需要申请麦位

  YBDExtendVo? extendVo;

  int? activityRanking;

  bool? playingLudo;
  String? ludoIconUrl;

  /// 分享的房间提示
  String? roomMark;

  // 该房间是否在pk
  bool? pk;

  bool? isOnline;
  String? vipIcon;
  //fuck 志强
  int? get aRanking {
    return extendVo == null ? activityRanking : extendVo!.activityRanking;
  }

  //* um/queryconcern 和 rank/popular 接口的pk字段位置不一样
  bool? get isPking {
    return extendVo == null ? pk : extendVo!.pk;
  }

  String? get roomTitle {
    String? _result = "";
    if (title != null && title!.isNotEmpty) {
      if (title!.contains(",")) {
        List<String> parts = title!.split(",");
        _result = parts[1];
      } else {
        _result = title;
      }
    }
    return _result;
  }

  String get roomCategory {
    String _result = "Standard";
    String? _tag = "0";
    if (tag != null && tag!.isNotEmpty) {
      _tag = tag;
    } else if (title != null && title!.isNotEmpty && title!.contains(",")) {
      List<String> parts = title!.split(",");
      _tag = parts[0];
    }

    switch (_tag) {
      case "1":
        _result = "Special";
        break;
      case "2":
        _result = "Heart";
        break;
      case "3":
        _result = "Party";
        break;
      case "4":
        _result = "Friends";
        break;
      case "5":
        _result = "Exclusive";
        break;
      case "6":
        _result = "Royal";
        break;
      case "0":
        _result = "Standard";
        break;
    }

    return _result;
  }

  String? get roomCategoryIndex {
    String? _tag = "0";
    if (tag != null && tag!.isNotEmpty) {
      _tag = tag;
    } else if (title != null && title!.isNotEmpty && title!.contains(",")) {
      List<String> parts = title!.split(",");
      _tag = parts[0];
    }

    return _tag;
  }

  /// 是否为加密房间
  bool isProtectRoom() {
    if (null != protectMode && protectMode == 1) {
      logger.v('protect room: $id');
      return true;
    }

    return false;
  }
  void isProtectRoomHTUU4oyelive() {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 拷贝房间信息
  static YBDRoomInfo? copyWithRoomInfo(YBDRoomInfo value) {
    if (null == value) {
      logger.v('roomInfo value is null');
      return null;
    }

    try {
      return JsonConvert.fromJsonAsT<YBDRoomInfo>(value.toJson());
    } catch (e) {
      logger.v('parse roomInfo error : $e');
      return null;
    }
  }
}

class YBDRoomCategory with JsonConvert<YBDRoomCategory> {
  int? id;
  String? name;
}

class YBDRoomTag with JsonConvert<YBDRoomTag> {
  String? name;
  String? value;
  int? expireAt;
}

class YBDExtendVo with JsonConvert<YBDExtendVo> {
  int? activityRanking;
  bool? pk;
  bool? playingLudo;
  String? ludoIconUrl;
}
