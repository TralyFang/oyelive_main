

import 'dart:convert';

import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/base/json_field.dart';
import 'package:oyelive_main/module/entity/unique_ybd_id_entity.dart';

class YBDQueryConfigsRespEntity with JsonConvert<YBDQueryConfigsRespEntity> {
  String? returnCode;
  String? returnMsg;
  YBDConfigInfo? record;
}

/// 所有平台的版本配置信息
class YBDAppVersion with JsonConvert<YBDAppVersion> {
  YBDAppVersionConfig? iosConfig;
  YBDAppVersionConfig? androidConfig;
}

/// 单个平台的版本配置信息
class YBDAppVersionConfig with JsonConvert<YBDAppVersionConfig> {
  /// 是否强制更新
  bool? forceUpdate;

  /// 是否显示更新弹框
  /// 使用场景：iOS 更新最新版本的配置项显示游戏但不显示更新弹框
  bool? showDialog;

  /// 版本号
  String? versionCode;

  /// 构建版本号
  int? buildCode;

  /// 版本更新描述
  String? description;

  /// 平台支持的最小构建版本，小于最小版本时强制更新
  int? minVersion;

  /// 白名单，可以看到全部的内容
  List<int>? whiteList;
}

class YBDConfigInfo with JsonConvert<YBDConfigInfo> {
  @JSONField(name: "super_room_manager")
  String? superRoomManager;
  @JSONField(name: "medal_price")
  String? medalPrice;
  @JSONField(name: "app_location_portaltype")
  String? appLocationPortaltype;
  @JSONField(name: "youtube_channels_home")
  String? youtubeChannelsHome;
  @JSONField(name: "ads_count_limit")
  String? adsCountLimit;
  @JSONField(name: "ep_coin_price")
  String? epCoinPrice;
  @JSONField(name: "wifi_support")
  String? wifiSupport;
  @JSONField(name: "max_size_upload_file")
  String? maxSizeUploadFile;
  @JSONField(name: "report_content_user")
  String? reportContentUser;
  @JSONField(name: "app_ramadan_event")
  String? appRamadanEvent;
  @JSONField(name: "label_show_in_home")
  String? labelShowInHome;
  @JSONField(name: "app_invite_show")
  String? appInviteShow;
  @JSONField(name: "app_android")
  String? appAndroid;
  @JSONField(name: "app_version")
  String? appVersion;
  @JSONField(name: "payment_server_url")
  String? paymentServerUrl;
  @JSONField(name: "tlevel_privilege_mic_10")
  String? tlevelPrivilegeMic10;
  @JSONField(name: "youtube_key")
  String? youtubeKey;
  @JSONField(name: "app_level_gift")
  String? appLevelGift;
  @JSONField(name: "path_share_image")
  String? pathShareImage;
  @JSONField(name: "fb_base_url")
  String? fbBaseUrl;
  @JSONField(name: "amazon_s3_address2")
  String? amazonS3Address2;
  @JSONField(name: "app_broadcast_requirement")
  String? appBroadcastRequirement;
  @JSONField(name: "room_sticks")
  String? roomSticks;
  @JSONField(name: "ulevel_privilege_mic_10")
  String? ulevelPrivilegeMic10;
  @JSONField(name: "ulevel_privilege_avatar")
  String? ulevelPrivilegeAvatar;
  @JSONField(name: "sign_by_account_pwd")
  String? signByAccountPwd;
  @JSONField(name: "tk_on_watch_ads")
  String? tkOnWatchAds;
  @JSONField(name: "app_topcoin_ranking")
  String? appTopcoinRanking;
  @JSONField(name: "support_paytm")
  String? supportPaytm;
  @JSONField(name: "app_event_ranking")
  String? appEventRanking;
  @JSONField(name: "tlevel_privilege_mic_9")
  String? tlevelPrivilegeMic9;
  @JSONField(name: "app_birthday_talents")
  String? appBirthdayTalents;
  @JSONField(name: "path_temp")
  String? pathTemp;
  @JSONField(name: "inbox_level")
  String? inboxLevel;
  @JSONField(name: "topup_banner")
  String? topupBanner;
  @JSONField(name: "new_label_id")
  String? newLabelId;
  @JSONField(name: "whitelist_countries")
  String? whitelistCountries;
  @JSONField(name: "wifi_credentials")
  String? wifiCredentials;
  @JSONField(name: "need_picture_validation")
  String? needPictureValidation;
  @JSONField(name: "amazon_s3_address")
  String? amazonS3Address;
  @JSONField(name: "android_log_expiry_period")
  String? androidLogExpiryPeriod;
  @JSONField(name: "ulevel_privilege_mic_9")
  String? ulevelPrivilegeMic9;
  @JSONField(name: "youtube_channel_official")
  String? youtubeChannelOfficial;
  @JSONField(name: "amazon_s3_bucket_name2")
  String? amazonS3BucketName2;
  @JSONField(name: "currency_unit")
  String? currencyUnit;
  @JSONField(name: "google_in_app_billing_item_ids")
  String? googleInAppBillingItemIds;
  @JSONField(name: "push_channel_vip_talent")
  String? pushChannelVipTalent;
  dynamic imageConfig;
  @JSONField(name: "sofa_price_add")
  String? sofaPriceAdd;
  @JSONField(name: "cash_out_currency_rate")
  String? cashOutCurrencyRate;
  @JSONField(name: "super_vip")
  String? superVip;
  @JSONField(name: "report_content_video")
  String? reportContentVideo;
  dynamic currencyRate;
  @JSONField(name: "payment_server")
  String? paymentServer;

  @JSONField(name: "push_channel_all")
  String? pushChannelAll;
  @JSONField(name: "amazon_s3_bucket_name")
  String? amazonS3BucketName;
  @JSONField(name: "ulevel_post_status")
  String? ulevel_post_status;
  @JSONField(name: "app_login_page_show_sms")
  String? loginShowSMS;
  @JSONField(name: "level_page_url")
  String? level_page_url;
  // 开播限制条件 以及  上传Cover：用户等级， 默认3
  @JSONField(name: "ulevel_go_live")
  String? ulevelGoLive;

  // 开播限制条件 以及  上传Cover：主播等级， 默认3
  @JSONField(name: "tlevel_go_live")
  String? tlevelGoLive;

  // 开播时，是否强制用户上传海报  0-不强制   1-强制， 默认1
  @JSONField(name: "go_live_cover")
  String? goLiveCover;

  // 签约主播页面
  @JSONField(name: "talent_apply_url")
  String? talentApplyUrl;

  // 预付单地址
  @JSONField(name: "razorpay_server_url")
  String? razorpay_server_url;

  // 修改国家码间隔时间（默认15天）
  @JSONField(name: "user_modify_country_day")
  String? user_modify_country_day;

  /// 系统头像库
  /// system_avatar 这个配置项一直没有用 2.6.0默认头像改为全路径  为版本兼容老版本 配置项改成[default_avatar]
  @JSONField(name: "default_avatar")
  String? tSystemAvatar;

  /// 可不通过申请即可上麦的最小用户等级
  @JSONField(name: "mic_request_user_level")
  String? micRequestUserLevel;

  ///  //播放页是否显示 水果游戏  不影响playcent里面的Game  1显示  0 不显示
  @JSONField(name: "quick_entry_greedy_show")
  String? showGreedy;

  /// slog 配置
  @JSONField(name: "slog_config")
  String? slogConfig;

  /// weeklyArUrl 配置
  @JSONField(name: "weeklyAr_url")
  String? weeklyArUrl;

  String? get systemAvatar => (tSystemAvatar ?? '').isNotEmpty
      ? tSystemAvatar
      : "Female Avatar|female_avatar.png,Male Avatar|male_avatar.png,Pinky Avatar|pinky_avatar.png,Purple Avatar|purple_avatar.png";
  @JSONField(name: "max_room_manager")
  String? maxRoomManager;

  List<YBDUniqueIDEntity?>? uniqueIds;

  /// 充值活动弹框的配置信息
  @JSONField(name: "topup_offer")
  String? topupOffer;

  /// 是否显示首页icon   0-不显示  1-显示  Popular 页面的功能入口列表
  @JSONField(name: "show_home_icon")
  String? showHomeIcon;

  /// combo活动弹框的配置信息
  @JSONField(name: "master_of_combos")
  String? masterOfCombos;

  /// 送礼活动弹框的配置信息
  @JSONField(name: "present_activity")
  String? presentActivity;

  /// 开斋节活动弹框的配置信息
  @JSONField(name: "eid_special_activity")
  String? eidSpecialActivity;

  ///转盘游戏配置项
  @JSONField(name: 'lottery_page_display')
  String? lotteryPageDisplay;

  @JSONField(name: 'lottery_game_url')
  String? lotteryGameUrl;

  /// 充值送礼活动弹框的配置信息
  @JSONField(name: "topup_gift_activity")
  String? topUpGiftActivity;

  /// 代理充值页面的 h5 地址
  @JSONField(name: "reseller_url")
  String? resellerUrl;

  /// ludo地址
  @JSONField(name: "ludo_test_rooms")
  String? ludoTestRooms;

  @JSONField(name: "top_talent_ranking_image")
  String? toptalentRankingImages;

  /// pk标签图片
  @JSONField(name: "pk_label_image")
  String? pkLabelImage;

  /// pk帮助地址
  @JSONField(name: "pk_help")
  String? pkHelp;

  /// 透传模式 默认0
  @JSONField(name: "transparent_method")
  String? transparentMethod;

  /// 主播工资单：talent_payroll
  @JSONField(name: "talent_payroll")
  String? talentPayroll;

  /// 主播申请页: talent_sign
  @JSONField(name: "talent_sign")
  String? talentSign;

  /// 钻石兑换豆子 gems_beans
  @JSONField(name: "gems_beans")
  String? gemsBeans;

  /// 客户端是否显示错误弹框 error_modal
  @JSONField(name: "error_modal")
  String? errorModal;

  /// 声网角色改变添加
  @JSONField(name: "room_abuser_enable")
  String? roomAbuserEnable;

  ///收集周期函数异常调用
  @JSONField(name: "collect_life_cle")
  String? collecLifeCleExt;

  /// teenpatti 翻牌间隔时间 0 - 1000 millsenconds
  @JSONField(name: "tp_card_transform")
  String? tpCardTransform;

  /// teenpatti 板凳上是否显示冒豆子动画 1 冒 0 不冒
  @JSONField(name: "tp_seat_dance")
  String? tpSeatDance;

  /// teenpatti一局结束客户端是否主动更新用户余额
  @JSONField(name: "tp_check_balance")
  String? tpCheckBalance;

  /// 200,1 : 连续点击的时间间隔,是否区分座位
  @JSONField(name: "tp_config")
  String? tpConfig;

  ///进入房间相关操作
  @JSONField(name: "room_operate")
  String? roomOperate;

  /// teenpatti钻石换豆子
  @JSONField(name: "tp_gems2beans")
  String? tpGems2Beans;

  /// 豆子换金币地址
  @JSONField(name: "beans2golds_url")
  String? beans2GoldsUrl;

  @JSONField(name: "skin_config")
  String? skinConfig;

  /// 分享开关 {facebook,whatsapp,instagram,messenger,snapchat,twitter,skype}
  @JSONField(name: "share_switch")
  String? shareSwitch;

  ///游戏房相关的json 配置项
  @JSONField(name: "game_room_config")
  String? gameRoomConfig;

  /// 统计接口时长的开关
  @JSONField(name: "analysis_api_duration")
  String? analysisApiDuration;

  /// 上传firebase日志的开关
  @JSONField(name: "firebase_log")
  String? firebaseLog;

  /// 埋点开关 0-都不报  1-只报Firebase埋点  2-只报数数埋点  3-都报
  @JSONField(name: "track_switch")
  String? trackSwitch;

  /// store page 初始标签页
  @JSONField(name: "store_init_index")
  String? storeInitIndex;

  YBDConfigInfo(
      {this.appLocationPortaltype,
      this.pathTemp,
      this.pathShareImage,
      this.amazonS3Address2,
      this.imageConfig,
      this.currencyRate});

  /// 通过解析 json 字符串获取所有平台的 app 版本配置信息
  /// 管理后台只支持配置字符串
  YBDAppVersion? get appVersionConfig {
    YBDAppVersion? result = YBDAppVersion();
    try {
      result = JsonConvert.fromJsonAsT<YBDAppVersion>(jsonDecode(appVersion!));
    } catch (e) {
      logger.e('===get app version config error : $e');
    }

    return result;
  }

  factory YBDConfigInfo.initial() => YBDConfigInfo(
        appLocationPortaltype: "en#201,ar#210",
        pathShareImage: "image",
        pathTemp: "temp",
        amazonS3Address2: "http://d3py0gff3tm44e.cloudfront.net/",
        imageConfig: {
          "1": {"A": 800, "B": 1280},
          "2": {"A": 128, "B": 480},
          "3": {"A": 256},
          "4": {"A": 48, "B": 128},
          "5": {"A": 256},
          "6": {"A": 128, "B": 256, "C": 480, "D": 800},
          "7": {"A": 80, "B": 128, "C": 256},
          "8": {"A": 80, "B": 128, "C": 256, "D": 640},
          "9": {"A": 480, "B": 128},
          "10": {"A": 1024},
          "11": {"A": 200},
          "12": {"A": 360, "B": 720},
          "13": {"A": 100, "B": 160},
          "15": {"A": 400, "B": 720}
        },
      );
}
