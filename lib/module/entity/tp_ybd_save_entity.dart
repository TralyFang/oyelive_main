

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDTpSaveEntity with JsonConvert<YBDTpSaveEntity> {
  String? returnCode;
  String? returnMsg;
  YBDTpSaveRecord? record;
}

class YBDTpSaveRecord with JsonConvert<YBDTpSaveRecord> {
  dynamic users;
  dynamic broadcastType;
  dynamic title;
  dynamic subTitle;
  String? content;
  List<YBDTpSaveRecordButtons?>? buttons;
  dynamic images;
  String? layout;
  YBDTpSaveRecordAttrs? attrs;
}

class YBDTpSaveRecordButtons with JsonConvert<YBDTpSaveRecordButtons> {
  String? name;
  dynamic type;
  dynamic action;
  dynamic url;
}

class YBDTpSaveRecordAttrs with JsonConvert<YBDTpSaveRecordAttrs> {
  YBDTpSaveRecordAttrsBeans? beans;
  YBDTpSaveRecordAttrsName? name;
}

class YBDTpSaveRecordAttrsBeans with JsonConvert<YBDTpSaveRecordAttrsBeans> {
  String? value;
  String? key;
}

class YBDTpSaveRecordAttrsName with JsonConvert<YBDTpSaveRecordAttrsName> {
  String? value;
  String? key;
}
