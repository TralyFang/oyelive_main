

class YBDDBDynamicLinkEntity {
  int? id;
  int? inviter; // inviter user id
  String? spf; // share platform
  int? time; // generate time
  String? link; // firebase dynamic link
  String? type; // invite , room ,  status
  int? rid; // room id
  String? sid; // status id
  String? prefix; // firebase dynamic url prefix

  YBDDBDynamicLinkEntity(this.inviter, this.spf, this.time, this.link, this.type, this.prefix,
      {this.id, this.rid, this.sid});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'inviter': inviter,
      'spf': spf,
      'time': time,
      'link': link,
      'type': type,
      'rid': rid,
      'sid': sid,
      'prefix': prefix,
    };
    return map;
  }

  YBDDBDynamicLinkEntity.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    inviter = map['inviter'];
    spf = map['spf'];
    time = map['time'];
    link = map['link'];
    type = map['type'];
    rid = map['rid'];
    sid = map['sid'];
    prefix = map['prefix'];
  }
}
