

/*
 * @Date: 2021-07-07 11:20:40
 * @LastEditors: William-Zhou
 * @LastEditTime: 2021-07-19 09:29:51
 * @FilePath: \oyetalk_flutter\lib\module\entity\pk_record_entity.dart
 */
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDPKRecordEntity with JsonConvert<YBDPKRecordEntity> {
  String? returnCode;
  String? returnMsg;
  String? recordSum;
  List<YBDPKRecord?>? record;
}

//* 要和后台对字段
class YBDPKRecord with JsonConvert<YBDPKRecord> {
  /// 自己的ID
  int? userId;

  /// pk时间
  int? createTime;

  /// 对手ID
  int? competitor;

  /// PK结果  0 平局，1 胜利，2 失败
  int? result;

  /// 自己收到的礼物
  int? gifts;

  /// 对手收到的礼物
  int? competitorGifts;
}
