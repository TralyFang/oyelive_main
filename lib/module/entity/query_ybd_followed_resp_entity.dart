

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

class YBDQueryFollowedRespEntity with JsonConvert<YBDQueryFollowedRespEntity> {
  String? returnCode;
  String? returnMsg;
  YBDQueryFollowedRespRecord? record;
  dynamic recordSum;
}

class YBDQueryFollowedRespRecord with JsonConvert<YBDQueryFollowedRespRecord> {
  List<YBDRoomInfo?>? users;
  List<int>? followed;
  dynamic fromid;
  dynamic toid;
}
