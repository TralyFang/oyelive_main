

import '../../generated/json/base/json_convert_content.dart';

class YBDVipValidInfoEntity with JsonConvert<YBDVipValidInfoEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDVipValidInfoRecord?>? record;
  String? recordSum;
}

class YBDVipValidInfoRecord with JsonConvert<YBDVipValidInfoRecord> {
  String? type;
  List<YBDVipValidInfoRecordItemList?>? itemList;
}

class YBDVipValidInfoRecordItemList with JsonConvert<YBDVipValidInfoRecordItemList> {
  int? number;
  int? personalId;
  int? price;
  int? name;
  int? expireAfter;
  int? id;
  String? vipName;
  int? index;
  String? vipIcon;
}
