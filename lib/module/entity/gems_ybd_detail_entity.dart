

/*
 * @Author: William-Zhou
 * @Date: 2021-08-09 15:47:54
 * @LastEditTime: 2021-08-18 17:53:56
 * @LastEditors: William-Zhou
 * @Description: 
 */
import '../../generated/json/base/json_convert_content.dart';

class YBDGemsDetailEntity with JsonConvert<YBDGemsDetailEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDGemsDetailRecord?>? record;
  String? recordSum;
}

class YBDGemsDetailRecord with JsonConvert<YBDGemsDetailRecord> {
  int? createTime;
  int? gems;
  String? type;
  int? userId;
}
