

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDBaggageUidEntity with JsonConvert<YBDBaggageUidEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDBaggageUidRecord?>? record;
  String? recordSum;
}

class YBDBaggageUidRecord with JsonConvert<YBDBaggageUidRecord> {
  String? type;
  List<YBDBaggageUidRecordItemList?>? itemList;
}

class YBDBaggageUidRecordItemList with JsonConvert<YBDBaggageUidRecordItemList> {
  int? number;
  int? personalId;
  bool? equipped;
  int? price;
  String? name;
  String? currency;
  int? expireAfter;
  int? id;
}
