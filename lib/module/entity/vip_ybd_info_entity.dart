

import '../../generated/json/base/json_convert_content.dart';

class YBDVipInfoEntity with JsonConvert<YBDVipInfoEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDVipInfoRecord?>? record;
}

class YBDVipInfoRecord with JsonConvert<YBDVipInfoRecord> {
  int? id;
  int? status;
  int? level;
  int? price;
  int? rights;
  String? name;
  bool? modify;
}
