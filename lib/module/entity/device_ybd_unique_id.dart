

import '../../generated/json/base/json_convert_content.dart';

class YBDDeviceUniqueID with JsonConvert<YBDDeviceUniqueID> {
  bool? blocked;
  bool? newDevice;
  String? content;

  YBDDeviceUniqueID({this.blocked, this.newDevice, this.content});

  YBDDeviceUniqueID.fromMap(Map<String, dynamic> map) {
    blocked = map['blocked'];
    newDevice = map['newDevice'];
    content = map['content'];
  }
}
