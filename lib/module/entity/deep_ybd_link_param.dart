

import '../../generated/json/base/json_convert_content.dart';

class YBDDeepLinkParam with JsonConvert<YBDDeepLinkParam> {
  String? spf;    // FirebaseDynamicLink   分享平台
  int? inviter;    // FirebaseDynamicLink   邀请人
  String? statusId;    // FirebaseDynamicLink   动态ID
  int? roomId;    // 房间ID    FirebaseDynamicLink - rid    FCM - talentId
}