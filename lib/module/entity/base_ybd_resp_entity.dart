

/*
 * @Author: William-Zhou
 * @Date: 2022-01-07 17:36:43
 * @LastEditTime: 2022-01-07 17:36:44
 * @LastEditors: William-Zhou
 * @Description: 
 */
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDBaseRespEntity with JsonConvert<YBDBaseRespEntity> {
  String? returnCode;
  String? returnMsg;
}
