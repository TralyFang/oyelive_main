

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/entity/room_ybd_info_entity.dart';

class YBDQueryLabelRoomsRespEntity with JsonConvert<YBDQueryLabelRoomsRespEntity> {
  String? returnCode;
  String? returnMsg;
  YBDRoomLabelInfo? record;
  dynamic recordSum;
}

class YBDRoomLabelInfo with JsonConvert<YBDRoomLabelInfo> {
  String? name;
  int? dateType;
  dynamic labelId;
  List<YBDRoomInfo?>? rank;
  dynamic scores;
}
