

import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:synchronized/synchronized.dart';
import 'package:oyelive_main/common/event/game_ybd_mission_event.dart';
import 'package:oyelive_main/common/navigator/navigator_ybd_helper.dart';
import 'package:oyelive_main/main.dart';
import 'package:oyelive_main/ui/page/profile/aristocracy/dialog_ybd_common.dart';

import '../../common/constant/const.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/sp_ybd_util.dart';
import '../../common/util/toast_ybd_util.dart';
import '../entity/new_ybd_result_entity.dart';
import '../user/entity/user_ybd_info_entity.dart';
import 'db/model_ybd_conversation.dart';
import 'db/model_ybd_message.dart';
import 'entity/conversation_ybd_list_entity.dart';
import 'entity/message_ybd_list_entity.dart';
import 'entity/message_ybd_offset_entity.dart';
import 'entity/send_ybd_entity.dart';
import 'entity/send_ybd_response_entity.dart';
import 'inbox_ybd_api_helper.dart';
import 'message_ybd_helper.dart';

Future<bool?>? fetchConversationList(BuildContext? context) async {
  await YBDInboxApiHelper.imLogin(context);
  YBDConversationListEntity? data = await YBDInboxApiHelper.pullConversationList(context);
  if (data?.code == Const.HTTP_SUCCESS_NEW) {
    logger.v('fetch conversation list success');
    await insertConversationList(data!.data!);
    return Future<bool>.value(true);
  } else {
    //失败后5秒重试直到成功
    String? session = await YBDSPUtil.get(Const.SP_JSESSION_ID);
    if (session != null && session.isNotEmpty) {
      await Future.delayed(Duration(seconds: 5), () {});
      return fetchConversationList(context);
    }
  }
}

insertConversationList(List<YBDConversationListData?> data, {String? newFriendMessage}) async {
  for (YBDConversationListData? element in data) {
    int time = DateTime.now().millisecondsSinceEpoch;
    YBDConversationModel conversationModel;

//    bool isExist = await messageDb.exists(
//        table: YBDConversationModel.conversationTableName, where: "queue_id = \"${element.queueId}\"");
    String where = '';
    if (element?.principal?.id == null) {
      where = 'queue_id = \"${element!.queueId}\"';
    } else {
      where = 'queue_id = \"${element!.queueId}\" or sender_id =\"${element.principal?.id}\"';
    }
    List<dynamic> data = await YBDConversationModel().sqlSelect(where: where);
    bool isExist = data.length != 0;
    if (isExist) {
      ///  update 只更新某些字段，并不是全部
//      conversationModel = YBDConversationModel(
//        title: element.principal.nickname,
//        isNotDisturb: element.disturb,
//        isBlocked: element.blockList,
//        messageJsonContent: jsonEncode(element.toJson()),
//        avatar: element.principal.avatar,
//        senderGender: element.principal.sex,
//        queueID: element.queueId,
//      );

      data[0]
        ..title = element.principal!.nickname
        ..isNotDisturb = element.disturb
        ..isBlocked = element.blockList
        ..messageJsonContent = jsonEncode(element.toJson())
        ..avatar = element.principal!.avatar
        ..senderGender = element.principal!.sex
        ..queueID = element.queueId;

      await data[0].sqlUpdate(verbose: true);

//      logger.v("update conversation data: ${conversationModel.toDb()}");
//      messageDb.update(
//          table: YBDConversationModel.conversationTableName,
//          where: where,
//          row: YBDCommonUtil.toStringsMap(conversationModel.toDb()));
    } else {
      conversationModel = YBDConversationModel(
        conversationType: element.category,
        createTime: time,
//      updateTime: time,
        lastMessage: newFriendMessage ?? "",
        title: element.principal!.nickname,
        unreadCount: 0,
        isNotDisturb: element.disturb,
        isBlocked: element.blockList,
        queueID: element.queueId,
        messageJsonContent: jsonEncode(element.toJson()),
        avatar: element.principal!.avatar,
        senderGender: element.principal!.sex,
        senderID: element.principal!.id.toString(),
      );
      if (element.principal?.id?.toString() == YBDMessageHelper.READING_CONVERSATION?.senderID) {
        YBDMessageHelper.READING_CONVERSATION = conversationModel;
      }
      logger.v("insert data: ${conversationModel.toDb()}");
      conversationModel.sqlInsert();
    }
  }
}

fetchMessageQueue(BuildContext? context, {int size: 100, bool needSyncQueueId: false}) async {
  YBDMessageOffsetEntity? messageOffsetEntity = await YBDSPUtil.getMessageOffset();
  YBDMessageListEntity? data =
      await YBDInboxApiHelper.pullMessage(context, size: size, pullQueueSequence: messageOffsetEntity?.sequenceId);
  if (data?.code == Const.HTTP_SUCCESS_NEW) {
    insertMessage(context, data!.data!.pullMessages!.messages!, needSyncQueueId: needSyncQueueId);
    logger.v("insert message!!");
  }
}

/*
*   处理insert 数据 im/messages http有个60秒 pull
* */
insertMessage(BuildContext? context, List<YBDMessageListDataPullMessage?> data, {bool needSyncQueueId: true}) async {
  Map conversationUpdateList = new Map();

  YBDUserInfo? userInfo = await YBDSPUtil.getUserInfo();

  for (YBDMessageListDataPullMessage? element in data) {
    if (element?.contentType?.startsWith("system") ?? false) {
      if (element!.contentType!.contains("newFriend")) {
        // new friend, insert new conversation

        try {
          await YBDConversationModel(
            conversationType: element.content!.category,
            createTime: DateTime.now().millisecondsSinceEpoch,
            //      updateTime: time,
            lastMessage: "",
            title: element.content?.principal?.nickname,
            unreadCount: 0,
            isNotDisturb: element.content?.disturb,
            isBlocked: element.content?.blockList,
            queueID: element.queueId,
            messageJsonContent: jsonEncode(element.content!.toJson()),
            avatar: element.content?.principal?.avatar,
            senderGender: element.content?.principal?.sex,
            senderID: element.content?.principal?.id?.toString(),
          ).sqlInsert();
        } catch (e) {
          logger.v('insert conversation error : $e');
        }
      }
    } else {
      YBDMessageModel messageModel;

//      bool isExist = await messageDb.exists(
//          table: YBDMessageModel.messageTableName,
//          where: "message_id = \"${element.messageId}\" or request_id = \"${element.requestId}\"");
      List data = await YBDMessageModel()
          .sqlSelect(where: "message_id = \"${element!.messageId}\" or request_id = \"${element.requestId}\"");
      bool isExist = data.length != 0;

      //正在阅读的,自己发的入库直接算已读
      bool isRead = element.sender?.id != null &&
          (YBDMessageHelper.READING_CONVERSATION?.queueID == element.queueId ||
              YBDMessageHelper.READING_CONVERSATION?.senderID == element.sender?.id?.toString() ||
              element.sender?.id == userInfo!.id);

      bool shoudAddUnread = false;
      if (isExist) {
        messageModel = YBDMessageModel(
            id: data[0].id,
            queueId: element.queueId,
            messageId: element.messageId,
            messageStatus: MessageStatus.received,
            jsonContent: jsonEncode(element.toJson()),
            category: element.category,
            contentType: element.contentType,
            senderID: element.sender?.id?.toString(),
            receiverID: element.receivers![0]!.id.toString(),
            sequenceId: element.sequenceId,
            sendTime: element.sendTime,
            requestId: element.requestId);

        await messageModel.sqlUpdate(verbose: true);
        //record message offset
        if (needSyncQueueId)
          await YBDSPUtil.save(Const.SP_MESSAGE_QUEUE, YBDMessageOffsetEntity()..sequenceId = element.sequenceId);
      } else {
        if (element.content?.layout == "text_friend#2") {
          isRead = true;
        }

        shoudAddUnread = true;
        messageModel = YBDMessageModel(
            messageId: element.messageId,
            messageStatus: MessageStatus.received,
            jsonContent: jsonEncode(element.toJson()),
            category: element.category,
            contentType: element.contentType,
            senderID: element.sender?.id.toString(),
            receiverID: element.receivers![0]!.id.toString(),
            sequenceId: element.sequenceId,
            sendTime: element.sendTime,
            queueId: element.queueId,
            text: element.content!.text,
            requestId: element.requestId,
            isRead: isRead);
        int? rowId = await messageModel.sqlInsert();

        try {
          //有新的游戏任务完成
          var jsonContent = json.decode(messageModel.jsonContent!);
          log("jsonContent$jsonContent");

          int view = jsonContent['content']['attributes']['view'];
          bool showPop = jsonContent['content']['attributes']['showPop'] ?? false;
          if (showPop) {
            showDialog<Null>(
                context: Get.context!, //BuildContext对象
                barrierDismissible: false,
                builder: (BuildContext c) {
                  return YBDCommonDialog(
                    element.content?.text ?? '',
                    dialogHeiht: 345,
                    yesText: 'GO',
                    onConfirm: () {
                      YBDNavigatorHelper.onTapShare(c, jsonContent);
                    },
                  );
                });
          }
          if (view != null && (view == 2 || view == 1)) {
            Future.delayed(Duration(seconds: 2), () {
              log("send task finished!");
              eventBus.fire(YBDGameMissionEvent());
            });
          }
        } catch (e) {}
        //
        if (rowId == null || rowId < 0) {
          //if sql error no more further action
          break;
        } else {
          if (needSyncQueueId)
            await YBDSPUtil.save(Const.SP_MESSAGE_QUEUE, YBDMessageOffsetEntity()..sequenceId = element.sequenceId);
        }
      }

      //record conversation unread count
      if (conversationUpdateList.containsKey(element.queueId)) {
        conversationUpdateList[element.queueId]["last_message"] = element.content!.comment;
        conversationUpdateList[element.queueId]["last_time"] = element.sendTime;
      } else {
        conversationUpdateList.addAll({
          element.queueId: {
            "last_message": element.content?.comment ?? "You have a new message.",
            "last_time": element.sendTime
          }
        });
      }
    }
  }

  ///updateConversation unread count
  conversationUpdateList.forEach((queueId, value) async {
    List<dynamic> row = await YBDConversationModel().sqlSelect(where: "queue_id = \"$queueId\"");
    List<dynamic> messages = await YBDMessageModel()
        .sqlSelect(where: "queue_id = '$queueId' and is_read =  'true' ", limit: 1, orderBy: "sequence_id DESC");
    if (row != null && row.length != 0) {
      int? sequenceId = 0;
      if (messages != null && messages.length != 0) {
        sequenceId = messages[0].sequenceId;
      }
      int? count =
          await YBDMessageModel().sqlCount(where: "queue_id = '$queueId' and sequence_id > $sequenceId ", verbose: true);

      row[0]
        ..updateTime = value["last_time"]
        ..lastMessage = value["last_message"]
        ..unreadCount = count
        ..sqlUpdate(verbose: true);
    }
  });

  if (needSyncQueueId) {
    bool isSyncSuccess = data.length != 0 && await syncSequenceId(context, data.last?.sequenceId);

    if (isSyncSuccess && data.length != 0) {
      //still got message up
      fetchMessageQueue(context, needSyncQueueId: true);
    }
  }
}

int? currentSequenceId;
final _lock = Lock();

Future<bool> syncSequenceId(BuildContext? context, int? sequenceId) async {
  bool result = await _lock.synchronized(() async {
    if (currentSequenceId != sequenceId) {
      currentSequenceId = sequenceId;
      YBDNewResultEntity? data = await YBDInboxApiHelper.syncSequenceId(context, sequenceId);
      return data?.code == Const.HTTP_SUCCESS_NEW;
    } else
      return false;
  });

  return result;
}

sendMessageTo(BuildContext context, YBDMessageModel messageModel, YBDSendEntity sendEntity, int? senderID) async {
  await messageModel.sqlInsert();

  YBDSendResponseEntity? sendResult = await YBDInboxApiHelper.sendMessage(context, sendEntity);
  YBDMessageModel message = YBDMessageModel(messageId: sendResult?.data?.messageId, messageStatus: MessageStatus.sent);
  MessageStatus status;
  if (sendResult?.code == Const.HTTP_SUCCESS_NEW) {
    //send success
    status = MessageStatus.sent;

    //if is a new contact
    if (sendResult?.data?.newContact != null && sendResult?.data?.newContact != null) {
      print("set a new conversation");
      await insertConversationList([sendResult!.data!.contactInfo], newFriendMessage: sendEntity.content!.text);
    }
    //
  } else if (sendResult?.code == Const.HTTP_MESSAGE_BLOCKED) {
    status = MessageStatus.reject;
  } else if (sendResult?.code == Const.HTTP_MESSAGE_BLOCKING) {
    status = MessageStatus.blocking;

    showDialog<Null>(
        context: context, //BuildContext对象
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(translate('block_hint')),
            actions: [
              TextButton(
                child: Text(
                  translate('cancel'),
                  style: TextStyle(color: Color(0xff5E94E7)),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              TextButton(
                child: Text(
                  translate('ok'),
                  style: TextStyle(color: Color(0xff5E94E7)),
                ),
                onPressed: () async {
                  // 跳转登录页
                  Navigator.pop(context);
                  bool success = await YBDMessageHelper.blockContact(
                      context, YBDMessageHelper.READING_CONVERSATION!.senderID, EditAction.Delete);
                  if (success) {
                    YBDMessageHelper.READING_CONVERSATION!.isBlocked = false;
                  }
                },
              )
            ],
          );
        });
  } else {
    YBDToastUtil.toast(translate('failed'));

    status = MessageStatus.send_failed;
  }
  List<dynamic> messages = await YBDMessageModel().sqlSelect(where: "request_id = \"${sendEntity.requestId}\"");
  if (messages != null && messages.length != 0) {
    messages[0]
      ..messageId = sendResult?.data?.messageId
      ..messageStatus = status;
    messages[0].sqlUpdate(verbose: true);
  }

  List<dynamic> conversations = await YBDConversationModel().sqlSelect(where: "sender_id = \"${sendEntity.toId}\"");
  if (conversations != null && conversations.length != 0) {
    conversations[0]
      ..lastMessage = sendEntity.content!.text
      ..updateTime = DateTime.now().millisecondsSinceEpoch;
    await conversations[0].sqlUpdate(verbose: true);
  }
}

//已读
readMessage(String? queueId) async {
  //消息列表已读

  List<dynamic> conversations = await YBDConversationModel().sqlSelect(where: "queue_id = \"$queueId\"");

  if (conversations != null && conversations.length != 0) {
    conversations[0]..unreadCount = 0;
    await conversations[0].sqlUpdate(verbose: true);
  }

  List<dynamic> messages =
      await YBDMessageModel().sqlSelect(where: "queue_id = \"$queueId\"", orderBy: "send_time DESC", limit: 1);
  if (messages != null && messages.length != 0) {
    messages.forEach((element) {
      element..isRead = true;
      element.sqlUpdate(verbose: true);
    });
  }
}
