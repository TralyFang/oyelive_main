

import 'package:flutter/cupertino.dart';
import 'package:sqlcool/sqlcool.dart';
import '../../common/db_ybd_config.dart';
import '../../common/http/http_ybd_util.dart';
import '../entity/new_ybd_result_entity.dart';
import 'entity/block_ybd_list_entity.dart';
import 'entity/message_ybd_list_entity.dart';
import 'entity/send_ybd_entity.dart';
import 'entity/send_ybd_response_entity.dart';
import 'inbox_ybd_api.dart';
import '../../ui/page/share/entity/friend_ybd_share_resp_entity.dart';

import 'db/model_ybd_message.dart';
import 'entity/conversation_ybd_list_entity.dart';

class YBDInboxApiHelper {
  static Future imLogin(
    BuildContext? context, {
    bool needErrorToast = true,
    int? timeout,
  }) async {
    return YBDHttpUtil.getInstance().doGet<YBDNewResultEntity>(
      context,
      YBDInboxApi.IM_LOGIN,
      baseUrl: await YBDInboxApi.getBaseUrl,
      connectTimeout: timeout,
      needErrorToast: needErrorToast,
    );
  }

  static Future<YBDConversationListEntity?> pullConversationList(BuildContext? context) async {
    return YBDHttpUtil.getInstance().doGet<YBDConversationListEntity>(
      context,
      YBDInboxApi.IM_PULL_CONVERSATION,
      baseUrl: await YBDInboxApi.getBaseUrl,
    );
  }

  static Future<YBDConversationListEntity?> queryConversation(BuildContext context, var userId) async {
    return YBDHttpUtil.getInstance().doGet<YBDConversationListEntity>(
      context,
      YBDInboxApi.IM_PULL_CONVERSATION + "/$userId",
      baseUrl: await YBDInboxApi.getBaseUrl,
    );
  }

  static Future<YBDSendResponseEntity?> sendMessage(BuildContext context, YBDSendEntity sendEntity) async {
    return YBDHttpUtil.getInstance().doPost<YBDSendResponseEntity>(context, YBDInboxApi.IM_SEND_MSG, sendEntity.toJson(),
        baseUrl: await YBDInboxApi.getBaseUrl, isJsonData: true);
  }

  /// 分享好友接口
  static Future<YBDFriendShareRespEntity?> shareToFriend(BuildContext? context, Map<String, dynamic> param) async {
    return YBDHttpUtil.getInstance().doPost<YBDFriendShareRespEntity>(
      context,
      YBDInboxApi.IM_SHARE_FRIEND,
      param,
      baseUrl: await YBDInboxApi.getBaseUrl,
      isJsonData: true,
    );
  }

  static Future<YBDMessageListEntity?> pullMessage(BuildContext? context, {int? pullQueueSequence, int size: 100}) async {
    return YBDHttpUtil.getInstance().doGet<YBDMessageListEntity>(
      context,
      YBDInboxApi.IM_PULL_MESSAGE,
      params: {
        "pullQueueSequence": pullQueueSequence,
        "size": size,
      },
      baseUrl: await YBDInboxApi.getBaseUrl,
    );
  }

  static Future<YBDNewResultEntity?> syncSequenceId(BuildContext? context, int? sequenceId) async {
    return YBDHttpUtil.getInstance().doPut<YBDNewResultEntity>(
        context,
        YBDInboxApi.IM_SYNC,
        {
          "pullQueueSequence": sequenceId,
        },
        baseUrl: await YBDInboxApi.getBaseUrl,
        isJsonData: true);
  }

  /**
   * where 查询条件，
   * page 从0开始
   * pageSize 默认50
   */
  static Future<List<YBDMessageModel>> getMessageList({YBDMessageModel? where, int page = 0, int pageSize = 50}) async {
    List<dynamic> result = await YBDMessageModel()
        .sqlSelect(where: where?.getWhere(), orderBy: "send_time DESC", limit: pageSize, offset: pageSize * page);

    if (result == null || result.length == 0) {
      return [];
    }

    List<YBDMessageModel> messageList = [];
    result.forEach((element) {
      messageList.add(YBDMessageModel().fromDb(element));
    });
    return messageList;
  }

  /**
   * 消息数据库变化流，做适时的监听
   */

  static Stream<DatabaseChangeEvent> getMessageStream() {
    return messageDb!.changefeed;
  }

  static Future<YBDNewResultEntity?> editContact(
      BuildContext? context, String? userID, EditType editType, EditAction editAction) async {
    String? url;
    switch (editType) {
      case EditType.Block:
        url = YBDInboxApi.IM_BLOCK_LIST;
        break;
      case EditType.Disturb:
        url = YBDInboxApi.IM_SET_DISTURB;
        break;
    }

    switch (editAction) {
      case EditAction.Add:
        return YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
            context,
            url,
            {
              "userId": userID,
            },
            baseUrl: await YBDInboxApi.getBaseUrl,
            isJsonData: true);
      case EditAction.Delete:
        return YBDHttpUtil.getInstance().doDelete<YBDNewResultEntity>(
          context,
          url,
          {
            "userId": userID,
          },
          baseUrl: await YBDInboxApi.getBaseUrl,
        );
    }
  }

  ///获取私信黑名单列表
  static Future<YBDBlockListEntity?> queryInboxBlacklist(BuildContext? context) async {
    return YBDHttpUtil.getInstance().doGet<YBDBlockListEntity>(
      context,
      YBDInboxApi.IM_BLOCK_LIST,
      baseUrl: await YBDInboxApi.getBaseUrl,
    );
  }
}

enum EditType { Block, Disturb }

enum EditAction { Add, Delete }
