


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDConversationListEntity with JsonConvert<YBDConversationListEntity> {
  String? code;
  List<YBDConversationListData?>? data;
}

class YBDConversationListData with JsonConvert<YBDConversationListData> {
  String? queueId;
  YBDConversationListDataPrincipal? principal;
  String? category;
  bool? disturb;
  bool? blockList;
}

class YBDConversationListDataPrincipal with JsonConvert<YBDConversationListDataPrincipal> {
  int? id;
  String? nickname;
  String? avatar;
  int? sex;
}
