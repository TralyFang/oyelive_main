

import '../../../generated/json/base/json_convert_content.dart';

class YBDBlockListEntity with JsonConvert<YBDBlockListEntity> {
	String? code;
	YBDBlockListData? data;
}

class YBDBlockListData with JsonConvert<YBDBlockListData> {
	List<YBDBlockListDataBlockList?>? blockList;
}

class YBDBlockListDataBlockList with JsonConvert<YBDBlockListDataBlockList> {
	int? userId;
	int? createTime;
	String? nickName;
	String? avatar;
}
