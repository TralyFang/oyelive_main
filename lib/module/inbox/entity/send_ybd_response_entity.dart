


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/inbox/entity/conversation_ybd_list_entity.dart';

class YBDSendResponseEntity with JsonConvert<YBDSendResponseEntity> {
  String? code;
  YBDSendResponseData? data;
}

class YBDSendResponseData with JsonConvert<YBDSendResponseData> {
  String? messageId;
  String? requestId;
  bool? newContact;
  YBDConversationListData? contactInfo;
}
