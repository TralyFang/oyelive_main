

import '../../../generated/json/base/json_convert_content.dart';

class YBDGiftMessageEntity with JsonConvert<YBDGiftMessageEntity> {
  String? messageId;
  String? contentType;
  YBDGiftMessageContent? content;
  dynamic sender;
  List<YBDGiftMessageReceiver?>? receivers;
  int? sequenceId;
  String? queueId;
  int? sendTime;
  String? category;
  YBDGiftMessageAttributes? attributes;
  String? requestId;
}

class YBDGiftMessageContent with JsonConvert<YBDGiftMessageContent> {
  String? layout;
  String? resource;
  String? icon;
  String? comment;
  dynamic attributes;
  dynamic text;
  String? title;
  YBDGiftMessageAttributes? extend;
  YBDGiftMessageContentClick? click;
}

class YBDGiftMessageContentClick with JsonConvert<YBDGiftMessageContentClick> {
  String? action;
  String? url;
}

class YBDGiftMessageReceiver with JsonConvert<YBDGiftMessageReceiver> {
  int? id;
}

class YBDGiftMessageAttributes with JsonConvert<YBDGiftMessageAttributes> {
  String? giftName;
  int? giftNumber;
  int? gender;
  int? level;
  String? userId;
  String? vipIcon;
}
