

import 'dart:convert';

import 'package:oyelive_main/common/util/log_ybd_util.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/inbox/entity/conversation_ybd_list_entity.dart';


class YBDMessageListEntity with JsonConvert<YBDMessageListEntity> {
  String? code;
  YBDMessageListData? data;
}

class YBDMessageListData with JsonConvert<YBDMessageListData> {
  YBDPullMessageListData? pullMessages;
}

class YBDPullMessageListData with JsonConvert<YBDPullMessageListData> {
  List<YBDMessageListDataPullMessage?>? messages;
  bool? end;
}

class YBDMessageListDataPullMessage with JsonConvert<YBDMessageListDataPullMessage> {
  String? messageId;
  String? contentType;
  YBDMessageListDataPullMessagesContent? content;
  var attributes;
  YBDMessageListDataPullMessagesSender? sender;
  List<YBDMessageListDataPullMessagesReceiver?>? receivers;
  int? sequenceId;
  String? queueId;
  int? sendTime;
  String? category;
  String? requestId;
}

class YBDMessageListDataPullMessagesContent with JsonConvert<YBDMessageListDataPullMessagesContent> {
  String? text;

  //********* new friend*****
  String? queueId;
  YBDConversationListDataPrincipal? principal;
  String? category;
  bool? disturb;
  bool? blockList;

  //***************************

  // TODO: 新增字段，用 Alt + j 生成 json 解析代码
  //********* system *****
  String? icon;
  String? title;
  String? subtitle;
  String? resource;
  String? comment;
  YBDMessageQuoteMessage? quoteMessage;
  //***************************
  var attributes;
  var click;
  String? layout;
  var extend;
  @override
  YBDMessageListDataPullMessagesContent fromJson(dynamic value) {
    if (value is String) {
      try {
        return super.fromJson(json.decode(value));
      } catch (e) {
        logger.v('=======json error : $e');
        logger.v('=======json error string : $value');
        return YBDMessageListDataPullMessagesContent();
      }
    } else {
      return super.fromJson(value);
    }
  }
}

class YBDMessageListDataPullMessagesSender with JsonConvert<YBDMessageListDataPullMessagesSender> {
  int? id;
  String? nickname;
}

class YBDMessageListDataPullMessagesReceiver with JsonConvert<YBDMessageListDataPullMessagesReceiver> {
  int? id;
}

/// 点赞评论消息
class YBDMessageQuoteMessage with JsonConvert<YBDMessageQuoteMessage> {
  String? icon;
  String? text;
}

class YBDMessageFontColorEntity with JsonConvert<YBDMessageFontColorEntity> {
  String? target;
  double? fontSize;
  int? fontWeight; // FontWeight.index 2, 4, 6
  String? color; // "#FFFFFF"

}