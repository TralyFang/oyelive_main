

import 'dart:convert';

import 'package:uuid/uuid.dart';

enum Category { friend }

class YBDSendEntity {
  YBDContent? content;
  String? contentType; //支持以json类型透传
  Category? category; //类型
  int? toId; //发送的目标id
  String? requestId; //自动生成

  YBDSendEntity({YBDContent? content, String? contentType, Category? category, int? toId}) {
    this.content = content;
    this.contentType = contentType;
    this.category = category;
    this.toId = toId;
    requestId = Uuid().v4();
  }

  toJson() => {
        "content": content!.toJson(),
        "contentType": contentType,
        "category": category.toString().split(".").last,
        "toId": toId,
        "requestId": requestId,
      };
}

class YBDContent {
  String? text;
  String? jsonText;
  bool isJson;
  YBDContent({this.text, this.isJson: false, this.jsonText});

  Map? toJson() {
    return isJson ? json.decode(jsonText!) : {"text": text, "comment": text};
  }
}

class YBDContentType {
  static final text = "text/plain";
  static final cell = "application/cell";
}
