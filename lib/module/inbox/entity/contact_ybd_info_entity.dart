

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/inbox/entity/conversation_ybd_list_entity.dart';

class YBDContactInfoEntity with JsonConvert<YBDContactInfoEntity> {
  String? code;
  YBDConversationListData? data;
}
