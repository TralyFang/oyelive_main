

import '../../common/constant/const.dart';
import '../../common/http/environment_ybd_config.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/remote_ybd_config_service.dart';

class YBDInboxApi {
  static Future<String?> get getBaseUrl async {
    if (Const.TEST_ENV) {
      return YBDEnvConfig.inboxBasePath();
    }

    return await _apiBaseUrl();
  }

  static Future<String?> _apiBaseUrl() async {
    String? imUrlProd = await YBDRemoteConfigService.baseUrlFromRemote('im_http_domain_prod');
    logger.v("im_http_domain_prod : $imUrlProd");

    if ((imUrlProd ?? '').isEmpty) {
      imUrlProd = Const.IM_PROD_URL;
    }

    logger.v('im base url : $imUrlProd');
    return imUrlProd;
  }

  static const IM_LOGIN = "/im/login";
  static const IM_PULL_CONVERSATION = "/im/contacts";
  static const IM_SEND_MSG = "/im/messages";
  static const IM_PULL_MESSAGE = "/im/messages";
  static const IM_SYNC = "/im/queues";
  static const IM_BLOCK_LIST = "/im/blockList";
  static const IM_SET_DISTURB = "/im/disturb";
  static const IM_SHARE_FRIEND = "/im/share";
}
