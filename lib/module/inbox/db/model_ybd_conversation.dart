

import 'dart:async';

import 'package:sqlcool/sqlcool.dart';
import '../../../common/util/common_ybd_util.dart';
import '../../../common/util/log_ybd_util.dart';
import 'model_ybd_message.dart';
import '../../../common/db_ybd_config.dart';

class YBDConversationModel with DbModel {
  static final conversationTableName = 't_conversation';
  final conversationTable = DbTable(conversationTableName)
    ..varchar('conversation_type')
    ..integer('create_time')
    ..integer('update_time', nullable: true)
    ..varchar('last_message')
    ..varchar('title')
    ..varchar('avatar', nullable: true)
    ..integer('unread_count', defaultValue: 0)
    ..integer('sender_gender', nullable: true)
    ..boolean('is_at_top', defaultValue: false)
    ..boolean('is_not_disturb', defaultValue: false)
    ..boolean('is_blocked', defaultValue: false)
    ..varchar('message_json_content')
    ..varchar('queue_id', unique: true)
    ..integer(
      'sequence_id',
      nullable: true,
    )
    ..varchar('sender_id', nullable: true);

  @override
  int? id;

  //friend,like,event,system,comment
  String? conversationType;
  int? createTime;
  int? updateTime;
  String? lastMessage;
  //conversation with ?
  String? title;
  String? avatar;
  int? unreadCount;
  int? senderGender;

  bool? isAtTop;
  bool? isNotDisturb;
  bool? isBlocked;
  String? messageJsonContent;
  String? queueID;
  String? senderID;

  int? sequenceID;

  YBDConversationModel(
      {this.id,
      this.conversationType,
      this.createTime,
      this.updateTime,
      this.lastMessage,
      this.title,
      this.avatar,
      this.unreadCount,
      this.isAtTop,
      this.isNotDisturb,
      this.messageJsonContent,
      this.queueID,
      this.isBlocked,
      this.sequenceID,
      this.senderID,
      this.senderGender});

  @override
  Db? get db => messageDb;

  @override
  DbTable get table => conversationTable;

  @override
  YBDConversationModel fromDb(Map<String, dynamic> map) {
    final conversation = YBDConversationModel(
        id: map['id'] as int?,
        conversationType: map['conversation_type'] as String?,
        lastMessage: map['last_message'] as String?,
        title: map['title'] as String?,
        avatar: map['avatar'] as String?,
        messageJsonContent: map['message_json_content'] as String?,
        senderID: map['sender_id'] as String?,
        queueID: map['queue_id'] as String?,
        sequenceID: YBDCommonUtil.getIntFormMap(map['sequence_id']),
        createTime: YBDCommonUtil.getIntFormMap(map['create_time']),
        updateTime: YBDCommonUtil.getIntFormMap(map['update_time']),
        unreadCount: YBDCommonUtil.getIntFormMap(map['unread_count']),
        senderGender: YBDCommonUtil.getIntFormMap(map['sender_gender']),
        isAtTop: YBDCommonUtil.getBooleanFormMap(map['is_at_top']),
        isNotDisturb: YBDCommonUtil.getBooleanFormMap(map['is_not_disturb']),
        isBlocked: YBDCommonUtil.getBooleanFormMap(map['is_blocked']));

    return conversation;
  }

  @override
  Map<String, dynamic> toDb() {
    final row = <String, dynamic>{
//      "id": id,
      'conversation_type': conversationType,
      'last_message': lastMessage,
      'title': title,
      'avatar': avatar,
      'message_json_content': messageJsonContent,
      'queue_id': queueID,
      'sequence_id': sequenceID,
      'create_time': createTime,
      'update_time': updateTime,
      'unread_count': unreadCount,
      'is_at_top': isAtTop,
      'is_not_disturb': isNotDisturb,
      'is_blocked': isBlocked,
      'sender_id': senderID,
      'sender_gender': senderGender
    };
    if (id != null) row.addAll({'id': id});
    row.removeWhere((key, value) => value == null);
    return row;
  }

  Future<List<YBDConversationModel?>> getConversationList() async {
    try {
      List<YBDConversationModel?>? top = (await sqlSelect(where: 'is_at_top=true', groupBy: 'update_time DESC')).cast<YBDConversationModel?>();
      List<YBDConversationModel?>? normal = (await sqlSelect(where: 'is_at_top=false', groupBy: 'update_time DESC')).cast<YBDConversationModel?>();
      if (top != null && normal != null) {
        top.addAll(normal);
        return top;
      }
    }catch (e) {
      logger.e('getConversationList error:${e.toString()}');
    }
    return [];
  }

  updateSingleUnread(String senderID, int count, String lastMessage) async {
    try {
      List<YBDConversationModel?>? result = (await sqlSelect(
          where: 'queue_id=$queueID')).cast<YBDConversationModel?>();
      if (result.length != 0) {
        result[0]!.unreadCount = count;
        result[0]!.lastMessage = lastMessage;
        await result[0]!.sqlUpdate(verbose: true);
      } else {
        logger.v('sql update failed. Conversation not found!');
      }
    }catch (e) {
      logger.e('updateSingleUnread error:${e.toString()}');
    }
  }

  refreshAllReadCount() async {
    try {
      List<YBDConversationModel?>? result = (await sqlSelect()).cast<
          YBDConversationModel?>();
      if (result.length != 0) {
        result.forEach((element) async {
          int? unreadCount = await YBDMessageModel().sqlCount(
              where: 'queue_id=${element!.queueID} and is_read=false');

          element.unreadCount = unreadCount;
          await element.sqlUpdate(verbose: true);
        });
      }
    }catch (e) {
      logger.e('refreshAllReadCount error:${e.toString()}');
    }
  }
}
