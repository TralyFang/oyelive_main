

import 'package:sqlcool/sqlcool.dart';
import '../../../common/db_ybd_config.dart';
import '../../../common/util/common_ybd_util.dart';

enum MessageStatus {
  sending,
  sent,
  send_failed,
  reject,
  blocking,
  received //收到的

}

parseMessageStatus(String? status) {
  if (status == null || status.isEmpty) {
    return null;
  }
  MessageStatus? result;
  MessageStatus.values.forEach((MessageStatus messageStatus) {
    if (messageStatus.toString().split(".").last == status) {
      result = messageStatus;
    }
  });

  return result;
}

class YBDMessageModel with DbModel {
  static final messageTableName = "t_message";
  final messageTable = DbTable(messageTableName)
    ..varchar("message_id", unique: true, nullable: true)
    ..varchar("content_type", nullable: true)
    ..varchar("category")
    ..varchar("text", nullable: true)
    ..varchar("json_content", nullable: true)
    ..varchar("sender_id", nullable: true)
    ..varchar("receiver_id")
    ..varchar("queue_id", nullable: true)
    ..varchar("remark", nullable: true)
    ..varchar("message_status")
    ..varchar("request_id")
    ..varchar("comment", nullable: true)
    ..integer("sequence_id", nullable: true)
    ..integer("send_time", nullable: true)
    ..boolean("is_read", defaultValue: false);
  @override
  int? id;

  //消息id
  String? messageId;

  //内容的类型 暂时固定为text/plain
  String? contentType;

  //消息类型    以下四种
  //    String CAT_FRIEND = "friend";
  //
  //    String CAT_SYSTEM = "system";
  //
  //    String CAT_COMMENT = "comment";
  //
  //    String CAT_LIKE = "like";
  String? category;

  ///消息原始json 对应 [YBDMessageListDataPullMessage] 类
  String? jsonContent;

  String? senderID;
  String? receiverID;

  //消息文本
  String? text;

  //队列id
  String? queueId;

  //发送的id
  String? requestId;

  //备注
  String? remark;

  //点评
  String? comment;

  //消息状态，在上面的枚举里有
  MessageStatus? messageStatus;

  //序列id
  int? sequenceId;

  //发送时间
  int? sendTime;

  //是否已读
  bool? isRead;

  YBDMessageModel(
      {this.id,
      this.messageId,
      this.contentType,
      this.category,
      this.jsonContent,
      this.senderID,
      this.requestId,
      this.receiverID,
      this.queueId,
      this.remark,
      this.messageStatus,
      this.sequenceId,
      this.text,
      this.sendTime,
      this.comment,
      this.isRead});

  @override
  Db? get db => messageDb;

  @override
  DbTable get table => messageTable;

  @override
  YBDMessageModel fromDb(Map<String, dynamic> map) {
    final message = YBDMessageModel(
        id: map["id"] as int?,
        messageId: map["message_id"] as String?,
        contentType: map["content_type"] as String?,
        category: map["category"] as String?,
        jsonContent: map["json_content"] as String?,
        senderID: map["sender_id"] as String?,
        receiverID: map["receiver_id"] as String?,
        queueId: map["queue_id"] as String?,
        remark: map["remark"] as String?,
        comment: map["comment"] as String?,
        messageStatus: parseMessageStatus(map["message_status"] as String?),
        requestId: map['request_id'] as String?,
        sequenceId: YBDCommonUtil.getIntFormMap(map["sequence_id"]),
        sendTime: YBDCommonUtil.getIntFormMap(map["send_time"]),
        isRead: YBDCommonUtil.getBooleanFormMap(map["is_read"]),
        text: map["text"] as String?);
    // the key will be present only with join queries
    // in a simple select this data is not present

    return message;
  }

  @override
  Map<String, dynamic> toDb() {
    final row = <String, dynamic>{
      "message_id": messageId,
      "content_type": contentType,
      "category": category,
      "json_content": jsonContent,
      "sender_id": senderID,
      "receiver_id": receiverID,
      "queue_id": queueId,
      "remark": remark,
      "message_status": messageStatus.toString().split(".").last,
      "sequence_id": sequenceId,
      "send_time": sendTime,
      "is_read": isRead,
      "request_id": requestId,
      "text": text,
      "comment": comment
    };
    if (id != null) row.addAll({"id": id});
    row.removeWhere((key, value) => value == null);

    print("row:\n $row");
    return row;
  }

  getWhere() {
    String where = "";
    Map<String, dynamic> afterRemoveNull = toDb();

    afterRemoveNull.forEach((key, value) {
      if (value is String) {
        where += "and $key = \"$value\"";
      } else {
        where += "and $key = $value";
      }
    });

    if (where.length != 0) where = where.substring(4);

    print("where condition: $where");

    return where;
  }

//  @override
//  Future<int> sqlInsert({bool verbose = false}) async {
//    int insertId = await super.sqlInsert(verbose: verbose);
//    int conversationUnreadCount = await sqlCount(where: "sender_id=$senderID and is_read=false");
//    return conversationUnreadCount;
//  }

  Future<int?> countAllUnread() {
    return sqlCount(where: "is_read=false");
  }

//  @override
//  Future<List<YBDMessageModel>> sqlSelect(
//      {String where, String orderBy, int limit, int offset, String groupBy, bool verbose = false}) {
//    return super.sqlSelect(where: where, offset: offset, orderBy: orderBy, groupBy: groupBy, verbose: verbose);
//  }
}
