

import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:uuid/uuid.dart';
import '../../common/constant/const.dart';
import '../../common/db_ybd_config.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/numeric_ybd_util.dart';
import '../../common/util/remote_ybd_config_service.dart';
import '../entity/new_ybd_result_entity.dart';
import 'db/model_ybd_conversation.dart';
import 'db/model_ybd_message.dart';
import 'inbox_ybd_api_helper.dart';
import 'queue_ybd_sync.dart';
import '../user/entity/user_ybd_info_entity.dart';
import '../user/util/user_ybd_util.dart';
import '../../ui/page/profile/my_profile/util/top_ybd_up_badges_alert_util.dart';

import 'entity/conversation_ybd_list_entity.dart';
import 'entity/message_ybd_list_entity.dart';
import 'entity/send_ybd_entity.dart';

class YBDMessageHelper {
  //正在聊天/阅读的queueID

  static YBDConversationModel? READING_CONVERSATION;
  static Timer? fetchMessageTimer;

  static Duration fetchMessageInterval = Duration(seconds: 60);

  static final int maxPageSize = 50;

  static setReading(BuildContext context, YBDConversationModel conversationModel) {
    READING_CONVERSATION = conversationModel;
  }

  static quitReading(BuildContext context) {
    READING_CONVERSATION = null;
  }

  static setRoutineInterval() async {
    YBDRemoteConfigService? r = await YBDRemoteConfigService.getInstance();

    String? message_final = r?.getConfig()?.getString("inbox_fetch_message_interval_final");

    if ((message_final ?? '').isNotEmpty) {
      try {
        fetchMessageInterval = Duration(seconds: int.parse(message_final!));
      } catch (e) {
        print(e);
      }
    }
  }

  static checkSingleContact(BuildContext context, userId) async {
    YBDConversationListEntity? contactInfoEntity = await YBDInboxApiHelper.queryConversation(context, userId);
    if (contactInfoEntity != null && contactInfoEntity.code == Const.HTTP_SUCCESS_NEW) {
      if (contactInfoEntity.data != null && contactInfoEntity.data!.length > 0)
        insertConversationList(contactInfoEntity.data!);
    }
  }

  static Future<YBDConversationModel?> getConversationByUserId(String userId) async {
    List<dynamic> result = await YBDConversationModel().sqlSelect(where: "sender_id = \'$userId\'");
    if (result != null && result.length != 0) {
      return result[0];
    }
    return null;
  }

  static setFetchMessageRoutine(BuildContext context) {
    fetchMessageTimer?.cancel();
    fetchMessageTimer = Timer.periodic(fetchMessageInterval, (_) {
      fetchMessageQueue(context, needSyncQueueId: true);
    });
  }

  static removeFetchMessageRoutine() {
    fetchMessageTimer?.cancel();
  }

  //来自socket，push的单条消息通知操作
  static singleMessageAdd(BuildContext? context, YBDMessageListDataPullMessage message) async {
//    bool isExist = await checkIfExistConversation(message);
    YBDTopUpBadgesAlertUtil.receiveMessage(message);
    await insertMessage(context, [message], needSyncQueueId: false);

//    await messageModel.sqlInsert();
    YBDSerialMessageFetch.addToFetchQueue(fetchMessageQueue, context);
  }

  /// 主动拉消息
  static pullMessage(BuildContext? context) {
    YBDSerialMessageFetch.addToFetchQueue(fetchMessageQueue, context);
  }

  static Future<bool> checkIfExistConversation(YBDMessageListDataPullMessage message) async {
    List<dynamic> result = await YBDConversationModel().sqlSelect(where: 'sender_id = \'${message.sender!.id}\'');
    return result.length == 0;
  }

  //已读列表
  static readConversation(String? queueId) {
    readMessage(queueId);
  }

  static deleteMessageModel(YBDMessageModel messageModel) async {
    String? queueId = messageModel.queueId;
    await messageModel.sqlDelete();
    updateLastMessage(queueId);
  }

  static deleteSingleChat(String? messageId, String? queueId) async {
    await YBDMessageModel().sqlDelete(
      where: "message_id = \"$messageId\"",
    );

    updateLastMessage(queueId);
  }

  static updateLastMessage(String? queueId) async {
    var rows = await YBDMessageModel()
        .sqlSelect(where: 'queue_id = \"${READING_CONVERSATION?.queueID}\"', orderBy: 'send_time DESC', limit: 1);
    String? lastMessage = '';
    int? lastTime = 0;
    if (rows.length != 0) {
      lastMessage = rows[0].text;
      lastTime = rows[0]?.sendTime;
    }

    List<dynamic> result =
        await YBDConversationModel().sqlSelect(where: "queue_id = \"${READING_CONVERSATION?.queueID}\"");
    if (result != null && result.length != 0) {
      result[0]
        ..lastMessage = lastMessage
        ..updateTime = lastTime;
      result[0].sqlUpdate(verbose: true);
    }
  }

  static tempConversation(YBDUserInfo userInfo) async {
    List<dynamic> conversations =
        await YBDConversationModel().sqlSelect(where: "sender_id = \"${userInfo.id.toString()}\"");

    if (conversations != null && conversations.length == 0) {
      YBDConversationModel(
              conversationType: 'friend',
              senderID: userInfo.id.toString(),
              avatar: userInfo.headimg,
              queueID: userInfo.id.toString(),
              senderGender: userInfo.sex,
              title: userInfo.nickname)
          .sqlInsert();
    }
  }

  static deleteChatHistory(String? queueId, String? senderId, {bool isFriend: false}) async {
    String where;
    if (isFriend) {
      where =
          "(sender_id = \"${senderId}\" or receiver_id = \"${senderId}\" or queue_id = \"${queueId}\") and category =\"friend\"";
    } else {
      where = "queue_id = \"$queueId\"";
    }

    await YBDMessageModel().sqlDelete(where: where);

    List<dynamic> conversations = await YBDConversationModel().sqlSelect(where: "queue_id = '$queueId'");

    if (conversations != null && conversations.length != 0) {
      conversations[0]
        ..lastMessage = ""
        ..unreadCount = 0
        ..updateTime = 0;
      await conversations[0].sqlUpdate(verbose: true);
    }
  }

  static setTopChat(String? queueId, bool setTo) async {
    List<dynamic> conversations = await YBDConversationModel().sqlSelect(where: "queue_id = \"$queueId\"");

    if (conversations != null && conversations.length != 0) {
      conversations[0]..isAtTop = setTo;
      print(conversations[0].toDb());
      await conversations[0].sqlUpdate(verbose: true);
    }
  }

  static YBDMessageModel sendMessageNow(BuildContext context, YBDSendEntity sendEntity, int? senderID) {
    YBDMessageModel message = YBDMessageModel(
        messageId: sendEntity.requestId,
        requestId: sendEntity.requestId,
        text: sendEntity.content!.text,
        category: sendEntity.category.toString().split(".").last,
        senderID: senderID.toString(),
        receiverID: sendEntity.toId.toString(),
        messageStatus: MessageStatus.sending,
        sendTime: DateTime.now().millisecondsSinceEpoch,
        isRead: true);
    message.jsonContent = json.encode(message.toDb()..addAll({'content': sendEntity.content!.toJson()}));

    sendMessageTo(context, message, sendEntity, senderID);
    return message;
  }

  // 更新会话的最后一条消息
  static updateConversation(YBDMessageModel messageModel) async {
    List<dynamic> conversations =
        await YBDConversationModel().sqlSelect(where: "sender_id = \"${messageModel.receiverID}\"");

    if (conversations != null && conversations.length != 0) {
      conversations[0]
        ..lastMessage = messageModel.comment
        ..updateTime = messageModel.sendTime;
      await conversations[0].sqlUpdate(verbose: true);
    } else {
      logger.v('no conversation');
    }
  }

  static blockContact(BuildContext? context, String? userId, EditAction editAction) async {
    YBDNewResultEntity? resultEntity = await YBDInboxApiHelper.editContact(context, userId, EditType.Block, editAction);
    if (resultEntity?.code == Const.HTTP_SUCCESS_NEW) {
      List<dynamic> conversations = await YBDConversationModel().sqlSelect(where: "sender_id = \"$userId\"");

      if (conversations != null && conversations.length != 0) {
        conversations[0]..isBlocked = editAction == EditAction.Add;
        await conversations[0].sqlUpdate(verbose: true);
      }

      return true;
    } else {
      return false;
    }
  }

  static unblock(userId) async {
    List<dynamic> conversations = await YBDConversationModel().sqlSelect(where: "sender_id = \"$userId\"");

    if (conversations != null && conversations.length != 0) {
      conversations[0]..isBlocked = false;
      await conversations[0].sqlUpdate(verbose: true);
    }
  }

  static Future<bool> setDisturbContact(BuildContext context, String? userId, EditAction editAction) async {
    YBDNewResultEntity? resultEntity = await YBDInboxApiHelper.editContact(context, userId, EditType.Disturb, editAction);
    if (resultEntity?.code == Const.HTTP_SUCCESS_NEW) {
      List<dynamic> conversations = await YBDConversationModel().sqlSelect(where: "sender_id = \"$userId\"");

      if (conversations != null && conversations.length != 0) {
        conversations[0]..isNotDisturb = editAction == EditAction.Add;
        await conversations[0].sqlUpdate(verbose: true);
      }

      return true;
    } else {
      return false;
    }
  }

  /// 插入一条 event notice 消息
  /// 获取 event notice 会话 id
  /// 获取最近一条 event notice 消息
  /// 生成新的 msg，根据最近一条 event notice 消息生成
  /// 插入消息
  /// 更新会话列表
  /// [url] 消息跳转链接
  /// [title] 消息标题
  /// [text] 消息内容
  /// [resource] 消息图片
  static Future<void> insertEventNoticeMessage({
    String? url,
    String? title,
    String? text,
    String? resource,
  }) async {
    // 获取 event notice 会话
    String? queueId = await _selectEventNoticeConversation();

    // 获取 event notice 会话里的最近一条消息
    YBDMessageModel? lastMsg = await _selectLatestEventNoticeMessage(queueId);

    // 新生成的消息
    YBDMessageModel noticeMsg = await _topupGiftEventNoticeMessage(
      lastMsg: lastMsg,
      queueId: queueId,
      text: text,
      url: url,
      resource: resource,
      title: title,
    );

    // 过滤重复的充值送礼弹框消息
    if (lastMsg?.text == noticeMsg.text) {
      logger.v('ignore duplicated msg');
      return;
    }

    try {
      // 插入消息
      await noticeMsg.sqlInsert();
    } catch (e) {
      logger.v('noticeMsg.sqlInsert error : $e');
    }

    // 更新会话列表
    _updateLastMessage(noticeMsg);
  }

  /// 更新会话列表里对应会话 item
  /// [msg] 最新的消息
  static Future<void> _updateLastMessage(YBDMessageModel msg) async {
    // 找到消息对应的会话记录
    List<dynamic> result = await YBDConversationModel().sqlSelect(where: "queue_id = '${msg.queueId}'");

    // 找到记录后执行更新操作
    if (result != null && result.length != 0) {
      result[0]
        ..lastMessage = msg.text
        ..updateTime = msg.sendTime
        ..unreadCount += 1;

      try {
        // 更新会话记录
        result[0].sqlUpdate(verbose: true);
      } catch (e) {
        logger.v('sqlUpdate notice conversation error : $e');
      }
    }
  }

  /// 查询 event notice 会话
  /// 返回 [queue_id]
  static Future<String?> _selectEventNoticeConversation() async {
    // 获取 event notice 会话记录
    List<Map<String, dynamic>>? rows = await messageDb!.select(
      table: YBDConversationModel.conversationTableName,
      limit: 1,
      where: "conversation_type = 'event'",
    );

    // 取出 event notice 会话的 queue_id
    if (null != rows && rows.isNotEmpty) {
      final conversationModel = YBDConversationModel().fromDb(rows.first);
      return conversationModel.queueID;
    }

    // 会话记录会从接口获取，这里没有查到本地的会话不新建会话，避免会话列表重复
    return null;
  }

  /// 查询最近一条 event notice 消息
  /// [queueId] 会话 id
  /// 返回 [sequence_id]
  static Future<YBDMessageModel?> _selectLatestEventNoticeMessage(String? queueId) async {
    // 查询最新的 event notice 消息
    List<Map<String, dynamic>>? rows = await messageDb!.select(
      table: YBDMessageModel.messageTableName,
      limit: 1,
      where: "queue_id = '$queueId'",
      orderBy: "send_time DESC ",
    );

    if (null != rows && rows.isNotEmpty) {
      return YBDMessageModel().fromDb(rows.first);
    } else {
      return null;
    }
  }

  /// [sequenceId] 该条 event notice 消息的 sequenceId
  /// [queueId] event notice 会话 id
  /// [lastMsg] 最近一条 event notice 消息
  /// [title] 消息标题
  /// [text] 消息文本内容
  /// [url] 点击 event notice 跳转的地址
  /// [resource] 消息图片
  static Future<YBDMessageModel> _topupGiftEventNoticeMessage({
    YBDMessageModel? lastMsg,
    String? queueId,
    String? title,
    String? text,
    String? url,
    String? resource,
  }) async {
    final userId = await YBDUserUtil.userId();

    YBDMessageModel noticeMsg = YBDMessageModel();
    // 随机生成的 uuid
    noticeMsg.messageId = Uuid().v4();
    noticeMsg.contentType = lastMsg?.contentType ?? 'application/cell';
    noticeMsg.category = lastMsg?.category ?? 'event';
    noticeMsg.text = text;
    noticeMsg.senderID = '$userId';
    noticeMsg.receiverID = '$userId';
    noticeMsg.queueId = queueId;
    noticeMsg.messageStatus = MessageStatus.received;

    // requestId 为 null 时插入消息会报错
    noticeMsg.requestId = Uuid().v4();
    noticeMsg.sequenceId = null != lastMsg ? lastMsg.sequenceId! + 1 : 0;
    noticeMsg.sendTime = DateTime.now().millisecondsSinceEpoch;
    noticeMsg.isRead = false;

    // 生成 json 字符串
    noticeMsg.jsonContent = json.encode(_eventNoticeJsonContentFromMsg(
      msg: noticeMsg,
      userId: YBDNumericUtil.stringToInt(userId),
      title: title,
      url: url,
      resource: resource,
    ));
    return noticeMsg;
  }

  /// 生成 [YBDMessageModel] 对应的 json
  /// [url] 消息跳转的地址
  /// [title] 消息标题
  /// [resource] 消息图片
  static Map<String, dynamic> _eventNoticeJsonContentFromMsg({
    required YBDMessageModel msg,
    int? userId,
    String? title,
    String? url,
    String? resource,
  }) {
    Map<String, dynamic> map = {
      "messageId": msg.messageId,
      "contentType": msg.contentType,
      "content": {
        "text": msg.text,
        "title": title,
        "comment": msg.text,
        "resource": resource,
        "click": {
          "action": "redirect",
          "url": url,
        },
        "layout": "image_text_status#1",
      },
      "receivers": [
        {"id": userId}
      ],
      "sequenceId": msg.sequenceId,
      "queueId": msg.queueId,
      "sendTime": msg.sendTime,
      "category": msg.category,
    };

    return map;
  }
}

class YBDSerialMessageFetch {
  static DateTime? lastFetchTime;
  static bool callWhenCooling = false;
  static final Duration coolDown = Duration(seconds: 5);

  static addToFetchQueue(Function fetch, BuildContext? context) {
    DateTime nowTime = DateTime.now();

    int secsFormLastFetch = nowTime.difference(lastFetchTime ?? nowTime).inSeconds;

    print("YBDSerialMessageFetch secsFormLastFetch $secsFormLastFetch  $lastFetchTime");
    if (lastFetchTime == null || secsFormLastFetch >= coolDown.inSeconds) {
      print("YBDSerialMessageFetch fetch fetch fetch ");
      fetch.call(context, needSyncQueueId: true);
      lastFetchTime = nowTime;
      if (!callWhenCooling) {
        print("YBDSerialMessageFetch set delay check");
        Future.delayed(coolDown, () {
          print("YBDSerialMessageFetch after 10 $callWhenCooling ");
          if (callWhenCooling) {
            //冷却时间内被调用了
            print("YBDSerialMessageFetch fetch 10 10 10 ");
            lastFetchTime = DateTime.now();
            fetch.call(context, needSyncQueueId: true);
            callWhenCooling = false;
          }
        });
      }
    } else if (!callWhenCooling) {
      print("YBDSerialMessageFetch call when cooling ");
      callWhenCooling = true;
    }
  }
}
