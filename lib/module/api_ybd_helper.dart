

import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:oyelive_main/common/abstract/analytics/analytics_ybd_service.dart';
import 'package:oyelive_main/common/analytics/thinking/ta_ybd_track.dart';
import 'package:oyelive_main/common/event/game_ybd_mission_event.dart';
import 'package:oyelive_main/common/ext/normal_type_ext/string_ybd_ext.dart';
import 'package:oyelive_main/common/service/iap_service/entity/deliver_ybd_entity.dart';
import 'package:oyelive_main/common/util/certification_ybd_util.dart';
import 'package:oyelive_main/common/util/config_ybd_util.dart';
import 'package:oyelive_main/module/agency/agency_ybd_api.dart';
import 'package:oyelive_main/module/entity/base_ybd_resp_entity.dart';
import 'package:oyelive_main/module/entity/bubble_ybd_entity.dart';
import 'package:oyelive_main/module/entity/card_ybd_frame_store_entity.dart';
import 'package:oyelive_main/module/entity/gems_ybd_detail_entity.dart';
import 'package:oyelive_main/module/entity/golds_ybd_detail_entity.dart';
import 'package:oyelive_main/module/entity/pk_ybd_setting_entity.dart';
import 'package:oyelive_main/module/entity/query_ybd_server_time_entity.dart';
import 'package:oyelive_main/module/entity/tp_ybd_save_entity.dart';
import 'package:oyelive_main/module/room/entity/balance_ybd_record_entity.dart';
import 'package:oyelive_main/module/status/status_ybd_api.dart';
import 'package:oyelive_main/module/user/entity/account_ybd_delete_resp_entity.dart';
import 'package:oyelive_main/module/user/entity/mobile_ybd_check_resp_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';
import 'package:oyelive_main/ui/page/baggage/entity/baggage_ybd_bubble_entity.dart';
import 'package:oyelive_main/ui/page/game_lobby/entity/game_ybd_lobby_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_category_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_config_display_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_gift_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/game_ybd_room_enter_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/quick_ybd_comment_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/quick_ybd_join_entity.dart';
import 'package:oyelive_main/ui/page/game_room/entity/zego_ybd_code_entity.dart';
import 'package:oyelive_main/ui/page/game_room/getx/game_ybd_room_get_logic.dart';
import 'package:oyelive_main/ui/page/game_room/rtc/rtc_helper/rtc_ybd_helper.dart';
import 'package:oyelive_main/ui/page/home/entity/event_ybd_center_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/king_ybd_kong_entity.dart';
import 'package:oyelive_main/ui/page/home/entity/new_ybd_user_status.dart';
import 'package:oyelive_main/ui/page/profile/my_profile/entity/safe_ybd_alert_entity.dart';
import 'package:oyelive_main/ui/page/room/entity/emoji_ybd_entity.dart';

import '../common/analytics/analytics_ybd_util.dart';
import '../common/constant/const.dart';
import '../common/event/common_ybd_event.dart';
import '../common/http/http_ybd_util.dart';
import '../common/service/iap_service/entity/apple_ybd_pay_config_entity.dart';
import '../common/service/iap_service/entity/pre_ybd_order_entity.dart';
import '../common/service/iap_service/iap_ybd_def.dart';
import '../common/util/common_ybd_util.dart';
import '../common/util/date_ybd_util.dart';
import '../common/util/log_ybd_util.dart';
import '../common/util/notification_ybd_util.dart';
import '../common/util/sp_ybd_util.dart';
import '../common/util/toast_ybd_util.dart';
import '../generated/json/base/json_convert_content.dart';
import '../main.dart';
import '../redux/app_ybd_state.dart';
import '../redux/followed_ybd_ids_redux.dart';
import '../redux/locale_ybd_redux.dart';
import '../redux/user_ybd_redux.dart';
import '../ui/page/activty/entity/configurable_ybd_activity_entity.dart';
import '../ui/page/baggage/entity/baggage_ybd_entrance_entity.dart';
import '../ui/page/baggage/entity/baggage_ybd_frame_entity.dart';
import '../ui/page/baggage/entity/baggage_ybd_gift_entity.dart';
import '../ui/page/baggage/entity/baggage_ybd_themes_entity.dart';
import '../ui/page/follow/entity/friend_ybd_list_entity.dart';
import '../ui/page/follow/entity/query_ybd_friend_entity.dart';
import '../ui/page/home/entity/banner_ybd_advertise_entity.dart';
import '../ui/page/home/entity/car_ybd_list_entity.dart';
import '../ui/page/home/entity/combo_ybd_rank_entity.dart';
import '../ui/page/home/entity/cp_ybd_board_entity.dart';
import '../ui/page/home/entity/daily_ybd_check_entity.dart';
import '../ui/page/home/entity/daily_ybd_task_entity.dart';
import '../ui/page/home/entity/discount_ybd_list_entity.dart';
import '../ui/page/home/entity/follow_ybd_info_entity.dart';
import '../ui/page/home/entity/gift_ybd_list_entity.dart';
import '../ui/page/home/entity/rank_ybd_info_entity.dart';
import '../ui/page/profile/guardian/query_ybd_room_list_resp_entity.dart';
import '../ui/page/profile/my_profile/entity/top_ybd_up_gift_entity.dart';
import '../ui/page/recharge/entity/query_ybd_recharge_record_entity.dart';
import '../ui/page/room/play_center/entity/game_ybd_entity.dart';
import '../ui/page/room/play_center/entity/personal_ybd_frame_entity.dart';
import '../ui/page/room/service/live_ybd_service.dart';
import '../ui/page/room/teen_patti/entity/teen_ybd_patti_entity.dart';
import '../ui/page/store/entity/discount_ybd_entity.dart';
import '../ui/page/store/entity/mic_ybd_frame_entity.dart';
import '../ui/page/store/entity/theme_ybd_entity.dart';
import 'api.dart';
import 'entity/baggage_ybd_uid_entity.dart';
import 'entity/bean_ybd_detail_entity.dart';
import 'entity/buy_ybd_vip_result_entity.dart';
import 'entity/card_ybd_frame_bag_entity.dart';
import 'entity/deep_ybd_link_param.dart';
import 'entity/device_ybd_unique_id.dart';
import 'entity/discount_ybd_info_entity.dart';
import 'entity/game_ybd_info_entity.dart';
import 'entity/image_ybd_code_entity.dart';
import 'entity/invite_ybd_info_entity.dart';
import 'entity/level_ybd_info_entity.dart';
import 'entity/my_ybd_invites_entity.dart';
import 'entity/pk_ybd_record_entity.dart';
import 'entity/query_ybd_configs_resp_entity.dart';
import 'entity/query_ybd_followed_resp_entity.dart';
import 'entity/query_ybd_followers_entity.dart';
import 'entity/query_ybd_label_rooms_resp_entity.dart';
import 'entity/query_ybd_star_resp_entity.dart';
import 'entity/result_ybd_bean.dart';
import 'entity/room_ybd_info_entity.dart';
import 'entity/uid_ybd_list_entity.dart';
import 'entity/user_ybd_certification_entity.dart';
import 'entity/vip_ybd_info_entity.dart';
import 'entity/vip_ybd_valid_info_entity.dart';
import 'payment/payment_ybd_api.dart';
import 'user/entity/login_ybd_resp_entity.dart';
import 'user/entity/query_ybd_my_invites_resp_entity.dart';
import 'user/entity/query_ybd_room_blacklist_entity.dart';
import 'user/entity/query_ybd_room_resp_entity.dart';
import 'user/entity/query_ybd_user_resp_entity.dart';
import 'user/entity/room_ybd_label_entity.dart';
import 'user/entity/topup_ybd_summary_entity.dart';
import 'user/entity/user_ybd_info_entity.dart';

YBDApiHelperX ApiHelper = YBDApiHelperX.instance;

class YBDApiHelperX {
  YBDApiHelperX._();
  static YBDApiHelperX? _instance;
  static YBDApiHelperX get instance => _instance ??= YBDApiHelperX._();

  /// mock类调用的构造方法
  YBDApiHelperX.mock();

  Future<YBDQueryConfigsRespEntity?> queryConfigs(
    BuildContext? context, {
    int? timeout,
  }) async {
    YBDQueryConfigsRespEntity? queryConfigsResp = await YBDHttpUtil.getInstance().doGet<YBDQueryConfigsRespEntity>(
      context,
      YBDApi.QUERY_CONFIGS,
      connectTimeout: timeout,
      needErrorToast: false,
    );
    return queryConfigsResp;
  }

  /// 社交账号登录
  Future<YBDLoginResp?> snsLogin(BuildContext context, String? token, String type,
      {String? firstName, String? lastName}) async {
    String ipCountry = await YBDCommonUtil.getIpCountryCode(Get.context);
    logger.v('login sns type : $type token : $token, ipCountry: $ipCountry');

    YBDDeepLinkParam? param = await YBDSPUtil.getDeepLinkParams();
    Map<String, dynamic> params = {
      'token': token,
      'type': type,
      'inviter': param?.inviter,
      'sharePlatform': param?.spf,
      'country': ipCountry,
      // 'distinctId': TA.distinctId,
      'distinctId': await TA.ta?.getDistinctId(),
    };

    if (type == '7') {
      String name = '';
      if (null != firstName) {
        name += firstName;
      }

      if (null != lastName) {
        name += lastName;
      }

      params['name'] = name;
    }

    return await YBDHttpUtil.getInstance().doPost(
      context,
      YBDApi.APP_LOGIN,
      params,
    );
  }

  /// 社交账号绑定
  Future<YBDLoginResp?> snsLink(
    BuildContext context,
    String? token,
    String type, {
    bool showLoading = false,
  }) async {
    logger.v('link sns type : $type token : $token');

    Map<String, dynamic> params = {
      'token': token,
      'snsType': type,
    };

    return await YBDHttpUtil.getInstance().doGet(
      context,
      YBDApi.APP_LINK,
      params: params,
      showLoading: showLoading,
    );
  }

  /// 社交账号解除绑定
  Future<YBDLoginResp?> snsUnlink(BuildContext context, String type) async {
    logger.v("unlink sns type : $type");

    Map<String, dynamic> params = {
      'snsType': type,
    };

    return await YBDHttpUtil.getInstance().doGet(
      context,
      YBDApi.APP_UNLINK,
      params: params,
    );
  }

  /// 账号注销
  Future<YBDAccountDeleteRespEntity?> accountDelete(BuildContext context) async {
    logger.v("accountDelete...");

    Map<String, dynamic> params = {
      'type': 'unsubscribe',
    };

    return await YBDHttpUtil.getInstance().doPost(
      context,
      YBDApi.APP_ACCOUNT_DELETE,
      params,
    );
  }

  /// 检查登录信息
  Future<YBDLoginRecord?> checkLogin(
    BuildContext? context, {
    int refresh = 1,
    bool showLoading = false,
  }) async {
    logger.v('check login info');
    Map<String, dynamic> params = {'refresh': '1'};
    YBDLoginResp? result = await YBDHttpUtil.getInstance().doGet<YBDLoginResp>(
      context,
      YBDApi.CHECK_LOGIN,
      params: params,
      showLoading: showLoading,
    );
    if (result == null || result.returnCode != Const.HTTP_SUCCESS) {
      logger.v('check login failed');
      return null;
    } else {
      logger.v('check login success with user info : ${result.record!.userInfo!.toJson()}');
      YBDSPUtil.save(Const.SP_USER_INFO, result.record!.userInfo);

      if (context != null) {
        try {
          Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
          store.dispatch(YBDUpdateUserAction(result.record!.userInfo));
          store.dispatch(YBDUpdateLocaleAction(store.state.locale));
        } catch (e) {
          logger.v('update user action error : $e');
        }
      }
      return result.record;
    }
  }

  /// 查询用户信息
  Future<YBDUserInfo?> queryUserInfo(
    BuildContext? context,
    int? userId,
  ) async {
    logger.v('query user info with userId : $userId');
    Map<String, dynamic> params = {'id': userId};

    YBDQueryUserRespEntity? result = await YBDHttpUtil.getInstance().doGet<YBDQueryUserRespEntity>(context, YBDApi.QUERY_USER, params: params, needErrorToast: true);

    if (result == null || result.returnCode != Const.HTTP_SUCCESS) {
      logger.v('query user info failed');
      return null;
    } else {
      if (result.record!.isNotEmpty) {
        log('query user info success with user info : ${result.record![0]!.toJson()}');
        return result.record![0];
      } else {
        return null;
      }
    }
  }

  /// 举报用户
  Future<bool> complain(BuildContext context, String type, dynamic target, String remark) async {
    logger.v("request report user");

    Map<String, dynamic> params = {
      'type': type,
      'target': '$target',
      'remark': remark,
    };

    YBDResultBeanEntity? result = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
      context,
      YBDApi.COMPLAIN_ADD,
      params,
    );

    if (null == result || result.returnCode != Const.HTTP_SUCCESS) {
      logger.v('report user failed');
      return false;
    } else {
      logger.v('report user success');
      return true;
    }
  }
  void complainxBX3Uoyelive(BuildContext context, String type, dynamic target, String remark)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询是否已关注某个用户
  Future<bool> queryConcern(BuildContext? context, String? fromId, String toId) async {
    YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(context, YBDApi.QUERY_CONCERN, params: {'fromId': fromId, 'toId': toId});
    if (null == resultBean || resultBean.returnCode != Const.HTTP_SUCCESS) {
      logger.v('request query concern failed');
      return false;
    } else {
      logger.v('request query concern success');
      List recordList = resultBean.record as List;

      if (recordList.isNotEmpty) {
        logger.v('concern list is not empty');
        return true;
      } else {
        logger.v('concern list is empty');
        return false;
      }
    }
  }
  void queryConcernee2vzoyelive(BuildContext? context, String? fromId, String toId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 批量关注
  /// <userIdList> 主播id用逗号隔开，比如2000343,2000345
  Future<void> followTalentList(BuildContext? context, List<int?> talentIds) async {
    String talentIdParam = talentIds.join(',');
    YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
      context,
      YBDApi.FOLLOW_USER,
      {'talentIds': talentIdParam},
    );

    if (resultBean?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('_followTalentListSuccess');
      _followTalentListSuccess(talentIds);
    }
  }
  void followTalentListmaAXRoyelive(BuildContext? context, List<int?> talentIds)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 推荐页面批量关注成功后刷新store
  void _followTalentListSuccess(List<int?> talentList) {
    Store<YBDAppState> store = YBDCommonUtil.storeFromContext()!;
    // 刷新关注数
    final userInfo = store.state.bean!;
    int concern = userInfo.concern ?? 0;
    concern += 1;
    userInfo.concern = concern;
    store.dispatch(YBDUpdateUserAction(userInfo));
    logger.v('store dispatch user concern: ${userInfo.concern}');

    // 刷新关注列表
    Set<int?> followedIds = store.state.followedIds ?? Set();
    followedIds.addAll(talentList);
    store.dispatch(YBDUpdateFollowedIdsAction(followedIds));
    logger.v('store dispatch followedIds: ${store.state.followedIds}');

    // 接收主播开播的推送消息
    talentList.forEach((element) {
      YBDNotificationUtil.subscribeTalent(Get.context, '$element');
    });
  }
  void _followTalentListSuccessMEdDIoyelive(List<int?> talentList) {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 关注接口
  Future<bool> followUser(BuildContext? context, String? userId) async {
    YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
      context,
      YBDApi.FOLLOW_USER,
      {'roomId': userId},
    );

    var isFollowed = false;
    if (null == resultBean || resultBean.returnCode != Const.HTTP_SUCCESS) {
      logger.v('request follow failed');
      isFollowed = false;
    } else {
      logger.v('request follow success');
      eventBus.fire(YBDFollowBusEvent(int.parse(userId!)));

      Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;
      final userInfo = store.state.bean!;
      int concern = userInfo.concern ?? 0;
      concern += 1;
      userInfo.concern = concern;
      logger.v('before dispatch userInfo concern : ${userInfo.concern}');
      store.dispatch(YBDUpdateUserAction(userInfo));
      logger.v('after dispatch userInfo concern : ${store.state.bean!.concern}');

      /// 更新redux followedIds
      Set<int> followedIds = store.state.followedIds ?? Set();
      followedIds.add(int.parse(userId));
      store.dispatch(YBDUpdateFollowedIdsAction(followedIds));

      YBDNotificationUtil.subscribeTalent(context, userId);
      isFollowed = true;
    }
    YBDGameRoomGetLogic.syncInvokeFollowUser(userId, isFollowed);
    return isFollowed;
  }
  void followUserv6xkYoyelive(BuildContext? context, String? userId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 取消关注接口
  Future<bool> unFollowUser(BuildContext context, String userId) async {
    YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
      context,
      YBDApi.UNFOLLOW_USER,
      {'roomId': userId},
    );
    var cancelFollowed = false;
    if (null == resultBean || resultBean.returnCode != Const.HTTP_SUCCESS) {
      logger.v('request unFollow failed');
    } else {
      logger.v('request unFollow success');
      eventBus.fire(YBDUnFollowBusEvent(int.parse(userId)));

      /// 更新redux followedIds
      Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;

      final userInfo = store.state.bean!;
      int concern = userInfo.concern ?? 0;
      concern -= 1;
      userInfo.concern = concern;
      logger.v('before dispatch userInfo concern : ${userInfo.concern}');
      store.dispatch(YBDUpdateUserAction(userInfo));
      logger.v('after dispatch userInfo concern : ${store.state.bean!.concern}');

      Set<int> followedIds = store.state.followedIds ?? Set();
      followedIds.remove(int.parse(userId));
      store.dispatch(YBDUpdateFollowedIdsAction(followedIds));

      YBDNotificationUtil.unsubscribeTalent(context, userId);
      cancelFollowed = true;
    }
    YBDGameRoomGetLogic.syncInvokeFollowUser(userId, !cancelFollowed);
    return cancelFollowed;
  }
  void unFollowUserFFrTWoyelive(BuildContext context, String userId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 修改用户信息接口
  Future<bool> editUserInfo(Map<String, dynamic> userInfo, {bool showLoading = false}) async {
    YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
      Get.context,
      YBDApi.EDIT_USER_INFO,
      userInfo,
      showLoading: showLoading,
    );

    if (null == resultBean || resultBean.returnCode != Const.HTTP_SUCCESS) {
      logger.v('request edit user info failed');
      YBDToastUtil.toast(resultBean!.returnMsg);
      return false;
    } else {
      logger.v('request edit user info  success');
      return true;
    }
  }

  /// 修改用户头像接口
  Future<bool> changeAvatar(BuildContext? context, String? uploadedFileName) async {
    Map<String, dynamic> params = {'fileName': uploadedFileName, 'x': 0, 'y': 0};

    YBDResultBeanEntity? resultBean =
        await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(context, YBDApi.EDIT_IMAGE, params);

    if (null == resultBean || resultBean.returnCode != Const.HTTP_SUCCESS) {
      logger.v('request edit user avatar failed');
      YBDToastUtil.toast(resultBean!.returnMsg);
      return false;
    } else {
      logger.v('request edit user avatar success');
      return true;
    }
  }
  void changeAvatarmAdChoyelive(BuildContext? context, String? uploadedFileName)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  @deprecated
  Future<YBDRoomLabelEntity?> queryRoomLabel(BuildContext context, id) async {
    YBDRoomLabelEntity? roomLabelEntity = await YBDHttpUtil.getInstance().doGet<YBDRoomLabelEntity>(context, YBDApi.QUERY_ROOM_LABEL, params: {'queryType': -1, 'roomId': id});

    if (roomLabelEntity == null) {
      logger.v('room label entity is empty');
      return null;
    } else if (roomLabelEntity.returnCode != Const.HTTP_SUCCESS) {
      YBDToastUtil.toast(roomLabelEntity.returnMsg);
      return null;
    } else {
      return roomLabelEntity;
    }
  }

  /// [timeout] 毫秒
  Future<YBDQueryLabelRoomsRespEntity?> queryPopularRooms(
    BuildContext? context,
    int start,
    int? offset, {
    int? timeout,
  }) async {
    YBDQueryLabelRoomsRespEntity? resp = await YBDHttpUtil.getInstance().doGet<YBDQueryLabelRoomsRespEntity>(
      context,
      YBDApi.POPULAR_LIST,
      params: {'start': start, 'offset': offset},
      connectTimeout: timeout,
    );
    return resp;
  }

  /// 搜索用户
  Future<List<YBDUserInfo?>?> searchUser(BuildContext context, String keyword) async {
    YBDQueryUserRespEntity? result = await YBDHttpUtil.getInstance().doGet(context, YBDApi.SEARCH_USER,
        params: {'keyword': keyword, 'queryType': 2, 'start': 0, 'offset': 100}, needErrorToast: true);

    if (result == null || result.returnCode != Const.HTTP_SUCCESS) {
      logger.v('query user info failed');
      return null;
    } else {
      //认证信息
      List<int?> userIds = [];
      result.record!.forEach((element) {
        if (!userIds.contains(element!.id)) {
          userIds.add(element.id);
        }
      });

      YBDCertificationUtil.getInstance()!.addUserCache(userIds);

      return result.record;
    }
  }

  /// 查询用户信息
  Future<YBDQueryMyInvitesRespEntity?> queryMyInvites(BuildContext context, int? page, int size) async {
    YBDQueryMyInvitesRespEntity? result =
        await YBDHttpUtil.getInstance().doGet(context, YBDApi.QUERY_MY_INVITES, params: {'page': page, 'size': size});
    return result;
  }

  /// 查询首页广告数据
  Future<YBDBannerAdvertiseEntity?> homeBannerAdvertise(BuildContext context, {int adType: 1}) async {
    YBDBannerAdvertiseEntity? advertiseEntity =
        await YBDHttpUtil.getInstance().doGet(context, YBDApi.HOME_BANNER_AD, params: {'adtype': adType});

    if (advertiseEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request banner advertise success');
      return advertiseEntity;
    } else {
      logger.v('request banner advertise failed');
      return null;
    }
  }

  /// Explore 房间列表数据
  Future<YBDQueryLabelRoomsRespEntity?> exploreList(BuildContext context, int start, int offset, int labelId) async {
    YBDQueryLabelRoomsRespEntity? popularListEntity =
        await YBDHttpUtil.getInstance().doGet<YBDQueryLabelRoomsRespEntity>(context, YBDApi.EXPLORE_LIST, params: {
      'start': start,
      'offset': offset,
      'labelId': labelId,
    });

    if (popularListEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request explore list success');
      return popularListEntity;
    } else {
      logger.v('request explore list failed');
      return null;
    }
  }

  /// 金币排行榜接口
  Future<YBDRankInfoEntity?> receiversList(BuildContext context, int start, int offset, int timeType) async {
    YBDQueryStarRespEntity? resp = await queryStar(context, start, offset, timeType);
    YBDRankInfoEntity? rankInfoEntity = await Future.value(JsonConvert.fromJsonAsT<YBDRankInfoEntity>(resp?.toJson() ?? ''));

    if (rankInfoEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request receivers rank list success');
      return rankInfoEntity;
    } else {
      logger.v('request receivers rank list failed');
      return null;
    }
  }

  /// 礼物排行榜接口
  Future<YBDRankInfoEntity?> giftersList(BuildContext context, int start, int offset, int timeType) async {
    YBDRankInfoEntity? rankInfoEntity = await YBDHttpUtil.getInstance().doGet<YBDRankInfoEntity>(context, YBDApi.RANK_RICH, params: {
      'start': start,
      'offset': offset,
      'timeType': timeType,
    });

    if (rankInfoEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request gifters rank list success');
      return rankInfoEntity;
    } else {
      logger.v('request gifters rank list failed');
      return null;
    }
  }

  /// 粉丝排行榜接口
  /// [start] 和 [offset] 这两个参数是没用的（已和后台确认过）
  Future<YBDRankInfoEntity?> followersList(BuildContext context, int start, int offset, int timeType) async {
    YBDRankInfoEntity? rankInfoEntity =
        await YBDHttpUtil.getInstance().doGet<YBDRankInfoEntity>(context, YBDApi.RANK_FLOWS, params: {
      'start': start,
      'offset': offset,
      'timeType': timeType,
    });

    if (rankInfoEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request followers rank list success');
      return rankInfoEntity;
    } else {
      logger.v('request followers rank list failed');
      return null;
    }
  }

  /// combo 排行榜
  /// 查询的排行榜类 1-总榜， 2-30天榜，3-七天榜
  Future<YBDComboRankEntity?> comboRank(BuildContext context, int timeType) async {
    YBDComboRankEntity? comboRankEntity = await YBDHttpUtil.getInstance().doGet<YBDComboRankEntity>(
      context,
      YBDApi.COMBO_GIFT_LIST,
      params: {'timeType': timeType},
    );

    if (comboRankEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request combo gift success');
      return comboRankEntity;
    } else {
      logger.v('request combo gift failed');
      return null;
    }
  }

  /// cp 排行榜
  Future<YBDCpBoardEntity?> cpRank(BuildContext context) async {
    YBDCpBoardEntity? cpRankEntity = await YBDHttpUtil.getInstance().doGet<YBDCpBoardEntity>(
      context,
      YBDApi.CP_BOARD_LIST,
    );

    if (cpRankEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request cp gift success');
      return cpRankEntity;
    } else {
      logger.v('request cp gift failed');
      return null;
    }
  }

  /// 首充礼物列表接口
  Future<YBDTopUpGiftEntity?> firstTopUpGift(BuildContext context) async {
    YBDTopUpGiftEntity? firstTopUpGiftEntity = await YBDHttpUtil.getInstance().doGet<YBDTopUpGiftEntity>(
      context,
      YBDApi.FIRST_TOP_UP_GIFT,
    );

    if (firstTopUpGiftEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request first top up gift success');
      return firstTopUpGiftEntity;
    } else {
      logger.v('request first top up gift failed');
      return null;
    }
  }

  /// 任务接口
  Future<YBDDailyTaskEntity?> queryTask(BuildContext context) async {
    YBDDailyTaskEntity? dailyCheckEntity = await YBDHttpUtil.getInstance().doGet<YBDDailyTaskEntity>(
      context,
      YBDApi.DAILY_TASK,
    );

    logger.v('daily task YBDApi.DAILY_TASK : ${YBDApi.DAILY_TASK}');
    if (dailyCheckEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request daily task success');
      return dailyCheckEntity;
    } else {
      logger.v('request daily task failed');
      return null;
    }
  }

  /// 安全弹窗接口
  Future<YBDSafeAlertEntity?> queryUserSafetyLevel(BuildContext? context) async {
    YBDSafeAlertEntity? safeAlertEntity = await YBDHttpUtil.getInstance().doGet<YBDSafeAlertEntity>(
      context,
      YBDApi.SAFETY_LEVEL,
    );

    logger.v('queryUserSafetyLevel api : ${YBDApi.SAFETY_LEVEL}');
    if (safeAlertEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request safety alert success');
      return safeAlertEntity;
    } else {
      logger.v('request safety alert failed');
      return null;
    }
  }

  /// 每日任务接口
  Future<YBDMyDailyTaskEntity?> queryDailyTask(BuildContext context, {int viewing: 1}) async {
    //
    // 1:app任务界面(包含2),2:游戏内任务界面
    // 不填则默认选择所有每日任务

    String nowDay = DateFormat('yyyy-MM-dd').format(DateTime.now());
    YBDMyDailyTaskEntity? dailyCheckEntity =
        await YBDHttpUtil.getInstance().doGet<YBDMyDailyTaskEntity>(context, YBDApi.THE_TASK, params: {
      'begin': nowDay,
      'end': nowDay,
      'view': viewing,
    });

    if (dailyCheckEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request daily task success');
      return dailyCheckEntity;
    } else {
      logger.v('request daily task failed');
      return null;
    }
  }

  ///  完成任务后，领取奖励接口
  Future<bool> claimReward(BuildContext context, int? taskId, {String? taskName}) async {
    if (null == taskId) {
      logger.v('task id is null');
      taskId = 0;
    }

    logger.v('Start claiming reward, task id: $taskId, task name: $taskName');
    YBDDailyTaskEntity? dailyCheckEntity = await YBDHttpUtil.getInstance().doGet<YBDDailyTaskEntity>(context, YBDApi.COMPLETE_TASK, params: {'userMissionId': taskId});

    if (dailyCheckEntity?.returnCode == Const.HTTP_SUCCESS) {
      YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.COMPLETE_TASK, itemID: '$taskId', itemName: taskName));
      YBDCommonUtil.saveReceiveExtraBeans(context);

      return true;
    } else {
      YBDToastUtil.toast("${dailyCheckEntity?.returnMsg ?? ''}");
      return false;
    }
  }
  void claimRewardpZx65oyelive(BuildContext context, int? taskId, {String? taskName})  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  ///  完成任务后，领取奖励接口
  Future<bool> claimDailyReward(BuildContext context, int? taskId) async {
    if (null == taskId) {
      logger.v('task id is null');
      taskId = 0;
    }

    logger.v('Start claiming daily reward, task id: $taskId, task name: ');
    YBDDailyTaskEntity? dailyCheckEntity =
        await YBDHttpUtil.getInstance().doPost<YBDDailyTaskEntity>(context, YBDApi.COMPLETE_DAILY_TASK, {'dailyId	': taskId});

    if (dailyCheckEntity?.returnCode == Const.HTTP_SUCCESS) {
      // YBDAnalyticsUtil.logEvent(YBDAnalyticsEvent(YBDEventName.COMPLETE_TASK, itemID: '$taskId', itemName: taskName));
      eventBus.fire(YBDGameMissionEvent());
      return true;
    } else {
      return false;
    }
  }

  /// 礼物列表接口
  Future<YBDGiftListEntity?> giftList(BuildContext? context) async {
    YBDGiftListEntity? giftListEntity = await YBDHttpUtil.getInstance().doGet<YBDGiftListEntity>(context, YBDApi.GIFT_LIST);

    if (giftListEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request gift list success');

      if (null != giftListEntity && null != giftListEntity.record && giftListEntity.record!.isNotEmpty) {
        // 保存礼物列表数据
        logger.v('===download save gift list');
        YBDLiveService.instance.giftList = giftListEntity.record![0]!.gift;
      }

      return giftListEntity;
    } else {
      logger.v('request gift list failed');
      return null;
    }
  }

  /// Discount列表接口
  Future<YBDDiscountListEntity?> discountList(BuildContext? context, {bool page = false}) async {
    YBDDiscountListEntity? discountListEntity = await YBDHttpUtil.getInstance().doGet<YBDDiscountListEntity>(context, YBDApi.STORE_DISCOUNT_LIST, params: {'page': page});

    if (discountListEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request store discount list success');
      return discountListEntity;
    } else {
      logger.v('request store discount list failed');
      return null;
    }
  }

  /// 汽车列表接口
  Future<YBDCarListEntity?> carList(
    BuildContext? context, {
    bool queryAll = false,
    int start = 0,
    int offset = Const.PAGE_SIZE,
  }) async {
    YBDCarListEntity? carListEntity = await YBDHttpUtil.getInstance().doGet<YBDCarListEntity>(context, YBDApi.CAR_LIST, params: {
      'queryType': 2,
      'queryAll': queryAll,
      'start': start,
      'offset': offset,
    });

    if (carListEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request car list success');

      if (null != carListEntity) {
        logger.v('===download save car list');
        YBDLiveService.instance.carList = carListEntity.record;
      }

      return carListEntity;
    } else {
      logger.v('request car list failed');
      return null;
    }
  }

  /// 指定用户已关注的用户 id 列表
  /// userId：指定用户
  Future<YBDFollowInfoEntity?> followedInfoList(
    BuildContext context,
    int queryType,
    int start,
    int offset,
    String userId,
    int concernType,
  ) async {
    YBDQueryFollowedRespEntity? resp =
        await queryFollowed(context, queryType, start, offset, int.parse(userId), concernType);
    YBDFollowInfoEntity? followInfoEntity = await Future.value(JsonConvert.fromJsonAsT<YBDFollowInfoEntity>(resp?.toJson() ?? ''));

    if (followInfoEntity?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request followed id list success');
      return followInfoEntity;
    } else {
      logger.v('request followed id list failed');
      return null;
    }
  }

  /// 查询 关注的用户列表
  Future<YBDQueryFollowedRespEntity?> queryFollowed(
    BuildContext context,
    int queryType,
    int start,
    int offset,
    int? userId,
    int concernType,
  ) async {
    YBDQueryFollowedRespEntity? resp = await YBDHttpUtil.getInstance().doGet(
      context,
      YBDApi.QUERY_FOLLOWED,
      params: {
        'queryType': queryType,
        'start': start,
        'offset': offset,
        'userId': userId,
        'concernType': concernType,
      },
    );

    if (resp?.returnCode == Const.HTTP_SUCCESS) {
      Store<YBDAppState> store = YBDCommonUtil.storeFromContext(context: context)!;

      /// 更新redux followedIds
      Set<int?> followedIds = store.state.followedIds ?? Set();

      resp!.record?.users?.forEach((element) {
        followedIds.add(element!.id);
      });

      store.dispatch(YBDUpdateFollowedIdsAction(followedIds));
    }

    return resp;
  }

  /// 查询 推荐主播列表
  Future<YBDQueryStarRespEntity?> queryStar(BuildContext context, int start, int offset, int timeType) async {
    YBDQueryStarRespEntity? resp = await YBDHttpUtil.getInstance().doGet(context, YBDApi.QUERY_STAR, params: {
      'start': start,
      'offset': offset,
      'timeType': timeType,
    });
    return resp;
  }

  /// 查询Followers数据  http://www.tkvivo.com/um/queryconcern?queryType=2&start=0&offset=15&roomId=2099934&concernType=1
  Future<List<YBDUserInfo?>?> queryFollowersConcern(BuildContext context, int start, int offset, int roomId) async {
    YBDQueryFollowersEntity? result = await YBDHttpUtil.getInstance().doGet<YBDQueryFollowersEntity>(context, YBDApi.QUERY_FOLLOWED,
        params: {'queryType': 2, 'start': start, 'offset': offset, 'roomId': roomId, 'concernType': 1});

    if (result == null || result.returnCode != Const.HTTP_SUCCESS) {
      logger.v('query queryFollowersConcern failed ${Const.HTTP_SUCCESS}');
      return null;
    } else {
      logger.v('query user info success');
      return result.record!.users;
    }
  }

  /// 上报设备ID
  Future<YBDDeviceUniqueID?> sentDeviceUniqueID(
    BuildContext? context,
    Map<String, dynamic>? deviceUniqueID, {
    int? timeout,
  }) async {
    print('Android Device   004001 ${YBDDateUtil.currentUtcTimestamp()}');
    YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
      context,
      YBDApi.POST_DEVICE_UNIQUE_ID,
      deviceUniqueID,
      isJsonData: true,
      needErrorToast: false,
      connectTimeout: timeout,
    );
    print('Android Device   004002 ${YBDDateUtil.currentUtcTimestamp()}');
    if (null == resultBean || resultBean.returnCode != Const.HTTP_SUCCESS) {
      logger.v('request UniqueID info failed ${resultBean?.returnMsg}');
      return null;
    } else {
      logger.v('request UniqueID info  success');
      print('Android Device   004 ${YBDDateUtil.currentUtcTimestamp()}');
      YBDDeviceUniqueID deviceUniqueID = YBDDeviceUniqueID.fromMap(resultBean.record);
      print(
          'request UniqueID info  success  isBlocked ${deviceUniqueID.blocked}  isNewDevice ${deviceUniqueID.newDevice} content ${deviceUniqueID.content}');
      return deviceUniqueID;
    }
  }

  /// 查询用户充值状态
  Future<YBDTopUpSummaryEntity?> queryTopUpSummary(BuildContext? context) async {
    YBDTopUpSummaryEntity? result = await YBDHttpUtil.getInstance().doGet(context, YBDApi.TOP_UP_SUMMARY);
    return result;
  }

  /// 查询充值记录
  Future<YBDQueryRechargeRecordEntity?> queryRechargeRecord(
      BuildContext context, String startDate, String endDate, int? nextTime) async {
    YBDQueryRechargeRecordEntity? result = await YBDHttpUtil.getInstance().doGet(
      context,
      YBDApi.TOP_UP_RECORD,
      params: {
        'searchType': 'app',
        'begin': startDate,
        'end': endDate,
        'direction': 'after',
        'nextTime': nextTime,
        'userId': await YBDUserUtil.userId()
      },
      baseUrl: Const.TEST_ENV ? await YBDStatusApi.getBaseUrl : await YBDApi.getBaseUrl,
    );
    return result;
  }

  /// 查询活动中心列表
  Future<List<YBDEventCenterDataEventInfos?>?> queryEventCenterList({
    int? start,
    int? offset,
    String? eventStatus,
  }) async {
    YBDEventCenterEntity? result = await YBDHttpUtil.getInstance().doGet(
      Get.context,
      YBDApi.EVENT_CENTER_LIST,
      params: {
        'isPage': 'true',
        'start': start,
        'offset': offset,
        'country': YBDCommonUtil.storeFromContext()!.state.bean?.country ?? '',
        'eventStatus': eventStatus,
      },
      baseUrl: Const.TEST_ENV ? await YBDStatusApi.getBaseUrl : await YBDApi.getBaseUrl,
    );

    if (result?.code == Const.HTTP_SUCCESS_NEW) {
      return result!.data!.eventInfos;
    }

    return [];
  }

  /// 获取好友列表
  Future<YBDFriendListEntity?> friendList(BuildContext context,
      {String? fromId, int? page, int? pageSize, String? keyWord, bool enablePage: false}) async {
    Map<String, dynamic> params = {
      "start": (page == null || pageSize == null) ? null : pageSize * page,
      "offset": pageSize,
      "keyword": keyWord,
      "enablePage": enablePage
    }..removeWhere((key, value) => value == null);

    if (params == null) params = {};
    if (null != fromId && fromId.isNotEmpty) {
      params['fromId'] = fromId;
    } else {
      params['fromId'] = YBDUserUtil.getUserIdSync;
    }

    YBDFriendListEntity? result = await YBDHttpUtil.getInstance().doGet<YBDFriendListEntity>(
      context,
      YBDApi.QUERY_FRIEND_LIST,
      params: params,
    );

    if (result == null || result.returnCode != Const.HTTP_SUCCESS) {
      logger.v('query friend list failed  ${Const.HTTP_SUCCESS}');
      return null;
    } else {
      logger.v('query friend list success');
      return result;
    }
  }

  /// 查询是否为好友
  Future<bool> queryFriendFromId(BuildContext? context, String? fromId, String toId) async {
    Map<String, dynamic> params = {
      'fromId': fromId,
      'toId': toId,
    };

    YBDQueryFriendEntity? result = await YBDHttpUtil.getInstance().doGet<YBDQueryFriendEntity>(
      context,
      YBDApi.QUERY_FRIEND,
      params: params,
    );

    if (result == null || result.returnCode != Const.HTTP_SUCCESS) {
      logger.v('query friend failed  ${Const.HTTP_SUCCESS}');
      return false;
    } else {
      logger.v('query friend success');

      // record 为 null 时表示非好友
      if (null == result.record) {
        return false;
      } else {
        return true;
      }
    }
  }
  void queryFriendFromIdGXBeVoyelive(BuildContext? context, String? fromId, String toId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询Vip
  Future<YBDVipInfoEntity?> queryVip(BuildContext context) async {
    YBDVipInfoEntity? result = await YBDHttpUtil.getInstance().doGet<YBDVipInfoEntity>(
      context,
      YBDApi.QUERY_VIP,
      params: {'vipStatus': 1},
    );

    return result;
  }

  /// 查询Discount
  Future<YBDDiscountInfoEntity?> queryVipDisCount(BuildContext context) async {
    YBDDiscountInfoEntity? result = await YBDHttpUtil.getInstance().doGet<YBDDiscountInfoEntity>(
      context,
      YBDApi.QUERY_DISCOUNT,
      params: {'goodsType': 'VIP'},
    );

    if (result != null && result.returnCode == Const.HTTP_SUCCESS) {
      YBDSPUtil.save(Const.SP_DISCOUNT_VIP, json.encode(result.record));
    }
    return result;
  }

  Future<YBDVipValidInfoEntity?> queryVipExpireDate(BuildContext context) async {
    YBDVipValidInfoEntity? result = await YBDHttpUtil.getInstance().doGet<YBDVipValidInfoEntity>(
      context,
      YBDApi.QUERY_PERSONAL,
      params: {'queryType': -1, 'goodsType': 'VIP', 'userId': await YBDUserUtil.userId()},
    );

    return result;
  }

  Future<YBDVipValidInfoEntity?> queryUserVipInfo(BuildContext? context, int? userId) async {
    YBDVipValidInfoEntity? result = await YBDHttpUtil.getInstance().doGet<YBDVipValidInfoEntity>(
      context,
      YBDApi.QUERY_PERSONAL,
      params: {'queryType': -1, 'goodsType': 'VIP', 'userId': userId},
    );

    return result;
  }

  Future<YBDBaggageEntranceEntity?> queryUserEntranceInfo(BuildContext context, int? userId) async {
    YBDBaggageEntranceEntity? result = await YBDHttpUtil.getInstance().doGet<YBDBaggageEntranceEntity>(
      context,
      YBDApi.QUERY_PERSONAL,
      params: {'queryType': 2, 'goodsType': 'CAR', 'userId': userId},
    );

    return result;
  }

  Future<YBDBaggageThemesEntity?> queryUserThemeInfo(BuildContext? context, {userId}) async {
    YBDBaggageThemesEntity? result = await YBDHttpUtil.getInstance().doGet<YBDBaggageThemesEntity>(
      context,
      YBDApi.QUERY_PERSONAL,
      params: {'goodsType': 'THEME', 'userId': userId ?? await YBDUserUtil.userId()},
    );

    return result;
  }

  Future<YBDBaggageFrameEntity?> queryUserFrameInfo(BuildContext? context, {userId}) async {
    YBDBaggageFrameEntity? result = await YBDHttpUtil.getInstance().doGet<YBDBaggageFrameEntity>(
      context,
      YBDApi.QUERY_PERSONAL,
      params: {'goodsType': 'FRAME', 'userId': await userId ?? YBDUserUtil.userId()},
    );

    return result;
  }

  //背包礼物
  Future<YBDBaggageGiftEntity?> queryUserGiftsInfo(BuildContext? context, {userId}) async {
    YBDBaggageGiftEntity? result = await YBDHttpUtil.getInstance().doGet<YBDBaggageGiftEntity>(
      context,
      YBDApi.QUERY_PERSONAL,
      params: {'goodsType': 'GIFT', 'userId': userId ?? await YBDUserUtil.userId()},
    );

    return result;
  }

  /// 获取商店Mic Frame
  Future<YBDMicFrameEntity?> queryFrameList(BuildContext? context, {int start = 0, int offset = Const.PAGE_SIZE}) async {
    Map<String, dynamic> params = {
      'start': start,
      'offset': offset,
    };
    YBDMicFrameEntity? result =
        await YBDHttpUtil.getInstance().doGet<YBDMicFrameEntity>(context, YBDApi.QUERY_MIC_FRAME, params: params);

    if (result?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request car list success');
      return result;
    } else {
      logger.v('request car list failed');
      return null;
    }
  }

  /// 获取商店bubble
  Future<YBDBubbleEntity?> queryBubbleList(BuildContext? context, {int start = 0, int offset = 50}) async {
    Map<String, dynamic> params = {
      // 'start': start,
      // 'offset': offset,
      'queryType': -1
    };
    YBDBubbleEntity? result = await YBDHttpUtil.getInstance().doGet<YBDBubbleEntity>(context, YBDApi.QUERY_BUBBLE, params: params);

    if (result?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request car list success');
      return result;
    } else {
      logger.v('request car list failed');
      return null;
    }
  }

  /// 获取商店 Theme
  Future<YBDThemeEntity?> queryThemeList(BuildContext? context, {int start = 0, int offset = Const.PAGE_SIZE}) async {
    Map<String, dynamic> params = {
      'start': start,
      'offset': offset,
    };
    YBDThemeEntity? result = await YBDHttpUtil.getInstance().doGet<YBDThemeEntity>(context, YBDApi.QUERY_THEME, params: params);

    if (result?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request car list success');
      return result;
    } else {
      logger.v('request car list failed');
      return null;
    }
  }

  /// 获取可配置的活动列表
  Future<YBDConfigurableActivityEntity?> queryConfigActivityList(
    BuildContext context, {
    bool isNewUser = false,
    bool firstLogin = false,
  }) async {
    Map<String, dynamic> params = {
      'isNewUser': isNewUser,
      'isFirstLoginToday': firstLogin,
    };

    YBDConfigurableActivityEntity? result = await YBDHttpUtil.getInstance().doGet<YBDConfigurableActivityEntity>(
      context,
      YBDApi.QUERY_CONFIG_ACTIVITY,
      params: params,
    );

    if (result?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request config activity list success');
      return result;
    } else {
      logger.v('request config activity list failed');
      return null;
    }
  }

  /// 获取商店 QUERY_DISCOUNT
  Future<YBDDiscountEntity?> queryDiscountList(BuildContext? context, String goodsType) async {
    if (goodsType == null || goodsType.isEmpty) {
      logger.v('goodsType is null');
    }
    Map<String, dynamic> params = {
      'goodsType': goodsType,
    };
    YBDDiscountEntity? result =
        await YBDHttpUtil.getInstance().doGet<YBDDiscountEntity>(context, YBDApi.QUERY_DISCOUNT, params: params);

    if (result?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('request Discount list success');
      logger.v('request Discount list success  ${result!.record}  ${result.record!.priceInfo}');
      print('result------------------- ${result.record}');
      Map data = new Map<String, dynamic>.from(result.record!.priceInfo); //修改
      logger.v('request Discount list success data:$data');
      List<dynamic>? sds = data['-1'];
      logger.v('request Discount list success sds:$sds');
      /* YBDDiscountRecordPriceInfo_1 discountRecordPriceInfo_1;
      List<YBDDiscountRecordPriceInfo_1> priceInfoList = [];
      for(int i = 0 ;i <sds.length;i++){
        discountRecordPriceInfo_1 = sds[i] as YBDDiscountRecordPriceInfo_1;
        priceInfoList.add(discountRecordPriceInfo_1);
        logger.v('request Discount list success sds:$discountRecordPriceInfo_1');
      }*/
      return result;
    } else {
      logger.v('request Discount list failed');
      return null;
    }
  }

  /// 商店 购买座驾
  Future<YBDDiscountEntity?> buyCar(BuildContext context, int? carId, String? unit, {bool isSpecial = false}) async {
    if (unit == null || unit.isEmpty) {
      logger.v('unit is null');
    }
    Map<String, dynamic> params = {'carId': carId, 'unit': unit, 'isSpecial': isSpecial};
    YBDDiscountEntity? result = await YBDHttpUtil.getInstance().doGet<YBDDiscountEntity>(context, YBDApi.ORDER_CAR, params: params);
    return result;
  }

  /// 商店 购买Theme Frame
  ///  @GET("um/purchase")
  //    Observable<BaseResp> purchase(@Query("goodsId") int goodsId, @Query("number") int number, @Query("period") int period);
  Future<YBDDiscountEntity?> purchase(BuildContext context, int? goodsId, int number, String? period,
      {bool isSpecial = false}) async {
    logger.v('purchase goodsId $goodsId ,number $number ,period $period');
    if (period == null || period.isEmpty) {
      logger.v('period is null');
    }
    Map<String, dynamic> params = {
      'goodsId': goodsId,
      'number': number,
      'period': period,
      'isSpecial': isSpecial,
    };
    YBDDiscountEntity? result = await YBDHttpUtil.getInstance().doGet<YBDDiscountEntity>(context, YBDApi.PURCHASE, params: params);
    return result;
    /*if (result?.returnCode == Const.HTTP_SUCCESS) {
      logger.v('purchase success');
      return true;
    } else {
      logger.v('purchase failed');
      return false;
    }*/
  }

  Future<YBDBuyVipResultEntity?> buyVip(BuildContext context, int? vipId, String? unit) async {
    YBDBuyVipResultEntity? result = await YBDHttpUtil.getInstance().doGet<YBDBuyVipResultEntity>(context, YBDApi.ORDER_VIP, params: {'vipId': vipId, 'unit': unit});
    getSelfEmoji();
    return result;
  }

  /// 查询用户信息
  Future<YBDRoomInfo?> queryOnlineInfo(
    BuildContext? context,
    int? roomId,
  ) async {
    Map<String, dynamic> params = {'id': roomId};

    YBDQueryRoomRespEntity? result =
        await YBDHttpUtil.getInstance().doGet(context, YBDApi.QUERY_ONLINE, params: params, needErrorToast: true);

    if (result == null || result.returnCode != Const.HTTP_SUCCESS) {
      logger.v('query room info failed');
      return null;
    } else {
      return result.record;
    }
  }

  /// 查询用户信息
  Future<List<YBDUserInfo?>?> queryRoomManager(
    BuildContext? context,
    int? roomId,
  ) async {
    Map<String, dynamic> params = {'roomId': roomId, 'queryType': -1};
    YBDQueryUserRespEntity? result =
        await YBDHttpUtil.getInstance().doGet(context, YBDApi.QUERY_ROOM_MANAGER, params: params, needErrorToast: true);
    return result?.record;
  }

  /// 查询 playCenter-Game
  /// countryCode 通过IP获取的code
  Future<YBDGameEntity?> queryGame(BuildContext? context, String countryCode) async {
    Map<String, dynamic> params = {
      'countryCode': countryCode,
    };
    YBDGameEntity? result = await YBDHttpUtil.getInstance().doGet<YBDGameEntity>(context, YBDApi.QUERY_GAME, params: params);
    return result;
  }

  /// 获取背包--theme&&frame
  Future<YBDPersonalFrameEntity?> queryPersonalFrame(BuildContext context, int? userId, String goodsType) async {
    Map<String, dynamic> params = {
      'userId': userId,
      'goodsType': goodsType,
    };
    YBDPersonalFrameEntity? result =
        await YBDHttpUtil.getInstance().doGet<YBDPersonalFrameEntity>(context, YBDApi.QUERY_PERSONAL, params: params);
    return result;
  }

  /// 装备背包--theme&&frame
  Future<YBDDiscountInfoEntity?> equip(BuildContext context, int? personalId, int? roomId, String? goodsType) async {
    Map<String, dynamic> params = {
      'personalId': personalId,
      'roomId': roomId,
      'goodsType': goodsType,
    };
    YBDDiscountInfoEntity? result =
        await YBDHttpUtil.getInstance().doGet<YBDDiscountInfoEntity>(context, YBDApi.EQUIP, params: params);
    return result;
  }

  /// 取消装备背包--theme&&frame
  Future<YBDDiscountInfoEntity?> unEquip(BuildContext context, int? personalId, int? roomId, String? goodsType) async {
    Map<String, dynamic> params = {
      'personalId': personalId,
      'roomId': roomId,
      'goodsType': goodsType,
    };
    YBDDiscountInfoEntity? result =
        await YBDHttpUtil.getInstance().doGet<YBDDiscountInfoEntity>(context, YBDApi.UN_EQUIP, params: params);
    return result;
  }

  /// 公共支付预下单---RazorPay
  //  Future<YBDRazorpayResultEntity> razorPayOrders(
  //     BuildContext context, String targetId, String money, String currency) async {
  //   logger.v('razorPayOrders parameter is invalid targetId:$targetId money:$money currency$currency');
  //   if (context == null || currency == null || money == null) {
  //     logger.v('payOrders parameter is invalid context:$context currency:$currency money$money');
  //     return null;
  //   }
  //   Map<String, dynamic> params = {
  //     'userId': await YBDUserUtil.userId(),
  //     'targetId': targetId,
  //     'money': money,
  //     'type': Const.RAZOR_PAY,
  //     'currency': currency,
  //   };
  //   YBDRazorpayResultEntity result = await YBDHttpUtil.getInstance()
  //       .doPost<YBDRazorpayResultEntity>(context, YBDApi.PAY_ORDERS, params, baseUrl: Const.ORDERS_URL, isJsonData: true);
  //   return result;
  // }

  /// 公共支付预下单---RazorPay--结果传递服务器
//    Future<bool> razorPayResultSu(BuildContext context, String paymentId, String orderId, String signature) async {
//     Map<String, dynamic> params = {
// //      'userId': await YBDUserUtil.userId(),
//       'razorpay_payment_id': paymentId,
//       'razorpay_order_id': orderId,
//       'razorpay_signature': signature,
//     };
//     YBDResultBeanEntity result = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
//         context, YBDApi.PAY_ORDERS_NOTIFY_RESULT_SU, params,
//         baseUrl: Const.ORDERS_URL, isJsonData: true);
//     if (result?.returnCode == Const.HTTP_SUCCESS) {
//       logger.v('razorPayResult success');
//       return true;
//     } else {
//       logger.v('razorPayResult failed');
//       return false;
//     }
//   }

  /// 更新用户设备国家
  Future<YBDResultBeanEntity?> updateLocation(BuildContext? context, String? selectCountry,
      {String? deviceCountry, String? deviceCity, int modifyByUser = 0}) async {
    Map<String, dynamic> params = {};
    if ((selectCountry ?? '').isNotEmpty) params.addAll({'selectCountry': selectCountry});
    if ((deviceCountry ?? '').isNotEmpty) params.addAll({'country': deviceCountry});
    if ((deviceCity ?? '').isNotEmpty) params.addAll({'city': deviceCity});
    params.addAll({'modifyByUser': modifyByUser});

    ///用户主动修改：    0：否（默认）  1：是

    YBDResultBeanEntity? result =
        await YBDHttpUtil.getInstance().doGet(context, YBDApi.ADD_LOCATION, params: params, needErrorToast: false);
    return result;
//    return result?.returnCode == Const.HTTP_SUCCESS;
  }

  ///获取房间黑名单列表
  Future<YBDQueryRoomBlacklistRespEntity?> queryRoomBlacklist(BuildContext? context, int? roomId,
      {int start: 0, int offset: Const.PAGE_SIZE}) async {
    return YBDHttpUtil.getInstance().doGet<YBDQueryRoomBlacklistRespEntity>(context, YBDApi.QUERY_ROOM_BLACKLIST,
        baseUrl: await YBDApi.getBaseUrl, params: {'talentId': roomId, 'start': start, 'offset': offset});
  }

  ///获取房间黑名单列表
  Future<bool> deleteRoomBlacklist(BuildContext? context, int? recordId) async {
    YBDResultBeanEntity? result = await YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(context, YBDApi.DELETE_ROOM_BLACKLIST,
        baseUrl: await YBDApi.getBaseUrl, params: {'id': recordId});
    return result?.returnCode == Const.HTTP_SUCCESS;
  }
  void deleteRoomBlackliststritoyelive(BuildContext? context, int? recordId)  {

     int needCount = 0;
     print('input result:$needCount');
  }
  

  /// 查询我守护的房间
  Future<YBDQueryRoomListRespEntity?> queryGuardedRooms(BuildContext context) async {
    return YBDHttpUtil.getInstance().doGet<YBDQueryRoomListRespEntity>(context, YBDApi.QUERY_GUARDED_ROOM,
        baseUrl: await YBDApi.getBaseUrl, params: {'managerId': YBDUserUtil.getLoggedUserID(context)});
  }

  /// 查询我的守护
  Future<YBDQueryUserRespEntity?> queryMyGuardians(BuildContext context) async {
    return YBDHttpUtil.getInstance().doGet<YBDQueryUserRespEntity>(context, YBDApi.QUERY_GUARDIAN,
        baseUrl: await YBDApi.getBaseUrl, params: {'queryType': -1, 'roomId': YBDUserUtil.getLoggedUserID(context)});
  }

  /// 移除指定守护
  Future<YBDResultBeanEntity?> deleteMyGuardian(BuildContext context, userId) async {
    return YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(context, YBDApi.DEL_GUARDIAN,
        baseUrl: await YBDApi.getBaseUrl, params: {'managerId': userId});
  }

  /// 获取苹果支付的配置信息
  Future<YBDApplePayConfigEntity?> queryApplePayConfig(BuildContext? context) async {
    Map<String, dynamic> params = {
      'dataId': 'pay_configs',
      'key': 'apple',
    };

    YBDApplePayConfigEntity? result = await YBDHttpUtil.getInstance().doGet<YBDApplePayConfigEntity>(
      context,
      YBDPaymentApi.PAY_APPLE_PAY_CONFIG,
      params: params,
      baseUrl: YBDApi.basePayUrl,
      needErrorToast: false,
    );

    if (result == null || result.code != Const.HTTP_SUCCESS_NEW) {
      logger.v('query apple pay config failed : ${result!.code}');
      return null;
    } else {
      logger.v('query apple pay config success');
      return result;
    }
  }

  /// 预下单
  Future<YBDPreOrderEntity?> requestPreOrder(
    BuildContext? context, {
    String? userId,
    double? money,
    String? currency,
    String? productId,
    String? type,
    int connectTimeout = 30 * 1000,
  }) async {
    Map<String, dynamic> params = {
      'userId': '$userId',
      'money': money,
      'type': type,
      'currency': currency,
      'contextMap': {'productId': productId},
    };

    YBDPreOrderEntity? result = await YBDHttpUtil.getInstance().doPost<YBDPreOrderEntity>(
      context,
      YBDPaymentApi.PAY_APPLE_PRE_ORDER,
      params,
      baseUrl: YBDApi.basePayUrl,
      needErrorToast: false,
      isJsonData: true,
      connectTimeout: connectTimeout,
    );

    if (result == null || result.code != Const.HTTP_SUCCESS_NEW) {
      logger.v('pre order failed : ${result!.code}');
      return null;
    } else {
      logger.v('pre order success');
      return result;
    }
  }

  /// 分发苹果支付购买内容
  Future<String> requestDeliverApplePurchase(
    BuildContext? context, {
    required String orderId,
    String? receipt,
    String? transactionId,
      }) async {
    Map<String, dynamic> params = {
      'payType': 'apple',
      'command': 'success',
      'receipt': receipt,
    };

    YBDDeliverEntity? result = await YBDHttpUtil.getInstance().doPatch<YBDDeliverEntity>(
      context,
      YBDPaymentApi.PAY_APPLE_DELIVER_PRODUCT + orderId,
      params,
      baseUrl: YBDApi.basePayUrl,
      isJsonData: true,
      needErrorToast: false,
    );

    if (result == null || result.code != Const.HTTP_SUCCESS_NEW) {
      logger.v('deliver purchase failed : ${result!.code}');
      return YBDIapPayResult.DELIVER_FAILED;
    } else {
      logger.v('deliver purchase success');
      return YBDIapPayResult.DELIVER_SUCCESS;
    }
  }

  /// 取消预下单的订单
  Future<bool> requestCancelApplePurchase(
    BuildContext? context, {
    required String orderId,
  }) async {
    Map<String, dynamic> params = {
      'payType': 'apple',
      'command': 'failure',
    };

    YBDPreOrderEntity? result = await YBDHttpUtil.getInstance().doPatch<YBDPreOrderEntity>(
      context,
      YBDPaymentApi.PAY_APPLE_DELIVER_PRODUCT + orderId,
      params,
      baseUrl: YBDApi.basePayUrl,
      isJsonData: true,
      needErrorToast: false,
    );

    if (result == null || result.code != Const.HTTP_SUCCESS_NEW) {
      logger.v('deliver purchase failed : ${result!.code}');
      return true;
    } else {
      logger.v('deliver purchase success');
      return false;
    }
  }

  Future<YBDGameInfoEntity?> getGameInfo(
    BuildContext context,
  ) async {
    return YBDHttpUtil.getInstance().doGet<YBDGameInfoEntity>(context, YBDApi.GAME_INFO,
        baseUrl: await YBDApi.getBaseUrl,
        params: {'countryCode': await YBDSPUtil.get(Const.SP_IP_COUNTRY_CODE), 'gameLocation': 1});
  }

  Future<YBDResultBeanEntity?> equipItem(BuildContext context, {personalId, goodsType, roomId}) async {
    return YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(context, YBDApi.EQUIP,
        baseUrl: await YBDApi.getBaseUrl, params: {'personalId': personalId, 'goodsType': goodsType, 'roomId': roomId});
  }

  Future<YBDResultBeanEntity?> unequipItem(BuildContext context, {personalId, goodsType, roomId}) async {
    return YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(context, YBDApi.UNEQUIP,
        baseUrl: await YBDApi.getBaseUrl, params: {'personalId': personalId, 'goodsType': goodsType, 'roomId': roomId});
  }

  Future<YBDTeenPattiHistoryEntity?> teenPattiHistory(BuildContext context, {size = 10}) async {
    return YBDHttpUtil.getInstance().doGet<YBDTeenPattiHistoryEntity>(
      context,
      YBDApi.TEENPATTI_HISTORY,
      baseUrl: await YBDApi.getBaseUrl,
      params: {'size': size},
    );
  }

  Future<YBDTeenPattiREntity?> teenPattiRecord(BuildContext? context, {dateType, required bool isRich}) async {
    return YBDHttpUtil.getInstance().doGet<YBDTeenPattiREntity>(
      context,
      isRich ? YBDApi.TEENPATTI_Record_RichEST : YBDApi.TEENPATTI_Record_LUCKIEST,
      baseUrl: await YBDApi.getBaseUrl,
      params: {'dateType': dateType},
    );
  }

  Future<YBDInviteInfoEntity?> getInviteInfo(BuildContext context) async {
    return YBDHttpUtil.getInstance().doGet<YBDInviteInfoEntity>(
      context,
      YBDApi.INVITE_INFO,
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  Future<YBDBeanDetailEntity?> getBeansDetail(BuildContext context, {int pageSize: 10, int page: 1}) async {
    return YBDHttpUtil.getInstance().doGet<YBDBeanDetailEntity>(context, YBDApi.BEAN_DETAIL,
        baseUrl: await YBDApi.getBaseUrl, params: {'size': pageSize, 'page': page});
  }

  Future<YBDGemsDetailEntity?> getGemsDetail(BuildContext context, {int pageSize: 10, int page: 1}) async {
    return YBDHttpUtil.getInstance().doGet<YBDGemsDetailEntity>(context, YBDApi.GEMS_DETAIL,
        baseUrl: await YBDApi.getBaseUrl, params: {'size': pageSize, 'page': page});
  }

  Future<YBDGoldsDetailEntity?> getGoldsDetail(BuildContext context, {int pageSize: 10, int page: 1}) async {
    return YBDHttpUtil.getInstance().doGet<YBDGoldsDetailEntity>(context, YBDApi.GOLD_DETAIL,
        baseUrl: await YBDApi.getBaseUrl, params: {'size': pageSize, 'page': page});
  }

  Future<YBDMyInvitesEntity?> getMyInvites(BuildContext context, {int pageSize: 10, int page: 1}) async {
    return YBDHttpUtil.getInstance().doGet<YBDMyInvitesEntity>(context, YBDApi.MY_INVITE,
        baseUrl: await YBDApi.getBaseUrl, params: {'pageSize': pageSize, 'page': page});
  }

  Future<YBDResultBeanEntity?> editTalent(BuildContext context, {title}) async {
    return YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(context, YBDApi.EDIT_TALENT, baseUrl: await YBDApi.getBaseUrl, params: {'title': title});
  }

  /// 获取用户等级
  Future<YBDLevelInfoEntity?> queryLevel(BuildContext context) async {
    return YBDHttpUtil.getInstance().doGet<YBDLevelInfoEntity>(
      context,
      YBDApi.QUERY_LEVEL,
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  //* pk记录
  Future<YBDPKRecordEntity?> getPKRecords(BuildContext context, {int pageSize: 10, int page: 1}) async {
    return YBDHttpUtil.getInstance().doGet<YBDPKRecordEntity>(context, YBDApi.PK_RECORD,
        baseUrl: await YBDApi.getBaseUrl, params: {'size': pageSize, 'page': page});
  }

  ///用户是否pk房主播
  Future<YBDResultBeanEntity?> getPkState(BuildContext context) async {
    return YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(
      context,
      YBDApi.PK_STATE,
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  ///用户是否pk房主播
  Future<YBDResultBeanEntity?> checkUserPhone(BuildContext context) async {
    return YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(
      context,
      YBDApi.GET_USER_PHONE,
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  ///高风险修改密码
  Future<YBDResultBeanEntity?> changePwd(BuildContext context, Map content) async {
    return YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(context, YBDApi.CHANGE_PWD, content as Map<String, dynamic>?, baseUrl: await YBDApi.getBaseUrl, isJsonData: false);
  }

  ///获取用户余额, 并且更新本地余额
  getBalance(BuildContext? context, Function callback) async {
    getBalanceApi(context).then((value) {
      var money = value?.record?.bean;
      if (money == null) {
        return;
      }
      logger.i('Royal Pattern get balance: $money');
      YBDUserUtil.updateUserMoney(money);
      callback();
    });
  }

  Future<YBDBalanceRecordEntity?> getBalanceApi(BuildContext? context) async {
    return YBDHttpUtil.getInstance().doGet<YBDBalanceRecordEntity>(
      context,
      YBDApi.GET_BALANCE,
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  ///查询认证信息
  Future<YBDUserCertificationEntity?> queryCertification(BuildContext? context, List<int?> userIds) async {
    if (userIds.isEmpty) {
      return null;
    }
    return YBDHttpUtil.getInstance().doGet<YBDUserCertificationEntity>(context, YBDApi.QUERY_CERTIFICATION,
        baseUrl: await YBDApi.getBaseUrl, params: {'userIds': userIds.toString().replaceAll('[', '').replaceAll(']', '')});
  }

  /// teenpatti钻石兑换金币
  Future<YBDResultBeanEntity?> gemsExchangeBeans(BuildContext context, int number, String currency) async {
    Map<String, dynamic> params = {
      'number': number,
      'currency': currency,
    };
    return YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
      context,
      YBDApi.EXCHANGE,
      params,
      baseUrl: await YBDApi.getBaseUrl,
      isJsonData: true,
      connectTimeout: 5000,
      needErrorToast: true,
    );
  }

  /// teenpatti挽留
  Future<YBDTpSaveEntity?> isTpSave(BuildContext? context) async {
    return YBDHttpUtil.getInstance().doGet<YBDTpSaveEntity>(
      context,
      YBDApi.TP_SAVE,
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  /// teenpatti召回
  Future<YBDTpSaveEntity?> tpInviteOffline(BuildContext? context, id) async {
    return YBDHttpUtil.getInstance().doGet<YBDTpSaveEntity>(
      context,
      YBDApi.TP_INVITE_OFFLINE,
      params: {'id': id},
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  /// teenpatti召回
  Future<YBDResultBeanEntity?> tpInviteOfflineRetrieve(BuildContext context, id) async {
    return YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(
      context,
      YBDApi.TP_INVITE_OFFLINE_RETRIEVE,
      params: {'id': id},
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  /// 上报设备信息
  Future<void> sentPhoneInfo(
    BuildContext context,
    Map<String, dynamic> phoneInfo, {
    int? timeout,
  }) async {
    logger.d('sentPhoneInfo ${YBDDateUtil.currentUtcTimestamp()}');
    YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
      context,
      YBDApi.POST_PHONE_INFO,
      phoneInfo,
      isJsonData: true,
      needErrorToast: false,
      connectTimeout: timeout,
    );
    if (null == resultBean || resultBean.returnCode != Const.HTTP_SUCCESS) {
      logger.v('sentPhoneInfo failed ${resultBean?.returnMsg}');
    } else {
      logger.v('sentPhoneInfo info  success');
    }
  }

  uploadFcmToken(BuildContext context) async {
    YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
        context, YBDApi.FCM_TOKEN_UPLOAD, {'fcmToken': await YBDNotificationUtil.instance.getToken()},
        isJsonData: true, needErrorToast: false);
    return resultBean;
  }

  Future<YBDPkSettingEntity?> getPkSetting(BuildContext context) async {
    YBDPkSettingEntity? resultBean = await YBDHttpUtil.getInstance().doGet<YBDPkSettingEntity>(
      context,
      YBDApi.PK_SETTING,
    );
    return resultBean;
  }

  Future<YBDResultBeanEntity?> setPkSetting(BuildContext context, {bool? rejectFriends, bool? rejectSystem}) async {
    YBDResultBeanEntity? resultBean = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(context, YBDApi.PK_SETTING,
        {'rejectFriends': rejectFriends, 'rejectSystem': rejectSystem, 'timeZone': DateTime.now().timeZoneName});
    return resultBean;
  }

  Future<YBDFriendListEntity?> getTargetPkList(BuildContext context, bool isFriend, {int offset: 20, int start: 0}) async {
    YBDFriendListEntity? resultBean = await YBDHttpUtil.getInstance().doGet<YBDFriendListEntity>(
        context, isFriend ? YBDApi.TARGET_PK_INVITE_F_HISTORY : YBDApi.TARGET_PK_INVITE_HISTORY,
        params: {
          'start': start,
          'offset': offset,
        });
    return resultBean;
  }

  Future<YBDBaseRespEntity?> queryRemainingTimes(BuildContext? context) async {
    return YBDHttpUtil.getInstance().doGet<YBDBaseRespEntity>(
      context,
      YBDApi.QUERY_RENAME_TIMES,
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  Future<YBDUidListEntity?> queryUidList(BuildContext? context) async {
    return YBDHttpUtil.getInstance().doGet<YBDUidListEntity>(context, YBDApi.QUERY_UID, baseUrl: await YBDApi.getBaseUrl, params: {'queryType': -1});
  }

  Future<YBDBuyVipResultEntity?> buyUid(BuildContext context, uniqueId, unit, {bool isRenew: false}) async {
    return YBDHttpUtil.getInstance().doPost<YBDBuyVipResultEntity>(
      context,
      isRenew ? YBDApi.RENEW_UID : YBDApi.BUY_UID,
      {
        'uniqueId': uniqueId,
        'unit': unit,
      },
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  Future<YBDBuyVipResultEntity?> buyBubble(BuildContext context, bubbleId, unit) async {
    return YBDHttpUtil.getInstance().doPost<YBDBuyVipResultEntity>(
      context,
      YBDApi.ORDER_BUBBLE,
      {
        'bubbleId': bubbleId,
        'unit': unit,
      },
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  Future<YBDBaggageUidEntity?> queryBaggageUidList(BuildContext? context, {userId}) async {
    return YBDHttpUtil.getInstance().doGet<YBDBaggageUidEntity>(context, YBDApi.QUERY_PERSONAL,
        baseUrl: await YBDApi.getBaseUrl,
        params: {'queryType': -1, 'goodsType': 'UNIQUEID', 'userId': userId ?? await YBDUserUtil.userId()});
  }

  Future<YBDBaggageBubbleEntity?> queryBaggageBubbleList(BuildContext? context, {userId}) async {
    return YBDHttpUtil.getInstance().doGet<YBDBaggageBubbleEntity>(context, YBDApi.QUERY_PERSONAL,
        baseUrl: await YBDApi.getBaseUrl,
        params: {'queryType': -1, 'goodsType': 'BUBBLE', 'userId': userId ?? await YBDUserUtil.userId()});
  }

  /// 获取机构code
  Future<YBDZegoCodeEntity?> getZegoCode(BuildContext? context) async {
    return YBDHttpUtil.getInstance().doGet<YBDZegoCodeEntity>(
      context,
      YBDApi.ZEGO_GET_CODE,
      baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null,
    );
  }

  /// 获取即构小游戏appkey
  Future<YBDZegoMGAPPKeyEntity?> getZegoMgAppKey(BuildContext context) async {
    return YBDHttpUtil.getInstance().doGet<YBDZegoMGAPPKeyEntity>(
      context,
      YBDApi.ZEGO_MG_APP_KEY,
      baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null,
    );
  }

  /// 游戏分类查询
  Future<List<YBDGameCategoryData?>?> gameCategorySearch(
    BuildContext context,
  ) async {
    YBDAppVersionConfig? appConfig = await ConfigUtil.appVersionInfo(context);
    int configVersionCode = appConfig?.buildCode ?? 0;

    YBDGameCategoryEntity? resp = await YBDHttpUtil.getInstance().doGet(
      context,
      YBDApi.GAME_CATEGORY_SEARCH,
      params: {
        'category': 'game',
        'appVersion': '$configVersionCode',
      },
      needErrorToast: true,
      baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null,
    );

    if (resp?.code == Const.HTTP_SUCCESS_NEW) {
      return resp?.data;
    }
    return null;
  }

  /// 游戏分类查询
  Future<YBDGameLobbyEntity?> gameCategorySearchV2(
    BuildContext context,
  ) async {
    YBDAppVersionConfig? appConfig = await ConfigUtil.appVersionInfo(context);
    int configVersionCode = appConfig?.buildCode ?? 0;

    YBDGameLobbyEntity? resp = await YBDHttpUtil.getInstance().doGet(
      context,
      YBDApi.GAME_CATEGORY_SEARCH_V2,
      params: {
        'category': 'game',
        'appVersion': '$configVersionCode',
      },
      needErrorToast: true,
      baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null,
      // baseUrl: "http://192.168.5.198:9230/",
    );

    if (resp?.code == Const.HTTP_SUCCESS_NEW) {
      return resp;
    }
    return null;
  }

  /// 游戏分类查询
  Future<YBDQuickCommentData?> queryQuickComment(
    BuildContext context,
  ) async {
    YBDQuickCommentEntity resp = await YBDHttpUtil.getInstance().doGet(
      context,
      YBDApi.GAME_QUICK_COMMENT_LIST,
      params: {
        'category': 'game',
        'subCategory': 'tp',
      },
      needErrorToast: true,
      baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null,
      // baseUrl: "http://192.168.5.198:9230/",
    );

    if (resp.code == Const.HTTP_SUCCESS_NEW) {
      return resp.data?.first;
    }
    return null;
  }

  /// 游戏房间列表查询
  Future<List<YBDGameRoomEnterDataRows?>?> gameRoomEnterList(
    BuildContext context, {
    String category = 'game', // 分类 game、voice
    required String subCategory, // 游戏类别 ludo
    String? projectCode, // 项目编码，可以不传
    String? tenantCode, // 项目编码，可以不传
    int page = 1, // 当前页数
    int pageSize = Const.PAGE_SIZE, // 分页大小
  }) async {
    var params = {
      'category': category, //
      'subCategory': subCategory,
      'page': page,
      'pageSize': pageSize
    };
    params.appendNotEmpty('tenantCode', tenantCode);
    params.appendNotEmpty('projectCode', projectCode);

    YBDGameRoomEnterEntity? resp = await YBDHttpUtil.getInstance().doGet(
      context,
      YBDApi.GAME_ROOM_LIST,
      params: params,
      needErrorToast: true,
      baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null,
    );
    if (resp?.code == Const.HTTP_SUCCESS_NEW) {
      return resp?.data?.rows;
    }
    return null;
  }

  /// 游戏分类查询
  Future<YBDQuickJoinEntity?> quickJoin({String? subCategory}) async {
    var gameSub = subCategory ?? YBDRtcHelper.getInstance().selectedGame?.subCategory;

    YBDQuickJoinEntity? quickJoinEntity = await YBDHttpUtil.getInstance().doPost<YBDQuickJoinEntity>(
        Get.context, YBDApi.QUICK_JOIN, {'category': 'game', 'subCategory': gameSub ?? 'ludo'},
        baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null, isJsonData: true);
    return quickJoinEntity;
  }

  /// ludo人机局匹配
  Future<YBDQuickJoinEntity?> quickGame({String? subCategory, String? modeName}) async {
    var gameSub = subCategory ?? YBDRtcHelper.getInstance().selectedGame?.subCategory;

    YBDQuickJoinEntity? quickJoinEntity = await YBDHttpUtil.getInstance().doPost<YBDQuickJoinEntity>(
      Get.context,
      YBDApi.QUICK_GAME,
      {'category': 'game', 'subCategory': gameSub ?? 'ludo', 'modeName': modeName},
      baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null,
      isJsonData: true,
      needErrorToast: true,
    );
    return quickJoinEntity;
  }

  /// 游戏房间礼物列表查询
  Future<YBDGameGiftEntity?> gameRoomGiftList(BuildContext? context) async {
    YBDGameGiftEntity? resp = await YBDHttpUtil.getInstance().doGet(
      context,
      YBDApi.GAME_GIFT_LIST,
      baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null,
    );

    return resp;
  }

  /// 游戏房间配置信息
  Future<YBDGameConfigDisplayData?> gameRoomConfigDisplay(BuildContext? context, String category) async {
    YBDGameConfigDisplayEntity? resp = await YBDHttpUtil.getInstance().doGet(context, YBDApi.GAME_CONFIG_DISPLAY,
        baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null, params: {'category': category});
    if (resp?.code == Const.HTTP_SUCCESS_NEW) {
      return resp?.data;
    }
    return null;
  }

  /// 图片验证码
  Future<YBDImageCodeEntity?> getImageCode(BuildContext context, String? phone, String operation) async {
    YBDImageCodeEntity? resp = await YBDHttpUtil.getInstance().doGet(context, YBDApi.IMAGE_CODE,
        baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null, params: {'phone': phone, 'operation': operation});

    return resp;
  }

  /// 图片验证码发短信
  Future<YBDMobileCheckResp?> sendSMSCode(BuildContext context, String? phone, String imageCode, String operation) async {
    Map<String, dynamic> upMap = {'phone': phone, 'operation': operation, 'code': imageCode};
    YBDMobileCheckResp? mobileSignInEntity = await YBDHttpUtil.getInstance().doPost(context, YBDApi.GENERATE_TOKEN, upMap);
    YBDTATrack().trackMap(YBDEventName.SMS_POST, properties: upMap);
    return mobileSignInEntity;
  }

  /// 短信
  Future<YBDMobileCheckResp?> getSMSCode(BuildContext context, {String? phone, String? operation}) async {
    Map<String, dynamic> upMap = {'phone': phone, 'operation': operation};
    YBDMobileCheckResp? data = await YBDHttpUtil.getInstance().doGet(context, YBDApi.GENERATE_TOKEN, params: upMap);
    YBDTATrack().trackMap(YBDEventName.SMS_POST, properties: upMap);
    return data;
  }

  /// 首页金刚区
  Future<YBDKingKongEntity?> kingKongList(BuildContext context) async {
    YBDKingKongEntity? resp = await YBDHttpUtil.getInstance().doGet<YBDKingKongEntity>(
      context,
      YBDApi.KINGKONG,
      baseUrl: Const.TEST_ENV ? await YBDAgencyApi.getBaseUrl : null,
    );

    return resp;
  }

  /// 上传用户背景图片
  Future<YBDBaseRespEntity?> uploadBg(BuildContext context, String key) async {
    return YBDHttpUtil.getInstance().doGet<YBDBaseRespEntity>(
      context,
      YBDApi.UPLOAD_BG,
      baseUrl: await YBDApi.getBaseUrl,
      params: {'resource': key},
    );
  }

  /// 替换用户背景图片
  Future<YBDBaseRespEntity?> replaceBg(BuildContext context, String? oldKey, String newKey) async {
    return YBDHttpUtil.getInstance().doGet<YBDBaseRespEntity>(
      context,
      YBDApi.REPLACE_BG,
      baseUrl: await YBDApi.getBaseUrl,
      params: {'resourceOld': oldKey, 'resourceNew': newKey},
    );
  }

  /// 删除用户背景图片
  Future<YBDBaseRespEntity?> deleteBg(BuildContext context, String key) async {
    return YBDHttpUtil.getInstance().doGet<YBDBaseRespEntity>(
      context,
      YBDApi.DELETE_BG,
      baseUrl: await YBDApi.getBaseUrl,
      params: {'resource': key},
      showLoading: true,
    );
  }

  /// 用户上传背景图片限制
  Future<YBDBaseRespEntity?> queryUploadBgRestrict(BuildContext context) async {
    return YBDHttpUtil.getInstance().doGet<YBDBaseRespEntity>(
      context,
      YBDApi.QUERY_UPLOAD_BG_RESTRICT,
      baseUrl: await YBDApi.getBaseUrl,
      showLoading: true,
    );
  }

  Future<YBDCardFrameStoreEntity?> queryCardFrameStore(
    BuildContext? context,
  ) async {
    YBDCardFrameStoreEntity? cardFrameStoreEntity = await YBDHttpUtil.getInstance().doGet(
      context,
      YBDApi.QUERY_CARD_FRAME,
    );
    return cardFrameStoreEntity;
  }

  Future<YBDCardFrameBagEntity?> queryCardFrameBag(BuildContext? context, userId) async {
    YBDCardFrameBagEntity? cardFrameBagEntity = await YBDHttpUtil.getInstance().doGet(context, YBDApi.QUERY_PERSONAL, params: {'goodsType': 'CARD_FRAME', 'userId': userId});
    return cardFrameBagEntity;
  }

  Future<YBDBuyVipResultEntity?> buyCardFrame(BuildContext context, cardFrameId, unit) async {
    return YBDHttpUtil.getInstance().doPost<YBDBuyVipResultEntity>(
      context,
      YBDApi.ORDER_CARD_FRAME,
      {
        'cardFrameId': cardFrameId,
        'unit': unit,
      },
      baseUrl: await YBDApi.getBaseUrl,
    );
  }

  /// 新人任务浮窗
  Future<YBDNewUserStatusEntity?> queryNewUserStauts(BuildContext context) async {
    return YBDHttpUtil.getInstance().doGet<YBDNewUserStatusEntity>(
      context,
      YBDApi.QUERY_NEW_USER_STATUS,
    );
  }

  /// 语聊房房间分享上报
  Future<YBDResultBeanEntity?> reportAudioRoomShare(BuildContext context,
      {String? userId, String? roomId, String? type}) async {
    return YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
      context,
      YBDApi.REPORT_ROOM_SHARE,
      {
        'userId': userId,
        'roomId': roomId,
        'type': type, // 分享类型：friends, copyLink, facebook...
      },
      isJsonData: true,
    );
  }

  /// 获取服务器当前时间
  Future<YBDServerTimeEntity?> getServerTime() async {
    return YBDHttpUtil.getInstance().doGet<YBDServerTimeEntity>(Get.context, YBDApi.SERVER_TIME);
  }

  Future<YBDEmojiEntity?> getAllEmoji({String? updateTime}) async {
    return YBDHttpUtil.getInstance().doGet<YBDEmojiEntity>(
      Get.context,
      YBDApi.QUERY_EMOJIS,
      params: {'updateTime': updateTime},
    );
  }

  Future<YBDSelfEmoji?> getSelfEmoji() async {
    YBDSelfEmoji? result = await YBDHttpUtil.getInstance().doGet<YBDSelfEmoji>(
      Get.context,
      YBDApi.QUERY_USER_EMOJIS,
      params: {'userId': YBDUserUtil.getUserIdSync},
    );
    if (result?.returnCode == Const.HTTP_SUCCESS) {
      await YBDSPUtil.save(Const.SP_MY_EMOJI, result);
    }
    return result;
  }
}
