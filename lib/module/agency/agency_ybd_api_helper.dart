

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/module/api.dart';
import 'package:oyelive_main/module/entity/agency_ybd_talent_entity.dart';

import '../../common/constant/const.dart';
import '../../common/http/http_ybd_util.dart';
import '../../common/util/log_ybd_util.dart';
import '../entity/result_ybd_bean.dart';
import '../user/entity/agreements_ybd_entity.dart';
import 'agency_ybd_api.dart';

class YBDAgencyApiHelper {
  /// 搜索主播签约信息
  /// searchType  -  check_status：检查申请状态，official：签约列表搜索，approval：审批列表搜索
  /// 当searchType = check_status时返回数据：
  /// status可能的值   pending：普通主播申请加入家族    decline：已拒绝    approval：审批中     complete：已完成     perfect：已签约主播待完善资料
  static Future<YBDAgreementsEntity?> queryTalentInfo(BuildContext context, int? userId, String searchType,
      {bool needErrorToast = true}) async {
    YBDAgreementsEntity? agreementsEntity = await YBDHttpUtil.getInstance().doGet<YBDAgreementsEntity>(
        context, YBDAgencyApi.QUERY_OFFICIAL_TALENT,
        baseUrl: await YBDAgencyApi.getBaseUrl,
        params: {"userId": userId, "searchType": searchType},
        needErrorToast: needErrorToast);

    if (agreementsEntity == null) {
      logger.v('room label entity is empty');
      return null;
    } else if (agreementsEntity.code != Const.HTTP_SUCCESS_NEW) {
      return null;
    } else {
      return agreementsEntity;
    }
  }

  /// 判断用户是否为签约主播
  static Future<bool> isOfficialTalent(BuildContext? context, int? userId) async {
    YBDAgencyTalentEntity? agencyTalentEntity = await YBDHttpUtil.getInstance().doGet<YBDAgencyTalentEntity>(
      context,
      YBDAgencyApi.AGENT_TALENTS,
      baseUrl: await YBDAgencyApi.getBaseUrl,
      params: {"talentId": userId, "queryType": "info"},
    );

    if (agencyTalentEntity?.data?.talentId != null) {
      // .data不等于null为签约主播
      logger.v('isOfficialTalent userId: $userId, data is not null');
      return true;
    }

    // .data等于null不是签约主播
    logger.v('isOfficialTalent userId: $userId, data is null');
    return false;
  }

  /// 判断用户是否为签约主播
  static bool isOfficial(YBDAgreementsEntity? agreementsEntity) {
    return agreementsEntity?.data?.status == 'complete' || agreementsEntity?.data?.status == 'perfect';
  }

  /// 判断用户是否为签约主播
  static Future<bool> searchAgentByUserId(BuildContext context, String? userId) async {
    YBDResultBeanEntity? angent = await YBDHttpUtil.getInstance().doGet<YBDResultBeanEntity>(context, YBDAgencyApi.AGENT_SEARCH,
        params: {'userId': userId, 'searchType': 'userId'}, baseUrl: await YBDAgencyApi.getBaseUrl);
    return angent?.data != null;
  }
}
