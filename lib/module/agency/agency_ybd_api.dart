

import '../../common/constant/const.dart';
import '../../common/http/environment_ybd_config.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/remote_ybd_config_service.dart';

class YBDAgencyApi {
  static Future<String?> get getBaseUrl async {
    if (Const.TEST_ENV) {
      return 'http://${YBDEnvConfig.currentConfig().statusTestIp}/';
    }

    return await _apiBaseUrl();
  }

  static Future<String?> _apiBaseUrl() async {
    String? httpUrlProd = await YBDRemoteConfigService.baseUrlFromRemote('agency_http_domain_prod');
    logger.v("agency_http_domain_prod : $httpUrlProd");

    if ((httpUrlProd ?? '').isEmpty) {
      httpUrlProd = '${Const.STATUS_PROD_URL}/';
    }

    logger.v('agency base url : $httpUrlProd');
    return httpUrlProd;
  }

  static const QUERY_OFFICIAL_TALENT = "oyetalk-agency/agreements/_checkStatus";

  static const AGENT_SEARCH = "oyetalk-agent/agents";

  /// 检查是否为签约主播
  static const AGENT_TALENTS = "oyetalk-agency/talents";
}
