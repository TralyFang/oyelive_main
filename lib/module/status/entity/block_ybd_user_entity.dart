

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDBlockUserEntity with JsonConvert<YBDBlockUserEntity> {
  String? code;
  YBDBlockUserData? data;
  bool? success;
}

class YBDBlockUserData with JsonConvert<YBDBlockUserData> {
  dynamic id;
  int? fromId;
  int? toId;
  int? state;
  dynamic nickname;
  dynamic avatar;
}
