


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDCommentListEntity with JsonConvert<YBDCommentListEntity> {
  String? code;
  String? message;
  YBDCommentListData? data;
}

class YBDCommentListData with JsonConvert<YBDCommentListData> {
  List<YBDCommantListDataRows?>? rows;
  int? total;
  int? commentTotalCount;
}

class YBDCommantListDataRows with JsonConvert<YBDCommantListDataRows> {
  String? id;
  dynamic tenantCode;
  dynamic projectCode;
  int? version;
  String? createTime;
  dynamic creator;
  String? creatorId;
  String? updateTime;
  dynamic updator;
  String? updatorId;
  dynamic status;
  int? deleted;
  dynamic remark;
  String? userId;
  String? statusId;
  int? statusUserId;
  String? content;
  int? likeCount;
  String? nickName;
  int? sex;
  int? vip;
  dynamic avatar;
  bool? officialTalent;
  bool? like;
  int? replyCount;
  List<YBDReplyData?>? replyList;
}

class YBDReplyData with JsonConvert<YBDReplyData> {
  String? id;
  dynamic tenantCode;
  dynamic projectCode;
  int? version;
  String? createTime;
  dynamic creator;
  String? creatorId;
  String? updateTime;
  dynamic updator;
  String? updatorId;
  dynamic status;
  int? deleted;
  dynamic remark;
  String? userId;
  String? statusId;
  int? statusUserId;
  String? content;
  int? likeCount;
  String? nickName;
  int? toUserId;
  String? toUserNickName;
  int? toCommentId;
  int? sex;
  int? vip;
  dynamic avatar;
  bool? officialTalent;
  bool? like;
}
