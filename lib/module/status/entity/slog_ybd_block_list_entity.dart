

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDSlogBlockListEntity with JsonConvert<YBDSlogBlockListEntity> {
  String? code;
  YBDSlogBlockListData? data;
  bool? success;
}

class YBDSlogBlockListData with JsonConvert<YBDSlogBlockListData> {
  List<YBDSlogBlockListDataRows?>? rows;
  int? total;
}

class YBDSlogBlockListDataRows with JsonConvert<YBDSlogBlockListDataRows> {
  int? id;
  int? fromId;
  int? toId;
  int? state;
  String? nickname;
  String? avatar;
}
