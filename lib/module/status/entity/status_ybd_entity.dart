

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/user/entity/user_ybd_info_entity.dart';

class YBDStatusInfo with JsonConvert<YBDStatusInfo> {
  String? id;

  /// 动态国家
  String? country;
  int? version;
  String? createTime;

  String? creatorId;
  String? updateTime;

  String? updatorId;
  int? status;
  int? deleted;

  String? userId;
  int? contentType;
  YBDStatusContent? content;
  String? label;

  YBDLocationInfo? location;
  int? shareCount;
  int? likeCount;
  int? commentCount;
  int? giftCount;

  /// 评论被喜欢的数量
  int? likeCommentCount;
  String? cause;

  String? addTime;
  String? nickName;
  int? sex;
  int? vip;
  String? avatar;
  String? vipIcon;
  bool? officialTalent;
  bool? like;
  bool? follow;
  bool? live;
  int? level;
  int? talentLevel;
  int? activityRanking;

  List<YBDBadgeInfo?>? badges;

  String? headFrame;
}

class YBDStatusContent with JsonConvert<YBDStatusContent> {
  String? text;
  String? images;
  YBDAudioInfo? audio;
  String? video;

  /// slog 图片列表
  List<YBDStatusImageInfo?>? image;
}

/// 图片信息，slog 新增字段
class YBDStatusImageInfo with JsonConvert<YBDStatusImageInfo> {
  String? id;
  String? url;
  YBDExtInfo? ext;
}

class YBDExtInfo with JsonConvert<YBDExtInfo> {
  String? size;

  dynamic gauss;

  /// 高斯模糊参数
  bool? get gausss {
    return (gauss != null && gauss.runtimeType == bool) ? gauss : true;
  }
}

/// 动态位置信息
class YBDLocationInfo with JsonConvert<YBDLocationInfo> {
  // ip 地址
  String? ip;
}

class YBDAudioInfo with JsonConvert<YBDAudioInfo> {
  String? resource;
  String? dialogue;
  String? lyric;
  String? dialogueName;
  String? lyricName;

  String? bgImage;
  String? text;
  String? mood;
  int? type;
  int? duration;

  /// 背景音乐
  String? bgMusic;

  /// 背景音乐 id
  String? bgMusicId;

  /// 背景音乐名称
  String? bgMusicName;

  /// 背景音乐歌手
  String? bgMusicSinger;

  /// 音量
  String? volume;

  /// 发布的时候录音片段数量
  String? segment;
}
