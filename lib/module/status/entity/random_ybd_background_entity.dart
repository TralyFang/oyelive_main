

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDRandomBackgroundEntity with JsonConvert<YBDRandomBackgroundEntity> {
  String? code;
  YBDRandomBackgroundData? data;
}

class YBDRandomBackgroundData with JsonConvert<YBDRandomBackgroundData> {
  YBDRandomBackgroundDataBgimage? bgimage;
}

class YBDRandomBackgroundDataBgimage with JsonConvert<YBDRandomBackgroundDataBgimage> {
  String? id;
  String? resource;
  int? top;
}
