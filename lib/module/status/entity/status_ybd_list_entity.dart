

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/base/json_field.dart';
import 'package:oyelive_main/module/status/entity/status_ybd_entity.dart';

class YBDStatusListEntity with JsonConvert<YBDStatusListEntity> {
  String? code;
  String? message;
  YBDStatusListData? data;
}

class YBDStatusListData with JsonConvert<YBDStatusListData> {
  @JSONField(name: "list")
  YBDStatusListDataList? xList;
  List<YBDtopContent?>? tops;
}

class YBDStatusListDataList with JsonConvert<YBDStatusListDataList> {
  List<YBDStatusInfo?>? rows;
  int? total;
}

class YBDtopContent with JsonConvert<YBDtopContent> {
  int? no;
  YBDStatusInfo? status;
}
