


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/status/entity/status_ybd_entity.dart';

class YBDStatusDetailEntity with JsonConvert<YBDStatusDetailEntity> {
  String? code;
  YBDStatusInfo? data;
}
