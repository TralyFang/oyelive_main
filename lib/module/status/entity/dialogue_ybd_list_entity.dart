

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/status/entity/random_ybd_dialogue_entity.dart';

class YBDDialogueListEntity with JsonConvert<YBDDialogueListEntity> {
  String? code;
  String? message;
  YBDDialogueListData? data;
}

class YBDDialogueListData with JsonConvert<YBDDialogueListData> {
  List<YBDRandomDialogueDataDialogue?>? rows;
}
