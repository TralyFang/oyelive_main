


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/status/entity/comment_ybd_list_entity.dart';

class YBDReplyListEntity with JsonConvert<YBDReplyListEntity> {
  String? code;
  YBDReplyListData? data;
  bool? success;
  String? message;
}

class YBDReplyListData with JsonConvert<YBDReplyListData> {
  List<YBDReplyData?>? rows;
  int? total;
}
