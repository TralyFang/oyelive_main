final String tableName = 'status_table';
final String columnId = 'id';
final String columnUserId = 'user_id';
final String columnStatusScene = 'status_scene';
final String columnStatusConent = 'status_content';
enum StatusScene { Featured, Following, MyProfile }

class YBDDBStatusEntity {
  int? id;
  int? usrId;
  String? StatusScene;
  String? YBDStatusContent;

  YBDDBStatusEntity(this.usrId, this.StatusScene, this.YBDStatusContent, {this.id});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
//      columnId: id,
      columnUserId: usrId,
      columnStatusScene: StatusScene,
      columnStatusConent: YBDStatusContent,
    };
    return map;
  }

  YBDDBStatusEntity.fromMap(Map<String, dynamic> map) {
    if (map == null) return;
    id = map[columnId];
    usrId = map['${usrId ?? -1}'];
    StatusScene = map[columnStatusScene];
    YBDStatusContent = map[columnStatusConent];
  }
}
