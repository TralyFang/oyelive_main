

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/status/entity/random_ybd_lyric_entity.dart';

class YBDLyricsListEntity with JsonConvert<YBDLyricsListEntity> {
  String? code;
  String? message;
  YBDLyricsListData? data;
}

class YBDLyricsListData with JsonConvert<YBDLyricsListData> {
  List<YBDRandomLyricDataLyric?>? rows;
}
