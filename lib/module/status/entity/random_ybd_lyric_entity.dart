

import 'package:json_annotation/json_annotation.dart';
import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

@JsonSerializable()
class YBDRandomLyricEntity with JsonConvert<YBDRandomLyricEntity> {
  String? code;
  YBDRandomLyricData? data;
}

class YBDRandomLyricData with JsonConvert<YBDRandomLyricData> {
  YBDRandomLyricDataLyric? lyric;
}

class YBDRandomLyricDataLyric with JsonConvert<YBDRandomLyricDataLyric> {
  String? id;
  String? name;
  String? author;
  int? no;
  int? top;
  String? text;
  bool? mark;
  String? activityH5;
}
