

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDAgentShareEntity with JsonConvert<YBDAgentShareEntity> {
  String? code;
  YBDAgentShareData? data;
  bool? success;
}

class YBDAgentShareData with JsonConvert<YBDAgentShareData> {
  int? layeredBeans;
  int? startTime;
  int? totalConsumption;
  int? endTime;
  String? userId;
}
