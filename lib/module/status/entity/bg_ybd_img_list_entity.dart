

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/status/entity/random_ybd_background_entity.dart';

class YBDBgImgListEntity with JsonConvert<YBDBgImgListEntity> {
  String? code;
  YBDBgImgListData? data;
}

class YBDBgImgListData with JsonConvert<YBDBgImgListData> {
  List<YBDRandomBackgroundDataBgimage?>? rows;
  int? total;
}
