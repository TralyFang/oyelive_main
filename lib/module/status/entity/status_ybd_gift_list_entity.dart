

import '../../../generated/json/base/json_convert_content.dart';

class YBDStatusGiftListEntity with JsonConvert<YBDStatusGiftListEntity> {
  String? code;
  bool? success;
  YBDStatusGiftListData? data;
}

class YBDStatusGiftListData with JsonConvert<YBDStatusGiftListData> {
  List<YBDStatusGiftListDataRows?>? records;
  int? total;
  int? size;
  int? current;
  int? pages;
}

class YBDStatusGiftListDataRows with JsonConvert<YBDStatusGiftListDataRows> {
  String? id;
  int? slogId;
  int? slogUserId;
  int? presentUserId;
  int? giftId;
  int? giftNumber;
  int? giftPrice;
  int? status;
  String? createTime;
  String? nickName;
  int? level;
  int? gender;
  String? giftUrl;
  String? avatar;
  String? vipIcon;
}
