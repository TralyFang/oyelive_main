

import '../../../generated/json/base/json_convert_content.dart';

class YBDMusicListEntity with JsonConvert<YBDMusicListEntity> {
  String? code;
  YBDMusicListData? data;
}

class YBDMusicListData with JsonConvert<YBDMusicListData> {
  List<YBDMusicListDataRow?>? rows;
}

class YBDMusicListDataRow with JsonConvert<YBDMusicListDataRow> {
  String? id;
  int? version;
  String? createTime;
  String? creatorId;
  String? updateTime;
  String? updatorId;
  int? status;
  int? deleted;
  String? name;
  String? author;
  int? userId;
  String? addTime;
  String? resource;
  int? count;
  int? duration;
}
