

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/status/entity/comment_ybd_list_entity.dart';

class YBDCommentSubmitRespEntity with JsonConvert<YBDCommentSubmitRespEntity> {
  String? code;
  YBDReplyData? data;
  bool? success;
}
