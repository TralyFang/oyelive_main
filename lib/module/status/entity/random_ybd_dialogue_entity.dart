


import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDRandomDialogueEntity with JsonConvert<YBDRandomDialogueEntity> {
  String? code;
  YBDRandomDialogueData? data;
}

class YBDRandomDialogueData with JsonConvert<YBDRandomDialogueData> {
  YBDRandomDialogueDataDialogue? dialogue;
}

class YBDRandomDialogueDataDialogue with JsonConvert<YBDRandomDialogueDataDialogue> {
  String? id;
  String? name;
  String? author;
  String? text;
  int? top;
}
