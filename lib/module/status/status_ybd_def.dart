

/// 动态列表的类型
enum StatusListType {
  // 动态列表 Featured 标签栏
  Featured,
  // 动态列表 Following 标签栏
  Following,
  // 个人中心的动态列表
  MyProfile,
}

/// 动态类型
enum StatusPostType {
  Text,
  Image,
  Audio,
  Video,
}

/// Feature 标签栏索引
const FeaturedTabBarIndex = 0;

/// Following 标签栏索引
const FollowingTabBarIndex = 1;
