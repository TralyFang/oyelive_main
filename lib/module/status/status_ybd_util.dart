

import 'dart:ui';

import 'package:oyelive_main/ui/widget/ybd_network_image.dart';
import 'package:flutter/material.dart';

import '../../common/util/image_ybd_util.dart';
import '../../common/util/log_ybd_util.dart';
import '../entity/query_ybd_configs_resp_entity.dart';
import '../user/entity/user_ybd_info_entity.dart';
import 'entity/status_ybd_entity.dart';

/// 动态模块的工具类
class YBDStatusUtil {
  /// 判断用户等级是否能够发布动态
  static bool isUserLevelEnable(YBDUserInfo? userInfo, YBDConfigInfo config) {
    if (null == userInfo || null == config) {
      return false;
    }

    return userInfo.level! >= int.parse(config.ulevel_post_status ?? "3");
  }

  /// 本地随机图片
  /// 根据动态ID最后一位数字，选择本地图片
  /// 0、5:图片一
  /// 1、6:图片二
  /// 2、7:图片三
  /// 3、8:图片四
  /// 4、9:图片五
  static String randomBgImgWithStatusId(String statusId) {
    logger.v('statusId : $statusId');
    final lastNumber = statusId.substring(statusId.length - 1);
    switch (lastNumber) {
      case '0':
      case '5':
        return 'assets/images/status/status_default_bg_0.webp';
      case '1':
      case '6':
        return 'assets/images/status/status_default_bg_1.webp';
      case '2':
      case '7':
        return 'assets/images/status/status_default_bg_2.webp';
      case '3':
      case '8':
        return 'assets/images/status/status_default_bg_3.webp';
      case '4':
      case '9':
        return 'assets/images/status/status_default_bg_4.webp';
      default:
        return 'assets/images/status/status_default_bg_0.webp';
    }
  }

  /// 接口是否返回了背景图
  static bool isBgImgEmpty(YBDStatusInfo? statusInfo) {
    if (null == statusInfo) {
      return false;
    }

    List<YBDStatusImageInfo?>? imgList = statusInfo.content!.image;
    if (null == imgList || imgList.isEmpty) {
      return true;
    } else {
      return false;
    }
  }

  /// 动态的第一张背景图
  static String? firstBgImg(BuildContext context, YBDStatusInfo? statusInfo) {
    if (YBDStatusUtil.isBgImgEmpty(statusInfo)) {
      return YBDStatusUtil.randomBgImgWithStatusId(statusInfo?.id ?? '');
    } else {
      List<YBDStatusImageInfo?>? imgList = statusInfo?.content?.image;
      return YBDImageUtil.status(context, imgList?.first?.url, scene: "A");
    }
  }

  /// 动态列表页面背景图
  static Widget statusListBgImg(BuildContext context, YBDStatusInfo statusInfo) {
    String img = YBDStatusUtil.firstBgImg(context, statusInfo)!;

    // 背景图遮罩
    final colorFilter = ColorFilter.mode(
      Colors.black.withOpacity(0.1),
      BlendMode.srcATop,
    );

    logger.v('status list bg img : $img');
    if (img.startsWith('assets')) {
      // 本地图片
      return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(img),
            fit: BoxFit.cover,
            colorFilter: colorFilter,
          ),
        ),
      );
    } else {
      // 网络图片
      return YBDNetworkImage(
        imageUrl: img,
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
              colorFilter: colorFilter,
            ),
          ),
        ),
        placeholder: (context, url) => Opacity(
          opacity: 0.15,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.black,
            ),
          ),
        ),
        fit: BoxFit.cover,
      );
    }
  }
}
