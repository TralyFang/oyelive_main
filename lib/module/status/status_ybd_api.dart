

import '../../common/constant/const.dart';
import '../../common/http/environment_ybd_config.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/remote_ybd_config_service.dart';

class YBDStatusApi {
  static Future<String?> get getBaseUrl async {
    if (Const.TEST_ENV) {
      // return 'http://${Const.STATUS_TEST_IP}/';
      return 'http://${YBDEnvConfig.currentConfig().statusTestIp}/';
    }

    return await _apiBaseUrl();
  }

  static Future<String?> _apiBaseUrl() async {
    String? statusUrlProd = await YBDRemoteConfigService.baseUrlFromRemote('status_http_domain_prod');

    if ((statusUrlProd ?? '').isEmpty) {
      statusUrlProd = '${Const.STATUS_PROD_URL}';
    }

    logger.v('status base url : $statusUrlProd');
    return statusUrlProd;
  }

  static const STATUS_LIKE = "oyetalk-status/statuslike";
  static const STATUS_SEARCH = "oyetalk-status/v2/status/search";
  static const USER_PROFILE_STATUS_LIST = "oyetalk-status/status/list";
  static const STATUS_SEARCH_FOLLOWING = "oyetalk-status/v2/statusfollow/search";
  static const STATUS_DETAIL = "oyetalk-status/status";

  static const STATUS_SHARE = 'oyetalk-status/status/share';
  static const STATUS_VIEW = 'oyetalk-status/statusclick';

  /// 发布动态接口
  static const STATUS_POST = 'oyetalk-status/status';

  /// 记录slog
  static const SLOG_EVENT = 'oyetalk-status/event';

  /// 上传背景音乐接口
  static const STATUS_UPLOAD_MUSIC = 'oyetalk-status/bgmusic';

  /// 上传背景图片接口
  static const STATUS_UPLOAD_BG_IMAGE = 'oyetalk-status/bgimage';

  /// 上传背景音乐接口
  static const STATUS_UPLOAD_LYRIC = 'oyetalk-status/lyric';

  /// 上传背景对白接口
  static const STATUS_UPLOAD_DIALOGUE = 'oyetalk-status/dialogue';

  static const STATUS_COMMENT_LIST = 'oyetalk-status/statuscomment/search';
  static const STATUS_MY_LIST = 'oyetalk-status/v2/status/my';
  static const STATUS_REPORT = 'oyetalk-status/statusinform';
  static const STATUS_COMMENT_NOW = 'oyetalk-status/statuscomment';

  //audio

  static const DIALOGUE_RANDOM = 'oyetalk-status/dialogue/random';
  static const LYRICS_RANDOM = 'oyetalk-status/lyric/random';
  static const BACKGROUND_RANDOM = 'oyetalk-status/bgimage/random';

  static const BACKGROUND_MUSIC_RANDOM = "oyetalk-status/v2/bgmusic/random";

  /// 背景图片列表
  static const BACKGROUND_IMG_LIST = 'oyetalk-status/bgimage/search';

  static const DIALOGUE_SEARCH = 'oyetalk-status/dialogue/search';
  static const LYRICS_SEARCH = 'oyetalk-status/lyric/search';
  static const SONG_SEARCH = 'oyetalk-status/bgmusic/search';

  /// 随机歌词
  static const RANDOM_LYRICS_SEARCH = 'oyetalk-status/v2/lyric/random';

  /// 随机对白
  static const RANDOM_DIALOG_SEARCH = 'oyetalk-status/v2/dialogue/random';

  /// 随机背景图
  static const RANDOM_BGIMG_SEARCH = 'oyetalk-status/v2/bgimage/random';

  static const COMMENT_REPLY_LIST = 'oyetalk-status/statuscomments';

  /// 动态送礼
  static const STATUS_GIFT = 'oyetalk-status/gift';

  /// 查询分成数据
  static const AGENT_SHARE_DATA = 'oyetalk-activity/tp-layered-user';

  /// 获取单个用户slog拉黑状态
  static const USER_BLOCK = 'oyetalk-status/userblock';

  /// 获取slog 拉黑列表
  static const USER_BLOCK_LIST = 'oyetalk-status/userblock/list';
}
