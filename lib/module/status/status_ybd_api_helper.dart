

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:get/get.dart';
import 'package:oyelive_main/common/util/certification_ybd_util.dart';
import 'package:oyelive_main/common/util/numeric_ybd_util.dart';
import 'package:oyelive_main/module/status/entity/block_ybd_user_entity.dart';
import 'package:oyelive_main/module/status/entity/slog_ybd_block_list_entity.dart';
import 'package:oyelive_main/module/user/util/user_ybd_util.dart';

import '../../common/constant/const.dart';
import '../../common/http/http_ybd_util.dart';
import '../../common/util/database_ybd_until.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/toast_ybd_util.dart';
import '../../main.dart';
import '../../ui/page/profile/my_profile/update_ybd_profile_event.dart';
import '../../ui/page/status/bloc/comment_ybd_bloc.dart';
import '../entity/new_ybd_result_entity.dart';
import 'entity/agent_ybd_share_entity.dart';
import 'entity/bg_ybd_img_list_entity.dart';
import 'entity/comment_ybd_list_entity.dart';
import 'entity/comment_ybd_submit_resp_entity.dart';
import 'entity/db_ybd_status_entity.dart';
import 'entity/dialogue_ybd_list_entity.dart';
import 'entity/lyrics_ybd_list_entity.dart';
import 'entity/music_ybd_list_entity.dart';
import 'entity/random_ybd_background_entity.dart';
import 'entity/random_ybd_dialogue_entity.dart';
import 'entity/random_ybd_lyric_entity.dart';
import 'entity/reply_ybd_list_entity.dart';
import 'entity/status_ybd_detail_entity.dart';
import 'entity/status_ybd_entity.dart';
import 'entity/status_ybd_gift_list_entity.dart';
import 'entity/status_ybd_list_entity.dart';
import 'status_ybd_api.dart';
import 'status_ybd_def.dart';

class YBDStatusApiHelper {
  static Future<YBDNewResultEntity?> like(BuildContext context,
      {String? statusId, int? likeType, int? targetType, String? commentId}) async {
    var req = {
      'statusId': statusId,
      'likeType': likeType,
      'targetType': targetType,
    };
    if (targetType == 2 && commentId!.isNotEmpty) {
      req.addAll({'commentId': commentId});
    }
    return YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(context, YBDStatusApi.STATUS_LIKE, req,
        baseUrl: await YBDStatusApi.getBaseUrl, isJsonData: true);
  }

  static Future<YBDStatusListEntity?> queryStatusList(BuildContext? context, StatusListType statusListType,
      {page, pageSize, status}) async {
    String? url;
    switch (statusListType) {
      case StatusListType.Featured:
        url = YBDStatusApi.STATUS_SEARCH;
        break;
      case StatusListType.Following:
        url = YBDStatusApi.STATUS_SEARCH_FOLLOWING;
        break;
      case StatusListType.MyProfile:
        url = YBDStatusApi.STATUS_MY_LIST;
        break;
    }
    var data = {
      'page': page,
      'pageSize': pageSize,
    };
    if (status != null) {
      //状态(0:待审，1发布，2拒绝)
      data.addAll({'status': status});
    }

    YBDStatusListEntity? result = await YBDHttpUtil.getInstance()
        .doGet<YBDStatusListEntity>(context, url, baseUrl: await YBDStatusApi.getBaseUrl, params: data);
    if (result != null && result.code == Const.HTTP_SUCCESS_NEW && page == 1 && result.data!.xList!.rows!.length != 0) {
      logger.v("insert status into db ");
      var dbResult = await YBDDataBaseUtil.getInstance().insert(result, StatusScene.values[statusListType.index]);
      logger.v("dbResult:$dbResult ");
    }
    //认证信息
    List<int> userIds = [];
    result?.data?.xList?.rows?.forEach((element) {
      int elementUserId = YBDNumericUtil.stringToInt(element!.userId);
      if (!userIds.contains(elementUserId)) {
        userIds.add(elementUserId);
      }
    });
    if (result?.data?.tops != null)
      result?.data?.tops?.forEach((element) {
        int elementUserId = YBDNumericUtil.stringToInt(element!.status!.userId);

        if (!userIds.contains(elementUserId)) {
          print("top add ${elementUserId}");
          userIds.add(elementUserId);
        }
      });

    YBDCertificationUtil.getInstance()!.addUserCache(userIds);

    return result;
  }

  /// 查询指定用户的动态列表
  static Future<YBDStatusListEntity?> queryStatusWithUserId(BuildContext context, int userId,
      {page, pageSize, status = 1}) async {
    return YBDHttpUtil.getInstance().doGet<YBDStatusListEntity>(context, YBDStatusApi.USER_PROFILE_STATUS_LIST,
        baseUrl: await YBDStatusApi.getBaseUrl,
        params: {
          'userId': userId,
          'page': page,
          'pageSize': pageSize,
          'status': status, //状态(0:待审，1发布，2拒绝)
        });
  }

  /// 分享动态
  static Future<bool> shareStatus(BuildContext context, String? id) async {
    YBDNewResultEntity? data = await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
      context,
      YBDStatusApi.STATUS_SHARE,
      {'id': id},
      baseUrl: await YBDStatusApi.getBaseUrl,
      isJsonData: true,
    );

    return data != null && data.code == Const.HTTP_SUCCESS_NEW;
  }

  /// 点击动态
  static Future<bool> viewStatus(BuildContext context, String? id) async {
    YBDNewResultEntity? data = await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
      context,
      YBDStatusApi.STATUS_VIEW,
      {'statusId': id},
      baseUrl: await YBDStatusApi.getBaseUrl,
      isJsonData: true,
    );

    return data != null && data.code == Const.HTTP_SUCCESS_NEW;
  }

  /// 发布动态接口
  static Future<YBDNewResultEntity?> postStatus(BuildContext context, StatusPostType contentType, Map content) async {
    String type = "";
    switch (contentType) {
      case StatusPostType.Text:
        type = "0";
        break;
      case StatusPostType.Image:
        type = "1";
        break;
      case StatusPostType.Audio:
        type = "2";
        break;
      case StatusPostType.Video:
        type = "3";
        break;
    }

    YBDNewResultEntity? result = await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
      context,
      YBDStatusApi.STATUS_POST,
      {
        'contentType': type,
        'content': content,
      },
      baseUrl: await YBDStatusApi.getBaseUrl,
      isJsonData: true,
    );

    if (result?.code == Const.HTTP_SUCCESS_NEW) {
      logger.v('post status success');
      eventBus.fire(YBDUpdateProfileEvent(type: UpdateProfileEventType.UpdateStatus));
    }

    return result;
  }

  /// 上传背景音乐接口
  static Future<YBDNewResultEntity?> uploadBgMusic(
    BuildContext context,
    String name,
    String author,
    String resource,
    double duration,
  ) async {
    return await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
      context,
      YBDStatusApi.STATUS_UPLOAD_MUSIC,
      {'name': name, 'author': author, 'resource': resource, 'duration': duration},
      baseUrl: await YBDStatusApi.getBaseUrl,
      isJsonData: true,
    );
  }

  /// 上传背景图片接口
  static Future<YBDNewResultEntity?> uploadBgImage(
    BuildContext context,
    String resource,
  ) async {
    return await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
      context,
      YBDStatusApi.STATUS_UPLOAD_BG_IMAGE,
      {'resource': resource},
      baseUrl: await YBDStatusApi.getBaseUrl,
      isJsonData: true,
    );
  }

  /// 上传歌词接口
  static Future<YBDNewResultEntity?> uploadLyric(
    BuildContext context,
    String name,
    String author,
    List lyrics,
  ) async {
    return await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
      context,
      YBDStatusApi.STATUS_UPLOAD_LYRIC,
      {'name': name, 'author': author, 'list': lyrics},
      baseUrl: await YBDStatusApi.getBaseUrl,
      isJsonData: true,
    );
  }

  /// 上传歌词接口
  static Future<YBDNewResultEntity?> uploadDialogue(
    BuildContext context,
    String name,
    String author,
    List dialogues,
  ) async {
    return await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
      context,
      YBDStatusApi.STATUS_UPLOAD_DIALOGUE,
      {'name': name, 'author': author, 'list': dialogues},
      baseUrl: await YBDStatusApi.getBaseUrl,
      isJsonData: true,
    );
  }

  static Future<YBDCommentListEntity?> queryCommentList(BuildContext context, String? statusId, {int? page}) async {
    YBDCommentListEntity? data = await YBDHttpUtil.getInstance().doGet<YBDCommentListEntity>(
      context,
      YBDStatusApi.STATUS_COMMENT_LIST,
      params: {'statusId': statusId, 'page': page, 'pageSize': 10},
      baseUrl: await YBDStatusApi.getBaseUrl,
    );

    //认证信息
    List<int> userIds = [];
    if (data?.code == Const.HTTP_SUCCESS_NEW) {
      data!.data!.rows!.forEach((element) {
        int elementUserId = YBDNumericUtil.stringToInt(element!.userId);
        if (!userIds.contains(elementUserId)) {
          userIds.add(elementUserId);
        }
        if (element.replyList?.isNotEmpty ?? false) {
          element.replyList!.forEach((replyData) {
            int replayUserId = YBDNumericUtil.stringToInt(replyData!.userId);

            if (!userIds.contains(replayUserId)) {
              userIds.add(replayUserId);
            }
          });
        }
      });

      YBDCertificationUtil.getInstance()!.addUserCache(userIds);
    }

    return data;
  }

  static Future<YBDReplyListEntity?> queryReplyList(BuildContext context, String? statusId, toCommentId, {int? page}) async {
    YBDReplyListEntity? data = await YBDHttpUtil.getInstance().doGet<YBDReplyListEntity>(
      context,
      YBDStatusApi.COMMENT_REPLY_LIST,
      params: {'statusId': statusId, 'page': page, 'pageSize': 10, 'toCommentId': toCommentId},
      baseUrl: await YBDStatusApi.getBaseUrl,
    );

    //认证信息
    List<int> userIds = [];
    if (data?.code == Const.HTTP_SUCCESS_NEW) {
      data!.data!.rows!.forEach((element) {
        if (!userIds.contains(element!.userId)) {
          userIds.add(YBDNumericUtil.stringToInt(element.userId));
        }
      });

      YBDCertificationUtil.getInstance()!.addUserCache(userIds);
    }
    return data;
  }

  static Future<bool> reportStatus(BuildContext context, String? statusId, String reason) async {
    YBDNewResultEntity? resultEntity = await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
        context, YBDStatusApi.STATUS_REPORT, {'statusId': statusId, 'reason': reason},
        baseUrl: await YBDStatusApi.getBaseUrl, isJsonData: true);

    if (resultEntity != null && resultEntity.code == Const.HTTP_SUCCESS_NEW) {
      YBDToastUtil.toast(translate('success'));
      return true;
    } else {
      YBDToastUtil.toast(translate('failed'));
      return false;
    }
  }

  static Future<bool> deleteStatus(BuildContext context, String? statusId) async {
    YBDNewResultEntity? resultEntity = await YBDHttpUtil.getInstance().doDelete<YBDNewResultEntity>(
        context, YBDStatusApi.STATUS_DETAIL, {'id': statusId},
        isJsonData: true, baseUrl: await YBDStatusApi.getBaseUrl);

    if (resultEntity != null && resultEntity.code == Const.HTTP_SUCCESS_NEW) {
      YBDToastUtil.toast(translate('success'));
      return true;
    } else {
      YBDToastUtil.toast(translate('failed'));
      return false;
    }
  }

  static Future<bool> deleteComment(BuildContext context, String? commentId) async {
    YBDNewResultEntity? resultEntity = await YBDHttpUtil.getInstance().doDelete<YBDNewResultEntity>(
        context, YBDStatusApi.STATUS_COMMENT_NOW, {'id': commentId},
        isJsonData: true, baseUrl: await YBDStatusApi.getBaseUrl);

    if (resultEntity != null && resultEntity.code == Const.HTTP_SUCCESS_NEW) {
      YBDToastUtil.toast(translate('success'));
      return true;
    } else {
      YBDToastUtil.toast(translate('failed'));
      return false;
    }
  }

  static Future<YBDCommentSubmitRespEntity?> commentNow(
      BuildContext context, String? statusId, String statusUserId, String content,
      {String? toCommentId, String? toUserId, YBDCommentBloc? commentBloc}) async {
    YBDCommentSubmitRespEntity? resultEntity = await YBDHttpUtil.getInstance().doPost<YBDCommentSubmitRespEntity>(
        context,
        YBDStatusApi.STATUS_COMMENT_NOW,
        {
          'statusId': statusId,
          'statusUserId': statusUserId,
          'content': content,
          'toCommentId': toCommentId,
          'toUserId': toUserId
        }..removeWhere((key, value) => value == null),
        baseUrl: await YBDStatusApi.getBaseUrl,
        isJsonData: true);
    if (resultEntity != null && resultEntity.code == Const.HTTP_SUCCESS_NEW) {
      YBDToastUtil.toast(translate('success'));
      if (toCommentId != null && toUserId != null && commentBloc != null) {
        commentBloc.replySubmit(resultEntity.data);
      }
      return resultEntity;
    } else {
      YBDToastUtil.toast(translate('failed'));
      return null;
    }
  }

  static Future<YBDStatusDetailEntity?> statusDetail(BuildContext context, String? statusId) async {
    return await YBDHttpUtil.getInstance()
        .doGet<YBDStatusDetailEntity>(context, YBDStatusApi.STATUS_DETAIL, baseUrl: await YBDStatusApi.getBaseUrl, params: {
      //状态(0:待审，1发布，2拒绝)
      'id': statusId
    });
  }

  static Future<YBDRandomLyricEntity?> getRandomLyric(BuildContext context, {int offset: 0}) async {
    return await YBDHttpUtil.getInstance().doGet<YBDRandomLyricEntity>(context, YBDStatusApi.LYRICS_RANDOM,
        baseUrl: await YBDStatusApi.getBaseUrl, params: {"offset": offset});
  }

  static Future<YBDRandomDialogueEntity?> getRandomDialogue(BuildContext context, {int offset: 0}) async {
    return await YBDHttpUtil.getInstance().doGet<YBDRandomDialogueEntity>(context, YBDStatusApi.DIALOGUE_RANDOM,
        baseUrl: await YBDStatusApi.getBaseUrl, params: {"offset": offset});
  }

  static Future<YBDRandomBackgroundEntity?> getRandomBackground(BuildContext context, {int offset: 0}) async {
    return await YBDHttpUtil.getInstance().doGet<YBDRandomBackgroundEntity>(context, YBDStatusApi.BACKGROUND_RANDOM,
        baseUrl: await YBDStatusApi.getBaseUrl, params: {"offset": offset});
  }

  /// 请求背景图片列表
  static Future<YBDBgImgListEntity?> getBgImgList(
    BuildContext context, {
    int page: 1,
    int pageSize: 10,
    int userId: 0,
  }) async {
    Map<String, dynamic> params = {
      'page': page,
      'pageSize': pageSize,
    };

    if (userId != 0) {
      params['userId'] = userId;
    }

    return await YBDHttpUtil.getInstance().doGet<YBDBgImgListEntity>(context, YBDStatusApi.BACKGROUND_IMG_LIST,
        baseUrl: await YBDStatusApi.getBaseUrl, params: params);
  }

  static Future<YBDMusicListEntity?> getMusicList(BuildContext context,
      {int page: 1, int pageSize: 10, String? searchKey, bool random: false}) async {
    Map<String, dynamic> params = {
      "page": page,
      "pageSize": pageSize,
    };
    if (searchKey != null) {
      params.addAll({"searchKey": searchKey});
    }
    return await YBDHttpUtil.getInstance().doGet<YBDMusicListEntity>(
        context, random ? YBDStatusApi.BACKGROUND_MUSIC_RANDOM : YBDStatusApi.SONG_SEARCH,
        baseUrl: await YBDStatusApi.getBaseUrl, params: params);
  }

  static Future<YBDDialogueListEntity?> getDialogueList(BuildContext context,
      {int page: 1, int pageSize: 10, String? searchKey}) async {
    Map<String, dynamic> params = {
      "page": page,
      "pageSize": pageSize,
    };
    if (searchKey != null) {
      params.addAll({"searchKey": searchKey});
    }
    return await YBDHttpUtil.getInstance().doGet<YBDDialogueListEntity>(context, YBDStatusApi.DIALOGUE_SEARCH,
        baseUrl: await YBDStatusApi.getBaseUrl, params: params);
  }

  static Future<YBDLyricsListEntity?> getLyricsList(BuildContext context,
      {int page: 1, int pageSize: 10, String? searchKey}) async {
    Map<String, dynamic> params = {
      "page": page,
      "pageSize": pageSize,
    };
    if (searchKey != null) {
      params.addAll({"searchKey": searchKey});
    }
    return await YBDHttpUtil.getInstance()
        .doGet<YBDLyricsListEntity>(context, YBDStatusApi.LYRICS_SEARCH, baseUrl: await YBDStatusApi.getBaseUrl, params: params);
  }

  /// 查询随机歌词
  static Future<YBDLyricsListEntity?> getRandomLyricsList(
    BuildContext context, {
    int page: 1,
    int pageSize: 10,
  }) async {
    Map<String, dynamic> params = {
      "page": page,
      "pageSize": pageSize,
    };
    return await YBDHttpUtil.getInstance().doGet<YBDLyricsListEntity>(
      context,
      YBDStatusApi.RANDOM_LYRICS_SEARCH,
      baseUrl: await YBDStatusApi.getBaseUrl,
      params: params,
    );
  }

  /// 查询随机对白
  static Future<YBDDialogueListEntity?> getRandomDialogList(
    BuildContext context, {
    int page: 1,
    int pageSize: 10,
  }) async {
    Map<String, dynamic> params = {
      "page": page,
      "pageSize": pageSize,
    };
    return await YBDHttpUtil.getInstance().doGet<YBDDialogueListEntity>(
      context,
      YBDStatusApi.RANDOM_DIALOG_SEARCH,
      baseUrl: await YBDStatusApi.getBaseUrl,
      params: params,
    );
  }

  /// 查询随机背景图
  static Future<YBDBgImgListEntity?> getRandomBgImgList(
    BuildContext context, {
    int page: 1,
    int pageSize: 10,
  }) async {
    Map<String, dynamic> params = {
      "page": page,
      "pageSize": pageSize,
    };
    return await YBDHttpUtil.getInstance().doGet<YBDBgImgListEntity>(
      context,
      YBDStatusApi.RANDOM_BGIMG_SEARCH,
      baseUrl: await YBDStatusApi.getBaseUrl,
      params: params,
    );
  }

  static Future<YBDNewResultEntity?> postSlog(BuildContext context, json) async {
    return await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(context, YBDStatusApi.STATUS_POST, json,
        baseUrl: await YBDStatusApi.getBaseUrl, isJsonData: true);
  }

  static Future<YBDNewResultEntity?> logSlog(BuildContext? context, YBDStatusInfo statusInfo, int progress) async {
    return await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
        context,
        YBDStatusApi.SLOG_EVENT,
        {
          'itemId': statusInfo.id,
          'itemUserId': statusInfo.userId,
          'eventType': "SHOWN",
          'eventValue': progress,
          'properties': {'slogType': statusInfo.contentType}
        },
        baseUrl: await YBDStatusApi.getBaseUrl,
        isJsonData: true);
  }

  /// 动态送礼
  static Future<bool> sendGift(
    BuildContext context, {
    int? slogId,
    int? slogUserId,
    int? giftId,
    int? giftNumber,
    int? personalId,
  }) async {
    YBDNewResultEntity? result = await YBDHttpUtil.getInstance().doPost<YBDNewResultEntity>(
      context,
      YBDStatusApi.STATUS_GIFT,
      {
        'slogId': slogId,
        'slogUserId': slogUserId,
        'giftId': giftId,
        'giftNumber': giftNumber,
        'personalId': personalId
      },
      baseUrl: await YBDStatusApi.getBaseUrl,
      isJsonData: true,
    );
    if (result?.code == Const.HTTP_SUCCESS_NEW) {
      logger.v('status send gift success');
      // YBDToastUtil.toast(translate('success'));
      return true;
    } else {
      YBDToastUtil.toast(result?.message);
      return false;
    }
  }

  /// 查询送礼记录
  static Future<YBDStatusGiftListEntity> queryStatusGiftList(BuildContext context, String? slogId,
      {int? page, int? pageSize}) async {
    YBDStatusGiftListEntity? data = await YBDHttpUtil.getInstance().doGet<YBDStatusGiftListEntity>(
      context,
      YBDStatusApi.STATUS_GIFT,
      params: {'slogId': slogId, 'page': page ?? 1, 'pageSize': pageSize ?? 999},
      baseUrl: await YBDStatusApi.getBaseUrl,
    );
    if (data?.code == Const.HTTP_SUCCESS_NEW) {
      //认证信息
      List<int?> userIds = [];

      data?.data?.records?.forEach((element) {
        if (!userIds.contains(element!.presentUserId)) {
          userIds.add(element.presentUserId);
        }
      });

      YBDCertificationUtil.getInstance()!.addUserCache(userIds);
    }
    return data ?? YBDStatusGiftListEntity();
  }

  static Future<YBDAgentShareEntity?> getAgentShare(BuildContext context) async {
    return await YBDHttpUtil.getInstance().doGet<YBDAgentShareEntity>(
      context,
      YBDStatusApi.AGENT_SHARE_DATA,
      baseUrl: await YBDStatusApi.getBaseUrl,
    );
  }

  /// slog 获取某个用户拉黑状态
  /// [operationType] 0 表示查询是否拉黑某个用户， 1 拉黑某个用户，2 解除拉黑
  static Future<YBDBlockUserEntity?> blockUserSearch(BuildContext? context, int operationType, int? toId) async {
    Map<String, dynamic> param = {'fromId': YBDUserUtil.getLoggedUserID(Get.context), 'toId': toId};
    if (operationType > 0) {
      param.addAll({'operationType': operationType});
      return await YBDHttpUtil.getInstance()
          .doPost(context, YBDStatusApi.USER_BLOCK, param, baseUrl: await YBDStatusApi.getBaseUrl, isJsonData: true);
    }
    return await YBDHttpUtil.getInstance()
        .doGet<YBDBlockUserEntity>(context, YBDStatusApi.USER_BLOCK, baseUrl: await YBDStatusApi.getBaseUrl, params: param);
  }

  /// 获取slog拉黑列表
  static Future<YBDSlogBlockListEntity?> slogBlockList(BuildContext? context,
      {int? page, int pageSize = Const.PAGE_SIZE}) async {
    Map<String, dynamic> param = {'fromId': YBDUserUtil.getLoggedUserID(Get.context), 'page': page, 'pageSize': pageSize};
    return await YBDHttpUtil.getInstance().doGet<YBDSlogBlockListEntity>(context, YBDStatusApi.USER_BLOCK_LIST,
        baseUrl: await YBDStatusApi.getBaseUrl, params: param);
  }
}
