

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDGiftPackageEntity with JsonConvert<YBDGiftPackageEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDGiftPackageRecord?>? record;
  String? recordSum;
}

class YBDGiftPackageRecord with JsonConvert<YBDGiftPackageRecord> {
  List<YBDGiftPackageRecordGift?>? gift;
  int? index;
  int? id;
  String? name;
}

class YBDGiftPackageRecordGift with JsonConvert<YBDGiftPackageRecordGift> {
  int? id;
  String? name;
  int? status;
  int? number;
  int? personalId;

  /// 商品购买限制条件类型  1 不限制购买 2 用户等级限制 3 主播等级限制 4 用户等级&直播等级限制
  int? conditionType;

  /// 限制条件内容  如 限制条件类型为2  extends 30 为限制用户等级为30才能购买
  String? conditionExtends;

  int? label;
  int? index;
  int? price;
  int? earn;
  int? agencyearn;
  int? expireAfter;

  String? img;
  dynamic animation;
  int? display;
  String? attribute;
  String? channel;
  int? animationDisplay;
  String? animationFile;
  YBDGiftPackageRecordGiftAnimationInfo? animationInfo;
}

class YBDGiftPackageRecordGiftAnimationInfo with JsonConvert<YBDGiftPackageRecordGiftAnimationInfo> {
  int? animationId;
  int? animationType;
  String? foldername;
  int? framDuration;
  String? name;
  int? version;
}
