

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';

class YBDBalanceRecordEntity with JsonConvert<YBDBalanceRecordEntity> {
	String? returnCode;
	String? returnMsg;
	YBDBalanceRecordRecord? record;
}

class YBDBalanceRecordRecord with JsonConvert<YBDBalanceRecordRecord> {
	int? bean;
	int? gem;
	int? gold;
}
