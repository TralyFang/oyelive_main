

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/generated/json/base/json_field.dart';

class YBDRoomBubbleEntity with JsonConvert<YBDRoomBubbleEntity> {
  @JSONField(name: "BUBBLE")
  YBDRoomBubbleBUBBLE? bUBBLE;
}

class YBDRoomBubbleBUBBLE with JsonConvert<YBDRoomBubbleBUBBLE> {
  String? image;
  int? itemId;
  String? name;
  int? expireAt;
  String? animation;
  int? cacheTime;
}
