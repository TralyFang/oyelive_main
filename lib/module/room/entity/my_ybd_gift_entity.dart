

import 'package:oyelive_main/generated/json/base/json_convert_content.dart';
import 'package:oyelive_main/module/room/entity/gift_ybd_package_entity.dart';

class YBDMyGiftEntity with JsonConvert<YBDMyGiftEntity> {
  String? returnCode;
  String? returnMsg;
  List<YBDMyGiftRecord?>? record;
  String? recordSum;
}

class YBDMyGiftRecord with JsonConvert<YBDMyGiftRecord> {
  String? type;
  List<YBDGiftPackageRecordGift?>? itemList;
}
