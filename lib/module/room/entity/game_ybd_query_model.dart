

import 'package:oyelive_main/common/global/tp_ybd_global.dart';
import 'package:oyelive_main/ui/page/room/define/room_ybd_define.dart';
import 'package:oyelive_main/ui/page/room/play_center/entity/game_ybd_entity.dart';

enum GameType {
  GameTypeGreedy,
  GameTypeLudo,
  GameTypeLucky1000,
  GameTypeTpGo,
}

///后续新增调整链接在这里新增配置
///接口修改 需要新增类型code标识
Map<GameType, String> GameCodes = {
  GameType.GameTypeGreedy: GREEDY_CODE,
  GameType.GameTypeLudo: LUDO_CODE,
  GameType.GameTypeLucky1000: LUCKY1000_CODE,
  GameType.GameTypeTpGo: TPGO_CODE,
};

class YBDGameQueryModel {
  List<YBDGameRecord?>? record;

  YBDGameQueryModel(this.record);

  ///组装需要拿[codes]中 的游戏配置信息
  static modelAssemble(List<YBDGameRecord?>? gameList) {
    YBDTPGlobal.model = YBDGameQueryModel(gameList);
  }
}
