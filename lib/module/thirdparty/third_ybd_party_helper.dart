

import 'package:flutter/cupertino.dart';
import 'package:oyelive_main/common/util/common_ybd_util.dart';
import '../../common/constant/const.dart';
import '../../common/http/http_ybd_util.dart';
import '../../common/reload_ybd_sp_event.dart';
import '../../common/util/date_ybd_util.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/sp_ybd_util.dart';
import '../../main.dart';
import 'entity/geo_ybd_location_entity.dart';
import 'entity/ip_ybd_api_entity.dart';

class YBDThirdParty {
  static Future<String?> getLocation(BuildContext? context) async {
    // LocationPermission permission = await checkPermission();
    // if (permission == LocationPermission.always || permission == LocationPermission.whileInUse) {
    //   logger.v('Getting location by Geolocator...');
    //   Position position = await GeolocatorPlatform.instance
    //       .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    //   logger.v('Got location by Geolocator...');
    //   final coordinates = new Coordinates(position.latitude, position.longitude);
    //   var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    //   if ((addresses?.first?.countryCode ?? '').isNotEmpty) {
    //     logger.v('Got location by Geolocator... ${addresses.first.countryCode}');
    //     return addresses.first.countryCode;
    //   }
    // }

    int startTime = YBDDateUtil.currentUtcTimestamp();

    YBDIpApiEntity? ipApiEntity = await YBDCommonUtil.getIp();
    if ((ipApiEntity?.countryCode ?? '').isEmpty) {
      YBDGeoLocationEntity? geoLocationEntity =
          await YBDHttpUtil.getInstance().doGet(context, 'http://geolocation-db.com/json', needErrorToast: false);

      YBDSPUtil.save(Const.SP_IP_COUNTRY_CODE, geoLocationEntity?.countryCode);
      eventBus.fire(YBDReloadSpEvent(Const.SP_IP_COUNTRY_CODE, geoLocationEntity?.countryCode));
      YBDLogUtil.d(
          'get ip country code through API 2: ${geoLocationEntity?.countryCode ?? ''}, duration: ${YBDDateUtil.currentUtcTimestamp() - startTime}');
      return geoLocationEntity?.countryCode;
    } else {
      YBDSPUtil.save(Const.SP_IP_COUNTRY_CODE, ipApiEntity!.countryCode);
      eventBus.fire(YBDReloadSpEvent(Const.SP_IP_COUNTRY_CODE, ipApiEntity.countryCode));
      YBDLogUtil.d(
          'get ip country code through API 1: ${ipApiEntity.countryCode}, duration: ${YBDDateUtil.currentUtcTimestamp() - startTime}');
      return ipApiEntity.countryCode;
    }
  }
}
