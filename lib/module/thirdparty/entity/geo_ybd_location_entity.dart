

import '../../../generated/json/base/json_convert_content.dart';
import '../../../generated/json/base/json_field.dart';

class YBDGeoLocationEntity with JsonConvert<YBDGeoLocationEntity> {
	@JSONField(name: "country_code")
	String? countryCode;
	@JSONField(name: "country_name")
	String? countryName;
	String? city;
	dynamic postal;
	double? latitude;
	double? longitude;
	@JSONField(name: "IPv4")
	String? iPv4;
	String? state;
}
