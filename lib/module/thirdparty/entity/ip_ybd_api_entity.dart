

import '../../../generated/json/base/json_convert_content.dart';
import '../../../generated/json/base/json_field.dart';

class YBDIpApiEntity with JsonConvert<YBDIpApiEntity> {
	String? status;
	String? country;
	String? countryCode;
	String? region;
	String? regionName;
	String? city;
	String? zip;
	double? lat;
	double? lon;
	String? timezone;
	String? isp;
	String? org;
	@JSONField(name: "as")
	String? xAs;
	String? query;
}
