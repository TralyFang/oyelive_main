

import 'dart:async';

import '../../common/constant/const.dart';
import '../../common/http/environment_ybd_config.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/remote_ybd_config_service.dart';

class YBDPaymentApi {
  static String? _baseUrl;

  static Future<String?> get getBaseUrl async {
    if (Const.TEST_ENV) {
      // return '${Const.TEST_URL}:${Const.TEST_PORT_PAYMENT}/';
      return '${YBDEnvConfig.getCurrentEnvPath}:${YBDEnvConfig.currentConfig().testPortPayment}/';
    }

    if (null == _baseUrl) {
      _baseUrl = await _apiBaseUrl();
    }

    return _baseUrl;
  }

  static Future<String> _apiBaseUrl() async {
    YBDRemoteConfigService? r = await YBDRemoteConfigService.getInstance();
    String? httpUrlProd = r?.getConfig()?.getString("payment_http_domain_prod");
    logger.v(" --- query firebase remote config , payment_http_domain_prod : $httpUrlProd");

    if ((httpUrlProd ?? '').isEmpty) {
      httpUrlProd = '${Const.PROD_URL}/';
    }

    logger.v('payment base url : $httpUrlProd');
    return httpUrlProd ?? '';
  }

  /// 公共支付
  static const PAY_ORDERS = "orders";

  /// 公共支付结果回调
  static const PAY_ORDERS_CALLBACKS = "callbacks/";

  /// easypaisa支付
  static const EPPAY_ORDERS = "orders/easypaisa";

  /// 公共支付-razorpay-成功支付结果通知服务器
  static const PAY_ORDERS_NOTIFY_RESULT_SU = "oyetalk-pay-center/pay_callback/_razorpay/_success";

  /// 公共支付-razorpay-失败支付结果通知服务器
  static const PAY_ORDERS_NOTIFY_RESULT_FA = "oyetalk-pay-center/pay_callback/_razorpay/_failure";

  /// 配置
  static final QUERY_PAY_CONFIGS = "oyetalk-user/configs";

  /// 苹果支付获取产品 ID 列表
  static const PAY_APPLE_PAY_CONFIG = "oyetalk-user/configs";

  /// 苹果支付预下单
  static const PAY_APPLE_PRE_ORDER = "oyetalk-pay-center/orders";

  /// 苹果支付分发购买内容
  static const PAY_APPLE_DELIVER_PRODUCT = "oyetalk-pay-center/callbacks/";
}
