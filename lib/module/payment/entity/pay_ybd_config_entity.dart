

import '../../../generated/json/base/json_convert_content.dart';

class YBDPayConfigEntity with JsonConvert<YBDPayConfigEntity> {
  String? code;
  List<YBDPayConfigData?>? data;
  bool? success;
}

class YBDPayConfigData with JsonConvert<YBDPayConfigData> {
  YBDPayConfigDataPayload? payload;
  String? nickName;
  String? name;
  String? icon;
  int? order;
  List<YBDPayConfigDataChild?>? child;
  String? currency;
  List<YBDPayConfigDataItem?>? items;
  double? rate;
  String? suffixIcon;
}

class YBDPayConfigDataPayload with JsonConvert<YBDPayConfigDataPayload> {
  String? regex;
  var desc;
  String? action;
}

class YBDPayConfigDataChild with JsonConvert<YBDPayConfigDataChild> {
  double? rate;
  YBDPayConfigDataPayload? payload;
  String? nickName;
  String? name;
  String? icon;
  String? currency;
  List<YBDPayConfigDataItem?>? items;
  int? order;
}

class YBDPayConfigDataItem with JsonConvert<YBDPayConfigDataItem> {
  double? money;
  String? productId;
  int? value;
}
