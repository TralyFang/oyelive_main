

import 'dart:async';

import 'package:advertising_id/advertising_id.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:oyelive_main/common/analytics/analytics_ybd_util.dart';

import '../../common/constant/const.dart';
import '../../common/http/http_ybd_util.dart';
import '../../common/util/config_ybd_util.dart';
import '../../common/util/log_ybd_util.dart';
import '../../common/util/sp_ybd_util.dart';
import '../../ui/page/recharge/razorpay/entity/razorpay_ybd_entity.dart';
import '../entity/result_ybd_bean.dart';
import '../status/status_ybd_api.dart';
import '../user/util/user_ybd_util.dart';
import 'entity/pay_ybd_config_entity.dart';
import 'payment_ybd_api.dart';

class YBDPaymentApiHelper {
  /// 公共支付预下单---RazorPay
  static Future<YBDRazorpayResultEntity?> razorPayOrders(
      BuildContext context, String targetId, String money, String currency) async {
    YBDLogUtil.d('razorPayOrders parameter is invalid targetId:$targetId money:$money currency$currency');
    if (context == null || currency == null || money == null) {
      YBDLogUtil.d('payOrders parameter is invalid context:$context currency:$currency money$money');
      return null;
    }
    Map<String, dynamic> params = {
      'userId': await YBDUserUtil.userId(),
      'targetId': targetId,
      'money': money,
      'type': Const.RAZOR_PAY,
      'currency': currency,
    };
    YBDRazorpayResultEntity? result = await YBDHttpUtil.getInstance().doPost<YBDRazorpayResultEntity>(
        context, YBDPaymentApi.PAY_ORDERS, params,
        baseUrl: await YBDPaymentApi.getBaseUrl, isJsonData: true);
    return result;
  }

  /// 公共支付预下单---通用
  static Future<YBDRazorpayResultEntity?> payOrders(BuildContext context, double? money, String? type, String? currency,
      {String? targetId, String? channel, String? productId}) async {
    logger.i('payOrders parameter targetId:$targetId money:$money  type:$type');
    if (context == null || money == null) {
      YBDLogUtil.d('payOrders parameter is invalid context:$context money$money');
      return null;
    }

    String baseUrl = await ConfigUtil.razorpayServerUrl(context);
    logger.i('payOrders baseUrl:$baseUrl');
    String? advertisingId;
    String? flyerUID;
    PackageInfo? packageInfo;
    try {
      advertisingId = await AdvertisingId.id(true);
      packageInfo = await PackageInfo.fromPlatform();
      flyerUID = await YBDAnalyticsUtil.appsflyerSdk?.getAppsFlyerUID();
    } on PlatformException catch (e) {
      logger.d('get phone info error:' + e.toString());
    }
    Map<String, dynamic> params = {
      'userId': await YBDUserUtil.userId(),
      'money': money,
      'type': type,
      'country': await YBDSPUtil.get(Const.SP_TOP_UP_COUNTRY_CODE),
      'channel': channel,
      'currency': currency, //货币类型
      'contextMap': {
        'productId': productId ?? '',
        'headers': {"advertising_id": advertisingId, "appsflyer_id": flyerUID, "app_version_name": packageInfo?.version}
      },
    };
    YBDRazorpayResultEntity? result = await YBDHttpUtil.getInstance().doPost<YBDRazorpayResultEntity>(context, YBDPaymentApi.PAY_ORDERS, params, baseUrl: baseUrl, isJsonData: true);
    logger.i('payOrders order Su: payOrder.data:${result?.toJson()}');
    return result;
  }

  /// 公共支付预下单---RazorPay--结果传递服务器
  static Future<bool> razorPayResultSu(BuildContext context, String paymentId, String orderId, String signature) async {
    Map<String, dynamic> params = {
//      'userId': await YBDUserUtil.userId(),
      'razorpay_payment_id': paymentId,
      'razorpay_order_id': orderId,
      'razorpay_signature': signature,
    };
    YBDResultBeanEntity? result = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
        context, YBDPaymentApi.PAY_ORDERS_NOTIFY_RESULT_SU, params,
        baseUrl: await YBDPaymentApi.getBaseUrl, isJsonData: true);
    if (result?.returnCode == Const.HTTP_SUCCESS) {
      YBDLogUtil.d('razorPayResult success');
      return true;
    } else {
      YBDLogUtil.d('razorPayResult failed');
      return false;
    }
  }

  /// 公共支付预下单---RazorPay--结果传递服务器
  static Future<bool> razorPayResultFa(BuildContext context, String orderId) async {
    Map<String, dynamic> params = {
      'userId': await YBDUserUtil.userId(),
      'orderId': orderId,
    };
    YBDResultBeanEntity? result = await YBDHttpUtil.getInstance().doPost<YBDResultBeanEntity>(
        context, YBDPaymentApi.PAY_ORDERS_NOTIFY_RESULT_FA, params,
        baseUrl: await YBDPaymentApi.getBaseUrl, isJsonData: true);
    if (result?.returnCode == Const.HTTP_SUCCESS) {
      YBDLogUtil.d('razorPayResult success');
      return true;
    } else {
      YBDLogUtil.d('razorPayResult failed');
      return false;
    }
  }

  /// 支付渠道配置
  static Future<YBDPayConfigEntity?> queryPayConfigs(BuildContext context, String dataId, String? country) async {
    return YBDHttpUtil.getInstance().doGet<YBDPayConfigEntity>(context, YBDPaymentApi.QUERY_PAY_CONFIGS,
        baseUrl: await YBDStatusApi.getBaseUrl, params: {'dataId': dataId, 'country': country});
  }

  /*3.google支付示例：
  {
  "payType":"google",
  "command":"success",
  "signedData":{},
  "signature":"signature"
  }
  失败示例：
  {
  "payType":"google",
  "command":"failure"
  }*/

  /// 结果回调服务器 google
  static Future<bool> requestCallbacksPurchase(
    BuildContext context, {
    required String orderId,
    String? payType,
    String? command,
    String? signedData,
    String? signature,
    String? errorMsg,
  }) async {
    YBDResultBeanEntity result = await requestCallbacksPurchaseAll(
      context,
      orderId: orderId,
      payType: payType,
      command: command,
      signedData: signedData,
      signature: signature,
      errorMsg: errorMsg,
    );
    YBDLogUtil.d('payOrders order Su: payOrder.data:${result.toJson()}');

    if (result.code == Const.HTTP_SUCCESS_NEW) {
      // YBDLogUtil.d('requestCallbacksPurchase success');
      return true;
    } else {
      // YBDLogUtil.d('requestCallbacksPurchase failed');
      return false;
    }
  }

  /// 结果回调服务器 google
  static Future<YBDResultBeanEntity> requestCallbacksPurchaseAll(
    BuildContext context, {
    required String orderId,
    String? payType,
    String? command,
    String? signedData,
    String? signature,
    String? errorMsg,
  }) async {
    Map<String, dynamic> params = {
      'payType': payType,
      'command': command,
      'signedData': signedData,
      'signature': signature,
      'errorMsg': errorMsg,
    };

    String baseUrl = await ConfigUtil.razorpayServerUrl(context);
    YBDLogUtil.d('requestCallbacksPurchaseAll baseUrl:$baseUrl');

    YBDResultBeanEntity result = (await YBDHttpUtil.getInstance().doPatch<YBDResultBeanEntity>(
        context, YBDPaymentApi.PAY_ORDERS_CALLBACKS + orderId, params,
        baseUrl: baseUrl, isJsonData: true)) ?? YBDResultBeanEntity();
    YBDLogUtil.d('requestCallbacksPurchaseAll order Su: payOrder.data:${result.toJson()}');

    return result;
  }

  /// 结果回调服务器 ()
  static Future<YBDResultBeanEntity> callbackPurchase(
    BuildContext context, {
    required String orderId,
    Map<String, dynamic>? params,
  }) async {
    String baseUrl = await ConfigUtil.razorpayServerUrl(context);
    YBDLogUtil.d('purchaseCallbackPurchase baseUrl:$baseUrl');

    YBDResultBeanEntity result = (await YBDHttpUtil.getInstance().doPatch<YBDResultBeanEntity>(
        context, YBDPaymentApi.PAY_ORDERS_CALLBACKS + orderId, params,
        baseUrl: baseUrl, isJsonData: true)) ?? YBDResultBeanEntity();
    YBDLogUtil.d('purchaseCallbackPurchase order Su: payOrder.data:${result.toJson()}');

    return result;
  }
}
