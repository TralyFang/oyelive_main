

import '../common/constant/const.dart';
import '../common/http/environment_ybd_config.dart';
import '../common/util/log_ybd_util.dart';
import '../common/util/remote_ybd_config_service.dart';

class YBDApi {
  static Future<String?> get getBaseUrl async {
    // return "http://ybd.k8s.test.live/";
    if (Const.TEST_ENV) {
      // TODO: 测试代码 跳转到生产环境
      // if (YBDEnvConfig.getCurrentEnvPath().contains('live')) {
      //   return '${Const.PROD_URL}/';
      // }
      if (YBDEnvConfig.currentConfig().testPort.isEmpty) {
        return '${YBDEnvConfig.getCurrentEnvPath()}/';
      }
      return '${YBDEnvConfig.getCurrentEnvPath()}:${YBDEnvConfig.currentConfig().testPort}/';
    }

    return await _apiBaseUrl();
  }

  /// 获取baseUrl
  /// 先从远端获取baseUrl
  /// 远端返回空值则取默认的baseUrl
  /// 远端返回有效值则用远端获取的baseUrl
  static Future<String?> _apiBaseUrl() async {
    String? httpUrlProd = await YBDRemoteConfigService.baseUrlFromRemote('http_domain_prod');

    if ((httpUrlProd ?? '').isEmpty) {
      httpUrlProd = '${Const.PROD_URL}/';
    }

    logger.v('http base url : $httpUrlProd');
    return httpUrlProd;
  }

//  -------------------------------------------------------------------
  static final UPLOAD_FILE = 'common/uploadfile';

  static final QUERY_CONFIGS = "common/queryconfigs";
  static final QUERY_CONCERN = "common/queryconcern";
  @deprecated
  static final QUERY_ROOM_LABEL = "room/queryroomlabel";
  static final QUERY_GUARDIAN = "room/queryroommanager"; // 查询我的守护
  static final QUERY_GUARDED_ROOM = "room/querymanagedroom"; // 查询我守护的房间
  static final DEL_GUARDIAN = "room/delroommanager";
  static final COMPLAIN_ADD = "complain/add";

  static final APP_LOGIN = "sns/applogin";
  static final APP_LINK = "sns/link";
  static final APP_UNLINK = "sns/unlink";
  static final APP_ACCOUNT_DELETE = "um/userPurchaseApply";
  static final VALID_PHONE = "um/mobileSignIn";
  static final USERNAME_PASSWORD_LOGIN = "um/applogin";
  static final CHECK_AUTH_CODE = "um/verifyToken";
  static final PROCESS_TOKEN = "um/processToken";
  static final GENERATE_TOKEN = "um/generateToken";
  static final RESET_PWD = "um/mobileResetPwd";
  static final BIND_PHONE = "um/bindPhoneNumber";
  static final CHECK_LOGIN = "um/checklogin";
  static final QUERY_USER = "um/queryuser";
  static final ADD_LOCATION = "um/addlocation";
  static final SEARCH_USER = "um/searchuser";
  static final FOLLOW_USER = "um/addconcern";
  static final UNFOLLOW_USER = "um/delconcern";
  static final EDIT_USER_INFO = "um/edituserinfo";
  static final EDIT_IMAGE = "um/editimage";

  static final QUERY_MY_INVITES = "um/myInvites";
  static final POST_DEVICE_UNIQUE_ID = "um/addDevice";

  static final TOP_UP_SUMMARY = "um/topUpSummary";
  static final TOP_UP_RECORD = "oyetalk-pay-center/orders_search";

  /// 首页广告
  static final HOME_BANNER_AD = "home/advertise";

  /// Popular 房间列表
  static final POPULAR_LIST = "rank/popular";

  /// explore 房间列表
  static final EXPLORE_LIST = "rank/label";

  /// 礼物排行榜接口
  static final RANK_RICH = "rank/rich";

  /// 粉丝排行榜接口
  static final RANK_FLOWS = "rank/flows";

  /// 我的任务接口
  static final DAILY_TASK = "um/querymission";

  /// 安全弹窗接口
  static final SAFETY_LEVEL = "um/queryUserSafetyLevel";

  /// 每日任务接口
  static final THE_TASK = "um/queryDailyList";

  /// 完成每日任务接口
  static final COMPLETE_TASK = "um/missionreward";

  /// 完成每日任务接口
  static final COMPLETE_DAILY_TASK = "um/userDailyReward";

  static final QUERY_FOLLOWED = "um/queryconcern";

  /// 礼物列表接口
  static final GIFT_LIST = "um/gifts";

  /// store discount列表接口
  static final STORE_DISCOUNT_LIST = "common/specialOffer";

  /// 汽车列表接口
  static final CAR_LIST = "common/querycar";

  static final QUERY_STAR = "rank/star";

  /// 获取好友列表
  static const QUERY_FRIEND_LIST = "um/queryFriends";

  /// 查询是否为好友
  static const QUERY_FRIEND = "common/queryfriend";

  /// 首充礼物列表接口
  static final FIRST_TOP_UP_GIFT = "um/first-top-up/rewards";

  /// combo 礼物列表接口
  static final COMBO_GIFT_LIST = "rank/gift/combo";

  /// cp 列表接口
  static final CP_BOARD_LIST = "rank/queryTalentUserCp";

  /// 查询VIP
  static const QUERY_VIP = "common/queryvip";

  /// 查询折扣
  static const QUERY_DISCOUNT = "common/querydiscount";

  //查询个人信息
  static const QUERY_PERSONAL = "um/querypersonal";

  /// 商店边框查询
  static const QUERY_MIC_FRAME = "common/queryFrame";

  /// qi气泡查询
  static const QUERY_BUBBLE = "common/queryBubble";

  /// 商店主题查询
  static const QUERY_THEME = "common/queryTheme";

  /// 查询可配置的活动列表
  static const QUERY_CONFIG_ACTIVITY = "um/popUp";

  /// ordercar
  static const ORDER_CAR = "um/ordercar";

  /// purchase
  static const PURCHASE = "um/purchase";

  /// purchase
  static const ORDER_VIP = "um/ordervip";

  static const ORDER_BUBBLE = "um/orderBubble";

  static const QUERY_ONLINE = "um/onlineinfo"; // 进入房间，查询单个房间详细信息（在线状态）
  static final QUERY_ROOM_MANAGER = "room/queryroommanager"; // 查询房间管理员 （守护）

  static final QUERY_GAME = "common/query/game"; // 查询游戏
  static final QUERY_GAME_STATUS = "um/gameStatus"; // 查询游戏状态

  static final EQUIP = "um/equip"; // 装备 边框款  主题

  static final UN_EQUIP = "um/unequip"; // 取消装备 边框款  主题

  /// 公共支付
  static const PAY_ORDERS = "orders";

  /// 公共支付-razorpay-成功支付结果通知服务器
  static const PAY_ORDERS_NOTIFY_RESULT_SU = "pay_callback/_razorpay/_success";

  /// 公共支付-razorpay-失败支付结果通知服务器
  static const PAY_ORDERS_NOTIFY_RESULT_FA = "pay_callback/_razorpay/_failure";

  // TODO:
  // 用 payment_api 里的 baseUrl
  /// 苹果支付 baseUrl
  static String? _basePayUrl;

  static String? get basePayUrl {
    if (null == _basePayUrl) {
      _basePayUrl = Const.TEST_ENV ? 'http://${YBDEnvConfig.currentConfig().statusTestIp}/' : '${Const.STATUS_PROD_URL}/';
    }
    return _basePayUrl;
  }

  /// 查询房间黑名单
  static const QUERY_ROOM_BLACKLIST = "talent/queryblacklist";

  /// 删除房间黑名单
  static const DELETE_ROOM_BLACKLIST = "talent/delblacklist";

  /// hotgame 配置
  static const GAME_INFO = "common/query/game";

  /// unequipItem
  static const UNEQUIP = "um/unequip";

  ///historyTeenPatti
  static const TEENPATTI_HISTORY = "um/teenpatti";

  //recordTeenPatti richest
  static const TEENPATTI_Record_RichEST = "rank/teenpatti/richest";

  //recordTeenPatti luckiest
  static const TEENPATTI_Record_LUCKIEST = "rank/teenpatti/luckiest";

  /// 邀请信息
  static const INVITE_INFO = "um/getShareInfo";

  /// bean detail
  static const BEAN_DETAIL = "um/bean-income";

  /// gems detail
  static const GEMS_DETAIL = "um/userGemIncome";

  /// 金币获得记录
  static const GOLD_DETAIL = "um/userGoldIncome";

  /// 我的邀请
  static const MY_INVITE = "um/queryInviteList";

  ///查询用户充值的勋章
  static const QUERY_BADGE_LIST = "um/queryBadgeList";

  ///修改talent信息
  static const EDIT_TALENT = "talent/edittalent";

  ///佩戴用户勋章
  static const USER_UPDATEINFO = 'um/userUpdateInfo';

  /// Returns experience required for each level and their rights etc
  static const QUERY_LEVEL = 'common/querylevel';

  /// pk记录
  static const PK_RECORD = 'um/pkRecords';

  ///用户是否pk房主播
  static const PK_STATE = 'um/pkStatus';

  ///查询是否是vip
  static const QUERY_TEENPATTI_VIP = 'um/queryTeenPattiRecord';

  ///查询用户手机号
  static const GET_USER_PHONE = 'um/getPhoneNumber';

  ///改密码
  static const CHANGE_PWD = 'um/changePassword';

  ///查询余额
  static const GET_BALANCE = 'um/balance';

  ///查询认证
  static const QUERY_CERTIFICATION = 'um/queryCerByUserIds';

  ///设备信息上传
  static final POST_PHONE_INFO = "um/addmobile";

  /// 兑换金币
  static const EXCHANGE = 'um/exchange';

  /// Royal Pattern挽留
  static const TP_SAVE = 'um/teenpatti/leave';

  /// Royal Pattern邀请
  static const TP_INVITE_OFFLINE = 'um/teenpatti/recall/popup';

  /// Royal Pattern邀请领取
  ///

  static const TP_INVITE_OFFLINE_RETRIEVE = 'um/teenpatti/recall/claim';

  ///fcm token上报

  static const FCM_TOKEN_UPLOAD = 'oyetalk-user/userActives/_info';

  ///pk设置
  static const PK_SETTING = "um/pk/invitation/setting";

  ///pk邀请过的历史
  static const TARGET_PK_INVITE_HISTORY = "um/pk/invitation/played";

  ///pk邀请过的朋友历史
  static const TARGET_PK_INVITE_F_HISTORY = "um/pk/invitation/friends";

  /// 查询修改昵称和修改头像剩余机会（时间）
  static const QUERY_RENAME_TIMES = 'um/queryRemainingTimes';

  ///即构CODE获取
  static const ZEGO_GET_CODE = 'oyetalk-uc/users/getZegoCode';

  /// 即构小游戏appkey
  static const ZEGO_MG_APP_KEY = "oyetalk-chat/game-config/zego/token";

  /// 查询靓号
  static const QUERY_UID = "common/queryUniqueId";

  /// 购买靓号
  static const BUY_UID = "um/orderUniqueId";

  /// 续费靓号
  static const RENEW_UID = "um/renewUniqueId";

  /// Game - API
  /// 游戏列表
  static const GAME_ROOM_LIST = "oyetalk-room/room/page";

  /// 游戏分类查询
  static const GAME_CATEGORY_SEARCH = "oyetalk-chat/room-category/search";

  /// 游戏分类查询
  static const GAME_CATEGORY_SEARCH_V2 = "oyetalk-chat/room-category/v2/search";

  /// 游戏快捷聊天查询
  static const GAME_QUICK_COMMENT_LIST = "oyetalk-chat/game-config/chat/config";

  /// 快速加入 获取房间信息
  static const QUICK_JOIN = "oyetalk-chat/game/quick-join";

  /// Ludo人机局
  static const QUICK_GAME = "oyetalk-chat/game/quick-game";

  /// 房间礼物列表
  static const GAME_GIFT_LIST = "oyetalk-chat/gift/search";

  /// 游戏房间配置信息
  static const GAME_CONFIG_DISPLAY = "oyetalk-chat/game-config/display";

  /// 游戏投降
  static const GAME_GIVE_UP = "oyetalk-chat/game/giveUp";

  /// 图形验证码
  static const IMAGE_CODE = "um/getImageCode";

  ///金刚区列表
  static const KINGKONG = 'home/kingkong';

  /// 上传用户背景图片
  static const UPLOAD_BG = "um/backgroundUpload";

  /// 替换用户背景图片
  static const REPLACE_BG = "um/backgroundReplace";

  /// 删除用户背景图片
  static const DELETE_BG = "um/backgroundRemove";

  /// 用户上传背景图片限制
  static const QUERY_UPLOAD_BG_RESTRICT = "um/queryBackgroundUploadStatus";

  static final QUERY_CARD_FRAME = "common/queryCardFrame";

  static const ORDER_CARD_FRAME = "um/orderCardFrame";

  /// 获取新人任务状态
  static const QUERY_NEW_USER_STATUS = "um/querymission/newuser/status";

  /// 上报房间分享
  static const REPORT_ROOM_SHARE = "room/share";

  /// 活动中心
  static const EVENT_CENTER_LIST = "oyetalk-marketing/center/activityForApp";

  /// 获取服务器当前时间
  static const SERVER_TIME = 'activity/getServerTime';

  /// 获取all emoji
  static const QUERY_EMOJIS = '/cs/queryEmojis';

  /// 获取self emoji
  static const QUERY_USER_EMOJIS = '/cs/queryUserEmojis';
}
