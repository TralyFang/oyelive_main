
import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class AmazonS3Cognito {
  String? filepath;
  String? bucket;
  String? identity;
  String? imageName;
  String? region;
  String? subRegion;

  AmazonS3Cognito({
    this.filepath,
    this.bucket,
    this.identity,
    this.imageName,
    this.region,
    this.subRegion,
  });

  static const MethodChannel _channel = const MethodChannel('amazon_s3_cognito');
  static const EventChannel _stream = const EventChannel('amazon_s3_cognito/progress');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

//  static Future<String> uploadImage(String filepath, String bucket, String identity) async {
//    final Map<String, dynamic> params = <String, dynamic>{
//      'filePath': filepath,
//      'bucket': bucket,
//      'identity': identity,
//    };
//    final String imagePath = await _channel.invokeMethod('uploadImageToAmazon', params);
//    return imagePath;
//  }

  Future<String?> upload() async {
    final Map<String, dynamic> params = <String, dynamic>{
      'filePath': filepath,
      'bucket': bucket,
      'identity': identity,
      'imageName': imageName,
      'region': region,
      'subRegion': subRegion
    };
    final String? imagePath = await _channel.invokeMethod('uploadImage', params);
    return imagePath;
  }

  Stream get getProgress => _stream.receiveBroadcastStream();

  Future<String?> delete() async {
    final Map<String, dynamic> params = <String, dynamic>{
      'bucket': bucket,
      'identity': identity,
      'imageName': imageName,
      'region': region,
      'subRegion': subRegion
    };
    final String? imagePath = await _channel.invokeMethod('deleteImage', params);
    return imagePath;
  }
}
