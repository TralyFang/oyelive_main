package com.roughike.fluttertwitterlogin.fluttertwitterlogin

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Dialog
import android.net.Uri
import android.os.Build
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import twitter4j.Twitter
import twitter4j.TwitterException
import twitter4j.TwitterFactory
import twitter4j.auth.AccessToken
import twitter4j.conf.ConfigurationBuilder
import java.util.*

class TwitterPlugin {
    private var registrar: Registrar? = null
    private var pendingResult: Result? = null

    var twitter: Twitter? = null
    var callbackUrl: String? = null

    var twitterDialog: Dialog? = null
    var accessToken: AccessToken? = null

    private fun initializeAuthClient(call: MethodCall): Twitter? {
        if (twitter == null) {
            val consumerKey = call.argument<String>("consumerKey")
            val consumerSecret = call.argument<String>("consumerSecret")
            callbackUrl = call.argument<String>("callbackUrl")
            val builder = ConfigurationBuilder()
                    .setDebugEnabled(true)
                    .setOAuthConsumerKey(consumerKey)
                    .setOAuthConsumerSecret(consumerSecret)
                    .setIncludeEmailEnabled(true)
            val configuration = builder.build()
            val twitterFactory = TwitterFactory(configuration)
            twitter = twitterFactory.instance
        }
        return twitter
    }

    fun authorize(call: MethodCall, result: Result, r: Registrar) {
        registrar = r
        pendingResult = result

        // init twitter auth client
        initializeAuthClient(call)

        GlobalScope.launch(Dispatchers.Default) {
            try {
                val requestToken = twitter!!.oAuthRequestToken
                withContext(Dispatchers.Main) {
                    setupTwitterWebviewDialog(requestToken.authorizationURL)
                }
            } catch (e: TwitterException) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    if (pendingResult != null) {
                        val resultMap: HashMap<String?, Any?> = object : HashMap<String?, Any?>() {
                            init {
                                put("status", "error")
                                put("errorMessage", e.message)
                            }
                        }
                        pendingResult!!.success(resultMap)
                        pendingResult = null
                    }
                }
            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun setupTwitterWebviewDialog(url: String) {
        if (registrar!!.activity() == null) {
            return
        }

        twitterDialog = Dialog(registrar!!.activity())
        val webView = WebView(registrar!!.activity())
        webView.isVerticalScrollBarEnabled = false
        webView.isHorizontalScrollBarEnabled = false
        webView.webViewClient = TwitterWebViewClient()
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(url)
        twitterDialog!!.setContentView(webView)
        twitterDialog!!.show()
    }

    inner class TwitterWebViewClient : WebViewClient() {
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            if (callbackUrl?.let { request.url.toString().startsWith(it) }!!) {
                Log.d("TwitterWebViewClient", "Authorization URL: " + request.url.toString())
                handleUrl(request.url.toString())

                // Close the dialog after getting the oauth_verifier
                if (request.url.toString().contains(callbackUrl!!)) {
                    twitterDialog?.dismiss()
                }
                return true
            }
            return false
            //            return super.shouldOverrideUrlLoading(view, request);
        }

        private fun handleUrl(url: String) {
            val uri = Uri.parse(url)
            val oauthVerifier = uri.getQueryParameter("oauth_verifier")

            GlobalScope.launch(Dispatchers.Default) {
                try {
                    accessToken = twitter?.getOAuthAccessToken(oauthVerifier)
                    withContext(Dispatchers.Main) {
                        if (pendingResult != null) {
                            val sessionMap: HashMap<String?, Any?>? = tokenToMap()
                            val resultMap: HashMap<String?, Any?> = object : HashMap<String?, Any?>() {
                                init {
                                    put("status", "loggedIn")
                                    put("session", sessionMap)
                                }
                            }
                            pendingResult!!.success(resultMap)
                            pendingResult = null
                        }
                    }
                } catch (e: TwitterException) {
                    e.printStackTrace()
                    withContext(Dispatchers.Main) {
                        if (pendingResult != null) {
                            val resultMap: HashMap<String?, Any?> = object : HashMap<String?, Any?>() {
                                init {
                                    put("status", "error")
                                    put("errorMessage", e.message)
                                }
                            }
                            pendingResult!!.success(resultMap)
                            pendingResult = null
                        }
                    }
                }
            }
        }

        private fun tokenToMap(): HashMap<String?, Any?>? {
            return if (accessToken == null) {
                null
            } else object : HashMap<String?, Any?>() {
                init {
                    put("secret", accessToken!!.tokenSecret)
                    put("token", accessToken!!.token)
                    put("userId", accessToken!!.userId)
                    //            put("username", session.getUserName());
                }
            }
        }
    }
}
