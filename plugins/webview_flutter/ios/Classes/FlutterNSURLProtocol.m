//
//  FlutterNSURLProtocol.m
//  Pods-Runner
//
//  Created by yanxx on 2019/11/18.
//
#import <Foundation/Foundation.h>
#import "FlutterNSURLProtocol.h"
#import <UIKit/UIKit.h>
#import "FlutterInstance.h"
#import <Flutter/Flutter.h>
#import <WebKit/WebKit.h>

static NSString* const KFlutterNSURLProtocolKey = @"KFlutterNSURLProtocol";
@interface FlutterNSURLProtocol ()<NSURLSessionDelegate>
@property (nonnull,strong) NSURLSessionDataTask *task;

@end
@implementation FlutterNSURLProtocol
+ (BOOL)canInitWithRequest:(NSURLRequest *)request
{
//    NSLog(@"request.URL.absoluteString = %@",[request.URL.absoluteString substringToIndex:10]);
    NSString *scheme = [[request URL] scheme];
    if ( ([scheme caseInsensitiveCompare:@"http"]  == NSOrderedSame ||
          [scheme caseInsensitiveCompare:@"https"] == NSOrderedSame ))
    {
        //看看是否已经处理过了，防止无限循环
        if ([NSURLProtocol propertyForKey:KFlutterNSURLProtocolKey inRequest:request])
            return NO;
        return YES;
    }
    return NO;
}

+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request
{
    NSMutableURLRequest *mutableReqeust = [request mutableCopy];
//    NSLog(@"client:%@",[]);
    //request截取重定向
//    if ([request.URL.absoluteString isEqualToString:sourUrl])
//    {
//        NSURL* url1 = [NSURL URLWithString:localUrl];
//        mutableReqeust = [NSMutableURLRequest requestWithURL:url1];
//    }

    return mutableReqeust;
}

+ (BOOL)requestIsCacheEquivalent:(NSURLRequest *)a toRequest:(NSURLRequest *)b
{
    return [super requestIsCacheEquivalent:a toRequest:b];
}

- (void)startLoading
{
    NSMutableURLRequest *mutableReqeust = [[self request] mutableCopy];
    //给我们处理过的请求设置一个标识符, 防止无限循环,
    [NSURLProtocol setProperty:@YES forKey:KFlutterNSURLProtocolKey inRequest:mutableReqeust];
   
    NSString *agent = [mutableReqeust valueForHTTPHeaderField:@"User-Agent"];
    
    
    FlutterMethodChannel *channel = [FlutterInstance channelWithAgent:agent];
    NSString *url = mutableReqeust.URL.absoluteString;
    NSLog(@"===========agent:%@, url:%@",agent, url);
    if(channel==nil){
        NSLog(@"===========channel null:%@",agent);
        dispatch_async(dispatch_get_main_queue(), ^{
            // 重新设置回cookie
            [self setCookieValueHTTPHeaderField:mutableReqeust];
        });
        return;
    }
    [channel invokeMethod:@"shouldInterceptRequest" arguments:url result:^(id  _Nullable result) {
        if(result!=nil && [result isKindOfClass:[NSDictionary class]]){
            
            NSDictionary *dic = (NSDictionary *)result;
            FlutterStandardTypedData *fData = (FlutterStandardTypedData *)[dic valueForKey:@"data"];
            NSString *mineType = dic[@"mineType"];
            NSString *encoding = dic[@"encoding"];
            if([encoding isEqual:[NSNull null]])encoding = nil;
            NSData *data = [fData data];
            NSLog(@"data.length:%lu, type:%@, encoding:%@",(unsigned long)data.length, mineType, encoding);
            NSURLResponse* response = [[NSURLResponse alloc] initWithURL:self.request.URL MIMEType:mineType expectedContentLength:data.length textEncodingName:encoding];
                    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageAllowed];
                    [self.client URLProtocol:self didLoadData:data];
                    [self.client URLProtocolDidFinishLoading:self];
        }else{
            // 重新设置回cookie
            [self setCookieValueHTTPHeaderField:mutableReqeust];
            
        }
    }];
}

- (void)setCookieValueHTTPHeaderField:(NSMutableURLRequest *)mutableReqeust {
    [[WKWebsiteDataStore defaultDataStore].httpCookieStore getAllCookies:^(NSArray<NSHTTPCookie *> * _Nonnull cookies) {
//        JSESSIONID=19be7215-1791-4173-8e63-9cbf0c5eaca6; app=6430494_19be7215-1791-4173-8e63-9cbf0c5eaca6
        NSString *cookieString = @"";
        for (NSHTTPCookie *cookie in cookies) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
            cookieString = [cookieString stringByAppendingFormat:@"%@=%@;",cookie.name, cookie.value];
        }
        if (![cookieString isEqualToString:@""]) {
            [mutableReqeust setValue:cookieString forHTTPHeaderField:@"Cookie"];
        }
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:nil];
                    self.task = [session dataTaskWithRequest:mutableReqeust];
                    [self.task resume];
        });
        NSLog(@"===========channel not intercept url:%@, allHeader:%@, body:%@, method:%@",mutableReqeust.URL, mutableReqeust.allHTTPHeaderFields, mutableReqeust.HTTPBody, mutableReqeust.HTTPMethod);
    }];
}


- (void)stopLoading
{
    if (self.task != nil)
    {
        [self.task  cancel];
    }
}


- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    [[self client] URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageAllowed];
    
    completionHandler(NSURLSessionResponseAllow);
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    [[self client] URLProtocol:self didLoadData:data];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(nullable NSError *)error {
    [self.client URLProtocolDidFinishLoading:self];
}

@end
