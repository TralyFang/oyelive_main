//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<alpha_player_plugin/AlphaPlayerPlugin.h>)
#import <alpha_player_plugin/AlphaPlayerPlugin.h>
#else
@import alpha_player_plugin;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [AlphaPlayerPlugin registerWithRegistrar:[registry registrarForPlugin:@"AlphaPlayerPlugin"]];
}

@end
