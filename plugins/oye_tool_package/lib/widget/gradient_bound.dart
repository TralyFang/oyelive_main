import 'package:flutter/material.dart';

// https://gitee.com/island-coder/flutter-beginner/blob/master/custom_paint/lib/painter/gradient_bound_demo.dart
class GradientBound extends StatelessWidget {
  final Gradient gradient;
  final double radius;
  final double strokeWidth;
  final Widget child;

  const GradientBound({
    Key? key,
    required this.gradient,
    required this.radius,
    required this.strokeWidth,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: GradientBoundPainter(
        gradient: gradient,
        radius: radius,
        strokeWidth: strokeWidth,
      ),
      child: child,
    );
  }
}

class GradientBoundPainter extends CustomPainter {
  final Gradient gradient;
  final double radius;
  final double strokeWidth;

  const GradientBoundPainter({
    Key? key,
    required this.gradient,
    required this.radius,
    required this.strokeWidth,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Rect rect = Offset.zero & size;
    var outerRRect = RRect.fromRectAndRadius(rect, Radius.circular(radius));

    final paint = Paint()
      ..shader = gradient.createShader(rect)
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;
    Path path = Path()..addRRect(outerRRect);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant GradientBoundPainter oldDelegate) {
    return oldDelegate.gradient != gradient;
  }
}
