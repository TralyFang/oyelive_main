import 'package:flutter/material.dart';

/// 多张动画间隔运动
class CardOpenWidget extends StatefulWidget {
  Size cardSize;
  List<Widget> children;
  CardOpenWidget({Key? key, required this.cardSize, required this.children}) : super(key: key);

  @override
  _CardOpenWidgetState createState() => _CardOpenWidgetState();
}

class _CardOpenWidgetState extends State<CardOpenWidget> {
  @override
  Widget build(BuildContext context) { // 3/3, 5/3, 7/3  012 024
    return SizedBox(
      width: widget.cardSize.width*(3+widget.children.length*2)/3,
      height: widget.cardSize.height,
      child: Stack(
        children: pokerCards(),
      ),
    );
  }
  List<Widget> pokerCards() {
    return widget.children.asMap().map((key, Widget value) {
      return MapEntry(key, Positioned(
        left: widget.cardSize.width*(key*1/3),
        top: 0,
        child: CardItemAnimation(
          offset: Offset(widget.cardSize.width*(key*1/3+1), 0.0),
          child: SizedBox(
            width: widget.cardSize.width,
              height: widget.cardSize.height,
              child: value),
          delay: Duration(milliseconds: (widget.children.length -1 - key) * 300),
        ),
      ));
    }).values.toList();
  }
}

// 单张牌运动动画
class CardItemAnimation extends StatefulWidget {
  Offset offset;
  Widget child;
  Duration? delay;

  CardItemAnimation({Key? key, required this.child, required this.offset, this.delay})
      : super(key: key);

  @override
  _CardItemAnimationState createState() => _CardItemAnimationState();
}

class _CardItemAnimationState extends State<CardItemAnimation>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<Offset> _slideAnimation;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    _slideAnimation = Tween<Offset>(
            begin: Offset(-widget.offset.dx, 0.0), end: const Offset(0.0, 0.0))
        .animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Curves.easeOutBack,
      ),
    );

    if (widget.delay != null) {
      Future.delayed(widget.delay!, () {
        _animationController.forward();
      });
    } else {
      _animationController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(animation: _slideAnimation, builder: (context, child){
      if (_animationController.value == 0.0){
        return Container();
      }
      return Transform.translate(offset: _slideAnimation.value, child: widget.child,);
    });
    // return SlideTransition(
    //   position: _slideAnimation,
    //   child: widget.child,
    // );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
