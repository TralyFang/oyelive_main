/*import 'package:shared_preferences/shared_preferences.dart';

class HiCache {

  late SharedPreferences prefs;

  /// 预初始化，防止在get使用时，sharedPreference还未初始化
  static Future<HiCache> preInstance() async {
    if (_instance == null) {
      var prefs = await SharedPreferences.getInstance();
      _instance = HiCache._pre(prefs);
    }
    return _instance!;
  }

  factory HiCache() => instance;
  static HiCache? _instance;
  static HiCache get instance => _instance ??= HiCache._();

  HiCache._();
  HiCache._pre(this.prefs);

  setString(String key, String value) {
    prefs.setString(key, value);
  }

  setDouble(String key, double value) {
    prefs.setDouble(key, value);
  }

  setInt(String key, int value) {
    prefs.setInt(key, value);
  }

  setBool(String key, bool value) {
    prefs.setBool(key, value);
  }

  setStringList(String key, List<String> value) {
    prefs.setStringList(key, value);
  }

  Object? get<T>(String key) {
    return prefs.get(key);
  }

}*/