import 'dart:math';

extension IntExt on int {
  /// 获取min(this)~max之间的随机数
  int toRandomMax(int max) {
    Random random = Random.secure();
    int randomInt = random.nextInt(max);
    return this + randomInt;
  }
}