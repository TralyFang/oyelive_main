extension StringExt on String {

  /// 将带有http开头的字符串转为Uri，获取queryParameters中的key对应的value
  String? valueUriForKey(String key) {
    if (!startsWith("http")) return null;
    Uri parseUri = Uri.parse(this);
    if (parseUri.queryParameters.containsKey(key)) {
      return parseUri.queryParameters[key];
    }
    return null;
  }
}