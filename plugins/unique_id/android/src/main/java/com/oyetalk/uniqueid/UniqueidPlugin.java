package com.oyetalk.uniqueid;

import androidx.annotation.NonNull;
import android.media.MediaDrm;
import android.media.UnsupportedSchemeException;
import java.util.UUID;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** UniqueidPlugin */
public class UniqueidPlugin implements FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "uniqueid");
    channel.setMethodCallHandler(this);
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "uniqueid");
    channel.setMethodCallHandler(new UniqueidPlugin());
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("getAndroidMediaDrm")) {
      //to-do return drm

      result.success(getmediaDrmId());
    } else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  private String getmediaDrmId(){
    String mediaDrmId = null;
//         final UUID mockScheme = new UUID(0x0102030405060708L, 0x090a0b0c0d0e0f10L);
    final UUID mockScheme = new UUID(-0x121074568629b532L, -0x5c37d8232ae2de13L);
    MediaDrm mediaDrm = null;
    try {
      mediaDrm = new MediaDrm(mockScheme);
      byte[] propertyByteArray = mediaDrm.getPropertyByteArray(MediaDrm.PROPERTY_DEVICE_UNIQUE_ID);
     /* for(int i=0;i<propertyByteArray.length;i++){
        Logger.d("MediaDrm mediaDrmId i: " + propertyByteArray[i]);
      }*/

//      mediaDrmId = new String(propertyByteArray,"GBK");
      mediaDrmId = bytesToString(propertyByteArray);
//      log.d("MediaDrm mediaDrmId ---: " + bytesToString(propertyByteArray));
    } catch (UnsupportedSchemeException e) {
      e.printStackTrace();
    }catch (Exception e){
      e.printStackTrace();
    }

    return mediaDrmId;
  }

  public static String bytesToString(byte[] bytes) {
    StringBuilder stringBuilder = new StringBuilder(bytes.length);
    for (byte byteChar : bytes)
      stringBuilder.append(String.format("%02X", byteChar));

    return stringBuilder.toString();
  }
}
