import 'dart:async';

import 'package:flutter/services.dart';

class Uniqueid {
  static const MethodChannel _channel = const MethodChannel('uniqueid');

  static Future<String> get getAndroidMediaDrm async {
    final String version = await _channel.invokeMethod('getAndroidMediaDrm');
    return version;
  }
}
