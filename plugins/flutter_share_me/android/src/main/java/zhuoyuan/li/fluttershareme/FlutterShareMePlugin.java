package zhuoyuan.li.fluttershareme;


import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.MalformedURLException;
import java.net.URL;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import zhuoyuan.li.fluttershareme.util.FileUtil;

/**
 * FlutterShareMePlugin
 */
public class FlutterShareMePlugin implements MethodCallHandler, FlutterPlugin, ActivityAware {

    private Activity activity;
    private static CallbackManager callbackManager;
    private MethodChannel methodChannel;

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final FlutterShareMePlugin instance = new FlutterShareMePlugin();
        instance.onAttachedToEngine(registrar.messenger());
        instance.activity = registrar.activity();
    }

    @Override
    public void onAttachedToEngine(FlutterPluginBinding binding) {
        onAttachedToEngine(binding.getBinaryMessenger());
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        methodChannel.setMethodCallHandler(null);
        methodChannel = null;
        activity = null;
    }

    private void onAttachedToEngine(BinaryMessenger messenger) {
        methodChannel = new MethodChannel(messenger, "flutter_share_me");
        methodChannel.setMethodCallHandler(this);
        callbackManager = CallbackManager.Factory.create();
    }

    /**
     * method
     *
     * @param call   methodCall
     * @param result Result
     */
    @Override
    public void onMethodCall(MethodCall call, @NonNull Result result) {
        String url, msg, appName;
        switch (call.method) {
            case "isAppInstalled":
                appName = call.argument("app");
                isAppInstalled(appName, result);
                break;
            case "shareFacebook":
                url = call.argument("url");
                msg = call.argument("msg");
                shareToFacebook(url, msg, result);
                break;
            case "shareTwitter":
                url = call.argument("url");
                msg = call.argument("msg");
                shareToTwitter(url, msg, result);
                break;
            case "shareWhatsApp":
                msg = call.argument("msg");
                url = call.argument("url");
                shareWhatsApp(url, msg, result, false);
                break;
            case "shareWhatsApp4Biz":
                msg = call.argument("msg");
                url = call.argument("url");
                shareWhatsApp(url, msg, result, true);
                break;
            case "system":
                msg = call.argument("msg");
                shareSystem(result, msg);
                break;
            case "shareMessenger":
                msg = call.argument("msg");
                shareMessenger(msg, result);
                break;
            case "shareSnapchat":
                msg = call.argument("msg");
                shareSnapchat(msg, result);
                break;
            case "shareSkype":
                msg = call.argument("msg");
                shareSkype(msg, result);
                break;
            case "shareInstagram":
                msg = call.argument("msg");
                shareInstagram(msg, result);
                break;
            case "copyToClipboard":
                msg = call.argument("msg");
                copyToClipboard(msg, result);
                break;
            case "shareSms":
                msg = call.argument("msg");
                shareSms(msg, result);
                break;
            default:
                result.notImplemented();
                break;
        }
    }

    /**
     * system share
     *
     * @param msg    String
     * @param result Result
     */
    private void shareSystem(Result result, String msg) {
        try {
            Intent textIntent = new Intent("android.intent.action.SEND");
            textIntent.setType("text/plain");
            textIntent.putExtra("android.intent.extra.TEXT", msg);
            activity.startActivity(Intent.createChooser(textIntent, "Share to"));
            result.success("success");
        } catch (Exception var7) {
            result.error("error", var7.toString(), "");
        }
    }

    /**
     * share to twitter
     *
     * @param url    String
     * @param msg    String
     * @param result Result
     */
    private void shareToTwitter(String url, String msg, Result result) {
        try {
            TweetComposer.Builder builder = new TweetComposer.Builder(activity)
                    .text(msg);
            if (url != null && url.length() > 0) {
                builder.url(new URL(url));
            }

            builder.show();
            result.success("success");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * share to Facebook
     *
     * @param url    String
     * @param msg    String
     * @param result Result
     */
    private void shareToFacebook(String url, String msg, Result result) {

        ShareDialog shareDialog = new ShareDialog(activity);
        // this part is optional
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                System.out.println("--------------------success");
            }

            @Override
            public void onCancel() {
                System.out.println("-----------------onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                System.out.println("---------------onError");
            }
        });

        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(url))
                .setQuote(msg)
                .build();
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            shareDialog.show(content);
            result.success("success");
        }

    }

    /**
     * share to whatsapp
     *
     * @param msg                String
     * @param result             Result
     * @param shareToWhatsAppBiz boolean
     */
    private void shareWhatsApp(String url, String msg, Result result, boolean shareToWhatsAppBiz) {
        if (!isAppInstalled(shareToWhatsAppBiz ? "com.whatsapp.w4b" : "com.whatsapp")) {
            Toast.makeText(activity, "WhatsApp not installed yet!", Toast.LENGTH_SHORT).show();
            result.error("error", "WhatsApp not installed yet!", "");
            return;
        }

        try {
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("text/plain");
            whatsappIntent.setPackage(shareToWhatsAppBiz ? "com.whatsapp.w4b" : "com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, msg);

            if (!TextUtils.isEmpty(url)) {
                FileUtil fileHelper = new FileUtil(activity, url);
                if (fileHelper.isFile()) {
                    whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    whatsappIntent.putExtra(Intent.EXTRA_STREAM, fileHelper.getUri());
                    whatsappIntent.setType(fileHelper.getType());
                }
            }
            whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(whatsappIntent);
            result.success("success");
        } catch (Exception var9) {
            result.error("error", var9.toString(), "");

            Toast.makeText(activity, "WhatsApp not installed yet!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttachedToActivity(ActivityPluginBinding binding) {
        activity = binding.getActivity();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {

    }

    @Override
    public void onReattachedToActivityForConfigChanges(ActivityPluginBinding binding) {
        activity = binding.getActivity();
    }

    @Override
    public void onDetachedFromActivity() {

    }

    private void isAppInstalled(String appName, Result result) {
        boolean isInstalled = true;
        if (appName != null) {
            switch (appName) {
                case "WhatsApp":
                    isInstalled = isAppInstalled("com.whatsapp");
                    break;
                case "Messenger":
                    isInstalled = isAppInstalled("com.facebook.orca");
                    break;
                case "Snapchat":
                    isInstalled = isAppInstalled("com.snapchat.android");
                    break;
                case "Skype":
                    isInstalled = isAppInstalled("com.skype.raider");
                    break;
                case "Instagram":
                    isInstalled = isAppInstalled("com.instagram.android");
                    break;
            }
        }

        if (isInstalled) {
            result.success("success");
        } else {
            Toast.makeText(activity, appName + " not installed yet!", Toast.LENGTH_SHORT).show();
            result.error("error", "App not installed", "");
        }
    }

    public boolean isAppInstalled(String packageName) {
        PackageManager pm = activity.getApplicationContext().getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        Log.d("Appppp", packageName+(app_installed));
        return app_installed;
    }

    private void shareMessenger(String msg, Result result) {
        if (!isAppInstalled("com.facebook.orca")) {
            Toast.makeText(activity, "Messenger not installed yet!", Toast.LENGTH_SHORT).show();
            result.error("error", "Messenger not installed yet!", "");
            return;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.setPackage("com.facebook.orca");
            intent.putExtra(Intent.EXTRA_TEXT, msg);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(intent);
            result.success("success");
        } catch (Exception var9) {
            result.error("error", var9.toString(), "");

            Toast.makeText(activity, "Messenger not installed yet!", Toast.LENGTH_SHORT).show();
        }
    }

    private void shareSnapchat(String msg, Result result) {
        if (!isAppInstalled("com.snapchat.android")) {
            Toast.makeText(activity, "Snapchat not installed yet!", Toast.LENGTH_SHORT).show();
            result.error("error", "Snapchat not installed yet!", "");
            return;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.setPackage("com.snapchat.android");
            intent.putExtra(Intent.EXTRA_TEXT, msg);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(intent);
            result.success("success");
        } catch (Exception var9) {
            result.error("error", var9.toString(), "");

            Toast.makeText(activity, "Snapchat not installed yet!", Toast.LENGTH_SHORT).show();
        }
    }

    private void shareSkype(String msg, Result result) {
        if (!isAppInstalled("com.skype.raider")) {
            Toast.makeText(activity, "Skype not installed yet!", Toast.LENGTH_SHORT).show();
            result.error("error", "Skype not installed yet!", "");
            return;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.setPackage("com.skype.raider");
            intent.putExtra(Intent.EXTRA_TEXT, msg);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(intent);
            result.success("success");
        } catch (Exception var9) {
            result.error("error", var9.toString(), "");

            Toast.makeText(activity, "Skype not installed yet!", Toast.LENGTH_SHORT).show();
        }
    }

    private void shareInstagram(String msg, Result result) {
        if (!isAppInstalled("com.instagram.android")) {
            Toast.makeText(activity, "Instagram not installed yet!", Toast.LENGTH_SHORT).show();
            result.error("error", "Instagram not installed yet!", "");
            return;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.setPackage("com.instagram.android");
            intent.putExtra(Intent.EXTRA_TEXT, msg);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            activity.startActivity(intent);
            result.success("success");
        } catch (Exception var9) {
            result.error("error", var9.toString(), "");
            Log.e("xins",var9.getMessage());
            Toast.makeText(activity, "Instagram not installed yet!", Toast.LENGTH_SHORT).show();
        }
    }

    private void copyToClipboard(String msg, Result result) {
        try {
            ClipboardManager clipboard = (ClipboardManager) activity.getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
            //把文本封装到ClipData中
            ClipData clip = ClipData.newPlainText("OyeVoice", msg);
            // Set the clipboard's primary clip.
            clipboard.setPrimaryClip(clip);

            result.success("success");
        } catch (Exception var9) {
            result.error("error", var9.toString(), "");
        }
    }

    private void shareSms(String msg, Result result) {
        Intent intent;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {  // Android 4.4 and up
                String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(activity);
                intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"));
                intent.putExtra("sms_body", msg);
                if (defaultSmsPackageName != null) {  // Can be null in case that there is no default, then the user would be able to choose any app that supports this intent.
                    intent.setPackage(defaultSmsPackageName);
                }
            } else {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setType("vnd.android-dir/mms-sms");
                intent.putExtra("sms_body", msg);
            }
            activity.startActivity(intent);
            result.success("success");
        } catch (Exception e) {
            Log.e("FlutterShareMePlugin", "", e);
            result.error("error", e.toString(), "");
        }
    }
}
