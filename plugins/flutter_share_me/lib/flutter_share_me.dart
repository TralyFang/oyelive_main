
import 'dart:io';

import 'package:flutter/services.dart';

class FlutterShareMe {
  final MethodChannel _channel = const MethodChannel('flutter_share_me');

  ///share to facebook
  Future<String?> shareToFacebook({String msg = '', String? url = ''}) async {
    final Map<String, Object?> arguments = Map<String, dynamic>();
    arguments.putIfAbsent('msg', () => msg);
    arguments.putIfAbsent('url', () => url);
    dynamic result;
    try {
      result = await _channel.invokeMethod('shareFacebook', arguments);
    } catch (e) {
      return "false";
    }
    return result;
  }

  ///share to twitter
  Future<String?> shareToTwitter({String? msg = '', String url = ''}) async {
    final Map<String, Object?> arguments = Map<String, dynamic>();
    arguments.putIfAbsent('msg', () => msg);
    arguments.putIfAbsent('url', () => url);
    dynamic result;
    try {
      result = await _channel.invokeMethod('shareTwitter', arguments);
    } catch (e) {
      return "false";
    }
    return result;
  }

  ///share to WhatsApp
  Future<String?> shareToWhatsApp({String? msg = '', String base64Image = ''}) async {
    final Map<String, Object?> arguments = Map<String, dynamic>();
    arguments.putIfAbsent('msg', () => msg);
    arguments.putIfAbsent('url', () => base64Image);
    dynamic result;
    try {
      result = await _channel.invokeMethod('shareWhatsApp', arguments);
    } catch (e) {
      return "false";
    }

    return result;
  }

  ///share to WhatsApp4Biz
  Future<String?> shareToWhatsApp4Biz({String msg = '', String base64Image = ''}) async {
    final Map<String, Object> arguments = Map<String, dynamic>() as Map<String, Object>;
    arguments.putIfAbsent('msg', () => msg);
    arguments.putIfAbsent('url', () => base64Image);
    dynamic result;
    try {
      result = await _channel.invokeMethod('shareWhatsApp4Biz', arguments);
    } catch (e) {
      return "false";
    }

    return result;
  }

  ///use system share ui
  Future<String?> shareToSystem({String? msg}) async {
    Map<String, Object?> arguments = Map<String, dynamic>();
    arguments.putIfAbsent('msg', () => msg);
    dynamic result;
    try {
      result = await _channel.invokeMethod('system', {'msg': msg});
    } catch (e) {
      return "false";
    }
    return result;
  }

  Future<String?> shareMessenger({String? msg}) async {
    dynamic result;
    try {
      result = await _channel.invokeMethod('shareMessenger', {'msg': msg});
    } catch (e) {
      return "false";
    }
    return result;
  }

  Future<String?> shareSnapchat({String? msg}) async {
    dynamic result;
    try {
      result = await _channel.invokeMethod('shareSnapchat', {'msg': msg});
    } catch (e) {
      return "false";
    }
    return result;
  }

  Future<String?> shareSkype({String? msg}) async {
    dynamic result;
    try {
      result = await _channel.invokeMethod('shareSkype', {'msg': msg});
    } catch (e) {
      return "false";
    }
    return result;
  }

  Future<String?> shareInstagram({String? msg}) async {
    dynamic result;
    try {
      result = await _channel.invokeMethod('shareInstagram', {'msg': msg});
    } catch (e) {
      return "false";
    }
    return result;
  }

  Future<String?> copyToClipboard({String? msg}) async {
    dynamic result;
    try {
      result = await _channel.invokeMethod('copyToClipboard', {'msg': msg});
    } catch (e) {
      return "false";
    }
    return result;
  }

  Future<bool> isAppInstalled({String? app}) async {
    print('check app installation : $app');
    dynamic result;
    try {
      result = await _channel.invokeMethod('isAppInstalled', {'app': app});
      print('$app installed: $result');
    } catch (e) {
      print('check app install failed : $app');
      return false;
    }
    return result == "success";
  }

  Future<String?> shareSms(String? msg, {String? url, String? trailingText}) async {
    Map<String, dynamic>? args;
    if (Platform.isIOS) {
      if (url == null) {
        args = <String, dynamic>{
          "msg": Uri.parse(msg!).toString(),
        };
      } else {
        args = <String, dynamic>{
          "msg": Uri.parse(msg! + " ").toString(),
          "urlLink": Uri.parse(url).toString(),
          "trailingText": Uri.parse(trailingText!).toString()
        };
      }
    } else if (Platform.isAndroid) {
      args = <String, dynamic>{
        "msg": msg! + url! + trailingText!,
      };
    }

    dynamic result;
    try {
      result = await await _channel.invokeMethod('shareSms', args);
    } catch (e) {
      return "false";
    }
    return result;
  }
}
